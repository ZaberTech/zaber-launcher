import {  createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';

export interface State {
  userPinned: boolean;
  mouseOver: boolean;
}

const initialState: State = {
  userPinned: false,
  mouseOver: false,
};

const pin = (state: State, { pin }: ActionsToPayloads[ActionTypes.PIN]): State => ({
  ...state,
  userPinned: pin,
});

const mouseOver = (state: State, { over }: ActionsToPayloads[ActionTypes.MOUSE_OVER]): State => ({
  ...state,
  mouseOver: over,
});

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.PIN]: pin,
  [ActionTypes.MOUSE_OVER]: mouseOver,
}, initialState);
