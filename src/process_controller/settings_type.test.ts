/* eslint-disable @typescript-eslint/ban-ts-comment,@typescript-eslint/no-unused-vars */

import { ProcessControllerMode as Mode } from '@zaber/motion/product';

import { hasAllModeSettings, ModeSettingNames, separatedSettings, SettingsMap } from './settings';

type ManualSettings = ModeSettingNames<Mode.MANUAL>;
type PidSettings = ModeSettingNames<Mode.PID>;

test('ModeSettingNames', () => {
  // @ts-ignore Only using this for type tests, value unused
  let _manualSetting: ManualSettings = 'process.voltage.on';
  // @ts-expect-error 'process.pid.kp' is not a valid manual mode setting
  _manualSetting = 'process.pid.kp';
  // @ts-ignore Only using this for type tests, value unused
  let _pidSetting: PidSettings = 'process.pid.kp';
  // @ts-expect-error 'process.voltage.on' is not a valid pid mode setting
  _pidSetting = 'process.voltage.on';
});

test('hasAllModeSettings', () => {
  const settings: SettingsMap = {};
  if (hasAllModeSettings(Mode.ON_OFF, settings)) {
    expect(settings['process.control.hysteresis.voltage']).not.toBeUndefined();
  } else {
    expect(settings['process.control.hysteresis.voltage']).toBeUndefined();
  }
});

test('separatedSettings', () => {
  const [general, pid] = separatedSettings(Mode.PID);
  expect(general).toContain('process.control.voltage.max');
  expect(pid).not.toContain('process.control.voltage.max');
  expect(general).not.toContain('process.pid.kp');
  expect(pid).toContain('process.pid.kp');
});
