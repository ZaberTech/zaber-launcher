import fs from 'fs';

import { stringify } from 'csv-stringify/sync';
import dataUriToBuffer from 'data-uri-to-buffer';
import _ from 'lodash';
import type { SagaIterator } from 'redux-saga';
import type { Observable } from 'rxjs';
import { all, call, delay, put, race, SagaReturnType, select, take, takeEvery } from 'redux-saga/effects';
import { Angle, ascii, CommandFailedException, InvalidDataException, Length, RequestTimeoutException, Time, Units } from '@zaber/motion';
import type { GetSetting, Oscilloscope, OscilloscopeData } from '@zaber/motion/ascii';
import { AsyncReturnType, ensureError, makeString, throwUnexpectedError } from '@zaber/toolbox';

import type { Logger, MonitorMessage, ZaberApi } from '../app_components';
import {
  ConnectionManagerActionTypes,
  connectionName,
  deviceLabelOrName,
  DevicesLoadedAction,
  getDevice,
  peripheralLabelOrName,
  selectConnections,
  selectIdentifiedAxes,
  selectIdentifiedDevices
} from '../connection_manager';
import { EntityKey, extractConnectionKey, getConnectionId, getDeviceAddress, getRouterUrl, makeAxisKey, makeDeviceKey } from '../keys';
import { getLogger } from '../log';
import { MessageRoutersService } from '../message_router';
import { getDefaultDimensionUnits, getUnitLabel, getUnits, Measurement } from '../units';
import { Action, RT, rxToEventChannel, tryAccess } from '../utils';
import { getContainer } from '../container';
import { TimeMeasuring } from '../time';
import { environment } from '../environment';
import { handleNonCriticalError } from '../errors';
import { selectOscilloscope } from '../store';
import { hasSetting } from '../devices';
import type { IUnitConverter } from '../zml';
import { MS_PER_SEC } from '../types';
import { Dialogs } from '../dialogs';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import {
  addAllPossibleMathChannels,
  createChannelForIo,
  createChannelForSetting,
  createEmptyRecordingForChannels,
  extractIoChannels,
  extractMathChannelsToCalculateForAllAxes,
  extractSettingsChannels,
  fillInMathRecordings,
  isMathChannel,
  makeIoChannelId,
  makeSettingChannelId,
  numFwChannelsUsed,
  splitChannelId,
  updateMathChannels
} from './channels';
import { autoSelectUnit, convertNativeData, groupByDimension, selectGroupUnit } from './dataConversion';
import type { CaptureSettings, ChannelGroup, Recording } from './reducers';
import {
  selectActiveRecording,
  selectSelectedDeviceKey,
  selectSelectedRecording,
  selectSelectedRecordingIndex,
  selectSavedRecordings,
  selectSelectedDeviceState,
  selectDevices,
  selectSelectedConnection,
  selectNewRecordingCaptureSettings,
  selectUnitConversionEnabled,
  selectAllChannels,
} from './selectors';
import {
  Mode,
  NEW_RECORDING_TAB,
  Sample,
  StorageBuffer,
  SignalType,
  UnitMap,
  YAxisId,
  Channel,
  RecordingBuffer,
  UnitConversionCache,
  MAX_DEVICE_CHANNELS,
  WAITING_FOR_TRIGGER_MESSAGE,
  WAITING_FOR_EXTERNAL_PRINT_MESSAGE,
} from './types';
import { ioPortTypeToZml, isNewRecTab, sortDimensionNames, zmlIoPortTypeToString } from './utils';
import { getDefaultColor } from './colors';


let logger: Logger;
export const MAX_POLLED_BUFFER_SIZE = environment.isTest ? 50 : 1000;
export const MATH_BUFFER_SIZE = MAX_POLLED_BUFFER_SIZE;
const SAMPLE_RATE_AVERAGING_WINDOW = 100;

const ERRMSG_NOT_RESPONDING = 'The device you have selected is not responding; try refreshing the connection.';
const ERRMSG_ALREADY_RECORDING = 'A scope recording is already in progress on the device; is it shared with other users or programs?';


export function* oscilloscopeSaga(): SagaIterator {
  logger = getLogger('oscilloscopeSaga');

  yield all([
    takeEvery(ActionTypes.ON_DEVICE_SELECTED, onDeviceSelected),
    takeEvery(ConnectionManagerActionTypes.DEVICES_LOADED, onDevicesLoaded),
    takeEvery(ActionTypes.CLOSE_ADD_MATH_DIALOG, onMathDialogClosed),
    takeEvery(ActionTypes.START, startRecording),
    takeEvery(ActionTypes.REPEAT, repeatRecording),
    takeEvery(ActionTypes.REPLACE, replaceRecording),
    takeEvery(ActionTypes.DOWNLOAD_EXISTING_DATA, recordCurrentData),
    takeEvery(ActionTypes.SAVE_RECORDING_TO_SPREADSHEET, saveRecordingToSpreadsheet),
    takeEvery(ActionTypes.SAVE_RECORDING_TO_IMAGE, saveRecordingToImage),

    // Events that cause bulk math channel calculation or Y axis range / units recalculation.
    takeEvery(ActionTypes.STORE_CHANNEL_DATA, updateSpecificChart),
    takeEvery(ActionTypes.REPLACE_CHANNELS, updateSpecificChart),
    takeEvery(ActionTypes.SET_CHANNEL_CHART_VISIBILITY, updateSpecificOrAllCharts),
    takeEvery(ActionTypes.SET_Y_AXIS, updateSpecificOrAllCharts),
    takeEvery(ActionTypes.SET_DIFFERENCING_SPAN, recalculateMathChannelsForSelectedChart),
    takeEvery(ActionTypes.DELETE_CHANNEL, updateSelectedOrAllCharts),
    takeEvery(ActionTypes.SET_SHARED_CHANNEL_SETTINGS, updateAllChartsIfSharingSettings),
  ]);
}

// Selects default settings channel names for a newly selected device.
export function getDefaultSettingNames(settingNames: string[]): string[] {
  return _.intersection(['pos', 'encoder.pos', 'lamp.flux', 'process.voltage', 'process.current'], settingNames);
}


function* getAccessLevel(device: ascii.Device): SagaIterator<number> {
  let accessLevel = 0;
  try {
    const settings = device.settings;
    accessLevel = yield call([settings, settings.get], 'system.access');
  } catch (err) {
    logger.warn(err);
    throwUnexpectedError(err);
    if (err instanceof CommandFailedException && err.details.responseData === 'BADCOMMAND') {
      throw new Error('Could not read the system.access setting from the device.');
    }
    if (err instanceof RequestTimeoutException) {
      throw new Error('The device is not responding.');
    }

    throw err;
  }

  return accessLevel;
}


function getConnectionAndDeviceName(deviceKey: EntityKey,
  connections: ReturnType<typeof selectConnections>,
  devices: ReturnType<typeof selectIdentifiedDevices>): string {
  const connectionKey = extractConnectionKey(deviceKey);
  const device = devices[deviceKey];
  return `${connectionName(connections[connectionKey])} > ${device.label ?? device.identity.name}`;
}


function* onDeviceSelected(): SagaIterator {
  // Only do this once per device.
  const deviceState = (yield select(selectSelectedDeviceState)) as ReturnType<typeof selectSelectedDeviceState>;
  if (deviceState.deviceId) {
    return;
  }

  const selectedDeviceKey: ReturnType<typeof selectSelectedDeviceKey> = yield select(selectSelectedDeviceKey);
  if (selectedDeviceKey) {
    let accessLevel: RT<typeof getAccessLevel>;
    const identifiedDevices: ReturnType<typeof selectIdentifiedDevices> = yield select(selectIdentifiedDevices);
    const deviceIdentity = identifiedDevices[selectedDeviceKey];
    if (!deviceIdentity) {
      return;
    }

    let zmlDevice: RT<typeof getDevice>;
    let numAxes = 0;
    try {
      zmlDevice = yield call(getDevice, selectedDeviceKey);
      accessLevel = yield call(getAccessLevel, zmlDevice) ?? 1;
      numAxes = zmlDevice.axisCount;
    } catch (err) {
      logger.warn(err);
      throwUnexpectedError(err);
      yield put(actions.setError(err.message));
      return;
    }

    // Determine if this device has a reshapable capture buffer.
    try {
      const deviceSettings = zmlDevice.settings;
      const maxChannels: number = yield call([deviceSettings, deviceSettings.get], 'scope.numchannels');
      const bufferSize: number = yield call([deviceSettings, deviceSettings.get], 'scope.channel.size.max');
      yield put(actions.storeAxisBufferInfo(selectedDeviceKey, maxChannels, bufferSize, true));
    } catch (err) {
      throwUnexpectedError(err);
      // Ignored because older devices don't have the buffer shape settings.
      if (!(err instanceof CommandFailedException)) {
        yield put(actions.setError(err.message));
        return;
      }
    }

    // Determine if this device has I/O pins.
    try {
      const ioInterface = zmlDevice.io;
      const info: ascii.DeviceIOInfo = yield call([ioInterface, ioInterface.getChannelsInfo]);
      yield put(actions.storeDeviceIoInfo(selectedDeviceKey, zmlDevice.firmwareVersion, info));
    } catch (err) {
      throwUnexpectedError(err);
      if (!(err instanceof CommandFailedException)) {
        yield put(actions.setError(err.message));
        return;
      }
    }

    // Scan the device and each axis for readable settings and unit conversion factors.
    if (zmlDevice.isIntegrated && zmlDevice.axisCount < 2) {
      yield call(scanDeviceOrAxis, selectedDeviceKey, zmlDevice, 0, accessLevel);
    } else {
      for (let axisNumber = 0; axisNumber <= numAxes; axisNumber++) {
        yield call(scanDeviceOrAxis, selectedDeviceKey, zmlDevice, axisNumber, accessLevel);
      }
    }
  }
}

function* onDevicesLoaded({
  payload: { connectionKey },
}: DevicesLoadedAction): SagaIterator {
  const { connectionManagerSelectedKey: selectedKey }: ReturnType<typeof selectOscilloscope> = yield select(selectOscilloscope);
  if (selectedKey && connectionKey === extractConnectionKey(selectedKey)) {
    yield put(actions.onDeviceSelected(selectedKey));
  }
}


function* scanDeviceOrAxis(deviceKey: string, device: ascii.Device, axisNumber: number, accessLevel: number): SagaIterator {
  let selectableSettings = [] as string[];
  let settings: ZaberApi.DeviceInfo['settings'] | null = null;
  let unitConverter: IUnitConverter | null = null;
  let conversionTable: ZaberApi.DeviceInfo['conversion_table'] | null = null;

  const connections: ReturnType<typeof selectConnections> = yield select(selectConnections);
  const identifiedDevices: ReturnType<typeof selectIdentifiedDevices> = yield select(selectIdentifiedDevices);
  const deviceIdentity = identifiedDevices[deviceKey];

  // Cache a list of all settings on the device or axis, and the dimension for signal generator units.
  if (!axisNumber) {
    unitConverter = device.settings;
    settings = deviceIdentity.product.settings;
    conversionTable = deviceIdentity.product.conversionTable;
    const name = getConnectionAndDeviceName(deviceKey, connections, identifiedDevices);
    yield put(actions.storeDeviceIdentity(deviceKey, name, deviceIdentity?.identity?.deviceId ?? 0));
    yield put(actions.storeAxisIdentity(deviceKey, 0, deviceLabelOrName(deviceIdentity), 0));
  } else {
    const axis = device.getAxis(axisNumber);
    const identifiedAxes: ReturnType<typeof selectIdentifiedAxes> = yield select(selectIdentifiedAxes);
    const axisState = identifiedAxes[makeAxisKey(deviceKey, axisNumber)];
    unitConverter = axis.settings;
    settings = axisState.product.settings;
    conversionTable = axisState.product.conversionTable;
    yield put(actions.storeAxisIdentity(
      deviceKey,
      axisNumber,
      peripheralLabelOrName(axisState) ?? 'Unidentified peripheral',
      axisState.identity?.peripheralId ?? 0));
  }

  if (settings) {
    selectableSettings = settings.rows
      .filter(row => (row.read_access_level ?? -1) <= accessLevel)
      .filter(row => row.realtime ?? false)
      .map(row => row.name);
  }

  yield put(actions.storeSelectableSettingNames(deviceKey, axisNumber, selectableSettings));

  // Generate default channels if not previously done.
  for (const settingName of getDefaultSettingNames(selectableSettings)) {
    const allChannels: ReturnType<typeof selectAllChannels> = yield select(selectAllChannels);
    if (numFwChannelsUsed(allChannels) >= MAX_DEVICE_CHANNELS) {
      break;
    }

    yield put(actions.addChannel(deviceKey, axisNumber, settingName));
    const id = makeSettingChannelId(settingName, axisNumber);
    yield put(actions.setChannelColor(NEW_RECORDING_TAB, id, getDefaultColor(id)));
  }

  const cache = cacheUnitConversions(settings, conversionTable, selectableSettings, unitConverter);
  yield put(actions.storeAxisUnitConversions(deviceKey, axisNumber, cache));
}


function cacheUnitConversions(settings: ZaberApi.DeviceInfo['settings'] | null,
  conversionTable: ZaberApi.DeviceInfo['conversion_table'] | null,
  selectableSettings: string[],
  unitConverter: IUnitConverter): UnitConversionCache {
  // Build a unit conversion table for this axis.
  const cache: UnitConversionCache = {
    baseDimensionScales: {},
    contextualDimensionScales: {},
    settingContextualDimensions: {},
  };

  if (settings && conversionTable && unitConverter) {
    const settingDimensions = _.keyBy(settings.rows, 'name');
    const dimensionsById = _.reduce(conversionTable.rows,
      (acc, row) => {
        acc[row.contextual_dimension_id] = row;
        return acc;
      }, {} as Record<number, ZaberApi.DeviceInfo['conversion_table']['rows'][0]>);

    const settingErrors: string[] = [];
    for (const settingName of selectableSettings) {
      const dimensionId = settingDimensions[settingName]?.contextual_dimension_id ?? null;

      if (dimensionId !== null) {
        const dimension = dimensionsById[dimensionId];
        cache.settingContextualDimensions[settingName] = dimensionId;
        if (dimension && !(dimensionId in cache.contextualDimensionScales)) {
          try {
            const scale = unitConverter.convertFromNativeUnits(settingName, 1, getDefaultDimensionUnits(dimension.dimension_name)) ?? null;
            if (scale !== null) {
              cache.contextualDimensionScales[dimensionId] = scale;
              if (dimension.dimension_name === dimension.contextual_dimension_name) {
                cache.baseDimensionScales[dimension.dimension_name] = scale;
              }
            }
          } catch {
            settingErrors.push(settingName);
          }
        }
      }
    }

    if (settingErrors) {
      logger.warn('Unable to obtain unit conversions for some settings or dimensions:', settingErrors);
    }
  }

  return cache;
}


function* startRecording(): SagaIterator {
  yield call(doRecording);
}


function* repeatRecording(): SagaIterator {
  yield call(doRecording, true);
}


function* replaceRecording({ payload: { recordingIndex } }: Action<ActionsToPayloads[ActionTypes.REPLACE]>): SagaIterator {
  yield call(doRecording, true, recordingIndex);
}


function* recordCurrentData({ payload: { recordingIndex } }: Action<ActionsToPayloads[ActionTypes.DOWNLOAD_EXISTING_DATA]>): SagaIterator {
  yield call(doRecording, true, recordingIndex ?? undefined);
}


function* doRecording(useSavedCaptureSettings: boolean = false, recordingIndex?: number): SagaIterator {
  logger.info('Oscilloscope started');
  let recording: ReturnType<typeof selectActiveRecording> = yield select(selectActiveRecording);
  if (!recording) {
    throw new Error('Bug: Active recording was not configured before starting capture.');
  }

  yield put(actions.setStopButtonEnabled(true));
  const settings: CaptureSettings = useSavedCaptureSettings ? recording : yield select(selectNewRecordingCaptureSettings);

  try {
    const { mode } = settings;
    switch (mode) {
      case Mode.POLLED:
        logger.info('Polled mode');
        yield race({
          poll: call(doPolledRecording, settings),
          stop: take(ActionTypes.STOP),
        });
        break;
      case Mode.BUFFERED_MANUAL:
        logger.info('Buffered manual mode');
        yield call(doBufferedRecording, settings);
        break;
      case Mode.EXTERNAL:
        logger.info('Buffered spy mode');
        yield call(doExternallyTriggeredRecording, settings);
        break;
      case Mode.DOWNLOAD:
        logger.info('Just download it mode');
        yield call(downloadExistingData, settings);
        break;
      default:
        throw new Error('Oscilloscope started without a mode selection.');
    }
  } catch (err) {
    logger.warn(err);
    throwUnexpectedError(err);
    if (err instanceof RequestTimeoutException) {
      yield put(actions.setError('The device is not responding.'));
    } else {
      yield put(actions.setError(err.message));
    }
  }

  recording = yield select(selectActiveRecording);
  if ((recording?.channels ?? []).some(channel => channel.samples.length > 0)) {
    yield put(actions.saveRecording(recordingIndex));
    logger.info('Oscilloscope stopped - new tab added.');
  } else {
    yield put(actions.discardRecording());
    logger.info('Oscilloscope stopped - no data was recorded.');
  }
}


function* doPolledRecording(captureSettings: CaptureSettings): SagaIterator {
  const recording: ReturnType<typeof selectActiveRecording> = yield select(selectActiveRecording);
  if (!recording) {
    return;
  }

  const time = getContainer().get(TimeMeasuring);
  let io: ascii.DeviceIO;
  let zmlDevice: ascii.Device;
  try {
    zmlDevice = yield call(getDevice, recording.deviceKey);
    io = zmlDevice.io;
  } catch {
    throw new Error(ERRMSG_NOT_RESPONDING);
  }

  const identifiedDevices: ReturnType<typeof selectIdentifiedDevices> = yield select(selectIdentifiedDevices);
  const deviceIdentity = identifiedDevices[recording.deviceKey];
  const hasUptime = hasSetting('system.uptime', deviceIdentity);
  const hasMultiget = hasSetting('get.settings.max', deviceIdentity);
  let maxGets = MAX_DEVICE_CHANNELS;
  if (hasMultiget) {
    maxGets = yield call([zmlDevice.settings, zmlDevice.settings.get], 'get.settings.max');
  }

  const channels = recording?.channels ?? [];
  const settingChannels = extractSettingsChannels(channels);
  const ioChannels = extractIoChannels(channels);
  const mathChannelsToUpdate = extractMathChannelsToCalculateForAllAxes(channels);
  const allData = createEmptyRecordingForChannels(channels, MAX_POLLED_BUFFER_SIZE);
  const recentData = createEmptyRecordingForChannels(channels, MATH_BUFFER_SIZE);
  const { mode, sampleRates, cDiffSpan } = captureSettings;
  const interval = MS_PER_SEC / sampleRates[mode];

  const ioPortsUsed = new Set(ioChannels.map(ch => {
    const { name } = splitChannelId(ch.id);
    return name;
  }));

  const ioGetters = {
    ai: ioPortsUsed.has('ai') ? readAnalogInputs : null,
    ao: ioPortsUsed.has('ao') ? readAnalogOutputs : null,
    di: ioPortsUsed.has('di') ? readDigitalInputs : null,
    do: ioPortsUsed.has('do') ? readDigitalOutputs : null,
  };

  const gets: GetSetting[] = settingChannels.map(ch => ({ setting: ch.name, axes: [ch.axis] }));
  const useDeviceTime = gets.length < maxGets && hasUptime && hasMultiget;
  let recordingStartTime: number | null = time.now();
  const ioStartTime = time.now();
  const userGetCount = gets.length;
  if (useDeviceTime) {
    recordingStartTime = null;
    if (!_.some(gets, g => g.setting === 'system.uptime')) {
      gets.push({ setting: 'system.uptime' });
    }
  }

  let rateWindowStart = time.now();
  let currentTime = 0;
  let numSamples = 0;
  let loopStart = time.now();
  while (true) {
    loopStart = time.now();
    try {
      const values: SagaReturnType<typeof zmlDevice.settings.getMany> =
        (yield call([zmlDevice.settings, zmlDevice.settings.getMany], ...gets));
      currentTime = (useDeviceTime ? values.find(v => v.setting === 'system.uptime')?.values[0] : null) ?? time.now();
      if (recordingStartTime == null) {
        recordingStartTime = currentTime;
      }

      for (let i = 0; i < userGetCount; i++) {
        const { id } = settingChannels[i];
        const value = values[i].values[0];
        const sample = { time: currentTime - recordingStartTime, value };
        allData[id].push(sample);
        recentData[id].push(sample);
      }

      // Read I/Os a port at a time, as needed.
      const ioData: _.Dictionary<number[]> = {};
      const ioTimes: _.Dictionary<number> = {};
      for (const [portType, saga] of Object.entries(ioGetters)) {
        if (saga != null) {
          ioTimes[portType] = time.now();
          ioData[portType] = yield call(saga, io);
        }
      }

      // Distribute I/O data among channels.
      for (const channel of ioChannels) {
        const { name: portType, axis: pin } = splitChannelId(channel.id);
        const data = ioData[portType];
        const sample = {
          time: ioTimes[portType] ? ioTimes[portType] - ioStartTime : currentTime - recordingStartTime,
          value: data[parseInt(pin!, 10) - 1],
        };
        allData[channel.id].push(sample);
        recentData[channel.id].push(sample);
      }
    } catch (err) {
      logger.warn(err);
      throwUnexpectedError(err);
      handleNonCriticalError(err, 'info');
      if (err instanceof CommandFailedException && err.details.responseData === 'BADCOMMAND') {
        throw new Error('Failed to read one of the selected settings or I/O channels from the device.');
      }

      throw err;
    }

    updateMathChannels(mathChannelsToUpdate, recentData, cDiffSpan, allData);

    yield put(actions.storeChannelData(allData));

    numSamples++;
    if (numSamples >= SAMPLE_RATE_AVERAGING_WINDOW) {
      const elapsedTime = time.now() - rateWindowStart;
      yield put(actions.storeActualSampleRate(Math.round(MS_PER_SEC * numSamples / elapsedTime)));
      numSamples = 0;
      rateWindowStart = time.now();
    }

    const delayTime = Math.max(0, interval - (time.now() - loopStart));
    if (delayTime > 0 || environment.isTest) {
      yield delay(environment.isTest ? 0 : delayTime);
    }
  }
}


function* readAnalogInputs(io: ascii.DeviceIO): SagaIterator<number[]> {
  return (yield call([io, io.getAllAnalogInputs])) as RT<typeof io.getAllAnalogInputs>;
}

function* readAnalogOutputs(io: ascii.DeviceIO): SagaIterator<number[]> {
  return (yield call([io, io.getAllAnalogOutputs])) as RT<typeof io.getAllAnalogOutputs>;
}

function* readDigitalInputs(io: ascii.DeviceIO): SagaIterator<number[]> {
  const bools: boolean[] = yield call([io, io.getAllDigitalInputs]);
  return bools.map(b => b ? 1 : 0);
}

function* readDigitalOutputs(io: ascii.DeviceIO): SagaIterator<number[]> {
  const bools: boolean[] = yield call([io, io.getAllDigitalOutputs]);
  return bools.map(b => b ? 1 : 0);
}


function* doBufferedRecording(captureSettings: CaptureSettings): SagaIterator {
  const recording: ReturnType<typeof selectActiveRecording> = yield select(selectActiveRecording)!;
  const allDeviceSettings: ReturnType<typeof selectDevices> = yield select(selectDevices);
  const deviceSettings = allDeviceSettings[recording!.deviceKey];

  let scope: Oscilloscope;
  let zmlDevice: ascii.Device;
  try {
    zmlDevice = yield call(getDevice, recording!.deviceKey);
    scope = zmlDevice.oscilloscope;
  } catch {
    throw new Error(ERRMSG_NOT_RESPONDING);
  }

  const { mode, sampleRates, startDelay, recordingDelay, cDiffSpan } = captureSettings;
  const sampleRate = sampleRates[mode];

  yield put(actions.reportProgress(-1, 'Configuring device...', false));

  // Configure FW oscilloscope.
  try {
    yield call([scope, scope.clear]);
    yield call([scope, scope.setDelay], recordingDelay);
    yield call([scope, scope.setTimebase], MS_PER_SEC / sampleRate);

    const actualSampleRate = MS_PER_SEC / ((yield call([scope, scope.getTimebase])) as number);
    yield put(actions.storeActualSampleRate(actualSampleRate));
  } catch (err) {
    if (err instanceof CommandFailedException && err.details.responseData === 'STATUSBUSY') {
      handleNonCriticalError(err, 'info');
      throw new Error(ERRMSG_ALREADY_RECORDING);
    }

    throw err;
  }

  // Add channels to the FW scope, combining digital I/Os.
  const addResult: BufferedChannelInfo = yield call(addChannelsForBufferedRecording, scope, recording!.channels);
  if (addResult == null) {
    return;
  }

  // Countdown for delay timer.
  const proceed: boolean = yield call(waitStartDelay, startDelay);
  if (!proceed) {
    return;
  }

  // Start signal generator and scope capture.
  const { signalAxis, signalReturnToStart: returnToStart, signalType, signalValue, samplesToCapture } = recording!;
  const { axis: zmlAxis, startPosition: pos }: RT<typeof startBufferedCapture> =
    yield call(startBufferedCapture, zmlDevice, signalType, signalAxis, signalValue, samplesToCapture);

  // Wait for capture to finish, while updating progress bar.
  let expectedSamples = (samplesToCapture ?? 1024) * addResult.channelsAdded;
  let captureTime = MS_PER_SEC * (samplesToCapture ?? 1024) / sampleRate;
  if (deviceSettings.bufferReshapable && samplesToCapture == null) {
    expectedSamples = deviceSettings.fwBufferSize;
    captureTime = MS_PER_SEC * (deviceSettings.fwBufferSize / addResult.channelsAdded) / sampleRate;
  }
  captureTime += recordingDelay;

  let cancelled: SagaReturnType<typeof progressTimer> = yield call(progressTimer, captureTime, 'Collecting...');
  if (cancelled) {
    try {
      yield call([scope, scope.stop]);
      if (zmlAxis) {
        yield call([zmlAxis, zmlAxis.stop]);
      }
    } catch (err) {
      logger.warn(err);
      throwUnexpectedError(err);
      // Not displaying errors when the capture is cancelled.
    }
  } else {
    // Retrieve captured data if any.
    yield put(actions.setStopButtonEnabled(false));
    let fwData: RT<typeof scope.read> = [];
    try {
      // The time estimate is based on average 25 bytes per message at typical ASCII serial baud rate because we
      // don't know what kind of connection we actually have.
      const downloadTime = MS_PER_SEC * 25 * expectedSamples / (115200 / 10);

      const result: {
        cancelled: RT<typeof progressTimer>;
        fwData: RT<typeof scope.read>;
      } = yield race({
        // We can't display an extrapolating progress bar because we can't measure how much of the data
        // has been received until it's finished.
        cancelled: call(progressTimer, downloadTime, 'Downloading...', true),
        fwData: call([scope, scope.read]),
      });

      if (result.fwData) {
        fwData = result.fwData;
      } else {
        cancelled = result.cancelled;
      }
    } catch (err) {
      yield call(handleScopeError, err);
      cancelled = true;
    }

    if (!cancelled && fwData.length > 0) {
      const allData: RT<typeof convertBufferData>
        = yield call(convertBufferData, zmlDevice, fwData, recording!.channels, addResult, cDiffSpan);
      yield put(actions.storeChannelData(allData));
    }
  }

  // Return to original position.
  if (zmlAxis && returnToStart) {
    yield call([zmlAxis, zmlAxis.genericCommandNoResponse], `move abs ${pos}`);
  }
}


interface BufferedChannelInfo {
  requestedPins: _.Dictionary<number[]>;
  di: { decode: boolean; output: boolean };
  do: { decode: boolean; output: boolean };
  channelsAdded: number;
}

function* addChannelsForBufferedRecording(scope: Oscilloscope, channels: Channel[]): SagaIterator<BufferedChannelInfo> {
  const settingsChannels = extractSettingsChannels(channels);
  const ioChannels = extractIoChannels(channels);
  let channelsAdded = 0;
  let channelBeingAdded = '';
  let decodeDi = false;
  let decodeDo = false;
  let outputDi = false;
  let outputDo = false;
  const requestedPins: _.Dictionary<number[]> = {};
  try {
    let diAdded = false;
    let doAdded = false;
    for (const channelData of settingsChannels) {
      channelBeingAdded = channelData.name;
      yield call([scope, scope.addChannel], channelData.axis, channelData.name);
      channelsAdded++;
      if (channelData.name === 'io.di.port') {
        diAdded = true;
        outputDi = true;
      } else if (channelData.name === 'io.do.port') {
        doAdded = true;
        outputDo = true;
      }
    }

    for (const channelData of ioChannels) {
      const { name: portType, axis: pin } = splitChannelId(channelData.id);
      if (portType === 'di') {
        decodeDi = true;
        requestedPins[portType] = (requestedPins[portType] ?? []).concat([parseInt(pin!, 10)]);
        if (!diAdded) {
          diAdded = true;
          channelsAdded++;
          yield call([scope, scope.addChannel], 0, 'io.di.port');
        }
      } else if (portType === 'do') {
        decodeDo = true;
        requestedPins[portType] = (requestedPins[portType] ?? []).concat([parseInt(pin!, 10)]);
        if (!doAdded) {
          doAdded = true;
          channelsAdded++;
          yield call([scope, scope.addChannel], 0, 'io.do.port');
        }
      } else {
        const { name: portType, axis: pin } = splitChannelId(channelData.id);
        yield call([scope, scope.addIoChannel], ioPortTypeToZml(portType), parseInt(pin!, 10));
        channelsAdded++;
      }
    }

    return {
      channelsAdded,
      requestedPins,
      di: { decode: decodeDi, output: outputDi },
      do: { decode: decodeDo, output: outputDo },
    };
  } catch (err) {
    if (err instanceof CommandFailedException) {
      handleNonCriticalError(err, 'info');
      switch (err.details.responseData) {
        case 'STATUSBUSY':
          throw new Error(ERRMSG_ALREADY_RECORDING);
        case 'FULL':
          throw new Error('You have selected too many settings for the device to record; please remove some.');
        case 'BADCOMMAND':
          throw new Error(`'${channelBeingAdded}' is not a valid channel name for the selected device.`);
        case 'BADDATA':
          throw new Error(
            `The setting '${channelBeingAdded}' cannot be recorded by the device's onboard oscilloscope; please remove it from the list.`);
      }
    }

    throw err;
  }
}


function* waitStartDelay(startDelay: number): SagaIterator<boolean> {
  const time = getContainer().get(TimeMeasuring);
  const delayStart = time.now();
  let increment = startDelay;
  while (increment > 0) {
    const timeLeft = startDelay - (time.now() - delayStart);
    yield put(actions.reportProgress(0, `Timer: ${Math.ceil(timeLeft / MS_PER_SEC)}s`, false));
    increment = Math.min(100, timeLeft);
    const { stop } = yield race({
      go: delay(increment),
      stop: take(ActionTypes.STOP),
    });

    if (stop) {
      yield put(actions.reportProgress(-1, 'Cancelled', false));
      return false;
    }
  }

  return true;
}


function* startBufferedCapture(
  zmlDevice: ascii.Device,
  signalType: SignalType,
  signalAxis: number,
  signalValue: Measurement,
  samplesToCapture: number | null):
  SagaIterator<{ axis: ascii.Axis; startPosition: number }> {
  yield put(actions.reportProgress(0, 'Starting movement...', false));
  let pos = 0;
  let zmlAxis: ascii.Axis | null = null;
  if (signalType !== SignalType.NONE) {
    try {
      zmlAxis = zmlDevice.getAxis(signalAxis ? signalAxis : 1);
      pos = yield call([zmlAxis, zmlAxis.getPosition], Units.NATIVE);
    } catch (err) {
      if (err instanceof CommandFailedException) {
        throw new Error(`Unable to read the current position of the axis. ${err.message}`);
      }
      throw err;
    }
  }

  // Start scope recording.
  try {
    const scope = zmlDevice.oscilloscope;
    yield call([scope, scope.start], samplesToCapture ?? undefined);
  } catch (err) {
    if (err instanceof CommandFailedException && err.details.responseData === 'STATUSBUSY') {
      throw new Error(ERRMSG_ALREADY_RECORDING);
    }
    throw err;
  }

  // Start signal generator.
  const signalMagnitude = signalValue?.value ?? 0;
  const signalUnit = getUnits(signalValue);
  if (zmlAxis) {
    try {
      switch (signalType) {
        case SignalType.MOVE_ABSOLUTE:
          yield call([zmlAxis, zmlAxis.moveAbsolute], signalMagnitude, signalUnit as Length | Angle, { waitUntilIdle: false });
          break;
        case SignalType.MOVE_RELATIVE:
          yield call([zmlAxis, zmlAxis.moveRelative], signalMagnitude, signalUnit as Length | Angle, { waitUntilIdle: false });
          break;
        default:
          break;
      }
    } catch (err) {
      if (err instanceof CommandFailedException) {
        throw new Error(`The axis rejected the signal generator's move command. ${err.message}`);
      } else {
        throw err;
      }
    }
  }

  return {
    axis: zmlAxis!,
    startPosition: pos,
  };
}


function convertBufferData(
  zmlDevice: ascii.Device,
  fwData: OscilloscopeData[],
  channels: Channel[],
  channelInfo: BufferedChannelInfo,
  cDiffSpan: number
): RecordingBuffer {
  const fwBuffers = fwData.map(data => data.getData());
  const bufferLength = fwBuffers[0].length; // All will be the same length.
  const allData = createEmptyRecordingForChannels(channels, bufferLength);
  const mathData = createEmptyRecordingForChannels(channels, MATH_BUFFER_SIZE);
  const mathChannelsToUpdate = extractMathChannelsToCalculateForAllAxes(channels);

  for (let i = 0; i < bufferLength; i++) {
    for (let channelIndex = 0; channelIndex < fwData.length; channelIndex++) {
      const data = fwData[channelIndex];
      let channelKey: string;
      if (data.dataSource === ascii.OscilloscopeDataSource.SETTING) {
        if ((data.setting === 'io.di.port' && channelInfo.di.decode) || (data.setting === 'io.do.port' && channelInfo.do.decode)) {
          const portType = data.setting.split('.')[1];
          for (const pin of channelInfo.requestedPins[portType]) {
            channelKey = makeIoChannelId(portType, pin);
            const sample: Sample = {
              time: data.getSampleTime(i),
              value: (fwBuffers[channelIndex][i] >> (pin - 1)) & 1,
            };

            allData[channelKey].push(sample);
            mathData[channelKey].push(sample);
          }

          // If the user did not also explicitly request the digital port, don't add the combined data.
          if ((data.setting === 'io.di.port' && !channelInfo.di.output) || (data.setting === 'io.do.port' && !channelInfo.do.output)) {
            continue;
          }
        }

        const axisNumber = zmlDevice.isIntegrated && zmlDevice.axisCount === 1 ? 0 : data.axisNumber;
        channelKey = makeSettingChannelId(data.setting, axisNumber);
      } else if (data.dataSource === ascii.OscilloscopeDataSource.IO) {
        channelKey = makeIoChannelId(zmlIoPortTypeToString(data.ioType), data.ioChannel);
      } else {
        throw new Error(`Unsupported oscilloscope data source ${makeString(data.dataSource)}.`);
      }

      const sample: Sample = {
        time: data.getSampleTime(i),
        value: fwBuffers[channelIndex][i],
      };

      allData[channelKey].push(sample);
      mathData[channelKey].push(sample);
    }

    updateMathChannels(mathChannelsToUpdate, mathData, cDiffSpan, allData);
  }

  return allData;
}


function* doExternallyTriggeredRecording(captureSettings: CaptureSettings): SagaIterator {
  const connection: ReturnType<typeof selectSelectedConnection> = yield select(selectSelectedConnection);
  const deviceKey: ReturnType<typeof selectSelectedDeviceKey> = yield select(selectSelectedDeviceKey);
  if (connection == null || !deviceKey) {
    return;
  }

  const deviceAddress = getDeviceAddress(deviceKey);

  const messageRouterService = getContainer().get(MessageRoutersService);
  const routerConnection: AsyncReturnType<typeof messageRouterService.getConnection> = yield call(
    [messageRouterService, messageRouterService.getConnection],
    getRouterUrl(connection.key));

  const monitorConnection = routerConnection.createMonitorConnection(getConnectionId(connection.key));
  try {
    yield call([monitorConnection, monitorConnection.start]);

    const { stop } = yield race({
      go: call(function* (): SagaIterator {
        yield put(actions.reportProgress(0, WAITING_FOR_TRIGGER_MESSAGE, false));
        yield call(waitForDeviceMessage, new RegExp(`^/0?${deviceAddress} .*scope start`), monitorConnection.monitorMessages);
        if (captureSettings.externalWaitForScopePrint) {
          yield put(actions.reportProgress(0, WAITING_FOR_EXTERNAL_PRINT_MESSAGE, false));
          yield call(waitForDeviceMessage, new RegExp(`^/0?${deviceAddress} .*scope print`), monitorConnection.monitorMessages);
          // The message router may batch messages at intervals of up to 500ms. Using 2000 as a safety margin.
          yield call(waitForEndOfDeviceMessages, new RegExp(`^#0?${deviceAddress} .* data `), 2000, monitorConnection.monitorMessages);
        }
      }),
      stop: take(ActionTypes.STOP),
    });

    if (stop) {
      yield put(actions.reportProgress(0, 'Cancelled', false));
      return;
    }
  } finally {
    yield call([monitorConnection, monitorConnection.stop]);
  }

  yield call(downloadAndReplace, connection.key, deviceAddress, captureSettings);
}


function* waitForDeviceMessage(match: RegExp, observable: Observable<MonitorMessage[]>): SagaIterator<MonitorMessage> {
  const channel = rxToEventChannel(observable);
  try {
    while (true) {
      const messages: MonitorMessage[] = yield take(channel);
      const first = messages.find(message => message.message.match(match));
      if (first) {
        return first;
      }
    }
  } finally {
    channel.close();
  }
}


function* waitForEndOfDeviceMessages(match: RegExp, timeout: number, observable: Observable<MonitorMessage[]>): SagaIterator {
  const time = getContainer().get(TimeMeasuring);
  const channel = rxToEventChannel(observable);
  try {
    let lastTime = time.now();
    while (time.now() - lastTime < timeout) {
      const { messages } = yield race({
        messages: take(channel),
        done: delay(timeout),
      });

      const first = (messages as MonitorMessage[] ?? []).find(message => message.message.match(match));
      if (first) {
        lastTime = time.now();
      }
    }
  } finally {
    channel.close();
  }
}


function* downloadExistingData(captureSettings: CaptureSettings): SagaIterator {
  const connection: ReturnType<typeof selectSelectedConnection> = yield select(selectSelectedConnection);
  const deviceKey: ReturnType<typeof selectSelectedDeviceKey> = yield select(selectSelectedDeviceKey);
  if (connection == null || !deviceKey) {
    return;
  }

  const deviceAddress = getDeviceAddress(deviceKey);
  // TODO when fw7-2918 is done: Enable the stop button here and poll until the device stops
  // capturing, if it is.

  yield call(downloadAndReplace, connection.key, deviceAddress, captureSettings);
}


function* downloadAndReplace(connectionKey: EntityKey, deviceAddress: number, captureSettings: CaptureSettings): SagaIterator {
  const recording: ReturnType<typeof selectActiveRecording> = yield select(selectActiveRecording);
  const deviceKey = makeDeviceKey(connectionKey, deviceAddress);
  const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
  const scope = zmlDevice.oscilloscope;

  const delayTime: number = yield call([scope, scope.getDelay], Time.MILLISECONDS);
  const timeBase: number = yield call([scope, scope.getTimebase], Time.MILLISECONDS);
  const maxSamples: number = yield call([scope, scope.getMaxBufferSize]);
  const numSamples: number = yield call([scope, scope.getBufferSize]);
  const sampleRate = MS_PER_SEC / timeBase;
  const waitTime = captureSettings.externalWaitForScopePrint ? 0 : delayTime + timeBase * numSamples;

  let cancelled: SagaReturnType<typeof progressTimer> = yield call(progressTimer, waitTime, 'Collecting...');
  if (cancelled) {
    return;
  }

  yield put(actions.setStopButtonEnabled(false));
  let fwData: RT<typeof scope.read> = [];
  try {
    const downloadTime = MS_PER_SEC * 25 * maxSamples / (115200 / 10);
    const result: {
      cancelled: RT<typeof progressTimer>;
      fwData: RT<typeof scope.read>;
    } = yield race({
      cancelled: call(progressTimer, downloadTime, 'Downloading...', true),
      fwData: call([scope, scope.read]),
    });

    if (result.fwData) {
      fwData = result.fwData;
    } else {
      cancelled = result.cancelled;
    }
  } catch (err) {
    yield call(handleScopeError, err);
    cancelled = true;
  }

  if (cancelled) {
    return;
  }

  const oldChannels = _.reduce(recording!.channels, (acc, chan) => {
    acc[chan.id] = chan;
    return acc;
  }, {} as Record<string, Channel>);

  const newChannels: Channel[] = [];
  const fwBuffers = fwData.map(data => data.getData());

  if (fwBuffers.length < 1 || fwBuffers[0].length < 1) {
    throw new Error('No data was returned by the device.');
  }

  const bufferLength = fwBuffers[0].length; // All will be the same length.

  for (let channelIndex = 0; channelIndex < fwData.length; channelIndex++) {
    const data = fwData[channelIndex];
    const axisNumber = zmlDevice.isIntegrated && zmlDevice.axisCount === 1 ? 0 : data.axisNumber;
    let id: string;
    if (data.dataSource === ascii.OscilloscopeDataSource.SETTING) {
      id = makeSettingChannelId(data.setting, axisNumber);
    } else if (data.dataSource === ascii.OscilloscopeDataSource.IO) {
      id = makeIoChannelId(zmlIoPortTypeToString(data.ioType), data.ioChannel);
    } else {
      throw new Error(`Unsupported oscilloscope data source ${makeString(data.dataSource)}`);
    }

    let channel: Channel;
    if (id in oldChannels) {
      channel = {
        ...oldChannels[id],
        samples: [],
        convertedData: [],
      };
    } else {
      if (data.dataSource === ascii.OscilloscopeDataSource.SETTING) {
        channel = createChannelForSetting(data.setting, axisNumber);
      } else if (data.dataSource === ascii.OscilloscopeDataSource.IO) {
        channel = createChannelForIo(zmlIoPortTypeToString(data.ioType), data.ioChannel);
      }

      channel!.color = getDefaultColor(channel!.id);
    }

    newChannels.push(channel!);

    for (let i = 0; i < bufferLength; i++) {
      const sample: Sample = {
        time: data.getSampleTime(i),
        value: fwBuffers[channelIndex][i],
      };

      channel!.samples.push(sample);
    }
  }

  yield put(actions.storeExternalSampleSettings(sampleRate, delayTime));
  yield put(actions.replaceChannelData(addAllPossibleMathChannels(newChannels)));
}


function* onMathDialogClosed(): SagaIterator {
  const recording: ReturnType<typeof selectSelectedRecording> = yield select(selectSelectedRecording);
  if (!recording) {
    return;
  }

  const recordingIndex: ReturnType<typeof selectSelectedRecordingIndex> = yield select(selectSelectedRecordingIndex);
  const newData = fillInMathRecordings(recording.channels, recording.cDiffSpan);
  yield put(actions.storeChannelData(newData, recordingIndex));

  // Recalculate axis ranges and units.
  if (!(yield call(updateAllChartsIfSharingSettings))) {
    yield call(updateChart, recordingIndex);
  }
}


function* updateSelectedOrAllCharts(): SagaIterator {
  if (!(yield call(updateAllChartsIfSharingSettings))) {
    const recordingIndex: ReturnType<typeof selectSelectedRecordingIndex> = yield select(selectSelectedRecordingIndex);
    yield call(updateChart, recordingIndex);
  }
}


type UpdateChartActionPayload = Action<
    ActionsToPayloads[ActionTypes.STORE_CHANNEL_DATA]
  | ActionsToPayloads[ActionTypes.SET_CHANNEL_CHART_VISIBILITY]
  | ActionsToPayloads[ActionTypes.SET_Y_AXIS]>;


function* updateSpecificChart({ payload: { recordingIndex } }: UpdateChartActionPayload): SagaIterator {
  yield call(updateChart, recordingIndex);
}


function* updateSpecificOrAllCharts({ payload: { recordingIndex } }: UpdateChartActionPayload): SagaIterator {
  if (!(yield call(updateAllChartsIfSharingSettings))) {
    yield call(updateChart, recordingIndex);
  }
}


function* updateAllChartsIfSharingSettings(): SagaIterator<boolean> {
  const oscilloscope: ReturnType<typeof selectOscilloscope> = yield select(selectOscilloscope);
  if (oscilloscope.shareChannelSettings) {
    for (let i = 0; i < oscilloscope.recordings.length; i++) {
      yield call(updateChart, i);
    }

    return true;
  }

  return false;
}


function* updateChart(recordingIndex: number | undefined): SagaIterator {
  recordingIndex = recordingIndex ?? NEW_RECORDING_TAB;
  let recording: Recording | null;
  if (recordingIndex === NEW_RECORDING_TAB) {
    recording = yield select(selectActiveRecording);
  } else {
    const recordings: ReturnType<typeof selectSavedRecordings> = yield select(selectSavedRecordings);
    recording = recordings[recordingIndex];
  }

  if (!recording) {
    return;
  }

  const axisIdentities: RT<typeof selectIdentifiedAxes> = yield select(selectIdentifiedAxes);
  const deviceIdentities: RT<typeof selectIdentifiedDevices> = yield select(selectIdentifiedDevices);
  const deviceStates: RT<typeof selectDevices> = yield select(selectDevices);

  const key = recording.deviceKey;
  const deviceIdentity = tryAccess(deviceIdentities, key);
  const deviceState = tryAccess(deviceStates, key);
  if (deviceState == null || deviceIdentity == null) { return }

  // Group and sort data channels into separate charts.
  const channels = recording.channels.filter(ch => ch.showInUi && ch.showInChart);
  const groups = groupByDimension(channels, key, deviceIdentities, axisIdentities);
  const groupOrder = Object.keys(groups);
  sortDimensionNames(groupOrder);

  // Auto-range units for each channel.
  const unitMap: UnitMap = {};
  for (const channel of channels) {
    const axisIdentity = axisIdentities[channel.axis ? makeAxisKey(key, channel.axis) : key];
    unitMap[channel.id] =
      autoSelectUnit(channel, deviceIdentity?.product, axisIdentity?.product, deviceState.axes[channel.axis].unitConversionCache);
  }

  const convertedChannelData: StorageBuffer = {};
  const channelGroups: ChannelGroup[] = groupOrder.map(dimensionName => {
    const groupChannels = groups[dimensionName] ?? [];
    // Channels that are on the same axis of the same chart need a common unit.
    const leftChannels = groupChannels.filter(ch => ch.yAxis === YAxisId.LEFT);
    const rightChannels = groupChannels.filter(ch => ch.yAxis === YAxisId.RIGHT);
    for (const axisGroup of [leftChannels, rightChannels]) {
      const commonUnit = selectGroupUnit(axisGroup.map(ch => unitMap[ch.id]));
      for (const channel of axisGroup) {
        unitMap[channel.id] = commonUnit;
        const { name: settingName } = splitChannelId(channel.id);
        convertedChannelData[channel.id] =
          convertNativeData(channel.samples,
            channel.prescale,
            commonUnit,
            settingName,
            deviceState.axes[channel.axis].unitConversionCache,
          );
      }
    }

    return {
      channelIds: groups[dimensionName].map(ch => ch.id),
    };
  });

  yield put(actions.storeChartData(recordingIndex, channelGroups, unitMap, convertedChannelData));
}


function* recalculateMathChannelsForSelectedChart(): SagaIterator {
  const recordingIndex: ReturnType<typeof selectSelectedRecordingIndex> = yield select(selectSelectedRecordingIndex);
  if (!isNewRecTab(recordingIndex)) {
    const recording: NonNullable<ReturnType<typeof selectSelectedRecording>> = yield select(selectSelectedRecording);
    const clearedMath = recording.channels.map(ch => ({
      ...ch,
      samples: isMathChannel(ch) ? [] : ch.samples,
    }));

    const newData = fillInMathRecordings(clearedMath, recording.cDiffSpan);
    yield put(actions.storeChannelData(newData, recordingIndex));
    yield call(updateChart, recordingIndex);
  }
}


type SaveSpreadsheetActionPayload = Action<ActionsToPayloads[ActionTypes.SAVE_RECORDING_TO_SPREADSHEET]>;
function* saveRecordingToSpreadsheet({ payload: { recordingIndex } }: SaveSpreadsheetActionPayload) {
  const recordings: ReturnType<typeof selectSavedRecordings> = yield select(selectSavedRecordings);
  const recording = recordings?.[recordingIndex];
  if (!recording) {
    return;
  }

  const useConverted: boolean = yield select(selectUnitConversionEnabled);
  const dialogResult: SagaReturnType<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
    title: 'Save Data to CSV File',
    filters: [{ name: 'CSV files', extensions: ['csv'] }, { name: 'All files', extensions: ['*'] }],
    defaultPath: `${recording.title}.csv`,
  });

  if (dialogResult.canceled) {
    return;
  }

  if (dialogResult.filePath == null) {
    yield put(actions.setError('Cannot save a file in the selected location.'));
    return;
  }

  const channelMap = _.keyBy(recording.channels, ch => ch.id);
  const { groups, unitMap } = recording;

  const channelTitle = (channel: Channel): string =>
    `${isMathChannel(channel) ? 'Calculated ' : ''}${channel.title}${channel.axis ? ` (axis ${channel.axis})` : ''}`;

  type Cell = (string | number | null);
  const columns: Cell[][] = [];
  const allIds = _.flatten(groups.map(group => group.channelIds));

  // Store data as columns first because it's simpler for our data format.
  allIds.forEach(id => {
    let column: Cell[] = [];
    const channel = channelMap[id];
    column.push(channelTitle(channel));
    column.push('Time (ms)');
    column = column.concat((useConverted ? channel.convertedData : channel.samples).map(sample => sample.time));
    columns.push(column);
    column = [];
    column.push(null);

    let nativeValueLabel = 'Native';
    if (isMathChannel(channel)) {
      if (channel.title.includes('Velocity')) {
        nativeValueLabel = 'Native position units/s';
      } else if (channel.title.includes('Acceleration')) {
        nativeValueLabel = 'Native position units/s²';
      }
    }

    column.push(`Value (${useConverted ? getUnitLabel(unitMap[channel.id]) : nativeValueLabel})`);
    column = column.concat((useConverted ? channel.convertedData : channel.samples).map(sample => sample.value));
    columns.push(column);
  });

  // Transpose the data to row format for the CSV output.
  const maxRows = _.max(columns.map(col => col.length)) ?? 0;
  const rows: Cell[][] = [];
  for (let ri = 0; ri < maxRows; ri++) {
    rows.push(Array<number | null>(columns.length).fill(null));
    for (let ci = 0; ci < columns.length; ci++) {
      rows[ri][ci] = columns[ci][ri] ?? null;
    }
  }

  try {
    const text =  stringify(rows);
    yield call(fs.promises.writeFile, dialogResult.filePath, text, { encoding: 'utf-8', flag: 'w' });
  } catch (err) {
    logger.warn(err);
    yield put(actions.setError(`There was an I/O error saving the data to disk. ${ensureError(err).toString()}`));
  }
}


type SaveImageActionPayload = Action<ActionsToPayloads[ActionTypes.SAVE_RECORDING_TO_IMAGE]>;
function* saveRecordingToImage({ payload: { format, data } }: SaveImageActionPayload) {
  const recording: ReturnType<typeof selectSelectedRecording> = yield select(selectSelectedRecording);
  if (!recording) {
    return;
  }

  const dialogResult: SagaReturnType<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
    title: `Save Chart to ${format.toLocaleUpperCase()} File`,
    properties: ['showOverwriteConfirmation'],
    filters: [{ name: `${format.toLocaleUpperCase()} files`, extensions: [format] }, { name: 'All files', extensions: ['*'] }],
    defaultPath: `${recording.title}.${format}`,
  });

  if (dialogResult.canceled) {
    return;
  }

  if (dialogResult.filePath == null) {
    yield put(actions.setError('Cannot save a file in the selected location.'));
    return;
  }

  try {
    const image = dataUriToBuffer(data);
    yield call(fs.promises.writeFile, dialogResult.filePath, image, { encoding: 'binary', flag: 'w' });
  } catch (err) {
    logger.warn(err);
    yield put(actions.setError(`There was an error saving the file: ${ensureError(err)}`));
  }
}


function* progressTimer(duration: number, message: string, dontExit: boolean | undefined = false): SagaIterator<boolean> {
  const time = getContainer().get(TimeMeasuring);
  const startTime = time.now();
  const progress = () => ((time.now() - startTime) / duration);
  let increment = duration;
  while (increment > 0 || dontExit) {
    yield put(actions.reportProgress(Math.min(progress(), 1), message, true));
    increment = Math.min(environment.isTest ? 0 : 100, duration - (time.now() - startTime));
    const { stop } = yield race({
      go: delay(increment),
      stop: take(ActionTypes.STOP),
    });

    if (stop) {
      yield put(actions.reportProgress(0, 'Cancelled', false));
      return true;
    }
  }

  return false;
}

function* handleScopeError(err: unknown): SagaIterator {
  logger.warn(err);
  throwUnexpectedError(err);
  handleNonCriticalError(err, 'info');
  let message = 'Unable to retrieve the recorded data from the device.';
  if (err instanceof InvalidDataException) {
    message = [
      'Received oscilloscope data are invalid.',
      'The oscilloscope puts strain on the communication channel and may cause data corruption with some USB cables.',
      'Make sure to use original Zaber equipment and keep your computer up to date.',
      `Error: ${err.message}`,
    ].join(' ');
  }
  yield put(actions.setError(message));
}
