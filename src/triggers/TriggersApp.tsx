import React, { useEffect }  from 'react';
import { useSelector } from 'react-redux';
import _ from 'lodash';

import { ConnectionsView, selectDevices } from '../connection_manager';
import { NoContentMessage, Title } from '../components';
import { RT, useActions } from '../utils';
import type { EntityKey } from '../keys';
import { usePolledTriggers } from '../polling/hooks';

import { actions as actionsDefinition } from './actions';
import { selectDeviceError, selectHasTriggers, selectSelectedDevice, selectTriggersExpanded, selectTriggersTriggers } from './selectors';
import { Trigger } from './Trigger';

export const TriggersApp: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const selectedDevice = useSelector(selectSelectedDevice);

  return (<div className="connection-view-and-app">
    <Title>Triggers</Title>

    <ConnectionsView
      selectable={['device']}
      selected={selectedDevice}
      onSelect={actions.setSelectedDevice}
    />
    <div className="app-ui triggers-app" key={selectedDevice}>
      {selectedDevice == null ?
        <NoContentMessage title="Welcome to Triggers" message="Select a device from the menu on the left."/>
        : <Triggers deviceKey={selectedDevice}/>}
    </div>
  </div>);
};

const Triggers: React.FC<{ deviceKey: EntityKey}> = ({ deviceKey }) => {
  const actions = useActions(actionsDefinition);

  const hasTriggers = useSelector(selectHasTriggers);
  const triggersExpanded = useSelector(selectTriggersExpanded);
  const triggers = useSelector(selectTriggersTriggers);
  const deviceError = useSelector(selectDeviceError);

  const title = useSelector(selectDevices)[deviceKey]?.identity?.name ?? 'Triggers';

  const enabledStatesResponse: RT<typeof usePolledTriggers> = usePolledTriggers(deviceKey);

  useEffect(() => {
    actions.deviceMount(deviceKey);
    return () => {
      actions.deviceUnmount(deviceKey);
    };
  }, [deviceKey]);

  useEffect(() => {
    if (hasTriggers) {
      if (enabledStatesResponse.type === 'triggers') {
        actions.setDeviceError(deviceKey, null);
      } else if (enabledStatesResponse.type === 'error') {
        actions.setDeviceError(deviceKey, enabledStatesResponse.details);
      }
    }
  }, [enabledStatesResponse]);

  if (hasTriggers === false) {
    return <NoContentMessage title={title} type="info" message="The selected device does not support triggers."/>;
  } else if (deviceError != null) {
    return <NoContentMessage title={title} type="error" message={deviceError}/>;
  } else if (hasTriggers === null) {
    return <NoContentMessage data-testid="triggers-loading" title={title} message="Loading..." type="working"/>;
  } else {
    return <>{_.zip(triggers, triggersExpanded).map(([trigger, triggerExpanded], i) => {
      if (trigger == null || triggerExpanded == null) {
        return null;
      } else {
        return <Trigger
          key={i}
          deviceKey={deviceKey}
          triggerIndex={i}
          enabledState={
            (enabledStatesResponse.type === 'triggers' && enabledStatesResponse.states[i]) ?
              enabledStatesResponse.states[i]
              : null
          }
          triggerState={trigger}
          expanded={triggerExpanded}
        />;
      }
    })}
    </>;
  }
};
