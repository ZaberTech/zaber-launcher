jest.useFakeTimers();

jest.mock('fs', () => ({
  promises: {
    access: jest.fn().mockResolvedValue(true),
    mkdir: jest.fn(),
    unlink: jest.fn(),
    stat: jest.fn().mockResolvedValue({ size: 1024 }),
  },
}));

// transitively included via ../test
jest.mock('electron', () => ({
  ipcRenderer: {
    on: jest.fn(),
    send: jest.fn(),
  }
}));

jest.mock('@electron/remote', () => ({
  app: {
    getPath: jest.fn().mockReturnValue('/electron/mock/path'),
    getName: () => 'ElectronMock',
  },
  getCurrentWindow: jest.fn().mockReturnValue({ id: 1 }),
}));

jest.mock('@zaber/motion', () => {
  const actual = jest.requireActual('@zaber/motion');
  return {
    ...actual,
    Library: {
      ...actual.Library,
      setLogOutput: jest.fn(),
    },
    LogOutputMode: {
      FILE: 'file',
      STDERR: 'stderr',
      OFF: 'off',
    }
  };
});

import fs from 'fs';
import path from 'path';

import moment from 'moment';
import * as zml from '@zaber/motion';
import { Container, injectable } from 'inversify';

import { PreferencesService } from '../preferences/service';
import { PreferenceKeys, PreferenceValue } from '../preferences/types';
import { createContainer, destroyContainer } from '../container';
import { waitUntilPass } from '../test';

import { ZMLLoggingService } from './logging_service';
import { DEFAULT_LOGGING_SETTINGS, LoggingSettings, loggingSettingsSymbol } from './logging_settings';

function osSpecificPath(genericPath: string): string {
  return genericPath.replace(/\//g, path.sep);
}

function genericPath(osSpecificPath: string): string {
  return osSpecificPath.replace(/\\/g, '/');
}

const mockFiles = [
  '2024-07-01-00_00_00-log-window-1.txt',
  '2024-07-01-00_00_01-log-window-1.txt',
  '2024-07-01-00_00_00-log-window-2.txt',
  '2024-07-01-00_00_01-log-window-2.txt',
];

const mockFolderPath = `/electron/mock/path/${ZMLLoggingService.LOG_FOLDER_NAME}`;

@injectable()
class PreferencesServiceMock {
  public preferences: _.Dictionary<PreferenceValue> = {
    [PreferenceKeys.ZML_LOGGING]: true,
  };

  public initialize = jest.fn(() => null);
  public registerHandler = jest.fn();
  public getPreferenceValue = jest.fn(() => this.preferences[PreferenceKeys.ZML_LOGGING]);
  public setPreferenceValue = jest.fn((key: string, value: PreferenceValue) => this.preferences[key] = value);
}

let container: Container;
let service: ZMLLoggingService | null = null;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(PreferencesService).to(PreferencesServiceMock);
  container.bind(loggingSettingsSymbol).toConstantValue(DEFAULT_LOGGING_SETTINGS);

  fs.promises.readdir = jest.fn().mockResolvedValue(mockFiles);
});


afterEach(() => {
  destroyContainer();
  jest.clearAllMocks();
  service = null;
});

describe('constructor', () => {
  beforeEach(() => {
    service = container.get(ZMLLoggingService);
  });

  test('sets log output mode to STDERR in development', () => {
    expect(zml.Library.setLogOutput).toHaveBeenCalledTimes(1);
    expect(zml.Library.setLogOutput).toHaveBeenCalledWith(zml.LogOutputMode.STDERR);
  });

  test('registers handler', () => {
    expect(container.get(PreferencesService).registerHandler).toHaveBeenCalledTimes(1);
    expect(container.get(PreferencesService).registerHandler).toHaveBeenCalledWith(
      PreferenceKeys.ZML_LOGGING, expect.any(Function)
    );
  });
});

describe('timeout handlers', () => {
  let loggingSettings: LoggingSettings;

  beforeEach(() => {
    loggingSettings = {
      maxLogFiles: 10,
      maxLogSize: 1024,
      sizeCheckInterval: 1000,
    };
    container.rebind(loggingSettingsSymbol).toConstantValue(loggingSettings);
  });

  afterEach(() => {
    jest.clearAllTimers();
    jest.clearAllMocks();
  });

  test('handleDayChange', async () => {
    loggingSettings.sizeCheckInterval = 1000000000;
    container.rebind(loggingSettingsSymbol).toConstantValue(loggingSettings);
    service = container.get(ZMLLoggingService);

    await service.setLogToFile(true);

    (zml.Library.setLogOutput as jest.Mock).mockClear();
    const timeToMidnight = 86400001;
    jest.advanceTimersByTime(timeToMidnight);

    await waitUntilPass(() => {
      expect(zml.Library.setLogOutput).toHaveBeenCalledTimes(1);
      expect(zml.Library.setLogOutput).toHaveBeenCalledWith(zml.LogOutputMode.FILE, expect.any(String));
    });
  });

  test('checkLogfileSize -- file size equal to or less than max', async () => {
    Date.now = jest.fn(() => new Date('2020-05-13T00:00:00.000Z').getTime());
    fs.promises.stat = jest.fn().mockResolvedValue({ size: 1024 });
    service = container.get(ZMLLoggingService);

    await service.setLogToFile(true);

    (fs.promises.stat as jest.Mock).mockClear();
    (zml.Library.setLogOutput as jest.Mock).mockClear();
    jest.advanceTimersByTime(1000);

    await waitUntilPass(() => {
      expect(fs.promises.stat).toHaveBeenCalledTimes(1);
      expect(zml.Library.setLogOutput).toHaveBeenCalledTimes(0);
    });
  });

  test('checkLogfileSize -- file size greater than max', async () => {
    Date.now = jest.fn(() => new Date('2020-05-13T00:00:00.000Z').getTime());
    fs.promises.stat = jest.fn().mockResolvedValue({ size: 1024 });
    loggingSettings.maxLogSize = 1023;

    service = container.get(ZMLLoggingService);

    await service.setLogToFile(true);

    (fs.promises.stat as jest.Mock).mockClear();
    (zml.Library.setLogOutput as jest.Mock).mockClear();
    jest.advanceTimersByTime(1000);

    await waitUntilPass(() => {
      expect(fs.promises.stat).toHaveBeenCalledTimes(1);
      expect(zml.Library.setLogOutput).toHaveBeenCalledTimes(1);
      expect(zml.Library.setLogOutput).toHaveBeenCalledWith(zml.LogOutputMode.FILE, expect.any(String));
    });
  });

  test('checkLogfileSize -- doesnt fail when stat throws error', async () => {
    Date.now = jest.fn(() => new Date('2020-05-13T00:00:00.000Z').getTime());
    fs.promises.stat = jest.fn().mockRejectedValue(new Error('File not found'));
    container.rebind(loggingSettingsSymbol).toConstantValue(loggingSettings);

    service = container.get(ZMLLoggingService);

    await service.setLogToFile(true);

    (fs.promises.stat as jest.Mock).mockClear();
    (zml.Library.setLogOutput as jest.Mock).mockClear();
    jest.advanceTimersByTime(1000);

    await waitUntilPass(() => {
      expect(fs.promises.stat).toHaveBeenCalledTimes(1);
      expect(zml.Library.setLogOutput).toHaveBeenCalledTimes(0);
    });
  });
});

describe('setLogToFile', () => {
  test('enable logging is idempotent', async () => {
    service = container.get(ZMLLoggingService);

    (zml.Library.setLogOutput as jest.Mock).mockClear();
    await service.setLogToFile(true);

    expect(zml.Library.setLogOutput).toHaveBeenCalledTimes(1);
    expect(zml.Library.setLogOutput).toHaveBeenCalledWith(zml.LogOutputMode.FILE, expect.any(String));

    await service.setLogToFile(true);

    expect(zml.Library.setLogOutput).toHaveBeenCalledTimes(1);
  });

  test('disable logging is idempotent', async () => {
    service = container.get(ZMLLoggingService);

    (zml.Library.setLogOutput as jest.Mock).mockClear();
    await service.setLogToFile(false);
    expect(zml.Library.setLogOutput).toHaveBeenCalledTimes(0);

    await service.setLogToFile(false);
    expect(zml.Library.setLogOutput).toHaveBeenCalledTimes(0);
  });

  test('updates log file path', async () => {
    service = container.get(ZMLLoggingService);

    expect(service.logFilePath).toBeNull();

    await service.setLogToFile(true);
    expect(typeof service.logFilePath).toBe('string');
  });
});

describe('requestFilePath', () => {
  let loggingSettings: LoggingSettings;

  beforeEach(() => {
    loggingSettings = DEFAULT_LOGGING_SETTINGS;
  });

  test('call mkdir if zml log folder doesnt exist', async () => {
    service = container.get(ZMLLoggingService);
    fs.promises.access = jest.fn().mockRejectedValue(new Error('Folder does not exist'));

    await service.requestFilePath();

    expect(fs.promises.mkdir).toHaveBeenCalledTimes(1);
    expect(fs.promises.mkdir).toHaveBeenCalledWith(osSpecificPath(mockFolderPath), { recursive: true });
  });

  test('removes older files from folder - window 1', async () => {
    loggingSettings.maxLogFiles = 1;
    container.rebind(loggingSettingsSymbol).toConstantValue(loggingSettings);
    service = container.get(ZMLLoggingService);

    await service.requestFilePath();

    // files associated with window 1 should be removed
    expect(fs.promises.unlink).toHaveBeenCalledTimes(2);
    for (let i = 0; i < 2; i++) {
      const file = path.join(mockFolderPath, mockFiles[i]);
      expect(fs.promises.unlink).toHaveBeenNthCalledWith(i + 1, file);
    }
  });

  test('removes some but not all files from folder', async () => {
    loggingSettings.maxLogFiles = 2;
    container.rebind(loggingSettingsSymbol).toConstantValue(loggingSettings);
    service = container.get(ZMLLoggingService);

    await service.requestFilePath();

    expect(fs.promises.unlink).toHaveBeenCalledTimes(1);
    const file = path.join(mockFolderPath, mockFiles[0]);
    expect(fs.promises.unlink).toHaveBeenNthCalledWith(1, file);
  });

  test('no files removed when fewer than max files', async () => {
    loggingSettings.maxLogFiles = 3;
    container.rebind(loggingSettingsSymbol).toConstantValue(loggingSettings);
    service = container.get(ZMLLoggingService);

    await service.requestFilePath();

    expect(fs.promises.unlink).toHaveBeenCalledTimes(0);
  });
});

test('ZMLLoggingservice.computeTimeToMidnight from various times of day', () => {
  let now = moment('2024-01-01T00:00:00');
  let result = ZMLLoggingService.computeTimeToMidnight(now);
  expect(result).toBe(86400000);

  now = moment('2024-01-01T12:00:00');
  result = ZMLLoggingService.computeTimeToMidnight(now);
  expect(result).toBe(43200000);

  now = moment('2024-01-02T23:00:00');
  result = ZMLLoggingService.computeTimeToMidnight(now);
  expect(result).toBe(3600000);

  now = moment('2024-01-03T23:30:00');
  result = ZMLLoggingService.computeTimeToMidnight(now);
  expect(result).toBe(1800000);

  now = moment('2024-01-04T23:59:30');
  result = ZMLLoggingService.computeTimeToMidnight(now);
  expect(result).toBe(30000);

  now = moment('2024-01-05T23:59:59');
  result = ZMLLoggingService.computeTimeToMidnight(now);
  expect(result).toBe(1000);
});

test('file path formatted correctly', async () => {
  service = container.get(ZMLLoggingService);
  const path = await service.requestFilePath();
  expect(genericPath(path)).toMatch(
    /electron\/mock\/path\/zmlLogs\/\d{4}-\d{2}-\d{2}-\d{2}_\d{2}_\d{2}-log-window-\d+\.txt/
  );
});
