export interface LoginData {
  email: string;
  password: string;
}

export enum AuthState {
  Authenticated,
  Checking,
  Unauthenticated,
}

export enum CustomAttributes {
  IdentityId = 'custom:identity_id'
}

export interface LoggedInUser {
  sub: string;
  email: string;
  [CustomAttributes.IdentityId]: string;
}

export interface CloudSharingStatus {
  sharingOn: boolean;
  id: string | null;
  token: string | null;
  isConnected: boolean;
}

export interface DiscoveredClient {
  id: string;
  url: string;
  name: string;
}
