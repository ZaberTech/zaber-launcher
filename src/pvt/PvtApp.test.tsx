const mockOpenDialog = jest.fn(() => ({
  cancelled: false,
  filePaths: ['foo.csv'],
}));

const saveFileName: string = 'bar.csv';
const mockSaveDialog = jest.fn(() => ({
  cancelled: false,
  filePath: saveFileName,
}));

const messageBoxResult: number = 0;
const mockMessageBox = jest.fn(async () => ({
  response: messageBoxResult,
}));

jest.mock('../dialogs', () => ({
  Dialogs: {
    showOpenDialog: mockOpenDialog,
    showSaveDialog: mockSaveDialog,
    showMessageBox: mockMessageBox,
  },
}));

let fileContent: string = '';
let fileName: string = '';

const fsMock = {
  readFile: jest.fn(async () => fileContent),
  writeFile: jest.fn(async (name, output) => {
    fileName = name;
    fileContent = output;
  }),
};

jest.mock('fs/promises', () => fsMock);

import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import type { Container } from 'inversify';

import { IntersectionObserverMockBase } from '../test/mocks/intersection_observer';
import 'jest-canvas-mock';
import {
  EChartsReactMock,
  expandPointPropertiesGroup,
  MessageRoutersServiceMockIntegrated,
  MessageRoutersServiceMockMultiAxis,
  PvtDeviceMock,
  SelectMock,
  selectPoint,
  setDeviceCallback,
} from './testing/mocks';

jest.mock('echarts-for-react', () => EChartsReactMock);

jest.mock('react-select', () => SelectMock);

class IntersectionObserverMock extends IntersectionObserverMockBase {
}

(window as unknown as { IntersectionObserver: typeof IntersectionObserverMock }).IntersectionObserver = IntersectionObserverMock;


import { createContainer, destroyContainer } from '../container';
import { htmlCollectionToArray, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import {
  mockDataForDeviceWithoutPeripherals,
  mockDataForDeviceWithPeripherals,
  mockLocalConnections,
  mockSingleDevice,
  mockSingleDeviceWithPeripherals
} from '../connection_manager/mocks';
import { MessageRoutersService } from '../message_router';

import {
  FILE_MENU_ID,
  OPEN_FILE_LABEL,
  PvtApp,
  RESET_DEVICE_TOOLTIP,
  RUN_TOOLTIP,
  SAVE_AS_FILE_LABEL,
  SAVE_FILE_LABEL,
  SELECT_AXES_TOOLTIP,
  SELECT_DEVICE_BUTTON_LABEL,
  UNSAVED_CHANGES_LABEL
} from './PvtApp';
import { selectDeviceOptions, selectDevicePositionNative, selectPlaybackTime, selectUserPath } from './selectors';
import _ from 'lodash';
import { actions } from './actions';
import { Angle, AngularVelocity, Length, PvtExecutionException, Velocity } from '@zaber/motion';
import type { Path, SeriesToAxisMap } from './types';
import { AXIS_MAPPING_DIALOG_TITLE, NO_COMPATIBLE_AXES } from './AxisMappingDialog';
import { formatCsv } from './csv';
import { convertToCsvData } from './utils';
import { IMPORT_BUTTON_LABEL } from './CsvImportDialog';
import { PvtAxisType } from '@zaber/motion/ascii';
import { TimeMeasuring } from '../time';
import { TimeMeasuringMock } from '../test/mocks/times';


let container: Container;
const TestApp = wrapWithNewStore(wrapWithRouter(PvtApp));
let wrapper: RenderResult;


function addMinimalPath() {
  const path: Path = {
    positionUnit: Length.METRES,
    velocityUnit: Velocity.METRES_PER_SECOND,
    series: [{
      name: 'X',
      points: [{ dt: 0, time: 0, pos: 1, vel: 0 }],
    }, {
      name: 'Y',
      points: [{ dt: 0, time: 0, pos: 1, vel: 0 }],
    }],
  };

  TestApp.testStore.dispatch(actions.storeUserPath(path));
  TestApp.testStore.dispatch(actions.fileSetLastSavedContent(path));
}


describe('With an integrated device', () => {
  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockIntegrated);
    wrapper = render(<TestApp/>);
    mockSingleDevice(TestApp.testStore);
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  describe('Device selection', () => {
    describe('without a file open', () => {
      it('stores the selected device key and no mapping', () => {
        fireEvent.click(wrapper.getByText(SELECT_DEVICE_BUTTON_LABEL));
        fireEvent.click(wrapper.getByTitle('Expand Connection'));
        fireEvent.click(wrapper.getByTestId('select-device'));
        const { deviceKey, axes } = selectDeviceOptions(TestApp.testStore.getState());
        expect(deviceKey).not.toBeNull();
        expect(deviceKey!.length).toBeGreaterThan(0);
        expect(_.keys(axes)).toEqual([]);
      });


      it('axes can be mapped after loading a file', () => {
        fireEvent.click(wrapper.getByText(SELECT_DEVICE_BUTTON_LABEL));
        fireEvent.click(wrapper.getByTitle('Expand Connection'));
        fireEvent.click(wrapper.getByTestId('select-device'));
        addMinimalPath();
        fireEvent.click(wrapper.getByTitle(SELECT_AXES_TOOLTIP));
        fireEvent.click(wrapper.getByTitle('OK'));
        const { axes } = selectDeviceOptions(TestApp.testStore.getState());
        expect(axes).toMatchObject<SeriesToAxisMap>({
          X: 0,
        });
      });
    });


    describe('with a file already open', () => {
      beforeEach(() => {
        addMinimalPath();
      });

      it('axis dialog shows after selecting a device', () => {
        fireEvent.click(wrapper.getByText(SELECT_DEVICE_BUTTON_LABEL));
        fireEvent.click(wrapper.getByTitle('Expand Connection'));
        fireEvent.click(wrapper.getByTestId('select-device'));
        fireEvent.click(wrapper.getByTitle('OK'));
        const { axes } = selectDeviceOptions(TestApp.testStore.getState());
        expect(axes).toMatchObject<SeriesToAxisMap>({
          X: 0,
        });
      });
    });


    describe('playback', () => {
      let _device: PvtDeviceMock;

      beforeEach(async () => {
        setDeviceCallback(d => _device = d);
        fileContent = formatCsv(convertToCsvData({
          positionUnit: Length.mm,
          velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
          series: [{
            name: 'X',
            points: [
              { dt: 0.000, time: 0.000, pos: 1, vel: 0 },
              { dt: 0.001, time: 0.001, pos: 2, vel: 0 },
              { dt: 0.001, time: 0.002, pos: 1, vel: -1 },
              { dt: 0.001, time: 0.003, pos: 0, vel: 0 }
            ],
          }],
        }));

        fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
        fireEvent.click(wrapper.getByText(OPEN_FILE_LABEL));
        await waitUntilPass(() => wrapper.getByText(IMPORT_BUTTON_LABEL));
        fireEvent.click(wrapper.getByText(IMPORT_BUTTON_LABEL));

        fireEvent.click(wrapper.getByText(SELECT_DEVICE_BUTTON_LABEL));
        fireEvent.click(wrapper.getByTitle('Expand Connection'));
        fireEvent.click(wrapper.getByTestId('select-device'));
        await waitUntilPass(() => fireEvent.click(wrapper.getByTitle('OK')));
        TestApp.testStore.dispatch(actions.storeDevicePosition([1], [1000]));
        await waitUntilPass(() => wrapper.getByTitle(RUN_TOOLTIP));
      });


      it('resets to the first position', async () => {
        fireEvent.click(wrapper.getByTitle(RESET_DEVICE_TOOLTIP));
        await waitUntilPass(() => expect(_device.getAxis(1).moveAbsolute)
          .toHaveBeenCalledWith(1, Length.mm, { waitUntilIdle: false }));
        const axis = _device.getAxis(1);
        expect(axis.stop).toHaveBeenCalledTimes(1);
        expect(axis.home).not.toHaveBeenCalled();
        await waitUntilPass(() => expect(axis.waitUntilIdle).toHaveBeenCalledTimes(2));
      });


      it('homes if needed', async () => {
        _device.getAxis(1)._isHomed = false;
        fireEvent.click(wrapper.getByTitle(RESET_DEVICE_TOOLTIP));
        await waitUntilPass(() => expect(_device.getAxis(1).home).toHaveBeenCalled());
      });


      it('sends expected PVT points', async () => {
        _device.genericCommand.mockResolvedValue({
          status: 'IDLE',
          replyFlag: 'OK',
        });
        fireEvent.click(wrapper.getByTitle(RUN_TOOLTIP));
        await waitUntilPass(() => expect(_device._pvt.setupLiveComposite)
          .toHaveBeenCalledWith({ axisNumber: 1, axisType: PvtAxisType.PHYSICAL }));
        await waitUntilPass(() => expect(_device.genericCommand).toHaveBeenCalledTimes(3));
        expect(_device.genericCommand.mock.calls).toEqual([
          expect.arrayContaining(['pvt 1 point abs p 2000 v 0 t 1.0']),
          expect.arrayContaining(['pvt 1 point abs p 1000 v -100 t 1.0']),
          expect.arrayContaining(['pvt 1 point abs p 0 v 0 t 1.0']),
        ]);
      });


      it('retries on AGAIN', async () => {
        _device.genericCommand
          .mockResolvedValueOnce({ status: 'IDLE', replyFlag: 'OK' })
          .mockResolvedValueOnce({ status: 'IDLE', replyFlag: 'RJ', data: 'AGAIN' })
          .mockResolvedValueOnce({ status: 'IDLE', replyFlag: 'OK' });
        fireEvent.click(wrapper.getByTitle(RUN_TOOLTIP));
        await waitUntilPass(() => expect(_device.genericCommand).toHaveBeenCalledTimes(4));
        expect(_device.genericCommand.mock.calls).toEqual([
          expect.arrayContaining(['pvt 1 point abs p 2000 v 0 t 1.0']),
          expect.arrayContaining(['pvt 1 point abs p 1000 v -100 t 1.0']),
          expect.arrayContaining(['pvt 1 point abs p 1000 v -100 t 1.0']),
          expect.arrayContaining(['pvt 1 point abs p 0 v 0 t 1.0']),
        ]);
      });


      it('tracks device position once moving', async () => {
        container.bind<unknown>(TimeMeasuring).to(TimeMeasuringMock);
        const timeMeasuringMock = container.get<unknown>(TimeMeasuring) as TimeMeasuringMock;
        timeMeasuringMock.mockTime = 0;

        TestApp.testStore.dispatch(actions.storeUserPath({
          positionUnit: Length.mm,
          velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
          series: [{
            name: 'X',
            points: [
              { dt: 0, time: 0, pos: 1, vel: 0 },
              { dt: 0.5, time: 0.5, pos: 1, vel: 0 },
              { dt: 0.5, time: 1, pos: 1, vel: 0 },
              { dt: 0.5, time: 1.5, pos: 1, vel: 0 },
            ],
          }],
        }));

        _device.genericCommand.mockImplementation(() => {
          timeMeasuringMock.mockTime! += 200;
          return { status: 'BUSY', replyFlag: 'OK' };
        });
        _device.settings.getFromAllAxes.mockResolvedValue([6000]);
        fireEvent.click(wrapper.getByTitle(RUN_TOOLTIP));
        await waitUntilPass(() => {
          expect(_device.settings.getFromAllAxes).toHaveBeenCalled();
        });

        expect(_device.settings.getFromAllAxes.mock.lastCall).toEqual(['pos']);
        const pos = selectDevicePositionNative(TestApp.testStore.getState());
        expect(pos).toEqual([6000]);
        const time = selectPlaybackTime(TestApp.testStore.getState());
        expect(time).toBeGreaterThanOrEqual(0.1);
      });


      it('corks the stream', async () => {
        _device.genericCommand.mockResolvedValue({
          status: 'IDLE',
          replyFlag: 'OK',
        });

        fireEvent.click(wrapper.getByTitle(RUN_TOOLTIP));
        await waitUntilPass(() => expect(_device._pvt.cork).toHaveBeenCalled());
        await waitUntilPass(() => expect(_device._pvt.uncork).toHaveBeenCalled());
      });


      it('checks warnings at the end of playback', async () => {
        _device.genericCommand.mockResolvedValue({
          status: 'IDLE',
          replyFlag: 'OK',
        });
        _device._pvt.isBusy.mockResolvedValue(false);
        _device._pvt.waitUntilIdle.mockRejectedValueOnce(new PvtExecutionException('Foo', {
          reason: 'Bar',
          errorFlag: 'FB',
          invalidPoints: [{ index: 17, point: 'seventeen' }],
        }));
        fireEvent.click(wrapper.getByTitle(RUN_TOOLTIP));
        await waitUntilPass(() => expect(_device._pvt.uncork).toHaveBeenCalled());
        await waitUntilPass(() => wrapper.getByText(/Bar.*18/));
      });
    });
  });
});


describe('With a multi-axis controller', () => {
  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockMultiAxis);
    wrapper = render(<TestApp/>);
    mockSingleDeviceWithPeripherals(TestApp.testStore, { peripheralCount: 2 });
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  describe('device selection', () => {
    beforeEach(() => {
      addMinimalPath();
    });

    it('default axis axis mapping includes both series', () => {
      fireEvent.click(wrapper.getByText(SELECT_DEVICE_BUTTON_LABEL));
      fireEvent.click(wrapper.getByTitle('Expand Connection'));
      fireEvent.click(wrapper.getByTestId('select-device'));
      fireEvent.click(wrapper.getByTitle('OK'));
      const { axes } = selectDeviceOptions(TestApp.testStore.getState());
      expect(axes).toMatchObject<SeriesToAxisMap>({
        X: 0,
        Y: 1,
      });
    });


    it('allows changing axis mappings', () => {
      fireEvent.click(wrapper.getByText(SELECT_DEVICE_BUTTON_LABEL));
      fireEvent.click(wrapper.getByTitle('Expand Connection'));
      fireEvent.click(wrapper.getByTestId('select-device'));

      fireEvent.change(wrapper.getByTestId('axis-map-select-0'), { target: { value: 1 } });
      fireEvent.change(wrapper.getByTestId('axis-map-select-1'), { target: { value: 0 } });

      fireEvent.click(wrapper.getByTitle('OK'));
      const { axes } = selectDeviceOptions(TestApp.testStore.getState());
      expect(axes).toMatchObject<SeriesToAxisMap>({
        X: 1,
        Y: 0,
      });
    });


    it('allows selecting a subset of axes', () => {
      fireEvent.click(wrapper.getByText(SELECT_DEVICE_BUTTON_LABEL));
      fireEvent.click(wrapper.getByTitle('Expand Connection'));
      fireEvent.click(wrapper.getByTestId('select-device'));

      fireEvent.change(wrapper.getByTestId('axis-map-select-0'), { target: { value: 1 } });

      fireEvent.click(wrapper.getByTitle('OK'));
      const { axes } = selectDeviceOptions(TestApp.testStore.getState());
      expect(axes).toMatchObject<SeriesToAxisMap>({
        X: 1,
      });
    });
  });
});


describe('Changing selected', () => {
  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockMultiAxis);
    wrapper = render(<TestApp/>);
    mockLocalConnections(TestApp.testStore, {
      COM17: connectionKey => [
        mockDataForDeviceWithPeripherals(connectionKey, 1, 2, 'legacy'),
        mockDataForDeviceWithoutPeripherals(connectionKey, 2, 1),
      ],
    });
    addMinimalPath();
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  it('device re-shows axis dialog', () => {
    fireEvent.click(wrapper.getByTestId('select-device-button'));
    fireEvent.click(wrapper.getByTitle('Expand Connection'));
    fireEvent.click(wrapper.getAllByTestId('select-device')[0]);
    fireEvent.click(wrapper.getByTitle('OK'));
    expect(wrapper.queryAllByText(AXIS_MAPPING_DIALOG_TITLE)).toEqual([]);
    fireEvent.click(wrapper.getByTestId('select-device-button'));
    fireEvent.click(wrapper.getByTitle('Expand Connection'));
    fireEvent.click(wrapper.getAllByTestId('select-device')[1]);
    expect(wrapper.queryAllByText(AXIS_MAPPING_DIALOG_TITLE)).not.toEqual([]);
  });


  it('file re-shows axis dialog', async () => {
    fireEvent.click(wrapper.getByTestId('select-device-button'));
    fireEvent.click(wrapper.getByTitle('Expand Connection'));
    fireEvent.click(wrapper.getAllByTestId('select-device')[0]);
    fireEvent.click(wrapper.getByTitle('OK'));
    expect(wrapper.queryAllByText(AXIS_MAPPING_DIALOG_TITLE)).toEqual([]);

    // Load a file with different series names.
    fileContent = formatCsv(convertToCsvData({
      positionUnit: Length.METRES,
      velocityUnit: Velocity.METRES_PER_SECOND,
      series: [{
        name: 'A',
        points: [{ dt: 1, time: 1, pos: 1, vel: 1 }],
      }, {
        name: 'B',
        points: [{ dt: 1, time: 1, pos: 1, vel: 1 }],
      }],
    }));

    fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
    fireEvent.click(wrapper.getByText(OPEN_FILE_LABEL));
    await waitUntilPass(() => wrapper.getByText(IMPORT_BUTTON_LABEL));
    fireEvent.change(wrapper.getByTitle('Length Unit Select'), { target: { value: Length.mm } });
    fireEvent.change(wrapper.getByTitle('Velocity Unit Select'), { target: { value: Velocity.MILLIMETRES_PER_SECOND } });
    fireEvent.click(wrapper.getByText(IMPORT_BUTTON_LABEL));

    await waitUntilPass(() => expect(wrapper.queryAllByText(AXIS_MAPPING_DIALOG_TITLE)).not.toEqual([]));
  });


  it('file with incompatible units shows dialog with no choices', async () => {
    fireEvent.click(wrapper.getByTestId('select-device-button'));
    fireEvent.click(wrapper.getByTitle('Expand Connection'));
    fireEvent.click(wrapper.getAllByTestId('select-device')[0]);
    fireEvent.click(wrapper.getByTitle('OK'));
    expect(wrapper.queryAllByText(AXIS_MAPPING_DIALOG_TITLE)).toEqual([]);

    fileContent = formatCsv(convertToCsvData({
      positionUnit: Angle.DEGREES,
      velocityUnit: AngularVelocity.DEGREES_PER_SECOND,
      series: [{
        name: 'X',
        points: [{ dt: 0, time: 0, pos: 1, vel: 0 }],
      }, {
        name: 'Y',
        points: [{ dt: 0, time: 0, pos: 1, vel: 0 }],
      }],
    }));

    fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
    fireEvent.click(wrapper.getByText(OPEN_FILE_LABEL));
    await waitUntilPass(() => wrapper.getByText(IMPORT_BUTTON_LABEL));
    fireEvent.change(wrapper.getByTitle('Angle Unit Select'), { target: { value: Angle.DEGREES } });
    fireEvent.change(wrapper.getByTitle('Velocity Unit Select'), { target: { value: AngularVelocity.DEGREES_PER_SECOND } });
    fireEvent.click(wrapper.getByText(IMPORT_BUTTON_LABEL));
    await waitUntilPass(() => expect(wrapper.queryAllByText(NO_COMPATIBLE_AXES)).not.toEqual([]));
  });
});


describe('With a lockstep group', () => {
  let _device: PvtDeviceMock;

  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockMultiAxis);
    wrapper = render(<TestApp/>);
    setDeviceCallback(d => _device = d);
    mockSingleDeviceWithPeripherals(TestApp.testStore, {
      peripheralCount: 3,
      modifier: device => ({
        ...device,
        locksteps: [{
          axisNumbers: [1, 3],
          groupNumber: 1,
        }],
      }),
    });

    addMinimalPath();
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  it('axis dialog shows group and not its component axes', () => {
    fireEvent.click(wrapper.getByTestId('select-device-button'));
    fireEvent.click(wrapper.getByTitle('Expand Connection'));
    fireEvent.click(wrapper.getAllByTestId('select-device')[0]);
    expect(wrapper.queryAllByText(AXIS_MAPPING_DIALOG_TITLE)).not.toEqual([]);
    const selector = wrapper.getByTestId('axis-map-select-0');
    expect(selector).toHaveTextContent('Lockstep group 1');
    expect(selector).toHaveTextContent('LHM025A-T3');
    expect(htmlCollectionToArray(selector.getElementsByTagName('option')).length).toBe(2);
  });


  describe('playback', () => {
    beforeEach(async () => {
      fileContent = formatCsv(convertToCsvData({
        positionUnit: Length.mm,
        velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
        series: [{
          name: 'X',
          points: [
            { dt: 0.000, time: 0.000, pos: 1, vel: 0 },
            { dt: 0.001, time: 0.001, pos: 2, vel: 0 },
            { dt: 0.001, time: 0.002, pos: 1, vel: -1 },
            { dt: 0.001, time: 0.003, pos: 0, vel: 0 }
          ],
        }, {
          name: 'Y',
          points: [
            { dt: 0.000, time: 0.000, pos: 2, vel: 0 },
            { dt: 0.001, time: 0.001, pos: 3, vel: 1 },
            { dt: 0.001, time: 0.002, pos: 2, vel: 0 },
            { dt: 0.001, time: 0.003, pos: 0, vel: 0 }
          ],
        }],
      }));

      fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
      fireEvent.click(wrapper.getByText(OPEN_FILE_LABEL));
      await waitUntilPass(() => wrapper.getByText(IMPORT_BUTTON_LABEL));
      fireEvent.click(wrapper.getByText(IMPORT_BUTTON_LABEL));

      fireEvent.click(wrapper.getByText(SELECT_DEVICE_BUTTON_LABEL));
      fireEvent.click(wrapper.getByTitle('Expand Connection'));
      fireEvent.click(wrapper.getByTestId('select-device'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByTitle('OK')));
      TestApp.testStore.dispatch(actions.storeDevicePosition([1, 2, 1], [1000, 2000, 1000]));
      await waitUntilPass(() => wrapper.getByTitle(RUN_TOOLTIP));
    });


    it('resets to the first position', async () => {
      fireEvent.click(wrapper.getByTitle(RESET_DEVICE_TOOLTIP));
      const axis = _device.getAxis(2);
      const group = _device.getLockstep(1);
      await waitUntilPass(() => expect(axis.moveAbsolute).toHaveBeenCalledWith(2, Length.mm, { waitUntilIdle: false }));
      await waitUntilPass(() => expect(group.moveAbsolute).toHaveBeenCalledWith(1, Length.mm, { waitUntilIdle: false }));
      expect(axis.stop).toHaveBeenCalledTimes(1);
      expect(axis.home).not.toHaveBeenCalled();
      expect(group.stop).toHaveBeenCalledTimes(1);
      await waitUntilPass(() => expect(axis.waitUntilIdle).toHaveBeenCalledTimes(2));
      await waitUntilPass(() => expect(group.waitUntilIdle).toHaveBeenCalledTimes(1)); // Homing is not done on locksteps.
    });


    it('sends expected PVT points', async () => {
      _device.genericCommand.mockResolvedValue({
        status: 'IDLE',
        replyFlag: 'OK',
      });

      fireEvent.click(wrapper.getByTitle(RUN_TOOLTIP));
      await waitUntilPass(() => expect(_device._pvt.setupLiveComposite)
        .toHaveBeenCalledWith({ axisNumber: 1, axisType: PvtAxisType.LOCKSTEP }, { axisNumber: 2, axisType: PvtAxisType.PHYSICAL }));
      await waitUntilPass(() => expect(_device.genericCommand).toHaveBeenCalledTimes(3));
      expect(_device.genericCommand.mock.calls).toEqual([
        expect.arrayContaining(['pvt 1 point abs p 2000 3000 v 0 100 t 1.0']),
        expect.arrayContaining(['pvt 1 point abs p 1000 2000 v -100 0 t 1.0']),
        expect.arrayContaining(['pvt 1 point abs p 0 0 v 0 0 t 1.0']),
      ]);
    });
  });
});


describe('Editing', () => {
  beforeEach(async () => {
    container = createContainer();
    wrapper = render(<TestApp/>);
    fileContent = formatCsv(convertToCsvData({
      positionUnit: Length.mm,
      velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
      series: [{
        name: 'X',
        points: [
          { dt: 0, time: 0, pos: 1, vel: 0 },
          { dt: 1, time: 1, pos: 2, vel: 1 },
          { dt: 1, time: 2, pos: 3, vel: 2 },
          { dt: 1, time: 3, pos: 4, vel: 0 }
        ],
      }],
    }));

    await openFile();
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  async function openFile() {
    fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
    fireEvent.click(wrapper.getByText(OPEN_FILE_LABEL));
    await waitUntilPass(() => wrapper.getByText(IMPORT_BUTTON_LABEL));
    fireEvent.click(wrapper.getByText(IMPORT_BUTTON_LABEL));
  }


  function editPointPosition(seriesName: string, value: number) {
    const input = wrapper.getByTestId(`edit-${seriesName}-pos`);
    fireEvent.change(input, { target: { value: value.toString() } });
    fireEvent.blur(input);
  }


  function makeAnEdit() {
    selectPoint(wrapper, 2);
    expandPointPropertiesGroup(wrapper, 'X');
    editPointPosition('X', 5);
  }


  function undoKey() {
    fireEvent.keyDown(wrapper.baseElement, { key: 'z', ctrlKey: true });
  }


  function redoKey() {
    fireEvent.keyDown(wrapper.baseElement, { key: 'y', ctrlKey: true });
  }


  function undoButton() {
    fireEvent.click(wrapper.getByTestId('undo-button'));
  }


  function redoButton() {
    fireEvent.click(wrapper.getByTestId('redo-button'));
  }


  it('shows that the data has been changed', () => {
    makeAnEdit();
    wrapper.getByText(UNSAVED_CHANGES_LABEL);
  });


  it('overwrites original file on save', () => {
    makeAnEdit();
    fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
    fireEvent.click(wrapper.getByText(SAVE_FILE_LABEL));
    expect(fileName).toBe('foo.csv');
    expect(fileContent).toEqual(formatCsv(convertToCsvData({
      positionUnit: Length.mm,
      velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
      series: [{
        name: 'X',
        points: [
          { dt: 0, time: 0, pos: 1, vel: 0 },
          { dt: 1, time: 1, pos: 2, vel: 1 },
          { dt: 1, time: 2, pos: 5, vel: 2 },
          { dt: 1, time: 3, pos: 4, vel: 0 }
        ],
      }],
    })));
  });


  it ('saves as', () => {
    makeAnEdit();
    fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
    fireEvent.click(wrapper.getByText(SAVE_AS_FILE_LABEL));
    expect(fileName).toBe('bar.csv');
  });


  it('undoes and redoes via keystrokes', () => {
    selectPoint(wrapper, 1);
    expandPointPropertiesGroup(wrapper, 'X');
    editPointPosition('X', 5);
    selectPoint(wrapper, 2);
    editPointPosition('X', 7);

    let path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[1].pos).toBe(5);
    expect(path.series[0].points[2].pos).toBe(7);

    undoKey();
    path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[2].pos).toBe(3);

    undoKey();
    path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[1].pos).toBe(2);

    redoKey();
    path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[1].pos).toBe(5);

    redoKey();
    path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[2].pos).toBe(7);
  });


  it('undoes and redoes via button clicks', () => {
    selectPoint(wrapper, 1);
    expandPointPropertiesGroup(wrapper, 'X');
    editPointPosition('X', 5);
    selectPoint(wrapper, 2);
    editPointPosition('X', 7);

    let path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[1].pos).toBe(5);
    expect(path.series[0].points[2].pos).toBe(7);

    undoButton();
    path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[2].pos).toBe(3);

    undoButton();
    path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[1].pos).toBe(2);

    redoButton();
    path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[1].pos).toBe(5);

    redoButton();
    path = selectUserPath(TestApp.testStore.getState())!;
    expect(path.series[0].points[2].pos).toBe(7);
  });


  it ('clears unsaved status and restores it on undo', async () => {
    makeAnEdit();
    wrapper.getByText(UNSAVED_CHANGES_LABEL);
    fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
    fireEvent.click(wrapper.getByText(SAVE_FILE_LABEL));
    await waitUntilPass(() => expect(wrapper.queryByText(UNSAVED_CHANGES_LABEL)).toBeNull());
    undoKey();
    wrapper.getByText(UNSAVED_CHANGES_LABEL);
  });


  it ('enables undo and redo buttons appropriately', async () => {
    expect(wrapper.getByTestId('undo-button')).toHaveClass('disabled');
    expect(wrapper.getByTestId('redo-button')).toHaveClass('disabled');
    makeAnEdit();
    expect(wrapper.getByTestId('undo-button')).not.toHaveClass('disabled');
    expect(wrapper.getByTestId('redo-button')).toHaveClass('disabled');
    undoKey();
    expect(wrapper.getByTestId('undo-button')).toHaveClass('disabled');
    expect(wrapper.getByTestId('redo-button')).not.toHaveClass('disabled');
    redoKey();
    expect(wrapper.getByTestId('undo-button')).not.toHaveClass('disabled');
    expect(wrapper.getByTestId('redo-button')).toHaveClass('disabled');
  });
});
