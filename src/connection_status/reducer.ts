import { createReducer, changeDictionary } from '../utils';
import type { EntityKey } from '../keys';

import { ActionsToPayloads, ActionTypes } from './actions';

export interface RouterState {
  key: EntityKey;
  connected: boolean;
}

export interface ConnectionState {
  key: EntityKey;
  connected: boolean;
}

export interface State {
  routers: Record<string, RouterState>;
  connections: Record<string, ConnectionState>;
}

const initialState: State = {
  routers: {},
  connections: {},
};

const routerConnected = (state: State, { routerKey }: ActionsToPayloads[ActionTypes.ROUTER_CONNECTED]): State =>
  ({
    ...state,
    routers: changeDictionary(state.routers, routerKey, { connected: true }, (key: EntityKey) => ({ key, connected: false })),
  });

const routerDisconnected = (state: State, { routerKey }: ActionsToPayloads[ActionTypes.ROUTER_DISCONNECTED]): State =>
  ({
    ...state,
    routers: changeDictionary(state.routers, routerKey, { connected: false }),
  });

const routedConnectionConnected = (state: State, { connectionKey }: ActionsToPayloads[ActionTypes.ROUTED_CONNECTION_CONNECTED]): State =>
  ({
    ...state,
    connections: changeDictionary(state.connections, connectionKey, { connected: true }, (key: EntityKey) => ({ key, connected: false })),
  });

const routedConnectionDisconnected = (
  state: State,
  { connectionKey }: ActionsToPayloads[ActionTypes.ROUTED_CONNECTION_DISCONNECTED],
): State =>
  ({
    ...state,
    connections: changeDictionary(state.connections, connectionKey, { connected: false }),
  });

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.ROUTER_CONNECTED]: routerConnected,
  [ActionTypes.ROUTER_DISCONNECTED]: routerDisconnected,
  [ActionTypes.ROUTED_CONNECTION_CONNECTED]: routedConnectionConnected,
  [ActionTypes.ROUTED_CONNECTION_DISCONNECTED]: routedConnectionDisconnected,
}, initialState);
