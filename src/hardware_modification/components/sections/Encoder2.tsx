import React from 'react';

import { SettingField } from '../fields/SettingField';

import type { ContentProps, HwModSectionTemplate } from './HwModSection';

const Encoder2Section: React.FC<ContentProps<'encoder2'>> = ({ values }) => {
  const sectionId = 'encoder2';

  return <>
    <SettingField {...{ values, sectionId, fieldId: 'mode' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'type' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'dir' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'fault' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'indexMode' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'filter' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'div' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'mult' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'interpolation' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'minSignal' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'cosDC' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'cosGain' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'sinDC' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'sinGain' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'powerUpDelay' }}/>
  </>;
};

export const encoder2Template: HwModSectionTemplate<'encoder2'> = {
  sectionId: 'encoder2',
  titleName: 'Encoder 2 (Differential Signalling)',
  menuName: 'Encoder 2',
  description: 'Information about the encoder connected on port 2, which only supports encoders with differential signalling.',
  generateContent: values => (<Encoder2Section values={values}/>),
};
