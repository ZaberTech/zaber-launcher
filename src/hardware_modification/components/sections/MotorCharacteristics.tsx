import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';

import { actions as actionsDefinition } from '../../actions';
import type { SectionValues } from '../../types';
import type { FieldIds } from '../../data_definitions';
import { SettingField } from '../fields/SettingField';
import { HwModField } from '../fields/HwModField';

import type { ContentProps, HwModSectionTemplate } from './HwModSection';
import ratedCurrentDocs from './custom_manuals/rated_current.html';
import targetRpmDocs from './custom_manuals/target_rpm.html';
import runPercentDocs from './custom_manuals/driver_current_run_percent.html';
import holdPercentDocs from './custom_manuals/driver_current_hold_percent.html';

interface CurrentLimitFieldWrapperProps {
  values: SectionValues<'motorCharacteristics'>;
  name: string;
  description: string;
  controllingFieldId: FieldIds<'motorCharacteristics'>;
  sliderInput: boolean;
  docs?: string;
}

const MotorCharacteristicField: React.FC<CurrentLimitFieldWrapperProps> = props => {
  const { name, description, values, controllingFieldId, sliderInput, docs } = props;
  const actions = useActions(actionsDefinition);
  const controllingField =  values[controllingFieldId];
  if (!controllingField) {
    return null;
  }


  return <HwModField
    sectionId="motorCharacteristics" fieldId={controllingFieldId} values={values}
    name={name} description={description}
    onUpdate={value => actions.updateValue({ sectionId: 'motorCharacteristics', fieldId: controllingFieldId, value })}
    docs={docs ? { type: 'custom', html: docs } : undefined}
    input={sliderInput ? { type: 'slider', min: 5, max: 100, step: 5 } : { type: 'numeric' }}
  />;
};

const MotorCharacteristicsSection: React.FC<ContentProps<'motorCharacteristics'>> = ({ values }) => {
  const sectionId = 'motorCharacteristics';

  return <div data-testid="motor-characteristics-section">
    <MotorCharacteristicField
      name="Steps Per Rotation"
      values={values}
      description="The motor's steps per rotation"
      controllingFieldId="stepsPerRotation"
      sliderInput={false}
    />
    <MotorCharacteristicField
      name="Rated Motor Current"
      values={values}
      description="The rated current for this axis (A)"
      controllingFieldId="ratedCurrent"
      sliderInput={false}
      docs={ratedCurrentDocs}
    />
    <MotorCharacteristicField
      name="Driver Run Current Percentage"
      values={values}
      description="The percentage of rated current to use when running the motor. "
      controllingFieldId="runFactor"
      sliderInput={true}
      docs={runPercentDocs}
    />
    <MotorCharacteristicField
      name="Driver Hold Current Percentage"
      values={values}
      description="The percentage of rated current to use when holding position. "
      controllingFieldId="holdFactor"
      sliderInput={true}
      docs={holdPercentDocs}
    />
    <SettingField {...{ values, sectionId, fieldId: 'motorInductance' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'motorResistance' }}/>
    <MotorCharacteristicField
      name="Holding Torque"
      values={values}
      description="The motor's rated maximum holding torque (Nm)"
      controllingFieldId="holdingTorque"
      sliderInput={false}
    />
    <SettingField {...{ values, sectionId, fieldId: 'driverDirection' }}/>
    <MotorCharacteristicField
      name="Target Motor Speed"
      values={values}
      description="The speed that the motor will turn (RPM)"
      controllingFieldId="rpm"
      sliderInput={false}
      docs={targetRpmDocs}
    />
  </div>;
};

export const motorCharacteristicsTemplate: HwModSectionTemplate<'motorCharacteristics'> = {
  sectionId: 'motorCharacteristics',
  titleName: 'Motor Characteristics',
  description: 'The specifications of the motor driving this product.',
  generateContent: values => (<MotorCharacteristicsSection values={values}/>),
};
