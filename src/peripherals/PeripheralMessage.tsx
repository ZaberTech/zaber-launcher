import React from 'react';

import type { AxisStateComplete } from './selectors';
import { Message } from './Message';

export const PeripheralMessage: React.FC<{ axis: AxisStateComplete }> = ({ axis }) => {
  const {
    pendingPeripheral,
    derived: {
      hasIssue, isIncompatible, canReactivate, isAutoDetectable, deviceCanAutoDetect, isUnused,
    },
  } = axis;
  if (hasIssue && pendingPeripheral) {
    if (!isIncompatible) {
      return <Message title="New Peripheral" type="warning">
        {`New peripheral ${pendingPeripheral.name} is connected.`}
      </Message>;
    } else {
      return <Message title="Incompatible Peripheral" type="error">
        Connected peripheral is not compatible; upgrade device's firmware.
      </Message>;
    }
  }
  if (hasIssue && isAutoDetectable && !pendingPeripheral) {
    return <Message title="Peripheral Disconnected" type="error">
      Peripheral is disconnected. Please reconnect the peripheral.
    </Message>;
  }
  if (canReactivate) {
    return <Message title="Peripheral Inactive" type="error">
      If a new peripheral is connected, set the peripheral ID.
      Otherwise, check the peripheral connection and reactivate.
    </Message>;
  }
  if (isUnused && !pendingPeripheral) {
    if (deviceCanAutoDetect) {
      return <Message title="Axis Disabled" type="info">Connect a peripheral then set the peripheral ID if none is detected.</Message>;
    } else {
      return <Message title="Axis Disabled" type="info">Set the peripheral ID then connect a peripheral.</Message>;
    }
  }

  return null;
};
