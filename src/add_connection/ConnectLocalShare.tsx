import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, ButtonPrevious, ButtonRow, Input, NoticeBanner } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { makeLocalShareUrl } from '../connection_manager';
import { LOCAL_SHARE_DEFAULT_PORT } from '../local_share';

import { actions as actionsDefinition } from './actions';
import { MAX_PORT_NUM, Steps } from './types';
import { selectLocalShare } from './selectors';
import { Loading } from './Elements';

export const ConnectLocalShare: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const { connecting, error } = useSelector(selectLocalShare);

  const [port, setPort] = useState(LOCAL_SHARE_DEFAULT_PORT);
  const [hostname, setHostname] = useState('');
  const isValid = !!hostname.trim() && Number.isInteger(port) && port > 0 && port < MAX_PORT_NUM;

  useEffect(() => () => { actions.connectLocalShareClear() }, []);

  return (
    <div className="connect-local-share">
      <h3>Connect manually</h3>

      <Input className="hostname" disabled={connecting}
        labelContent="Hostname/IP Address"
        value={hostname}
        onValueChange={setHostname}/>

      <Input className="tcp-port" disabled={connecting}
        labelContent="Port"
        min={1}
        max={MAX_PORT_NUM}
        value={Number.isFinite(port) ? port : ''}
        onValueChange={value => setPort(parseInt(value, 10))}/>

      {connecting && <Loading type="row"/>}
      {error && <NoticeBanner>{error.title}: {error.message}</NoticeBanner>}


      <ButtonRow justify="spread">
        <ButtonPrevious onClick={() => actions.changeStep(Steps.CHOOSE_CONNECTION_TYPE)}>Back</ButtonPrevious>
        <Button
          disabled={!isValid || connecting}
          onClick={() => actions.connectLocalShare(makeLocalShareUrl(hostname, port))}
        >Create Connection</Button>
      </ButtonRow>
    </div>
  );
};
