import React, { useLayoutEffect, forwardRef } from 'react';
import { Input, NumericInput } from '@zaber/react-library';
import classNames from 'classnames';
import { Units } from '@zaber/motion';

import { UnitSelect } from './UnitSelect';
import { convertNullable, convertValueUnits, dimensionsByName, truncateValue } from './utils';
import { hasUnits, UnitFrom, UnitInfo, Measurement, MeasurementWithUnit, titleOfUnitFrom } from './types';
import { useUnitInfo } from './hooks';

export interface InputWithUnitProps extends Omit<
  React.ComponentProps<typeof NumericInput>,
  'value' | 'onChange' | 'onNumberChange' | 'inputLineChildren'
> {
  /** A setting name or dimension name to get unit info for. */
  unitFrom: UnitFrom;
  /**
   * The value and units used to populate this component.
   * If null is passed, the component will display as loading. Can pass this in while waiting for initial data to arrive.
   * If value is set as null, the value field will not be effected.
   * If the unit is:
   * * A Unit: The unit will be used
   * * 'default': The default unit will be used
   * * 'readable': The unit resulting in the most readable value will be used
   *
   * If 'default' or 'readable' are passed in, value should be in native units, and this component will call `onChange`
   * with the new value and unit once it's computed
   */
  measure: Measurement | null;
  onChange: (value: MeasurementWithUnit) => void;
  /** Set this to false if the displayed units cannot be directly changed by the user */
  unitsEditable?: boolean;
  alignValue?: 'left' | 'right';
}

/** Returns true if the candidate is more readable than the current value */
function numberIsMoreReadable(candidateValue: number, currentValue: number): boolean {
  const candidate = Math.abs(candidateValue);
  const current = Math.abs(currentValue);
  if (candidate > 1) {
    if (current < 1) {
      // Always prefer values that are left of the decimal
      return true;
    } else if (candidate < current) {
      // Otherwise, prefer smaller numbers
      return true;
    }
  } else if (candidate > current) {
    // When both numbers are < 1, prefer the larger number
    return true;
  }
  return false;
}

function recommendUnits(unitInfo: UnitInfo, value: number): MeasurementWithUnit {
  if (!unitInfo.supported || value === 0) {
    return { value, units: unitInfo.default };
  }

  const dimension = dimensionsByName[unitInfo.dimension];
  const defaultUnitDef = dimension.units.find(unit => unit.id === unitInfo.default)!;

  let bestUnit = defaultUnitDef.id as Units;
  let bestValue = convertValueUnits(unitInfo, value, Units.NATIVE, bestUnit);
  for (const unitDef of dimension.units) {
    if (unitDef.Offset !== defaultUnitDef.Offset || Math.log10(unitDef.Scale / defaultUnitDef.Scale) % 3 !== 0) {
      // Do not consider units with scales that are not a multiple of thousands of the default.
      continue;
    }
    const toUnit = unitDef.id as Units;
    const candidateValue = convertValueUnits(unitInfo, value, Units.NATIVE, toUnit);
    if (numberIsMoreReadable(candidateValue, bestValue)) {
      bestValue = candidateValue;
      bestUnit = toUnit;
    }
  }

  return { value: bestValue, units: bestUnit };
}

export const InputWithUnits = forwardRef<HTMLInputElement, InputWithUnitProps>((props, ref) => {
  const {
    unitFrom, measure, onChange, unitsEditable = true,
    disabled = false, className, alignValue = 'right', displayPrecision, title, ...rest
  } = props;
  const unitInfo = useUnitInfo(unitFrom);
  const descriptionInTitle = title ?? titleOfUnitFrom(unitFrom);

  useLayoutEffect(() => {
    if (unitInfo != null && measure != null) {
      if (measure.units == null || measure.units === 'default') {
        const inDefaultUnits: MeasurementWithUnit = unitInfo.supported ? {
          value: convertNullable(unitInfo, measure.value, Units.NATIVE, unitInfo.default),
          units: unitInfo.default,
        } : { ...measure, units: Units.NATIVE };
        onChange(inDefaultUnits);
      } else if (measure.units === 'readable') {
        if (measure.value != null) {
          const inBetterUnits = recommendUnits(unitInfo, measure.value);
          onChange(inBetterUnits);
        }
      } else if (!!disabled && measure.value != null) {
        const truncatedValue = truncateValue(unitInfo, measure.value, measure.units);
        if (truncatedValue !== value) {
          onChange({ value: truncatedValue, units: measure.units });
        }
      }
    }
  }, [unitInfo?.supported, measure?.value, measure?.units, disabled]);

  if (unitInfo == null || !hasUnits(measure)) {
    if (!disabled) {
      return (
        <div className="input-with-units">
          <Input onChange={() => null} disabled={true} value="Loading..."/>
        </div>
      );
    } else {
      return null;
    }
  }

  const { value, units } = measure;
  const entityKeyMissing = 'entity' in unitFrom && unitFrom.entity == null;
  const inputDisabled = disabled || entityKeyMissing;

  const classes = classNames(
    'input-with-units',
    className,
    `align-${alignValue}`,
    inputDisabled ? 'input-disabled' : 'input-enabled',
  );
  return (
    <div className={classes}>
      <NumericInput
        ref={ref} value={value}
        {...rest}
        disabled={inputDisabled}
        displayPrecision={measure.units === Units.NATIVE ? unitInfo.precision : displayPrecision}
        title={`Value of ${descriptionInTitle}`}
        onNumberChange={newValue => onChange({ value: newValue, units })}
        inputLineChildren={unitInfo.supported && units != null && <>
          <div className="value-unit-separator"/>
          <UnitSelect
            value={units}
            dimensions={unitInfo.dimension} includeNative={unitInfo.conversion != null}
            disabled={inputDisabled === true}
            editable={unitsEditable}
            title={`Units of ${descriptionInTitle}`}
            portalToBody={true}
            borderless={true}
            onChange={newUnits => {
              const newValue = convertNullable(unitInfo, value, units, newUnits);
              if (newValue != null) {
                onChange({ value: truncateValue(unitInfo, newValue, newUnits), units: newUnits });
              } else {
                onChange({ value: newValue, units: newUnits });
              }
            }}
          />
        </>}
      />
    </div>
  );
});

InputWithUnits.displayName = 'InputWithUnits';
