import { NoticeBanner } from '@zaber/react-library';
import React from 'react';
import { useSelector } from 'react-redux';

import { Editions, environment } from '../../environment';
import { selectHasInternalDevices } from '../selectors';

export const InternalDevicesWarning: React.FC = () => {
  const hasInternal = useSelector(selectHasInternalDevices);

  if (environment.edition === Editions.Public && hasInternal) {
    return <NoticeBanner className="internal-devices-warning" type="warning">
      Internal devices detected. Use Internal edition of Zaber Launcher.
    </NoticeBanner>;
  }
  return null;
};
