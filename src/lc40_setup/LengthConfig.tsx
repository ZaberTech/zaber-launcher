import React from 'react';
import { useSelector } from 'react-redux';
import { NoticeBorder, NoticeMessage, HelpTooltip, Input, Text, HeaderCard, Flex } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { MAXIMUM_TRAVEL_LENGTH_MM } from './types';
import { actions as actionsDefinition } from './actions';
import { selectValidationErr, selectResultError, selectCurrentAxisOrDevice } from './selectors';
import { ErrorContext } from './exception';
import sticker from './assets/sticker.png';

interface Props {
  validate: boolean;
}

export const LengthConfig: React.FC<Props> = ({ validate }: Props) => {
  const actions = useActions(actionsDefinition);
  const { config } = useSelector(selectCurrentAxisOrDevice)!;
  const validationErr = useSelector(selectValidationErr)!;
  const resultError = useSelector(selectResultError);

  const hasError = (validate && validationErr.travelDistance != null) || (resultError?.errContext === ErrorContext.TRAVEL_DISTANCE);

  return (
    <NoticeBorder type={hasError ? 'error' : null}>
      <HeaderCard
        className="section"
        header={<Flex.Row className="travel-length-header">
          <Text e={Text.Emphasis.Bold}>Travel Length </Text>
          <HelpTooltip>
            <img className="length-sticker-image" src={sticker} alt="Device sticker"/>
            <div>Device maximum travel length can be found on the label.</div>
          </HelpTooltip>
        </Flex.Row>}
      >
        <Flex.Row className="travel-length-input">
          <Input
            type="number"
            min="0"
            max={MAXIMUM_TRAVEL_LENGTH_MM}
            step="0.000001"
            data-testid="travel-distance"
            placeholder="Specify..."
            value={config.travelDistanceMm ?? ''}
            onChange={({ target: { value } }) => actions.updateConfig({ travelDistanceMm: value !== '' ? +value : undefined })}
          />
          mm
        </Flex.Row>
      </HeaderCard>
      {hasError &&
        <NoticeMessage className="lc40-error-message">
          {validate && validationErr.travelDistance}
          {resultError?.errContext === ErrorContext.TRAVEL_DISTANCE && resultError.message}
        </NoticeMessage>
      }
    </NoticeBorder>
  );
};
