import React from 'react';
import { AnimationClasses, Flex, Icons, Loader, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';
import { ProcessControllerMode } from '@zaber/motion/product';
import { Voltage } from '@zaber/motion';
import { match, P } from 'ts-pattern';

import type { ControllerInfo } from '../types';
import { InputWithUnits } from '../../units';
import { EntityKey, makeAxisKey } from '../../keys';
import { selectProcessLabels, selectProcesses } from '../selectors';
import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import { DigitalInputs, DigitalOutputs } from '../../device_io/DeviceIo';
import { InputWithFixedUnit } from '../../units/InputWithFixedUnit';


const modeName = (mode: ProcessControllerMode): string => (
  mode === ProcessControllerMode.MANUAL ? 'Manual' :
  mode === ProcessControllerMode.ON_OFF ? 'Two-State (On/Off)' :
  mode === ProcessControllerMode.PID ? 'PID Standard' :
  mode === ProcessControllerMode.PID_HEATER ? 'PID Heater' :
  'Unknown'
);

const ChannelRow: React.FC<{ entity: EntityKey; process: number; info: ControllerInfo }> = ({ entity, process, info }) => {
  const processInfo = useSelector(selectProcesses)[process];
  const label = useSelector(selectProcessLabels)[process].join(' ');
  const actions = useActions(actionDefinitions);

  if (info.bridges[process].reading) {
    return <Flex.Row>
      <Text>{label}</Text>
      <div className="spacer"/>
      <Loader size="small"/>
    </Flex.Row>;
  }

  return <Flex.Row>
    <Text>{label}</Text>
    <div className="spacer"/>
    {processInfo ? <Text className="mode" e={Text.Emphasis.Light}>{modeName(processInfo.mode)}</Text> : <Loader size="small"/>}
    <InputWithUnits
      unitFrom={{ setting: 'process.voltage', entity: makeAxisKey(entity, process) }}
      measure={{ value: info.voltsOut[process], units: Voltage.V }}
      disabled="readonly"
      unitsEditable={false}
      onChange={() => null}
    />
    {match(info.faults[process])
      .with(P.nullish, () => match(info.states[process])
        .with({ reading: true }, () => <Icons.Refresh className={AnimationClasses.Rotation}/>)
        .otherwise(({ on }) => (
          <Icons.Power
            title={`Turn ${label} ${on ? 'off' : 'on'}`}
            className={on ? 'on' : 'off'}
            onClick={() => actions.processPower(process, !on)}
          />
        ))
      )
      .otherwise(() => <Icons.ErrorWarning className="fault"/>)
    }
  </Flex.Row>;
};

const ChannelRowGroup: React.FC<{ entity: EntityKey; process: 1 | 3; info: ControllerInfo }> = ({ entity, process, info }) => {
  if (info.bridges[process].on) {
    return <ChannelRow {...{ entity, process, info }}/>;
  } else {
    return <>
      <ChannelRow {...{ entity, process, info }}/>
      <ChannelRow process={process + 1} {...{ entity, info }}/>
    </>;
  }
};

export const Dashboard: React.FC<{ entity: EntityKey; info: ControllerInfo }> = ({ entity, info }) => (
  <div className="dashboard">
    <div className="inputs">
      <div className="column">
        <Text t={Text.Type.H5}>Thermistor Inputs</Text>
        <InputWithFixedUnit
          labelContent="TC1 Measured"
          value={info.temperatures[1].value}
          unit="˚C"
          disabled="readonly"
        />
        <InputWithFixedUnit
          labelContent="TC2 Measured"
          value={info.temperatures[2].value}
          unit="˚C"
          disabled="readonly"
        />
      </div>
      <div className="column">
        <Text t={Text.Type.H5}>Analog Inputs</Text>
        <InputWithFixedUnit
          labelContent="AI1 Measured"
          value={info.analogIns[1].value}
          unit="V"
          disabled="readonly"
        />
        <InputWithFixedUnit
          labelContent="AI2 Measured"
          value={info.analogIns[2].value}
          unit="V"
          disabled="readonly"
        />
      </div>
    </div>
    <hr/>
    <div className="outputs">
      <ChannelRowGroup process={1} entity={entity} info={info}/>
      <ChannelRowGroup process={3} entity={entity} info={info}/>
    </div>
    <hr/>
    <div className="device-io">
      <DigitalInputs inputs={info.digitalIns}/>
      <DigitalOutputs deviceKey={entity} outputs={info.digitalOuts}/>
    </div>
  </div>
);
