import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';
import { SimpleSelect } from '@zaber/react-library';
import { P, match } from 'ts-pattern';

import type { EntityKey } from '../keys';
import { selectDevices } from '../connection_manager';
import { InputWithUnits } from '../units';

import { actions as actionsDefinition } from './actions';
import { ACTION_TYPES, OPERATIONS, Operation, type ActionType } from './types';
import {
  TriggerActionCommandState,
  TriggerActionSettingState,
  TriggerActionSettingToSettingState,
  defaultTriggerActionCommandState,
  defaultTriggerActionSettingState,
  defaultTriggerActionSettingToSettingState,
  defaultTriggerActionState,
  defaultValue,
  type TriggerActionState
} from './reducer';
import { selectAxisCount, selectHasActionSettingToSetting, selectIsController } from './selectors';
import { SelectAxis, SelectSetting } from './components';
import { SelectCommand } from './SelectCommand';


interface TriggerActionProps {
  deviceKey: EntityKey;
  triggerIndex: number;
  actionIndex: number;
  inputState: TriggerActionState;
}

export const TriggerAction: React.FC<TriggerActionProps> = ({ deviceKey, triggerIndex, actionIndex, inputState }) => {
  const actions = useActions(actionsDefinition);

  const deviceInfo = useSelector(selectDevices)[deviceKey];
  const isController = useSelector(selectIsController);
  const axisCount = useSelector(selectAxisCount);
  const hasActionSettingToSetting = useSelector(selectHasActionSettingToSetting);

  const multiAxis = axisCount > 1 || isController;

  const changeInput = (state: Partial<TriggerActionState>) =>
    actions.setActionInput(triggerIndex, actionIndex, { ...inputState, ...state } as TriggerActionState, true);

  const actionTypes = ACTION_TYPES.filter(type => {
    if (type === 'Set Setting to Setting') {
      return hasActionSettingToSetting;
    }
    return true;
  });

  return <>
    <SimpleSelect<ActionType>
      title={`Select Action ${getActionLetter(actionIndex)} Type`}
      className="select-action-type"
      value={inputState.type}
      isSearchable
      onValueChange={type => {
        if (type === 'Command') {
          changeInput(defaultTriggerActionCommandState);
        } else if (type === 'Set Setting to Value') {
          changeInput(defaultTriggerActionSettingState);
        } else if (type === 'Set Setting to Setting') {
          changeInput(defaultTriggerActionSettingToSettingState);
        } else {
          changeInput(defaultTriggerActionState);
        }
      }}
      options={actionTypes.map(type => ({ value: type, label: type }))}
    />
    {match(inputState)
      .with(P.when(state => state.type === 'Command'), (state: TriggerActionCommandState) => <>
        {multiAxis &&
          <SelectAxis
            axes={axisCount}
            axis={state.axis}
            allowDevice
            onChange={axis => changeInput({ axis })}
          />}
        <SelectCommand
          command={state.command}
          onChange={command => changeInput({
            command,
            axis: multiAxis ? state.axis : 0,
          })}
        />
      </>)
      .with(P.when(state => state.type === 'Set Setting to Value'), (state: TriggerActionSettingState) => {
        const axisKey = deviceInfo ? deviceInfo.axes[state.axis > 0 ? state.axis - 1 : 0] : null;
        const unitFrom = {
          setting: state.setting,
          entity: state.axis === 0 ? deviceKey : axisKey,
        };
        return <>
          {multiAxis &&
            <SelectAxis
              axes={axisCount}
              axis={state.axis}
              allowDevice
              onChange={axis => changeInput({
                axis,
                setting: '',
                value: defaultValue
              })}
            />}
          <SelectSetting
            settingsFor={state.axis === 0 ? 'all' : state.axis}
            setting={state.setting}
            write
            onChange={setting => changeInput({
              setting,
              value: defaultValue,
              axis: multiAxis ? state.axis : 0,
            })}
          />
          <SelectOperation
            operation={state.operation}
            onChange={operation => changeInput({ operation })}
          />
          <InputWithUnits
            title="Setting Value"
            className="input-value"
            unitFrom={unitFrom}
            measure={state.value}
            onChange={value => {
              const inputChanged = !(inputState.type === 'Set Setting to Value' && inputState.value.units === 'default');
              actions.setActionInput(triggerIndex, actionIndex, { ...(inputState as TriggerActionSettingState), value }, inputChanged);
            }}
          />
        </>;
      })
      .with(P.when(state => state.type === 'Set Setting to Setting'), (state: TriggerActionSettingToSettingState) => <>
        {multiAxis &&
          <SelectAxis
            axes={axisCount}
            axis={state.axis}
            allowDevice
            onChange={axis => changeInput({
              axis,
              setting: '',
            })}
          />}
        <SelectSetting
          settingsFor={state.axis === 0 ? 'all' : state.axis}
          setting={state.setting}
          write={true}
          onChange={setting => changeInput({
            setting,
            axis: multiAxis ? state.axis : 0,
          })}
        />
        <SelectOperation
          operation={state.operation}
          onChange={operation => changeInput({ operation })}
        />
        {multiAxis &&
          <SelectAxis
            title="Select Source Axis"
            axes={axisCount}
            axis={state.fromAxis}
            allowDevice
            onChange={fromAxis => changeInput({
              fromAxis,
              fromSetting: '',
            })}
          />}
        <SelectSetting
          settingsFor={state.fromAxis === 0 ? 'all' : state.fromAxis}
          title="Select Source Setting"
          setting={state.fromSetting}
          onChange={fromSetting => changeInput({
            fromSetting,
            fromAxis: multiAxis ? state.fromAxis : 0,
          })}
        />
      </>)
      .otherwise(() => null)}
  </>;
};

interface SelectOperationProps {
  operation: Operation;
  onChange: (operation: Operation) => void;
}

const SelectOperation: React.FC<SelectOperationProps> = ({ operation, onChange }) =>
  <SimpleSelect<Operation>
    title="Select Operation"
    className="select-operator"
    value={operation}
    onValueChange={onChange}
    options={OPERATIONS.map(op => ({ value: op, label: op }))}
  />;

export function getActionLetter(i: number): string {
  return String.fromCharCode(i + 65);
}
