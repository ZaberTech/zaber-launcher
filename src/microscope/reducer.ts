import { P } from 'ts-pattern';
import { Length } from '@zaber/motion';
import _ from 'lodash';

import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes, DevicesLoadedPayload } from '../connection_manager';
import { EntityKey, tryExtractConnectionKey } from '../keys';
import { createReducer, isMatch } from '../utils';
import { convertBetweenUnits, MeasurementOK, pureMeasurement } from '../units';
import { ConnectionStatusActionTypes, type ConnectionStatusActionPayloads } from '../connection_status';

import { ActionsToPayloads, ActionTypes } from './actions';
import {
  type MonitorStableInfo, type MonitorInfo, type Operations, type Axes, type OperationState,
  isOperationError,
  StoredPosition,
  AXES,
  type MicroscopeConfig,
  TriggerSettings,
  EMPTY_TRIGGER_SETTINGS,
} from './types';
import type { Preset } from './presets';

interface AxisState {
  step: MeasurementOK;
}

const initialAxisState: AxisState = {
  step: { value: 10, units: Length.MICROMETRES },
};

export interface State {
  mounted: boolean;
  selectedKey: EntityKey | null;
  productionMode: boolean;
  demoMode: boolean;
  config: {
    loading: { loading: true } | { loaded: MicroscopeConfig } | { error: string } | null;
    writing: { writing: true } | { success: true } | { error: string } | null;
    wdiSearch: { searching: true } | { host: string } | { error: string } | null;
    dialogOpen: boolean;
    edited: MicroscopeConfig;
  };
  test: { error?: string } | { inProgress: string } | null;
  monitor: {
    stableInfo: MonitorStableInfo | null;
    info: MonitorInfo | null;
    refreshing: boolean;
    refreshPending: boolean;
    burstCounter: number;
    blockedBy: Record<string, boolean>;
    error: string | null;
  };
  operations: Partial<Record<Operations, OperationState>>;
  positionUnits: Record<Axes, Length>;
  axes: Record<Axes, AxisState>;
  presets: Preset[];
  preset: {
    current: number | null;
    error: string | null;
    applying: boolean;
  };
  presetEditor: {
    open: boolean;
    presets: Preset[];
  };
  storedPositions: StoredPosition[];
  autofocusSettings: {
    open: boolean;
  };
  factoryReset: {
    step: 'confirm' | 'perform' | 'show-errors' | null;
    errors: string[];
  };
  validationErrors: string[];
  cameraTrigger: {
    open: boolean;
    settings: TriggerSettings;
    beforeOpen: TriggerSettings;
  };
}

const initialState: State = {
  mounted: false,
  selectedKey: null,
  productionMode: false,
  demoMode: false,
  config: {
    loading: null,
    writing: null,
    wdiSearch: null,
    dialogOpen: false,
    edited: {
      filterChanger: null,
      x: null,
      y: null,
      focus: null,
      autofocus: null,
      cameraTrigger: null,
    },
  },
  test: null,
  monitor: {
    stableInfo: null,
    info: null,
    refreshing: false,
    refreshPending: true,
    burstCounter: 0,
    blockedBy: {},
    error: null,
  },
  operations: {},
  positionUnits: {
    xAxis: Length.MILLIMETRES,
    yAxis: Length.MILLIMETRES,
    focusAxis: Length.MILLIMETRES,
  },
  axes: {
    xAxis: initialAxisState,
    yAxis: initialAxisState,
    focusAxis: initialAxisState,
  },
  presets: [],
  preset: {
    current: null,
    error: null,
    applying: false,
  },
  presetEditor: {
    open: false,
    presets: [],
  },
  storedPositions: [],
  autofocusSettings: {
    open: false,
  },
  factoryReset: {
    step: null,
    errors: [],
  },
  validationErrors: [],
  cameraTrigger: {
    open: false,
    settings: EMPTY_TRIGGER_SETTINGS,
    beforeOpen: EMPTY_TRIGGER_SETTINGS,
  },
};


function preservedState(state: State): Partial<State> {
  return {
    mounted: state.mounted,
    productionMode: state.productionMode,
    positionUnits: state.positionUnits,
  };
}

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const setSelectedKey: Reducer<ActionTypes.SET_SELECTED_KEY> = (state, { key }) => ({
  ...initialState,
  ...preservedState(state),
  selectedKey: key,
  config: state.selectedKey === key ? state.config : initialState.config,
});

const mount: Reducer<ActionTypes.MOUNT> = state => ({ ...state, mounted: true });
const unmount: Reducer<ActionTypes.UNMOUNT> = state => ({ ...state, mounted: false });

const setProductionMode: Reducer<ActionTypes.SET_PRODUCTION_MODE> = (state, { production }) => ({
  ...state,
  productionMode: production,
});
const setDemoMode: Reducer<ActionTypes.SET_DEMO_MODE> = (state, { demo }) => ({
  ...state,
  demoMode: demo,
});

const loadConfig: Reducer<ActionTypes.LOAD_CONFIG> = state => ({
  ...state,
  config: {
    ...state.config,
    loading: { loading: true },
  },
});

const loadConfigDone: Reducer<ActionTypes.LOAD_CONFIG_DONE> = (state, result) => ({
  ...state,
  config: {
    ...state.config,
    loading: result,
    edited: 'loaded' in result ? result.loaded : initialState.config.edited,
  },
});

const editConfig: Reducer<ActionTypes.EDIT_CONFIG> = (state, { config }) => ({
  ...state,
  config: {
    ...state.config,
    edited: { ...state.config.edited, ...config },
    writing: isMatch({ success: true }, state.config.writing) ? null : state.config.writing,
  }
});

const resetConfigDialog: Reducer<ActionTypes.RESET_CONFIG_DIALOG> = (state, { open }) => ({
  ...state,
  config: {
    ...initialState.config,
    dialogOpen: open,
  },
});

const writeConfig: Reducer<ActionTypes.WRITE_CONFIG> = state => ({
  ...state,
  config: {
    ...state.config,
    writing: { writing: true },
  },
});

const writeConfigDone: Reducer<ActionTypes.WRITE_CONFIG_DONE> = (state, { error }) => ({
  ...state,
  config: {
    ...state.config,
    writing: error ? { error } : { success: true },
    dialogOpen: error ? state.config.dialogOpen : false,
  },
});

const test: Reducer<ActionTypes.TEST> = state => ({
  ...state,
  test: { inProgress: '' },
});

const testDone: Reducer<ActionTypes.TEST_DONE> = (state, { error }) => {
  if (!isMatch({ inProgress: P.string }, state.test)) { return state }

  if (error == null) {
    return { ...state, test: null };
  }

  const message = `${state.test.inProgress.replace('...', ':')} ${error}`;
  return ({
    ...state,
    test: { error: message },
  });
};

const testProgress: Reducer<ActionTypes.TEST_PROGRESS> = (state, { progress }) => ({
  ...state,
  test: isMatch({ inProgress: P.any }, state.test) ? { inProgress: progress } : state.test,
});

const clearTestResult: Reducer<ActionTypes.CLEAR_TEST_RESULT> = state => ({
  ...state,
  test: null,
});

const restartMonitor: Reducer<ActionTypes.RESTART_MONITOR> = state => ({
  ...state,
  monitor: initialState.monitor,
  operations: initialState.operations,
});

const monitorStableInfo: Reducer<ActionTypes.MONITOR_STABLE_INFO> = (state, { info }) => ({
  ...state,
  monitor: {
    ...initialState.monitor,
    stableInfo: info,
  },
  operations: initialState.operations,
  validationErrors: [],
});

const monitorRefresh: Reducer<ActionTypes.MONITOR_REFRESH> = (state, { burst }) => ({
  ...state,
  monitor: {
    ...state.monitor,
    refreshPending: true,
    burstCounter: Math.max(state.monitor.burstCounter, burst ?? 0),
  },
});

const monitorRefreshStarted: Reducer<ActionTypes.MONITOR_REFRESH_STARTED> = state => ({
  ...state,
  monitor: {
    ...state.monitor,
    refreshPending: false,
    refreshing: true,
    burstCounter: Math.max(state.monitor.burstCounter - 1, 0),
  },
});

const monitorInfo: Reducer<ActionTypes.MONITOR_INFO> = (state, { info }) => ({
  ...state,
  monitor: { ...state.monitor, info, error: null, refreshing: false },
});

const monitorError: Reducer<ActionTypes.MONITOR_ERROR> = (state, { error }) => ({
  ...state,
  monitor: { ...state.monitor, error, refreshing: false },
});

const blockMonitor: Reducer<ActionTypes.BLOCK_MONITOR> = (state, { process, block }) => ({
  ...state,
  monitor: {
    ...state.monitor,
    blockedBy: {
      ...state.monitor.blockedBy,
      [process]: block,
    },
  },
});

const operationStart: Reducer<ActionTypes.OPERATION_START> = (state, { name }) => ({
  ...state,
  operations: { ...state.operations, [name]: { inProgress: true } },
});

const operationDone: Reducer<ActionTypes.OPERATION_DONE> = (state, { name, error, result }) => ({
  ...state,
  operations: { ...state.operations, [name]: error ? { error } : { result } },
});

const operationClearError: Reducer<ActionTypes.OPERATION_CLEAR_ERROR> = (state, { name }) => ({
  ...state,
  operations: isOperationError(state.operations[name]) ? { ...state.operations, [name]: null } : state.operations,
});


const factoryResetSetStep: Reducer<ActionTypes.FACTORY_RESET_SET_STEP> = (state, { step }) => ({
  ...state,
  factoryReset: {
    ...state.factoryReset,
    step,
    errors: step === 'perform' ? [] : state.factoryReset.errors,
  },
});


const factoryResetAddError: Reducer<ActionTypes.FACTORY_RESET_ADD_ERROR> = (state, { error }) => ({
  ...state,
  factoryReset: {
    ...state.factoryReset,
    errors: [...state.factoryReset.errors, error],
  },
});


const convertAxisPosition = (state: MonitorInfo['xAxis'], units: Length) => state && ({
  ...state,
  position: {
    value: convertBetweenUnits(state.position.value, state.position.units, units),
    units,
  },
});

const convertStoredPosition = (old: StoredPosition, units: Record<Axes, Length>): StoredPosition =>
  AXES.reduce((position, axis) => ({
    ...position,
    [axis]: {
      value: convertBetweenUnits(position[axis].value, position[axis].units, units[axis]),
      units: units[axis],
    },
  }), old);


const updateUnits = (state: State, { axis, units }: ActionsToPayloads[ActionTypes.UPDATE_UNITS]): State => {
  const positionUnits = {
    ...state.positionUnits,
    [axis]: units,
  };

  let autofocus = state.monitor.info?.autofocus ?? null;
  if (axis === 'focusAxis' && autofocus) {
    const { min, max } = autofocus.limits;
    autofocus = {
      ...autofocus,
      limits: {
        min: {
          value: convertBetweenUnits(min.value, min.units, units),
          units,
        },
        max: {
          value: convertBetweenUnits(max.value, max.units, units),
          units,
        },
      },
    };
  }

  return ({
    ...state,
    positionUnits,
    monitor: {
      ...state.monitor,
      info: state.monitor.info && {
        ...state.monitor.info,
        [axis]: convertAxisPosition(state.monitor.info[axis], units),
        autofocus,
      },
    },
    storedPositions: state.storedPositions.map(pos => convertStoredPosition(pos, positionUnits)),
  });
};

const updateAxisStep = (state: State, { axis, step }: ActionsToPayloads[ActionTypes.UPDATE_AXIS_STEP]): State => ({
  ...state,
  axes: {
    ...state.axes,
    [axis]: {
      ...state.axes[axis],
      step: pureMeasurement(step),
    },
  },
});

const applyPreset: Reducer<ActionTypes.APPLY_PRESET> = (state, { preset: index }) => {
  const preset = state.presets[index];
  if (preset == null) { return state }
  return ({
    ...state,
    preset: {
      ...state.preset,
      error: null,
      applying: true,
    },
  });
};

const applyPresetData: Reducer<ActionTypes.APPLY_PRESET_DATA> = (
  state,
  { preset: { axes, cameraTrigger: trigger } },
) => ({
  ...state,
  axes: _.mapValues(state.axes, (axisState, axis) => ({
    ...axisState,
    step: axes[axis as Axes].step.apply ? axes[axis as Axes].step.value : axisState.step,
  })),
  cameraTrigger: {
    ...state.cameraTrigger,
    settings: _.mapValues(state.cameraTrigger.settings, (value, key) =>
      trigger[key as keyof TriggerSettings].apply ? trigger[key as keyof TriggerSettings].value : value),
  },
});

const applyPresetDone: Reducer<ActionTypes.APPLY_PRESET_DONE> = (state, { preset, error }) => ({
  ...state,
  preset: {
    ...state.preset,
    current: error ? state.preset.current : preset,
    error: error ?? null,
    applying: false,
  },
});

const addPreset: Reducer<ActionTypes.ADD_PRESET> = (state, { preset }) => {
  const { presets } = state.presetEditor;
  let index = presets.length + 1;
  let name: string;
  do {
    name = `Preset ${index}`;
    index++;
  } while (presets.some(p => p.name === name));

  return ({
    ...state,
    presetEditor: {
      ...state.presetEditor,
      presets: [...presets, { name, data: preset }],
    },
  });
};

const updatePreset: Reducer<ActionTypes.UPDATE_PRESET> = (state, { preset: index, update }) => ({
  ...state,
  presetEditor: {
    ...state.presetEditor,
    presets: state.presetEditor.presets.map((preset, i) => i === index ? _.merge({}, preset, update) : preset),
  },
});

const removePreset: Reducer<ActionTypes.REMOVE_PRESET> = (state, { preset }) => ({
  ...state,
  presetEditor: {
    ...state.presetEditor,
    presets: state.presetEditor.presets.filter((_, i) => i !== preset),
  },
});

const presetEditorOpen: Reducer<ActionTypes.PRESET_EDITOR_OPEN> = state => ({
  ...state,
  presetEditor: {
    open: true,
    presets: state.presets,
  },
});

const presetEditorSave: Reducer<ActionTypes.PRESET_EDITOR_SAVE> = state =>  {
  const presets = _.sortBy(state.presetEditor.presets, 'name');

  const { current } = state.preset;
  let newCurrent = current;
  if (current != null) {
    newCurrent = presets.findIndex(p => p.name === state.presets[current]?.name);
    newCurrent = newCurrent >= 0 ? newCurrent : null;
  }

  return ({
    ...state,
    presets,
    preset: {
      ...state.preset,
      current: newCurrent,
    },
    presetEditor: initialState.presetEditor,
  });
};

const presetEditorCancel: Reducer<ActionTypes.PRESET_EDITOR_CANCEL> = state => ({
  ...state,
  presetEditor: initialState.presetEditor,
});

const presetsLoaded: Reducer<ActionTypes.PRESETS_LOADED> = (state, { presets }) => ({
  ...state,
  presets,
});

const loadStoredPositions: Reducer<ActionTypes.LOAD_STORED_POSITIONS> = (state, { positions }) => ({
  ...state,
  storedPositions: positions.map(pos => convertStoredPosition(pos, state.positionUnits)),
});
const updateStoredPosition: Reducer<ActionTypes.UPDATE_STORED_POSITION> = (state, { index, position }) => ({
  ...state,
  storedPositions: state.storedPositions.map((other, i) => i === index ? { ...other, ...position } : other),
});
const deleteStoredPosition: Reducer<ActionTypes.DELETE_STORED_POSITION> = (state, { index }) => ({
  ...state,
  storedPositions: state.storedPositions.filter((_, i) => i !== index),
});
const addStoredPosition: Reducer<ActionTypes.ADD_STORED_POSITION> = (state, { position }) => ({
  ...state,
  storedPositions: [...state.storedPositions, convertStoredPosition(position, state.positionUnits)],
});

const setAutofocusSettingsOpen: Reducer<ActionTypes.SET_AUTOFOCUS_SETTINGS_OPEN> = (state, { open }) => ({
  ...state,
  autofocusSettings: {
    ...state.autofocusSettings,
    open,
  },
});

const devicesLoaded = (state: State, { connectionKey }: DevicesLoadedPayload): State =>
  tryExtractConnectionKey(state.selectedKey) === connectionKey ? {
    ...initialState,
    ...preservedState(state),
    selectedKey: state.selectedKey,
  } : state;

const routerDisconnect = (
  state: State,
  { connectionKey }: ConnectionStatusActionPayloads[ConnectionStatusActionTypes.ROUTED_CONNECTION_DISCONNECT],
): State => connectionKey === state.selectedKey ? ({
  ...initialState,
  ...preservedState(state),
}) : state;

const searchWdi = (state: State): State => ({
  ...state,
  config: {
    ...state.config,
    wdiSearch: { searching: true },
  },
});

const searchWdiDone: Reducer<ActionTypes.SEARCH_WDI_DONE> = (state, result) => ({
  ...state,
  config: {
    ...state.config,
    wdiSearch: result,
    edited: 'host' in result ? {
      ...state.config.edited,
      autofocus: {
        provider: 'wdi',
        host: result.host,
        port: null,
      },
    } : state.config.edited,
  },
});

const clearValidationErrors: Reducer<ActionTypes.CLEAR_VALIDATION_ERRORS> = state => ({
  ...state,
  validationErrors: [],
});

const addValidationError: Reducer<ActionTypes.ADD_VALIDATION_ERROR> = (state, { error }) => ({
  ...state,
  validationErrors: [...state.validationErrors, error],
});

const setTriggerSettingsOpen: Reducer<ActionTypes.SET_TRIGGER_SETTINGS_OPEN> = (state, { change }) => ({
  ...state,
  cameraTrigger: {
    ...state.cameraTrigger,
    open: change === 'open',
    beforeOpen: change === 'open' ? state.cameraTrigger.settings : EMPTY_TRIGGER_SETTINGS,
    settings: change === 'close' ? state.cameraTrigger.beforeOpen : state.cameraTrigger.settings,
  },
});

const updateTriggerSettings: Reducer<ActionTypes.UPDATE_TRIGGER_SETTINGS> = (state, { settings }) => ({
  ...state,
  cameraTrigger: {
    ...state.cameraTrigger,
    settings: {
      ...state.cameraTrigger.settings,
      ...settings,
    },
  },
});

export const reducer = createReducer<
  ActionsToPayloads & ConnectionManagerActionPayloads & ConnectionStatusActionPayloads, typeof initialState
>({
  [ActionTypes.SET_SELECTED_KEY]: setSelectedKey,
  [ActionTypes.MOUNT]: mount,
  [ActionTypes.UNMOUNT]: unmount,
  [ActionTypes.SET_PRODUCTION_MODE]: setProductionMode,
  [ActionTypes.SET_DEMO_MODE]: setDemoMode,
  [ActionTypes.LOAD_CONFIG]: loadConfig,
  [ActionTypes.LOAD_CONFIG_DONE]: loadConfigDone,
  [ActionTypes.EDIT_CONFIG]: editConfig,
  [ActionTypes.RESET_CONFIG_DIALOG]: resetConfigDialog,
  [ActionTypes.WRITE_CONFIG]: writeConfig,
  [ActionTypes.WRITE_CONFIG_DONE]: writeConfigDone,
  [ActionTypes.TEST]: test,
  [ActionTypes.TEST_PROGRESS]: testProgress,
  [ActionTypes.TEST_DONE]: testDone,
  [ActionTypes.CLEAR_TEST_RESULT]: clearTestResult,
  [ActionTypes.SEARCH_WDI]: searchWdi,
  [ActionTypes.SEARCH_WDI_DONE]: searchWdiDone,
  [ActionTypes.RESTART_MONITOR]: restartMonitor,
  [ActionTypes.MONITOR_STABLE_INFO]: monitorStableInfo,
  [ActionTypes.MONITOR_INFO]: monitorInfo,
  [ActionTypes.MONITOR_ERROR]: monitorError,
  [ActionTypes.MONITOR_REFRESH]: monitorRefresh,
  [ActionTypes.MONITOR_REFRESH_STARTED]: monitorRefreshStarted,  [ActionTypes.OPERATION_START]: operationStart,
  [ActionTypes.BLOCK_MONITOR]: blockMonitor,
  [ActionTypes.OPERATION_DONE]: operationDone,
  [ActionTypes.OPERATION_CLEAR_ERROR]: operationClearError,
  [ActionTypes.FACTORY_RESET_SET_STEP]: factoryResetSetStep,
  [ActionTypes.FACTORY_RESET_ADD_ERROR]: factoryResetAddError,
  [ActionTypes.UPDATE_UNITS]: updateUnits,
  [ActionTypes.UPDATE_AXIS_STEP]: updateAxisStep,
  [ActionTypes.ADD_PRESET]: addPreset,
  [ActionTypes.APPLY_PRESET]: applyPreset,
  [ActionTypes.APPLY_PRESET_DATA]: applyPresetData,
  [ActionTypes.APPLY_PRESET_DONE]: applyPresetDone,
  [ActionTypes.UPDATE_PRESET]: updatePreset,
  [ActionTypes.REMOVE_PRESET]: removePreset,
  [ActionTypes.PRESET_EDITOR_OPEN]: presetEditorOpen,
  [ActionTypes.PRESET_EDITOR_SAVE]: presetEditorSave,
  [ActionTypes.PRESET_EDITOR_CANCEL]: presetEditorCancel,
  [ActionTypes.PRESETS_LOADED]: presetsLoaded,
  [ActionTypes.LOAD_STORED_POSITIONS]: loadStoredPositions,
  [ActionTypes.ADD_STORED_POSITION]: addStoredPosition,
  [ActionTypes.UPDATE_STORED_POSITION]: updateStoredPosition,
  [ActionTypes.DELETE_STORED_POSITION]: deleteStoredPosition,
  [ActionTypes.SET_AUTOFOCUS_SETTINGS_OPEN]: setAutofocusSettingsOpen,
  [ActionTypes.CLEAR_VALIDATION_ERRORS]: clearValidationErrors,
  [ActionTypes.ADD_VALIDATION_ERROR]: addValidationError,
  [ActionTypes.SET_TRIGGER_SETTINGS_OPEN]: setTriggerSettingsOpen,
  [ActionTypes.UPDATE_TRIGGER_SETTINGS]: updateTriggerSettings,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
  [ConnectionStatusActionTypes.ROUTED_CONNECTION_DISCONNECT]: routerDisconnect,
}, initialState);
