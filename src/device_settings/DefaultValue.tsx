import React from 'react';
import { Units } from '@zaber/motion';

import { convertNullable, getUnitLabel, getUnits, truncateValue } from '../units';
import type { EntityKey } from '../keys';
import { useUnitInfo } from '../units/hooks';

import type { SettingsEntry } from './reducer';

interface Props {
  setting: SettingsEntry;
  entityKey: EntityKey;
}

export const DefaultValue: React.FC<Props> = ({ setting, entityKey }) => {
  const unitInfo = useUnitInfo({ setting: setting.name, entity: entityKey });

  if (setting.defaultValue == null || setting.type !== 'number') {
    return null;
  }

  const units = getUnits(setting.value);

  const defaultValue = convertNullable(unitInfo, +setting.defaultValue, Units.NATIVE, units);
  if (defaultValue == null || unitInfo == null) { return null }

  return <div className="default-value">
    Default value:&emsp;
    {truncateValue(unitInfo, defaultValue, units)}&nbsp;
    {units && getUnitLabel(units)}
  </div>;
};
