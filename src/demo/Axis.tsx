import { Card, Icons, NoticeBanner, Text } from '@zaber/react-library';
import React from 'react';

import { DeviceNumber } from '../components';
import { connectionName } from '../connection_manager';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import type { AxisStateComplete } from './selectors';

export const Axis: React.FC<{ axis: AxisStateComplete }> = ({ axis }) => {
  const actions = useActions(actionsDefinition);
  return <Card className="axis">
    <div className="header">
      <Text t={Text.Type.H5}>
        {connectionName(axis.connection)}
      </Text>
      <Icons.ArrowCollapsed/>
      <Text t={Text.Type.H5}>
        <DeviceNumber>{axis.device.address}</DeviceNumber>&nbsp;
        {axis.device.identity?.name ?? 'Unknown'}
      </Text>
      {axis.isPeripheralLike && <>
        <Icons.ArrowCollapsed/>
        <Text t={Text.Type.H5}>
          Axis {axis.axisNumber} {axis.identity?.peripheralName ?? 'Unknown'}
        </Text>
      </>}

      <div className="spacer"/>

      {!axis.running && <Icons.RightNormal title="Start Axis" onClick={() => actions.startAxis(axis.key)}/>}
      {axis.running && <Icons.Stop title="Stop Axis" onClick={() => actions.stopAxis(axis.key)}/>}
    </div>
    {axis.error && <NoticeBanner>{axis.error}</NoticeBanner>}
  </Card>;
};
