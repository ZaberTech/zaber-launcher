import { throwUnexpectedError } from '@zaber/toolbox';
import { Length, microscopy, NamedParameter } from '@zaber/motion';
import { put } from 'redux-saga/effects';

import { notNil, SagaIter } from '../utils';

import {
  Axes, MonitorStableInfo,
} from './types';
import { PresetData } from './presets';
import { changeObjectiveBetweenObjectives } from './operation_sagas';
import { actions } from './actions';

type CatchErrorResult = { error: string } | null;

async function catchError(
  fn: (changeTag: (tag: string) => void) => Promise<void>,
  firstTag: string = 'unknown',
): Promise<CatchErrorResult> {
  let tag = firstTag;
  try {
    await fn(newTag => { tag = newTag });
    return null;
  } catch (err) {
    throwUnexpectedError(err);
    return { error: `${tag}: ${err.message}` };
  }
}

export function* applyPresetToScope(
  scope: microscopy.Microscope, info: MonitorStableInfo, preset: PresetData,
): SagaIter {
  yield put(actions.applyPresetData(preset));

  const { axes, filter, objective, illuminator } = preset;
  const results: CatchErrorResult[] = yield Promise.all([
    catchError(async () => {
      if (filter.apply && scope.filterChanger) {
        await scope.filterChanger.change(filter.value);
      }
    }, 'Filter Changer'),
    catchError(async setTag => {
      setTag('Objective Changer');
      if (objective.apply && scope.objectiveChanger && info.objectiveChanger) {
        await changeObjectiveBetweenObjectives(scope, info.objectiveChanger, objective.value);
      }

      setTag('Focus Axis');
      if (axes.focusAxis.position.apply && scope.focusAxis) {
        const { value } = axes.focusAxis.position;
        await scope.focusAxis.moveAbsolute(value.value, value.units as Length);
      }

      setTag('Autofocus');
      await applyAutofocusPreset(scope, preset);
    }),
    catchError(async () => {
      if (scope.illuminator == null || info.illuminator == null) {
        return;
      }

      for (const { channel: channelNum } of Object.values(info.illuminator.channels)) {
        const channelPreset = illuminator[channelNum];
        const channel = scope.illuminator.getChannel(channelNum);

        if (channelPreset.intensity.apply) {
          await channel.setIntensity(channelPreset.intensity.value);
        }
        if (channelPreset.on.apply) {
          await channel.setOn(channelPreset.on.value);
        }
      }
    }, 'Illuminator'),
    catchError(async () => {
      await Promise.all((['xAxis', 'yAxis'] satisfies Axes[]).map(async axisName => {
        const axis = scope[axisName];
        if (axis == null) { return }

        const axisPreset = axes[axisName];
        if (axisPreset.position.apply) {
          const { value } = axisPreset.position;
          await axis.moveAbsolute(value.value, value.units as Length);
        }
      }));
    }, 'Plate'),
  ]);

  const errors = results.filter(notNil);
  if (errors.length) {
    throw new Error(`Could not apply preset: ${errors.map(e => e.error).join(', ')}`);
  }
}

async function applyAutofocusPreset(scope: microscopy.Microscope, preset: PresetData) {
  if (scope.autofocus == null) { return }
  const { autofocus } = preset;

  if (autofocus.limitMin.apply) {
    const { value } = autofocus.limitMin;
    await scope.autofocus.setLimitMin(value.value, value.units as Length);
  }
  if (autofocus.limitMax.apply) {
    const { value } = autofocus.limitMax;
    await scope.autofocus.setLimitMax(value.value, value.units as Length);
  }

  const parameters: NamedParameter[] = [];
  for (const [key, { apply, value }] of Object.entries(autofocus.settings)) {
    if (apply) {
      parameters.push({ name: key, value });
    }
  }

  if (parameters.length > 0) {
    let objective = 1;
    if (scope.objectiveChanger) {
      objective = await scope.objectiveChanger.getCurrentObjective();
    }
    await scope.autofocus.setObjectiveParameters(objective, parameters);
  }
}
