import { createSelector } from 'reselect';
import _ from 'lodash';

import type { IdentifiedDeviceInfo } from '../connection_manager';
import { selectIdentifiedDevices, selectIdentifiedAxes, selectRouters } from '../connection_manager/selectors';
import { getDeviceAddress, tryExtractConnectionKey, tryExtractRouterKey } from '../keys';
import { selectFirmwareUpdate } from '../store';
import { tryAccess } from '../utils';
import { Version } from '../app_components';

import { selectSkippedVersions } from './selectors_external';
import { compareVersions, compareWithDeviceVersion, versionCategory, isPossibleUpgrade } from './utility';
import type { VersionCategory } from './types';

export const selectDeviceKey = createSelector(selectFirmwareUpdate, state => state.selectedDeviceKey);
export const selectDeviceInfoWithAxes = createSelector(
  selectDeviceKey, selectIdentifiedDevices, selectIdentifiedAxes, (deviceKey, devices, axes) => {
    const device = tryAccess(devices, deviceKey);
    if (!device) {
      return null;
    }
    const deviceWithAxes: IdentifiedDeviceInfo = {
      ...device,
      axes: device.axes.map(axisKey => axes[axisKey]),
    };
    return deviceWithAxes;
  }
);
export const selectRouter = createSelector(selectDeviceKey, selectRouters, (key, routers) =>
  tryAccess(routers, tryExtractRouterKey(key))
);

export const selectUpgradeProgress = createSelector(selectFirmwareUpdate, state => state.progress);

export const selectCurrentConnection = createSelector(selectFirmwareUpdate, state => {
  const connectionKey = tryExtractConnectionKey(state.selectedDeviceKey);
  return tryAccess(state.connections, connectionKey);
});

export const selectDeviceIdentifiers = createSelector(selectFirmwareUpdate, selectCurrentConnection, (state, connection) => {
  if (!connection) { return null }
  const deviceAddress = getDeviceAddress(state.selectedDeviceKey!);
  return connection.identifiers.find(id => id.DeviceNumber === deviceAddress) ?? null;
});

export const selectDeviceAvailableVersions = createSelector(selectFirmwareUpdate, selectCurrentConnection, (state, connection) => {
  if (!connection) { return [] }
  const deviceAddress = getDeviceAddress(state.selectedDeviceKey!);
  return connection.versions[deviceAddress] ?? [];
});

const versionSort = (v1: Version, v2: Version) => compareVersions(v2, v1);

const selectAllNewerVersions = createSelector(
  selectDeviceInfoWithAxes, selectDeviceAvailableVersions, (device, versions) => {
    if (!device) { return [] }
    const newerVersions = versions.filter(version =>
      isPossibleUpgrade(version, device) && compareWithDeviceVersion(version, device) > 0);
    return newerVersions.sort(versionSort);
  }
);

const selectDeviceSkippedVersion = createSelector(selectSkippedVersions,
  selectDeviceInfoWithAxes, (skippedMap, device) => tryAccess(skippedMap, device?.identity.serialNumber) ?? null);

export const selectNotSkippedNewerVersions = createSelector(
  selectAllNewerVersions, selectDeviceSkippedVersion, (newer, skippedToVersion) => {
    const versions = newer.filter(version => compareVersions(version, skippedToVersion) > 0);
    return versions.sort(versionSort);
  }
);

export const selectSkippedNewerVersions = createSelector(
  selectAllNewerVersions, selectDeviceSkippedVersion, (newer, skippedToVersion) => {
    const versions = newer.filter(version => compareVersions(version, skippedToVersion) <= 0);
    return versions.sort(versionSort);
  }
);

export const selectOlderVersions = createSelector(
  selectDeviceInfoWithAxes, selectDeviceAvailableVersions, (device, versions) => {
    if (!device) { return [] }
    const olderVersions = versions.filter(version =>
      isPossibleUpgrade(version, device) && compareWithDeviceVersion(version, device) < 0);
    return olderVersions.sort(versionSort);
  }
);

export const selectOtherVersions = createSelector(
  selectDeviceInfoWithAxes, selectDeviceAvailableVersions, (device, versions) => {
    if (!device) { return null }
    const otherVersions = versions.filter(version => !isPossibleUpgrade(version, device));
    otherVersions.sort(versionSort);
    const categories = _.groupBy<Version>(otherVersions, version => versionCategory(version.parsed));
    return categories as Record<VersionCategory, Version[]>;
  }
);

export const selectCurrentVersion = createSelector(selectDeviceInfoWithAxes, selectDeviceAvailableVersions, (device, versions) => {
  if (!device) { return null }
  const versionOnDevice = versions.find(version => compareWithDeviceVersion(version, device) === 0);
  return versionOnDevice ?? null;
});
