import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

export enum ActionTypes {
  BEGIN = 'FACTORY_RESET_BEGIN',
  CONFIRM = 'FACTORY_RESET_CONFIRM',
  DONE = 'FACTORY_RESET_DONE',
  CLEAR = 'FACTORY_RESET_CLEAR',
}

export interface ActionsToPayloads {
  [ActionTypes.BEGIN]: { deviceKey: EntityKey };
  [ActionTypes.CONFIRM]: { hardReset: boolean };
  [ActionTypes.DONE]: { error?: string };
  [ActionTypes.CLEAR]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  begin: (deviceKey: EntityKey) => buildAction(ActionTypes.BEGIN, { deviceKey }),
  confirm: (hardReset: boolean) => buildAction(ActionTypes.CONFIRM, { hardReset }),
  done: (error?: string) => buildAction(ActionTypes.DONE, { error }),
  clear: () => buildAction(ActionTypes.CLEAR),
};
