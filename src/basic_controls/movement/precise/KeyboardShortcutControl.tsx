import React from 'react';
import { iconFromSvg, PopUp, Text, UnorderedList, Select } from '@zaber/react-library';
import { match } from 'ts-pattern';

import { KeyPressCombo } from '../types';
import { actionDefinitions } from '../actions';
import { useActions } from '../../../utils';
import { environment } from '../../../environment';
import type { IdentifiedAxisState } from '../../../connection_manager';
import UpDownSVG from '../../../assets/basic_controls_up_down.svg';
import PageUpDownSVG from '../../../assets/basic_controls_page_updown.svg';
import OffSVG from '../../../assets/basic_controls_off.svg';

interface ShortCutIconProps {
  shortcut: KeyPressCombo;
}

interface ShortcutToggleProps {
    axis: IdentifiedAxisState;
    shortcut: KeyPressCombo;
}

interface ShortcutSelectOptions {
  value: number;
  label: string;
  icon: JSX.Element;
}

const UpDownIcon = iconFromSvg(UpDownSVG);
const PageUpDownIcon = iconFromSvg(PageUpDownSVG);
const OffIcon = iconFromSvg(OffSVG);

const ShortCutOff = <OffIcon className="short-cut-icon"/>;
const ShortCutLeftRight = <UpDownIcon className="short-cut-icon rotate-90"/>;
const ShortCutUpDown = <UpDownIcon className="short-cut-icon"/>;
const ShortCutPageUpPageDown = <PageUpDownIcon className="short-cut-icon"/>;

const SHORTCUT_LABEL_DATA: Record<KeyPressCombo, { label: string; icon: JSX.Element }> = {
  [KeyPressCombo.None]: { label: 'Off', icon: ShortCutOff },
  [KeyPressCombo.LeftRight]: { label: 'Left / Right', icon: ShortCutLeftRight },
  [KeyPressCombo.UpDown]: { label: 'Up / Down', icon: ShortCutUpDown },
  [KeyPressCombo.PageUpPageDown]: { label: 'Page Up / Page Down', icon: ShortCutPageUpPageDown },
};

export const ShortCutIcon: React.FC<ShortCutIconProps> = ({ shortcut }) => (
  match(shortcut)
    .with(KeyPressCombo.LeftRight, () => (
      <PopUp triggerAction="hover" trigger={() => ShortCutLeftRight}>
        Axis selected for control with left / right keys.
      </PopUp>
    ))
    .with(KeyPressCombo.UpDown, () => (
      <PopUp triggerAction="hover" trigger={() => ShortCutUpDown}>
        Axis selected for control with up / down keys.
      </PopUp>
    ))
    .with(KeyPressCombo.PageUpPageDown, () => (
      <PopUp triggerAction="hover" trigger={() => ShortCutPageUpPageDown}>
        Axis selected for control with page up / page down keys.
      </PopUp>
    ))
    .otherwise(() => null)
);

export const ShortCutDropDown: React.FC<ShortcutToggleProps> = ({ axis, shortcut }) => {
  const { key } = axis;
  const actions = useActions(actionDefinitions);

  const options: ShortcutSelectOptions[] = Object.entries(SHORTCUT_LABEL_DATA).map((pair, idx) =>
    ({  value: idx, label: pair[1].label, icon: pair[1].icon }));

  const relModifierKey = environment.platform === 'darwin' ? 'cmd' : 'ctrl';
  return (
    <Select
      options={options}
      data-testid="basic-controls-shortcut-select"
      labelContent={{
        label: 'Set Keyboard Shortcut',
        help: (
          <UnorderedList header="Add keyboard shortcut command to control this axis. Key press behaviour is as follows:">
            <Text><Text e={Text.Emphasis.Bold}>key</Text>: move at 1/4 of target velocity</Text>
            <Text><Text e={Text.Emphasis.Bold}>shift + key</Text>: move at target velocity</Text>
            <Text><Text e={Text.Emphasis.Bold}>{relModifierKey} + key</Text>: perform relative move</Text>
          </UnorderedList>
        )
      }}
      title="Select Keyboard Shortcut"
      disabled={false}
      menuPlacement="auto"
      value={shortcut}
      onValueChange={option => { actions.setKeyboardShortcut(key, option) }}
      blurInputOnSelect={true}
    />);
};
