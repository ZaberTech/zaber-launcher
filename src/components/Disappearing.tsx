import React, { useEffect, useState } from 'react';

export const Disappearing: React.FC<{
  children: React.ReactNode;
  timeout: number;
  onDisappear?: () => void;
}> = ({ children, timeout, onDisappear }) => {
  const [visible, setVisible] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setVisible(false);
      onDisappear?.();
    }, timeout);

    return () => clearTimeout(timer);
  }, [timeout]);

  if (!visible) {
    return null;
  }
  return <>{children}</>;
};
