import { ipcRenderer, IpcRendererEvent } from 'electron';
import { injectable, inject } from 'inversify';
import type { Store } from 'redux';
import { filter, Observable, Subject } from 'rxjs';

import type { AnyAction } from '../utils';
import { reduxStoreSymbol } from '../store';

import { REDUX_BROADCAST_CHANNEL, IpcReceivedAction, REDUX_MAIN_PROCESS_CHANNEL } from './types';

@injectable()
export class IPC {
  private actions = new Subject<AnyAction>();

  constructor(@inject(reduxStoreSymbol) readonly store: Store) {
    ipcRenderer.on(REDUX_BROADCAST_CHANNEL, this.onBroadcastAction.bind(this));
  }

  broadcastAction(action: AnyAction): void {
    ipcRenderer.send(REDUX_BROADCAST_CHANNEL, action);
  }
  sendActionToMainProcess(action: AnyAction): void {
    ipcRenderer.send(REDUX_MAIN_PROCESS_CHANNEL, action);
  }

  onBroadcastAction(event: IpcRendererEvent, action: AnyAction): void {
    const ipcAction: IpcReceivedAction = { ...action, __ipcReceived: true };
    this.store.dispatch(ipcAction);
    this.actions.next(action);
  }

  getActions<TAction extends AnyAction>(actionType: string): Observable<TAction> {
    return this.actions.pipe(filter(a => a.type === actionType)) as Observable<TAction>;
  }
}
