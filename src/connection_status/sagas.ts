import { all, fork, take, put, call, race } from 'redux-saga/effects';
import type { SagaIterator } from 'redux-saga';
import { notNil } from '@zaber/toolbox';

import { getContainer } from '../container';
import { rxToEventChannel, Action } from '../utils';
import type { MessageRouterConnection } from '../message_router';
import { MessageRoutersService } from '../message_router/service';
import { environment } from '../environment';
import { EntityKey, makeConnectionKey, makeRouterKey } from '../keys';
import type { RoutedConnection } from '../app_components';

import { actions, ActionTypes, ActionsToPayloads } from './actions';

export function* connectionStatusSaga(): SagaIterator {
  yield all([
    !environment.isTest ? fork(monitorRouters) : null,
  ].filter(notNil));
}

export function* monitorRouters(): SagaIterator {
  const service = getContainer().get(MessageRoutersService);
  const newConnections = rxToEventChannel(service.newConnections);

  for (;;) {
    const connection: MessageRouterConnection = yield take(newConnections);
    yield fork(watchRouterConnection, connection);
  }
}

function* watchRouterConnection(connection: MessageRouterConnection): SagaIterator {
  const key = makeRouterKey(connection.url);
  yield put(actions.routerConnected(key));

  const newRoutedConnection = rxToEventChannel(connection.newRoutedConnection);
  const disconnectedPromise = connection.ended.toPromise();

  for (;;) {
    const { disconnected, newConnection }: { disconnected?: true; newConnection?: RoutedConnection } = yield race({
      newConnection: take(newRoutedConnection),
      disconnected: call(() => disconnectedPromise),
    });

    if (disconnected) {
      break;
    }

    yield fork(watchRoutedConnection, newConnection!, key);
  }

  yield put(actions.routerDisconnected(key));
}

function* watchRoutedConnection(connection: RoutedConnection, routerKey: EntityKey): SagaIterator {
  const key = makeConnectionKey(routerKey, connection.id);

  try {
    yield call([connection, connection.ensureOpen]);
  } catch {
    // we don't care about the error of opening
    return;
  }

  yield put(actions.routedConnectionConnected(key));

  const { forceDisconnect }: { forceDisconnect: boolean } = yield race({
    hasDisconnected: call(() => connection.ended.toPromise()),
    forceDisconnect: take((action: Partial<Action<ActionsToPayloads[ActionTypes.ROUTED_CONNECTION_DISCONNECT]>>) =>
      action.type === ActionTypes.ROUTED_CONNECTION_DISCONNECT && action.payload?.connectionKey === key
    ),
  });

  if (forceDisconnect) {
    yield call([connection, connection.close]);
  }

  yield put(actions.routedConnectionDisconnected(key));
}
