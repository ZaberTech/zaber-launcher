import { hasSetting } from '../../devices';
import type { SectionDataDefinition, SectionValues } from '../types';

import { encoderConversionRatioEditable, encoderIsAnalog, encoderIsOn } from './encoder';

function conversionRatioEditable2(values: SectionValues<'encoder2'>): boolean {
  if (values.mode == null) { return false }
  return encoderConversionRatioEditable(values.mode.is, values);
}

export const encoder2: SectionDataDefinition<'encoder2'> = {
  initializers: {
    motorType: { type: 'custom', backing: ascii => ascii.settings.get('motor.type') },
    mode: { type: 'setting', backing: 'encoder.2.mode' },
    type: { type: 'setting', backing: 'encoder.2.type' },
    dir: { type: 'setting', backing: 'encoder.2.dir' },
    fault: { type: 'setting', backing: 'encoder.2.fault.type' },
    indexMode: { type: 'setting', backing: 'encoder.2.index.mode' },
    filter: { type: 'setting', backing: 'encoder.2.filter' },
    mult: { type: 'setting', backing: 'encoder.2.ratio.mult' },
    div: { type: 'setting', backing: 'encoder.2.ratio.div' },
    interpolation: { type: 'setting', backing: 'encoder.2.interpolation' },
    minSignal: { type: 'setting', backing: 'encoder.2.signal.min' },
    cosDC: { type: 'variant', backing: info => info.axis ? 'encoder.2.cos.dc' : 'encoder.2.cos.dc.tune' },
    cosGain: { type: 'variant', backing: info => info.axis ? 'encoder.2.cos.gain' : 'encoder.2.cos.gain.tune' },
    sinDC: { type: 'variant', backing: info => info.axis ? 'encoder.2.sin.dc' : 'encoder.2.sin.dc.tune' },
    sinGain: { type: 'variant', backing: info => info.axis ? 'encoder.2.sin.gain' : 'encoder.2.sin.gain.tune' },
    powerUpDelay: { type: 'setting', backing: 'encoder.2.power.up.delay' },
  },
  sectionPresent: async ({ info }) => hasSetting('encoder.2.mode', info.axis ?? info.device),
  fieldEnabled: {
    type: encoderIsOn,
    dir: encoderIsOn,
    fault: encoderIsOn,
    indexMode: encoderIsOn,
    filter: encoderIsOn,
    mult: conversionRatioEditable2,
    div: conversionRatioEditable2,
    interpolation: v => encoderIsOn(v) && encoderIsAnalog(v),
    minSignal: v => encoderIsOn(v) && encoderIsAnalog(v),
    cosDC: v => encoderIsOn(v) && encoderIsAnalog(v),
    cosGain: v => encoderIsOn(v) && encoderIsAnalog(v),
    sinDC: v => encoderIsOn(v) && encoderIsAnalog(v),
    sinGain: v => encoderIsOn(v) && encoderIsAnalog(v),
  },
};
