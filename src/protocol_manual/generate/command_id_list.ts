import { parsedManual } from './profile';

const COMMAND_ID_PREFIX = 'topic_command_';

export function generateCommandIdList() : string[] {
  const body = parsedManual();
  return Array.from(body.querySelectorAll(`[id*="${COMMAND_ID_PREFIX}"]`))
    .filter(node =>
      node.classList.contains('reference') && !node.hasAttribute('data-disclosure')
    ).map(node =>
      node.id.substring(COMMAND_ID_PREFIX.length));
}
