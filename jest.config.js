module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  maxWorkers: '50%',
  transform: {
    '^.+\\.(ts|tsx)?$': ['ts-jest', { tsconfig: 'tsconfig.jest.json', isolatedModules: true }],
    '^.+\\.(mjs|js|jsx)$': 'babel-jest',
  },
  transformIgnorePatterns: [
    // If a dependency is causing tests to fail because it's a module (uses import statements)
    // add it here so babel transforms it into a format jest can use
    'node_modules/(?!(react-colorful|react-horizontal-scrolling-menu)/)'
  ],
  moduleNameMapper: {
    '\\.(png|jpg|ttf|woff|woff2|yml|mp3|gif)$': 'jest-transform-stub',
    '\\.svg\\?url$': '<rootDir>/scripts/mocks/svg_url.js',
    '\\.svg\\?raw$': '<rootDir>/scripts/mocks/svg_raw.js',
    '\\.svg$': '<rootDir>/scripts/mocks/svg.js',
    'ascii-7\\.html$': '<rootDir>/scripts/mocks/doc_mock.js',
    '\\.html$': '<rootDir>/scripts/mocks/html.js',
    '@electron/remote': '<rootDir>/src/test/mocks/electron_remote.ts',
    'monaco-editor': '<rootDir>/scripts/mocks/monaco.js',
    '^csv-stringify/sync': '<rootDir>/node_modules/csv-stringify/dist/cjs/sync.cjs',
    '.+\\.(template)$': '<rootDir>/src/test/mocks/txt_file.js',
    'react-syntax-highlighter': '<rootDir>/src/test/mocks/react-syntax-highlighter.js',
    'react-syntax-highlighter/dist/.*': '<rootDir>/src/test/mocks/react-syntax-highlighter.js',
  },
  setupFilesAfterEnv: ['<rootDir>/src/test/setup.ts'],
  moduleDirectories: ['node_modules'],
  testMatch: ['<rootDir>/src/**/*.test.(ts|tsx)'],
};
