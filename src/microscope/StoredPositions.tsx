import React from 'react';
import { useSelector } from 'react-redux';
import { ContextMenu, EditableField, Icons } from '@zaber/react-library';
import _ from 'lodash';

import { getUnitLabel, type MeasurementOK } from '../units';
import { useActions } from '../utils';

import {
  selectMonitorState, selectPositionWithoutOffset, selectStoredPositions, selectStoredPositionsToDisplay, StoredPositionDisplayed,
} from './selectors';
import { AXES, Axes, AXES_XYZ, Operations } from './types';
import { actions as actionsDefinition } from './actions';
import { OperationError } from './components';
import { getPrecision } from './utils';

function displayNumber(value: MeasurementOK): string {
  const precision = getPrecision(value);
  return `${_.round(value.value, precision).toString()} ${getUnitLabel(value.units)}`;
}

const Position: React.FC<{
  position: StoredPositionDisplayed;
}> = ({ position }) => {
  const actions = useActions(actionsDefinition);
  const numbersFullStr = AXES_XYZ.map(axis => position[axis].value).join(', ');
  const positionStr = [
    `[X: ${displayNumber(position.xAxis)},`,
    `Y: ${displayNumber(position.yAxis)},`,
    `F: ${displayNumber(position.focusAxis)}]`,
  ].join(' ');
  const positionXYStr = [
    `[X: ${displayNumber(position.xAxis)},`,
    `Y: ${displayNumber(position.yAxis)}]`,
  ].join(' ');

  const goto = (axes?: Axes[]) =>
    actions.operationStart(Operations.moveToStored, { stored: position, axes: axes ?? AXES.slice() });

  const copyToClipBoard = () => {
    navigator.clipboard.writeText(numbersFullStr).catch(() => null);
  };

  return <div className="position">
    <Icons.RightNormal title={`Go to ${positionStr}`} onClick={() => goto()}/>
    <EditableField hideButtons
      value={position.label}
      onValueChange={label => actions.updateStoredPosition(position.index, { label })}/>
    <ContextMenu>
      <ContextMenu.Item onClick={() => goto()} icon={<Icons.RightNormal/>}>
        Go to {positionStr}
      </ContextMenu.Item>
      <ContextMenu.Item onClick={() => goto(['xAxis', 'yAxis'])} icon={<Icons.RightNormal/>}>
        Go to {positionXYStr}
      </ContextMenu.Item>
      <ContextMenu.Item onClick={copyToClipBoard} icon={<Icons.Copy/>}>
        Copy to Clipboard
      </ContextMenu.Item>
      <ContextMenu.Item onClick={() => actions.deleteStoredPosition(position.index)} icon={<Icons.Trash/>}>
        Delete
      </ContextMenu.Item>
    </ContextMenu>
  </div>;
};

export const StoredPositions: React.FC = () => {
  const { stableInfo, info } = useSelector(selectMonitorState);
  const positions = useSelector(selectStoredPositionsToDisplay);

  if (stableInfo == null || info == null || positions.length === 0) {
    return null;
  }

  return <div className="stored-positions">
    <div className="positions">
      {positions.map(position =>
        <Position key={position.index} position={position}/>
      )}
    </div>
    <OperationError operation={Operations.moveToStored}/>
  </div>;
};


export const StorePositionButton: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const positions = useSelector(selectStoredPositions);

  const currentPosition = useSelector(selectPositionWithoutOffset);
  const { xAxis, yAxis, focusAxis } = currentPosition ?? {};
  if (xAxis == null || yAxis == null || focusAxis == null) {
    return null;
  }

  function getNextPositionLabel() {
    const maxPosition = _.max(positions.map(pos => Number(pos.label.match(/^Position (\d+)/)?.[1]))) ?? 0;
    const nextPosition = positions.length;
    return `Position ${_.padStart(`${Math.max(maxPosition, nextPosition) + 1}`, 2, '0')}`;
  }

  return <Icons.Save title="Save Position"
    onClick={() => actions.addStoredPosition({
      xAxis,
      yAxis,
      focusAxis,
      label: getNextPositionLabel(),
    })}/>;
};
