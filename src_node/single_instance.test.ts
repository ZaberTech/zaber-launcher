import { EventEmitter } from 'events';

let appMock: AppMock;
class AppMock extends EventEmitter {
  constructor() {
    super();
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    appMock = this;
  }

  requestSingleInstanceLock = jest.fn().mockReturnValue(true);
  quit = jest.fn();

  mockClear() {
    this.requestSingleInstanceLock.mockClear();
    this.quit.mockClear();
    this.removeAllListeners();
  }
}

class BrowserWindowMock {
  static only: BrowserWindowMock | null = null;

  isMinimized() {
    return true;
  }
  restore = jest.fn();
  focus = jest.fn();

  static getAllWindows() {
    if (!BrowserWindowMock.only) {
      BrowserWindowMock.only = new BrowserWindowMock();
    }
    return [BrowserWindowMock.only];
  }
}

jest.mock('electron', () => ({
  app: new AppMock(),
  BrowserWindow: BrowserWindowMock,
}));

import { createContainer, destroyContainer } from './container';
import { SingleInstance } from './single_instance';

let singleInstance: SingleInstance;

beforeEach(() => {
  const container = createContainer();
  singleInstance = container.get(SingleInstance);
});

afterEach(() => {
  destroyContainer();
  singleInstance = null!;

  appMock.mockClear();
  BrowserWindowMock.only = null;
});

test('acquires the lock', () => {
  expect(singleInstance.checkIsSingle()).toBe(true);
  expect(appMock.requestSingleInstanceLock).toHaveBeenCalled();
});

test('quits the app if the lock cannot be acquired', () => {
  appMock.requestSingleInstanceLock.mockReturnValueOnce(false);
  expect(singleInstance.checkIsSingle()).toBe(false);
  expect(appMock.quit).toHaveBeenCalled();
});

test('focuses and restores the first window when second instance is launched', () => {
  singleInstance.checkIsSingle();

  appMock.emit('second-instance');

  const window = BrowserWindowMock.only!;
  expect(window.restore).toHaveBeenCalled();
  expect(window.focus).toHaveBeenCalled();
});
