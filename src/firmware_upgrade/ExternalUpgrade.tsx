import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';
import { tryAccess } from '@zaber/toolbox';

import type { EntityKey } from '../keys';
import { Version } from '../app_components';

import { actions as actionsDefinition } from './actions';
import { UpgradeProgressModal } from './UpgradeProgressModal';
import { selectDeviceInfoWithAxes } from './selectors';
import { selectDevicesWithVersions } from './selectors_external';

type InstallCallback = (version: Version) => void;

interface Props {
  deviceKey: EntityKey;

  title?: string;
  offerSaveState?: boolean;

  content: (versions: Version[], install: InstallCallback) => React.ReactNode;
  noContent?: () => React.ReactNode;
}

export const ExternalUpgrade: React.FC<Props> = ({ deviceKey, content, noContent, title, offerSaveState }) => {
  const actions = useActions(actionsDefinition);
  const versions = tryAccess(useSelector(selectDevicesWithVersions), deviceKey);
  const currentDevice = useSelector(selectDeviceInfoWithAxes);
  const isActive = currentDevice?.key === deviceKey;

  return (<>
    {versions != null && content(versions.versions, version => {
      actions.setSelectedDevice(deviceKey);
      actions.downloadAndInstall(deviceKey, versions.identifier, version, offerSaveState);
    })}
    {versions == null && noContent?.()}

    {isActive && <UpgradeProgressModal customTitle={title}/>}
  </>);
};
