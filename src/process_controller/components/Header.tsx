import { AnimationClasses, iconFromSvg, Icons, Text } from '@zaber/react-library';
import classNames from 'classnames';
import React from 'react';
import { useSelector } from 'react-redux';
import { match } from 'ts-pattern';

import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import CoilSingleError from '../coils/single/error.svg';
import CoilSingleOff from '../coils/single/off.svg';
import CoilSingleOn from '../coils/single/on.svg';
import CoilBridgedError from '../coils/bridged/error.svg';
import CoilBridgedOff from '../coils/bridged/off.svg';
import CoilBridgedOn from '../coils/bridged/on.svg';
import { selectChosenProcess, selectControllerLabel } from '../selectors';
import type { ControllerInfo } from '../types';


const Coil = {
  Single: {
    Error: iconFromSvg(CoilSingleError),
    Off: iconFromSvg(CoilSingleOff),
    On: iconFromSvg(CoilSingleOn),
  },
  Bridged: {
    Error: iconFromSvg(CoilBridgedError),
    Off: iconFromSvg(CoilBridgedOff),
    On: iconFromSvg(CoilBridgedOn),
  },
};

type HeaderProcessCoilProps = {
  state: ControllerInfo['states'][number];
  fault: ControllerInfo['faults'][number];
};

const SingleCoil: React.FC<HeaderProcessCoilProps> = ({ state, fault }) => <div className="single coil">
  {
    fault != null ? <Coil.Single.Error/> :
    state.on ? <Coil.Single.On/> :
    <Coil.Single.Off/>
  }
</div>;

const BridgedCoil: React.FC<HeaderProcessCoilProps> = ({ state, fault }) => <div className="bridged coil">
  {
    fault != null ? <Coil.Bridged.Error/> :
    state.on ? <Coil.Bridged.On/> :
    <Coil.Bridged.Off/>
  }
</div>;

/** Shows the state of a pair of terminals that may or may not be bridged together */
const CoilGroup: React.FC<{process: 1 | 3 } & Pick<ControllerInfo, 'bridges' | 'states' | 'faults'>> = props => {
  const { process, bridges, states, faults } = props;
  return <div className="group">
    {bridges[process].on ? (
      <BridgedCoil state={states[process]} fault={faults[process]}/>
    ) : <>
      <SingleCoil state={states[process]} fault={faults[process]}/>
      <SingleCoil state={states[process + 1]} fault={faults[process + 1]}/>
    </>}
  </div>;
};

const SelectGroup: React.FC<{ process: 1 | 3; bridges: ControllerInfo['bridges'] }> = ({ process, bridges }) => {
  const selectedProcess = useSelector(selectChosenProcess);

  return <div className="group">
    {bridges[process].on ? <>
      <div className="bridged-space"/>
      <div className={classNames('select', { selected: selectedProcess === process })}></div>
      <div className="bridged-space"/>
    </> : <>
      <div className="single-outer-space"/>
      <div className={classNames('select', { selected: selectedProcess === process })}></div>
      <div className="single-outer-space"/>
      <div className="single-outer-space"/>
      <div className={classNames('select', { selected: selectedProcess === (process + 1) })}></div>
      <div className="single-outer-space"/>
    </>}
  </div>;
};

const Bridger: React.FC<{ process: 1 | 3; bridges: ControllerInfo['bridges'] }> = ({ process, bridges }) => {
  const actions = useActions(actionDefinitions);

  return <div className="bridge-spacer">
    <div className="pad"></div>
    {match(bridges[process])
      .with({ reading: true }, () => <Icons.Refresh className={AnimationClasses.Rotation}/>)
      .with({ on: true }, () => (<Icons.Disconnect
        title={`Unbridge process ${process} and ${process + 1}`} onClick={() => actions.bridge(process, false)}
      />))
      .with({ on: false }, () => <Icons.Link
        title={`Bridge process ${process} and ${process + 1}`} onClick={() => actions.bridge(process, true)}
      />)
      .exhaustive()
    }
    <div className="pad"></div>
  </div>;
};

const TerminalGroup: React.FC<{process: 1 | 3; bridges: ControllerInfo['bridges'] }> = ({ process, bridges }) => <div className="group">
  {bridges[process].on ? <>
    <div className="bridged-space"/>
    <Text t={Text.Type.Helper} e={Text.Emphasis.Bold}>CH{process}+</Text>
    <div className="flex-space"/>
    <Bridger process={process} bridges={bridges}/>
    <div className="flex-space"/>
    <Text t={Text.Type.Helper} e={Text.Emphasis.Bold}>CH{process + 1}+</Text>
    <div className="bridged-space"/>
  </> : <>
    <div className="single-outer-space"/>
    <Text t={Text.Type.Helper} e={Text.Emphasis.Bold}>CH{process}+</Text>
    <div className="flex-space"/>
    <Text t={Text.Type.Helper} e={Text.Emphasis.Bold}>CH{process}-</Text>
    <div className="single-inner-space"/>
    <Bridger process={process} bridges={bridges}/>
    <div className="single-inner-space"/>
    <Text t={Text.Type.Helper} e={Text.Emphasis.Bold}>CH{process + 1}+</Text>
    <div className="flex-space"/>
    <Text t={Text.Type.Helper} e={Text.Emphasis.Bold}>CH{process + 1}-</Text>
    <div className="single-outer-space"/>
  </>}
</div>;

export const Header: React.FC<ControllerInfo> = ({ states, faults, bridges }) => {
  const label = useSelector(selectControllerLabel);
  return (
    <div className="process-controller-header">
      <Text t={Text.Type.H5}>{label}</Text>
      <div className="schematic">
        <div className="line coils">
          <CoilGroup process={1} {...{ states, faults, bridges }}/>
          <CoilGroup process={3} {...{ states, faults, bridges }}/>
        </div>
        <div className="line selects">
          <SelectGroup process={1} bridges={bridges}/>
          <SelectGroup process={3} bridges={bridges}/>
        </div>
        <div className="line terminals">
          <TerminalGroup process={1} bridges={bridges}/>
          <TerminalGroup process={3} bridges={bridges}/>
        </div>
      </div>
    </div>);
};
