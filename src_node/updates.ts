import path from 'path';
import fs from 'fs/promises';
import os from 'os';

import { take, timeout } from 'rxjs/operators';
import * as updater from 'electron-updater';
import { BrowserWindow, app } from 'electron';
import { inject, injectable } from 'inversify';
import { Log, Logger } from '@zaber/app-components/lib/log';
import { duration } from 'moment';
import { throwIfJsError } from '@zaber/toolbox';

import { handleNonCriticalError, handleUnexpectedError } from '../src/errors/errors';
import { Editions, Flavors, environment, processArguments } from '../src/environment';
import { actions, ActionTypes, ActionsToPayloads } from '../src/updates/actions';
import type { Action } from '../src/utils';

import { Ipc } from './ipc';

const UPDATE_CHECK_INTERVAL = {
  enabled: duration(12, 'h').asMilliseconds(),
  disabled: duration(3, 'd').asMilliseconds(),
};
export const STARTUP_CHECK_DELAY = duration(10, 's').asMilliseconds();
export const RENDER_PROCESS_DISPLAY_NOTIFICATION_TIMEOUT = duration(10, 's').asMilliseconds();
export const INSTALL_BEGIN_TIMEOUT = duration(10, 's').asMilliseconds();

@injectable()
export class Updates {
  private readonly logger: Logger;
  private checkingVersion = false;
  private readonly pendingInstallFile = path.join(app.getPath('userData'), 'pending-update.txt');
  private latestVersion: string | null = null;
  private pendingInstallVersion: string | null = null;
  private updater: typeof updater.autoUpdater;
  private updatesDisabled = false;

  constructor(
    @inject(Ipc) private readonly ipc: Ipc,
    @inject(Log) log: Log,
  ) {
    this.logger = log.getLogger('Updates');

    if (environment.edition !== Editions.Public && !environment.isTest) {
      this.updater = Updates.createInternalUpdater();
      if (environment.edition === Editions.Dev) {
        this.updater.logger = {
          info: this.logger.info,
          warn: this.logger.warn,
          error: this.logger.error,
          debug: this.logger.info,
        };
      }
    } else {
      this.updater = updater.autoUpdater; // creates default instance
    }
    this.updater.autoInstallOnAppQuit = false;
    this.updater.addListener('error', err => this.logger.warn(err));

    this.registerIpcActions();
  }

  private static createInternalUpdater() {
    const publishPath: string[] = [];
    if (environment.flavor !== Flavors.Zaber) {
      publishPath.push(environment.flavor);
    }
    publishPath.push(environment.edition);
    if (environment.offline) {
      publishPath.push('offline');
    }
    const url = [
      `http://${process.env.ZABER_LAUNCHER_UPDATE_HOST ?? 'zaber-launcher.izaber.com'}`,
      environment.edition,
      publishPath.join('-'),
    ].join('/');

    const options = {
      provider: 'generic' as const,
      useMultipleRangeRequest: false,
      url,
    };
    switch (os.platform()) {
      case 'win32':
        return new updater.NsisUpdater(options);
      case 'darwin':
        return new updater.MacUpdater(options);
      default:
        return new updater.AppImageUpdater(options);
    }
  }

  private async checkDisabledUpdates() {
    let disable = processArguments.disableUpdates;
    if (!disable) {
      const installDir = path.dirname(app.getPath('exe'));
      try {
        await fs.readFile(path.join(installDir, 'disable-updates.txt'), 'utf-8');
        disable = true;
      } catch {
        // file does not exist / not accessible
      }
    }

    if (!disable) { return }

    this.updatesDisabled = true;
    this.logger.info('Updates are disabled');

    this.updater.autoDownload = false;
  }

  private registerIpcActions() {
    this.ipc.getActions(ActionTypes.QUIT_AND_INSTALL).subscribe(() => {
      this.updater.quitAndInstall();
    });
    this.ipc.getActions<Action<ActionsToPayloads[ActionTypes.IGNORE_UPDATE]>>(ActionTypes.IGNORE_UPDATE)
      .subscribe(({ action: { payload: { installOnStart } } }) => {
        if (installOnStart) {
          this.scheduleInstall().catch(handleNonCriticalError);
        }
      });
    this.ipc.getActions(ActionTypes.CHECK_NEW_VERSION).subscribe(() => {
      this.checkForUpdate().catch(handleUnexpectedError);
    });
  }

  async init() {
    await this.checkPendingInstall();
    await this.checkDisabledUpdates();
  }

  static getNotificationWindow() {
    return BrowserWindow.getFocusedWindow() ?? BrowserWindow.getAllWindows().at(0);
  }

  private async performCheck(): Promise<void> {
    this.logger.info('Checking for update...');

    const update = await this.updater.checkForUpdates();
    if (update == null || update.updateInfo.version === app.getVersion()) {
      this.logger.info('No update');
      return;
    }

    this.latestVersion = update.updateInfo.version;
    this.logger.info('New version available', this.latestVersion);

    if (this.updatesDisabled) {
      const toNotify = Updates.getNotificationWindow();
      if (toNotify) {
        this.ipc.sendAction(toNotify.webContents, actions.newVersionReady(this.latestVersion, false));
      }
      return;
    }

    if (update.downloadPromise) {
      await update.downloadPromise;
      this.logger.info('Downloaded update', this.latestVersion);
    }

    if (this.pendingInstallVersion) {
      if (this.pendingInstallVersion === this.latestVersion) {
        this.updater.quitAndInstall();
        return;
      } else {
        this.logger.info(`Pending install version does not match: ${this.pendingInstallVersion} !== ${this.latestVersion}`);
        this.pendingInstallVersion = null;
      }
    }

    const toNotify = Updates.getNotificationWindow();
    if (!toNotify) {
      return;
    }

    const ensureNotificationDisplayed = this.ipc.getActions(ActionTypes.NEW_VERSION_NOTIFICATION_DISPLAYED).pipe(
      timeout(RENDER_PROCESS_DISPLAY_NOTIFICATION_TIMEOUT),
      take(1),
    ).toPromise();

    this.ipc.sendAction(toNotify.webContents, actions.newVersionReady(this.latestVersion));

    try {
      await ensureNotificationDisplayed;
    } catch (err) {
      throwIfJsError(err);

      this.logger.warn('No reply from the window, applying the update...');
      this.updater.quitAndInstall();
    }
  }

  private async checkForUpdate(): Promise<void> {
    if (this.checkingVersion) { return }
    this.checkingVersion = true;
    try {
      await this.performCheck();
      this.ipc.broadcastAction(actions.versionCheckDone());
    } catch (err) {
      const message = err instanceof Error ? String(err.message) : String(err);
      this.ipc.broadcastAction(actions.versionCheckDone(message));
      this.logger.warn('Cannot check for update', err);
    } finally {
      this.checkingVersion = false;
    }
  }

  private async scheduleInstall(): Promise<void> {
    if (!this.latestVersion) { return }

    this.logger.info('Scheduling update to', this.latestVersion);
    await fs.writeFile(this.pendingInstallFile, this.latestVersion, 'utf-8');
  }

  private async checkPendingInstall(): Promise<void> {
    try {
      this.pendingInstallVersion = await fs.readFile(this.pendingInstallFile, 'utf-8');
      await fs.unlink(this.pendingInstallFile);
    } catch {
      // file does not exist / not accessible
      return;
    }

    this.checkForUpdate().catch(handleUnexpectedError);
    // if the update install does not start fast enough, don't block the app
    await new Promise<void>(r => setTimeout(r, INSTALL_BEGIN_TIMEOUT));
  }

  public scheduleUpdateChecks(): void {
    if (!environment.isProduction || environment.offline) {
      return;
    }

    const doCheck = () => { this.checkForUpdate().catch(handleUnexpectedError) };
    const checkInterval = this.updatesDisabled ? UPDATE_CHECK_INTERVAL.disabled : UPDATE_CHECK_INTERVAL.enabled;
    setInterval(doCheck, checkInterval);
    setTimeout(doCheck, STARTUP_CHECK_DELAY);
  }
}
