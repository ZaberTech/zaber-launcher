import React from 'react';
import { SimpleSelect, Text } from '@zaber/react-library';

import { useActions } from '../../utils';
import { actions as actionDefinitions } from '../actions';

import { XAXIS, YAXIS } from './constants';


export interface Props {
  axisOptions: SimpleSelect.Option<number>[];
  drawingAxes: number[];
}


export const DrawingAxisSelector: React.FC<Props> = ({ axisOptions, drawingAxes }) => {
  const actions = useActions(actionDefinitions);
  if (axisOptions.length < 2 || drawingAxes.length < 2) {
    return null;
  }

  return <div className="axis-selection">
    {axisOptions.length > 1 && <>
      <Text>X Axis</Text>
      <SimpleSelect
        className="axis-selector"
        options={axisOptions}
        value={axisOptions.find(o => o.value === drawingAxes[XAXIS])?.value}
        onValueChange={num => actions.setDrawingAxis(XAXIS, num)}/>
      <Text>Y Axis</Text>
      <SimpleSelect
        className="axis-selector"
        options={axisOptions}
        value={axisOptions.find(o => o.value === drawingAxes[YAXIS])?.value}
        onValueChange={num => actions.setDrawingAxis(YAXIS, num)}/>
    </>}</div>;
};

