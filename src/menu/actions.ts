import { actionBuilder } from '../utils';

export enum ActionTypes {
  PIN = 'MENU_PIN',
  MOUSE_OVER = 'MENU_MOUSE_OVER',
}

export interface ActionsToPayloads {
  [ActionTypes.PIN]: { pin: boolean; saveToStorage: boolean };
  [ActionTypes.MOUSE_OVER]: { over: boolean };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  pinMenu: (pin: boolean, saveToStorage: boolean = true) => buildAction(ActionTypes.PIN, { pin, saveToStorage }),
  mouseOver: (over: boolean) => buildAction(ActionTypes.MOUSE_OVER, { over }),
};
