import React, { useLayoutEffect } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import type { Store } from 'redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import { BrowserWindow } from '@electron/remote';

import { environment, Flavors } from './environment';
import { ErrorHandler } from './errors';
import { browserHistory } from './store/history';
import { ConnectionManager } from './connection_manager';
import { BasicControls } from './basic_controls/BasicControls';
import { BasicMovement } from './basic_movement';
import { Apps } from './apps';
import { DeviceSettings } from './device_settings';
import { Menu } from './menu';
import { Cloud } from './cloud';
import { Terminal } from './terminal';
import { UpdateNotification } from './updates';
import { LocalShare } from './local_share';
import { CurrentTuner } from './current_tuner';
import { FirmwareUpgradeApp } from './firmware_upgrade';
import { Preferences } from './preferences';
import { Title } from './components';
import { Welcome } from './help';
import { About, Contact } from './about';
import { FeedbackModal } from './feedback';
import { HardwareModificationApp } from './hardware_modification';
import { LC40Setup } from './lc40_setup';
import { GCode } from './gcode';
import { LockstepConfiguration } from './lockstep_configuration';
import { Demo } from './demo';
import { ServoTuningApp } from './servo_tuning';
import { DeviceIoApp } from './device_io';
import { JoystickApp } from './joystick';
import { Oscilloscope } from './oscilloscope';
import { InternalRouter } from './internal';
import { SaveLoadModal } from './save_load';
import { PvtApp } from './pvt';
import { MAIN_FLEX_CLASS, MAIN_FLEX_CONTENT_CLASS } from './styles/constants';
import { ProcessControllerApp } from './process_controller';
import { Calibration } from './calibration';
import { MicroscopeApp } from './microscope';
import { FullscreenNotice } from './help/FullscreenNotice';
import { TriggersApp } from './triggers';


export const App: React.FunctionComponent<{ store: Store }> = ({ store }) => {
  useLayoutEffect(() => {
    Title.setBaseTitle();
  });

  return (<ErrorHandler>
    <Provider store={store}>
      <ConnectedRouter history={browserHistory}>
        <UpdateNotification/>
        {environment.flavor === Flavors.Zaber && <Welcome/>}
        <FeedbackModal/>
        <SaveLoadModal/>
        <FullscreenNotice window={BrowserWindow.getFocusedWindow()}/>
        <div className={MAIN_FLEX_CLASS}>
          <Menu/>

          <div className={MAIN_FLEX_CONTENT_CLASS}>
            <Switch>
              <Route path="/connection-manager" component={ConnectionManager}/>
              <Route path="/apps" component={Apps}/>
              <Route path="/cloud" component={Cloud}/>
              <Route path="/local-share" component={LocalShare}/>
              <Route path="/basic-controls" component={BasicControls}/>
              <Route path="/basic-movement" component={BasicMovement}/>
              <Route path="/device-settings" component={DeviceSettings}/>
              <Route path="/terminal" component={Terminal}/>
              <Route path="/firmware-upgrade" component={FirmwareUpgradeApp}/>
              <Route path="/hardware-modification" component={HardwareModificationApp}/>
              <Route path="/servo-tuner" component={ServoTuningApp}/>
              <Route path="/process-controller" component={ProcessControllerApp}/>
              <Route path="/current-tuner" component={CurrentTuner}/>
              <Route path="/preferences" component={Preferences}/>
              <Route path="/about" component={About}/>
              <Route path="/contact" component={Contact}/>
              <Route path="/lc40-setup" component={LC40Setup}/>
              <Route path="/shb40-setup" component={LC40Setup}/>
              <Route path="/gcode" component={GCode}/>
              <Route path="/lockstep-configuration" component={LockstepConfiguration}></Route>
              <Route path="/demo" component={Demo}/>
              <Route path="/device-io" component={DeviceIoApp}/>
              <Route path="/joystick" component={JoystickApp}/>
              <Route path="/oscilloscope" component={Oscilloscope}/>
              <Route path="/internal" component={InternalRouter}/>
              <Route path="/pvt" component={PvtApp}/>
              <Route path="/calibration" component={Calibration}/>
              <Route path="/microscope" component={MicroscopeApp}/>
              <Route path="/triggers" component={TriggersApp}/>
              <Route><Redirect to="/connection-manager"/></Route>
            </Switch>
          </div>
        </div>
      </ConnectedRouter>
    </Provider>
  </ErrorHandler>);
};
