import React from 'react';
import { Text } from '@zaber/react-library';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../../container';
import { wrapWithNewStore } from '../../test';

import { TuningControl } from './TuningControl';

let wrapper: RenderResult;

const TestTuningControl = wrapWithNewStore(TuningControl);
let sliderValue = 2;
const mockOnChange = jest.fn((value: number) => {
  sliderValue = value;
});

beforeEach(() => {
  createContainer();

  wrapper = render(<TestTuningControl name="test slider" sliderValue={sliderValue} disabled={false}
    onChange={mockOnChange} min={-1} max={10} step={1} manual={<Text>This is the manual</Text>}/>);
});

afterEach(() => {
  sliderValue = 2;
  jest.clearAllMocks();
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
});

describe('The slider in the control', () => {
  test('the slider shows the label and both maximum and minimum values', () => {
    expect(wrapper.getByText(/-1/));
    expect(wrapper.getByText(/10/));
    expect(wrapper.getByText(/test slider/));
  });

  test('the slider shows the slider value passed to it', () => {
    expect(wrapper.getByText(/2/));
    sliderValue = 5;
    wrapper.rerender(<TestTuningControl name="test slider" sliderValue={sliderValue} disabled={false}
      onChange={mockOnChange} min={-1} max={10} step={1} manual={<Text>This is the manual</Text>}/>);
    expect(wrapper.getByText(/5/));
  });

  test('the slider shows the correct value when it is moved, and calls the callback', () => {
    fireEvent.change(wrapper.getByTitle('test slider'), { target: { value: 7 } });
    expect(mockOnChange).toHaveBeenCalledWith(7);
    wrapper.rerender(<TestTuningControl name="test slider" sliderValue={sliderValue} disabled={false}
      onChange={mockOnChange} min={-1} max={10} step={1} manual={<Text>This is the manual</Text>}/>);
    expect(wrapper.getByText(/7/));
  });
});


test('the manual opens and closes', () => {
  fireEvent.click(wrapper.getByTitle('Show documentation'));
  expect(wrapper.getByText(/This is the manual/));
  fireEvent.click(wrapper.getByTitle('Hide documentation'));
  expect(wrapper.getByTitle(/Show documentation/));
});
