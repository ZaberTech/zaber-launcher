import { injectable, inject } from 'inversify';
import { asapScheduler, Subject } from 'rxjs';
import { observeOn } from 'rxjs/operators';

import { handleUnexpectedError } from '../errors';
import { RoutedConnection, Logger, Log } from '../app_components';
import { PreferencesService } from '../preferences/service';
import { PreferenceKeys } from '../preferences/types';

import { MessageRouterConnection } from './connection';

@injectable()
export class MessageRoutersService {
  private connections = new Map<string, MessageRouterConnection>();
  private log: Logger;
  private timeout: { local?: number; remote?: number } = {};

  private readonly newConnectionsSubject = new Subject<MessageRouterConnection>();
  public readonly newConnections = this.newConnectionsSubject.pipe(observeOn(asapScheduler));

  constructor(
    @inject(Log) logService: Log,
    @inject(PreferencesService) private readonly preferencesService: PreferencesService,
  ) {
    this.log = logService.getLogger('MessageRoutersService');

    this.preferencesService.registerHandler(
      PreferenceKeys.TIMEOUT_LOCAL,
      value => this.setTimeout('local', value)
    );
    this.preferencesService.registerHandler(
      PreferenceKeys.TIMEOUT_REMOTE,
      value => this.setTimeout('remote', value)
    );
  }

  private async setTimeout(type: 'local' | 'remote', value: number): Promise<void> {
    this.timeout[type] = value;
    for (const routerConnection of this.connections.values()) {
      await routerConnection.closeAll();
    }
  }

  public async getConnection(url: string): Promise<MessageRouterConnection> {
    let connection = this.connections.get(url);
    if (!connection) {
      connection = new MessageRouterConnection(url, this.timeout);

      this.connections.set(url, connection);
      connection.errors.subscribe(err => this.onError(connection!, err));
      this.newConnectionsSubject.next(connection);
    }

    await connection.ensureConnected();

    return connection;
  }

  public dropConnection(url: string): void {
    const connection = this.connections.get(url);
    if (connection) {
      this.removeConnection(connection);
    }
  }

  private onError(connection: MessageRouterConnection, err: Error): void {
    this.removeConnection(connection);
    this.log.info(`Removing '${connection.url}' due to error: ${err}`);
  }

  private removeConnection(connection: MessageRouterConnection): void {
    if (this.connections.get(connection.url) !== connection) {
      return;
    }
    this.connections.delete(connection.url);
    connection.close().catch(handleUnexpectedError);
  }

  public async getAsciiConnection(url: string, connectionId: string): Promise<RoutedConnection['connection']> {
    const routedConnection = await this.getRoutedConnection(url, connectionId);
    return routedConnection.connection;
  }

  public async getRoutedConnection(url: string, connectionId: string): Promise<RoutedConnection> {
    const routerConnection = await this.getConnection(url);
    const routedConnection = await routerConnection.getRoutedConnection(connectionId);
    return routedConnection;
  }
}
