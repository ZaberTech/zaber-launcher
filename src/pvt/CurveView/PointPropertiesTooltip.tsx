import type { Properties } from 'csstype';
import React from 'react';

import { getUnitLabel } from '../../units';
import { Path } from '../types';


export interface Props {
  userPath: Path;
  selectedPointIndex: number;
  drawingAxes: number[];
  style?: Properties<string | number, string>;
}


// Tooltip displayed when the mouse hovers over a control point in the curve view.
export const PointPropertiesToolTip: React.FC<Props> = ({ userPath, selectedPointIndex, drawingAxes, style }) => {
  if (!userPath || drawingAxes.length < 2) {
    return null;
  }

  return <div className="tooltip" style={style}>
    <table>
      <thead><tr>
        <th>Point {selectedPointIndex + 1}</th>
        {drawingAxes.slice(0, 2).map(axis => <th key={axis}>{userPath.series[axis].name}</th>)}
      </tr></thead>
      <tbody>
        <tr>
          <td>Position</td>
          {drawingAxes.slice(0, 2).map(axis => <td key={axis}>
            {userPath.series[axis].points[selectedPointIndex].pos.toLocaleString(undefined, { maximumFractionDigits: 3 })}&nbsp;
            {getUnitLabel(userPath.positionUnit)}
          </td>)}
        </tr>
        <tr>
          <td>Velocity</td>
          {drawingAxes.slice(0, 2).map(axis => <td key={axis}>
            {userPath.series[axis].points[selectedPointIndex].vel.toLocaleString(undefined, { maximumFractionDigits: 3 })}&nbsp;
            {getUnitLabel(userPath.velocityUnit)}
          </td>)}
        </tr>
      </tbody>
    </table>
  </div>;
};
