import React from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, NoticeBorder, Text } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import motorBaseRight from './assets/lc40b-right.png';
import motorBaseLeft from './assets/lc40b-left.png';
import motorCarriageRight from './assets/lc40c-right.png';
import motorCarriageLeft from './assets/lc40c-left.png';
import { actions as actionsDefinition } from './actions';
import { DEVICE_NAME, MotorOrientation, StageType } from './types';
import { selectCurrentAxisOrDevice, selectResultError } from './selectors';
import { ImageSelect } from './ImageSelect';
import { ErrorContext } from './exception';


export const MotorConfig: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const { config } = useSelector(selectCurrentAxisOrDevice)!;
  const resultError = useSelector(selectResultError);

  const deviceName = `${DEVICE_NAME}${config.stageType === StageType.CARRIAGE_MOUNT ? 'C' : 'B'}`;

  return <NoticeBorder type={resultError?.errContext === ErrorContext.DRIVER_ORIENTATION ? 'error' : null}>
    <HeaderCard className="section" header={<Text e={Text.Emphasis.Bold}>Motor Orientation</Text>}>
      <ImageSelect
        selected={config.motorOrientation ?? ''}
        options={[
          {
            name: MotorOrientation.LEFT,
            imageSource: config.stageType === StageType.CARRIAGE_MOUNT ? motorCarriageLeft : motorBaseLeft,
            altText: `Diagram of ${deviceName} stage with motor on left when viewed from the top.`,
            label: 'Left',
          },
          {
            name: MotorOrientation.RIGHT,
            imageSource: config.stageType === StageType.CARRIAGE_MOUNT ? motorCarriageRight : motorBaseRight,
            altText: `Diagram of ${deviceName} stage with motor on right when viewed from the top.`,
            label: 'Right',
          },
        ]}
        onSelect={name => actions.updateConfig({ motorOrientation: name as MotorOrientation })}/>
    </HeaderCard>
  </NoticeBorder>;
};
