import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import type { Container } from 'inversify';

import { IntersectionObserverMockBase } from '../test/mocks/intersection_observer';
import 'jest-canvas-mock';
import { MessageRoutersServiceMockMultiAxis, SelectMock, setDeviceCallback } from './testing/mocks';

jest.mock('react-select', () => SelectMock);

class IntersectionObserverMock extends IntersectionObserverMockBase {
}

(window as unknown as { IntersectionObserver: typeof IntersectionObserverMock }).IntersectionObserver = IntersectionObserverMock;

import _ from 'lodash';
import { ascii, Length, Velocity } from '@zaber/motion';

import { IdentifiedAxisInfo, selectDevices } from '../connection_manager';
import { createContainer, destroyContainer } from '../container';
import { mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import { MessageRoutersService } from '../message_router';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import { getSelectByTestId } from '../units/test_utils';

import { actions } from './actions';
import { BufferLoadDialog } from './BufferLoadDialog';
import { selectDevicePath, selectErrors, selectUserPath } from './selectors';
import type { Path } from './types';


let container: Container;
const TestDialog = wrapWithNewStore(wrapWithRouter(BufferLoadDialog));
let wrapper: RenderResult;
let bufferList: string[] = [];
const bufferContents: _.Dictionary<string[]> = {};


function createDevice() {
  mockSingleDeviceWithPeripherals(TestDialog.testStore, {
    peripheralCount: 2,
    axisModifier: (info: IdentifiedAxisInfo) => ({
      ...info,
      identity: {
        ...info.identity,
        peripheralName: `${info.identity.peripheralName} (axis ${info.axisNumber})`,
      },
    }),
  });

  const device = _.sample(selectDevices(TestDialog.testStore.getState()))!;
  TestDialog.testStore.dispatch(actions.selectDevice(device.key));
  TestDialog.testStore.dispatch(actions.commitDeviceSelection());
}


function showDialog() {
  // The dialog is already rendered above but this action triggers the saga
  // to load the list of buffers.
  TestDialog.testStore.dispatch(actions.setBufferLoadDialogVisible(true));
}


async function respondToGenericCommand(cmd: string): Promise<ascii.Response[]> {
  const ACK = { data: '0', status: 'OK', replyFlag: '--' };
  if (cmd.match(/pvt buffer list/)) {
    return [ACK,
      ...bufferList.map(i => ({
        messageType: ascii.MessageType.INFO,
        status: 'OK',
        replyFlag: '--',
        data: `buffer ${i}`,
      }))] as ascii.Response[];
  } else {
    const match = cmd.match(/pvt buffer (\d+) print/);
    if (match) {
      return [ACK,
        ...bufferContents[match[1]].map(data => ({
          messageType: ascii.MessageType.INFO,
          status: 'OK',
          replyFlag: '--',
          data,
        }))] as ascii.Response[];
    }
  }

  return [];
}


describe('Buffer dialog', () => {
  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockMultiAxis);
    wrapper = render(<TestDialog/>);
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  describe('with no buffers on the device', () => {
    beforeEach(() => {
      setDeviceCallback(device => {
        device.genericCommandMultiResponse = jest.fn(cmd => respondToGenericCommand(cmd));
      });

      bufferList = [];

      createDevice();
      showDialog();
    });


    it('shows an error', async () => {
      await waitUntilPass(() => expect(wrapper.queryByText(/no PVT buffers/)).not.toBeNull());
    });
  });


  describe('with buffers on the device', () => {
    beforeEach(() => {
      setDeviceCallback(device => {
        device.genericCommandMultiResponse = jest.fn(cmd => respondToGenericCommand(cmd));
        device._pvtBuffers = bufferContents;
      });

      bufferList = ['78', '45'];
      bufferContents['45'] = [
        'setup store 45 2',
        'point abs p 12000 13000 v 5000 6000 t 500.0',
        'point abs p 14000 15000 v 7000 8000 t 600.0',
        'setup disable'];
      bufferContents['78'] = [
        'setup store 78 2',
        'call 45',
        'point rel p 16000 17000 v 9000 10000 t 700.0',
        'setup disable'];

      createDevice();
      showDialog();
    });


    it('displays the device identity', async () => {
      await waitUntilPass(() => expect(wrapper.queryByText(/X-MCC2/)).not.toBeNull());
      expect(wrapper.queryByText(/SN: 1235/)).not.toBeNull();
      expect(wrapper.queryByText(/FW: 7./)).not.toBeNull();
    });


    it('chooses lowest buffer number as default', async () => {
      await waitUntilPass(() => expect(wrapper.queryByText(/45/)).not.toBeNull());
    });


    it('fills in defaults for axis selections', async () => {
      await waitUntilPass(() => expect(wrapper.queryByText(/Buffer 45 Axis 1/)).not.toBeNull());
      const axisSelect1 = getSelectByTestId(wrapper, 'buffer-axis-0');
      const axisSelect2 = getSelectByTestId(wrapper, 'buffer-axis-1');
      expect(axisSelect1.value).toBe('1');
      expect(axisSelect2.value).toBe('2');
    });


    it('defaults other axis selection on change', async () => {
      await waitUntilPass(() => expect(wrapper.queryByText(/Buffer 45 Axis 1/)).not.toBeNull());
      const axisSelect1 = getSelectByTestId(wrapper, 'buffer-axis-0');
      fireEvent.change(axisSelect1, { target: { value: '2' } });
      const axisSelect2 = getSelectByTestId(wrapper, 'buffer-axis-1');
      expect(axisSelect1.value).toBe('2');
      expect(axisSelect2.value).toBe('1');
    });


    it('imports the data', async () => {
      await waitUntilPass(() => expect(wrapper.queryByText(/Buffer 45 Axis 1/)).not.toBeNull());
      fireEvent.click(wrapper.getByTitle('Read'));
      let path: Path | null = null;
      await waitUntilPass(() => {
        path = selectUserPath(TestDialog.testStore.getState());
        expect(path).not.toBeNull();
      });

      path = path!;
      expect(path.positionUnit).toBe(Length.mm);
      expect(path.velocityUnit).toBe(Velocity.MILLIMETRES_PER_SECOND);
      expect(path.series.length).toBe(2);
      expect(path.series[0].name).toBe('Buffer 45 Axis 1');
      expect(path.series[1].name).toBe('Buffer 45 Axis 2');
      expect(path.series[0].points.map(p => p.pos)).toEqual([0, 12, 14]);
      expect(path.series[1].points.map(p => p.pos)).toEqual([0, 13, 15]);
      expect(path.series[0].points.map(p => p.vel)).toEqual([0, 50, 0]);
      expect(path.series[1].points.map(p => p.vel)).toEqual([0, 60, 0]);
      expect(path.series[0].points.map(p => p.relative ?? false)).toEqual([false, false, false]);
      expect(path.series[1].points.map(p => p.relative ?? false)).toEqual([false, false, false]);
    });


    it('imports called buffers', async () => {
      fireEvent.change(getSelectByTestId(wrapper, 'buffer-select'), { target: { value: '78' } });
      await waitUntilPass(() => expect(wrapper.queryByText(/Buffer 78 Axis 1/)).not.toBeNull());
      fireEvent.click(wrapper.getByTitle('Read'));
      let path: Path | null = null;
      await waitUntilPass(() => {
        path = selectUserPath(TestDialog.testStore.getState());
        expect(path).not.toBeNull();
      });

      path = path!;
      expect(path.series[0].name).toBe('Buffer 78 Axis 1');
      expect(path.series[1].name).toBe('Buffer 78 Axis 2');
      expect(path.series[0].points.map(p => p.pos)).toEqual([0, 12, 14, 16]);
      expect(path.series[1].points.map(p => p.pos)).toEqual([0, 13, 15, 17]);
      expect(path.series[0].points.map(p => p.relative ?? false)).toEqual([false, false, false, true]);
      expect(path.series[1].points.map(p => p.relative ?? false)).toEqual([false, false, false, true]);

      const devicePath = selectDevicePath(TestDialog.testStore.getState())!;
      const lastPoint = _.last(devicePath.series[0].points)!;
      expect(lastPoint.pos).toBeCloseTo(14 + 16);
      expect(lastPoint.relative ?? false).toBe(false);
    });
  });


  it('detects circular buffer calls', async () => {
    setDeviceCallback(device => {
      device.genericCommandMultiResponse = jest.fn(cmd => respondToGenericCommand(cmd));
      device._pvtBuffers = bufferContents;
    });

    bufferList = ['78', '45'];
    bufferContents['45'] = [
      'setup store 45 1',
      'point abs p 12000 v 0 t 500.0',
      'call 78',
      'setup disable'];
    bufferContents['78'] = [
      'setup store 78 1',
      'call 45',
      'setup disable'];

    createDevice();
    showDialog();

    await waitUntilPass(() => expect(wrapper.queryByText(/Buffer 45 Axis 1/)).not.toBeNull());
    fireEvent.click(wrapper.getByTitle('Read'));
    await waitUntilPass(() => {
      const messages = selectErrors(TestDialog.testStore.getState());
      expect(messages.map(m => m.text.toLowerCase())).toContainEqual(expect.stringMatching(/circular/));
    });
  });


  it('detects empty buffers', async () => {
    setDeviceCallback(device => {
      device.genericCommandMultiResponse = jest.fn(cmd => respondToGenericCommand(cmd));
      device._pvtBuffers = bufferContents;
    });

    bufferList = ['45'];
    bufferContents['45'] = [
      'setup store 45 1',
      'setup disable'];

    createDevice();
    showDialog();

    await waitUntilPass(() => expect(wrapper.queryByText(/Buffer 45 Axis 1/)).not.toBeNull());
    fireEvent.click(wrapper.getByTitle('Read'));
    await waitUntilPass(() => {
      const messages = selectErrors(TestDialog.testStore.getState());
      expect(messages.map(m => m.text.toLowerCase())).toContainEqual(expect.stringMatching(/no points/));
    });
  });


  it('detects calls to empty buffers', async () => {
    setDeviceCallback(device => {
      device.genericCommandMultiResponse = jest.fn(cmd => respondToGenericCommand(cmd));
      device._pvtBuffers = bufferContents;
    });

    bufferList = ['78', '45'];
    bufferContents['45'] = [
      'setup store 45 1',
      'point abs p 12000 v 0 t 500.0',
      'call 78',
      'setup disable'];
    bufferContents['78'] = [];

    createDevice();
    showDialog();

    await waitUntilPass(() => expect(wrapper.queryByText(/Buffer 45 Axis 1/)).not.toBeNull());
    fireEvent.click(wrapper.getByTitle('Read'));
    await waitUntilPass(() => {
      const messages = selectErrors(TestDialog.testStore.getState());
      expect(messages.map(m => m.text.toLowerCase())).toContainEqual(expect.stringMatching(/empty or disabled/));
    });
  });


  it('detects inconsistent buffer widths', async () => {
    setDeviceCallback(device => {
      device.genericCommandMultiResponse = jest.fn(cmd => respondToGenericCommand(cmd));
      device._pvtBuffers = bufferContents;
    });

    bufferList = ['78', '45'];
    bufferContents['45'] = [
      'setup store 45 1',
      'point abs p 12000 v 0 t 500.0',
      'call 78',
      'setup disable'];
    bufferContents['78'] = [
      'setup store 78 2',
      'point abs p 12000 12000 v 0 0 t 500.0',
      'setup disable'];

    createDevice();
    showDialog();

    await waitUntilPass(() => expect(wrapper.queryByText(/Buffer 45 Axis 1/)).not.toBeNull());
    fireEvent.click(wrapper.getByTitle('Read'));
    await waitUntilPass(() => {
      const messages = selectErrors(TestDialog.testStore.getState());
      expect(messages.map(m => m.text.toLowerCase())).toContainEqual(expect.stringMatching(/different number of axes/));
    });
  });
});
