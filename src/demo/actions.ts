import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

export enum ActionTypes {
  MOUNT = 'DEMO_MOUNT',
  UNMOUNT = 'DEMO_UNMOUNT',
  START_AXIS = 'DEMO_START_AXIS',
  STOP_AXIS = 'DEMO_STOP_AXIS',
  AXIS_STOPPED = 'DEMO_AXIS_STOPPED',
  START_ALL = 'DEMO_START_ALL',
  STOP_ALL = 'DEMO_STOP_ALL',
  UPDATE_SETTINGS = 'DEMO_UPDATE_SETTINGS',
}

export interface ActionsToPayloads {
  [ActionTypes.MOUNT]: void;
  [ActionTypes.UNMOUNT]: void;
  [ActionTypes.START_AXIS]: { key: EntityKey };
  [ActionTypes.STOP_AXIS]: { key: EntityKey };
  [ActionTypes.AXIS_STOPPED]: { key: EntityKey; error?: string };
  [ActionTypes.START_ALL]: void;
  [ActionTypes.STOP_ALL]: void;
  [ActionTypes.UPDATE_SETTINGS]: { demoCode?: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  mount: () => buildAction(ActionTypes.MOUNT),
  unmount: () => buildAction(ActionTypes.UNMOUNT),
  startAxis: (key: EntityKey) => buildAction(ActionTypes.START_AXIS, { key }),
  stopAxis: (key: EntityKey) => buildAction(ActionTypes.STOP_AXIS, { key }),
  axisStopped: (key: EntityKey, error?: string) => buildAction(ActionTypes.AXIS_STOPPED, { key, error }),
  startAll: () => buildAction(ActionTypes.START_ALL),
  stopAll: () => buildAction(ActionTypes.STOP_ALL),
  updateSettings: (payload: ActionsToPayloads[ActionTypes.UPDATE_SETTINGS]) => buildAction(ActionTypes.UPDATE_SETTINGS, payload),
};
