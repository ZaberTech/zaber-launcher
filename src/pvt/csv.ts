import fs from 'fs/promises';

import _ from 'lodash';
import { parse } from 'csv-parse/sync';
import { stringify } from 'csv-stringify/sync';
import { Length, Time, Units, Velocity } from '@zaber/motion';

import { unitsFromSymbol } from '../units';


export type CsvCellType = string | number | null;

export async function loadCsv(path: string): Promise<CsvCellType[][]> {
  const fileData = await fs.readFile(path, 'utf-8');
  return parseCsv(fileData);
}


export async function saveCsv(cells: CsvCellType[][], path: string) {
  const output = formatCsv(cells);
  await fs.writeFile(path, output);
}


export function parseCsv(fileData: string): CsvCellType[][] {
  return parse(fileData, {
    bom: true,
    columns: false,
    cast: true,
    trim: true,
  }) as CsvCellType[][];
}


export function formatCsv(cells: CsvCellType[][]): string {
  return stringify(cells, { encoding: 'utf-8' });
}


export function detectHeaders(csv: CsvCellType[][]): string[][] {
  const result = [];

  for (const row of csv) {
    if (row.every(cell => (cell == null || typeof cell === 'string'))) {
      result.push(row.map(cell => cell == null ? '' : cell as string));
    } else {
      break;
    }
  }

  return result;
}


export function getDataRows(csv: CsvCellType[][], headerRows: number): number[][] {
  const result: number[][] = [];

  const input = csv.slice(headerRows);
  input.forEach((row, i) => {
    if (_.some(row, cell => typeof cell !== 'number')) {
      throw new Error(`Non-numeric data was found on row ${i + 1 + headerRows} of the table.`);
    }

    result.push(row.map(cell => cell as number));
  });

  return result;
}


export interface CsvSeriesMap {
  timeColumn: number;
  timeUnit: Time | null;
  timeUnitDetected: boolean;
  positionUnit: Length | typeof Units.NATIVE;
  positionUnitDetected: boolean;
  velocityUnit: Velocity | typeof Units.NATIVE;
  velocityUnitDetected: boolean;
  series: {
    name: string;
    positionColumn: number;
    velocityColumn: number;
  }[];
}


export function detectSeriesAndUnits(headerRows: string[][], numColumns: number): CsvSeriesMap {
  let timeColumn = 0;
  let timeUnit: Time | null = null;
  const posColumns: number[] = [];
  const velColumns: number[] = [];
  const namesToColumns: _.Dictionary<{ p: number; v: number }> = {};
  const columnNames = Array(numColumns).fill('');
  let positionUnit: Units = Units.NATIVE;
  let velocityUnit: Units = Units.NATIVE;
  let timeUnitDetected = false;
  let positionUnitDetected = false;
  let velocityUnitDetected = false;

  function defaultSeriesName(i: number): string {
    return `Series ${String.fromCharCode('A'.charCodeAt(0) + i)}`;
  }

  switch (headerRows.length) {
    case 0:
      if ((numColumns & 1) === 0) {
        throw new Error(
          'Found an even number of columns. Expected one time column and pairs of position and velocity columns.');
      }
      for (let i = 1; i < numColumns; i += 2) {
        const name = defaultSeriesName((i - 1) / 2);
        namesToColumns[name] = { p: posColumns.length, v: velColumns.length };
        columnNames[posColumns.length] = name;
        posColumns.push(i);
        velColumns.push(i + 1);
      }
      break;

    case 1:
      {
        const headers = headerRows[0];
        let timeColumnDetected = false;

        for (let i = 0; i < headers.length; i++) {
          const cell = headers[i];
          // eslint-disable-next-line no-useless-escape
          const timeHeader = cell.match(/[Tt]ime(?:.*[\(\[](\w+)[\)\]])?/);
          if (timeHeader) {
            timeColumn = i;
            timeColumnDetected = true;
            if (timeHeader.length) {
              const parsedUnit = unitsFromSymbol(timeHeader[1]) as Time;
              if (parsedUnit) {
                timeUnitDetected = true;
                timeUnit = parsedUnit;
              }
            }
          }

          // eslint-disable-next-line no-useless-escape
          const posHeader = cell.match(/(?:(.*))[Pp](?:os\b|osition\b)(?:.*[\(\[](\S+)[\)\]])?/);
          if (posHeader) {
            if (posHeader.length) {
              const name = posHeader[1]?.length ? posHeader[1].trim() : defaultSeriesName(posColumns.length);
              namesToColumns[name] = { p: posColumns.length, v: (namesToColumns[name]?.v ?? -1) };
              columnNames[posColumns.length] = name;
              posColumns.push(i);
              const parsedUnit = unitsFromSymbol(posHeader[2]) as Length;
              if (parsedUnit) {
                if (positionUnitDetected && parsedUnit !== positionUnit) {
                  throw new Error('All position columns must use the same unit of measure.');
                }
                positionUnitDetected = true;
                positionUnit = parsedUnit;
              }
            }
          }

          // eslint-disable-next-line no-useless-escape
          const velHeader = cell.match(/(?:(.*))[Vv](?:el\b|elocity\b)(?:.*[\(\[]([\S]+)[\)\]])?/);
          if (velHeader) {
            if (velHeader.length) {
              const name = velHeader[1]?.length ? velHeader[1].trim() : defaultSeriesName(velColumns.length);
              namesToColumns[name] = { p: (namesToColumns[name]?.p ?? -1), v: velColumns.length };
              columnNames[velColumns.length] = name;
              velColumns.push(i);
              const parsedUnit = unitsFromSymbol(velHeader[2]) as Velocity;
              if (parsedUnit) {
                if (velocityUnitDetected && parsedUnit !== velocityUnit) {
                  throw new Error('All velocity columns must use the same unit of measure.');
                }
                velocityUnitDetected = true;
                velocityUnit = parsedUnit;
              }
            }
          }
        }

        if (!timeColumnDetected) {
          throw new Error('The file has headings but no time column was found.');
        }

        if (posColumns.length !== velColumns.length) {
          throw new Error('The file must contain equal numbers of columns for position and velocity.');
        }
      }
      break;

    default:
      throw new Error('Only zero or one header row is currently supported.');
  }

  if (posColumns.length < 1) {
    throw new Error('No position data was identified in the file.');
  }

  _.toPairs(namesToColumns).forEach(([key, value]) => {
    if (value.p < 0 || value.v < 0) {
      throw new Error(`Could not identify position and velocity columns for name ${key}`);
    }
  });

  if (positionUnitDetected !== velocityUnitDetected) {
    throw new Error('Position and velocity units must both be specified or both be omitted.');
  }

  return {
    timeColumn,
    timeUnit,
    timeUnitDetected,
    positionUnit,
    positionUnitDetected,
    velocityUnit,
    velocityUnitDetected,
    series: posColumns.map((_, i) => ({
      name: columnNames[i],
      positionColumn: posColumns[i],
      velocityColumn: velColumns[i]
    })),
  };
}
