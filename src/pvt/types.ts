import type { Angle, AngularVelocity, Length, Native, Time, Velocity } from '@zaber/motion';

import type { EntityKey } from '../keys';


export type PositionUnits = Length | Angle | Native;
export type VelocityUnits = Velocity | AngularVelocity | Native;


/**
 * Representation of control points/segments in user paths and generated points in discretized paths.
 */
export interface Point {
  /** Duration of this segment in seconds */
  dt: number;

  /** Absolute time of the END of the segment in seconds. */
  time: number;

  pos: number;
  vel: number;

  /** Currently can only be set for points loaded from device buffers. */
  relative?: boolean;

  /** Not loaded from CSV files; calculated on load and in discretized path points. */
  accel?: number;
}


export interface Peak {
  time: number;
  value: number;
}


export interface Range {
  min: Peak;
  max: Peak;
}


export interface Span {
  min: number;
  max: number;
}


export interface Series {
  name: string;
  points: Point[];

  /* Fields below are only populated in the discretized path, not in user-supplied data. */
  positionRange?: Range;
  velocityRange?: Range;
  accelerationRange?: Range;
  positionLimits?: Span;
  velocityLimits?: Span;
  accelerationLimits?: Span;
}


export interface Path {
  positionUnit: PositionUnits;
  velocityUnit: VelocityUnits;
  series: Series[];
}


export interface CsvImportOptions {
  dialogVisible: boolean;
  autoReload: boolean;
  allowAbsoluteTime: boolean;
  absoluteTime: boolean;
  detectedTimeUnit: Time | null;
  detectedPositionUnit: PositionUnits;
  detectedVelocityUnit: VelocityUnits;
  selectedTimeUnit: Time;
  selectedPositionUnit: PositionUnits;
  selectedVelocityUnit: VelocityUnits;
}


export type SeriesToAxisMap = Record<string, number>;

export interface DeviceSelection {
  deviceKey: EntityKey | null;
  axes: SeriesToAxisMap;
}


export enum ViewMode {
  Welcome = 'Welcome',
  TimeSeries = 'Time Series',
  Curve = '2D Plot',
  Table = 'Table',
}


export enum LimitType {
  HARD_LIMIT = 'HARD_LIMIT',
  SOFT_LIMIT = 'SOFT_LIMIT',
}


export interface Bug {
  seriesIndex: number;
  pointIndex: number;
  time: number;
  value: number;
  dimension: string;
  message: string;
  type: LimitType;
}


export enum Sign {
  Negative = -1,
  Positive = 1,
}


export interface LimitCrossing {
  time: number;
  index: number;
}


export type Severity = 'error' | 'warning' | 'info';

export interface Message {
  severity: Severity;
  text: string;
}
