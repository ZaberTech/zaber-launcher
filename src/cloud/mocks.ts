import { injectable } from 'inversify';

import type { CloudConfig } from '../app_components';

@injectable()
export class MessageRoutersServiceMock {
  async getConnection() {
    return this;
  }
  get api() {
    return this;
  }

  config: CloudConfig | null = null;
  isConnected = false;

  getCloudStatus = jest.fn(async () => ({ isConnected: this.isConnected }));
  getCloudConfig = jest.fn(async () => this.config);
  setCloudConfig = jest.fn(async (config: CloudConfig) => { this.config = config });
  setName = jest.fn().mockResolvedValue(undefined);
  getName = jest.fn().mockResolvedValue('name');

  mockSharingOn() {
    this.config = {
      apiUrl: 'https://api.test.io',  token: 'token', id: 'uuid', realm: 'identity',
    };
    this.isConnected = true;
  }
}
