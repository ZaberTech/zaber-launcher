import { fromVersion1 } from './save_file_compatibility';

test('converts from version 1: adds axisType from command', () => {
  expect(fromVersion1({
    something: 123,
    keys: [{
      keyNumber: 1,
      commands: [{
        event: 1,
        targetDevice: 2,
        targetAxis: 1,
        command: 'move max',
      }, {
        event: 2,
        targetDevice: 3,
        targetAxis: 0,
        command: 'lockstep 1 move min',
      }],
    }],
  })).toEqual({
    something: 123,
    keys: [{
      keyNumber: 1,
      commands: [{
        event: 1,
        targetDevice: 2,
        targetAxis: 1,
        axisType: 'axis',
        command: 'move max',
      }, {
        event: 2,
        targetDevice: 3,
        targetAxis: 1,
        axisType: 'lockstep',
        command: 'move min',
      }],
    }],
  });
});
