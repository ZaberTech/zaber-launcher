import React from 'react';
import { RenderResult, render } from '@testing-library/react';
import { injectable } from 'inversify';
import { Subject } from 'rxjs';

let terminal: TerminalMock;
class TerminalMock {
  constructor() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    terminal = this;
  }

  disposeToken = { dispose: jest.fn() };
  write = jest.fn();
  onData = jest.fn<void, [(data: string) => void]>(() => this.disposeToken);
  resize = jest.fn();
  focus = jest.fn();
  reset = jest.fn();
  open = jest.fn();
  dispose = jest.fn();
}

jest.mock('xterm', () => ({
  Terminal: TerminalMock,
}));

import { createContainer, destroyContainer } from '../container';
import { Pty } from './pty';
import { Terminal } from './Terminal';

let ptyMock: PtyMock;
@injectable()
class PtyMock {
  data = new Subject<string>();
  getDataSteam = jest.fn(() => this.data);
  sendData = jest.fn();
}

let container: ReturnType<typeof createContainer>;
let wrapper: RenderResult;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(Pty).to(PtyMock);
  ptyMock = container.get<unknown>(Pty) as PtyMock;
  wrapper = render(<Terminal id="id"/>);
});

afterEach(() => {
  wrapper?.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
  ptyMock = null!;
  terminal = null!;
});

test('opens terminal and disposes on unmount', async () => {
  expect(terminal.open).toHaveBeenCalled();
  expect(ptyMock.data.observed).toBe(true);

  wrapper?.unmount();
  wrapper = null!;

  expect(terminal.dispose).toHaveBeenCalled();
  expect(terminal.disposeToken.dispose).toHaveBeenCalled();
  expect(ptyMock.data.observed).toBe(false);
});

test('sends data written to terminal', async () => {
  terminal.onData.mock.calls[0][0]('data');
  expect(ptyMock.sendData).toHaveBeenLastCalledWith('id', 'data');
});

test('writes received data to terminal', async () => {
  ptyMock.data.next('data');
  expect(terminal.write).toHaveBeenLastCalledWith('data');
});

test('resets terminal on resetKey change', async () => {
  wrapper.rerender(<Terminal id="id" resetKey="reset"/>);
  expect(terminal.reset).toHaveBeenCalled();
});

test('focuses terminal on focusKey change', async () => {
  wrapper.rerender(<Terminal id="id" focusKey="focus"/>);
  expect(terminal.focus).toHaveBeenCalled();
});

test('resizes terminal on rows change', async () => {
  wrapper.rerender(<Terminal id="id" cols={60} rows={30}/>);
  expect(terminal.resize).toHaveBeenLastCalledWith(60, 30);
});
