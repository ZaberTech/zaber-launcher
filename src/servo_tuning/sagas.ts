import fs from 'fs';

import { ServoTuner, ServoTuningParam, ServoTuningParamset } from '@zaber/motion/ascii';
import { throwUnexpectedError, makeString } from '@zaber/toolbox';
import type { SagaIterator } from 'redux-saga';
import { all, select, call, put, takeLatest, takeEvery } from 'redux-saga/effects';
import * as remote from '@electron/remote';
import { CommandFailedException, ConnectionFailedException, DCElectricCurrent, Inertia } from '@zaber/motion';
import { takeLeadingUniq } from '@zaber/toolbox/lib/redux';
import _ from 'lodash';

import { getAxisOrOnlyAxis } from '../connection_manager';
import type { EntityKey } from '../keys';
import type { Action, RT } from '../utils';
import { getContainer } from '../container';
import { ZaberApi } from '../app_components';
import { commandExists } from '../devices';
import { handleNonCriticalError } from '../errors';
import { selectServoTuning } from '../store';
import { convertBetweenUnits, measurementOK } from '../units';
import { Dialogs } from '../dialogs';

import { actionDefinitions as actions, ActionsToPayloads, ActionTypes } from './actions';
import { selectChosen, selectEntity, selectProduct, selectSavedRows } from './selectors';
import { SimpleTuningInfo, SimpleTuningSlider, TuningMethod, defaultServoTuningSetting, SimpleTuning } from './types';
import { generateCsv, parseSavedRows, SavedRow, SavedRowsErrors } from './saved_rows/saved_rows';
import { paramsetNames } from './names';

export function* servoTunerSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.CHOOSE_ENTITY, chooseEntity),
    takeLatest(ActionTypes.CHOOSE_TUNING, chooseTuning),
    takeLatest(ActionTypes.READ_TUNING_PARAMS, forceReadParamsetTuning),
    takeLatest(ActionTypes.CHANGE_STARTUP_PRESET, changeStartupPreset),
    takeEvery(ActionTypes.LOAD_PARAMSET, loadPreset),
    takeLatest(ActionTypes.UPDATE_SETTING, updateSetting),
    takeLatest(ActionTypes.APPLY_ADVANCED, applyAdvanced),
    takeLatest(ActionTypes.APPLY_PID, applyPid),
    takeLatest(ActionTypes.APPLY_SIMPLE, applySimple),
    takeLatest(ActionTypes.APPLY_SAVED_ROW, applySavedRow),
    takeLatest(ActionTypes.IMPORT_CSV, importCSV),
    takeLeadingUniq(ActionTypes.ADD_ROW_OF_PARAMSET, action => action.payload.paramset.toString(), addRowOfParamset),
    takeLatest(ActionTypes.EXPORT_CSV, exportCsv),
    takeEvery(ActionTypes.SET_DRIVER_ENABLED, setDriverEnabled),
    takeLatest(ActionTypes.SET_DRIVER_STATE, setDriverState),
  ]);
}

function* chooseEntity(params: Action<ActionsToPayloads[ActionTypes.CHOOSE_ENTITY]>) {
  const { chosenKey } = params.payload;
  const product: RT<typeof selectProduct> = yield select(selectProduct);
  if (chosenKey == null || product == null) { return }
  if (!commandExists('servo get startup', product)) {
    yield put(actions.setMessage({ type: 'top-note', message: 'This product cannot be servo tuned' }));
    return;
  }

  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, chosenKey);
    const startupParamset: RT<typeof tuner.getStartupParamset> = yield call([tuner, tuner.getStartupParamset]);
    yield put(actions.chooseTuning({ entity: chosenKey, paramset: startupParamset, method: 'simple' }));
    const api = getContainer().get(ZaberApi);
    const info: RT<typeof api.getServoTuningInfo> = yield call([api, api.getServoTuningInfo], product.product.id);
    const driverCurrent: RT<typeof tuner.axis.settings.get> = yield call(
      [tuner.axis.settings, tuner.axis.settings.get], 'driver.current.servo', DCElectricCurrent.AMPERES
    );
    const driverEnabledSetting: RT<typeof tuner.axis.settings.get> = yield call(
      [tuner.axis.settings, tuner.axis.settings.get], 'driver.enabled'
    );
    const accel: RT<typeof tuner.axis.settings.get> = yield call([tuner.axis.settings, tuner.axis.settings.get], 'accel');
    const tolerance: RT<typeof tuner.axis.settings.get> = yield call(
      [tuner.axis.settings, tuner.axis.settings.get], 'cloop.settle.tolerance'
    );
    const period: RT<typeof tuner.axis.settings.get> = yield call([tuner.axis.settings, tuner.axis.settings.get], 'cloop.settle.period');
    yield put(actions.infoFetched({
      startupParamset,
      settings: {
        'accel': defaultServoTuningSetting('accel', accel),
        'cloop.settle.tolerance': defaultServoTuningSetting('cloop.settle.tolerance', tolerance),
        'cloop.settle.period': defaultServoTuningSetting('cloop.settle.period', period),
      },
      defaultCarriageMassKg: info.CarriageInertia / 1000,
      maxForceN: info.Forque * driverCurrent,
      recommendedAccel: null,
      driverEnabled: driverEnabledSetting === 1,
    }));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setMessage({ type: 'top-error', message: `There was an error getting tuning for this product: ${e}` }));
  }
}

function* chooseTuning({ payload: { entity, paramset, method } }: Action<ActionsToPayloads[ActionTypes.CHOOSE_TUNING]>) {
  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, entity);
    yield call(readTuningParams, tuner, paramset, method);
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setMessage({ type: 'top-error', message: `There was an error getting tuning for this paramset: ${e}` }));
  }
}

function* forceReadParamsetTuning({ payload: { entity, paramset, method } }: Action<ActionsToPayloads[ActionTypes.READ_TUNING_PARAMS]>) {
  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, entity);
    yield call(readTuningParams, tuner, paramset, method);
  } catch (e) {
    handleNonCriticalError(e, 'info');
  }
}

function* getTuner(key: EntityKey): SagaIterator<ServoTuner> {
  const axis: RT<typeof getAxisOrOnlyAxis> = yield call(getAxisOrOnlyAxis, key);
  return new ServoTuner(axis);
}

function tuningFromSliders(sliders: SimpleTuning['sliders']): ServoTuningParam[] {
  return Object.values(sliders).map(slider => ({ name: slider.name, value: slider.position }));
}

async function getSimpleTuningInfo(
  tuner: ServoTuner,
  product: NonNullable<RT<typeof selectProduct>>,
  paramset: ServoTuningParamset,
  defaultCarriageMass: number
): Promise<SimpleTuningInfo> {
  if (!commandExists('storage axis print', product)) {
    return { method: 'simple', paramset, supported: false };
  }
  const paramList = await tuner.getSimpleTuningParamDefinitions();
  const defaultSliders = Object.fromEntries(paramList.map((param): [string, SimpleTuningSlider] => {
    const defaultValue = param.defaultValue ?? 0.5;
    return [param.name, { ...param, defaultValue, position: defaultValue }];
  }));
  try {
    const tuning = await tuner.getSimpleTuning(paramset);
    const carriageModified = tuning.carriageMass != null;
    const positionedSliders = _.mapValues(defaultSliders, (slider, name) => ({
      ...slider,
      position: tuning.tuningParams.find(p => p.name === name)?.value ?? slider.position
    }));
    const message = tuning.isUsed ? undefined : { status: 'will-override' } as const;

    return {
      method: 'simple',
      paramset,
      supported: true,
      tuning: {
        carriageModified,
        sliders: positionedSliders,
        loadMass: { value: tuning.loadMass, units: Inertia.KILOGRAMS },
        carriageMass: { value: tuning.carriageMass ?? defaultCarriageMass, units: Inertia.KILOGRAMS },
      },
      message
    };
  } catch (e) {
    throwUnexpectedError(e);
    return {
      method: 'simple',
      paramset,
      supported: true,
      tuning: {
        sliders: defaultSliders,
        loadMass: { value: 0, units: Inertia.KILOGRAMS },
        carriageModified: false,
        carriageMass: { value: defaultCarriageMass, units: Inertia.KILOGRAMS }
      },
      message: {
        status: 'error',
        message: `The simple tuning saved for this paramset is invalid (${e.message}). To fix this set a valid simple tuning.`
      },
    };
  }
}

/**
 * Reads the specified paramset's current parameters if a method has been chosen
 * @param tuner The tuner to get values from
 * @param chosenParamset The paramset to read from
 */
function* readTuningParams(tuner: ServoTuner, paramset: ServoTuningParamset, method: TuningMethod) {
  const state: ReturnType<typeof selectServoTuning> = yield select(selectServoTuning);
  const info = state.tuning;

  if (method === 'advanced' && info?.method !== 'advanced') {
    const raw: RT<typeof tuner.getTuning> = yield call([tuner, tuner.getTuning], paramset);
    const tuning = Object.fromEntries(raw.params.map((param): [string, number] => ([param.name, param.value])));
    yield put(actions.updateParamTuning({ method: 'advanced', paramset, tuning }));
  } else if (method === 'pid' && info?.method !== 'pid') {
    const tuning: RT<typeof tuner.getPidTuning> = yield call([tuner, tuner.getPidTuning], paramset);
    yield put(actions.updateParamTuning({ method: 'pid', paramset, tuning }));
  } else if (method === 'simple' && info?.method !== 'simple') {
    const product: RT<typeof selectProduct> = yield select(selectProduct);
    if (product != null) {
      const simple: RT<typeof getSimpleTuningInfo> = yield call(
        getSimpleTuningInfo, tuner, product, paramset, state.deviceInfo?.defaultCarriageMassKg ?? 0
      );
      yield put(actions.updateParamTuning(simple));
    } else {
      yield put(actions.setMessage({ type: 'top-error', message: 'Could not fetch product info.' }));
    }
  }
}

function* changeStartupPreset(params: Action<ActionsToPayloads[ActionTypes.CHANGE_STARTUP_PRESET]>) {
  const { key, paramset } = params.payload;
  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, key);
    yield call([tuner, tuner.setStartupParamset], paramset);
    yield put(actions.changeStartupPresetDone(paramset));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setMessage({ type: 'modal-error', message: `Failed to set new power-on preset: ${e}` }));
  }
}

function* loadPreset({ payload: { key, to, from } }: Action<ActionsToPayloads[ActionTypes.LOAD_PARAMSET]>) {
  const chosen: RT<typeof selectChosen> = yield select(selectChosen);
  yield put(actions.setMessage({ type: 'modal-working', message: 'Loading Preset...' }));
  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, key);
    yield call([tuner, tuner.loadParamset], to, from);
    yield put(actions.applyDone(to, false));
    if (chosen?.paramset === to) {
      yield call(readTuningParams, tuner, to, chosen.method);
    }
    yield put(actions.setMessage({
      type: 'modal-success', message: `Successfully loaded tuning from ${paramsetNames[from]} to ${paramsetNames[to]}`
    }));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setMessage({ type: 'modal-error', message: `Failed to load preset ${from} to ${to}: ${e}` }));
  }
}

function* updateSetting(params: Action<ActionsToPayloads[ActionTypes.UPDATE_SETTING]>) {
  const { setting, update } = params.payload;
  if (update.mode !== 'write') { return }
  yield put(actions.updateSettingError(setting, null));

  const entity: RT<typeof selectEntity> = yield select(selectEntity);
  if (entity == null) { return }

  try {
    const axis: RT<typeof getAxisOrOnlyAxis> = yield call(getAxisOrOnlyAxis, entity);
    yield call([axis.settings, axis.settings.set], setting, update.value.value, update.value.units);
    yield put(actions.updateSetting(setting, { ...update, mode: 'display' }));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.updateSettingError(setting, e.message));
    yield put(actions.updateSetting(setting, { ...update, mode: 'edit' }));
  }
}

function* copyStagingToLive(tuner: ServoTuner) {
  yield call([tuner, tuner.loadParamset], ServoTuningParamset.LIVE, ServoTuningParamset.STAGING);
  yield put(actions.applyDone(ServoTuningParamset.LIVE, false));
}

function* usePresetAtPowerUp(tuner: ServoTuner, paramset: ServoTuningParamset, toLive: boolean) {
  try {
    yield call([tuner, tuner.setStartupParamset], paramset);
    yield put(actions.changeStartupPresetDone(paramset));
  } catch (e) {
    const details = e instanceof Error ? `: ${e.message}` : '';
    yield put(actions.applyDone(paramset, toLive, `Failed to make ${paramsetNames[paramset]} the power-up paramset${details}`));
  }
}

function* applyAdvanced(params: Action<ActionsToPayloads[ActionTypes.APPLY_ADVANCED]>) {
  const { entity: chosenKey, paramset: chosenParamset, tuning, toLive, toPowerOn } = params.payload;

  const servoParams: ServoTuningParam[] = [];
  for (const [name, value] of Object.entries(tuning)) {
    if (value == null) {
      yield put(actions.setMessage({
        type: 'modal-error', message: `Parameter ${name} has an invalid value. Please fix it and try again.`
      }));
      return;
    }
    servoParams.push({ name, value });
  }

  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, chosenKey);
    yield call([tuner, tuner.setTuning], ServoTuningParamset.STAGING, servoParams);
    yield call([tuner, tuner.loadParamset], chosenParamset, ServoTuningParamset.STAGING);
    if (toLive) {
      yield call(copyStagingToLive, tuner);
    }
    yield put(actions.applyDone(chosenParamset, toLive));
    if (toPowerOn) {
      yield call(usePresetAtPowerUp, tuner, chosenParamset, toLive);
    }
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.applyDone(chosenParamset, toLive, `Failed to apply this tuning: ${e.message}`));
    return;
  }
}

function* applyPid(params: Action<ActionsToPayloads[ActionTypes.APPLY_PID]>) {
  const { entity: chosenKey, paramset, tuning, toLive, toPowerOn } = params.payload;
  const { p, i, d, fc } = tuning;

  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, chosenKey);
    yield call([tuner, tuner.setPidTuning], ServoTuningParamset.STAGING, p, i, d, fc);
    yield call([tuner, tuner.loadParamset], paramset, ServoTuningParamset.STAGING);
    if (toLive) {
      yield call(copyStagingToLive, tuner);
    }
    yield put(actions.applyDone(params.payload.paramset, toLive));
    if (toPowerOn) {
      yield call(usePresetAtPowerUp, tuner, paramset, toLive);
    }
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.applyDone(params.payload.paramset, toLive, `Failed to apply this tuning: ${e.message}`));
  }
}

function* applySimple(params: Action<ActionsToPayloads[ActionTypes.APPLY_SIMPLE]>) {
  const { entity, paramset, tuning: { sliders, loadMass, carriageMass, carriageModified }, toLive, toPowerOn } = params.payload;

  if (!measurementOK(loadMass)) {
    yield put(actions.applyDone(paramset, toLive, 'Invalid Load Mass.'));
    return;
  }
  const loadMassKg = convertBetweenUnits(loadMass.value, loadMass.units, Inertia.KILOGRAMS);

  if (!measurementOK(carriageMass)) {
    yield put(actions.setMessage({
      type: 'modal-error', message: 'Invalid Carriage Mass. If your carriage is not modified, uncheck the "Custom Carriage" control'
    }));
    return;
  }
  const carriageMassParamKg = carriageModified ? convertBetweenUnits(carriageMass.value, carriageMass.units, Inertia.KILOGRAMS) : undefined;

  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, entity);
    const tuning = tuningFromSliders(sliders);
    // Tune the axis
    yield call([tuner, tuner.setSimpleTuning], ServoTuningParamset.STAGING, tuning, loadMassKg, { carriageMass: carriageMassParamKg });
    yield call([tuner, tuner.loadParamset], paramset, ServoTuningParamset.STAGING);
    if (toLive) {
      yield call(copyStagingToLive, tuner);
    }
    yield put(actions.applyDone(paramset, toLive));
    if (toPowerOn) {
      yield call(usePresetAtPowerUp, tuner, paramset, toLive);
    }
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.applyDone(paramset, toLive, `Failed to apply this tuning: ${e.message}`));
  }
}

function* applySavedRow(params: Action<ActionsToPayloads[ActionTypes.APPLY_SAVED_ROW]>) {
  const { entity, paramset, row } = params.payload;
  const tuning = row.params;
  yield put(actions.setMessage({ type: 'modal-working', message: `Applying ${row.name} to ${paramsetNames[paramset]}` }));

  const servoParams: ServoTuningParam[] = [];
  for (const [name, value] of Object.entries(tuning)) {
    if (value == null) {
      yield put(actions.setMessage({
        type: 'modal-error', message: `Parameter ${name} has an invalid value. Please fix it and try again.`
      }));
      return;
    }
    servoParams.push({ name, value });
  }

  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, entity);
    yield call([tuner, tuner.setTuning], ServoTuningParamset.STAGING, servoParams);
    yield call([tuner, tuner.loadParamset], paramset, ServoTuningParamset.STAGING);
    yield put(actions.setMessage({ type: 'modal-success', message: `Applied ${row.name} to ${paramsetNames[paramset]}` }));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setMessage({ type: 'modal-error', message: `Applying ${row.name} failed: ${e}` }));
    return;
  }
}

function* importCSV() {
  yield put(actions.setMessage({ type: 'modal-screen' }));
  try {
    const fileLocation: RT<typeof Dialogs.showOpenDialog> = yield call([remote.dialog, Dialogs.showOpenDialog], {
      properties: ['openFile'],
      filters: [{ name: 'CSV', extensions: ['csv'] }],
    });
    if (fileLocation.canceled) {
      yield put(actions.setMessage(null));
      return;
    }
    const path = fileLocation.filePaths[0];
    if (path == null) {
      yield put(actions.setMessage({ type: 'modal-error', message: 'Could not open this file. Choose a different file or try again.' }));
      return;
    }
    const fileContents: RT<typeof fs.promises.readFile> = yield call([fs.promises, fs.promises.readFile], path);
    const tuningCSV = fileContents.toString();

    const rows = parseSavedRows(tuningCSV);
    yield put(actions.importCsvDone(rows));
    yield put(actions.setMessage(null));
  } catch (e) {
    if (e instanceof SavedRowsErrors) {
      yield put(actions.setMessage({ type: 'modal-error', message: `Badly formatted file: ${e.message}` }));
    } else {
      handleNonCriticalError(e);
      yield put(actions.setMessage({ type: 'modal-error', message: `Failed to import from file: ${makeString(e)}` }));
    }
  }
}

function* addRowOfParamset(params: Action<ActionsToPayloads[ActionTypes.ADD_ROW_OF_PARAMSET]>) {
  const { entity: chosenKey, paramset: chosenParamset } = params.payload;
  try {
    const tuner: RT<typeof getTuner> = yield call(getTuner, chosenKey);
    const tuning: RT<typeof tuner.getTuning> = yield call([tuner, tuner.getTuning], chosenParamset);
    const row: SavedRow = {
      name: paramsetNames[chosenParamset],
      controller: {
        type: tuning.type,
        version: tuning.version,
      },
      params: Object.fromEntries(tuning.params.map((param): [string, number] => ([param.name, param.value]))),
    };
    yield put(actions.addRow(row));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setMessage({ type: 'modal-error', message: `Failed to add this tuning to a row: ${e}` }));
  }
}

function* exportCsv() {
  yield put(actions.setMessage({ type: 'modal-screen' }));
  try {
    const rows: RT<typeof selectSavedRows> = yield select(selectSavedRows);
    const csvString = generateCsv(rows);
    const saveLocation: RT<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
      title: 'Exporting Servo Tuning CSV',
      defaultPath: 'tuning.csv',
    });
    if (saveLocation.canceled) {
      yield put(actions.setMessage(null));
      return;
    }
    if (saveLocation.filePath == null) {
      yield put(actions.setMessage({ type: 'modal-error', message: 'Invalid location to save a file to.' }));
      return;
    }

    yield call([fs.promises, fs.promises.writeFile], saveLocation.filePath, csvString);
    yield put(actions.setMessage(null));
  } catch (e) {
    handleNonCriticalError(e);
    yield put(actions.setMessage({ type: 'modal-error', message: `Failed to get save your state to a CSV: ${makeString(e)}` }));
  }
}

function* setDriverEnabled({ payload: { key, enable } }: Action<ActionsToPayloads[ActionTypes.SET_DRIVER_ENABLED]>) {
  try {
    const axis: RT<typeof getAxisOrOnlyAxis> = yield call(getAxisOrOnlyAxis, key);
    if (enable) {
      yield call([axis, axis.driverEnable]);
    } else {
      yield call([axis, axis.driverDisable]);
    }
    yield put(actions.setDriverEnabledDone(enable));
  } catch (e) {
    if (![ConnectionFailedException, CommandFailedException].some(errType => e instanceof errType)) {
      handleNonCriticalError(e);
    }
  }
}

function* setDriverState({ payload: { key } }: Action<ActionsToPayloads[ActionTypes.SET_DRIVER_STATE]>) {
  try {
    const axis: RT<typeof getAxisOrOnlyAxis> = yield call(getAxisOrOnlyAxis, key);
    const enabled: RT<typeof axis.settings.get> = yield call([axis.settings, axis.settings.get], 'driver.enabled');
    yield put(actions.setDriverEnabledDone(enabled > 0));
  } catch (e) {
    if (![ConnectionFailedException, CommandFailedException].some(errType => e instanceof errType)) {
      handleNonCriticalError(e);
    }
  }
}
