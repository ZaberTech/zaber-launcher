import { Units } from '@zaber/motion';
import type { GetSetting, GetSettingResult, TriggerEnabledState } from '@zaber/motion/ascii';

export type GetIoPort = {
  type: 'in' | 'out';
  channel: number;
};

/** The supported types of polling */
export const POLLED = ['settings', 'analogIo', 'warnings', 'triggers'] as const;
export type Polled = typeof POLLED[number];

/** The types of the getters for each type of polling. A getter is the data needed to know what should be polled */
export type PolledGetter = {
  settings: GetSetting;
  analogIo: GetIoPort;
  warnings: { axisNumber: number };
  triggers: null;
};

export type PollEntry<GetterType> = {
  /** Information on the information to poll */
  get: GetterType;
  /** The number of watchers expecting a poll at a fast cadence */
  fast: number;
  /** The number of watchers expecting a poll at a slow cadence */
  slow: number;
};

export type PollRequest = { [P in Polled]: Record<string, PollEntry<PolledGetter[P]>> };

/** A map of functions used to generate keys for a poll result */
export const keyGenerators: {[P in Polled]: (getter: PolledGetter[P]) => string} = {
  settings: getter => `${getter.setting}(${getter.unit ?? Units.NATIVE})[${getter.axes?.join(',') ?? '0'}]`,
  analogIo: getter => `${getter.type}.${getter.channel}`,
  warnings: getter => getter.axisNumber.toString(),
  triggers: () => 'all',
};

/** A map from polling type to the type of result of the poll */
type PolledResults = {
  settings: GetSettingResult;
  analogIo: number;
  warnings: string[];
  triggers: TriggerEnabledState[];
};

type PolledResultsMap = { [P in Polled]: Partial<Record<string, PolledResults[P]>> };

export type PollResultSuccess = { type: 'success' } & PolledResultsMap;

export type PollingError = {
  type: 'error';
  message: string;
  details: string;
};

export type PollResult = PollResultSuccess | PollingError;
