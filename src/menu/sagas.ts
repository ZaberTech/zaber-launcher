import { all, takeLatest, put, fork } from 'redux-saga/effects';
import type { Action } from '@zaber/toolbox/lib/redux';
import type { SagaIterator } from 'redux-saga';
import { notNil } from '@zaber/toolbox';

import { getContainer } from '../container';
import { environment } from '../environment';
import { Storage } from '../app_components';

import { ActionTypes, ActionsToPayloads, actions } from './actions';

export const MENU_PINNED_STORAGE_KEY = 'MENU_PINNED';

export function* menuSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.PIN, pinMenu),
    !environment.isTest ? fork(setPinnedAfterStart) : null,
  ].filter(notNil));
}

function pinMenu({ payload: { pin, saveToStorage } }: Action<ActionsToPayloads[ActionTypes.PIN]>) {
  if (!saveToStorage) { return }
  const storage = getContainer().get(Storage);
  storage.save<boolean>(MENU_PINNED_STORAGE_KEY, pin);
}

export function* setPinnedAfterStart(): SagaIterator {
  const storage = getContainer().get(Storage);
  const isPinned = storage.load<boolean>(MENU_PINNED_STORAGE_KEY, false);
  yield put(actions.pinMenu(isPinned, false));
}
