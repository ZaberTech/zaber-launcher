import { Container } from 'inversify';

let container: Container;

export function getContainer(): Container {
  return container;
}

export function createContainer(): Container {
  container = new Container({ autoBindInjectable: true, defaultScope: 'Singleton' });

  return container;
}

export function destroyContainer(): void {
  if (container) {
    container.unbindAll();
    container = null!;
  }
}
