import React, { useState }  from 'react';
import { useSelector } from 'react-redux';
import { Button, ContextMenu, HelpTooltip, Icons, MinorMenu, NoticeBanner, Text, SimpleSelect, Loader } from '@zaber/react-library';
import classNames from 'classnames';
import _ from 'lodash';

import { useActions } from '../utils';
import { Editions, environment } from '../environment';

import { actions as actionsDefinition } from './actions';
import { JoystickKey } from './Key';
import {
  selectConfigNeedsApply,
  selectConfigWriting,
  selectManualMode,
  selectSaveLoadConfigError,
  selectJoystick,
  selectProductionMode,
  selectConfigRestoring,
  selectConfigWriteOrRestoreError,
} from './selectors';
import { Calibration } from './Calibration';
import { Axes } from './Axes';
import { JoystickPicture } from './Picture';

export const Joystick: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const configWriting = useSelector(selectConfigWriting);
  const configRestoring = useSelector(selectConfigRestoring);
  const configWriteOrRestoreError = useSelector(selectConfigWriteOrRestoreError);
  const manualMode = useSelector(selectManualMode);
  const productionMode = useSelector(selectProductionMode);
  const saveLoadError = useSelector(selectSaveLoadConfigError);
  const needsApply = useSelector(selectConfigNeedsApply);
  const joystick = useSelector(selectJoystick)!;

  const [calibrationOpen, setCalibrationOpen] = useState(false);

  return (<div className="joystick">
    <MinorMenu barStyle="horizontal">
      <div className={classNames(!manualMode && 'active')} onClick={() => actions.setManualMode(false)}>
        <MinorMenu.Item>
          Simple Mode
        </MinorMenu.Item>
      </div>
      <div className={classNames(manualMode && 'active')} onClick={() => actions.setManualMode(true)}>
        <MinorMenu.Item>
          Advanced Mode&nbsp;
          <HelpTooltip>Advanced mode allows direct editing of device and axis numbers as well as some additional settings.</HelpTooltip>
        </MinorMenu.Item>
      </div>

      <div className="right-side">
        {needsApply && <div className="changes-pending"><Icons.ErrorWarning/>Changes pending</div>}
        {needsApply && !configRestoring &&
          <Icons.Restore
            className="undo-changes" title="Revert Pending Changes" onClick={() => actions.restoreConfig()} data-testid="restore"/>}
        {configRestoring && <Loader className="undo-changes" size="medium"/>}
        <Button disabled={configWriting} onClick={() => actions.writeConfig()}>
          {!configWriting ? <>Apply</> : <>Applying...</>}
        </Button>

        <ContextMenu>
          <ContextMenu.Item onClick={() => actions.saveConfig()} icon={<Icons.Save/>}>Save to File</ContextMenu.Item>
          <ContextMenu.Item onClick={() => actions.loadConfig()} icon={<Icons.OpenFile/>}>Load from File</ContextMenu.Item>
          {joystick.axisCount > 0 &&
            <ContextMenu.Item onClick={() => setCalibrationOpen(true)} icon={<Icons.HardwareModification/>}>
              Calibrate Joystick
            </ContextMenu.Item>}
          {environment.edition !== Editions.Public &&
            <ContextMenu.CheckboxItem className="production-mode"
              checked={productionMode} onChecked={actions.setProductionMode}>
              Production Mode&ensp;<HelpTooltip>Production Mode makes axes use native units.</HelpTooltip>
            </ContextMenu.CheckboxItem>}
        </ContextMenu>
      </div>
    </MinorMenu>

    {configWriteOrRestoreError != null && <NoticeBanner>{configWriteOrRestoreError}</NoticeBanner>}
    {saveLoadError != null && <NoticeBanner closer={() => actions.setSaveLoadConfigError(null)}>{saveLoadError}</NoticeBanner>}

    <Axes/>
    <Keys/>

    <Calibration isOpen={calibrationOpen} onClose={() => setCalibrationOpen(false)}/>
  </div>);
};

const Keys: React.FC = () => {
  const joystick = useSelector(selectJoystick);
  const [selectedKey, setSelectedKey] = useState(1);

  if (!joystick?.keyCount) { return null }

  return (<>
    {joystick.isDefaultModel && <div className="select-key">
      <Text t={Text.Type.Instruction} e={Text.Emphasis.Light}>Click on a key to configure it.</Text>
      <JoystickPicture activeButton={selectedKey} onClick={setSelectedKey}/>
    </div>}
    {!joystick.isDefaultModel && <div className="select-key-2">
      <Text>Selected key:</Text>
      <SimpleSelect value={selectedKey} onValueChange={setSelectedKey}
        options={_.range(1, joystick.keyCount + 1).map(value => ({ value, label: `Key ${value}` }))}/>
    </div>}

    <JoystickKey key={selectedKey} keyNumber={selectedKey}/>
  </>);
};
