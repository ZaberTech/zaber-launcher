import { type DeviceInfoWithAxes } from '../connection_manager';
import { changeDictionary, createReducer } from '../utils';
import { ConnectionType } from '../app_components';
import { environment, Flavors } from '../environment';
import { EntityKey } from '../keys';

import { ActionsToPayloads, ActionTypes } from './actions';
import {
  ErrorWithTitle, Steps, MDNSCandidate,
  MDNS_SINGLE_PORT, MDNS_CHAIN_PORT, tcpConnectionType
} from './types';

export interface State {
  dialogOpen: boolean;
  step: Steps | null;

  addingConnection: boolean;
  addConnectionError: string | null;

  connectionsBeingRemoved: Record<string, {
      removing: boolean;
      error: string | null;
    }>;

  connectionType: ConnectionType | null;

  hideSimulatorNotice: boolean;

  serialPort: {
    selectedPort: string | null;
    baudRate: number | null;
    ports: string[] | null;
    portFoundByConnecting: string | null;
    showOnlyRecommendedPorts: boolean;
  };

  tcp: {
    selectedConnectionType: tcpConnectionType;
    hostname: string;
    port: number | null;
    mdnsCandidates: MDNSCandidate[];
    discovering: boolean;
    fetchingDeviceInfoError: string | null;
    error: string | null;
  };

  connectCloud: {
    cloudId: string;
    connecting: boolean;
    error: ErrorWithTitle | null;
  };

  localShare: {
    connecting: boolean;
    error: ErrorWithTitle | null;
  };

  detectDevices: {
    detecting: boolean;
    detected: DeviceInfoWithAxes[] | null;
    error: ErrorWithTitle | null;
  };

  lastAddedConnection: EntityKey | null;
}

const initialState: State = {
  step: null,
  dialogOpen: false,
  addingConnection: false,
  addConnectionError: null,
  connectionsBeingRemoved: {},

  connectionType: null,

  hideSimulatorNotice: false,

  serialPort: {
    selectedPort: null,
    baudRate: null,
    ports: null,
    portFoundByConnecting: null,
    showOnlyRecommendedPorts: true,
  },

  tcp: {
    selectedConnectionType: 'chain',
    hostname: '',
    port: MDNS_CHAIN_PORT,
    mdnsCandidates: [],
    discovering: false,
    fetchingDeviceInfoError: null,
    error: null,
  },

  connectCloud: {
    cloudId: '',
    connecting: false,
    error: null,
  },

  localShare: {
    connecting: false,
    error: null,
  },

  detectDevices: {
    detecting: false,
    detected: null,
    error: null,
  },

  lastAddedConnection: null,
};

const addConnection = (state: State): State =>
  ({
    ...state,
    addingConnection: true,
    addConnectionError: null,
  });

const addConnectionDone = (state: State, { connectionKey }: ActionsToPayloads[ActionTypes.ADD_CONNECTION_DONE]): State =>
  ({
    ...initialState,
    addingConnection: false,
    lastAddedConnection: connectionKey,
  });

const addConnectionErr = (state: State, { error }: ActionsToPayloads[ActionTypes.ADD_CONNECTION_ERR]): State =>
  ({
    ...state,
    addingConnection: false,
    addConnectionError: error,
  });

const changePort = (state: State, { selectedPort }: ActionsToPayloads[ActionTypes.CHANGE_PORT]): State =>
  ({
    ...state,
    addingConnection: false,
    serialPort: {
      ...state.serialPort,
      selectedPort,
    },
  });

const clearPort = (state: State): State =>
  ({
    ...state,
    serialPort: {
      ...state.serialPort,
      selectedPort: null,
    },
  });

const removeConnection = (state: State, { connectionKey }: ActionsToPayloads[ActionTypes.REMOVE_CONNECTION]): State =>
  ({
    ...state,
    connectionsBeingRemoved: {
      ...state.connectionsBeingRemoved,
      [connectionKey]: { removing: true, error: null },
    },
  });

const removeConnectionDone = (state: State, { connectionKey, error }: ActionsToPayloads[ActionTypes.REMOVE_CONNECTION_DONE]): State =>
  ({
    ...state,
    connectionsBeingRemoved: changeDictionary(state.connectionsBeingRemoved, connectionKey, { removing: false, error }),
  });

const changeBaudRate = (state: State, { baudRate }: ActionsToPayloads[ActionTypes.CHANGE_BAUD_RATE]): State =>
  ({
    ...state,
    serialPort: {
      ...state.serialPort,
      baudRate,
    },
  });

const portsChanged = (state: State, { ports }: ActionsToPayloads[ActionTypes.PORTS_CHANGED]): State => {
  const { serialPort: { selectedPort, portFoundByConnecting }, serialPort } = state;
  return ({
    ...state,
    serialPort: {
      ...serialPort,
      ports,
      selectedPort: ports.includes(selectedPort ?? '') ? selectedPort : null,
      portFoundByConnecting: ports.includes(portFoundByConnecting ?? '') ? portFoundByConnecting : null,
    },
  });
};
const newPortConnected = (state: State, { port }: ActionsToPayloads[ActionTypes.NEW_PORT_CONNECTED]): State =>
  ({
    ...state,
    serialPort: {
      ...state.serialPort,
      portFoundByConnecting: port,
      selectedPort: port,
    },
  });

const showOnlyRecommendedPorts = (state: State, { recommend }: ActionsToPayloads[ActionTypes.SHOW_ONLY_RECOMMENDED_PORTS]): State =>
  ({
    ...state,
    serialPort: {
      ...state.serialPort,
      showOnlyRecommendedPorts: recommend,
    },
  });

const startMonitoring = (state: State): State => ({
  ...state,
  serialPort: {
    ...state.serialPort,
    ports: null,
    portFoundByConnecting: null,
  },
});
const stopMonitoring = (state: State): State => ({
  ...state,
  serialPort: {
    ...state.serialPort,
    ports: null,
    portFoundByConnecting: null,
  },
});
const changeStep = (state: State, { step }: ActionsToPayloads[ActionTypes.CHANGE_STEP]): State => ({
  ...state,
  step,
  ...(step === Steps.CHOOSE_CONNECTION_TYPE && { connectionType: null }),
  ...(step === Steps.CONFIGURE_TCP && { connectionType: ConnectionType.TCP_IP }),
  ...(step === Steps.CONFIGURE_SERIAL && { connectionType: ConnectionType.SERIAL_PORT }),
});

const changeTcp = (state: State, tcp: ActionsToPayloads[ActionTypes.CHANGE_TCP]): State => ({
  ...state,
  tcp: {
    ...state.tcp,
    ...tcp,
  },
});

const changeCloud = (state: State, { cloudId }: ActionsToPayloads[ActionTypes.CHANGE_CLOUD]): State => ({
  ...state,
  connectCloud: {
    ...state.connectCloud,
    cloudId,
  },
});

const changeDialogOpen = (state: State, { open }: ActionsToPayloads[ActionTypes.CHANGE_DIALOG_OPEN]): State =>
  open ? ({
    ...state,
    dialogOpen: true,
    step: environment.flavor === Flavors.Zaber ? Steps.CHOOSE_CONNECTION_TYPE : Steps.CONFIGURE_SERIAL,
  }) : ({
    ...initialState,
    hideSimulatorNotice: state.hideSimulatorNotice,
  });

const detectingDevices = (state: State): State => ({
  ...state,
  detectDevices: {
    detecting: true,
    detected: null,
    error: null,
  },
});
const detectingDevicesDone = (state: State, { devices, error }: ActionsToPayloads[ActionTypes.DETECT_DEVICES_DONE]): State =>
  state.detectDevices ? ({
    ...state,
    detectDevices: {
      detecting: false,
      detected: devices ?? null,
      error: error ?? null,
    },
  }) : state;

const connectToCloud = (state: State): State => ({
  ...state,
  connectCloud: {
    ...state.connectCloud,
    connecting: true,
    error: null,
  },
});

const connectToCloudErr = (state: State, { error }: ActionsToPayloads[ActionTypes.CONNECT_CLOUD_ERR]): State => ({
  ...state,
  connectCloud: {
    ...state.connectCloud,
    connecting: false,
    error,
  },
});

const connectToCloudClear = (state: State): State => ({
  ...state,
  connectCloud: initialState.connectCloud,
});

const localShareConnect = (state: State): State => ({
  ...state,
  localShare: {
    connecting: true,
    error: null,
  },
});

const localShareDone = (state: State, { error }: ActionsToPayloads[ActionTypes.CONNECT_LOCAL_SHARE_ERROR]): State => ({
  ...state,
  localShare: {
    connecting: false,
    error,
  },
});

const localShareClear = (state: State): State => ({
  ...state,
  localShare: initialState.localShare,
});

const changeTcpConnectionType = (state: State, { selectedConnectionType }:
  ActionsToPayloads[ActionTypes.CHANGE_TCP_CONNECTION_TYPE]): State => {
  const selectedCandidate = state.tcp.mdnsCandidates.find(candidate => candidate.hostname === state.tcp.hostname);
  const chainPort = selectedCandidate?.chainPort ?? MDNS_CHAIN_PORT;
  const singlePort = selectedCandidate?.singlePort ?? MDNS_SINGLE_PORT;
  let hostname = '';
  let port = null;
  if (selectedConnectionType === 'chain') {
    port = chainPort;
  } else if (selectedConnectionType === 'single') {
    port = singlePort;
  } else if (selectedConnectionType === 'simulator') {
    hostname = 'simulator.izaber.com';
  }

  return {
    ...state,
    tcp: {
      ...state.tcp,
      selectedConnectionType,
      hostname,
      port,
    },
  };
};

const mdnsDiscover = (state: State): State =>
  ({
    ...state,
    tcp: {
      ...state.tcp,
      mdnsCandidates: [],
      discovering: true,
      fetchingDeviceInfoError: null,
      error: null,
    },
  });

const mdnsDiscoveryDone = (state: State, { error }: ActionsToPayloads[ActionTypes.MDNS_DISCOVER_DONE]): State =>
  ({
    ...state,
    tcp: {
      ...state.tcp,
      discovering: false,
      error: error ?? null,
    },
  });

const mdnsClearCandidates = (state: State): State =>
  ({
    ...state,
    tcp: {
      ...state.tcp,
      mdnsCandidates: [],
    },
  });

const mdnsAddCandidate = (state: State, { candidate }: ActionsToPayloads[ActionTypes.MDNS_DISCOVER_ADD_CANDIDATE]): State => {
  const existingCandidate = state.tcp.mdnsCandidates.find(other => other.hostname === candidate.hostname);
  return {
    ...state,
    tcp: {
      ...state.tcp,
      mdnsCandidates: [
        ...state.tcp.mdnsCandidates.filter(other => other.hostname !== candidate.hostname),
        { ...existingCandidate, ...candidate },
      ],
    },
  };
};

const mdnsUpdateCandidateName = (state: State, { hostname, name }: ActionsToPayloads[ActionTypes.MDNS_UPDATE_CANDIDATE_NAME]): State => {
  const existingCandidate = state.tcp.mdnsCandidates.find(other => other.hostname === hostname);
  if (existingCandidate == null) {
    return state;
  }
  return {
    ...state,
    tcp: {
      ...state.tcp,
      mdnsCandidates: [
        ...state.tcp.mdnsCandidates.filter(other => other.hostname !== hostname),
        { ...existingCandidate, name },
      ],
    },
  };
};

const mdnsFetchDeviceInfoDoneErr = (state: State, { error }: ActionsToPayloads[ActionTypes.MDNS_FETCH_DEVICE_INFO_DONE_ERR]): State =>
  ({
    ...state,
    tcp: {
      ...state.tcp,
      fetchingDeviceInfoError: error,
    },
  });

const hideSimulatorNotice = (state: State): State => ({
  ...state,
  hideSimulatorNotice: true,
});

const firstDevicesLoad = (state: State) => ({
  ...state,
  lastAddedConnection: null,
});

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.ADD_CONNECTION]: addConnection,
  [ActionTypes.ADD_CONNECTION_DONE]: addConnectionDone,
  [ActionTypes.ADD_CONNECTION_ERR]: addConnectionErr,
  [ActionTypes.CHANGE_PORT]: changePort,
  [ActionTypes.CLEAR_PORT]: clearPort,
  [ActionTypes.CHANGE_CLOUD]: changeCloud,
  [ActionTypes.CHANGE_BAUD_RATE]: changeBaudRate,
  [ActionTypes.REMOVE_CONNECTION]: removeConnection,
  [ActionTypes.REMOVE_CONNECTION_DONE]: removeConnectionDone,
  [ActionTypes.PORTS_CHANGED]: portsChanged,
  [ActionTypes.NEW_PORT_CONNECTED]: newPortConnected,
  [ActionTypes.SHOW_ONLY_RECOMMENDED_PORTS]: showOnlyRecommendedPorts,
  [ActionTypes.START_MONITORING_PORTS]: startMonitoring,
  [ActionTypes.STOP_MONITORING_PORTS]: stopMonitoring,
  [ActionTypes.CHANGE_DIALOG_OPEN]: changeDialogOpen,
  [ActionTypes.CHANGE_STEP]: changeStep,
  [ActionTypes.CHANGE_TCP]: changeTcp,
  [ActionTypes.DETECT_DEVICES]: detectingDevices,
  [ActionTypes.DETECT_DEVICES_DONE]: detectingDevicesDone,
  [ActionTypes.CONNECT_CLOUD]: connectToCloud,
  [ActionTypes.CONNECT_CLOUD_ERR]: connectToCloudErr,
  [ActionTypes.CONNECT_CLOUD_CLEAR]: connectToCloudClear,
  [ActionTypes.CONNECT_LOCAL_SHARE]: localShareConnect,
  [ActionTypes.CONNECT_LOCAL_SHARE_ERROR]: localShareDone,
  [ActionTypes.CONNECT_LOCAL_SHARE_CLEAR]: localShareClear,
  [ActionTypes.CHANGE_TCP_CONNECTION_TYPE]: changeTcpConnectionType,
  [ActionTypes.MDNS_DISCOVER]: mdnsDiscover,
  [ActionTypes.MDNS_DISCOVER_CLEAR_CANDIDATES]: mdnsClearCandidates,
  [ActionTypes.MDNS_DISCOVER_ADD_CANDIDATE]: mdnsAddCandidate,
  [ActionTypes.MDNS_DISCOVER_DONE]: mdnsDiscoveryDone,
  [ActionTypes.MDNS_UPDATE_CANDIDATE_NAME]: mdnsUpdateCandidateName,
  [ActionTypes.MDNS_FETCH_DEVICE_INFO_DONE_ERR]: mdnsFetchDeviceInfoDoneErr,
  [ActionTypes.HIDE_SIMULATOR_NOTICE]: hideSimulatorNotice,
  [ActionTypes.FIRST_DEVICES_LOAD]: firstDevicesLoad,
}, initialState);
