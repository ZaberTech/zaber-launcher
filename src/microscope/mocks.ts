/* eslint-disable @typescript-eslint/no-this-alias */
import _ from 'lodash';
import { injectable } from 'inversify';

export const ADDRESSES = {
  filterChanger: 1,
  objectiveChanger: 2,
  focus: 3,
  illuminator: 4,
  xyStage: 5,
};
export const FOCUS_DATUM = 10000;
export function getFocusOffset(objective: number) {
  return objective * 1100;
}

export async function asyncNoop() { }

export let microscopeMock: MicroscopeMock;
let microscopeCallback: (microscope: MicroscopeMock) => void;
export function setMicroscopeCallback(callback: typeof microscopeCallback) {
  microscopeCallback = callback;
}

export class MicroscopeMock {
  constructor(public connection: ConnectionMock) {
    microscopeMock = this;
    if (microscopeCallback) { microscopeCallback(this) }
  }

  static async find(connection: ConnectionMock) {
    return new MicroscopeMock(connection);
  }

  isInitialized = jest.fn(async () => true);
  initialize = jest.fn(asyncNoop);

  currentFilter = 1;
  filterChanger = {
    getCurrentFilter: jest.fn(async () => this.currentFilter),
    getNumberOfFilters: jest.fn(async () => 4),
    change: jest.fn(async (filter: number) => this.currentFilter = filter),
    device: (() => {
      const device = this.connection.getDevice(ADDRESSES.filterChanger);
      device.getAxis(1).settings.canConvertNativeUnits.mockReturnValue(false);
      return device;
    })(),
  };
  currentObjective = 1;
  objectiveChanger = {
    parent: this,
    getCurrentObjective: jest.fn(async () => this.currentObjective),
    getNumberOfObjectives: jest.fn(async () => 4),
    turret: this.connection.getDevice(ADDRESSES.objectiveChanger),
    get focusAxis(): AxisMock { return this.parent.focusAxis },
    change: jest.fn(async (o: number, { focusOffset }: { focusOffset?: Measurement } = {}) => {
      this.currentObjective = o;
      await this.focusAxis.moveAbsolute(FOCUS_DATUM + (focusOffset?.value ?? 0));
    }),
    getFocusDatum: jest.fn(async () => FOCUS_DATUM),
  };

  illuminator = {
    device: (() => {
      const illuminator = new IlluminatorDeviceMock(ADDRESSES.illuminator, this.connection);
      this.connection.devices.set(ADDRESSES.illuminator, illuminator);
      return illuminator;
    })(),
    getChannel(channel: number) {
      return this.device.getAxis(channel) as unknown as AxisIlluminatorMock;
    }
  };

  xyDevice = this.connection.getDevice(ADDRESSES.xyStage);
  xAxis = new AxisMock(1, this.xyDevice);
  yAxis = new AxisMock(2, this.xyDevice);
  focusDevice = this.connection.getDevice(ADDRESSES.focus);
  focusAxis = new AxisMock(1, this.focusDevice);
  plate = {
    moveMin: jest.fn(asyncNoop),
    moveMax: jest.fn(asyncNoop),
    moveAbsolute: jest.fn(async (m1: Measurement, m2: Measurement) => {
      await this.xAxis.moveAbsolute(m1.value, m1.unit);
      await this.yAxis.moveAbsolute(m2.value, m1.unit);
    }),
    getPosition: jest.fn(async () => [0, 0]),
  };
  autofocus = new AutofocusMock(this.focusAxis);
  cameraTrigger = {
    device: this.connection.getDevice(ADDRESSES.focus),
    channel: 2,
    trigger: jest.fn(asyncNoop),
  };
}

export class WdiAutofocusProviderMock {
  static instance: WdiAutofocusProviderMock | null = null;
  static openTcp = jest.fn(async () => new WdiAutofocusProviderMock());

  currentObjective = 0;
  laserOn = true;

  constructor() {
    WdiAutofocusProviderMock.instance = this;
  }

  getStatus = jest.fn(async () => ({ laserOn: this.laserOn }));

  genericRead = jest.fn(async (register: number) => {
    switch (register) {
      case 50:
        return [this.currentObjective];
      default:
        throw new Error('Bad register');
    }
  });

  close = jest.fn(asyncNoop);

  enableLaser = jest.fn(asyncNoop);
  disableLaser = jest.fn(asyncNoop);
}

jest.mock('@zaber/motion', () => {
  const original = jest.requireActual('@zaber/motion');
  return {
    ...original,
    microscopy: {
      Microscope: MicroscopeMock,
      WdiAutofocusProvider: WdiAutofocusProviderMock,
    },
  };
});

import {
  CommandFailedException, CommandFailedExceptionData, Length, Measurement, NoValueForKeyException, Units, Velocity, ascii,
} from '@zaber/motion';

import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';

import { MocksDictionary } from '../test/dictionary';
import { convertBetweenUnits, getDimensionName } from '../units';
import { EMPTY_OBJECTIVE_PARAMETERS } from './types';
import { makeConnectionKey, makeRouterKey } from '../keys';
import { LOCAL_ROUTER_URL } from '../message_router';
import { MOCK_SERIAL_PORT } from '../connection_manager/mocks';

export function badCmd() {
  return new CommandFailedException('Rejected', {
    replyFlag: 'RJ',
    responseData: 'BADCOMMAND',
  } as CommandFailedExceptionData);
}

export function badData() {
  return new CommandFailedException('Rejected', {
    replyFlag: 'RJ',
    responseData: 'BADDATA',
  } as CommandFailedExceptionData);
}

const NATIVE_UNITS = {
  [getDimensionName(Length.MICROMETRES)!]: Length.MICROMETRES,
  [getDimensionName(Velocity.MICROMETRES_PER_SECOND)!]: Velocity.MICROMETRES_PER_SECOND,
};

class AxisMock extends AxisMockBase {
  axisType = ascii.AxisType.UNKNOWN;

  storage = {
    parent: this,
    async listKeys({ prefix }: { prefix: string }): Promise<string[]> {
      return _.range(2).map(i => `${prefix}${i + 1}`);
    },
    async getString(key: string): Promise<string> {
      if (key.includes('.label.')) {
        return `Special label ${key.replace(/.*label\./, '')}`;
      }
      throw new Error('No value');
    },
    async getNumber(key: string): Promise<number> {
      if (key.includes('.focus_offset.')) {
        return getFocusOffset(+key.replace(/.*focus_offset\./, ''));
      }
      throw new Error('No value');
    },
    getBool: jest.fn(async (key: string) => {
      if (key.includes('inverted')) {
        return false;
      }
      throw new Error('No value');
    }),
    keyExists: jest.fn(async () => true),
    eraseKey: jest.fn(asyncNoop),
    setString: jest.fn(asyncNoop),
    setNumber: jest.fn(asyncNoop),
  };

  settings = {
    values: {
      'limit.min': 1000,
      'limit.max': 10000,
      'motion.tracking.limit.min': 7000,
      'motion.tracking.limit.max': 8000,
      'maxspeed': 150,
    } as _.Dictionary<number>,

    convertFromNativeUnits(setting: string, value: number, units: Units) {
      const dimension = getDimensionName(units)!;
      return convertBetweenUnits(value, NATIVE_UNITS[dimension], units);
    },

    convertToNativeUnits(setting: string, value: number, units: Units) {
      const dimension = getDimensionName(units)!;
      return convertBetweenUnits(value, units, NATIVE_UNITS[dimension]);
    },

    canConvertNativeUnits: jest.fn(() => true),

    badSettingErr: badCmd,
    async get(setting: string) {
      if (!_.has(this.values, setting)) {
        throw this.badSettingErr();
      }

      return this.values[setting];
    },

    async set(setting: string, value: number) {
      if (!_.has(this.values, setting)) {
        throw this.badSettingErr();
      }

      this.values[setting] = value;
    },

    getMany(...settings: { setting: string; unit: string }[]) {
      return Promise.all(settings.map(async ({ setting, unit }) => {
        const value = await this.get(setting);
        return { setting, value, unit };
      }));
    },
  };

  _position = 0;
  getPosition = jest.fn(async (units: Length) => {
    if (!units) {
      return this._position;
    }
    const dimension = getDimensionName(units)!;
    return convertBetweenUnits(this._position, NATIVE_UNITS[dimension], units);
  });

  _isBusy = false;
  isBusy = jest.fn(async () => this._isBusy);

  home = jest.fn(asyncNoop);
  isHomed = jest.fn(async () => true);
  moveAbsolute = jest.fn(async (position: number, units?: Units | null) => {
    if (units != null && units !== '') {
      const dimension = getDimensionName(units)!;
      position = convertBetweenUnits(position, units, NATIVE_UNITS[dimension]);
    }
    this._position = position;
  });
  moveRelative = jest.fn(async (distance: number, units?: Units | null) => {
    if (units != null && units !== '') {
      const dimension = getDimensionName(units)!;
      distance = convertBetweenUnits(distance, units, NATIVE_UNITS[dimension]);
    }
    this._position += distance;
  });
  moveVelocity = jest.fn(asyncNoop);
  stop = jest.fn(asyncNoop);
  moveMax = jest.fn(async () => this._position = 10000);
  moveMin = jest.fn(async () => this._position = 0);
}

/** Serves as both axis and channel. */
class AxisIlluminatorMock extends AxisMockBase {
  axisType = ascii.AxisType.LAMP;
  identity = {
    peripheralName: '',
  };

  constructor(id: number, device: IlluminatorDeviceMock) {
    super(id, device);

    this.identity.peripheralName = `LED${id * 1000}`;
  }

  settings = {
    get: async (setting: string) => {
      if (setting === 'lamp.wavelength.peak') {
        return 1000 * this.axisNumber;
      }
      throw badCmd();
    },
  };

  _isOn = false;
  _intensity = 0;
  async isOn() {
    return this._isOn;
  }
  setOn = jest.fn(async (on: boolean) => { this._isOn = on });
  on = () => this.setOn(true);
  off = () => this.setOn(false);
  async getIntensity() {
    return this._intensity;
  }
  setIntensity = jest.fn(async (intensity: number) => { this._intensity = intensity });
}

class DeviceMock extends DeviceMockBase<AxisMock> {
  isIdentified = true;
  async identify() {
    this.isIdentified = true;
  }

  axisCount = 1;
  identity = {
    name: 'X-BLA',
  };

  warnings = {
    getFlags: jest.fn(async () => new Set()),
  };

  waitToRespond = jest.fn(asyncNoop);
  storage = new DeviceStorageMock();
  restore = jest.fn(asyncNoop);
}

const STORAGE_KEYS = ['zaber.foo', 'zaber.bar'];

class DeviceStorageMock {
  getNumber = jest.fn(async (_key: string): Promise<number> => {
    throw new NoValueForKeyException('No value');
  });
  setNumber = jest.fn(asyncNoop);
  listKeys = jest.fn(async () => (STORAGE_KEYS));
  eraseKey = jest.fn(async () => undefined);
}

class IlluminatorDeviceMock extends DeviceMock {
  axisCount = 2;
  identity = {
    name: 'X-LCA4',
  };

  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);

    this.axes = new MocksDictionary<AxisIlluminatorMock>(
      i => new AxisIlluminatorMock(i as number, this)
    ) as unknown as MocksDictionary<AxisMock>;
  }
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  detectDevices = jest.fn(async () =>
    _.range(4).map(i => {
      const device = new DeviceMock(i + 1, this);
      device.isIdentified = false;
      return device;
    }));

  genericCommandNoResponse = jest.fn(asyncNoop);
  genericCommandMultiResponse = jest.fn(async (): Promise<{ status: 'BUSY' | 'IDLE' }[]> => []);
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
export class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

class AutofocusMock {
  constructor(public focusAxis: AxisMock) { }

  status = { inFocus: true, inRange: true };

  getStatus = jest.fn(async () => this.status);

  getObjectiveParameters = jest.fn(async (_objective: number) =>
    _.toPairs(EMPTY_OBJECTIVE_PARAMETERS).map(([name, value]) => ({ name, value })));
  setObjectiveParameters = jest.fn(asyncNoop);
  setLimitMin = jest.fn((value: number) => this.focusAxis.settings.set('motion.tracking.limit.min', value));
  setLimitMax = jest.fn((value: number) => this.focusAxis.settings.set('motion.tracking.limit.max', value));

  focusOnce = jest.fn(asyncNoop);
  setFocusZero = jest.fn(asyncNoop);
  startFocusLoop = jest.fn(async () => {
    this.focusAxis._isBusy = true;
  });
  stopFocusLoop = jest.fn(async () => {
    this.focusAxis._isBusy = false;
  });
}

afterEach(() => {
  microscopeCallback = null!;
  microscopeMock = null!;

  WdiAutofocusProviderMock.instance = null;
});

export const ROUTER_KEY = makeRouterKey(LOCAL_ROUTER_URL);
export const CONNECTION_KEY = makeConnectionKey(ROUTER_KEY, MOCK_SERIAL_PORT);
