import React from 'react';
import { Text } from '@zaber/react-library';

import LogoMotto from '../assets/zaber_logo_motto.svg';
import { UpdateCheck } from '../updates';
import { ExternalLink, ContentHeader, InternalOnlyWarning } from '../components';
import { environment, Flavors } from '../environment';

export const About: React.FC = () => (<div className="about-page">
  <InternalOnlyWarning/>
  <ContentHeader>About</ContentHeader>

  <div className="content">
    <UpdateCheck/>
    {environment.flavor === Flavors.Zaber && <div className="info">
      <LogoMotto/>
      <div><Text>Zaber Technologies Inc.</Text></div>
      <div><ExternalLink url="https://www.zaber.com">www.zaber.com</ExternalLink></div>
      <div>
          Repository:&nbsp;
        <ExternalLink url="https://gitlab.com/ZaberTech/zaber-launcher">gitlab.com/ZaberTech/zaber-launcher</ExternalLink>
      </div>
      <div>
        <ExternalLink url="https://gitlab.com/ZaberTech/zaber-launcher/-/blob/master/CHANGELOG.md">Changelog</ExternalLink>
      </div>
      <div>
        <ExternalLink url="https://gitlab.com/ZaberTech/zaber-launcher/-/blob/master/LICENSE.txt">License MIT</ExternalLink>
      </div>
    </div>}
  </div>

  <div className="spacer"/>
  {environment.flavor === Flavors.Zaber && <div className="copyright">
    <Text e={Text.Emphasis.Light}>© {new Date().getFullYear()} Zaber Technologies Inc.</Text>
  </div>}
  {environment.flavor !== Flavors.Zaber && <div className="copyright">
    <ExternalLink url="https://gitlab.com/ZaberTech/zaber-launcher/-/blob/master/LICENSE.txt">License / Copyright</ExternalLink>
  </div>}
</div>);
