import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectConnectionManager } from '../store';

import type { RouterState, DeviceState, AxisState } from './reducer';
import { RouterType, RouterTypeOrder } from './types';
import { isInternalDevice } from './utils';

export interface IdentifiedDeviceState extends DeviceState  {
  identity: NonNullable<DeviceState['identity']>;
  product: NonNullable<DeviceState['product']>;
  locksteps: NonNullable<DeviceState['locksteps']>;
  nonIdentifiedInfo: null;
}
export function isDeviceStateIdentified(device: DeviceState): device is IdentifiedDeviceState {
  return device.identity != null;
}
export interface IdentifiedAxisState extends AxisState  {
  identity: NonNullable<AxisState['identity']>;
  product: NonNullable<AxisState['product']>;
  nonIdentifiedInfo: null;
}
export function isAxisStateIdentified(axis: AxisState): axis is IdentifiedAxisState {
  return axis.identity != null;
}

export const selectLocalRouter = createSelector(selectConnectionManager,
  state => _.find(state.routers, router => router.type === RouterType.Local)
);
export const selectLoadingRouters = createSelector(selectConnectionManager, state => state.loadingRouters);
export const selectDevices = createSelector(selectConnectionManager, state => state.devices);
export const selectIdentifiedDevices = createSelector(selectConnectionManager, state => _.pickBy(state.devices, isDeviceStateIdentified));
export const selectDevicesWithAxes = createSelector(selectConnectionManager, state => _.mapValues(state.devices, device => ({
  ...device,
  axes: device.axes.map(axis => state.axes[axis]),
})));
export const selectAxes = createSelector(selectConnectionManager, state => state.axes);
export const selectIdentifiedAxes = createSelector(selectConnectionManager,  state => _.pickBy(state.axes, isAxisStateIdentified));
export const selectConnections = createSelector(selectConnectionManager, state => state.connections);
export const selectLocalConnections = createSelector(selectConnectionManager, selectLocalRouter,
  (state, localRouter) => _.pick(state.connections, ...(localRouter?.connections ?? [])));
export const selectRouters = createSelector(selectConnectionManager, state => state.routers);

function compareRouters(a: RouterState, b: RouterState): number {
  if (a.type !== b.type) {
    return RouterTypeOrder[a.type] - RouterTypeOrder[b.type];
  }
  return a.url.localeCompare(b.url);
}
export const selectRoutersArray = createSelector(selectConnectionManager, state => _.toArray(state.routers).sort(compareRouters));

export const selectAxesKeysInDisplayOrder = createSelector(
  selectRoutersArray, selectConnections, selectDevices,
  (routers, connections, devices) => _.flatMap(routers,
    router => _.flatMap(router.connections,
      connectionKey => _.flatMap(connections[connectionKey].devices,
        deviceKey => devices[deviceKey].axes
      )
    )
  )
);

export const selectHasConnections = createSelector(selectConnectionManager, state =>
  Object.keys(state.connections).length > 0
  || _.some(state.routers, router => router.connectionsErr != null),
);

export const selectHasConnectionsToManage = createSelector(selectConnectionManager, selectHasConnections, (state, hasConnections) =>
  Object.keys(state.routers).length > 1 || hasConnections,
);

export const selectAllRoutersLoading = createSelector(selectConnectionManager,
  state => state.loadingRouters || _.every(state.routers, router => router.loadingConnections)
);

export const selectThumbnails = createSelector(selectConnectionManager, state => state.thumbnails);

export const selectHasInternalDevices = createSelector(selectConnectionManager,
  state => _.chain(state.devices).values().some(isInternalDevice).value());

export const selectAllEntities = createSelector(selectRouters, selectConnections, selectDevices, selectAxes,
  (routers, connections, devices, axes) => ({
    ...routers,
    ...connections,
    ...devices,
    ...axes,
  })
);
