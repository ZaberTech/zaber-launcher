import React from 'react';
import { useSelector } from 'react-redux';

import { ConnectionsView } from '../connection_manager';
import { NoContentMessage, Title } from '../components';
import { useActions } from '../utils';

import { actionDefinitions } from './actions';
import { ChooseParamset } from './ChooseParamset';
import { selectChosen, selectEntity, selectMessage } from './selectors';
import { ServoTuningModal } from './components/ServoTuningModal';
import { TITLE } from './types';

const AppUi: React.FC = () => {
  const entity = useSelector(selectEntity);
  const chosen = useSelector(selectChosen);
  const message = useSelector(selectMessage);

  if (entity == null) {
    return <NoContentMessage title={TITLE} message="Select a device or peripheral to tune."/>;
  }
  if (message?.type === 'top-error' || message?.type === 'top-note') {
    return <NoContentMessage title={TITLE} message={message.message} type={message.type === 'top-error' ? 'error' : 'info'}/>;
  }
  if (chosen == null) {
    return <NoContentMessage title={TITLE} message="Loading..." type="working"/>;
  }
  return <ChooseParamset {...{ entity, ...chosen }}/>;
};

export const App: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const entity = useSelector(selectEntity);

  return <div className="connection-view-and-app">
    <Title>Servo Tuner</Title>
    <ConnectionsView
      selectable={['axis', 'integrated']}
      selected={entity}
      onSelect={actions.chooseEntity}
    />
    <div className="app-ui servo-tuner">
      <AppUi/>
      <ServoTuningModal/>
    </div>
  </div>;
};
