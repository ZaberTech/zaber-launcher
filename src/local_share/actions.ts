import type { LocalShareConfig, LocalShareStatus } from '../app_components';
import { actionBuilder } from '../utils';

import type { LocalShareCandidate } from './types';

export enum ActionTypes {
  UPDATE_CONFIG = 'LOCAL_SHARE_UPDATE_CONFIG',
  UPDATE_CONFIG_DONE = 'LOCAL_SHARE_UPDATE_CONFIG_DONE',
  GET_LOCAL_SHARE_CONFIG = 'LOCAL_SHARE_GET_CONFIG',
  GET_LOCAL_SHARE_CONFIG_RESULT = 'LOCAL_SHARE_GET_CONFIG_RESULT',
  LOCAL_SHARE_STATUS = 'LOCAL_SHARE_GET_STATUS',
  LOCAL_SHARE_STATUS_RESULT = 'LOCAL_SHARE_STATUS_RESULT',
  TOGGLE_SHARING_WARNING = 'TOGGLE_SHARING_WARNING',

  DISCOVER = 'LOCAL_SHARE_DISCOVER',
  DISCOVER_CLEAR_CANDIDATES = 'LOCAL_SHARE_DISCOVER_SET_CANDIDATES',
  DISCOVER_ADD_CANDIDATE = 'LOCAL_SHARE_DISCOVER_ADD_CANDIDATE',
  DISCOVER_DONE = 'LOCAL_SHARE_DISCOVER_DONE',
  DISCOVER_CANCEL = 'LOCAL_SHARE_DISCOVER_CANCEL',
}

export interface ActionsToPayloads {
  [ActionTypes.UPDATE_CONFIG]: { configUpdate: Partial<LocalShareConfig> };
  [ActionTypes.UPDATE_CONFIG_DONE]: { err?: string };
  [ActionTypes.GET_LOCAL_SHARE_CONFIG]: void;
  [ActionTypes.GET_LOCAL_SHARE_CONFIG_RESULT]: { config?: LocalShareConfig; err?: string };
  [ActionTypes.LOCAL_SHARE_STATUS]: void;
  [ActionTypes.LOCAL_SHARE_STATUS_RESULT]: { status?: LocalShareStatus; err?: string };
  [ActionTypes.TOGGLE_SHARING_WARNING]: { on: boolean };

  [ActionTypes.DISCOVER]: void;
  [ActionTypes.DISCOVER_CLEAR_CANDIDATES]: void;
  [ActionTypes.DISCOVER_ADD_CANDIDATE]: { candidate: LocalShareCandidate };
  [ActionTypes.DISCOVER_DONE]: { error?: string };
  [ActionTypes.DISCOVER_CANCEL]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  updateConfig: (configUpdate: Partial<LocalShareConfig>) => buildAction(ActionTypes.UPDATE_CONFIG, { configUpdate }),
  updateConfigDone: (err?: string) => buildAction(ActionTypes.UPDATE_CONFIG_DONE, { err }),
  getCurrentConfig: () => buildAction(ActionTypes.GET_LOCAL_SHARE_CONFIG),
  localShareConfigResult: (config: LocalShareConfig) => buildAction(ActionTypes.GET_LOCAL_SHARE_CONFIG_RESULT, { config }),
  localShareConfigResultErr: (err: string) => buildAction(ActionTypes.GET_LOCAL_SHARE_CONFIG_RESULT, { err }),
  getCurrentStatus: () => buildAction(ActionTypes.LOCAL_SHARE_STATUS),
  localShareStatusResult: (status: LocalShareStatus) => buildAction(ActionTypes.LOCAL_SHARE_STATUS_RESULT, { status }),
  localShareStatusResultErr: (err: string) => buildAction(ActionTypes.LOCAL_SHARE_STATUS_RESULT, { err }),
  toggleSharingWarning: (on: boolean) => buildAction(ActionTypes.TOGGLE_SHARING_WARNING, { on }),

  discover: () => buildAction(ActionTypes.DISCOVER),
  discoverClearCandidates: () => buildAction(ActionTypes.DISCOVER_CLEAR_CANDIDATES),
  discoverAddCandidate: (candidate: LocalShareCandidate) =>
    buildAction(ActionTypes.DISCOVER_ADD_CANDIDATE, { candidate }),
  discoverDone: (error?: string) => buildAction(ActionTypes.DISCOVER_DONE, { error }),
  discoverCancel: () => buildAction(ActionTypes.DISCOVER_CANCEL),
};
