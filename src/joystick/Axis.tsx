import { NumericInput, Text, SimpleSelect } from '@zaber/react-library';
import _ from 'lodash';
import React, { useEffect, useState }  from 'react';
import { useSelector } from 'react-redux';

import type { EntityKey } from '../keys';
import { useActions, notNil } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectAxes, selectChainInfo, selectConfig, selectDevices, selectManualMode, selectProductionMode } from './selectors';
import { AxisChainInfo, AxisConfig, AXIS_NAMES, SpeedProfile, TargetAxisType, TargetAxisTypeLabels } from './types';

function getMaxspeedPercent(axis: AxisChainInfo | null, maxspeedNative: number | null) {
  if (axis == null || maxspeedNative == null) {
    return 100;
  }
  return _.round(maxspeedNative / axis.maxspeedNative * 100, 1);
}

function getMaxSpeedNative(axis: AxisChainInfo, maxspeedPercent: number | null) {
  return _.round(axis.maxspeedNative * (maxspeedPercent ?? 0) / 100);
}

export const Axis: React.FC<{ axisNumber: number }> = ({ axisNumber }) => {
  const actions = useActions(actionsDefinition);
  const updateConfig = (config: Partial<AxisConfig>) => actions.updateAxisConfig(axisNumber, config);

  const config = useSelector(selectConfig).axes[axisNumber - 1];
  const chainInfo = useSelector(selectChainInfo);
  const manualMode = useSelector(selectManualMode);
  const productionMode = useSelector(selectProductionMode);
  const devices = useSelector(selectDevices);
  const axes = useSelector(selectAxes);
  const availableAxes = _.values(axes).filter(axis => axis.device.key === config.device?.key);

  const isUnknownDevice = config.device == null && config.targetDevice !== chainInfo.nonExistentDeviceAddress;
  const isUnknownAxis = config.axis == null && config.targetAxis !== 0;

  const [maxspeed, setMaxspeed] = useState<number | null>(() => getMaxspeedPercent(config.axis, config.maxspeedNative));

  useEffect(() => {
    const syncMaxspeed = getMaxspeedPercent(config.axis, config.maxspeedNative);
    if (config.axis != null && maxspeed !== syncMaxspeed) {
      setMaxspeed(syncMaxspeed);
    }
  }, [config.maxspeedNative, config.axis]);

  const onAxisChange = (newAxisKey: EntityKey) => {
    if (!newAxisKey) { return }
    const newAxis = axes[newAxisKey];
    let update: Partial<AxisConfig>;
    if (newAxis.lockstep) {
      update = {
        axisType: 'lockstep',
        targetAxis: newAxis.lockstep.groupNumber,
      };
    } else {
      update = {
        axisType: 'axis',
        targetAxis: newAxis.axisNumber,
      };
    }
    if (!productionMode) {
      update.maxspeedNative = getMaxSpeedNative(newAxis, maxspeed);
    }
    updateConfig(update);
  };

  const onDeviceChange = (newAddress: number) => {
    if (newAddress === chainInfo.nonExistentDeviceAddress) {
      const update: Partial<AxisConfig> = {
        targetDevice: newAddress,
        axisType: 'axis',
        targetAxis: 0,
      };
      if (!productionMode) {
        update.maxspeedNative = 0;
      }
      updateConfig(update);
      return;
    }

    const newDevice = _.find(devices, device => device.address === newAddress);
    if (newDevice == null) { return }

    const update: Partial<AxisConfig> = {
      targetDevice: newDevice.address,
      axisType: 'axis',
    };
    if (newDevice.axisCount === 1) {
      const newAxis = axes[newDevice.axes[0]];
      update.targetAxis = 1;
      if (!productionMode) {
        update.maxspeedNative = getMaxSpeedNative(newAxis, maxspeed);
      }
    } else {
      update.targetAxis = 0;
      if (!productionMode) {
        update.maxspeedNative = 0;
      }
    }
    updateConfig(update);
  };

  const onSetMaxspeed = (newValue: number | null) => {
    setMaxspeed(newValue);
    if (config.axis != null && newValue != null) {
      updateConfig({
        maxspeedNative: getMaxSpeedNative(config.axis, newValue),
      });
    }
  };

  const disable = config.targetDevice == null || config.targetDevice === chainInfo.nonExistentDeviceAddress;

  return (<>
    <Text t={Text.Type.H5} className="axis-name">Axis {AXIS_NAMES[axisNumber - 1]}</Text>

    {!manualMode && <>
      <SimpleSelect
        className="device-select" data-testid={`device-select-${axisNumber}`}
        value={config.targetDevice ?? chainInfo.nonExistentDeviceAddress}
        onValueChange={onDeviceChange}
        options={[
          { value: chainInfo.nonExistentDeviceAddress, label: 'Disabled' },
          ..._.values(devices).map(device => ({
            value: device.address,
            label: `${device.address} ${device.label ?? device.identity.name}`,
          })),
          isUnknownDevice ? ({ value: config.targetDevice ?? 0, label: `Unknown Device ${config.targetDevice}` }) : null,
        ].filter(notNil)}/>
      <SimpleSelect
        className="axis-select" data-testid={`axis-select-${axisNumber}`} disabled={disable}
        value={config.axis?.key ?? ''}
        onValueChange={onAxisChange}
        options={[
          ...availableAxes.map(axis => ({
            value: axis.key,
            label: `Axis ${axis.axisNumber} ${axis.label ?? axis.identity.peripheralName}`,
          })),
          isUnknownAxis ? { value: '', label: 'Unknown Axis' } : null,
        ].filter(notNil)}/>
    </>}

    {manualMode && <>
      <NumericInput className="device-num" data-testid={`device-num-${axisNumber}`}
        value={config.targetDevice}
        onNumberChange={targetDevice => updateConfig({ targetDevice })}/>
      <SimpleSelect
        className="axis-type-select" data-testid={`axis-type-${axisNumber}`} disabled={disable}
        value={config.axisType}
        onValueChange={axisType => updateConfig({ axisType })}
        options={_.map(TargetAxisTypeLabels, (label, key) => ({ value: key as TargetAxisType, label }))}/>
      <NumericInput className="axis-num" data-testid={`axis-num-${axisNumber}`} disabled={disable}
        value={config.targetAxis}
        onNumberChange={targetAxis => updateConfig({ targetAxis })}/>
    </>}

    {(!manualMode && !productionMode) ?
      <NumericInput className="maxspeed" data-testid={`maxspeed-${axisNumber}`} disabled={disable}
        value={maxspeed} onNumberChange={onSetMaxspeed}/> :
      <NumericInput className="maxspeed-native" data-testid={`maxspeed-${axisNumber}`} disabled={disable}
        value={config.maxspeedNative}
        onNumberChange={maxspeedNative => {
          updateConfig({ maxspeedNative });
          setMaxspeed(getMaxspeedPercent(config.axis, maxspeedNative));
        }}/>}

    <SimpleSelect
      className="speed-profile-select" data-testid={`speed-profile-${axisNumber}`} disabled={disable}
      value={config.profile}
      onValueChange={profile => updateConfig({ profile })}
      options={_.chain(SpeedProfile)
        .pickBy(value => typeof value === 'number')
        .map((value, label) => ({ value, label }))
        .value()}/>
    <SimpleSelect
      className="inverted-select" data-testid={`inverted-${axisNumber}`} disabled={disable}
      value={Number(config.inverted)}
      onValueChange={inverted => updateConfig({ inverted: Boolean(inverted) })}
      options={[
        { value: 0, label: 'No' },
        { value: 1, label: 'Yes' },
      ]}/>

    {(manualMode || productionMode) && <NumericInput className="resolution" data-testid={`resolution-${axisNumber}`} disabled={disable}
      value={config.resolution} displayPrecision={0}
      onNumberChange={resolution => updateConfig({ resolution })}/>}
  </>);
};
