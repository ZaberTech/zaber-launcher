import React, { memo } from 'react';
import { HeaderCard, Icons, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';
import { match } from 'ts-pattern';

import { isMatch, useActions } from '../../../utils';
import { commandExists } from '../../../devices';
import { actionDefinitions } from '../actions';
import { Moveable, moveableKey } from '../../types';
import { selectIdentifiedAxes, selectIdentifiedDevices } from '../../../connection_manager';
import type { AxisCommand, AxisMovementState } from '../types';
import { extractDeviceKey } from '../../../keys';
import type { MeasurementOK } from '../../../units';

import { PreciseMovementControl } from './PreciseMovementControl';
import { ShortCutDropDown } from './KeyboardShortcutControl';

export const preciseCommand = (type: 'abs' | 'rel' | 'vel', target: MeasurementOK, direction: 'home' | 'away' = 'away') => (
  match<'abs' | 'rel' | 'vel', AxisCommand>(type)
    .with('abs', type => ({ type, initiator: 'precise', position: target.value, units: target.units }))
    .with('rel', type => ({ type, initiator: 'precise', direction, distance: target.value, units: target.units }))
    .with('vel', type => ({ type, initiator: 'precise', direction, velocity: target.value, units: target.units }))
    .exhaustive()
);

const PreciseMovementImpl: React.FC<{moveable: Moveable; state: AxisMovementState}> = ({ moveable, state }) => {
  const key = moveableKey(moveable);
  const axis = useSelector(selectIdentifiedAxes)[key];
  const device = useSelector(selectIdentifiedDevices)[extractDeviceKey(key)];

  const actions = useActions(actionDefinitions);
  const fwVersion = device.identity.firmwareVersion;
  const cyclable = commandExists('move rel ? ?', axis);

  const cycleTip =
  fwVersion.major < 7 ? 'Cycling is not supported with Firmware 6' :
  fwVersion.minor < 25 ? 'Please update Firmware in order to use the cycling feature' :
  'Cycle at specified velocity';

  return (
    <HeaderCard
      className="subsection precise-movements" edge="none"
      collapsible expanded={state.preciseMovementExpanded}
      onToggle={() => actions.expandPreciseMovement(key, !state.preciseMovementExpanded)}
      titleExpanded="Collapse Precise Movement section"
      titleCollapsed="Expand Precise Movement section"
      header={<Text t={Text.Type.H5}>Precise Movement</Text>}
    >
      <PreciseMovementControl
        axis={axis}
        state={state}
        setting="pos"
        title="Absolute Position Control"
        label={{
          label: 'Move to Absolute Position',
          help: <Text>
              Moves the stage to a specific position relative to the stage's zero.
          </Text>
        }}
        controls={t => (
          <Icons.RightNormal
            title="Move to Absolute Position"
            activated={isMatch({ type: 'abs', initiator: 'precise' }, state.lastCommand)}
            onClick={t ? () => actions.issueCommand(moveable, preciseCommand('abs', t)) : 'disabled'}
          />
        )}
        onEnterPress={t => actions.issueCommand(moveable, preciseCommand('abs', t))}
        limits={moveable.motion}
        movementType="absolute"
      />
      <PreciseMovementControl
        axis={axis}
        state={state}
        setting="pos"
        title="Relative Movement Control"
        label={{
          label: 'Move by Relative Distance',
          help: <Text>
              Moves the stage relative to its current location by the amount you enter, either forward or backward.
          </Text>
        }}
        controls={t => <>
          <Icons.LeftNormal
            title="Move by Relative Distance towards Beginning"
            activated={isMatch({ type: 'rel', initiator: 'precise', direction: 'home' }, state.lastCommand)}
            onClick={t ? () => actions.issueCommand(moveable, preciseCommand('rel', t, 'home')) : 'disabled'}
          />
          <Icons.RightNormal
            title="Move by Relative Distance towards End"
            activated={isMatch({ type: 'rel', initiator: 'precise', direction: 'away' }, state.lastCommand)}
            onClick={t ? () => actions.issueCommand(moveable, preciseCommand('rel', t)) : 'disabled'}
          />
        </>}
        onEnterPress={t => actions.issueCommand(moveable, preciseCommand('rel', t))}
        movementType="relative"
      />
      <PreciseMovementControl
        axis={axis}
        state={state}
        setting="maxspeed"
        title="Move at Velocity Control"
        label={{
          label: 'Move at Velocity',
          help: <>
            <Text>
                Starts the stage moving at the velocity you enter, in either direction.
                This does not change the device's <Text f={Text.Family.Mono}>maxspeed</Text> setting.</Text>
            <br/><br/>
            <Text>
                The cycle button will cause the stage to move back and forth at the velocity you enter until stopped.
            </Text>
          </>
        }}
        controls={t => <>
          <Icons.LeftNormal
            title="Move at Velocity towards Beginning"
            activated={isMatch({ type: 'vel', initiator: 'precise', direction: 'home' }, state.lastCommand)}
            onClick={t ? () => actions.issueCommand(moveable, preciseCommand('vel', t, 'home')) : 'disabled'}
          />
          <Icons.RightNormal
            title="Move at Velocity towards End"
            activated={isMatch({ type: 'vel', initiator: 'precise', direction: 'away' }, state.lastCommand)}
            onClick={t ? () => actions.issueCommand(moveable, preciseCommand('vel', t, 'away')) : 'disabled'}
          />
          <Icons.CycleVelocity
            title={cycleTip}
            activated={isMatch({ type: 'cycle' }, state.lastCommand)}
            disabled={!cyclable}
            onClick={t ? () => actions.issueCommand(moveable, { type: 'cycle', velocity: t.value, units: t.units }) : 'disabled'}
          />
        </>}
        onEnterPress={t => actions.issueCommand(moveable, preciseCommand('vel', t))}
        movementType="velocity"
        defaultValue={moveable.motion.maxSpeed}
      />
      <ShortCutDropDown axis={axis} shortcut={state.shortcut}/>
    </HeaderCard>
  );
};

export const PreciseMovement = memo(PreciseMovementImpl);
