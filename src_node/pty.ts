import os from 'os';
import childProcess from 'child_process';
import util from 'util';

import _ from 'lodash';
import { WebContents, ipcMain, IpcMainEvent } from 'electron';
import { inject, injectable } from 'inversify';
import { Log, Logger } from '@zaber/app-components/lib/log';
import type nodePty from 'node-pty';
import { ensureArray, makeString } from '@zaber/toolbox';

import type { Action } from '../src/utils';
import {
  actions, ActionTypes, ActionsToPayloads, TransmittedData, PROCESS_RX_IPC_CHANNEL, PROCESS_TX_IPC_CHANNEL,
  DEFAULT_TERMINAL_COLUMNS, DEFAULT_TERMINAL_ROWS, FAILED_EXIT_CODE,
} from '../src/pty/common_types';
import { environment } from '../src/environment';

import { Ipc, ReceivedAction } from './ipc';

const exec = util.promisify(childProcess.exec);

type SpawnAction = Action<ActionsToPayloads[ActionTypes.SPAWN]>;
type KillAction = Action<ActionsToPayloads[ActionTypes.KILL]>;

interface Child {
  id: string;
  pty: nodePty.IPty;
  parent: WebContents;
  killed: boolean;
  destroyedCallback: () => void;
}

@injectable()
export class Pty {
  private logger: Logger;
  private nodePty: typeof nodePty | null = null;
  private readonly children = new Map<string, Child>();

  constructor(
    @inject(Ipc) private readonly ipc: Ipc,
    @inject(Log) log: Log,
  ) {
    this.logger = log.getLogger('Pty');

    this.ipc.getActions<SpawnAction>(ActionTypes.SPAWN).subscribe(this.onSpawn.bind(this));
    this.ipc.getActions<KillAction>(ActionTypes.KILL).subscribe(this.onKill.bind(this));

    ipcMain.on(PROCESS_TX_IPC_CHANNEL, this.onWrite.bind(this));
  }

  private onProcessData(child: Child, data: string) {
    const rx: TransmittedData = { id: child.id, data };
    if (child.parent.isDestroyed()) { return }
    child.parent.send(PROCESS_RX_IPC_CHANNEL, rx);
  }

  private onProcessExit(child: Child, exitCode: number, signal: number | null) {
    if (signal === 0) { signal = null } // for some reason 0 is received on UNIX

    this.logger.info(`Exit ${child.id} code: ${exitCode} signal: ${signal}`);

    this.children.delete(child.id);
    child.parent.off('destroyed', child.destroyedCallback);

    this.ipc.sendAction(child.parent, actions.exit(child.id, exitCode, signal));
  }

  private onSpawn({ action: { payload: { options } }, sender: parent }: ReceivedAction<SpawnAction>) {
    this.logger.info(`Spawning ${options.id}: ${options.cmd} ${ensureArray(options.args).join(' ')}`);

    if (this.children.get(options.id)) {
      const err = `Process with ID ${options.id} is still running.`;
      this.logger.error(err);
      throw new Error(err);
    }

    let pty: nodePty.IPty;
    try {
      pty = this.requirePty().spawn(options.cmd, options.args, {
        cwd: options.cwd,
        env: {
          ...(process.env as Record<string, string>),
          ...options.env,
        },
        cols: options.cols ?? DEFAULT_TERMINAL_COLUMNS,
        rows: options.rows ?? DEFAULT_TERMINAL_ROWS,
      });
    } catch (err) {
      this.logger.warn(`Cannot spawn ${options.id}: ${makeString(err)}`);
      this.ipc.sendAction(parent, actions.exit(options.id, FAILED_EXIT_CODE, null));
      return;
    }

    const child: Child = {
      id: options.id,
      pty,
      parent,
      killed: false,
      destroyedCallback: () => { this.kill(child).catch(_.noop) },
    };
    this.children.set(child.id, child);

    parent.once('destroyed', child.destroyedCallback);

    pty.onData(data => this.onProcessData(child, data));
    pty.onExit(({ exitCode, signal }) => this.onProcessExit(child, exitCode, signal ?? null));
  }

  private onKill({ action: { payload: { id } } }: ReceivedAction<KillAction>) {
    const child = this.children.get(id);
    if (child == null) { return }
    this.kill(child).catch(_.noop);
  }

  private async kill(child: Child) {
    if (child.killed) { return }
    child.killed = true;

    this.logger.info(`Killing ${child.id}.`);
    try {
      // child.pty.kill() often results in Electron hanging up.
      if (os.platform() === 'win32' && !environment.isTest) {
        // process.kill is unable to kill the entire process group on Windows.
        await exec(`taskkill /PID ${child.pty.pid} /T /F`);
      } else {
        process.kill(child.pty.pid);
      }
    } catch (err) {
      this.logger.warn(`Error killing ${child.id}: ${makeString(err)}`);
    }
  }

  private onWrite(event: IpcMainEvent, { id, data }: TransmittedData) {
    this.children.get(id)?.pty.write(data);
  }

  private requirePty() {
    // For some reason node-pty does not import properly, it must be required.
    // It also causes troubles with dependencies. For example, the official build happens on Ubuntu 22
    // which locks glibc version requirement and causes crashes on Ubuntu 20.
    // We require it dynamically to minimize the trouble.
    if (this.nodePty == null) {
      // eslint-disable-next-line @typescript-eslint/no-require-imports
      this.nodePty = require('node-pty');
    }
    return this.nodePty!;
  }
}
