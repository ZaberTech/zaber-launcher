import classNames from 'classnames';
import _ from 'lodash';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Categories, categoriesMetadata, settingsMetadata } from '@zaber/device-db-metadata';
import { Button, Checkbox, ColorPicker, HeaderCard, Icons, Input, MinorMenu, Modal, PopUp, Text } from '@zaber/react-library';
import { changeCollection } from '@zaber/toolbox';

import { AppIconsNeutral } from '../apps';

import { ADD_SETTING_BUTTON_LABEL, Channel, ChannelListMap, MAX_DEVICE_CHANNELS_TOOLTIP } from './types';
import { selectSelectedDeviceState } from './selectors';
import {
  createChannelForSetting,
  extractAxisChannels,
  extractSettingsChannels,
  makeSettingChannelId,
  MostUsedSettings,
  sortAlphabetic,
  sortByRelevance
} from './channels';
import { getDefaultColor, UNSET_COLOR } from './colors';


const groupSettings = function(channels: Channel[], showAll: boolean): [ChannelListMap, string[]] {
  const groups: ChannelListMap = {};

  for (const channel of channels) {
    let category: Categories | null = settingsMetadata[channel.name]?.category ?? null;
    const categories: string[] = [];
    while (category) {
      categories.push(categoriesMetadata[category].humanName);
      category = categoriesMetadata[category].parent ?? null;
    }

    const groupName = categories.reverse().join(' / ') || 'Uncategorized';

    if (!(groupName in groups)) {
      groups[groupName] = [];
    }

    groups[groupName].push(channel);
  }

  for (const key of Object.keys(groups)) {
    groups[key] = groups[key].sort((a, b) => showAll ? sortAlphabetic(a.name, b.name) : sortByRelevance(a.name, b.name));
  }

  const groupNames = Array.from(Object.keys(groups));
  const sortedNames = showAll ?
    groupNames.toSorted(sortAlphabetic) :
    groupNames.toSorted((a, b) => sortByRelevance(groups[a][0].name, groups[b][0].name));
  return [groups, sortedNames];
};


interface PillProps {
  label: string;
  onClickIcon: () => void;
}

const Pill: React.FC<PillProps> = ({ label, onClickIcon }) => <div className="pill">
  <Text data-testid="pill-label">{label}</Text>
  <Icons.Cross onClick={onClickIcon}/>
</div>;


interface RowProps {
  channel: Channel;
  canSelect: boolean;
  setVisible: (visible: boolean) => void;
  setColor: (color: string) => void;
}


const SettingRow: React.FC<RowProps> = ({ channel, canSelect, setVisible, setColor }) => <>
  <PopUp position="top"
    triggerAction={!canSelect && !channel.showInUi ? 'hover' : 'none'}
    trigger={() =>   <Checkbox checked={channel.showInUi}
      className="visibility"
      disabled={!canSelect && !channel.showInUi}
      data-testid={`dlg-show-channel ${channel.id}`}
      onChecked={() => setVisible(!channel.showInUi)}/>}>
    {MAX_DEVICE_CHANNELS_TOOLTIP}
  </PopUp>
  {channel.showInUi ?
    <ColorPicker color={channel.color} onChange={setColor}/>
    : <div className="color-picker-placeholder"/>}
  <div className="label">
    <Text t={Text.Type.Body}>{channel.title}</Text><br/>
    <Text t={Text.Type.BodySm} data-testid={`row-subtitle ${channel.subtitle}`}>{channel.subtitle}</Text>
  </div>
</>;


interface Props {
  dialogOpen: boolean;
  axisNames: string[];
  apply: (newSelections: Channel[]) => void;
  cancel: () => void;
}


export const DataSourceDialog: React.FC<Props> = ({ dialogOpen, axisNames, apply, cancel }) => {
  const device = useSelector(selectSelectedDeviceState);

  const [filter, setFilter] = useState('');
  const [showAll, setShowAll] = useState(false);

  let selectableChannels = extractSettingsChannels(device.axes.map(axisState => axisState.channels).flat()).map(ch => _.cloneDeep(ch));
  const oldSelections = new Set<string>(selectableChannels.map(ch => ch.id));

  const nonEmptyAxes: number[] = [];

  device.axes.forEach((axisState, axisNo) => {
    if (axisState.settingNames.length > 0 && (showAll || axisNo === 0 || axisState.peripheralId !== 0)) {
      nonEmptyAxes.push(axisNo);
    }

    selectableChannels = selectableChannels.concat(axisState.settingNames
      .filter(setting => !(oldSelections.has(makeSettingChannelId(setting, axisNo))))
      .map(setting => ({
        ...createChannelForSetting(setting, axisNo),
        showInUi: false,
      })));
  });

  const defaultAxis = nonEmptyAxes.includes(1) ? 1 : nonEmptyAxes[0];

  const [axis, setAxis] = useState(defaultAxis);
  const [channels, setChannels] = useState<Channel[]>(selectableChannels);

  const filteredChannels = extractAxisChannels(channels, axis)
    .filter(ch => showAll || MostUsedSettings.includes(ch.name))
    .filter(ch => !filter || ch.subtitle.includes(filter) || ch.title.toLocaleLowerCase().includes(filter));
  const [groups, groupNames] = groupSettings(filteredChannels, showAll);

  const allSelectedChannels = channels.filter(ch => ch.showInUi);
  allSelectedChannels.sort((a, b) => showAll ? sortAlphabetic(a.name, b.name) : sortByRelevance(a.name, b.name));
  const buttonLabel = groupNames.length ? 'Apply' : 'Add';

  return <Modal
    className="add-data-source-dialog"
    data-testid="add-data-source-dialog"
    headerIcon={<AppIconsNeutral.Oscilloscope/>}
    headerText={ADD_SETTING_BUTTON_LABEL}
    isOpen={dialogOpen}
    onRequestClose={cancel}
  >
    <div className="control-bar">
      <div className="left-controls">
        <Input
          className="search-bar"
          clearable
          data-testid="setting-name-filter"
          value={filter}
          onValueChange={setFilter}
          icon={<Icons.Search className="search-icon"/>}
          placeholder="Search..."
        />
        <Checkbox
          labelContent="Show All"
          className="show-all-settings"
          data-testid="show-all-settings"
          checked={showAll}
          onChecked={setShowAll}
        />
      </div>
      <Button className="apply-button" onClick={() => {
        const newSettings = channels.filter(ch => ch.showInUi);
        if (!groupNames.length && filter) {
          const customChannel = createChannelForSetting(filter, axis);
          customChannel.color = getDefaultColor(customChannel.id);
          newSettings.push(customChannel);
        }

        apply(newSettings);
        setFilter('');
      }}>{buttonLabel}</Button>
    </div>
    <div className="settings-pills">
      {allSelectedChannels.map((channel, i) =>
        <Pill key={i}
          label={`${channel.name}${(channel.axis > 0 ? ` (${channel.axis})` : '')}`}
          onClickIcon={() => setChannels(changeCollection(channels, 'id', channel.id, ch => ({ ...ch, showInUi: false })))}/>)}
    </div>
    {axisNames.length && <MinorMenu barStyle="horizontal">
      {axisNames.map((name, i) => nonEmptyAxes.includes(i) ?
        <div key={i} className={classNames({ active: axis === i })}>
          <MinorMenu.Item data-testid={`data-source-axis-tab-${i}`} onClick={() => setAxis(i)}>{name}</MinorMenu.Item>
        </div> : null)}</MinorMenu>}
    <div className="settings-list">
      {groupNames.map(groupName => {
        if (groups[groupName].length) {
          return <HeaderCard key={groupName} header={<span>{groupName}</span>}>
            {groups[groupName].map(channel =>
              <SettingRow
                key={channel.id}
                channel={channel}
                data-testid={`setting-dialog-row ${channel.id}`}
                canSelect={allSelectedChannels.length < device.maxChannels}
                setVisible={visible => setChannels(changeCollection(channels, 'id', channel.id, ch => ({
                  ...ch,
                  showInUi: visible,
                  color: visible && ch.color === UNSET_COLOR ? getDefaultColor(ch.id) : ch.color,
                })))}
                setColor={color => setChannels(changeCollection(channels, 'id', channel.id, ch => ({ ...ch, color })))}/>)}
          </HeaderCard>;
        } else {
          return null;
        }
      })}
    </div>
  </Modal>;
};
