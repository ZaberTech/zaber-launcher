import type { EntityKey } from '../keys';
import { createReducer } from '../utils';

import { ActionTypes, ActionsToPayloads } from './actions';
import type { LoadStateCompatibility, PeripheralCompatibilityError, SaveLoadUiState } from './types';

export interface State {
  entity: EntityKey | null;
  ui: SaveLoadUiState | null;
}

const initialState: State = {
  entity: null,
  ui: null,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const begin: Reducer<ActionTypes.BEGIN> = (_, { entity }) => ({ entity, ui: { step: 'choose' } });
const close: Reducer<ActionTypes.CLOSE> = () => initialState;
const cancelFileSelect: Reducer<ActionTypes.CANCEL_FILE_SELECT> = state => ({ ...state, ui: { step: 'choose' } });

const beginSave: Reducer<ActionTypes.BEGIN_SAVE> = state => ({
  ...state,
  ui: { step: 'working', message: 'Saving the state of your product...' },
});
const chooseFile: Reducer<ActionTypes.CHOOSE_FILE> = state => ({ ...state, ui: { step: 'choosing-file' } });
const saveDone: Reducer<ActionTypes.SAVE_DONE> = (state, { error }) => ({
  ...state,
  ui: error == null ? { step: 'saved' } : { step: 'save-error', error },
});

const beginLoad: Reducer<ActionTypes.BEGIN_LOAD> = state => ({ ...state, ui: { step: 'choosing-file' } });
function resultToCompatibility(result: ActionsToPayloads[ActionTypes.CHECK_LOAD_DONE]['result']): LoadStateCompatibility {
  if (result == null || typeof result === 'string') {
    // TODO ZML-811: In the future this should stop returning empty strings when there's no error and clean up some of this logic
    const error = result != null && result !== '' ? result : null;
    return { type: 'axis', error, peripheralErrors: null };
  } else if (result.axisErrors.length < 1) {
    const error = result.error != null && result.error !== '' ? result.error : null;
    return { type: 'device', error, peripheralErrors: null };
  } else {
    // TODO ZML-811: This too can do just a null check once I'm sure that's always the return type
    const peripheralErrors = result.axisErrors.filter((e): e is PeripheralCompatibilityError => e.error != null && e.error !== '');
    return {
      type: 'controller',
      error: result.error != null && result.error !== '' ? result.error : null,
      peripheralErrors: peripheralErrors.length > 0 ? peripheralErrors : null,
    };
  }
}
const checkLoadDone: Reducer<ActionTypes.CHECK_LOAD_DONE> = (state, { context, result }) => ({
  ...state,
  ui: { step: 'load-check-result', context, compatibility: resultToCompatibility(result) }
});
const applyLoad: Reducer<ActionTypes.APPLY_LOAD> = (state, { context, target }) => ({
  ...state,
  ui: { ...context, step: 'working', message: `Loading the saved state to your ${target.name}...` },
});
const loadError: Reducer<ActionTypes.LOAD_ERROR> = (state, { error }) => ({ ...state, ui: { step: 'load-error', error } });
const loadDone: Reducer<ActionTypes.LOAD_DONE> = (state, { context, response }) => ({
  ...state,
  ui: { step: 'loaded', context, response }
});

const restore: Reducer<ActionTypes.RESTORE> = state => ({ ...state, ui: { step: 'working', message: 'Restoring...' } });
const restoreDone: Reducer<ActionTypes.RESTORE_DONE> = state => ({ ...state, ui: { step: 'restored' } });
const restoreError: Reducer<ActionTypes.RESTORE_ERROR> = (state, { error }) => ({ ...state, ui: { step: 'restore-error', error } });

export const reducer = createReducer<ActionsToPayloads, State>({
  // UI
  [ActionTypes.BEGIN]: begin,
  [ActionTypes.CLOSE]: close,
  [ActionTypes.CANCEL_FILE_SELECT]: cancelFileSelect,

  // Save
  [ActionTypes.BEGIN_SAVE]: beginSave,
  [ActionTypes.CHOOSE_FILE]: chooseFile,
  [ActionTypes.SAVE_DONE]: saveDone,

  // Load
  [ActionTypes.BEGIN_LOAD]: beginLoad,
  [ActionTypes.CHECK_LOAD_DONE]: checkLoadDone,
  [ActionTypes.APPLY_LOAD]: applyLoad,
  [ActionTypes.LOAD_ERROR]: loadError,
  [ActionTypes.LOAD_DONE]: loadDone,

  // Restore
  [ActionTypes.RESTORE]: restore,
  [ActionTypes.RESTORE_DONE]: restoreDone,
  [ActionTypes.RESTORE_ERROR]: restoreError,
}, initialState);
