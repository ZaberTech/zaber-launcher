import React, { Component } from 'react';
import { Icons, Text, TextType } from '@zaber/react-library';

import { RouterType, StoredRouter } from '../types';
import { parseCloudUrl, parseLocalShareUrl } from '../utils';

interface Router extends StoredRouter {
  name: string | null;
}

interface Props {
  router: Router;
  textType?: TextType;
}

export const LOCAL_ROUTER_NAME = 'This Computer';

export class RouterName extends Component<Props> {
  static defaultProps: Partial<Props> = {
    textType: Text.Type.H4,
  };

  formatLocalShareUrl(url: string): string {
    const parsedUrl = parseLocalShareUrl(url);
    if (!parsedUrl) { return '' }
    return `${parsedUrl.hostname}:${parsedUrl.port}`;
  }

  render(): React.ReactNode {
    const { router: { type, url, name }, textType } = this.props;
    return (<>
      {type === RouterType.Local && <Icons.Computer/>}
      {type === RouterType.Cloud && <Icons.Cloud/>}
      {type === RouterType.LocalShare && <Icons.Network/>}
      &nbsp;
      <Text t={textType}>
        {type === RouterType.Local && LOCAL_ROUTER_NAME}
        {type === RouterType.Cloud && (name ?? parseCloudUrl(url)?.cloudId)}
        {type === RouterType.LocalShare && (name ?? this.formatLocalShareUrl(url))}
      </Text>
    </>);
  }
}
