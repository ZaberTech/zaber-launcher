import type { AnyAction, Action } from '@zaber/toolbox/lib/redux';

import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

import type { StoredRouter, DeviceInfoWithAxes, RouterInfo, LoadingDevicesError } from './types';

const ACTIONS_PREFIX = 'CONNECTION_MANAGER_';

export enum ActionTypes {
  LOAD_ROUTERS = 'CONNECTION_MANAGER_LOAD_ROUTERS',
  ROUTERS_LOADED = 'CONNECTION_MANAGER_ROUTERS_LOADED',
  LOAD_CONNECTIONS = 'CONNECTION_MANAGER_RELOAD_CONNECTIONS',
  CONNECTIONS_LOADED = 'CONNECTION_MANAGER_CONNECTIONS_LOADED',
  CONNECTIONS_LOADED_ERR = 'CONNECTION_MANAGER_CONNECTIONS_LOADED_ERR',
  LOAD_DEVICES = 'CONNECTION_MANAGER_LOAD_DEVICES',
  DEVICES_LOADED = 'CONNECTION_MANAGER_DEVICES_LOADED',
  DEVICES_IDENTIFIED = 'CONNECTION_MANAGER_DEVICES_IDENTIFIED',
  DEVICES_LOADED_ERR = 'CONNECTION_MANAGER_DEVICES_LOADED_ERR',
  ADD_ROUTER = 'CONNECTION_MANAGER_ADD_ROUTER',
  REMOVE_ROUTER = 'CONNECTION_MANAGER_REMOVE_ROUTER',
  UPDATE_LOCAL_ROUTER_NAME = 'CONNECTION_MANAGER_UPDATE_LOCAL_ROUTER_NAME',
  SET_THUMBNAILS = 'CONNECTION_MANAGER_SET_THUMBNAILS',
  SET_REMOTE_MODE = 'CONNECTION_MANAGER_SET_REMOTE_MODE',
}

export interface ActionsToPayloads {
  [ActionTypes.ADD_ROUTER]: { router: StoredRouter; saveAndBroadcast: boolean };
  [ActionTypes.REMOVE_ROUTER]: { key: EntityKey; saveAndBroadcast: boolean };
  [ActionTypes.LOAD_ROUTERS]: void;
  [ActionTypes.ROUTERS_LOADED]: { routers: StoredRouter[] };
  [ActionTypes.LOAD_CONNECTIONS]: { routerKey: EntityKey; useCache: boolean };
  [ActionTypes.CONNECTIONS_LOADED]: { routerKey: EntityKey; info: RouterInfo };
  [ActionTypes.CONNECTIONS_LOADED_ERR]: { routerKey: EntityKey; err: string };
  [ActionTypes.LOAD_DEVICES]: { connectionKey: EntityKey; useCache: boolean };
  [ActionTypes.DEVICES_LOADED]: { connectionKey: EntityKey; devices: DeviceInfoWithAxes[] };
  [ActionTypes.DEVICES_IDENTIFIED]: { connectionKey: EntityKey };
  [ActionTypes.DEVICES_LOADED_ERR]: { connectionKey: EntityKey; err: LoadingDevicesError };
  [ActionTypes.UPDATE_LOCAL_ROUTER_NAME]: { name: string };
  [ActionTypes.SET_THUMBNAILS]: { thumbnails: Record<number, string> };
  [ActionTypes.SET_REMOTE_MODE]: { key: EntityKey; isRemote: boolean };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  loadRouters: () => buildAction(ActionTypes.LOAD_ROUTERS),
  addRouter: (router: StoredRouter) => buildAction(ActionTypes.ADD_ROUTER, { router, saveAndBroadcast: true }),
  removeRouter: (key: EntityKey) => buildAction(ActionTypes.REMOVE_ROUTER, { key, saveAndBroadcast: true }),
  routersLoaded: (routers: StoredRouter[]) => buildAction(ActionTypes.ROUTERS_LOADED, { routers }),
  loadConnections: (routerKey: EntityKey, useCache: boolean) => buildAction(ActionTypes.LOAD_CONNECTIONS, { routerKey, useCache }),
  connectionsLoaded: (routerKey: EntityKey, info: RouterInfo) =>
    buildAction(ActionTypes.CONNECTIONS_LOADED, { routerKey, info }),
  connectionsLoadedErr: (routerKey: EntityKey, err: string) =>
    buildAction(ActionTypes.CONNECTIONS_LOADED_ERR, { routerKey, err }),
  loadDevices: (connectionKey: EntityKey, useCache: boolean) =>
    buildAction(ActionTypes.LOAD_DEVICES, { connectionKey, useCache }),
  devicesLoaded: (connectionKey: EntityKey, devices: DeviceInfoWithAxes[]) =>
    buildAction(ActionTypes.DEVICES_LOADED, { connectionKey, devices }),
  devicesIdentified: (connectionKey: EntityKey) =>
    buildAction(ActionTypes.DEVICES_IDENTIFIED, { connectionKey }),
  devicesLoadedErr: (connectionKey: EntityKey, err: LoadingDevicesError) =>
    buildAction(ActionTypes.DEVICES_LOADED_ERR, { connectionKey, err }),
  updateLocalRouterName: (name: string) => buildAction(ActionTypes.UPDATE_LOCAL_ROUTER_NAME, { name }),
  setThumbnails: (thumbnails: Record<number, string>) => buildAction(ActionTypes.SET_THUMBNAILS, { thumbnails }),
  setRemoteMode: (key: EntityKey, isRemote: boolean) => buildAction(ActionTypes.SET_REMOTE_MODE, { key, isRemote }),
};

export type DevicesLoadedPayload = ActionsToPayloads[ActionTypes.DEVICES_LOADED];
export type DevicesLoadedAction = Action<DevicesLoadedPayload>;
export type DevicesLoadedErrAction = Action<ActionsToPayloads[ActionTypes.DEVICES_LOADED_ERR]>;

export const allConnectionManagerActions = (action: AnyAction) => action.type.startsWith(ACTIONS_PREFIX);
