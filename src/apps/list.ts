import _ from 'lodash';

import { Editions, environment, Flavors } from '../environment';

export type AppUrls = '/basic-controls' | '/basic-movement' | '/device-settings' | '/terminal' | '/firmware-upgrade'
  | '/current-tuner' | '/lc40-setup' | '/shb40-setup' | '/hardware-modification' | '/gcode' | '/demo' | '/oscilloscope'
  | '/lockstep-configuration' | '/servo-tuner' | '/device-io' | '/joystick' | '/process-controller'
  | '/internal/production-app' | '/internal/fw7-tool' | '/pvt' | '/calibration' | '/microscope' | '/triggers';

interface AppMeta {
  name: string;
  url: AppUrls;
  editions?: Editions[];
  flavours?: Flavors[];
  unlockCode?: string;
  deprecated?: boolean;
}

export const APP_LIST: AppMeta[] = [{
  name: 'Basic Controls',
  url: '/basic-controls',
}, {
  name: 'Basic Movement',
  url: '/basic-movement',
  flavours: [Flavors.Zaber, Flavors.Rexroth],
  deprecated: true,
}, {
  name: 'Device Settings',
  url: '/device-settings',
  flavours: [Flavors.Zaber, Flavors.Rexroth],
}, {
  name: 'Terminal',
  url: '/terminal',
  flavours: [Flavors.Zaber, Flavors.Rexroth],
}, {
  name: 'Firmware Upgrade',
  url: '/firmware-upgrade',
  flavours: [Flavors.Zaber, Flavors.Rexroth],
}, {
  name: 'Current Tuner',
  url: '/current-tuner',
}, {
  name: 'LC40 Setup',
  url: '/lc40-setup',
}, {
  name: 'SHB40 Setup',
  url: '/shb40-setup',
  flavours: [Flavors.Rexroth],
}, {
  name: 'Advanced Hardware Setup',
  url: '/hardware-modification',
}, {
  name: 'Servo Tuner',
  url: '/servo-tuner',
}, {
  name: 'Process Controller',
  url: '/process-controller',
}, {
  name: 'G-Code',
  url: '/gcode',
}, {
  name: 'Lockstep Configuration',
  url: '/lockstep-configuration',
}, {
  name: 'Cycle Demo',
  url: '/demo',
  editions: [Editions.Dev, Editions.Internal],
  unlockCode: 'cycledemo',
}, {
  name: 'Device IO',
  url: '/device-io',
  deprecated: true,
}, {
  name: 'X-JOY',
  url: '/joystick',
}, {
  name: 'Oscilloscope',
  url: '/oscilloscope',
}, {
  name: 'Production',
  url: '/internal/production-app',
  editions: [Editions.Dev, Editions.Internal],
}, {
  name: 'FW7 Programmer',
  url: '/internal/fw7-tool',
  editions: [Editions.Dev, Editions.Internal],
}, {
  name: 'PVT Viewer',
  url: '/pvt',
}, {
  name: 'Calibration',
  url: '/calibration',
  editions: [Editions.Dev, Editions.Internal],
}, {
  name: 'Microscope',
  url: '/microscope',
}, {
  name: 'Triggers',
  url: '/triggers',
}];

export const APP_DICTIONARY = _.keyBy(APP_LIST, 'url') as Record<AppUrls, AppMeta>;
export const FILTERED_APP_LIST = APP_LIST
  .filter(app => app.editions == null || app.editions.includes(environment.edition))
  .filter(app => (app.flavours ?? [Flavors.Zaber]).includes(environment.flavor));

export const UNLOCKABLE_APPS = APP_LIST
  .filter(app => app.unlockCode != null)
  .reduce((accum, meta) => {
    accum[meta.unlockCode!] = meta.url;
    return accum;
  }, {} as Record<string, AppUrls>);

export const MAX_RECENT_APPS = 6;
export const DEFAULT_RECENT_APPS: AppUrls[] = ['/basic-controls', '/device-settings', '/terminal'];
if (environment.flavor === Flavors.Rexroth) {
  DEFAULT_RECENT_APPS.push('/shb40-setup');
}

export const DEFAULT_PINNED_APPS: AppUrls[] = ['/basic-controls'];
