export enum Editions {
  Dev = 'dev',
  Internal = 'internal',
  Public = 'public',
}

export enum Flavors {
  Zaber = 'zaber',
  Rexroth = 'rexroth',
}

export interface Environment {
  /**
   * Denotes whether the code is running as built program or in development environment
   */
  isProduction: boolean;
  isTest: boolean;
  edition: Editions;
  offline: boolean;
  flavor: Flavors;
  releaseName: string;
  appName: string;
  apiUrl: string;

  cognitoIdentityPoolId: string;
  awsRegion: string;
  cognitoUserPoolId: string;
  cognitoUserPoolClientId: string;
  iotCoreHost: string;
  cloudPortalUrl: string;
  platform: string;
}

export const environment = ((): Readonly<Environment> => {
  const isTest = !!process.env.JEST_WORKER_ID;

  if (isTest) {
    return {
      isProduction: false,
      isTest,
      releaseName: 'Test',
      appName: 'Zaber Launcher',
      edition: Editions.Dev,
      offline: false,
      flavor: Flavors.Zaber,
      apiUrl: 'https://api.test.io',
      awsRegion: 'region',
      cognitoUserPoolId: 'region_XXXXXX',
      cognitoUserPoolClientId: 'poolClientId',
      cognitoIdentityPoolId: 'identityPoolId',
      iotCoreHost: '',
      cloudPortalUrl: 'https://cloud.test.io',
      platform: process.platform,
    };
  }

  // Some libraries use actual NODE_ENV to determine debug behavior.
  // Since we don't process node_modules by webpack we have set NODE_ENV manually.
  (() => process.env)().NODE_ENV = process.env.NODE_ENV;

  const flavor = process.env.ZL_FLAVOR as Flavors;

  return Object.freeze({
    isProduction: process.env.NODE_ENV === 'production',
    edition: process.env.ZL_EDITION as Editions,
    offline: process.env.ZL_OFFLINE === 'true',
    flavor,
    isTest,
    releaseName: process.env.RELEASE_NAME ?? '',
    appName: flavor === Flavors.Zaber ? 'Zaber Launcher' : 'Rexroth Dashboard',
    apiUrl: process.env.API_URL ?? '',
    awsRegion: process.env.AWS_REGION ?? '',
    cognitoUserPoolId: process.env.COGNITO_USER_POOL_ID ?? '',
    cognitoUserPoolClientId: process.env.COGNITO_USER_POOL_CLIENT_ID ?? '',
    cognitoIdentityPoolId: process.env.COGNITO_IDENTITY_POOL_ID ?? '',
    iotCoreHost: process.env.IOT_CORE_HOST ?? '',
    cloudPortalUrl: process.env.CLOUD_PORTAL_URL ?? '',
    platform: process.platform,
  });
})();

export const processArguments = (() => {
  // eslint-disable-next-line @typescript-eslint/no-require-imports,@typescript-eslint/no-unsafe-member-access
  const mainProcess: NodeJS.Process = typeof global.window !== 'undefined' ? require('@electron/remote').process : global.process;
  const argv = environment.isTest ? [] : mainProcess.argv;

  return Object.freeze({
    clearLocalStorage: argv.includes('--zaber-clear-local-storage'),
    disableUpdates: argv.includes('--zaber-disable-updates'),
  });
})();
