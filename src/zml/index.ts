import path from 'path';

import * as remote from '@electron/remote';
import { inject, injectable } from 'inversify';
import { Tools, Library, DeviceDbSourceType, ascii, binary, Units } from '@zaber/motion';

import { Editions, environment } from '../environment';

import { ZMLLoggingService } from './logging_service';

@injectable()
export class ZML {
  constructor(
    @inject(ZMLLoggingService) readonly loggingService: ZMLLoggingService,
  ) {
    if (environment.edition !== Editions.Public) {
      Library.setInternalMode(true);
    }

    if (environment.offline) {
      let installDir: string;
      if (environment.isProduction) {
        installDir = path.dirname(remote.app.getPath('exe'));
        if (environment.platform === 'darwin') {
          installDir = path.join(installDir, '..');
        }
      } else {
        installDir = path.join(__dirname, '..');
      }

      const db = path.join(installDir, 'device_db_service', 'device_database.sqlite');
      Library.setDeviceDbSource(DeviceDbSourceType.FILE, db);
    } else {
      const deviceDbUrl = `${environment.apiUrl}/device-db/${environment.edition === Editions.Public ? 'public' : 'master'}`;
      Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, deviceDbUrl);
    }
    const deviceDbStorePath = path.join(remote.app.getPath('userData'), 'zmlDeviceDb');
    Library.enableDeviceDbStore(deviceDbStorePath);
  }

  public listPorts(): Promise<string[]> {
    return Tools.listSerialPorts();
  }

  public openSerialPort(portName: string, baudRate: number): Promise<ascii.Connection> {
    return ascii.Connection.openSerialPort(portName, { baudRate, direct: true });
  }

  public openTcp(hostname: string, port: number): Promise<ascii.Connection> {
    return ascii.Connection.openTcp(hostname, port);
  }

  public openBinarySerialPort(portName: string, baudRate: number): Promise<binary.Connection> {
    return binary.Connection.openSerialPort(portName, { baudRate });
  }

  public openBinaryTcp(hostname: string, port: number): Promise<binary.Connection> {
    return binary.Connection.openTcp(hostname, port);
  }
}


/**
 * Common interface for device and axis settings get and set functions.
 */
export interface ISettingsAccessor {
  get(setting: string, unit?: Units): Promise<number>;
  set(setting: string, value: number, unit?: Units): Promise<void>;
  getString(setting: string): Promise<string>;
  setString(setting: string, value: string): Promise<void>;
}


/**
 * Common interface for device and axis settings unit conversion.
 */
export interface IUnitConverter {
  convertToNativeUnits(setting: string, value: number, unit: Units): number;
  convertFromNativeUnits(setting: string, value: number, unit: Units): number;
}
