import { dialog } from 'electron';

import { initErrors, errorEmitter } from '../src/errors/errors';

initErrors();
errorEmitter.once('error', () => { afterError().catch(() => null) });

const WAIT_FOR_SENTRY_TO_REPORT = 5000;

async function afterError(): Promise<void> {
  try {
    await dialog.showMessageBox({
      type: 'error',
      title: 'Zaber Launcher Error',
      buttons: ['OK'],
      message: [
        'Error has occurred and the application must be closed.',
        'The error was automatically reported.',
        'If the issue persists, please contact Zaber support at https://www.zaber.com/contact.',
      ].join(' '),
    });
  } finally {
    setTimeout(() => process.exit(1), WAIT_FOR_SENTRY_TO_REPORT);
  }
}
