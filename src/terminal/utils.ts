/* eslint-disable camelcase */
import { tryAccess } from '@zaber/toolbox';
import _ from 'lodash';
import { Measurement } from '@zaber/motion';

import { ProductInfo, selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import { getIncludedKeys, type EntityKey, EntityKeyType, getEntityType, makeDeviceKey, makeAxisKey } from '../keys';
import { versionCategory } from '../firmware_upgrade/utility';
import { Editions, environment } from '../environment';
import { fwToString } from '../devices';
import { dimensionsByName, UnitDefinition, unitsFromSymbol } from '../units';

import { ASCII_PROTOCOL_REFERENCE, Command, CommandPart, CommandTree, INTERNAL_MANUAL_REFERENCE } from './types';


const REGEX_PARAM = /^(?<value>-?[0-9]+(.[0-9]+)?)(?<unit>[^.\n]+)?$/;
export type CommandWithMeasurements = { command: string; measurements: Measurement[] };

export function splitAndCleanCommands(commands: string) {
  return commands.split('\n').map(line => line.trim()).filter(line => line.length > 0);
}

export function getMostSpecificEntityKey(baseKey: EntityKey, device?: string, axis?: string): EntityKey {
  let specificKey = baseKey;

  const deviceNumber = device ? Number(device) : 0;
  const axisNumber = axis ? Number(axis) : 0;

  if (getEntityType(specificKey) === EntityKeyType.CONNECTION && deviceNumber !== 0) {
    specificKey = makeDeviceKey(specificKey, deviceNumber);
  }
  if (getEntityType(specificKey) === EntityKeyType.DEVICE && axisNumber !== 0) {
    specificKey = makeAxisKey(specificKey, axisNumber);
  }

  return specificKey;
}

export function makeProtocolLink(
  key: EntityKey | null,
  devices: ReturnType<typeof selectIdentifiedDevices>,
  axes: ReturnType<typeof selectIdentifiedAxes>,
) {
  let url = ASCII_PROTOCOL_REFERENCE;
  if (key == null) { return url }

  const includedKeys = getIncludedKeys(key);
  if (EntityKeyType.DEVICE in includedKeys) {
    const device = tryAccess(devices, includedKeys[EntityKeyType.DEVICE]);
    if (device) {
      const firmware = device.identity.firmwareVersion;
      if (versionCategory(firmware) === 'internal' && environment.edition !== Editions.Public) {
        return INTERNAL_MANUAL_REFERENCE;
      }

      url += `&device=${encodeURIComponent(device.identity.name)}`;
      url += `&version=${encodeURIComponent(fwToString(device.identity.firmwareVersion, false))}`;

      if (device.axisCount > 0) {
        const axisKey = EntityKeyType.AXIS in includedKeys ? includedKeys[EntityKeyType.AXIS] : device.axes[0];
        const axis = tryAccess(axes, axisKey);
        if (axis?.isPeripheralLike) {
          url += `&peripheral=${encodeURIComponent(axis.identity.peripheralName)}`;
        }
      }
    }
  }

  return url;
}

export function commandTreeToCommandList(commandTree: CommandTree, productInfo: ProductInfo): Command[] {
  const { command, nodes, is_command } = commandTree;

  const commandPart = generateCommandPart(commandTree, productInfo);
  const commandList: Command[] = is_command ? [{ type: 'command', parts: [commandPart] }] : [];

  nodes?.forEach(node =>
    commandTreeToCommandList(node, productInfo).forEach(subCommand => {
      if (command === '') {
        commandList.push(subCommand);
      } else {
        commandList.push({
          type: 'command',
          parts: [commandPart, ...subCommand.parts]
        });
      }
    })
  );

  return commandList;
}

function generateCommandPart(commandTree: CommandTree, productInfo: ProductInfo): CommandPart {
  const { command, is_param, param_type, param_arity, values, contextual_dimension_id } = commandTree;

  if (is_param) {
    if (param_type === 'Enum') {
      return { type: 'enum', text: values ? values : [], multiple: param_arity !== 1 };
    } else {
      const units = contextual_dimension_id ? getUnitDefinitions(productInfo, contextual_dimension_id) : [];
      return { type: 'param', text: command, multiple: param_arity !== 1, units };
    }
  } else {
    return { type: 'command', text: command };
  }
}

export function getUnitDefinitions(productInfo: ProductInfo, contextualDimensionId: number): UnitDefinition[] {
  const dimensionRow = productInfo.conversionTable.rows.find(row => row.contextual_dimension_id === contextualDimensionId);
  if (dimensionRow != null) {
    const dimension = dimensionsByName[dimensionRow.dimension_name];
    if (dimension != null) {
      return dimension.units;
    }
  }
  return [];
}

export function commandPartToText(commandPart: CommandPart): string {
  const { type, text } = commandPart;
  switch (type) {
    case 'command':
      return `${text} `;
    case 'param':
      return `<${text}> `;
    case 'enum':
      return `${text.join('|')} `;
  }
}

export function suggestionToText(suggestion: CommandPart[], commandInput: string): string {
  const inputParts = commandInput.split(/\s+/);

  return suggestion.reduce((text, suggestionPart, index) => {
    const { type } = suggestionPart;
    if (inputParts[index]) {
      if (type === 'param' || type === 'enum') {
        if (suggestionPart.multiple === true) {
          return text.concat(inputParts.slice(index).join(' '));
        } else {
          return text.concat(`${inputParts[index]} `);
        }
      } else {
        return text.concat(commandPartToText(suggestionPart));
      }
    } else if (inputParts[index - 1]) {
      return text.concat(commandPartToText(suggestionPart));
    } else {
      return text;
    }
  }, '').trim();
}

export function matchCommand(command: Command, inputParts: string[]): boolean {
  const { parts: commandParts } = command;

  for (const [i, inputPart] of inputParts.entries()) {
    if (!commandParts[i]) {
      return false;
    }
    const commandPart = commandParts[i];
    const { text, type } = commandPart;

    if (type === 'command') {
      if (!text.startsWith(inputPart)) {
        return false;
      }
    } else if (type === 'param') {
      if (inputPart) {
        const inputPartValueUnit = getCommandPartValueUnit(inputPart);
        if (!inputPartValueUnit.value) {
          return false;
        }
      }
      // Assume match is true if param is infinite-arrity
      if (commandPart.multiple) {
        return true;
      }
    } else if (type === 'enum') {
      // Assume match is true if enum is infinite-arity
      if (commandPart.multiple) {
        return true;
      }
      if (!text.some(option => option.startsWith(inputPart))) {
        return false;
      }
    }
  }

  return true;
}

function getLongestMatchingString(strA: string, strB: string): string {
  for (let i = 0; i < strA.length; i++) {
    if (strA.charAt(i) !== strB.charAt(i)) {
      return strA.slice(0, i);
    }
  }
  return strA;
}

export function getLongestMatchingSuggestion(possibleCommands: Command[]): CommandPart[] {
  if (possibleCommands.length === 0) {
    return [];
  }

  return possibleCommands.reduce((suggestion, possibleCommand) => {
    for (const [i, possibleCommandPart] of possibleCommand.parts.entries()) {
      if (i >= suggestion.length) {
        return suggestion;
      }
      const suggestionPart = suggestion[i];

      if (possibleCommandPart.type !== suggestionPart.type) {
        return suggestion.slice(0, i);
      }

      const suggestionPartIsCommandOrParam = suggestionPart.type === 'command' || suggestionPart.type === 'param';
      const possibleCommandPartIsCommandOrParam = possibleCommandPart.type === 'command' || possibleCommandPart.type === 'param';

      if (suggestionPartIsCommandOrParam && possibleCommandPartIsCommandOrParam) {
        if (possibleCommandPart.text !== suggestionPart.text) {
          const longestMatchingText = getLongestMatchingString(possibleCommandPart.text, suggestionPart.text);
          if (longestMatchingText === '') {
            return suggestion.slice(0, i);
          } else {
            suggestionPart.text = longestMatchingText;
            return suggestion.slice(0, i + 1);
          }
        }
      } else if (suggestionPart.type === 'enum') {
        if (!_.zip([suggestionPart.text, possibleCommandPart.text])
          .every(([suggestionOption, possibleOption]) =>
            suggestionOption === possibleOption)) {
          return suggestion.slice(0, i);
        }
      }
    }

    return suggestion;
  }, possibleCommands[0].parts.map(part => ({ ...part })));
}

export const getCommandPartValueUnit = (inputPart: string): { value: string; unit: string } => {
  const inputPartMatch = inputPart.match(REGEX_PARAM);
  return { value: inputPartMatch?.groups?.value ?? '', unit: inputPartMatch?.groups?.unit ?? '' };
};

export const getCommandSuggestions = (commandList: Command[], inputParts: string[]): Command[] => commandList
  .filter(command => matchCommand(command, inputParts))
  .flatMap(command => addUnitsSuggestions(command, inputParts))
  .map(command => replaceParams(command, inputParts))
  .sort(compareCommandsLength);

export const addUnitsSuggestions = (suggestion: Command, inputParts: string[]): Command[] => {
  if (suggestion.parts.length < inputParts.length) {
    return [suggestion];
  }

  if (suggestion.parts.slice(0, inputParts.length).some(part =>
    (part.type === 'param' && part.multiple) || (part.type === 'enum' && part.multiple)
  )) {
    return [suggestion];
  }

  const suggestionPart = suggestion.parts[inputParts.length - 1];
  if (suggestionPart.type !== 'param' || suggestionPart.units.length === 0 || suggestionPart.multiple) {
    return [suggestion];
  }

  const inputPartIndex = inputParts.length - 1;
  const inputPart = inputParts[inputPartIndex];
  const inputPartValueUnit = getCommandPartValueUnit(inputPart);

  if (!inputPartValueUnit.value) {
    return [suggestion];
  }

  const unitsSuggestions = suggestionPart.units.map(unit => {
    const newLastPart: CommandPart = {
      type: 'command',
      text: inputPartValueUnit.value + unit.ShortName,
    };
    const command: Command = {
      type: 'commandWithUnits',
      parts: [
        ...suggestion.parts.slice(0, inputPartIndex),
        newLastPart,
        ...suggestion.parts.slice(inputPartIndex + 1)
      ],
    };
    return command;
  });

  if (!inputPartValueUnit.unit) {
    unitsSuggestions.push(suggestion);
  }

  return unitsSuggestions;
};

export const replaceParams = (suggestion: Command, inputParts: string[]): Command => {
  let hasParamMultiple = false;
  return {
    ...suggestion,
    parts: suggestion.parts.map((part, i) => {
      if (hasParamMultiple || (part.type === 'param' && part.multiple) || (part.type === 'enum' && part.multiple)) {
        hasParamMultiple = true;
        return part;
      } else if (part.type === 'param' && inputParts[i]) {
        return {
          type: 'command',
          text: inputParts[i]
        };
      } else {
        return part;
      }
    })
  };
};

export const compareCommandsLength = (a: Command, b: Command): number => {
  if (a.parts.length === b.parts.length) {
    return a.parts[a.parts.length - 1].text.length - b.parts[b.parts.length - 1].text.length;
  } else {
    return a.parts.length - b.parts.length;
  }
};

export const getCommandWithMeasurements = (commandList: Command[], command: string): CommandWithMeasurements => {
  const commandParts = command.split(/\s+/);
  const commandSuggestion = commandList.find(command =>
    command.parts.length === commandParts.length && matchCommand(command, commandParts)
  );

  if (commandSuggestion == null) {
    return { command, measurements: [] };
  }

  return commandSuggestion.parts.reduce(({ command, measurements }: CommandWithMeasurements, part, i) => {
    const inputPart = commandParts[i];
    const inputPartValueUnit = getCommandPartValueUnit(inputPart);
    if (part.type === 'param' && part.units.length !== 0 && inputPartValueUnit.unit) {
      const unitDefinition = part.units.find(unit => unit.ShortName === inputPartValueUnit.unit);
      const unit = unitsFromSymbol(inputPartValueUnit.unit);
      if (unitDefinition != null && unit != null) {
        const measurement: Measurement = { value: parseFloat(inputPartValueUnit.value), unit };
        return { command: `${command} ?`.trim(), measurements: [...measurements, measurement] };
      }
    }
    return { command: `${command} ${inputPart}`.trim(), measurements };
  }, { command: '', measurements: [] });
};
