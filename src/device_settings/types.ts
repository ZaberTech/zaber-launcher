import type { SettingMetadata, CategoryMetadata } from '@zaber/device-db-metadata';
import { InputEditor } from '@zaber/react-library';

import { MeasurementInputEditorState } from '../units/MeasurementInputEditor';

export const ACCESS_LEVEL_SETTING = 'system.access';
export const DEFAULT_ACCESS_LEVEL = 1;
export const MAX_USER_ACCESS_LEVEL = 2;
export const DISPLAY_PRECISION = 3;
export const NO_CATEGORY = 'uncategorized';
export const NO_CATEGORY_META: CategoryMetadata = { humanName: 'Uncategorized' };
export const NETWORK_COMMAND_DELAY_TCP_IP = 2000;
export const NETWORK_COMMAND_DELAY_SERIAL = 400;
export const STRING_SETTING_TYPES = ['IPv4Address', 'IPv4Mask', 'MACAddress'];
export const NETWORK_SETTINGS = ['comm.en.ipv4.address', 'comm.en.ipv4.dhcp.enabled', 'comm.en.ipv4.netmask', 'comm.en.ipv4.gateway'];
export const DEPENDENT_SETTINGS = [
  ['accel', 'motion.accelonly', 'motion.decelonly'],
].map(group => new Set(group));
export const REFRESH_DEVICES_SETTINGS = ['peripheral.id', 'resolution', 'comm.address'];

export type SettingValue =
{
  type: 'string' | 'network';
} & InputEditor.State<string | null, string> |
{
  type: 'number';
} & MeasurementInputEditorState;

export type customDialog = 'network settings';

export type SettingsEntryInfo = SettingValue & {
  id: string;
  name: string;
  readAccessLevel: number;
  writeAccessLevel?: number;
  defaultValue: string | null;
  meta: SettingMetadata | null;
};

export interface NetworkConfig {
  dhcp: boolean;
  IPAddress?: string;
  defaultGateway?: string;
  subnetMask?: string;
}

export function getValueAsString(entry: SettingValue | undefined): string {
  if (entry?.type === 'number') {
    return entry.value?.value?.toString() ?? '';
  }
  return entry?.value ?? '';
}
