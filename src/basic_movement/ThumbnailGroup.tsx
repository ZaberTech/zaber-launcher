import { ensureArray } from '@zaber/toolbox';
import classNames from 'classnames';
import _ from 'lodash';
import React from 'react';

import { Thumbnail } from '../components/Thumbnail';
import { axisDescription } from '../connection_manager';

import type { Movable } from './selectors';

interface Props {
  movable: Movable;
}

export const ThumbnailGroup: React.FC<Props> = props => {
  const { movable } = props;
  const thumbnailCount = _.isArray(movable) ? movable.length : 1;
  return <div className={classNames('thumbnail-container', `count${thumbnailCount}`)}>{
    ensureArray(movable).map(
      (axis, index) =>
        <div key={axis.key} className={classNames('axis-image', thumbnailCount > 1 ? 'compressed' : 'normal', `index${index}`)}>
          <Thumbnail
            deviceOrPeripheralId={axis.identity.isPeripheral ? axis.identity.peripheralId : axis.device.identity.deviceId}
            title={axisDescription(axis, axis.device)}
            altExt="axis"/>
        </div>)
  }</div>;
};
