import React from 'react';

import { Container } from '../Container';

import { Grid } from './Grid';
import { Focus } from './Focus';
import { Presets } from './Presets';
import { StartStop } from './StartStop';
import { DimensionOrder } from './DimensionOrder';

export const MultiDAcquisition: React.FC = () =>
  <Container className="microscope-multidacq" headerChildren={<StartStop/>}>
    <Grid/>
    <Focus/>
    <Presets/>
    <DimensionOrder/>
  </Container>;
