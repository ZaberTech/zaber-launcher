import type { SagaIterator } from 'redux-saga';
import { all, call, delay, put, select, spawn, takeEvery, takeLeading } from 'redux-saga/effects';
import { ascii, CommandFailedException, InvalidDataException, Units } from '@zaber/motion';
import { settingsMetadata } from '@zaber/device-db-metadata';
import type { Nullable } from '@zaber/toolbox';
import _ from 'lodash';

import {
  getDevice, peripheralLikeAxesOfDevice, reloadDevices,
} from '../connection_manager';
import { getEntityType, EntityKeyType, EntityKey, getAxisNumber, extractConnectionKey, makeDeviceKey, tryExtractDeviceKey } from '../keys';
import { Action, RT, throwUnexpectedError } from '../utils';
import { RejectReasons } from '../protocol';
import type { ZaberApi } from '../app_components';
import { environment } from '../environment';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import {
  selectCurrentEntity as selectCurrent, selectCurrentDevice, selectSelectedConnection, selectReadableSettingIDs,
  selectSettingState, selectSettingToBeRead,
} from './selectors';
import type { SettingsEntry } from './reducer';
import {
  ACCESS_LEVEL_SETTING, NETWORK_COMMAND_DELAY_TCP_IP, NETWORK_COMMAND_DELAY_SERIAL,
  SettingsEntryInfo, STRING_SETTING_TYPES, DEPENDENT_SETTINGS, REFRESH_DEVICES_SETTINGS, NETWORK_SETTINGS, SettingValue
} from './types';

type CommonSettings = ascii.AxisSettings | ascii.DeviceSettings;

export function* deviceSettingsSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.SELECT_DEVICE_OR_AXIS, onSelectDeviceOrAxis),
    takeLeading(ActionTypes.READ_SETTING, readSettingLoop),
    takeEvery(ActionTypes.WRITE_SETTING, writeSetting),
    takeEvery(ActionTypes.WRITE_STRING_SETTING, writeStringSetting),
    takeEvery(ActionTypes.CONFIGURE_NETWORK, configureNetwork),
    takeEvery(ActionTypes.UPDATE_SETTING, updateSetting),
    takeEvery(ActionTypes.READ_ALL_SETTINGS, readAllSettings),
  ]);
}

function* onSelectDeviceOrAxis(): SagaIterator {
  const current: RT<typeof selectCurrent> = yield select(selectCurrent);
  if (!current) { return }

  yield put(actions.clearSettingQueue());
  if (!current.settingIDsSorted) {
    yield call(populateSettingsInfo, current);
  }

  if (current.axis) {
    const parentDevice: RT<typeof selectCurrentDevice> = yield select(selectCurrentDevice);
    if (parentDevice != null && parentDevice.currentAccessLevel == null) {
      yield put(actions.readSetting(parentDevice.key, ACCESS_LEVEL_SETTING));
      return;
    }
  }

  yield put(actions.readAllSettings());
}

function* populateSettingsInfo(deviceOrAxis: NonNullable<RT<typeof selectCurrent>>): SagaIterator {
  const { device: { product } } = deviceOrAxis;
  const peripheralLikeAxes: RT<typeof peripheralLikeAxesOfDevice> = yield call(peripheralLikeAxesOfDevice, deviceOrAxis.device);

  const transformedSettings = makeSettingDataForProduct(product.settings);
  yield put(actions.populateSettings(deviceOrAxis.device.key, transformedSettings));

  for (const axis of peripheralLikeAxes) {
    const transformedSettings = makeSettingDataForProduct(axis.product.settings);
    yield put(actions.populateSettings(axis.key, transformedSettings));
  }
}

function* getSettings(deviceOrAxis: EntityKey): SagaIterator<CommonSettings> {
  const device: ascii.Device = yield call(getDevice, deviceOrAxis);

  let settings: CommonSettings = device.settings;
  if (getEntityType(deviceOrAxis) === EntityKeyType.AXIS) {
    settings = device.getAxis(getAxisNumber(deviceOrAxis)).settings;
  }

  return settings;
}

function* readSettingLoop(): SagaIterator {
  while (true) {
    const toBeRead: ReturnType<typeof selectSettingToBeRead> = yield select(selectSettingToBeRead);
    if (!toBeRead) {
      return;
    }
    yield call(readSetting, toBeRead.deviceOrAxis, toBeRead.setting, toBeRead.units ?? null);
  }
}

function* readSetting(deviceOrAxis: EntityKey, setting: string, units: Units | null): SagaIterator {
  try {
    const settings: CommonSettings = yield call(getSettings, deviceOrAxis);
    const settingState: Nullable<SettingsEntry> = yield select(selectSettingState(deviceOrAxis, setting));
    if (settingState == null) {
      yield put(actions.readSettingDoneErr(deviceOrAxis, setting, 'Invalid state'));
      return;
    }

    const type = settingState.type;
    if (type === 'string' || type === 'network') {
      const stringValue: string = yield call([settings, settings.getString], setting);
      yield put(actions.readSettingDone(deviceOrAxis, setting, { type, value: stringValue, mode: 'display' }));
    } else {
      const value: number = yield call([settings, settings.get], setting, units ?? Units.NATIVE);
      yield put(actions.readSettingDone(deviceOrAxis, setting, { type, mode: 'display', value: { value, units: units ?? 'readable' } }));
      if (setting === ACCESS_LEVEL_SETTING) {
        yield call(updateAccessLevelAndReadSettings, deviceOrAxis, value);
      }
    }
  } catch (err) {
    throwUnexpectedError(err);
    let message = err.message ?? String(err);
    if (err instanceof CommandFailedException) {
      if (err.details.responseData === RejectReasons.BADCOMMAND) {
        message = `Setting does not exist on the device. (${RejectReasons.BADCOMMAND})`;
      }
      yield put(actions.readSettingDoneErr(deviceOrAxis, setting, message));
    } else {
      yield put(actions.clearSettingQueue());
      yield put(actions.setError(deviceOrAxis, message));
    }
  }
}

function* updateSetting({
  payload: { deviceOrAxis, setting, update }
}: Action<ActionsToPayloads[ActionTypes.UPDATE_SETTING]>): SagaIterator {
  if (update.mode === 'write') {
    if (update.type === 'number') {
      yield put(actions.writeSetting(deviceOrAxis, setting, update.value.value, update.value.units));
    } else if (update.type === 'string') {
      yield put(actions.writeStringSetting(deviceOrAxis, setting, update.value));
    }
  }
}

function* writeSetting({
  payload: { deviceOrAxis, setting, value, units }
}: Action<ActionsToPayloads[ActionTypes.WRITE_SETTING]>): SagaIterator {
  try {
    const settings: CommonSettings = yield call(getSettings, deviceOrAxis);
    yield call([settings, settings.set], setting, value, units ?? Units.NATIVE);

    yield put(actions.writeSettingDone(deviceOrAxis, setting));
    yield put(actions.readSetting(deviceOrAxis, setting));
    yield spawn(customSettingAction, setting, deviceOrAxis);
  } catch (err) {
    throwUnexpectedError(err);
    let message = err.message ?? String(err);
    if (err instanceof CommandFailedException) {
      if (err.details.responseData === RejectReasons.BADDATA) {
        message = `The provided value is incorrect. (${RejectReasons.BADDATA})`;
      }
    } else if (setting === 'comm.address' && err instanceof InvalidDataException) {
      yield spawn(reloadAfterAddressChange, deviceOrAxis, value as number);
      message = 'Device no longer valid. Reloading...';
    }

    yield put(actions.writeSettingDone(deviceOrAxis, setting, message));
  }
}

function* writeStringSetting({
  payload: { deviceOrAxis, setting, value }
}: Action<ActionsToPayloads[ActionTypes.WRITE_STRING_SETTING]>): SagaIterator {
  try {
    const settings: CommonSettings = yield call(getSettings, deviceOrAxis);
    yield call([settings, settings.setString], setting, value);

    yield put(actions.writeSettingDone(deviceOrAxis, setting));
    yield put(actions.readSetting(deviceOrAxis, setting));
    yield spawn(customSettingAction, setting, deviceOrAxis);
  } catch (err) {
    throwUnexpectedError(err);
    let message = err.message ?? String(err);
    if (err instanceof CommandFailedException) {
      if (err.details.responseData === RejectReasons.BADDATA) {
        message = `The provided value is incorrect. (${RejectReasons.BADDATA})`;
      }
    }

    yield put(actions.writeSettingDone(deviceOrAxis, setting, message));
  }
}

function* customSettingAction(setting: string, deviceOrAxisKey: EntityKey): SagaIterator {
  if (REFRESH_DEVICES_SETTINGS.includes(setting)) {
    const connectionKey = extractConnectionKey(deviceOrAxisKey);
    const devices: RT<typeof reloadDevices> = yield call(reloadDevices, connectionKey);
    if (devices) {
      yield put(actions.selectDeviceOrAxis(deviceOrAxisKey));
    }
  }

  for (const group of DEPENDENT_SETTINGS) {
    if (group.has(setting)) {
      const toRead = Array.from(group).filter(other => other !== setting);
      yield put(actions.readSetting(deviceOrAxisKey, toRead));
    }
  }
}

function* reloadAfterAddressChange(deviceOrAxisKey: EntityKey, newAddress: number): SagaIterator {
  const connectionKey = extractConnectionKey(deviceOrAxisKey);
  const devices: RT<typeof reloadDevices> = yield call(reloadDevices, connectionKey);
  if (devices?.find(device => device.address === newAddress)) {
    yield put(actions.selectDeviceOrAxis(makeDeviceKey(connectionKey, newAddress)));
  }
}

function* configureNetwork({ payload: { config } }: Action<ActionsToPayloads[ActionTypes.CONFIGURE_NETWORK]>): SagaIterator {
  try {
    const current: NonNullable<RT<typeof selectCurrentDevice>> = yield select(selectCurrentDevice);
    const device: ascii.Device = yield call(getDevice, current.key);
    let cmd = 'comm en ipv4 dhcp';
    if (!config.dhcp) {
      cmd = `comm en ipv4 static ${config.IPAddress} ${config.subnetMask} ${config.defaultGateway}`;
    }
    yield call([device, device.genericCommand], cmd);

    const connection: ReturnType<typeof selectSelectedConnection> = yield select(selectSelectedConnection);
    if (connection?.type === 'tcp/ip') {
      yield delay(environment.isTest ? 0 : NETWORK_COMMAND_DELAY_TCP_IP);
    } else if (connection?.type === 'serial') {
      yield delay(environment.isTest ? 0 : NETWORK_COMMAND_DELAY_SERIAL);
    }

    yield put(actions.readSetting(current.key, NETWORK_SETTINGS));
    yield put(actions.configureNetworkDone());
    yield put(actions.setCustomDialog(null));
  } catch (err) {
    throwUnexpectedError(err);
    const message = err.message ?? String(err);
    yield put(actions.configureNetworkDone(message));
  }
}

function* updateAccessLevelAndReadSettings(deviceOrAxis: EntityKey, newAccessLevel: number): SagaIterator {
  const current: RT<typeof selectCurrent> = yield select(selectCurrent);
  const isCurrentDeviceOrAxis = current?.key === deviceOrAxis || tryExtractDeviceKey(current?.key ?? null) === deviceOrAxis;
  if (!isCurrentDeviceOrAxis) {
    yield put(actions.updateCurrentAccessLevel(deviceOrAxis, newAccessLevel));
    return;
  }

  const oldReadableSettings: string[] = yield select(selectReadableSettingIDs);
  yield put(actions.updateCurrentAccessLevel(deviceOrAxis, newAccessLevel));
  const newReadableSettings: string[] = yield select(selectReadableSettingIDs);

  const newSettingsToRead = _.difference(newReadableSettings, oldReadableSettings);
  yield put(actions.readSetting(current!.key, newSettingsToRead));
}

function* readAllSettings(): SagaIterator {
  const readableSettings: string[] = yield select(selectReadableSettingIDs);
  const deviceOrAxis: RT<typeof selectCurrent> = yield select(selectCurrent);
  if (deviceOrAxis == null) { return }

  const toRead = readableSettings.filter(settingId => {
    const { mode } = deviceOrAxis.settings[settingId];
    return mode === 'initialize' || mode === 'display';
  });
  yield put(actions.readSetting(deviceOrAxis.key, toRead));
}

type SettingVisibility = ZaberApi.DeviceInfo['settings']['rows'][0]['visibility'];
const ALLOWED_VISIBILITY: SettingVisibility[] = ['always', 'advanced'];

function defaultSettingValue(row: ZaberApi.DeviceInfo['settings']['rows'][number]): SettingValue {
  if (NETWORK_SETTINGS.includes(row.name)) {
    return { type: 'network', value: null, mode: 'initialize' };
  } else if (STRING_SETTING_TYPES.includes(row.param_type)) {
    return { type: 'string', value: null, mode: 'initialize' };
  }
  return { type: 'number', value: { value: null, units: 'readable' }, mode: 'initialize' };
}

function makeSettingDataForProduct(
  settings: ZaberApi.DeviceInfo['settings'],
): SettingsEntryInfo[] {
  return settings.rows
    .filter(row => ALLOWED_VISIBILITY.includes(row.visibility))
    .map((settingRow): SettingsEntryInfo => ({
      id: settingRow.name,
      name: settingRow.name,
      ...defaultSettingValue(settingRow),
      readAccessLevel: settingRow.read_access_level ?? 0,
      writeAccessLevel: settingRow.write_access_level,
      defaultValue: settingRow.default_value ?? null,
      meta: settingsMetadata[settingRow.name] ?? null,
    }));
}
