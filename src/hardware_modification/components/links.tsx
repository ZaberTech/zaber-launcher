import React from 'react';

import { ExternalLink } from '../../components';

export const troubleshootingWiki = <ExternalLink url=" https://www.zaber.com/w/Troubleshooting/Modifying_Hardware">
  troubleshooting documentation
</ExternalLink>;

export const supportLink = <ExternalLink url=" https://www.zaber.com/contact">Customer Support</ExternalLink>;
