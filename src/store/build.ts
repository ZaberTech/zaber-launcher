import { AnyAction, applyMiddleware, CombinedState, compose, createStore, Middleware, PreloadedState, Store, StoreEnhancer } from 'redux';
import reduxLogger from 'redux-logger';
import createSagaMiddleware, { SagaMiddleware } from 'redux-saga';
import storeFreeze from 'redux-freeze';
import { routerMiddleware } from 'connected-react-router';
import type { RecursivePartial } from '@zaber/toolbox';
import _ from 'lodash';

import { environment } from '../environment';
import { handledBySagaErrorSymbol } from '../utils';
import { handleUnexpectedError } from '../errors';
import { getLogger } from '../log';

import { rootReducer, State } from './reducer';
import { rootSaga } from './saga';
import { browserHistory } from './history';
import { sentryReduxEnhancer } from './sentry';
import { shouldLogStoreActions } from './testing';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: () => StoreEnhancer;
  }
}

export type AppStore = Store<CombinedState<State>, AnyAction> & { saga: SagaMiddleware  };
type ExtendedError = Error & { [handledBySagaErrorSymbol]?: true };

export const buildStore = (preloadedState?: RecursivePartial<State>): AppStore => {
  const logger = getLogger('redux');

  const sagaMiddleware = createSagaMiddleware({
    onError: (err: ExtendedError, info) => {
      if (err[handledBySagaErrorSymbol]) {
        delete err[handledBySagaErrorSymbol];
        return;
      }
      handleUnexpectedError(err);
      logger.error(err, info);
    },
  });

  const middlewares: Middleware[] = [
    sagaMiddleware,
    routerMiddleware(browserHistory),
  ];
  if (!environment.isProduction) {
    if (!environment.isTest) {
      middlewares.push(reduxLogger);
    } else {
      middlewares.push(() => next => action => {
        if (shouldLogStoreActions()) {
          // eslint-disable-next-line no-console, @typescript-eslint/no-unsafe-member-access
          console.log(`\x1b[33m${action.type}\x1b[0m`, action.payload);
        }
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        return next(action);
      });
    }
    middlewares.push(storeFreeze);
  }

  const store = createStore(
    rootReducer,
    (preloadedState ?? {}) as PreloadedState<State>,
    compose(
      window.__REDUX_DEVTOOLS_EXTENSION__?.() ?? _.identity,
      applyMiddleware(...middlewares),
      sentryReduxEnhancer,
    ),
  ) as AppStore;

  sagaMiddleware.run(rootSaga);
  store.saga = sagaMiddleware;

  return store;
};
