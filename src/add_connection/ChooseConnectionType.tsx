import React, { useState } from 'react';
import { ButtonNext, ButtonRow, NoticeBanner, RadioButton, Text } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';

import { selectIsAuthenticated } from '../cloud';
import { SimulatorMessage } from '../connection_manager/components/SimulatorMessage';

import { actions as actionsDefinition } from './actions';
import { Steps } from './types';
import { selectShowSimulatorNotice } from './selectors';

export const ChooseConnectionType: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const [nextStep, setNextStep] = useState<string>();
  const isAuthenticated = useSelector(selectIsAuthenticated);
  const showSimulatorNotice = useSelector(selectShowSimulatorNotice);

  return (
    <div className="choose-connection-type">
      <Text className="subheading" t={Text.Type.BodyLg}>Choose the type of connection you would like to add:</Text>

      <h4>Device Chain</h4>
      <RadioButton
        name="connection-type"
        value={Steps.CONFIGURE_SERIAL}
        onValueChange={setNextStep}
        checked={nextStep === Steps.CONFIGURE_SERIAL}>
        Serial Port / USB
      </RadioButton>
      <RadioButton
        name="connection-type"
        value={Steps.DISCOVER_MDNS}
        onValueChange={setNextStep}
        checked={nextStep === Steps.DISCOVER_MDNS}>
        Network (TCP/IP)
      </RadioButton>
      <RadioButton
        name="connection-type"
        value={Steps.CONNECT_CLOUD}
        onValueChange={setNextStep}
        checked={nextStep === Steps.CONNECT_CLOUD}>
        Virtual Device (Early Access)
      </RadioButton>

      <h4>Sharing</h4>
      <RadioButton
        name="connection-type"
        value={Steps.DISCOVER_LOCAL_SHARE}
        onValueChange={setNextStep}
        checked={nextStep === Steps.DISCOVER_LOCAL_SHARE}>
        Network Sharing
      </RadioButton>
      {isAuthenticated && <RadioButton
        name="connection-type"
        value={Steps.DISCOVER_CLOUD}
        onValueChange={setNextStep}
        checked={nextStep === Steps.DISCOVER_CLOUD}>
        Cloud Sharing
      </RadioButton>}

      {showSimulatorNotice && <NoticeBanner
        className="simulator-notice"
        type="info"
        closer={() => actions.hideSimulatorNotice()}>
        <SimulatorMessage/>
      </NoticeBanner>}

      <ButtonRow>
        <ButtonNext disabled={!nextStep} onClick={() => actions.changeStep(nextStep as Steps)}>Next</ButtonNext>
      </ButtonRow>
    </div>
  );
};
