/* eslint-disable @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-unsafe-call */

import _ from 'lodash';
import React, { Component, createRef } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { CustomSeriesRenderItemAPI, CustomSeriesRenderItemParams, graphic } from 'echarts';
import ReactECharts, { EChartsOption } from 'echarts-for-react';
import { Units } from '@zaber/motion';
import { Colors } from '@zaber/react-library';

import {
  ZoomPanCollection,
  type ZoomPanSet,
  type ZoomPanState,
  DEFAULT_ZOOM_PAN,
  ChartZoomPanController,
  MouseZoomRegion,
  ZoomEventSingleAxis,
  ZoomEventMultiAxis,
  ZoomEventRectangle
} from '../components';
import { getContainer } from '../container';
import {
  ECHARTS_LEGEND_STYLE,
  ECHARTS_SLIDER_STYLE,
  ECHARTS_X_AXIS_DEFAULTS,
  ECHARTS_Y_AXIS_DEFAULTS,
  ECHARTS_YAXIS_FONT_SIZE
} from '../eChartsCommon';
import { RootState, selectOscilloscope } from '../store';
import { TimeMeasuring } from '../time';
import { getUnitLabel } from '../units';

import { actions as actionDefinitions } from './actions';
import { ProgressDialog } from './ProgressDialog';
import { Channel, Mode, Sample, YAxisId } from './types';
import type { Recording } from './reducers';
import { extractSettingsChannels, findNearestSampleIndex, sortAlphabetic, splitChannelId } from './channels';


const TOP_MARGIN = 5;
const CHART_HEIGHT = 250;
const TIMELINE_MARGIN = 50;
const TIMELINE_HEIGHT = 40;
const LEGEND_HEIGHT = 20;
const LEGEND_MARGIN = 20;
const LEFT_MARGIN = 130;
const RIGHT_MARGIN = 145;
const Y_AXIS_LABEL_MARGIN = 100;
const TOOLTIP_MOUSE_OFFSET = 5;

const chartTop = function(groupIndex: number): number {
  return TOP_MARGIN + groupIndex * (CHART_HEIGHT + TIMELINE_MARGIN + TIMELINE_HEIGHT + TOP_MARGIN + LEGEND_HEIGHT + LEGEND_MARGIN);
};

const timelineTop = function(groupIndex: number): number {
  return chartTop(groupIndex) + CHART_HEIGHT + TIMELINE_MARGIN;
};

const legendTop = function(groupIndex: number): number {
  return timelineTop(groupIndex) + TIMELINE_HEIGHT;
};


const channelTitleWithAxis = (ch: Channel) => `${(ch.axis > 0 ? `Axis ${ch.axis} ` : '')}${ch.title}`;


const formatTooltip = function(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  params: any, channelsBySeriesIndex: Channel[], recording: Recording | null, useConverted: boolean): string {
  if (!recording || !params) {
    return '';
  }

  const _params = Array.isArray(params) ? params : [params];
  if (_params.length < 1) {
    return '';
  }

  const result: string[] = ['<div class="chart-tooltip">'];
  const units = recording.unitMap ?? {};
  const showTimes = recording.mode === Mode.POLLED;

  let time = 0;
  if (_params[0].seriesType === 'custom') {
    time = _params[0].value[1];
  } else {
    time = _params[0].axisValue;
  }

  for (const channel of channelsBySeriesIndex) {
    let unit = getUnitLabel(units[channel.id] ?? Units.NATIVE);
    unit = unit === 'Native' || !useConverted ? '' : unit;

    const seriesData = useConverted ? channel.convertedData : channel.samples;
    const sampleIndex = findNearestSampleIndex(seriesData, time);
    let sampleTime = 'No data';
    let sampleValue = 'No data';
    if (sampleIndex >= 0) {
      const sample = seriesData[sampleIndex];
      if (sample) {
        sampleTime = `${sample.time.toLocaleString()} ms`;
        sampleValue = `${sample.value.toLocaleString()} ${unit}`;
      }
    }

    result.push(`<text class="time helper">${showTimes ? sampleTime : ''}</text>`);
    result.push(`<div class="swatch" style="background: ${channel.color}"></div>`);
    result.push(`<text class="title helper">${channelTitleWithAxis(channel)}</text>`);
    result.push(`<text class="value helper" style="color: ${channel.color}">${sampleValue}</text>`);
    result.push('<br/>');
    result.push(`<text class="subtitle mono">${channel.subtitle}</text>`);
    result.push('<br/>');
  }
  result.push('</div>');

  return result.join('');
};


// Converts digital I/O data to the format used by renderLogicTrace():
// [channel index, start time, end time, previous value, current value]
// We do not combine runs of the same value, because that would cause the
// item - triggered tooltip to show distant values when only digital channels
// are shown. This has a high impact on rendering performance, unfortunately.
function samplesToLogicTraces(channelIndex: number, samples: Sample[]): number[][] {
  const result = [];
  if (samples.length) {
    const s = samples[0];
    result.push([channelIndex, s.time, s.time, s.value, s.value]);
  }

  for (let i = 1; i < samples.length; i++) {
    const prev = samples[i - 1];
    const cur = samples[i];
    result[result.length - 1][2] = cur.time;
    result.push([channelIndex, cur.time, cur.time, prev.value, cur.value]);
  }

  return result;
}


function renderLogicTrace(params: CustomSeriesRenderItemParams, api: CustomSeriesRenderItemAPI, color: string) {
  const categoryIndex = api.value(0);
  const start = api.coord([api.value(1), categoryIndex]);
  const end = api.coord([api.value(2), categoryIndex]);
  const prevValue = api.value(3) as number;
  const value = api.value(4) as number;
  const height = (api.size!([0, 1]) as number[])[1] * 0.5;
  const coordSys = params.coordSys as unknown as { // echarts doesn't expose its RectLike type.
      x: number;
      y: number;
      width: number;
      height: number;
  };

  const clippedLines = graphic.clipPointsByRect([
    [start[0], start[1] - prevValue * height + height / 2],
    [start[0], start[1] - value * height + height / 2],
    [end[0], end[1] - value * height + height / 2],
  ], {
    x: coordSys.x,
    y: coordSys.y,
    width: coordSys.width,
    height: coordSys.height
  });

  return ({
    type: 'polyline',
    shape: {
      points: clippedLines,
    },
    style: {
      fill: null,
      lineWidth: 2,
      stroke: color,
    },
  });
}

export interface ChartScreenshot {
  saveToImage: () => void;
}


interface Props {
  actions: typeof actionDefinitions;
  activeRecording: Recording | null;
  recordings: Recording[];
  selectedRecordingIndex: number;
  unitConversionEnabled: boolean;
  initialZoom?: ZoomPanSet;
  onZoomChanged: (zoom: ZoomPanSet) => void;
}


class ChartsBase extends Component<Props> implements ChartScreenshot {
  private viewPort = createRef<HTMLDivElement>();
  private echarts = createRef<ReactECharts>();
  private mouseQuadrant = [false, false];

  private zoomPan = new ZoomPanCollection(this.notifyZoomPan.bind(this));
  private zoomController = new ChartZoomPanController(
    this.zoomPan,
    ChartsBase.determineZoomRegion,
    ChartsBase.getMouseChartIndex,
    ChartsBase.getNormalizedMouseCoords,
    this.getZoomRangeFromData.bind(this));

  // This stores the last known range of the data associated with each chart axis (identified by zoom and pan control ID).
  // Unlike normal zoom and pan, the start and end values can be anything.
  // This exists because the ECharts rectangle zoom returns ranges in terms of data values instead of percentages.
  private dataRange: ZoomPanSet = {};


  private readonly baseOptions: EChartsOption = {
    animation: false,
    tooltip: {
      trigger: 'axis',
      transitionDuration: 0,
      order: 'seriesAsc',
      axisPointer: {
        type: 'cross',
        animation: false,
        lineStyle: {
          color: Colors.zaberGrey,
          type: 'solid',
        },
        crossStyle: {
          color: Colors.zaberGrey,
          type: 'solid',
        },
      },
      position: (point: [number, number], _1: unknown, _2: unknown, _3: unknown, size: { contentSize: [number, number] }) =>
        point.map((coord, i) => this.mouseQuadrant[i]
          ? coord - (size.contentSize[i] + TOOLTIP_MOUSE_OFFSET)
          : coord + TOOLTIP_MOUSE_OFFSET),
    },
    axisPointer: {
      link: [{
        xAxisIndex: 'all'
      }],
      label: {
        backgroundColor: '#777'
      },
    },
  };


  constructor(props: Props) {
    super(props);
    if (props.initialZoom) {
      this.zoomPan.setState(props.initialZoom);
    }
  }


  componentDidMount(): void {
    if (this.echarts.current) {
      const echarts = this.echarts.current.getEchartsInstance();
      const renderer = echarts?.getZr();
      renderer?.on('contextmenu', this.resetZoom);
      window.addEventListener('keydown', this.onKeyDown);
      window.addEventListener('keyup', this.onKeyUp);
    }
  }


  componentWillUnmount(): void {
    window.removeEventListener('keydown', this.onKeyDown);
    window.removeEventListener('keyup', this.onKeyUp);
  }


  onKeyDown = (event: KeyboardEvent) => {
    if (event.key === 'Shift' && !this.zoomController.enableMouseZoomAndPan) {
      event.preventDefault();
      this.zoomController.enableMouseZoomAndPan = true;
    }
  };


  onKeyUp = (event: KeyboardEvent) => {
    if (event.key === 'Shift' && this.zoomController.enableMouseZoomAndPan) {
      event.preventDefault();
      this.zoomController.enableMouseZoomAndPan = false;
    }
  };


  onMouseDownCapture = () => {
    // Activate rectangle select zoom when not in zoom & pan mode.
    if (!this.zoomController.enableMouseZoomAndPan) {
      this.echarts.current?.getEchartsInstance().dispatchAction({
        type: 'takeGlobalCursor',
        key: 'dataZoomSelect',
        dataZoomSelectActive: true,
      });
    }
  };


  onMouseMove = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const canvasBounds = event?.currentTarget?.getBoundingClientRect();
    if (canvasBounds && this.viewPort.current) {
      const x = event.clientX - canvasBounds.left;
      const y = event.clientY - canvasBounds.top;
      this.mouseQuadrant = [(x > this.viewPort.current.clientWidth / 2), (y > this.viewPort.current.clientHeight / 2)];
      this.zoomController.onMouseMove(event, this.viewPort.current, LEFT_MARGIN + RIGHT_MARGIN, CHART_HEIGHT);
    }
  };


  onMouseWheel = (event: React.WheelEvent) => {
    this.zoomController.onMouseWheel(event);
  };


  private notifyZoomPan(changedZoomIds: string[]): void {
    if (changedZoomIds.length) {
      this.echarts.current?.getEchartsInstance()?.dispatchAction({
        type: 'dataZoom',
        batch: changedZoomIds.map(id => ({
          dataZoomId: id,
          ...this.zoomPan.get(id),
        })),
      });
    }
  }


  private static determineZoomRegion(x: number, y: number, width: number): MouseZoomRegion {
    let yMod = (y - TOP_MARGIN);
    yMod = yMod % (CHART_HEIGHT + TIMELINE_MARGIN + TIMELINE_HEIGHT + TOP_MARGIN + LEGEND_HEIGHT + LEGEND_MARGIN);

    let zoomRegion = MouseZoomRegion.NONE;
    if (yMod < CHART_HEIGHT) {
      if (x < LEFT_MARGIN) {
        zoomRegion = MouseZoomRegion.LEFTAXIS;
      } else if (x > width - RIGHT_MARGIN) {
        zoomRegion = MouseZoomRegion.RIGHTAXIS;
      } else {
        zoomRegion = MouseZoomRegion.CHART;
      }
    } else if (yMod >= CHART_HEIGHT + TIMELINE_MARGIN && yMod < CHART_HEIGHT + TIMELINE_MARGIN + TIMELINE_HEIGHT) {
      if (x >= LEFT_MARGIN && x <= width - RIGHT_MARGIN) {
        zoomRegion = MouseZoomRegion.TIMELINE;
      }
    }

    return zoomRegion;
  }


  private static getNormalizedMouseCoords(x: number, y: number, width: number): { x: number; y: number } {
    const yMod = (y - TOP_MARGIN) % (CHART_HEIGHT + TIMELINE_MARGIN + TIMELINE_HEIGHT + TOP_MARGIN + LEGEND_HEIGHT + LEGEND_MARGIN);
    return {
      x: (x - LEFT_MARGIN) / (width - LEFT_MARGIN - RIGHT_MARGIN),
      y: 1 - yMod / CHART_HEIGHT,
    };
  }


  private static getMouseChartIndex(y: number): number {
    return Math.floor((y - TOP_MARGIN) /
      (CHART_HEIGHT + TIMELINE_MARGIN + TIMELINE_HEIGHT + TOP_MARGIN + LEGEND_HEIGHT + LEGEND_MARGIN));
  }


  private getZoomRangeFromData(start: number, end: number, axisId: string): ZoomPanState {
    const dataRange = this.dataRange[axisId];
    if (!dataRange) {
      return DEFAULT_ZOOM_PAN;
    }

    const diff = dataRange.end - dataRange.start;
    return {
      start: 100 * (start - dataRange.start) / diff,
      end: 100 * (end - dataRange.start) / diff,
    };
  };


  onDataZoom = (event: ZoomEventSingleAxis | ZoomEventMultiAxis | ZoomEventRectangle) => {
    this.zoomController.onDataZoom(event);
    this.props.onZoomChanged?.(this.zoomPan.getState());
  };


  resetZoom = () => {
    this.echarts.current?.getEchartsInstance()?.dispatchAction({
      type: 'dataZoom',
      batch: this.zoomPan.getIds(() => true).map(id => ({
        dataZoomId: id,
        ...DEFAULT_ZOOM_PAN,
      })),
    });
  };


  saveToImage = () => {
    const data = this.echarts.current?.getEchartsInstance()?.getDataURL({ type: 'png' });
    if (data) {
      this.props.actions.saveRecordingToImage('png', data);
    }
  };


  render(): React.ReactNode {
    const { activeRecording, recordings, selectedRecordingIndex, unitConversionEnabled } = this.props;

    const recording = selectedRecordingIndex >= 0 ? recordings[selectedRecordingIndex] : activeRecording;
    let channelsBySeriesIndex: Channel[] = [];
    const addedSettingNames =
      extractSettingsChannels(recording?.channels ?? []).map(ch => `${ch.name}${(ch.axis > 0 ? ` (axis ${ch.axis})` : '')}`);

    let zoomId: string;
    const options: EChartsOption = {
      ...this.baseOptions,
      tooltip: activeRecording || this.zoomController.enableMouseZoomAndPan ? null : {
        ...this.baseOptions.tooltip,
        formatter: (params: unknown) => formatTooltip(params, channelsBySeriesIndex, recording, unitConversionEnabled),
      },
      dataZoom: [],
      xAxis: [],
      yAxis: [],
      dataset: [],
      series: [],
      grid: [],
      legend: [],
      // Create rectangle select zoom tool when not in pan & zoom mode, and hide it.
      toolbox: this.zoomController.enableMouseZoomAndPan ? undefined : {
        feature: {
          dataZoom: {
            brushStyle: {
              color: `${Colors.blue}80`,
            },
            icon: {
              // Hack to not display icons.
              zoom: 'path://',
              back: 'path://',
            },
          },
        },
      }
    };


    const minTime = Math.floor(_.min((recording?.channels ?? []).filter(ch => ch.samples.length > 0).map(ch => ch.samples[0].time)) ?? 0);
    const maxTime = Math.ceil(
      _.max((recording?.channels ?? []).filter(ch => ch.samples.length > 0).map(ch => _.last(ch.samples)!.time)) ?? 1000);
    this.dataRange[this.zoomController.makeControlId(0, 'x', 'bottom', 0)] = { start: minTime, end: maxTime };
    const channelMap = _.keyBy(recording?.channels ?? [], 'id');
    recording?.groups?.forEach((group, groupIndex) => {
      const groupChannels = group.channelIds.map(id => channelMap[id]).sort((a, b) => sortAlphabetic(a.title, b.title));
      const maxSamplesInThisGroup = _.max(groupChannels.map(ch => ch.samples.length)) ?? 0;
      const isDigitalGroup = _.some(groupChannels, ch => {
        const { name } = splitChannelId(ch.id);
        return (name === 'di' || name === 'do');
      });

      options.grid.push({
        top: chartTop(groupIndex),
        height: CHART_HEIGHT,
        left: LEFT_MARGIN,
        right: RIGHT_MARGIN,
        // For digital traces, change tooltip triggering mode because 'axis' doesn't work.
        // This isn't a perfect solution but better than nothing.
        tooltip: isDigitalGroup ? { trigger: 'item' } : undefined,
      });

      options.legend.push({
        ...ECHARTS_LEGEND_STYLE,
        top: legendTop(groupIndex),
        data: groupChannels.map(ch => ({
          name: channelTitleWithAxis(ch),
          color: ch.color,
        })),
      });

      options.xAxis.push({
        ...ECHARTS_X_AXIS_DEFAULTS,
        gridIndex: groupIndex,
        min: minTime,
        max: maxTime,
      });

      zoomId = this.zoomController.makeControlId(groupIndex, 'x', 'bottom', 0);
      options.dataZoom = options.dataZoom.concat([{
        ...ECHARTS_SLIDER_STYLE,
        ...this.zoomPan.get(zoomId),
        id: zoomId,
        type: 'slider',
        filterMode: 'none',
        brushSelect: false,
        showDataShadow: !isDigitalGroup,
        xAxisIndex: groupIndex,
        top: timelineTop(groupIndex),
        throttle: 0,
        selectedDataBackground: {
          lineStyle: {
            color: groupChannels[0].color,
            width: 2,
            opacity: 1,
          },
          areaStyle: {
            color: groupChannels[0].color,
            opacity: 0.25,
          }
        },
      }]);


      const dataSetIndex = options.dataset.length;
      options.dataset = options.dataset.concat(groupChannels.map(channel => ({
        source: (unitConversionEnabled ? channel.convertedData : channel.samples).map(sample => ([sample.time, sample.value])),
      })));

      let usedSplitLine = false;
      let leftYAxisIndex: number | undefined;
      let rightYAxisIndex: number | undefined;
      const leftYAxisChannels = groupChannels.filter(ch => ch.yAxis === YAxisId.LEFT);
      if (leftYAxisChannels.length) {
        leftYAxisIndex = options.yAxis.length;
        zoomId = this.zoomController.makeControlId(groupIndex, 'y', 'left', leftYAxisIndex ?? 0);
        usedSplitLine = true;
        const [label, highlight] = this.getYAxisLabel(recording, leftYAxisChannels, zoomId, unitConversionEnabled);
        let yAxisOptions = {
          ..._.merge(_.cloneDeep(ECHARTS_Y_AXIS_DEFAULTS), {
            nameTextStyle: {
              rich: {
                u: {
                  backgroundColor: highlight,
                }
              },
            },
          }),
          nameGap: Y_AXIS_LABEL_MARGIN - ECHARTS_YAXIS_FONT_SIZE * label.split('\n').length,
          gridIndex: groupIndex,
          name: label,
        };

        if (isDigitalGroup) {
          yAxisOptions = _.merge(yAxisOptions, {
            name: null,
            data: leftYAxisChannels.map(ch => ch.name), // Change to category mode and set Y labels.
            axisLabel: {
              interval: 0, // Forces all channel labels to be displayed. Otherwise the first and last are dropped.
              color: Colors.zaberGrey,
              fontWeight: 'bold',
              fontFamily: 'Roboto',
              fontSize: ECHARTS_YAXIS_FONT_SIZE,
            },
            axisTick: {
              alignWithLabel: true,
            },
          });
        }

        options.yAxis.push(yAxisOptions);


        options.dataZoom.push({
          ...ECHARTS_SLIDER_STYLE,
          ...(isDigitalGroup ? { start: 0, end: 100 } : this.zoomPan.get(zoomId)),
          id: zoomId,
          type: 'slider',
          filterMode: 'none',
          orient: 'vertical',
          brushSelect: false,
          showDataShadow: false,
          left: 10,
          width: 15,
          yAxisIndex: leftYAxisIndex,
          throttle: 0,
          show: !isDigitalGroup, // No vertical zooming for digital channels, but we still need this object.
        });

        this.dataRange[zoomId] = {
          start: _.min(leftYAxisChannels.map(
            ch => _.min((unitConversionEnabled ? ch.convertedData : ch.samples).map(point => point.value)))) ?? 0,
          end: _.max(leftYAxisChannels.map(
            ch => _.max((unitConversionEnabled ? ch.convertedData : ch.samples).map(point => point.value)))) ?? 100,
        };
      }

      const rightYAxisChannels = groupChannels.filter(ch => ch.yAxis === YAxisId.RIGHT);
      if (rightYAxisChannels.length) {
        if (isDigitalGroup) {
          throw new Error('Displaying digital channels on the right Y axis is not supported.');
        }

        rightYAxisIndex = options.yAxis.length;
        zoomId = this.zoomController.makeControlId(groupIndex, 'y', 'right', rightYAxisIndex ?? 0);
        const [label, highlight] = this.getYAxisLabel(recording, rightYAxisChannels, zoomId, unitConversionEnabled);
        options.yAxis.push({
          ..._.merge(_.cloneDeep(ECHARTS_Y_AXIS_DEFAULTS), {
            nameTextStyle: {
              rich: {
                u: {
                  backgroundColor: highlight,
                }
              },
            },
          }),
          nameGap: Y_AXIS_LABEL_MARGIN - ECHARTS_YAXIS_FONT_SIZE * label.split('\n').length,
          gridIndex: groupIndex,
          name: label,
          position: 'right',
          splitLine: {
            ...ECHARTS_Y_AXIS_DEFAULTS.splitLine,
            show: !usedSplitLine,
          },
        });

        options.dataZoom.push({
          ...ECHARTS_SLIDER_STYLE,
          ...this.zoomPan.get(zoomId),
          id: zoomId,
          type: 'slider',
          filterMode: 'none',
          orient: 'vertical',
          brushSelect: false,
          showDataShadow: false,
          right: 10,
          width: 15,
          yAxisIndex: rightYAxisIndex,
          throttle: 0,
        });

        this.dataRange[zoomId] = {
          start: _.min(rightYAxisChannels.map(
            ch => _.min((unitConversionEnabled ? ch.convertedData : ch.samples).map(point => point.value)))) ?? 0,
          end: _.max(rightYAxisChannels.map(
            ch => _.max((unitConversionEnabled ? ch.convertedData : ch.samples).map(point => point.value)))) ?? 100,
        };
      }

      if (maxSamplesInThisGroup > 0) {
        if (isDigitalGroup) {
          options.series = options.series.concat(groupChannels.map((channel, chIndex) => ({
            name: channelTitleWithAxis(channel),
            type: 'custom',
            xAxisIndex: groupIndex,
            yAxisIndex: channel.yAxis === YAxisId.RIGHT ? rightYAxisIndex : leftYAxisIndex,
            datasetIndex: dataSetIndex + chIndex,
            data: samplesToLogicTraces(chIndex, channel.samples),
            encode: { x: [1, 2], y: [3, 4], tooltip: [0, 4] }, // See samplesToLogicTraces().
            color: [channel.color],
            renderItem: (params: CustomSeriesRenderItemParams, api: CustomSeriesRenderItemAPI) =>
              renderLogicTrace(params, api, channel.color),
          })));
        } else {
          options.series = options.series.concat(groupChannels.map((channel, chIndex) => ({
            name: channelTitleWithAxis(channel),
            type: 'line',
            xAxisIndex: groupIndex,
            yAxisIndex: channel.yAxis === YAxisId.RIGHT ? rightYAxisIndex : leftYAxisIndex,
            datasetIndex: dataSetIndex + chIndex,
            color: [channel.color],
            symbol: maxSamplesInThisGroup > 150 ? 'none' : 'circle',
            symbolSize: 6,
          })));
        }

        channelsBySeriesIndex = channelsBySeriesIndex.concat(groupChannels);
      }
    });

    // Display an empty graph if there is no data yet.
    if (!options.grid.length) {
      options.grid.push({
        top: TOP_MARGIN,
        height: CHART_HEIGHT,
      });

      options.xAxis.push({
        ...ECHARTS_X_AXIS_DEFAULTS,
        min: 0,
        max: 1000,
      });

      options.yAxis.push({
        name: addedSettingNames.length ? addedSettingNames[0] : '',
        ...ECHARTS_Y_AXIS_DEFAULTS,
        nameGap: 50,
        min: 0,
        max: 8,
      });

      options.dataZoom = [];
    }

    // Disable mouse events on the chart during recording to work around an ECharts crash.
    const style: React.CSSProperties = { width: '100%', height: chartTop(recording?.groups?.length ?? 1) + TOP_MARGIN + LEGEND_HEIGHT };
    if (activeRecording) {
      style.pointerEvents = 'none';
    }

    return <div className="chart-area"
      ref={this.viewPort}
      onMouseDownCapture={this.onMouseDownCapture}
      onMouseMove={this.onMouseMove}
      onWheel={this.onMouseWheel}>
      {activeRecording && activeRecording.mode !== Mode.POLLED && <ProgressDialog/>}

      <ReactECharts
        ref={this.echarts}
        option={options}
        lazyUpdate={true}
        notMerge={true}
        style={style}
        opts={{
          renderer: 'canvas'
        }}
        onEvents={{
          dataZoom: this.onDataZoom,
        }}/>
    </div>;
  }

  private lastUnits: Record<string, { unit: string; lastChange: number }> = {};

  private getYAxisLabel = (recording: Recording, channels: Channel[], id: string, unitConversionEnabled: boolean): [string, string] => {
    if (!recording || !channels) {
      return ['', '#00000000'];
    }

    const channelNames = channels.map(ch => channelTitleWithAxis(ch)).join(',\n');
    if (unitConversionEnabled) {
      const time = getContainer().get(TimeMeasuring);
      const unit = recording.unitMap[channels[0].id];
      if (this.lastUnits[id]?.unit !== unit) {
        this.lastUnits[id] = { unit, lastChange: time.now() };
      }

      const fade = Math.round(Math.max(0, 255 - (time.now() - this.lastUnits[id].lastChange) / 5));
      return [`${channelNames} {u|(${getUnitLabel(unit)})}`, `${Colors.orange}${fade.toString(16).padStart(2, '0')}`];
    }

    return [channelNames, '#00000000'];
  };
}


export const Charts = connect(
  (state: RootState): Omit<Props, 'actions' | 'initialZoom' | 'onZoomChanged'> => {
    const oscilloscope = selectOscilloscope(state);
    return {
      activeRecording: oscilloscope.activeRecording,
      recordings: oscilloscope.recordings,
      selectedRecordingIndex: oscilloscope.selectedRecordingIndex,
      unitConversionEnabled: oscilloscope.unitConversionEnabled,
    };
  },
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionDefinitions, dispatch),
  }),
  null,
  { forwardRef: true }
)(ChartsBase);
