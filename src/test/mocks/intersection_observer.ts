import _ from 'lodash';

export class IntersectionObserverMockBase {
  public readonly elements: HTMLElement[] = [];
  get root() {
    return this.init.root;
  }
  constructor(readonly callback: IntersectionObserverCallback, readonly init: IntersectionObserverInit) { }

  disconnect = jest.fn();
  observe = jest.fn<void, [HTMLElement]>().mockImplementation(element => this.elements.push(element));
  unobserve = jest.fn<void, [HTMLElement]>().mockImplementation(element => _.remove(this.elements, element));
  takeRecords = jest.fn<IntersectionObserverEntry[], []>().mockReturnValue([]);

  callCallback(entries: Partial<IntersectionObserverEntry>[]) {
    this.callback(entries as IntersectionObserverEntry[], this as unknown as IntersectionObserver);
  }
}
