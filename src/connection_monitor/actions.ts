import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

export enum ActionTypes {
  START = 'CONNECTION_MONITOR_START',
  STOP = 'CONNECTION_MONITOR_STOP',
  MONITOR_DATA = 'CONNECTION_MONITOR_MONITOR_DATA',
  MONITOR_ERR = 'CONNECTION_MONITOR_MONITOR_ERR',
  UPDATE_DEVICES_FF = 'CONNECTION_MONITOR_UPDATE_DEVICES_FF',
}

export interface ActionsToPayloads {
  [ActionTypes.START]: { connectionKey: EntityKey };
  [ActionTypes.STOP]: { connectionKey: EntityKey };
  [ActionTypes.MONITOR_DATA]: { connectionKey: EntityKey; detectedDevices: EntityKey[] };
  [ActionTypes.MONITOR_ERR]: { connectionKey: EntityKey;  error: string };
  [ActionTypes.UPDATE_DEVICES_FF]: { connectionKey: EntityKey; deviceKeys: EntityKey[] };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  start: (connectionKey: EntityKey) => buildAction(ActionTypes.START, { connectionKey }),
  stop: (connectionKey: EntityKey) => buildAction(ActionTypes.STOP, { connectionKey }),
  monitorData: (connectionKey: EntityKey, detectedDevices: EntityKey[]) =>
    buildAction(ActionTypes.MONITOR_DATA, { connectionKey, detectedDevices }),
  monitorErr: (connectionKey: EntityKey, error: string) =>
    buildAction(ActionTypes.MONITOR_ERR, { connectionKey, error }),
  updateDevicesFF: (connectionKey: EntityKey, deviceKeys: EntityKey[]) =>
    buildAction(ActionTypes.UPDATE_DEVICES_FF, { connectionKey, deviceKeys }),
};
