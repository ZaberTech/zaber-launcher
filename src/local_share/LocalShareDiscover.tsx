import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Button, Loader, Text } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { NewRouters } from '../connection_manager';

import { selectDiscovery, selectDiscoveryCandidates } from './selectors';
import { actions as actionsDefinition } from './actions';

interface Props {
  onRouterAdded: () => void;
}

export const LocalShareDiscover: React.FC<Props> = ({ onRouterAdded }) => {
  const { discovering, error } = useSelector(selectDiscovery);
  const candidates = useSelector(selectDiscoveryCandidates);
  const actions = useActions(actionsDefinition);
  useEffect(() => {
    actions.getCurrentConfig();
    actions.discover();
    return () => { actions.discoverCancel() };
  }, []);

  return (
    <div className="share-discover">
      <Text className="subheading" t={Text.Type.BodyLg}>
        The following are Zaber Launcher instances that we found on your Local Area Network.
        If you cannot see the instance you are looking for, try `Connect Manually`.
      </Text>

      <div className="search">
        {discovering && <><Loader/>&ensp;Discovering...</>}
        {!discovering && <Button color="grey" onClick={actions.discover}>Discover Again</Button>}
        {error && <>Error discovering network: {error}</>}
      </div>

      <NewRouters routers={candidates} onAdded={onRouterAdded}/>
    </div>
  );
};
