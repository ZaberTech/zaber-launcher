import { hsv } from 'color-convert';
import { packRGBFromArray } from '@zaber/toolbox';


const fixedColors: { h: number; s: number; v: number }[] = [
  // Bright
  { h: 0, s: 100, v: 100 },
  { h: 30, s: 100, v: 100 },
  { h: 80, s: 100, v: 100 },
  { h: 150, s: 100, v: 100 },
  { h: 180, s: 100, v: 100 },
  { h: 200, s: 100, v: 100 },
  { h: 260, s: 100, v: 100 },
  { h: 300, s: 100, v: 100 },
  { h: 330, s: 100, v: 100 },
  // Pastels
  { h: 0, s: 50, v: 100 },
  { h: 30, s: 50, v: 100 },
  { h: 200, s: 50, v: 100 },
  { h: 260, s: 50, v: 100 },
  { h: 300, s: 50, v: 100 },
  // Dark
  { h: 0, s: 100, v: 70 },
  { h: 150, s: 100, v: 70 },
  { h: 200, s: 100, v: 70 },
  { h: 300, s: 100, v: 70 },
  // Greys
  { h: 0, s: 0, v: 1 },
  { h: 0, s: 0, v: 50 },
];

/**
 * Generates a bunch of predefined distinct colors.
 * Output colors are packed in 0x00RRGGBB format.
 */
export function generateColorWheels(): number[] {
  const result: number[] = [];

  for (const { h, s, v } of fixedColors) {
    result.push(packRGBFromArray(hsv.rgb([h, s, v])));
  }

  return result;
}


/** Generates a color from a string. Usually ugly and indistinct. */
export function hashStringToColor(str: string): number {
  let hash = str.split('').reduce((a, b) => (((a << 5) - a) + b.charCodeAt(0)) | 0, 0) & 0x7FFFFFFF;
  const hue = hash % 360;
  hash /= 360;
  const sat = hash % 69 + 32;
  hash /= 69;
  const val = hash % 69 + 32;
  hash /= 69;

  return packRGBFromArray(hsv.rgb([hue, sat, val]));
}
