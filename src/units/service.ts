import { inject, injectable } from 'inversify';
import type { SagaIterator } from 'redux-saga';
import { call, select } from 'redux-saga/effects';

import { Log, Logger } from '../app_components';
import { getDeviceOrAxis, ProductInfo, selectAxes, selectDevices } from '../connection_manager';
import { environment } from '../environment';
import { EntityKey, EntityKeyType, extractDeviceKey, getEntityType } from '../keys';
import { selectUnits } from '../store';
import { Nullable, RT, throwUnexpectedError, tryAccess } from '../utils';
import type { CommandTree } from '../terminal/types';

import { UnitFrom, UnitInfo, unsupportedUnitInfo } from './types';
import { dimensionExists, getDefaultDimensionUnits, getUnitInfoOfDimension } from './utils';

@injectable()
export class UnitInfoService {
  private log: Logger;

  constructor(
    @inject(Log) logService: Log,
  ) {
    this.log = logService.getLogger('UnitInfoService');
  }

  private* getProduct(entity: EntityKey): SagaIterator<Nullable<ProductInfo>> {
    const deviceInfos: RT<typeof selectDevices> = yield select(selectDevices);
    switch (getEntityType(entity)) {
      case EntityKeyType.DEVICE: {
        const deviceInfo = tryAccess(deviceInfos, entity);
        return deviceInfo?.product;
      }
      case EntityKeyType.AXIS: {
        const axisInfos: RT<typeof selectAxes> = yield select(selectAxes);
        const deviceInfo = tryAccess(deviceInfos, extractDeviceKey(entity));
        const axisInfo = tryAccess(axisInfos, entity);
        if (axisInfo?.product != null) {
          return {
            ...axisInfo.product,
            conversionTable: { rows: [...(deviceInfo?.product?.conversionTable.rows ?? []), ...axisInfo.product.conversionTable.rows] }
          };
        }
        return null;
      }
      default: throw new TypeError(`Expected a device or axis key. Got ${getEntityType(entity)}: ${entity}`);
    }
  }

  /**
   * Gets the unit info for a product setting
   * @param entity The entity key to get info for
   * @param setting The setting name to get info for
   * @returns The `UnitInfo` of this product setting, or null if the device isn't available
   */
  public* getSettingUnitInfo(entity: EntityKey, setting: string): SagaIterator<UnitInfo | null> {
    try {
      const deviceAscii: RT<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, entity);
      const product: RT<typeof this.getProduct> = yield call([this, this.getProduct], entity);

      if (product == null) {
        return unsupportedUnitInfo();
      }

      const settingRow = product.settings.rows.find(row => row.name === setting);
      if (settingRow == null) {
        return unsupportedUnitInfo();
      }

      const dimensionData = product.conversionTable.rows.find(row => row.contextual_dimension_id === settingRow.contextual_dimension_id);
      if (dimensionData == null) {
        return unsupportedUnitInfo(settingRow.decimal_places);
      }

      const dimension = dimensionData.dimension_name;
      if (!dimensionExists(dimension)) {
        return unsupportedUnitInfo(settingRow.decimal_places);
      }
      const defaultUnit = getDefaultDimensionUnits(dimension);
      const settings = deviceAscii.settings;
      if (settings?.canConvertNativeUnits == null || settings?.convertFromNativeUnits == null) {
        return {
          supported: true,
          dimension,
          default: defaultUnit,
          conversion: environment.isTest ? 1 : null,
          precision: settingRow.decimal_places
        };
      }
      if (settings.canConvertNativeUnits(settingRow.name)) {
        return {
          supported: true,
          dimension,
          default: defaultUnit,
          conversion: settings.convertFromNativeUnits(setting, 1, defaultUnit),
          precision: settingRow.decimal_places
        };
      }

      return unsupportedUnitInfo(settingRow.decimal_places);
    } catch (e) {
      throwUnexpectedError(e);
      this.log.warn(e);
      return null;
    }
  }

  /**
   * Gets the unit info for a product command
   * @param entity The entity key to get info for
   * @param command The command  to get info for
   * @returns The `UnitInfo` of this product command, or null if the device isn't available
   */
  public* getCommandUnitInfo(entity: EntityKey, command: string): SagaIterator<UnitInfo | null> {
    try {
      const product: RT<typeof this.getProduct> = yield call([this, this.getProduct], entity);

      if (product == null) {
        return unsupportedUnitInfo();
      }

      const getSubCommand = (command: string[], commandTree: Nullable<CommandTree>): Nullable<CommandTree> => {
        if (command.length === 0) {
          return commandTree ?? null;
        }
        if (commandTree?.nodes == null) {
          return null;
        }
        return getSubCommand(command.slice(1), commandTree.nodes.find(node => node.command === command[0]));
      };

      const commandTree = getSubCommand(command.split(' '), product.commandTree);
      if (commandTree == null) {
        return unsupportedUnitInfo();
      }

      const dimensionData = product.conversionTable.rows.find(row => row.contextual_dimension_id === commandTree.contextual_dimension_id);
      if (dimensionData == null) {
        return unsupportedUnitInfo();
      }

      const dimension = dimensionData.dimension_name;
      if (!dimensionExists(dimension)) {
        return unsupportedUnitInfo(0);
      }
      const defaultUnit = getDefaultDimensionUnits(dimension);

      return {
        supported: true,
        dimension,
        default: defaultUnit,
        conversion: environment.isTest ? 1 : (dimensionData.scale ? (1 / dimensionData.scale) : null),
        precision: 0
      };
    } catch (e) {
      throwUnexpectedError(e);
      this.log.warn(e);
      return null;
    }
  }

  public* getUnitInfo(from: UnitFrom): SagaIterator<UnitInfo> {
    if ('setting' in from) {
      if (from.entity == null) {
        return unsupportedUnitInfo();
      }
      const state: RT<typeof selectUnits> = yield select(selectUnits);
      const cachedUnitInfo = state.info[from.entity]?.settings[from.setting];
      if (cachedUnitInfo != null) {
        return cachedUnitInfo;
      }
      const unitInfo: RT<typeof this.getSettingUnitInfo > = yield call([this, this.getSettingUnitInfo], from.entity, from.setting);
      if (unitInfo == null) {
        return unsupportedUnitInfo();
      }
      return unitInfo;
    } else if ('command' in from) {
      if (from.entity == null) {
        return unsupportedUnitInfo();
      }
      const state: RT<typeof selectUnits> = yield select(selectUnits);
      const cachedUnitInfo = state.info[from.entity]?.settings[from.command];
      if (cachedUnitInfo != null) {
        return cachedUnitInfo;
      }
      const unitInfo: RT<typeof this.getCommandUnitInfo > = yield call([this, this.getCommandUnitInfo], from.entity, from.command);
      if (unitInfo == null) {
        return unsupportedUnitInfo();
      }
      return unitInfo;
    } else if ('dimension' in from) {
      return getUnitInfoOfDimension(from.dimension);
    }
    return from.info;
  }
}
