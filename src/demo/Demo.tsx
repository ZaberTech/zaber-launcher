import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { Title } from '../components';
import { useActions } from '../utils';

import { Axis } from './Axis';
import { selectAxesInOrder } from './selectors';
import { actions as actionsDefinition } from './actions';
import { All } from './All';

export const Demo: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const axes = useSelector(selectAxesInOrder);

  useEffect(() => {
    actions.mount();
    return () => { actions.unmount() };
  }, []);

  return (<div className="demo">
    <Title>Cycle Demo</Title>
    <All/>
    {axes.map(axis => <Axis key={axis.key} axis={axis}/>)}
  </div>);
};
