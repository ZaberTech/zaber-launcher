import React from 'react';
import { fireEvent, render } from '@testing-library/react';

import *  as desktop from '../desktop';

import { ErrorReport } from './ErrorReport';

test('Error page', () => {
  const openExternalSpy = jest.spyOn(desktop, 'openExternalLink').mockImplementation();
  const restartSpy = jest.spyOn(desktop, 'restart').mockImplementation();

  const wrapper = render(<ErrorReport/>);
  fireEvent.click(wrapper.getByTitle('Contact Support'));
  expect(openExternalSpy).toHaveBeenCalledWith('https://www.zaber.com/contact');

  fireEvent.click(wrapper.getByTitle('Restart Zaber Launcher'));
  expect(restartSpy).toHaveBeenCalled();
});
