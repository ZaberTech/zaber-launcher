import { all, call, put, SagaReturnType as ASR, takeEvery, select } from '@redux-saga/core/effects';
import type { SagaIterator } from 'redux-saga';
import { MessageType } from '@zaber/motion/ascii';

import { connectionManagerActions, getDevice, getDeviceOrAxis, reloadDevices } from '../connection_manager';
import { extractConnectionKey, extractRouterKey, getConnectionId, getRouterUrl } from '../keys';
import { getContainer } from '../container';
import { takeLeadingUniq, throwUnexpectedError, type Action } from '../utils';
import { MessageRouterConnection, MessageRoutersService } from '../message_router';

import { ActionTypes, actions, ActionsToPayloads } from './actions';
import { selectSetLabel } from './selectors';

export function* rootSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.SET_LABEL_WRITE, setLabelWrite),
    takeEvery(ActionTypes.SET_CONNECTION_NAME, setConnectionName),
    takeLeadingUniq(ActionTypes.GET_DEVICE_SYSTEM_ERRORS, action => action.payload.deviceKey, getSpecificFFError),
  ]);
}

function* setLabelWrite(): SagaIterator {
  const state: ReturnType<typeof selectSetLabel> = yield select(selectSetLabel);
  if (state.entityKey == null) { throw new Error('No entity key') }

  try {
    const device: ASR<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, state.entityKey);
    yield call([device, device.setLabel], state.label);
    yield call(reloadDevices, extractConnectionKey(state.entityKey));
    yield put(actions.setLabelWriteDone());
  } catch (e) {
    yield put(actions.setLabelWriteDone((e as Error).message || String(e)));
  }
}


function* setConnectionName({
  payload: { connectionKey, name }
}: Action<ActionsToPayloads[ActionTypes.SET_CONNECTION_NAME]>): SagaIterator {
  try {
    const service = getContainer().get(MessageRoutersService);
    const connection: MessageRouterConnection = yield call([service, service.getConnection], getRouterUrl(connectionKey));
    const api = connection.api;
    yield call([api, api.setConnectionName], getConnectionId(connectionKey), name);

    yield put(actions.setConnectionNameDone(connectionKey));
    yield put(connectionManagerActions.loadConnections(extractRouterKey(connectionKey), false));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setConnectionNameDone(connectionKey, String(err)));
  }
}

function* getSpecificFFError({
  payload: { deviceKey }
}: Action<ActionsToPayloads[ActionTypes.GET_DEVICE_SYSTEM_ERRORS]>): SagaIterator {
  try {
    const device: ASR<typeof getDevice> = yield call(getDevice, deviceKey);
    const responses: ASR<typeof device.genericCommandMultiResponse> =
      yield call([device, device.genericCommandMultiResponse], 'system errors');
    const infoResponses = responses.filter(response => response.messageType === MessageType.INFO);
    yield put(actions.setDeviceSystemErrors(deviceKey, infoResponses.map(response => response.data)));
  } catch (err) {
    throwUnexpectedError(err);
    // intentionally no error handling.
  }
}
