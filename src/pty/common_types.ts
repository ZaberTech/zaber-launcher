import { actionBuilder } from '../utils';

export const PROCESS_TX_IPC_CHANNEL = 'PTY_TX';
export const PROCESS_RX_IPC_CHANNEL = 'PTY_RX';
export const DEFAULT_TERMINAL_COLUMNS = 120;
export const DEFAULT_TERMINAL_ROWS = 24;
export const FAILED_EXIT_CODE = -1;

export interface TransmittedData {
  id: string;
  data: string;
}

export interface SpawnOptions {
  /**
   * Unique ID identifying the process across electron.
   * Only a single process with given ID can run at the same time.
   * The ID must be allocated by the caller to allow for preparation of terminal, etc.
   */
  id: string;

  cmd: string;
  args: string[] | string;
  cwd: string;
  env?: Record<string, string>;

  cols?: number;
  rows?: number;
}

export enum ActionTypes {
  SPAWN = 'PTY_REMOTE_SPAWN',
  EXIT = 'PTY_REMOTE_EXIT',
  KILL = 'PTY_REMOTE_KILL',
}

export interface ActionsToPayloads {
  [ActionTypes.SPAWN]: { options: SpawnOptions };
  [ActionTypes.EXIT]: { id: string; exitCode: number; signal: number | null };
  [ActionTypes.KILL]: { id: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  spawn: (options: SpawnOptions) => buildAction(ActionTypes.SPAWN, { options }),
  exit: (id: string, exitCode: number, signal: number | null) => buildAction(ActionTypes.EXIT, { id, exitCode, signal }),
  kill: (id: string) => buildAction(ActionTypes.KILL, { id }),
};
