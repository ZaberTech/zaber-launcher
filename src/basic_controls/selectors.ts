import _ from 'lodash';
import { createSelector } from 'reselect';
import { match } from 'ts-pattern';

import { selectAxes, selectConnections, selectDevices, selectRouters } from '../connection_manager';
import { EntityKey, EntityKeyType, getEntityType, isSubKeyOf } from '../keys';
import { selectBasicControls as selectBasicControlsRoot } from '../store';


export const selectBasicControls = createSelector(selectBasicControlsRoot, state => state.controls);
export const selectEntity = createSelector(selectBasicControls, state => state.selectedEntity);
export const selectPinned = createSelector(selectBasicControls, state => state.pinned);

/**
 * Looks for a more general version of `key` in `keys` (eg. `@device|local|/dev/ttyUSB0|1` for `@axis|local|/dev/ttyUSB0|1|1`)
 * @param key A specific key to check
 * @param keys A list of keys to search for more general keys within
 * @returns the most general key from `keys` that is included in `key`, or `key` if there is no more-general key
 */
function mostGeneralKey(key: string, keys: string[]): string {
  let moreGeneralKey = key;
  for (const k of keys) {
    if (isSubKeyOf(moreGeneralKey, k)) {
      moreGeneralKey = k;
    }
  }
  return moreGeneralKey;
}

const selectPinnedOrSelectedKeys = createSelector(selectEntity, selectPinned, (selected, pinned): EntityKey[] => {
  const generalizedSelected = selected == null ? [] : [mostGeneralKey(selected, pinned)];
  const mostGeneralPinnedEntities = pinned.map(key => mostGeneralKey(key, [...generalizedSelected, ...pinned]));
  return _.uniq([...mostGeneralPinnedEntities, ...generalizedSelected]);
});

export const selectProductKeys = createSelector(
  selectPinnedOrSelectedKeys,
  selectRouters, selectConnections, selectDevices, selectAxes,
  (keys, routers, connections, devices, axes): EntityKey[] => (
    keys.flatMap(key => match(getEntityType(key))
      .with(EntityKeyType.AXIS, () => axes[key] ? [key] : [])
      .with(EntityKeyType.DEVICE, () => devices[key] ? [key] : [])
      .with(EntityKeyType.CONNECTION, () => connections[key]?.devices ?? [])
      .with(EntityKeyType.ROUTER, () => routers[key]?.connections?.flatMap(key => connections[key]?.devices ?? []) ?? [])
      .otherwise(type => { throw new Error(`Invalid entity key type ${type} found for key '${key}'`) }),
    )
  )
);
export const selectDeviceInfo = createSelector(selectBasicControls, state => state.deviceInfo);

export const selectErrors = createSelector(selectBasicControls, state => state.errors);

export const selectIoExpanded = createSelector(selectBasicControls, state => state.ioExpanded);

export const selectMoveSinModal = createSelector(selectBasicControls, state => state.moveSinModal);

export const selectTravelLimitModal = createSelector(selectBasicControls, state => state.travelLimitModal);
