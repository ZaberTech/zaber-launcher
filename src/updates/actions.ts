import { actionBuilder } from '../utils';

export enum ActionTypes {
  NEW_VERSION_READY = 'UPDATES_NEW_VERSION_READY',
  QUIT_AND_INSTALL = 'UPDATES_QUIT_AND_INSTALL',
  CHECK_NEW_VERSION = 'UPDATES_CHECK_NEW_VERSION',
  VERSION_CHECK_DONE = 'UPDATES_VERSION_CHECK_DONE',
  IGNORE_UPDATE = 'UPDATES_IGNORE_UPDATE',
  SET_CURRENT_VERSION = 'UPDATES_SET_CURRENT_VERSION',
  APP_UPDATED_NOTIFICATION_CLOSE = 'UPDATES_APP_UPDATED_NOTIFICATION_CLOSE',
  NEW_VERSION_NOTIFICATION_DISPLAYED = 'UPDATES_NEW_VERSION_NOTIFICATION_DISPLAYED',
}

export interface ActionsToPayloads {
  [ActionTypes.NEW_VERSION_READY]: { version: string; canUpdate: boolean };
  [ActionTypes.QUIT_AND_INSTALL]: void;
  [ActionTypes.IGNORE_UPDATE]: { installOnStart: boolean };
  [ActionTypes.CHECK_NEW_VERSION]: void;
  [ActionTypes.VERSION_CHECK_DONE]: { error?: string };
  [ActionTypes.SET_CURRENT_VERSION]: { version: string; newVersionInstalled: boolean };
  [ActionTypes.APP_UPDATED_NOTIFICATION_CLOSE]: void;
  [ActionTypes.NEW_VERSION_NOTIFICATION_DISPLAYED]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  newVersionReady: (version: string, canUpdate = true) => buildAction(ActionTypes.NEW_VERSION_READY, { version, canUpdate }),
  quitAndInstall: () => buildAction(ActionTypes.QUIT_AND_INSTALL),
  ignoreUpdate: (installOnStart: boolean) => buildAction(ActionTypes.IGNORE_UPDATE, { installOnStart }),
  checkNewVersion: () => buildAction(ActionTypes.CHECK_NEW_VERSION),
  versionCheckDone: (error?: string) => buildAction(ActionTypes.VERSION_CHECK_DONE, { error }),
  setCurrentVersion: (version: string, newVersionInstalled: boolean) =>
    buildAction(ActionTypes.SET_CURRENT_VERSION, { version, newVersionInstalled }),
  appUpdatedNotificationClose: () => buildAction(ActionTypes.APP_UPDATED_NOTIFICATION_CLOSE),
  newVersionNotificationDisplayed: () => buildAction(ActionTypes.NEW_VERSION_NOTIFICATION_DISPLAYED),
};
