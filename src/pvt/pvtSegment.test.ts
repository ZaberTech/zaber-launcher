import { toBeDeepCloseTo } from 'jest-matcher-deep-close-to';

import { PvtSegment } from './pvtSegment';
import type { Point } from './types';

expect.extend({ toBeDeepCloseTo });


const pZero: Point = {
  dt: 0,
  pos: 0,
  time: 0,
  vel: 0,
};

const pOne: Point = {
  dt: 1,
  pos: 1,
  time: 1,
  vel: 0,
};

describe('Segment helper interpolation', () => {
  it('interpolates the unit square diagonal with zero velocities', () => {
    const segment = new PvtSegment(pZero, pOne, 1, 1);
    expect(segment.getPosRange()).toEqual({ min: { time: 0, value: 0 }, max: { time: 1, value: 1 } });
    expect(segment.getVelRange()).toEqual({ min: { time: 0, value: 0 }, max: { time: 0.5, value: 1.5 } });
    expect(segment.getAccelRange()).toEqual({ min: { time: 1, value: -6 }, max: { time: 0, value: 6 } });
    expect(segment.getPositionCrossings(0.5)).toBeDeepCloseTo([0.5]);
    [0, 0.5, 1].forEach(x => expect(segment.getPos(x)).toBe(x));
    expect(segment.getVel(0.5)).toBe(1.5);
    expect(segment.getAccel(0)).toBe(6);
    expect(segment.getJerk()).toBe(-12);
    expect(segment.getVelocityCrossings(1.5)).toBeDeepCloseTo([0.5]);
    expect(segment.getVelocityCrossings(0)).toBeDeepCloseTo([0, 1]);
    expect(segment.getAccelerationCrossings(0)).toBeDeepCloseTo([0.5]);
  });


  it('interpolates the unit square diagonal with endpoint velocities', () => {
    const segment = new PvtSegment({ ...pZero, vel: 0.5 }, { ...pOne, vel: 0.5 }, 1, 1);
    expect(segment.getPosRange()).toEqual({ min: { time: 0, value: 0 }, max: { time: 1, value: 1 } });
    expect(segment.getVelRange()).toEqual({ min: { time: 0, value: 0.5 }, max: { time: 0.5, value: 1.25 } });
    expect(segment.getAccelRange()).toEqual({ min: { time: 1, value: -3 }, max: { time: 0, value: 3 } });
    [0, 0.5, 1].forEach(x => expect(segment.getPos(x)).toBe(x));
    expect(segment.getPositionCrossings(0.5)).toBeDeepCloseTo([0.5]);
    expect(segment.getVelocityCrossings(1)).toBeDeepCloseTo([0.211, 0.788]);
    expect(segment.getAccelerationCrossings(0)).toBeDeepCloseTo([0.5]);
  });


  it('does not find crossings outside the range', () => {
    const segment = new PvtSegment(pZero, pOne, 1, 1);
    expect(segment.getPositionCrossings(2)).toEqual([]);
    expect(segment.getVelocityCrossings(2)).toEqual([]);
    expect(segment.getAccelerationCrossings(7)).toEqual([]);
  });


  it('identifies position peaks resulting from deceleration', () => {
    const segment = new PvtSegment({ ...pZero, vel: 1 }, { ...pZero, dt: 1, vel: 0 }, 1, 1);
    expect(segment.getStopPositionPeakTimes(0.1)).toBeDeepCloseTo([
      { time: 0, value: 5 },
      { time: 0.65, value: -0.473 },
      { time: 1, value: 0 },
    ]);
  });
});
