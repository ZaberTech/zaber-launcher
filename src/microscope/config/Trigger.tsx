import React from 'react';
import { useSelector } from 'react-redux';
import { NoticeBanner, NumericInput, SimpleSelect } from '@zaber/react-library';
import { P } from 'ts-pattern';

import { actions as actionsDefinition } from '../actions';
import { isMatch, useActions } from '../../utils';
import { TriggerConfig } from '../types';

import { selectEditedConfig } from './selectors';
import { useDeviceOptions } from './DeviceAndAxisSelect';

export const Trigger: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const { cameraTrigger: trigger } = useSelector(selectEditedConfig);
  const options = useDeviceOptions();
  const isValid = isMatch({ key: P.string }, trigger);
  const selected = isValid ? trigger.key : '';
  const digitalOutput = isValid ? trigger.digitalOutput : null;

  function editConfig(config: Partial<TriggerConfig>) {
    if (config.key === '') {
      actions.editConfig({ cameraTrigger: null });
      return;
    }
    actions.editConfig({ cameraTrigger: { key: selected, digitalOutput, ...config } });
  }

  return <>
    <div className="label">Camera Trigger</div>
    <SimpleSelect className="device" placeholder="Trigger Device"
      value={selected} options={options} onValueChange={key => editConfig({ key })}
      portalToBody/>
    <div className="digital-output">
      <span>Digital Output: </span>
      <NumericInput placeholder="Trigger Digital Output"
        status={!selected ? null : undefined}
        value={digitalOutput}
        onNumberChange={digitalOutput => editConfig({ digitalOutput })}/>
    </div>
    {isMatch({ error: P.string }, trigger) &&
      <NoticeBanner className="message" type="warning">
        {trigger.error}
      </NoticeBanner>}
  </>;
};
