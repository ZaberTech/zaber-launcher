import fs from 'fs/promises';

import { all, takeLatest, takeEvery, select, put, call } from '@redux-saga/core/effects';
import { CommandFailedException } from '@zaber/motion';
import { P, match } from 'ts-pattern';

import { SagaIter, RT, throwUnexpectedError } from '../utils';
import { getAxis } from '../connection_manager';
import { loadCsv } from '../pvt/csv';
import { Dialogs } from '../dialogs';

import { ActionTypes, actions } from './actions';
import { VALUE_COUNT, type CalibrationTable, fromEditedTableOrThrow, toEditedTable } from './types';
import { selectDevice, selectState } from './selectors';

const FACTORY_TIMEOUT = 10000;

export function* rootSaga(): SagaIter {
  yield all([
    takeLatest(ActionTypes.LOAD_TABLE, loadTable),
    takeEvery(ActionTypes.LOAD_FROM_FILE, loadFromFile),
    takeEvery(ActionTypes.SAVE_TO_FILE, saveToFile),
    takeLatest(ActionTypes.APPLY, apply),
    takeLatest(ActionTypes.APPLY_DELETE, applyDelete),
  ]);
}

function* loadTable(): SagaIter {
  const device: RT<typeof selectDevice> = yield select(selectDevice);
  if (!device) { return }

  try {
    const axis: RT<typeof getAxis> = yield getAxis(device.axisKey);
    const responses: RT<typeof axis.genericCommandMultiResponse> = yield axis.genericCommandMultiResponse('calibration print');
    const header = responses[0].data.split(' ').map(part => +part);
    const table: CalibrationTable = {
      calibrationType: header[0] ?? 0,
      width: header[1] ?? 0,
      offset: header[2] ?? 0,
      values: [],
    };

    for (const response of responses.slice(1).filter(response => response.data.startsWith('data '))) {
      table.values.push(...response.data.split(' ').slice(1).map(part => +part));
    }

    yield put(actions.tableLoaded(table));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.tableErr(err.message));
  }
}

function* apply(): SagaIter {
  const device: RT<typeof selectDevice> = yield select(selectDevice);
  if (!device) { throw new Error('No device selected') }

  try {
    const { editedTable, factoryData }: RT<typeof selectState> = yield select(selectState);
    const table = fromEditedTableOrThrow(editedTable);

    const axis: RT<typeof getAxis> = yield getAxis(device.axisKey);

    yield axis.genericCommand('calibration delete');
    let response: RT<typeof axis.genericCommand> =
      yield axis.genericCommand(`calibration load start ${table.calibrationType} ${table.width} ${table.offset}`);
    for (let i = 0; i < table.values.length; i += 4) {
      response = yield axis.genericCommand(`calibration load ${table.values.slice(i, i + 4).join(' ')}`);
    }
    if (response.data !== '0') {
      throw new Error(`Calibration not complete: ${response.data}}`);
    }

    if (factoryData) {
      yield axis.genericCommand('factory calibration delete', { timeout: FACTORY_TIMEOUT });
      yield axis.genericCommand('factory calibration save', { timeout: FACTORY_TIMEOUT });
    }

    yield put(actions.applyDone(table));
  } catch (err) {
    throwUnexpectedError(err);
    let message = err.message;
    if (err instanceof CommandFailedException && err.details.responseData === 'FULL') {
      message += 'Internal memory full. Reformat the device.';
    }
    yield put(actions.applyErr(message));
  }
}

function* applyDelete(): SagaIter {
  const device: RT<typeof selectDevice> = yield select(selectDevice);
  if (!device) { throw new Error('No device selected') }
  const { factoryData }: RT<typeof selectState> = yield select(selectState);

  try {
    const axis: RT<typeof getAxis> = yield getAxis(device.axisKey);
    yield axis.genericCommand('calibration delete');
    if (factoryData) {
      yield axis.genericCommand('factory calibration delete', { timeout: FACTORY_TIMEOUT });
    }
    yield put(actions.applyDeleteDone());
    yield put(actions.loadTable());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.applyErr(err.message));
  }
}

function* loadFromFile(): SagaIter {
  const dialogResult: RT<typeof Dialogs.showOpenDialog> = yield call(Dialogs.showOpenDialog, {
    title: 'Open CSV file',
    filters: [{ name: 'CSV files', extensions: ['csv'] }, { name: 'All files', extensions: ['*'] }],
  });

  if (dialogResult.canceled || dialogResult.filePaths.length !== 1) {
    return;
  }
  const filePath = dialogResult.filePaths[0];
  try {
    const csv: RT<typeof loadCsv> = yield loadCsv(filePath);
    if (csv.length < 2) {
      throw new Error('Expected at least 2 rows');
    }
    const data = csv[0].reduce((data, cell, i) => {
      if (typeof cell !== 'string') {
        throw new Error('Expected header row to contain only strings');
      }
      data[cell] = csv[1][i];
      return data;
    }, {} as Record<string, string | number | null>);

    const table: CalibrationTable = {
      calibrationType: 0,
      width: 0,
      offset: 0,
      values: [],
    };
    match(data).with({
      'Calibration Type': P.number,
      'Table Width': P.number,
      'Table Offset': P.number,
    }, data => {
      table.calibrationType = data['Calibration Type'];
      table.width = data['Table Width'];
      table.offset = data['Table Offset'];
    }).otherwise(() => { throw new Error('Invalid data format') });

    for (let i = 1; i <= VALUE_COUNT; i++) {
      const value = data[`Data_${i}`];
      if (typeof value !== 'number') {
        throw new Error(`Expected value ${i} to be a number`);
      }
      table.values.push(value);
    }

    yield put(actions.updateTable(toEditedTable(table)));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.fileErr(err.message));
  }
}

function* saveToFile(): SagaIter {
  let defaultPath = 'calibration.csv';
  const device: RT<typeof selectDevice> = yield select(selectDevice);
  if (device) {
    defaultPath = `${device.axis?.identity.peripheralName ?? device.device.identity.name}_calibration.csv`;
  }

  const dialogResult: RT<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
    title: 'Save CSV file',
    properties: ['showOverwriteConfirmation'],
    filters: [{ name: 'CSV files', extensions: ['csv'] }, { name: 'All files', extensions: ['*'] }],
    defaultPath,
  });
  if (dialogResult.canceled) {
    return;
  }

  const filePath = dialogResult.filePath!;
  try {
    const { editedTable }: RT<typeof selectState> = yield select(selectState);
    const table = fromEditedTableOrThrow(editedTable);

    const data: Record<string, string | number> = {
      'Data Version': 1,
      'Time Stamp': new Date().toString(),
      'Serial Number': 0,
      'Description': '',
      'Calibration Type': table.calibrationType,
      'Table Width': table.width,
      'Table Offset': table.offset,
    };

    if (device) {
      data['Serial Number'] = device.axis?.serialNumber ?? device.device.identity.serialNumber;
      data.Description = [
        'Retrieved from device:',
        device.axis?.identity.peripheralName ?? device.device.identity.name,
      ].join(' ');
    }

    for (let i = 1; i <= VALUE_COUNT; i++) {
      data[`Data_${i}`] = table.values[i - 1];
    }

    const csv = [
      Object.keys(data),
      Object.values(data),
    ].map(row => row.join(',')).join('\n');

    yield fs.writeFile(filePath, csv, 'utf8');
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.fileErr(err.message));
  }
}
