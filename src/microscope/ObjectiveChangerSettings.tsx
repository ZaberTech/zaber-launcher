import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { ButtonRowConfirmCancel, Icons, Input, Modal, Text } from '@zaber/react-library';

import { useActions } from '../utils';
import { InputWithUnits } from '../units';

import { selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations, POSITION_DIMENSION } from './types';
import { OperationError } from './components';
import { sanitizeMeasurement } from './utils';
import { useOperationState } from './hooks';


export const ObjectiveChangerSettings: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const monitor = useSelector(selectMonitorState);

  const stableInfo = monitor.stableInfo!.objectiveChanger!;

  const [isOpen, setIsOpen] = useState(false);
  const [labels, setLabels] = useState(stableInfo.labels);
  const [focusOffsets, setFocusOffsets] = useState(stableInfo.focusOffsets);

  const [isSaving] = useOperationState(Operations.objectiveChangerSettings);

  return <>
    <Modal
      className="objective-changer-settings"
      headerIcon={<Icons.Settings/>}
      headerText="Objective Changer Settings"
      onRequestClose={() => setIsOpen(false)}
      isOpen={isOpen}
      buttons={<ButtonRowConfirmCancel
        confirmText={isSaving ? 'Saving...' : 'Save'}
        onConfirm={isSaving ? 'disabled' : () => actions.operationStart(Operations.objectiveChangerSettings, { labels, focusOffsets })}
        onCancel={() => setIsOpen(false)}
      />}
    >
      <div className="table">
        <span/>
        <Text>Label</Text>
        <Text>Focus Offset</Text>

        {labels.map((label, index) => <React.Fragment key={index}>
          <Text>Position {index + 1}</Text>
          <Input key={index}
            placeholder={`Objective ${index + 1}`}
            value={label} onValueChange={label => {
              const newLabels = [...labels];
              newLabels[index] = label;
              setLabels(newLabels);
            }}/>
          <InputWithUnits
            className="focus-offset"
            title="Focus Offset"
            displayPrecision={3}
            unitFrom={{ dimension: POSITION_DIMENSION }}
            measure={focusOffsets[index]}
            onChange={value => {
              const newFocusOffsets = [...focusOffsets];
              newFocusOffsets[index] = sanitizeMeasurement(value);
              setFocusOffsets(newFocusOffsets);
            }}/>
        </React.Fragment>)}
      </div>
      <OperationError operation={Operations.objectiveChangerSettings}/>
    </Modal>
    <Icons.Settings className="settings-icon" title="Settings" onClick={() => setIsOpen(true)}/>
  </>;
};
