import React, { useEffect, useState } from 'react';
import { EditableField, Text, InputEditor, TextInputEditor } from '@zaber/react-library';
import { useDispatch, useSelector } from 'react-redux';
import { match, P } from 'ts-pattern';

import { selectLocalRouter } from '../connection_manager/selectors';
import { omit } from '../utils';

import { selectLauncherName } from './selectors';
import { actions } from './actions';

type Props = Omit<React.ComponentPropsWithoutRef<typeof EditableField>, 'value' | 'onValueChange' | 'validate' | 'defaultValue'>;

export const LauncherName: React.FC<Props> = props => {
  const name = useSelector(selectLocalRouter)?.name;
  const { error, isSetting } = useSelector(selectLauncherName);
  const dispatch = useDispatch();

  const [editedName, setEditedName] = useState<string | null>(null);

  useEffect(() => {
    if (!name) {
      dispatch(actions.setNameDefault());
    }
  }, []);

  const state = match({ name, isSetting, editedName })
    .returnType<{name: string; mode: InputEditor.Mode}>()
    .with({ name: P.nullish }, () => ({ name: '', mode: 'initialize' }))
    .with({ name: P.string, isSetting: true }, ({ name }) => ({ name, mode: 'write' }))
    .with({ name: P.string, editedName: P.nullish }, ({ name }) => ({ name, mode: 'display' }))
    .with({ editedName: P.string }, ({ editedName }) => ({ name: editedName, mode: 'edit' }))
    .exhaustive();

  const labelId = 'launcher-name';

  return (<div className="launcher-name">
    <label htmlFor={labelId}><Text>Launcher Name:</Text></label>
    <TextInputEditor
      {...omit(props, 'labelContent', 'disabled')}
      value={state.name}
      mode={state.mode}
      onChange={({ value, mode }) => {
        if (mode === 'write') {
          dispatch(actions.setName(value));
        }
        if (mode === 'edit') {
          setEditedName(value);
        } else {
          setEditedName(null);
        }
      }}
      message={!!error && `Cannot set name: ${error}`}
      isValid={name => name.length > 0}
      id={labelId}
    />
  </div>);
};
