import { all, call, put, select, takeEvery, delay, race, take } from 'redux-saga/effects';
import type { SagaIterator } from 'redux-saga';
import { P, match } from 'ts-pattern';
import { NoValueForKeyException, type Angle, type Length, type Native, type Time } from '@zaber/motion';
import { IoPortType, Trigger, TriggerCondition, TriggerState, TriggerOperation } from '@zaber/motion/ascii';

import { type SagaIter, type Action, type RT, takeIfTrue, throwUnexpectedError } from '../utils';
import { getDevice, selectIdentifiedDevices } from '../connection_manager';
import { commandExists } from '../devices';
import { Measurement, getUnits } from '../units';
import type { EntityKey } from '../keys';
import { getCommandWithMeasurements } from '../terminal/utils';
import { prepareCommand } from '../terminal/sagas';
import { actionDefinitions as pollingActions, ActionTypes as PollingActionTypes } from '../polling/actions';

import { ActionTypes, ActionsToPayloads, actions } from './actions';
import { selectCommandList, selectHasTriggers, selectSelectedDevice, selectTriggersExpanded, selectTriggersTriggers } from './selectors';
import {
  TriggerActionState,
  TriggerConditionState,
  defaultTriggerActionCommandState,
  defaultTriggerActionSettingState,
  defaultTriggerActionSettingToSettingState,
  defaultTriggerActionState,
  defaultTriggerConditionDistState,
  defaultTriggerConditionEncoderState,
  defaultTriggerConditionIoState,
  defaultTriggerConditionPosState,
  defaultTriggerConditionSettingAbsoluteState,
  defaultTriggerConditionSettingState,
  defaultTriggerConditionState,
  defaultTriggerConditionTimeState
} from './reducer';
import type { Operation, IoType, Operator } from './types';

type DistUnit = Native | Length | Angle | undefined;

export function* triggersSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.DEVICE_MOUNT, deviceMount),
    takeEvery(ActionTypes.ENABLE_TRIGGER, enableTrigger),
    takeEvery(ActionTypes.DISABLE_TRIGGER, disableTrigger),
    takeEvery(ActionTypes.REQUEST_DETAILS_DEVICE, requestDetailsDevice),
    takeEvery(ActionTypes.SEND_DETAILS_DEVICE, sendDetailsDevice),
    takeEvery(ActionTypes.SET_LABEL, setLabel),
  ]);
}

function* deviceMount({ payload: { deviceKey } }: Action<ActionsToPayloads[ActionTypes.DEVICE_MOUNT]>): SagaIter {
  const hasTriggers = (yield select(selectHasTriggers));

  if (hasTriggers) {
    yield race([
      call(triggersInitialize, deviceKey),
      takeIfTrue<ActionsToPayloads[ActionTypes.DEVICE_UNMOUNT]>(ActionTypes.DEVICE_UNMOUNT, payload => payload.deviceKey === deviceKey)
    ]);
  } else {
    const identifiedDevices: RT<typeof selectIdentifiedDevices> = yield select(selectIdentifiedDevices);

    if (identifiedDevices[deviceKey]) {
      const deviceInfo = identifiedDevices[deviceKey];

      if (commandExists('trigger show', deviceInfo)) {
        if (deviceInfo.identity.firmwareVersion.major < 7) {
          yield put(actions.setDeviceError(deviceKey, 'Triggers App requires device firmware version 7 or higher.'));
        } else {
          yield race([
            call(deviceInitialize, deviceKey, deviceInfo.isController),
            takeIfTrue<ActionsToPayloads[ActionTypes.DEVICE_UNMOUNT]>(ActionTypes.DEVICE_UNMOUNT, payload =>
              payload.deviceKey === deviceKey),
          ]);
        }
      } else {
        yield put(actions.setDeviceNoTriggers(deviceKey));
      }
    }
  }
}

function* deviceInitialize(deviceKey: EntityKey, isController: boolean): SagaIter {
  while (true) {
    try {
      const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
      const numTriggers: number = yield device.triggers.getNumberOfTriggers();
      const ioInfo: RT<typeof device.io.getChannelsInfo> = yield device.io.getChannelsInfo();

      yield put(actions.setDeviceCapabilities(deviceKey, isController, numTriggers, ioInfo));
      yield triggersInitialize(deviceKey);
      break;
    } catch (err) {
      throwUnexpectedError(err);
      yield put(actions.setDeviceError(deviceKey, err.message));
    }
    yield delay(1000);
  }
}

function* triggersInitialize(deviceKey: EntityKey): SagaIter {
  const triggersExpanded: RT<typeof selectTriggersExpanded> = yield select(selectTriggersExpanded);
  const numTriggers = triggersExpanded.length;
  yield put(actions.initializeTriggers(numTriggers));

  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
    const { triggers } = device;
    const enabledStates: RT<typeof triggers.getEnabledStates> = yield triggers.getEnabledStates();

    for (let i = 0; i < enabledStates.length; i++) {
      if (enabledStates[i].enabled) {
        yield put(actions.requestDetailsDevice(i, true));
      }
      yield put(actions.setEnabledLoading(i, false));
    }

    const devicesInfo: RT<typeof selectIdentifiedDevices> = yield select(selectIdentifiedDevices);
    const deviceInfo = devicesInfo[deviceKey];
    if (commandExists('storage print prefix', deviceInfo, true)) {
      const labels: RT<typeof triggers.getAllLabels> = yield triggers.getAllLabels();

      for (let i = 0; i < labels.length; i++) {
        if (labels[i] !== '') {
          yield put(actions.storeLabel(i, labels[i]));
        }
      }
    }

    yield put(actions.setDeviceError(deviceKey, null));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setDeviceError(deviceKey, err.message));
  }
}

function* getLabel(triggerIndex: number): SagaIter {
  const deviceKey: RT<typeof selectSelectedDevice> = yield select(selectSelectedDevice);
  if (deviceKey == null) { return }

  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
    const trigger = device.triggers.getTrigger(triggerIndex + 1);
    const label: RT<typeof trigger.getLabel> = yield trigger.getLabel();

    yield put(actions.storeLabel(triggerIndex, label));
    yield put(actions.setTriggerError(triggerIndex, null));
  } catch (err) {
    if (err instanceof NoValueForKeyException) {
      yield put(actions.storeLabel(triggerIndex, ''));
    } else {
      yield put(actions.setTriggerError(triggerIndex, String(err)));
      yield restoreLabel(triggerIndex);
    }
  }
}

function* setLabel({ payload: { triggerIndex, label } }: Action<ActionsToPayloads[ActionTypes.SET_LABEL]>): SagaIter {
  const deviceKey: RT<typeof selectSelectedDevice> = yield select(selectSelectedDevice);
  if (deviceKey == null) { return }

  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
    const trigger = device.triggers.getTrigger(triggerIndex + 1);

    yield trigger.setLabel(label);
    yield getLabel(triggerIndex);
  } catch (err) {
    yield put(actions.setTriggerError(triggerIndex, String(err)));
    yield restoreLabel(triggerIndex);
  }
}

function* restoreLabel(triggerIndex: number): SagaIter {
  const triggers: RT<typeof selectTriggersTriggers> = yield select(selectTriggersTriggers);
  yield put(actions.storeLabel(triggerIndex, triggers[triggerIndex]?.labelDevice ?? ''));
}

function* enableTrigger({ payload: { triggerIndex, fires } }: Action<ActionsToPayloads[ActionTypes.ENABLE_TRIGGER]>): SagaIter {
  const deviceKey: RT<typeof selectSelectedDevice> = yield select(selectSelectedDevice);
  if (deviceKey == null) { return }

  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
    yield device.triggers.getTrigger(triggerIndex + 1).enable(fires);
    yield put(pollingActions.pollNow());
    yield take(PollingActionTypes.RECEIVE_RESULTS);
    yield put(actions.setEnabledLoading(triggerIndex, false));
  } catch (err) {
    yield put(actions.setTriggerError(triggerIndex, String(err)));
  }
}

function* disableTrigger({ payload: { triggerIndex } }: Action<ActionsToPayloads[ActionTypes.DISABLE_TRIGGER]>): SagaIter {
  const deviceKey: RT<typeof selectSelectedDevice> = yield select(selectSelectedDevice);
  if (deviceKey == null) { return }

  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
    yield device.triggers.getTrigger(triggerIndex + 1).disable();
    yield put(pollingActions.pollNow());
    yield take(PollingActionTypes.RECEIVE_RESULTS);
    yield put(actions.setEnabledLoading(triggerIndex, false));
  } catch (err) {
    yield put(actions.setTriggerError(triggerIndex, String(err)));
  }
}

function* requestDetailsDevice({ payload: { triggerIndex, initialize } }: Action<ActionsToPayloads[ActionTypes.REQUEST_DETAILS_DEVICE]>):
  SagaIter {
  const deviceKey: RT<typeof selectSelectedDevice> = yield select(selectSelectedDevice);
  if (deviceKey == null) { return }

  const trigger: RT<typeof selectTriggersTriggers> = yield select(selectTriggersTriggers);
  const inputChanged = trigger[triggerIndex]?.inputChanged;

  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
    const trigger = device.triggers.getTrigger(triggerIndex + 1);

    const triggerState: TriggerState = yield trigger.getState();

    yield put(actions.setTriggerError(triggerIndex, null));

    yield put(actions.setFiresTotal(triggerIndex, triggerState.firesTotal));

    const conditionParts = triggerState.condition.split(' ');
    const condition = parseCondition(conditionParts.slice(1));

    yield put(actions.setConditionDevice(triggerIndex, condition));

    const actionsParts = triggerState.actions.map(action => action.split(' '));
    const triggerActions = actionsParts.map(actionParts => parseAction(actionParts.slice(2)));

    for (let i = 0; i < triggerActions.length; i++) {
      yield put(actions.setActionDevice(triggerIndex, i, triggerActions[i]));
    }

    if (!(initialize && inputChanged)) {
      yield put(actions.resetInput(triggerIndex));
    }
  } catch (err) {
    yield put(actions.setTriggerError(triggerIndex, String(err)));
  }
}

function parseCondition(condition: string[]): TriggerConditionState {
  return match([condition[0], condition.slice(1)])
    .with([P.when(val => !isNaN(Number(val))), P._], ([axis, rest]) => ({
      ...parseConditionWithoutAxis(rest),
      axis: Number(axis),
    }))
    .otherwise(() => ({
      ...parseConditionWithoutAxis(condition),
    }));
}

function parseConditionWithoutAxis(condition: string[]): TriggerConditionState {
  return match([condition[0], condition.slice(1)])
    .with(['none', P._], () =>
      defaultTriggerConditionState,
    )
    .with(['pos', P._], ([_pos, rest]) => ({
      ...defaultTriggerConditionPosState,
      operator: asciiOperatorToOperator(rest[0]),
      value: { value: Number(rest[1]), units: 'default' } as Measurement,
    }))
    .with(['dist', P._], ([_dist, rest]) => ({
      ...defaultTriggerConditionDistState,
      value: { value: Number(rest[0]), units: 'default' } as Measurement,
    }))
    .with(['encoder', P._], ([_encoder, rest]) => ({
      ...defaultTriggerConditionEncoderState,
      value: { value: Number(rest[1]), units: 'default' } as Measurement,
    }))
    .with(['io', P._], ([_io, rest]) => ({
      ...defaultTriggerConditionIoState,
      ioType: asciiIoTypeToIoType(rest[0]),
      ioChannel: Number(rest[1]),
      operator: asciiOperatorToOperator(rest[2]),
      value: { value: Number(rest[3]), units: 'default' } as Measurement,
    }))
    .with(['time', P._], ([_time, rest]) => ({
      ...defaultTriggerConditionTimeState,
      value: { value: Number(rest[0]), units: 'default' } as Measurement,
    }))
    .with(['abs', P._], ([_abs, rest]) => ({
      ...defaultTriggerConditionSettingAbsoluteState,
      ...parseConditionSetting(rest),
    }))
    .otherwise(() => ({
      ...defaultTriggerConditionSettingState,
      ...parseConditionSetting(condition),
    }));
}

function parseConditionSetting(conditionSetting: string[]) {
  return {
    setting: conditionSetting[0],
    operator: asciiOperatorToOperator(conditionSetting[1]),
    value: { value: Number(conditionSetting[2]), units: 'default' } as Measurement,
  };
}

function parseAction(action: string[]): TriggerActionState {
  return match([action[0], action.slice(1)])
    .with(['none', P._], ([_pos]) =>
      defaultTriggerActionState,
    )
    .with([P.when(val => !isNaN(Number(val))), P._], ([axis, rest]) => ({
      ...parseActionWithoutAxis(rest),
      axis: Number(axis),
    }))
    .otherwise(() => parseActionWithoutAxis(action));
}

function parseActionWithoutAxis(action: string[]): TriggerActionState {
  return match([action[0], action.slice(1)])
    .with([P.string, P.when(val => ['=', '+=', '-='].includes(val[0]))], ([setting, rest]) => ({
      ...parseActionSetting(rest.slice(1)),
      setting,
      operation: rest[0] as Operation,
    }))
    .otherwise(() => ({
      ...defaultTriggerActionCommandState,
      command: action.join(' '),
    }));
}

function parseActionSetting(actionSetting: string[]): TriggerActionState {
  return match([actionSetting[0], actionSetting.slice(1)])
    .with(['setting', P.when(val => !isNaN(Number(val[0])))], ([_setting, rest]) => ({
      ...defaultTriggerActionSettingToSettingState,
      fromAxis: Number(rest[0]),
      fromSetting: rest[1],
    }))
    .with(['setting', P._], ([_setting, rest]) => ({
      ...defaultTriggerActionSettingToSettingState,
      fromSetting: rest[0],
    }))
    .otherwise(() => ({
      ...defaultTriggerActionSettingState,
      value: { value: Number(actionSetting[0]), units: 'default' },
    }));
}

function* sendDetailsDevice({ payload: { triggerIndex } }: Action<ActionsToPayloads[ActionTypes.SEND_DETAILS_DEVICE]>): SagaIter {
  const deviceKey: RT<typeof selectSelectedDevice> = yield select(selectSelectedDevice);
  if (deviceKey == null) { return }

  const triggersState: RT<typeof selectTriggersTriggers> = yield select(selectTriggersTriggers);

  const triggerState = triggersState[triggerIndex];
  const condition = triggerState.conditionInput;

  try {
    if (condition != null) {
      const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
      const trigger = device.triggers.getTrigger(triggerIndex + 1);
      yield call(sendConditionDevice, trigger, condition);

      for (let i = 0; i < triggerState.actionsInput.length; i++) {
        yield call(sendActionToDevice, trigger, i + 1, triggerState.actionsInput[i]);
      }

      yield put(actions.requestDetailsDevice(triggerIndex, false));
    }
  } catch (err) {
    yield put(actions.setTriggerError(triggerIndex, String(err)));
  }
}

function* sendConditionDevice(trigger: Trigger, condition: TriggerConditionState): SagaIter {
  switch (condition.type) {
    case 'Trajectory Position':
      yield trigger.fireWhenSetting(
        condition.axis,
        'pos',
        operatorToTriggerCondition(condition.operator),
        condition.value.value ?? 0,
        getUnits(condition.value));
      break;
    case 'Distance Interval':
      yield trigger.fireWhenDistanceTravelled(
        condition.axis,
        condition.value.value ?? 0,
        getUnits(condition.value) as DistUnit);
      break;
    case 'Encoder Distance Interval':
      yield trigger.fireWhenEncoderDistanceTravelled(
        condition.axis,
        condition.value.value ?? 0,
        getUnits(condition.value) as DistUnit);
      break;
    case 'IO Channel':
      yield trigger.fireWhenIo(
        ioTypeToIoPortType(condition.ioType),
        condition.ioChannel,
        operatorToTriggerCondition(condition.operator),
        condition.value.value ?? 0);
      break;
    case 'Time Elapsed':
      yield trigger.fireAtInterval(
        condition.value.value ?? 0,
        getUnits(condition.value) as Native | Time | undefined);
      break;
    case 'Setting':
      yield trigger.fireWhenSetting(
        condition.axis,
        condition.setting,
        operatorToTriggerCondition(condition.operator),
        condition.value.value ?? 0,
        getUnits(condition.value));
      break;
    case 'Absolute Setting':
      yield trigger.fireWhenAbsoluteSetting(
        condition.axis,
        condition.setting,
        operatorToTriggerCondition(condition.operator),
        condition.value.value ?? 0,
        getUnits(condition.value));
      break;
  }
}

function operatorToTriggerCondition(operator: Operator): TriggerCondition {
  switch (operator) {
    case '==': return TriggerCondition.EQ;
    case '≠': return TriggerCondition.NE;
    case '<': return TriggerCondition.LT;
    case '>': return TriggerCondition.GT;
    case '≤': return TriggerCondition.LE;
    case '≥': return TriggerCondition.GE;
  }
}

function asciiOperatorToOperator(asciiOperator: string): Operator {
  switch (asciiOperator) {
    case '==': return '==';
    case '<>': return '≠';
    case '<': return '<';
    case '>': return '>';
    case '<=': return '≤';
    case '>=': return '≥';
    default: return '==';
  }
}

function ioTypeToIoPortType(ioType: IoType): number {
  switch (ioType) {
    case 'Digital Input': return IoPortType.DIGITAL_INPUT;
    case 'Analog Input': return IoPortType.ANALOG_INPUT;
    case 'Digital Output': return IoPortType.DIGITAL_OUTPUT;
    case 'Analog Output': return IoPortType.ANALOG_OUTPUT;
  }
}

function asciiIoTypeToIoType(asciiIoType: string): IoType {
  switch (asciiIoType) {
    case 'di': return 'Digital Input';
    case 'ai': return 'Analog Input';
    case 'do': return 'Digital Output';
    case 'ao': return 'Analog Output';
    default: return 'Digital Output';
  }
}

function* sendActionToDevice(trigger: Trigger, actionNumber: number, action: TriggerActionState): SagaIter {
  switch (action.type) {
    case 'None':
      yield trigger.clearAction(actionNumber);
      break;
    case 'Command': {
      const commandList: RT<typeof selectCommandList> = yield select(selectCommandList);
      const commandWithMeasurements = getCommandWithMeasurements(commandList, action.command);
      let commandToSend = action.command;
      if (commandWithMeasurements.measurements.length > 0) {
        commandToSend = yield call(prepareCommand, trigger.device, action.command, commandWithMeasurements);
      }

      try {
        yield trigger.onFire(actionNumber, action.axis, commandToSend);
      } catch {
        throw new Error(`Command not valid for device/axis: '${commandToSend}'`);
      }
      break;
    }
    case 'Set Setting to Value':
      yield trigger.onFireSet(
        actionNumber,
        action.axis,
        action.setting,
        operationToTriggerOperation(action.operation),
        action.value.value ?? 0,
        getUnits(action.value));
      break;
    case 'Set Setting to Setting':
      yield trigger.onFireSetToSetting(
        actionNumber,
        action.axis,
        action.setting,
        operationToTriggerOperation(action.operation),
        action.fromAxis,
        action.fromSetting);
      break;
  }
}

function operationToTriggerOperation(operation: Operation): TriggerOperation {
  switch (operation) {
    case '=': return TriggerOperation.SET_TO;
    case '+=': return TriggerOperation.INCREMENT_BY;
    case '-=': return TriggerOperation.DECREMENT_BY;
  }
}
