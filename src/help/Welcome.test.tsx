import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';

import { wrapWithNewStore } from '../test';
import { createContainer, destroyContainer } from '../container';

import { Welcome } from './Welcome';

const TestWelcome = wrapWithNewStore(Welcome);
let wrapper: RenderResult;

beforeEach(() => {
  createContainer();

  wrapper = render(<TestWelcome/>);
});
afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
});

test('can go through the steps', () => {
  expect(wrapper.queryByText('Previous')).toBeNull();

  fireEvent.click(wrapper.getByText('Next'));

  fireEvent.click(wrapper.getByText('Previous'));
  expect(wrapper.queryByText('Previous')).toBeNull();

  for (let i = 0; i < 3; i++) {
    fireEvent.click(wrapper.getByText('Next'));
  }

  expect(wrapper.queryByText('Next')).toBeNull();
  wrapper.getByText('Get Started');
});
