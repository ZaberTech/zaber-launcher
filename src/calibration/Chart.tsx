import React, { useRef } from 'react';
import ReactECharts from 'echarts-for-react';
import { useSelector } from 'react-redux';

import { selectConvertedTable } from './selectors';

export const PopulatedChart: React.FC = () => {
  const table = useSelector(selectConvertedTable);
  const values = useRef<number[]>([]);
  if ('values' in table) {
    values.current = table.values;
  }

  return <div className="chart">
    <Chart values={values.current}/>
  </div>;
};

const Chart: React.FC<{ values: number[] }> = ({ values }) => {
  const options = {
    grid: { top: 8, right: 8, bottom: 8, left: 64 },
    xAxis: {
      name: 'x',
      show: false,
      min: 0,
      max: values.length,
    },
    yAxis: {
      type: 'value',
      name: 'Native Units',
      nameLocation: 'center',
      nameGap: 48,
    },
    series: [{
      data: values.map((value, i) => [i, value]),
      type: 'line',
    }],
    tooltip: {
      trigger: 'axis',
    },
  };

  return <ReactECharts option={options}/>;
};
