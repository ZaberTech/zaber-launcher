import { HeaderCard, Text, Button, Input, Icons, NumericInput, Checkbox, HelpTooltip, SimpleSelect } from '@zaber/react-library';
import _ from 'lodash';
import React  from 'react';
import { useSelector } from 'react-redux';

import { ExternalLink } from '../components';
import { ASCII_PROTOCOL_REFERENCE } from '../terminal';
import { useActions, notNil } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectPlainAxes, selectConfig, selectDevices, selectManualMode, PlainAxis } from './selectors';
import { allEventTypes, KeyCommand, KeyEventTypeLabel, KeyEventTypes, TargetAxisType, TargetAxisTypeLabels } from './types';

interface AxisOptions {
  axisType: TargetAxisType;
  targetAxis: number;
  label: string;
  value: string;
}

function makeAxisOptions(command: KeyCommand, axes: PlainAxis[]) {
  const options: AxisOptions[] = [
    {
      axisType: 'axis' as TargetAxisType,
      targetAxis: 0,
      label: 'Device / All Axes',
    },
    ...axes.map(axis => ({
      axisType: 'axis' as TargetAxisType,
      targetAxis: axis.axisNumber,
      label: axisLabel(axis, 'axis'),
    })),
    ...axes.filter(axis => axis.lockstep != null && axis.isPrimary).map(axis => ({
      axisType: 'lockstep' as TargetAxisType,
      targetAxis: axis.lockstep!.groupNumber,
      label: axisLabel(axis, 'lockstep'),
    })),
  ].map(option => ({
    ...option,
    value: `${option.axisType}-${option.targetAxis}`,
  }));

  const selected = `${command.axisType}-${command.targetAxis ?? 0}`;
  const axis = options.find(option => option.value === selected);
  const isUnknownAxis = axis == null;

  if (isUnknownAxis) {
    options.push({
      axisType: command.axisType,
      targetAxis: command.targetAxis ?? 0,
      value: selected,
      label: `Unknown ${_.upperFirst(command.axisType)} ${command.targetAxis}`,
    });
  }

  return {
    options,
    selected,
    isUnknownAxis,
  };
}

const KeyCommandCtrl: React.FC<{
  keyNumber: number;
  commandIndex: number;
}> = ({ keyNumber, commandIndex }) => {
  const actions = useActions(actionsDefinition);
  const updateCommand = (cmd: Partial<KeyCommand>) => actions.updateKeyCommand(keyNumber, commandIndex, cmd);

  const manualMode = useSelector(selectManualMode);
  const command = useSelector(selectConfig).keys[keyNumber - 1].commands[commandIndex];
  const devices = useSelector(selectDevices);
  const axes = useSelector(selectPlainAxes);

  const availableAxes = _.values(axes).filter(axis => axis.device.address === command.targetDevice);
  const device = _.find(devices, device => device.address === command.targetDevice);

  const { options: axisOptions, selected: selectedAxis, isUnknownAxis } = makeAxisOptions(command, availableAxes);

  const isUnknownDevice = device == null && command.targetDevice !== 0;
  const targetDeviceId = command.targetDevice ?? 0;
  const disableAxes = (!isUnknownDevice && !isUnknownAxis && (device?.axisCount ?? 0) <= 1) || !targetDeviceId;

  return (<>
    <SimpleSelect className="event-select" data-testid={`key-event-select-${commandIndex}`}
      value={command.event}
      onValueChange={event => updateCommand({ event })}
      options={_.map(KeyEventTypeLabel, (label, eventType) => ({ label, value: +eventType }))}/>

    {!manualMode && <>
      <SimpleSelect className="device-select" data-testid={`key-device-select-${commandIndex}`}
        value={targetDeviceId}
        onValueChange={newDevice => updateCommand({ targetDevice: newDevice, targetAxis: 0, axisType: 'axis' })}
        options={[
          { value: 0, label: 'All Devices' },
          ..._.values(devices).map(device => ({
            value: device.address,
            label: `${device.address} ${device.label ?? device.identity.name}`,
          })),
          isUnknownDevice ? ({ value: targetDeviceId, label: `Unknown Device ${command.targetDevice}` }) : null,
        ].filter(notNil)}/>
      <SimpleSelect className="axis-select" data-testid={`key-axis-select-${commandIndex}`} disabled={disableAxes}
        value={selectedAxis}
        onValueChange={value => {
          const newAxis = axisOptions.find(axis => axis.value === value)!;
          updateCommand({ axisType: newAxis.axisType, targetAxis: newAxis.targetAxis });
        }}
        options={axisOptions}/>
    </>}

    {manualMode && <>
      <NumericInput className="device-num" data-testid={`key-device-num-${commandIndex}`}
        value={command.targetDevice}
        onNumberChange={targetDevice => updateCommand({ targetDevice })}/>
      <SimpleSelect
        className="axis-type-select" data-testid={`key-axis-type-${commandIndex}`}
        value={command.axisType}
        onValueChange={axisType => updateCommand({ axisType })}
        options={_.map(TargetAxisTypeLabels, (label, key) => ({ value: key as TargetAxisType, label }))}/>
      <NumericInput className="axis-num" data-testid={`key-axis-num-${commandIndex}`}
        value={command.targetAxis}
        onNumberChange={targetAxis => updateCommand({ targetAxis })}/>
    </>}

    <Input className="command-text" data-testid={`key-cmd-${commandIndex}`}
      value={command.command}
      onValueChange={command => updateCommand({ command })}/>
    <Icons.Trash title="Delete Event" className="delete-command" onClick={() => actions.removeKeyCommand(keyNumber, commandIndex)}/>
  </>);
};

export const JoystickKey: React.FC<{ keyNumber: number }> = ({ keyNumber }) => {
  const actions = useActions(actionsDefinition);
  const config = useSelector(selectConfig).keys[keyNumber - 1];
  const manualMode = useSelector(selectManualMode);

  return (<HeaderCard className="joystick-key" edge="outline"
    header={<>
      <Text t={Text.Type.H4}>Key {keyNumber}</Text>
      <div className="spacer"/>
      <Checkbox
        labelContent="ASCII Protocol Alerts"
        checked={KeyEventTypes.some(eventType => config.alerts[eventType])}
        onChecked={on => actions.updateKeyConfig(keyNumber, { alerts: allEventTypes(on) })}/>
      <HelpTooltip>
        Turning on alerts allows you to receive key events on the side of the computer using Zaber Motion Library.
      </HelpTooltip>
    </>}>
    {config.commands.length > 0 && <>
      <Text t={Text.Type.Helper} style={{ gridColumnStart: 1 }}>Event</Text>
      {!manualMode && <>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 2 }}>Device</Text>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 4 }}>Axis</Text>
      </>}
      {manualMode && <>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 2 }}>Device Number</Text>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 3 }}>Axis Type</Text>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 4 }}>Axis Number</Text>
      </>}
      <Text t={Text.Type.Helper} style={{ gridColumnStart: 5 }}>Command</Text>
    </>}

    {config.commands.map((command, i) => <KeyCommandCtrl key={i} keyNumber={keyNumber} commandIndex={i}/>)}
    <Button color="grey" onClick={() => actions.addKeyCommand(keyNumber)}>Add Event</Button>
    <ExternalLink className="ascii-ref" url={ASCII_PROTOCOL_REFERENCE}>
      ASCII Protocol Reference
    </ExternalLink>
  </HeaderCard>);
};

function axisLabel(axis: PlainAxis, axisType: TargetAxisType): string {
  const label = [`Axis ${axis.axisNumber}`];
  if (axis.identity.isPeripheral) {
    label.push(axis.identity.peripheralName);
  }
  if (axis.lockstep != null && axisType === 'lockstep') {
    label.push(`(Lockstep ${axis.lockstep.groupNumber})`);
  }
  return label.join(' ');
}
