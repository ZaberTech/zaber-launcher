import React, { useState } from 'react';
import { Button, ButtonNext, ButtonPrevious, ButtonRow, Flex, Icons, Modal, Steps, Text } from '@zaber/react-library';

import { ExternalLink } from '../components';
import { environment } from '../environment';

import { OneTimeMessage } from './OneTimeMessage';

const STEP_COUNT = 4;

const Step: React.FC<{ step: number }> = ({ step }) => {
  switch (step) {
    case 0: return <>
      <h4>Welcome to Zaber Launcher</h4>
      <p>We made Zaber Launcher to help you set up, control and explore your devices.</p>
      <p>Our goal is to make starting with Zaber devices easy and convenient.</p>
    </>;
    case 1: return <>
      <h4>Compatibility</h4>
      <p>This software only supports
      the <ExternalLink url="https://www.zaber.com/protocol-manual?protocol=ASCII">Zaber ASCII Protocol</ExternalLink>.
      For the legacy Zaber Binary Protocol, or for devices with Firmware
      versions lower than 6.14 (December 2014), please download <ExternalLink url="https://www.zaber.com/software">
        Zaber Console
      </ExternalLink>
      .</p>
      <p>We will not be adding any new features to Zaber Console
      and only maintain it to support our legacy products.</p>
    </>;
    case 2: return <>
      <h4>Help Us Improve</h4>
      <p>We are actively developing Zaber Launcher.
      If there is a feature you would like added, please leave us a suggestion
      using the <b>Feedback</b> button in the menu.</p>
      <p>Also, please report any issues you encounter.</p>
      <p>We appreciate your help!</p>
    </>;
    case 3: return <>
      <h4>Stay Up To Date</h4>
      {!environment.offline && <p>Keep your application updated to access the latest bug fixes and features.
      The application will notify you when we release a new version.</p>}
      {environment.offline && <p>Keep your application updated to access the latest bug fixes and features.
      To update Zaber Launcher Offline, download the newest version and reinstall.
      You will need to download a new version of Zaber Launcher Offline to use devices running future firmware versions.</p>}
      <p>We hope you enjoy using Zaber Launcher.</p>
      <Text className="sincerely" e={Text.Emphasis.Light}>Sincerely,</Text>
      <Text className="sincerely" e={Text.Emphasis.Light}>Zaber Software Team</Text>
    </>;
  }
  throw new Error(`Invalid step ${step}`);
};

const Message: React.FC<{ dismiss: () => void }> = ({ dismiss }) => {
  const [step, setStep] = useState(0);
  return (
    <Modal
      className="welcome-modal"
      headerIcon={<Icons.Information/>}
      headerText="Welcome to Zaber Launcher!"
      buttons={<ButtonRow justify="spread">
        {step > 0 &&  <ButtonPrevious onClick={() => setStep(step - 1)}>Previous</ButtonPrevious>}
        <Flex.Spacer/>
        {step < STEP_COUNT - 1 && <ButtonNext onClick={() => setStep(step + 1)}>Next</ButtonNext>}
        {step === STEP_COUNT - 1 && <Button onClick={dismiss}>Get Started</Button>}
      </ButtonRow>}
      onRequestClose={dismiss}>
      <Steps count={STEP_COUNT} current={step}/>
      <Step step={step}/>
    </Modal>
  );
};

export const Welcome: React.FC = () => (
  <OneTimeMessage messageId="welcome" message={dismiss => (
    <Message dismiss={dismiss}/>
  )}/>
);
