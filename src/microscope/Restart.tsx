import React from 'react';
import { useSelector } from 'react-redux';
import { ContextMenu, Icons, NoticeBanner } from '@zaber/react-library';

import { isMatch, useActions } from '../utils';

import { selectMonitorState, selectOperations } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations } from './types';
import { OperationError } from './components';

export const RestartMenuItem: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const monitor = useSelector(selectMonitorState);
  const { stableInfo } = monitor;

  const { restart } = useSelector(selectOperations);
  const inProgress = isMatch({ inProgress: true }, restart);

  if (stableInfo == null) {
    return null;
  }

  return (
    <ContextMenu.Item
      onClick={() => actions.operationStart(Operations.restart)}
      disabled={inProgress} icon={<Icons.Restore/>}>
      Restart Microscope
    </ContextMenu.Item>);
};

export const RestartInProgress: React.FC = () => {
  const { restart } = useSelector(selectOperations);
  const inProgress = isMatch({ inProgress: true }, restart);
  return <>
    {inProgress && <NoticeBanner type="info">
      Restarting microscope...
    </NoticeBanner>}
    <OperationError operation={Operations.restart}/>
  </>;
};
