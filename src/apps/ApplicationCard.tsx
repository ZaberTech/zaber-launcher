import React from 'react';
import { Button, Card, iconFromSvg, Icons, Text } from '@zaber/react-library';
import classNames from 'classnames';

import { useActions } from '../utils';
import { Editions } from '../environment';
import StarSvg from '../assets/star.svg';
import { actions as ipcActionDefinitions } from '../ipc/actions';

import type { selectAppsToRender } from './selectors';
import { actions as appsActionDefinitions } from './actions';
import { APPS_UI_META } from './ui_meta';

const Star = iconFromSvg(StarSvg);

interface Props {
  app: ReturnType<typeof selectAppsToRender>[number];
  onOpen: (newWindow: boolean) => void;
  className?: string;
}

export const ApplicationCard: React.FC<Props> = ({ app, onOpen, className }) => {
  const appActions = useActions(appsActionDefinitions);
  const ipcActions = useActions(ipcActionDefinitions);
  const isInternal = app.editions?.includes(Editions.Internal);
  const ui = APPS_UI_META[app.url];

  return (
    <Card className={classNames('app-card', className)}>
      <div className="app-logo">
        {isInternal && <div className="internal-sticker">
          <Text f={Text.Family.Mono}>
            internal
          </Text>
        </div>}
        <img src={ui.logoSrc} alt={`${app.name} Logo`}/>
      </div>
      <div className="app-info">
        <div className="description">
          <div className="title">
            <h4>{app.name}</h4>
            {!app.deprecated && (
              <Star
                className={classNames('pin', { pinned: app.pinned })}
                title={app.pinned ? 'Pinned in Sidebar' : 'Pin in Sidebar'}
                onClick={() => {
                  const action = appActions.pinApp(app.url, !app.pinned);
                  ipcActions.ipcBroadcast(action);
                }}
              />
            )}
          </div>
          <div>{ui.description}</div>
        </div>
        <div className="bottom">
          <Button color="grey" onClick={() => onOpen(false)} title={`Open ${app.name}`}>Open</Button>
          <Icons.NewWindow title={`Open ${app.name} in New Window`} onClick={() => onOpen(true)}/>
        </div>
      </div>
    </Card>
  );
};
