import { call, select, put } from 'redux-saga/effects';
import { throwUnexpectedError } from '@zaber/toolbox';
import _ from 'lodash';
import { Length, ascii, microscopy } from '@zaber/motion';

import { type SagaIter, type RT, type Action, delay } from '../utils';
import { getConnection } from '../connection_manager';
import { environment } from '../environment';

import { ActionTypes, ActionsToPayloads, actions } from './actions';
import { selectSelectedKey } from './selectors';

export function* testScope({ payload: { testType } }: Action<ActionsToPayloads[ActionTypes.TEST]>): SagaIter {
  const connectionKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  if (!connectionKey) { throw new Error('No connection selected') }

  try {
    yield put(actions.testProgress('Detecting devices...'));
    const connection: RT<typeof getConnection> = yield call(getConnection, connectionKey);
    yield ensureAllIdentified(connection);

    yield put(actions.testProgress('Searching for components...'));
    const scope: RT<typeof microscopy.Microscope.find> = yield microscopy.Microscope.find(connection);
    yield put(actions.testProgress('Initializing...'));
    yield scope.initialize({ force: testType === 'force-home' });

    if (testType === 'full') {
      yield fullTest(scope);
    }

    yield put(actions.testDone());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.testDone(err.message));
  }
}

function* fullTest(scope: microscopy.Microscope): SagaIter {
  if (scope.objectiveChanger) {
    yield put(actions.testProgress('Testing objective changer...'));
    const changer = scope.objectiveChanger;
    const count: RT<typeof changer.getNumberOfObjectives> = yield changer.getNumberOfObjectives();
    for (let i = 0; i <= count; i++) {
      yield changer.change(i % count + 1);
    }
  } else if (scope.focusAxis) {
    yield put(actions.testProgress('Testing focus axis...'));
    const axis = scope.focusAxis;
    yield axis.moveMin();
    yield axis.moveAbsolute(10, Length.mm);
    yield axis.moveMin();
  }

  if (scope.filterChanger) {
    yield put(actions.testProgress('Testing filter changer...'));
    const changer = scope.filterChanger;
    const count: RT<typeof changer.getNumberOfFilters> = yield changer.getNumberOfFilters();
    for (let i = 0; i <= count; i++) {
      yield changer.change(i % count + 1);
    }
  }

  if (scope.illuminator) {
    const channels = scope.illuminator.device.axisCount;
    for (let i = 0; i < channels; i++) {
      const axis = scope.illuminator.device.getAxis(i + 1);
      if (axis.axisType !== ascii.AxisType.LAMP) { continue }

      yield put(actions.testProgress(`Testing illuminator channel ${i + 1}...`));
      const channel = scope.illuminator.getChannel(i + 1);

      // Don't test UV lamps.
      const wavelength: RT<typeof channel.settings.get> = yield channel.settings.get('lamp.wavelength.peak');
      if (wavelength <= 385) { continue }

      yield channel.setIntensity(0.5);
      yield channel.on();
      yield delay(environment.isTest ? 0 : 500);
      yield channel.setIntensity(1);
      yield delay(environment.isTest ? 0 : 500);
      yield channel.off();
    }
  }

  for (const { axis, label } of [
    { label: 'X', axis: scope.xAxis },
    { label: 'Y', axis: scope.yAxis },
  ]) {
    if (axis == null) { continue }

    yield put(actions.testProgress(`Testing ${label} axis...`));
    const pos: RT<typeof axis.getPosition> = yield axis.getPosition();
    yield axis.moveMax();
    yield axis.moveMin();
    yield axis.moveAbsolute(pos);
  }

  if (scope.plate) {
    yield put(actions.testProgress('Testing plate...'));
    const plate = scope.plate;

    yield plate.moveMin();
    const minPosition: RT<typeof plate.getPosition> = yield plate.getPosition();
    yield plate.moveMax();
    const maxPosition: RT<typeof plate.getPosition> = yield plate.getPosition();

    const middle = _.zip(minPosition, maxPosition).map(([min, max]) => (min! + max!) / 2);
    yield plate.moveAbsolute(...middle.map(value => ({ value })));
  }
}

function* ensureAllIdentified(connection: ascii.Connection) {
  const devices: RT<typeof connection.detectDevices> = yield connection.detectDevices({ identifyDevices: false });
  for (const device of devices) {
    if (!device.isIdentified) {
      yield device.identify();
    }
  }
  return devices;
}
