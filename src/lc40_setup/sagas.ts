import { ascii, CommandFailedException, Length, MotionLibException, MovementFailedException, Velocity } from '@zaber/motion';
import { castAny, throwUnexpectedError } from '@zaber/toolbox';
import { Action, SagaLock } from '@zaber/toolbox/lib/redux';
import _ from 'lodash';
import type { SagaIterator } from 'redux-saga';
import { all, call, cancelled, delay, put, race, SagaReturnType as RT, select, take, takeEvery, takeLatest } from 'redux-saga/effects';

import { ConnectionsView, getAxis, IdentificationHooks, IdentifiedDeviceInfo } from '../connection_manager';
import { getContainer } from '../container';
import { deviceSetupActions } from '../device_setup';
import { environment, Flavors } from '../environment';
import { handleNonCriticalError } from '../errors';
import { EntityKey, makeAxisKey } from '../keys';
import { RejectReasons } from '../protocol';
import { isSerialNumberValid } from '../devices/product';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import { ErrorContext, errorMessages, getErrorContext, TestSetupError, TestSetupErrors } from './exception';
import { selectCurrentAxisOrDevice, selectIsCurrentSupported } from './selectors';
import {
  AxisConfigComplete, DISPLAY_SETUP_ALL_TIME, DriverDirection, MAXIMUM_TRAVEL_LENGTH_MM, MotorOrientation,
  StageType,
  SUPPORTED_DEVICE_OR_PERIPHERAL_IDS,
  SetupStep,
} from './types';

type CurrentAxisOrDevice = NonNullable<ReturnType<typeof selectCurrentAxisOrDevice>>;

export const STAGE_TYPE_KEY = 'zaber.lc40.stage-type';
export const SAFE_APPROACH_SPEED_MM_PER_S = 100;
export const MAX_HOME_SENSOR_DISTANCE_HEURISTIC_MM = 100;
const SYSTEM_ACCESS_ADVANCED = 2;
const MIN_TRAVEL_DISTANCE_FOR_HEURISTIC_MM = 150;

interface TestSetupContext {
  axis: ascii.Axis;
  state: CurrentAxisOrDevice;
  lock: SagaLock;
}

export function* rootSaga(): SagaIterator {
  getContainer().get(IdentificationHooks).register(SUPPORTED_DEVICE_OR_PERIPHERAL_IDS, onIdentification);

  yield all([
    takeLatest(ActionTypes.LOAD_DEVICE_OR_AXIS, loadDeviceOrAxis),
    takeEvery(ActionTypes.SETUP_START, startSetup),
    takeLatest(ActionTypes.ESTOP_MOUNTED, estopMounted),
  ]);
}

const APP_URL = environment.flavor === Flavors.Rexroth ? '/shb40-setup' : '/lc40-setup';

export function* onIdentification(device: IdentifiedDeviceInfo): SagaIterator {
  if (SUPPORTED_DEVICE_OR_PERIPHERAL_IDS.includes(device.identity.deviceId)) {
    const config: RT<typeof getAxisConfig> = yield call(getAxisConfig, makeAxisKey(device.key, 1));
    const serialNumber = device.identity.serialNumber;
    if ((config == null || DISPLAY_SETUP_ALL_TIME) && isSerialNumberValid(serialNumber)) {
      yield put(deviceSetupActions.setupNeeded(serialNumber, `${APP_URL}?${ConnectionsView.SELECT_PARAM}=${device.key}`));
    } else {
      yield put(deviceSetupActions.setupNotNeeded(device.identity.serialNumber));
    }
  }
  for (const axis of device.axes) {
    if (SUPPORTED_DEVICE_OR_PERIPHERAL_IDS.includes(axis.identity.peripheralId) && axis.serialNumber != null) {
      const config: RT<typeof getAxisConfig> = yield call(getAxisConfig, axis.key);
      const serialNumber = axis.serialNumber;
      if ((config == null || DISPLAY_SETUP_ALL_TIME) && isSerialNumberValid(serialNumber)) {
        yield put(deviceSetupActions.setupNeeded(serialNumber, `${APP_URL}?${ConnectionsView.SELECT_PARAM}=${axis.key}`));
      } else {
        yield put(deviceSetupActions.setupNotNeeded(serialNumber));
      }
    }
  }
}

function* loadDeviceOrAxis(): SagaIterator {
  const current: ReturnType<typeof selectCurrentAxisOrDevice> = yield select(selectCurrentAxisOrDevice);
  if (!current) { return }
  const isSupported: ReturnType<typeof selectIsCurrentSupported> = yield select(selectIsCurrentSupported);
  if (!isSupported) { return }

  try {
    const config: RT<typeof getAxisConfig> = yield call(getAxisConfig, getAxisKey(current));
    if (config) {
      yield put(actions.updateConfig(config));
    }
  } catch (err) {
    yield put(actions.setSetupStep(SetupStep.CONFIG_ERROR));
    throwUnexpectedError(err);
  } finally {
    yield put(actions.setLoaded());
  }
}

function* getAxisConfig(axisKey: EntityKey): SagaIterator<AxisConfigComplete | null> {
  const axis: RT<typeof getAxis> = yield call(getAxis, axisKey);
  const { settings, storage } = axis;

  const hasStageType: boolean = yield call([storage, storage.keyExists], STAGE_TYPE_KEY);
  const stageTypeString: string = hasStageType ? yield call([storage, storage.getString], STAGE_TYPE_KEY) : '';
  const stageType: StageType = (Object.values(StageType) as string[]).includes(stageTypeString) ?
    stageTypeString as StageType :
    StageType.BASE_MOUNT;
  const driverDirection: number = yield call([settings, settings.get], 'driver.dir');
  const travelDistanceMm: number = yield call([settings, settings.get], 'limit.max', Length.MILLIMETRES);
  const isConfigured = travelDistanceMm <= MAXIMUM_TRAVEL_LENGTH_MM;

  const motorOrientation = (Object.entries(DriverDirection[stageType])
    .find(([_, value]) => value === driverDirection)?.[0] ?? MotorOrientation.LEFT) as MotorOrientation;

  if (!isConfigured) { return null }

  return {
    stageType,
    motorOrientation,
    travelDistanceMm: _.round(travelDistanceMm, 2),
  };
}

function* startSetup({ payload: { test } }: Action<ActionsToPayloads[ActionTypes.SETUP_START]>): SagaIterator {
  const deviceOrAxis: CurrentAxisOrDevice = yield select(selectCurrentAxisOrDevice);
  const context = {
    state: deviceOrAxis,
    lock: new SagaLock(),
  };

  yield put(actions.clearResultError());

  const { wasStopped } = yield race({
    setup: call(runSetup, context, test),
    wasStopped: take(ActionTypes.SETUP_STOP),
  });

  if (wasStopped) {
    const { unlock }: RT<typeof context.lock.lock> = yield call(context.lock.lock);
    unlock();
    yield put(actions.setSetupStep(SetupStep.SAVE_WITH_TEST_STOPPED));
  }
}

function* runSetup(initContext: Omit<TestSetupContext, 'axis'>, test: boolean): SagaIterator {
  const { state: deviceOrAxis } = initContext;
  let axis: RT<typeof getAxis> | undefined;
  let tempSettings: RT<typeof getTempSettings> | undefined;
  let configSuccess = false;

  const { unlock }: RT<typeof initContext.lock.lock> = yield call(initContext.lock.lock);
  try {
    const axisKey = getAxisKey(deviceOrAxis);
    axis = yield call(getAxis, axisKey);
    if (axis == null) {
      throw new Error(`Unable to get axis ${axisKey}`);
    }
    const context = { ...initContext, axis };
    const { settings: deviceSettings } = axis.device;

    yield call(stopAndClearWarnings, axis);

    tempSettings = yield* getTempSettings(axis);
    if (tempSettings.accessLevel < SYSTEM_ACCESS_ADVANCED) {
      yield call([deviceSettings, deviceSettings.set], 'system.access', SYSTEM_ACCESS_ADVANCED);
    }

    yield call(saveConfiguration, context);

    configSuccess = true;

    if (test) {
      yield call(runTest, context);
      yield put(actions.setSetupStep(SetupStep.SAVE_WITH_TEST_COMPLETE));
    } else {
      yield put(actions.setSetupStep(SetupStep.SAVE_WITHOUT_TEST_COMPLETE));
    }

    yield call(setupDone, deviceOrAxis);
  } catch (err) {
    let message;
    let errContext: ErrorContext;
    if (err instanceof TestSetupError) {
      const testSetupError = (err as TestSetupError);
      errContext = getErrorContext(testSetupError.error);
      message = errorMessages[testSetupError.error];
    } else {
      errContext = ErrorContext.GENERIC;
      message = [
        'Unexpected error occurred.',
        String(err),
      ].join(' ');
      handleNonCriticalError(err, 'info');
    }

    yield put(actions.setResultError(message, errContext));
    if (test) {
      if (configSuccess) {
        yield put(actions.setSetupStep(SetupStep.SAVE_WITH_TEST_ERROR));
      } else {
        yield put(actions.setSetupStep(SetupStep.SAVE_WITHOUT_TEST_ERROR));
      }
    } else {
      yield put(actions.setSetupStep(SetupStep.SAVE_WITHOUT_TEST_ERROR));
    }
  } finally {
    if (axis != null && tempSettings != null) {
      yield* revertTempSettings(axis, tempSettings);
    }
    unlock();
  }
}

function getAxisKey(state: CurrentAxisOrDevice) {
  return state.axis?.key ?? makeAxisKey(state.device.key, 1);
}

function* stopAndClearWarnings(axis: ascii.Axis): SagaIterator {
  try {
    yield call([axis, axis.stop]);
  } catch (err) {
    if (!(err instanceof MotionLibException)) {
      throw err;
    }
  }
}

function* getTempSettings(axis: ascii.Axis) {
  const { settings } = axis;
  const { settings: deviceSettings } = axis.device;

  const limitApproachMaxSpeed: number = yield call([settings, settings.get], 'limit.approach.maxspeed');
  const accessLevel: number = yield call([deviceSettings, deviceSettings.get], 'system.access');

  return {
    limitApproachMaxSpeed,
    accessLevel,
  };
}

function* saveConfiguration(context: TestSetupContext): SagaIterator {
  const { axis } = context;
  const { settings, storage } = axis;
  const { config } = context.state;
  const { stageType, motorOrientation, travelDistanceMm } = config as AxisConfigComplete;

  yield call(disableDriver, axis);

  const direction = DriverDirection[stageType][motorOrientation];
  yield call([settings, settings.set], 'driver.dir', direction);
  yield call([settings, settings.set], 'encoder.dir', direction === 1 ? 0 : 1);
  yield call([settings, settings.set], 'limit.max', travelDistanceMm, Length.MILLIMETRES);
  yield call([storage, storage.setString], STAGE_TYPE_KEY, stageType);

  yield call(enableDriver, axis);
}

function* disableDriver(axis: ascii.Axis): SagaIterator {
  yield call([axis, axis.driverDisable]);
}

function* enableDriver(axis: ascii.Axis): SagaIterator {
  try {
    yield call([axis, axis.driverEnable]);
  } catch (err) {
    if (castAny(err, CommandFailedException)?.details.responseData === RejectReasons.DRIVERDISABLED) {
      throw new TestSetupError(TestSetupErrors.DRIVER_DISABLED);
    } else {
      throw err;
    }
  }
}

function* runTest(context: TestSetupContext): SagaIterator {
  const { axis } = context;
  const { settings } = axis;
  const config = context.state.config as AxisConfigComplete;

  try {
    yield call([settings, settings.set], 'limit.approach.maxspeed', SAFE_APPROACH_SPEED_MM_PER_S, Velocity.MILLIMETRES_PER_SECOND);
    yield call(ensureInFrontOfHomeSensor, axis);
    const useHeuristic = config.travelDistanceMm >= MIN_TRAVEL_DISTANCE_FOR_HEURISTIC_MM;
    yield call(homeAndCheckTravelDistance, axis, useHeuristic);
  } finally {
    if (yield cancelled()) {
      yield call(stopAndClearWarnings, axis);
    }
  }
}

function* ensureInFrontOfHomeSensor(axis: ascii.Axis): SagaIterator {
  let carriageMaybeBehindSensor = false;
  try {
    yield call([axis, axis.genericCommand], 'tools gotolimit home neg 1 0');
    yield call([axis, axis.waitUntilIdle]);
  } catch (err) {
    if (err instanceof MovementFailedException) {
      carriageMaybeBehindSensor = true;
    } else {
      throw err;
    }
  }

  // Products with brake will disable the driver after a stall, this is required to ensure they can continue
  yield call(enableDriver, axis);

  if (carriageMaybeBehindSensor) {
    try {
      yield call([axis, axis.genericCommand], 'tools gotolimit home pos 1 0');
      yield call([axis, axis.waitUntilIdle]);
    } catch (err) {
      if (err instanceof MovementFailedException) {
        throw new TestSetupError(TestSetupErrors.CANNOT_FIND_SENSOR);
      }
      throw err;
    }
  }
}

function* homeAndCheckTravelDistance(axis: ascii.Axis, useHeuristic: boolean): SagaIterator {
  try {
    yield call([axis, axis.home]);
  } catch (err) {
    if (err instanceof MovementFailedException) {
      throw new TestSetupError(TestSetupErrors.CANNOT_HOME);
    }
  }

  const startPos = yield call([axis, axis.getPosition], Length.mm);
  try {
    yield call([axis, axis.moveMax], {
      velocity: SAFE_APPROACH_SPEED_MM_PER_S,
      velocityUnit: Velocity.MILLIMETRES_PER_SECOND
    });
  } catch (err) {
    if (err instanceof MovementFailedException) {
      if (!useHeuristic) {
        throw new TestSetupError(TestSetupErrors.WRONG_ORIENTATION_OR_DISTANCE);
      }

      const endPos = yield call([axis, axis.getPosition], Length.mm);
      const distanceTraveledMm = endPos - startPos;

      if (distanceTraveledMm < MAX_HOME_SENSOR_DISTANCE_HEURISTIC_MM) {
        throw new TestSetupError(TestSetupErrors.WRONG_ORIENTATION);
      } else {
        throw new TestSetupError(TestSetupErrors.WRONG_DISTANCE);
      }
    }
    throw err;
  }

  try {
    yield call([axis, axis.moveMin], {
      velocity: SAFE_APPROACH_SPEED_MM_PER_S,
      velocityUnit: Velocity.MILLIMETRES_PER_SECOND
    });
  } catch (err) {
    if (err instanceof MovementFailedException) {
      throw new TestSetupError(TestSetupErrors.WRONG_ORIENTATION_OR_DISTANCE);
    }
  }
}

function* setupDone(deviceOrAxis: CurrentAxisOrDevice): SagaIterator {
  if (DISPLAY_SETUP_ALL_TIME) {
    return;
  }
  if (deviceOrAxis.axis?.serialNumber != null) {
    yield put(deviceSetupActions.setupNotNeeded(deviceOrAxis.axis.serialNumber));
  } else if (deviceOrAxis.axis == null) {
    yield put(deviceSetupActions.setupNotNeeded(deviceOrAxis.device.identity.serialNumber));
  }
}

function* revertTempSettings(axis: ascii.Axis, data: RT<typeof getTempSettings>) {
  const { settings } = axis;
  const { settings: deviceSettings } = axis.device;

  try {
    yield call([settings, settings.set], 'limit.approach.maxspeed', data.limitApproachMaxSpeed);
    yield call([deviceSettings, deviceSettings.set], 'system.access', data.accessLevel);
  } catch (err) {
    if (!(err instanceof MotionLibException)) {
      throw err;
    }
  }
}

export const CHECK_ESTOP_PERIOD = 1000;

function* estopMounted(): SagaIterator {
  yield race([
    take(ActionTypes.ESTOP_UNMOUNTED),
    call(estopLoop),
  ]);
}

function* estopLoop(): SagaIterator {
  for (; ;) {
    const { enable }: { enable: unknown } = yield race({
      checkStatus: call(function* (): SagaIterator {
        const deviceOrAxis: RT<typeof selectCurrentAxisOrDevice> = yield select(selectCurrentAxisOrDevice);
        if (deviceOrAxis != null) {
          try {
            const axis: RT<typeof getAxis> = yield call(getAxis, getAxisKey(deviceOrAxis));
            const flags: RT<typeof axis.warnings.getFlags> = yield call([axis.warnings, axis.warnings.getFlags]);
            const ready = !flags.has(ascii.WarningFlags.HARDWARE_EMERGENCY_STOP);
            yield put(actions.estopData({ status: 'ok', ready }));
          } catch (err) {
            yield put(actions.estopData({ status: 'error', error: String(err) }));
          }
        }
        yield delay(CHECK_ESTOP_PERIOD);
      }),
      enable: take(ActionTypes.ENABLE_DRIVER),
    });

    if (enable) {
      const deviceOrAxis: RT<typeof selectCurrentAxisOrDevice> = yield select(selectCurrentAxisOrDevice);
      if (deviceOrAxis == null) { return }
      try {
        const axis: RT<typeof getAxis> = yield call(getAxis, getAxisKey(deviceOrAxis));
        yield call([axis, axis.driverEnable]);
      } catch (err) {
        if (castAny(err, CommandFailedException)?.details.responseData !== RejectReasons.DRIVERDISABLED) {
          yield put(actions.estopData({ status: 'error', error: String(err) }));
        }
        yield delay(1000); // for the UI effect
      }
    }
  }
}
