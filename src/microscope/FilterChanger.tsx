import React from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, Icons, Text, SimpleSelect } from '@zaber/react-library';

import { useActions } from '../utils';

import { selectFilterLabels, selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations } from './types';
import { useOperationValue } from './hooks';
import { NextPrevious, OperationError } from './components';
import { FilterChangerSettings } from './FilterChangerSettings';

export const FilterChanger: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.filterChanger;
  const info = monitor.info?.filterChanger;
  const labels = useSelector(selectFilterLabels);

  const [filter, setFilter, inProgress] = useOperationValue(info?.filter ?? 0, Operations.changeFilter);

  if (stableInfo == null || info == null) {
    return null;
  }

  const options = labels.map((label, i) => ({ label, value: i + 1 }));
  if (info.filter === 0) {
    options.push({ label: '', value: 0 });
  }

  const changeTo = (filter: number, home?: boolean) => {
    if (filter === 0) { return }
    setFilter(filter);
    actions.operationStart(Operations.changeFilter, { filter, home });
  };

  return <HeaderCard
    className="controls filter-changer"
    header={<>
      <Text t={Text.Type.H4}>Filter Changer</Text>
      <div className="icons">
        {!stableInfo.hasEncoder &&
          <Icons.Home
            title="Home (Re-align) Filter Changer"
            disabled={inProgress}
            onClick={() => changeTo(1, true)}/>}
        <FilterChangerSettings/>
      </div>
    </>}>
    <div className="select-row">
      <SimpleSelect
        title="Current Filter" portalToBody
        disabled={inProgress}
        value={filter}
        onValueChange={changeTo}
        options={options}/>
      <NextPrevious current={filter} changeTo={changeTo} numberOf={stableInfo.numberOf}/>
      {inProgress && <Text>Changing...</Text>}
    </div>
    <OperationError operation={Operations.changeFilter}/>
  </HeaderCard>;
};
