import { useContext, createContext } from 'react';
import * as monaco from 'monaco-editor';

import { BlockMessage } from './types';

export interface EditorContextType {
  editor: monaco.editor.IStandaloneCodeEditor;
  model: monaco.editor.ITextModel;
  setPositionToMessage(message: BlockMessage, immediateScroll: boolean): void;
}

export const EditorContext = createContext<EditorContextType>(null!);

export const useEditor = () => useContext(EditorContext);
