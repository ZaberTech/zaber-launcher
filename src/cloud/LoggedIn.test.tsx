import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

class User {
  attributes = [{
    Name: 'email',
    Value: 'email@test.io',
  }];
}

class AuthMock {
  static currentUserPoolUser = jest.fn(() => Promise.resolve(new User()));
  static userAttributes = jest.fn((user: User) => user.attributes);
  static signOut = jest.fn(() => Promise.resolve());
}

jest.mock('aws-amplify', () => ({
  Auth: AuthMock,
}));

jest.mock('./CloudDiscover', () => <>CloudDiscover</>);
jest.mock('./CloudSharing', () => <>CloudSharing</>);

@injectable()
class AwsCredentialsServiceMock {
  setUser = jest.fn().mockResolvedValue(null);
}

import { createContainer, destroyContainer } from '../container';
import { StorageMock, mockStorage, waitUntil, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import { MessageRoutersService } from '../message_router';
import { AwsCredentialsService } from '../app_components';

import { CLOUD_SHARE_ID_STORAGE_KEY, TEST_CHECK_LOGGED_IN } from './sagas';
import { LoggedIn } from './LoggedIn';
import { selectAuthState } from './selectors';
import { AuthState } from './types';
import { MessageRoutersServiceMock } from './mocks';

let container: Container;

let credentialsMock: AwsCredentialsServiceMock;
let messageRouter: MessageRoutersServiceMock;
let storageMock: StorageMock;

const TestLoggedIn = wrapWithNewStore(wrapWithRouter(LoggedIn));

let wrapper: RenderResult;

beforeEach(async () => {
  AuthMock.currentUserPoolUser.mockClear();
  AuthMock.userAttributes.mockClear();

  container = createContainer();
  container.bind<unknown>(AwsCredentialsService).to(AwsCredentialsServiceMock);
  credentialsMock = container.get<unknown>(AwsCredentialsService) as AwsCredentialsServiceMock;

  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  messageRouter = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;
  messageRouter.mockSharingOn();

  storageMock = mockStorage(container);

  wrapper = render(<TestLoggedIn/>);

  TestLoggedIn.testStore.dispatch({ type: TEST_CHECK_LOGGED_IN });
  await waitUntil(() => selectAuthState(TestLoggedIn.testStore.getState()) === AuthState.Authenticated);
  credentialsMock.setUser.mockClear();
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

test('renders with user', () => {
  wrapper.getByText(/email@test.io/);
});

test('retrieves cloud config and status from the router', async () => {
  await waitUntilPass(() => {
    expect(messageRouter.getCloudConfig).toHaveBeenCalled();
    expect(messageRouter.getCloudStatus).toHaveBeenCalled();
  });
});

describe('logout', () => {
  const clickLogout = () => {
    fireEvent.click(wrapper.getByText(/email@test.io/));
    fireEvent.click(wrapper.getByText(/Sign out/));
  };

  test('logouts user', async () => {
    clickLogout();

    await waitUntilPass(() => expect(AuthMock.signOut).toHaveBeenCalled());
    expect(selectAuthState(TestLoggedIn.testStore.getState())).toBe(AuthState.Unauthenticated);
    expect(credentialsMock.setUser).toHaveBeenCalledWith('unauthenticated');
  });

  test('cleans cloud ID from storage', async () => {
    storageMock.stored[CLOUD_SHARE_ID_STORAGE_KEY] = 'cloud_id';
    clickLogout();

    await waitUntilPass(() => expect(storageMock.stored[CLOUD_SHARE_ID_STORAGE_KEY]).toBeNull());
  });

  test('clears credentials despite the error', async () => {
    AuthMock.signOut.mockRejectedValueOnce(new Error('Something broke'));

    clickLogout();

    await waitUntilPass(() => expect(credentialsMock.setUser).toHaveBeenCalledWith('unauthenticated'));
  });

  test('turns off sharing before logging out', async () => {
    await waitUntil(() => !!TestLoggedIn.testStore.getState().cloud.sharingStatus);

    clickLogout();

    await waitUntilPass(() => expect(messageRouter.setCloudConfig).toHaveBeenCalledWith(null));
  });
});
