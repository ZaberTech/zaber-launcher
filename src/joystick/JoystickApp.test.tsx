/* eslint-disable no-cond-assign */
import React from 'react';
import { injectable } from 'inversify';
import { fireEvent, getByText, render, RenderResult } from '@testing-library/react';
import _ from 'lodash';

const JoystickPicture: React.FC<{
  onClick: (button: number) => void;
}> = ({ onClick }) => (<>
  {_.range(8).map(i => <button key={i} onClick={() => onClick(i + 1)}>
    Select Key {i + 1}
  </button>)}
</>);

jest.mock('./Picture', () => ({
  JoystickPicture,
}));

const readFileMock = jest.fn(async (_path: string) => '');
const writeFileMock = jest.fn(async (_path: string, _data: string) => undefined);

jest.mock('fs/promises', () => ({
  readFile: readFileMock,
  writeFile: writeFileMock,
}));

const showSaveDialogMock = jest.fn(async () => ({ filePath: 'filepath', canceled: false }));
const showOpenDialogMock = jest.fn(async () => ({ filePaths: ['filepath'], canceled: false }));

jest.mock('../dialogs', () => ({
  Dialogs: {
    showSaveDialog: showSaveDialogMock,
    showOpenDialog: showOpenDialogMock,
  },
}));


import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { SettingsMock } from '../test/mocks/settings';
import { wrapWithNewStore, wrapWithRouter, waitUntilPass, waitTick, StorageMock } from '../test';
import { createContainer, destroyContainer } from '../container';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import { mockDataForDeviceWithoutPeripherals, mockDataForDeviceWithPeripherals, mockLocalConnections } from '../connection_manager/mocks';
import { DeviceInfoWithAxes, selectDevices } from '../connection_manager';
import { makeAxisKey, makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';
import { Storage } from '../app_components';

import { JoystickApp } from './JoystickApp';
import { KeyEventType, KeyEventTypes } from './types';
import { PRODUCTION_MODE_STORAGE_KEY } from './sagas';

let axisCallback: (device: AxisMock) => void;

class AxisMock extends AxisMockBase {
  settings = new SettingsMock();

  constructor(id: number, device: DeviceMock) {
    super(id, device);

    this.settings.values.maxspeed = { value: 1000 };
    axisCallback?.(this);
  }
}

interface JoyStickAxis {
  device: number;
  axis: number;
  axisType: string;
  maxspeed: number;
  speedProfile: number;
  inverted: number;
  resolution: number;
}

interface AlertEnabled {
  key: number;
  event: KeyEventType;
}

interface KeyCommand {
  key: number;
  event: KeyEventType;
  device: number;
  axis: number;
  cmd: string;
}

let device: DeviceMock;
let deviceCallback: (device: DeviceMock) => void;

class DeviceMock extends DeviceMockBase<AxisMock> {
  settings = new SettingsMock();

  constructor(readonly deviceAddress: number, readonly connection: ConnectionMock) {
    super(deviceAddress, connection);
    if (deviceAddress === 1) {
      // eslint-disable-next-line @typescript-eslint/no-this-alias
      device = this;
    }
    deviceCallback?.(this);
  }

  readonly stickAxes: JoyStickAxis[] = [
    { device: 2, axis: 1, axisType: '', maxspeed: 1000, speedProfile: 1, inverted: 0, resolution: 50 },
    { device: 3, axis: 1, axisType: '', maxspeed: 500, speedProfile: 1, inverted: 0, resolution: 50 },
    { device: 3, axis: 2, axisType: '', maxspeed: 650, speedProfile: 2, inverted: 1, resolution: 50 },
  ];

  alertsEnabled: AlertEnabled[] = [
    { key: 1, event: KeyEventType.Press },
  ];

  keyCommands: KeyCommand[] = [
    { key: 1, event: KeyEventType.Press, device: 0, axis: 0, cmd: 'home' },
    { key: 1, event: KeyEventType.Release, device: 0, axis: 0, cmd: 'stop' },
    { key: 2, event: KeyEventType.LongPress, device: 2, axis: 0, cmd: 'driver disabled' },
    { key: 3, event: KeyEventType.Press, device: 3, axis: 0, cmd: 'move min' },
    { key: 3, event: KeyEventType.Release, device: 3, axis: 1, cmd: 'move max' },
  ];

  genericCommand = jest.fn(async (cmd: string) => {
    let match: ReturnType<typeof cmd.match>;
    if (match = cmd.match(/^joystick (\d) info$/)) {
      const axis = this.stickAxes[+match[1] - 1];
      return { data: [axis.device, axis.axisType, axis.axis, axis.maxspeed, axis.speedProfile, axis.inverted, axis.resolution].join(' ') };
    } else if (cmd.match(/^joystick (\d) \w+/)) {
      return { data: '0' };
    } else if (cmd.match(/^key (\d) (\d) (clear|alert|add)/)) {
      return { data: '0' };
    } else if (cmd.match(/^joystick calibrate /)) {
      return { data: '0' };
    } else {
      throw new Error(`Unknown command ${cmd}`);
    }
  });
  genericCommandMultiResponse = jest.fn(async (cmd: string) => {
    let match: ReturnType<typeof cmd.match>;
    if (match = cmd.match(/^key (\d) (\d) info$/)) {
      const key = +match[1];
      const eventType = +match[2] as KeyEventType;
      return [
        { data: `alerts ${this.alertsEnabled.find(a => a.key === key && a.event === eventType) ? 'enabled' : 'disabled'}` },
        ...this.keyCommands
          .filter(a => a.key === key && a.event === eventType)
          .map(cmd => ({ data: `cmd ${cmd.device} ${cmd.axis} ${cmd.cmd}` })),
      ];
    } else {
      throw new Error(`Unknown command ${cmd}`);
    }
  });
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

function xJoyModifier(info: DeviceInfoWithAxes): DeviceInfoWithAxes {
  info.identity!.deviceId = 51000;
  return info;
}

function mockDeviceChain() {
  function addLockstepModifier(info: DeviceInfoWithAxes): DeviceInfoWithAxes {
    info.locksteps = [{
      groupNumber: 1, axisNumbers: [2, 3],
    }];
    return info;
  }

  mockLocalConnections(TestJoystickApp.testStore, {
    COM1: connectionKey => [
      xJoyModifier(mockDataForDeviceWithoutPeripherals(connectionKey, 1, 0)),
      mockDataForDeviceWithoutPeripherals(connectionKey, 2, 1),
      mockDataForDeviceWithPeripherals(connectionKey, 3, 4, 'smart'),
      addLockstepModifier(mockDataForDeviceWithPeripherals(connectionKey, 4, 4, 'smart')),
    ],
  });
}

let storageMock: StorageMock;

const TestJoystickApp = wrapWithNewStore(wrapWithRouter(JoystickApp));
let wrapper: RenderResult;

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(Storage).to(StorageMock);
  storageMock = container.get<unknown>(Storage) as StorageMock;

  wrapper = render(<TestJoystickApp/>);

  mockDeviceChain();
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  device = null!;
  deviceCallback = null!;
  axisCallback = null!;

  destroyContainer();

  readFileMock.mockClear();
  writeFileMock.mockClear();

  showOpenDialogMock.mockClear();
  showSaveDialogMock.mockClear();

  storageMock = null!;
});

const CONNECTION_KEY = makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), 'COM1');

function makeKeyClear(key: number, alerts: KeyEventType[] = []) {
  return [
    `key ${key} 1 clear`,
    `key ${key} 1 alert ${+alerts.includes(1)}`,
    `key ${key} 2 clear`,
    `key ${key} 2 alert ${+alerts.includes(2)}`,
    `key ${key} 3 clear`,
    `key ${key} 3 alert ${+alerts.includes(3)}`,
    `key ${key} 4 clear`,
    `key ${key} 4 alert ${+alerts.includes(4)}`,
  ];
}

function selectXJoy() {
  const xjoy = _.find(selectDevices(TestJoystickApp.testStore.getState()), device => device.address === 1)!;
  connectionViewMockInstance.props.onSelect(xjoy.key);
}

async function writeToDevice() {
  device.genericCommand.mockClear();
  fireEvent.click(wrapper.getByText('Apply'));
  await waitUntilPass(() => wrapper.getByText('Apply'));
}

test('populates configuration when joystick gets selected', async () => {
  selectXJoy();

  const LOADING = 'Reading configuration...';
  wrapper.getByText(LOADING);
  await waitUntilPass(() => wrapper.getByText('Apply'));
  expect(wrapper.queryByText(LOADING)).toBeNull();
});

test('shows error and try again button if the joystick cannot be contacted', async () => {
  deviceCallback = device => device.genericCommand.mockRejectedValueOnce(new Error('Cannot connect'));

  selectXJoy();

  await waitUntilPass(() => wrapper.getByText('Cannot connect'));
  fireEvent.click(wrapper.getByText('Try Again'));

  await waitUntilPass(() => wrapper.getByText('Axis X'));
  expect(wrapper.queryByText('Cannot connect')).toBeNull();
});

describe('joystick selected', () => {
  beforeEach(async () => {
    selectXJoy();

    await waitUntilPass(() => wrapper.getByText('Apply'));
    device.genericCommand.mockClear();
  });

  test('populates the configuration', async () => {
    expect(wrapper.getByTestId('device-select-1')).toHaveValue('2');
    expect(wrapper.queryByTestId('axis-select-1')).toHaveValue(makeAxisKey(makeDeviceKey(CONNECTION_KEY, 2), 1));
    expect(wrapper.getByTestId('maxspeed-1')).toHaveValue(100);
    expect(wrapper.getByTestId('speed-profile-1')).toHaveValue('1');
    expect(wrapper.getByTestId('inverted-1')).toHaveValue('0');

    expect(wrapper.getByTestId('device-select-2')).toHaveValue('3');
    expect(wrapper.getByTestId('axis-select-2')).toHaveValue(makeAxisKey(makeDeviceKey(CONNECTION_KEY, 3), 1));
    expect(wrapper.getByTestId('maxspeed-2')).toHaveValue(50);
    expect(wrapper.getByTestId('speed-profile-2')).toHaveValue('1');
    expect(wrapper.getByTestId('inverted-2')).toHaveValue('0');

    expect(wrapper.getByTestId('device-select-3')).toHaveValue('3');
    expect(wrapper.getByTestId('axis-select-3')).toHaveValue(makeAxisKey(makeDeviceKey(CONNECTION_KEY, 3), 2));
    expect(wrapper.getByTestId('maxspeed-3')).toHaveValue(65);
    expect(wrapper.getByTestId('speed-profile-3')).toHaveValue('2');
    expect(wrapper.getByTestId('inverted-3')).toHaveValue('1');

    expect(wrapper.getByTestId('key-event-select-0')).toHaveValue('1');
    expect(wrapper.getByTestId('key-device-select-0')).toHaveValue('0');
    expect(wrapper.queryByTestId('key-axis-select-0')).toHaveValue('axis-0');
    expect(wrapper.getByTestId('key-cmd-0')).toHaveValue('home');

    expect(wrapper.getByTestId('key-event-select-1')).toHaveValue('2');
    expect(wrapper.getByTestId('key-device-select-1')).toHaveValue('0');
    expect(wrapper.queryByTestId('key-axis-select-1')).toHaveValue('axis-0');
    expect(wrapper.getByTestId('key-cmd-1')).toHaveValue('stop');

    expect(wrapper.getByLabelText(/Protocol Alerts/)).toBeChecked();

    fireEvent.click(wrapper.getByText('Select Key 2'));

    expect(wrapper.getByTestId('key-event-select-0')).toHaveValue('3');
    expect(wrapper.getByTestId('key-device-select-0')).toHaveValue('2');
    expect(wrapper.queryByTestId('key-axis-select-1')).toBeNull();
    expect(wrapper.getByTestId('key-cmd-0')).toHaveValue('driver disabled');

    expect(wrapper.getByLabelText(/Protocol Alerts/)).not.toBeChecked();

    fireEvent.click(wrapper.getByText('Select Key 3'));

    expect(wrapper.getByTestId('key-event-select-0')).toHaveValue('1');
    expect(wrapper.getByTestId('key-device-select-0')).toHaveValue('3');
    expect(wrapper.getByTestId('key-axis-select-0')).toHaveValue('axis-0');
    expect(wrapper.getByTestId('key-cmd-0')).toHaveValue('move min');

    expect(wrapper.getByTestId('key-event-select-1')).toHaveValue('2');
    expect(wrapper.getByTestId('key-device-select-1')).toHaveValue('3');
    expect(wrapper.getByTestId('key-axis-select-1')).toHaveValue('axis-1');
    expect(wrapper.getByTestId('key-cmd-1')).toHaveValue('move max');
  });

  test('writes back changed configuration', async () => {
    const disabledOption = getByText(wrapper.getByTestId('device-select-1'), 'Disabled');
    fireEvent.change(wrapper.getByTestId('device-select-1'), { target: { value: disabledOption.getAttribute('value') } });

    fireEvent.change(wrapper.getByTestId('device-select-2'), { target: { value: '3' } });
    let axisOption = getByText(wrapper.getByTestId('axis-select-2'), /Axis 2/);
    fireEvent.change(wrapper.getByTestId('axis-select-2'), { target: { value: axisOption.getAttribute('value') } });
    fireEvent.change(wrapper.getByTestId('maxspeed-2'), { target: { value: '30' } });
    fireEvent.change(wrapper.getByTestId('speed-profile-2'), { target: { value: '2' } });
    fireEvent.change(wrapper.getByTestId('inverted-2'), { target: { value: '1' } });

    fireEvent.change(wrapper.getByTestId('device-select-3'), { target: { value: '2' } });
    expect(wrapper.queryByTestId('axis-select-3')).toHaveValue(makeAxisKey(makeDeviceKey(CONNECTION_KEY, 2), 1));
    fireEvent.change(wrapper.getByTestId('maxspeed-2'), { target: { value: '70' } });

    fireEvent.click(wrapper.getByText('Select Key 3'));
    fireEvent.click(wrapper.queryAllByTitle('Delete Event')[0]);
    fireEvent.click(wrapper.getByLabelText(/Protocol Alerts/));

    fireEvent.click(wrapper.getByText('Select Key 1'));
    fireEvent.change(wrapper.getByTestId('key-event-select-1'), { target: { value: '3' } });
    fireEvent.change(wrapper.getByTestId('key-device-select-1'), { target: { value: '3' } });
    axisOption = getByText(wrapper.getByTestId('key-axis-select-1'), /Axis 2/);
    fireEvent.change(wrapper.getByTestId('key-axis-select-1'), { target: { value: axisOption.getAttribute('value') } });
    fireEvent.change(wrapper.getByTestId('key-cmd-1'), { target: { value: 'estop' } });

    fireEvent.click(wrapper.getByText('Select Key 6'));
    fireEvent.click(wrapper.getByText('Add Event'));
    fireEvent.change(wrapper.getByTestId('key-event-select-0'), { target: { value: '3' } });
    fireEvent.change(wrapper.getByTestId('key-device-select-0'), { target: { value: '2' } });
    fireEvent.change(wrapper.getByTestId('key-cmd-0'), { target: { value: 'tools echo' } });

    fireEvent.click(wrapper.getByText('Apply'));
    expect(wrapper.getByText('Applying...')).toHaveClass('disabled');
    await waitUntilPass(() => wrapper.getByText('Apply'));

    expect(device.genericCommand.mock.calls.map(call => call[0])).toEqual([
      'joystick 1 target 99  0',
      'joystick 1 speedprofile 1',
      'joystick 1 maxspeed 0',
      'joystick 1 invert 0',
      'joystick 1 resolution 50',
      'joystick 2 target 3  2',
      'joystick 2 speedprofile 2',
      'joystick 2 maxspeed 700',
      'joystick 2 invert 1',
      'joystick 2 resolution 50',
      'joystick 3 target 2  1',
      'joystick 3 speedprofile 2',
      'joystick 3 maxspeed 650',
      'joystick 3 invert 1',
      'joystick 3 resolution 50',
      ...makeKeyClear(1, [KeyEventType.Press]),
      'key 1 1 add 0 0 home',
      'key 1 3 add 3 2 estop',
      ...makeKeyClear(2),
      'key 2 3 add 2 0 driver disabled',
      ...makeKeyClear(3, KeyEventTypes),
      'key 3 2 add 3 1 move max',
      ...makeKeyClear(4),
      ...makeKeyClear(5),
      ...makeKeyClear(6),
      'key 6 3 add 2 0 tools echo',
      ...makeKeyClear(7),
      ...makeKeyClear(8),
    ]);
  });

  test('indicates that there are changes to apply', async () => {
    expect(wrapper.queryByText('Changes pending')).toBeNull();
    fireEvent.change(wrapper.getByTestId('device-select-2'), { target: { value: '3' } });
    wrapper.getByText('Changes pending');
    await writeToDevice();
    expect(wrapper.queryByText('Changes pending')).toBeNull();
  });

  test('restore - correctly reverts pending changes', async () => {
    expect(wrapper.queryByText('Changes pending')).toBeNull();
    expect(wrapper.getByTestId('device-select-1')).toHaveValue('2');

    fireEvent.change(wrapper.getByTestId('device-select-1'), { target: { value: '3' } });
    wrapper.getByText('Changes pending');
    expect(wrapper.getByTestId('device-select-1')).toHaveValue('3');

    fireEvent.click(wrapper.getByTitle('Revert Pending Changes'));

    await waitUntilPass(() => expect(wrapper.queryByText('Changes pending')).toBeNull());
    expect(wrapper.getByTestId('device-select-1')).toHaveValue('2');
  });

  test('restore - shows error if reverting pending changes fails', async () => {
    device.genericCommand.mockRejectedValue(new Error('Cannot connect to device'));

    expect(wrapper.queryByText('Changes pending')).toBeNull();
    expect(wrapper.getByTestId('device-select-1')).toHaveValue('2');

    fireEvent.change(wrapper.getByTestId('device-select-1'), { target: { value: '3' } });
    wrapper.getByText('Changes pending');
    expect(wrapper.getByTestId('device-select-1')).toHaveValue('3');

    fireEvent.click(wrapper.getByTitle('Revert Pending Changes'));
    await waitUntilPass(() => expect(wrapper.queryByText('Cannot connect to device')));
  });

  test('shows error if the configuration cannot be written to the device', async () => {
    device.genericCommand.mockRejectedValueOnce(new Error('Cannot connect'));

    await writeToDevice();
    wrapper.getByText('Cannot connect');

    await writeToDevice();
    expect(wrapper.queryByText('Cannot connect')).toBeNull();
  });

  describe('calibration', () => {
    beforeEach(() => {
      fireEvent.click(wrapper.getByTitle(/Open Menu/));
      fireEvent.click(wrapper.getByText(/Calibrate Joystick/));
    });

    test('calibrating limits', async () => {
      fireEvent.click(wrapper.getByText('Start Limit Calibration'));
      await waitUntilPass(() =>
        expect(device.genericCommand).toHaveBeenLastCalledWith('joystick calibrate limits start'));

      fireEvent.click(wrapper.getByText('Finish Calibration'));
      await waitUntilPass(() =>
        expect(device.genericCommand).toHaveBeenLastCalledWith('joystick calibrate limits save'));
    });

    test('calibrating deadbands', async () => {
      fireEvent.click(wrapper.getByText('Start Deadband Calibration'));
      await waitUntilPass(() =>
        expect(device.genericCommand).toHaveBeenLastCalledWith('joystick calibrate deadbands start'));

      fireEvent.click(wrapper.getByText('Finish Calibration'));
      await waitUntilPass(() =>
        expect(device.genericCommand).toHaveBeenLastCalledWith('joystick calibrate deadbands save'));
    });

    test('cancelling calibration', async () => {
      fireEvent.click(wrapper.getByText('Start Limit Calibration'));
      await waitTick();

      fireEvent.click(wrapper.getByText('Cancel Calibration'));
      await waitUntilPass(() =>
        expect(device.genericCommand).toHaveBeenLastCalledWith('system reset'));

      device.genericCommand.mockClear();
      fireEvent.click(wrapper.getByText('Start Deadband Calibration'));
      await waitTick();

      fireEvent.click(wrapper.getByText('Cancel Calibration'));
      await waitUntilPass(() =>
        expect(device.genericCommand).toHaveBeenLastCalledWith('system reset'));
    });

    test('shows error', async () => {
      device.genericCommand.mockRejectedValueOnce(new Error('Failure'));
      fireEvent.click(wrapper.getByText('Start Limit Calibration'));
      await waitUntilPass(() => wrapper.getByText('Failure'));
    });

    test('dialog cannot be closed during calibration', async () => {
      fireEvent.click(wrapper.getByText('Start Limit Calibration'));

      fireEvent.click(wrapper.getByTitle('Close Dialog'));

      fireEvent.click(wrapper.queryAllByText('Cancel Calibration')[0]);

      fireEvent.click(wrapper.getByTitle('Close Dialog'));
      expect(wrapper.queryByTitle('Close Dialog')).toBeNull();
    });
  });

  describe('save/load', () => {
    beforeEach(() => {
      fireEvent.click(wrapper.getByTitle(/Open Menu/));
    });

    test('saves configuration structure', async () => {
      fireEvent.click(wrapper.getByText(/Save to File/));

      await waitUntilPass(() =>
        expect(writeFileMock).toHaveBeenLastCalledWith('filepath', expect.anything()));

      const structure = JSON.parse(writeFileMock.mock.calls[0][1]);
      expect(structure).toMatchObject(expect.objectContaining({
        type: 'zaber-joystick-config',
        version: 2,
        deviceId: 51000,
        axes: expect.arrayContaining(_.range(3).map(() => expect.anything())),
        keys: expect.arrayContaining(_.range(8).map(() => expect.anything())),
      }));
    });

    test('cancels save if file is not picked', async () => {
      showSaveDialogMock.mockResolvedValueOnce({ canceled: true, filePath: null! });
      fireEvent.click(wrapper.getByText(/Save to File/));
      await waitUntilPass(() => expect(showSaveDialogMock).toHaveBeenCalled());
      expect(writeFileMock).not.toHaveBeenCalled();
    });

    test('displays error if config cannot be saved', async () => {
      writeFileMock.mockRejectedValueOnce(new Error('Cannot save file'));
      fireEvent.click(wrapper.getByText(/Save to File/));
      await waitUntilPass(() => wrapper.getByText(/Cannot save file/));
    });

    test('loads configuration structure', async () => {
      readFileMock.mockResolvedValueOnce(JSON.stringify({
        deviceId: 51000,
        type: 'zaber-joystick-config',
        version: 2,
        axes: _.range(3).map(i => ({
          axisNumber: i + 1,
          targetDevice: 3,
          targetAxis: i + 1,
          axisType: 'axis',
          inverted: false,
          maxspeedNative: (i + 1) * 100,
          profile: 1,
          resolution: 50,
        })),
        keys: [{
          keyNumber: 1,
          alerts: {
            1: true,
            2: false,
            3: false,
            4: false,
          },
          commands: [{
            event: 1,
            targetDevice: 2,
            targetAxis: 0,
            axisType: 'axis',
            command: 'move max',
          }, {
            event: 2,
            targetDevice: 3,
            targetAxis: 1,
            axisType: 'axis',
            command: 'stop',
          }],
        },
        ..._.range(7).map(i => ({
          keyNumber: i + 2,
          alerts: {
            1: false,
            2: false,
            3: false,
            4: false,
          },
          commands: [],
        }))],
      }));

      fireEvent.click(wrapper.getByText(/Load from File/));

      await waitUntilPass(() =>
        expect(readFileMock).toHaveBeenLastCalledWith('filepath'));

      const device3Key  = makeDeviceKey(CONNECTION_KEY, 3);

      expect(wrapper.getByTestId('device-select-1')).toHaveValue('3');
      expect(wrapper.getByTestId('axis-select-1')).toHaveValue(makeAxisKey(device3Key, 1));
      expect(wrapper.getByTestId('maxspeed-1')).toHaveValue(10);

      expect(wrapper.getByTestId('device-select-2')).toHaveValue('3');
      expect(wrapper.getByTestId('axis-select-2')).toHaveValue(makeAxisKey(device3Key, 2));
      expect(wrapper.getByTestId('maxspeed-2')).toHaveValue(20);

      expect(wrapper.getByTestId('key-event-select-0')).toHaveValue('1');
      expect(wrapper.getByTestId('key-device-select-0')).toHaveValue('2');
      expect(wrapper.queryByTestId('key-axis-select-0')).toHaveValue('axis-0');
      expect(wrapper.getByTestId('key-cmd-0')).toHaveValue('move max');

      expect(wrapper.getByTestId('key-event-select-1')).toHaveValue('2');
      expect(wrapper.getByTestId('key-device-select-1')).toHaveValue('3');
      expect(wrapper.getByTestId('key-axis-select-1')).toHaveValue('axis-1');
      expect(wrapper.getByTestId('key-cmd-1')).toHaveValue('stop');

      expect(wrapper.getByLabelText(/Protocol Alerts/)).toBeChecked();
    });

    test('displays error if config cannot be loaded', async () => {
      readFileMock.mockRejectedValueOnce(new Error('Cannot read file'));
      fireEvent.click(wrapper.getByText(/Load from File/));
      await waitUntilPass(() => wrapper.getByText(/Cannot read file/));
    });

    test('displays error if config type does not match', async () => {
      readFileMock.mockResolvedValueOnce(JSON.stringify({
        deviceId: 51000,
        type: 'zaber-pedal-config',
        version: 1,
      }));
      fireEvent.click(wrapper.getByText(/Load from File/));
      await waitUntilPass(() => wrapper.getByText(/File does not appear to be a joystick config/));
    });

    test('displays error if config version does not match', async () => {
      readFileMock.mockResolvedValueOnce(JSON.stringify({
        deviceId: 51000,
        type: 'zaber-joystick-config',
        version: 3,
      }));
      fireEvent.click(wrapper.getByText(/Load from File/));
      await waitUntilPass(() => wrapper.getByText(/Unsupported config file version 3/));
    });

    test('displays error if device id does not match', async () => {
      readFileMock.mockResolvedValueOnce(JSON.stringify({
        deviceId: 123,
        type: 'zaber-joystick-config',
        version: 1,
      }));
      fireEvent.click(wrapper.getByText(/Load from File/));
      await waitUntilPass(() => wrapper.getByText(/Configuration not compatible with device/));
    });

    test('cancels load if file is not picked', async () => {
      showOpenDialogMock.mockResolvedValueOnce({ canceled: true, filePaths: null! });
      fireEvent.click(wrapper.getByText(/Load from File/));
      await waitUntilPass(() => expect(showOpenDialogMock).toHaveBeenCalled());
      expect(readFileMock).not.toHaveBeenCalled();
    });
  });

  describe('production mode', () => {
    function turnOn() {
      fireEvent.click(wrapper.getByTitle(/Open Menu/));
      fireEvent.click(wrapper.getByText(/Production Mode/));
    }

    test('persists the mode', () => {
      expect(storageMock.load).toHaveBeenCalledWith(PRODUCTION_MODE_STORAGE_KEY);
      turnOn();
      expect(storageMock.save).toHaveBeenLastCalledWith(PRODUCTION_MODE_STORAGE_KEY, true);
    });

    test('works in native speed that does not change when changing devices', async () => {
      turnOn();

      expect(wrapper.getByTestId('maxspeed-1')).toHaveValue(1000);

      fireEvent.change(wrapper.getByTestId('device-select-1'), { target: { value: '3' } });
      const axisOption = getByText(wrapper.getByTestId('axis-select-1'), /Axis 2/);
      fireEvent.change(wrapper.getByTestId('axis-select-1'), { target: { value: axisOption.getAttribute('value') } });

      expect(wrapper.getByTestId('maxspeed-1')).toHaveValue(1000);
      fireEvent.change(wrapper.getByTestId('maxspeed-1'), { target: { value: '1500' } });

      fireEvent.change(wrapper.getByTestId('device-select-1'), { target: { value: '2' } });

      expect(wrapper.getByTestId('maxspeed-1')).toHaveValue(1500);
    });
  });
});

test('can read and write lockstep axes', async () => {
  deviceCallback = device => {
    if (device.deviceAddress !== 1) {
      return;
    }

    device.stickAxes[2].device = 4;
    device.stickAxes[2].axisType = 'lockstep';
    device.stickAxes[2].axis = 1; // group number
  };

  selectXJoy();
  await waitUntilPass(() => wrapper.getByText('Apply'));

  const expectedAxis = getByText(wrapper.getByTestId('axis-select-3'), /Axis 2/);
  expect(wrapper.getByTestId('axis-select-3')).toHaveValue(expectedAxis.getAttribute('value'));

  const disabledOption = getByText(wrapper.getByTestId('device-select-3'), 'Disabled');
  fireEvent.change(wrapper.getByTestId('device-select-3'), { target: { value: disabledOption.getAttribute('value') } });

  fireEvent.change(wrapper.getByTestId('device-select-1'), { target: { value: '4' } });
  const axisOption = getByText(wrapper.getByTestId('axis-select-1'), /Axis 2/);
  fireEvent.change(wrapper.getByTestId('axis-select-1'), { target: { value: axisOption.getAttribute('value') } });

  await writeToDevice();

  expect(device.genericCommand.mock.calls.map(call => call[0]).filter(cmd => cmd.startsWith('joy'))).toEqual([
    'joystick 1 target 4 lockstep 1',
    'joystick 1 speedprofile 1',
    'joystick 1 maxspeed 1000',
    'joystick 1 invert 0',
    'joystick 1 resolution 50',
    'joystick 2 target 3  1',
    'joystick 2 speedprofile 1',
    'joystick 2 maxspeed 500',
    'joystick 2 invert 0',
    'joystick 2 resolution 50',
    'joystick 3 target 99  0',
    'joystick 3 speedprofile 2',
    'joystick 3 maxspeed 0',
    'joystick 3 invert 1',
    'joystick 3 resolution 50',
  ]);
});

test('can read and write lockstep buttons', async () => {
  deviceCallback = device => {
    if (device.deviceAddress !== 1) {
      return;
    }

    device.keyCommands[0].device = 4;
    device.keyCommands[0].axis = 2;

    device.keyCommands[1].device = 4;
    device.keyCommands[1].axis = 0;
    device.keyCommands[1].cmd = 'lockstep 1 move max';
  };

  selectXJoy();
  await waitUntilPass(() => wrapper.getByText('Apply'));

  const expectedAxis = getByText(wrapper.getByTestId('key-axis-select-0'), /^Axis 2 \S+$/);
  expect(wrapper.getByTestId('key-axis-select-0')).toHaveValue(expectedAxis.getAttribute('value'));

  const expectedAxis2 = getByText(wrapper.getByTestId('key-axis-select-1'), /^Axis 2 \S+ \(Lockstep 1\)$/);
  expect(wrapper.getByTestId('key-axis-select-1')).toHaveValue(expectedAxis2.getAttribute('value'));

  fireEvent.click(wrapper.getByText('Add Event'));
  fireEvent.change(wrapper.getByTestId('key-event-select-2'), { target: { value: '3' } });
  fireEvent.change(wrapper.getByTestId('key-device-select-2'), { target: { value: '4' } });
  fireEvent.change(wrapper.getByTestId('key-axis-select-2'), { target: { value: expectedAxis2.getAttribute('value') } });
  fireEvent.change(wrapper.getByTestId('key-cmd-2'), { target: { value: 'move abs 123' } });

  await writeToDevice();

  expect(device.genericCommand.mock.calls.map(call => call[0]).filter(cmd => cmd.startsWith('key 1'))).toEqual([
    'key 1 1 clear',
    'key 1 1 alert 1',
    'key 1 2 clear',
    'key 1 2 alert 0',
    'key 1 3 clear',
    'key 1 3 alert 0',
    'key 1 4 clear',
    'key 1 4 alert 0',
    'key 1 1 add 4 2 home',
    'key 1 2 add 4 0 lockstep 1 move max',
    'key 1 3 add 4 0 lockstep 1 move abs 123',
  ]);
});

test('can deal with unknown devices and axes', async () => {
  deviceCallback = device => {
    if (device.deviceAddress !== 1) {
      return;
    }

    device.stickAxes[0].device = 13;
    device.stickAxes[0].axis = 1;

    // exists but does not have axis 3
    device.stickAxes[1].device = 2;
    device.stickAxes[1].axis = 3;

    // exists but does not have lockstep
    device.stickAxes[2].device = 3;
    device.stickAxes[2].axisType = 'lockstep';
    device.stickAxes[2].axis = 1;

    device.keyCommands[0].device = 13;
    device.keyCommands[0].axis = 1;

    device.keyCommands[1].device = 2;
    device.keyCommands[1].axis = 3;

    // exists but does not have lockstep
    device.keyCommands[2].key = 1;
    device.keyCommands[2].device = 3;
    device.keyCommands[2].axis = 0;
    device.keyCommands[2].cmd = 'lockstep 1 move max';
  };

  selectXJoy();
  await waitUntilPass(() => wrapper.getByText('Apply'));

  expect(wrapper.getByTestId('device-select-1')).toHaveValue('13');
  expect(getByText(wrapper.getByTestId('device-select-1'), 'Unknown Device 13')).toHaveValue('13');

  expect(wrapper.getByTestId('device-select-2')).toHaveValue('2');
  expect(wrapper.getByTestId('axis-select-2')).toHaveValue('');
  expect(getByText(wrapper.getByTestId('axis-select-2'), 'Unknown Axis')).toHaveValue('');

  expect(wrapper.getByTestId('device-select-3')).toHaveValue('3');
  expect(wrapper.getByTestId('axis-select-3')).toHaveValue('');
  expect(getByText(wrapper.getByTestId('axis-select-3'), 'Unknown Axis')).toHaveValue('');

  expect(wrapper.getByTestId('key-device-select-0')).toHaveValue('13');
  expect(getByText(wrapper.getByTestId('key-device-select-0'), 'Unknown Device 13')).toHaveValue('13');
  expect(wrapper.getByTestId('key-axis-select-0')).toHaveValue('axis-1');
  expect(getByText(wrapper.getByTestId('key-axis-select-0'), 'Unknown Axis 1')).toHaveValue('axis-1');

  expect(wrapper.getByTestId('key-device-select-1')).toHaveValue('2');
  expect(wrapper.getByTestId('key-axis-select-1')).toHaveValue('axis-3');
  expect(getByText(wrapper.getByTestId('key-axis-select-1'), 'Unknown Axis 3')).toHaveValue('axis-3');

  expect(wrapper.getByTestId('key-device-select-2')).toHaveValue('3');
  expect(wrapper.getByTestId('key-axis-select-2')).toHaveValue('lockstep-1');
  expect(getByText(wrapper.getByTestId('key-axis-select-2'), 'Unknown Lockstep 1')).toHaveValue('lockstep-1');
});

test('advanced mode allows free form editing', async () => {
  selectXJoy();
  await waitUntilPass(() => wrapper.getByText('Apply'));

  fireEvent.click(wrapper.getByText(/Advanced Mode/));

  expect(wrapper.getByTestId('device-num-1')).toHaveValue(2);
  expect(wrapper.getByTestId('axis-num-1')).toHaveValue(1);
  expect(wrapper.getByTestId('maxspeed-1')).toHaveValue(1000);
  expect(wrapper.getByTestId('resolution-1')).toHaveValue(50);

  fireEvent.change(wrapper.getByTestId('device-num-1'), { target: { value: '13' } });
  fireEvent.change(wrapper.getByTestId('axis-type-1'), { target: { value: 'virtual' } });
  fireEvent.change(wrapper.getByTestId('axis-num-1'), { target: { value: '2' } });
  fireEvent.change(wrapper.getByTestId('maxspeed-1'), { target: { value: '12000' } });
  fireEvent.change(wrapper.getByTestId('resolution-1'), { target: { value: '30' } });

  expect(wrapper.getByTestId('key-device-num-0')).toHaveValue(0);
  expect(wrapper.getByTestId('key-axis-num-0')).toHaveValue(0);

  fireEvent.change(wrapper.getByTestId('key-device-num-0'), { target: { value: '17' } });
  fireEvent.change(wrapper.getByTestId('key-axis-num-0'), { target: { value: '4' } });

  fireEvent.click(wrapper.getByText('Add Event'));
  fireEvent.change(wrapper.getByTestId('key-device-num-2'), { target: { value: '6' } });
  fireEvent.change(wrapper.getByTestId('key-axis-num-2'), { target: { value: '3' } });
  fireEvent.change(wrapper.getByTestId('key-axis-type-2'), { target: { value: 'lockstep' } });
  fireEvent.change(wrapper.getByTestId('key-cmd-2'), { target: { value: 'move max' } });

  await writeToDevice();

  expect(device.genericCommand.mock.calls.map(call => call[0]).filter(cmd => cmd.match(/joystick 1|key 1/))).toEqual([
    'joystick 1 target 13 virtual 2',
    'joystick 1 speedprofile 1',
    'joystick 1 maxspeed 12000',
    'joystick 1 invert 0',
    'joystick 1 resolution 30',
    ...makeKeyClear(1, [KeyEventType.Press]),
    'key 1 1 add 17 4 home',
    'key 1 2 add 0 0 stop',
    'key 1 1 add 6 0 lockstep 3 move max',
  ]);
});

test('switching from advanced mode deals with unknown devices and axes', async () => {
  selectXJoy();
  await waitUntilPass(() => wrapper.getByText('Apply'));

  fireEvent.click(wrapper.getByText(/Advanced Mode/));

  fireEvent.change(wrapper.getByTestId('device-num-1'), { target: { value: '13' } });
  fireEvent.change(wrapper.getByTestId('axis-num-1'), { target: { value: '2' } });

  fireEvent.change(wrapper.getByTestId('device-num-2'), { target: { value: '4' } });
  fireEvent.change(wrapper.getByTestId('axis-num-2'), { target: { value: '2' } }); // in lockstep

  fireEvent.change(wrapper.getByTestId('key-device-num-0'), { target: { value: '17' } });
  fireEvent.change(wrapper.getByTestId('key-axis-num-0'), { target: { value: '4' } });

  fireEvent.change(wrapper.getByTestId('key-device-num-1'), { target: { value: '17' } });
  fireEvent.change(wrapper.getByTestId('key-axis-num-1'), { target: { value: '3' } });
  fireEvent.change(wrapper.getByTestId('key-axis-type-1'), { target: { value: 'lockstep' } });

  fireEvent.click(wrapper.getByText(/Simple Mode/));

  expect(wrapper.getByTestId('device-select-1')).toHaveValue('13');
  expect(getByText(wrapper.getByTestId('device-select-1'), 'Unknown Device 13')).toHaveValue('13');
  expect(wrapper.getByTestId('axis-select-1')).toHaveValue('');
  expect(getByText(wrapper.getByTestId('axis-select-1'), 'Unknown Axis')).toHaveValue('');

  expect(wrapper.getByTestId('axis-select-2')).toHaveValue('');
  expect(getByText(wrapper.getByTestId('axis-select-2'), 'Unknown Axis')).toHaveValue('');

  expect(wrapper.getByTestId('key-device-select-0')).toHaveValue('17');
  expect(getByText(wrapper.getByTestId('key-device-select-0'), 'Unknown Device 17')).toHaveValue('17');
  expect(wrapper.getByTestId('key-axis-select-0')).toHaveValue('axis-4');
  expect(getByText(wrapper.getByTestId('key-axis-select-0'), 'Unknown Axis 4')).toHaveValue('axis-4');

  expect(wrapper.getByTestId('key-axis-select-1')).toHaveValue('lockstep-3');
  expect(getByText(wrapper.getByTestId('key-axis-select-1'), 'Unknown Lockstep 3')).toHaveValue('lockstep-3');
});


test('it selects device address that does not exist for disabled axes', async () => {
  mockLocalConnections(TestJoystickApp.testStore, {
    COM1: connectionKey => [
      xJoyModifier(mockDataForDeviceWithoutPeripherals(connectionKey, 1, 0)),
      mockDataForDeviceWithoutPeripherals(connectionKey, 99, 1),
      mockDataForDeviceWithoutPeripherals(connectionKey, 98, 1),
    ],
  });

  selectXJoy();
  await waitUntilPass(() => wrapper.getByText('Apply'));

  const disabledOption = getByText(wrapper.getByTestId('device-select-1'), 'Disabled');
  fireEvent.change(wrapper.getByTestId('device-select-1'), { target: { value: disabledOption.getAttribute('value') } });

  await writeToDevice();

  expect(device.genericCommand.mock.calls[0][0]).toEqual('joystick 1 target 97  0');
});
