import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { injectable } from 'inversify';

import { wrapWithNewStore, wrapWithRouter } from '../test';
import { createContainer, destroyContainer } from '../container';
import { Cache } from '../cache';
import { IpcMock, mockIpc } from '../ipc/mock';

import { DeviceSetupButton } from './DeviceSetupButton';
import { actions } from './actions';
import type { PendingSetups } from './reducer';

const NOT_EXISTING_SERIAL_NO = 6;

const Component: React.FC = () => (<>
  <DeviceSetupButton serialNo={2}/>
  <DeviceSetupButton serialNo={3}/>
</>);

const TestComponent = wrapWithNewStore(wrapWithRouter(Component));
let wrapper: RenderResult;

let cacheMock: CacheMock;
@injectable()
class CacheMock {
  load = jest.fn().mockReturnValue(null);
  save = jest.fn();
}

let ipcMock: IpcMock;

beforeEach(() => {
  const container = createContainer();
  ipcMock = mockIpc(container);
  container.bind<unknown>(Cache).to(CacheMock);
  cacheMock = container.get<unknown>(Cache) as CacheMock;

  wrapper = render(<TestComponent/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  ipcMock = null!;
  cacheMock = null!;
});

test('displays the button when setup is needed', async () => {
  TestComponent.testStore.dispatch(actions.setupNeeded(2, '/setup-path'));
  const link = wrapper.getByText('Setup Device').parentElement!;
  expect(link).toHaveAttribute('href', '#/setup-path');
});

test('hides the button when setup is done', async () => {
  TestComponent.testStore.dispatch(actions.setupNeeded(2, '/setup-path'));
  wrapper.getByText('Setup Device');
  TestComponent.testStore.dispatch(actions.setupNotNeeded(2));
  expect(wrapper.queryByText('Setup Device')).toBeNull();
});

describe('caching', () => {
  test('loads the pending setups from the cache', async () => {
    const pending: PendingSetups = {
      3: { serialNo: 3, path: 'path' },
    };
    cacheMock.load.mockReturnValue(pending);
    TestComponent.testStore.dispatch(actions.loadPendingSetups());
    wrapper.getByText('Setup Device');
  });

  test('caches the pending setups', () => {
    TestComponent.testStore.dispatch(actions.setupNeeded(2, '/setup-path'));
    expect(cacheMock.save).toHaveBeenCalled();
  });

  test('only updates the cache when needed', () => {
    TestComponent.testStore.dispatch(actions.setupNeeded(2, '/setup-path'));
    expect(cacheMock.save).toHaveBeenCalledTimes(1);

    TestComponent.testStore.dispatch(actions.setupNotNeeded(NOT_EXISTING_SERIAL_NO));
    expect(cacheMock.save).toHaveBeenCalledTimes(1);

    TestComponent.testStore.dispatch(actions.setupNotNeeded(2));
    expect(cacheMock.save).toHaveBeenCalledTimes(2);
  });
});

test('broadcasts loading action when setups change', () => {
  TestComponent.testStore.dispatch(actions.setupNeeded(2, '/setup-path'));
  TestComponent.testStore.dispatch(actions.setupNotNeeded(NOT_EXISTING_SERIAL_NO));
  expect(ipcMock.broadcastAction).toHaveBeenCalledTimes(1);
});
