/* eslint-disable camelcase */
import type { Store } from 'redux';
import { ascii } from '@zaber/motion';
import _ from 'lodash';

import { makeConnectionKey, makeRouterKey, makeAxisKey, makeDeviceKey, EntityKey } from '../keys';
import { LOCAL_ROUTER_URL } from '../message_router';
import { ConnectionType } from '../app_components';
import type { RootState } from '../store';
import { getMovementDimensions } from '../units';

import { actions } from './actions';
import { DeviceInfoWithAxes, IdentifiedAxisInfo, IdentifiedDeviceInfo, ProductInfo, RouterType, StoredRouter } from './types';

export const MOCK_SERIAL_PORT = 'COM5';
export const MOCK_DEVICE_NUMBER = 1;

type ConnectionData = Record<string, (connectionKey: EntityKey) => DeviceInfoWithAxes[]>;

export function mockProduct(options: { mobile?: boolean; encoder?: boolean } = {}) {
  const product: ProductInfo = {
    id: 0,
    capabilities: [],
    commandTree: {
      command: '',
      nodes: [],
    },
    conversionTable: {
      rows: [],
    },
    settings: {
      rows: [],
    }
  };

  if (options.mobile) {
    product.commandTree.nodes!.push({
      command: 'move',
      nodes: [{
        command: 'abs',
        nodes: [{
          command: 'x',
          is_command: true,
          is_param: true,
        }]
      }]
    });

    product.conversionTable.rows = product.conversionTable.rows.concat([{
      contextual_dimension_id: 1,
      contextual_dimension_name: 'Length',
      dimension_name: 'Length',
      function_name: 'linear',
      scale: 0.000001,
    }, {
      contextual_dimension_id: 2,
      contextual_dimension_name: 'Velocity',
      dimension_name: 'Velocity',
      function_name: 'linear',
      scale: 0.001,
    }, {
      contextual_dimension_id: 3,
      contextual_dimension_name: 'Acceleration',
      dimension_name: 'Acceleration',
      function_name: 'linear',
      scale: 0.1,
    }, {
      contextual_dimension_id: 11,
      contextual_dimension_name: 'Time',
      dimension_name: 'Time',
      function_name: 'linear',
      scale: 1000
    }] satisfies typeof product.conversionTable.rows);

    product.settings.rows = product.settings.rows.concat([{
      name: 'pos',
      param_type: 'Int64',
      decimal_places: 0,
      contextual_dimension_id: 1,
      read_access_level: 1,
      write_access_level: 1,
      visibility: 'always',
      realtime: true,
    }, {
      name: 'encoder.pos',
      param_type: 'Int64',
      decimal_places: 0,
      contextual_dimension_id: 1,
      read_access_level: 1,
      write_access_level: 1,
      visibility: 'always',
      realtime: true,
    }, {
      name: 'limit.min',
      param_type: 'Int64',
      decimal_places: 0,
      contextual_dimension_id: 2,
      read_access_level: 1,
      write_access_level: 1,
      visibility: 'always',
      realtime: true,
    }, {
      name: 'limit.max',
      param_type: 'Int64',
      decimal_places: 0,
      contextual_dimension_id: 2,
      read_access_level: 1,
      write_access_level: 1,
      visibility: 'always',
      realtime: true,
    }, {
      name: 'maxspeed',
      param_type: 'Int64',
      decimal_places: 0,
      contextual_dimension_id: 2,
      read_access_level: 1,
      write_access_level: 1,
      visibility: 'always',
      realtime: true,
    }, {
      name: 'accel',
      contextual_dimension_id: 3,
      param_type: 'Uint32',
      decimal_places: 0,
      read_access_level: 1,
      write_access_level: 1,
      visibility: 'always',
      realtime: true
    }, {
      name: 'cloop',
      param_type: 'UInt8',
      decimal_places: 0,
      read_access_level: 1,
      write_access_level: 1,
      visibility: 'always',
      realtime: false,
    }]);
  }

  if (options.encoder) {
    product.conversionTable.rows = product.conversionTable.rows.concat([{
      contextual_dimension_id: 14,
      contextual_dimension_name: 'Voltage',
      dimension_name: 'Voltage',
      function_name: 'linear',
      scale: 1,
    }, {
      contextual_dimension_id: 15,
      contextual_dimension_name: 'Voltage (mV)',
      dimension_name: 'Voltage',
      function_name: 'linear',
      scale: 0.001,
    }, {
      contextual_dimension_id: 16,
      contextual_dimension_name: 'Voltage (µV)',
      dimension_name: 'Voltage',
      function_name: 'linear',
      scale: 0.000001,
    }]);

    product.settings.rows = product.settings.rows.concat([{
      name: 'encoder.2.cos',
      param_type: 'Int32',
      decimal_places: 0,
      contextual_dimension_id: 16,
      read_access_level: 1,
      write_access_level: 1,
      visibility: 'hardware_modification',
      realtime: true,
    }, {
      name: 'encoder.2.sin',
      param_type: 'Int32',
      decimal_places: 0,
      contextual_dimension_id: 16,
      read_access_level: 1,
      write_access_level: 1,
      visibility: 'hardware_modification',
      realtime: true,
    }]);
  }

  return product;
}

function mockLegacyPeripheral(deviceKey: EntityKey, axisNumber: number): IdentifiedAxisInfo {
  return ({
    axisNumber, key: makeAxisKey(deviceKey, axisNumber),
    serialNumber: 56789,
    label: null,
    identity: {
      axisType: ascii.AxisType.LINEAR,
      isPeripheral: true,
      peripheralId: 43211,
      peripheralName: 'LHM025A-T3',
      peripheralSerialNumber: 1234,
      isModified: false,
    },
    product: {
      ...mockProduct({ mobile: true }),
      id: 6642780,
      capabilities: ['motor-stepper', 'motion', 'multiprocessor'],
    },
    nonIdentifiedInfo: null,
    isPeripheralLike: true,
    canMove: true,
    movementDimensions: getMovementDimensions(ascii.AxisType.LINEAR),
  });
}

function mockSmartPeripheral(deviceKey: EntityKey, axisNumber: number): IdentifiedAxisInfo {
  return ({
    axisNumber, key: makeAxisKey(deviceKey, axisNumber),
    serialNumber: 56789,
    label: null,
    identity: {
      axisType: ascii.AxisType.LINEAR,
      isPeripheral: true,
      peripheralId: 66112,
      peripheralName: 'LRM150A-E03T4A',
      peripheralSerialNumber: 1234,
      isModified: false,
    },
    product: {
      ...mockProduct({ mobile: true }),
      id: 7689828,
      capabilities: ['motor-stepper', 'encoder-motor', 'motion', 'encoder',
        'encoder-1', 'encoder-1-motor', 'auto-detect', 'multiprocessor'],
      settings: { rows: [{ name: 'peripheral.hw.modified', param_type: '', decimal_places: 0, visibility: 'always' }] },
    },
    nonIdentifiedInfo: null,
    isPeripheralLike: true,
    canMove: true,
    movementDimensions: getMovementDimensions(ascii.AxisType.LINEAR),
  });
}

function mockUnusedPeripheral(deviceKey: EntityKey, axisNumber: number): IdentifiedAxisInfo {
  return ({
    axisNumber, key: makeAxisKey(deviceKey, axisNumber),
    serialNumber: 56789,
    label: null,
    identity: {
      axisType: ascii.AxisType.LINEAR,
      isPeripheral: true,
      peripheralId: 0,
      peripheralName: 'Unused',
      peripheralSerialNumber: 1234,
      isModified: false,
    },
    product: {
      ...mockProduct({ mobile: false }),
      id: 7686582,
      capabilities: ['multiprocessor'],
    },
    nonIdentifiedInfo: null,
    isPeripheralLike: true,
    canMove: true,
    movementDimensions: getMovementDimensions(ascii.AxisType.LINEAR),
  });
}

function mockRotaryPeripheral(deviceKey: EntityKey, axisNumber: number): IdentifiedAxisInfo {
  return ({
    axisNumber, key: makeAxisKey(deviceKey, axisNumber),
    serialNumber: 56789,
    label: null,
    identity: {
      axisType: ascii.AxisType.ROTARY,
      isPeripheral: true,
      peripheralId: 48308,
      peripheralName: 'RSW60A-E03T3',
      peripheralSerialNumber: 1234,
      isModified: false,
    },
    product: {
      ...mockProduct({ mobile: true }),
      id: 9764352,
      capabilities: [
        'motor-stepper', 'encoder-motor', 'cyclic', 'motion', 'encoder',
        'encoder-1', 'encoder-1-motor', 'binary-all', 'activate', 'hardware-modifiable-peripheral',
      ],
      conversionTable: {
        rows: [{
          contextual_dimension_id: 1,
          contextual_dimension_name: 'Angle',
          dimension_name: 'Angle',
          function_name: 'rotary',
          scale: 0.000001,
        }, {
          contextual_dimension_id: 2,
          contextual_dimension_name: 'AngularVelocity',
          dimension_name: 'AngularVelocity',
          function_name: 'rotary',
          scale: 0.001,
        }, {
          contextual_dimension_id: 3,
          contextual_dimension_name: 'AngularAcceleration',
          dimension_name: 'AngularAcceleration',
          function_name: 'rotary',
          scale: 0.1,
        }]
      }
    },
    nonIdentifiedInfo: null,
    isPeripheralLike: true,
    canMove: true,
    movementDimensions: getMovementDimensions(ascii.AxisType.ROTARY),
  });
}

function mockThirdPartyPeripherals(deviceKey: EntityKey, axisNumber: number): IdentifiedAxisInfo {
  return ({
    axisNumber, key: makeAxisKey(deviceKey, axisNumber),
    serialNumber: 56789,
    label: null,
    identity: {
      axisType: ascii.AxisType.LINEAR,
      isPeripheral: true,
      peripheralId: 88800,
      peripheralName: 'CLS-T',
      peripheralSerialNumber: 1234,
      isModified: true,
    },
    product: {
      ...mockProduct({ mobile: false }),
      id: 7686582,
      capabilities: ['multiprocessor', 'motion'],
      settings: { rows: [{ name: 'peripheral.hw.modified', param_type: '', decimal_places: 0, visibility: 'always' }] },
    },
    nonIdentifiedInfo: null,
    isPeripheralLike: true,
    canMove: true,
    movementDimensions: getMovementDimensions(ascii.AxisType.LINEAR),
  });
}

type Peripherals = 'legacy' | 'smart' | 'unused' | 'rotary' | 'thirdParty';
const mockPeripherals = {
  legacy: mockLegacyPeripheral,
  smart: mockSmartPeripheral,
  unused: mockUnusedPeripheral,
  rotary: mockRotaryPeripheral,
  thirdParty: mockThirdPartyPeripherals,
};

export function mockDataForDeviceWithPeripherals(
  connectionKey: EntityKey,
  deviceNumber: number,
  peripheralCount: number = 1,
  type: Peripherals | Peripherals[],
  axisModifier: (info: IdentifiedAxisInfo) => IdentifiedAxisInfo = _.identity,
): IdentifiedDeviceInfo {
  const deviceKey = makeDeviceKey(connectionKey, deviceNumber);
  return {
    address: deviceNumber,
    key: deviceKey,
    nonIdentifiedInfo: null,
    axisCount: peripheralCount,
    isMultiAxis: peripheralCount > 1,
    isController: true,
    isRemote: false,
    product: {
      ...mockProduct(),
      id: 6641902,
      capabilities: [
        'usb', 'discrete-drive', 'io', 'knob', 'multiaxis', 'controller', 'io-do',
        'io-di', 'io-ai', 'led', 'io-ao', 'motion-driver', 'multiprocessor', 'auto-detector'
      ],
    },
    platformId: 987343,
    isKeyed: false,
    label: null,
    identity: {
      axisCount: peripheralCount,
      deviceId: 30341,
      name: `X-MCC${peripheralCount}`,
      serialNumber: 1235,
      firmwareVersion: { major: 7, minor: 13, build: 8074 },
      isModified: false,
      isIntegrated: false,
    },
    axes: _.range(1, peripheralCount + 1).map(axisNumber =>
      axisModifier(mockPeripherals[Array.isArray(type) ? type[axisNumber - 1] : type](deviceKey, axisNumber))),
    locksteps: [],
  };
}

export function mockDataForDeviceWithoutPeripherals(
  connectionKey: EntityKey, deviceNumber: number,
  axisCount: number = 1,
): IdentifiedDeviceInfo {
  const product = {
    ...mockProduct({ mobile: true }),
    id: 8630764,
    capabilities: ['motor-stepper', 'knob', 'motion', 'binary-all', 'led', 'motion-driver', 'field-upgradeable'],
  };
  if (axisCount > 1) {
    product.capabilities.push('multiaxis');
  }
  const deviceKey = makeDeviceKey(connectionKey, deviceNumber);

  let axisList: IdentifiedAxisInfo[] = [];
  if (axisCount === 1) {
    axisList = [{
      key: makeAxisKey(deviceKey, 1),
      axisNumber: 1,
      serialNumber: null,
      product: {
        ...product,
        id: 0,
      },
      identity: {
        axisType: ascii.AxisType.LINEAR,
        isPeripheral: false,
        peripheralId: 0,
        peripheralName: '',
        peripheralSerialNumber: 1234,
        isModified: false,
      },
      nonIdentifiedInfo: null,
      label: null,
      isPeripheralLike: false,
      canMove: true,
      movementDimensions: getMovementDimensions(ascii.AxisType.LINEAR),
    }];
  } else {
    for (let i = 1; i <= axisCount; i++) {
      axisList.push({
        key: makeAxisKey(deviceKey, i),
        axisNumber: i,
        serialNumber: null,
        label: null,
        product: {
          ...product,
          id: 50960 + i,
        },
        identity: {
          axisType: ascii.AxisType.LINEAR,
          isPeripheral: false,
          peripheralId: 1230 + i,
          peripheralName: '',
          peripheralSerialNumber: 1234,
          isModified: false,
        },
        nonIdentifiedInfo: null,
        isPeripheralLike: false,
        canMove: true,
        movementDimensions: getMovementDimensions(ascii.AxisType.LINEAR),
      });
    }
  }

  return {
    address: deviceNumber,
    key: deviceKey,
    nonIdentifiedInfo: null,
    axisCount,
    isMultiAxis: axisCount > 1,
    isController: false,
    isRemote: false,
    product,
    platformId: 987343,
    isKeyed: false,
    label: null,
    identity: {
      axisCount,
      deviceId: 50081,
      name: 'X-LHM',
      serialNumber: 1234,
      firmwareVersion: { major: 7, minor: 11, build: 1234 },
      isModified: false,
      isIntegrated: true,
    },
    axes: axisList,
    locksteps: [],
  };
}

export function mockConnections(store: Store<RootState>, data: { router: StoredRouter; connectionsData: ConnectionData }[]): void {
  store.dispatch(
    actions.routersLoaded(data.map(data => data.router))
  );

  for (const { router, connectionsData } of data) {
    const routerKey = makeRouterKey(router.url);
    const connections = _.keys(connectionsData).map(serialPort => ({
      id: serialPort, type: ConnectionType.SERIAL_PORT,
      config: { serialPort, name: '', baudRate: 115200 },
    }));

    store.dispatch(
      actions.connectionsLoaded(routerKey, { connections, name: 'router_name' })
    );

    _.each(connectionsData, (dataFunc, serialPort) => {
      const connectionKey = makeConnectionKey(routerKey, serialPort);
      store.dispatch(
        actions.devicesLoaded(connectionKey, dataFunc(connectionKey))
      );
    });
  }
}

export function mockLocalConnections(store: Store<RootState>, connectionsData: ConnectionData): void {
  mockConnections(store, [{ router: { type: RouterType.Local, url: LOCAL_ROUTER_URL }, connectionsData }]);
}

export function mockSingleDevice(
  store: Store<RootState>,
  modifier: (info: IdentifiedDeviceInfo) => IdentifiedDeviceInfo = _.identity,
  deviceNumber = MOCK_DEVICE_NUMBER,
  axisCount = 1,
): void {
  mockLocalConnections(store, {
    [MOCK_SERIAL_PORT]: connectionKey => [
      modifier(mockDataForDeviceWithoutPeripherals(connectionKey, deviceNumber, axisCount)),
    ],
  });
}

export function mockNonIdentifiedDevice(
  store: Store<RootState>,
  modifier: (info: DeviceInfoWithAxes) => DeviceInfoWithAxes = _.identity,
  deviceNumber = MOCK_DEVICE_NUMBER,
): void {
  mockLocalConnections(store, {
    [MOCK_SERIAL_PORT]: connectionKey => [
      modifier({
        key: makeDeviceKey(connectionKey, deviceNumber),
        address: deviceNumber,
        axisCount: 1,
        identity: null,
        isController: false,
        isRemote: false,
        isKeyed: false,
        isMultiAxis: false,
        nonIdentifiedInfo: {
          deviceId: 1234,
          firmwareVersion: {
            major: 7,
            minor: 99,
            build: 0,
          },
        },
        platformId: null,
        product: null,
        label: null,
        axes: [{
          key: makeAxisKey(makeDeviceKey(connectionKey, deviceNumber), 1),
          nonIdentifiedInfo: {
            peripheralId: null
          },
          axisNumber: 1,
          identity: null,
          isPeripheralLike: false,
          product: null,
          serialNumber: null,
          canMove: false,
          movementDimensions: null,
          label: null,
        }],
        locksteps: null,
      }),
    ],
  });
}

interface MockSingleDeviceWithPeripheralsOptions {
  deviceNumber?: number;
  modifier?: (info: IdentifiedDeviceInfo) => IdentifiedDeviceInfo;
  peripheralCount?: number;
  type?: Peripherals | Peripherals[];
  axisModifier?: (info: IdentifiedAxisInfo) => IdentifiedAxisInfo;
}
export function mockSingleDeviceWithPeripherals(
  store: Store<RootState>,
  options: MockSingleDeviceWithPeripheralsOptions = {},
): void {
  const { modifier = _.identity, axisModifier, peripheralCount = 4, type = 'legacy' } = options;
  mockLocalConnections(store, {
    [MOCK_SERIAL_PORT]: connectionKey => [
      modifier(
        mockDataForDeviceWithPeripherals(connectionKey, options.deviceNumber ?? MOCK_DEVICE_NUMBER, peripheralCount, type, axisModifier)
      )!,
    ],
  });
}

export function mockRouterConnectionError(store: Store<RootState>): void {
  store.dispatch(
    actions.routersLoaded([{ type: RouterType.Local, url: LOCAL_ROUTER_URL }])
  );
  const routerKey = makeRouterKey(LOCAL_ROUTER_URL);
  store.dispatch(
    actions.connectionsLoadedErr(routerKey, 'Connection to router is broken')
  );
}

export function mockConnectionError(store: Store<RootState>): void {
  store.dispatch(
    actions.routersLoaded([{ type: RouterType.Local, url: LOCAL_ROUTER_URL }])
  );
  const routerKey = makeRouterKey(LOCAL_ROUTER_URL);
  const connections = [{
    id: MOCK_SERIAL_PORT, type: ConnectionType.SERIAL_PORT,
    config: { serialPort: MOCK_SERIAL_PORT, name: '', baudRate: 115200 },
  }];
  store.dispatch(
    actions.connectionsLoaded(routerKey, { connections, name: 'router_name' })
  );
  const connectionKey = makeConnectionKey(routerKey, MOCK_SERIAL_PORT);
  store.dispatch(
    actions.devicesLoadedErr(connectionKey, { message: 'Cannot open serial port', reason: 'connection' })
  );
}
