import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { IoState } from './types';

export enum ActionTypes {
  MOUNT = 'DEVICE_IO_MOUNT',
  UNMOUNT = 'DEVICE_IO_UNMOUNT',
  STATE_READ = 'DEVICE_IO_STATE_READ',
  STATE_READ_ERROR = 'DEVICE_IO_STATE_READ_ERROR',
  REFRESH_IO = 'DEVICE_IO_REFRESH_IO',
  SET_OUTPUT = 'DEVICE_IO_SET_OUTPUT',
  SET_OUTPUT_DONE = 'DEVICE_IO_SET_OUTPUT_DONE',
  SET_SELECTED_KEY = 'DEVICE_IO_SET_SELECTED_KEY',
}

export interface ActionsToPayloads {
  [ActionTypes.MOUNT]: { deviceKey: EntityKey };
  [ActionTypes.UNMOUNT]: { deviceKey: EntityKey };
  [ActionTypes.STATE_READ]: { deviceKey: EntityKey; stateUpdate: Partial<IoState> };
  [ActionTypes.STATE_READ_ERROR]: { deviceKey: EntityKey; error: string };
  [ActionTypes.REFRESH_IO]: { deviceKey: EntityKey };
  [ActionTypes.SET_OUTPUT]: { deviceKey: EntityKey; output: number; value: boolean | number };
  [ActionTypes.SET_OUTPUT_DONE]: { deviceKey: EntityKey; error?: string };
  [ActionTypes.SET_SELECTED_KEY]: { key: EntityKey | null };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  mount: (deviceKey: EntityKey) => buildAction(ActionTypes.MOUNT, { deviceKey }),
  unmount: (deviceKey: EntityKey) => buildAction(ActionTypes.UNMOUNT, { deviceKey }),
  stateRead: (deviceKey: EntityKey, stateUpdate: Partial<IoState>) => buildAction(ActionTypes.STATE_READ, { deviceKey, stateUpdate }),
  stateReadError: (deviceKey: EntityKey, error: string) => buildAction(ActionTypes.STATE_READ_ERROR, { deviceKey, error }),
  refreshIo: (deviceKey: EntityKey) => buildAction(ActionTypes.REFRESH_IO, { deviceKey }),
  setOutput: (deviceKey: EntityKey, output: number, value: boolean | number) =>
    buildAction(ActionTypes.SET_OUTPUT, { deviceKey, output, value }),
  setOutputDone: (deviceKey: EntityKey, error?: string) =>
    buildAction(ActionTypes.SET_OUTPUT_DONE, { deviceKey, error }),
  setSelectedKey: (key: EntityKey | null) => buildAction(ActionTypes.SET_SELECTED_KEY, { key }),
};
