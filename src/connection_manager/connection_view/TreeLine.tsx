import React from 'react';

interface Props {
    bendRadius?: number;
    connectRight?: boolean;
    connectDown?: boolean;
}

const TREE_LINE_BEND_RADIUS = 26;

export const TreeLine: React.FC<Props> = ({ bendRadius = TREE_LINE_BEND_RADIUS, connectDown, connectRight }) => {
  let path = '';
  if (connectRight && connectDown) {
    // A line starting from the top (horizontally centered), going to the middle of the view
    // box and then turning to the right. The turn part has a radius defined by `bendRadius`
    // prop. The cursor then moves to the point where the bend had started and draws a straight
    // line down to the bottom of the view box.
    path = `
      M 52 0
      v ${52 - bendRadius}
      a ${bendRadius} ${bendRadius} 0 0 0 ${bendRadius} ${bendRadius}
      h ${52 - bendRadius}
      M 52 ${52 - bendRadius}
      v ${52 + bendRadius}
    `;
  } else if (connectRight) {
    // A line starting from the top (horizontally centered), going to the middle of the view
    // box and then turning to the right. The turn part has a radius defined by `bendRadius`
    // prop
    path = `
      M 52 0
      v ${52 - bendRadius}
      a ${bendRadius} ${bendRadius} 0 0 0 ${bendRadius} ${bendRadius}
      h ${52 - bendRadius}
    `;
  } else if (connectDown) {
    // A horizontally centered straight line from top to bottom
    path = 'M 52 0 v 104';
  }

  return (
    <svg className="tree-line" viewBox="0 0 104 104" preserveAspectRatio="xMidYMid slice">
      <path d={path}/>
    </svg>
  );
};
