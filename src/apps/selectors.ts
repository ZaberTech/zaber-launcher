import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectApps as selectState } from '../store';

import { AppUrls, APP_DICTIONARY, FILTERED_APP_LIST } from './list';

function sortApps(a: { name: string }, b: { name: string }) {
  return a.name.localeCompare(b.name);
}

const selectPinned = createSelector(selectState, state => state.pinnedApps);

const selectUnlocked = createSelector(selectState, state => _.keys(state.unlockedApps).map(key => key as AppUrls));

export const selectApps = createSelector(selectPinned, selectUnlocked, (pinned, unlocked) =>
  _(FILTERED_APP_LIST).concat(unlocked.map(url => APP_DICTIONARY[url]))
    .uniqBy(app => app.url)
    .map(app => ({
      ...app,
      pinned: pinned[app.url] ?? false,
    }))
    .keyBy(app => app.url)
    .value() as Record<AppUrls, typeof FILTERED_APP_LIST[number] & { pinned: boolean }>);


export const selectAppsToRender = createSelector(selectApps, apps =>
  _.values(apps).sort(sortApps));

export const selectRecentApps = createSelector(selectState, selectApps, (state, apps) =>
  state.recentApps.map(url => apps[url]).filter(_.identity).sort(sortApps)
);

export const selectPinnedApps = createSelector(selectApps, apps =>
  _.values(apps).filter(apps => apps.pinned).sort(sortApps)
);

export const selectAppsToStore = createSelector(selectState, state => ({
  recent: state.recentApps,
  pinned: _.keys(state.pinnedApps) as AppUrls[],
}));
