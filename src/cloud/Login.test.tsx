import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

class User {
  attributes = [{
    Name: 'email',
    Value: 'email@test.io',
  }];
}

class AuthMock {
  static signIn = jest.fn(() => Promise.resolve(new User()));
  static userAttributes = jest.fn((user: User) => user.attributes);
}

jest.mock('aws-amplify', () => ({
  Auth: AuthMock,
}));

@injectable()
class AwsCredentialsServiceMock {
  setUser = jest.fn().mockResolvedValue(null);
}

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore } from '../test';
import { AwsCredentialsService } from '../app_components';

import { selectAuthState } from './selectors';
import { Login } from './Login';
import { AuthState } from './types';

let container: Container;
let credentialsMock: AwsCredentialsServiceMock;

const TestLogin = wrapWithNewStore(Login);

let wrapper: RenderResult;

beforeEach(() => {
  AuthMock.signIn.mockClear();
  AuthMock.userAttributes.mockClear();

  container = createContainer();
  container.bind<unknown>(AwsCredentialsService).to(AwsCredentialsServiceMock);
  credentialsMock = container.get<unknown>(AwsCredentialsService) as AwsCredentialsServiceMock;

  wrapper = render(<TestLogin/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

test('allows to Sign In', async () => {
  fireEvent.change(wrapper.getByLabelText(/Email Address/), { target: { value: 'email' } });
  fireEvent.change(wrapper.getByLabelText(/Password/), { target: { value: 'pwd' } });

  fireEvent.click(wrapper.getByText('Sign In'));
  expect(wrapper.getByText('Sign In')).toBeDisabled();

  await waitUntilPass(() => expect(selectAuthState(TestLogin.testStore.getState())).toBe(AuthState.Authenticated));
  expect(credentialsMock.setUser).toHaveBeenCalled();
  expect(AuthMock.signIn).toHaveBeenCalledWith(expect.objectContaining({ username: 'email', password: 'pwd' }));
});

test('handles error', async () => {
  // cognito does not throw Error instances but throws objects that looks like one
  AuthMock.signIn.mockRejectedValueOnce({ message: 'User does not exist' });
  fireEvent.change(wrapper.getByLabelText(/Email Address/), { target: { value: 'email' } });
  fireEvent.change(wrapper.getByLabelText(/Password/), { target: { value: 'pwd' } });

  fireEvent.click(wrapper.getByText('Sign In'));

  await waitUntilPass(() => wrapper.getByText(/User does not exist/));
});

test('can login by pressing Enter', async () => {
  fireEvent.change(wrapper.getByLabelText(/Email Address/), { target: { value: 'email' } });
  fireEvent.keyDown(wrapper.getByLabelText(/Password/), { key: 'Enter' });
  await waitUntilPass(() => wrapper.getByText(/Password not provided/));
});
