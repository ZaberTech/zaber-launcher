import React, { useLayoutEffect } from 'react';
import { render, RenderResult, fireEvent } from '@testing-library/react';

import { StickyScrollDiv } from './StickyScrollDiv';

let resizeObserver: ResizeObserverMock;
class ResizeObserverMock {
  constructor(readonly callback: ResizeObserverCallback) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    resizeObserver = this;
  }
  observe = jest.fn();
  disconnect = jest.fn();
}
(global.ResizeObserver as unknown) = ResizeObserverMock;


interface SetAnything {
  scrollHeight: number;
  clientHeight: number;
}
for (const prop of ['scrollHeight', 'clientHeight']) {
  Object.defineProperty(HTMLElement.prototype, prop, {
    configurable: true,
    get() {
      return this[`_${prop}`] ?? 0;
    },
    set(val) {
      this[`_${prop}`] = val;
    }
  });
}

const SCROLL_HEIGHT = 100;
const CLIENT_HEIGHT = 20;

let wrapper: RenderResult;
const scrollIntoViewMock = jest.fn();

function renderChildren() {
  return <>Text</>;
}

beforeAll(() => {
  window.HTMLElement.prototype.scrollIntoView = scrollIntoViewMock;
});
beforeEach(() => {
  wrapper = render(<StickyScrollDiv>{renderChildren}</StickyScrollDiv>);

  const div = wrapper.getByText('Text') as SetAnything;
  div.scrollHeight = SCROLL_HEIGHT;
  div.clientHeight = CLIENT_HEIGHT;
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  resizeObserver = null!;

  scrollIntoViewMock.mockClear();
});

function scrollUp() {
  const div = wrapper.getByText('Text');

  fireEvent.wheel(div, { deltaY: -100 });
}

test('sets scroll on resize when last scroll was at the bottom', () => {
  scrollIntoViewMock.mockClear();
  const div = wrapper.getByText('Text');

  scrollUp();

  resizeObserver.callback([], null!);
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(0);

  div.scrollTop = SCROLL_HEIGHT - CLIENT_HEIGHT;
  fireEvent.scroll(div);
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(1);

  resizeObserver.callback([], null!);
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(2);
});

test('scroll to bottom on mount', () => {
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(1);
});

test('scroll to bottom on button click', () => {
  scrollIntoViewMock.mockClear();
  scrollUp();

  fireEvent.click(wrapper.getByText(/Bottom/));
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(1);
});

test('scroll when scrollKey changes unless user scrolls up', () => {
  scrollIntoViewMock.mockClear();

  wrapper.rerender(<StickyScrollDiv scrollKey="newKey">{renderChildren}</StickyScrollDiv>);
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(1);

  wrapper.rerender(<StickyScrollDiv scrollKey="otherKey">{renderChildren}</StickyScrollDiv>);
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(2);

  scrollUp();

  wrapper.rerender(<StickyScrollDiv scrollKey="otherKey2">{renderChildren}</StickyScrollDiv>);
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(2);
});

test('provides context with scrollToBottom', () => {
  scrollIntoViewMock.mockClear();

  wrapper.rerender(<StickyScrollDiv>{({ scrollToBottom }) => {
    function Child() {
      useLayoutEffect(() => {
        scrollToBottom();
      }, []);

      return <>Text</>;
    }

    return <Child/>;
  }}</StickyScrollDiv>);

  expect(scrollIntoViewMock).toHaveBeenCalledTimes(1);
});

test('context scrollToBottom scrolls only when sticky or forced', () => {
  scrollIntoViewMock.mockClear();
  scrollUp();

  wrapper.rerender(<StickyScrollDiv>{({ scrollToBottom }) =>
    <>
      <button onClick={() => scrollToBottom()}>Regular</button>
      <button onClick={() => scrollToBottom('smooth', true)}>Force</button>
    </>
  }</StickyScrollDiv>);

  fireEvent.click(wrapper.getByText('Regular'));
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(0);

  fireEvent.click(wrapper.getByText('Force'));
  expect(scrollIntoViewMock).toHaveBeenCalledTimes(1);
});
