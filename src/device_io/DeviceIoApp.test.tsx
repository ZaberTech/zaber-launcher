import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import { CommandFailedException, CommandFailedExceptionData } from '@zaber/motion';
import { DigitalOutputAction } from '@zaber/motion/ascii';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { createContainer, destroyContainer } from '../container';
import { wrapWithNewStore, waitUntilPass, wrapWithRouter, defer } from '../test';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import {
  mockSingleDevice, MOCK_DEVICE_NUMBER, MOCK_SERIAL_PORT,
} from '../connection_manager/mocks';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';

import { DeviceIoApp } from './DeviceIoApp';
import { actions } from './actions';

const makeBADCOMMAND = () => new CommandFailedException('', {
  responseData: 'BADCOMMAND',
} as CommandFailedExceptionData);

const DEVICE_TITLE = '01 X-LHM';

let container: Container;

class AxisMock extends AxisMockBase {
}

function badCommandIfEmpty<T>(cb: () => Promise<T[]>) {
  return () => cb().then(val => {
    if (val.length === 0) {
      throw makeBADCOMMAND();
    }
    return val;
  });
}

let device: DeviceMock;
let deviceCallback: (device: DeviceMock) => void;

class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    device = this;
    if (deviceCallback) { deviceCallback(this) }
  }

  analogInputs: number[] = [];
  analogOutputs: number[] = [];
  digitalInputs: boolean[] = [];
  digitalOutputs: boolean[] = [];

  io = {
    getAllDigitalInputs: jest.fn(badCommandIfEmpty(async () => this.digitalInputs.slice())),
    getAllDigitalOutputs: jest.fn(badCommandIfEmpty(async () => this.digitalOutputs.slice())),
    getAllAnalogInputs: jest.fn(badCommandIfEmpty(async () => this.analogInputs.slice())),
    getAllAnalogOutputs: jest.fn(badCommandIfEmpty(async () => this.analogOutputs.slice())),
    setDigitalOutput: jest.fn().mockResolvedValue(undefined),
    setAnalogOutput: jest.fn().mockResolvedValue(undefined),
  };
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const connectionKey = makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), MOCK_SERIAL_PORT);
const deviceKey = makeDeviceKey(connectionKey, MOCK_DEVICE_NUMBER);

const TestDeviceIoApp = wrapWithNewStore(wrapWithRouter(DeviceIoApp));

let wrapper: RenderResult;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

  wrapper = render(<TestDeviceIoApp/>);
  mockSingleDevice(TestDeviceIoApp.testStore);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;

  device = null!;
  deviceCallback = null!;
});

describe('device with IO', () => {
  beforeEach(async () => {
    deviceCallback = device => {
      device.analogInputs = [1.222, 3.444];
      device.analogOutputs = [5.666, 6.777];
      device.digitalInputs = [true, false];
      device.digitalOutputs = [false, true];
    };

    connectionViewMockInstance.props.onSelect(connectionKey);
    await waitUntilPass(() => {
      wrapper.getByText(DEVICE_TITLE);
      expect(wrapper.queryByText('Loading...')).toBeNull();
    });
  });

  test('refreshes IO repeatedly', async () => {
    wrapper.getByText('1.222');
    expect(wrapper.queryAllByText('On')).toHaveLength(2);
    expect(wrapper.queryAllByText('Off')).toHaveLength(2);

    device.analogInputs[0] = 9.999;
    device.digitalInputs[0] = false;
    fireEvent.click(wrapper.getByTitle('Refresh IO'));

    await waitUntilPass(() => {
      wrapper.getByText('9.999');
      expect(wrapper.queryAllByText('On')).toHaveLength(1);
      expect(wrapper.queryAllByText('Off')).toHaveLength(3);
    });
  });

  test('sets digital output', async () => {
    fireEvent.click(wrapper.queryAllByRole('checkbox')[0]);
    await waitUntilPass(() => {
      expect(device.io.setDigitalOutput).toHaveBeenLastCalledWith(1, DigitalOutputAction.ON);
    });
  });

  test('sets analog output', async () => {
    fireEvent.click(wrapper.queryAllByTitle('Edit Value')[1]);
    fireEvent.change(wrapper.getByRole('spinbutton'), { target: { value: '1.234' } });
    fireEvent.click(wrapper.getByTitle('Confirm'));
    await waitUntilPass(() => {
      expect(device.io.setAnalogOutput).toHaveBeenLastCalledWith(2, 1.234);
    });
  });

  test('displays error from setting output', async () => {
    device.io.setDigitalOutput.mockRejectedValueOnce(new Error('Something went wrong'));
    fireEvent.click(wrapper.queryAllByRole('checkbox')[0]);
    await waitUntilPass(() => {
      wrapper.getByText(/Something went wrong/);
    });
  });
});


describe('device with some IO', () => {
  beforeEach(async () => {
    deviceCallback = device => {
      device.digitalInputs = [true, false];
      device.digitalOutputs = [false, true];
    };

    connectionViewMockInstance.props.onSelect(connectionKey);
    await waitUntilPass(() => {
      wrapper.getByText(DEVICE_TITLE);
      expect(wrapper.queryByText('Loading...')).toBeNull();
    });
  });

  test('does not refresh non-existent IO', async () => {
    TestDeviceIoApp.testStore.dispatch(actions.refreshIo(deviceKey));
    await waitUntilPass(() => {
      expect(device.io.getAllDigitalInputs).toHaveBeenCalledTimes(2);
      expect(device.io.getAllDigitalOutputs).toHaveBeenCalledTimes(2);
      expect(device.io.getAllAnalogInputs).toHaveBeenCalledTimes(1);
      expect(device.io.getAllAnalogOutputs).toHaveBeenCalledTimes(1);
    });
  });
});

describe('misc tests', () => {
  test('hides device without IO ', async () => {
    connectionViewMockInstance.props.onSelect(connectionKey);

    await waitUntilPass(() => {
      expect(device.io.getAllDigitalInputs).toHaveBeenCalled();
      expect(device.io.getAllDigitalOutputs).toHaveBeenCalled();
      expect(device.io.getAllAnalogInputs).toHaveBeenCalled();
      expect(device.io.getAllAnalogOutputs).toHaveBeenCalled();
    });
    expect(wrapper.queryByText(DEVICE_TITLE)).toBeNull();
  });

  test('displays loading message before getting information ', async () => {
    const deferred = defer<boolean[]>();
    deviceCallback = device => {
      device.io.getAllDigitalInputs.mockImplementation(() => deferred.promise);
    };

    connectionViewMockInstance.props.onSelect(connectionKey);
    await waitUntilPass(() =>
      wrapper.getByText('Loading...'));

    deferred.resolve([false]);

    await waitUntilPass(() =>
      expect(wrapper.queryByText('Loading...')).toBeNull());
  });

  test('displays reading IO error', async () => {
    deviceCallback = device => {
      device.io.getAllDigitalInputs.mockRejectedValue(new Error('Some failure'));
    };
    connectionViewMockInstance.props.onSelect(connectionKey);
    await waitUntilPass(() => wrapper.getByText(/Some failure/));
  });

  test('reloads entire IO state on reloading of devices', async () => {
    deviceCallback = device => {
      device.analogInputs = [1.333];
    };
    connectionViewMockInstance.props.onSelect(connectionKey);
    await waitUntilPass(() => wrapper.getByText('1.333'));

    device.analogInputs = [];
    device.analogOutputs = [2.444];
    mockSingleDevice(TestDeviceIoApp.testStore);

    wrapper.getByText('Loading...');
    await waitUntilPass(() => wrapper.getByText('2.444'));
  });
});
