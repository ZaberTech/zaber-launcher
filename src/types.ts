export const DEFAULT_POLL_INTERVAL = 1000;

export const MS_PER_SEC = 1000;

export type Predicate<T> = (item: T) => boolean;
