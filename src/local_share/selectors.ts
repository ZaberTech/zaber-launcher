import { createSelector } from 'reselect';

import { selectLocalShare } from '../store';
import type { NewRouters } from '../connection_manager/components/NewRouters';
import { RouterType } from '../connection_manager/types';

export const selectShareConfig = createSelector(selectLocalShare, ({ localShare, localShareMode, configReadErr, readingConfig }) => ({
  config: localShare, error: configReadErr, reading: readingConfig, mode: localShareMode,
}));
export const selectShareStatus = createSelector(selectLocalShare, ({ status, statusReadErr, readingStatus }) => ({
  status, error: statusReadErr, reading: readingStatus,
}));
export const selectUpdateConfig = createSelector(selectLocalShare, ({ configUpdating, configUpdateErr, sharingWarningVisible }) => ({
  updating: configUpdating, error: configUpdateErr, warningVisible: sharingWarningVisible,
}));
export const selectDiscovery = createSelector(selectLocalShare, ({ discovery }) => (discovery));

export const selectDiscoveryCandidates = createSelector(selectLocalShare, state =>
  state.discovery.candidates
    .filter(candidate => !candidate.serviceName.startsWith(state.localShare?.instanceName ?? 'noname'))
    .map<NewRouters.NewRouter>(candidate => ({
      type: RouterType.LocalShare,
      name: candidate.name,
      url: candidate.url
    }))
    .sort((a, b) => a.name.localeCompare(b.name))
);
