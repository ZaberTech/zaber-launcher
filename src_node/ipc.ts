import { BrowserWindow, ipcMain, IpcMainEvent, WebContents } from 'electron';
import { injectable } from 'inversify';
import { Subject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { handleUnexpectedError } from '../src/errors/errors';
import { REDUX_BROADCAST_CHANNEL, REDUX_MAIN_PROCESS_CHANNEL } from '../src/ipc/types';
import type { AnyAction } from '../src/utils';

export interface ReceivedAction<TAction extends AnyAction = AnyAction> {
  action: TAction;
  sender: WebContents;
}

@injectable()
export class Ipc {
  private messages = new Subject<ReceivedAction>();

  constructor() {
    ipcMain.on(REDUX_BROADCAST_CHANNEL, this.onBroadcastAction.bind(this));
    ipcMain.on(REDUX_MAIN_PROCESS_CHANNEL, this.onActionForMe.bind(this));
  }

  getActions<TAction extends AnyAction>(actionType: string): Observable<ReceivedAction<TAction>> {
    return this.messages.pipe(filter(received => received.action.type === actionType)) as Observable<ReceivedAction<TAction>>;
  }

  sendAction(target: WebContents, action: AnyAction): void {
    if (target.isDestroyed()) { return }
    target.send(REDUX_BROADCAST_CHANNEL, action);
  }

  broadcastAction(action: AnyAction): void {
    this.sendActionToWindows(BrowserWindow.getAllWindows(), action);
  }

  private onBroadcastAction(event: IpcMainEvent, action: AnyAction): void  {
    const otherWindows = BrowserWindow.getAllWindows().filter(window => window.webContents !== event.sender);
    this.sendActionToWindows(otherWindows, action);
  }

  private sendActionToWindows(windows: BrowserWindow[], action: AnyAction): void {
    for (const window of windows) {
      this.sendAction(window.webContents, action);
    }
  }

  private onActionForMe(event: IpcMainEvent, action: AnyAction): void  {
    try {
      this.messages.next({ action, sender: event.sender });
    } catch (err) {
      handleUnexpectedError(err);
    }
  }
}
