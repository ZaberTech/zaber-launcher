/* eslint-disable camelcase */
import React from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { wrapWithNewStore, StorageMock } from '../test';
import { Storage } from '../app_components';

import { OneTimeMessage } from './OneTimeMessage';

const TestOneTimeMessage = wrapWithNewStore(OneTimeMessage);
const TestMessage = () => <TestOneTimeMessage messageId="testId" message={
  dismiss => <button onClick={dismiss}>Close</button>
}/>;

let storageMock: StorageMock;

let wrapper: RenderResult;

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(Storage).to(StorageMock);
  storageMock = container.get<unknown>(Storage) as StorageMock;
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  storageMock = null!;
});

test('shows the message as default', () => {
  wrapper = render(<TestMessage/>);
  wrapper.getByText('Close');
});

test('does not show the message if already shown', () => {
  storageMock.stored.HELP_MESSAGE_testId = false;

  wrapper = render(<TestMessage/>);
  expect(wrapper.queryByText('Close')).toBeNull();
});

test('dismissing the message hides the message and stores the state', async () => {
  wrapper = render(<TestMessage/>);

  fireEvent.click(wrapper.getByText('Close'));

  expect(wrapper.queryByText('Close')).toBeNull();
  expect(storageMock.stored).toEqual({
    HELP_MESSAGE_testId: false,
  });
});
