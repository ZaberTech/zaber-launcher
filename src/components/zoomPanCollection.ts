import _ from 'lodash';

import type { Predicate } from '../types';

export interface ZoomPanState {
  start: number;
  end: number;
}

export type ZoomPanSet = Record<string, ZoomPanState>;

export const DEFAULT_ZOOM_PAN: ZoomPanState = { start: 0, end: 100 };


/**
 * Ensapsulates multiple zoom and pan states identified by string IDs, provides
 * functions for manipulating them as dynamic groups, and batches up change
 * notifications.
 * */
export class ZoomPanCollection {
  private states: ZoomPanSet = {};
  private pendingNotifications = new Set<string>();


  constructor(private onChange: (ids: string[]) => void) {
  }


  public set(ids: string | string[] | Predicate<string>, range: ZoomPanState): void {
    this.idArray(ids).forEach(id => {
      this.states[id] = { ...range };
      this.pendingNotifications.add(id);
    });
  }


  public get(id: string): ZoomPanState {
    if (!(id in this.states)) {
      this.states[id] = { ...DEFAULT_ZOOM_PAN };
    }

    return this.states[id];
  }


  public getState(): ZoomPanSet {
    return _.cloneDeep(this.states);
  }


  public setState(state: ZoomPanSet) {
    this.states = state;
  }


  public getIds(predicate: Predicate<string>): string[] {
    return _.keys(this.states).filter(k => predicate(k));
  }


  public fireNotifications() {
    const ids = Array.from(this.pendingNotifications);
    this.clearNotifications();
    this.onChange(ids);
  }


  public clearNotifications() {
    this.pendingNotifications.clear();
  }


  public copy(fromId: string, toIds: string | string[] | Predicate<string>): void {
    this.set(toIds, this.get(fromId));
  }


  public pan(ids: string | string[] | Predicate<string>, amount: number, clamp: ZoomPanState = DEFAULT_ZOOM_PAN): void {
    if (amount !== 0) {
      this.idArray(ids).forEach(id => {
        const range = this.get(id);
        const newRange = ZoomPanCollection.panRange(range, amount * (range.end - range.start), clamp);
        this.set(id, newRange);
        this.pendingNotifications.add(id);
      });
    }
  }


  private static panRange(range: ZoomPanState, amount: number, clamp: ZoomPanState = DEFAULT_ZOOM_PAN): ZoomPanState {
    if (amount < 0) {
      amount = Math.max(amount, -(range.start - clamp.start));
    } else {
      amount = Math.min(amount, clamp.end - range.end);
    }

    return { start: range.start + amount, end: range.end + amount };
  }


  public zoom(ids: string | string[] | Predicate<string>,
    center: number,
    proportion: number,
    clamp: ZoomPanState = DEFAULT_ZOOM_PAN): void {
    this.idArray(ids).forEach(id => {
      const range = this.get(id);
      const newRange = ZoomPanCollection.zoomRange(range, center, proportion, clamp);
      this.set(id, newRange);
      this.pendingNotifications.add(id);
    });
  }


  private static zoomRange(range: ZoomPanState, center: number, proportion: number, clamp: ZoomPanState): ZoomPanState {
    let result = { ...range };

    const diff = proportion * (range.end - range.start);
    result = {
      start: result.start + center * diff,
      end: result.end - (1 - center) * diff,
    };

    if (result.start > result.end) {
      result = { start: result.end, end: result.start };
    }

    result = { start: Math.max(result.start, clamp.start), end: Math.min(result.end, clamp.end) };
    return result;
  }


  private idArray(ids: string | string[] | Predicate<string>): string[] {
    let result: string[];

    if (!Array.isArray(ids)) {
      if (typeof ids === 'string') {
        result = [ids];
      } else {
        result = this.getIds(ids as Predicate<string>);
      }
    } else {
      result = ids;
    }

    return result;
  }
}
