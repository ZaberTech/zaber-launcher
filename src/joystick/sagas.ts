import fs from 'fs/promises';

import { all, call, select, put, takeLatest, takeEvery, take } from 'redux-saga/effects';
import type { SagaIterator } from 'redux-saga';
import { ensureError, throwUnexpectedError } from '@zaber/toolbox';
import { ascii, Units } from '@zaber/motion';
import _ from 'lodash';
import moment from 'moment';

import { Action, SagaIter, RT } from '../utils';
import { getAxis, getDevice } from '../connection_manager';
import { getContainer } from '../container';
import { Storage } from '../app_components';
import { Dialogs } from '../dialogs';

import { ActionTypes, actions, ActionsToPayloads } from './actions';
import { selectPrimaryAxes, selectJoystick, selectConfig, selectConfigReading, selectDevices, selectChainInfo } from './selectors';
import {
  AxisConfig, KeyConfig, KeyEventTypes, ChainInfo, JoystickConfig, SavedConfigHeader,
  TargetAxisType, allEventTypes, SavedConfig, SAVE_FILE_VERSION, SAVE_FILE_IDENT,
} from './types';
import { convertToCurrentVersion } from './save_file_compatibility';

export function* rootSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.MOUNT, mount),
    takeLatest(ActionTypes.SET_SELECTED_KEY, setSelectedKey),
    takeEvery(ActionTypes.WRITE_CONFIG, writeConfig),
    takeEvery(ActionTypes.SAVE_CONFIG, saveConfig),
    takeEvery(ActionTypes.LOAD_CONFIG, loadConfig),
    takeLatest(ActionTypes.START_CALIBRATION, startCalibration),
    takeEvery(ActionTypes.SET_PRODUCTION_MODE, setProductionMode),
    takeEvery(ActionTypes.RESTORE_CONFIG, restoreSettings),
  ]);
}

export const PRODUCTION_MODE_STORAGE_KEY = 'JOYSTICK_PRODUCTION_MODE';

function* readJoystickConfig(joystick: NonNullable<RT<typeof selectJoystick>>) {
  const chainInfo: ChainInfo = {
    nonExistentDeviceAddress: 99,
    axes: {},
  };

  const devices: RT<typeof selectDevices> = yield select(selectDevices);
  while (_.find(devices, device => device.address === chainInfo.nonExistentDeviceAddress)) {
    chainInfo.nonExistentDeviceAddress--;
  }

  const axes: RT<typeof selectPrimaryAxes> = yield select(selectPrimaryAxes);
  for (const axis of _.values(axes)) {
    const asciiAxis: RT<typeof getAxis> = yield call(getAxis, axis.key);
    const maxspeedNative: RT<typeof asciiAxis.settings.get> = yield asciiAxis.settings.get('maxspeed', Units.NATIVE);
    chainInfo.axes[axis.key] = {
      maxspeedNative,
    };
  }

  const device: RT<typeof getDevice> = yield call(getDevice, joystick.key);
  const config: JoystickConfig = {
    axes: [],
    keys: [],
  };
  for (let i = 1; i <= joystick.axisCount; i++) {
    const axisConfig: RT<typeof readAxisConfig> = yield readAxisConfig(device, i);
    config.axes.push(axisConfig);
  }
  for (let i = 1; i <= joystick.keyCount; i++) {
    const keyConfig: RT<typeof readKeyConfig> = yield readKeyConfig(device, i);
    config.keys.push(keyConfig);
  }
  yield put(actions.configRead(config, chainInfo));
};

function* mount(): SagaIter {
  const storage = getContainer().get(Storage);

  const productionMode = storage.load<boolean>(PRODUCTION_MODE_STORAGE_KEY);
  if (productionMode != null) {
    yield put(actions.setProductionMode(productionMode, false));
  }
}

function* setSelectedKey(): SagaIter {
  const joystick: RT<typeof selectJoystick> = yield select(selectJoystick);
  if (joystick == null) { return }
  try {
    yield call(readJoystickConfig, joystick);
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.configReadErr(err.message));
  }
}

function* readAxisConfig(device: ascii.Device, axisNumber: number): SagaIter<AxisConfig> {
  const info: RT<typeof device.genericCommand> = yield device.genericCommand(`joystick ${axisNumber} info`);
  const parts = info.data.split(/\s+/);
  const config: Partial<AxisConfig> = {
    axisNumber,
  };
  config.targetDevice = parseNumber(parts.shift());
  if (!Number.isInteger(+parts[0])) {
    config.axisType = parts.shift() as TargetAxisType;
  } else {
    config.axisType = 'axis';
  }
  config.targetAxis = parseNumber(parts.shift());
  config.maxspeedNative = parseNumber(parts.shift());
  config.profile = parseNumber(parts.shift());
  config.inverted = parts.shift() === '1';
  config.resolution = parseNumber(parts.shift());
  return config as AxisConfig;
}

function parseNumber(data: string | undefined): number {
  if (data == null || data === '') { throw new Error('Data missing') }
  const parsed = +data;
  if (!Number.isFinite(parsed)) { throw new Error(`Invalid number ${data}`) }
  return parsed;
}

const KEY_CMD_PREFIX = 'cmd ';

function* readKeyConfig(device: ascii.Device, keyNumber: number): SagaIter<KeyConfig> {
  const config: KeyConfig = {
    keyNumber,
    alerts: allEventTypes(false),
    commands: [],
  };
  for (const eventType of KeyEventTypes) {
    const info: RT<typeof device.genericCommandMultiResponse> = yield device.genericCommandMultiResponse(
      `key ${keyNumber} ${eventType} info`);

    config.alerts[eventType] = info[0].data.includes('enabled');

    for (const command of info.slice(1).filter(cmd => cmd.data.startsWith(KEY_CMD_PREFIX))) {
      const parts = command.data.split(/\s+/);
      const targetDevice = +parts[1];
      let targetAxis = +parts[2];
      let remainingParts = parts.slice(3);
      let axisType: TargetAxisType = 'axis';
      if ((remainingParts[0] === 'lockstep' || remainingParts[0] === 'virtual') && remainingParts[1]?.match(/^\d+$/)) {
        axisType = remainingParts[0];
        targetAxis = +remainingParts[1];
        remainingParts = remainingParts.slice(2);
      }
      config.commands.push({
        event: eventType,
        targetDevice,
        targetAxis,
        axisType,
        command: remainingParts.join(' '),
      });
    }
  }
  return config;
}

function* writeConfig(): SagaIter {
  const config: RT<typeof selectConfig> = yield select(selectConfig);
  const joystick: NonNullable<RT<typeof selectJoystick>> = yield select(selectJoystick);
  try {
    const device: RT<typeof getDevice> = yield call(getDevice, joystick.key);
    const { nonExistentDeviceAddress }: RT<typeof selectChainInfo> = yield select(selectChainInfo);

    for (const axis of config.axes) {
      const axisType = axis.axisType === 'axis' ? '' : axis.axisType;
      const targetDevice = axis.targetDevice ?? nonExistentDeviceAddress;
      yield device.genericCommand(`joystick ${axis.axisNumber} target ${targetDevice} ${axisType} ${axis.targetAxis ?? 0}`);
      yield device.genericCommand(`joystick ${axis.axisNumber} speedprofile ${axis.profile}`);
      yield device.genericCommand(`joystick ${axis.axisNumber} maxspeed ${_.round(axis.maxspeedNative ?? 0)}`);
      yield device.genericCommand(`joystick ${axis.axisNumber} invert ${Number(axis.inverted)}`);
      yield device.genericCommand(`joystick ${axis.axisNumber} resolution ${axis.resolution}`);
    }

    for (const key of config.keys) {
      for (const eventType of KeyEventTypes) {
        yield device.genericCommand(`key ${key.keyNumber} ${eventType} clear`);
        yield device.genericCommand(`key ${key.keyNumber} ${eventType} alert ${Number(key.alerts[eventType])}`);
      }

      for (const command of key.commands) {
        if (!command.command.trim()) { continue }

        const prefix: string[] = [];
        prefix.push(`${(command.targetDevice ?? nonExistentDeviceAddress)}`);
        if (command.axisType !== 'axis') {
          prefix.push('0');
          prefix.push(command.axisType);
        }
        prefix.push(`${command.targetAxis ?? 0}`);

        yield device.genericCommand(
          `key ${key.keyNumber} ${command.event} add ${prefix.join(' ')} ${command.command}`);
      }
    }

    if (config.keys.some(key => Object.values(key.alerts).some(on => on))) {
      yield device.settings.set('comm.alert', 1);
    }

    yield put(actions.writeConfigDone());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.writeConfigDone(err.message));
  }
}

function* saveConfig(): SagaIter {
  const joystick: RT<typeof selectJoystick> = yield select(selectJoystick);
  const { config }: RT<typeof selectConfigReading> = yield select(selectConfigReading);
  if (config == null || joystick == null) { return }

  try {
    const saveLocation: RT<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
      title: 'Save Configuration',
      defaultPath: `joystick-config-${moment().format('YYYY-MM-DD-HHmm')}.json`,
    });
    if (saveLocation.canceled) {
      return;
    }

    const saveConfig: SavedConfig = {
      type: SAVE_FILE_IDENT,
      version: SAVE_FILE_VERSION,
      deviceId: joystick.identity.deviceId,
      ...config,
    };

    yield fs.writeFile(saveLocation.filePath!, JSON.stringify(saveConfig, null, 2));
  } catch (err) {
    yield put(actions.setSaveLoadConfigError(ensureError(err).message));
  }
}

function* loadConfig(): SagaIter {
  const joystick: RT<typeof selectJoystick> = yield select(selectJoystick);
  if (joystick == null) { return }

  try {
    const saveLocation: RT<typeof Dialogs.showOpenDialog> = yield call(Dialogs.showOpenDialog, {
      title: 'Load Configuration',
      filters: [{
        name: 'JSON',
        extensions: ['json'],
      }],
    });
    if (saveLocation.canceled) {
      return;
    }

    const rawData: RT<typeof fs.readFile> = yield fs.readFile(saveLocation.filePaths[0]);
    const savedConfig = JSON.parse(rawData as string) as SavedConfigHeader;

    const { type, deviceId, version, ...pureConfig } = savedConfig;

    if (type !== SAVE_FILE_IDENT) {
      throw new Error(`File does not appear to be a joystick config (type ${type}).`);
    } else if (!Number.isInteger(version) || version > SAVE_FILE_VERSION) {
      throw new Error(`Unsupported config file version ${version}.`);
    } else if (deviceId !== joystick.identity.deviceId) {
      throw new Error(`Configuration not compatible with device ${joystick.identity.deviceId} (requires ${deviceId}).`);
    }

    const currentConfigVersion = convertToCurrentVersion(pureConfig, version) as JoystickConfig;

    yield put(actions.loadConfigDone(currentConfigVersion));
  } catch (err) {
    yield put(actions.setSaveLoadConfigError(ensureError(err).message));
  }
}

function* startCalibration({ payload: { type: calibrationType } }: Action<ActionsToPayloads[ActionTypes.START_CALIBRATION]>): SagaIter {
  const joystick: RT<typeof selectJoystick> = yield select(selectJoystick);
  if (joystick == null) { return }
  try {
    const device: RT<typeof getDevice> = yield call(getDevice, joystick.key);
    yield device.genericCommand(`joystick calibrate ${calibrationType} start`);
    const stopAction: RT<typeof actions.stopCalibration> = yield take(ActionTypes.STOP_CALIBRATION);
    if (stopAction.payload?.save) {
      yield device.genericCommand(`joystick calibrate ${calibrationType} save`);
    } else {
      yield device.genericCommand('system reset');
    }
  } catch (err) {
    yield put(actions.calibrationError(ensureError(err).message));
  }
}

function setProductionMode(
  { payload: { mode, persist } }: Action<ActionsToPayloads[ActionTypes.SET_PRODUCTION_MODE]>,
) {
  if (!persist) { return }

  const storage = getContainer().get(Storage);
  storage.save(PRODUCTION_MODE_STORAGE_KEY, mode);
}

function* restoreSettings(): SagaIter {
  const joystick: RT<typeof selectJoystick> = yield select(selectJoystick);
  if (joystick == null) { return }
  try {
    yield call(readJoystickConfig, joystick);
    yield put(actions.restoreConfigDone());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.restoreConfigDone(err.message));
  }
}
