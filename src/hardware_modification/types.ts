import { ascii } from '@zaber/motion';

import type settingsData from '../protocol_manual/settings.json';
import type { AsciiComms } from '../connection_manager/types';

import type { HwModDeviceOrAxisState } from './selectors';
import type { FieldIds, SectionIds } from './data_definitions';
import type { FieldError } from './validators';

// Types of the protocol manual data

export interface SettingData {
  description: string;
  options?: EnumProtocolDataOption[];
  units?: string;
}

export interface EnumProtocolDataOption {
  value: number;
  label: string;
  profiling_attributes?: {
    min_fw?: string;
    max_fw?: string;
    capability?: string[];
    setting?: string[];
  };
}

type AllSettingKeys = keyof typeof settingsData;
export type DescribedSettings = {[K in AllSettingKeys]: typeof settingsData[K] extends SettingData ? K : never}[AllSettingKeys];

export type Initializer = {
  type: 'setting';
  backing: DescribedSettings;
} | {
  type: 'variant';
  backing: (info: HwModDeviceOrAxisState) => DescribedSettings;
} | {
  type: 'custom';
  backing: (ascii: AsciiComms, info: HwModDeviceOrAxisState) => Promise<number | null | Partial<FieldValue>>;
  /** A list of the settings that this field needs to be present */
  requires?: string[];
};

type CheckIfEnabled<S extends SectionIds> = Partial<Record<FieldIds<S>, (values: SectionValues<S>) => boolean>>;
export interface SectionDataDefinition<S extends SectionIds> {
  initializers: Record<FieldIds<S>, Initializer>;
  sectionPresent: (device: { info: HwModDeviceOrAxisState; ascii: AsciiComms }) => Promise<boolean>;
  fieldEnabled: CheckIfEnabled<S>;
}

export type SomeSectionDataDefinition = {
  [S in SectionIds]: SectionDataDefinition<S>
}[SectionIds];

export type ValueKey = {
  [S in SectionIds]:  {
      sectionId: S;
      fieldId: FieldIds<S>;
  };
}[SectionIds];

export type ValueUpdate<S extends SectionIds> = { sectionId: S; fieldId: FieldIds<S>; value: number | 'revert' | null };

export type FieldValue = {
  is: number | null;
  was: number | null;
  updateApplied: boolean;
  error: FieldError | null;
  /** The setting this field should reference and write to, or null if this setting should not be written */
  setting: DescribedSettings | null;
};

export type SectionValues<S extends SectionIds> = Partial<Record<FieldIds<S>, FieldValue>>;

export type Values = {
  [S in SectionIds]: SectionValues<S> | null;
};

export type FieldKey = { sectionId: string; fieldId: string };

export interface NonNullValue {
  /** The location of this value in the field value record */
  at: ValueKey;
  /** The setting this value should be written to */
  setting: DescribedSettings | null;
  /** Whether this value should be written */
  enabled: boolean;
  /** The guaranteed non-null value */
  value: number;
  /** Whether this value has changed since the last read */
  changed: boolean;
}
export type NonNullSectionValues<S extends SectionIds> = Partial<Record<FieldIds<S>, NonNullValue>>;

/** A collection of all of the enabled field's values */
export type NonNullValues = {
  [SK in SectionIds]?: NonNullSectionValues<SK>
};

export type AxisType = ascii.AxisType;

export const AxisType = ascii.AxisType;

export const TITLE = 'Advanced Hardware Setup';
