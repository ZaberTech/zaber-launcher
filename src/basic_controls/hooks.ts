import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { EntityKey, EntityKeyType, extractDeviceKey, getAxisNumber, getEntityType } from '../keys';
import { useActions } from '../utils';

import { actionDefinitions } from './actions';
import { selectDeviceInfo } from './selectors';

export function useProductInfo(key: EntityKey, reload = false) {
  const actions = useActions(actionDefinitions);
  const info = useSelector(selectDeviceInfo);
  const deviceKey = extractDeviceKey(key);
  const deviceInfo = info[deviceKey];
  useEffect(() => {
    if (deviceInfo == null || reload) {
      actions.fetchDeviceInfo(deviceKey);
    } else if (deviceInfo.type === 'invalid' && deviceInfo.reason === 'disconnected') {
      actions.fetchDeviceInfo(deviceKey);
    }
  }, [key, deviceInfo?.type]);
  if (deviceInfo == null) {
    return undefined;
  }
  switch (getEntityType(key)) {
    case EntityKeyType.DEVICE: return deviceInfo;
    case EntityKeyType.AXIS: {
      if (deviceInfo.type === 'invalid' || deviceInfo.type === 'process-controller' || deviceInfo.type === 'lamp-controller') {
        return deviceInfo;
      }
      if (deviceInfo.type !== 'controller') {
        throw new Error(`Cannot get axes of device of type ${deviceInfo.type} with key ${key}`);
      }
      const requestedAxisNumber = getAxisNumber(key);
      for (const [axisNumber, peripheral] of deviceInfo.peripherals.entries()) {
        if (axisNumber === requestedAxisNumber) {
          return peripheral;
        }
        if (peripheral.type === 'lockstep' && peripheral.secondaries.some(s => s.key === key)) {
          return peripheral;
        }
      }
      throw new Error(`Cannot get peripheral with key ${key}`);
    }
    default: throw new Error(`Cannot get device info of entity with key ${key}`);
  }
}
