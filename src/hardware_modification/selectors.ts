/* eslint-disable camelcase */
import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectIdentifiedDevices, selectIdentifiedAxes, IdentifiedDeviceState, IdentifiedAxisState } from '../connection_manager';
import { hasSetting } from '../devices';
import { EntityKeyType, getEntityType, tryExtractDeviceKey } from '../keys';
import { selectHardwareModification } from '../store';
import { tryAccess } from '../utils';
import settingsData from '../protocol_manual/settings.json';
import { selectManualProfilingData } from '../protocol_manual';
import { compareFirmwareVersions, parseFirmwareVersion } from '../app_components';

import type { ValidationError } from './validators';
import type { NonNullSectionValues, NonNullValues, SettingData, ValueKey } from './types';
import { fieldEnabled, getSectionFields } from './utils';
import { allSectionIds, SectionIds } from './data_definitions';

export const selectChosenProductKey = createSelector(selectHardwareModification, state => state.chosenProductKey);

export interface HwModDeviceOrAxisState {
  key: string;
  isModified: boolean;
  isModifiable: boolean;

  device: IdentifiedDeviceState;
  axis?: IdentifiedAxisState;
}

export const selectDeviceOrAxisInfo = createSelector(
  selectChosenProductKey, selectIdentifiedDevices, selectIdentifiedAxes, (productKey, devices, axes): HwModDeviceOrAxisState | null => {
    const device = tryAccess(devices, tryExtractDeviceKey(productKey));
    if (device == null) {
      return null;
    }

    const axis = tryAccess(axes, productKey);
    if (axis != null) {
      return {
        key: axis.key,
        isModified: axis.identity.isModified,
        isModifiable: hasSetting('peripheral.hw.modified', axis),

        device,
        axis,
      };
    } else {
      if (getEntityType(productKey) === EntityKeyType.AXIS) { return null }

      return {
        key: device.key,
        isModified: device.identity.isModified,
        isModifiable: hasSetting('device.hw.modified', device),

        device,
      };
    }
  }
);

export const selectValues = createSelector(selectHardwareModification, state => state.values);

export const selectScrollToError = createSelector(selectHardwareModification, state => state.scrollToError);

interface ValuesToWrite {
  /** List of values that have a null is value */
  invalid: ValueKey[];
  /** List of required fields that have not been completed yet */
  unset: ValueKey[];
  /** The values currently in the form. Any values here will be non-null */
  values: NonNullValues;
}
const selectValidatedFields = createSelector(selectValues, values => {
  const valuesToWrite: ValuesToWrite = { invalid: [], unset: [], values: {} };
  if (values == null) { return valuesToWrite }
  for (const sectionId of allSectionIds) {
    const sectionFields = getSectionFields(sectionId, values);
    if (sectionFields == null) { continue }
    const fieldValues: NonNullSectionValues<typeof sectionId> = {};
    for (const [fieldId, field] of Object.entries(sectionFields)) {
      const at = { sectionId, fieldId } as ValueKey;
      const enabled = fieldEnabled(at.sectionId, at.fieldId, sectionFields);
      fieldValues[at.fieldId] = {
        at,
        setting: field.setting,
        enabled,
        changed: field.is !== field.was,
        value: field.is ?? 0,
      };
      if (enabled && field.is == null) {
        if (field.was == null) {
          valuesToWrite.unset.push(at);
        } else {
          valuesToWrite.invalid.push(at);
        }
      }
    }
    valuesToWrite.values[sectionId] = fieldValues;
  }
  return valuesToWrite;
});

export const selectUnsetCount = createSelector(selectValidatedFields, (values): number => values.unset.length);

export const selectChangeCount = createSelector(selectValidatedFields, (values): number => (
  Object.values(values.values).flatMap(sectionValues => Object.values(sectionValues).filter(field => field.changed)).length
));

export const selectUnsetFields = createSelector(selectValidatedFields, values => {
  const unsetFields: Partial<Record<SectionIds, Partial<Record<string, true>>>> = {};
  for (const unsetValue of values.unset) {
    const section = unsetFields[unsetValue.sectionId];
    if (!section) {
      unsetFields[unsetValue.sectionId] = { [unsetValue.fieldId]: true };
    } else {
      section[unsetValue.fieldId] = true;
    }
  }
  return unsetFields;
});

export const selectChangedFields = createSelector(selectValidatedFields, values => _.mapValues(values.values, sectionValues => {
  if (!sectionValues) { return undefined }
  const changed: Partial<Record<string, true>> = {};
  for (const [fieldId, value] of Object.entries(sectionValues)) {
    if (value.changed) {
      changed[fieldId] = true;
    }
  }
  return changed;
}));

export const selectAllFieldsIfValid = createSelector(selectValidatedFields, values => {
  if (values.unset.length > 0 || values.invalid.length > 0) {
    return null;
  }
  return values.values;
});

export const selectErrorsOnForm = createSelector(selectValues, values => {
  if (!values) { return [] }
  return Object.values(values).flatMap(sectionValues => {
    if (!sectionValues) { return [] }
    return Object.values(sectionValues).reduce<ValidationError[]>((errors, fieldValue) => {
      if (fieldValue.error) {
        return [...errors, fieldValue.error];
      } else {
        return errors;
      }
    }, []);
  });
});

export const selectApplyProgress = createSelector(selectHardwareModification, state => state.applyProgress);
export const selectScrollPosition = createSelector(selectHardwareModification, state => state.scrollPosition);

export const selectShouldShowBanner = createSelector(
  selectHardwareModification, selectErrorsOnForm, selectValidatedFields, (state, formErrors, values) => (
    state.error.on !== 'none' || formErrors.length > 0 || values.unset.length > 0
  )
);

export const selectProfiledSettingsData = createSelector(selectDeviceOrAxisInfo, selectManualProfilingData,
  (deviceOrAxis, manualProfilingData) => {
    if (deviceOrAxis == null) { return null }
    const profile = tryAccess(manualProfilingData, deviceOrAxis.key);
    if (profile == null) { return null }

    return _.mapValues(settingsData as Record<string, SettingData>, setting => {
      if ('options' in setting) {
        const options = setting.options?.filter(({ profiling_attributes: attributes }) => {
          if (attributes == null) { return true }

          const { capability, min_fw, max_fw, setting } = attributes;

          if (capability?.some(capability => !profile.capabilities.includes(capability))) {
            return false;
          }
          if (min_fw && compareFirmwareVersions(parseFirmwareVersion(min_fw), profile.firmwareVersion) > 0) {
            return false;
          }
          if (max_fw && compareFirmwareVersions(parseFirmwareVersion(max_fw), profile.firmwareVersion) < 0) {
            return false;
          }
          if (setting?.some(setting => !profile.settings.includes(setting))) {
            return false;
          }

          return true;
        });

        setting = { ...setting, options };
      }

      return setting;
    });
  });
