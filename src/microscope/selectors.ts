import { createSelector } from 'reselect';
import { tryAccess } from '@zaber/toolbox';
import { Length } from '@zaber/motion';
import _ from 'lodash';

import { selectMicroscope } from '../store';
import {
  selectConnections,
  selectIdentifiedDevices,
} from '../connection_manager';
import { convertBetweenUnits, type MeasurementOK } from '../units';
import { extractConnectionKey } from '../keys';

import { isApplied, makePresetValue, type PresetData } from './presets';
import { type Axes, StoredPosition, ILLUMINATOR_CHANNELS, getOperationResult, Operations, EMPTY_OBJECTIVE_PARAMETERS } from './types';

export const selectState = createSelector(selectMicroscope, state => state.app);

export const selectSelectedKey = createSelector(selectState, state => state.selectedKey);

export const selectConnection = createSelector(selectSelectedKey, selectConnections,
  (connectionKey, connections) => tryAccess(connections, connectionKey));

export const selectTestState = createSelector(selectState, state => state.test);

export const selectMonitorState = createSelector(selectState, state => state.monitor);
export const selectStableInfo = createSelector(selectMonitorState, state => state.stableInfo);
export const selectMonitorWillRefresh = createSelector(selectMonitorState, state => state.refreshing || state.refreshPending);
export const selectRefreshPending = createSelector(selectMonitorState, state => state.refreshPending);
export const selectMonitorBursting = createSelector(selectMonitorState, state => state.burstCounter > 0);
export const selectedMounted = createSelector(selectState, state => state.mounted);
export const selectMonitorError = createSelector(selectMonitorState, state => state.error);

export const selectOperations = createSelector(selectState, state => state.operations);

export const selectFactoryResetStep = createSelector(selectState, state => state.factoryReset.step);
export const selectFactoryResetErrors = createSelector(selectState, state => state.factoryReset.errors);
export const selectFactoryResettableDevices = createSelector(selectIdentifiedDevices, selectConnection,
  (allDevices, connection) => _.values(allDevices)
    .filter(device => extractConnectionKey(device.key) === connection?.key)
    .filter(device => !device.identity.name.toUpperCase().startsWith('X-JOY')));

export const selectPositionUnits = createSelector(selectState, state => state.positionUnits);
export const selectAxes = createSelector(selectState, state => state.axes);
export const selectPresetEditor = createSelector(selectState, state => state.presetEditor);
export const selectPresets = createSelector(selectState, state => state.presets);
export const selectPreset = createSelector(selectState, state => state.preset);

export const selectAutofocusObjectiveSettings = createSelector(selectOperations, selectMonitorState,
  (operations, monitor) => {
    const result = getOperationResult(operations.autofocusGetSettings, Operations.autofocusGetSettings);
    const currentObjective = monitor.info?.objectiveChanger?.objective ?? 1;
    const settings = result?.objectives?.[currentObjective - 1] ?? EMPTY_OBJECTIVE_PARAMETERS;
    return settings;
  });

const ZERO_MEASUREMENT = { value: 0, units: Length.MICROMETRES } as MeasurementOK;

export const selectPresetDataFromState = createSelector(
  selectState, selectAutofocusObjectiveSettings,
  (state, autofocusSettings): PresetData => {
    const monitor = state.monitor.info;
    const { cameraTrigger: { settings: trigger } } = state;
    return {
      filter: makePresetValue(monitor?.filterChanger?.filter ?? 1),
      objective: makePresetValue(monitor?.objectiveChanger?.objective ?? 1),
      axes: _.mapValues(state.axes, (axisState, axis): PresetData['axes']['xAxis'] => ({
        step: makePresetValue(axisState.step),
        position: makePresetValue(monitor?.[axis as Axes]?.position ?? ZERO_MEASUREMENT),
      })),
      illuminator: Object.fromEntries(
        ILLUMINATOR_CHANNELS.map(channel => {
          const channelState = monitor?.illuminator?.channels?.[channel];
          return ([channel, {
            on: makePresetValue(channelState?.on ?? false),
            intensity: makePresetValue(channelState?.intensity ?? 0),
          }]);
        })),
      autofocus: {
        limitMin: makePresetValue(monitor?.autofocus?.limits?.min ?? ZERO_MEASUREMENT),
        limitMax: makePresetValue(monitor?.autofocus?.limits?.max ?? ZERO_MEASUREMENT),
        settings: _.mapValues(autofocusSettings, value => makePresetValue(value ?? 0)),
      },
      cameraTrigger: {
        exposure: makePresetValue(trigger.exposure),
        waitBefore: makePresetValue(trigger.waitBefore),
        waitAfter: makePresetValue(trigger.waitAfter),
      },
    };
  });

export const selectAppliedPreset = createSelector(selectPresets, selectPreset,
  (presets, preset) => presets[preset.current ?? -1] ?? null);

export const selectAppliedPresetMatches = createSelector(selectAppliedPreset, selectPresetDataFromState,
  (preset, statePreset) => {
    if (preset == null) {
      return true;
    }
    return isApplied(preset.data, statePreset);
  });

export const selectProductionMode = createSelector(selectState, state => state.productionMode);

export const selectCurrentFocusOffset = createSelector(selectMonitorState, selectPositionUnits,
  (state, units): MeasurementOK => {
    const { stableInfo, info } = state;
    const currentObjective = info?.objectiveChanger?.objective;
    if (currentObjective == null || currentObjective === 0 || stableInfo?.objectiveChanger == null) {
      return { value: 0, units: units.focusAxis };
    }
    const offset = stableInfo.objectiveChanger.focusOffsets[currentObjective - 1];
    return {
      value: convertBetweenUnits(offset.value, offset.units, units.focusAxis),
      units: units.focusAxis,
    };
  });

export const selectPositions = createSelector(selectMonitorState, state => ({
  x: state.info?.xAxis?.position,
  y: state.info?.yAxis?.position,
  focus: state.info?.focusAxis?.position,
}));

export const selectPositionWithoutOffset = createSelector(selectPositions, selectCurrentFocusOffset,
  (info, focusOffset) => info && ({
    xAxis: info.x,
    yAxis: info.y,
    focusAxis: info.focus && {
      value: info.focus.value - focusOffset.value,
      units: info.focus.units,
    } satisfies MeasurementOK,
  }));

export const selectStoredPositions = createSelector(selectState, state => state.storedPositions);
export interface StoredPositionDisplayed extends StoredPosition {
  index: number;
}
export const selectStoredPositionsToDisplay = createSelector(selectStoredPositions, selectCurrentFocusOffset,
  (positions, focusOffset) => positions.map((position, i): StoredPositionDisplayed => ({
    ...position,
    focusAxis: {
      value: position.focusAxis.value + focusOffset.value,
      units: position.focusAxis.units,
    },
    index: i,
  })).sort((a, b) => a.label.localeCompare(b.label)));

export const selectAutofocusSettings = createSelector(selectState, state => state.autofocusSettings);

export const selectValidationErrors = createSelector(selectState, state => state.validationErrors);

export const selectTriggerSettings = createSelector(selectState, state => state.cameraTrigger);
export const selectTriggerSettingsValues = createSelector(selectTriggerSettings, trigger => trigger.settings);

export const selectMonitorBlocked = createSelector(selectMonitorState,
  state => _.values(state.blockedBy).some(Boolean));

export const selectDemoMode = createSelector(selectState, state => state.demoMode);

export const selectObjectiveLabels = createSelector(selectStableInfo, stableInfo => {
  const info = stableInfo?.objectiveChanger;
  if (info == null) { return [] }

  return _.range(1, info.numberOf + 1)
    .map(objective => info.labels[objective - 1] || `Objective ${objective}`);
});

export const selectFilterLabels = createSelector(selectStableInfo, stableInfo => {
  const info = stableInfo?.filterChanger;
  if (info == null) { return [] }

  return _.range(1, info.numberOf + 1)
    .map(filter => info.labels[filter - 1] || `Filter ${filter}`);
});
