import type { SagaIterator } from 'redux-saga';
import { all, put, call, select, takeLatest, SagaReturnType, takeEvery, delay } from 'redux-saga/effects';
import { throwUnexpectedError, tryAccess } from '@zaber/toolbox';
import { ascii, Units, NoValueForKeyException, CommandFailedException } from '@zaber/motion';

import { getAxisOrOnlyAxis } from '../connection_manager';
import { getContainer } from '../container';
import type { Action, SagaIter, RT } from '../utils';
import { getLogger } from '../log';
import { Logger, Storage } from '../app_components';
import type { EntityKey } from '../keys';
import { is3rdPartyId } from '../hardware_modification';
import * as hwModStorageKeys from '../hardware_modification/storageKeys';

import { Setting, MotorTypes, LoadingErrorEnum } from './types';
import { ActionsToPayloads, ActionTypes, actions } from './actions';
import { selectAxesAndDevices, selectSelectedAxisOrDevice } from './selectors';
import { CurrentTunerAPI } from './components/service';
import { getSetting, resolveInputSettings } from './utils';

let logger: Logger;
const NEEDED_ACCESS_LEVEL = 2;

export const CURRENT_TUNER_WARNING_STORAGE_KEY = 'CURRENT_TUNER_SHOW_WARNING';

export function* currentTunerSaga(): SagaIterator {
  logger = getLogger('currentTunerSaga');
  yield all([
    takeLatest([ActionTypes.SELECT_AXIS_OR_DEVICE], onSelectAxisOrDevice),
    takeLatest([ActionTypes.UPDATE_SLIDERS], onUpdateSliders),
    takeEvery([ActionTypes.APPLY_SLIDER_VALUES], onApplySliderValues),
    takeEvery([ActionTypes.SET_DRIVER], onSetDriver),
    takeEvery([ActionTypes.ON_FIRST_MOUNT], onFirstMount),
    takeEvery([ActionTypes.UPDATE_SHOW_WARNING], onUpdateShowWarning),
  ]);
}

function* onSetDriver({
  payload: { enableDriver }
}: Action<ActionsToPayloads[ActionTypes.SET_DRIVER]>): SagaIterator {
  try {
    const selectedDeviceOrAxis: NonNullable<ReturnType<typeof selectSelectedAxisOrDevice>> = yield select(selectSelectedAxisOrDevice);
    const axis: ascii.Axis = yield call(getAxisOrOnlyAxis, selectedDeviceOrAxis, { allowNonIdentified: true });
    if (enableDriver) {
      yield call([axis, axis.driverEnable]);
    } else {
      yield call([axis, axis.driverDisable]);
    }
    yield put(actions.updateDriverEnabled(selectedDeviceOrAxis, enableDriver));
    yield put(actions.setDriverDone());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setDriverDone(err.message));
  }
}

async function disableDriverIfEnabled(axis: ascii.Axis): Promise<boolean> {
  const warningFlags = await axis.warnings.getFlags();
  const wasDisabled = warningFlags.has(ascii.WarningFlags.DRIVER_DISABLED_NO_FAULT);
  if (!wasDisabled) {
    await axis.driverDisable();
  }
  return wasDisabled;
}

function* resolve3rdParty(key: EntityKey, axis: ascii.Axis): SagaIter {
  if (!is3rdPartyId(axis.peripheralId)) { return }

  try {
    const ratedCurrent: RT<typeof axis.storage.getNumber> = yield axis.storage.getNumber(hwModStorageKeys.RATED_CURRENT_KEY);
    const stepsPerRotation: RT<typeof axis.storage.getNumber> = yield axis.storage.getNumber(hwModStorageKeys.STEPS_PER_ROTATION_KEY);
    const holdingTorque: RT<typeof axis.storage.getNumber> = yield axis.storage.getNumber(hwModStorageKeys.HOLDING_TORQUE_KEY);
    yield axis.storage.getBool(hwModStorageKeys.SETUP_COMPLETE_KEY);

    yield put(actions.set3rdPartyInfo(key, {
      ratedCurrent,
      stepsPerRotation,
      holdingTorque,
    }));
  } catch (err) {
    if ((err instanceof NoValueForKeyException)) {
      throw new Error(LoadingErrorEnum.THIRD_PARTY_NOT_COMPLETE);
    }
    throw err;
  }
}

function* onSelectAxisOrDevice({
  payload: { axisOrDevice }
}: Action<ActionsToPayloads[ActionTypes.SELECT_AXIS_OR_DEVICE]>): SagaIterator {
  const deviceOrAxis = tryAccess((yield select(selectAxesAndDevices)) as ReturnType<typeof selectAxesAndDevices>, axisOrDevice);
  if (!deviceOrAxis || !axisOrDevice) { return }

  try {
    const api = getContainer().get(CurrentTunerAPI);
    const axis: ascii.Axis = yield call(getAxisOrOnlyAxis, axisOrDevice, { allowNonIdentified: true });

    const isStepperMotor = (yield call(getSetting, axis, { name: 'motor.type' })) === MotorTypes.StepperMotor;
    if (!isStepperMotor) {
      throw new Error(LoadingErrorEnum.NON_STEPPER);
    }

    const tuningInfo: RT<typeof api.getInfo> = yield call([api, api.getInfo], deviceOrAxis.firmwareVersion);
    const inputSettings: RT<typeof resolveInputSettings> = yield call(resolveInputSettings, axis, tuningInfo.inputSettings);
    yield put(actions.populateTuningInfo(axisOrDevice, inputSettings, tuningInfo));

    // Get existing tuning values
    const actualValues: Setting[] = [];
    for (const setting of tuningInfo.settings) {
      const value: RT<typeof getSetting> = yield call(getSetting, axis, setting);
      actualValues.push({ ...setting, value });
    }
    yield put(actions.updateActualSettings(axisOrDevice, actualValues));

    // Get the slider positions corresponding to the existing tuning values
    const sliderPositions: RT<typeof api.getSliderPositions> = yield call([api, api.getSliderPositions],
      inputSettings,
      deviceOrAxis.firmwareVersion,
      actualValues,
    );
    // Update the slider values based on the result.
    if (sliderPositions != null) {
      yield put(actions.updateSliderValues(axisOrDevice, sliderPositions));
      yield put(actions.updateAppliedSliders(axisOrDevice, sliderPositions));
      const ictrlSettings: RT<typeof api.getCurrentControllerParams> = yield call([api, api.getCurrentControllerParams],
        inputSettings,
        deviceOrAxis.firmwareVersion,
        sliderPositions,
      );
      yield put(actions.updateTargetSettings(axisOrDevice, ictrlSettings));
    } else {
      yield put(actions.updateSliderValues(axisOrDevice, null));
    }

    const driverEnabled: number = yield call([axis.settings, axis.settings.get], 'driver.enabled');
    yield put(actions.updateDriverEnabled(axisOrDevice, driverEnabled === 1));

    yield call(resolve3rdParty, axisOrDevice, axis);

    yield put(actions.updateLoaded(true));
  } catch (err) {
    throwUnexpectedError(err);
    let { message } = err;
    if (err instanceof CommandFailedException && err.details.responseData === 'BADCOMMAND') {
      message = LoadingErrorEnum.INCOMPATIBLE_PRODUCT;
    }
    yield put(actions.loadingErr(message));
    logger.warn('Cannot load axis:', err);
  }
}

function* onApplySliderValues({
  payload: { axisOrDevice }
}: Action<ActionsToPayloads[ActionTypes.APPLY_SLIDER_VALUES]>): SagaIterator {
  const deviceOrAxis = ((yield select(selectAxesAndDevices)) as ReturnType<typeof selectAxesAndDevices>)[axisOrDevice];
  try {
    const axis: ascii.Axis = yield call(getAxisOrOnlyAxis, axisOrDevice, { allowNonIdentified: true });
    const device = axis.device;

    const api = getContainer().get(CurrentTunerAPI);
    const ictrlSettings: Setting[] = yield call([api, api.getCurrentControllerParams],
      deviceOrAxis.inputSettings!,
      deviceOrAxis.firmwareVersion,
      deviceOrAxis.sliderPositions!,
    );

    // Apply the settings to the device.
    // Note: the following step is done using the ZML generic command, because the current controller settings are prototype settings.
    // When the underscore is removed from these settings, this code should be switched to use the ZML set command. This is covered
    // by CCT-11. There is no need to keep track of the units for now, since ZML does not support unit conversions for underscore settings.
    // All settings are assumed to be in native units for now.
    const initialAccessLevel: RT<typeof device.settings.get> = yield call(
      [device.settings, device.settings.get], ascii.SettingConstants.SYSTEM_ACCESS
    );
    if (initialAccessLevel < NEEDED_ACCESS_LEVEL) {
      yield call([device.settings, device.settings.set], ascii.SettingConstants.SYSTEM_ACCESS, NEEDED_ACCESS_LEVEL);
    }

    let driverInitiallyDisabled: boolean | undefined;
    try {
      driverInitiallyDisabled = yield call(disableDriverIfEnabled, axis);
      yield put(actions.updateDriverEnabled(axisOrDevice, false));

      for (const ictrlSetting of ictrlSettings) {
        yield call([axis.settings, axis.settings.set], ictrlSetting.name, ictrlSetting.value, ictrlSetting.units as Units | undefined);
      }
      // CCT-48: The software should eventually validate the settings here.
      const actualIctrlSettings: Setting[] = [];
      for (const ictrlSetting of ictrlSettings) {
        const actualValue: SagaReturnType<typeof getSetting> = (yield call(getSetting, axis, ictrlSetting));
        actualIctrlSettings.push({ ...ictrlSetting, value: actualValue });
      }
      // Send the target and actual settings back so that they can be displayed. This is helpful for internal debugging
      // since we don't have robust validation yet.
      yield put(actions.updateAppliedSliders(axisOrDevice, deviceOrAxis.sliderPositions));
      yield put(actions.updateTargetSettings(axisOrDevice, ictrlSettings));
      yield put(actions.updateActualSettings(axisOrDevice, actualIctrlSettings));
    } finally {
      // Restore the access level.
      if (initialAccessLevel < NEEDED_ACCESS_LEVEL) {
        yield call([device.settings, device.settings.set], 'system.access', initialAccessLevel);
      }
    }
    if (driverInitiallyDisabled === false) {
      yield call([axis, axis.driverEnable]);
      yield put(actions.updateDriverEnabled(axisOrDevice, true));
    }
    yield put(actions.applySliderValuesDone(axisOrDevice));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.applySliderValuesDone(axisOrDevice, err.message));
    logger.warn('Cannot apply values:', err);
  }
}

function* onUpdateSliders({
  payload: { axisOrDevice }
}: Action<ActionsToPayloads[ActionTypes.UPDATE_SLIDERS]>): SagaIterator {
  yield delay(300);
  const deviceOrAxis = ((yield select(selectAxesAndDevices)) as ReturnType<typeof selectAxesAndDevices>)[axisOrDevice];
  try {
    const api = getContainer().get(CurrentTunerAPI);
    const ictrlSettings: RT<typeof api.getCurrentControllerParams> = yield call([api, api.getCurrentControllerParams],
      deviceOrAxis.inputSettings!,
      deviceOrAxis.firmwareVersion,
      deviceOrAxis.sliderPositions!,
    );
    yield put(actions.updateTargetSettings(axisOrDevice, ictrlSettings));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.updateApplyErr(axisOrDevice, err.message));
    logger.warn('Error updating settings table', err);
  }
}

function* onFirstMount(): SagaIterator {
  try {
    const storage = getContainer().get(Storage);
    const showWarning = storage.load<boolean>(CURRENT_TUNER_WARNING_STORAGE_KEY, true);

    yield put(actions.updateShowWarning(showWarning, false));
  } catch (err) {
    throwUnexpectedError(err);
  }
}

function onUpdateShowWarning({ payload: { showWarning, permanent } }: Action<ActionsToPayloads[ActionTypes.UPDATE_SHOW_WARNING]>) {
  if (!permanent) {
    return;
  }
  try {
    const storage = getContainer().get(Storage);
    storage.save(CURRENT_TUNER_WARNING_STORAGE_KEY, showWarning);
  } catch (err) {
    throwUnexpectedError(err);
  }
}
