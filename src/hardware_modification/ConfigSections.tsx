import React, { useEffect, useLayoutEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { useActions } from '@zaber/toolbox/lib/redux';
import classNames from 'classnames';

import type { EntityKey } from '../keys';
import { NoContentMessage, SectionMenu } from '../components';

import { actions as actionsDefinition } from './actions';
import { TITLE, type Values } from './types';
import { ConfirmationSection } from './ConfirmationSection';
import { selectScrollPosition, selectScrollToError, selectShouldShowBanner } from './selectors';
import { Banner } from './Banner';
import { HwModSection, HwModSectionTemplate, sectionTemplates } from './components/sections/HwModSection';
import { getSectionFields } from './utils';
import type { SectionIds } from './data_definitions';

interface Props {
  values: Values;
  deviceKey: EntityKey;
}

interface GeneratedSection<S extends SectionIds> extends HwModSectionTemplate<S> {
  id: string;
  name: React.ReactNode;
  content: React.ReactNode;
}

export const ConfigSections: React.FC<Props> = ({ deviceKey, values }) => {
  const scrollToError = useSelector(selectScrollToError);
  const storedScrollPosition = useSelector(selectScrollPosition);
  const bannerShown = useSelector(selectShouldShowBanner);
  const actions = useActions(actionsDefinition);
  const sectionsRef = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    const sectionDiv = sectionsRef.current;
    if (sectionDiv == null) { return }
    setTimeout(() => sectionDiv.scroll({ top: storedScrollPosition }));
    return () => { actions.storeScrollPosition(sectionDiv.scrollTop) };
  }, []);

  useEffect(() => {
    if (scrollToError) {
      const sectionDiv = sectionsRef.current!;
      const id = `HwModError-${scrollToError.sectionId}-${scrollToError.fieldId}`;
      const errorToView = sectionDiv.querySelector(`#${id}`);
      if (errorToView) {
        errorToView.parentElement!.scrollIntoView({ behavior: 'smooth' });
      }
      actions.scrollToError(null);
    }
  }, [scrollToError]);

  const activeSections = sectionTemplates.reduce<GeneratedSection<SectionIds>[]>((cur, template) => {
    const sectionValues = getSectionFields(template.sectionId, values);
    if (!sectionValues) { return cur }

    return [...cur, {
      ...template,
      id: template.sectionId,
      name: template.menuName ?? template.titleName,
      content: template.generateContent(sectionValues),
    }];
  }, []);

  if (activeSections.length <= 0) {
    return <NoContentMessage title={TITLE}
      type="info" bannerTitle="No Supported Modifications"
      message="No modifications to this product can be configured with this app."
    />;
  }

  return <div className={classNames('configuration-settings', { 'banner-shown': bannerShown })}>
    <Banner/>
    <SectionMenu
      refToScrollingSections={sectionsRef}
      menuItems={activeSections}
      content={(on, off) => <>
        {activeSections.map(section => (
          <HwModSection key={section.id} {...{ ...section, on, off }}>{section.content}</HwModSection>
        ))}
        <div className="bottom-padding"/>
      </>}
    />
    <ConfirmationSection deviceKey={deviceKey}/>
  </div>;
};
