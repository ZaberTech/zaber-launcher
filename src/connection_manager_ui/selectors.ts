import { createSelector } from 'reselect';
import { tryAccess } from '@zaber/toolbox';
import _ from 'lodash';

import { selectCManagerUi } from '../store';
import { selectConnections, selectIdentifiedDevices } from '../connection_manager/selectors';
import { extractDeviceKey } from '../keys';
import { commandExists } from '../devices';
import { selectConnectionsBeingRemoved } from '../add_connection/selectors';

export const selectSetLabel = createSelector(selectCManagerUi, state => state.setLabel);
export const selectIsLabelValid = createSelector(selectSetLabel,
  state => state.label.match(/^.{0,32}$/) != null);
export const selectSetLabelSupported = createSelector(selectSetLabel, selectIdentifiedDevices, (state, devices) => {
  if (state.entityKey == null) { return false }
  const device = tryAccess(devices, extractDeviceKey(state.entityKey));
  if (device == null) { return false }
  return commandExists('storage', device, true);
});

export const selectSetConnectionsName = createSelector(selectCManagerUi, state => state.setConnectionsName);

export const selectConnectionsError = createSelector(
  selectConnections,
  selectSetConnectionsName,
  selectConnectionsBeingRemoved,
  (connections, setConnectionsName, connectionsBeingRemoved) =>
    _.mapValues(connections, connection => {
      const setConnectionName = tryAccess(setConnectionsName, connection.key);
      const connectionBeingRemoved = tryAccess(connectionsBeingRemoved, connection.key);

      return connection.loadingDevicesErr?.message ?? connectionBeingRemoved?.error ?? setConnectionName?.error;
    })
);

export const selectDevicesSystemErrors = createSelector(selectCManagerUi, state => state.devicesSystemErrors);
