import { Angle } from '@zaber/motion';

import type { IdentifiedAxisState } from '../connection_manager';
import { getDimensionName } from '../units';

export enum MovementType {
  Home,
  Min,
  Max,
  Stop,
  Absolute,
  Relative,
  Velocity,
  WaitUntilIdle,
  Cycle,
}

export type Initiator = 'home' | 'min' | 'max' | 'stop' | 'absolute' | 'relative left' | 'relative right' |
'velocity left' | 'velocity right' | 'home all' | 'min all' | 'max all' | 'stop all' | 'jog left' | 'jog right' |
'jog done' | 'track' | 'cycle';

export enum AxisErrorType {
  Note,
  Warning,
  Fault,
}

export function warningFlagToErrorType(flag: string): AxisErrorType {
  switch (flag[0]) {
    case 'N':
      return AxisErrorType.Note;
    case 'W':
      return AxisErrorType.Warning;
    case 'F':
    default:
      return AxisErrorType.Fault;
  }
}

export type AxisErrorOrigin = 'movement' | 'connection' | 'warning_flag' | 'set_driver';

export interface AxisError {
  type: AxisErrorType;
  origin: AxisErrorOrigin;
  warningFlag?: string;
  message: string;
  title?: string;
}

/** Information about the devices travel capabilities. In NATIVE units */
export interface TravelInfo {
  min: number;
  max: number;
  cycleDistance: number | 'not-cyclic';
  distance: number;
  maxSpeed: number;
}

export function getDisplayPrecision(axis: IdentifiedAxisState) {
  return axis.movementDimensions?.position === getDimensionName(Angle.DEGREES) ? 6 : 3;
}
