import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

export enum ActionTypes {
  ROUTER_CONNECTED = 'CS_ROUTER_CONNECTED',
  ROUTER_DISCONNECTED = 'CS_ROUTER_DISCONNECTED',
  ROUTED_CONNECTION_CONNECTED = 'CS_ROUTED_CONNECTION_CONNECTED',
  ROUTED_CONNECTION_DISCONNECTED = 'CS_ROUTED_CONNECTION_DISCONNECTED',
  ROUTED_CONNECTION_DISCONNECT = 'CS_ROUTED_CONNECTION_DISCONNECT',
}

export interface ActionsToPayloads {
  [ActionTypes.ROUTER_CONNECTED]: { routerKey: EntityKey };
  [ActionTypes.ROUTER_DISCONNECTED]: { routerKey: EntityKey };
  [ActionTypes.ROUTED_CONNECTION_CONNECTED]: { connectionKey: EntityKey };
  [ActionTypes.ROUTED_CONNECTION_DISCONNECTED]: { connectionKey: EntityKey };
  [ActionTypes.ROUTED_CONNECTION_DISCONNECT]: { connectionKey: EntityKey };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  routerConnected: (routerKey: EntityKey) => buildAction(ActionTypes.ROUTER_CONNECTED, { routerKey }),
  routerDisconnected: (routerKey: EntityKey) => buildAction(ActionTypes.ROUTER_DISCONNECTED, { routerKey }),
  routedConnectionConnected: (connectionKey: EntityKey) => buildAction(ActionTypes.ROUTED_CONNECTION_CONNECTED, { connectionKey }),
  routedConnectionDisconnected: (connectionKey: EntityKey) => buildAction(ActionTypes.ROUTED_CONNECTION_DISCONNECTED, { connectionKey }),
  routedConnectionDisconnect: (connectionKey: EntityKey) => buildAction(ActionTypes.ROUTED_CONNECTION_DISCONNECT, { connectionKey }),
};
