import { ContextMenu, Icons, Text } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import { routerActions as routerActionsDefinition } from 'connected-react-router';
import React from 'react';
import { useSelector } from 'react-redux';

import type { DeviceInfo } from '../connection_manager';

import { selectDevicesWithHigherVersionsAvailable } from './selectors_external';
import { canUpgrade } from './utility';

export const FirmwareUpgradeContextMenuItem: React.FC<{ device: DeviceInfo }> = ({ device }) => {
  const routerActions = useActions(routerActionsDefinition);
  const upgradeAvailable = useSelector(selectDevicesWithHigherVersionsAvailable).includes(device.key);
  if (!canUpgrade(device)) {
    return null;
  }
  return (
    <ContextMenu.Item
      onClick={() => routerActions.push(`/firmware-upgrade?selected=${device.key}`)}
      title="Firmware Upgrade" icon={upgradeAvailable ? <Icons.UpgradeWithNotification/> : <Icons.Upgrade/>}>
      <Text>Firmware Upgrade</Text>
    </ContextMenu.Item>
  );
};
