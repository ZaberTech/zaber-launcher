import { filterDictionary } from '@zaber/toolbox';

import {  createReducer, changeDictionary } from '../utils';
import type { EntityKey } from '../keys';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { SupportedPeripheral, AxisMonitorInfo, MonitoringError } from './types';

export interface AxisState {
  key: EntityKey;
  monitorInfo: AxisMonitorInfo | null;
  monitorInfoErr: MonitoringError | null;
  commandInProgress: boolean;
  commandError: string | null;
  supportedPeripherals: SupportedPeripheral[] | null;
  fetchingSupportedPeripherals: boolean;
  fetchSupportedPeripheralsErr: string | null;
}

export interface State {
  axes: Record<EntityKey, AxisState>;
}

const initialState: State = {
  axes: {},
};

const didMount = (state: State, { axisKey }: ActionsToPayloads[ActionTypes.DID_MOUNT]): State =>
  ({
    ...state,
    axes: {
      ...state.axes,
      [axisKey]: {
        key: axisKey,
        commandError: null,
        commandInProgress: false,
        monitorInfo: null,
        monitorInfoErr: null,
        fetchingSupportedPeripherals: false,
        supportedPeripherals: null,
        fetchSupportedPeripheralsErr: null,
      },
    },
  });

const willUnmount = (state: State, { axisKey }: ActionsToPayloads[ActionTypes.WILL_UNMOUNT]): State =>
  ({
    ...state,
    axes: filterDictionary(state.axes, axis => axis.key !== axisKey),
  });


const axisMonitorData = (state: State, { axisKey, info, error }: ActionsToPayloads[ActionTypes.AXIS_MONITOR_DATA]): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axisKey, axis => ({ ...axis, monitorInfo: info ?? null, monitorInfoErr: error ?? null }))
  });

const axisCommand = (
  state: State,
  { axisKey }: ActionsToPayloads[ActionTypes.ACTIVATE] | ActionsToPayloads[ActionTypes.SET_PERIPHERAL_ID]
): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axisKey, axis => ({ ...axis, commandError: null, commandInProgress: true })),
  });

const axisCommandDone = (
  state: State,
  { axisKey, error }: ActionsToPayloads[ActionTypes.AXIS_COMMAND_DONE]
): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axisKey, axis => ({ ...axis, commandError: error ?? null, commandInProgress: false })),
  });

const fetchSupportedPeripherals = (
  state: State,
  { axisKey }: ActionsToPayloads[ActionTypes.FETCH_SUPPORTED_PERIPHERALS]
): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axisKey, { fetchingSupportedPeripherals: true, fetchSupportedPeripheralsErr: null }),
  });

const fetchSupportedPeripheralsDone = (
  state: State,
  { axisKey, error, peripherals }: ActionsToPayloads[ActionTypes.FETCH_SUPPORTED_PERIPHERALS_DONE]
): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axisKey, ({
      fetchSupportedPeripheralsErr: error,
      supportedPeripherals: peripherals,
      fetchingSupportedPeripherals: false,
    })),
  });

export const reducer = createReducer<ActionsToPayloads, State>({
  [ActionTypes.AXIS_MONITOR_DATA]: axisMonitorData,
  [ActionTypes.ACTIVATE]: axisCommand,
  [ActionTypes.SET_PERIPHERAL_ID]: axisCommand,
  [ActionTypes.AXIS_COMMAND_DONE]: axisCommandDone,
  [ActionTypes.DID_MOUNT]: didMount,
  [ActionTypes.WILL_UNMOUNT]: willUnmount,
  [ActionTypes.FETCH_SUPPORTED_PERIPHERALS]: fetchSupportedPeripherals,
  [ActionTypes.FETCH_SUPPORTED_PERIPHERALS_DONE]: fetchSupportedPeripheralsDone,
}, initialState);
