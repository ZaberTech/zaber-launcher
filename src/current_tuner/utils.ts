import type { ascii, Units } from '@zaber/motion';

import type { SettingDefinition, Setting } from './types';

export async function getSetting(axis: ascii.Axis, setting: SettingDefinition): Promise<number> {
  return axis.settings.get(setting.name, setting.units as Units | undefined);
}

export async function resolveInputSettings(axis: ascii.Axis, inputSettings: SettingDefinition[]): Promise<Setting[]> {
  const settings: Setting[] = [];
  for (const definition of inputSettings) {
    const value = await getSetting(axis, definition);
    settings.push({ ...definition, value });
  }
  return settings;
}
