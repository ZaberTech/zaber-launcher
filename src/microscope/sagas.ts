import { all, call, select, put, takeLatest, takeLeading, race, take, takeEvery, fork } from 'redux-saga/effects';
import { type SagaIterator } from 'redux-saga';
import { throwUnexpectedError } from '@zaber/toolbox';
import { ascii, microscopy, SettingNotFoundException, CommandFailedException } from '@zaber/motion';
import { duration } from 'moment';
import _ from 'lodash';

import { type SagaIter, type RT, type Action, delay, attachSagaCancel, compare } from '../utils';
import {
  ConnectionManagerActionTypes,
  deviceNameWithLabel,
  DevicesLoadedPayload,
  getConnection,
  getDevice,
} from '../connection_manager';
import { getDefaultDimensionUnits, getUnitLabelExceptNative } from '../units';
import { getContainer } from '../container';
import { Storage } from '../app_components';
import { EntityKey, makeAxisKey, makeDeviceKey } from '../keys';
import { Editions, environment } from '../environment';
import { ISettingsAccessor } from '../zml';

import { ActionTypes, ActionsToPayloads, actions } from './actions';
import {
  selectFactoryResettableDevices,
  selectMonitorBlocked,
  selectMonitorBursting, selectPositionUnits, selectPresetDataFromState,
  selectPresets, selectRefreshPending, selectSelectedKey, selectStoredPositions, selectedMounted,
} from './selectors';
import {
  AUTOFOCUS_STORAGE_KEY,
  AXES, AXIS_INVERTED_KEY, MONITOR_BURST_DELAY, MONITOR_DELAY, MonitorInfo, MonitorStableInfo,
  POSITION_DIMENSION,
  StoredPosition,
  TEST_END,
  SETTINGS_VALIDATORS,
  WDI_CURRENT_OBJECTIVE_REG,
  COMPONENT_NAMES,
  Components,
} from './types';
import { loadDetectableConfig, searchWdi, writeConfig } from './config/saga';
import { isPresetStructureSound, migratePreset, PRESETS_VERSION, type Preset } from './presets';
import {
  getFocusOffsets, getIndexPositionLabels, loadTriggerSettings, MicroscopeContext, operationStart, refreshMonitorAndWait,
  runInitialOperations,
} from './operation_sagas';
import { testScope } from './test_saga';
import { autofocusConfigFromUrl, getStorageBool } from './utils';
import { State } from './reducer';
import { multiDAcqSaga } from './multidacq/sagas';
import { applyPresetToScope } from './preset_sagas';
import { actions as appActions } from '../apps/actions';
import { AddConnectionActionTypes } from '../add_connection';

export function* rootSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.LOAD_CONFIG, loadDetectableConfig),
    takeLatest(ActionTypes.WRITE_CONFIG, writeConfig),
    takeLatest(ActionTypes.SEARCH_WDI, searchWdi),
    takeLatest(ActionTypes.TEST, testScope),
    takeLeading(ActionTypes.MOUNT, mount),
    takeEvery(ActionTypes.MOUNT, loadProductionMode),
    takeEvery(ActionTypes.PRESET_EDITOR_SAVE, savePresets),
    takeLatest(ActionTypes.SET_SELECTED_KEY, loadPresets),
    takeEvery(ActionTypes.SET_PRODUCTION_MODE, setProductionMode),
    takeLatest(ActionTypes.SET_SELECTED_KEY, loadStoredPositions),
    takeLatest([
      ActionTypes.UPDATE_STORED_POSITION,
      ActionTypes.DELETE_STORED_POSITION,
      ActionTypes.ADD_STORED_POSITION,
    ], saveStoredPositions),
    takeEvery(ActionTypes.FACTORY_RESET_SET_STEP, factoryReset),
    takeEvery(AddConnectionActionTypes.FIRST_DEVICES_LOAD, firstDevicesLoad),
  ]);
}

function* ensureAllIdentified(connectionKey: EntityKey, connection: ascii.Connection) {
  const devices: RT<typeof connection.detectDevices> = yield connection.detectDevices({ identifyDevices: false });
  for (const device of devices) {
    // delegate identification to connection manager
    yield getDevice(makeDeviceKey(connectionKey, device.deviceAddress));
  }
  return devices;
}

function* mount() {
  yield race({
    mainLoop: call(mainLoop),
    unmount: call(waitAfterUnmount),
  });
}

const STOP_MAIN_LOOP_AFTER_UNMOUNT = duration(1, 'hour').asMilliseconds();

function* waitAfterUnmount() {
  for (;;) {
    yield take(ActionTypes.UNMOUNT);
    const { expired } = yield race({
      remounted: take(ActionTypes.MOUNT),
      expired: !environment.isTest ? delay(STOP_MAIN_LOOP_AFTER_UNMOUNT) : take(TEST_END),
    });
    if (expired) {
      return;
    }
  }
}

const WAIT_AFTER_ERROR = duration(5, 'seconds').asMilliseconds();

function* mainLoop() {
  const restartEffects = {
    restartMonitor: take(ActionTypes.RESTART_MONITOR),
    newKey: take(ActionTypes.SET_SELECTED_KEY),
    reload: call(waitForDevicesReload),
  };

  while (true) {
    try {
      yield race({
        microscopeSaga: call(microscopeSaga),
        ...restartEffects,
      });
    } catch (err) {
      throwUnexpectedError(err);
      yield put(actions.monitorError(err.message));

      const mounted: RT<typeof selectedMounted> = yield select(selectedMounted);
      if (!mounted) { return }

      yield race({
        waitAfterError: delay(WAIT_AFTER_ERROR),
        ...restartEffects,
      });
    }
  }
}

function* waitForDevicesReload() {
  const connectionKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  while (true) {
    const loaded: Action<DevicesLoadedPayload> = yield take(ConnectionManagerActionTypes.DEVICES_LOADED);
    if (loaded.payload.connectionKey === connectionKey) {
      return;
    }
  }
}

function* connectAutofocus(connectionKey: EntityKey) {
  const storage = getContainer().get(Storage);
  const url = storage.load<string>(AUTOFOCUS_STORAGE_KEY + connectionKey);
  if (!url) { return null }

  const config = autofocusConfigFromUrl(url);
  if (!config) { return null }

  try {
    switch (config.provider) {
      case 'wdi': {
        const openPromise = microscopy.WdiAutofocusProvider.openTcp(
          config.host,
          config.port ?? undefined,
        );
        attachSagaCancel(openPromise, provider => provider.close());
        const provider: RT<typeof microscopy.WdiAutofocusProvider.openTcp> = yield openPromise;

        yield fork(wdiCloseSaga, provider);
        return provider;
      }
      default:
        throw new Error(`Unknown autofocus type: ${url}`);
    }
  } catch (err) {
    throwUnexpectedError(err);
    throw new Error(`Could not connect autofocus ${url}: ${err.message}`);
  }
}

function* wdiCloseSaga(provider: microscopy.WdiAutofocusProvider): SagaIter {
  try {
    yield new Promise(_.noop);
  } finally {
    yield provider.close();
  }
}

function* microscopeSaga(): SagaIter {
  const connectionKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  if (!connectionKey) {
    yield take(ActionTypes.SET_SELECTED_KEY);
    return;
  }

  const posUnits: RT<typeof selectPositionUnits> = yield select(selectPositionUnits);

  const autofocusProvider: RT<typeof connectAutofocus> = yield* connectAutofocus(connectionKey);

  const connection: RT<typeof getConnection> = yield call(getConnection, connectionKey);
  const devices = yield ensureAllIdentified(connectionKey, connection);
  const scope: RT<typeof microscopy.Microscope.find> = yield microscopy.Microscope.find(connection, {
    autofocus: autofocusProvider?.providerId,
  });

  const info: MonitorStableInfo = {
    filterChanger: null,
    objectiveChanger: null,
    illuminator: null,
    focusAxis: null,
    xAxis: null,
    yAxis: null,
    autofocus: null,
    cameraTrigger: null,
  };

  if (scope.filterChanger) {
    const changer = scope.filterChanger;
    const axis = changer.device.getAxis(1);
    const numberOf: RT<typeof changer.getNumberOfFilters> = yield changer.getNumberOfFilters();
    const labels: RT<typeof getIndexPositionLabels> = yield getIndexPositionLabels(axis, numberOf);
    let hasEncoder = false;
    try {
      hasEncoder = axis.settings.canConvertNativeUnits('encoder.1.pos');
    } catch (err) {
      if (!(err instanceof SettingNotFoundException)) {
        throw err;
      }
    }
    info.filterChanger = { numberOf, labels, hasEncoder };
  }

  if (scope.objectiveChanger) {
    const changer = scope.objectiveChanger;
    const numberOf: RT<typeof changer.getNumberOfObjectives> = yield changer.getNumberOfObjectives();
    const labels: RT<typeof getIndexPositionLabels> = yield getIndexPositionLabels(changer.turret.getAxis(1), numberOf);
    const focusOffsets: RT<typeof getFocusOffsets> = yield call(getFocusOffsets, changer.focusAxis, numberOf, posUnits.focusAxis);
    const focusDatum: RT<typeof changer.getFocusDatum> = yield changer.getFocusDatum();
    info.objectiveChanger = { numberOf, labels, focusOffsets, focusDatum };
  }

  if (scope.illuminator) {
    const { axisCount: channelCount, deviceAddress } = scope.illuminator.device;
    info.illuminator = { channels: {}, channelCount };
    const deviceKey = makeDeviceKey(connectionKey, deviceAddress);

    for (let channelNum = 1; channelNum <= channelCount; channelNum++) {
      const axis = scope.illuminator.device.getAxis(channelNum);
      if (axis.axisType !== ascii.AxisType.LAMP) { continue }
      const { peripheralName } = axis.identity;

      const channel = scope.illuminator.getChannel(channelNum);
      const wavelength: RT<typeof channel.settings.get> = yield channel.settings.get('lamp.wavelength.peak');
      const name = axis.label || (peripheralName.match(/^LED([^-]+)-/)?.[1] ?? peripheralName);
      info.illuminator.channels[channelNum] = {
        axisKey: makeAxisKey(deviceKey, channelNum),
        channel: channelNum,
        wavelength,
        name,
      };
    }
  }

  if (scope.cameraTrigger) {
    const { device, channel } = scope.cameraTrigger;
    info.cameraTrigger = {
      device: device.deviceAddress,
      channel,
    };
  }

  const defaultUnits = getDefaultDimensionUnits(POSITION_DIMENSION);
  for (const axisName of AXES) {
    const axis = scope[axisName];
    if (!axis) { continue }

    const nativeStepSize = axis.settings.convertFromNativeUnits('pos', 1, defaultUnits);
    const min: number = yield axis.settings.get('limit.min');
    const max: number = yield axis.settings.get('limit.max');
    const maxSpeed: number = yield axis.settings.get('maxspeed');
    const inverted = yield getStorageBool(axis, AXIS_INVERTED_KEY);
    info[axisName] = { nativeStepSize, min, max, maxSpeed, inverted };
  }

  if (scope.autofocus) {
    info.autofocus = { text: autofocusProvider!.toString() };
  }

  if (info.cameraTrigger) {
    yield loadTriggerSettings(scope);
  }

  yield put(actions.monitorStableInfo(info));

  const context: MicroscopeContext = { scope, devices, stableInfo: info, autofocusProvider  };

  yield all([
    takeEvery(ActionTypes.OPERATION_START, operationStart, context),
    fork(multiDAcqSaga, scope, info),
    takeLeading(ActionTypes.APPLY_PRESET, applyPreset, scope, info),
    fork(monitorLoop, context),
    fork(validateSettings, scope),
  ]);
}

export function* validateSettings(scope: microscopy.Microscope): SagaIter {
  const deviceGetters: Record<Components, () => ISettingsAccessor | undefined> = {
    focusAxis: () => scope.focusAxis?.settings,
    illuminator: () => scope.illuminator?.device.settings,
    xAxis: () => scope.xAxis?.settings,
    yAxis: () => scope.yAxis?.settings,
    objectiveChanger: () => scope.objectiveChanger?.turret.settings,
    filterChanger: () => scope.filterChanger?.device.settings,
  };

  for (const validator of SETTINGS_VALIDATORS) {
    try {
      const settingsAccess = deviceGetters[validator.component]?.();
      if (!settingsAccess) {
        continue;
      } else if (validator.precondition && !validator.precondition(scope)) {
        continue;
      }

      const value: number = yield call([settingsAccess, settingsAccess.get], validator.setting, validator.unit);
      const inRange = compare(value, validator.comparison, validator.value);
      if (!inRange) {
        yield put(actions.addValidationError(
          `${COMPONENT_NAMES[validator.component]} setting '${validator.setting}' ` +
          `is ${value} but should be ${validator.comparison} ` +
          `${validator.value}${getUnitLabelExceptNative(validator.unit)}.`));
      }
    } catch (err) {
      throwUnexpectedError(err);
      if ((err instanceof CommandFailedException) && (err.details.responseData === 'BADCOMMAND')) {
        continue;
      }

      yield put(actions.addValidationError(
        `Could not check ${COMPONENT_NAMES[validator.component]} setting '${validator.setting}': ${err.message}`));
    }
  }
}


function* monitorLoop(context: MicroscopeContext): SagaIter {
  yield runInitialOperations(context);

  const { scope, stableInfo } = context;

  while (true) {
    while (yield select(selectMonitorBlocked)) {
      yield take(ActionTypes.BLOCK_MONITOR);
    }

    yield put(actions.monitorRefreshStarted());

    const info: MonitorInfo = {
      needsInitialization: false,
      filterChanger: null,
      objectiveChanger: null,
      illuminator: null,
      xAxis: null,
      yAxis: null,
      focusAxis: null,
      autofocus: null,
    };

    info.needsInitialization = !(yield scope.isInitialized());

    if (scope.filterChanger) {
      const changer = scope.filterChanger;
      const filter: RT<typeof changer.getCurrentFilter> = yield changer.getCurrentFilter();
      info.filterChanger = { filter };
    }

    if (scope.objectiveChanger) {
      const changer = scope.objectiveChanger;
      const objective: RT<typeof changer.getCurrentObjective> = yield changer.getCurrentObjective();
      info.objectiveChanger = { objective };
    }

    if (scope.illuminator && stableInfo.illuminator) {
      info.illuminator = { channels: {} };
      for (const { channel } of Object.values(stableInfo.illuminator.channels)) {
        const channelObj = scope.illuminator.getChannel(channel);
        const on: RT<typeof channelObj.isOn> = yield channelObj.isOn();
        const intensity: RT<typeof channelObj.getIntensity> = yield channelObj.getIntensity();
        info.illuminator.channels[channel] = { channel, on, intensity };
      }
    }

    const positionUnits: RT<typeof selectPositionUnits> = yield select(selectPositionUnits);
    for (const axisName of AXES) {
      const axis = scope[axisName];
      if (axis) {
        const position: RT<typeof axis.getPosition> = yield axis.getPosition(positionUnits[axisName]);
        info[axisName] = { position: { value: position, units: positionUnits[axisName] } };
      }
    }

    info.autofocus = yield autofocusMonitorInfo(context, positionUnits);

    yield put(actions.monitorInfo(info));

    const refreshPending: RT<typeof selectRefreshPending> = yield select(selectRefreshPending);
    if (refreshPending) {
      continue;
    }

    const mounted: RT<typeof selectedMounted> = yield select(selectedMounted);
    if (mounted) {
      const burst: RT<typeof selectMonitorBursting> = yield select(selectMonitorBursting);
      yield race({
        refresh: take(ActionTypes.MONITOR_REFRESH),
        wait: delay(burst ? MONITOR_BURST_DELAY : MONITOR_DELAY),
      });
    } else {
      yield take(ActionTypes.MOUNT);
    }
  }
}

async function autofocusMonitorInfo(
  { scope, autofocusProvider }: MicroscopeContext,
  positionUnits: RT<typeof selectPositionUnits>,
): Promise<MonitorInfo['autofocus']> {
  if (!scope.autofocus || !autofocusProvider) { return null }

  const status = await scope.autofocus.getStatus();
  if (!status.inRange) {
    status.inFocus = false; // FW bug workaround
  }
  const providerStatus = await autofocusProvider.getStatus();
  const providerObjective = await autofocusProvider.genericRead(WDI_CURRENT_OBJECTIVE_REG, 4);

  const focusSettings = scope.autofocus.focusAxis.settings;
  const limits = await focusSettings.getMany(...[
    'motion.tracking.limit.min',
    'motion.tracking.limit.max'
  ].map(setting => ({ setting, unit: positionUnits.focusAxis })));
  const [min, max] = limits.map(({ value, unit }) => ({ value, units: unit }));

  return {
    inRange: status.inRange,
    inFocus: status.inFocus,
    laserOn: providerStatus.laserOn,
    limits: { min, max },
    providerObjective: providerObjective[0] + 1,
  };
}

export const PRESETS_STORAGE_KEY = 'microscopePresets_';
export interface StoredPresets {
  version: number;
  presets: Preset[];
}

function* savePresets(): SagaIter {
  const presets: RT<typeof selectPresets> = yield select(selectPresets);
  const selectedKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  if (!selectedKey) { return }

  const storage = getContainer().get<Storage>(Storage);
  storage.save(PRESETS_STORAGE_KEY + selectedKey, {
    presets,
    version: PRESETS_VERSION,
  } satisfies StoredPresets);
}

function* loadPresets(): SagaIter {
  const selectedKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  if (!selectedKey) { return }

  const storage = getContainer().get<Storage>(Storage);
  const stored = storage.load<StoredPresets>(PRESETS_STORAGE_KEY + selectedKey);
  if (stored == null || stored.version > PRESETS_VERSION) {
    return;
  }

  const presetData: RT<typeof selectPresetDataFromState> = yield select(selectPresetDataFromState);
  const presetStructTemplate: Preset = { name: '', data: presetData };
  const presets = stored.presets.filter((preset, i) => {
    migratePreset(preset, stored.version);

    const isSound = isPresetStructureSound(presetStructTemplate, preset);
    if (!isSound && environment.edition === Editions.Dev) {
      throw new Error(`Stored preset structure index ${i} is not structurally sound.`);
    }
    return isSound;
  });

  yield put(actions.presetsLoaded(presets));
}

function* applyPreset(
  scope: microscopy.Microscope,
  info: MonitorStableInfo,
  { payload: { preset: index } }: Action<ActionsToPayloads[ActionTypes.APPLY_PRESET]>,
): SagaIter {
  const presets: RT<typeof selectPresets> = yield select(selectPresets);
  const preset = presets[index];
  if (preset == null) { return }

  try {
    yield applyPresetToScope(scope, info, preset.data);
    yield refreshMonitorAndWait();
    yield put(actions.applyPresetDone(index));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.refreshMonitor());
    yield put(actions.applyPresetDone(index, err.message));
  }
}

export const PRODUCTION_MODE_STORAGE_KEY = 'microscopeProductionMode';
function* loadProductionMode(): SagaIter {
  const storage = getContainer().get(Storage);
  const production = storage.load<boolean>(PRODUCTION_MODE_STORAGE_KEY, false);
  yield put(actions.setProductionMode(production, false));
}

function setProductionMode({
  payload: { production, store },
}: Action<ActionsToPayloads[ActionTypes.SET_PRODUCTION_MODE]>) {
  if (!store) { return }

  const storage = getContainer().get(Storage);
  storage.save(PRODUCTION_MODE_STORAGE_KEY, production);
}

export const POSITIONS_STORAGE_KEY = 'microscopePositions_';
const STORED_POSITIONS_VERSION = 1;
export interface StoredPositions {
  version: number;
  positions: StoredPosition[];
}

export function* loadStoredPositions(): SagaIter {
  const selectedKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  if (!selectedKey) { return }

  const storage = getContainer().get(Storage);
  const stored = storage.load<StoredPositions>(POSITIONS_STORAGE_KEY + selectedKey);
  if (stored == null || stored.version !== STORED_POSITIONS_VERSION) { return }

  yield put(actions.loadStoredPositions(stored.positions));
}

export function* saveStoredPositions(): SagaIter {
  const selectedKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  if (!selectedKey) { return }

  const positions: RT<typeof selectStoredPositions> = yield select(selectStoredPositions);
  const storage = getContainer().get(Storage);
  storage.save(POSITIONS_STORAGE_KEY + selectedKey, {
    version: STORED_POSITIONS_VERSION,
    positions,
  } satisfies StoredPositions);
}


function* factoryReset({ payload: { step } }: Action<ActionsToPayloads[ActionTypes.FACTORY_RESET_SET_STEP]>): SagaIter {
  if (step !== 'perform') {
    return;
  }

  const devices = (yield select(selectFactoryResettableDevices)) as ReturnType<typeof selectFactoryResettableDevices>;
  let nextStep: State['factoryReset']['step'] = null;
  for (const deviceState of devices) {
    try {
      const device: RT<typeof getDevice> = yield getDevice(deviceState.key);
      yield device.restore();
    } catch (err) {
      nextStep = 'show-errors';
      yield put(actions.factoryResetAddError(`While resetting ${deviceNameWithLabel(deviceState)}: ${String(err)}`));
    }
  }

  yield put(actions.factoryResetSetStep(nextStep));
  yield put(actions.restartMonitor());
}

const MICROSCOPE_SPECIFIC_DEVICES = /^X-(FCR|MOR|LCA|ADR|ASR)/;
const PIN_MICROPSCOPE_APP_STORAGE_KEY = 'pinMicroscopeApp';

function* firstDevicesLoad({ payload: { devices } }: Action<DevicesLoadedPayload>): SagaIter {
  const isMicroscope = devices.filter(device =>
    device.identity?.name.match(MICROSCOPE_SPECIFIC_DEVICES)).length >= 2;
  if (!isMicroscope) { return }

  const storage = getContainer().get(Storage);
  const wasPinnedOnce = storage.load<boolean>(PIN_MICROPSCOPE_APP_STORAGE_KEY);
  if (wasPinnedOnce) { return } // only do it once to avoid annoying users

  yield put(appActions.pinApp('/microscope', true));
  storage.save(PIN_MICROPSCOPE_APP_STORAGE_KEY, true);
}
