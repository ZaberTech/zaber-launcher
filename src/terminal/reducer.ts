import _ from 'lodash';

import { EntityKey, extractConnectionKey, tryExtractConnectionKey } from '../keys';
import { createReducer, filterDictionary } from '../utils';
import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes, DevicesLoadedPayload } from '../connection_manager';

import { ActionTypes, ActionsToPayloads } from './actions';
import { Command, ConnectionStates, HistoryItem, CommandBatchReply, CommandBatchSettings } from './types';
import { splitAndCleanCommands } from './utils';

export const MAX_HISTORY_ITEMS = 2048;
export const MAX_TYPING_HISTORY = 50;

type HistoryItemWithKey = HistoryItem & { key: number };

export interface CommandBatch {
  state: 'edit' | 'closed' | 'executing' | 'done';
  text: string;
  commands: string[];
  replies: CommandBatchReply[][];
  error: string | null;
  settings: CommandBatchSettings;
}

export interface State {
  selectedKey: string | null;
  connectionState: ConnectionStates;
  nextItemKey: number;
  history: Record<string, HistoryItemWithKey[]>;
  commandHistory: string[];
  commandHistoryIndex: number | null;
  commandInput: string;
  selectedSuggestion: number | null;
  showExternal: boolean;
  commandBatch: CommandBatch;
  commandLists: Record<EntityKey, Command[]>;
}

const initialState: State = {
  selectedKey: null,
  connectionState: ConnectionStates.NotConnected,
  nextItemKey: 0,
  history: {},
  commandHistory: [],
  commandHistoryIndex: null,
  commandInput: '',
  selectedSuggestion: null,
  showExternal: true,
  commandBatch: {
    state: 'closed',
    text: '',
    commands: [],
    replies: [],
    error: null,
    settings: {
      waitUntilIdle: true,
      stopOnError: true,
    },
  },
  commandLists: {},
};

function limitHistory(historyItems: HistoryItemWithKey[]): HistoryItemWithKey[] {
  const toRemove = historyItems.length - MAX_HISTORY_ITEMS;
  if (toRemove <= 0) { return historyItems }

  let afterRemoval = historyItems.reduce((prev, current, i) => {
    const removed = i - prev.length;
    if (current.local || toRemove <= removed) {
      prev.push(current);
    }
    return prev;
  }, [] as HistoryItemWithKey[]);

  if (afterRemoval.length > MAX_HISTORY_ITEMS) {
    afterRemoval = afterRemoval.slice(-MAX_HISTORY_ITEMS);
  }
  return afterRemoval;
}

const selectEntity = (state: State, { key }: ActionsToPayloads[ActionTypes.SELECT_ENTITY]) => ({
  ...state,
  selectedKey: key,
  connectionState:
    tryExtractConnectionKey(key) !== tryExtractConnectionKey(state.selectedKey) ? ConnectionStates.NotConnected : state.connectionState,
  selectedSuggestion: null,
});

const appendToHistory = (state: State, { connection, messages }: ActionsToPayloads[ActionTypes.APPEND_TO_HISTORY]) => ({
  ...state,
  nextItemKey: state.nextItemKey + messages.length,
  history: {
    ...state.history,
    [connection]: limitHistory((state.history[connection] || []).concat(messages.map((message, i) => ({
      key: state.nextItemKey + i,
      ...message,
    })))),
  }
});

const clearConnectionHistory = (state: State, { connection }: ActionsToPayloads[ActionTypes.CLEAR_CONNECTION_HISTORY]) => ({
  ...state,
  history: {
    ...state.history,
    [connection]: [],
  }
});

const setConnectionState = (state: State, { connection, newState }: ActionsToPayloads[ActionTypes.SET_CONNECTION_STATE]) =>
  tryExtractConnectionKey(state.selectedKey) === connection ? ({
    ...state,
    connectionState: newState,
  }) : state;

const setShowExternal = (state: State, { showExternal }: ActionsToPayloads[ActionTypes.SET_SHOW_EXTERNAL]) => ({
  ...state,
  showExternal,
});

const setCommand = (state: State, { input }: ActionsToPayloads[ActionTypes.SET_COMMAND_INPUT]) => ({
  ...state,
  commandHistoryIndex: null,
  selectedSuggestion: null,
  commandInput: input,
});

const setCommandHistoryOlder = (state: State) => {
  const newIndex = state.commandHistoryIndex == null ? state.commandHistory.length - 1 : state.commandHistoryIndex - 1;
  if (newIndex < 0) {
    return state;
  }
  return ({
    ...state,
    commandHistoryIndex: newIndex,
    selectedSuggestion: null,
  });
};

const setCommandHistoryNewer = (state: State) => {
  if (state.commandHistoryIndex == null) {
    return state;
  }
  const newIndex = state.commandHistoryIndex + 1;
  return ({
    ...state,
    commandHistoryIndex: newIndex < state.commandHistory.length ? newIndex : null,
    selectedSuggestion: null,
  });
};

const setSelectedSuggestion = (state: State, { suggestion }: ActionsToPayloads[ActionTypes.SET_SELECTED_SUGGESTION]) => ({
  ...state,
  selectedSuggestion: suggestion,
});

const sendCommand = (state: State, { command }: ActionsToPayloads[ActionTypes.SEND_COMMAND]): State => {
  if (!command || _.last(state.commandHistory) === command) {
    return state;
  }
  const commandHistory = [...state.commandHistory, command];
  if (commandHistory.length > MAX_TYPING_HISTORY) {
    commandHistory.shift();
  }
  return ({
    ...state,
    commandHistory,
  });
};

const commandBatchEdit = (state: State, { text }: ActionsToPayloads[ActionTypes.COMMAND_BATCH_EDIT]): State => ({
  ...state,
  commandBatch: {
    ...state.commandBatch,
    text: text ?? state.commandBatch.text,
    state: 'edit',
  },
});

const commandBatchClose = (state: State): State => ({
  ...state,
  commandBatch: {
    ...initialState.commandBatch,
    settings: state.commandBatch.settings,
  },
});

const commandBatchExecute = (state: State): State => ({
  ...state,
  commandBatch: {
    ...state.commandBatch,
    state: 'executing',
    commands: splitAndCleanCommands(state.commandBatch.text),
    replies: [],
    error: null,
  },
});

const commandBatchReply = (state: State, { replies }: ActionsToPayloads[ActionTypes.COMMAND_BATCH_REPLY]): State => ({
  ...state,
  commandBatch: {
    ...state.commandBatch,
    replies: [...state.commandBatch.replies, replies],
  },
});

const commandBatchDone = (state: State, { error }: ActionsToPayloads[ActionTypes.COMMAND_BATCH_DONE]): State => ({
  ...state,
  commandBatch: {
    ...state.commandBatch,
    state: 'done',
    error: error ?? null,
  },
});

const commandBatchStop = (state: State): State => ({
  ...state,
  commandBatch: {
    ...state.commandBatch,
    state: 'done',
  },
});

const commandBatchSetSettings = (state: State, settings: ActionsToPayloads[ActionTypes.COMMAND_BATCH_SET_SETTINGS]): State => ({
  ...state,
  commandBatch: {
    ...state.commandBatch,
    settings: {
      ...state.commandBatch.settings,
      ...settings,
    },
  },
});

const storeCommandList = (state: State, { entityKey, commandList }: ActionsToPayloads[ActionTypes.STORE_COMMAND_LIST]): State => {
  const { commandLists } = state;
  return {
    ...state,
    commandLists: {
      ...commandLists,
      [entityKey]: commandList,
    }
  };
};

const devicesLoaded = (state: State, { connectionKey }: DevicesLoadedPayload): State => {
  const { commandLists } = state;
  return {
    ...state,
    commandLists: filterDictionary(commandLists, (value, key) => key != null && extractConnectionKey(key) !== connectionKey)
  };
};

type Payloads = ActionsToPayloads & ConnectionManagerActionPayloads;
export const reducer = createReducer<Payloads, typeof initialState>({
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
  [ActionTypes.SELECT_ENTITY]: selectEntity,
  [ActionTypes.SET_CONNECTION_STATE]: setConnectionState,
  [ActionTypes.APPEND_TO_HISTORY]: appendToHistory,
  [ActionTypes.CLEAR_CONNECTION_HISTORY]: clearConnectionHistory,
  [ActionTypes.SET_SHOW_EXTERNAL]: setShowExternal,
  [ActionTypes.SET_COMMAND_INPUT]: setCommand,
  [ActionTypes.SET_COMMAND_HISTORY_OLDER]: setCommandHistoryOlder,
  [ActionTypes.SET_COMMAND_HISTORY_NEWER]: setCommandHistoryNewer,
  [ActionTypes.SET_SELECTED_SUGGESTION]: setSelectedSuggestion,
  [ActionTypes.SEND_COMMAND]: sendCommand,
  [ActionTypes.COMMAND_BATCH_EDIT]: commandBatchEdit,
  [ActionTypes.COMMAND_BATCH_CLOSE]: commandBatchClose,
  [ActionTypes.COMMAND_BATCH_STOP]: commandBatchStop,
  [ActionTypes.COMMAND_BATCH_EXECUTE]: commandBatchExecute,
  [ActionTypes.COMMAND_BATCH_REPLY]: commandBatchReply,
  [ActionTypes.COMMAND_BATCH_DONE]: commandBatchDone,
  [ActionTypes.COMMAND_BATCH_SET_SETTINGS]: commandBatchSetSettings,
  [ActionTypes.STORE_COMMAND_LIST]: storeCommandList,
}, initialState);
