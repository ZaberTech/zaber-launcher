import React, { useState } from 'react';
import { Chevron, Text } from '@zaber/react-library';

interface Props {
  title?: string;
  titleExpanded?: string;
  titleCollapsed?: string;
}

export const ExpandableSection: React.FC<Props> = ({ title, titleExpanded, titleCollapsed, children }) => {
  const [isExpanded, setIsExpanded] = useState(false);

  return (
    <>
      <div className="expandable-section" onClick={() => setIsExpanded(!isExpanded)}>
        <Chevron
          expanded={isExpanded}
          titleExpanded={titleExpanded}
          titleCollapsed={titleCollapsed}/>
        <Text t={Text.Type.H4}>
          {title}
        </Text>
      </div>
      {isExpanded && children}
    </>
  );
};
