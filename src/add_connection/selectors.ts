import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectAddConnection } from '../store';
import { Connection, ConnectionType, isConnectionSerialPort } from '../app_components';
import { selectLocalConnections as selectExistingConnections } from '../connection_manager/selectors';
import { Editions, environment } from '../environment';


export const selectAddingConnection = createSelector(selectAddConnection, state => state.addingConnection);
export const selectAddingConnectionError = createSelector(selectAddConnection, state => state.addConnectionError);
export const selectConnectionsBeingRemoved = createSelector(selectAddConnection, state => state.connectionsBeingRemoved);
export const selectSerialPort = createSelector(selectAddConnection, state => state.serialPort);
export const selectPorts = createSelector(selectSerialPort, state => state.ports);
export const selectPortsWithInfo = createSelector(selectSerialPort, selectExistingConnections, (config, existingConnections) => {
  const allPortsWithInfo = config.ports?.map(port => ({
    name: port,
    exists: _.some(existingConnections, connection => isConnectionSerialPort(connection) && connection.config.serialPort === port),
  }));
  if (environment.platform === 'darwin' && config.showOnlyRecommendedPorts) {
    return allPortsWithInfo?.filter(port => /^\/dev\/tty.*/.test(port.name) && !/(blth|bluetooth)/ig.test(port.name));
  }
  return allPortsWithInfo;
});
export const selectDialogOpen = createSelector(selectAddConnection, state => state.dialogOpen);
export const selectStep = createSelector(selectAddConnection, state => state.step);
export const selectTcpConfig = createSelector(selectAddConnection, ({ tcp }) => (tcp));

export const selectDetectDevices = createSelector(selectAddConnection, state => state.detectDevices);

export const selectCloud = createSelector(selectAddConnection, state => state.connectCloud);

export const selectLocalShare = createSelector(selectAddConnection, state => state.localShare);

export const selectConfiguration = createSelector(selectAddConnection, (state): Connection => ({
  id: 'dummy',
  type: state.connectionType ?? ConnectionType.SERIAL_PORT,
  config: state.connectionType === ConnectionType.TCP_IP ? {
    hostname: state.tcp.hostname,
    name: '',
    tcpPort: state.tcp.port!,
  } : {
    serialPort: state.serialPort.selectedPort!,
    name: '',
    baudRate: state.serialPort.baudRate,
  },
}));

export const selectShowSimulatorNotice = createSelector(selectAddConnection, state =>
  [Editions.Dev, Editions.Internal].includes(environment.edition) && !state.hideSimulatorNotice
);

export const selectLastAddedConnection = createSelector(selectAddConnection, state => state.lastAddedConnection);
