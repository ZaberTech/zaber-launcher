import { call, select, put, race } from 'redux-saga/effects';
import { throwUnexpectedError } from '@zaber/toolbox';
import _ from 'lodash';
import { Frequency, Length, NotSupportedException, Time, Velocity, Voltage, ascii } from '@zaber/motion';
import { P } from 'ts-pattern';
import snmp from 'net-snmp';

import { SagaIter, RT, isMatch } from '../../utils';
import { getAxis, getConnection, getDeviceOrAxis } from '../../connection_manager';
import { makeAxisKey, makeDeviceKey } from '../../keys';
import { actions } from '../actions';
import { selectSelectedKey } from '../selectors';
import {
  ALT_PART_KEY_PREFIX,
  AUTOFOCUS_STORAGE_KEY,
  AXIS_INVERTED_KEY,
  DetectableConfig,
  MicroscopeConfig,
  PART_KEY,
} from '../types';
import { autofocusConfigFromUrl, autofocusConfigToUrl, getStorageBool } from '../utils';
import { getContainer } from '../../container';
import { Storage } from '../../app_components';

import { selectEditedConfig, selectObjectiveChanger } from './selectors';

function makeAltPartKey(part: keyof DetectableConfig): string {
  return `${ALT_PART_KEY_PREFIX}${_.snakeCase(part)}`;
}

function getAltPartKey(key: string): string {
  if (!key.startsWith(ALT_PART_KEY_PREFIX)) { return '' }
  return key.substring(ALT_PART_KEY_PREFIX.length).split('.')[0];
}

export function* loadDetectableConfig(): SagaIter {
  const connectionKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  if (!connectionKey) { throw new Error('No connection') }
  try {
    const connection: RT<typeof getConnection> = yield call(getConnection, connectionKey);
    const partResps: RT<typeof connection.genericCommandMultiResponse> =
      yield connection.genericCommandMultiResponse(`storage all print prefix ${PART_KEY}`, { checkErrors: false });
    const parts = partResps.filter(resp =>
      resp.messageType === ascii.MessageType.INFO && resp.data.startsWith(`set ${PART_KEY}`)
    ).map(resp => {
      const data = resp.data.split(/\s+/);

      let part: string;
      let dataStart: number;
      if (data[1] === PART_KEY) {
        part = data[2];
        dataStart = 3;
      } else {
        part = getAltPartKey(data[1]);
        dataStart = 2;
      }
      const parameters = Object.fromEntries(data.slice(dataStart).map(param => param.split('='))) as Record<string, string>;

      return ({
        part: _.camelCase(part) as keyof DetectableConfig,
        device: resp.deviceAddress,
        axis: resp.axisNumber,
        parameters,
      });
    });

    const config: MicroscopeConfig = {
      filterChanger: null,
      x: null,
      y: null,
      focus: null,
      autofocus: null,
      cameraTrigger: null,
    };

    for (const part of parts) {
      const deviceKey = makeDeviceKey(connectionKey, part.device);
      const axisKey = makeAxisKey(deviceKey, part.axis);
      const { parameters, part: partKey } = part;
      switch (partKey) {
        case 'filterChanger':
          if (config[partKey] != null) {
            config[partKey] = { error: 'Multiple tags found' };
          } else {
            config[partKey] = { key: deviceKey };
          }
          break;
        case 'x':
        case 'y':
        case 'focus':
          if (config[partKey] != null) {
            config[partKey] = { error: 'Multiple tags found' };
          } else if (part.axis === 0) {
            config[partKey] = { error: 'Device tagged instead of axis' };
          } else {
            config[partKey] = { key: axisKey, inverted: false };
          }
          break;
        case 'cameraTrigger':
          if (config[partKey] != null) {
            config[partKey] = { error: 'Multiple tags found' };
          } else {
            const digitalOutput = Number.isInteger(+parameters.do) ? +parameters.do : null;
            config[partKey] = {
              key: deviceKey,
              digitalOutput,
            };
          }
          break;
      }
    }

    for (const axisName of ['x', 'y', 'focus'] as const) {
      const axisConfig = config[axisName];
      if (!isMatch({ key: P.string }, axisConfig)) { continue }

      const axis: RT<typeof getAxis> = yield getAxis(axisConfig.key);
      axisConfig.inverted = yield getStorageBool(axis, AXIS_INVERTED_KEY);
    }

    const storage = getContainer().get<Storage>(Storage);
    const autofocusUrl = storage.load<string>(AUTOFOCUS_STORAGE_KEY + connectionKey);
    if (autofocusUrl) {
      config.autofocus = autofocusConfigFromUrl(autofocusUrl);
    }

    yield put(actions.loadConfigDone(config));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.loadConfigError(err.message));
  }
}

export function* writeConfig(): SagaIter {
  const editedConfig: RT<typeof selectEditedConfig> = yield select(selectEditedConfig);
  const connectionKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  if (!connectionKey) { throw new Error('No connection selected') }
  try {
    const connection: RT<typeof getConnection> = yield call(getConnection, connectionKey);
    yield deleteStoragePartKeys(connection);

    const keys: (keyof DetectableConfig)[] = ['filterChanger', 'x', 'y', 'focus'];
    for (const key of keys) {
      const config = editedConfig[key];
      if (!isMatch({ key: P.string }, config)) { continue }

      const deviceOrAxis: RT<typeof getDeviceOrAxis> = yield getDeviceOrAxis(config.key);
      yield deviceOrAxis.storage.setString(PART_KEY, _.snakeCase(key));

      if (isMatch({ inverted: P.boolean }, config)) {
        if (config.inverted) {
          yield deviceOrAxis.storage.setBool(AXIS_INVERTED_KEY, true);
        } else {
          yield deviceOrAxis.storage.eraseKey(AXIS_INVERTED_KEY);
        }
      }
    }

    if (isMatch({ key: P.string }, editedConfig.cameraTrigger)) {
      const { cameraTrigger } = editedConfig;
      const device: RT<typeof getDeviceOrAxis> = yield getDeviceOrAxis(cameraTrigger.key);
      yield device.storage.setString(makeAltPartKey('cameraTrigger'), `do=${cameraTrigger.digitalOutput}`);
    }

    yield objectiveChangerConfig(editedConfig);
    yield xyAxesConfig(editedConfig, 'x');
    yield xyAxesConfig(editedConfig, 'y');
    yield autofocusConfig(editedConfig);

    const storage = getContainer().get<Storage>(Storage);
    if (editedConfig.autofocus) {
      storage.save(AUTOFOCUS_STORAGE_KEY + connectionKey, autofocusConfigToUrl(editedConfig.autofocus));
    } else {
      storage.save(AUTOFOCUS_STORAGE_KEY + connectionKey, null);
    }

    yield put(actions.writeConfigDone());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.writeConfigDone(err.message));
  }

  yield put(actions.setSelectedKey(connectionKey));
  yield put(actions.loadConfig());
}

function* deleteStoragePartKeys(connection: ascii.Connection): SagaIter  {
  yield connection.genericCommandMultiResponse(`storage all erase ${PART_KEY}`, { checkErrors: false });

  const altPartKeys: RT<typeof connection.genericCommandMultiResponse> = yield connection.genericCommandMultiResponse(
    `storage all print keys prefix ${ALT_PART_KEY_PREFIX}`, { checkErrors: false });
  for (const reply of altPartKeys.filter(reply => reply.messageType === ascii.MessageType.INFO)) {
    const parts = reply.data.split(/\s+/);
    if (parts[0] !== 'set') { continue }
    yield connection.genericCommand(`storage all erase ${parts[1]}`, { device: reply.deviceAddress });
  }
}

function* objectiveChangerConfig(config: DetectableConfig): SagaIter {
  const objectiveChanger: RT<typeof selectObjectiveChanger> = yield select(selectObjectiveChanger);
  if (!isMatch({ key: P.string }, objectiveChanger)) { return }

  if (!isMatch({ key: P.string }, config.focus)) { return }
  const axis: RT<typeof getAxis> = yield getAxis(config.focus.key);
  if (axis.device.identity.name.startsWith('X-LDA')) {
    yield axis.settings.set('limit.home.offset', 15, Length.mm);
    yield axis.settings.set('maxspeed', 50, Velocity['mm/s']);
    try {
      yield axis.settings.set('limit.hardstop.retraction', 1, Length.mm);
    } catch (err) {
      if (!(err instanceof NotSupportedException)) { throw err }
    }
  }
}

function* autofocusConfig(config: MicroscopeConfig): SagaIter {
  if (!isMatch({ key: P.string }, config.focus) || config.autofocus == null) { return }

  if (config.autofocus.provider === 'wdi') {
    const axis: RT<typeof getAxis> = yield getAxis(config.focus.key);
    const settings = axis.settings;

    const toApply = {
      'maxspeed': [50, Velocity['mm/s']],
      'cloop.settle.period': [50, Time.ms],
      'motion.tracking.ai': 1,
      'motion.tracking.signal.valid.di': 1,
      'motion.tracking.settle.period': [50, Time.ms],
      'motion.tracking.settle.tolerance': [0.04, Voltage.V],
      'motion.tracking.scan.maxspeed': [1, Velocity['mm/s']],
      'motion.tracking.scan.period': [1, Time.ms],
      'motion.tracking.scan.dir': 0,
      'motion.tracking.scan.signal.valid.delay': [5, Time.ms],
      'motion.tracking.scan.tolerance': [1, Voltage.V],
    } as const;

    for (const [key, value] of Object.entries(toApply)) {
      if (typeof value === 'number') {
        yield settings.set(key, value);
      } else {
        yield settings.set(key, value[0], value[1]);
      }
    }

    const toApplyDevice = {
      'io.ai.1.fc': [100, Frequency.Hz],
    } as const;

    const settingsDevice = axis.device.settings;
    for (const [key, value] of Object.entries(toApplyDevice)) {
      yield settingsDevice.set(key, value[0], value[1]);
    }
  }
}

function* xyAxesConfig(config: DetectableConfig, axisKey: 'x' | 'y'): SagaIter {
  const axisConfig = config[axisKey];
  if (!isMatch({ key: P.string }, axisConfig)) { return }

  const axis: RT<typeof getAxis> = yield getAxis(axisConfig.key);
  if (axis.identity.peripheralName.startsWith('ASR')) {
    const RAMPTIME_MS = 50;
    if (axis.settings.canConvertNativeUnits('motion.accel.ramptime')) {
      yield axis.settings.set('motion.accel.ramptime', RAMPTIME_MS, Time.ms);
    } else {
      yield axis.settings.set('motion.accel.ramptime', RAMPTIME_MS); // units only since 7.35, in ms
    }
  } else if (axis.identity.peripheralName.startsWith('ADR')) {
    yield setSimpleTuningADR(axis);
  }
}

async function setSimpleTuningADR(axis: ascii.Axis) {
  const tuner = new ascii.ServoTuner(axis);

  const paramList = await tuner.getSimpleTuningParamDefinitions();
  const allParams = paramList.map(param => param.name).join();
  if (allParams !== 'Stiffness') {
    throw new Error(`Unexpected ADR tuning parameters: ${allParams}`);
  }
  const loadMass = 0;
  const setParams = [{ name: 'Stiffness', value: 0 }];
  await tuner.setSimpleTuning(ascii.ServoTuningParamset.P_1, setParams, loadMass);
  await tuner.setStartupParamset(ascii.ServoTuningParamset.P_1);
  await tuner.loadParamset(ascii.ServoTuningParamset.LIVE, ascii.ServoTuningParamset.P_1);
}

export function* searchWdi() {
  // 57416=WDI Wise Device Inc.
  const IP_OID = '1.3.6.1.4.1.57416.780721.1.4.1.2.9.105.112.97.100.100.114.101.115.115.1';
  const LINK_LOCAL_BROADCAST = '169.254.255.255';

  try {
    const session = snmp.createSession(LINK_LOCAL_BROADCAST, 'public', {
      version: snmp.Version2c,
    });
    try {
      yield race([
        call(function* (): SagaIter {
          yield new Promise<void>((resolve, reject) => {
            session.on('error', reject);
            session.on('close', resolve);
          });
        }),
        call(function* (): SagaIter {
          const ip: string = yield new Promise<string>((resolve, reject) =>
            session.get([IP_OID], (err, varbinds) => {
              if (err) {
                return reject(err);
              } else if (varbinds.length < 1) {
                return reject(new Error('No response'));
              }
              const ip = varbinds[0].value.toString();
              resolve(ip);
            }));
          yield put(actions.searchWdiDone({ host: ip }));
        }),
      ]);
    } finally {
      session.close();
    }
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.searchWdiDone({ error: err.message }));
  }
}
