import _ from 'lodash';
import { createSelector } from 'reselect';
import { notNil, tryAccess } from '@zaber/toolbox';

import { selectDemo } from '../store';
import {
  ConnectionManagerAxisState, ConnectionManagerDeviceState, selectAxesKeysInDisplayOrder, selectConnections,
  ConnectionManagerConnectionState,
  LockstepGroup,
  selectIdentifiedDevices,
  selectIdentifiedAxes,
} from '../connection_manager';
import { extractConnectionKey, extractDeviceKey } from '../keys';
import { isMobile } from '../devices';

import { AxisState, defaultAxisState } from './reducer';

export interface AxisStateComplete extends ConnectionManagerAxisState, AxisState {
  device: ConnectionManagerDeviceState;
  connection: ConnectionManagerConnectionState;
  lockstep: LockstepGroup | null;
}

function isPrimary(axis: AxisStateComplete) {
  return axis.lockstep == null || axis.lockstep.axisNumbers[0] === axis.axisNumber;
}

export const selectAxes = createSelector(selectDemo, selectIdentifiedAxes, selectIdentifiedDevices, selectConnections,
  (state, axes, devices, connections) => _.chain(axes)
    .mapValues((axis): AxisStateComplete => {
      const device = devices[extractDeviceKey(axis.key)];
      const connection = connections[extractConnectionKey(axis.key)];
      return {
        device,
        connection,
        lockstep: device.locksteps?.find(lockstep => lockstep.axisNumbers.includes(axis.axisNumber)) ?? null,
        ...axis,
        ...(tryAccess(state.axes, axis.key) ??  defaultAxisState),
      };
    }).pickBy(axis => isMobile(axis) && isPrimary(axis))
    .value());

export const selectAxesInOrder = createSelector(selectAxesKeysInDisplayOrder, selectAxes,
  (keys, axes) => keys.map(key => axes[key]).filter(notNil));

export const selectCounts = createSelector(selectAxes, axes => ({
  running: _.filter(axes, axis => axis.running).length,
  stopped: _.filter(axes, axis => !axis.running).length,
}));

export const selectDemoCode = createSelector(selectDemo, state => state.demoCode);
