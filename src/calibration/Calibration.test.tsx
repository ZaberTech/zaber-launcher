/* eslint-disable @typescript-eslint/no-require-imports */
/* eslint-disable max-len */
import React from 'react';
import _ from 'lodash';
import { RenderResult, fireEvent, render } from '@testing-library/react';
import { injectable } from 'inversify';

import {
  CommandFailedException, CommandFailedExceptionData,
} from '@zaber/motion';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { MOCK_DEVICE_NUMBER, MOCK_SERIAL_PORT, mockSingleDevice } from '../connection_manager/mocks';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';

jest.mock('./Chart', () => ({
  PopulatedChart: () => <div>PopulatedChart</div>,
}));

const CSV_CONTENT = `
Data Version,Time Stamp,Serial Number,Calibration Type,Table Width,Table Offset,Description,${_.range(64).map(i => `Data_${i + 1}`).join(',')}
1,2023-08-22 12:15:11 PM,104016,2,32,12,Comment,${_.range(64).map(i => i).reverse().join(',')}
`.trimStart();

jest.mock('fs/promises', () => ({
  readFile: jest.fn(() => Promise.resolve(CSV_CONTENT)),
  writeFile: jest.fn(() => Promise.resolve()),
}));

jest.mock('../dialogs', () => ({
  Dialogs: {
    showSaveDialog: jest.fn(() => Promise.resolve({ filePath: 'test.csv', canceled: false })),
    showOpenDialog: jest.fn(() => Promise.resolve({ filePaths: ['test.csv'], canceled: false })),
  },
}));

import { Calibration } from './Calibration';
import { makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';
import { VALUE_COUNT } from './types';
import { parseCsv } from '../pvt/csv';

function badCmd() {
  return new CommandFailedException('Rejected', {
    replyFlag: 'RJ',
    responseData: 'BADCOMMAND',
  } as CommandFailedExceptionData);
}

let axes: AxisMock[];
let axisCallback: (axis: AxisMock) => void;

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);

    axes.push(this);
    if (axisCallback) { axisCallback(this) }
  }

  calibrationLeft = 0;

  genericCommand = jest.fn<Promise<{ data: string }>, [string]>(async (cmd: string) => {
    if ([
      'calibration delete',
      'factory calibration delete',
      'factory calibration save',
    ].includes(cmd)) {
      return { data: '0' };
    } else if (cmd.startsWith('calibration load start')) {
      this.calibrationLeft = VALUE_COUNT;
      return { data: `${this.calibrationLeft}` };
    } else if (cmd.startsWith('calibration load')) {
      if (this.calibrationLeft <= 0) {
        throw badCmd();
      }
      this.calibrationLeft -= 4;
      return { data: `${this.calibrationLeft}` };
    } else {
      throw badCmd();
    }
  });
  genericCommandMultiResponse = jest.fn<Promise<{ data: string }[]>, [string]>(async (cmd: string) => {
    switch (cmd) {
      case 'calibration print':
        return [
          { data: '1 16 0' },
          ..._.range(16)
            .map(i => i * 4)
            .map(i => ({ data: `data ${i} ${i + 1} ${i + 2} ${i + 3}` })),
        ];
      default:
        throw badCmd();
    }
  });
}

let devices: DeviceMock[];
class DeviceMock extends DeviceMockBase<AxisMock> {
  identity = { };

  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    devices.push(this);
  }
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const connectionKey = makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), MOCK_SERIAL_PORT);
const deviceKey = makeDeviceKey(connectionKey, MOCK_DEVICE_NUMBER);

const TestApp = wrapWithNewStore(wrapWithRouter(Calibration));

let wrapper: RenderResult;

beforeEach(() => {
  devices = [];
  axes = [];
  axisCallback = null!;

  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
});

afterEach(() => {
  wrapper?.unmount();
  wrapper = null!;

  destroyContainer();

  axisCallback = null!;
});

describe('integrated device', () => {
  beforeEach(() => {
    wrapper = render(<TestApp/>);
    mockSingleDevice(TestApp.testStore);
  });

  test('loads calibration table when selected', async () => {
    wrapper.getByText(/Select device or axis to calibrate/);
    connectionViewMockInstance.props.onSelect(deviceKey);

    await waitUntilPass(() => wrapper.getByText(/Calibration Type/));

    expect(wrapper.getAllByRole('spinbutton')
      .map(input => (input as HTMLInputElement).value))
      .toEqual(['1', '16', '0']);
    expect((wrapper.getByRole('textbox') as HTMLTextAreaElement).value).toMatch(/^\s+0\s+1\s+2\s+3/);
  });

  test('handles error loading calibration table', async () => {
    axisCallback = axis => axis.genericCommandMultiResponse.mockRejectedValueOnce(badCmd());
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => wrapper.getByText(/Rejected/));
  });

  describe('with calibration table', () => {
    beforeEach(async () => {
      connectionViewMockInstance.props.onSelect(deviceKey);
      await waitUntilPass(() => wrapper.getByText(/Calibration Type/));
    });

    test('deletes table', async () => {
      fireEvent.click(wrapper.getByText('Delete Calibration'));
      expect(wrapper.getByText('Delete Calibration')).toBeDisabled();
      await waitUntilPass(() => expect(wrapper.getByText('Delete Calibration')).not.toBeDisabled());

      expect(axes[0].genericCommand.mock.calls.slice(-2).map(([cmd]) => cmd)).toEqual([
        'calibration delete',
        'factory calibration delete',
      ]);
    });

    test('applies table', async () => {
      let i = 1;
      for (const input of wrapper.getAllByRole('spinbutton')) {
        fireEvent.change(input, { target: { value: `${i++ * 2}` } });
      }
      fireEvent.change(wrapper.getByRole('textbox'), { target: { value: _.reverse(_.range(64)).join(' ') } });

      const button = wrapper.getByText(/Apply/).parentElement!;
      fireEvent.click(button);
      expect(button).toBeDisabled();
      await waitUntilPass(() => expect(button).not.toBeDisabled());

      expect(axes[0].genericCommand.mock.calls.slice(0, 3).map(([cmd]) => cmd)).toEqual([
        'calibration delete',
        'calibration load start 2 4 6',
        'calibration load 63 62 61 60',
      ]);
      expect(axes[0].genericCommand.mock.calls.slice(-3).map(([cmd]) => cmd)).toEqual([
        'calibration load 3 2 1 0',
        'factory calibration delete',
        'factory calibration save',
      ]);
    });

    test('handles apply error', async () => {
      axes[0].genericCommand.mockRejectedValueOnce(badCmd());
      fireEvent.change(wrapper.getAllByRole('spinbutton')[0], { target: { value: '-1' } });
      fireEvent.click(wrapper.getByText(/Apply/));
      await waitUntilPass(() => wrapper.getByText(/Rejected/));
    });

    test('validates values', async () => {
      const valueBefore = (wrapper.getByRole('textbox') as HTMLTextAreaElement).value;
      fireEvent.change(wrapper.getByRole('textbox'), { target: { value: `${valueBefore}z` } });
      wrapper.getByText('Value at index 63 is invalid.');
      fireEvent.change(wrapper.getByRole('textbox'), { target: { value: `${valueBefore} 64` } });
      wrapper.getByText('Expected 64 values, got 65.');
      expect(wrapper.getByText(/Apply/).parentElement).toBeDisabled();
    });

    test('saves table to file', async () => {
      const fs = require('fs/promises');
      fireEvent.click(wrapper.getByText('Save to File'));
      await waitUntilPass(() => expect(fs.writeFile).toBeCalledTimes(1));

      const args = (fs.writeFile as jest.MockedFunction<(...args: string[]) => void>).mock.calls[0];
      const csv = parseCsv(args[1]);
      expect(csv[0].slice(0, 9)).toEqual([
        'Data Version',
        'Time Stamp',
        'Serial Number',
        'Description',
        'Calibration Type',
        'Table Width',
        'Table Offset',
        'Data_1',
        'Data_2',
      ]);
      expect(csv[1].slice(0, 11)).toEqual([
        1,
        expect.anything(),
        1234,
        'Retrieved from device: X-LHM',
        1, 16, 0,
        0, 1, 2, 3
      ]);
    });

    test('loads table from file', async () => {
      fireEvent.click(wrapper.getByText('Load from File'));

      await waitUntilPass(() => expect(wrapper.getAllByRole('spinbutton')[0]).toHaveValue(2));
      expect(wrapper.getAllByRole('spinbutton')
        .map(input => (input as HTMLInputElement).value))
        .toEqual(['2', '32', '12']);
      expect((wrapper.getByRole('textbox') as HTMLTextAreaElement).value).toMatch(/^\s+63\s+62\s+61\s+60/);
    });

    test('displays file error', async () => {
      const fs = require('fs/promises');
      (fs.readFile as jest.MockedFunction<(...args: string[]) => Promise<string>>)
        .mockResolvedValueOnce('some text file\nbla bla');
      fireEvent.click(wrapper.getByText('Load from File'));

      await waitUntilPass(() => wrapper.getByText(/File Error: Invalid data format/));
    });
  });
});
