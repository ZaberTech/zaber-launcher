import { EditableField, HeaderCard, Icons, Loader, NoticeBanner, Text, Toggle } from '@zaber/react-library';
import { notNil, tryAccess } from '@zaber/toolbox';
import classNames from 'classnames';
import _ from 'lodash';
import React, { useEffect }  from 'react';
import { useSelector } from 'react-redux';

import { DeviceNumber } from '../components';
import type { EntityKey } from '../keys';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectDevices } from './selectors';
import { IoState, maybeHasIO, VOLTAGE_DECIMAL_NUM } from './types';

const Channels: React.FC<{ count: number }> = ({ count, children }) => (<>
  <tr className="channel-lines">
    <td className="channel-name">{children}</td>
    {_.range(count).map(i =>
      <td key={i}>
        <div className="lines">
          <div/><div/>
        </div>
      </td>)}
  </tr>
  <tr className="channel-numbers">
    <td/>
    {_.range(count).map(i =>
      <td key={i}>{i + 1}</td>)}
  </tr>
</>);

export const DigitalInputs: React.FC<{ inputs: boolean[] }> = ({ inputs }) => {
  if (inputs.length === 0) { return null }
  return <table className="io-digital-inputs">
    <tbody>
      <Channels count={inputs.length}>Digital Inputs</Channels>
      <tr className="values">
        <td/>
        {inputs.map((value, i) =>
          <td key={i}>
            <div>
              <div className={classNames('dot', value ? 'on' : 'off')}/>
              <Text t={Text.Type.BodySm} className={value ? 'on' : 'off'}>{value ? 'On' : 'Off'}</Text>
            </div>
          </td>)}
      </tr>
    </tbody>
  </table>;
};

const AnalogInputs: React.FC<{ io: IoState }> = ({ io: { analogInputs } }) => {
  if (analogInputs.length === 0) { return null }
  return <table className="io-analog-inputs">
    <tbody>
      <Channels count={analogInputs.length}>Analog Inputs [V]</Channels>
      <tr className="values">
        <td/>
        {analogInputs.map((value, i) =>
          <td key={i}>{value.toFixed(VOLTAGE_DECIMAL_NUM)}</td>)}
      </tr>
    </tbody>
  </table>;
};

export const DigitalOutputs: React.FC<{ deviceKey: EntityKey; outputs: boolean[] }> = ({ deviceKey, outputs }) => {
  const actions = useActions(actionsDefinition);

  if (outputs.length === 0) { return null }
  return <table className="io-digital-outputs">
    <tbody>
      <Channels count={outputs.length}>Digital Outputs</Channels>
      <tr className="values">
        <td/>
        {outputs.map((value, i) =>
          <td key={i}>
            <div>
              <Toggle value={value} onValueChange={newValue => actions.setOutput(deviceKey, i, newValue)}/>
              <Text t={Text.Type.BodySm} className={value ? 'on' : 'off'}>{value ? 'On' : 'Off'}</Text>
            </div>
          </td>)}
      </tr>
    </tbody>
  </table>;
};

const AnalogOutputs: React.FC<{ deviceKey: EntityKey; io: IoState }> = ({ deviceKey, io: { analogOutputs } }) => {
  const actions = useActions(actionsDefinition);

  if (analogOutputs.length === 0) { return null }
  return <table className="io-analog-outputs">
    <tbody>
      <Channels count={analogOutputs.length}>Analog Outputs [V]</Channels>
      <tr className="values">
        <td/>
        {analogOutputs.map((value, i) =>
          <td key={i}>
            <EditableField type="number"
              validate={EditableField.validators.float}
              value={value.toFixed(VOLTAGE_DECIMAL_NUM)}
              onValueChange={newValue => actions.setOutput(deviceKey, i, +newValue)}/>
          </td>)}
      </tr>
    </tbody>
  </table>;
};

export const DeviceIo: React.FC<{ deviceKey: EntityKey }> = ({ deviceKey }) => {
  const actions = useActions(actionsDefinition);

  useEffect(() => {
    actions.mount(deviceKey);
    return () => { actions.unmount(deviceKey) };
  }, [deviceKey]);

  const device = tryAccess(useSelector(selectDevices), deviceKey);
  if (device == null || !maybeHasIO(device.ioState)) { return null }

  return <HeaderCard className="device-io" header={
    <>
      <Text t={Text.Type.H5}>
        <DeviceNumber>{device.address}</DeviceNumber>&nbsp;
        {device.identity.name}
      </Text>
      <div className="spacer"/>
      <Icons.Refresh title="Refresh IO" onClick={() => actions.refreshIo(device.key)}/>
    </>
  }>
    {device.ioState == null && <div className="loading">
      <Loader size="small"/> Loading...
    </div>}
    {device.ioState != null && <>
      <div className="io-container">
        <DigitalInputs inputs={device.ioState.digitalInputs}/>
        <AnalogInputs io={device.ioState}/>
      </div>
      <div className="io-container">
        <DigitalOutputs deviceKey={deviceKey} outputs={device.ioState.digitalOutputs}/>
        <AnalogOutputs deviceKey={deviceKey} io={device.ioState}/>
      </div>
    </>}
    {[device.readError, device.setOutputError].filter(notNil).map((error, i) =>
      <NoticeBanner key={i}>{error}</NoticeBanner>)}
  </HeaderCard>;
};
