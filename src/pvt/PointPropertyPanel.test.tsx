import _ from 'lodash';
import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import { Length, Velocity } from '@zaber/motion';

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';

import { actions } from './actions';
import { PointPropertyPanel } from './PointPropertyPanel';
import type { Path } from './types';
import { expandPointPropertiesGroup, selectPoint } from './testing/mocks';
import { selectUserPath } from './selectors';


const TestApp = wrapWithNewStore(wrapWithRouter(PointPropertyPanel));
let wrapper: RenderResult;

const TestPath: Path = {
  positionUnit: Length.INCHES,
  velocityUnit: Velocity.INCHES_PER_SECOND,
  series: [{
    name: 'Ecks',
    points: [{
      dt: 0,
      time: 0,
      pos: 1,
      vel: 2,
    }, {
      dt: 5,
      time: 5,
      pos: 3,
      vel: 4,
    }],
  }, {
    name: 'Why',
    points: [{
      dt: 0,
      time: 0,
      pos: 6,
      vel: 7,
    }, {
      dt: 5,
      time: 5,
      pos: 8,
      vel: 9,
    }],
  }],
};


beforeEach(() => {
  createContainer();
  wrapper = render(<TestApp/>);
  const store = TestApp.testStore;
  store.dispatch(actions.storeUserPath(TestPath));
});


afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
});


test('Displays a section for each series', () => {
  const headers = wrapper.getAllByTestId(/point-header-/);
  expect(headers.length).toBe(3);
  expect(headers.map(h => h.firstChild?.textContent))
    .toEqual(['Data Point', 'Ecks', 'Why']);
});


test('Displays expected units of measure in series details', async () => {
  let cards = await wrapper.findAllByTestId(/point-header-/);
  cards.forEach(card => fireEvent.click(card.firstChild!.firstChild!));
  cards = await wrapper.findAllByTestId(/point-header-/);
  await waitUntilPass(() => {
    expect(_.some(cards, card => card.getElementsByClassName('chevron collapsed').length > 0)).toBe(false);
  });

  expect(await wrapper.findAllByText(/\(s\)/)).toHaveLength(2);
  expect(await wrapper.findAllByText(/\(in\)/)).toHaveLength(2);
  expect(await wrapper.findAllByText(/\(in\/s\)/)).toHaveLength(2);
});


test('Displays properties of selected point', () => {
  expandPointPropertiesGroup(wrapper, 'Ecks');
  expandPointPropertiesGroup(wrapper, 'Why');

  selectPoint(wrapper, 0);
  expect((wrapper.getByTestId('edit-Ecks-pos') as HTMLInputElement).value).toBe('1');
  expect((wrapper.getByTestId('edit-Ecks-vel') as HTMLInputElement).value).toBe('2');
  expect((wrapper.getByTestId('edit-Why-pos') as HTMLInputElement).value).toBe('6');
  expect((wrapper.getByTestId('edit-Why-vel') as HTMLInputElement).value).toBe('7');

  selectPoint(wrapper, 1);
  expect((wrapper.getByTestId('edit-Ecks-pos') as HTMLInputElement).value).toBe('3');
  expect((wrapper.getByTestId('edit-Ecks-vel') as HTMLInputElement).value).toBe('4');
  expect((wrapper.getByTestId('edit-Why-pos') as HTMLInputElement).value).toBe('8');
  expect((wrapper.getByTestId('edit-Why-vel') as HTMLInputElement).value).toBe('9');
});


test('Cannot delete if there are less than three points', () => {
  expect(wrapper.queryAllByTestId('properties-delete-point-button')).toHaveLength(0);
});


describe('Adding a point', () => {
  beforeEach(() => {
    expandPointPropertiesGroup(wrapper, 'Ecks');
    expandPointPropertiesGroup(wrapper, 'Why');
    selectPoint(wrapper, 0);
    fireEvent.click(wrapper.getByTestId('properties-add-point-button'));
  });


  it('places the new point halfway in time', () => {
    const path = selectUserPath(TestApp.testStore.getState());
    expect(path).not.toBeNull();
    expect(path!.series[0].points.length).toBe(3);
    expect(path!.series[1].points.length).toBe(3);
    expect(path!.series[0].points[1].time).toBe(2.5);
    expect(path!.series[0].points[2].time).toBe(5);
    expect(path!.series[1].points[1].time).toBe(2.5);
    expect(path!.series[1].points[2].time).toBe(5);
    expect(path!.series[0].points[1].dt).toBe(2.5);
    expect(path!.series[0].points[2].dt).toBe(2.5);
    expect(path!.series[1].points[1].dt).toBe(2.5);
    expect(path!.series[1].points[2].dt).toBe(2.5);
    expect(path!.series[0].points[1].pos).toBeCloseTo(0.75); // Determined empirically.
    expect(path!.series[0].points[1].vel).toBeCloseTo(-0.9);
    expect(path!.series[0].points[1].accel).toBeCloseTo(0.4);
    expect(path!.series[1].points[1].pos).toBeCloseTo(5.75);
    expect(path!.series[1].points[1].vel).toBeCloseTo(-3.4);
    expect(path!.series[1].points[1].accel).toBeCloseTo(0.4);
  });


  it('can delete the first point', () => {
    selectPoint(wrapper, 0);
    fireEvent.click(wrapper.getByTestId('properties-delete-point-button'));
    const path = selectUserPath(TestApp.testStore.getState());
    expect(path!.series[0].points.length).toBe(2);
    expect(path!.series[1].points.length).toBe(2);
    expect(path!.series[0].points[0].time).toBe(0);
    expect(path!.series[0].points[1].time).toBe(2.5);
    expect(path!.series[1].points[0].time).toBe(0);
    expect(path!.series[1].points[1].time).toBe(2.5);
    expect(path!.series[0].points[0].dt).toBe(0);
    expect(path!.series[0].points[1].dt).toBe(2.5);
    expect(path!.series[1].points[0].dt).toBe(0);
    expect(path!.series[1].points[1].dt).toBe(2.5);
    expect(path!.series[0].points[0].pos).toBeCloseTo(0.75); // Determined empirically.
    expect(path!.series[0].points[0].vel).toBe(0);
    expect(path!.series[0].points[0].accel).toBeUndefined();
    expect(path!.series[0].points[1].accel).toBeCloseTo(-2.16);
    expect(path!.series[1].points[0].pos).toBeCloseTo(5.75);
    expect(path!.series[1].points[0].vel).toBe(0);
    expect(path!.series[1].points[0].accel).toBeUndefined();
    expect(path!.series[1].points[1].accel).toBeCloseTo(-2.16);
  });


  it('can delete the middle point', () => {
    selectPoint(wrapper, 1);
    fireEvent.click(wrapper.getByTestId('properties-delete-point-button'));
    const path = selectUserPath(TestApp.testStore.getState());
    expect(path!.series[0].points.length).toBe(2);
    expect(path!.series[1].points.length).toBe(2);
    expect(path!.series[0].points[0].time).toBe(0);
    expect(path!.series[0].points[1].time).toBe(5);
    expect(path!.series[1].points[0].time).toBe(0);
    expect(path!.series[1].points[1].time).toBe(5);
    expect(path!.series[0].points[0].dt).toBe(0);
    expect(path!.series[0].points[1].dt).toBe(5);
    expect(path!.series[1].points[0].dt).toBe(0);
    expect(path!.series[1].points[1].dt).toBe(5);
    expect(path!.series[0].points[0].pos).toBe(1);
    expect(path!.series[0].points[0].vel).toBe(0);
    expect(path!.series[0].points[0].accel).toBeUndefined();
    expect(path!.series[0].points[1].accel).toBeCloseTo(-0.48);
    expect(path!.series[1].points[0].pos).toBe(6);
    expect(path!.series[1].points[0].vel).toBe(0);
    expect(path!.series[1].points[0].accel).toBeUndefined();
    expect(path!.series[1].points[1].accel).toBeCloseTo(-0.48);
  });


  it('can delete the last point', () => {
    selectPoint(wrapper, 2);
    fireEvent.click(wrapper.getByTestId('properties-delete-point-button'));
    const path = selectUserPath(TestApp.testStore.getState());
    expect(path!.series[0].points.length).toBe(2);
    expect(path!.series[1].points.length).toBe(2);
    expect(path!.series[0].points[0].time).toBe(0);
    expect(path!.series[0].points[1].time).toBe(2.5);
    expect(path!.series[1].points[0].time).toBe(0);
    expect(path!.series[1].points[1].time).toBe(2.5);
    expect(path!.series[0].points[0].dt).toBe(0);
    expect(path!.series[0].points[1].dt).toBe(2.5);
    expect(path!.series[1].points[0].dt).toBe(0);
    expect(path!.series[1].points[1].dt).toBe(2.5);
    expect(path!.series[0].points[0].pos).toBe(1);
    expect(path!.series[0].points[0].vel).toBe(0);
    expect(path!.series[0].points[1].pos).toBeCloseTo(0.75);
    expect(path!.series[0].points[1].vel).toBe(0);
    expect(path!.series[0].points[0].accel).toBeUndefined();
    expect(path!.series[0].points[1].accel).toBeCloseTo(0.24);
    expect(path!.series[1].points[0].pos).toBe(6);
    expect(path!.series[1].points[0].vel).toBe(0);
    expect(path!.series[1].points[1].pos).toBeCloseTo(5.75);
    expect(path!.series[1].points[1].vel).toBe(0);
    expect(path!.series[1].points[0].accel).toBeUndefined();
    expect(path!.series[1].points[1].accel).toBeCloseTo(0.24);
  });
});
