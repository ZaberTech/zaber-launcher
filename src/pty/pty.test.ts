import { injectable } from 'inversify';
import { Subject } from 'rxjs';
import { EventEmitter } from 'events';

let ipcRenderer: IpcRendererMock;
class IpcRendererMock extends EventEmitter {
  constructor() {
    super();
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    ipcRenderer = this;
  }

  send = jest.fn();
}

jest.mock('electron', () => ({
  ipcRenderer: new IpcRendererMock(),
}));

import { createContainer, destroyContainer } from '../container';
import type { AnyAction } from '../utils';
import { IPC } from '../ipc';
import {
  actions, ActionTypes, PROCESS_TX_IPC_CHANNEL, PROCESS_RX_IPC_CHANNEL,
} from './common_types';
import { Pty } from './pty';

let ipcMock: IpcMock;
@injectable()
class IpcMock {
  channels: Record<string, Subject<AnyAction>> = {};

  sendActionToMainProcess = jest.fn<void, [AnyAction]>();
  getActions = jest.fn((actionType: string) => {
    if (!this.channels[actionType]) {
      this.channels[actionType] = new Subject<AnyAction>();
    }
    return this.channels[actionType];
  });
}

let container: ReturnType<typeof createContainer>;
let pty: Pty;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(IPC).to(IpcMock);
  ipcMock = container.get<unknown>(IPC) as IpcMock;
  pty = container.get(Pty);
});

afterEach(() => {
  destroyContainer();
  container = null!;
  ipcMock = null!;
  pty = null!;
});

test('spawns process and waits for exit', async () => {
  const promise = pty.spawn({ id: 'id', cmd: 'cmd', cwd: 'cwd', args: [] });
  expect(ipcMock.sendActionToMainProcess).toHaveBeenCalledWith(
    expect.objectContaining({ type: ActionTypes.SPAWN }));

  setTimeout(() => ipcMock.channels[ActionTypes.EXIT].next(actions.exit('id', 0, null)));

  expect(await promise).toStrictEqual(
    expect.objectContaining({ exitCode: 0, signal: null }));
});

test('kill sends command for killing', () => {
  pty.kill('id');
  expect(ipcMock.sendActionToMainProcess).toHaveBeenCalledWith(actions.kill('id'));
});

test('sendData sends data to ipc', () => {
  pty.sendData('id', 'data');
  expect(ipcRenderer.send).toHaveBeenCalledWith(PROCESS_TX_IPC_CHANNEL, { id: 'id', data: 'data' });
});

test('getDataSteam returns data for a particular process', () => {
  const allData: string[] = [];
  pty.getDataSteam('id').subscribe(data => allData.push(data));
  ipcRenderer.emit(PROCESS_RX_IPC_CHANNEL, 'event', { id: 'id', data: 'data' });
  ipcRenderer.emit(PROCESS_RX_IPC_CHANNEL, 'event', { id: 'id2', data: 'data' });
  ipcRenderer.emit(PROCESS_RX_IPC_CHANNEL, 'event', { id: 'id', data: 'data2' });
  expect(allData).toStrictEqual(['data', 'data2']);
});
