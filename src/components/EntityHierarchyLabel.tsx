import React, { BaseHTMLAttributes } from 'react';
import { Flex, Icons, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';
import classnames from 'classnames';

import { type EntityKey, getEntityType, EntityKeyType, tryExtractDeviceKey, tryExtractConnectionKey } from '../keys';
import { connectionName, deviceNameWithLabel, selectAxes, selectConnections, selectDevices } from '../connection_manager';
import { ConnectionType } from '../app_components';
import { prependZeros } from '../utils';

interface EntityHierarchyLabelProps extends BaseHTMLAttributes<HTMLDivElement> {
  entityKey: EntityKey | null;
}

export const EntityHierarchyLabel: React.FC<EntityHierarchyLabelProps> = ({ entityKey, className, ...rest }) => {
  const connections = useSelector(selectConnections);
  const devices = useSelector(selectDevices);
  const axes = useSelector(selectAxes);

  const connectionEntityKey = tryExtractConnectionKey(entityKey);
  const connection = connectionEntityKey ? connections[connectionEntityKey] : null;
  const deviceEntityKey = tryExtractDeviceKey(entityKey);
  const device = deviceEntityKey ? devices[deviceEntityKey] : null;
  const axisEntityKey = getEntityType(entityKey) === EntityKeyType.AXIS ? entityKey : null;
  const axis = axisEntityKey ? axes[axisEntityKey] : null;

  const axisName = axis?.label ?? axis?.identity?.peripheralName ?? axis?.nonIdentifiedInfo?.peripheralId ?? 'Unknown';

  return <Flex.Row className={classnames(className, 'entity-hierarchy-label')} {...rest}>
    {connection?.type === ConnectionType.SERIAL_PORT && <Icons.Usb className="connection-icon"/>}
    {connection?.type === ConnectionType.TCP_IP && <Icons.EthernetSocket className="connection-icon"/>}
    {connection && <Text t={Text.Type.H4} title="Connection">{connectionName(connection)}</Text>}
    {device && <Icons.ArrowCollapsed className="arrow-icon"/>}
    {device &&
      <Text t={Text.Type.H4} title="Device">
        {prependZeros(device.address)}&nbsp;
        {deviceNameWithLabel(device)}
      </Text>
    }
    {axis && <Icons.ArrowCollapsed className="arrow-icon"/>}
    {axis && <Text t={Text.Type.H4} title="Axis / Peripheral">{axisName}</Text>}
  </Flex.Row>;
};
