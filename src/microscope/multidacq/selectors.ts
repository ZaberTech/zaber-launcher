import { createSelector } from 'reselect';
import { notNil } from '@zaber/toolbox';

import { selectMicroscope } from '../../store';
import { MeasurementMath } from '../utils';
import { selectPresets } from '../selectors';
import { ZERO_PRESET } from '../presets';

export const selectState = createSelector(selectMicroscope, state => state.multiDAcq);

export const selectConfig = createSelector(selectState, state => state.config);
export const selectGrid = createSelector(selectConfig, state => state.xy.grid);
export const selectFocus = createSelector(selectConfig, state => state.focus);
export const selectPresetsConfig = createSelector(selectConfig, state => state.presets);
export const selectSelectedPresets = createSelector(selectPresetsConfig, selectPresets,
  (state, presets) => state.selected
    .map(selected => presets.find(other => other.name === selected))
    .filter(notNil));

export const selectAppliedPresets = createSelector(selectSelectedPresets, presets =>
  presets.map(preset => ({
    ...preset,
    data: {
      ...preset.data,
      axes: ZERO_PRESET.axes,
    },
  })));

export const selectGridCoordinates = createSelector(selectGrid, grid => {
  const { size, align } = grid;
  const cols = size.x ?? 1;
  const rows = size.y ?? 1;

  const convert = MeasurementMath.toNumber;

  const spacing = {
    x: convert(grid.spacing.x),
    y: convert(grid.spacing.y),
  };
  const reference = {
    x: convert(grid.reference.x),
    y: convert(grid.reference.y),
  };

  const offset = align === 'center' ? {
    x: (cols - 1) / 2 * -spacing.x,
    y: (rows - 1) / 2 * -spacing.y,
  } : {
    x: 0, y: 0
  };

  const points: { x: number; y: number }[] = [];

  if (grid.order === 'rows') {
    for (let j = 0; j < rows; j++) {
      for (let i = 0; i < cols; i++) {
        points.push({
          x: reference.x + i * spacing.x + offset.x,
          y: reference.y + j * spacing.y + offset.y,
        });
      }
    }
  } else if (grid.order === 'columns') {
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        points.push({
          x: reference.x + i * spacing.x + offset.x,
          y: reference.y + j * spacing.y + offset.y,
        });
      }
    }
  } else if (grid.order === 'snake-rows') {
    for (let j = 0; j < rows; j++) {
      for (let i = 0; i < cols; i++) {
        points.push({
          x: reference.x + (j % 2 === 0 ? i : cols - 1 - i) * spacing.x + offset.x,
          y: reference.y + j * spacing.y + offset.y,
        });
      }
    }
  } else if (grid.order === 'snake-columns') {
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        points.push({
          x: reference.x + i * spacing.x + offset.x,
          y: reference.y + (i % 2 === 0 ? j : rows - 1 - j) * spacing.y + offset.y,
        });
      }
    }
  }

  return {
    units: MeasurementMath.mathUnits(grid.reference.x),
    points,
  };
});

export const selectAcquisition = createSelector(selectState, state => state.acquisition);

export const selectFocusPositions = createSelector(selectConfig, config => {
  const focus = config.focus;
  const c = MeasurementMath.toNumber;

  const reference = c(focus.reference);

  const step = c(focus.spacing);
  const points: number[] = [];
  const neg = focus.count.neg ?? 0;
  const pos = focus.count.pos ?? 0;

  for (let i = neg; i >= 1; i--) {
    points.push(-step * i);
  }

  points.push(0);

  for (let i = 1; i <= pos; i++) {
    points.push(step * i);
  }

  return {
    units: MeasurementMath.mathUnits(focus.reference),
    points,
    reference,
    autofocus: focus.autofocus,
  };
});
