let logStoreActions = 0;

export function shouldLogStoreActions(): boolean {
  return logStoreActions > 0;
}

export function withLogStoreActions<TReturn>(callback: () => TReturn) {
  return (() => {
    logStoreActions++;
    try {
      const result = callback();
      if (result instanceof Promise) {
        logStoreActions++;
        return result.finally(() => {
          logStoreActions--;
        }) as TReturn;
      } else {
        return result;
      }
    } finally {
      logStoreActions--;
    }
  });
}
