import React from 'react';
import { Flex, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';
import { IoPortType } from '@zaber/motion/ascii';

import type { EntityKey } from '../../keys';
import { usePolledAnalogIo } from '../../polling/hooks';

import { selectDeviceStates } from './selectors';

type AnalogInputProps = {
  deviceKey: EntityKey;
  channel: number;
};

export const AnalogInput: React.FC<AnalogInputProps> = ({ deviceKey, channel }) => {
  const input = usePolledAnalogIo(deviceKey, 'in', channel);
  const labels = useSelector(selectDeviceStates)[deviceKey]?.labels;
  const label = labels?.[`${IoPortType.ANALOG_INPUT}-${channel}`] ?? '';

  return <Flex.Row className="ai control">
    <Text e={Text.Emphasis.Bold}>{channel}.</Text>
    <Text className="fixed-width">{input == null ? '-' : Number(input).toFixed(2)}</Text>
    <Text>{label}</Text>
  </Flex.Row>;
};
