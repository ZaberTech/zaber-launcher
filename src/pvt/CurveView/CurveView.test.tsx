import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import type { Container } from 'inversify';

import { MessageRoutersServiceMockMultiAxis, SelectMock, setAxisCallback } from '../testing/mocks';

jest.mock('react-select', () => SelectMock);


import { Length, Velocity } from '@zaber/motion';
import _ from 'lodash';
import userEvent from '@testing-library/user-event';

import { IdentifiedAxisInfo, selectDevices } from '../../connection_manager';
import { mockSingleDeviceWithPeripherals } from '../../connection_manager/mocks';
import { createContainer, destroyContainer } from '../../container';
import { MessageRoutersService } from '../../message_router';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../../test';

import { actions } from '../actions';
import { discretizePath } from '../pvtCurveUtils';
import { selectAccelerationLimits, selectPositionLimits, selectUserPath, selectVelocityLimits } from '../selectors';
import { Path } from '../types';

import { CurveView } from './CurveView';


const path1: Path = {
  positionUnit: Length.mm,
  velocityUnit: Velocity['mm/s'],
  series: [{
    name: 'Horizontal',
    points: [
      { dt: 0, time: 0, pos: 0, vel: 0 },
      { dt: 3, time: 3, pos: 10, vel: 10 },
      { dt: 3, time: 6, pos: 25, vel: 0 },
      { dt: 3, time: 9, pos: 10, vel: -10 },
      { dt: 3, time: 12, pos: 0, vel: 0 },
    ],
  }, {
    name: 'Vertical',
    points: [
      { dt: 0, time: 0, pos: 0, vel: 0 },
      { dt: 3, time: 3, pos: 0, vel: 0 },
      { dt: 3, time: 6, pos: 10, vel: 10 },
      { dt: 3, time: 9, pos: 20, vel: 0 },
      { dt: 3, time: 12, pos: 20, vel: 0 },
    ],
  }],
};

const user = userEvent.setup();
let container: Container;
const TestDisplay = wrapWithNewStore(wrapWithRouter(CurveView));
let wrapper: RenderResult;


async function loadFile(content: Path) {
  TestDisplay.testStore.dispatch(actions.storeUserPath(content));
  const devicePath = await discretizePath(content, 'high');
  if (devicePath == null) {
    throw new Error('Test setup problem: devicePath is null');
  }

  TestDisplay.testStore.dispatch(actions.storeDevicePath(devicePath));
}

function createDevice() {
  mockSingleDeviceWithPeripherals(TestDisplay.testStore, {
    peripheralCount: 2,
    axisModifier: (info: IdentifiedAxisInfo) => ({
      ...info,
      identity: {
        ...info.identity,
        peripheralName: `${info.identity.peripheralName} (axis ${info.axisNumber})`,
      },
    }),
  });

  const device = _.sample(selectDevices(TestDisplay.testStore.getState()))!;
  TestDisplay.testStore.dispatch(actions.selectDevice(device.key));
  TestDisplay.testStore.dispatch(actions.commitDeviceSelection());
}


const oldBoundingRect = SVGSVGElement.prototype.getBoundingClientRect;

beforeAll(() => {
  SVGSVGElement.prototype.getBoundingClientRect = jest.fn(() => ({
    width: 1000,
    height: 1000,
    top: 0,
    left: 0,
    bottom: 1000,
    right: 1000,
    x: 0,
    y: 0,
    toJSON(): void {},
  }));
});

afterAll(() => {
  SVGSVGElement.prototype.getBoundingClientRect = oldBoundingRect;
});


describe('Curve view', () => {
  beforeEach(async () => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockMultiAxis);
    wrapper = render(<TestDisplay/>);
    await loadFile(path1);
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  it('displays grid lines', async () => {
    const horizontal = await wrapper.findAllByRole('horizontal-grid-line');
    expect(horizontal).toHaveLength(7);

    const vertical = await wrapper.findAllByRole('vertical-grid-line');
    expect(vertical).toHaveLength(7);
  });


  it('displays tick mark values', async () => {
    const horizontal = await wrapper.findAllByRole('x-tick-value');
    expect(horizontal.map(node => node.textContent)).toEqual(_.range(-5, 30, 5).map(String));

    const vertical = await wrapper.findAllByRole('y-tick-value');
    expect(vertical.map(node => node.textContent)).toEqual(_.range(-5, 30, 5).map(String));
  });


  it('displays axis labels', async () => {
    const xlabel = await wrapper.findByRole('x-axis-label');
    expect(xlabel.textContent).toBe('Horizontal (mm)');
    const ylabel = await wrapper.findByRole('y-axis-label');
    expect(ylabel.textContent).toBe('Vertical (mm)');
  });


  it('displays control points', async () => {
    const points = await wrapper.findAllByRole('control-point');
    expect(points).toHaveLength(5);
  });


  it('does not display velocity controls when no point is selected', () => {
    const elements = wrapper.queryAllByRole('velocity-control');
    expect(elements).toHaveLength(0);
  });


  it('shows the smooth curve', async () => {
    await wrapper.findByRole('curve-display');
  });


  describe('with a control point selected', () => {
    beforeEach(() => {
      TestDisplay.testStore.dispatch(actions.setSelection(2));
    });


    it('displays velocity controls', () => {
      const elements = wrapper.queryAllByRole('velocity-control');
      expect(elements).toHaveLength(2);
    });
  });


  describe('with a device connected', () => {
    beforeEach(() => {
      setAxisCallback(axis => {
        axis._settings.pos = 0;
        axis._settings['limit.min'] = 0;
        axis._settings['limit.max'] = 1000000;
      });

      createDevice();
      TestDisplay.testStore.dispatch(actions.storeAxisSelections({ [path1.series[0].name]: 0, [path1.series[1].name]: 1 }));
      TestDisplay.testStore.dispatch(actions.storeDevicePosition([0, 0], [0, 0], null));
    });


    it('displays device position', async () => {
      await waitUntilPass(async () => await wrapper.findByRole('device-position-marker'));
    });


    it('displays device travel limits', async () => {
      await waitUntilPass(async () => await wrapper.findAllByRole('x-travel-limit'));
      const xLimits = wrapper.queryAllByRole('x-travel-limit');
      expect(xLimits.length).toBeGreaterThan(0);
      const yLimits = wrapper.queryAllByRole('y-travel-limit');
      expect(yLimits.length).toBeGreaterThan(0);
    });


    it('displays limit excursion warnings', async () => {
      const posLimits = _.cloneDeep(selectPositionLimits(TestDisplay.testStore.getState()));
      const velLimits = selectVelocityLimits(TestDisplay.testStore.getState());
      const accelLimits = selectAccelerationLimits(TestDisplay.testStore.getState());
      posLimits[0]!.min = 5;
      TestDisplay.testStore.dispatch(actions.setDeviceLimits(posLimits, velLimits, accelLimits));
      await waitUntilPass(async () => await wrapper.findAllByRole('error-marker'));
    });
  });


  describe('with mouse interaction', () => {
    it('dragging a control point edits the curve', async () => {
      const oldPath = selectUserPath(TestDisplay.testStore.getState());
      expect(oldPath?.series[0].points[2].pos).toBeCloseTo(25, 1);

      const points = await wrapper.findAllByRole('control-point');
      await user.pointer([
        // Coordinates determined experimentally and not perfectly accurate.
        // It's not easy to calculate what they should be.
        { coords: { x: 1011, y: 622 }, target: points[2] },
        { keys: '[MouseLeft>]', target: points[2] },
        { coords: { x: 570, y: 622 } },
        { keys: '[/MouseLeft>]' },
      ]);

      const newPath = selectUserPath(TestDisplay.testStore.getState());
      expect(newPath?.series[0].points[2].pos).toBeCloseTo(10, 1);
    });


    it('dragging the velocity handle edits the curve', async () => {
      TestDisplay.testStore.dispatch(actions.setSelection(2));
      const oldPath = selectUserPath(TestDisplay.testStore.getState());
      expect(oldPath?.series[1].points[2].vel).toBeCloseTo(10, 1);

      const handle = await wrapper.findAllByRole('velocity-control');
      await user.pointer([
        { coords: { x: 830, y: 218 }, target: handle[1] },
        { keys: '[MouseLeft>]', target: handle[1] },
        { coords: { x: 830, y: 500 }, target: handle[1] },
        { keys: '[/MouseLeft>]' },
      ]);

      const newPath = selectUserPath(TestDisplay.testStore.getState());
      expect(newPath?.series[1].points[2].vel).toBeCloseTo(0, 1);
    });
  });
});
