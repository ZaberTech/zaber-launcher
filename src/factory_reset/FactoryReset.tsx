import { ContextMenu, Icons, Modal, Loader, ButtonRowConfirmCancel, Checkbox, UnorderedList, Text } from '@zaber/react-library';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { DeviceNumber } from '../components';
import { deviceLabel } from '../connection_manager/device_table';
import { Editions, environment } from '../environment';
import type { EntityKey } from '../keys';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectAll, selectAxis, selectDevice } from './selectors';

export const FactoryResetDeviceMenuItem: React.FC<{ deviceKey: EntityKey }> = ({ deviceKey }) => {
  const { step } = useSelector(selectAll);
  const actions = useActions(actionsDefinition);
  return (
    <ContextMenu.Item icon={<Icons.FactoryReset/>}
      disabled={step != null}
      onClick={() => actions.begin(deviceKey)}>
      Factory Reset Device {environment.edition !== Editions.Public && <> (Restore)</>}
    </ContextMenu.Item>);
};

export const FactoryResetAxisMenuItem: React.FC<{ axisKey: EntityKey }> = ({ axisKey }) => {
  const { step } = useSelector(selectAll);
  const actions = useActions(actionsDefinition);
  return (
    <ContextMenu.Item icon={<Icons.FactoryReset/>}
      disabled={step != null}
      onClick={() => actions.begin(axisKey)}>
      Factory Reset Axis {environment.edition !== Editions.Public && <> (Restore)</>}
    </ContextMenu.Item>);
};

export const FactoryResetModal: React.FC = () => {
  const { error, step } = useSelector(selectAll);
  const actions = useActions(actionsDefinition);
  const device = useSelector(selectDevice);
  const axis = useSelector(selectAxis);
  const [hardReset, setHardReset] = useState(false);
  return (<div className="factory-reset-modal">
    {error != null && <Modal
      headerIcon={<Icons.FactoryReset/>}
      headerText="Factory Reset Error"
      small
      bodyIcon="error"
      onRequestClose={actions.clear}>
      Error resetting the device: {error}
    </Modal>}

    {step === 'confirm' && device != null &&
    <Modal
      headerIcon={<Icons.FactoryReset/>}
      headerText="Confirm Factory Reset"
      buttons={<ButtonRowConfirmCancel confirmText="Reset"
        onConfirm={() => actions.confirm(hardReset)}
        onCancel={actions.clear}/>}
      onRequestClose={actions.clear}
      small
      bodyIcon="warning"
    >
      <div>Are you sure you want to factory reset&#32;
        {axis != null && <>
          <b>Axis {axis.axisNumber}</b> of device&#32;
          <b><DeviceNumber>{device.address}</DeviceNumber> {deviceLabel(device)}</b>
        </>}
        {axis == null && <>
          device <b><DeviceNumber>{device.address}</DeviceNumber> {deviceLabel(device)}</b>
        </>}
        ?
      </div>

      {axis == null && <>
        <UnorderedList className="extra-padding" header={<>Factory reset:</>}>
          <Text>Reverts most of the settings to the default values.</Text>
          <Text>Deletes all triggers, stream/PVT buffers, servo tunings.</Text>
          <Text>Deletes all zaber storage keys (keys with "zaber." prefix).</Text>
          <Text>Disables locksteps, unparks axes.</Text>
        </UnorderedList>

        <Checkbox labelContent={<>
          Reset communication settings and erase non-zaber storage keys.
        </>} checked={hardReset} onChecked={setHardReset}/>
      </>}
      {axis != null &&
        <UnorderedList header={<>Factory reset:</>}>
          <Text>Reverts all of the axis settings to the default values.</Text>
          <Text>Deletes all zaber axis storage keys (keys with "zaber." prefix).</Text>
          <Text>Disables locksteps, unparks the axis.</Text>
          <Text>Preserves non-zaber storage keys.</Text>
        </UnorderedList>}
    </Modal>}

    {step === 'resetting' &&
    <Modal
      headerIcon={<Icons.FactoryReset/>}
      headerText="Factory Reset"
      small
    >
      <div className="resetting">
        <Loader size="large"/> Resetting...
      </div>
    </Modal>}
  </div>);
};
