import _ from 'lodash';
import { createSelector } from 'reselect';
import { Nullable, tryAccess } from '@zaber/toolbox';

import { selectGCode } from '../store';
import {
  IdentifiedDeviceInfo, selectIdentifiedAxes, selectIdentifiedDevices, selectConnections,
  ConnectionManagerConnectionState,
} from '../connection_manager';
import { EntityKey, extractConnectionKey } from '../keys';

import { isPrimaryAxis } from './types';

export const selectPerformingBlockIndex = createSelector(selectGCode, state => state.performingBlockIndex);
export const selectWarnings = createSelector(selectGCode, state => state.warnings);
export const selectError = createSelector(selectGCode, state => state.error);
export const selectEvaluatingProgress = createSelector(selectGCode, state => state.evaluatingProgress);
export const selectDisabled = createSelector(selectGCode, state => state.performing || state.evaluating || state.command.executing);
export const selectTranslatingDisabled = createSelector(selectGCode, selectDisabled,
  (state, disabled) => disabled || !state.translatorRunning || _.some(state.axesLiveStates, axis => axis.needsHoming));
export const selectStatus = createSelector(selectGCode, state =>
  _.pick(state, 'performing', 'translatorRunning', 'step', 'coordinateSystem', 'translatorError'));
export const selectStep = createSelector(selectGCode, state => state.step);
export const selectSettings = createSelector(selectGCode, state => state.currentSettings);
export const selectEditedSettings = createSelector(selectGCode, state => state.editedSettings);
export const selectEditedSettingsValid = createSelector(selectEditedSettings, settings => {
  if (settings == null) {
    return false;
  }
  const primaryAxes = settings.axes?.filter(isPrimaryAxis) ?? [];
  if (primaryAxes.length === 0 || _.uniqBy(primaryAxes, axis => axis.letter).length !== primaryAxes.length) {
    return false;
  }

  if (settings.traverseRate == null || settings.traverseRate.value <= 0) {
    return false;
  }
  return true;
});
export const selectLoadingDevice = createSelector(selectGCode,
  ({ loadingDevice: loading, loadingDeviceError: error }) => ({ loading, error }));

export interface Device extends IdentifiedDeviceInfo {
  connection: ConnectionManagerConnectionState;
}

export const selectCurrentDevice = createSelector(selectGCode, selectIdentifiedDevices, selectIdentifiedAxes, selectConnections,
  (state,  ...rest) => selectDevice(state.currentSettings?.deviceKey, ...rest));
export const selectSettingsDevice = createSelector(selectGCode, selectIdentifiedDevices, selectIdentifiedAxes, selectConnections,
  (state, ...rest) => selectDevice(state.editedSettings?.deviceKey, ...rest));

function selectDevice(
  key: Nullable<EntityKey>,
  devices: ReturnType<typeof selectIdentifiedDevices>,
  axes: ReturnType<typeof selectIdentifiedAxes>,
  connections: ReturnType<typeof selectConnections>,
): Device | null {
  const device = tryAccess(devices, key);
  if (!device) { return null }
  return ({
    ...device,
    axes: device.axes.map(key => axes[key]),
    connection: connections[extractConnectionKey(device.key)],
  });
}

export const selectLoadedContent = createSelector(selectGCode, state => state.loadedContent);

export const selectStatusText = createSelector(selectGCode, state => {
  if (state.translatorError) {
    return 'Translator error';
  } else if (!state.translatorRunning) {
    return 'Translator not running';
  } else if (state.hardStopping) {
    return 'Stopping...';
  } else if (state.stopping) {
    return 'Waiting for stream to finish buffered movements...';
  } else if (state.performing) {
    if (state.step) {
      return 'Stepping...';
    } else {
      return 'Streaming...';
    }
  } else  {
    return 'Ready';
  }
});

export const selectLiveStates = createSelector(selectGCode, state => state.axesLiveStates);
export const selectPositionUpdateInterval = createSelector(selectGCode, state => {
  if (state.performing) {
    return 300;
  } else if (_.some(state.axesLiveStates, axis => axis.moving)) {
    return 150;
  } else {
    return 2000;
  }
});
export const selectIsBusy = createSelector(selectGCode, state => state.performing || _.some(state.axesLiveStates, axis => axis.moving));
export const selectCommand = createSelector(selectGCode, state => state.command);
export const selectFeedRateOverride = createSelector(selectGCode, state => state.feedRateOverride);
export const selectHolding = createSelector(selectGCode, state => state.holding);
