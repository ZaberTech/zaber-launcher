import React, { Component } from 'react';

import type { ConnectionsView } from './ConnectionsView';

jest.mock('./ConnectionsView', () => ({
  get ConnectionsView(): typeof ConnectionsViewMock {
    return ConnectionsViewMock;
  },
}));

export let connectionViewMockInstance: ConnectionsViewMock;

type Props = React.ComponentPropsWithoutRef<typeof ConnectionsView>;

export class ConnectionsViewMock extends Component<Props> {
  constructor(props: Props) {
    super(props);
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    connectionViewMockInstance = this;
  }

  componentWillUnmount(): void {
    connectionViewMockInstance = null!;
  }

  render(): React.ReactNode {
    return <div title={`Selected: ${this.props.selected}`}>ConnectionsView</div>;
  }
}
