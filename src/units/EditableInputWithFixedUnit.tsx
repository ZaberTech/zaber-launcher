import { ConfirmCancel, Icons } from '@zaber/react-library';
import classNames from 'classnames';
import React, { Component, createRef } from 'react';
import { match, P } from 'ts-pattern';

import type { EditableInputMode } from './EditableInputWithUnits';
import { FixedUnitInputProps, InputWithFixedUnit } from './InputWithFixedUnit';

type NonWritingState = { mode: 'displaying' | 'reading' | 'editing'; value: number | null };
type WritingState = { mode: 'writing'; value: number };

export type ValueState = NonWritingState | WritingState;

export function isEditingState(state: EditableInputMode): state is 'editing' | 'writing' {
  return ['editing', 'writing'].includes(state);
}

interface Props extends Omit<FixedUnitInputProps, 'onChange'> {
  mode: EditableInputMode;
  onChange: (update: ValueState) => void;
}

interface State {
  preEditValue: number | null;
}

export class EditableInputWithFixedUnit extends Component<Props, State> {
  private inputRef: React.RefObject<HTMLInputElement>;

  constructor(props: Props) {
    super(props);
    this.state = {
      preEditValue: props.value ?? 0,
    };
    this.inputRef = createRef<HTMLInputElement>();
  }

  onConfirm = () => {
    const { value, onChange } = this.props;
    if (value == null) { return }
    onChange({ mode: 'writing', value });
  };

  onInputKeyDown = (e: React.KeyboardEvent<HTMLInputElement>): void => {
    if (this.props.mode === 'editing') {
      switch (e.key) {
        case 'Enter': this.onConfirm(); break;
        case 'Escape': this.cancelEditing(); break;
      }
    }
  };

  componentDidUpdate(prevProps: Props) {
    if (this.props.mode === 'editing' && prevProps.mode !== 'editing') {
      if (this.inputRef.current) {
        this.inputRef.current.focus();
        this.inputRef.current.select();
      }
    }
  }

  beginEditing = (currentValue: number | null) => {
    this.setState({ preEditValue: currentValue });
    this.props.onChange({ mode: 'editing', value: currentValue });
  };

  cancelEditing = () => this.props.onChange({ mode: 'displaying', value: this.state.preEditValue });

  render() {
    const {
      className, displayPrecision, mode, value, onChange, disabled, title,
      ...inputProps
    } = this.props;
    const rect = this.inputRef.current?.getBoundingClientRect();
    const editing = isEditingState(mode);

    return (
      <InputWithFixedUnit
        {...inputProps}
        className={classNames(className, 'editable-input-with-unit')}
        value={value}
        disabled={mode === 'reading' || disabled}
        ref={this.inputRef}
        displayPrecision={!editing ? displayPrecision : undefined}
        onChange={newValue => match(mode)
          .with(P.union('reading', 'displaying', 'editing'), mode => onChange({ value: newValue, mode }))
          .otherwise(() => null)}
        onKeyDown={this.onInputKeyDown}
        title={title}
      >
        <div className="edit-controls">
          {match(mode)
            .with(P.union('displaying', 'reading'), () => (
              <Icons.Edit
                title={title ? `Edit ${title}` : undefined}
                className="edit"
                disabled={!!disabled || mode === 'reading'}
                onClick={() => this.beginEditing(value)}
              />
            ))
            .with(P.union('editing', 'writing'), () => (
              <ConfirmCancel
                confirmTitle={title ? `Confirm ${title} edit` : undefined} cancelTitle={title ? `Cancel editing ${title}` : undefined}
                confirmDisabled={mode === 'writing' || !Number.isFinite(value)}
                onConfirm={this.onConfirm} onCancel={this.cancelEditing}
              />
            ))
            .exhaustive()
          }
        </div>
        {rect && mode === 'displaying' && !disabled && value != null &&
          <div
            className="click-to-edit"
            style={{ width: `calc(${rect.width}px + var(--value-separator-margin) + var(--input-line-padding))` }}
            onClick={() => this.beginEditing(value)}
          />
        }
      </InputWithFixedUnit>);
  }
}
