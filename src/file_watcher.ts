/* eslint-disable @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-unsafe-call */

import { injectable } from 'inversify';
import filewatcher from 'filewatcher';

export type Callback = (path: string) => void;

@injectable()
export class FileWatcher {
  private watcher;
  private callbacks: _.Dictionary<Callback> = {};

  constructor() {
    this.watcher = filewatcher();
    this.watcher.on('change', this.onChange.bind(this));
  }


  private onChange(path: string) {
    this.callbacks[path]?.(path);
  }


  register(path: string, callback: Callback) {
    const added = !(path in this.callbacks);
    this.callbacks[path] = callback;

    if (added) {
      this.watcher.add(path);
    }
  }


  unregister(path: string) {
    delete this.callbacks[path];
    this.watcher.remove(path);
  }
}
