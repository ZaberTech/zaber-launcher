import { createSelector } from 'reselect';
import _ from 'lodash';
import { ascii } from '@zaber/motion';

import { selectPeripherals } from '../store';
import {
  selectIdentifiedAxes as selectManagerAxes, selectIdentifiedDevices as selectManagerDevices, selectConnections, IdentifiedDeviceState,
  IdentifiedAxisState,
} from '../connection_manager/selectors';
import type {
  DeviceState as ConnectionManagerDeviceState,
} from '../connection_manager/reducer';
import { extractConnectionKey, extractDeviceKey } from '../keys';
import { Capabilities, hasCapability } from '../devices';

import type { AxisState } from './reducer';
import type { SupportedPeripheral } from './types';

export type DeviceState = ConnectionManagerDeviceState;

export interface AxisStateComplete extends AxisState, IdentifiedAxisState {
  device: IdentifiedDeviceState;
  pendingPeripheral: SupportedPeripheral | null;
  connectionLoading: boolean;
  derived: DerivedState;
}

/**
 * Couple of derived helpful indicators.
 */
interface DerivedState {
  /** Peripheral can be automatically detected. */
  isAutoDetectable: boolean;
  /** Controller has capability to detect peripherals. */
  deviceCanAutoDetect: boolean;
  /** Axis has some issue with the peripheral. */
  hasIssue: boolean;
  /** Incompatible peripheral is connected. */
  isIncompatible: boolean;
  /** Axis is unused. */
  isUnused: boolean;
  /** Axis can be disabled at this moment. */
  canDisable: boolean;
  /** Peripheral ID can be set manually. */
  canSetManually: boolean;
  /** New peripheral can be activated. */
  canActivate: boolean;
  /** Connected peripheral can be reactivated. */
  canReactivate: boolean;
}

const selectDerivedState = (axis: Omit<AxisStateComplete, 'derived'>): DerivedState => {
  const { device, identity: { peripheralId }, pendingPeripheral } = axis;
  const isAutoDetectable = hasCapability(axis, Capabilities.PeripheralIsAutodetect);
  const deviceCanAutoDetect = hasCapability(device, Capabilities.DeviceCanAutodetect);
  const hasIssue = axis.monitorInfo?.warningFlags[ascii.WarningFlags.PERIPHERAL_INACTIVE] ?? false;
  const isIncompatible = axis.monitorInfo?.warningFlags[ascii.WarningFlags.PERIPHERAL_NOT_SUPPORTED] ?? false;
  const isUnused = peripheralId === 0;
  const canDisable = !isUnused && !pendingPeripheral;
  const canSetManually = !pendingPeripheral && (hasIssue || !isAutoDetectable);
  const canActivate = hasIssue && !!pendingPeripheral && !isIncompatible;
  const canReactivate = hasIssue && !isAutoDetectable && !pendingPeripheral;
  return {
    isAutoDetectable,
    deviceCanAutoDetect,
    hasIssue,
    isIncompatible,
    isUnused,
    canDisable,
    canSetManually,
    canActivate,
    canReactivate,
  };
};

const selectPendingPeripheral = (axis: AxisState): SupportedPeripheral | null => {
  if (!axis.monitorInfo) {
    return null;
  }
  const { peripheralIdPending } = axis.monitorInfo;
  if (peripheralIdPending === 0) {
    return null;
  }
  const pendingPeripheral = axis.supportedPeripherals?.find(({ peripheralId }) => peripheralId === peripheralIdPending);
  return pendingPeripheral ?? { peripheralId: peripheralIdPending, name: 'Unknown Peripheral' };
};

const selectValidAxes = createSelector(selectPeripherals, selectManagerAxes,
  (state, managerAxes) => _.pickBy(state.axes, axis => managerAxes[axis.key]));

export const selectAxes = createSelector(selectValidAxes, selectManagerAxes, selectManagerDevices, selectConnections,
  (axes, managerAxes, devices, connections) => _.mapValues(axes, _.flow(
    axis => ({
      ...managerAxes[axis.key],
      ...axis,
      device: devices[extractDeviceKey(axis.key)],
      pendingPeripheral: selectPendingPeripheral(axis),
      connectionLoading: connections[extractConnectionKey(axis.key)]?.loadingDevices || false,
    }), (axis): AxisStateComplete => ({
      ...axis,
      derived: selectDerivedState(axis),
    })
  ))
);

export const selectAxesCount = createSelector(selectPeripherals, state => _.size(state.axes));

export const selectAxesErrors = createSelector(selectAxes, axes =>
  _.mapValues(axes, (axis): 'error' | null => axis.derived.hasIssue ? 'error' : null)
);
