import type { SetDeviceStateExceptionData, SetPeripheralStateExceptionData } from '@zaber/motion';
import { Button, Icons, Text, UnorderedList } from '@zaber/react-library';
import React from 'react';

import type { LoadErrorData, SaveLoadTarget } from '../types';
import { useActions } from '../../utils';
import { actions as actionDefinitions } from '../actions';

function isDeviceDetails(details: LoadErrorData['details'] | undefined): details is SetDeviceStateExceptionData {
  return details != null && !('axisNumber' in details);
}

function isPeripheralDetails(details: LoadErrorData['details'] | undefined): details is SetPeripheralStateExceptionData {
  return details != null && 'axisNumber' in details;
}

const AxisErrorDetails: React.FC<{details: SetPeripheralStateExceptionData | SetDeviceStateExceptionData}> = ({ details }) => (
  <div>
    {details.settings.length > 0 && (
      <UnorderedList header={<Text>Could not set the following settings:</Text>}>
        {details.settings}
      </UnorderedList>
    )}
    {details.servoTuning !== '' && <Text>Error setting servo tuning: {details.servoTuning}</Text>}
    {details.storedPositions.length > 0 && (
      <UnorderedList header={<Text>Could not set the following stored positions:</Text>}>
        {details.storedPositions}
      </UnorderedList>
    )}
    {details.storage.length > 0 && (
      <UnorderedList header={<Text>Could not restore storage:</Text>}>
        {details.storage}
      </UnorderedList>
    )}
  </div>
);

export const LoadErrorDetails: React.FC<{details: LoadErrorData['details']}> = ({ details }) => {
  if (isDeviceDetails(details)) {
    return <div>
      <AxisErrorDetails details={details}/>
      {details.streamBuffers.length > 0 && (
        <UnorderedList header={<Text>The following errors occurred setting stream buffers:</Text>}>
          {details.streamBuffers}
        </UnorderedList>
      )}
      {details.pvtBuffers.length > 0 && (
        <UnorderedList header={<Text>The following errors occurred setting PVT buffers:</Text>}>
          {details.pvtBuffers}
        </UnorderedList>
      )}
      {details.triggers.length > 0 && (
        <UnorderedList header={<Text>The following errors occurred setting triggers:</Text>}>
          {details.triggers}
        </UnorderedList>
      )}
      {details.peripherals.map(e => <div className="indent" key={e.axisNumber}>
        <Text e={Text.Emphasis.Bold}>Axis {e.axisNumber}</Text>
        <AxisErrorDetails details={e}/>
      </div>)}
    </div>;
  } else if (isPeripheralDetails(details)) {
    return <AxisErrorDetails details={details}/>;
  } else {
    return null;
  }
};

export const LoadError: React.FC<{ error: LoadErrorData; target: SaveLoadTarget }> = ({
  target, error: { message, details, recommendRestore },
}) => {
  const actions = useActions(actionDefinitions);
  return (
    <div className="info-grid load-errors">
      <Icons.ErrorFault className="icon error"/>
      <Text t={Text.Type.H4} className="title">Failed</Text>
      <div className="info">
        <Text>{message}</Text>
        <LoadErrorDetails details={details}/>
        {recommendRestore && <>
          <div>
          The partial application of the state may have left the {target.description} in inconsistent configuration.
          If your device is not working as expected following this partial state application,
          you may want to factory restore the {target.description},
          which will reset all settings to factory defaults.
          </div>
          <Button color="grey" onClick={() => actions.restore(target)}>Factory Reset</Button>
        </>}
      </div>
    </div>);
};
