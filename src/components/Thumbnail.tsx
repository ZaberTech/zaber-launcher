import { tryAccess } from '@zaber/toolbox';
import React from 'react';
import { useSelector } from 'react-redux';

import unknownDevice from '../assets/unknown_device.svg?url';
import rotaryDevice from '../assets/rotary_device.svg?url';
import linearDevice from '../assets/linear_device.svg?url';
import { selectThumbnails } from '../connection_manager';
import { is3rdPartyId, isRotary3rdPartyId } from '../hardware_modification/peripheral_id_picker/peripheralIdMapping';

type ImgProps = React.DetailedHTMLProps<React.ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>;
interface Props extends Omit<ImgProps, 'src'> {
  deviceOrPeripheralId: number | undefined;
  title?: string;
  altExt: 'device' | 'axis' | 'peripheral';
}

export const Thumbnail: React.FC<Props> = ({ deviceOrPeripheralId, title, altExt, className, ...rest }) => {
  const src = tryAccess(useSelector(selectThumbnails), deviceOrPeripheralId);

  if (is3rdPartyId(deviceOrPeripheralId)) {
    if (isRotary3rdPartyId(deviceOrPeripheralId)) {
      return <img src={rotaryDevice} alt="Rotary third party peripheral" title={title} {...rest} className={className}/>;
    } else {
      return <img src={linearDevice} alt="Linear third party peripheral" title={title} {...rest} className={className}/>;
    }
  }

  if (src == null) {
    return <img src={unknownDevice} alt={`Unknown ${altExt}`} title={title} {...rest} className={className}/>;
  }

  // Try to use the provided thumbnail. If for any reason fetching the image fails, fall back to the default
  return (
    <object data={src} title={title} type="image/png" className={className}>
      <img src={unknownDevice} alt={`Generic ${altExt}`} {...rest}/>
    </object>
  );
};
