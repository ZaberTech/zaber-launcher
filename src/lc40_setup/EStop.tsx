import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Icons, NoticeMessage, NoticeBorder, Button, HeaderCard, Text } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import { match, isMatching } from 'ts-pattern';

import { actions as actionsDefinition } from './actions';
import { selectEstop, selectValidationErr } from './selectors';

export const EStop: React.FC<{ validate: boolean }> = ({ validate }) => {
  const actions = useActions(actionsDefinition);
  const estop = useSelector(selectEstop)!;
  const validationErr = useSelector(selectValidationErr)!;
  const hasValidationErr = validate && validationErr.estop != null;

  useEffect(() => {
    actions.estopMounted();
    return () => { actions.estopUnmounted() };
  }, []);

  return (
    <NoticeBorder type={isMatching({ status: 'error' }, estop) || hasValidationErr ? 'error' : null}>
      <HeaderCard className="section" header={<Text e={Text.Emphasis.Bold}>Stage Type</Text>}>
        <div className="estop">
          {match(estop)
            .with({ status: 'ok', ready: true }, () =>
              <div className="connected">
                  Connected <Icons.Tick/>
              </div>
            ).with({ status: 'enabling' }, () =>
              <div className="checking">
                  Checking...
                <Button disabled>Check</Button>
              </div>
            ).otherwise(() =>
              <div className="disconnected">
                  Disconnected <Icons.Cross/>
                <Button onClick={actions.enableDriver}>Check</Button>
              </div>)}
        </div>
      </HeaderCard>

      {match(estop)
        .with({ status: 'error' }, ({ error }) =>
          <NoticeMessage className="lc40-error-message">
            Cannot determine E-Stop status: {error}
          </NoticeMessage>)
        .when(() => hasValidationErr, () =>
          <NoticeMessage className="lc40-error-message">
            {validationErr.estop}
          </NoticeMessage>
        ).otherwise(() => null)}
    </NoticeBorder>
  );
};
