import React from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, Text, SimpleSelect } from '@zaber/react-library';

import { useActions } from '../utils';

import { selectMonitorState, selectObjectiveLabels } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations } from './types';
import { useOperationValue } from './hooks';
import { OperationError, NextPrevious } from './components';
import { ObjectiveChangerSettings } from './ObjectiveChangerSettings';

export const ObjectiveChanger: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.objectiveChanger;
  const info = monitor.info?.objectiveChanger;
  const labels = useSelector(selectObjectiveLabels);

  const [objective, setObjective, inProgress] = useOperationValue(info?.objective ?? 0, Operations.changeObjective);

  if (stableInfo == null || info == null) {
    return null;
  }

  const options = labels.map((label, i) => ({ label, value: i + 1 }));
  if (info.objective === 0) {
    options.push({ label: '', value: 0 });
  }

  const changeTo = (objective: number) => {
    if (objective === 0) { return }
    setObjective(objective);
    actions.operationStart(Operations.changeObjective, { objective });
  };

  return <HeaderCard
    className="controls objective-changer"
    header={<><Text t={Text.Type.H4}>Objective Changer</Text><ObjectiveChangerSettings/></>}>
    <div className="select-row">
      <SimpleSelect
        title="Current Objective" portalToBody
        disabled={inProgress}
        value={objective}
        onValueChange={changeTo}
        options={options}/>
      <NextPrevious current={objective} changeTo={changeTo} numberOf={stableInfo.numberOf}/>
      {inProgress && <Text>Changing...</Text>}
    </div>
    <OperationError operation={Operations.changeObjective}/>
  </HeaderCard>;
};
