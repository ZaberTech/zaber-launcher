const FILENAME = 'Foo.csv';

const mockOpenDialog = jest.fn(() => ({
  cancelled: false,
  filePaths: [FILENAME],
}));

jest.mock('../dialogs', () => ({
  Dialogs: {
    showOpenDialog: mockOpenDialog,
  },
}));

let fileContent: string = '';

const fsMock = {
  readFile: jest.fn(async () => fileContent),
};

jest.mock('fs/promises', () => fsMock);

import { EChartsReactMock } from './testing/mocks';

jest.mock('echarts-for-react', () => EChartsReactMock);

import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import type { Container } from 'inversify';

import { createContainer, destroyContainer } from '../container';
import { getTableCells, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';

import { FILE_MENU_ID, OPEN_FILE_LABEL, PvtApp, TABLE_BUTTON_LABEL } from './PvtApp';
import { FileWatcher } from '../file_watcher';
import { FileWatcherMock } from '../test/mocks/file_watcher';
import { IMPORT_BUTTON_LABEL } from './CsvImportDialog';
import { Length, Velocity, type Units, Angle, AngularVelocity } from '@zaber/motion';

const file1 = '0,1,2\n' +
  '100,3,4\n' +
  '200,5,6\n';

const file2 = '0,1,2\n' +
  '200,3,4\n' +
  '100,5,6\n';

let container: Container;
const TestApp = wrapWithNewStore(wrapWithRouter(PvtApp));
let wrapper: RenderResult;


async function loadFile(content: string) {
  fileContent = content;
  fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
  fireEvent.click(wrapper.getByText(OPEN_FILE_LABEL));
  await waitUntilPass(() => wrapper.getByText(IMPORT_BUTTON_LABEL));
}


function selectTimeMode(accumulated: boolean) {
  if (accumulated) {
    fireEvent.click(wrapper.getByTestId('accumulated-time-mode'));
  } else {
    fireEvent.click(wrapper.getByTestId('segment-time-mode'));
  }
}


function selectUnit(title: string, unit: Units) {
  fireEvent.change(wrapper.getByTitle(title), { target: { value: unit } });
}


function finishImport() {
  fireEvent.click(wrapper.getByText(IMPORT_BUTTON_LABEL));
  fireEvent.click(wrapper.getByTitle(TABLE_BUTTON_LABEL));
}


beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(FileWatcher).to(FileWatcherMock);
  wrapper = render(<TestApp/>);
});


afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});


describe('Importing file with no headers', () => {
  beforeEach(async () => {
    await loadFile(file1);
  });


  it('accumulated time mode works and is default where possible', () => {
    selectUnit('Length Unit Select', Length.mm);
    selectUnit('Velocity Unit Select', Velocity.MILLIMETRES_PER_SECOND);
    finishImport();
    const table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
    expect(table[3][2]).toBe('0.1');
    expect(table[4][2]).toBe('0.2');
  });


  it('segment time mode works', () => {
    selectUnit('Length Unit Select', Length.mm);
    selectUnit('Velocity Unit Select', Velocity.MILLIMETRES_PER_SECOND);
    selectTimeMode(false);
    finishImport();
    const table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
    expect(table[3][2]).toBe('0.1');
    expect(table[4][2]).toBe('0.3');
  });


  it('allows selecting both linear and rotary units', () => {
    selectUnit('Length Unit Select', Length.mm);
    selectUnit('Length Unit Select', Angle.DEGREES);
    selectUnit('Velocity Unit Select', Velocity.MILLIMETRES_PER_SECOND);
    selectUnit('Velocity Unit Select', AngularVelocity.RADIANS_PER_SECOND);
  });


  it('disables OK button if incompatible units are selected', () => {
    selectUnit('Length Unit Select', Length.mm);
    selectUnit('Velocity Unit Select', AngularVelocity.RADIANS_PER_SECOND);
    expect(wrapper.getByText(IMPORT_BUTTON_LABEL)).toHaveAttribute('disabled');
    selectUnit('Velocity Unit Select', Velocity.CENTIMETRES_PER_SECOND);
    expect(wrapper.getByText(IMPORT_BUTTON_LABEL)).not.toHaveAttribute('disabled');
  });
});


test('segment time is the default if the data makes sense', async () => {
  await loadFile(file2);
  selectUnit('Length Unit Select', Length.mm);
  selectUnit('Velocity Unit Select', Velocity.MILLIMETRES_PER_SECOND);
  finishImport();
  const table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
  expect(table[3][2]).toBe('0.2');
  expect(table[4][2]).toBe('0.3');
});
