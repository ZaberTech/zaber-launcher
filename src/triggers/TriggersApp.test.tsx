/* eslint-disable camelcase */
import { injectable, type Container } from 'inversify';
import { RenderResult, fireEvent, getByText, render } from '@testing-library/react';
import React from 'react';
import { IoPortType, TriggerCondition, TriggerEnabledState, TriggerOperation, TriggerState } from '@zaber/motion/ascii';
import { Acceleration, Length, Measurement, Time, Voltage } from '@zaber/motion';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase
} from '../test/mocks/ascii';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import {
  MOCK_DEVICE_NUMBER, MOCK_SERIAL_PORT, mockProduct, mockSingleDevice, mockSingleDeviceWithPeripherals
} from '../connection_manager/mocks';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import { createContainer, destroyContainer } from '../container';
import { makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';
import { actionDefinitions as pollingActions } from '../polling/actions';

import { TriggersApp } from './TriggersApp';
import type { TriggerActionState, TriggerConditionState } from './reducer';
import { getSettingAxes } from './utils';


window.HTMLElement.prototype.scrollIntoView = jest.fn();

let hasTriggers = true;

jest.mock('../devices', () => ({
  commandExists: jest.fn((command: string) => {
    if (command === 'trigger show') {
      return hasTriggers;
    } else if (command === 'trigger number when abs') {
      return true;
    } else if (command === 'trigger number action triggeract settingName assignmentoperator setting') {
      return true;
    } else if (command === 'storage print prefix') {
      return true;
    }
    return false;
  }),
  isMobile: jest.fn(() => true),
}));

jest.mock('./utils', () => ({
  ...jest.requireActual('./utils'),
  getSettingAxes: jest.fn(() => [1, 2, 3, 4]),
}));

const DEVICE_NAME = 'X-MCC4';

let container: Container;

class AxisMock extends AxisMockBase {
}

let device: DeviceMock;
let deviceCallback: (device: DeviceMock) => void;

const defaultTriggerState: TriggerState = {
  condition: 'when none',
  actions: ['action a none', 'action b none'],
  enabled: false,
  firesTotal: 0,
  firesRemaining: 0,
};

const getActionLetter = (index: number): string => String.fromCharCode(97 + index);

const triggerOperationToOperation = (operation: TriggerOperation): string => {
  switch (operation) {
    case TriggerOperation.SET_TO: return '=';
    case TriggerOperation.INCREMENT_BY: return '+=';
    case TriggerOperation.DECREMENT_BY: return '-=';
  }
};

const triggerOperatorToOperator = (operator: TriggerCondition): string => {
  switch (operator) {
    case TriggerCondition.EQ: return '==';
    case TriggerCondition.NE: return '<>';
    case TriggerCondition.LT: return '<';
    case TriggerCondition.LE: return '<=';
    case TriggerCondition.GT: return '>';
    case TriggerCondition.GE: return '>=';
  }
};

const triggerIoTypeToIoType = (ioType: IoPortType): string => {
  switch (ioType) {
    case IoPortType.ANALOG_INPUT: return 'ai';
    case IoPortType.ANALOG_OUTPUT: return 'ao';
    case IoPortType.DIGITAL_INPUT: return 'di';
    case IoPortType.DIGITAL_OUTPUT: return 'do';
    case IoPortType.NONE: return '';
  }
};

class TriggerMock {
  constructor(device: DeviceMock, state: TriggerState = defaultTriggerState) {
    this.device = device;
    this.state = state;
  }

  device: DeviceMock;
  state: TriggerState;
  label: string = '';

  getState = jest.fn((): Promise<TriggerState> => Promise.resolve(this.state));
  enable = jest.fn((count: number): Promise<void> => {
    const fires = count === 0 ? -1 : count;
    this.state = {
      ...this.state,
      enabled: true,
      firesRemaining: fires,
      firesTotal: fires,
    };
    return Promise.resolve();
  });
  disable = jest.fn((): Promise<void> => {
    this.state = {
      ...this.state,
      enabled: false,
      firesRemaining: 0,
      firesTotal: 0,
    };
    return Promise.resolve();
  });
  testDecrementFiresRemaining = jest.fn((): void => {
    if (this.state.firesRemaining > 0) {
      this.state = {
        ...this.state,
        enabled: this.state.firesRemaining > 1,
        firesRemaining: this.state.firesRemaining - 1,
      };
    }
  });
  fireWhenSetting = jest.fn((axis: number, setting: string, operator: TriggerCondition, value: number): Promise<void> => {
    this.state = {
      ...this.state,
      condition: `when ${axis} ${setting} ${triggerOperatorToOperator(operator)} ${value}`,
    };
    return Promise.resolve();
  });
  fireWhenAbsoluteSetting = jest.fn((axis: number, setting: string, operator: TriggerCondition, value: number): Promise<void> => {
    this.state = {
      ...this.state,
      condition: `when ${axis} abs ${setting} ${triggerOperatorToOperator(operator)} ${value}`,
    };
    return Promise.resolve();
  });
  fireWhenDistanceTravelled = jest.fn((axis: number, value: number): Promise<void> => {
    this.state = {
      ...this.state,
      condition: `when ${axis} dist ${value}`,
    };
    return Promise.resolve();
  });
  fireWhenEncoderDistanceTravelled = jest.fn((axis: number, value: number): Promise<void> => {
    this.state = {
      ...this.state,
      condition: `when ${axis} encoder dist ${value}`,
    };
    return Promise.resolve();
  });
  fireWhenIo = jest.fn((ioType: IoPortType, channel: number, operator: TriggerCondition, value: number): Promise<void> => {
    this.state = {
      ...this.state,
      condition: `when io ${triggerIoTypeToIoType(ioType)} ${channel} ${triggerOperatorToOperator(operator)} ${value}`,
    };
    return Promise.resolve();
  });
  fireAtInterval = jest.fn((time: number): Promise<void> => {
    this.state = {
      ...this.state,
      condition: `when time ${time}`,
    };
    return Promise.resolve();
  });
  clearAction = jest.fn((actionNumber: number): Promise<void> => {
    const actionIndex = actionNumber - 1;
    this.state = {
      ...this.state,
      actions: [
        ...this.state.actions.slice(0, actionIndex),
        `action ${getActionLetter(actionIndex)} none`,
        ...this.state.actions.slice(actionNumber),
      ]
    };
    return Promise.resolve();
  });
  onFire = jest.fn((actionNumber: number, axis: number, command: string): Promise<void> => {
    const actionIndex = actionNumber - 1;
    this.state = {
      ...this.state,
      actions: [
        ...this.state.actions.slice(0, actionIndex),
        `action ${getActionLetter(actionIndex)} ${axis} ${command}`,
        ...this.state.actions.slice(actionNumber),
      ]
    };
    return Promise.resolve();
  });
  onFireSet = jest.fn((actionNumber: number, axis: number, setting: string, operation: TriggerOperation, value: number): Promise<void> => {
    const actionIndex = actionNumber - 1;
    this.state = {
      ...this.state,
      actions: [
        ...this.state.actions.slice(0, actionIndex),
        `action ${getActionLetter(actionIndex)} ${axis} ${setting} ${triggerOperationToOperation(operation)} ${value}`,
        ...this.state.actions.slice(actionNumber),
      ]
    };
    return Promise.resolve();
  });
  onFireSetToSetting =
    jest.fn((actionNumber: number, axis: number, setting: string, operation: TriggerOperation, fromAxis: number, fromSetting: string):
      Promise<void> => {
      const actionIndex = actionNumber - 1;
      this.state = {
        ...this.state,
        actions: [
          ...this.state.actions.slice(0, actionIndex),
          // eslint-disable-next-line max-len
          `action ${getActionLetter(actionIndex)} ${axis} ${setting} ${triggerOperationToOperation(operation)} setting ${fromAxis} ${fromSetting}`,
          ...this.state.actions.slice(actionNumber),
        ]
      };
      return Promise.resolve();
    });
  setLabel = jest.fn((label: string): Promise<void> => {
    this.label = label;
    return Promise.resolve();
  });
  getLabel = jest.fn((): Promise<string> => Promise.resolve(this.label));
}

class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    device = this;
    if (deviceCallback) { deviceCallback(this) }
  }

  triggerObjects: TriggerMock[] = [
    new TriggerMock(this),
    new TriggerMock(this),
    new TriggerMock(this),
    new TriggerMock(this),
    new TriggerMock(this),
    new TriggerMock(this)
  ];

  ioInfo = {
    numberAnalogOutputs: 4,
    numberAnalogInputs: 4,
    numberDigitalOutputs: 4,
    numberDigitalInputs: 4,
  };

  io = {
    getChannelsInfo: jest.fn(() => Promise.resolve(this.ioInfo))
  };

  triggers = {
    getNumberOfTriggers: jest.fn((): Promise<number> => Promise.resolve(this.triggerObjects.length)),
    getEnabledStates: jest.fn(async (): Promise<TriggerEnabledState[]> =>
      await Promise.all(this.triggerObjects.map(async trigger => await trigger.getState()))),
    getTrigger: jest.fn((triggerNumber: number) => this.triggerObjects[triggerNumber - 1]),
    getAllLabels: jest.fn(async (): Promise<string[]> =>
      await Promise.all(this.triggerObjects.map(async trigger => await trigger.getLabel()))),
  };

  prepareCommand = jest.fn(async (cmd: string, measurement: Measurement) => cmd.replace('?', (measurement.value * 100).toString()));
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const connectionKey = makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), MOCK_SERIAL_PORT);
const deviceKey = makeDeviceKey(connectionKey, MOCK_DEVICE_NUMBER);

const TestTriggersApp = wrapWithNewStore(wrapWithRouter(TriggersApp));

let wrapper: RenderResult;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

  wrapper = render(<TestTriggersApp/>);

  const mockedProduct = mockProduct({ mobile: true, encoder: true });
  mockSingleDeviceWithPeripherals(TestTriggersApp.testStore, {
    modifier: info => ({
      ...info,
      product: {
        ...mockedProduct,
        commandTree: {
          command: '', nodes: [
            {
              command: 'trigger',
              nodes: [{
                command: 'number',
                nodes: [{
                  command: 'action',
                  nodes: [{
                    command: 'triggeract',
                    nodes: [
                      {
                        command: 'scope',
                        nodes: [{
                          command: 'start',
                          is_command: true,
                        }]
                      },
                      {
                        command: 'stop',
                        is_command: true,
                      },
                      {
                        command: 'move',
                        nodes: [
                          {
                            command: 'abs',
                            nodes: [
                              {
                                command: 'positionExt',
                                is_command: true,
                                is_param: true,
                                param_arity: 1,
                                contextual_dimension_id: 1,
                              }
                            ]
                          },
                          {
                            command: 'rel',
                            nodes: [{
                              command: 'positionExt',
                              is_command: true,
                              is_param: true,
                            }]
                          }
                        ]
                      }
                    ]
                  }]
                }, {
                  command: 'when',
                  nodes: [{
                    command: 'io',
                    nodes: [{
                      command: 'ai',
                      nodes: [{
                        command: 'channel',
                        nodes: [{
                          command: 'relationaloperator',
                          nodes: [{
                            command: 'value',
                            contextual_dimension_id: 14,
                          }]
                        }]
                      }]
                    }]
                  },
                  {
                    command: 'time',
                    nodes: [{
                      command: 'period',
                      contextual_dimension_id: 11,
                    }]
                  }],
                }],
              }],
            }
          ],
        },
        conversionTable: {
          rows: [
            ...mockedProduct.conversionTable.rows,
            {
              contextual_dimension_id: 1,
              dimension_name: 'Length',
              contextual_dimension_name: '',
              function_name: '',
              scale: null
            }
          ]
        },
      },
    })
  });
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;

  device = null!;
  deviceCallback = null!;
});

describe('Load triggers', () => {
  test('shows welcome message', async () => {
    wrapper.getByText('Welcome to Triggers');
  });

  test('Shows loading trigger message', async () => {
    deviceCallback = device => {
      device.triggers.getNumberOfTriggers = jest.fn(() =>
        new Promise(resolve => {
          setTimeout(() => resolve(6), 1000);
        }));
    };
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      wrapper.getByText(DEVICE_NAME);
      wrapper.getByText('Loading...');
    });
  });

  test('Device does not have triggers', async () => {
    hasTriggers = false;
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      wrapper.getByText(DEVICE_NAME);
      wrapper.getByText('The selected device does not support triggers.');
    });
    hasTriggers = true;
  });

  test('Loads 6 triggers', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      expect(wrapper.getAllByText(/Trigger \d+/).length).toBe(6);
    });
  });

  test('Trigger has error loading', async () => {
    deviceCallback = device => {
      device.triggers.getNumberOfTriggers = jest.fn(() => {
        throw new Error('Test Error');
      });
    };
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      wrapper.getByText(DEVICE_NAME);
      wrapper.getByText('Test Error');
    });
  });

  test('Loads triggers when loading labels fails', async () => {
    deviceCallback = device => {
      device.triggers.getAllLabels = jest.fn(() => {
        throw new Error('Test Error');
      });
    };
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      toggles.forEach(toggle => {
        expect(toggle).toHaveClass('toggle-off');
        expect(toggle).not.toHaveClass('loading');
      });
    });
  });
});

describe('Trigger enabled state', () => {
  beforeEach(async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
  });

  test('Load all triggers disabled', async () => {
    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      toggles.forEach(toggle => {
        expect(toggle).toHaveClass('toggle-off');
        expect(toggle).not.toHaveClass('loading');
      });
    });
    await waitUntilPass(() => {
      const countInputs = wrapper.getAllByTitle('Trigger Count Input');
      countInputs.forEach(input => {
        expect(input).toHaveValue('');
      });
    });
  });

  test('Load some triggers enabled', async () => {
    deviceCallback = device => {
      device.triggerObjects[0].state = { ...device.triggerObjects[0].state, enabled: true, firesRemaining: -1, firesTotal: -1 };
      device.triggerObjects[2].state = { ...device.triggerObjects[2].state, enabled: true, firesRemaining: 10, firesTotal: 12 };
    };

    TestTriggersApp.testStore.dispatch(pollingActions.pollNow());

    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      expect(toggles[0]).toHaveClass('toggle-on');
      expect(toggles[1]).toHaveClass('toggle-off');
      expect(toggles[2]).toHaveClass('toggle-on');
      expect(toggles[3]).toHaveClass('toggle-off');
      expect(toggles[4]).toHaveClass('toggle-off');
      expect(toggles[5]).toHaveClass('toggle-off');

      const countTexts = wrapper.getAllByTitle('Trigger Count');
      expect(countTexts[0]).toHaveTextContent('∞');
      expect(countTexts[1]).toHaveTextContent('10 / 12');

      const countInputs = wrapper.getAllByTitle('Trigger Count Input');
      countInputs.forEach(input => {
        expect(input).toHaveValue('');
      });
    });
  });

  test('Enable trigger infinite', async () => {
    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      toggles.forEach(toggle => {
        expect(toggle).toHaveClass('toggle-off');
        expect(toggle).not.toHaveClass('loading');
      });
    });

    const toggle = wrapper.getAllByTitle('Enable Trigger Toggle')[0];
    fireEvent.click(toggle);

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('loading');
    });

    TestTriggersApp.testStore.dispatch(pollingActions.pollNow());

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('toggle-on');
      expect(toggle).not.toHaveClass('loading');
      expect(wrapper.getAllByTitle('Trigger Count')[0]).toHaveTextContent('∞');
    });
  });

  test('Enable trigger for count', async () => {
    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      toggles.forEach(toggle => {
        expect(toggle).toHaveClass('toggle-off');
        expect(toggle).not.toHaveClass('loading');
      });
    });

    await waitUntilPass(() => {
      const countInputs = wrapper.getAllByTitle('Trigger Count Input');
      fireEvent.change(countInputs[0], { target: { value: '10' } });
      fireEvent.change(countInputs[2], { target: { value: '12' } });
    });
    const toggle = wrapper.getAllByTitle('Enable Trigger Toggle')[2];
    fireEvent.click(toggle);

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('loading');
    });

    TestTriggersApp.testStore.dispatch(pollingActions.pollNow());

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('toggle-on');
      expect(toggle).not.toHaveClass('loading');
      expect(wrapper.getAllByTitle('Trigger Count')[0]).toHaveTextContent('12 / 12');
    });
  });

  test('Disable infinite trigger', async () => {
    deviceCallback = device => {
      device.triggerObjects[1].state = { ...device.triggerObjects[1].state, enabled: true, firesRemaining: -1, firesTotal: -1 };
    };

    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      expect(toggles[1]).not.toHaveClass('loading');
      expect(toggles[1]).toHaveClass('toggle-on');
      expect(wrapper.getAllByTitle('Trigger Count')[0]).toHaveTextContent('∞');
    });

    const toggle = wrapper.getAllByTitle('Enable Trigger Toggle')[1];
    fireEvent.click(toggle);

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('loading');
    });

    TestTriggersApp.testStore.dispatch(pollingActions.pollNow());

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('toggle-off');
      expect(toggle).not.toHaveClass('loading');
      expect(wrapper.getAllByTitle('Trigger Count Input')[1]).toHaveTextContent('');
    });
  });

  test('Disable trigger with count', async () => {
    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      toggles.forEach(toggle => {
        expect(toggle).toHaveClass('toggle-off');
        expect(toggle).not.toHaveClass('loading');
      });
    });

    await waitUntilPass(() => {
      const countInputs = wrapper.getAllByTitle('Trigger Count Input');
      fireEvent.change(countInputs[4], { target: { value: '4' } });
    });
    const toggle = wrapper.getAllByTitle('Enable Trigger Toggle')[4];
    fireEvent.click(toggle);

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('loading');
    });

    TestTriggersApp.testStore.dispatch(pollingActions.pollNow());

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('toggle-on');
      expect(toggle).not.toHaveClass('loading');
      expect(wrapper.getAllByTitle('Trigger Count')[0]).toHaveTextContent('4 / 4');
    });

    fireEvent.click(toggle);

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('loading');
    });

    TestTriggersApp.testStore.dispatch(pollingActions.pollNow());

    await waitUntilPass(() => {
      expect(toggle).toHaveClass('toggle-off');
      expect(toggle).not.toHaveClass('loading');
      expect(wrapper.getAllByTitle('Trigger Count Input')[4]).toHaveValue('4');
    });
  });

  test('Trigger counts down', async () => {
    deviceCallback = device => {
      device.triggerObjects[0].state = { ...defaultTriggerState, enabled: true, firesRemaining: 2, firesTotal: 4 };
    };

    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      expect(toggles[0]).toHaveClass('toggle-on');
      expect(wrapper.getAllByTitle('Trigger Count')[0]).toHaveTextContent('2 / 4');
    });

    device.triggerObjects[0].testDecrementFiresRemaining();

    TestTriggersApp.testStore.dispatch(pollingActions.pollNow());

    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      expect(toggles[0]).toHaveClass('toggle-on');
      expect(wrapper.getAllByTitle('Trigger Count')[0]).toHaveTextContent('1 / 4');
    });

    device.triggerObjects[0].testDecrementFiresRemaining();

    TestTriggersApp.testStore.dispatch(pollingActions.pollNow());

    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      expect(toggles[0]).toHaveClass('toggle-off');
      expect(wrapper.getAllByTitle('Trigger Count Input')[0]).toHaveValue('4');
    });
  });

  test('Trigger cannot be enabled with unsaved changes', async () => {
    await waitUntilPass(() => {
      const expandArrows = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(expandArrows[0]);
    });

    await selectOption('Select Action A Type', 'Command');

    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      expect(toggles[0]).toHaveClass('disabled');
    });
  });

  test('Trigger can be disabled with unsaved changes', async () => {
    await waitUntilPass(() => {
      const expandArrows = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(expandArrows[0]);
    });

    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      expect(toggles[0]).not.toHaveClass('loading');
      fireEvent.click(toggles[0]);
    });

    TestTriggersApp.testStore.dispatch(pollingActions.pollNow());

    await selectOption('Select Action A Type', 'Command');

    await waitUntilPass(() => {
      const toggles = wrapper.getAllByTitle('Enable Trigger Toggle');
      expect(toggles[0]).not.toHaveClass('disabled');
    });
  });
});

const selectOption = async (selectorTitle: string, optionText: string) => {
  await waitUntilPass(() => {
    wrapper.getByTitle(selectorTitle);
  });

  const selector = wrapper.getByTitle(selectorTitle);
  const option = getByText(selector, optionText) as HTMLOptionElement;
  fireEvent.change(selector, { target: { value: option.value } });

  await waitUntilPass(() => {
    expect(selector).toHaveAttribute('data-current', optionText);
  });
};

describe('Trigger Labels', () => {
  test('Default trigger labels load', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      expect(wrapper.getAllByTitle('Expand Trigger')).toHaveLength(6);
    });

    await waitUntilPass(() => {
      expect(device.triggers.getAllLabels).toHaveBeenCalled();
      wrapper.getByText('Trigger 1');
      wrapper.getByText('Trigger 2');
      wrapper.getByText('Trigger 3');
      wrapper.getByText('Trigger 4');
      wrapper.getByText('Trigger 5');
      wrapper.getByText('Trigger 6');
    });
  });

  test('Custom trigger label loads', async () => {
    deviceCallback = device => {
      device.triggerObjects[2].label = 'My Trigger';
    };

    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      expect(wrapper.getAllByTitle('Expand Trigger')).toHaveLength(6);
    });

    await waitUntilPass(() => {
      expect(device.triggers.getAllLabels).toHaveBeenCalled();
      wrapper.getByText('My Trigger');
      wrapper.getByText('Trigger 3');
    });
  });

  test('Save custom trigger label', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      expect(wrapper.getAllByTitle('Expand Trigger')).toHaveLength(6);
    });

    fireEvent.click(wrapper.getAllByTitle('Edit Value')[1]);
    fireEvent.change(wrapper.getByTitle('Trigger Label'), { target: { value: 'My Trigger' } });
    fireEvent.click(wrapper.getByTitle('Confirm'));

    await waitUntilPass(() => {
      expect(device.triggerObjects[1].setLabel).toHaveBeenCalled();
      expect(device.triggerObjects[1].label).toBe('My Trigger');
      expect(device.triggerObjects[1].getLabel).toHaveBeenCalled();
      wrapper.getByText('My Trigger');
    });
  });
});

describe('Trigger details', () => {
  beforeEach(async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      expect(wrapper.getAllByTitle('Expand Trigger')).toHaveLength(6);
    });
  });

  test('Trigger details expand and collapse', async () => {
    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Trigger Details')).toBeNull();
    });

    fireEvent.click(wrapper.getAllByTitle('Expand Trigger')[0]);

    await waitUntilPass(() => {
      expect(wrapper.getAllByTitle('Trigger Details')).toHaveLength(1);
    });

    fireEvent.click(wrapper.getByTitle('Collapse Trigger'));

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Trigger Details')).toBeNull();
    });
  });

  test('Changing a field makes save controls appear', async () => {
    fireEvent.click(wrapper.getAllByTitle('Expand Trigger')[0]);

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await selectOption('Select Condition Type', 'Trajectory Position');

    await waitUntilPass(() => {
      wrapper.getByTitle('Save Controls');
    });
  });

  test('Input restored when pressing cancel', async () => {
    fireEvent.click(wrapper.getAllByTitle('Expand Trigger')[0]);

    await selectOption('Select Action A Type', 'Command');

    await waitUntilPass(() => {
      const cancelButton = wrapper.getByTitle('Cancel Changes');
      fireEvent.click(cancelButton);
    });

    await waitUntilPass(async () => {
      const selectorActionType = wrapper.getByTitle('Select Action A Type');
      expect(selectorActionType).toHaveAttribute('data-current', 'None');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });
  });

  test('Input restored when trigger details collapsed', async () => {
    fireEvent.click(wrapper.getAllByTitle('Expand Trigger')[0]);

    await selectOption('Select Action B Type', 'Set Setting to Setting');
    await selectOption('Select Setting', 'accel');

    fireEvent.click(wrapper.getByTitle('Collapse Trigger'));

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Trigger Details')).toBeNull();
    });

    fireEvent.click(wrapper.getAllByTitle('Expand Trigger')[0]);

    await waitUntilPass(async () => {
      const selectorActionType = wrapper.getByTitle('Select Action B Type');
      expect(selectorActionType).toHaveAttribute('data-current', 'None');
    }, 3000);

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });
  });
});

const inputValue = async (value: number, units: string = '') => {
  if (units !== '') {
    await selectOption('Units of Condition Value', units);
  }

  await waitUntilPass(() => {
    wrapper.getByTitle('Value of Condition Value');
  });

  const valueInput = wrapper.getByTitle('Value of Condition Value');
  fireEvent.change(valueInput, { target: { value } });

  await waitUntilPass(() => {
    expect(valueInput).toHaveValue(value);
  });
};

const checkCondition = async (condition: TriggerConditionState) => {
  await waitUntilPass(() => {
    expect(TestTriggersApp.testStore.getState().triggers.triggers[0].conditionInput).toEqual(condition);
  });
};

describe('Trigger Conditions Input', () => {
  beforeEach(async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });
  });

  test('Trigger condition trajectory position', async () => {
    await selectOption('Select Condition Type', 'Trajectory Position');
    await selectOption('Select Axis', 'Axis 2');
    await selectOption('Select Operator', '>');
    await inputValue(10);

    await checkCondition({
      type: 'Trajectory Position',
      axis: 2,
      operator: '>',
      value: { value: 10, units: Length.MILLIMETRES }
    });
  });

  test('Trigger condition distance Interval', async () => {
    await selectOption('Select Condition Type', 'Distance Interval');
    await selectOption('Select Axis', 'Axis 3');
    await inputValue(1, 'm');

    await checkCondition({
      type: 'Distance Interval',
      axis: 3,
      value: { value: 1, units: Length.METRES }
    });
  });

  test('Trigger condition IO Channel Analog', async () => {
    await selectOption('Select Condition Type', 'IO Channel');
    await selectOption('Select IO Type', 'Analog Input');
    await selectOption('Select IO Channel', 'Channel 4');
    await selectOption('Select Operator', '≤');
    await inputValue(2.5);

    await checkCondition({
      type: 'IO Channel',
      ioType: 'Analog Input',
      ioChannel: 4,
      operator: '≤',
      value: { value: 2.5, units: Voltage.VOLTS }
    });
  });

  test('Trigger condition IO Channel Digital', async () => {
    await selectOption('Select Condition Type', 'IO Channel');
    await selectOption('Select IO Type', 'Digital Output');
    await selectOption('Select Digital Value', 'On');

    await checkCondition({
      type: 'IO Channel',
      ioType: 'Digital Output',
      ioChannel: 1,
      operator: '==',
      value: { value: 1, units: '' }
    });
  });

  test('Trigger condition Time', async () => {
    await selectOption('Select Condition Type', 'Time Elapsed');
    await inputValue(250, 'ms');

    await checkCondition({
      type: 'Time Elapsed',
      value: { value: 250, units: Time.MILLISECONDS }
    });
  });

  test('Trigger condition axis Setting', async () => {
    await selectOption('Select Condition Type', 'Setting');
    await selectOption('Select Axis', 'Axis 2');
    await selectOption('Select Setting', 'accel');
    await selectOption('Select Operator', '<');
    await inputValue(10, 'cm/s²');

    await checkCondition({
      type: 'Setting',
      setting: 'accel',
      axis: 2,
      operator: '<',
      value: { value: 10, units: Acceleration.CENTIMETRES_PER_SECOND_SQUARED }
    });
  });

  test('Trigger condition device Setting', async () => {
    await selectOption('Select Condition Type', 'Setting');
    await selectOption('Select Axis', 'Device');
    await selectOption('Select Setting', 'encoder.2.cos');

    await checkCondition({
      type: 'Setting',
      setting: 'encoder.2.cos',
      axis: 0,
      operator: '==',
      value: { value: 0, units: Voltage.VOLTS }
    });
  });

  test('Trigger condition absolute Setting', async () => {
    await selectOption('Select Condition Type', 'Absolute Setting');
    await selectOption('Select Setting', 'encoder.2.cos');

    await checkCondition({
      type: 'Absolute Setting',
      setting: 'encoder.2.cos',
      axis: 0,
      operator: '==',
      value: { value: 0, units: Voltage.VOLTS }
    });
  });
});

describe('Trigger Actions Input', () => {
  beforeEach(async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });
  });

  const checkAction = async (action: TriggerActionState) => {
    await waitUntilPass(() => {
      expect(TestTriggersApp.testStore.getState().triggers.triggers[0].actionsInput[1]).toEqual(action);
    });
  };

  test('Trigger action Command device', async () => {
    await selectOption('Select Action B Type', 'Command');
    await selectOption('Select Axis', 'Device');

    const commandInput = wrapper.getByTitle('Command');
    fireEvent.change(commandInput, { target: { value: 'home' } });

    await waitUntilPass(() => {
      expect(commandInput).toHaveValue('home');
    });

    await checkAction({
      type: 'Command',
      axis: 0,
      command: 'home'
    });
  });

  test('Trigger action Command axis', async () => {
    await selectOption('Select Action B Type', 'Command');
    await selectOption('Select Axis', 'Axis 4');

    const commandInput = wrapper.getByTitle('Command');
    fireEvent.change(commandInput, { target: { value: 'move abs 1000' } });

    await waitUntilPass(() => {
      expect(commandInput).toHaveValue('move abs 1000');
    });

    await checkAction({
      type: 'Command',
      axis: 4,
      command: 'move abs 1000'
    });
  });

  test('Trigger action Command suggestions', async () => {
    await selectOption('Select Action B Type', 'Command');

    const commandInput = wrapper.getByTitle('Command');
    fireEvent.focus(commandInput);
    fireEvent.change(commandInput, { target: { value: 's' } });

    await waitUntilPass(() => {
      wrapper.getByText('stop');
      wrapper.getByText('scope');
    });

    fireEvent.keyDown(commandInput, { key: 'ArrowDown' });
    fireEvent.keyDown(commandInput, { key: 'Enter' });

    expect(commandInput).toHaveValue('stop');

    fireEvent.change(commandInput, { target: { value: 'move' } });

    await waitUntilPass(() => {
      wrapper.getByText('abs');
      wrapper.getByText('rel');
    });

    fireEvent.mouseDown(wrapper.getByText('abs'));

    expect(commandInput).toHaveValue('move abs');
  });

  test('Trigger action Command suggestions units', async () => {
    await selectOption('Select Action B Type', 'Command');

    const commandInput = wrapper.getByTitle('Command');
    fireEvent.focus(commandInput);
    fireEvent.change(commandInput, { target: { value: 'move abs 2' } });

    await waitUntilPass(() => {
      wrapper.getByText('2mm');
      wrapper.getByText('2cm');
    });
  });

  test('Trigger action Command suggestions shows note next to unit conversions', async () => {
    await selectOption('Select Action B Type', 'Command');

    const commandInput = wrapper.getByTitle('Command');
    fireEvent.focus(commandInput);
    fireEvent.change(commandInput, { target: { value: 'move abs 2' } });

    await waitUntilPass(() => {
      expect(wrapper.getAllByText('Unit conversion will apply')).not.toHaveLength(0);
    });
  });

  test('Trigger action Setting to Value', async () => {
    await selectOption('Select Action B Type', 'Set Setting to Value');
    await selectOption('Select Axis', 'Axis 2');
    await selectOption('Select Setting', 'accel');
    await selectOption('Select Operation', '+=');

    const valueInput = wrapper.getByTitle('Value of Setting Value');
    fireEvent.change(valueInput, { target: { value: 2 } });

    await waitUntilPass(() => {
      expect(valueInput).toHaveValue(2);
    });


    await checkAction({
      type: 'Set Setting to Value',
      setting: 'accel',
      axis: 2,
      operation: '+=',
      value: { value: 2, units: Acceleration.MILLIMETRES_PER_SECOND_SQUARED }
    });
  });

  test('Trigger action Setting to Setting', async () => {
    await selectOption('Select Action B Type', 'Set Setting to Setting');
    await selectOption('Select Axis', 'Axis 1');
    await selectOption('Select Setting', 'accel');
    await selectOption('Select Operation', '-=');
    await selectOption('Select Source Setting', 'encoder.2.cos');

    await checkAction({
      type: 'Set Setting to Setting',
      setting: 'accel',
      axis: 1,
      operation: '-=',
      fromSetting: 'encoder.2.cos',
      fromAxis: 0
    });
  });
});

describe('Axis Selectors Hidden', () => {
  beforeEach(async () => {
    mockSingleDevice(TestTriggersApp.testStore, info => ({
      ...info,
      product: mockProduct({ mobile: true, encoder: true })
    }));

    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });
  });

  test('Axis selector hidden for setting in condition', async () => {
    await selectOption('Select Condition Type', 'Setting');
    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Select Axis')).toBeNull();
    });
    await selectOption('Select Setting', 'accel');

    await checkCondition({
      type: 'Setting',
      setting: 'accel',
      axis: 0,
      operator: '==',
      value: { value: 0, units: Acceleration.MILLIMETRES_PER_SECOND_SQUARED }
    });
  });

  test('Axis selectors hidden for actions', async () => {
    await selectOption('Select Action B Type', 'Command');
    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Select Axis')).toBeNull();
    });

    await selectOption('Select Action B Type', 'Set Setting to Value');
    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Select Axis')).toBeNull();
    });

    await selectOption('Select Action B Type', 'Set Setting to Setting');
    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Select Axis')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Select From Axis')).toBeNull();
    });
  });
});

describe('Device Communication', () => {
  beforeEach(async () => {
    deviceCallback = device => {
      device.triggerObjects[0] = new TriggerMock(device, {
        condition: 'when 1 pos == 100',
        actions: [
          'action a 0 move max',
          'action b 0 accel = 34'],
        enabled: false,
        firesTotal: 0,
        firesRemaining: 0,
      });
    };
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });
  });

  test('Loads device state', async () => {
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Condition Type')).toHaveAttribute('data-current', 'Trajectory Position');
      expect(wrapper.getAllByTitle('Select Axis')[0]).toHaveAttribute('data-current', 'Axis 1');
      expect(wrapper.getByTitle('Select Operator')).toHaveAttribute('data-current', '==');
      expect(wrapper.getByTitle('Value of Condition Value')).toHaveValue(100);
      expect(wrapper.getByTitle('Units of Condition Value')).toHaveAttribute('data-current', 'mm');
      expect(wrapper.getByTitle('Select Action A Type')).toHaveAttribute('data-current', 'Command');
      expect(wrapper.getByTitle('Command')).toHaveValue('move max');
      expect(wrapper.getAllByTitle('Select Axis')[1]).toHaveAttribute('data-current', 'Device');
      expect(wrapper.getByTitle('Select Action B Type')).toHaveAttribute('data-current', 'Set Setting to Value');
      expect(wrapper.getByTitle('Select Setting')).toHaveAttribute('data-current', 'accel');
      expect(wrapper.getByTitle('Select Operation')).toHaveAttribute('data-current', '=');
      expect(wrapper.getByTitle('Value of Setting Value')).toHaveValue(34);
      expect(wrapper.getByTitle('Units of Setting Value')).toHaveAttribute('data-current', 'mm/s²');
    });
  });

  test('Saves device state with condition encoder distance interval', async () => {
    await selectOption('Select Condition Type', 'Encoder Distance Interval');
    await inputValue(500);

    await waitUntilPass(() => {
      const saveButton = wrapper.getByTitle('Save Changes');
      fireEvent.click(saveButton);
    });

    await waitUntilPass(() => {
      expect(device.triggerObjects[0].state.condition).toEqual('when 1 encoder dist 500');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Condition Type')).toHaveAttribute('data-current', 'Encoder Distance Interval');
      expect(wrapper.getByTitle('Value of Condition Value')).toHaveValue(500);
    });
  });

  test('Saves device state with condition io channel', async () => {
    await selectOption('Select Condition Type', 'IO Channel');
    await selectOption('Select IO Type', 'Analog Input');
    await selectOption('Select IO Channel', 'Channel 4');
    await selectOption('Select Operator', '≥');
    await inputValue(2.5);

    await waitUntilPass(() => {
      const saveButton = wrapper.getByTitle('Save Changes');
      fireEvent.click(saveButton);
    });

    await waitUntilPass(() => {
      expect(device.triggerObjects[0].state.condition).toEqual('when io ai 4 >= 2.5');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Condition Type')).toHaveAttribute('data-current', 'IO Channel');
      expect(wrapper.getByTitle('Select IO Type')).toHaveAttribute('data-current', 'Analog Input');
      expect(wrapper.getByTitle('Select IO Channel')).toHaveAttribute('data-current', 'Channel 4');
      expect(wrapper.getByTitle('Select Operator')).toHaveAttribute('data-current', '≥');
      expect(wrapper.getByTitle('Value of Condition Value')).toHaveValue(2.5);
    });
  });

  test('Saves device state with condition time', async () => {
    await selectOption('Select Condition Type', 'Time Elapsed');
    await inputValue(2);

    await waitUntilPass(() => {
      const saveButton = wrapper.getByTitle('Save Changes');
      fireEvent.click(saveButton);
    });

    await waitUntilPass(() => {
      expect(device.triggerObjects[0].state.condition).toEqual('when time 2');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Condition Type')).toHaveAttribute('data-current', 'Time Elapsed');
      expect(wrapper.getByTitle('Value of Condition Value')).toHaveValue(2);
    });
  });

  test('Saves device state with condition setting', async () => {
    await selectOption('Select Condition Type', 'Setting');
    await waitUntilPass(() => {
      const axisSelect = wrapper.getAllByTitle('Select Axis')[0];
      const option = getByText(axisSelect, 'Axis 2') as HTMLOptionElement;
      fireEvent.change(axisSelect, { target: { value: option.value } });
    });
    await waitUntilPass(() => {
      const settingSelect = wrapper.getAllByTitle('Select Setting')[0];
      const option = getByText(settingSelect, 'accel') as HTMLOptionElement;
      fireEvent.change(settingSelect, { target: { value: option.value } });
    });
    await selectOption('Select Operator', '≠');
    await inputValue(10);

    await waitUntilPass(() => {
      const saveButton = wrapper.getByTitle('Save Changes');
      fireEvent.click(saveButton);
    });

    await waitUntilPass(() => {
      expect(device.triggerObjects[0].state.condition).toEqual('when 2 accel <> 10');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Condition Type')).toHaveAttribute('data-current', 'Setting');
      expect(wrapper.getAllByTitle('Select Setting')[0]).toHaveAttribute('data-current', 'accel');
      expect(wrapper.getAllByTitle('Select Axis')[0]).toHaveAttribute('data-current', 'Axis 2');
      expect(wrapper.getByTitle('Select Operator')).toHaveAttribute('data-current', '≠');
      expect(wrapper.getByTitle('Value of Condition Value')).toHaveValue(10);
    });
  });

  test('Saves device state with condition absolute setting', async () => {
    await selectOption('Select Condition Type', 'Absolute Setting');
    await waitUntilPass(() => {
      const axisSelect = wrapper.getAllByTitle('Select Axis')[0];
      const option = getByText(axisSelect, 'Axis 2') as HTMLOptionElement;
      fireEvent.change(axisSelect, { target: { value: option.value } });
    });
    await waitUntilPass(() => {
      const settingSelect = wrapper.getAllByTitle('Select Setting')[0];
      const option = getByText(settingSelect, 'accel') as HTMLOptionElement;
      fireEvent.change(settingSelect, { target: { value: option.value } });
    });
    await selectOption('Select Operator', '≠');
    await inputValue(10);

    await waitUntilPass(() => {
      const saveButton = wrapper.getByTitle('Save Changes');
      fireEvent.click(saveButton);
    });

    await waitUntilPass(() => {
      expect(device.triggerObjects[0].state.condition).toEqual('when 2 abs accel <> 10');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Condition Type')).toHaveAttribute('data-current', 'Absolute Setting');
      expect(wrapper.getAllByTitle('Select Setting')[0]).toHaveAttribute('data-current', 'accel');
      expect(wrapper.getAllByTitle('Select Axis')[0]).toHaveAttribute('data-current', 'Axis 2');
      expect(wrapper.getByTitle('Select Operator')).toHaveAttribute('data-current', '≠');
      expect(wrapper.getByTitle('Value of Condition Value')).toHaveValue(10);
    });
  });

  test('Saves device state with action command', async () => {
    await selectOption('Select Action A Type', 'Command');

    const commandInput = wrapper.getByTitle('Command');
    fireEvent.change(commandInput, { target: { value: 'home' } });
    await waitUntilPass(() => {
      expect(commandInput).toHaveValue('home');
    });

    await waitUntilPass(() => {
      const saveButton = wrapper.getByTitle('Save Changes');
      fireEvent.click(saveButton);
    });

    await waitUntilPass(() => {
      expect(device.triggerObjects[0].state.actions[0]).toEqual('action a 0 home');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Action A Type')).toHaveAttribute('data-current', 'Command');
      expect(wrapper.getByTitle('Command')).toHaveValue('home');
    });
  });

  test('Saves device state with action command with units', async () => {
    await selectOption('Select Action A Type', 'Command');

    const commandInput = wrapper.getByTitle('Command');
    fireEvent.change(commandInput, { target: { value: 'move abs 2mm' } });
    await waitUntilPass(() => {
      expect(commandInput).toHaveValue('move abs 2mm');
    });

    await waitUntilPass(() => {
      const saveButton = wrapper.getByTitle('Save Changes');
      fireEvent.click(saveButton);
    });

    await waitUntilPass(() => {
      expect(device.triggerObjects[0].state.actions[0]).toEqual('action a 0 move abs 200');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Action A Type')).toHaveAttribute('data-current', 'Command');
      expect(wrapper.getByTitle('Command')).toHaveValue('move abs 200');
    });
  });

  test('Saves device state with action setting to value', async () => {
    await selectOption('Select Action B Type', 'Set Setting to Value');
    await waitUntilPass(() => {
      const axisSelect = wrapper.getAllByTitle('Select Axis')[2];
      const option = getByText(axisSelect, 'Axis 1') as HTMLOptionElement;
      fireEvent.change(axisSelect, { target: { value: option.value } });
    });
    await selectOption('Select Setting', 'accel');
    await selectOption('Select Operation', '-=');
    await waitUntilPass(() => {
      const valueInput = wrapper.getByTitle('Value of Setting Value');
      fireEvent.change(valueInput, { target: { value: 42 } });
    });

    await waitUntilPass(() => {
      const saveButton = wrapper.getByTitle('Save Changes');
      fireEvent.click(saveButton);
    });

    await waitUntilPass(() => {
      expect(device.triggerObjects[0].state.actions[1]).toEqual('action b 1 accel -= 42');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Action B Type')).toHaveAttribute('data-current', 'Set Setting to Value');
      expect(wrapper.getByTitle('Select Setting')).toHaveAttribute('data-current', 'accel');
      expect(wrapper.getAllByTitle('Select Axis')[2]).toHaveAttribute('data-current', 'Axis 1');
      expect(wrapper.getByTitle('Select Operation')).toHaveAttribute('data-current', '-=');
      expect(wrapper.getByTitle('Value of Setting Value')).toHaveValue(42);
    });
  });

  test('Saves device state with action setting to setting', async () => {
    await selectOption('Select Action B Type', 'Set Setting to Setting');
    await waitUntilPass(() => {
      const axisSelect = wrapper.getAllByTitle('Select Axis')[2];
      const option = getByText(axisSelect, 'Axis 1') as HTMLOptionElement;
      fireEvent.change(axisSelect, { target: { value: option.value } });
    });
    await selectOption('Select Setting', 'accel');
    await selectOption('Select Operation', '+=');
    await selectOption('Select Source Setting', 'encoder.2.cos');

    await waitUntilPass(() => {
      const saveButton = wrapper.getByTitle('Save Changes');
      fireEvent.click(saveButton);
    });

    await waitUntilPass(() => {
      expect(device.triggerObjects[0].state.actions[1]).toEqual('action b 1 accel += setting 0 encoder.2.cos');
    });

    await waitUntilPass(() => {
      expect(wrapper.queryByTitle('Save Controls')).toBeNull();
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Action B Type')).toHaveAttribute('data-current', 'Set Setting to Setting');
      expect(wrapper.getByTitle('Select Setting')).toHaveAttribute('data-current', 'accel');
      expect(wrapper.getAllByTitle('Select Axis')[2]).toHaveAttribute('data-current', 'Axis 1');
      expect(wrapper.getByTitle('Select Operation')).toHaveAttribute('data-current', '+=');
      expect(wrapper.getByTitle('Select Source Setting')).toHaveAttribute('data-current', 'encoder.2.cos');
    });
  });
});

describe('Trigger Validation', () => {
  afterEach(() => {
    (getSettingAxes as jest.Mock).mockImplementation(() => [1, 2, 3, 4]);
  });

  test('Missing Condition Type', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await selectOption('Select Action A Type', 'Command');
    const commandInput = wrapper.getByTitle('Command');
    fireEvent.change(commandInput, { target: { value: 'home' } });
    await waitUntilPass(() => {
      expect(commandInput).toHaveValue('home');
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Missing Condition Type');
    });

    await selectOption('Select Condition Type', 'Trajectory Position');
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Condition Type invalid (Trajectory Position)', async () => {
    (getSettingAxes as jest.Mock).mockImplementation(() => []);

    deviceCallback = device => {
      device.triggerObjects[0] = new TriggerMock(device, {
        condition: 'when 1 pos == 100',
        actions: [
          'action a none',
          'action b none'],
        enabled: false,
        firesTotal: 0,
        firesRemaining: 0,
      });
    };

    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Condition Type')).toHaveAttribute('data-current', 'Trajectory Position');
      wrapper.getByText('Unsupported Trigger');
    });

    await inputValue(101);

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Condition Type not supported by device and configured peripherals');
    });
  });

  test('Condition Axis invalid (Trajectory Position)', async () => {
    (getSettingAxes as jest.Mock).mockImplementation(() => [1]);

    deviceCallback = device => {
      device.triggerObjects[0] = new TriggerMock(device, {
        condition: 'when 2 pos == 100',
        actions: [
          'action a none',
          'action b none'],
        enabled: false,
        firesTotal: 0,
        firesRemaining: 0,
      });
    };

    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Axis')).toHaveAttribute('data-current', 'Axis 2');
      wrapper.getByText('Unsupported Trigger');
    });

    await inputValue(101);

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Condition Axis not supported by configured peripheral');
    });

    await selectOption('Select Axis', 'Axis 1');
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Condition Type invalid (Distance Interval)', async () => {
    (getSettingAxes as jest.Mock).mockImplementation(() => []);

    deviceCallback = device => {
      device.triggerObjects[0] = new TriggerMock(device, {
        condition: 'when 1 dist 100',
        actions: [
          'action a none',
          'action b none'],
        enabled: false,
        firesTotal: 0,
        firesRemaining: 0,
      });
    };

    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Condition Type')).toHaveAttribute('data-current', 'Distance Interval');
      wrapper.getByText('Unsupported Trigger');
    });

    await inputValue(101);

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Condition Type not supported by device and configured peripherals');
    });
  });

  test('Condition Type invalid (Encoder Distance Interval)', async () => {
    (getSettingAxes as jest.Mock).mockImplementation(() => []);

    deviceCallback = device => {
      device.triggerObjects[0] = new TriggerMock(device, {
        condition: 'when 1 encoder dist 100',
        actions: [
          'action a none',
          'action b none'],
        enabled: false,
        firesTotal: 0,
        firesRemaining: 0,
      });
    };

    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Condition Type')).toHaveAttribute('data-current', 'Encoder Distance Interval');
      wrapper.getByText('Unsupported Trigger');
    });

    await inputValue(101);

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Condition Type not supported by device and configured peripherals');
    });
  });

  test('Condition setting missing', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await selectOption('Select Condition Type', 'Setting');
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Missing Condition Setting');
    });

    await selectOption('Select Setting', 'accel');
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Condition setting invalid', async () => {
    deviceCallback = device => {
      device.triggerObjects[0] = new TriggerMock(device, {
        condition: 'when 1 invalidsetting == 100',
        actions: [
          'action a none',
          'action b none'],
        enabled: false,
        firesTotal: 0,
        firesRemaining: 0,
      });
    };

    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Select Setting')).toHaveAttribute('data-current', 'invalidsetting');
      wrapper.getByText('Unsupported Trigger');
    });

    await inputValue(101);

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Condition Setting not supported by selected Device or Axis');
    });

    await selectOption('Select Setting', 'accel');
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Condition not positive', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await selectOption('Select Condition Type', 'Time Elapsed');

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Condition Value must be greater than 0');
    });

    await inputValue(1);
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Action Command missing', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await selectOption('Select Condition Type', 'Trajectory Position');
    await selectOption('Select Action A Type', 'Command');

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Missing Command for Action A');
    });

    fireEvent.change(wrapper.getByTitle('Command'), { target: { value: 'home' } });
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Action Command missing variables', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await selectOption('Select Condition Type', 'Trajectory Position');
    await selectOption('Select Action A Type', 'Command');

    const commandInput = wrapper.getByTitle('Command');
    fireEvent.change(commandInput, { target: { value: 'move rel <positionExt>' } });

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Missing Command Variables for Action A');
    });

    fireEvent.change(commandInput, { target: { value: 'move rel 1000' } });
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Action missing Setting', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await selectOption('Select Condition Type', 'Trajectory Position');
    await selectOption('Select Action A Type', 'Set Setting to Value');

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Missing Setting for Action A');
    });

    await selectOption('Select Setting', 'accel');
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Action invalid Setting', async () => {
    deviceCallback = device => {
      device.triggerObjects[0] = new TriggerMock(device, {
        condition: 'when 1 pos == 100',
        actions: [
          'action a invalidsetting = 100',
          'action b none'],
        enabled: false,
        firesTotal: 0,
        firesRemaining: 0,
      });
    };

    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await selectOption('Select Operation', '+=');

    await waitUntilPass(() => {
      wrapper.getByText('Unsupported Trigger');
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Setting not supported by selected Device or Axis for Action A');
    });

    await selectOption('Select Setting', 'accel');
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Action missing Source Setting', async () => {
    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await selectOption('Select Condition Type', 'Trajectory Position');
    await selectOption('Select Action A Type', 'Set Setting to Setting');
    await selectOption('Select Setting', 'accel');

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Missing Source Setting for Action A');
    });

    await selectOption('Select Source Setting', 'accel');
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });

  test('Action invalid SourceSetting', async () => {
    deviceCallback = device => {
      device.triggerObjects[0] = new TriggerMock(device, {
        condition: 'when 1 pos == 100',
        actions: [
          'action a 1 accel = setting invalidsetting',
          'action b none'],
        enabled: false,
        firesTotal: 0,
        firesRemaining: 0,
      });
    };

    connectionViewMockInstance.props.onSelect(deviceKey);
    await waitUntilPass(() => {
      const chevrons = wrapper.getAllByTitle('Expand Trigger');
      fireEvent.click(chevrons[0]);
    });

    await selectOption('Select Operation', '+=');

    await waitUntilPass(() => {
      wrapper.getByText('Unsupported Trigger');
      expect(wrapper.getByTitle('Save Changes')).toBeDisabled();
      fireEvent.mouseEnter(wrapper.getByTitle('Save Changes'));
      wrapper.getByText('Source Setting not supported by selected Device or Axis for Action A');
    });

    await selectOption('Select Source Setting', 'accel');
    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Save Changes')).not.toBeDisabled();
    });
  });
});
