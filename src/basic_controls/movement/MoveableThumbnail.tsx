import React from 'react';
import { match } from 'ts-pattern';
import classNames from 'classnames';

import { Thumbnail } from '../../components/Thumbnail';
import type { Moveable } from '../types';

type ThumbnailMoveable = Extract<Moveable, { type: 'integrated' | 'motion-axis' }>;

function moveableDescription(moveable: ThumbnailMoveable, lockstepInfo: string | null): string {
  const description = match(moveable)
    .with({ type: 'integrated' }, () => moveable.motion.type === 'linear' ? 'Linear stage axis' : 'Rotary stage axis')
    .otherwise(() => moveable.motion.type === 'linear' ? 'Linear stage peripheral' : 'Rotary stage peripheral');
  return `${description}${lockstepInfo != null ? ` (${lockstepInfo})` : ''}`;
}

const MoveableThumbnail: React.FC<{ moveable: ThumbnailMoveable; lockstepInfo: string | null }> = ({ moveable, lockstepInfo }) => (
  <Thumbnail
    deviceOrPeripheralId={moveable.type === 'integrated' ? moveable.identity.deviceId : moveable.identity.peripheralId}
    title={moveableDescription(moveable, lockstepInfo)}
    className="thumbnail"
    altExt="axis"
  />
);

export const MoveableThumbnails: React.FC<{ moveable: Moveable }> = ({ moveable }) => {
  const primaryAxis = moveable.type === 'lockstep' ? moveable.primary : moveable;
  return <div className={classNames('thumbnails', moveable.type)}>
    <div className={classNames('thumbnail-box',  moveable.type)}>
      <MoveableThumbnail
        moveable={primaryAxis}
        lockstepInfo={moveable.type === 'lockstep' ? 'Primary Lockstep Axis' : null}/>
    </div>
    {moveable.type === 'lockstep' && (
      <>
        <div className="lockstep-size">{`x${moveable.secondaries.length + 1}`}</div>
        <div className="thumbnail-box secondary">
          <MoveableThumbnail moveable={moveable.secondaries[0]} lockstepInfo="Secondary Lockstep Axis 1"/>
        </div>
      </>
    )}
  </div>;
};
