
import { ServoTuningParamset } from '@zaber/motion/ascii';
import { NoticeBanner } from '@zaber/react-library';
import React from 'react';
import { match } from 'ts-pattern';

import { paramsetNames } from '../names';
import type { TuningMessage } from '../types';

interface Props {
  message: TuningMessage | null | undefined;
  paramset: ServoTuningParamset;
}

export const StatusMessage: React.FC<Props> = ({ message, paramset }) => match(message)
  .with({ status: 'success' }, ({ toLive }) => {
    if (toLive) {
      return <NoticeBanner type="success">
          Successfully applied this tuning from {paramsetNames[paramset]} to Live.
      </NoticeBanner>;
    } else {
      return <NoticeBanner type="success">
          Successfully {paramset === ServoTuningParamset.LIVE ? 'applied' : 'saved'} settings to {paramsetNames[paramset]}
      </NoticeBanner>;
    }
  })
  .with({ status: 'error' }, ({ message }) => (
    <NoticeBanner type="error">{message}</NoticeBanner>
  ))
  .otherwise(() => null);
