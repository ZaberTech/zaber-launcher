import React, { useLayoutEffect } from 'react';
import { useSelector } from 'react-redux';
import { Icons, Loader } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import type { EntityKey } from '../keys';

import { selectAxes } from './selectors';
import { actions as actionsDefinition } from './actions';
import { PeripheralName } from './PeripheralName';
import { PeripheralControls } from './PeripheralControls';
import { PeripheralMessage } from './PeripheralMessage';

interface Props {
  axisKey: EntityKey;
}

export const Peripheral: React.FC<Props> = ({ axisKey }) => {
  const actions = useActions(actionsDefinition);
  useLayoutEffect(() => {
    actions.didMount(axisKey);
    return () => { actions.willUnmount(axisKey) };
  }, []);

  const axis = useSelector(selectAxes)[axisKey];
  if (!axis) {
    return null;
  }
  const {
    monitorInfo,
    monitorInfoErr,
  } = axis;
  return (
    <div className="axis-peripheral">
      <PeripheralName axis={axis}/>

      {monitorInfo && <>
        <PeripheralMessage axis={axis}/>
        <PeripheralControls axis={axis}/>
      </>}
      {!monitorInfo && <Loader size="small"/>}
      {monitorInfoErr?.reason === 'other' && <Icons.ErrorFault className="monitor-error" title={monitorInfoErr.message}/>}
    </div>
  );
};
