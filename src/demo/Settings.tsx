import React, { useState } from 'react';
import { Icons, Modal, Button, TextArea, Text, ButtonRowConfirmCancel } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectDemoCode } from './selectors';
import { DEFAULT_PROGRAM } from './types';

const SettingsModal: React.FC<{
  onClose: () => void;
}> = ({ onClose }) => {
  const actions = useActions(actionsDefinition);
  const originalDemoCode = useSelector(selectDemoCode);

  const [demoCode, setCode] = useState(originalDemoCode);

  const onSave = () => {
    actions.updateSettings({ demoCode });
    onClose();
  };
  const onReset = () => {
    setCode(DEFAULT_PROGRAM);
  };

  return (
    <Modal
      className="settings-dialog"
      onRequestClose={onClose}
      headerIcon={<Icons.Settings/>}
      headerText="Settings"
      buttons={<ButtonRowConfirmCancel confirmText="Save" onConfirm={onSave} onCancel={onClose}>
        <Button className="reset-button" color="clear" onClick={onReset}><Icons.Restore/>Reset to Default</Button>
      </ButtonRowConfirmCancel>}>
      <TextArea placeholder="Your G-Code..." value={demoCode} onValueChange={setCode}/>
      <Text t={Text.Type.Helper}>
        You can enter a special flavor of G-Code
        where "%" symbol after coordinate and feed rate means fraction of travel limit and max speed respectively.
      </Text>
    </Modal>
  );
};

export const Settings: React.FC = () => {
  const [isOpen, setOpen] = useState(false);

  return (<>
    {isOpen && <SettingsModal onClose={() => setOpen(false)}/>}
    <Icons.Settings title="Settings" onClick={() => setOpen(true)}/>
  </>);
};
