import React from 'react';
import { useSelector } from 'react-redux';
import { Button, ButtonRow, ContextMenu, Icons, NoticeBanner } from '@zaber/react-library';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectEditValidation, selectIsSynced, selectState } from './selectors';

export const Controls: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const { apply, fileErr, factoryData } = useSelector(selectState);
  const isInvalid = useSelector(selectEditValidation) != null;
  const isApplying = apply?.state === 'applying';
  const isSynced = useSelector(selectIsSynced);
  return <>
    <ButtonRow className="controls">
      <Button color={isSynced ? 'grey' : 'red'} onClick={actions.apply} disabled={isInvalid || isApplying}>
        <span>Apply {!isSynced && <Icons.ErrorWarning className="warn"/>}</span>
      </Button>
      <Button color="grey" onClick={actions.loadFromFile}>Load from File</Button>
      <Button color="grey" onClick={actions.saveToFile} disabled={isInvalid}>Save to File</Button>
      <Button color="grey" onClick={actions.applyDelete} disabled={isApplying}>
        Delete Calibration
      </Button>
      <div className="spacer"/>
      <ContextMenu>
        <ContextMenu.CheckboxItem checked={factoryData} onChecked={actions.setFactoryData}>
          Factory Data
        </ContextMenu.CheckboxItem>
      </ContextMenu>
    </ButtonRow>
    <div className="errors">
      {apply?.state === 'error' && <NoticeBanner closer={actions.clearApplyError}>
        Apply Error: {apply.error}
      </NoticeBanner>}
      {fileErr && <NoticeBanner closer={actions.clearFileErr}>
        File Error: {fileErr}
      </NoticeBanner>}
    </div>
  </>;
};
