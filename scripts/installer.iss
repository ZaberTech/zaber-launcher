[Setup]
AppName=$$NAME$$
AppVersion=$$VERSION$$
WizardStyle=modern
DefaultDirName={autopf}\$$NAME$$
DefaultGroupName=$$NAME$$
UninstallDisplayIcon={app}\$$NAME$$.exe
Compression=lzma2
SolidCompression=yes
OutputBaseFilename=$$NAME$$ Alt-Setup $$VERSION$$
OutputDir=.
PrivilegesRequiredOverridesAllowed=dialog
SetupIconFile=..\icons\favicon.ico
DisableWelcomePage=no
AllowNoIcons=yes
WizardImageFile=..\icons\installer.bmp
ArchitecturesAllowed=x64compatible
ArchitecturesInstallIn64BitMode=x64compatible

[Messages]
WelcomeLabel2=Welcome to the alternative installer for $$NAME$$.%n%nThis installer is intended for custom installations such as automatic deployment. Unlike the standard installer, this installer does not include the automatic update feature. The application must be updated manually.

[Files]
Source: "win-unpacked\*"; DestDir: "{app}"; Flags: recursesubdirs; Check: IsX64
Source: "win-arm64-unpacked\*"; DestDir: "{app}"; Flags: recursesubdirs; Check: IsArm64
Source: "disable-updates.txt"; DestDir: "{app}"

[InstallDelete]
Type: filesandordirs; Name: "{app}\*"

[Icons]
Name: "{group}\$$NAME$$"; Filename: "{app}\$$NAME$$.exe"

[Code]
function IsX64: Boolean;
begin
  Result := (ProcessorArchitecture = paX64);
end;

function IsArm64: Boolean;
begin
  Result := (ProcessorArchitecture = paArm64);
end;
