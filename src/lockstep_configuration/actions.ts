import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { LockstepAxesData, LockstepCreateAxisInfo, LockstepSelectAxisInfo } from './reducer';

export enum ActionTypes {
  SET_SELECTED_DEVICE = 'LOCKSTEP-SET_SELECTED_DEVICE',
  SET_LOCKSTEP_DATA = 'LOCKSTEP-SET_LOCKSTEP_DATA',
  SET_ERROR = 'LOCKSTEP-SET_ERROR',

  // creation modal state
  BEGIN_CREATION = 'LOCKSTEP-BEGIN_LOCKSTEP_CREATION',
  CHOSE_CREATION_AXES = 'LOCKSTEP-CHOSE_CREATION_AXES',
  RECEIVED_LOCKSTEP_CREATION_INFO = 'LOCKSTEP-LOADED_LOCKSTEP_CREATION_INFO',
  CREATE_LOCKSTEP_GROUP = 'LOCKSTEP-CREATE_LOCKSTEP_GROUP',
  CREATE_LOCKSTEP_GROUP_DONE = 'LOCKSTEP-CREATE_LOCKSTEP_GROUP_DONE',
  CREATE_LOCKSTEP_GROUP_ERROR = 'LOCKSTEP-CREATE_LOCKSTEP_GROUP_ERROR',
  END_LOCKSTEP_CREATION = 'LOCKSTEP-END_LOCKSTEP_CREATION',

  BREAK_LOCKSTEP_GROUP = 'LOCKSTEP-BREAK_LOCKSTEP_GROUP',
  UPDATE_FETCHED_AXIS_DATA = 'LOCKSTEP-UPDATE_FETCHED_AXIS_DATA', // axis twists, offsets, and home warnings
  UPDATE_OFFSET_ESTIMATES = 'LOCKSTEP-UPDATE_OFFSET_ESTIMATES', // offset estimates for creation modal axis viewing

  WRITE_TWIST_TOLERANCE = 'LOCKSTEP-WRITE_TWIST_TOLERANCE'
}

export interface ActionsToPayloads {
  [ActionTypes.SET_SELECTED_DEVICE]: {deviceKey: EntityKey | null};
  [ActionTypes.SET_LOCKSTEP_DATA]: {lockstepData: LockstepAxesData};
  [ActionTypes.SET_ERROR]: {message: string; blocking: boolean};

  [ActionTypes.BEGIN_CREATION]: null;
  [ActionTypes.RECEIVED_LOCKSTEP_CREATION_INFO]: { axesInfo: LockstepSelectAxisInfo[] };
  [ActionTypes.CHOSE_CREATION_AXES]: {
    primary: LockstepCreateAxisInfo;
    chosen: LockstepCreateAxisInfo[];
    axesInfo: LockstepSelectAxisInfo[];
  };
  [ActionTypes.CREATE_LOCKSTEP_GROUP]: { axisNumbers: number[] };
  [ActionTypes.CREATE_LOCKSTEP_GROUP_DONE]: { groupNumber: number };
  [ActionTypes.CREATE_LOCKSTEP_GROUP_ERROR]: { error: string };
  [ActionTypes.END_LOCKSTEP_CREATION]: null;

  [ActionTypes.BREAK_LOCKSTEP_GROUP]: {groupNumber: number};
  [ActionTypes.UPDATE_FETCHED_AXIS_DATA]: void;
  [ActionTypes.UPDATE_OFFSET_ESTIMATES]: {creationAxisNumbers: number[]};

  [ActionTypes.WRITE_TWIST_TOLERANCE]: {twistTolerance : number; axisNumber: number};
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  setSelectedDevice: (deviceKey: EntityKey | null) => buildAction(ActionTypes.SET_SELECTED_DEVICE, { deviceKey }),
  setAxesData: (lockstepData: LockstepAxesData) => buildAction(ActionTypes.SET_LOCKSTEP_DATA, { lockstepData }),
  setError: (message: string, blocking = false) => buildAction(ActionTypes.SET_ERROR, { message, blocking }),

  beginCreation: () => buildAction(ActionTypes.BEGIN_CREATION),
  receivedCreationInfo: (axesInfo: LockstepSelectAxisInfo[]) => buildAction(ActionTypes.RECEIVED_LOCKSTEP_CREATION_INFO, { axesInfo }),
  choseCreationAxes: (primary: LockstepCreateAxisInfo, chosen: LockstepCreateAxisInfo[], axesInfo: LockstepSelectAxisInfo[]) => (
    buildAction(ActionTypes.CHOSE_CREATION_AXES, { primary, chosen, axesInfo })
  ),
  createLockstepGroup: (axisNumbers: number[]) => buildAction(ActionTypes.CREATE_LOCKSTEP_GROUP, { axisNumbers }),
  createLockstepDone: (groupNumber: number) => buildAction(ActionTypes.CREATE_LOCKSTEP_GROUP_DONE, { groupNumber }),
  createLockstepError: (error: string) => buildAction(ActionTypes.CREATE_LOCKSTEP_GROUP_ERROR, { error }),
  endLockstepCreation: () => buildAction(ActionTypes.END_LOCKSTEP_CREATION),

  breakLockstepGroup: (groupNumber: number) => buildAction(ActionTypes.BREAK_LOCKSTEP_GROUP, { groupNumber }),
  updateFetchedAxisDataRecord: () => buildAction(ActionTypes.UPDATE_FETCHED_AXIS_DATA),
  updateOffsetEstimates: (creationAxisNumbers: number[]) => buildAction(ActionTypes.UPDATE_OFFSET_ESTIMATES, { creationAxisNumbers }),

  writeTwistTolerance: (twistTolerance: number, axisNumber: number) =>
    buildAction(ActionTypes.WRITE_TWIST_TOLERANCE, { twistTolerance, axisNumber }),
};
