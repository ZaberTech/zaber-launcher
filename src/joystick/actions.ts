import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { AxisConfig, KeyCommand, KeyConfig, ChainInfo, JoystickConfig, CalibrationType } from './types';

export enum ActionTypes {
  MOUNT = 'JOYSTICK_MOUNT',
  SET_SELECTED_KEY = 'JOYSTICK_SET_SELECTED_KEY',
  CONFIG_READ = 'JOYSTICK_CONFIG_READ',
  CONFIG_READ_ERR = 'JOYSTICK_CONFIG_READ_ERR',
  UPDATE_AXIS_CONFIG = 'JOYSTICK_UPDATE_AXIS_CONFIG',
  UPDATE_KEY_CONFIG = 'JOYSTICK_UPDATE_KEY_CONFIG',
  UPDATE_KEY_COMMAND = 'JOYSTICK_UPDATE_KEY_COMMAND',
  ADD_KEY_COMMAND = 'JOYSTICK_ADD_KEY_COMMAND',
  REMOVE_KEY_COMMAND = 'JOYSTICK_REMOVE_KEY_COMMAND',
  WRITE_CONFIG = 'JOYSTICK_WRITE_CONFIG',
  WRITE_CONFIG_DONE = 'JOYSTICK_WRITE_CONFIG_DONE',
  SET_MANUAL_MODE = 'JOYSTICK_SET_MANUAL_MODE',
  SET_PRODUCTION_MODE = 'JOYSTICK_SET_PRODUCTION_MODE',
  SAVE_CONFIG = 'JOYSTICK_SAVE_CONFIG',
  LOAD_CONFIG = 'JOYSTICK_LOAD_CONFIG',
  LOAD_CONFIG_DONE = 'JOYSTICK_LOAD_CONFIG_DONE',
  SET_SAVE_LOAD_CONFIG_ERR = 'JOYSTICK_SET_SAVE_LOAD_CONFIG_ERR',
  START_CALIBRATION = 'JOYSTICK_START_CALIBRATION',
  STOP_CALIBRATION = 'JOYSTICK_STOP_CALIBRATION',
  CALIBRATION_ERROR = 'JOYSTICK_CALIBRATION_ERROR',
  RESTORE_CONFIG = 'JOYSTICK_RESTORE_CONFIG',
  RESTORE_CONFIG_DONE = 'JOYSTICK_RESTORE_COMPLETE',
}

export interface ActionsToPayloads {
  [ActionTypes.MOUNT]: void;
  [ActionTypes.SET_SELECTED_KEY]: { key: EntityKey | null };
  [ActionTypes.CONFIG_READ]: { config: JoystickConfig; chainInfo: ChainInfo };
  [ActionTypes.CONFIG_READ_ERR]: { error: string };
  [ActionTypes.UPDATE_AXIS_CONFIG]: { axisNumber: number; config: Partial<AxisConfig> };
  [ActionTypes.UPDATE_KEY_CONFIG]: { keyNumber: number; config: Partial<KeyConfig> };
  [ActionTypes.UPDATE_KEY_COMMAND]: { keyNumber: number; commandIndex: number; command: Partial<KeyCommand> };
  [ActionTypes.ADD_KEY_COMMAND]: { keyNumber: number };
  [ActionTypes.REMOVE_KEY_COMMAND]: { keyNumber: number; commandIndex: number };
  [ActionTypes.WRITE_CONFIG]: void;
  [ActionTypes.WRITE_CONFIG_DONE]: { error?: string };
  [ActionTypes.SET_MANUAL_MODE]: { mode: boolean };
  [ActionTypes.SET_PRODUCTION_MODE]: { mode: boolean; persist: boolean };
  [ActionTypes.SAVE_CONFIG]: void;
  [ActionTypes.LOAD_CONFIG]: void;
  [ActionTypes.LOAD_CONFIG_DONE]: { config: JoystickConfig };
  [ActionTypes.SET_SAVE_LOAD_CONFIG_ERR]: { error: string | null };
  [ActionTypes.START_CALIBRATION]: { type: CalibrationType };
  [ActionTypes.STOP_CALIBRATION]: { save: boolean };
  [ActionTypes.CALIBRATION_ERROR]: { error: string };
  [ActionTypes.RESTORE_CONFIG]: void;
  [ActionTypes.RESTORE_CONFIG_DONE]: { error?: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  mount: () => buildAction(ActionTypes.MOUNT),
  setSelectedKey: (key: EntityKey | null) => buildAction(ActionTypes.SET_SELECTED_KEY, { key }),
  configRead: (config: JoystickConfig, chainInfo: ChainInfo) => buildAction(ActionTypes.CONFIG_READ, { config, chainInfo }),
  configReadErr: (error: string) => buildAction(ActionTypes.CONFIG_READ_ERR, { error }),
  updateAxisConfig: (axisNumber: number, config: Partial<AxisConfig>) =>
    buildAction(ActionTypes.UPDATE_AXIS_CONFIG, { axisNumber, config }),
  updateKeyConfig: (keyNumber: number, config: Partial<KeyConfig>) =>
    buildAction(ActionTypes.UPDATE_KEY_CONFIG, { keyNumber, config }),
  updateKeyCommand: (keyNumber: number, commandIndex: number, command: Partial<KeyCommand>) =>
    buildAction(ActionTypes.UPDATE_KEY_COMMAND, { keyNumber, commandIndex, command }),
  addKeyCommand: (keyNumber: number) =>
    buildAction(ActionTypes.ADD_KEY_COMMAND, { keyNumber }),
  removeKeyCommand: (keyNumber: number, commandIndex: number) =>
    buildAction(ActionTypes.REMOVE_KEY_COMMAND, { keyNumber, commandIndex }),
  writeConfig: () => buildAction(ActionTypes.WRITE_CONFIG),
  writeConfigDone: (error?: string) => buildAction(ActionTypes.WRITE_CONFIG_DONE, { error }),
  setManualMode: (mode: boolean) => buildAction(ActionTypes.SET_MANUAL_MODE, { mode }),
  setProductionMode: (mode: boolean, persist = true) => buildAction(ActionTypes.SET_PRODUCTION_MODE, { mode, persist }),
  saveConfig: () => buildAction(ActionTypes.SAVE_CONFIG),
  loadConfig: () => buildAction(ActionTypes.LOAD_CONFIG),
  loadConfigDone: (config: JoystickConfig) => buildAction(ActionTypes.LOAD_CONFIG_DONE, { config }),
  setSaveLoadConfigError: (error: string | null) => buildAction(ActionTypes.SET_SAVE_LOAD_CONFIG_ERR, { error }),
  startCalibration: (type: CalibrationType) => buildAction(ActionTypes.START_CALIBRATION, { type }),
  stopCalibration: (save: boolean) => buildAction(ActionTypes.STOP_CALIBRATION, { save }),
  calibrationError: (error: string) => buildAction(ActionTypes.CALIBRATION_ERROR, { error }),
  restoreConfig: () => buildAction(ActionTypes.RESTORE_CONFIG),
  restoreConfigDone: (error?: string) => buildAction(ActionTypes.RESTORE_CONFIG_DONE, { error }),
};
