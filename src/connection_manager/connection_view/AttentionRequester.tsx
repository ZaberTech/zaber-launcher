import React from 'react';
import { Text } from '@zaber/react-library';

import type { EntityKey } from '../../keys';

import type { AttentionRequests } from './types';

interface Props {
  entityKey: EntityKey;
  extractor: (key: EntityKey) => EntityKey | null | undefined;
  attentionRequests?: AttentionRequests;
  showChildrenCount?: boolean;
}

export function AttentionRequester(props: Props) {
  const { entityKey, extractor, attentionRequests, showChildrenCount } = props;
  if (!attentionRequests) { return null }

  let thisRequestsAttention = false;
  let childrenRequestingAttention = 0;

  for (const entity of attentionRequests.requestAttention) {
    if (entity === entityKey) {
      thisRequestsAttention = true;
      break;
    }
    if (extractor(entity) === entityKey) {
      childrenRequestingAttention++;
    }
  }

  if (thisRequestsAttention) {
    return attentionRequests.showIcon;
  } else if (childrenRequestingAttention > 0 && showChildrenCount) {
    const subs = childrenRequestingAttention === 1 ? 'sub-entity is' : 'sub-entities are';
    return (
      <div className="attention multi">
        <Text t={Text.Type.BodyXSm} title={`${childrenRequestingAttention} ${subs} actionable`}>
          {childrenRequestingAttention}
        </Text>
      </div>
    );
  } else {
    return null;
  }
}
