import { Button, ButtonRow, HeaderCard, NoticeBanner, Text } from '@zaber/react-library';
import React from 'react';
import { useSelector } from 'react-redux';
import { P } from 'ts-pattern';

import { useActions, isMatch } from '../utils';

import { selectTestState } from './selectors';
import { actions as actionsDefinition } from './actions';

export const ScopeTest: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const state = useSelector(selectTestState);
  const isDisabled = isMatch({ inProgress: P.any }, state);
  return (<HeaderCard className="test controls" header={<Text t={Text.Type.H4}>Production Tools</Text>}>
    <ButtonRow justify="left">
      <Button disabled={isDisabled} onClick={() => actions.test('force-home')}>Home</Button>
      <Button disabled={isDisabled} onClick={() => actions.test('full')}>Full Test</Button>
      {isMatch({ inProgress: P.string }, state) && state.inProgress &&
        <Text t={Text.Type.Helper}>{state.inProgress}</Text>}
    </ButtonRow>
    {isMatch({ error: P.string }, state) &&
      <NoticeBanner type="error" closer={actions.clearTestResult}>
        {state.error}
      </NoticeBanner>}
  </HeaderCard>);
};
