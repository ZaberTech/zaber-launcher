
import type { SagaIterator } from 'redux-saga';
import { all, select, put, takeLatest, call, race, take, delay, takeEvery, fork } from 'redux-saga/effects';
import {
  Process,
  ProcessController
} from '@zaber/motion/product';
import { AbsoluteTemperature, Units, Voltage } from '@zaber/motion';
import { throwUnexpectedError } from '@zaber/toolbox';
import type { Device } from '@zaber/motion/ascii';

import { Action, RT, takeLatestUniq } from '../utils';
import { getDevice } from '../connection_manager';
import type { ValueUnitState } from '../units/EditableInputWithUnits';
import { getUnits } from '../units';
import type { EntityKey } from '../keys';

import { actionDefinitions as actions, ActionsToPayloads, ActionTypes } from './actions';
import { selectAutoTune, selectEntity, selectProcesses } from './selectors';
import { PROCESS_NUMBERS, type ControllerInfo, type ProcessInfo } from './types';
import { POLL_DELAY_MS } from './constants';
import { modeSettings, SettingsMap } from './settings';
import { autoTuneLoop, processControllerAutoTuneSaga } from './auto_tune/sagas';

const V = Voltage.VOLTS;

function uniqProcess(action: { payload: { process: number } }) {
  return `${action.payload.process}`;
}

export function* processControllerSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.CHOOSE_ENTITY, chooseEntity),
    takeLatest(ActionTypes.CHOOSE_PROCESS, chooseProcess),
    takeLatestUniq(ActionTypes.REFRESH_PROCESS, uniqProcess, refreshProcess),
    takeLatestUniq(ActionTypes.PROCESS_POWER, uniqProcess, setProcessPower),
    takeLatestUniq(ActionTypes.UPDATE_MODE, uniqProcess, updateMode),
    takeLatestUniq(ActionTypes.UPDATE_SOURCE, uniqProcess, updateSource),
    takeLatestUniq(ActionTypes.BRIDGE_PROCESSES, uniqProcess, bridgeProcess),
    takeEvery(ActionTypes.UPDATE_PROCESS_SETTING, updateProcessSetting),
    takeLatestUniq(ActionTypes.REENABLE_DRIVER, uniqProcess, reenableProcess),
    fork(processControllerAutoTuneSaga),
  ]);
}

/** Takes a list and converts it to a 1-indexed record */
function mapPorts<T>(values: T[]): Record<number, T> {
  const mapping: Record<number, T> = {};
  for (let i = 0; i < values.length; i++) {
    mapping[i + 1] = values[i];
  }
  return mapping;
}

async function getMeasure(process: Process, setting: string, prev?: ValueUnitState): Promise<ValueUnitState> {
  if (prev?.mode === 'writing' || prev?.mode === 'editing') { return prev }
  const units = prev?.units ?? 'readable';
  const mode = prev?.mode ?? 'displaying';
  const value = await process.settings.get(setting, typeof units !== 'string' ? units : Units.NATIVE);
  return { value, units, mode };
}

/**
 * Gets the current state of a process
 * @param controller A controller object from ZML
 * @param processNumber The process number to get information from
 * @param prevInfo The existing saved process info (if any)
 * @returns The current process info
 */
async function getProcessInfo(controller: ProcessController, processNumber: number, prevInfo?: ProcessInfo): Promise<ProcessInfo> {
  const process = controller.getProcess(processNumber);
  const mode = await process.getMode();
  const source = await process.getSource();
  const settings: SettingsMap = {};
  for (const setting of modeSettings[mode]) {
    settings[setting] = await getMeasure(process, setting, prevInfo?.settings[setting]);
  }
  return {
    number: processNumber,
    mode,
    source,
    settings,
    error: prevInfo?.error ?? null,
  };
}

function* refreshProcess({ payload: { process } }: Action<ActionsToPayloads[ActionTypes.REFRESH_PROCESS]>) {
  const processes: RT<typeof selectProcesses> = yield select(selectProcesses);

  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController);
    const info: RT<typeof getProcessInfo> = yield call(getProcessInfo, controller, process, processes[process]);
    yield put(actions.setProcess(process, { ...info, error: null }));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setAppError('There was an error reading settings for this controller', err.message));
  }
}

async function getFaults(device: Device, prevFaults: ControllerInfo['faults']): Promise<ControllerInfo['faults']> {
  const faultsExist = Object.entries(prevFaults).length > 0 ? true : (await device.warnings.getFlags()).size > 0;
  if (faultsExist) {
    const newFaults: ControllerInfo['faults'] = {};
    for (const process of PROCESS_NUMBERS) {
      const processFaults = await device.getAxis(process).warnings.getFlags();
      if (processFaults.size > 0) {
        newFaults[process] = processFaults;
      }
    }
    return newFaults;
  }
  return {};
}

/** Gets the current state of the process controller */
async function getInputsInfo(controller: ProcessController, prev?: ControllerInfo): Promise<ControllerInfo> {
  const device = controller.device;
  return {
    temperatures: {
      1: ({ value: await device.settings.get('sensor.temperature.1'), units: AbsoluteTemperature.DEGREES_CELSIUS }),
      2: ({ value: await device.settings.get('sensor.temperature.2'), units: AbsoluteTemperature.DEGREES_CELSIUS }),
    },
    analogIns: mapPorts((await device.io.getAllAnalogInputs()).map(value => ({ value, units: V }))),
    digitalIns: await device.io.getAllDigitalInputs(),
    digitalOuts: await device.io.getAllDigitalOutputs(),
    bridges: mapPorts((await device.settings.getFromAllAxes('driver.bipolar')).map(bridged => ({ on: bridged !== 0, reading: false }))),
    states: mapPorts((await device.settings.getFromAllAxes('process.state')).map(state => ({ on: state !== 0, reading: false }))),
    faults: await getFaults(device, prev?.faults ?? {}),
    voltsOut: mapPorts(await device.settings.getFromAllAxes('process.voltage')),
    currentOut: mapPorts(await device.settings.getFromAllAxes('process.current'))
  };
}

function* controllerUpdateLoop(controller: ProcessController) {
  const startTime = performance.now();
  let info: RT<typeof getInputsInfo> = yield call(getInputsInfo, controller);
  if (Object.keys(info.analogIns).length !== 2) {
    const message = `This app expects a process controller with 2 analog inputs, this product has ${Object.keys(info.analogIns).length}.`;
    yield put(actions.setAppError('Invalid Product', message));
    return;
  } else if (Object.keys(info.voltsOut).length !== 4) {
    const message = `This app expects a process controller with 4 outputs, this product has ${Object.keys(info.voltsOut).length}.`;
    yield put(actions.setAppError('Invalid Product', message));
    return;
  }
  for (let nextPollTime = startTime; true; nextPollTime += POLL_DELAY_MS) {
    yield delay(nextPollTime - performance.now());
    const autoTuneInfo: RT<typeof selectAutoTune> = yield select(selectAutoTune);
    if (autoTuneInfo != null) {
      yield call(autoTuneLoop, controller, autoTuneInfo.process);
      nextPollTime = performance.now();
    }
    info = yield call(getInputsInfo, controller, info);
    yield put(actions.setControllerInfo(info, performance.now() - startTime));
  }
}

function* getControllerInfo(entity: EntityKey) {
  try {
    const device: RT<typeof getDevice> = yield call(getDevice, entity);
    const controller = new ProcessController(device);
    for (const process of PROCESS_NUMBERS) {
      yield put(actions.refreshProcessInfo(process));
    }
    yield race([
      controllerUpdateLoop(controller),
      take(ActionTypes.CHOOSE_ENTITY),
      take(ActionTypes.UNMOUNT),
      take(ActionTypes.SET_APP_ERROR),
    ]);
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.detectNonPcEntity());
  }
}

function* chooseEntity(params: Action<ActionsToPayloads[ActionTypes.CHOOSE_ENTITY]>) {
  const { entity } = params.payload;
  if (entity == null) { return }

  yield call(getControllerInfo, entity);
}

function* chooseProcess({ payload: { process } }: Action<ActionsToPayloads[ActionTypes.CHOOSE_PROCESS]>) {
  if (process == null) { return }
  yield(actions.refreshProcessInfo(process));
}

class GetProcessControllerException extends Error { }

export function* getProcessController(entity?: RT<typeof selectEntity>) {
  if (entity == null) {
    entity = yield select(selectEntity);
  }
  if (entity == null) {
    throw new GetProcessControllerException('No entity selected');
  }
  const device: RT<typeof getDevice> = yield call(getDevice, entity);
  return new ProcessController(device);
}

function* setProcessPower({ payload: { process: processNumber, on } }: Action<ActionsToPayloads[ActionTypes.PROCESS_POWER]>) {
  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController);
    const process = controller.getProcess(processNumber);
    if (on) {
      yield call([process, process.on]);
    } else {
      yield call([process, process.off]);
    }
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setProcessError(processNumber, `There was an error turning this process ${on ? 'on' : 'off'}`, err.message));
  }
}

function* updateProcessSetting({
  payload: { process: processNumber, setting, update }
}: Action<ActionsToPayloads[ActionTypes.UPDATE_PROCESS_SETTING]>) {
  if (update.mode === 'displaying' || update.mode === 'editing') { return }
  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController);
    const process = controller.getProcess(processNumber);
    if (update.mode === 'writing') {
      yield call([process.settings, process.settings.set], setting, update.value, update.units);
    }
    const newValue: RT<typeof process.settings.get> = yield call([process.settings, process.settings.get], setting, getUnits(update));
    yield put(actions.updateProcessSetting(processNumber, setting, { value: newValue, units: update.units, mode: 'displaying' }));
  } catch (err) {
    throwUnexpectedError(err);
    const tune: RT<typeof selectAutoTune> = yield select(selectAutoTune);
    const message = `There was an error updating '${setting}'`;
    const details = err.message;
    if (tune) {
      yield put(actions.autoTuneError({ type: 'error', message, details }));
    }
    yield put(actions.setProcessError(processNumber, message, details));
  }
}

function* updateMode({ payload: { process: processNumber, mode } }: Action<ActionsToPayloads[ActionTypes.UPDATE_MODE]>) {
  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController);
    const process = controller.getProcess(processNumber);
    yield call([process, process.setMode], mode);
    yield put(actions.refreshProcessInfo(processNumber));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setProcessError(processNumber, 'There was an error updating the mode', err.message));
  }
}

function* updateSource({ payload: { process: processNumber, source } }: Action<ActionsToPayloads[ActionTypes.UPDATE_SOURCE]>) {
  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController);
    const process = controller.getProcess(processNumber);
    yield call([process, process.setSource], source);
    yield put(actions.refreshProcessInfo(processNumber));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setProcessError(processNumber, 'There was an error updating the source', err.message));
  }
}

function* bridgeProcess({ payload: { process: processNumber, bridge } }: Action<ActionsToPayloads[ActionTypes.BRIDGE_PROCESSES]>) {
  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController);
    const process = controller.getProcess(processNumber);
    if (bridge) {
      yield call([process, process.bridge]);
    } else {
      yield call([process, process.unbridge]);
    }
  } catch (err) {
    throwUnexpectedError(err);
    const msg = `There was an error ${bridge ? 'bridging' : 'unbridging'} process ${processNumber} and ${processNumber + 1}`;
    yield put(actions.setProcessError(processNumber, msg, err.message));
  }
}

function* reenableProcess({ payload: { process: processNumber } }: Action<ActionsToPayloads[ActionTypes.REENABLE_DRIVER]>) {
  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController);
    const process = controller.getProcess(processNumber);
    yield call([process, process.enable]);
    yield put(actions.refreshProcessInfo(processNumber));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setProcessError(processNumber, `There was an error reenabling process ${processNumber}`, err.message));
  }
}
