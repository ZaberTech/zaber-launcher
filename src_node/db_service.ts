
import path from 'path';
import os from 'os';
import childProcess from 'child_process';

import { injectable } from 'inversify';
import { app } from 'electron';
import { ReplaySubject } from 'rxjs';

import { environment } from '../src/environment';

@injectable()
export class DbService {
  private dbServiceProcess: childProcess.ChildProcess | null = null;
  private readonly runSubject = new ReplaySubject<void>();
  private _runPromise?: Promise<void>;
  private state: 'running' | 'stopping' | 'stopped' | null = null;

  public get runPromise() {
    if (this._runPromise == null) {
      this._runPromise = this.runSubject.toPromise();
    }
    return this._runPromise;
  }

  runDbService() {
    if (!environment.offline) {
      return;
    }

    let installDir: string;
    if (environment.isProduction) {
      installDir = path.dirname(app.getPath('exe'));
      if (os.platform() === 'darwin') {
        installDir = path.join(installDir, '..');
      }
    } else {
      installDir = path.join(__dirname, '..');
    }

    let serviceExe = path.join(installDir, 'device_db_service', `zaber-device-db-service-${os.arch()}`);
    if (os.platform() === 'win32') {
      serviceExe += '.exe';
    }

    const db = path.join(installDir, 'device_db_service', 'device_database.sqlite');

    this.dbServiceProcess = childProcess.spawn(serviceExe, ['run'], {
      // This process crashes in windows production if the IO is not ignored
      stdio: environment.isProduction ? 'ignore' : 'inherit',
      env: {
        ...process.env,
        ZABER_DEVICE_DB_PATH: db,
        URL_PREFIX: '',
        PORT: 'pipe',
      },
      windowsHide: true,
    });
    this.state = 'running';

    this.dbServiceProcess.once('error', this.onError);
    this.dbServiceProcess.once('exit', this.onExit);

    // failsafe when the process exits unexpectedly the service is not left running
    process.once('exit', this.killProcess);
  }

  private onError = (error: Error) => {
    (process as NodeJS.EventEmitter).off('exit', this.killProcess);

    this.state = 'stopped';
    this.runSubject.error(new DbServiceError(`Cannot start: ${error.message}`, -1));
  };

  private onExit = (code: number | null) => {
    (process as NodeJS.EventEmitter).off('exit', this.killProcess);

    const isUnexpected = this.state !== 'stopping';
    this.state = 'stopped';
    if (isUnexpected) {
      this.runSubject.error(new DbServiceError('Unexpected exit', code ?? -1));
    }
  };

  public async stopProcess() {
    if (this.dbServiceProcess == null || this.state !== 'running') {
      return;
    }
    this.state = 'stopping';

    try {
      const waitForClose = new Promise<void>((resolve, reject) => {
        const sleeper = setTimeout(reject, 1000);
        this.dbServiceProcess!.once('exit', () => {
          clearTimeout(sleeper);
          resolve();
        });
      });
      this.dbServiceProcess.kill('SIGINT');
      await waitForClose;

      this.runSubject.next();
      this.runSubject.complete();
    } catch {
      this.killProcess();
    }
  }

  private killProcess = () => {
    try {
      this.dbServiceProcess?.kill('SIGKILL');
    } catch {
      // if this fails there is nothing else to do
    }
    this.runSubject.error(new DbServiceError('Falling back to uncontrolled shutdown.', -1));
  };
}

export class DbServiceError extends Error {
  constructor(
    message: string,
    public readonly exitCode: number,
  ) {
    super(message);
  }
}
