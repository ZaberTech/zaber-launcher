import React, { useEffect } from 'react';
import { Route } from 'react-router';

import { Title } from '../components';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { AxesPanel } from './AxesPanel';
import { Editor } from './Editor';
import { SettingsModal } from './SettingsModal';
import { Paths } from './types';

export const GCode: React.FC = () => {
  const actions = useActions(actionsDefinition);
  useEffect(() => {
    actions.mount();
    return () => { actions.unmount() };
  }, []);
  return (
    <div className="gcode">
      <Title>G-Code</Title>
      <AxesPanel/>
      <Editor/>
      <Route path={Paths.AllSettings}>
        <SettingsModal/>
      </Route>
    </div>
  );
};
