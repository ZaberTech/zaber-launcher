import React from 'react';
import classNames from 'classnames';
import { Text, Card } from '@zaber/react-library';

import LogoMotto from '../assets/zaber_logo_motto.svg';
import Phone from '../assets/phone_red.svg';
import Feedback from '../assets/feedback_red.svg';
import { ContentHeader, ExternalLink } from '../components';
import { FeedbackButton } from '../feedback';
import { environment, Flavors } from '../environment';

interface Props {
  header: React.ReactNode;
  icon: React.ReactNode;
  centered?: boolean;
}

const ContactCard: React.FC<Props> = ({ header, icon, children, centered }) => (
  <Card>
    <div className="heading">
      {icon}
      <Text t={Text.Type.H4}>{header}</Text>
    </div>
    <div className={classNames('action', { centered })}>
      {children}
    </div>
  </Card>
);

export const Contact: React.FC = () => (<div className="contact-page">
  <ContentHeader>Contact Us</ContentHeader>

  <div className="content">
    {environment.flavor === Flavors.Zaber && <>
      <LogoMotto/>
      <ContactCard
        header="Do you have a question or need support with our products?"
        icon={<Phone/>}>
          Follow instructions on our webpage:&ensp;
        <ExternalLink url="https://www.zaber.com/contact">zaber.com/contact</ExternalLink>
      </ContactCard>
    </>}

    <ContactCard
      header="Have you found a bug or do you have a feature request?"
      icon={<Feedback/>} centered>
      <FeedbackButton color="grey">Leave Feedback</FeedbackButton>
    </ContactCard>
  </div>
</div>);
