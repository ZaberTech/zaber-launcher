import { Angle, AngularVelocity, Length, Units } from '@zaber/motion';

import {
  convertBetweenUnits, convertFromDefaultUnits, convertToDefaultUnits, getDefaultDimensionUnits, getDimensionName, getUnitLabel,
} from './utils';

describe('getUnitLabel', () => {
  test('returns short name of unit', () => {
    expect(getUnitLabel(Length.cm)).toBe('cm');
    expect(getUnitLabel(AngularVelocity.DEGREES_PER_SECOND)).toBe('°/s');
    expect(getUnitLabel(Units.NATIVE)).toBe('Native');
  });
});

describe('getDimensionName', () => {
  test('returns dimension name', () => {
    expect(getDimensionName(Length.cm)).toBe('Length');
    expect(getDimensionName(AngularVelocity.DEGREES_PER_SECOND)).toBe('Angular Velocity');
  });
  test('returns null for native', () => {
    expect(getDimensionName(Units.NATIVE)).toBeNull();
  });
});

describe('convertFromDefaultUnits', () => {
  test('converts to default units', () => {
    expect(getDefaultDimensionUnits(getDimensionName(Length.METRES)!)).toBe(Length.mm);

    expect(convertFromDefaultUnits(11, Length.METRES)).toBeCloseTo(0.011, 4);
    expect(convertFromDefaultUnits(11, Length.mm)).toBeCloseTo(11, 4);
  });
});
describe('convertToDefaultUnits', () => {
  test('converts to default units', () => {
    expect(getDefaultDimensionUnits(getDimensionName(Length.METRES)!)).toBe(Length.mm);

    expect(convertToDefaultUnits(11, Length.MICROMETRES)).toBeCloseTo(0.011, 4);
    expect(convertToDefaultUnits(11, Length.mm)).toBeCloseTo(11, 4);
  });
});
describe('convertBetweenUnits', () => {
  test('converts between units', () => {
    expect(convertBetweenUnits(11, Length.MICROMETRES, Length.cm)).toBeCloseTo(0.0011, 4);
    expect(convertBetweenUnits(180, Angle.DEGREES, Angle.rad)).toBeCloseTo(3.14159, 4);
    expect(convertBetweenUnits(11, Length.cm, Length.cm)).toBeCloseTo(11, 4);
  });
  test('does not convert non-numbers', () => {
    expect(convertBetweenUnits(null, Length.m, Length.mm)).toBeNull();
    expect(convertBetweenUnits(Number.NaN, Length.m, Length.mm)).toBeNaN();
  });
  test('does not convert native units', () => {
    expect(() => convertBetweenUnits(10, Units.NATIVE, Length.mm)).toThrowError();
    expect(() => convertBetweenUnits(10, Length.mm, Units.NATIVE)).toThrowError();
  });
  test('does incompatible dimensions', () => {
    expect(() => convertBetweenUnits(10, Angle.DEGREES, Length.mm)).toThrowError();
  });
  test('throws error on unknown units', () => {
    expect(() => convertBetweenUnits(10, 'bla' as Units, Length.mm)).toThrowError();
  });
});
