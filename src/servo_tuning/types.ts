import type { PidTuning, ServoTuningParamset, SimpleTuningParamDefinition } from '@zaber/motion/ascii';

import type { EntityKey } from '../keys';
import type { MeasurementWithUnit } from '../units';
import { MeasurementInputEditorState } from '../units/MeasurementInputEditor';

export type TuningMethod = 'advanced' | 'pid' | 'simple' | 'load' | 'csv';

export type TuningMessage = {
  status: 'writing';
} | {
  status: 'success';
  toLive: boolean;
} | {
  status: 'error';
  message: string;
};

export interface SimpleTuningSlider extends SimpleTuningParamDefinition {
  position: number;
  defaultValue: number;
}
export interface SimpleTuning {
  loadMass: MeasurementWithUnit;
  carriageModified: boolean;
  carriageMass: MeasurementWithUnit;
  sliders: Record<string, SimpleTuningSlider>;
}
export type SimpleTuningInfo = {
  method: 'simple';
  paramset: ServoTuningParamset;
  supported: boolean;
  tuning?: SimpleTuning;
  message?: TuningMessage | { status: 'will-override' };
};

export interface PidTuningInfo {
  method: 'pid';
  paramset: ServoTuningParamset;
  tuning: PidTuning;
  message?: TuningMessage;
}

export type ParamValue = { name: string; value: number | null };
export type AdvancedTuning = Record<string, number | null>;
export type AdvancedTuningInfo = {
  method: 'advanced';
  paramset: ServoTuningParamset;
  tuning: AdvancedTuning;
  message?: TuningMessage;
};

export type TuningInfo = SimpleTuningInfo | PidTuningInfo | AdvancedTuningInfo | null;

export type ServoTuningSetting = {
  measure: MeasurementInputEditorState['value'];
  mode: MeasurementInputEditorState['mode'];
  error: string | null;
  name: keyof ServoTuningSettings;
};
export const defaultServoTuningSetting = (name: keyof ServoTuningSettings, value: number): ServoTuningSetting => ({
  measure: { value, units: 'readable' },
  mode: 'display',
  error: null,
  name,
});
export interface ServoTuningSettings {
  accel: ServoTuningSetting;
  'cloop.settle.tolerance': ServoTuningSetting;
  'cloop.settle.period': ServoTuningSetting;
}

export interface ServoTuningAxisInfo {
  startupParamset: ServoTuningParamset;
  settings: ServoTuningSettings;
  /** recommended acceleration in units of m/s^2 */
  recommendedAccel: number | null;
  /** Default carriage mass in kilograms */
  defaultCarriageMassKg: number;
  /** maximum force the device should output in Newtons */
  maxForceN: number;
  driverEnabled: boolean;
}

export type ChosenParamset = { entity: EntityKey; paramset: ServoTuningParamset };
export type ChosenTuning = { paramset: ServoTuningParamset; method: TuningMethod };
export type Chosen = ChosenParamset & ChosenTuning;

export const TITLE = 'Servo Tuner';
