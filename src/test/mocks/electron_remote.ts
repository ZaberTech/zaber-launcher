const mockRemote = {
  app: {
    getName: () => 'ElectronMock',
    getPath: () => '',
  },
};

export const app = mockRemote.app;
