import React, { ComponentType, Component as ReactComponent } from 'react';
import { Provider } from 'react-redux';

import { buildStore, AppStore } from '../store/build';
import type { RootState } from '../store';
import { omit, type RecursivePartial } from '../utils';

interface PropsForWrapper {
  preloadedState?: RecursivePartial<RootState>;
  store?: AppStore;
}

export function wrapWithNewStore<Props>(Component: ComponentType<Props>):
  React.ComponentType<Props & PropsForWrapper> & { testStore: AppStore } {
  class WrappedWithStore extends ReactComponent<Props & PropsForWrapper> {
    private store: AppStore;

    public static testStore: AppStore;

    constructor(props: Props & PropsForWrapper) {
      super(props);
      this.store = props.store ?? buildStore(props.preloadedState);
      WrappedWithStore.testStore = this.store;
    }

    public componentWillUnmount(): void {
      WrappedWithStore.testStore = null!;
    }

    public render(): React.ReactNode {
      const props = omit(this.props, 'preloadedState', 'store') as Props & JSX.IntrinsicAttributes;
      return (
        <Provider store={this.store}>
          <Component {...props}/>
        </Provider>
      );
    }
  }

  return WrappedWithStore;
}
