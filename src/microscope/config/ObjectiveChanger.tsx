import React from 'react';
import { useSelector } from 'react-redux';
import { NoticeBanner, SimpleSelect } from '@zaber/react-library';
import { P } from 'ts-pattern';

import { isMatch } from '../../utils';

import { selectObjectiveChanger } from './selectors';
import { useDeviceOptions } from './DeviceAndAxisSelect';

export const ObjectiveChanger: React.FC = () => {
  const objectiveChanger = useSelector(selectObjectiveChanger);
  const options = useDeviceOptions();
  const selected = isMatch({ key: P.string }, objectiveChanger) ? objectiveChanger.key : '';

  return <>
    <div className="label">Objective Changer</div>
    <SimpleSelect className="device" disabled placeholder="Objective Changer"
      value={selected} options={options} onValueChange={() => null}
      portalToBody/>
    {isMatch({ error: P.string }, objectiveChanger) &&
      <NoticeBanner className="message" type="warning">
        {objectiveChanger.error}
      </NoticeBanner>}
  </>;
};
