import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';
import type { DeviceInfoWithAxes } from '../connection_manager';

import type { ErrorWithTitle, MDNSCandidate, Steps, tcpConnectionType } from './types';

export enum ActionTypes {
  REMOVE_CONNECTION = 'ADD_CONNECTION_REMOVE_CONNECTION',
  REMOVE_CONNECTION_DONE = 'ADD_CONNECTION_REMOVE_CONNECTION_DONE',
  CHANGE_DIALOG_OPEN = 'ADD_CONNECTION_CHANGE_DIALOG_OPEN',
  ADD_CONNECTION = 'ADD_CONNECTION_ADD_CONNECTION',
  ADD_CONNECTION_DONE = 'ADD_CONNECTION_ADD_CONNECTION_DONE',
  ADD_CONNECTION_ERR = 'ADD_CONNECTION_ADD_CONNECTION_ERR',
  CHANGE_PORT = 'ADD_CONNECTION_CHANGE_PORT',
  CLEAR_PORT = 'ADD_CONNECTION_CLEAR_PORT',
  CHANGE_BAUD_RATE = 'ADD_CONNECTION_CHANGE_BAUD_RATE',
  START_MONITORING_PORTS = 'ADD_CONNECTION_START_MONITORING_PORTS',
  STOP_MONITORING_PORTS = 'ADD_CONNECTION_STOP_MONITORING_PORTS',
  PORTS_CHANGED = 'ADD_CONNECTION_PORTS_CHANGED',
  NEW_PORT_CONNECTED = 'ADD_CONNECTION_NEW_PORT_CONNECTED',
  SHOW_ONLY_RECOMMENDED_PORTS = 'ADD_CONNECTION_SHOW_ONLY_RECOMMENDED_PORTS',
  CHANGE_STEP = 'ADD_CONNECTION_CHANGE_STEP',
  CHANGE_TCP = 'ADD_CONNECTION_CHANGE_TCP',
  CHANGE_CLOUD = 'ADD_CONNECTION_CHANGE_CLOUD',
  DETECT_DEVICES = 'ADD_CONNECTION_DETECT_DEVICES',
  DETECT_DEVICES_DONE = 'ADD_CONNECTION_DETECT_DEVICES_DONE',
  CONNECT_CLOUD = 'ADD_CONNECTION_CONNECT_CLOUD',
  CONNECT_CLOUD_ERR = 'ADD_CONNECTION_CONNECT_CLOUD_ERR',
  CONNECT_CLOUD_CLEAR = 'ADD_CONNECTION_CONNECT_CLOUD_CLEAR',
  CONNECT_LOCAL_SHARE = 'ADD_CONNECTION_CONNECT_LOCAL_SHARE',
  CONNECT_LOCAL_SHARE_ERROR = 'ADD_CONNECTION_CONNECT_LOCAL_SHARE_ERROR',
  CONNECT_LOCAL_SHARE_CLEAR = 'ADD_CONNECTION_CONNECT_LOCAL_SHARE_CLEAR',
  CHANGE_TCP_CONNECTION_TYPE = 'ADD_CONNECTION_CHANGE_TCP_CONNECTION_TYPE',
  MDNS_DISCOVER = 'MDNS_DISCOVER',
  MDNS_DISCOVER_CLEAR_CANDIDATES = 'MDNS_DISCOVER_CLEAR_CANDIDATES',
  MDNS_DISCOVER_ADD_CANDIDATE = 'MDNS_DISCOVER_ADD_CANDIDATE',
  MDNS_DISCOVER_DONE = 'MDNS_DISCOVER_DONE',
  MDNS_DISCOVER_CANCEL = 'MDNS_DISCOVER_CANCEL',
  MDNS_UPDATE_CANDIDATE_NAME = 'MDNS_DISCOVER_UPDATE_CANDIDATE_NAME',
  MDNS_FETCH_DEVICE_INFO_DONE_ERR = 'MDNS_DISCOVER_FETCH_DEVICE_INFO_DONE_ERR',
  HIDE_SIMULATOR_NOTICE = 'ADD_CONNECTION_HIDE_SIMULATOR_NOTICE',
  FIRST_DEVICES_LOAD = 'FIRST_DEVICES_LOAD',
}

export interface ActionsToPayloads {
  [ActionTypes.CHANGE_DIALOG_OPEN]: { open: boolean };
  [ActionTypes.ADD_CONNECTION]: void;
  [ActionTypes.ADD_CONNECTION_DONE]: { connectionKey: EntityKey };
  [ActionTypes.ADD_CONNECTION_ERR]: { error: string };
  [ActionTypes.CHANGE_PORT]: { selectedPort: string };
  [ActionTypes.CLEAR_PORT]: void;
  [ActionTypes.CHANGE_BAUD_RATE]: { baudRate: number | null };
  [ActionTypes.CHANGE_TCP]: { hostname?: string; port?: number | null };
  [ActionTypes.CHANGE_CLOUD]: { cloudId: string };
  [ActionTypes.REMOVE_CONNECTION]: { connectionKey: EntityKey };
  [ActionTypes.REMOVE_CONNECTION_DONE]: { connectionKey: EntityKey; error?: string };
  [ActionTypes.PORTS_CHANGED]: { ports: string[] };
  [ActionTypes.START_MONITORING_PORTS]: void;
  [ActionTypes.STOP_MONITORING_PORTS]: void;
  [ActionTypes.NEW_PORT_CONNECTED]: { port: string };
  [ActionTypes.SHOW_ONLY_RECOMMENDED_PORTS]: { recommend: boolean };
  [ActionTypes.CHANGE_STEP]: { step: Steps };
  [ActionTypes.DETECT_DEVICES]: void;
  [ActionTypes.DETECT_DEVICES_DONE]: { devices?: DeviceInfoWithAxes[]; error?: ErrorWithTitle };
  [ActionTypes.CONNECT_CLOUD]: void;
  [ActionTypes.CONNECT_CLOUD_ERR]: { error: ErrorWithTitle };
  [ActionTypes.CONNECT_CLOUD_CLEAR]: void;
  [ActionTypes.CONNECT_LOCAL_SHARE]: { url: string };
  [ActionTypes.CONNECT_LOCAL_SHARE_ERROR]: { error: ErrorWithTitle };
  [ActionTypes.CONNECT_LOCAL_SHARE_CLEAR]: void;
  [ActionTypes.CHANGE_TCP_CONNECTION_TYPE]: { selectedConnectionType: tcpConnectionType };
  [ActionTypes.MDNS_DISCOVER]: void;
  [ActionTypes.MDNS_DISCOVER_CLEAR_CANDIDATES]: void;
  [ActionTypes.MDNS_DISCOVER_ADD_CANDIDATE]: { candidate: MDNSCandidate };
  [ActionTypes.MDNS_DISCOVER_DONE]: { error?: string };
  [ActionTypes.MDNS_DISCOVER_CANCEL]: void;
  [ActionTypes.MDNS_UPDATE_CANDIDATE_NAME]: { hostname: string; name: string };
  [ActionTypes.MDNS_FETCH_DEVICE_INFO_DONE_ERR]: { candidate: MDNSCandidate; error: string };
  [ActionTypes.HIDE_SIMULATOR_NOTICE]: void;
  [ActionTypes.FIRST_DEVICES_LOAD]: { devices: DeviceInfoWithAxes[]; connectionKey: EntityKey };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  changeDialogOpen: (open: boolean) => buildAction(ActionTypes.CHANGE_DIALOG_OPEN, { open }),
  addConnection: () => buildAction(ActionTypes.ADD_CONNECTION),
  addConnectionDone: (connectionKey: EntityKey) => buildAction(ActionTypes.ADD_CONNECTION_DONE, { connectionKey }),
  addConnectionErr: (error: string) => buildAction(ActionTypes.ADD_CONNECTION_ERR, { error }),
  changePort: (selectedPort: string) => buildAction(ActionTypes.CHANGE_PORT, { selectedPort }),
  clearPort: () => buildAction(ActionTypes.CLEAR_PORT),
  changeBaudRate: (baudRate: number | null) => buildAction(ActionTypes.CHANGE_BAUD_RATE, { baudRate }),
  changeCloud: (cloudId: string) => buildAction(ActionTypes.CHANGE_CLOUD, { cloudId }),
  removeConnection: (connectionKey: EntityKey) => buildAction(ActionTypes.REMOVE_CONNECTION, { connectionKey }),
  removeConnectionDone: (connectionKey: EntityKey, error?: string) =>
    buildAction(ActionTypes.REMOVE_CONNECTION_DONE, { connectionKey, error }),
  portsChanged: (ports: string[]) => buildAction(ActionTypes.PORTS_CHANGED, { ports }),
  startMonitoringPorts: () => buildAction(ActionTypes.START_MONITORING_PORTS),
  stopMonitoringPorts: () => buildAction(ActionTypes.STOP_MONITORING_PORTS),
  newPortConnected: (port: string) => buildAction(ActionTypes.NEW_PORT_CONNECTED, { port }),
  showOnlyRecommendedPorts: (recommend: boolean) => buildAction(ActionTypes.SHOW_ONLY_RECOMMENDED_PORTS, { recommend }),
  changeStep: (step: Steps) => buildAction(ActionTypes.CHANGE_STEP, { step }),
  changeTcp: (tcpConfig: ActionsToPayloads[ActionTypes.CHANGE_TCP]) => buildAction(ActionTypes.CHANGE_TCP, tcpConfig),
  detectDevices: () => buildAction(ActionTypes.DETECT_DEVICES),
  detectDevicesDone: (devices: DeviceInfoWithAxes[]) =>
    buildAction(ActionTypes.DETECT_DEVICES_DONE, { devices }),
  detectDevicesDoneErr: (error: ErrorWithTitle) =>
    buildAction(ActionTypes.DETECT_DEVICES_DONE, { error }),
  connectCloud: () => buildAction(ActionTypes.CONNECT_CLOUD),
  connectCloudDoneErr: (error: ErrorWithTitle) =>
    buildAction(ActionTypes.CONNECT_CLOUD_ERR, { error }),
  connectCloudClear: () => buildAction(ActionTypes.CONNECT_CLOUD_CLEAR),
  connectLocalShare: (url: string) => buildAction(ActionTypes.CONNECT_LOCAL_SHARE, { url }),
  connectLocalShareErr: (error: ErrorWithTitle) => buildAction(ActionTypes.CONNECT_LOCAL_SHARE_ERROR, { error }),
  connectLocalShareClear: () => buildAction(ActionTypes.CONNECT_LOCAL_SHARE_CLEAR),
  changeTcpConnectionType: (selectedConnectionType: tcpConnectionType) =>
    buildAction(ActionTypes.CHANGE_TCP_CONNECTION_TYPE, { selectedConnectionType }),
  mdnsDiscover: () => buildAction(ActionTypes.MDNS_DISCOVER),
  mdnsDiscoverClearCandidates: () => buildAction(ActionTypes.MDNS_DISCOVER_CLEAR_CANDIDATES),
  mdnsDiscoverAddCandidate: (candidate: MDNSCandidate) =>
    buildAction(ActionTypes.MDNS_DISCOVER_ADD_CANDIDATE, { candidate }),
  mdnsDiscoverDone: (error?: string) => buildAction(ActionTypes.MDNS_DISCOVER_DONE, { error }),
  mdnsDiscoverCancel: () => buildAction(ActionTypes.MDNS_DISCOVER_CANCEL),
  mdnsUpdateCandidateName: (hostname: string, name: string) => buildAction(ActionTypes.MDNS_UPDATE_CANDIDATE_NAME, { hostname, name }),
  mdnsFetchDeviceInfoDoneErr: (candidate: MDNSCandidate, error: string) =>
    buildAction(ActionTypes.MDNS_FETCH_DEVICE_INFO_DONE_ERR, { candidate, error }),
  hideSimulatorNotice: () => buildAction(ActionTypes.HIDE_SIMULATOR_NOTICE),
  firstDevicesLoad: (connectionKey: EntityKey, devices: DeviceInfoWithAxes[]) =>
    buildAction(ActionTypes.FIRST_DEVICES_LOAD, { connectionKey, devices }),
};
