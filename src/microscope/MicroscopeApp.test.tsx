import React from 'react';
import _ from 'lodash';
import {
  RenderResult, fireEvent, getAllByRole, getAllByTitle, getByLabelText, getByPlaceholderText, getByRole, getByText, getByTitle, render,
} from '@testing-library/react';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import {
  ADDRESSES,
  asyncNoop, badData, CONNECTION_KEY, FOCUS_DATUM, getFocusOffset, MessageRoutersServiceMock,
  MicroscopeMock, microscopeMock, setMicroscopeCallback, WdiAutofocusProviderMock,
} from './mocks';

import {
  Length, NoValueForKeyException, Time, Velocity,
} from '@zaber/motion';

import { createContainer, destroyContainer } from '../container';
import { StorageMock, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import { MessageRoutersService } from '../message_router';
import { mockSingleDevice } from '../connection_manager/mocks';
import { selectConnections } from '../connection_manager';

import { MicroscopeApp } from './MicroscopeApp';
import { round, TEST_SKIP_SAGA_DELAYS } from '../utils';
import { act } from 'react-dom/test-utils';
import { FIRST_STEP_TIME, SPEED_INCREASE_DELAY } from './operation_sagas';
import { Storage } from '../app_components';
import { POSITIONS_STORAGE_KEY, PRESETS_STORAGE_KEY, PRODUCTION_MODE_STORAGE_KEY, StoredPositions, type StoredPresets } from './sagas';
import { isPresetValue } from './presets';
import { NO_PRESET, PRESET1 } from './testing';
import { actions } from './actions';
import { AUTOFOCUS_STORAGE_KEY, TEST_END } from './types';
import { WDI_MAGNIFICATIONS } from './AutofocusSettings';

let storageMock: StorageMock;

function getCardContent(name: RegExp | string) {
  return wrapper.getAllByText(name)[0].closest('.card')!.querySelector('.contents')! as HTMLElement;
}
function getAxisPositions() {
  return getAllByTitle(getCardContent('Position'), 'Value of Position').map(e => (e as HTMLInputElement).value);
}
function getStoredPresets() {
  return _.find(storageMock.stored, (_v, key) => key.startsWith(PRESETS_STORAGE_KEY)) as StoredPresets;
}
const TestApp = wrapWithNewStore(wrapWithRouter(MicroscopeApp));

let wrapper: RenderResult;

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(Storage).to(StorageMock);
  storageMock = container.get<unknown>(Storage) as StorageMock;
  storageMock.stored[AUTOFOCUS_STORAGE_KEY + CONNECTION_KEY] = 'wdi://host:1234';
});

afterEach(() => {
  const store = TestApp.testStore;
  wrapper?.unmount();
  wrapper = null!;

  if (store) {
    store.dispatch({ type: TEST_END });
  }

  destroyContainer();

  storageMock = null!;
});

describe('detection', () => {
  beforeEach(() => {
    wrapper = render(<TestApp/>);
    mockSingleDevice(TestApp.testStore);
  });

  test('error during detection', async () => {
    const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    setMicroscopeCallback(() => { throw new Error('Something went wrong') });
    await waitUntilPass(() => expect(wrapper.getByText(/Something went wrong/)));
  });

  test('detects full microscope', async () => {
    const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    await waitUntilPass(() => expect(wrapper.queryByText('Loading Components...')).toBeNull());

    expect(getAllByTitle(getCardContent('Position'), 'Value of Position')).toHaveLength(3);
    _.range(2).forEach(i => wrapper.getByText(`Illuminator Channel ${i + 1} (LED${i + 1}000)`));
    getByText(getCardContent(/Filter Changer$/), 'Special label 1');
    getByText(getCardContent(/Objective Changer$/), 'Special label 1');
    getByTitle(getCardContent(/Autofocus$/), 'In Focus');
  });

  test('detects empty microscope', async () => {
    setMicroscopeCallback((scope: MicroscopeMock) => {
      scope.currentFilter = null!;
      scope.objectiveChanger = null!;
      scope.illuminator = null!;
      scope.xAxis = null!;
      scope.yAxis = null!;
      scope.focusAxis = null!;
      scope.autofocus = null!;
    });
    const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    await waitUntilPass(() => expect(wrapper.getAllByText(/Presets/)));
  });


  test('validation errors displayed', async () => {
    setMicroscopeCallback(mock => {
      mock.focusDevice.identity.name = 'X-LDA';
      mock.focusAxis.settings.badSettingErr = badData;
    });

    const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    await waitUntilPass(() => wrapper.getByText(/settings outside the recommended range/));
    wrapper.getByText(/Could not check.*setting/);
  });


  test('validation failures displayed', async () => {
    setMicroscopeCallback(mock => {
      mock.focusDevice.identity.name = 'X-LDA';
      mock.focusAxis.settings.values['limit.hardstop.retraction'] = 2;
    });

    const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    await waitUntilPass(() => wrapper.getByText(/settings outside the recommended range/));
    wrapper.getByText(/Focus axis setting 'limit.hardstop.retraction' is 2 but should be <= 1.*/);
  });


  test('validation passes', async () => {
    setMicroscopeCallback(mock => {
      mock.focusAxis.settings.values['limit.hardstop.retraction'] = 1;
    });

    const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    await waitUntilPass(() => expect(wrapper.queryByText('Loading Components...')).toBeNull());
    expect(wrapper.queryByText(/settings outside the recommended range/)).toBeNull();
  });
});

describe('detected', () => {
  beforeEach(async () => {
    wrapper = render(<TestApp/>);
    mockSingleDevice(TestApp.testStore);
    const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;
    storageMock.stored[PRESETS_STORAGE_KEY + connection.key] = {
      version: 2,
      presets: [PRESET1, NO_PRESET]
    } satisfies StoredPresets;
    storageMock.stored[POSITIONS_STORAGE_KEY + connection.key] = {
      version: 1,
      positions: [{
        label: 'Position 01',
        xAxis: { value: 10, units: Length.mm },
        yAxis: { value: 20, units: Length.mm },
        focusAxis: { value: 30, units: Length.mm },
      }]
    } satisfies StoredPositions;

    setMicroscopeCallback(microscope => {
      microscope.connection.getDevice(ADDRESSES.focus).storage.getNumber.mockImplementation(
        async key => {
          if (key.startsWith('zaber.microscope.trigger.')) {
            return key.length * 10;
          }
          throw new NoValueForKeyException('No value');
        });
    });

    connectionViewMockInstance.props.onSelect(connection.key);
    await waitUntilPass(() => expect(wrapper.queryByText('Loading Components...')).toBeNull());
  });

  test('monitors for changes', async () => {
    microscopeMock.currentFilter = 2;
    microscopeMock.currentObjective = 3;
    microscopeMock.illuminator.getChannel(1)._isOn = true;
    microscopeMock.illuminator.getChannel(1)._intensity = 0.5;
    microscopeMock.illuminator.getChannel(2)._intensity = 0.1;
    microscopeMock.xAxis._position = 11000;
    microscopeMock.yAxis._position = 12000;
    microscopeMock.focusAxis._position = 13000;
    microscopeMock.autofocus.status.inFocus = false;
    microscopeMock.autofocus.status.inRange = false;

    TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Current Filter')).toHaveValue('2');
      expect(wrapper.getByTitle('Current Objective')).toHaveValue('3');
      expect(getByRole(getCardContent(/Illuminator Channel 1/), 'checkbox')).toBeChecked();
      expect(getByRole(getCardContent(/Illuminator Channel 1/), 'slider')).toHaveValue('50');
      expect(getByRole(getCardContent(/Illuminator Channel 2/), 'slider')).toHaveValue('10');
      expect(getAllByTitle(getCardContent('Position'), 'Value of Position')[1]).toHaveValue(11);
      expect(getAllByTitle(getCardContent('Position'), 'Value of Position')[2]).toHaveValue(12);
      expect(getAllByTitle(getCardContent('Position'), 'Value of Position')[0]).toHaveValue(13);
      expect(getByTitle(getCardContent(/Autofocus$/), 'Out of Focus'));
      expect(getByTitle(getCardContent(/Autofocus$/), 'Out of Range'));
    });
  });

  test('monitor persists when unmounted', async () => {
    const store = TestApp.testStore;

    microscopeMock.filterChanger.getCurrentFilter.mockClear();
    wrapper.unmount();

    store.dispatch(actions.refreshMonitor()); // wake up from the last sleep
    await waitUntilPass(() => expect(microscopeMock.filterChanger.getCurrentFilter).toHaveBeenCalled());

    microscopeMock.currentFilter = 2;
    wrapper = render(<TestApp store={store}/>);
    await waitUntilPass(() => expect(wrapper.getByTitle('Current Filter')).toHaveValue('2'));
  });

  test('displays monitor errors but recovers asap', async () => {
    microscopeMock.filterChanger.getCurrentFilter.mockRejectedValueOnce(new Error('Disconnected totally'));
    TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    const ERROR = /Disconnected totally/;
    await waitUntilPass(() => wrapper.getByText(ERROR));

    setMicroscopeCallback(m => m.currentFilter = 4);
    TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    await waitUntilPass(() => {
      expect(wrapper.queryByText(ERROR)).toBeNull();
      expect(wrapper.getByTitle('Current Filter')).toHaveValue('4');
    });
  });

  test('initializes microscope', async () => {
    const MESSAGE = /The microscope is not initialized/;
    microscopeMock.isInitialized.mockImplementationOnce(async () => false);
    TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    await waitUntilPass(() => wrapper.getByText(MESSAGE));

    fireEvent.click(wrapper.getByText('Initialize'));
    wrapper.getByText('Initializing...');

    await waitUntilPass(() => expect(wrapper.queryByText(MESSAGE)).toBeNull());
    expect(microscopeMock.initialize).toHaveBeenCalled();
    expect(microscopeMock.plate.moveAbsolute).toHaveBeenCalledWith(
      { value: 5500 },
      { value: 5500 },
    );
  });

  describe('filter changer', () => {
    test('changes filter', (async () => {
      fireEvent.change(wrapper.getByTitle('Current Filter'), { target: { value: '3' } });
      expect(wrapper.getByTitle('Current Filter')).toBeDisabled();
      expect(wrapper.getByTitle('Current Filter')).toHaveValue('3');

      await waitUntilPass(() => expect(wrapper.getByTitle('Current Filter')).not.toBeDisabled());
      expect(wrapper.getByTitle('Current Filter')).toHaveValue('3');
      expect(microscopeMock.filterChanger.change).toHaveBeenCalledWith(3);
    }));

    test('homes (when not homed)', (async () => {
      microscopeMock.filterChanger.device.getAxis(1).isHomed.mockResolvedValueOnce(false);
      fireEvent.change(wrapper.getByTitle('Current Filter'), { target: { value: '3' } });
      await waitUntilPass(() => expect(wrapper.getByTitle('Current Filter')).not.toBeDisabled());
      expect(microscopeMock.filterChanger.device.getAxis(1).home).toHaveBeenCalled();
    }));

    test('homes (when asked to do so)', (async () => {
      fireEvent.click(wrapper.getByTitle('Home (Re-align) Filter Changer'));
      await waitUntilPass(() => expect(wrapper.getByTitle('Current Filter')).not.toBeDisabled());
      expect(microscopeMock.filterChanger.device.getAxis(1).home).toHaveBeenCalled();
    }));

    test('handles errors', (async () => {
      microscopeMock.filterChanger.change.mockRejectedValueOnce(new Error('Cannot change'));
      fireEvent.change(wrapper.getByTitle('Current Filter'), { target: { value: '3' } });
      expect(wrapper.getByTitle('Current Filter')).toHaveValue('3');
      await waitUntilPass(() => wrapper.getByText('Cannot change'));
      expect(wrapper.getByTitle('Current Filter')).toHaveValue('1');
    }));

    test('changes settings', (async () => {
      const card = getCardContent(/Filter Changer/).closest('.card')! as HTMLElement;
      fireEvent.click(getByTitle(card, 'Settings'));

      fireEvent.change(getByPlaceholderText(card, 'Filter 1'), { target: { value: '' } });
      fireEvent.change(getByPlaceholderText(card, 'Filter 3'), { target: { value: 'New Label' } });

      fireEvent.click(getByText(card, 'Save'));

      const changer = microscopeMock.filterChanger;
      const storage = changer.device.getAxis(1).storage;
      await waitUntilPass(() =>
        expect(storage.setString).toHaveBeenCalled());
      expect(storage.setString.mock.calls).toEqual([
        ['zaber.motion.index.label.2', 'Special label 2', { encode: true }],
        ['zaber.motion.index.label.3', 'New Label', { encode: true }],
      ]);
      expect(storage.eraseKey.mock.calls).toEqual([
        ['zaber.motion.index.label.1'],
        ['zaber.motion.index.label.4'],
      ]);
    }));

    test('next and previous buttons change objectives', (async () => {
      TestApp.testStore.dispatch(actions.setProductionMode(true, false));

      fireEvent.click(getByTitle(getCardContent(/Filter Changer/), 'Previous'));
      await waitUntilPass(() => expect(microscopeMock.filterChanger.change).toHaveBeenCalledWith(4));
      fireEvent.click(getByTitle(getCardContent(/Filter Changer/), 'Next'));
      await waitUntilPass(() => expect(microscopeMock.filterChanger.change).toHaveBeenCalledWith(1));
    }));
  });

  describe('objective changer', () => {
    test('changes objective and applies offsets maintaining current position', (async () => {
      const POSITION = 13; // in datum coordinates
      microscopeMock.objectiveChanger.focusAxis._position = FOCUS_DATUM + getFocusOffset(1) + POSITION;
      fireEvent.change(wrapper.getByTitle('Current Objective'), { target: { value: '2' } });
      expect(wrapper.getByTitle('Current Objective')).toBeDisabled();
      expect(wrapper.getByTitle('Current Objective')).toHaveValue('2');

      await waitUntilPass(() => expect(wrapper.getByTitle('Current Objective')).not.toBeDisabled());
      expect(wrapper.getByTitle('Current Objective')).toHaveValue('2');
      expect(microscopeMock.objectiveChanger.change).toHaveBeenCalledWith(2, { focusOffset: { value: getFocusOffset(2) + POSITION } });
      expect(microscopeMock.objectiveChanger.focusAxis._position).toEqual(FOCUS_DATUM + getFocusOffset(2) + POSITION);
    }));
    test('applies only the final offset if not homed', (async () => {
      microscopeMock.objectiveChanger.focusAxis._position = -1e8;
      microscopeMock.objectiveChanger.focusAxis.isHomed.mockResolvedValueOnce(false);

      fireEvent.change(wrapper.getByTitle('Current Objective'), { target: { value: '2' } });
      await waitUntilPass(() => expect(wrapper.getByTitle('Current Objective')).not.toBeDisabled());

      expect(microscopeMock.objectiveChanger.change).toHaveBeenCalledWith(expect.anything(), { focusOffset: { value: getFocusOffset(2) } });
      expect(microscopeMock.objectiveChanger.focusAxis._position).toEqual(FOCUS_DATUM + getFocusOffset(2));
    }));
    test('prevents position going below datum with current offset', (async () => {
      // position is -1 below datum with offset
      microscopeMock.objectiveChanger.focusAxis._position = FOCUS_DATUM + getFocusOffset(1) - 1;

      fireEvent.change(wrapper.getByTitle('Current Objective'), { target: { value: '2' } });
      await waitUntilPass(() => expect(wrapper.getByTitle('Current Objective')).not.toBeDisabled());

      expect(microscopeMock.objectiveChanger.change).toHaveBeenCalledWith(expect.anything(), { focusOffset: { value: getFocusOffset(2) } });
      expect(microscopeMock.objectiveChanger.focusAxis._position).toEqual(FOCUS_DATUM + getFocusOffset(2));
    }));


    test('handles errors', (async () => {
      microscopeMock.objectiveChanger.change.mockRejectedValueOnce(new Error('Cannot move'));
      fireEvent.change(wrapper.getByTitle('Current Objective'), { target: { value: '3' } });
      expect(wrapper.getByTitle('Current Objective')).toHaveValue('3');
      await waitUntilPass(() => wrapper.getByText('Cannot move'));
      expect(wrapper.getByTitle('Current Objective')).toHaveValue('1');
    }));

    test('changes settings', (async () => {
      const card = getCardContent(/Objective Changer/).closest('.card')! as HTMLElement;
      fireEvent.click(getByTitle(card, 'Settings'));

      fireEvent.change(getByPlaceholderText(card, 'Objective 1'), { target: { value: '' } });
      fireEvent.change(getByPlaceholderText(card, 'Objective 3'), { target: { value: 'New Label' } });
      fireEvent.change(getAllByTitle(card, 'Value of Focus Offset')[0], { target: { value: '0' } });
      fireEvent.change(getAllByTitle(card, 'Value of Focus Offset')[1], { target: { value: '12' } });

      fireEvent.click(getByText(card, 'Save'));

      const changer = microscopeMock.objectiveChanger;
      const turretStorage = changer.turret.getAxis(1).storage;
      const focusStorage = changer.focusAxis.storage;
      await waitUntilPass(() =>
        expect(turretStorage.setString).toHaveBeenCalled());
      expect(turretStorage.setString.mock.calls).toEqual([
        ['zaber.motion.index.label.2', 'Special label 2', { encode: true }],
        ['zaber.motion.index.label.3', 'New Label', { encode: true }],
      ]);
      expect(turretStorage.eraseKey.mock.calls).toEqual([
        ['zaber.motion.index.label.1'],
        ['zaber.motion.index.label.4'],
      ]);

      expect(focusStorage.setNumber.mock.calls).toEqual([
        ['zaber.microscope.focus_offset.2', 12000],
      ]);
      expect(focusStorage.eraseKey.mock.calls).toEqual([
        ['zaber.microscope.focus_offset.1'],
        ['zaber.microscope.focus_offset.3'],
        ['zaber.microscope.focus_offset.4'],
      ]);
    }));
  });

  describe('illuminator', () => {
    let card1: HTMLElement;
    beforeEach(() => {
      jest.useFakeTimers();
      card1 = getCardContent(/Illuminator Channel 1/);
    });
    afterEach(() => {
      jest.useRealTimers();
    });

    test('turns on and off', (async () => {
      expect(getByRole(card1, 'checkbox')).not.toBeChecked();
      fireEvent.click(getByRole(card1, 'checkbox'));
      expect(getByRole(card1, 'checkbox')).toBeChecked();
      await waitUntilPass(() =>
        expect(microscopeMock.illuminator.getChannel(1).setOn).toHaveBeenCalledWith(true));
      expect(getByRole(card1, 'checkbox')).toBeChecked();

      fireEvent.click(getByRole(card1, 'checkbox'));
      await waitUntilPass(() =>
        expect(microscopeMock.illuminator.getChannel(1).setOn).toHaveBeenCalledWith(false));
      expect(getByRole(card1, 'checkbox')).not.toBeChecked();
    }));

    test('handles errors', (async () => {
      microscopeMock.illuminator.getChannel(1).setOn.mockRejectedValueOnce(new Error('Cannot turn on'));
      fireEvent.click(getByRole(card1, 'checkbox'));
      expect(getByRole(card1, 'checkbox')).toBeChecked();
      await waitUntilPass(() => wrapper.getByText(/Cannot turn on/));
      expect(getByRole(card1, 'checkbox')).not.toBeChecked();
    }));

    test('changes intensity with slider', (async () => {
      fireEvent.change(getByRole(card1, 'slider'), { target: { value: '50' } });
      act(() => { jest.advanceTimersByTime(10) });
      await waitUntilPass(() =>
        expect(microscopeMock.illuminator.getChannel(1).setIntensity).toHaveBeenCalledWith(0.5));
      expect(getByRole(card1, 'slider')).toHaveValue('50');
    }));

    test('changes intensity with input', (async () => {
      fireEvent.click(getByTitle(card1, 'Edit Intensity'));
      fireEvent.change(getByRole(card1, 'spinbutton'), { target: { value: '44' } });
      fireEvent.click(getByTitle(card1, 'Confirm Intensity edit'));
      act(() => { jest.advanceTimersByTime(10) });
      await waitUntilPass(() =>
        expect(microscopeMock.illuminator.getChannel(1).setIntensity).toHaveBeenCalledWith(0.44));
      expect(getByRole(card1, 'spinbutton')).toHaveValue(44);
    }));
  });

  describe('axis movement', () => {
    beforeEach(() => {
      jest.useFakeTimers();
    });
    afterEach(() => {
      jest.useRealTimers();
    });

    test('moves axis by a step when clicked', async () => {
      fireEvent.mouseDown(wrapper.getByTitle('Move Left'));
      await waitUntilPass(() =>
        expect(microscopeMock.xAxis.moveRelative).toHaveBeenCalledWith(-0.01, Length.mm));
      fireEvent.mouseUp(wrapper.getByTitle('Move Left'));
    });

    test('allows to change step', (async () => {
      const card = getCardContent('Position');
      fireEvent.click(getAllByTitle(card, 'Edit Step')[2]);
      fireEvent.change(getAllByTitle(card, 'Value of Step')[2], { target: { value: '50' } });
      fireEvent.click(getByTitle(card, /Confirm Step/));
      fireEvent.mouseDown(wrapper.getByTitle('Move Down'));
      await waitUntilPass(() =>
        expect(microscopeMock.yAxis.moveRelative).toHaveBeenCalledWith(0.05, Length.mm));
      fireEvent.mouseUp(wrapper.getByTitle('Move Down'));
    }));

    test('inverts movements for inverted axis', async () => {
      fireEvent.mouseDown(wrapper.getByTitle('Move Up'));
      await waitUntilPass(() =>
        expect(microscopeMock.yAxis.moveRelative).toHaveBeenLastCalledWith(-0.01, Length.mm));

      setMicroscopeCallback(microscope => {
        microscope.yAxis.storage.getBool.mockResolvedValueOnce(true);
      });
      TestApp.testStore.dispatch(actions.restartMonitor());
      await waitUntilPass(() => expect(wrapper.getByTitle('Move Up')).not.toBeDisabled());

      fireEvent.mouseDown(wrapper.getByTitle('Move Up'));
      await waitUntilPass(() =>
        expect(microscopeMock.yAxis.moveRelative).toHaveBeenLastCalledWith(0.01, Length.mm));
    });

    for (const direction of [1, -1]) {
      const BUTTON = direction > 0 ? 'Focus In' : 'Focus Out';

      test(`the axis speeds increases over time until maxspeed when moved ${direction}`, async () => {
        fireEvent.mouseDown(wrapper.getByTitle(BUTTON));
        await waitUntilPass(() =>
          expect(microscopeMock.focusAxis.moveRelative).toHaveBeenCalled());
        jest.advanceTimersByTime(FIRST_STEP_TIME);

        for (let i = 0; i < 7; i++) {
          await waitUntilPass(() =>
            expect(microscopeMock.focusAxis.moveVelocity).toHaveBeenCalledTimes(i + 1));
          jest.advanceTimersByTime(SPEED_INCREASE_DELAY);
        }
        fireEvent.mouseUp(wrapper.getByTitle(BUTTON));
        await waitUntilPass(() =>
          expect(microscopeMock.focusAxis.stop).toHaveBeenCalled());

        expect(microscopeMock.focusAxis.moveVelocity.mock.calls.map(_.head)).toEqual([
          0.02, 0.03, 0.045, 0.0675, 0.10125, 0.15, 0.15,
        ].map(speed => speed * direction));
      });

      test(`the axis moves at a steady rate when shift is pressed ${direction}`, async () => {
        fireEvent.mouseDown(wrapper.getByTitle(BUTTON), { shiftKey: true });
        await waitUntilPass(() =>
          expect(microscopeMock.focusAxis.moveVelocity).toHaveBeenCalled());
        fireEvent.mouseUp(wrapper.getByTitle(BUTTON));
        await waitUntilPass(() =>
          expect(microscopeMock.focusAxis.stop).toHaveBeenCalled());

        expect(microscopeMock.focusAxis.moveVelocity).toHaveBeenCalledTimes(1);
        expect(microscopeMock.focusAxis.moveVelocity).toHaveBeenCalledWith(
          0.15 * direction, Velocity.MILLIMETRES_PER_SECOND);
      });
    }

    test('allows to set position', (async () => {
      const card = getCardContent('Position');
      fireEvent.click(getAllByTitle(card, 'Edit Position')[0]);
      fireEvent.change(getAllByTitle(card, 'Value of Position')[0], { target: { value: '500' } });
      fireEvent.click(getByTitle(card, /Confirm Position/));
      expect(microscopeMock.focusAxis.moveAbsolute).toHaveBeenCalledWith(500, Length.mm);
    }));

    test('changes units for a position', async () => {
      const card = getCardContent('Position');

      microscopeMock.focusAxis._position = 5000;
      microscopeMock.xAxis._position = 2000;
      microscopeMock.yAxis._position = 3000;
      TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
      await waitUntilPass(() => expect(getAxisPositions()).toEqual(['5', '2', '3']));

      fireEvent.change(getAllByTitle(card, 'Units of Position')[0], { target: { value: Length.MICROMETRES } });
      fireEvent.change(getAllByTitle(card, 'Units of Position')[1], { target: { value: Length.CENTIMETRES } });
      expect(getAxisPositions()).toEqual(['5000', '0.2', '3']);
    });
  });

  describe('camera trigger', () => {
    let card: HTMLElement;
    beforeEach(() => {
      card = getCardContent(/Camera Trigger/).parentElement!;
    });

    test('triggers camera', async () => {
      fireEvent.click(getByText(card, 'Trigger'));
      getByText(card, 'Triggering...');
      await waitUntilPass(() =>
        expect(microscopeMock.cameraTrigger.trigger).toHaveBeenCalledWith(330, Time.ms));
    });

    test('loads settings', async () => {
      fireEvent.click(getByTitle(card, 'Settings'));
      expect(getByTitle(card, /Value of Exposure/)).toHaveValue(330);
      expect(getByTitle(card, 'Value of Wait after Pulse')).toHaveValue(350);
      expect(getByTitle(card, 'Value of Wait before Pulse')).toHaveValue(360);
    });

    test('changes, saves, and applies settings', async () => {
      fireEvent.click(getByTitle(card, 'Settings'));

      fireEvent.change(getByTitle(card, /Units of Exposure/), { target: { value: Time.SECONDS } });
      fireEvent.change(getByTitle(card, /Value of Exposure/), { target: { value: '0.123' } });
      fireEvent.change(getByTitle(card, 'Value of Wait before Pulse'), { target: { value: '3' } });
      fireEvent.change(getByTitle(card, 'Value of Wait after Pulse'), { target: { value: '4' } });
      fireEvent.click(getByText(card, 'Save'));

      await waitUntilPass(() => {
        const storage = microscopeMock.cameraTrigger.device.storage;
        expect(storage.setNumber).toHaveBeenCalledWith('zaber.microscope.trigger.exposure', 123);
        expect(storage.setNumber).toHaveBeenCalledWith('zaber.microscope.trigger.wait_before', 3);
        expect(storage.setNumber).toHaveBeenCalledWith('zaber.microscope.trigger.wait_after', 4);
      });

      fireEvent.click(getByText(card, 'Trigger'));
      await waitUntilPass(() =>
        expect(microscopeMock.cameraTrigger.trigger).toHaveBeenCalledWith(0.123, Time.SECONDS));
    });

    test('reverts settings after cancel', async () => {
      fireEvent.click(getByTitle(card, 'Settings'));
      fireEvent.change(getByTitle(card, 'Value of Wait before Pulse'), { target: { value: '123' } });
      fireEvent.click(getByText(card, 'Cancel'));
      fireEvent.click(getByTitle(card, 'Settings'));
      expect(getByTitle(card, 'Value of Wait before Pulse')).toHaveValue(360);
    });
  });

  describe('stored positions', () => {
    function getStoredPositions() {
      return _.find(storageMock.stored, (_v, key) => key.startsWith(POSITIONS_STORAGE_KEY)) as StoredPositions;
    }

    test('saves position to storage', (async () => {
      const card = getCardContent('Position').parentElement!;
      const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;

      microscopeMock.xAxis._position = 11000;
      microscopeMock.yAxis._position = 12000;
      microscopeMock.focusAxis._position = 13000;
      TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
      await waitUntilPass(() => expect(getAxisPositions()[0]).toBe('13'));

      fireEvent.click(getByTitle(card, 'Save Position'));

      const storeKey = Object.keys(storageMock.stored).find(key => key.startsWith(POSITIONS_STORAGE_KEY));
      expect(storeKey).toBe(`${POSITIONS_STORAGE_KEY}${connection.key}`);

      expect(getStoredPositions()).toEqual({
        positions: [expect.anything(), {
          label: 'Position 02',
          focusAxis: { units: Length.mm, value: 11.9 }, // -1100 for focus offset
          xAxis: { units: Length.mm, value: 11 },
          yAxis: { units: Length.mm, value: 12 },
        }],
        version: 1,
      });
    }));

    test('renames position', (async () => {
      const card = getCardContent('Position');

      fireEvent.click(getByText(card, 'Position 01'));
      const input = wrapper.getByDisplayValue('Position 01');
      fireEvent.change(input, { target: { value: 'Some cell' } });
      fireEvent.blur(input);

      expect(getStoredPositions()).toEqual({
        positions: [{
          label: 'Some cell',
          focusAxis: expect.anything(),
          xAxis: expect.anything(),
          yAxis: expect.anything(),
        }],
        version: 1,
      });
    }));

    test('goes to stored position', (async () => {
      const card = getCardContent('Position');

      fireEvent.click(getByTitle(card, /Go to/));
      await waitUntilPass(() => {
        expect(microscopeMock.focusAxis.moveAbsolute).toHaveBeenCalledWith(31.1, Length.mm);
        expect(microscopeMock.xAxis.moveAbsolute).toHaveBeenCalledWith(10, Length.mm);
        expect(microscopeMock.yAxis.moveAbsolute).toHaveBeenCalledWith(20, Length.mm);
      });
    }));

    test('goes to stored position x,y only', (async () => {
      const card = getCardContent('Position');

      fireEvent.click(getByTitle(card, /Open Menu/));
      fireEvent.click(wrapper.getByText(/Go to [^F]*$/));
      await waitUntilPass(() => {
        expect(microscopeMock.xAxis.moveAbsolute).toHaveBeenCalledWith(10, Length.mm);
        expect(microscopeMock.yAxis.moveAbsolute).toHaveBeenCalledWith(20, Length.mm);
        expect(microscopeMock.focusAxis.moveAbsolute).not.toHaveBeenCalled();
      });
    }));

    test('deletes position', (async () => {
      const card = getCardContent('Position');

      fireEvent.click(getByTitle(card, /Open Menu/));
      fireEvent.click(wrapper.getByText(/Delete/));
      expect(getStoredPositions()).toEqual({
        positions: [],
        version: 1,
      });
    }));

    test('copies to clipboard', (async () => {
      (navigator as any).clipboard = {
        writeText: jest.fn(asyncNoop),
      };
      const card = getCardContent('Position');
      fireEvent.click(getByTitle(card, /Open Menu/));
      fireEvent.click(wrapper.getByText(/Copy/));
      expect(navigator.clipboard.writeText).toHaveBeenCalledWith('10, 20, 31.1');
    }));
  });

  describe('autofocus', () => {
    let autofocus: HTMLElement;

    beforeEach(() => {
      autofocus = getCardContent(/Autofocus/);
    });

    test('focus buttons disabled when out of range', async () => {
      microscopeMock.autofocus.status.inRange = false;
      TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
      await waitUntilPass(() => {
        expect(getByText(autofocus, 'Focus Once')).toBeDisabled();
        expect(getByText(autofocus, 'Hold Focus')).toBeDisabled();
        expect(getByText(autofocus, 'Set Focus')).toBeDisabled();
        expect(getByText(autofocus, /Scan/)).not.toBeDisabled();
      });
    });

    test('focus buttons disabled when laser is off', async () => {
      WdiAutofocusProviderMock.instance!.laserOn = false;
      TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
      await waitUntilPass(() => {
        expect(getByText(autofocus, 'Focus Once')).toBeDisabled();
        expect(getByText(autofocus, 'Hold Focus')).toBeDisabled();
        expect(getByText(autofocus, 'Set Focus')).toBeDisabled();
        expect(getByText(autofocus, /Scan/)).toBeDisabled();
      });
    });

    test('focuses once', async () => {
      fireEvent.click(getByText(autofocus, 'Focus Once'));
      await waitUntilPass(() => expect(microscopeMock.autofocus.focusOnce).toHaveBeenCalled());
    });

    test('zeros focus', async () => {
      fireEvent.click(getByText(autofocus, 'Set Focus'));
      await waitUntilPass(() => expect(microscopeMock.autofocus.setFocusZero).toHaveBeenCalled());
    });

    test('hold focus until stopped', async () => {
      fireEvent.click(getByText(autofocus, 'Hold Focus'));
      await waitUntilPass(() => expect(microscopeMock.autofocus.startFocusLoop).toHaveBeenCalled());
      fireEvent.click(getByText(autofocus, /Stop/));
      await waitUntilPass(() => expect(microscopeMock.autofocus.stopFocusLoop).toHaveBeenCalled());
    });

    test('hold focus retrieves error by calling stopFocusLoop', async () => {
      microscopeMock.autofocus.stopFocusLoop.mockRejectedValueOnce(new Error('Error occurred during tracking'));

      fireEvent.click(getByText(autofocus, 'Hold Focus'));
      await waitUntilPass(() => expect(microscopeMock.autofocus.startFocusLoop).toHaveBeenCalled());

      microscopeMock.focusAxis._isBusy = false; // error
      TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);

      await waitUntilPass(() => getByText(autofocus, /Error occurred during tracking/));
    });

    test('enables/disables laser', async () => {
      const toggle = getByRole(autofocus.parentElement!, 'checkbox');
      fireEvent.click(toggle);
      await waitUntilPass(() => expect(WdiAutofocusProviderMock.instance!.disableLaser).toBeCalled());
      fireEvent.click(toggle);
      await waitUntilPass(() => expect(WdiAutofocusProviderMock.instance!.enableLaser).toBeCalled());
    });

    test('closes wdi provider in case of an unrelated error in monitor loop', async () => {
      microscopeMock.focusAxis.getPosition.mockRejectedValue(new Error('Cannot get position'));
      TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
      await waitUntilPass(() => expect(WdiAutofocusProviderMock.instance!.close).toHaveBeenCalled());
    });

    test('sets autofocus limits', async () => {
      fireEvent.click(getByTitle(autofocus, 'Edit Limit min'));
      const minInput = getAllByRole(autofocus, 'spinbutton')[0];
      fireEvent.change(minInput, { target: { value: '5' } });
      fireEvent.click(getByTitle(autofocus, 'Confirm Limit min edit'));

      await waitUntilPass(() => expect(microscopeMock.focusAxis.settings.values['motion.tracking.limit.min']).toBe(5000));
    });

    test('edits settings', async () => {
      const openIcon = getByTitle(autofocus.parentElement!, 'Settings');
      const dialog = openIcon.parentElement!;

      microscopeMock.autofocus.getObjectiveParameters.mockImplementation(async objective => ([
        { name: 'control_loop_ki', value: 200 + objective * 10 },
        { name: 'control_loop_kp', value: round(objective * 0.1, 1) },
        { name: 'magnification', value: WDI_MAGNIFICATIONS[objective] },
        { name: 'max_exposure', value: 600 + objective * 10 },
        { name: 'signal_to_noise_ratio', value: 0.02 + objective * 0.01 },
      ]));

      fireEvent.click(openIcon);

      await waitUntilPass(() => {
        expect(getByLabelText(dialog, 'Control Loop Ki')).toHaveValue(210);
        expect(getByLabelText(dialog, 'Magnification')).toHaveValue('2');
      });

      fireEvent.change(getByLabelText(dialog, 'Objective'), { target: { value: '2' } });
      expect(getByLabelText(dialog, 'Control Loop Ki')).toHaveValue(230);
      expect(getByLabelText(dialog, 'Control Loop Kp')).toHaveValue(0.3);
      expect(getByLabelText(dialog, 'Magnification')).toHaveValue('10');
      expect(getByLabelText(dialog, 'Max Exposure')).toHaveValue(630);
      expect(getByLabelText(dialog, 'Signal to Noise Ratio')).toHaveValue(0.05);

      fireEvent.change(getByLabelText(dialog, 'Magnification'), { target: { value: '50' } });
      fireEvent.change(getByLabelText(dialog, 'Control Loop Ki'), { target: { value: 300 } });
      fireEvent.change(getByLabelText(dialog, 'Control Loop Kp'), { target: { value: 0.1 } });
      fireEvent.change(getByLabelText(dialog, 'Max Exposure'), { target: { value: '650' } });
      fireEvent.change(getByLabelText(dialog, 'Signal to Noise Ratio'), { target: { value: '0.08' } });

      fireEvent.change(getByLabelText(dialog, 'Objective'), { target: { value: '0' } });
      fireEvent.change(getByLabelText(dialog, 'Control Loop Ki'), { target: { value: 150 } });

      fireEvent.click(getByText(dialog, 'Save'));

      await waitUntilPass(() => {
        expect(microscopeMock.autofocus.setObjectiveParameters).toHaveBeenCalledWith(1, [
          { name: 'control_loop_ki', value: 150 },
          expect.anything(),
          { name: 'magnification', value: 2 },
          expect.anything(),
          expect.anything(),
        ]);
        expect(microscopeMock.autofocus.setObjectiveParameters).toHaveBeenCalledWith(3, [
          { name: 'control_loop_ki', value: 300 },
          { name: 'control_loop_kp', value: 0.1 },
          { name: 'magnification', value: 50 },
          { name: 'max_exposure', value: 650 },
          { name: 'signal_to_noise_ratio', value: 0.08 },
        ]);
      });
    });
  });

  describe('presets', () => {
    function getStepValues() {
      return getAllByTitle(getCardContent('Position'), 'Value of Step').map(e => (e as HTMLInputElement).value);
    }

    test('applies entire preset', (async () => {
      fireEvent.change(wrapper.getByPlaceholderText('Select preset'), { target: { value: '0' } });
      expect(wrapper.getByPlaceholderText('Select preset')).toBeDisabled();
      wrapper.getByText('Applying...');
      await waitUntilPass(() => wrapper.getByText(/Applied/));
      expect(wrapper.getByPlaceholderText('Select preset').getAttribute('data-current')).toEqual(PRESET1.name);

      expect(microscopeMock.filterChanger.change).toBeCalledWith(PRESET1.data.filter.value);
      expect(microscopeMock.objectiveChanger.change).toBeCalledWith(PRESET1.data.objective.value, expect.anything());

      for (const channelKey of [1, 2] as const) {
        const data = PRESET1.data.illuminator[channelKey];
        const channel = microscopeMock.illuminator.getChannel(channelKey);
        expect(channel.setOn).toBeCalledWith(data.on.value);
        expect(channel.setIntensity).toBeCalledWith(data.intensity.value);
      }

      for (const axisKey of ['focusAxis', 'xAxis', 'yAxis'] as const) {
        const data = PRESET1.data.axes[axisKey];
        const axis = microscopeMock[axisKey];
        expect(axis.moveAbsolute).toBeCalledWith(data.position.value.value, Length.mm);
      }
      expect(getStepValues()).toEqual([
        '30', '60', '62',
      ]);

      expect(microscopeMock.autofocus.setLimitMin).toHaveBeenCalledWith(10, Length.mm);
      expect(microscopeMock.autofocus.setLimitMax).toHaveBeenCalledWith(20, Length.mm);
      expect(microscopeMock.autofocus.setObjectiveParameters).toHaveBeenCalledWith(
        PRESET1.data.objective.value,
        [
          { name: 'control_loop_ki', value: 0.5 },
          { name: 'control_loop_kp', value: 0.1 },
          { name: 'magnification', value: 20 },
        ]);
    }));

    test('does not apply non-applicable preset', (async () => {
      fireEvent.change(wrapper.getByPlaceholderText('Select preset'), { target: { value: '1' } });
      await waitUntilPass(() => wrapper.getByText(/Applied/));
      expect(wrapper.getByPlaceholderText('Select preset').getAttribute('data-current')).toEqual(NO_PRESET.name);

      expect(microscopeMock.filterChanger.change).not.toBeCalled();
      expect(microscopeMock.objectiveChanger.change).not.toBeCalled();
      for (const channelKey of [1, 2] as const) {
        const channel = microscopeMock.illuminator.getChannel(channelKey);
        expect(channel.setOn).not.toBeCalled();
        expect(channel.setIntensity).not.toBeCalled();
      }
      for (const axisKey of ['focusAxis', 'xAxis', 'yAxis'] as const) {
        const axis = microscopeMock[axisKey];
        expect(axis.moveAbsolute).not.toBeCalled();
      }
      expect(getStepValues()).toEqual([
        '10', '10', '10',
      ]);
      expect(microscopeMock.autofocus.setLimitMin).not.toBeCalled();
      expect(microscopeMock.autofocus.setLimitMax).not.toBeCalled();
      expect(microscopeMock.autofocus.setObjectiveParameters).not.toBeCalled();
    }));

    test('handles errors when applying', (async () => {
      const selectPreset = () => fireEvent.change(wrapper.getByPlaceholderText('Select preset'), { target: { value: '0' } });

      microscopeMock.filterChanger.change.mockRejectedValueOnce(new Error('Cannot change'));
      selectPreset();
      await waitUntilPass(() => wrapper.getByText('Could not apply preset: Filter Changer: Cannot change'));

      microscopeMock.objectiveChanger.change.mockRejectedValueOnce(new Error('Cannot change'));
      selectPreset();
      await waitUntilPass(() => wrapper.getByText(/Objective Changer: Cannot change/));

      microscopeMock.objectiveChanger.change.mockResolvedValueOnce(undefined);
      microscopeMock.focusAxis.moveAbsolute.mockRejectedValueOnce(new Error('Cannot move'));
      selectPreset();
      await waitUntilPass(() => wrapper.getByText(/Focus Axis: Cannot move/));

      microscopeMock.autofocus.setLimitMin.mockRejectedValueOnce(new Error('Invalid limit'));
      selectPreset();
      await waitUntilPass(() => wrapper.getByText(/Autofocus: Invalid limit/));

      microscopeMock.illuminator.getChannel(1).setOn.mockRejectedValueOnce(new Error('Cannot turn on'));
      selectPreset();
      await waitUntilPass(() => wrapper.getByText(/Illuminator: Cannot turn on/));

      microscopeMock.xAxis.moveAbsolute.mockRejectedValueOnce(new Error('Cannot move'));
      selectPreset();
      await waitUntilPass(() => wrapper.getByText(/Plate: Cannot move/));
    }));

    test('allows to restore when settings change', (async () => {
      // except for axis positions
      const CHANGED = /has additional changes/;

      async function verifyAndRestore() {
        TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
        await waitUntilPass(() => wrapper.getByText(CHANGED));
        fireEvent.click(wrapper.getByText(/Restore/));
        await waitUntilPass(() => expect(wrapper.queryByText(CHANGED)).toBeNull());
      }

      fireEvent.change(wrapper.getByPlaceholderText('Select preset'), { target: { value: '0' } });
      await waitUntilPass(() => wrapper.getByText(/Applied/));
      expect(wrapper.getByPlaceholderText('Select preset').getAttribute('data-current')).toEqual(PRESET1.name);
      expect(wrapper.queryByText(CHANGED)).toBeNull();

      microscopeMock.currentFilter++;
      await verifyAndRestore();
      microscopeMock.currentObjective++;
      await verifyAndRestore();
      for (const channelKey of [1, 2] as const) {
        const channel = microscopeMock.illuminator.getChannel(channelKey);
        channel._isOn = !channel._isOn;
        await verifyAndRestore();
        channel._intensity = 0.1;
        await verifyAndRestore();
      }
      const motion = getCardContent('Position');
      for (let i = 0; i < 3; i++) {
        fireEvent.change(getAllByTitle(motion, 'Value of Step')[i], { target: { value: '50' } });
        await verifyAndRestore();
      }
    }));
  });

  describe('test', () => {
    test('restarts devices', (async () => {
      const connection = microscopeMock.connection;

      fireEvent.click(wrapper.getAllByTitle('Open Menu')[0]);
      fireEvent.click(wrapper.getByText(/Restart Microscope/));

      const RESTARTING = /Restarting microscope/;
      wrapper.getByText(RESTARTING);
      await waitUntilPass(() => expect(wrapper.queryByText(RESTARTING)).not.toBeNull());

      expect(connection.genericCommandNoResponse).toHaveBeenCalledWith('system reset');
    }));
  });

  describe('factory reset', () => {
    test('resets devices', (async () => {
      const connection = microscopeMock.connection;
      const device = connection.getDevice(1);

      fireEvent.click(wrapper.getAllByTitle('Open Menu')[0]);
      fireEvent.click(wrapper.getByText(/Factory Reset/));

      wrapper.getByText(/Are you sure/);

      fireEvent.click(wrapper.getByText('Reset'));
      await waitUntilPass(() => wrapper.getByText('Resetting...'));
      await waitUntilPass(() => expect(wrapper.queryByText(/\w+ Factory Reset/)).toBeNull());

      expect(device.restore).toHaveBeenCalled();
    }));


    test('reports errors', (async () => {
      const connection = microscopeMock.connection;
      const dev = connection.getDevice(1);
      dev.restore.mockRejectedValueOnce(new Error('Cannot reset'));

      fireEvent.click(wrapper.getAllByTitle('Open Menu')[0]);
      fireEvent.click(wrapper.getByText(/Factory Reset/));

      wrapper.getByText(/Are you sure/);

      fireEvent.click(wrapper.getByText('Reset'));
      await waitUntilPass(() => wrapper.getByText('Resetting...'));
      await waitUntilPass(() => wrapper.queryByText('Failed Factory Reset'));
      wrapper.getByText(/While resetting/);
    }));
  });

  describe('presets editor', () => {
    function getDialogRoot() {
      return wrapper.getAllByText('Presets')[1].closest('.modal')! as HTMLElement;
    }
    beforeEach(async () => {
      fireEvent.click(wrapper.getByText('Edit Presets'));
    });

    test('adds preset using the current values', (() => {
      const dialog = getDialogRoot();
      fireEvent.click(getByTitle(dialog, 'Add Preset'));
      fireEvent.click(getByText(dialog, 'Save'));

      const presets = getStoredPresets();
      const newPreset = _.last(presets.presets)!;

      expect(newPreset.name).toEqual('Preset 3');
      expect(newPreset.data.filter.value).toEqual(microscopeMock.currentFilter);
      expect(newPreset.data.objective.value).toEqual(microscopeMock.currentObjective);
      for (const channelKey of [1, 2] as const) {
        const data = newPreset.data.illuminator[channelKey];
        const channel = microscopeMock.illuminator.getChannel(channelKey);
        expect(data.on.value).toEqual(channel._isOn);
        expect(data.intensity.value).toEqual(channel._intensity);
      }
      for (const axisKey of ['focusAxis', 'xAxis', 'yAxis'] as const) {
        const data = newPreset.data.axes[axisKey];
        const axis = microscopeMock[axisKey];
        expect(data.position.value).toEqual({ value: axis._position, units: Length.mm });
        expect(data.step.value).toEqual({ value: 10, units: Length.MICROMETRES });
      }
    }));

    test('apply checkboxes works', () => {
      const dialog = getDialogRoot();
      fireEvent.click(getByTitle(dialog, 'Add Preset'));
      for (const checkbox of getAllByRole(dialog, 'checkbox')) {
        fireEvent.click(checkbox);
      }
      fireEvent.click(getByText(dialog, 'Save'));

      const presets = getStoredPresets();
      const newPreset = _.last(presets.presets)!;

      const allApplyAreTrue = _.isEqualWith(newPreset.data, newPreset.data, (a, b, name) => {
        if (name === 'apply') {
          return a === true && b === true;
        }
        return;
      });
      expect(allApplyAreTrue).toBeTruthy();
    });

    test('can edit preset', (() => {
      const dialog = getDialogRoot();
      fireEvent.change(getByPlaceholderText(dialog, 'Select preset'), { target: { value: '1' } });
      expect(getByPlaceholderText(dialog, 'Preset Name')).toHaveValue('No Preset');

      fireEvent.change(getByPlaceholderText(dialog, 'Preset Name'), { target: { value: 'New Name' } });
      fireEvent.change(getByTitle(dialog, 'Filter'), { target: { value: '2' } });
      fireEvent.change(getByTitle(dialog, 'Objective'), { target: { value: '3' } });
      for (const channelKey of [1, 2] as const) {
        fireEvent.change(getByTitle(dialog, `Channel ${channelKey} Power`), { target: { value: 1 } });
        fireEvent.change(getByPlaceholderText(dialog, `Channel ${channelKey} Intensity`),
          { target: { value: `${channelKey * 10}` } });
      }
      for (const axisKey of ['Focus', 'X', 'Y']) {
        const code = axisKey.charCodeAt(0);
        fireEvent.change(getByTitle(dialog, `Value of ${axisKey} Step`), { target: { value: code / 10 } });
        fireEvent.change(getByTitle(dialog, `Value of ${axisKey} Position`), { target: { value: code } });
      }

      fireEvent.change(getByTitle(dialog, 'Value of Exposure (Pulse Width)'), { target: { value: 15 } });
      fireEvent.change(getByTitle(dialog, 'Value of Wait before Pulse'), { target: { value: 25 } });
      fireEvent.change(getByTitle(dialog, 'Value of Wait after Pulse'), { target: { value: 35 } });

      fireEvent.click(getByText(dialog, 'Save'));

      const presets = getStoredPresets();
      const changedPreset = _.first(presets.presets)!;

      expect(changedPreset.name).toEqual('New Name');
      const { data } = changedPreset;
      expect(data.filter.value).toEqual(2);
      expect(data.objective.value).toEqual(3);
      for (const channelKey of [1, 2] as const) {
        const channelData = data.illuminator[channelKey];
        expect(channelData.on.value).toEqual(true);
        expect(channelData.intensity.value).toEqual(channelKey / 10);
      }
      for (const axisKey of ['focusAxis', 'xAxis', 'yAxis'] as const) {
        const axisData = data.axes[axisKey];
        const code = axisKey.toUpperCase().charCodeAt(0);
        expect(axisData.step.value).toEqual({ value: code / 10, units: Length.MICROMETRES });
        expect(axisData.position.value).toEqual({ value: code, units: Length.mm });
      }
      expect(data.cameraTrigger.exposure.value).toEqual({ value: 15, units: Time.ms });
      expect(data.cameraTrigger.waitBefore.value).toEqual({ value: 25, units: Time.ms });
      expect(data.cameraTrigger.waitAfter.value).toEqual({ value: 35, units: Time.ms });
    }));

    test('can delete preset', (() => {
      const dialog = getDialogRoot();
      fireEvent.change(getByPlaceholderText(dialog, 'Select preset'), { target: { value: '1' } });
      fireEvent.click(getByTitle(dialog, 'Delete Preset'));
      fireEvent.click(getByText(dialog, 'Delete'));
      fireEvent.click(getByText(dialog, 'Save'));

      const presets = getStoredPresets();
      expect(presets.presets).toHaveLength(1);
    }));

    test('does not lose current preset during changes', (async () => {
      let dialog = getDialogRoot();
      fireEvent.click(getByText(dialog, 'Cancel'));

      fireEvent.change(wrapper.getByPlaceholderText('Select preset'), { target: { value: '1' } });
      await waitUntilPass(() => wrapper.getByText(/Applied/));

      fireEvent.click(wrapper.getByText('Edit Presets'));
      dialog = getDialogRoot();

      fireEvent.change(getByPlaceholderText(dialog, 'Select preset'), { target: { value: '0' } });
      fireEvent.click(getByTitle(dialog, 'Delete Preset'));
      fireEvent.click(getByText(dialog, 'Delete'));
      fireEvent.click(getByText(dialog, 'Save'));

      expect(wrapper.getByPlaceholderText('Select preset').getAttribute('data-current')).toEqual(NO_PRESET.name);
    }));

    test('loads current values', (async () => {
      microscopeMock.currentFilter = 3;
      microscopeMock.currentObjective = 4;
      for (const channelKey of [1, 2] as const) {
        microscopeMock.illuminator.getChannel(channelKey)._isOn = true;
        microscopeMock.illuminator.getChannel(channelKey)._intensity = 0.5;
      }
      for (const axisKey of ['focusAxis', 'xAxis', 'yAxis'] as const) {
        microscopeMock[axisKey]._position = 1000;
      }
      TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
      await waitUntilPass(() => {
        expect(wrapper.getByTitle('Current Filter')).toHaveValue('3');
      });

      const dialog = getDialogRoot();
      fireEvent.change(getByPlaceholderText(dialog, 'Select preset'), { target: { value: '1' } });
      expect(getByPlaceholderText(dialog, 'Preset Name')).toHaveValue(NO_PRESET.name);
      fireEvent.click(getByTitle(dialog, 'Open Menu'));
      fireEvent.click(wrapper.getByText('Load Current Values'));
      fireEvent.click(getByText(dialog, 'Save'));

      const presets = getStoredPresets();
      const changedPreset = presets.presets.find(p => p.name === NO_PRESET.name)!;

      const isEveryValueChanged = _.isEqualWith(changedPreset.data, NO_PRESET.data, (a, b) => {
        if (isPresetValue(a) && isPresetValue(b) && a.value !== b.value) {
          return true;
        }
        return;
      });
      expect(isEveryValueChanged).toBeTruthy();
    }));
  });
});


describe('production mode', () => {
  beforeEach(async () => {
    storageMock.stored[PRODUCTION_MODE_STORAGE_KEY] = true;

    wrapper = render(<TestApp/>);
    mockSingleDevice(TestApp.testStore);
    const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    await waitUntilPass(() => expect(wrapper.getAllByText(/Presets/)));
  });

  test('disables/enables production mode', () => {
    function menuClick() {
      fireEvent.click(wrapper.getAllByTitle('Open Menu')[0]);
      fireEvent.click(wrapper.getByText('Production Mode'));
    }

    wrapper.getByText('Production Tools');

    menuClick();
    expect(storageMock.stored[PRODUCTION_MODE_STORAGE_KEY]).toBe(false);
    expect(wrapper.queryByText('Production Tools')).toBeNull();

    menuClick();
    expect(storageMock.stored[PRODUCTION_MODE_STORAGE_KEY]).toBe(true);
    wrapper.getByText('Production Tools');
  });

  test('runs full test', async () => {
    fireEvent.click(wrapper.getByText('Full Test'));
    await waitUntilPass(() => expect(wrapper.getByText('Full Test')).not.toBeDisabled());
    // we don't really care about the specific operations of the test
  });

  test('handles test error', async () => {
    setMicroscopeCallback(microscope => {
      microscope.initialize.mockRejectedValueOnce(new Error('Cannot initialize'));
    });
    fireEvent.click(wrapper.getByText('Full Test'));
    await waitUntilPass(() => wrapper.getByText(/Cannot initialize/));
  });

  test('tests focus axis when there is no objective changer', async () => {
    setMicroscopeCallback(microscope => {
      microscope.objectiveChanger = null!;
    });
    fireEvent.click(wrapper.getByText('Full Test'));
    await waitUntilPass(() => expect(wrapper.getByText('Full Test')).not.toBeDisabled());
    expect(microscopeMock.focusAxis.moveMin).toHaveBeenCalled();
  });

  test('forces initialization', async () => {
    setMicroscopeCallback(microscope => {
      microscope.objectiveChanger = null!;
    });
    fireEvent.click(wrapper.getByText('Home'));
    await waitUntilPass(() => expect(wrapper.getByText('Home')).not.toBeDisabled());

    expect(microscopeMock.initialize).toHaveBeenCalledWith({ force: true });
  });
});
