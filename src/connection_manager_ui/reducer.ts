import { type EntityKey } from '../keys';
import { changeDictionary, createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';

export const setConnectionNameDefault: State['setConnectionsName'][string] = {
  writing: false,
  error: null,
};

// Additional state for connection manager containing UI-related state.
export interface State {
  setConnectionsName: Record<string, {
      writing: boolean;
      error: string | null;
    }>;
  setLabel: {
    isOpen: boolean;
    entityKey: EntityKey | null;
    label: string;
    inProgress: boolean;
    error: string | null;
  };
  devicesSystemErrors: Record<string, string[]>;
}

const initialState: State = {
  setConnectionsName: {},
  setLabel: {
    isOpen: false,
    entityKey: null,
    label: '',
    inProgress: false,
    error: null,
  },
  devicesSystemErrors: {},
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const setLabelOpen: Reducer<ActionTypes.SET_LABEL_OPEN> = (state, { entityKey, label }) => ({
  ...state,
  setLabel: {
    ...state.setLabel,
    isOpen: true,
    entityKey,
    label,
  },
});

const setLabelClose: Reducer<ActionTypes.SET_LABEL_CLOSE> = state => ({
  ...state,
  setLabel: initialState.setLabel,
});

const setLabelSetLabel: Reducer<ActionTypes.SET_LABEL_SET_LABEL> = (state, { label }) => ({
  ...state,
  setLabel: {
    ...state.setLabel,
    label,
  },
});

const setLabelWrite: Reducer<ActionTypes.SET_LABEL_WRITE> = state => ({
  ...state,
  setLabel: {
    ...state.setLabel,
    inProgress: true,
    error: null,
  },
});

const setLabelWriteDone: Reducer<ActionTypes.SET_LABEL_WRITE_DONE> = (state, { error }) => ({
  ...state,
  setLabel: error ? {
    ...state.setLabel,
    inProgress: false,
    error,
  } : initialState.setLabel,
});

const setConnectionName = (state: State, { connectionKey }: ActionsToPayloads[ActionTypes.SET_CONNECTION_NAME]): State =>
  ({
    ...state,
    setConnectionsName: changeDictionary(state.setConnectionsName, connectionKey, {
      writing: true,
      error: null,
    }, () => setConnectionNameDefault),
  });

const setConnectionNameDone = (state: State, { connectionKey, err }: ActionsToPayloads[ActionTypes.SET_CONNECTION_NAME_DONE]): State =>
  ({
    ...state,
    setConnectionsName: changeDictionary(state.setConnectionsName, connectionKey, {
      writing: false,
      error: err ?? null,
    })
  });


const setDeviceSystemErrors = (state: State, { deviceKey, errors }: ActionsToPayloads[ActionTypes.SET_DEVICE_SYSTEM_ERRORS]) => ({
  ...state,
  devicesSystemErrors: {
    ...state.devicesSystemErrors,
    [deviceKey]: errors,
  },
});

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.SET_LABEL_OPEN]: setLabelOpen,
  [ActionTypes.SET_LABEL_CLOSE]: setLabelClose,
  [ActionTypes.SET_LABEL_SET_LABEL]: setLabelSetLabel,
  [ActionTypes.SET_LABEL_WRITE]: setLabelWrite,
  [ActionTypes.SET_LABEL_WRITE_DONE]: setLabelWriteDone,
  [ActionTypes.SET_CONNECTION_NAME]: setConnectionName,
  [ActionTypes.SET_CONNECTION_NAME_DONE]: setConnectionNameDone,
  [ActionTypes.SET_DEVICE_SYSTEM_ERRORS]: setDeviceSystemErrors,
}, initialState);
