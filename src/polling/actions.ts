import type { GetSetting } from '@zaber/motion/ascii';

import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

import type { State } from './reducer';
import type { GetIoPort } from './types';


export enum ActionTypes {
  START_POLLING_SETTING = 'POLLING-START_POLLING_SETTING',
  STOP_POLLING_SETTING = 'POLLING-STOP_POLLING_SETTING',

  START_POLLING_ANALOG_IO = 'POLLING-START_POLLING_ANALOG_IO',
  STOP_POLLING_ANALOG_IO = 'POLLING-STOP_POLLING_ANALOG_IO',

  START_POLLING_FLAGS = 'POLLING-START_POLLING_FLAGS',
  STOP_POLLING_FLAGS = 'POLLING-STOP_POLLING_FLAGS',
  CLEAR_FLAGS = 'POLLING-CLEAR_FLAGS',

  START_POLLING_TRIGGERS = 'POLLING-START_POLLING_TRIGGERS',
  STOP_POLLING_TRIGGERS = 'POLLING-STOP_POLLING_TRIGGERS',

  RECEIVE_RESULTS = 'POLLING-RECEIVE_RESULTS',
  POLL_NOW = 'POLLING-POLL_NOW',
}

export interface ActionsToPayloads {
  [ActionTypes.START_POLLING_SETTING]: { deviceKey: EntityKey; poll: GetSetting; fast: boolean  };
  [ActionTypes.STOP_POLLING_SETTING]: { deviceKey: EntityKey; poll: GetSetting; fast: boolean };

  [ActionTypes.START_POLLING_ANALOG_IO]: { deviceKey: EntityKey; port: GetIoPort; fast: boolean };
  [ActionTypes.STOP_POLLING_ANALOG_IO]: { deviceKey: EntityKey; port: GetIoPort; fast: boolean };

  [ActionTypes.START_POLLING_FLAGS]: { deviceKey: EntityKey; axisNumber: number };
  [ActionTypes.STOP_POLLING_FLAGS]: { deviceKey: EntityKey; axisNumber: number };
  [ActionTypes.CLEAR_FLAGS]: { deviceOrAxisKey: EntityKey };

  [ActionTypes.START_POLLING_TRIGGERS]: { deviceKey: EntityKey };
  [ActionTypes.STOP_POLLING_TRIGGERS]: { deviceKey: EntityKey };

  [ActionTypes.RECEIVE_RESULTS]: { results: State['results'] };
  [ActionTypes.POLL_NOW]: null;
}

type K = keyof ActionsToPayloads;
const buildAction = (type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actionDefinitions = {
  startPollingSetting: (deviceKey: EntityKey, poll: GetSetting, fast: boolean) => (
    buildAction(ActionTypes.START_POLLING_SETTING, { deviceKey, poll, fast })
  ),
  stopPollingSetting: (deviceKey: EntityKey, poll: GetSetting, fast: boolean) => (
    buildAction(ActionTypes.STOP_POLLING_SETTING, { deviceKey, poll, fast })
  ),

  startPollingAnalogIo: (deviceKey: EntityKey, port: GetIoPort, fast: boolean) => (
    buildAction(ActionTypes.START_POLLING_ANALOG_IO, { deviceKey, port, fast })
  ),
  stopPollingAnalogIo: (deviceKey: EntityKey, port: GetIoPort, fast: boolean) => (
    buildAction(ActionTypes.STOP_POLLING_ANALOG_IO, { deviceKey, port, fast })
  ),

  startPollingFlags: (deviceKey: EntityKey, axisNumber: number) => (
    buildAction(ActionTypes.START_POLLING_FLAGS, { deviceKey, axisNumber })
  ),
  stopPollingFlags: (deviceKey: EntityKey, axisNumber: number) => (
    buildAction(ActionTypes.STOP_POLLING_FLAGS, { deviceKey, axisNumber })
  ),
  clearFlags: (deviceOrAxisKey: EntityKey) => buildAction(ActionTypes.CLEAR_FLAGS, { deviceOrAxisKey }),

  startPollingTriggers: (deviceKey: EntityKey) => (
    buildAction(ActionTypes.START_POLLING_TRIGGERS, { deviceKey })),
  stopPollingTriggers: (deviceKey: EntityKey) => (
    buildAction(ActionTypes.STOP_POLLING_TRIGGERS, { deviceKey })),

  receiveResults: (results: State['results']) => buildAction(ActionTypes.RECEIVE_RESULTS, { results }),
  pollNow: () => buildAction(ActionTypes.POLL_NOW),
};
