import React from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, Text, Button } from '@zaber/react-library';

import { useActions } from '../utils';
import { Shortcut } from '../components';

import { selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations } from './types';
import { useOperationState } from './hooks';
import { OperationError } from './components';
import { TriggerSettings } from './TriggerSettings';

export const Trigger: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.cameraTrigger;

  const [triggered] = useOperationState(Operations.triggerCamera);
  const trigger = () => actions.operationStart(Operations.triggerCamera);

  if (stableInfo == null) {
    return null;
  }

  return <HeaderCard
    className="controls trigger"
    header={<>
      <Text t={Text.Type.H4}>Camera Trigger</Text>
      <div className="icons">
        <TriggerSettings/>
      </div>
    </>}>

    <div className="row">
      <Button disabled={triggered} color="grey"
        onClick={trigger}>
        Trigger
      </Button>
      <Shortcut k=" " ctrl disabled={triggered} onDown={trigger}/>

      {triggered && <Text>Triggering...</Text>}
    </div>

    {[
      Operations.triggerCamera,
    ].map(op => (
      <OperationError key={op} operation={op}/>
    ))}
  </HeaderCard>;
};
