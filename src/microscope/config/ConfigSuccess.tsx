import React from 'react';
import { useSelector } from 'react-redux';
import { NoticeBanner } from '@zaber/react-library';

import { isMatch, useActions } from '../../utils';
import { actions as actionsDefinition } from '../actions';

import { selectWriting } from './selectors';

export const ConfigSuccess: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const writing = useSelector(selectWriting);
  if (!isMatch({ success: true }, writing)) { return null }
  return <NoticeBanner type="success" closer={() => actions.resetConfigDialog()}>
    Component setup successful.
  </NoticeBanner>;
};
