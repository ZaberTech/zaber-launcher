import React, { memo } from 'react';
import classNames from 'classnames';

import type { HistoryItem } from './types';
import type { ErrorDetailsType } from './ErrorDetails';

function tagPartChecksum(historyItem: HistoryItem, onClickError: (type: ErrorDetailsType) => void): React.ReactElement {
  const { type } = historyItem.parsed;
  if (type === 'unknown') {
    throw new Error('Error flag details expected reply or alert message');
  }
  const { checksum } = historyItem.parsed;
  return <>
    {checksum && <>
      <span className="checksum">:</span>
      {checksum.error ?
        <span className={classNames('checksum', 'fault')} onClick={() => onClickError('checksum')}>
          {checksum.text}
        </span> :
        <span className="checksum">
          {checksum.text}
        </span>}
    </>}
  </>;
}

function tagPartErrorFlag(historyItem: HistoryItem, onClickError: (type: ErrorDetailsType) => void): React.ReactElement {
  const { type } = historyItem.parsed;
  if (type !== 'reply' && type !== 'alert') {
    throw new Error('Error flag details expected reply or alert message');
  }
  const { errorFlag } = historyItem.parsed;

  return <>
    {errorFlag.type == null ?
      <span className="part">
        {errorFlag.text}
      </span> :
      <span className={classNames('part', errorFlag.type)} onClick={() => onClickError('errorFlag')}>
        {errorFlag.text}
      </span>}
    <span className="part">
      {' '}
    </span>
  </>;
}

function tagCommand(historyItem: HistoryItem, onClickError: (type: ErrorDetailsType) => void): React.ReactElement {
  const { type } = historyItem.parsed;
  if (type !== 'command') {
    throw new Error('Message Tagger expected command message');
  }
  const { device, axis, id, data } = historyItem.parsed;

  return <>
    <span className="part">
      /{device}{device && ' '}{axis}{axis && ' '}{id}{id && ' '}{data}
    </span>
    {tagPartChecksum(historyItem, onClickError)}
  </>;
}

function tagReply(historyItem: HistoryItem, onClickError: (type: ErrorDetailsType) => void): React.ReactElement {
  const { type } = historyItem.parsed;
  if (type !== 'reply') {
    throw new Error('Message Tagger expected reply message');
  }
  const { device, axis, id, replyFlag, status, data } = historyItem.parsed;

  return <>
    <span className="part">
      @{device} {axis} {id}{id && ' '}
    </span>
    {replyFlag === 'OK' ?
      <span className="part">
        {replyFlag}
      </span> :
      <span className={classNames('part', 'fault')} onClick={() => onClickError('rejectedCommand')}>
        {replyFlag}
      </span>}
    <span className="part">
      {' '}{status}{' '}
    </span>
    {tagPartErrorFlag(historyItem, onClickError)}
    {replyFlag === 'OK' ?
      <span className="part">
        {data}
      </span> :
      <span className={classNames('part', 'fault')} onClick={() => onClickError('rejectedCommand')}>
        {data}
      </span>}
    {tagPartChecksum(historyItem, onClickError)}
  </>;
}

function tagInfo(historyItem: HistoryItem, onClickError: (type: ErrorDetailsType) => void): React.ReactElement {
  const { type } = historyItem.parsed;
  if (type !== 'info') {
    throw new Error('Message Tagger expected info message');
  }
  const { device, axis, id, data } = historyItem.parsed;

  return <>
    <span className="part">
      #{device} {axis} {id}{id && ' '}{data}
    </span>
    {tagPartChecksum(historyItem, onClickError)}
  </>;
}

function tagAlert(historyItem: HistoryItem, onClickError: (type: ErrorDetailsType) => void): React.ReactElement {
  const { type } = historyItem.parsed;
  if (type !== 'alert') {
    throw new Error('Message Tagger expected alert message');
  }
  const { device, axis, status, data } = historyItem.parsed;

  return <>
    <span className="part">
      !{device} {axis} {status}{' '}
    </span>
    {tagPartErrorFlag(historyItem, onClickError)}
    <span className="part">
      {data && ' '}{data}
    </span>
    {tagPartChecksum(historyItem, onClickError)}
  </>;
}

// eslint-disable-next-line prefer-arrow-callback
export const MessageTagger = memo(function MessageTagger(
  { historyItem, onClickError }: { historyItem: HistoryItem; onClickError: (type: ErrorDetailsType) => void }) {
  const { text } = historyItem;
  const { type } = historyItem.parsed;

  switch (type) {
    case 'command':
      return tagCommand(historyItem, onClickError);
    case 'reply':
      return tagReply(historyItem, onClickError);
    case 'info':
      return tagInfo(historyItem, onClickError);
    case 'alert':
      return tagAlert(historyItem, onClickError);
    default:
      return <span className="part">{text}</span>;
  }
});
