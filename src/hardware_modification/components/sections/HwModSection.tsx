import { Text } from '@zaber/react-library';
import React from 'react';

import { Section } from '../../../components';
import type { SectionIds } from '../../data_definitions';
import type { SectionValues } from '../../types';

import { motionTemplate } from './Motion';
import { motorCharacteristicsTemplate } from './MotorCharacteristics';
import { limitHomeTemplate } from './LimitHome';
import { limitAwayTemplate } from './LimitAway';
import { limitCTemplate } from './LimitC';
import { encoder1Template } from './Encoder1';
import { encoder2Template } from './Encoder2';


export interface SectionProps {
  on: (element: HTMLDivElement) => void;
  off: (element: HTMLDivElement) => void;
}

export interface HwModSectionTemplate<S extends SectionIds> {
  /** The value section this section relies on */
  sectionId: S;
  /** The name in the scrollable view */
  titleName: React.ReactNode;
  /** The name in the menu. If not set, this will be the same as `titleName` */
  menuName?: React.ReactNode;
  /** A description of the section */
  description: string;
  /** A function that will generate the content in the scrollable view */
  generateContent: (values: SectionValues<S>) => React.ReactNode;
}

export const HwModSection: React.FC<SectionProps & HwModSectionTemplate<SectionIds>> = props => {
  const { sectionId, on, off, titleName, description, children } = props;
  return <Section sectionId={sectionId} {...{ on, off }}>
    <div className="section">
      <div className="section-name">
        <Text t={Text.Type.H4}>{titleName}</Text>
      </div>
      <Text className="section-description" e={Text.Emphasis.Light}>{description}</Text>
      {children}
    </div>
  </Section>;
};

export interface ContentProps<S extends SectionIds> {
  values: SectionValues<S>;
}

export const sectionTemplates: HwModSectionTemplate<SectionIds>[] = [
  motionTemplate,
  motorCharacteristicsTemplate,
  limitHomeTemplate,
  limitAwayTemplate,
  limitCTemplate,
  encoder1Template,
  encoder2Template,
];
