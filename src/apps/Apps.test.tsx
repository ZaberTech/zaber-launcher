jest.mock('../environment', () => {
  const actual = jest.requireActual('../environment');
  return ({
    ...actual,
    environment: {
      ...actual.environment,
      edition: 'public',
    },
  });
});

import React from 'react';
import { RenderResult, render, fireEvent, getByTitle, queryByTitle } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { wrapWithNewStore, wrapWithRouter } from '../test';
import { createContainer, destroyContainer } from '../container';
import { mockIpc } from '../ipc/mock';

import { Apps } from './Apps';
import { loadRecent } from './sagas';


const user = userEvent.setup();
const TestApps = wrapWithNewStore(wrapWithRouter(Apps));

let wrapper: RenderResult;

beforeEach(() => {
  const container = createContainer();
  mockIpc(container);

  wrapper = render(<TestApps/>);
});

afterEach(() => {
  destroyContainer();

  wrapper.unmount();
  wrapper = null!;
});

test('renders', () => {
  wrapper.getByText(/Basic Movement/);
  wrapper.getByText(/Device Settings/);
});

test('opens app', () => {
  fireEvent.click(wrapper.queryAllByText(/^Open$/)[0]);
  expect(TestApps.testStore.getState().router.location.pathname).toBe('/basic-controls');
});

test('opens app in new window', () => {
  window.open = jest.fn();
  fireEvent.click(wrapper.getByTitle(/Open Basic Controls in New Window/));
  expect(window.open).toBeCalledWith('http://localhost/#/basic-controls');
});

test('it pins basic controls by default', () => {
  TestApps.testStore.saga.run(loadRecent);
  const app = wrapper.getByText(/Basic Controls/).parentElement!;
  getByTitle(app, 'Pinned in Sidebar');
});

test('pins and unpins the app', () => {
  const app = wrapper.getByText(/Terminal/).parentElement!;
  const star = getByTitle(app, 'Pin in Sidebar');
  expect(star).not.toHaveClass('pinned');
  fireEvent.click(star);
  expect(star).toHaveClass('pinned');
  fireEvent.click(star);
  expect(star).not.toHaveClass('pinned');
});

test('deprecated apps cannot be pinned', () => {
  const app = wrapper.getByText(/Basic Movement/).parentElement!;
  expect(queryByTitle(app, 'Pin in Sidebar')).toBeNull();
});

test('does not show internal apps in public edition', () => {
  expect(wrapper.queryByText(/Cycle Demo/)).toBeNull();
});

test('reveals internal apps when the secret code is typed', async () => {
  await user.keyboard('cycledemo');
  wrapper.getByText(/Cycle Demo/);
});
