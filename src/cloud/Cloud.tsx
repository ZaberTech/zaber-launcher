import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import { Title } from '../components';
import type { RootState } from '../store';

import { selectAuthState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { AuthState } from './types';
import { LoggedIn } from './LoggedIn';
import { CheckingAuth } from './CheckingAuth';
import { UnauthenticatedRoutes } from './UnauthenticatedRoutes';

interface Props {
  actions: typeof actionsDefinition;
  authState: AuthState;
}

class CloudBase extends Component<Props> {
  render(): React.ReactNode {
    const { authState } = this.props;

    return (<div className="cloud">
      <Title>Cloud</Title>
      {authState === AuthState.Checking && <CheckingAuth/>}
      {authState === AuthState.Authenticated && <LoggedIn/>}
      {authState === AuthState.Unauthenticated && <UnauthenticatedRoutes/>}
    </div>);
  }
}

export const Cloud = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    authState: selectAuthState(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(CloudBase);
