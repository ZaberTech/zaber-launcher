import { useActions } from '@zaber/toolbox/lib/redux';
import React from 'react';
import { useSelector } from 'react-redux';

import { Title } from '../components';
import { ConnectionsView } from '../connection_manager';

import { actions as actionsDefinition } from './actions';
import { ApplyHardwareModificationProgressModal } from './ApplyModal';
import { HardwareModification } from './HardwareModification';
import { selectChosenProductKey } from './selectors';

export const HardwareModificationApp: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const chosenProductKey = useSelector(selectChosenProductKey);

  return (
    <div className="connection-view-and-app">
      <Title>Advanced Hardware Setup</Title>

      <ConnectionsView
        selectable={['integrated', 'axis']}
        selected={chosenProductKey}
        onSelect={actions.productChosen}/>

      <div className="app-ui hw-mod">
        <HardwareModification/>
        <ApplyHardwareModificationProgressModal/>
      </div>
    </div>
  );
};
