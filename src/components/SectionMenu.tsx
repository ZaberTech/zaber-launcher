import { MinorMenu } from '@zaber/react-library';
import classNames from 'classnames';
import _ from 'lodash';
import React, { Component, createRef, useEffect, useRef } from 'react';

import { Editions, environment } from '../environment';

type SectionEffect = (element: HTMLDivElement) => void;

interface SectionProps {
  sectionId: string;
  on: SectionEffect;
  off: SectionEffect;
  className?: string;
}

export const Section: React.FC<SectionProps> = ({ sectionId, on, off, children, className }) => {
  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    const element = ref.current!;
    on(element);
    return () => off(element);
  }, []);

  // eslint-disable-next-line react/no-unknown-property
  return <div className={classNames('section', className)} ref={ref} section-id={sectionId}>
    {children}
  </div>;
};

function getSectionIdFromElement(element: Element) {
  return element.getAttribute('section-id')!;
}

interface Props {
  menuItems: { id: string; name: React.ReactNode; disabled?: boolean; isSub?: boolean }[];
  content: (on: SectionEffect, off: SectionEffect) => React.ReactNode;
  refToScrollingSections?: React.RefObject<HTMLDivElement>;
}

interface State {
  activeSection: string;
  scrollToSection: string;
}

export class SectionMenu extends Component<Props, State> {
  settingListObserver!: IntersectionObserver;
  innerRef: React.RefObject<HTMLDivElement>;
  sectionVisibilities: Record<string, number>;

  constructor(props: Props) {
    super(props);
    this.innerRef = createRef<HTMLDivElement>();
    this.state = {
      activeSection: '',
      scrollToSection: '',
    };
    this.sectionVisibilities = {};
    for (const item of props.menuItems) {
      if (item.id != null) {
        this.sectionVisibilities[item.id] = 0;
      }
    }
  }

  compareSectionOrder(id1: string, id2: string) {
    const { menuItems } = this.props;
    const index1 = menuItems.findIndex(item => item.id === id1);
    const index2 = menuItems.findIndex(item => item.id === id2);
    return index1 - index2;
  }

  componentDidMount() {
    this.settingListObserver = new IntersectionObserver(this.onSettingListVisibility, {
      root: this.props.refToScrollingSections?.current ?? this.innerRef.current!,
      threshold: [..._.range(0, 10).map(n => n / 10), 0.99],
    });
  }

  componentWillUnmount() {
    this.settingListObserver.disconnect();
  }

  getActiveSection = (): string => {
    const maxRatio = _.max(Object.values(this.sectionVisibilities)) ?? 0;
    if (maxRatio === 0) { return '' }

    const [visible] = _.partition(
      Object.keys(this.sectionVisibilities),
      sectionId => this.sectionVisibilities[sectionId] >= maxRatio
    );
    return visible.sort(this.compareSectionOrder.bind(this))[0];
  };

  onSettingListVisibility: IntersectionObserverCallback = entries => {
    for (const entry of entries) {
      const section = getSectionIdFromElement(entry.target);
      this.sectionVisibilities[section] = _.ceil(entry.intersectionRatio, 2);
    }
    const newActiveSection = this.getActiveSection();
    if (this.state.activeSection !== newActiveSection) {
      this.setState({ activeSection: newActiveSection });
    }
  };

  sectionOn = (element: HTMLElement) => {
    const sectionId = getSectionIdFromElement(element);
    if (this.sectionVisibilities[sectionId] == null) {
      if (environment.edition === Editions.Dev) {
        throw new Error(`There is no category ${sectionId} defined`);
      } else {
        this.sectionVisibilities[sectionId] = 0;
      }
    }

    if (
      this.settingListObserver.root !== this.props.refToScrollingSections?.current &&
        this.settingListObserver.root !== this.innerRef.current
    ) {
      throw new Error('Root not valid anymore; ensure persistence of the root');
    }
    this.settingListObserver.observe(element);
  };

  sectionOff = (element: HTMLElement) => {
    this.settingListObserver.unobserve(element);

    const sectionId = getSectionIdFromElement(element);
    this.sectionVisibilities[sectionId] = 0;

    this.onSettingListVisibility(this.settingListObserver.takeRecords(), this.settingListObserver);
  };

  scrollToSection(sectionId: string) {
    const ref = this.props.refToScrollingSections ?? this.innerRef;
    ref.current?.querySelector(`[section-id="${sectionId}"]`)?.scrollIntoView({ behavior: 'smooth' });
  }

  render() {
    const { menuItems, content, refToScrollingSections } = this.props;
    const { activeSection } = this.state;
    return <div className="section-menu">
      <MinorMenu className="menu">
        {menuItems.map(({ id, name, isSub, disabled }) => {
          const active = activeSection === id;
          return <div key={id} className={classNames({ active })}>
            <MinorMenu.Item
              className={classNames({ 'is-sub': isSub, disabled })}
              onClick={() => id && !disabled && this.scrollToSection(id)}
            >
              {name}
            </MinorMenu.Item>
          </div>;
        })}
      </MinorMenu>
      <div className="sections" data-testid="scrolling-sections" ref={refToScrollingSections ?? this.innerRef}>
        {content(this.sectionOn, this.sectionOff)}
      </div>
    </div>;
  }
}
