import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

import type { LoadingError, Setting, Sliders, ThirdPartyInfo, TuningInfo } from './types';

export enum ActionTypes {
  SELECT_AXIS_OR_DEVICE = 'CURRENT_TUNER_SELECT_AXIS_OR_DEVICE',
  UPDATE_LOADED = 'CURRENT_TUNER_UPDATE_LOADED',
  LOADING_ERR = 'CURRENT_TUNER_LOADING_ERR',
  UPDATE_SLIDERS = 'CURRENT_TUNER_UPDATE_SLIDERS',
  UPDATE_APPLIED_SLIDERS = 'CURRENT_TUNER_UPDATE_APPLIED_SLIDERS',
  UPDATE_SLIDER_VALUES = 'CURRENT_TUNER_UPDATE_SLIDER_VALUES',
  APPLY_SLIDER_VALUES = 'CURRENT_TUNER_APPLY_SLIDER_VALUES',
  APPLY_SLIDER_VALUES_DONE = 'CURRENT_TUNER_APPLY_SLIDER_VALUES_DONE',
  UPDATE_CYCLE_SPEED = 'CURRENT_TUNER_UPDATE_CYCLE_SPEED',
  POPULATE_TUNING_INFO = 'CURRENT_TUNER_POPULATE_TUNING_INFO',
  UPDATE_TARGET_SETTINGS = 'CURRENT_TUNER_SET_TARGET_SETTINGS',
  UPDATE_ACTUAL_SETTINGS = 'CURRENT_TUNER_SET_ACTUAL_SETTINGS',
  UPDATE_APPLY_ERR = 'CURRENT_TUNER_UPDATE_APPLY_ERR',
  CLEAR_APPLY_RESULT = 'CURRENT_TUNER_CLEAR_APPLY_RESULT',
  SET_DRIVER = 'CURRENT_TUNER_SET_DRIVER',
  SET_DRIVER_DONE = 'CURRENT_TUNER_SET_DRIVER_DONE',
  UPDATE_DRIVER_ENABLED = 'CURRENT_TUNER_UPDATE_DRIVER_ENABLED',
  ON_FIRST_MOUNT = 'CURRENT_TUNER_ON_FIRST_MOUNT',
  UPDATE_SHOW_WARNING = 'CURRENT_TUNER_UPDATE_SHOW_WARNING',
  SET_3RD_PARTY_INFO = 'CURRENT_TUNER_SET_3RD_PARTY_INFO',
  CONFIRM_3RD_PARTY = 'CURRENT_TUNER_CONFIRM_3RD_PARTY',
}

export interface ActionsToPayloads {
  [ActionTypes.SELECT_AXIS_OR_DEVICE]: { axisOrDevice: EntityKey | null };
  [ActionTypes.UPDATE_LOADED]: { value: boolean };
  [ActionTypes.LOADING_ERR]: { error: LoadingError };
  [ActionTypes.UPDATE_SLIDERS]: { axisOrDevice: EntityKey; sliders: Partial<Sliders> };
  [ActionTypes.UPDATE_APPLIED_SLIDERS]: { axisOrDevice: EntityKey; appliedSliders: Sliders | null };
  [ActionTypes.UPDATE_SLIDER_VALUES]: { axisOrDevice: EntityKey; sliders: Sliders | null };
  [ActionTypes.APPLY_SLIDER_VALUES]: { axisOrDevice: EntityKey };
  [ActionTypes.APPLY_SLIDER_VALUES_DONE]: { axisOrDevice: EntityKey; error?: string };
  [ActionTypes.UPDATE_CYCLE_SPEED]: { axisOrDevice: EntityKey; log10Speed: number };
  [ActionTypes.POPULATE_TUNING_INFO]: { axisOrDevice: EntityKey; inputSettings: Setting[]; info: TuningInfo };
  [ActionTypes.UPDATE_TARGET_SETTINGS]: { axisOrDevice: EntityKey; targetIctrlSettings: Setting[] };
  [ActionTypes.UPDATE_ACTUAL_SETTINGS]: { axisOrDevice: EntityKey; actualIctrlSettings: Setting[] };
  [ActionTypes.UPDATE_APPLY_ERR]: { axisOrDevice: EntityKey; error: string };
  [ActionTypes.CLEAR_APPLY_RESULT]: { axisOrDevice: EntityKey };
  [ActionTypes.SET_DRIVER]: { enableDriver: boolean };
  [ActionTypes.SET_DRIVER_DONE]: { error?: string };
  [ActionTypes.UPDATE_DRIVER_ENABLED]: { axisOrDevice: EntityKey; driverEnabled: boolean };
  [ActionTypes.ON_FIRST_MOUNT]: void;
  [ActionTypes.UPDATE_SHOW_WARNING]: { showWarning: boolean; permanent: boolean };
  [ActionTypes.SET_3RD_PARTY_INFO]: { axisOrDevice: EntityKey; info: ThirdPartyInfo };
  [ActionTypes.CONFIRM_3RD_PARTY]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  selectAxisOrDevice: (axisOrDevice: EntityKey | null) => buildAction(ActionTypes.SELECT_AXIS_OR_DEVICE, { axisOrDevice }),
  updateLoaded: (value: boolean) =>
    buildAction(ActionTypes.UPDATE_LOADED, { value }),
  loadingErr: (error: LoadingError) =>
    buildAction(ActionTypes.LOADING_ERR, { error }),
  updateSliders: (axisOrDevice: EntityKey, sliders: Partial<Sliders>) =>
    buildAction(ActionTypes.UPDATE_SLIDERS, { axisOrDevice, sliders }),
  updateSliderValues: (axisOrDevice: EntityKey, sliders: Sliders | null) =>
    buildAction(ActionTypes.UPDATE_SLIDER_VALUES, { axisOrDevice, sliders }),
  applySliderValues: (axisOrDevice: EntityKey) =>
    buildAction(ActionTypes.APPLY_SLIDER_VALUES, { axisOrDevice }),
  applySliderValuesDone: (axisOrDevice: EntityKey, error?: string) =>
    buildAction(ActionTypes.APPLY_SLIDER_VALUES_DONE, { axisOrDevice, error }),
  populateTuningInfo: (axisOrDevice: EntityKey, inputSettings: Setting[], info: TuningInfo) =>
    buildAction(ActionTypes.POPULATE_TUNING_INFO, { axisOrDevice, inputSettings, info }),
  updateTargetSettings: (axisOrDevice: EntityKey, targetIctrlSettings: Setting[]) =>
    buildAction(ActionTypes.UPDATE_TARGET_SETTINGS, { axisOrDevice, targetIctrlSettings }),
  updateActualSettings: (axisOrDevice: EntityKey, actualIctrlSettings: Setting[]) =>
    buildAction(ActionTypes.UPDATE_ACTUAL_SETTINGS, { axisOrDevice, actualIctrlSettings }),
  updateAppliedSliders: (axisOrDevice: EntityKey, appliedSliders: Sliders | null) =>
    buildAction(ActionTypes.UPDATE_APPLIED_SLIDERS, { axisOrDevice, appliedSliders }),
  updateApplyErr: (axisOrDevice: EntityKey, error: string) =>
    buildAction(ActionTypes.UPDATE_APPLY_ERR, { axisOrDevice, error }),
  clearApplyResult: (axisOrDevice: EntityKey) =>
    buildAction(ActionTypes.CLEAR_APPLY_RESULT, { axisOrDevice }),
  setDriver: (enableDriver: boolean) =>
    buildAction(ActionTypes.SET_DRIVER, { enableDriver }),
  setDriverDone: (error?: string) => buildAction(ActionTypes.SET_DRIVER_DONE, { error }),
  updateDriverEnabled: (axisOrDevice: EntityKey, driverEnabled: boolean) =>
    buildAction(ActionTypes.UPDATE_DRIVER_ENABLED, { axisOrDevice, driverEnabled }),
  onFirstMount: () => buildAction(ActionTypes.ON_FIRST_MOUNT),
  updateShowWarning: (showWarning: boolean, permanent: boolean) =>
    buildAction(ActionTypes.UPDATE_SHOW_WARNING, { showWarning, permanent }),
  set3rdPartyInfo: (axisOrDevice: EntityKey, info: ThirdPartyInfo) =>
    buildAction(ActionTypes.SET_3RD_PARTY_INFO, { axisOrDevice, info }),
  confirm3rdParty: () => buildAction(ActionTypes.CONFIRM_3RD_PARTY),
};
