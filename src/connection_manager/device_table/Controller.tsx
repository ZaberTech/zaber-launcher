import React, { HTMLProps } from 'react';
import classNames from 'classnames';

import type { DeviceInfoWithAxes, IdentifiedDeviceInfo } from '../types';

import { ControllerHeader } from './ControllerHeader';

interface Props extends HTMLProps<HTMLDivElement> {
  device: DeviceInfoWithAxes;
  renderContent?: (device: IdentifiedDeviceInfo) => React.ReactNode;
}

export const Controller: React.FC<Props> = ({ children, device, className, renderContent, ...rest }) => (
  <div className={classNames('controller', className)} {...rest}>
    <ControllerHeader device={device} renderContent={renderContent}/>
    <span className="spacer"/>
    {children}
  </div>
);
