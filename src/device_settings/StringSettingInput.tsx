import { ConfirmCancel, Icons, Input } from '@zaber/react-library';
import React, { Component, createRef } from 'react';
import { match, P } from 'ts-pattern';

import { isEditingState, EditableInputMode } from '../units/EditableInputWithUnits';

interface Props {
  value: string;
  setting: string;
  editable: boolean;
  mode: EditableInputMode;
  onChange: (value: string) => void;
  onBeginEdit: () => void;
  onCancelEdit: () => void;
}

interface State {
  edited: string;
}

export class StringSettingInput extends Component<Props, State> {
  private inputRef: React.RefObject<HTMLInputElement>;

  constructor(props: Props) {
    super(props);
    this.state = { edited: props.value };
    this.inputRef = createRef<HTMLInputElement>();
  }

  onConfirm = () => {
    this.props.onChange(this.state.edited);
  };

  onInputKeyDown = (e: React.KeyboardEvent<HTMLInputElement>): void => {
    switch (e.key) {
      case 'Enter': this.onConfirm(); break;
      case 'Escape': this.props.onCancelEdit(); break;
    }
  };

  componentDidUpdate(prevProps: Props) {
    const { mode: state } = this.props;
    if (this.inputRef.current && state === 'editing' && prevProps.mode !== 'editing') {
      this.inputRef.current.focus();
      this.inputRef.current.select();
    }
  }

  static getDerivedStateFromProps(props: Props): State | null {
    if (!isEditingState(props.mode)) {
      return { edited: props.value };
    }
    return null;
  }

  render() {
    const {
      mode: state, editable, value, onBeginEdit, onCancelEdit, setting,
    } = this.props;

    const editing = isEditingState(state);
    return <div className="editable-input">
      <Input
        ref={this.inputRef} disabled={!editing} title={`Value of ${setting}`}
        value={editing ? this.state.edited : value}
        onValueChange={newValue => {
          if (state === 'editing') {
            this.setState({ edited: newValue });
          }
        }}
        onKeyDown={this.onInputKeyDown}
      />
      <div className="edit-controls">
        {match(state)
          .with(P.union('displaying', 'reading'), () => {
            if (!editable) {
              return null;
            } else {
              return <Icons.Edit
                className="edit" disabled={state === 'reading'} title={`Edit ${setting}`}
                onClick={onBeginEdit}
              />;
            }
          })
          .with(P.union('editing', 'writing'), () => <ConfirmCancel
            confirmTitle={`Confirm ${this.props.setting} edit`} cancelTitle={`Cancel editing ${this.props.setting}`}
            confirmDisabled={state === 'writing'}
            onConfirm={this.onConfirm}
            onCancel={onCancelEdit}
          />)
          .exhaustive()
        }
      </div>
    </div>;
  }
}
