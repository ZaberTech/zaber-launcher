import React from 'react';
import { ButtonRowConfirmCancel, Text } from '@zaber/react-library';

import { openExternalLink, restart } from '../desktop';
import { environment } from '../environment';

export const ErrorReport: React.FunctionComponent = () => (
  <div className="error-report">
    <Text t={Text.Type.H2} e={Text.Emphasis.Light}>We have encountered an error.</Text>
    <Text t={Text.Type.BodyLg} e={Text.Emphasis.Light}> The error has been reported, please restart the app and try again.</Text>
    <Text t={Text.Type.BodyLg} e={Text.Emphasis.Light}>If this issue persists please contact Zaber Support.</Text>
    <ButtonRowConfirmCancel
      justify="center"
      confirmText={`Restart ${environment.appName}`}
      cancelText="Contact Support"
      onConfirm={restart}
      onCancel={() => openExternalLink('https://www.zaber.com/contact')}
    />
  </div>
);
