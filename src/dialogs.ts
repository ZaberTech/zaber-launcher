import * as remote from '@electron/remote';

export class Dialogs {
  static showOpenDialog = (options: Electron.OpenDialogOptions) =>
    remote.dialog.showOpenDialog(remote.getCurrentWindow(), options);
  static showSaveDialog = (options: Electron.SaveDialogOptions) =>
    remote.dialog.showSaveDialog(remote.getCurrentWindow(), options);
  static showMessageBox = (options: Electron.MessageBoxOptions) =>
    remote.dialog.showMessageBox(remote.getCurrentWindow(), options);
}
