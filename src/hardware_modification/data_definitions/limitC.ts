import { CommandFailedException } from '@zaber/motion';

import type { SectionDataDefinition } from '../types';

import { actionEnabled, actionRequiresPreset, limitEnabled, widthEnabled } from './limit';

export const limitC: SectionDataDefinition<'limitC'> = {
  initializers: {
    type: { type: 'setting', backing: 'limit.c.type' },
    source: { type: 'setting', backing: 'limit.c.source' },
    edge: { type: 'setting', backing: 'limit.c.edge' },
    width: { type: 'setting', backing: 'limit.c.width' },
    action: { type: 'setting', backing: 'limit.c.action' },
    preset: { type: 'setting', backing: 'limit.c.preset' },
    posUpdate: { type: 'setting', backing: 'limit.c.posupdate' },
  },
  sectionPresent: async device => {
    try {
      await device.ascii.settings.get('limit.c.type');
    } catch (e) {
      if (e instanceof CommandFailedException && e.details.responseData === 'BADCOMMAND') {
        return false;
      }
      throw e;
    }
    return true;
  },
  fieldEnabled: {
    source: limitEnabled,
    edge: limitEnabled,
    width: widthEnabled,
    action: limitEnabled,
    posUpdate: actionEnabled,
    preset: actionRequiresPreset,
  },
};
