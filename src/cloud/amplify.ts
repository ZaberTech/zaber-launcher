import { Amplify } from 'aws-amplify';
import * as AWS from 'aws-sdk';

import { environment } from '../environment';

AWS.config.region = environment.awsRegion;

Amplify.configure({
  Auth: {
    identityPoolId: environment.cognitoIdentityPoolId,
    region: environment.awsRegion,
    userPoolId: environment.cognitoUserPoolId,
    userPoolWebClientId: environment.cognitoUserPoolClientId,
    mandatorySignIn: false,
  }
});
