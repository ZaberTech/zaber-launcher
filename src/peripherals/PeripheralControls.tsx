import React from 'react';
import { Button, ContextMenu, Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { DeviceSetupButton } from '../device_setup/DeviceSetupButton';
import { SaveLoadContextMenuItem } from '../save_load';
import { FactoryResetAxisMenuItem } from '../factory_reset';
import { SetLabelContextMenuItem } from '../connection_manager_ui';

import type { AxisStateComplete } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Message } from './Message';

export const PeripheralControls: React.FC<{ axis: AxisStateComplete }> = ({ axis }) => {
  const actions = useActions(actionsDefinition);
  const {
    commandError,
    commandInProgress,
    connectionLoading,
    derived: {
      canActivate, canReactivate, canDisable, hasIssue,
    }
  } = axis;
  const uiDisabled = commandInProgress || connectionLoading;
  return (<>
    {commandError && <Message title="Command Failed" type="error">{commandError}</Message>}

    {(canActivate || canReactivate) && <Button
      disabled={uiDisabled} color="grey"
      onClick={() => actions.activate(axis.key)}
    >
      {canActivate ? 'Activate' : 'Reactivate'}
    </Button>}

    <div className="spacer"/>

    {!hasIssue && <DeviceSetupButton serialNo={axis.serialNumber}/>}

    <ContextMenu>
      <ContextMenu.Item disabled={!canDisable || uiDisabled}
        onClick={() => actions.setPeripheralId(axis.key, 0)}
        icon={<Icons.Disable/>}>
        Disable Axis
      </ContextMenu.Item>
      <SaveLoadContextMenuItem productKey={axis.key}/>
      <FactoryResetAxisMenuItem axisKey={axis.key}/>
      <SetLabelContextMenuItem entityKey={axis.key} label={axis.label}/>
    </ContextMenu>
  </>);
};
