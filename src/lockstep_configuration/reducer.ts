import type { EntityKey } from '../keys';
import type { MeasurementOK } from '../units';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';

export type AxisLockstepInfo = {
  isPrimary: true;
} | {
  isPrimary: false;
  twistTolerance: number;
  offset: MeasurementOK | null;
};

export interface AxisDataLockstep {
  axisNumber: number;
  lockstepInfo: AxisLockstepInfo | null;
}
export type LockstepAxesData = Record<number, AxisDataLockstep[]>;

export type LockstepCreateAxisInfo = {
  supported: true;
  axisNumber: number;
  position: MeasurementOK;
  homed: boolean;
};

export type LockstepSelectAxisInfo = LockstepCreateAxisInfo | {
  supported: false;
  axisNumber: number;
};

export type CreationModalState = {
  step: 'preparing' | 'creating';
} | {
  step: 'choose';
  axesInfo: LockstepSelectAxisInfo[];
} | {
  step: 'confirm';
  primary: LockstepCreateAxisInfo;
  chosen: LockstepCreateAxisInfo[];
  axesInfo: LockstepSelectAxisInfo[];
} | {
  step: 'error';
  message: string;
} | {
  step: 'complete';
  groupNumber: number;
};
export interface State {
  selectedDeviceKey: EntityKey | null;
  lockstepData: LockstepAxesData | null;
  error: { blocking: boolean; message: string } | null;
  creationModalState: CreationModalState | null;
}

const initialState: State = {
  selectedDeviceKey: null,
  lockstepData: null,
  error: null,
  creationModalState: null,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const setSelectedDevice: Reducer<ActionTypes.SET_SELECTED_DEVICE> = (state: State, { deviceKey }) => ({
  ...state, selectedDeviceKey: deviceKey, error: null,
});

const setLockstepData: Reducer<ActionTypes.SET_LOCKSTEP_DATA> = (state: State, { lockstepData }): State => ({
  ...state, lockstepData, error: null
});

const setError: Reducer<ActionTypes.SET_ERROR> = (state: State, error): State => ({ ...state, error });

const beginCreation: Reducer<ActionTypes.BEGIN_CREATION> = state => ({ ...state, creationModalState: { step: 'preparing' } });

const receivedCreationInfo: Reducer<ActionTypes.RECEIVED_LOCKSTEP_CREATION_INFO> = (state, { axesInfo }) => ({
  ...state,
  creationModalState: { step: 'choose', axesInfo },
});

const choseCreationAxes: Reducer<ActionTypes.CHOSE_CREATION_AXES> = (state, chosenInfo) => ({
  ...state,
  creationModalState: { step: 'confirm', ...chosenInfo },
});

const createLockstepGroup: Reducer<ActionTypes.CREATE_LOCKSTEP_GROUP> = state => ({ ...state, creationModalState: { step: 'creating' } });

const createLockstepDone: Reducer<ActionTypes.CREATE_LOCKSTEP_GROUP_DONE> = (state, { groupNumber }) => ({
  ...state, creationModalState: { step: 'complete', groupNumber }, lockstepData: null,
});

const createLockstepError: Reducer<ActionTypes.CREATE_LOCKSTEP_GROUP_ERROR> = (state, { error }) => ({
  ...state, creationModalState: { step: 'error', message: error  }
});

const endLockstepCreation: Reducer<ActionTypes.END_LOCKSTEP_CREATION> = state => ({ ...state, creationModalState: null });

const breakLockstepGroup: Reducer<ActionTypes.BREAK_LOCKSTEP_GROUP> = state => ({ ...state, lockstepData: null });

const clearError = (state: State): State => ({
  ...state,
  error: null
});

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.SET_SELECTED_DEVICE]: setSelectedDevice,
  [ActionTypes.SET_LOCKSTEP_DATA]: setLockstepData,
  [ActionTypes.SET_ERROR]: setError,

  // Creation
  [ActionTypes.BEGIN_CREATION]: beginCreation,
  [ActionTypes.RECEIVED_LOCKSTEP_CREATION_INFO]: receivedCreationInfo,
  [ActionTypes.CHOSE_CREATION_AXES]: choseCreationAxes,
  [ActionTypes.CREATE_LOCKSTEP_GROUP]: createLockstepGroup,
  [ActionTypes.CREATE_LOCKSTEP_GROUP_DONE]: createLockstepDone,
  [ActionTypes.CREATE_LOCKSTEP_GROUP_ERROR]: createLockstepError,
  [ActionTypes.END_LOCKSTEP_CREATION]: endLockstepCreation,

  // Break
  [ActionTypes.BREAK_LOCKSTEP_GROUP]: breakLockstepGroup,

  // error clearing actions
  [ActionTypes.WRITE_TWIST_TOLERANCE]: clearError,
  [ActionTypes.UPDATE_OFFSET_ESTIMATES]: clearError,
}, initialState);
