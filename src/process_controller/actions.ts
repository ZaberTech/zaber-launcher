import type { ProcessControllerMode, ProcessControllerSource } from '@zaber/motion/product';

import type { EntityKey } from '../keys';
import type { MeasurementOK } from '../units';
import type { ValueUnitState } from '../units/EditableInputWithUnits';
import { actionBuilder } from '../utils';

import type { AutoTuneError, AutoTuneIdentify, AutoTuneUserStep, AutoTuneTest, AutoTuneTestRun, InitialState } from './auto_tune/types';
import type { SettingsMap } from './settings';
import type { ControllerInfo, ProcessInfo } from './types';

export enum ActionTypes {
  CHOOSE_ENTITY = 'PROCESS_CONTROLLER-CHOOSE_ENTITY',
  SET_CONTROLLER_INFO = 'PROCESS_CONTROLLER-SET_CONTROLLER_INFO',
  UPDATE_INPUTS_INFO = 'PROCESS_CONTROLLER-UPDATE_INPUTS_INFO',
  DETECT_NON_PC_ENTITY = 'PROCESS_CONTROLLER-DETECT_NON_PC_ENTITY',
  SET_APP_ERROR = 'PROCESS_CONTROLLER-SET_APP_ERROR',

  CHOOSE_PROCESS = 'PROCESS_CONTROLLER-CHOOSE_PROCESS',
  REFRESH_PROCESS = 'PROCESS_CONTROLLER-REFRESH_PROCESS',
  SET_PROCESS = 'PROCESS_CONTROLLER-SET_PROCESS',
  PROCESS_POWER = 'PROCESS_CONTROLLER-PROCESS_POWER',
  UPDATE_PROCESS_SETTING = 'PROCESS_CONTROLLER-UPDATE_PROCESS_SETTING',
  UPDATE_MODE = 'PROCESS_CONTROLLER-UPDATE_MODE',
  UPDATE_SOURCE = 'PROCESS_CONTROLLER-UPDATE_SOURCE',
  REENABLE_DRIVER = 'PROCESS_CONTROLLER-REENABLE_DRIVER',
  SET_PROCESS_ERROR = 'PROCESS_CONTROLLER-SET_PROCESS_ERROR',
  BRIDGE_PROCESSES = 'PROCESS_CONTROLLER-BRIDGE_PROCESSES',

  UNMOUNT = 'PROCESS_CONTROLLER-UNMOUNT',

  BEGIN_AUTO_TUNE = 'PROCESS_CONTROLLER-BEGIN_AUTO_TUNE',
  UPDATE_AUTO_TUNE = 'PROCESS_CONTROLLER-UPDATE_AUTO_TUNE',
  AUTO_TUNE_ERROR = 'PROCESS_CONTROLLER-AUTO_TUNE_ERROR',
  AUTO_TUNE_GO_TO_STEP = 'PROCESS_CONTROLLER-AUTO_TUNE_GO_TO_STEP',
  AUTO_TUNE_STEP_READY = 'PROCESS_CONTROLLER-AUTO_TUNE_STEP_READY',
  AUTO_TUNE_RUN_IDENTIFICATION = 'PROCESS_CONTROLLER-AUTO_TUNE_RUN_IDENTIFICATION',
  AUTO_TUNE_SET_IDENTIFY_PROGRESS = 'PROCESS_CONTROLLER-AUTO_TUNE_SET_IDENTIFY_PROGRESS',
  AUTO_TUNE_CANCEL_IDENTIFICATION = 'PROCESS_CONTROLLER-AUTO_TUNE_CANCEL_IDENTIFICATION',
  AUTO_TUNE_COMPLETE_IDENTIFICATION = 'PROCESS_CONTROLLER-AUTO_TUNE_COMPLETE_IDENTIFICATION',
  AUTO_TUNE_DOWNLOAD_IDENTIFICATION = 'PROCESS_CONTROLLER-AUTO_TUNE_DOWNLOAD_IDENTIFICATION',
  AUTO_TUNE_RUN_TEST = 'PROCESS_CONTROLLER-AUTO_TUNE_RUN_TEST',
  AUTO_TUNE_END_TEST = 'PROCESS_CONTROLLER-AUTO_TUNE_END_TEST',
  END_AUTO_TUNE = 'PROCESS_CONTROLLER-END_AUTO_TUNE',
}

export interface ActionsToPayloads {
  [ActionTypes.CHOOSE_ENTITY]: { entity: EntityKey | null; process: number | null };
  [ActionTypes.SET_CONTROLLER_INFO]: { info: ControllerInfo; time: number };
  [ActionTypes.UPDATE_INPUTS_INFO]: { info: Partial<ControllerInfo> };
  [ActionTypes.DETECT_NON_PC_ENTITY]: null;
  [ActionTypes.SET_APP_ERROR]: { title: string; details?: string };

  [ActionTypes.CHOOSE_PROCESS]: { process: number | null };
  [ActionTypes.REFRESH_PROCESS]: { process: number };
  [ActionTypes.SET_PROCESS]: { process: number; info: ProcessInfo };
  [ActionTypes.PROCESS_POWER]: { process: number; on: boolean };
  [ActionTypes.UPDATE_PROCESS_SETTING]: { process: number; setting: keyof SettingsMap; update: ValueUnitState; processOff?: boolean };
  [ActionTypes.UPDATE_MODE]: { process: number; mode: ProcessControllerMode };
  [ActionTypes.UPDATE_SOURCE]: { process: number; source: ProcessControllerSource };
  [ActionTypes.REENABLE_DRIVER]: { process: number };
  [ActionTypes.SET_PROCESS_ERROR]: { process: number; title: string; message: string } | { process: number };
  [ActionTypes.BRIDGE_PROCESSES]: { process: 1 | 3; bridge: boolean };

  [ActionTypes.UNMOUNT]: null;

  [ActionTypes.BEGIN_AUTO_TUNE]: { entity: EntityKey; process: number };
  [ActionTypes.UPDATE_AUTO_TUNE]: { time: number; measured: MeasurementOK; voltage: number; on: boolean };
  [ActionTypes.AUTO_TUNE_ERROR]: { error?: AutoTuneError };
  [ActionTypes.AUTO_TUNE_GO_TO_STEP]: { tune: AutoTuneUserStep };
  [ActionTypes.AUTO_TUNE_STEP_READY]: { tune: AutoTuneUserStep };
  [ActionTypes.AUTO_TUNE_RUN_IDENTIFICATION]: { tune: AutoTuneIdentify; hysteresis: MeasurementOK };
  [ActionTypes.AUTO_TUNE_SET_IDENTIFY_PROGRESS]: { progress: number; message: string; voltage: number; clearData: boolean };
  [ActionTypes.AUTO_TUNE_CANCEL_IDENTIFICATION]: null;
  [ActionTypes.AUTO_TUNE_COMPLETE_IDENTIFICATION]: { criticalGain: number; criticalFrequency: number };
  [ActionTypes.AUTO_TUNE_DOWNLOAD_IDENTIFICATION]: { data: AutoTuneIdentify['readings'] };
  [ActionTypes.AUTO_TUNE_RUN_TEST]: { tune: AutoTuneTest; run: AutoTuneTestRun };
  [ActionTypes.AUTO_TUNE_END_TEST]: null;
  [ActionTypes.END_AUTO_TUNE]: { entity: EntityKey; process: number; initial: InitialState; cancel: boolean };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actionDefinitions = {
  chooseEntity: (entity: EntityKey | null, process: number | null = null) => buildAction(ActionTypes.CHOOSE_ENTITY, { entity, process }),
  setControllerInfo: (info: ControllerInfo, time: number) => buildAction(ActionTypes.SET_CONTROLLER_INFO, { info, time }),
  updateInputsInfo: (info: Partial<ControllerInfo>) => buildAction(ActionTypes.UPDATE_INPUTS_INFO, { info }),
  detectNonPcEntity: () => buildAction(ActionTypes.DETECT_NON_PC_ENTITY),
  setAppError: (title: string, details?: string) => buildAction(ActionTypes.SET_APP_ERROR, { title, details }),

  chooseProcess: (process?: number) => buildAction(ActionTypes.CHOOSE_PROCESS, { process: process ?? null }),
  refreshProcessInfo: (process: number) => buildAction(ActionTypes.REFRESH_PROCESS, { process }),
  setProcess: (process: number, info: ProcessInfo) => buildAction(ActionTypes.SET_PROCESS, { process, info }),
  processPower: (process: number, on: boolean) => buildAction(ActionTypes.PROCESS_POWER, { process, on }),
  updateProcessSetting: (process: number, setting: keyof SettingsMap, update: ValueUnitState) => (
    buildAction(ActionTypes.UPDATE_PROCESS_SETTING, { process, setting, update })
  ),
  updateMode: (process: number, mode: ProcessControllerMode) => buildAction(ActionTypes.UPDATE_MODE, { process, mode }),
  updateSource: (process: number, source: ProcessControllerSource) => buildAction(ActionTypes.UPDATE_SOURCE, { process, source }),
  reenableDriver: (process: number) => buildAction(ActionTypes.REENABLE_DRIVER, { process }),
  setProcessError: (process: number, title: string, message: string) => (
    buildAction(ActionTypes.SET_PROCESS_ERROR, { process, title, message })
  ),
  clearProcessError: (process: number) => buildAction(ActionTypes.SET_PROCESS_ERROR, { process }),
  bridge: (process: 1 | 3, bridge: boolean) => buildAction(ActionTypes.BRIDGE_PROCESSES, { process, bridge }),

  unmount: () => buildAction(ActionTypes.UNMOUNT),

  beginAutoTune: (entity: EntityKey, process: number) => buildAction(ActionTypes.BEGIN_AUTO_TUNE, { entity, process }),
  updateAutoTune: (time: number, measured: MeasurementOK, voltage: number, on: boolean) => (
    buildAction(ActionTypes.UPDATE_AUTO_TUNE, { time, measured, voltage, on })
  ),
  autoTuneError: (error: AutoTuneError) => buildAction(ActionTypes.AUTO_TUNE_ERROR, { error }),
  clearAutotuneError: () => buildAction(ActionTypes.AUTO_TUNE_ERROR, { error: undefined }),
  autoTuneGoToStep: (tune: AutoTuneUserStep) => buildAction(ActionTypes.AUTO_TUNE_GO_TO_STEP, { tune }),
  autoTuneStepReady: (tune: AutoTuneUserStep) => buildAction(ActionTypes.AUTO_TUNE_STEP_READY, { tune }),
  autoTuneRunIdentification: (tune: AutoTuneIdentify, hysteresis: MeasurementOK) => (
    buildAction(ActionTypes.AUTO_TUNE_RUN_IDENTIFICATION, { tune, hysteresis })
  ),
  autoTuneSetIdentifyProgress: (progress: number, message: string, voltage: number, clearData = false) => (
    buildAction(ActionTypes.AUTO_TUNE_SET_IDENTIFY_PROGRESS, { progress, message, voltage, clearData })
  ),
  autoTuneCancelIdentification: () => buildAction(ActionTypes.AUTO_TUNE_CANCEL_IDENTIFICATION),
  autoTuneCompleteIdentification: (criticalGain: number, criticalFrequency: number) => (
    buildAction(ActionTypes.AUTO_TUNE_COMPLETE_IDENTIFICATION, { criticalGain, criticalFrequency })
  ),
  autoTuneDownloadIdentification: (data: AutoTuneIdentify['readings']) => (
    buildAction(ActionTypes.AUTO_TUNE_DOWNLOAD_IDENTIFICATION, { data })
  ),
  autoTuneRunTest: (tune: AutoTuneTest, run: AutoTuneTestRun) => buildAction(ActionTypes.AUTO_TUNE_RUN_TEST, { tune, run }),
  autoTuneEndTest: () => buildAction(ActionTypes.AUTO_TUNE_END_TEST),
  endAutoTune: (entity: EntityKey, process: number, initial: InitialState, cancel: boolean) => (
    buildAction(ActionTypes.END_AUTO_TUNE, { entity, process, initial, cancel })
  ),
};
