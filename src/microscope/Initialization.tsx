import React from 'react';
import { useSelector } from 'react-redux';
import { Button, NoticeBanner } from '@zaber/react-library';

import { isMatch, useActions } from '../utils';

import { selectOperations, selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations } from './types';
import { OperationError } from './components';

export const Initialization: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const { initialize } = useSelector(selectOperations);
  const monitor = useSelector(selectMonitorState);
  const inProgress = isMatch({ inProgress: true }, initialize);
  const needsInitialization = monitor.info?.needsInitialization ?? false;

  return <>
    {needsInitialization && <NoticeBanner
      type="warning"
      className="init-warning">
      The microscope is not initialized. The initialization homes some of the microscope components.
      <br/>
      <Button disabled={inProgress} onClick={() => actions.operationStart(Operations.initialize)}>
        {inProgress ? 'Initializing...' : 'Initialize'}
      </Button>
    </NoticeBanner>}
    <OperationError operation={Operations.initialize}/>
  </>;
};
