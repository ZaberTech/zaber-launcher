import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

import type { AxisConfig, EstopData, SetupStep } from './types';
import type { ErrorContext } from './exception';

export enum ActionTypes {
  SELECT_DEVICE_OR_AXIS = 'LC40_SETUP_SELECT_DEVICE_OR_AXIS',
  LOAD_DEVICE_OR_AXIS = 'LC40_SETUP_LOAD_DEVICE_OR_AXIS',
  UPDATE_CONFIG = 'LC40_SETUP_UPDATE_CONFIG',
  SET_LOADED = 'LC40_SETUP_SET_LOADED',
  SET_SETUP_MODAL_OPEN = 'LC40_SETUP_SET_SETUP_MODAL_OPEN',
  SET_SETUP_STEP = 'LC40_SETUP_SET_SETUP_STEP',
  SETUP_START = 'LC40_SETUP_SETUP_START',
  SETUP_STOP = 'LC40_SETUP_TEST_STOP',
  SET_RESULT_ERROR = 'LC40_SETUP_SET_RESULT_ERROR',
  CLEAR_RESULT_ERROR = 'LC40_SETUP_CLEAR_RESULT_ERROR',
  ESTOP_MOUNTED = 'LC40_ESTOP_MOUNTED',
  ESTOP_UNMOUNTED = 'LC40_ESTOP_UNMOUNTED',
  ESTOP_DATA = 'LC40_ESTOP_DATA',
  ENABLE_DRIVER = 'LC40_ENABLE_DRIVER',
}

export interface ActionsToPayloads {
  [ActionTypes.SELECT_DEVICE_OR_AXIS]: { deviceOrAxis: EntityKey | null };
  [ActionTypes.LOAD_DEVICE_OR_AXIS]: void;
  [ActionTypes.UPDATE_CONFIG]: { config: AxisConfig };
  [ActionTypes.SET_LOADED]: void;
  [ActionTypes.SET_SETUP_MODAL_OPEN]: { open: boolean };
  [ActionTypes.SET_SETUP_STEP]: { step: SetupStep };
  [ActionTypes.SETUP_START]: { test: boolean };
  [ActionTypes.SETUP_STOP]: void;
  [ActionTypes.SET_RESULT_ERROR]: { message?: string; errContext?: ErrorContext };
  [ActionTypes.CLEAR_RESULT_ERROR]: void;
  [ActionTypes.ESTOP_MOUNTED]: void;
  [ActionTypes.ESTOP_UNMOUNTED]: void;
  [ActionTypes.ESTOP_DATA]: { data: EstopData };
  [ActionTypes.ENABLE_DRIVER]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  selectDeviceOrAxis: (deviceOrAxis: EntityKey | null) => buildAction(ActionTypes.SELECT_DEVICE_OR_AXIS, { deviceOrAxis }),
  loadDeviceOrAxis: () => buildAction(ActionTypes.LOAD_DEVICE_OR_AXIS),
  updateConfig: (config: AxisConfig) => buildAction(ActionTypes.UPDATE_CONFIG, { config }),
  setLoaded: () => buildAction(ActionTypes.SET_LOADED),
  setSetupModalOpen: (open: boolean) => buildAction(ActionTypes.SET_SETUP_MODAL_OPEN, { open }),
  setSetupStep: (step: SetupStep) => buildAction(ActionTypes.SET_SETUP_STEP, { step }),
  setupStart: (test: boolean) => buildAction(ActionTypes.SETUP_START, { test }),
  setupStop: () => buildAction(ActionTypes.SETUP_STOP),
  setResultError: (message?: string, errContext?: ErrorContext) => buildAction(ActionTypes.SET_RESULT_ERROR, { message, errContext }),
  clearResultError: () => buildAction(ActionTypes.CLEAR_RESULT_ERROR),
  estopMounted: () => buildAction(ActionTypes.ESTOP_MOUNTED),
  estopUnmounted: () => buildAction(ActionTypes.ESTOP_UNMOUNTED),
  estopData: (data: EstopData) => buildAction(ActionTypes.ESTOP_DATA, { data }),
  enableDriver: () => buildAction(ActionTypes.ENABLE_DRIVER),
};
