import _ from 'lodash';
import React  from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, Text } from '@zaber/react-library';

import { Axis } from './Axis';
import { selectManualMode, selectJoystick, selectProductionMode } from './selectors';

export const Axes: React.FC = () => {
  const manualMode = useSelector(selectManualMode);
  const productionMode = useSelector(selectProductionMode);
  const joystick = useSelector(selectJoystick);

  if (!joystick?.axisCount) {
    return null;
  }

  return (
    <HeaderCard className="axes" edge="outline" header={<Text t={Text.Type.H4}>Axes</Text>}>
      {!manualMode && <>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 2 }}>Device</Text>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 3 }}>Axis</Text>
      </>}
      {manualMode && <>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 2 }}>Device Number</Text>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 3 }}>Axis Type</Text>
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 4 }}>Axis Number</Text>
      </>}
      {(!manualMode && !productionMode) ?
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 5 }}>Maximum Speed [%]</Text> :
        <Text t={Text.Type.Helper} style={{ gridColumnStart: 5 }}>Maximum Speed</Text>}
      <Text t={Text.Type.Helper} style={{ gridColumnStart: 6 }}>Speed Profile</Text>
      <Text t={Text.Type.Helper} style={{ gridColumnStart: 7 }}>Inverted</Text>
      {(manualMode || productionMode) && <Text t={Text.Type.Helper} style={{ gridColumnStart: 8 }}>Resolution</Text>}
      {_.range(joystick.axisCount).map(i => <Axis key={i} axisNumber={i + 1}/>)}
    </HeaderCard>
  );
};
