import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { ButtonRowConfirmCancel, Icons, Input, Modal, Text } from '@zaber/react-library';

import { useActions } from '../utils';

import { selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations } from './types';
import { OperationError } from './components';
import { useOperationState } from './hooks';


export const FilterChangerSettings: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const monitor = useSelector(selectMonitorState);

  const stableInfo = monitor.stableInfo!.filterChanger!;

  const [isOpen, setIsOpen] = useState(false);
  const [labels, setLabels] = useState(stableInfo.labels);

  const [isSaving] = useOperationState(Operations.filterChangerSettings);

  return (<>
    <Modal
      className="filter-changer-settings"
      headerIcon={<Icons.Settings/>}
      headerText="Filter Changer Settings"
      isOpen={isOpen}
      onRequestClose={() => setIsOpen(false)}
      buttons={<ButtonRowConfirmCancel
        confirmText={isSaving ? 'Saving...' : 'Save'}
        onConfirm={isSaving ? 'disabled' : () => actions.operationStart(Operations.filterChangerSettings, { labels })}
        onCancel={() => setIsOpen(false)}
      />}
    >
      <div className="table">
        <span/>
        <Text>Label</Text>
        {labels.map((label, index) => <React.Fragment key={index}>
          <Text>Position {index + 1}</Text>
          <Input key={index}
            placeholder={`Filter ${index + 1}`}
            value={label} onValueChange={label => {
              const newLabels = [...labels];
              newLabels[index] = label;
              setLabels(newLabels);
            }}/>
        </React.Fragment>)}
      </div>
      <OperationError operation={Operations.filterChangerSettings}/>
    </Modal>
    <Icons.Settings className="settings-icon" title="Settings" onClick={() => setIsOpen(true)}/>
  </>);
};
