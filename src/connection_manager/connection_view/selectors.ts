import { createSelector } from 'reselect';

import { selectConnectionView } from '../../store';
import { selectAllEntities } from '../selectors';

export const selectLastKey = createSelector(selectConnectionView, state => state.lastKey);
export const selectValidLastKey = createSelector(selectLastKey, selectAllEntities, (lastKey, entities) => {
  if (lastKey && entities[lastKey]) {
    return lastKey;
  }
  return null;
});
export const selectExpanded = createSelector(selectConnectionView, state => state.expanded);
