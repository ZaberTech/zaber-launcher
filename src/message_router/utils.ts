export function parseApiVersion(version: string): { major: number; minor: number } | null {
  if (typeof version !== 'string' || !version.match(/^\d+\.\d+$/)) {
    return null;
  }
  const [major, minor] = version.split('.').map(Number);
  return { major, minor };
}
