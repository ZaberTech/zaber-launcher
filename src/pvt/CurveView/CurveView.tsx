/* eslint-disable import-x/namespace */
import type { Properties } from 'csstype';
import _ from 'lodash';
import React, { Component, createRef } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as math from 'mathjs';
import { Icons, NoticeBanner, Text, HelpTooltip, UnorderedList } from '@zaber/react-library';
import { Angle } from '@zaber/motion';

import type { RootState } from '../../store';
import { getUnitLabel } from '../../units';
import { DEFAULT_POLL_INTERVAL } from '../../types';
import { IdentifiedDeviceState, selectIdentifiedDevices } from '../../connection_manager';
import { actions as actionDefinitions } from '../actions';
import type { Bug, Path, SeriesToAxisMap, Span } from '../types';
import { PvtSeriesHelper } from '../pvtSeriesHelper';
import { getControlPointPosition } from '../utils';
import {
  selectBugs,
  selectDeviceOptions,
  selectDevicePath,
  selectDevicePosition,
  selectDrawingAxes,
  selectSelectedPointIndex,
  selectUserPath
} from '../selectors';
import { closestPointOnCurve } from '../pvtCurveUtils';

import { XAXIS, YAXIS } from './constants';
import { ControlPoints } from './ControlPoints';
import { BugMarker, Grid } from './Grid';
import { PointPropertiesToolTip } from './PointPropertiesTooltip';
import { VelocityControl } from './VelocityControl';
import { DrawingAxisSelector } from './DrawingAxisSelector';
import { Filename } from '../Filename';
import { environment } from '../../environment';

const EPSILON = 0.000001;
const ZOOM_FACTOR = 1.2;
const DEVICE_CURSOR_RADIUS = 3;
const TOOLTIP_OFFSET = 10;
const MAX_EDITS_PER_SECOND = 15;
const DRAG_START_DISTANCE = 10;

const MARKERS = <defs>
  <marker id="dot" className="marker start"
    viewBox="0 0 12 12"
    refX="6" refY="6"
    z="-1"
    markerWidth="12" markerHeight="12">
    <circle cx="6" cy="6" r="4"/>
  </marker>
  <marker id="arrow" className="marker end"
    viewBox="0 0 14 14"
    refX="6" refY="6"
    z="-1"
    markerWidth="14" markerHeight="14"
    orient="auto-start-reverse">
    <path d="M 0 1 L 10 6 L 0 11"/>
  </marker>
</defs>;


const MouseHelp = <HelpTooltip title="Mouse Commands">
  <UnorderedList header={<Text t={Text.Type.H4}>Mouse commands for the 2D view:</Text>}>
    <Text><Text e={Text.Emphasis.Bold}>Left-click</Text>: select a control point</Text>
    <Text><Text e={Text.Emphasis.Bold}>Shift + mouse wheel</Text>: zoom in and out</Text>
    <Text><Text e={Text.Emphasis.Bold}>Left-drag</Text>: select an area to zoom to</Text>
    <Text><Text e={Text.Emphasis.Bold}>Shift + left-drag</Text>: pan the view</Text>
    <Text><Text e={Text.Emphasis.Bold}>Right-click in empty space</Text>: reset the zoom & pan to show the whole curve</Text>
    <Text><Text e={Text.Emphasis.Bold}>Right-click on the curve</Text>: insert a new control point</Text>
    <Text><Text e={Text.Emphasis.Bold}>Left-drag a control point</Text>: change its position</Text>
    <Text><Text e={Text.Emphasis.Bold}>Left-drag the orange handle</Text>: change the velocity at the selected control point</Text>
  </UnorderedList></HelpTooltip>;


function translationMatrix(dx: number, dy: number): math.Matrix {
  const m = math.identity(math.matrix([3])) as math.Matrix;
  m.subset(math.index([0, 1], 2), [dx, dy]);
  return m;
}


function scaleMatrix(scale: number, scale2?: number): math.Matrix {
  const m = math.identity(math.matrix([3])) as math.Matrix;
  m.set([0, 0], scale);
  m.set([1, 1], (scale2 ?? scale));
  return m;
}


interface CurveProps {
  actions: typeof actionDefinitions;
  userPath: Path | null;
  devicePath: Path | null;
  selectedIndex: number | null;
  axisMap: SeriesToAxisMap;
  drawingAxes: number[];
  devicePosition: number[];
  bugs: Bug[];
  selectedDevice: IdentifiedDeviceState | null;
}


interface CurveState {
  canvasWidth: number;
  canvasHeight: number;
  resized: boolean;
  userScale: number;
  pan: math.Matrix;
  prevPath: Path | null;
  prevAxes: number[];
  prevBugs: Bug[];
  rescan: boolean;
  zoomSelection: { x1: number; x2: number; y1: number; y2: number } | null; // Canvas coordinates
  hoveredPointIndex: number | null;
}


class CurveViewBase extends Component<CurveProps, CurveState> {
  private svgRoot = createRef<SVGSVGElement>();
  private polyLine: string = '';
  private curveBoundsX: Span = { min: -1, max: 1 };
  private curveBoundsY: Span = { min: -1, max: 1 };
  private autoScale = 1;
  private enableMouseZoomAndPan = false;
  private draggingPoint: number | null = null;        // Index of the point or point velocity handle being dragged
  private pointDragStart: math.Matrix | null = null;  // Drag start position, or null if drag distance threshold passed
  private draggingWhat: 'position' | 'velocity' | null = null; // Dragging position or velocity of the point?
  private panning = false;
  private lastEditTime = 0;
  private bugs: BugMarker[] = [];
  private mouseCoords: number[] = [];


  constructor(props: CurveProps) {
    super(props);

    this.state = {
      canvasWidth: 100,
      canvasHeight: 100,
      resized: true,
      userScale: 1,
      pan: math.zeros(2) as math.Matrix,
      prevPath: null,
      prevAxes: [],
      prevBugs: [],
      rescan: true,
      zoomSelection: null,
      hoveredPointIndex: null,
    };
  }


  componentDidMount(): void {
    window.addEventListener('keydown', this.onKeyDown);
    window.addEventListener('keyup', this.onKeyUp);
    const element = this.svgRoot.current;
    if (element) {
      element.addEventListener('wheel', this.onMouseWheel);
      element.addEventListener('mousedown', this.onMouseDown);
      element.addEventListener('mouseup', this.onMouseUp);
      element.addEventListener('mousemove', this.onMouseMove);
    }

    window.addEventListener('resize', this.handleResize);
    this.handleResize();

    this.props.actions.setPollDefault(DEFAULT_POLL_INTERVAL);
    if (this.props.selectedDevice) {
      this.props.actions.pollDevicePosition(DEFAULT_POLL_INTERVAL);
    }
  }


  componentWillUnmount(): void {
    window.removeEventListener('keydown', this.onKeyDown);
    window.removeEventListener('keyup', this.onKeyUp);
    const element = this.svgRoot.current;
    if (element) {
      element.removeEventListener('wheel', this.onMouseWheel);
      element.removeEventListener('mousedown', this.onMouseDown);
      element.removeEventListener('mouseup', this.onMouseUp);
      element.removeEventListener('mousemove', this.onMouseMove);
    }

    window.removeEventListener('resize', this.handleResize);
    this.props.actions.setPollDefault('stop');
    this.props.actions.pollDevicePosition('stop');
  }


  onKeyDown = (event: KeyboardEvent) => {
    if (event.key === 'Shift' && !this.enableMouseZoomAndPan) {
      event.preventDefault();
      this.enableMouseZoomAndPan = true;
    }
  };


  onKeyUp = (event: KeyboardEvent) => {
    if (event.key === 'Shift' && this.enableMouseZoomAndPan) {
      event.preventDefault();
      this.enableMouseZoomAndPan = false;
    }
  };


  onMouseWheel = (event: WheelEvent) => {
    // Windows puts wheel deltas in deltaX and Mac in deltaY, and they may also switch if horizontal scrolling is enabled.
    const delta = (Math.abs(event.deltaX) > Math.abs(event.deltaY)) ? event.deltaX : event.deltaY;
    if (this.enableMouseZoomAndPan && delta !== 0 && !this.state.zoomSelection) {
      const { canvasWidth, canvasHeight, userScale, pan } = this.state;
      const { autoScale } = this;
      const bounds = this.svgRoot.current!.getBoundingClientRect();
      const mouseInCenteredCanvasSpace =
        math.matrix([event.clientX - bounds.left - canvasWidth / 2, event.clientY - bounds.top - canvasHeight / 2, 1]);
      const oldProjection = this.getCurveProjection();
      const unProject = math.inv(oldProjection);
      const mouseInCurveSpace = math.multiply(unProject, mouseInCenteredCanvasSpace);

      let newUserScale = userScale;
      if (delta < 0) {
        newUserScale = userScale * ZOOM_FACTOR;
      } else if (delta > 0) {
        newUserScale = userScale / ZOOM_FACTOR;
      }

      const newProjection = this.getCurveProjection(autoScale * newUserScale);
      const mouseAtOldPanAndScale = math.multiply(oldProjection, mouseInCurveSpace);
      const mouseAtnewPanAndScale = math.multiply(newProjection, mouseInCurveSpace);
      const panDelta = math.subset(math.subtract(mouseAtOldPanAndScale, mouseAtnewPanAndScale), math.index([0, 1]));

      this.setState({ userScale: newUserScale, pan: math.add(pan, panDelta) });
    }
  };


  onMouseDown = (event: MouseEvent) => {
    if (this.enableMouseZoomAndPan && event.button === 0 && !this.panning) {
      this.panning = true;
      event.preventDefault();
    } else if (event.button === 2) {
      const { userPath, drawingAxes }  = this.props;
      if (userPath) {
        const mouse = this.mousePositionToCurveSpace(event.x, event.y);
        const target = Array<number | null>(userPath.series.length).fill(null);
        target[drawingAxes[XAXIS]] = mouse.get([XAXIS]);
        target[drawingAxes[YAXIS]] = mouse.get([YAXIS]);

        // Search with finer resolution when the zoom level is higher.
        const refinement = Math.pow(10, Math.max(0, Math.round(Math.log10(this.autoScale * this.state.userScale))));
        const point = closestPointOnCurve(userPath, target, refinement);
        if (point?.distanceToTarget) {
          const distanceInPixels = point.distanceToTarget * this.autoScale * this.state.userScale;
          if (distanceInPixels < 10) {
            this.props.actions.addPoint(point.segment, point.timeWithinSegment);
            this.props.actions.commitEdit();
            this.props.actions.setSelection(point.segment + 1);
            return;
          }
        }
      }

      this.setState({ userScale: 1, pan: math.zeros(2) as math.Matrix, zoomSelection: null });
    } else if (event.button === 0) {
      event.preventDefault();
      const rect = this.svgRoot.current!.getBoundingClientRect();
      const x = event.x - rect.left;
      const y = event.y - rect.top;
      this.setState({ zoomSelection: { x1: x, y1: y, x2: x, y2: y } });
    }
  };


  onMouseUp = (event: MouseEvent) => {
    if (event.button === 0) {
      if (this.draggingPoint != null) {
        this.props.actions.setFidelity('high');
        switch (this.draggingWhat) {
          case 'position':
            if (this.pointDragStart == null) {
              this.editSelectedPointPosition(event.clientX, event.clientY);
              this.props.actions.commitEdit();
            }
            break;
          case 'velocity':
            this.editSelectedPointVelocity(event.clientX, event.clientY);
            this.props.actions.commitEdit();
            break;
          default:
            break;
        }

        this.draggingPoint = null;
      } else if (this.panning) {
        this.panning = false;
      } else if (this.state.zoomSelection) {
        event.preventDefault();
        const { canvasWidth, canvasHeight, zoomSelection } = this.state;
        const bounds = this.svgRoot.current!.getBoundingClientRect();
        const rect = { ...zoomSelection, x2: event.x - bounds.left, y2: event.y - bounds.top };
        const corner1InCanvasSpace = math.matrix([rect.x1 - canvasWidth / 2, rect.y1 - canvasHeight / 2, 1]);
        const corner2InCanvasSpace = math.matrix([rect.x2 - canvasWidth / 2, rect.y2 - canvasHeight / 2, 1]);

        const unproj = math.inv(this.getCurveProjection());
        const corner1InCurveSpace = math.multiply(unproj, corner1InCanvasSpace);
        const corner2InCurveSpace = math.multiply(unproj, corner2InCanvasSpace);

        const { autoScale } = this;
        const userScale = Math.min(canvasWidth / Math.abs(corner1InCurveSpace.get([0]) - corner2InCurveSpace.get([0])),
          canvasHeight / Math.abs(corner1InCurveSpace.get([1]) - corner2InCurveSpace.get([1]))) / autoScale;
        if (Number.isNaN(userScale) || !Number.isFinite(userScale)) {
          this.setState({ zoomSelection: null });
          return;
        }

        const newProjection = this.getCurveProjection(autoScale * userScale);
        const panInCurveSpace = math.multiply(math.add(corner1InCurveSpace, corner2InCurveSpace), -0.5);
        const pan = math.multiply(newProjection, panInCurveSpace);

        this.setState({ zoomSelection: null, userScale, pan: math.subset(pan, math.index([0, 1])) });
      }
    }
  };


  onMouseMove = (event: MouseEvent) => {
    this.mouseCoords = [event.x, event.y];
    if (this.draggingPoint != null) {
      // Limit rate of drag updates in order to improve UI performance.
      if (Date.now() - this.lastEditTime >= 1000 / MAX_EDITS_PER_SECOND) {
        switch (this.draggingWhat) {
          case 'position':
            if (this.pointDragStart != null) {
              const mousePos = math.matrix([...this.mouseCoords, 1]);
              const distance = math.distance(this.pointDragStart, mousePos);
              // To prevent clicking on a point accidentally causing an edit, we don't really
              // start the drag edits until the mouse has moved a few pixels from the click location.
              if (Number(distance) > DRAG_START_DISTANCE) {
                this.pointDragStart = null;
                this.editSelectedPointPosition(event.clientX, event.clientY);
                // Edit commit action is only in onMouseUp because we don't want every single mouse
                // movement during the drag to be undoable.
              }
            } else {
              this.editSelectedPointPosition(event.clientX, event.clientY);
            }
            break;
          case 'velocity':
            this.editSelectedPointVelocity(event.clientX, event.clientY);
            break;
        }

        this.lastEditTime = Date.now();
      }
    } else if (this.enableMouseZoomAndPan && this.panning) {
      event.preventDefault();
      const { pan } = this.state;
      this.setState({
        pan: math.add(pan, math.matrix([event.movementX / window.devicePixelRatio, event.movementY / window.devicePixelRatio])),
      });
    } else if (this.state.zoomSelection) {
      event.preventDefault();
      const bounds = this.svgRoot.current!.getBoundingClientRect();
      this.setState({ zoomSelection: { ...this.state.zoomSelection, x2: event.x - bounds.left, y2: event.y - bounds.top } });
    }
  };


  handleResize = () => {
    if (this.svgRoot.current) {
      const rect = this.svgRoot.current.getBoundingClientRect();
      this.setState({
        canvasWidth: rect.width,
        canvasHeight: rect.height,
        resized: true,
      });
    }
  };


  private updateCurve() {
    const { devicePath, drawingAxes } = this.props;
    const { canvasWidth, canvasHeight } = this.state;

    if (!devicePath?.series.length || drawingAxes.length < 2) {
      this.polyLine = '';
      this.curveBoundsX = { min: -1, max: 1 };
      this.curveBoundsY = { min: -1, max: 1 };
      return;
    }

    let min = devicePath.series[drawingAxes[XAXIS]].positionRange?.min?.value ?? 0;
    let max = devicePath.series[drawingAxes[XAXIS]].positionRange?.max?.value ?? 100;
    if (max - min < EPSILON) {
      max += 10;
    }
    const xBounds = { min, max };

    min = devicePath.series[drawingAxes[YAXIS]].positionRange?.min?.value ?? 0;
    max = devicePath.series[drawingAxes[YAXIS]].positionRange?.max?.value ?? 100;
    if (max - min < EPSILON) {
      max += 10;
    }
    const yBounds = { min, max };

    const xs = devicePath.series[drawingAxes[XAXIS]].points.map(p => p.pos);
    const ys = devicePath.series[drawingAxes[YAXIS]].points.map(p => p.pos);
    const fmt = (n: number) => n.toLocaleString(undefined, { maximumSignificantDigits: 6, useGrouping: false });
    this.polyLine = _.range(0, xs.length).map(i => (`${fmt(xs[i])},${fmt(ys[i])}`)).join(' ');
    this.curveBoundsX = xBounds;
    this.curveBoundsY = yBounds;

    this.autoScale = 0.75 * Math.min(canvasWidth / (xBounds.max - xBounds.min), canvasHeight / (yBounds.max - yBounds.min));
  }


  private updateBugs() {
    const { bugs, drawingAxes, devicePath, userPath } = this.props;

    this.bugs = [];
    if (!devicePath || !userPath?.series.length || drawingAxes.length < 2) {
      return;
    }

    const xSeries = PvtSeriesHelper.FromUnits(userPath!.series[drawingAxes[XAXIS]], userPath!.positionUnit, userPath!.velocityUnit);
    const ySeries = PvtSeriesHelper.FromUnits(userPath!.series[drawingAxes[YAXIS]], userPath!.positionUnit, userPath!.velocityUnit);

    bugs.filter(bug => bug.dimension === 'Length' || bug.dimension === 'Angle').forEach(bug => {
      if (bug.seriesIndex === drawingAxes[XAXIS]) {
        this.bugs.push({
          x: bug.value,
          y: ySeries.getValueAtTime(bug.time).position,
          text: bug.message,
        });
      } else if (bug.seriesIndex === drawingAxes[YAXIS]) {
        this.bugs.push({
          x: xSeries.getValueAtTime(bug.time).position,
          y: bug.value,
          text: bug.message,
        });
      }
    });
  }


  private getCurveProjection(scale?: number): math.Matrix {
    const { userScale, pan } = this.state;
    const { curveBoundsX, curveBoundsY, autoScale } = this;
    if (scale === undefined) {
      scale = autoScale * userScale;
    }

    const center = translationMatrix(-(curveBoundsX.max + curveBoundsX.min) / 2, -(curveBoundsY.max + curveBoundsY.min) / 2);
    // Flip the Y axis, because in the SVG coordinate space +Y is downward but in charts it's upward.
    const scaleM = scaleMatrix(scale, -scale);
    const panM = translationMatrix(pan.get([0]) as number, pan.get([1]) as number);

    // Order of transformations from curve space to svg chart space:
    // Center curve on origin
    // Scale by default zoom factor and user zoom factor
    // Translate by user pan vector
    return math.multiply(panM, math.multiply(scaleM, center));
  }


  private mousePositionToCurveSpace(x: number, y: number): math.Matrix {
    const { canvasWidth, canvasHeight } = this.state;
    const bounds = this.svgRoot.current!.getBoundingClientRect();
    const mouseInCenteredCanvasSpace =
      math.matrix([x - bounds.left - canvasWidth / 2, y - bounds.top - canvasHeight / 2, 1]);
    const unProject = math.inv(this.getCurveProjection());
    return math.multiply(unProject, mouseInCenteredCanvasSpace);
  }


  private editSelectedPointPosition(x: number, y: number) {
    const location = this.mousePositionToCurveSpace(x, y);
    const { actions, userPath, drawingAxes } = this.props;
    const posEdit = {
      [userPath!.series[drawingAxes[XAXIS]].name]: location.get([XAXIS]),
      [userPath!.series[drawingAxes[YAXIS]].name]: location.get([YAXIS]),
    };

    actions.editPosition(this.draggingPoint!, posEdit);
  }


  private editSelectedPointVelocity(x: number, y: number) {
    const pointPos = getControlPointPosition(this.props.userPath!, this.draggingPoint!);
    if (pointPos != null) {
      const { actions, userPath, drawingAxes } = this.props;
      const location = this.mousePositionToCurveSpace(x, y);
      const velEdit = {
        [userPath!.series[drawingAxes[XAXIS]].name]: location.get([XAXIS]) - pointPos[drawingAxes[XAXIS]],
        [userPath!.series[drawingAxes[YAXIS]].name]: location.get([YAXIS]) - pointPos[drawingAxes[YAXIS]],
      };

      actions.editVelocity(this.draggingPoint!, velEdit);
    }
  }


  static getDerivedStateFromProps(newProps: CurveProps, oldState: CurveState) {
    if (oldState.resized || newProps.devicePath !== oldState.prevPath
      || !_.isEqual(newProps.drawingAxes, oldState.prevAxes) || !_.isEqual(newProps.bugs, oldState.prevBugs)) {
      return {
        prevPath: newProps.devicePath,
        prevAxes: newProps.drawingAxes,
        prevBugs: newProps.bugs,
        resized: false,
        rescan: true,
      };
    }

    return {
      rescan: false,
    };
  }


  render() {
    const { actions, userPath, devicePath, selectedIndex, axisMap, drawingAxes, devicePosition } = this.props;
    const { canvasWidth, canvasHeight, userScale, rescan, hoveredPointIndex, zoomSelection } = this.state;

    if (rescan) {
      this.updateCurve();
      this.updateBugs();
    }

    const { polyLine, autoScale } = this;
    const scale = autoScale * userScale;
    const axisOptions = (userPath?.series ?? []).map((series, i) => ({ label: series.name, value: i }));
    const canvasCenter = { x: canvasWidth / 2, y: canvasHeight / 2 };

    const isAngular = (userPath && (Object.values(Angle).includes(userPath.positionUnit as Angle)));

    let toolTip;
    if (hoveredPointIndex !== null && userPath && !environment.isTest) {
      const style = this.getPopupPositionAsStyle(hoveredPointIndex);
      toolTip = <PointPropertiesToolTip
        userPath={userPath}
        selectedPointIndex={hoveredPointIndex}
        drawingAxes={drawingAxes}
        style={style}/>;
    }

    const transform = this.getCurveProjection();
    return <div className="curve-view">
      <div className="controls">
        <Filename/>
        <DrawingAxisSelector axisOptions={axisOptions} drawingAxes={drawingAxes}/>
        <div/>
        {userPath && userPath.series.length >= 2 && <div className="buttons">
          {MouseHelp}
          <Icons.Scale title="Reset zoom" onClick={() => this.setState({ userScale: 1, pan: math.zeros(2) as math.Matrix })}/>
          <Icons.Download onClick={this.onSaveImage} title="Save Image"/>
        </div>}
      </div>
      <div className="usage">
        {isAngular &&
          <NoticeBanner>
            A selected series has angular units; this visualization only supports linear motion.
          </NoticeBanner>}
        {userPath && userPath.series.length < 2 && !isAngular &&
          <Text>The currently loaded file has only one data series; this visualization requires at least two.</Text>}
        {userPath && userPath.series.length >= 2 && this.bugs.length > 0 && !isAngular &&
          <NoticeBanner>
            The path exceeds device travel limits at the 'x' markers; see the Time Series view for more details.
          </NoticeBanner>}
      </div>

      {toolTip}

      <svg className="drawing" ref={this.svgRoot} preserveAspectRatio="xMidYMid meet">
        {MARKERS}

        {zoomSelection && <rect className="zoom-selection"
          x={Math.min(zoomSelection.x1, zoomSelection.x2)}
          y={Math.min(zoomSelection.y1, zoomSelection.y2)}
          width={Math.abs(zoomSelection.x2 - zoomSelection.x1)}
          height={Math.abs(zoomSelection.y2 - zoomSelection.y1)}/>}

        { /* This transform defines the chart space - the origin is at the center of the screen. +Y is downward. */ }
        <g transform={`translate(${canvasCenter.x} ${canvasCenter.y})`}>
          {devicePath && drawingAxes.length >= 2 && !isAngular && <Grid
            viewPort={{
              x: { min: -canvasWidth / 2, max: canvasWidth / 2 },
              y: { min: -canvasHeight / 2, max: canvasHeight / 2 },
            }}
            deviceLimits={{
              x: devicePath.series[drawingAxes[XAXIS]].positionLimits,
              y: devicePath.series[drawingAxes[YAXIS]].positionLimits,
            }}
            projection={transform}
            axisLabels={{
              x: `${devicePath.series[drawingAxes[XAXIS]].name} (${getUnitLabel(devicePath.positionUnit)})`,
              y: `${devicePath.series[drawingAxes[YAXIS]].name} (${getUnitLabel(devicePath.positionUnit)})`,
            }}
            bugs={this.bugs}
            drawingAxes={drawingAxes.slice(0, 2)}/>}

          { /* This transform is the curve coordinate space zoomed and panned relative to the origin. */}
          { /* eslint-disable-next-line max-len */ }
          <g transform={`matrix(${transform.get([0, 0])} ${transform.get([1, 0])} ${transform.get([0, 1])}, ${transform.get([1, 1])} ${transform.get([0, 2])} ${transform.get([1, 2])})`}>
            {devicePath != null && !isAngular && devicePath.series.length > 1 && <>
              { /* Uncomment this section to draw the discretized points along the path. */}
              {/* {devicePath.series[0].points.map((point, i) =>
                <circle key={`_${i}`}
                  fill="grey"
                  cx={devicePath.series[drawingAxes[XAXIS]].points[i].pos}
                  cy={devicePath.series[drawingAxes[YAXIS]].points[i].pos}
                  r={3 / scale}
                  stroke="0"/>
              )} */}
            </>}
            {polyLine.length > 0 && !isAngular &&
              <polyline className="curve"
                role="curve-display"
                points={polyLine}
                vectorEffect="non-scaling-stroke"
                markerStart="url(#dot)"
                markerEnd="url(#arrow)"/>}
            {userPath && userPath.series.length >= 2 && !isAngular && ((devicePosition?.length ?? 0) >= Math.max(...drawingAxes)) &&
              <rect
                className="device-position"
                role="device-position-marker"
                vectorEffect="non-scaling-stroke"
                width={(2 * DEVICE_CURSOR_RADIUS + 1) / scale}
                height={(2 * DEVICE_CURSOR_RADIUS + 1) / scale}
                x={devicePosition[axisMap[userPath.series[drawingAxes[XAXIS]].name]] - DEVICE_CURSOR_RADIUS / scale}
                y={devicePosition[axisMap[userPath.series[drawingAxes[YAXIS]].name]] - DEVICE_CURSOR_RADIUS / scale}/>}
            {userPath && userPath.series.length >= 2 && !isAngular &&
              <ControlPoints
                userPath={userPath}
                drawingAxes={drawingAxes}
                selectedPointIndex={selectedIndex}
                scale={scale}
                onSelected={i => {
                  actions.setSeriesDetailsVisible(userPath.series[drawingAxes[XAXIS]].name, true);
                  actions.setSeriesDetailsVisible(userPath.series[drawingAxes[YAXIS]].name, true);
                  actions.setSelection(i);
                  this.setState({ zoomSelection: null, hoveredPointIndex: null });
                  this.draggingWhat = 'position';
                  this.draggingPoint = i;
                  this.pointDragStart = math.matrix([...this.mouseCoords, 1]);
                  actions.setFidelity('low');
                }}
                onHover={i => this.setState({ hoveredPointIndex: this.draggingPoint == null ? i : null })}/>}
            {userPath && userPath.series.length >= 2 && !isAngular &&
              selectedIndex != null && selectedIndex > 0 && selectedIndex < userPath.series[0].points.length - 1 &&
              <VelocityControl
                userPath={userPath}
                drawingAxes={drawingAxes}
                selectedPointIndex={selectedIndex}
                scale={scale}
                onSelected={i => {
                  actions.setFidelity('low');
                  this.setState({ zoomSelection: null, hoveredPointIndex: null });
                  this.draggingWhat = 'velocity';
                  this.draggingPoint = i;
                }}/>}
          </g>
        </g>
      </svg>
    </div>;
  }


  private getPopupPositionAsStyle(position: number | { x: number; y: number }): Properties<string | number, string> | undefined {
    const { userPath, drawingAxes } = this.props;
    if (!userPath?.series.length || drawingAxes.length < 2) {
      return undefined;
    }

    let pointCoords: math.Matrix;
    if (typeof position === 'number') {
      pointCoords = math.matrix([...drawingAxes.slice(0, 2).map(axis => userPath.series[axis].points[position].pos), 1]);
    } else {
      pointCoords = math.matrix([position.x, position.y, 1]);
    }

    const projection = this.getCurveProjection();
    const canvasCoords = math.multiply(projection, pointCoords);

    const style: Properties<string | number, string> = {};
    if (canvasCoords.get([XAXIS]) >= 0) {
      style.right = window.innerWidth - this.mouseCoords[XAXIS] + TOOLTIP_OFFSET;
    } else {
      style.left = this.mouseCoords[XAXIS] + TOOLTIP_OFFSET;
    }

    if (canvasCoords.get([YAXIS]) >= 0) {
      style.bottom = window.innerHeight - this.mouseCoords[YAXIS] + TOOLTIP_OFFSET;
    } else {
      style.top = this.mouseCoords[YAXIS] + TOOLTIP_OFFSET;
    }

    return style;
  }


  private onSaveImage = () => {
    if (this.svgRoot.current) {
      const rect = this.svgRoot.current.getBoundingClientRect();
      this.props.actions.exportImage(rect);
    }
  };
}


export const CurveView = connect(
  (state: RootState): Omit<CurveProps, 'actions'> => {
    const deviceOptions = selectDeviceOptions(state);
    const identifiedDevices = selectIdentifiedDevices(state);
    const selectedDevice = deviceOptions.deviceKey ? identifiedDevices[deviceOptions.deviceKey] : null;
    return ({
      userPath: selectUserPath(state),
      selectedIndex: selectSelectedPointIndex(state),
      devicePath: selectDevicePath(state),
      axisMap: deviceOptions.axes,
      drawingAxes: selectDrawingAxes(state),
      devicePosition: selectDevicePosition(state),
      bugs: selectBugs(state),
      selectedDevice,
    });
  },
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionDefinitions, dispatch),
  }),
  null,
  { forwardRef: true }
)(CurveViewBase);
