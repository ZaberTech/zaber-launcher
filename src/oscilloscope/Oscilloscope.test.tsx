const TEST_CSV = 'test.csv';
const TEST_PNG = 'test.png';

const mockSaveDialog = jest.fn(() => ({
  cancelled: false,
  filePath: TEST_CSV,
}));

jest.mock('../dialogs', () => ({
  Dialogs: {
    showSaveDialog: mockSaveDialog,
  },
}));

const writeFile = jest.fn();
jest.mock('fs', () => ({
  ...jest.requireActual('fs'),
  promises: {
    writeFile,
  },
}));

import React from 'react';
import { Dialogs } from '../dialogs';
import _ from 'lodash';
import { fireEvent, render, RenderResult, within } from '@testing-library/react';
import { CommandFailedException, Length, RequestTimeoutException } from '@zaber/motion';
import type { Container } from 'inversify';

import { IntersectionObserverMockBase } from '../test/mocks/intersection_observer';
import 'jest-canvas-mock';
import {
  chartsMock,
  clearMonitors,
  DeviceMockBaseForScope,
  DeviceMockIntegrated,
  EChartsReactMock,
  getMonitor,
  makeRejectReason,
  MessageRoutersServiceMockIntegrated,
  MessageRoutersServiceMockMultiAxis,
  SelectMock,
  setAxisCallback,
  setDeviceCallback
} from './testing/mocks';

jest.mock('react-select', () => SelectMock);

jest.mock('echarts-for-react', () => EChartsReactMock);


class IntersectionObserverMock extends IntersectionObserverMockBase {
}

(window as unknown as { IntersectionObserver: typeof IntersectionObserverMock }).IntersectionObserver = IntersectionObserverMock;

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { connectionManagerActions, selectConnections, selectDevices } from '../connection_manager';
import { mockSingleDevice, mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import { createContainer, destroyContainer } from '../container';
import { MessageRoutersService } from '../message_router';
import type { AppStore } from '../store/build';
import { defer, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import { TimeMeasuring } from '../time';

import { actions } from './actions';
import {
  createChannelForIo,
  extractMathChannels,
  isSettingChannelId,
  makeIoChannelId,
  makeMathChannelId,
  makeSettingChannelId,
  splitChannelId
} from './channels';
import { extractConnectionKey, makeAxisKey, makeDeviceKey } from '../keys';
import { Oscilloscope } from './Oscilloscope';
import type { Recording } from './reducers';
import { MAX_POLLED_BUFFER_SIZE } from './sagas';
import { selectActiveRecording, selectAllChannels, selectSavedRecordings } from './selectors';
import {
  addDeviceScopeSettingsInfo,
  selectRecordingControls,
  selectRecordingTab,
  selectRecordMode,
  setDifferencingWindow,
  setNumSamples,
  setSampleRate
} from './testing/utils';
import {
  CAPTURE_SETTINGS_TAB_LABEL,
  DELETE_ALL_BUTTON_LABEL,
  MathChannelNames,
  Mode,
  NEW_RECORDING_TAB_LABEL,
  REPEAT_RECORDING_BUTTON_LABEL,
  REPLACE_RECORDING_BUTTON_LABEL,
  SignalType,
  START_RECORDING_BUTTON_LABEL,
  STOP_RECORDING_BUTTON_LABEL,
  WAITING_FOR_EXTERNAL_PRINT_MESSAGE,
  WAITING_FOR_TRIGGER_MESSAGE
} from './types';
import { OscilloscopeDataSource } from '@zaber/motion/ascii';
import { ioPortTypeToZml } from './utils';
import { getUnitSelectByTitle, getValueInputByTitle } from '../units/test_utils';
import { mockReloadDevices } from '../test/mocks/reload_devices';


const mockFWBufferedData = function(device: DeviceMockBaseForScope, channelIds: string[]) {
  device._data = channelIds.map(id => {
    const { name, axis } = splitChannelId(id);
    if (isSettingChannelId(id)) {
      return {
        getData: () => Array.from(Array(10).keys()),
        getSampleTime: (i: number) => i,
        setting: name,
        axisNumber: axis,
        dataSource: OscilloscopeDataSource.SETTING,
      };
    } else {
      return {
        getData: () => Array.from(Array(10).keys()).map(x => x & 1),
        getSampleTime: (i: number) => i,
        ioType: ioPortTypeToZml(name),
        ioChannel: axis,
        dataSource: OscilloscopeDataSource.IO,
      };
    }
  });
};


const getMaxChannelLength = function(recording: Recording | null | undefined): number {
  return _.max((recording?.channels ?? []).map(ch => ch.samples.length)) ?? 0;
};


// Expects the buffered mode controls to already be visible.
const configureSignal = function(wrapper: RenderResult, multiAxis = false) {
  fireEvent.change(wrapper.getByTestId('signal-type-selector'), { target: { value: SignalType.MOVE_RELATIVE } });
  if (multiAxis) {
    fireEvent.change(wrapper.getByTestId('signal-axis-selector'), { target: { value: '1' } });
  }

  fireEvent.change(getValueInputByTitle(wrapper, 'Position Control'), { target: { value: '2' } });
  fireEvent.change(getUnitSelectByTitle(wrapper, 'Position Control'), { target: { value: 'Length:centimetres' } });
  fireEvent.change(wrapper.getByTestId('enable-return-to-start'), { target: { checked: true } });
};


const getErrorDialogText = function(wrapper: RenderResult): string {
  const dialog = wrapper.getByTestId('oscilloscope-error');
  const content = dialog.getElementsByClassName('modal-content')[0];
  const texts = content.getElementsByClassName('text-span');
  const result: string[] = [];
  for (const textElement of texts) {
    const span = textElement.textContent;
    if (span) {
      result.push(span);
    }
  }

  return result.join('\n');
};


let container: Container;
const TestOscilloscope = wrapWithNewStore(wrapWithRouter(Oscilloscope));
let wrapper: RenderResult;
let selectedDevice: DeviceMockBaseForScope;


/* =================================== Start of tests ================================== */

beforeEach(() => {
  clearMonitors();
});


describe('With an integrated device', () => {
  let reloadDevicesMock: ReturnType<typeof mockReloadDevices>;
  let changeDeviceId = false;

  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockIntegrated);

    wrapper = render(<TestOscilloscope/>);

    changeDeviceId = false;
    reloadDevicesMock = mockReloadDevices(() => TestOscilloscope.testStore, devices => {
      if (changeDeviceId) {
        devices[0].identity!.deviceId++;
      }
    });
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;

    setAxisCallback(null);
    setDeviceCallback(null);
  });


  describe('Oscilloscope', () => {
    beforeEach(() => {
      const store = TestOscilloscope.testStore;
      mockSingleDevice(store, device => {
        device.identity!.firmwareVersion.minor = 29;
        return device;
      });

      setDeviceCallback(device => {
        selectedDevice = device;
        setDeviceCallback(null);
      });
    });


    it('displays a welcome screen before device selection', () => {
      wrapper.getByText(/Welcome to/);
      expect(wrapper.queryAllByText(NEW_RECORDING_TAB_LABEL)).toHaveLength(0);
      expect(wrapper.queryAllByText(START_RECORDING_BUTTON_LABEL)).toHaveLength(0);
    });


    it('populates and enables controls on device selection', async () => {
      const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);

      wrapper.getByText(NEW_RECORDING_TAB_LABEL);
      await waitUntilPass(() => expect(wrapper.getByText(START_RECORDING_BUTTON_LABEL)).toBeEnabled());
      expect(wrapper.queryAllByText('Select an axis')).toEqual([]);
      await waitUntilPass(() => wrapper.getByTestId('Data Source add'));
      wrapper.getByTestId('Computed Math add');
      within(wrapper.getByTestId('channel-card-Data Source')).getByText('pos');
      within(wrapper.getByTestId('channel-card-Data Source')).getByText('encoder.pos');
    });


    it('handles error during device selection', async () => {
      setDeviceCallback(() => {
        throw new Error('Foo!');
      });

      const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/Foo!/));
    });


    it('handles selection of an unidentified device', async () => {
      const conn = _.sample(selectConnections(TestOscilloscope.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(makeAxisKey(makeDeviceKey(conn.key, 7), 1));
      await waitUntilPass(() => wrapper.getByTestId('Data Source add'));
    });


    it('displays the capture settings tab when clicked', async () => {
      const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => wrapper.getByTestId('Data Source add'));

      fireEvent.click(wrapper.getByText(CAPTURE_SETTINGS_TAB_LABEL));
      wrapper.getByText('Capture Mode');
      wrapper.getByText('Target Sampling Rate (Hz)');
    });


    describe('recording', () => {
      let store: AppStore;
      const nonEmptyChannelIds = ['pos', 'encoder.pos'].map(id => makeSettingChannelId(id, 0))
        .concat([MathChannelNames.TRAJECTORY_VEL, MathChannelNames.FOLLOW_ERR].map(id => makeMathChannelId(id, 0)))
        .concat([makeIoChannelId('di', 2)]);

      beforeEach(async () => {
        store = TestOscilloscope.testStore;
        const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
        connectionViewMockInstance.props.onSelect(device.key);
        await waitUntilPass(() => expect(selectAllChannels(store.getState()).length).toBeGreaterThan(0));

        // Add a derivative and a difference math channel
        const math = _.cloneDeep(extractMathChannels(selectAllChannels(store.getState())));
        math.find(ch => ch.name === MathChannelNames.TRAJECTORY_VEL)!.showInUi = true;
        math.find(ch => ch.name === MathChannelNames.FOLLOW_ERR)!.showInUi = true;
        store.dispatch(actions.closeAddMathDialog(math));

        // Add a digital input channel.
        store.dispatch(actions.closeAddIoDialog([createChannelForIo('di', 2)]));
      });


      describe('in polled mode', () => {
        beforeEach(async () => {
          await selectRecordingTab(wrapper, NEW_RECORDING_TAB_LABEL);
          await selectRecordingControls(wrapper);
          selectRecordMode(wrapper, Mode.POLLED);

          let now = 0;
          container.get(TimeMeasuring).now = jest.fn(() => { now += 100; return now });
        });


        afterEach(() => {
          store.dispatch(actions.stopRecording());
        });


        it('hides and shows the control panel on a recording when the expander is clicked', async () => {
          store.dispatch(actions.startRecording());
          await waitUntilPass(() =>
            expect(selectActiveRecording(store.getState())!.channels[0].samples.length).toBeGreaterThanOrEqual(10), 1000);
          store.dispatch(actions.stopRecording());
          await waitUntilPass(() => expect(wrapper.getByTestId('recording-tab Recording 1')), 1000);

          await selectRecordingTab(wrapper, 'Recording 1');
          expect(wrapper.queryAllByText(CAPTURE_SETTINGS_TAB_LABEL)).toHaveLength(1);
          fireEvent.click(wrapper.getByTestId('control-panel-expander'));
          expect(wrapper.queryAllByText(CAPTURE_SETTINGS_TAB_LABEL)).toHaveLength(0);
          fireEvent.click(wrapper.getByTestId('control-panel-expander'));
          expect(wrapper.queryAllByText(CAPTURE_SETTINGS_TAB_LABEL)).toHaveLength(1);
        });


        it('always shows the control panel on the new recording tab', async () => {
          store.dispatch(actions.startRecording());
          await waitUntilPass(() =>
            expect(selectActiveRecording(store.getState())!.channels[0].samples.length).toBeGreaterThanOrEqual(10), 1000);
          store.dispatch(actions.stopRecording());
          await waitUntilPass(() => expect(wrapper.getByTestId('recording-tab Recording 1')), 1000);

          await selectRecordingTab(wrapper, 'Recording 1');
          fireEvent.click(wrapper.getByTestId('control-panel-expander'));
          expect(wrapper.queryAllByText(CAPTURE_SETTINGS_TAB_LABEL)).toHaveLength(0);
          await selectRecordingTab(wrapper, NEW_RECORDING_TAB_LABEL);
          expect(wrapper.queryAllByText(CAPTURE_SETTINGS_TAB_LABEL)).toHaveLength(1);
        });


        it('records data and calculates math channels on the fly', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          let recording: Recording | null;
          await waitUntilPass(() => {
            recording = selectActiveRecording(store.getState());
            expect(getMaxChannelLength(recording)).toBeGreaterThan(4);
          }, 5000);

          for (const chId of nonEmptyChannelIds) {
            expect(recording!.channels.find(ch => ch.id === chId)?.samples.length).toBeGreaterThan(1);
          }

          await waitUntilPass(() => {
            wrapper.getByText(/Sample rate: [,\d.]+Hz/);
          }, 5000);
        }, 10000);


        it('handles device errors while reading data', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => {
            const recording = selectActiveRecording(store.getState());
            expect(getMaxChannelLength(recording)).toBeGreaterThan(0);
          });
          selectedDevice.settings.getMany.mockRejectedValueOnce(new CommandFailedException('', makeRejectReason('BADCOMMAND')));
          await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/Failed to read one of the selected/));
        });


        it('handles stop button press while reading data', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          let maxLength = 0;
          await waitUntilPass(() => {
            const recording = selectActiveRecording(store.getState());
            maxLength = getMaxChannelLength(recording);
            expect(maxLength).toBeGreaterThan(0);
          });

          fireEvent.click(wrapper.getByText(STOP_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => {
            const recordings = selectSavedRecordings(store.getState());
            expect(recordings.length).toBe(1);
            const recording = recordings[0];
            expect(getMaxChannelLength(recording)).toBeGreaterThanOrEqual(maxLength);
          });
        });


        async function doMultipleRecordings(times: number, repeatButtonLabel: string, skipInitial = false) {
          if (!skipInitial) {
            fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
            await waitUntilPass(() => {
              const recording = selectActiveRecording(store.getState());
              expect(getMaxChannelLength(recording)).toBeGreaterThan(0);
            });

            fireEvent.click(wrapper.getByText(STOP_RECORDING_BUTTON_LABEL));
            await waitUntilPass(() => {
              wrapper.getByText(repeatButtonLabel);
            });
          }

          for (let i = skipInitial ? 0 : 1; i < times; i++) {
            fireEvent.click(wrapper.getByText(repeatButtonLabel));

            await waitUntilPass(() => {
              const recording = selectActiveRecording(store.getState());
              expect(getMaxChannelLength(recording)).toBeGreaterThan(0);
            });

            fireEvent.click(wrapper.getByText(STOP_RECORDING_BUTTON_LABEL));

            await waitUntilPass(() => {
              wrapper.getByText(repeatButtonLabel);
            });
          }
        }


        it('can repeat a recording', async () => {
          await doMultipleRecordings(2, REPEAT_RECORDING_BUTTON_LABEL);
          const recordings = selectSavedRecordings(store.getState());
          expect(recordings.length).toBe(2);
          for (const recording of recordings) {
            expect(getMaxChannelLength(recording)).toBeGreaterThan(0);
          }
        });


        it('can replace a recording', async () => {
          await doMultipleRecordings(2, REPLACE_RECORDING_BUTTON_LABEL);
          const recordings = selectSavedRecordings(store.getState());
          expect(recordings.length).toBe(1);
          for (const recording of recordings) {
            expect(getMaxChannelLength(recording)).toBeGreaterThan(0);
          }
        });


        it('can repeat a recording after device reload', async () => {
          await doMultipleRecordings(1, REPEAT_RECORDING_BUTTON_LABEL);
          const recordings = selectSavedRecordings(store.getState());
          const connectionKey = extractConnectionKey(recordings[0].deviceKey);

          TestOscilloscope.testStore.dispatch(connectionManagerActions.loadDevices(connectionKey, false));
          await reloadDevicesMock.waitForReload;

          await doMultipleRecordings(1, REPEAT_RECORDING_BUTTON_LABEL, true);
          const newRecordings = selectSavedRecordings(store.getState());
          expect(newRecordings.length).toBe(2);
        });


        it('cannot repeat when device changes', async () => {
          await doMultipleRecordings(1, REPEAT_RECORDING_BUTTON_LABEL);
          const recordings = selectSavedRecordings(store.getState());
          const connectionKey = extractConnectionKey(recordings[0].deviceKey);

          changeDeviceId = true;
          TestOscilloscope.testStore.dispatch(connectionManagerActions.loadDevices(connectionKey, false));
          await reloadDevicesMock.waitForReload;

          expect(wrapper.getByText(REPEAT_RECORDING_BUTTON_LABEL)).toBeDisabled();
        });


        it('deletes individual recordings when tab trash buttons are pressed', async () => {
          await doMultipleRecordings(2, REPEAT_RECORDING_BUTTON_LABEL);
          const buttons = await wrapper.findAllByTestId('delete-recording');
          fireEvent.click(buttons[0]);
          const recordings = selectSavedRecordings(store.getState());
          expect(recordings.length).toBe(1);
          expect(recordings[0].title).toBe('Recording 2');
        });


        it('deletes all recordings when clear all button is pressed', async () => {
          await doMultipleRecordings(2, REPEAT_RECORDING_BUTTON_LABEL);
          fireEvent.click(wrapper.getByText(DELETE_ALL_BUTTON_LABEL));
          fireEvent.click(wrapper.getAllByText('Delete All')[0]);
          const recordings = selectSavedRecordings(store.getState());
          expect(recordings.length).toBe(0);
        });


        it('handles device timeouts', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => {
            const recording = selectActiveRecording(store.getState());
            expect(getMaxChannelLength(recording)).toBeGreaterThan(0);
          });
          selectedDevice.settings.getMany.mockRejectedValueOnce(new RequestTimeoutException(''));
          await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/not responding/));
        });


        it('expires old data when the maximum buffer size is reached', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => {
            const recording = selectActiveRecording(store.getState());
            expect(getMaxChannelLength(recording)).toEqual(MAX_POLLED_BUFFER_SIZE);
          }, 15000);

          const getCalls = selectedDevice.settings.getMany.mock.calls.length;
          await waitUntilPass(() => {
            expect(selectedDevice.settings.getMany).toHaveBeenCalledTimes(getCalls + 10);
          }, 2000);

          const recording = selectActiveRecording(store.getState());
          expect(getMaxChannelLength(recording)).toEqual(MAX_POLLED_BUFFER_SIZE);
        }, 20000);
      });


      describe('in buffered mode', () => {
        beforeEach(async () => {
          await selectRecordingTab(wrapper, NEW_RECORDING_TAB_LABEL);
          await selectRecordingControls(wrapper);
          selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
          mockFWBufferedData(selectedDevice, ['pos', 'encoder.pos'].map(name => makeSettingChannelId(name, 1))
            .concat([makeSettingChannelId('io.di.port', 0)]));
        });


        afterEach(() => {
          store.dispatch(actions.stopRecording());
        });


        it('records data and calculates math channels', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          const scope = selectedDevice.oscilloscope;
          await waitUntilPass(() => expect(scope.addChannel).toHaveBeenCalledTimes(3));
          expect(scope.clear).toHaveBeenCalled();
          expect(scope.setDelay).toHaveBeenCalledWith(0);
          expect(scope.setTimebase).toHaveBeenCalledWith(0.1);

          let recording: Recording | null;
          await waitUntilPass(() => {
            recording = selectSavedRecordings(store.getState())[0] ?? null;
            expect(recording).not.toBeNull();
            const nonEmptyChannels = nonEmptyChannelIds.map(id => recording!.channels.find(ch => ch.id === id)!);
            // 9 because derivative math channels have less data.
            expect(_.every(nonEmptyChannels, ch => ch.samples.length >= 8)).toBeTruthy();

            // With integrated devices the FW-reported axis number 1 should be mapped to 0 to match the app's representation.
            expect(_.every(nonEmptyChannels, ch => ch.axis === 0)).toBeTruthy();
          });

          wrapper.getByText(/Sample rate: 10,000Hz/);
        });


        it('sets number of samples to record', async () => {
          setNumSamples(wrapper, 5);
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          const scope = selectedDevice.oscilloscope;
          await waitUntilPass(() => {
            expect(scope.start).toHaveBeenCalledWith(5);
          });
        });


        async function waitForRecordingToFinish(expectedCount: number) {
          let lastRecording: Recording | undefined;
          await waitUntilPass(() => {
            const recordings = selectSavedRecordings(store.getState());
            expect(recordings).toHaveLength(expectedCount);
            lastRecording = recordings[expectedCount - 1];
            expect(getMaxChannelLength(lastRecording)).toBeGreaterThan(0);
          });
          return lastRecording!;
        }


        it('can repeat the recording', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitForRecordingToFinish(1);

          fireEvent.click(wrapper.getByText(REPEAT_RECORDING_BUTTON_LABEL));
          await waitForRecordingToFinish(2);
        });


        it('can repeat a recording after device reload', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          const recording = await waitForRecordingToFinish(1);

          const connectionKey = extractConnectionKey(recording.deviceKey);
          TestOscilloscope.testStore.dispatch(connectionManagerActions.loadDevices(connectionKey, false));
          await reloadDevicesMock.waitForReload;

          fireEvent.click(wrapper.getByText(REPEAT_RECORDING_BUTTON_LABEL));
          await waitForRecordingToFinish(2);
        });


        it('handles scope capture already in progress during setup', async () => {
          selectedDevice.oscilloscope.clear.mockRejectedValueOnce(new CommandFailedException('', makeRejectReason('STATUSBUSY')));

          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/already in progress/));
        });


        it('handles scope rejecting add setting with STATUSBUSY', async () => {
          selectedDevice.oscilloscope.addChannel.mockRejectedValueOnce(new CommandFailedException('', makeRejectReason('STATUSBUSY')));

          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/already in progress/));
        });


        it('handles scope rejecting add setting with FULL', async () => {
          selectedDevice.oscilloscope.addChannel.mockRejectedValueOnce(new CommandFailedException('', makeRejectReason('FULL')));

          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/too many settings/));
        });


        it('handles scope rejecting add setting with BADCOMMAND', async () => {
          selectedDevice.oscilloscope.addChannel.mockRejectedValueOnce(new CommandFailedException('', makeRejectReason('BADCOMMAND')));

          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/not a valid channel name/));
        });


        it('handles scope rejecting add setting with BADDATA', async () => {
          selectedDevice.oscilloscope.addChannel.mockRejectedValueOnce(new CommandFailedException('', makeRejectReason('BADDATA')));

          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/cannot be recorded/));
        });


        it('handles cancel button during countdown', async () => {
          fireEvent.click(wrapper.getByText(CAPTURE_SETTINGS_TAB_LABEL));
          fireEvent.change(wrapper.getByTestId('start-timer'), { target: { value: '5' } });
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => wrapper.getByText(/Timer:\s+5s/));
          fireEvent.click(wrapper.getByText(STOP_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          expect(selectActiveRecording(store.getState())).toBeNull();
          expect(selectSavedRecordings(store.getState())).toHaveLength(0);
        });


        it('handles cancel button during device capture', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => wrapper.getByText(/Collecting.../));
          fireEvent.click(wrapper.getByText(STOP_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          expect(selectActiveRecording(store.getState())).toBeNull();
          expect(selectSavedRecordings(store.getState())).toHaveLength(0);
        });


        it('disables the stop button during download', async () => {
          const readDeferred = defer<unknown[]>();
          selectedDevice.oscilloscope.read.mockResolvedValueOnce(readDeferred.promise);
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => wrapper.getByText(/Downloading.../));
          expect(wrapper.getByText(STOP_RECORDING_BUTTON_LABEL)).toHaveAttribute('disabled');
          readDeferred.resolve(selectedDevice._data);
          await waitUntilPass(() => wrapper.getByText(REPEAT_RECORDING_BUTTON_LABEL));
          expect(selectActiveRecording(store.getState())).toBeNull();
          expect(selectSavedRecordings(store.getState())).toHaveLength(1);
        });


        it('sets scope delay', async () => {
          fireEvent.input(wrapper.getByTestId('recording-delay'), { target: { value: '2000' } });
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          await waitUntilPass(() => wrapper.getByText(REPEAT_RECORDING_BUTTON_LABEL));
          expect(selectedDevice.oscilloscope.setDelay).toHaveBeenCalledWith(2000);
        });


        it('saves CSVs with native data', async () => {
          setSampleRate(wrapper, 1000);
          setDifferencingWindow(wrapper, 2);
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          let recording: Recording | null;
          await waitUntilPass(() => {
            recording = selectSavedRecordings(store.getState())[0] ?? null;
            expect(recording).not.toBeNull();
            const nonEmptyChannels = nonEmptyChannelIds.map(id => recording!.channels.find(ch => ch.id === id)!);
            expect(_.every(nonEmptyChannels, ch => ch.samples.length >= 8)).toBeTruthy();
          });

          fireEvent.click(wrapper.getByTestId('recording-tab Recording 1'));
          fireEvent.click(wrapper.getByTestId('control-area-menu').getElementsByClassName('clickable')[0]);
          fireEvent.click(wrapper.getByTestId('native-toggle'));
          fireEvent.click(wrapper.getByText('Save CSV'));
          expect(Dialogs.showSaveDialog).toHaveBeenCalled();
          const expected = [
            'Trajectory Position,,Position,,Calculated Position Following Error,,Calculated Trajectory Velocity,,DI - Pin 2,',
            // eslint-disable-next-line max-len
            'Time (ms),Value (Native),Time (ms),Value (Native),Time (ms),Value (Native),Time (ms),Value (Native position units/s),Time (ms),Value (Native)',
            '0,0,0,0,0,0,0.5,1000,0,0',
            '1,1,1,1,1,0,1,1000,1,0',
            '2,2,2,2,2,0,2,1000,2,1',
            '3,3,3,3,3,0,3,1000,3,1',
            '4,4,4,4,4,0,4,1000,4,0',
            '5,5,5,5,5,0,5,1000,5,0',
            '6,6,6,6,6,0,6,1000,6,1',
            '7,7,7,7,7,0,7,1000,7,1',
            '8,8,8,8,8,0,8,1000,8,0',
            '9,9,9,9,9,0,,,9,0\n'].join('\n');
          await waitUntilPass(() => expect(writeFile).toHaveBeenCalledWith(TEST_CSV, expected, { encoding: 'utf-8', flag: 'w' }));
        });


        it('saves CSVs with converted data', async () => {
          setSampleRate(wrapper, 1000);
          setDifferencingWindow(wrapper, 2);
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          let recording: Recording | null;
          await waitUntilPass(() => {
            recording = selectSavedRecordings(store.getState())[0] ?? null;
            expect(recording).not.toBeNull();
            const nonEmptyChannels = nonEmptyChannelIds.map(id => recording!.channels.find(ch => ch.id === id)!);
            expect(_.every(nonEmptyChannels, ch => ch.samples.length >= 8)).toBeTruthy();
          });

          fireEvent.click(wrapper.getByTestId('recording-tab Recording 1'));
          fireEvent.click(wrapper.getByTestId('control-area-menu').getElementsByClassName('clickable')[0]);
          fireEvent.click(wrapper.getByText('Save CSV'));
          expect(Dialogs.showSaveDialog).toHaveBeenCalled();
          const expected = [
            'Trajectory Position,,Position,,Calculated Position Following Error,,Calculated Trajectory Velocity,,DI - Pin 2,',
            'Time (ms),Value (m),Time (ms),Value (m),Time (ms),Value (m),Time (ms),Value (m/s),Time (ms),Value (Native)',
            '0,0,0,0,0,0,0.5,1000,0,0',
            '1,1,1,1,1,0,1,1000,1,0',
            '2,2,2,2,2,0,2,1000,2,1',
            '3,3,3,3,3,0,3,1000,3,1',
            '4,4,4,4,4,0,4,1000,4,0',
            '5,5,5,5,5,0,5,1000,5,0',
            '6,6,6,6,6,0,6,1000,6,1',
            '7,7,7,7,7,0,7,1000,7,1',
            '8,8,8,8,8,0,8,1000,8,0',
            '9,9,9,9,9,0,,,9,0\n'].join('\n');
          await waitUntilPass(() => expect(writeFile).toHaveBeenCalledWith(TEST_CSV, expected, { encoding: 'utf-8', flag: 'w' }));
        });


        it('saves PNGs', async () => {
          mockSaveDialog.mockReturnValueOnce({
            cancelled: false,
            filePath: TEST_PNG,
          });

          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          let recording: Recording | null;
          await waitUntilPass(() => {
            recording = selectSavedRecordings(store.getState())[0] ?? null;
            expect(recording).not.toBeNull();
            const nonEmptyChannels = nonEmptyChannelIds.map(id => recording!.channels.find(ch => ch.id === id)!);
            expect(_.every(nonEmptyChannels, ch => ch.samples.length >= 8)).toBeTruthy();
          });

          fireEvent.click(wrapper.getByTestId('recording-tab Recording 1'));
          fireEvent.click(wrapper.getByTestId('control-area-menu').getElementsByClassName('clickable')[0]);
          fireEvent.click(wrapper.getByText('Save Image'));
          expect(Dialogs.showSaveDialog).toHaveBeenCalled();
          await waitUntilPass(() => expect(writeFile)
            .toHaveBeenCalledWith(TEST_PNG, expect.anything(), { encoding: 'binary', flag: 'w' }));
          expect(writeFile.mock.calls[writeFile.mock.calls.length - 1][1].length).toBeGreaterThan(0);
        });


        describe('with signal generator', () => {
          beforeEach(() => {
            configureSignal(wrapper);
          });


          afterEach(() => {
            (selectedDevice as DeviceMockIntegrated)._pos = 0;
          });


          it('does the move and returns to start', async () => {
            const axisMock = selectedDevice.getAxis(1);
            axisMock._pos = 100;
            fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
            await waitUntilPass(() =>
              expect(axisMock.moveRelative).toHaveBeenCalledWith(0.2, Length.CENTIMETRES, { waitUntilIdle: false }));
            const scope = selectedDevice.oscilloscope;
            await waitUntilPass(() => expect(scope.start).toHaveBeenCalled());
            await waitUntilPass(() => expect(axisMock.genericCommandNoResponse).toHaveBeenCalledWith('move abs 100'));
          });


          it('still returns to start if the capture is aborted', async () => {
            const axisMock = selectedDevice.getAxis(1);
            axisMock._pos = 1000;
            fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
            await waitUntilPass(() =>
              expect(axisMock.moveRelative).toHaveBeenCalledWith(0.2, Length.CENTIMETRES, { waitUntilIdle: false }));
            const scope = selectedDevice.oscilloscope;
            await waitUntilPass(() => expect(scope.start).toHaveBeenCalled());
            fireEvent.click(wrapper.getByText(STOP_RECORDING_BUTTON_LABEL));
            await waitUntilPass(() => expect(axisMock.genericCommandNoResponse).toHaveBeenCalledWith('move abs 1000'));
          });


          it('does the move but does not return to start if unselected', async () => {
            store.dispatch(actions.setSignalReturnToStart(false));
            const axisMock = selectedDevice.getAxis(1);
            axisMock._pos = 100;
            fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
            await waitUntilPass(() =>
              expect(axisMock.moveRelative).toHaveBeenCalledWith(0.2, Length.CENTIMETRES, { waitUntilIdle: false }));
            const scope = selectedDevice.oscilloscope;
            await waitUntilPass(() => expect(scope.read).toHaveBeenCalled());
            await waitUntilPass(() => expect(axisMock.genericCommandNoResponse).not.toHaveBeenCalled());
          });


          it('handles failure to read device position', async () => {
            selectedDevice.getAxis(1).getPosition.mockRejectedValueOnce(new CommandFailedException('', makeRejectReason('BADCOMMAND')));
            fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
            await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/Unable to read/));
          });


          it('handles failure to start movement', async () => {
            selectedDevice.getAxis(1).moveRelative.mockRejectedValueOnce(new CommandFailedException('', makeRejectReason('BADDATA')));
            fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
            await waitUntilPass(() => expect(getErrorDialogText(wrapper)).toMatch(/rejected the signal/));
          });
        });
      });


      describe('in externally triggered mode', () => {
        beforeEach(async () => {
          await selectRecordingTab(wrapper, NEW_RECORDING_TAB_LABEL);
          await selectRecordingControls(wrapper);
          selectRecordMode(wrapper, Mode.EXTERNAL);
          mockFWBufferedData(selectedDevice, ['pos', 'encoder.pos'].map(name => makeSettingChannelId(name, 1)));
          selectedDevice._timebase = 0.1;
        });


        afterEach(() => {
          store.dispatch(actions.stopRecording());
        });


        function toggleWait() {
          const checkbox = wrapper.getByTestId('wait-for-external-scope-read').nextSibling!; // Input element itself is not visible.
          fireEvent.click(checkbox);
        }


        it('accepts stop signal before data is ready', async () => {
          toggleWait();
          fireEvent.click(wrapper.getByTestId('record-button'));
          await waitUntilPass(() => {
            wrapper.getByText(WAITING_FOR_TRIGGER_MESSAGE);
          });

          fireEvent.click(wrapper.getByTestId('stop-button'));
          await waitUntilPass(() => {
            wrapper.getByTestId('record-button');
          });
        });


        it('waits for the external trigger and captures data', async () => {
          toggleWait();
          fireEvent.click(wrapper.getByTestId('record-button'));
          await waitUntilPass(() => {
            wrapper.getByText(WAITING_FOR_TRIGGER_MESSAGE);
          });

          const monitor = getMonitor()!;
          monitor.mockRequest(`/0${selectedDevice.deviceAddress} 0 scope start`);
          monitor.mockResponse(`/0${selectedDevice.deviceAddress} 0 OK IDLE -- 0`);

          await waitUntilPass(() => {
            const recording = selectSavedRecordings(store.getState())[0];
            expect(getMaxChannelLength(recording)).toBeGreaterThan(0);
          });
        });


        it('displays error if there is no scope data after external trigger', async () => {
          selectedDevice._data = [];
          toggleWait();
          fireEvent.click(wrapper.getByTestId('record-button'));
          await waitUntilPass(() => {
            wrapper.getByText(WAITING_FOR_TRIGGER_MESSAGE);
          });

          const monitor = getMonitor()!;
          monitor.mockRequest(`/0${selectedDevice.deviceAddress} 0 scope start`);
          monitor.mockResponse(`/0${selectedDevice.deviceAddress} 0 OK IDLE -- 0`);

          await waitUntilPass(() => {
            wrapper.getByText('No data was returned by the device.');
          });
        });


        it('waits for the external trigger and external data capture', async () => {
          let now = 0;
          container.get(TimeMeasuring).now = jest.fn(() => { now += 3000; return now });
          fireEvent.click(wrapper.getByTestId('record-button'));
          await waitUntilPass(() => {
            wrapper.getByText(WAITING_FOR_TRIGGER_MESSAGE);
          });

          const monitor = getMonitor()!;
          monitor.mockRequest(`/0${selectedDevice.deviceAddress} 0 scope start`);
          monitor.mockResponse(`@0${selectedDevice.deviceAddress} 0 OK IDLE -- 0`);

          await waitUntilPass(() => {
            wrapper.getByText(WAITING_FOR_EXTERNAL_PRINT_MESSAGE);
          });

          monitor.mockRequest(`/0${selectedDevice.deviceAddress} 0 scope print`);
          monitor.mockResponse(`@0${selectedDevice.deviceAddress} 0 OK IDLE -- 0`);
          monitor.mockResponse(`#0${selectedDevice.deviceAddress} 0 data 0`);

          await waitUntilPass(() => {
            const recording = selectSavedRecordings(store.getState())[0];
            expect(getMaxChannelLength(recording)).toBeGreaterThan(0);
          });
        });
      });


      describe('in download mode', () => {
        beforeEach(async () => {
          await selectRecordingTab(wrapper, NEW_RECORDING_TAB_LABEL);
          await selectRecordingControls(wrapper);
          selectRecordMode(wrapper, Mode.DOWNLOAD);
          mockFWBufferedData(selectedDevice, ['pos', 'encoder.pos'].map(name => makeSettingChannelId(name, 1)));
          selectedDevice._timebase = 0.1;
        });

        // TODO when fw7-2918 is done: Enable the stop button while the device is capturing; right now we cannot
        // easily identify that state in ZL.

        it('captures data', async () => {
          fireEvent.click(wrapper.getByTestId('download-button'));
          await waitUntilPass(() => {
            const recording = selectSavedRecordings(store.getState())[0];
            expect(getMaxChannelLength(recording)).toBeGreaterThan(0);
          });
        });


        it('displays error if there is no scope data', async () => {
          selectedDevice._data = [];
          fireEvent.click(wrapper.getByTestId('download-button'));
          await waitUntilPass(() => {
            wrapper.getByText('No data was returned by the device.');
          });
        });
      });


      describe('chart update', () => {
        beforeEach(async () => {
          await selectRecordingTab(wrapper, NEW_RECORDING_TAB_LABEL);
          await selectRecordingControls(wrapper);
          selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
          mockFWBufferedData(selectedDevice, ['pos', 'encoder.pos'].map(name => makeSettingChannelId(name, 1))
            .concat([makeSettingChannelId('io.di.port', 0)]));
        });


        afterEach(() => {
          store.dispatch(actions.stopRecording());
        });


        it('groups channels as specified', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          let recording: Recording | null;
          await waitUntilPass(() => {
            recording = selectSavedRecordings(store.getState())[0] ?? null;
            expect(recording).not.toBeNull();
            const nonEmptyChannels = nonEmptyChannelIds.map(id => recording!.channels.find(ch => ch.id === id)!);
            expect(_.every(nonEmptyChannels, ch => ch.samples.length >= 8)).toBeTruthy();
          });

          await selectRecordingTab(wrapper, 'Recording 1');
          // Uncomment to see the entire data structure - Jest's output on error is terrible.
          // await jest.requireActual('fs').promises.writeFile('json.json',
          //   JSON.stringify(chartsMock.mock.lastCall, null, 2));
          expect(chartsMock).toHaveBeenLastCalledWith(expect.objectContaining({
            option: expect.objectContaining({
              yAxis: [
                expect.objectContaining({
                  name: 'Position,\nTrajectory Position {u|(m)}',
                }),
                expect.objectContaining({
                  name: 'Position Following Error {u|(m)}',
                }),
                expect.objectContaining({
                  name: 'Trajectory Velocity {u|(m/s)}',
                }),
                expect.objectContaining({
                  data: ['di 2'],
                })
              ],
              legend: [
                expect.objectContaining({
                  data: [
                    expect.objectContaining({ name: 'Position' }),
                    expect.objectContaining({ name: 'Position Following Error' }),
                    expect.objectContaining({ name: 'Trajectory Position' }),
                  ]
                }),
                expect.objectContaining({
                  data: [
                    expect.objectContaining({ name: 'Trajectory Velocity' }),
                  ]
                }),
                expect.objectContaining({
                  data: [
                    expect.objectContaining({ name: 'DI - Pin 2' }),
                  ]
                }),
              ],
            }),
          }));
        });
      });
    });
  });
});


describe('With a multi-axis device', () => {
  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockMultiAxis);
    wrapper = render(<TestOscilloscope/>);
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;

    setAxisCallback(null);
    setDeviceCallback(null);
  });


  describe('Oscilloscope', () => {
    beforeEach(() => {
      setDeviceCallback(device => {
        selectedDevice = device;
        setDeviceCallback(null);
      });
    });


    describe('recording', () => {
      let store: AppStore;

      beforeEach(async () => {
        store = TestOscilloscope.testStore;
        mockSingleDeviceWithPeripherals(store, {
          peripheralCount: 2,
          modifier: addDeviceScopeSettingsInfo,
        });
        const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
        connectionViewMockInstance.props.onSelect(device.key);
        await waitUntilPass(() => expect(selectAllChannels(store.getState()).length).toBeGreaterThan(0));

        // Add a derivative and a difference math channel to axis 1 only
        const math = _.cloneDeep(extractMathChannels(selectAllChannels(store.getState())));
        math.find(ch => ch.id === makeMathChannelId(MathChannelNames.TRAJECTORY_VEL, 1))!.showInUi = true;
        math.find(ch => ch.id === makeMathChannelId(MathChannelNames.FOLLOW_ERR, 1))!.showInUi = true;
        store.dispatch(actions.closeAddMathDialog(math));
      });


      describe('in buffered mode', () => {
        beforeEach(async () => {
          await selectRecordingTab(wrapper, NEW_RECORDING_TAB_LABEL);
          await selectRecordingControls(wrapper);
          selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
          mockFWBufferedData(selectedDevice,
            [1, 2].map(axis => ['pos', 'encoder.pos'].map(name => makeSettingChannelId(name, axis))).flat());
        });


        afterEach(() => {
          store.dispatch(actions.stopRecording());
        });


        it('does not remap FW axis 1 to 0', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          const scope = selectedDevice.oscilloscope;
          await waitUntilPass(() => expect(scope.addChannel).toHaveBeenCalledTimes(4));

          let recording: Recording | null;
          await waitUntilPass(() => {
            recording = selectSavedRecordings(store.getState())[0] ?? null;
            expect(recording).not.toBeNull();
            const nonEmptyChannels = recording!.channels.filter(ch => ch.samples.length >= 8);
            expect(nonEmptyChannels.length).toBe(6);
            expect(_.every(nonEmptyChannels, ch => ch.axis === 1 || ch.axis === 2)).toBeTruthy();
          });
        });


        describe('with signal generator', () => {
          beforeEach(() => {
            const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
            connectionViewMockInstance.props.onSelect(makeAxisKey(device.key, 1));
            configureSignal(wrapper, true);
          });


          afterEach(() => {
            (selectedDevice as DeviceMockIntegrated)._pos = 0;
          });


          it('signals the selected axis', async () => {
            await selectRecordingControls(wrapper);
            selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
            const selector = wrapper.getByTestId('signal-axis-selector');
            fireEvent.change(selector, { target: { value: '2' } });
            const axis1 = selectedDevice.getAxis(1);
            const axis2 = selectedDevice.getAxis(2);
            axis2._pos = 100;
            fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
            await waitUntilPass(() => expect(axis2.moveRelative).toHaveBeenCalledWith(0.2, Length.CENTIMETRES, { waitUntilIdle: false }));
            const scope = selectedDevice.oscilloscope;
            await waitUntilPass(() => expect(scope.start).toHaveBeenCalled());
            await waitUntilPass(() => expect(axis2.genericCommandNoResponse).toHaveBeenCalledWith('move abs 100'));
            expect(axis1.genericCommandNoResponse).not.toHaveBeenCalled();
          });
        });
      });


      describe('chart update', () => {
        const nonEmptyChannelIds =
          ['pos', 'encoder.pos'].map(id => makeSettingChannelId(id, 1))
            .concat([MathChannelNames.TRAJECTORY_VEL, MathChannelNames.FOLLOW_ERR].map(id => makeMathChannelId(id, 1)))
            .concat(['pos', 'encoder.pos'].map(id => makeSettingChannelId(id, 2)));


        beforeEach(async () => {
          await selectRecordingTab(wrapper, NEW_RECORDING_TAB_LABEL);
          await selectRecordingControls(wrapper);
          selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
          mockFWBufferedData(selectedDevice,
            [1, 2].map(axis => ['pos', 'encoder.pos'].map(name => makeSettingChannelId(name, axis))).flat());
        });


        afterEach(() => {
          store.dispatch(actions.stopRecording());
        });


        it('groups channels as specified', async () => {
          fireEvent.click(wrapper.getByText(START_RECORDING_BUTTON_LABEL));
          let recording: Recording | null;
          await waitUntilPass(() => {
            recording = selectSavedRecordings(store.getState())[0] ?? null;
            expect(recording).not.toBeNull();
            const nonEmptyChannels = nonEmptyChannelIds.map(id => recording!.channels.find(ch => ch.id === id)!);
            expect(_.every(nonEmptyChannels, ch => ch.samples.length >= 8)).toBeTruthy();
          });

          await selectRecordingTab(wrapper, 'Recording 1');
          expect(chartsMock).toHaveBeenLastCalledWith(expect.objectContaining({
            option: expect.objectContaining({
              yAxis: [
                expect.objectContaining({
                  name: 'Axis 1 Position,\nAxis 2 Position,\n' +
                    'Axis 1 Trajectory Position,\nAxis 2 Trajectory Position {u|(nm)}',
                }),
                expect.objectContaining({
                  name: 'Axis 1 Position Following Error {u|(nm)}',
                }),
                expect.objectContaining({
                  name: 'Axis 1 Trajectory Velocity {u|(mm/s)}',
                })
              ],
              legend: [
                expect.objectContaining({
                  data: [
                    expect.objectContaining({ name: 'Axis 1 Position' }),
                    expect.objectContaining({ name: 'Axis 2 Position' }),
                    expect.objectContaining({ name: 'Axis 1 Position Following Error' }),
                    expect.objectContaining({ name: 'Axis 1 Trajectory Position' }),
                    expect.objectContaining({ name: 'Axis 2 Trajectory Position' }),
                  ]
                }),
                expect.objectContaining({
                  data: [
                    expect.objectContaining({ name: 'Axis 1 Trajectory Velocity' }),
                  ]
                }),
              ],
            }),
          }));
        });
      });
    });
  });
});
