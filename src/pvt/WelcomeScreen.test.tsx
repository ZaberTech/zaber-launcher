const mockOpenDialog = jest.fn(() => ({
  cancelled: true,
  filePaths: [],
}));

const mockSaveDialog = jest.fn(() => ({
  filePath: 'foo.csv',
}));

jest.mock('../dialogs', () => ({
  Dialogs: {
    showOpenDialog: mockOpenDialog,
    showSaveDialog: mockSaveDialog,
  },
}));

const openExternalLink = jest.fn();
jest.mock('../desktop', () => ({
  openExternalLink,
}));


let fileContent = '';

const fsMock = {
  readFile: jest.fn(async () => fileContent),
  writeFile: jest.fn(async (_, data) => fileContent = data),
};

jest.mock('fs/promises', () => fsMock);

import { EChartsReactMock } from './testing/mocks';

jest.mock('echarts-for-react', () => EChartsReactMock);

import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';

import { PvtApp } from './PvtApp';
import { selectUserPath } from './selectors';
import { EXAMPLE_DATA_LINEAR } from './WelcomeScreen';
import { IMPORT_BUTTON_LABEL } from './CsvImportDialog';


const TestApp = wrapWithNewStore(wrapWithRouter(PvtApp));
let wrapper: RenderResult;

describe('Welcome screen', () => {
  beforeEach(() => {
    createContainer();
    wrapper = render(<TestApp/>);
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
  });


  it('opens the import dialog', () => {
    fireEvent.click(wrapper.getByText('Import a CSV'));
    expect(mockOpenDialog).toHaveBeenCalled();
  });


  it('opens online documentation', () => {
    fireEvent.click(wrapper.getByText('Zaber website'));
    expect(openExternalLink).toHaveBeenCalled();
  });


  it('saves and opens example data', async () => {
    expect(selectUserPath(TestApp.testStore.getState())).toBeNull();
    fireEvent.click(wrapper.getByText('Click here'));
    await waitUntilPass(() => expect(fileContent).toMatch(/Horizontal position \(mm\)/));
    await waitUntilPass(() => wrapper.getByText(IMPORT_BUTTON_LABEL));
    fireEvent.click(wrapper.getByText(IMPORT_BUTTON_LABEL));
    await waitUntilPass(() => expect(selectUserPath(TestApp.testStore.getState())).toMatchObject(EXAMPLE_DATA_LINEAR));
  });
});
