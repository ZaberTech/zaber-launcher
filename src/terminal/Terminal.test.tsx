/* eslint-disable camelcase */
import React from 'react';
import _ from 'lodash';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import type { Container } from 'inversify';
import { Subject } from 'rxjs';
import { clipboard } from 'electron';
import { ascii, Measurement, RequestTimeoutException } from '@zaber/motion';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import {
  mockDataForDeviceWithoutPeripherals, mockLocalConnections, mockSingleDevice, mockSingleDeviceWithPeripherals,
} from '../connection_manager/mocks';
import { createContainer, destroyContainer } from '../container';
import { connectionManagerActions, ProductInfo, selectAxes, selectConnections, selectDevices, selectRouters } from '../connection_manager';
import { MessageRoutersService } from '../message_router';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase
} from '../test/mocks/ascii';
import type { MonitorMessage, MonitorMessageSource } from '../app_components';
import { makeAxisKey, makeDeviceKey } from '../keys';

import { Terminal } from './Terminal';
import { MAX_HISTORY_ITEMS } from './reducer';
import type { HistoryItem } from './types';
import type { ErrorDetailsType } from './ErrorDetails';

const scrollIntoViewMock = jest.fn();
window.HTMLElement.prototype.scrollIntoView = scrollIntoViewMock;

jest.mock('./MessageTagger', () => ({
  MessageTagger: ({ historyItem, onClickError }: { historyItem: HistoryItem; onClickError: (type: ErrorDetailsType) => void }) =>
    <span onClick={() => onClickError('rejectedCommand')}>{historyItem.text}</span>
}));

jest.mock('../protocol_manual/hooks', () => ({
  useCommandIdList: () => [],
  useSettingIdList: () => [],
  useCommandRejectedReasonManual: () => undefined,
}));

const DEFAULT_REPLY = {
  deviceAddress: 1,
  axisNumber: 0,
  replyFlag: 'OK',
  data: '0',
  status: 'IDLE',
  warningFlag: '--',
  messageType: ascii.MessageType.REPLY,
};

const TestTerminal = wrapWithNewStore(wrapWithRouter(Terminal));
let wrapper: RenderResult;
let container: Container;
let connections: ConnectionMock[];
let devices: DeviceMock[];
let axes: AxisMock[];
let connectionCallback: (connection: ConnectionMock) => void;
let monitorConnections: MonitorConnectionMock[];
let failToConnectToNextConnection: boolean;
let throwErrorInNextSendMessage: boolean;

class AxisMock extends AxisMockBase {
  constructor(axisNumber: number, device: DeviceMock) {
    super(axisNumber, device);
    axes.push(this);
  }

  genericCommandNoResponse = jest.fn(async (cmd: string) => {
    const message = `/${this.device.deviceAddress} ${this.axisNumber} ${cmd}`;
    const monitor = _.last(monitorConnections)!;
    monitor.mockRequest(message);
  });

  prepareCommand = jest.fn(async (cmd: string, measurement: Measurement) => cmd.replace('?', (measurement.value * 100).toString()));
}

class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(deviceAddress: number, connection: ConnectionMock) {
    super(deviceAddress, connection);
    devices.push(this);
  }

  allAxes = {
    isBusy: jest.fn(async () => false),
  };

  genericCommandNoResponse = jest.fn(async (cmd: string) => {
    const message = `/${this.deviceAddress} ${cmd}`;
    const monitor = _.last(monitorConnections)!;
    monitor.mockRequest(message);
  });

  prepareCommand = jest.fn(async (cmd: string, measurement: Measurement) => cmd.replace('?', (measurement.value * 100).toString()));
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  constructor(id: string, router: RouterConnectionMock) {
    super(id, router);
    connections.push(this);
    connectionCallback?.(this);
  }

  connection = {
    genericCommandNoResponse: jest.fn(async (cmd: string, device: number, axis: number) => {
      const deviceStr = device >= 0 ? `${device} ` : '';
      const axisStr = axis >= 0 ? `${axis} ` : '';
      const message = `/${deviceStr}${axisStr}${cmd}`;

      const monitor = _.last(monitorConnections)!;
      monitor.mockRequest(message);

      if (throwErrorInNextSendMessage) {
        throw Error('Mock error');
      }

      switch (message) {
        case '/get pos':
          monitor.mockResponse('@1 0 OK IDLE -- 0');
          break;
        case '/home':
          monitor.mockResponse('@1 0 OK BUSY -- 0');
          break;
        default:
          monitor.mockResponse('@1 0 RJ IDLE -- BADCOMMAND');
          break;
      }
    }),
  };

  genCmdNoResp = this.connection.genericCommandNoResponse;
  genericCommand = jest.fn(async (_cmd: string) => DEFAULT_REPLY);
  genericCommandMultiResponse = jest.fn(async (_cmd: string) => ([DEFAULT_REPLY]));
}

class MonitorConnectionMock {
  monitorMessages = new Subject<MonitorMessage[]>();

  constructor(public readonly id: string, public readonly router: RouterConnectionMock) {
    monitorConnections.push(this);
  }

  start = jest.fn(async () => {
    if (failToConnectToNextConnection) { throw new Error('Failed') }
  });
  stop = jest.fn().mockResolvedValue(undefined);

  mockRequest = jest.fn((message: string, source: MonitorMessageSource = 'cmd') => this.monitorMessages.next([{ message, source }]));
  mockResponse = jest.fn((message: string, source: MonitorMessageSource = 'reply') => this.monitorMessages.next([{ message, source }]));
  mockMessages = jest.fn((messages: MonitorMessage[]) => this.monitorMessages.next(messages));
  mockError = jest.fn(() => {
    this.monitorMessages.error(new Error('Failed'));
  });
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
  createMonitorConnection = jest.fn((id: string) => new MonitorConnectionMock(id, this));
}

class MessageRouterServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

jest.mock('electron', () => ({
  clipboard: {
    writeText: jest.fn(),
  },
}));

beforeEach(() => {
  connections = [];
  devices = [];
  axes = [];
  monitorConnections = [];
  failToConnectToNextConnection = false;
  throwErrorInNextSendMessage = false;

  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRouterServiceMock);

  wrapper = render(<TestTerminal/>);

  mockSingleDevice(TestTerminal.testStore);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  connectionCallback = null!;

  scrollIntoViewMock.mockClear();
});

async function waitForConnected(): Promise<void> {
  await waitUntilPass(() => expect(wrapper.getByTitle('Terminal input')).not.toBeDisabled());
}

function getTerminalInput() {
  return wrapper.getByTitle('Terminal input') as HTMLInputElement;
}

test('terminal can only be used once a connection is selected', async () => {
  expect(wrapper.queryByTitle('Terminal input')).toBeNull();

  const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
  connectionViewMockInstance.props.onSelect(connectionData.key);

  wrapper.getByTitle('Terminal input');
});

describe('connection selected', () => {
  beforeEach(async () => {
    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitForConnected();
  });

  test('Send a command to selected connection and see the request and response text in the UI', async () => {
    const terminalInput = getTerminalInput();
    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.click(wrapper.getByTitle('Send'));

    await waitUntilPass(() => {
      expect(terminalInput.value).toEqual('');
      wrapper.getByText('/get pos');
      wrapper.getByText('@1 0 OK IDLE -- 0');
    });
    expect(connections[0].genCmdNoResp).toHaveBeenCalledWith('get pos');
  });

  test('terminal history also shows other arbitrary background messages being passed around using the given connection', async () => {
    // All this needs to be done so that the connection gets initialized and the `connections` array
    // gets populated correctly
    fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'get pos' } });
    fireEvent.click(wrapper.getByTitle('Send'));
    await waitUntilPass(() => {
      wrapper.getByText('@1 0 OK IDLE -- 0');
    });

    const connection = monitorConnections[0];
    connection.mockRequest('/1 0 1 get deviceid:70');
    connection.mockResponse('@1 0 1 OK IDLE -- 30341');

    await waitUntilPass(() => {
      wrapper.getByText('/1 0 1 get deviceid:70');
      wrapper.getByText('@1 0 1 OK IDLE -- 30341');
    });
  });

  test('History only shows local history when "show external messages" is unchecked', async () => {
    fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'get pos' } });
    fireEvent.click(wrapper.getByTitle('Send'));
    await waitUntilPass(() => {
      wrapper.getByText('@1 0 OK IDLE -- 0');
    });

    const connection = monitorConnections[0];
    connection.mockRequest('/1 0 1 get deviceid:70', 'cmd-other');
    connection.mockResponse('@1 0 1 OK IDLE -- 30341', 'reply-other');

    await waitUntilPass(() => {
      wrapper.getByText('/1 0 1 get deviceid:70');
      wrapper.getByText('@1 0 1 OK IDLE -- 30341');
    });

    fireEvent.click(wrapper.getByTitle('Terminal Actions'));
    fireEvent.click(wrapper.getByTitle('Show Messages Originating from Outside This Window'));
    expect(wrapper.queryByText('/1 0 1 get deviceid:70')).toBeNull();
    expect(wrapper.queryByText('@1 0 1 OK IDLE -- 30341')).toBeNull();
    wrapper.getByText('@1 0 OK IDLE -- 0');
  });

  test('Shows checksums', async () => {
    const HIDE_CLASS = 'hide-checksums';
    expect(wrapper.baseElement.querySelector(`.${HIDE_CLASS}`)).not.toBeNull();
    fireEvent.click(wrapper.getByTitle('Terminal Actions'));
    fireEvent.click(wrapper.getByText('Show Checksums'));
    expect(wrapper.baseElement.querySelector(`.${HIDE_CLASS}`)).toBeNull();
  });

  test('"Enter" key can be used to send a command', async () => {
    const terminalInput = getTerminalInput();
    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.focus(terminalInput);
    fireEvent.keyDown(terminalInput, { key: 'Enter', code: 'Enter' });

    await waitUntilPass(() => {
      expect(terminalInput.value).toEqual('');
      wrapper.getByText('/get pos');
      wrapper.getByText('@1 0 OK IDLE -- 0');
    });
  });

  test('"Clear history" button can be clicked to clear history for selected connection', async () => {
    fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'get pos' } });
    fireEvent.click(wrapper.getByTitle('Send'));

    await waitUntilPass(() => {
      wrapper.getByText('/get pos');
      wrapper.getByText('@1 0 OK IDLE -- 0');
    });

    fireEvent.click(wrapper.getByTitle('Terminal Actions'));
    fireEvent.click(wrapper.getByTitle('Clear History'));
    expect(wrapper.queryByText('/get pos')).toBeNull();
    expect(wrapper.queryByText('@1 0 OK IDLE -- 0')).toBeNull();
  });

  test('copy history to the clipboard', async () => {
    fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'get pos' } });
    fireEvent.click(wrapper.getByTitle('Send'));
    fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'home' } });
    fireEvent.click(wrapper.getByTitle('Send'));

    await waitUntilPass(() => {
      wrapper.getByText('/home');
      wrapper.getByText('@1 0 OK BUSY -- 0');
    });

    fireEvent.click(wrapper.getByTitle('Terminal Actions'));
    fireEvent.click(wrapper.getByTitle('Copy Command History to Clipboard'));
    expect(clipboard.writeText).toHaveBeenCalledWith('/get pos\n@1 0 OK IDLE -- 0\n/home\n@1 0 OK BUSY -- 0');
  });

  test('nothing is sent through the connection if user sends an empty command', async () => {
    // All this needs to be done so that the connection gets initialized and the `connections` array
    // gets populated correctly
    fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'get pos' } });
    fireEvent.click(wrapper.getByTitle('Send'));
    await waitUntilPass(() => {
      wrapper.getByText('@1 0 OK IDLE -- 0');
    });
    expect(connections[0].genCmdNoResp).toHaveBeenCalledTimes(1);

    fireEvent.click(wrapper.getByTitle('Send'));
    expect(connections[0].genCmdNoResp).toHaveBeenCalledTimes(1);
  });

  test('Display any errors that happen while sending a message in the history', async () => {
    throwErrorInNextSendMessage = true;
    fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'get pos' } });
    fireEvent.click(wrapper.getByTitle('Send'));

    await waitUntilPass(() => wrapper.getByText('Error: Mock error'));
  });

  test('adds slash in front of commands', async () => {
    fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'home' } });
    fireEvent.click(wrapper.getByTitle('Send'));
    await waitUntilPass(() => {
      wrapper.getByText('/home');
    });
    expect(connections[0].genCmdNoResp).toHaveBeenCalledWith('home');
  });

  test('removes non-local messages when the message count goes over the limit', async () => {
    const connection = _.last(monitorConnections)!;
    connection.mockMessages([{
      message: 'Reply Local 1',
      source: 'reply',
    }, {
      message: 'Reply Other',
      source: 'reply-other',
    }, {
      message: 'Reply Local 2',
      source: 'reply',
    }]);
    connection.mockMessages(_.range(MAX_HISTORY_ITEMS - 3).map(i => ({
      message: `Reply Fill ${i + 1}`,
      source: 'reply-other',
    })));
    await waitUntilPass(() => {
      wrapper.getByText('Reply Local 1');
      wrapper.getByText('Reply Other');
      wrapper.getByText('Reply Local 2');

      wrapper.getByText('Reply Fill 1');
      wrapper.getByText(`Reply Fill ${MAX_HISTORY_ITEMS - 3}`);
    });

    connection.mockMessages(_.range(3).map(() => ({
      message: 'Reply Over Limit',
      source: 'reply-other',
    })));

    await waitUntilPass(() => {
      wrapper.getByText('Reply Local 1');
      wrapper.getByText('Reply Local 2');

      expect(wrapper.queryByText('Reply Other')).toBeNull();
      expect(wrapper.queryByText('Reply Fill 1')).toBeNull();
      expect(wrapper.queryByText('Reply Fill 2')).toBeNull();
    });
  });

  test('removes even local messages when the message count goes over the limit', async () => {
    const connection = _.last(monitorConnections)!;
    connection.mockMessages([{
      message: 'Reply Other',
      source: 'reply-other',
    }]);
    connection.mockMessages(_.range(MAX_HISTORY_ITEMS - 1).map(i => ({
      message: `Reply Fill ${i + 1}`,
      source: 'reply',
    })));
    await waitUntilPass(() => {
      wrapper.getByText('Reply Other');
      wrapper.getByText('Reply Fill 1');
      wrapper.getByText(`Reply Fill ${MAX_HISTORY_ITEMS - 1}`);
    });

    connection.mockMessages(_.range(2).map(() => ({
      message: 'Reply Over Limit',
      source: 'reply',
    })));

    await waitUntilPass(() => {
      expect(wrapper.queryByText('Reply Other')).toBeNull();
      expect(wrapper.queryByText('Reply Fill 1')).toBeNull();
      wrapper.getByText('Reply Fill 2');
    });
  });

  test('does not crash when connection is removed', () => {
    const router = _.sample(selectRouters(TestTerminal.testStore.getState()))!;
    TestTerminal.testStore.dispatch(connectionManagerActions.connectionsLoaded(router.key, {
      name: '',
      connections: [],
    }));
    wrapper.getByText(/Welcome to Terminal/);
  });

  test('displays connection name in the title', async () => {
    const titleConnection = wrapper.getByTitle('Connection');
    expect(titleConnection).toHaveTextContent('COM5');
  });
});

test('selecting a different connection shows history elements specific to that connection', async () => {
  mockLocalConnections(TestTerminal.testStore, {
    COM5: connectionKey => [mockDataForDeviceWithoutPeripherals(connectionKey, 1)],
    COM6: connectionKey => [mockDataForDeviceWithoutPeripherals(connectionKey, 1)],
  });

  const connectionKeys = _.keys(selectConnections(TestTerminal.testStore.getState()));

  connectionViewMockInstance.props.onSelect(connectionKeys[0]);
  await waitForConnected();
  fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'get pos' } });
  fireEvent.click(wrapper.getByTitle('Send'));
  await waitUntilPass(() => {
    wrapper.getByText('/get pos');
    wrapper.getByText('@1 0 OK IDLE -- 0');
  });

  connectionViewMockInstance.props.onSelect(connectionKeys[1]);
  await waitForConnected();
  expect(wrapper.queryByText('/get pos')).toBeNull();
  expect(wrapper.queryByText('@1 0 OK IDLE -- 0')).toBeNull();
  fireEvent.change(wrapper.getByTitle('Terminal input'), { target: { value: 'home' } });
  fireEvent.click(wrapper.getByTitle('Send'));
  await waitUntilPass(() => {
    wrapper.getByText('/home');
    wrapper.getByText('@1 0 OK BUSY -- 0');
  });

  connectionViewMockInstance.props.onSelect(connectionKeys[0]);
  await waitForConnected();
  expect(wrapper.queryByText('/home')).toBeNull();
  expect(wrapper.queryByText('@1 0 OK BUSY -- 0')).toBeNull();
  wrapper.getByText('/get pos');
  wrapper.getByText('@1 0 OK IDLE -- 0');
});

describe('History navigation in the terminal input', () => {
  beforeEach(async () => {
    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitForConnected();
  });

  test('pressing UP and DOWN keys navigate to older and newer local requests, respectively', async () => {
    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');
    const connection = monitorConnections[0];

    connection.mockRequest('/1 0 1 get deviceid:70', 'cmd-other');
    connection.mockResponse('@1 0 1 OK IDLE -- 30341', 'reply-other');
    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.click(sendButton);

    connection.mockRequest('/1 0 1 get deviceid:70', 'cmd-other');
    connection.mockResponse('@1 0 1 OK IDLE -- 30341', 'reply-other');
    fireEvent.change(terminalInput, { target: { value: 'home' } });
    fireEvent.click(sendButton);

    await waitUntilPass(() => wrapper.getByText('@1 0 OK BUSY -- 0'));
    expect(terminalInput.value).toEqual('');

    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(terminalInput.value).toEqual('home');

    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(terminalInput.value).toEqual('get pos');

    fireEvent.keyDown(terminalInput, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(terminalInput.value).toEqual('home');

    fireEvent.keyDown(terminalInput, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(terminalInput.value).toEqual('');
  });

  test('navigating to the bottom resets terminal input text to what it used to be in the beginning', async () => {
    const terminalInput = getTerminalInput();

    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.click(wrapper.getByTitle('Send'));
    await waitUntilPass(() => wrapper.getByText('@1 0 OK IDLE -- 0'));

    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(terminalInput.value).toEqual('get pos');

    fireEvent.keyDown(terminalInput, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(terminalInput.value).toEqual('');
  });

  test('navigating to an old request and directly sending it resets history navigation index back to bottom', async () => {
    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.click(sendButton);
    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.click(sendButton);
    fireEvent.change(terminalInput, { target: { value: 'home' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() => wrapper.getByText('@1 0 OK BUSY -- 0'));

    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });
    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });

    fireEvent.click(sendButton);
    await waitUntilPass(() => expect(wrapper.queryAllByText('@1 0 OK IDLE -- 0')).toHaveLength(2));

    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(terminalInput.value).toEqual('get pos');
  });

  test('navigation stops when user tries to go beyond the number of requests a connection actually has in its history', async () => {
    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() => wrapper.getByText('@1 0 OK IDLE -- 0'));
    expect(terminalInput.value).toEqual('');

    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(terminalInput.value).toEqual('get pos');

    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(terminalInput.value).toEqual('get pos');

    fireEvent.keyDown(terminalInput, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(terminalInput.value).toEqual('');
  });

  test('command send multiple times only appears once in the history', async () => {
    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: 'home' } });
    fireEvent.click(sendButton);

    for (let i = 0; i < 3; i++) {
      fireEvent.change(terminalInput, { target: { value: 'get pos' } });
      fireEvent.click(sendButton);
    }

    await waitUntilPass(() => expect(wrapper.queryAllByText(/^@1 0 OK/)).toHaveLength(4));
    expect(terminalInput.value).toEqual('');

    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(terminalInput.value).toEqual('get pos');

    fireEvent.keyDown(terminalInput, { key: 'ArrowUp', code: 'ArrowUp' });
    expect(terminalInput.value).toEqual('home');

    fireEvent.keyDown(terminalInput, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(terminalInput.value).toEqual('get pos');

    fireEvent.keyDown(terminalInput, { key: 'ArrowDown', code: 'ArrowDown' });
    expect(terminalInput.value).toEqual('');
  });

  test('Pressing Escape clears the input', async () => {
    const terminalInput = getTerminalInput();
    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.keyDown(terminalInput, { key: 'Escape' });
    expect(terminalInput.value).toEqual('');
  });
});

describe('scrolling', () => {
  beforeEach(async () => {
    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitForConnected();
  });

  test('Adding messages will scroll to bottom', async () => {
    const connection = monitorConnections[0];
    connection.mockRequest('/1 0 1 get deviceid:70', 'cmd');
    await waitUntilPass(() => expect(scrollIntoViewMock).toHaveBeenCalledTimes(1));
    connection.mockResponse('@1 0 1 OK IDLE -- 30341', 'reply');
    await waitUntilPass(() => expect(scrollIntoViewMock).toHaveBeenCalledTimes(2));
  });

  test('Opening error details at the bottom will scroll to bottom', async () => {
    const connection = monitorConnections[0];
    connection.mockResponse('@1 0 RJ IDLE -- BADCOMMAND', 'reply');
    await waitUntilPass(() => wrapper.getByText('@1 0 RJ IDLE -- BADCOMMAND'));

    scrollIntoViewMock.mockClear();
    const taggedErrorMessage = wrapper.getByText('@1 0 RJ IDLE -- BADCOMMAND');
    fireEvent.click(taggedErrorMessage);
    expect(scrollIntoViewMock).toHaveBeenCalledTimes(1);
  });
});

describe('axis and device selection', () => {
  beforeEach(async () => {
    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitForConnected();

    mockSingleDeviceWithPeripherals(TestTerminal.testStore, { type: ['legacy', 'rotary'], peripheralCount: 2 });
  });

  test('select device sends command to that device', async () => {
    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() =>
      expect(devices[0].genericCommandNoResponse).toHaveBeenLastCalledWith('get pos'));
  });

  test('select axis sends command to that axis', async () => {
    const axis = _.sample(selectAxes(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitForConnected();

    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: 'get pos' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() =>
      expect(axes[0].genericCommandNoResponse).toHaveBeenLastCalledWith('get pos'));
  });

  test('select device shows device name in title', async () => {
    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    const titleDevice = wrapper.getByTitle('Device');
    expect(titleDevice).toHaveTextContent('X-MCC2');
  });

  test('select axis shows axis name in title', async () => {
    const axis = _.sample(selectAxes(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitForConnected();

    const titleAxis = wrapper.getByTitle('Axis / Peripheral');
    expect(titleAxis).toHaveTextContent(axis.identity!.peripheralName);
  });

  test('switching between connection, device, and axis does not reconnect the connection', async () => {
    const connection = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    await waitForConnected();

    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    const axis = _.sample(selectAxes(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitForConnected();

    expect(monitorConnections).toHaveLength(1);
    expect(monitorConnections[0].router.createMonitorConnection).toHaveBeenCalledTimes(1);
  });

  test('command reference adapts to selected device', async () => {
    function getLink() {
      return wrapper.getAllByText('ASCII Protocol Reference')[0].getAttribute('data-link');
    }

    const connection = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    expect(getLink()).toEqual('https://www.zaber.com/protocol-manual?protocol=ASCII');

    const deviceKey = makeDeviceKey(connection.key, 1);
    connectionViewMockInstance.props.onSelect(deviceKey);
    expect(getLink()).toEqual(
      'https://www.zaber.com/protocol-manual?protocol=ASCII&device=X-MCC2&version=7.13&peripheral=LHM025A-T3');

    const axisKey = makeAxisKey(deviceKey, 2);
    connectionViewMockInstance.props.onSelect(axisKey);
    expect(getLink()).toEqual(
      'https://www.zaber.com/protocol-manual?protocol=ASCII&device=X-MCC2&version=7.13&peripheral=RSW60A-E03T3');
  });

  test('selecting device shows only messages from device and commands with no device', async () => {
    const connection = monitorConnections[0];
    const deviceKeys = _.keys(selectDevices(TestTerminal.testStore.getState()));

    connection.mockRequest('/home');
    connection.mockResponse('@1 1 OK BUSY -- 0');
    connection.mockResponse('@1 2 OK BUSY -- 0');
    connection.mockResponse('@2 1 OK BUSY -- 0');
    connection.mockResponse('@2 2 OK BUSY -- 0');
    await waitUntilPass(() => {
      wrapper.getByText('/home');
      wrapper.getByText('@1 1 OK BUSY -- 0');
      wrapper.getByText('@1 2 OK BUSY -- 0');
      wrapper.getByText('@2 1 OK BUSY -- 0');
      wrapper.getByText('@2 2 OK BUSY -- 0');
    });

    connectionViewMockInstance.props.onSelect(deviceKeys[0]);

    await waitUntilPass(() => {
      wrapper.getByText('/home');
      wrapper.getByText('@1 1 OK BUSY -- 0');
      wrapper.getByText('@1 2 OK BUSY -- 0');
    });

    expect(wrapper.queryByText('@2 1 OK BUSY -- 0')).toBeNull();
    expect(wrapper.queryByText('@2 2 OK BUSY -- 0')).toBeNull();
  });

  test('selecting axis shows only messages from axis, messages from device with no axis, and commands with no device', async () => {
    const connection = monitorConnections[0];
    const axisKeys = _.keys(selectAxes(TestTerminal.testStore.getState()));

    connection.mockRequest('/home');
    connection.mockResponse('@1 1 OK BUSY -- 0');
    connection.mockResponse('@1 2 OK BUSY -- 0');
    connection.mockResponse('@2 1 OK BUSY -- 0');
    connection.mockResponse('@2 2 OK BUSY -- 0');
    connection.mockRequest('/get get.settings.max');
    connection.mockResponse('@1 0 OK IDLE -- 12');
    connection.mockResponse('@2 0 OK IDLE -- 14');
    await waitUntilPass(() => {
      wrapper.getByText('/home');
      wrapper.getByText('@1 1 OK BUSY -- 0');
      wrapper.getByText('@1 2 OK BUSY -- 0');
      wrapper.getByText('@2 1 OK BUSY -- 0');
      wrapper.getByText('@2 2 OK BUSY -- 0');
      wrapper.getByText('/get get.settings.max');
      wrapper.getByText('@1 0 OK IDLE -- 12');
      wrapper.getByText('@2 0 OK IDLE -- 14');
    });

    connectionViewMockInstance.props.onSelect(axisKeys[1]);

    await waitUntilPass(() => {
      wrapper.getByText('/home');
      wrapper.getByText('@1 2 OK BUSY -- 0');
      wrapper.getByText('/get get.settings.max');
      wrapper.getByText('@1 0 OK IDLE -- 12');
    });

    expect(wrapper.queryByText('@1 1 OK BUSY -- 0')).toBeNull();
    expect(wrapper.queryByText('@2 1 OK BUSY -- 0')).toBeNull();
    expect(wrapper.queryByText('@2 2 OK BUSY -- 0')).toBeNull();
    expect(wrapper.queryByText('@2 0 OK IDLE -- 14')).toBeNull();
  });

  test('select device shows device prefix', async () => {
    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    await waitUntilPass(() =>
      expect(wrapper.getByTestId('command-prefix')).toHaveTextContent('/1'));
  });

  test('select axis shows axis and device prefix', async () => {
    const axis = _.sample(selectAxes(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitForConnected();

    await waitUntilPass(() =>
      expect(wrapper.getByTestId('command-prefix')).toHaveTextContent(`/1 ${axis.axisNumber}`));
  });

  test('typing slash when connection selected sends only command', async () => {
    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: '/get pos' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() =>
      expect(connections[0].genCmdNoResp).toHaveBeenLastCalledWith('get pos'));
  });

  test('typing device number when connection selected sends command to device', async () => {
    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: '1 get pos' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() =>
      expect(connections[0].genCmdNoResp).toHaveBeenLastCalledWith('get pos', { device: 1 }));
  });

  test('typing device and axis number when connection selected sends command to axis', async () => {
    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: '1 2 get pos' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() =>
      expect(connections[0].genCmdNoResp).toHaveBeenLastCalledWith('get pos', { device: 1, axis: 2 }));
  });

  test('typing axis number when device selected sends command to axis', async () => {
    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: '3 get pos' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() =>
      expect(devices[0].genericCommandNoResponse).toHaveBeenLastCalledWith('get pos', { axis: 3 }));
  });
});

describe('Connection state', () => {
  test('Connection state is "Disconnected" if connection can not be established', async () => {
    failToConnectToNextConnection = true;
    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitUntilPass(() => wrapper.getByText(/Disconnected/));
  });

  test('Connection state becomes "Disconnected" if connection gets lost', async () => {
    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitForConnected();

    monitorConnections[0].mockError();
    await waitUntilPass(() => wrapper.getByText(/Disconnected/));
  });

  test('User can click on "Reconnect" button to try reconnecting if connection state is "Disconnected', async () => {
    failToConnectToNextConnection = true;
    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitUntilPass(() => wrapper.getByText(/Disconnected/));

    failToConnectToNextConnection = false;
    fireEvent.click(wrapper.getByText(/Reconnect/));
    await waitForConnected();
  });

  test('Terminal input and send button are disabled when connection state is not "Connected"', async () => {
    failToConnectToNextConnection = true;
    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitUntilPass(() => wrapper.getByText(/Disconnected/));

    expect(wrapper.getByTitle('Terminal input')).toBeDisabled();
    expect(wrapper.getByTitle('Send')).toHaveAttribute('aria-disabled', 'true');
  });
});

describe('Command suggestion', () => {
  let terminalInput: HTMLInputElement;

  function getSuggestion(text: string) {
    return wrapper.getByRole('button', { name: (_, element) => element.textContent?.trim() === text });
  }

  beforeEach(async () => {
    mockSingleDevice(TestTerminal.testStore, device => {
      device.product!.commandTree = {
        command: '',
        nodes: [
          {
            command: 'renumber',
            is_command: true,
            nodes: [{
              command: 'number',
              is_command: true,
              is_param: true,
              param_type: 'Uint32',
              param_arity: 1,
            }]
          },
          {
            command: 'reg',
            nodes: [{
              command: 'info',
              nodes: [{
                command: 'dump',
                is_command: true,
              }]
            }]
          },
          {
            command: 'move',
            nodes: [{
              command: 'rel',
              nodes: [{
                command: 'position',
                is_command: true,
                is_param: true,
                param_arity: 1,
                contextual_dimension_id: 1,
              }]
            }]
          }
        ]
      } as ProductInfo['commandTree'];
      device.product!.settings.rows = [
        {
          name: 'limit.max',
          param_type: 'Uint',
          visibility: 'advanced',
          decimal_places: 0,
        }, {
          name: 'limit.min',
          param_type: 'Uint',
          visibility: 'advanced',
          decimal_places: 0,
        },
      ];
      device.product!.conversionTable.rows = [
        {
          contextual_dimension_id: 1,
          dimension_name: 'Length',
          contextual_dimension_name: '',
          function_name: '',
          scale: null
        }
      ];
      return device;
    });
    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitForConnected();
    terminalInput = getTerminalInput();
  });

  test('Shows possible commands based on input', async () => {
    fireEvent.change(terminalInput, { target: { value: 're' } });
    await waitUntilPass(() => {
      getSuggestion('renumber');
      getSuggestion('renumber <number>');
      getSuggestion('reg info dump');
    });
  });

  test('Clicking a command suggestion fills in the rest of the command', async () => {
    fireEvent.change(terminalInput, { target: { value: 'renum' } });
    fireEvent.click(getSuggestion('renumber <number>'));
    expect(terminalInput.value).toEqual('renumber <number>');
  });

  test('Clicking a command suggestion adds the correct amount of space', async () => {
    fireEvent.change(terminalInput, { target: { value: 'renumber' } });
    fireEvent.click(getSuggestion('renumber <number>'));
    expect(terminalInput.value).toEqual('renumber <number>');

    fireEvent.change(terminalInput, { target: { value: 'renumber ' } });
    fireEvent.click(getSuggestion('renumber <number>'));
    expect(terminalInput.value).toEqual('renumber <number>');

    fireEvent.change(terminalInput, { target: { value: 'renumber  ' } });
    fireEvent.click(getSuggestion('renumber <number>'));
    expect(terminalInput.value).toEqual('renumber <number>');
  });

  test('Pressing the tab key fills in the next command as much as possible until next separator', async () => {
    fireEvent.change(terminalInput, { target: { value: 'renu' } });
    fireEvent.keyDown(terminalInput, { key: 'Tab' });
    expect(terminalInput.value).toEqual('renumber');

    fireEvent.change(terminalInput, { target: { value: 'r' } });
    fireEvent.keyDown(terminalInput, { key: 'Tab' });
    expect(terminalInput.value).toEqual('re');

    fireEvent.change(terminalInput, { target: { value: 'get lim' } });
    fireEvent.keyDown(terminalInput, { key: 'Tab' });
    expect(terminalInput.value).toEqual('get limit.m');
    fireEvent.change(terminalInput, { target: { value: 'get limit.ma' } });
    fireEvent.keyDown(terminalInput, { key: 'Tab' });
    expect(terminalInput.value).toEqual('get limit.max');
  });

  test('Using arrow keys allows suggestion selection', async () => {
    fireEvent.change(terminalInput, { target: { value: 're' } });
    fireEvent.keyDown(terminalInput, { key: 'ArrowUp' });
    fireEvent.keyDown(terminalInput, { key: 'Enter' });
    expect(terminalInput.value).toEqual('renumber');

    fireEvent.change(terminalInput, { target: { value: 'reg' } });
    fireEvent.keyDown(terminalInput, { key: 'ArrowUp' });
    fireEvent.keyDown(terminalInput, { key: 'Enter' });
    expect(terminalInput.value).toEqual('reg info');
    fireEvent.keyDown(terminalInput, { key: 'ArrowUp' });
    fireEvent.keyDown(terminalInput, { key: 'Enter' });
    expect(terminalInput.value).toEqual('reg info dump');
  });

  test('Shows possible units', async () => {
    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    terminalInput = getTerminalInput();

    fireEvent.change(terminalInput, { target: { value: 'move rel 2' } });
    await waitUntilPass(() => {
      getSuggestion('move rel 2');
      getSuggestion('move rel 2mm');
      getSuggestion('move rel 2cm');
    });

    fireEvent.change(terminalInput, { target: { value: 'move rel -3' } });
    await waitUntilPass(() => {
      getSuggestion('move rel -3');
      getSuggestion('move rel -3mm');
      getSuggestion('move rel -3cm');
    });
  });

  test('Shows note next to unit suggestions', async () => {
    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    terminalInput = getTerminalInput();

    fireEvent.change(terminalInput, { target: { value: 'move rel 2' } });
    await waitUntilPass(() => {
      expect(wrapper.getAllByText('Unit conversion will apply').length).not.toEqual(0);
    });
  });

  test('does not crash on empty suggestion', async () => {
    fireEvent.change(terminalInput, { target: { value: 'bla' } });
    fireEvent.keyDown(terminalInput, { key: 'ArrowUp' });
  });
});

describe('command batch', () => {
  beforeEach(async () => {
    mockSingleDeviceWithPeripherals(TestTerminal.testStore);

    const connectionData = _.sample(selectConnections(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connectionData.key);
    await waitForConnected();
  });

  function pasteCommands(commands: string) {
    const terminalInput = getTerminalInput();
    fireEvent.paste(terminalInput, {
      clipboardData: {
        getData() {
          return commands;
        },
      },
    });
  }

  function getCommandsAndReplies() {
    const commandBatch = wrapper.getByTestId('command-batch');
    const text: string[] = [];
    const explore = (el: Element) => {
      if (el.children.length === 0) {
        const content = el.textContent?.trim();
        if (content) {
          text.push(content);
        }
      }
      return Array.from(el.children).forEach(explore);
    };
    explore(commandBatch);
    return text;
  }

  test('runs a batch of commands displaying replies', async () => {
    connectionCallback = connection => {
      let replyIndex = 0;
      connection.genericCommand.mockImplementation(async () => {
        switch (replyIndex++) {
          case 0:
            return ({ ...DEFAULT_REPLY, replyFlag: 'RJ', data: 'BADCOMMAND' });
          case 1:
            throw new RequestTimeoutException('timeout');
        }
        return DEFAULT_REPLY;
      });
      connection.genericCommandMultiResponse.mockImplementation(async () =>
        [DEFAULT_REPLY, { ...DEFAULT_REPLY, deviceAddress: 2 }]);
    };

    pasteCommands(`
      whatever
      /1 set maxspeed 30
      /2 3 set accel 10
    `);
    fireEvent.click(wrapper.getByLabelText(/Stop on error/));

    fireEvent.click(wrapper.getByText('Execute'));

    await waitUntilPass(() => wrapper.getByText('Close'));
    expect(connections[0].genericCommand.mock.calls).toEqual([
      ['set maxspeed 30', { device: 1, checkErrors: false }],
      ['set accel 10', { device: 2, axis: 3, checkErrors: false }],
    ]);
    expect(connections[0].genericCommandMultiResponse.mock.calls).toEqual([
      ['whatever', { checkErrors: false }],
    ]);

    expect(getCommandsAndReplies()).toEqual([
      'whatever',
      '@01 0 OK IDLE -- 0',
      '@02 0 OK IDLE -- 0',
      '/1 set maxspeed 30',
      '@01 0 RJ IDLE -- BADCOMMAND',
      '/2 3 set accel 10',
      'TIMEOUT'
    ]);
  });

  test('adds prefix when device/axis is selected', async () => {
    const axis = _.sample(selectAxes(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);

    pasteCommands(`
      whatever
      /2 3 set accel 10
    `);

    fireEvent.click(wrapper.getByText('Execute'));
    await waitUntilPass(() => wrapper.getByText('Close'));

    expect(connections[0].genericCommand.mock.calls).toEqual([
      ['whatever', { device: 1, axis: axis.axisNumber, checkErrors: false }],
      ['set accel 10', { device: 2, axis: 3, checkErrors: false }],
    ]);
  });

  test('reruns the commands', async () => {
    pasteCommands(`
      cmd 1
      cmd 2
    `);

    fireEvent.click(wrapper.getByText('Execute'));
    await waitUntilPass(() => wrapper.getByText('Close'));
    expect(connections[0].genericCommandMultiResponse).toHaveBeenCalledTimes(2);
    fireEvent.click(wrapper.getByText('Execute Again'));
    await waitUntilPass(() => wrapper.getByText('Close'));
    expect(connections[0].genericCommandMultiResponse).toHaveBeenCalledTimes(4);
  });

  test('stops the execution', async () => {
    connectionCallback = connection => {
      let replyIndex = 0;
      connection.genericCommandMultiResponse.mockImplementation(async () => {
        if (replyIndex++ === 1) {
          fireEvent.click(wrapper.getByText('Stop'));
        }
        return [DEFAULT_REPLY];
      });
    };

    pasteCommands(`
      cmd 1
      cmd 2
      cmd 3
    `);

    fireEvent.click(wrapper.getByText('Execute'));
    await waitUntilPass(() => wrapper.getByText('Close'));

    expect(connections[0].genericCommandMultiResponse).toHaveBeenCalledTimes(2);
    expect(getCommandsAndReplies()).toEqual([
      'cmd 1',
      '@01 0 OK IDLE -- 0',
      'cmd 2',
      'cmd 3',
    ]);
  });

  test('waits for IDLE after every command', async () => {
    connectionCallback = connection => {
      connection.genericCommandMultiResponse.mockImplementation(async cmd => ([{
        ...DEFAULT_REPLY,
        status: cmd.match(/home|move/) ? 'BUSY' : 'IDLE',
        deviceAddress: 13,
      }]));
    };

    pasteCommands(`
      home
      warnings clear
      move abs 10
      warnings clear
      move abs 5
    `);

    fireEvent.click(wrapper.getByText('Execute'));
    await waitUntilPass(() => wrapper.getByText('Close'));

    expect(connections[0].genericCommandMultiResponse).toHaveBeenCalledTimes(5);
    expect(connections[0].getDevice(13).allAxes.isBusy).toHaveBeenCalledTimes(3);
  });

  test('does not wait for IDLE if unchecked', async () => {
    connectionCallback = connection => {
      connection.genericCommand.mockImplementation(async () => ({
        ...DEFAULT_REPLY,
        status: 'BUSY',
      }));
    };

    pasteCommands(`
      /1 cmd 1
      /1 cmd 2
    `);

    fireEvent.click(wrapper.getByLabelText(/Wait until IDLE/));

    fireEvent.click(wrapper.getByText('Execute'));
    await waitUntilPass(() => wrapper.getByText('Close'));

    expect(connections[0].getDevice(1).allAxes.isBusy).toHaveBeenCalledTimes(0);
  });

  test('stops on error', async () => {
    connectionCallback = connection => {
      let replyIndex = 0;
      connection.genericCommand.mockImplementation(async () => {
        if (replyIndex++ === 1) {
          return ({ ...DEFAULT_REPLY, replyFlag: 'RJ', data: 'BADCOMMAND' });
        }
        return DEFAULT_REPLY;
      });
    };

    pasteCommands(`
      /1 set maxspeed 30
      /1 whatever
      /1 home
    `);

    fireEvent.click(wrapper.getByText('Execute'));

    await waitUntilPass(() => wrapper.getByText('Close'));
    expect(connections[0].genericCommand).toHaveBeenCalledTimes(2);

    expect(getCommandsAndReplies()).toEqual([
      '/1 set maxspeed 30',
      '@01 0 OK IDLE -- 0',
      '/1 whatever',
      '@01 0 RJ IDLE -- BADCOMMAND',
      '/1 home',
    ]);
  });

  test('displays error', async () => {
    connectionCallback = connection => {
      connection.genericCommandMultiResponse.mockRejectedValueOnce(new Error('Unexpected error'));
    };
    pasteCommands(`
      cmd 1
      cmd 2
    `);
    fireEvent.click(wrapper.getByText('Execute'));
    await waitUntilPass(() => wrapper.getByText('Unexpected error'));
  });

  test('edits the commands', async () => {
    fireEvent.click(wrapper.getByTitle('Terminal Actions'));
    fireEvent.click(wrapper.getByTitle('Command Batch'));

    fireEvent.change(wrapper.getAllByRole('textbox')[1], { target: { value: 'cmd 1\ncmd 2' } });
    fireEvent.click(wrapper.getByText('Execute'));

    await waitUntilPass(() => wrapper.getByText('Close'));
    expect(connections[0].genericCommandMultiResponse.mock.calls).toEqual([
      ['cmd 1', { checkErrors: false }],
      ['cmd 2', { checkErrors: false }],
    ]);
    connections[0].genericCommandMultiResponse.mockClear();

    fireEvent.click(wrapper.getByText('Edit'));

    fireEvent.change(wrapper.getAllByRole('textbox')[1], { target: { value: 'cmd 3\ncmd 4' } });
    fireEvent.click(wrapper.getByText('Execute'));

    await waitUntilPass(() => wrapper.getByText('Close'));
    expect(connections[0].genericCommandMultiResponse.mock.calls).toEqual([
      ['cmd 3', { checkErrors: false }],
      ['cmd 4', { checkErrors: false }],
    ]);
  });
});

describe('Unit conversion', () => {
  test('Valid unit conversion in command input will show note to user', async () => {
    mockSingleDevice(TestTerminal.testStore, device => {
      device.product!.commandTree = {
        command: '',
        nodes: [
          {
            command: 'move',
            nodes: [{
              command: 'rel',
              nodes: [{
                command: 'position',
                is_command: true,
                is_param: true,
                param_arity: 1,
                contextual_dimension_id: 1,
                nodes: [{
                  command: 'speed',
                  is_command: true,
                  is_param: true,
                  param_arity: 1,
                }]
              }]
            }]
          }
        ]
      } as ProductInfo['commandTree'];
      device.product!.conversionTable.rows = [
        {
          contextual_dimension_id: 1,
          dimension_name: 'Length',
          contextual_dimension_name: '',
          function_name: '',
          scale: null
        }
      ];
      return device;
    });

    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    const terminalInput = getTerminalInput();

    fireEvent.change(terminalInput, { target: { value: 'move rel 2mm 3' } });
    await waitUntilPass(() => {
      wrapper.getByText('Unit conversion will apply');
    });
  });

  test('converts units for device', async () => {
    mockSingleDevice(TestTerminal.testStore, device => {
      device.product!.commandTree = {
        command: '',
        nodes: [
          {
            command: 'move',
            nodes: [{
              command: 'rel',
              nodes: [{
                command: 'position',
                is_command: true,
                is_param: true,
                param_arity: 1,
                contextual_dimension_id: 1,
              }]
            }]
          }
        ]
      } as ProductInfo['commandTree'];
      device.product!.conversionTable.rows = [
        {
          contextual_dimension_id: 1,
          dimension_name: 'Length',
          contextual_dimension_name: '',
          function_name: '',
          scale: null
        }
      ];
      return device;
    });

    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: 'move rel 2mm' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() =>
      expect(devices[0].genericCommandNoResponse).toHaveBeenLastCalledWith('move rel 200'));
  });

  test('converts negative units for device', async () => {
    mockSingleDevice(TestTerminal.testStore, device => {
      device.product!.commandTree = {
        command: '',
        nodes: [
          {
            command: 'move',
            nodes: [{
              command: 'rel',
              nodes: [{
                command: 'position',
                is_command: true,
                is_param: true,
                param_arity: 1,
                contextual_dimension_id: 1,
              }]
            }]
          }
        ]
      } as ProductInfo['commandTree'];
      device.product!.conversionTable.rows = [
        {
          contextual_dimension_id: 1,
          dimension_name: 'Length',
          contextual_dimension_name: '',
          function_name: '',
          scale: null
        }
      ];
      return device;
    });

    const device = _.sample(selectDevices(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitForConnected();

    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: 'move rel -5mm' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() =>
      expect(devices[0].genericCommandNoResponse).toHaveBeenLastCalledWith('move rel -500'));
  });

  test('converts units for axis', async () => {
    mockSingleDeviceWithPeripherals(TestTerminal.testStore, {
      axisModifier: axis => {
      axis.product!.commandTree = {
        command: '',
        nodes: [
          {
            command: 'move',
            nodes: [{
              command: 'rel',
              nodes: [{
                command: 'position',
                is_command: true,
                is_param: true,
                param_arity: 1,
                contextual_dimension_id: 1,
              }]
            }]
          }
        ]
      } as ProductInfo['commandTree'];
      axis.product!.conversionTable.rows = [
        {
          contextual_dimension_id: 1,
          dimension_name: 'Length',
          contextual_dimension_name: '',
          function_name: '',
          scale: null
        }
      ];
      return axis;
      }
    });

    const axis = _.sample(selectAxes(TestTerminal.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitForConnected();

    const terminalInput = getTerminalInput();
    const sendButton = wrapper.getByTitle('Send');

    fireEvent.change(terminalInput, { target: { value: 'move rel 2cm' } });
    fireEvent.click(sendButton);
    await waitUntilPass(() =>
      expect(axes[0].genericCommandNoResponse).toHaveBeenLastCalledWith('move rel 200'));
  });
});
