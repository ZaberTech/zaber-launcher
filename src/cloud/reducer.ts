import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import { AuthState, CloudSharingStatus, DiscoveredClient, LoggedInUser, LoginData } from './types';

export interface State {
  authState: AuthState;
  loggedInUser: LoggedInUser | null;

  loginData: LoginData;
  loginError: string | null;
  loginInProgress: boolean;

  sharingStatus: CloudSharingStatus | null;
  sharingActionInProgress: boolean;
  sharingError: string | null;

  discoveredClients: DiscoveredClient[] | null;
  discoveryError: string | null;
}

const defaultLoginData: LoginData = {
  email: '',
  password: '',
};

const initialState: State = {
  authState: AuthState.Checking,
  loggedInUser: null,

  loginData: defaultLoginData,
  loginError: null,
  loginInProgress: false,

  sharingStatus: null,
  sharingActionInProgress: false,
  sharingError: null,

  discoveredClients: null,
  discoveryError: null,
};

const login = (state: State): State =>
  ({
    ...state,
    loginInProgress: true,
    loginError: null,
  });

const loginResult = (state: State, { error, loggedInUser }: ActionsToPayloads[ActionTypes.LOGIN_RESULT]): State =>
  ({
    ...state,
    loginInProgress: false,
    ...(!error ? {
      loginError: null,
      loginData: defaultLoginData,
      authState: AuthState.Authenticated,
      loggedInUser: loggedInUser!,
    } : {
      loginError: error,
      loginData: { ...state.loginData, password: '' },
    })
  });

const updateLoginData = (state: State, data: ActionsToPayloads[ActionTypes.UPDATE_LOGIN_DATA]): State =>
  ({
    ...state,
    loginData: {
      ...state.loginData,
      ...data,
    }
  });

const checkAuthStateResult = (state: State, { loggedInUser }: ActionsToPayloads[ActionTypes.CHECK_AUTH_RESULT]): State =>
  ({
    ...state,
    authState: loggedInUser ? AuthState.Authenticated : AuthState.Unauthenticated,
    loggedInUser,
  });

const logout = (state: State): State =>
  ({
    ...state,
    authState: AuthState.Unauthenticated,
    loggedInUser: null,
  });

const turnCloudSharing = (state: State,  { on }: ActionsToPayloads[ActionTypes.TURN_CLOUD_SHARING]): State =>
  ({
    ...state,
    sharingError: null,
    sharingActionInProgress: true,
    sharingStatus: state.sharingStatus && { ...state.sharingStatus, sharingOn: on },
  });

const turnCloudSharingDone = (state: State, { error }: ActionsToPayloads[ActionTypes.TURN_CLOUD_SHARING_DONE]): State =>
  ({
    ...state,
    sharingError: error ?? null,
    sharingActionInProgress: false,
  });

const cloudSharingStatusResult = (state: State, payload: ActionsToPayloads[ActionTypes.CLOUD_SHARING_STATUS_RESULT]): State =>
  ({
    ...state,
    sharingError: payload.error ?? null,
    sharingStatus: payload.status ?? null,
  });

const resetDiscovery = (state: State): State =>
  ({
    ...state,
    discoveredClients: [],
    discoveryError: null,
  });

const discoveryClientFound = (state: State, client: ActionsToPayloads[ActionTypes.DISCOVERY_CLIENT_FOUND]): State =>
  ({
    ...state,
    discoveredClients: [...(state.discoveredClients ?? []).filter(oldClient => oldClient.id !== client.id), client],
  });

const discoveryError = (state: State, { error }: ActionsToPayloads[ActionTypes.DISCOVERY_ERROR]): State =>
  ({
    ...state,
    discoveryError: error,
  });

export const reducer = createReducer<ActionsToPayloads, State>({
  [ActionTypes.LOGIN]: login,
  [ActionTypes.LOGIN_RESULT]: loginResult,
  [ActionTypes.UPDATE_LOGIN_DATA]: updateLoginData,
  [ActionTypes.CHECK_AUTH_RESULT]: checkAuthStateResult,
  [ActionTypes.LOGOUT]: logout,
  [ActionTypes.TURN_CLOUD_SHARING]: turnCloudSharing,
  [ActionTypes.TURN_CLOUD_SHARING_DONE]: turnCloudSharingDone,
  [ActionTypes.CLOUD_SHARING_STATUS_RESULT]: cloudSharingStatusResult,
  [ActionTypes.STOP_DISCOVERY]: resetDiscovery,
  [ActionTypes.START_DISCOVERY]: resetDiscovery,
  [ActionTypes.DISCOVERY_CLIENT_FOUND]: discoveryClientFound,
  [ActionTypes.DISCOVERY_ERROR]: discoveryError,
}, initialState);
