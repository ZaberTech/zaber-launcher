import { Button, ButtonRow, Icons, Loader, Modal, Text, UnorderedList } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import { match, P } from 'ts-pattern';
import React from 'react';
import { useSelector } from 'react-redux';
import { ModalButtons } from '@zaber/react-library/dist/types/elements/Modal/Modal';

import { selectSaveLoad } from '../store';

import { actions as actionDefinitions } from './actions';
import { LoadCompatibility } from './components/LoadCompatibility';
import { LoadError } from './components/LoadError';
import { selectTarget } from './selectors';
import type { SaveLoadTarget, SaveLoadTargetMaybe, SaveLoadUiState } from './types';
import { SetStateResponseWarnings } from './components/SetStateWarning';

const getSaveLoadModalContent = (
  target: SaveLoadTargetMaybe,
  ui: SaveLoadUiState,
  actions: typeof actionDefinitions,
) =>
  match(target)
    .returnType<{content: React.ReactNode; buttons?: ModalButtons}>()
    .with({ type: 'unidentified' }, () => ({
      content:
      <div className="info-grid">
        <Icons.ErrorFault className="icon error"/>
        <Text t={Text.Type.H4} className="title">Unidentified Device</Text>
        <Text className="info">Cannot load state to an unidentified device.</Text>
      </div>
    }))
    .with({ type: 'not-a-device' }, () => ({
      content:
      <div className="info-grid">
        <Icons.ErrorFault className="icon error"/>
        <Text t={Text.Type.H4} className="title">Not a device</Text>
        <Text className="info">Only integrated devices, axes and controllers can have their state saved.</Text>
      </div>
    }))
    .with({ type: P.not(P.union('unidentified', 'not-a-device')) }, target =>
      match(ui)
        .returnType<{content: React.ReactNode; buttons?: ModalButtons}>()
        .with({ step: 'choose' }, () => {
          const willSave: string[] = [];
          if (target.type === 'axis') {
            willSave.push('Axis Settings');
            willSave.push('Servo Tuning Parameters');
          } else {
            willSave.push('Device Settings');
            willSave.push('Stream Buffers');
            willSave.push('Triggers');
            willSave.push('Servo Tuning Parameters');
            if (target.type === 'controller') {
              willSave.push('The full state of all connected peripherals');
            }
          }
          return {
            content:
          <div className="info-grid">
            <div className="info">
              <Text>
                  Would you like to create a new save file from this product's state,
                  or load a previously created save file to this {target.description}?
              </Text>
              <UnorderedList header={<Text t={Text.Type.H5}>This will save to following:</Text>}>
                {willSave}
              </UnorderedList>
            </div>
          </div>,
            buttons: <ButtonRow>
              <Button onClick={() => actions.beginSave(target as SaveLoadTarget)}>Create Save File</Button>
              <Button onClick={() => actions.beginLoad(target as SaveLoadTarget)}>Load Save File</Button>
              <Button color="grey" onClick={actions.close}>Cancel</Button>
            </ButtonRow>
          };
        })
        .with({ step: 'working' }, ({ message }) => ({
          content:
        <div className="working">
          <Loader size="large"/>
          <Text>{message}</Text>
        </div>
        }))
        .with({ step: 'save-error' }, ({ error }) => ({
          content:
        <div className="info-grid">
          <Icons.ErrorFault className="icon error"/>
          <Text t={Text.Type.H4} className="title">Failed</Text>
          <Text className="info">{error}</Text>
        </div>
        }))
        .with({ step: 'saved' }, () => ({
          content:
        <div className="info-grid">
          <Icons.Confirmation className="icon success"/>
          <Text t={Text.Type.H4} className="title">Completed</Text>
          <div className="info">
            <Text>
          The custom state of {target.name} has
          been successfully saved to a file and can be reloaded to the {target.description} at any time.
            </Text>
            {target.type === 'controller' && <>
              <Text t={Text.Type.H5}>Peripherals Data</Text>
              <Text>
            Data related to peripherals which are currently attached to the controller is included into the file.
            You can restore the custom controller state together with peripherals if you will use the same hardware
            or restore the peripherals states separately.
              </Text>
            </>}
          </div>
        </div>,
          buttons: <Button onClick={actions.close}>Done</Button>
        }))
        .with({ step: 'load-check-result' }, ui => ({
          content: <LoadCompatibility target={target} ui={ui}/>
        }))
        .with({ step: 'load-error' }, ({ error }) => ({
          content: <LoadError target={target} error={error}/>
        }))
        .with({ step: 'loaded' }, ({ context: { fileName }, response }) => ({
          content:
          <div className="info-grid">
            <Icons.Confirmation className="icon success"/>
            <Text t={Text.Type.H4} className="title">Completed</Text>
            <div className="info">
              <Text>
                The custom state from the <Text e={Text.Emphasis.Bold}>{fileName}</Text> file have been successfully applied
                to this <Text e={Text.Emphasis.Bold}>{target.name}</Text> {target.description}.
              </Text>
              <SetStateResponseWarnings
                header="The following exceptions were handled, but prevented the state from being fully loaded:"
                response={response}
              />
            </div>
          </div>,
          buttons: <Button onClick={actions.close}>Done</Button>
        }))
        .with({ step: 'choosing-file' }, () => ({ content: null }))
        .with({ step: 'restored' }, () => ({
          content: <div className="info-grid">
            <Icons.ErrorWarning className="icon warning"/>
            <Text t={Text.Type.H4} className="title">Factory Reset</Text>
            <div className="info">
              <Text>
                The loading of the state failed but the {target.description} has
                been factory reset.
              </Text>
            </div>
          </div>,
          buttons: <Button onClick={actions.close}>Done</Button>
        }))
        .with({ step: 'restore-error' }, ({ error }) => ({
          content: <div className="info-grid">
            <Icons.ErrorFault className="icon error"/>
            <Text t={Text.Type.H4} className="title">Failed</Text>
            <div className="info">
              <Text>Could not factory reset: {error}</Text>
            </div>
          </div>,
        }))
        .exhaustive()
    )
    .exhaustive();

export const SaveLoadModal: React.FC = () => {
  const actions = useActions(actionDefinitions);

  const target = useSelector(selectTarget);
  const ui = useSelector(selectSaveLoad).ui;

  if (ui == null || target == null) {
    return null;
  } else if (ui.step === 'choosing-file') {
    return <div className="modal-background-overlay"/>;
  }

  const { content, buttons } = getSaveLoadModalContent(target, ui, actions);

  return <Modal
    className="save-load"
    headerIcon={<Icons.SaveLoad/>}
    headerText="Save/Load State"
    onRequestClose={actions.close}
    buttons={buttons}
  >
    {content}
  </Modal>;
};
