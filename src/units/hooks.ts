import { useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { match, P } from 'ts-pattern';
import { createSelector } from 'reselect';

import { selectUnits } from '../store';
import { tryAccess, useActions } from '../utils';

import { actionDefinitions } from './actions';
import type { UnitFrom, UnitInfo } from './types';
import { getUnitInfoOfDimension } from './utils';

function getKey(unit: UnitFrom): string {
  return match<UnitFrom>(unit)
    .with({ setting: P.string }, ({ setting, entity }) => `${entity}:${setting}`)
    .with({ command: P.string }, ({ command, entity }) => `${entity}:${command}`)
    .with({ dimension: P.string }, ({ dimension }) => dimension)
    .with({ info: P._ }, ({ info }) => JSON.stringify(info))
    .exhaustive();
}

type Selector = (state: ReturnType<typeof selectUnits>) => UnitInfo | null;

/**
 *  Gets information to use converting units. If that information isn't stored yet, this hook automatically fetches it.
 * @param entityKey The device or axis that this info describes.
 * @param unit What property to get the unit int from. This can be a setting name (eg. 'accel') or a dimension name (eg. 'Inertia')
 * @returns Unit info
 */
export function useUnitInfo(unit: UnitFrom): UnitInfo | null {
  const actions = useActions(actionDefinitions);

  const unitKey = getKey(unit);

  const selector = useMemo(() => {
    const selector =
      match<UnitFrom, Selector>(unit)
        .with({ setting: P.string }, ({ setting, entity }) => state => tryAccess(state.info, entity)?.settings[setting] ?? null)
        .with({ command: P.string }, ({ command, entity }) => state => tryAccess(state.info, entity)?.commands[command] ?? null)
        .with({ dimension: P.string }, ({ dimension }) => () => getUnitInfoOfDimension(dimension))
        .with({ info: P._ }, ({ info }) => () => info)
        .exhaustive();
    return createSelector(selectUnits, selector);
  }, [unitKey]);

  const cachedUnitInfo = useSelector(selector);

  useEffect(() => {
    if ('setting' in unit && cachedUnitInfo == null && unit.entity != null) {
      actions.fetchSettingUnitInfo(unit.entity, unit.setting);
    }
    if ('command' in unit && cachedUnitInfo == null && unit.entity != null) {
      actions.fetchCommandUnitInfo(unit.entity, unit.command);
    }
  }, [cachedUnitInfo, unitKey]);

  return cachedUnitInfo;
}
