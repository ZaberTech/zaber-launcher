import { Button, Icons } from '@zaber/react-library';
import classNames from 'classnames';
import React, { Component, createRef } from 'react';

import { environment } from '../environment';
import { omit } from '../utils';

interface RenderContext {
  scrollToBottom: (behavior?: ScrollBehavior, force?: boolean) => void;
}

interface Props extends Omit<
  React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
  'ref' | 'onWheel' | 'onScroll' | 'children'> {
  /** Scrolls to the bottom when the key changes. */
  scrollKey?: string | number | null;
  children?: (ctx: RenderContext) => React.ReactNode;
}

interface State {
  sticky: boolean;
}

/**
 * Keeps the scroll at bottom during resizing of the element.
 */
export class StickyScrollDiv extends Component<Props, State> {
  private containerRef = createRef<HTMLInputElement>();
  private bottomRef = createRef<HTMLDivElement>();

  private observer?: ResizeObserver;

  private lastScrollTop: number = 0;
  private scrollingToBottom: boolean = false;

  get container() {
    return this.containerRef.current!;
  }

  readonly renderContext: RenderContext;

  constructor(props: Props) {
    super(props);
    this.state = {
      sticky: true
    };
    this.renderContext = {
      scrollToBottom: (behavior, force) => {
        if (this.state.sticky || force) {
          this.scrollToBottom(behavior);
        }
      },
    };
  }

  componentDidMount() {
    if (environment.isTest && typeof ResizeObserver === 'undefined') {
      return;
    }
    this.observer = new ResizeObserver(this.onResize);
    this.observer.observe(this.container);

    this.scrollToBottom('instant');
  }

  componentWillUnmount() {
    this.observer?.disconnect();
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    const { scrollKey } = this.props;
    const { sticky } = this.state;
    if (sticky && (!prevState.sticky || prevProps.scrollKey !== scrollKey)) {
      this.scrollToBottom();
    }
  }

  onResize: ResizeObserverCallback = () => {
    const { sticky } = this.state;
    if (sticky) {
      this.scrollToBottom('instant');
    }
  };

  scrollToBottom = (behavior: ScrollBehavior = 'smooth') => {
    if (behavior === 'smooth') {
      this.scrollingToBottom = true;
    }
    this.bottomRef.current?.scrollIntoView?.({ behavior });
  };

  onWheel = (e: React.WheelEvent<HTMLDivElement>) => {
    const { sticky } = this.state;
    const { scrollHeight, clientHeight } = this.container;
    const needScroll = scrollHeight > clientHeight;
    if (e.deltaY < 0 && sticky && needScroll) {
      this.setState({ sticky: false });
    }
  };

  onScroll = () => {
    const { sticky } = this.state;
    const { scrollHeight, clientHeight, scrollTop } = this.container;
    const isAtBottom = scrollTop + clientHeight + 1 >= scrollHeight;
    const ascending = scrollTop < this.lastScrollTop;
    if (sticky && !isAtBottom && ascending && !this.scrollingToBottom) {
      this.setState({ sticky: false });
    } else if (isAtBottom) {
      this.setState({ sticky: true });
      this.scrollingToBottom = false;
    }
    this.lastScrollTop = scrollTop;
  };

  render(): React.ReactNode {
    const { className, children, ...rest } = this.props;
    const { sticky } = this.state;
    return <div
      ref={this.containerRef}
      className={classNames('sticky-scroll-div', className)}
      onWheel={this.onWheel}
      onScroll={this.onScroll}
      {...omit(rest, 'scrollKey')}
    >
      {children?.(this.renderContext)}

      <div ref={this.bottomRef}/>

      <div className="go-to-bottom">
        <Button className={classNames({ hidden: sticky })} color="grey"
          onClick={() => this.setState({ sticky: true })}>
          <Icons.ArrowExpanded/>Bottom
        </Button>
      </div>
    </div>;
  }
}
