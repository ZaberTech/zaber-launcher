import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';
import { LinkClasses } from '@zaber/react-library';

import { openExternalLink } from '../desktop';

type AnchorProps = React.DetailedHTMLProps<React.HTMLAttributes<HTMLButtonElement>, HTMLButtonElement>;

interface Props extends Omit<AnchorProps, 'onClick' | 'href'> {
  url: string;
}

export const ExternalLink: React.FC<Props> = ({ url, className, ...props }) => (
  <button className={classNames(className, LinkClasses.Default, 'external-link')}
    data-link={url}
    onClick={() => openExternalLink(url)}
    {..._.omit(props, 'onClick')}
  />
);

export const NewWindowLink: React.FC<Props> = ({ url, className, ...props }) => {
  const openApp = (appUrl: string) => {
    const newUrl = window.location.href.replace(/#.*$/, `#${appUrl}`);
    window.open(newUrl);
  };

  return (<button className={classNames(className, LinkClasses.Default, 'external-link')}
    onClick={() => openApp(url)}
    {..._.omit(props, 'onClick')}
  />);
};
