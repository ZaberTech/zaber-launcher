import type { CommandRejectedReasonManual, ManualProfilingData } from '../types';
import noDocs from '../no-docs.html';
import noDocsFw from '../no-docs-fw.html';

import { injectLinks, parsedManual, pointImgToSource, profileElement, stripZaber } from './profile';

export function generateCommandRejectedReasonManual(reason: string, { firmwareVersion, baseUrl, includes }: ManualProfilingData)
  : CommandRejectedReasonManual {
  const body = parsedManual();
  const name = `Command Rejected - ${reason}`;
  if (firmwareVersion.major < 7) {
    return { html: noDocsFw, name };
  }
  const selector = `#topic_message_format_replies__topic_message_format_replies_rj_${reason}`;
  let articleInBody;
  try {
    articleInBody = body.querySelector(selector);
  } catch {
    articleInBody = null;
  }
  if (!articleInBody) {
    return { html: noDocs, name };
  }

  if (articleInBody.tagName === 'DT' && articleInBody.nextElementSibling?.tagName === 'DD') {
    articleInBody = articleInBody.nextElementSibling;
  }

  const article = articleInBody.cloneNode(true) as HTMLElement;
  profileElement(article, includes);
  injectLinks(article, baseUrl);
  pointImgToSource(article);
  stripZaber(article);

  return { html: article.outerHTML, name };
}
