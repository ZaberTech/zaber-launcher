import React from 'react';
import { Toast } from '@zaber/react-library';
import type { BrowserWindow } from 'electron';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';
import { environment } from '../environment';

import { actions as actionsDefinitions } from './actions';
import { selectFullscreenNoticeVisibility } from './selectors';

interface Props {
  window: BrowserWindow | null;
}

export const FullscreenNotice: React.FC<Props> = ({ window }) => {
  const actions = useActions(actionsDefinitions);
  const showFullscreenNotice = useSelector(selectFullscreenNoticeVisibility);

  window?.on('enter-full-screen', () => {
    actions.updateFullscreenNoticeVisibility(true);
  });

  window?.on('leave-full-screen', () => {
    actions.updateFullscreenNoticeVisibility(false);
  });

  return showFullscreenNotice ?
    <Toast type="info" duration={5000} onClose={() => actions.updateFullscreenNoticeVisibility(false)}>
      Press {environment.platform === 'darwin' ? '[fn-F]' : '[F11]'} to exit full screen
    </Toast>
    :
    null;
};
