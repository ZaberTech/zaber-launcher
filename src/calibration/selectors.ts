import _ from 'lodash';
import { createSelector } from 'reselect';

import { selectCalibration } from '../store';
import {
  selectIdentifiedAxes as selectManagerAxes,
  selectIdentifiedDevices as selectManagerDevices,
  IdentifiedDeviceState,
  IdentifiedAxisState,
} from '../connection_manager';
import { extractDeviceKey, getEntityType, EntityKeyType, makeAxisKey, EntityKey } from '../keys';
import { tryAccess } from '../utils';

import { fromEditedTable } from './types';

export interface SelectedDevice {
  selected: EntityKey;
  axisKey: EntityKey;
  device: IdentifiedDeviceState;
  axis?: IdentifiedAxisState;
}

export const selectDevice = createSelector(selectCalibration, selectManagerAxes, selectManagerDevices,
  (state, axes, devices): SelectedDevice | null => {
    const key = state.selected;
    if (!key) { return null }

    const device = tryAccess(devices, extractDeviceKey(key));
    if (!device) { return null }

    const axis = tryAccess(axes, key);
    if (axis == null && getEntityType(key) === EntityKeyType.AXIS) { return null }

    return ({
      device,
      axis,
      axisKey: axis?.key ?? makeAxisKey(device.key, 1),
      selected: key,
    });
  }
);

export const selectEditedTable = createSelector(selectCalibration, state => state.editedTable);
export const selectConvertedTable = createSelector(selectEditedTable, table => fromEditedTable(table));
export const selectEditValidation = createSelector(selectConvertedTable,
  table => 'error' in table ? table.error : null);
export const selectState = createSelector(selectCalibration, state => state);
export const selectIsSynced = createSelector(selectConvertedTable, selectCalibration, (convertedTable, state) => {
  if (state.table?.state !== 'loaded' || 'error' in convertedTable) { return false }
  return _.isEqual(convertedTable, state.table.table);
});
