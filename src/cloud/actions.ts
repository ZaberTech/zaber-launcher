import { actionBuilder } from '../utils';

import type { CloudSharingStatus, DiscoveredClient, LoggedInUser, LoginData } from './types';

export enum ActionTypes {
  LOGIN = 'CLOUD_LOGIN',
  LOGIN_RESULT = 'CLOUD_LOGIN_RESULT',
  UPDATE_LOGIN_DATA = 'CLOUD_UPDATE_LOGIN_DATA',
  CHECK_AUTH_RESULT = 'CLOUD_CHECK_AUTH_RESULT',
  CHECK_AUTH_NOW = 'CLOUD_CHECK_AUTH_AGAIN_NOW',
  LOGOUT = 'CLOUD_LOGOUT',

  TURN_CLOUD_SHARING = 'TURN_CLOUD_SHARING',
  TURN_CLOUD_SHARING_DONE = 'TURN_CLOUD_SHARING_DONE',
  CLOUD_SHARING_STATUS = 'CLOUD_SHARING_STATUS',
  CLOUD_SHARING_STATUS_RESULT = 'CLOUD_SHARING_STATUS_RESULT',

  START_DISCOVERY = 'CLOUD_START_DISCOVERY',
  STOP_DISCOVERY = 'CLOUD_STOP_DISCOVERY',
  DISCOVERY_CLIENT_FOUND = 'CLOUD_DISCOVERY_CLIENT_FOUND',
  DISCOVERY_ERROR = 'CLOUD_DISCOVERY_ERROR',
}

export interface ActionsToPayloads {
  [ActionTypes.LOGIN]: void;
  [ActionTypes.LOGIN_RESULT]: { error?: string; loggedInUser?: LoggedInUser };
  [ActionTypes.UPDATE_LOGIN_DATA]: Partial<LoginData>;
  [ActionTypes.CHECK_AUTH_RESULT]: { loggedInUser: LoggedInUser | null };
  [ActionTypes.CHECK_AUTH_NOW]: void;
  [ActionTypes.LOGOUT]: void;
  [ActionTypes.TURN_CLOUD_SHARING]: { on: boolean };
  [ActionTypes.TURN_CLOUD_SHARING_DONE]: { error?: string };
  [ActionTypes.CLOUD_SHARING_STATUS]: void;
  [ActionTypes.CLOUD_SHARING_STATUS_RESULT]: { status?: CloudSharingStatus; error?: string };
  [ActionTypes.START_DISCOVERY]: void;
  [ActionTypes.STOP_DISCOVERY]: void;
  [ActionTypes.DISCOVERY_CLIENT_FOUND]: DiscoveredClient;
  [ActionTypes.DISCOVERY_ERROR]: { error: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  login: () => buildAction(ActionTypes.LOGIN),
  loginResult: (loggedInUser: LoggedInUser) => buildAction(ActionTypes.LOGIN_RESULT, { loggedInUser }),
  loginResultErr: (error: string) => buildAction(ActionTypes.LOGIN_RESULT, { error }),
  updateLoginData: (data: Partial<LoginData>) => buildAction(ActionTypes.UPDATE_LOGIN_DATA, data),
  checkAuthResult: (loggedInUser: LoggedInUser | null = null) => buildAction(ActionTypes.CHECK_AUTH_RESULT, { loggedInUser }),
  checkAuthNow: () => buildAction(ActionTypes.CHECK_AUTH_NOW),
  logout: () => buildAction(ActionTypes.LOGOUT),

  turnCloudSharing: (on: boolean) => buildAction(ActionTypes.TURN_CLOUD_SHARING, { on }),
  turnCloudSharingDone: (error?: string) => buildAction(ActionTypes.TURN_CLOUD_SHARING_DONE, { error }),
  cloudSharingStatus: () => buildAction(ActionTypes.CLOUD_SHARING_STATUS),
  cloudSharingStatusResult: (status: CloudSharingStatus) =>
    buildAction(ActionTypes.CLOUD_SHARING_STATUS_RESULT, { status }),
  cloudSharingStatusResultErr: (error: string) =>
    buildAction(ActionTypes.CLOUD_SHARING_STATUS_RESULT, { error }),

  startDiscovery: () => buildAction(ActionTypes.START_DISCOVERY),
  stopDiscovery: () => buildAction(ActionTypes.STOP_DISCOVERY),
  discoveryClientFound: (client: DiscoveredClient) => buildAction(ActionTypes.DISCOVERY_CLIENT_FOUND, client),
  discoveryError: (error: string) => buildAction(ActionTypes.DISCOVERY_ERROR, { error }),
};
