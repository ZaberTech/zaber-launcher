/* eslint-disable @typescript-eslint/non-nullable-type-assertion-style */
import { BoundFunctions, fireEvent, queries, RenderResult, within } from '@testing-library/react';
import type { Units } from '@zaber/motion';
import { injectable } from 'inversify';
import type { SagaIterator } from 'redux-saga';

import { waitUntilPass } from '../test';
import { EntityKey } from '../keys';

import { UnitFrom, UnitInfo, unsupportedUnitInfo } from './types';
import { getUnitInfoOfDimension } from './utils';

export type Wrapper = BoundFunctions<typeof queries> | RenderResult;

export function getValueInputByTitle(wrapper: Wrapper, title: string) {
  return wrapper.getByTitle(`Value of ${title}`) as HTMLInputElement;
}

export function getUnitSelectByTitle(wrapper: Wrapper, title: string) {
  return wrapper.getByTitle(`Units of ${title}`) as HTMLSelectElement;
}

export function getSelectByTestId(wrapper: Wrapper, id: string) {
  return wrapper.getByTestId(id) as HTMLSelectElement;
}

export function getInputAndSelectByTitle(wrapper: Wrapper, title: string) {
  return {
    valueInput: getValueInputByTitle(wrapper, title),
    unitSelect: getUnitSelectByTitle(wrapper, title),
  };
}

/**
 * Sets the value of an input in a `InputWithUnits` or `EditableInputWithUnits`
 * @param wrapper The result to search for the component within
 * @param title The base title of the component (by default the setting name)
 * @param value The value to set
 * @param openToEdit Set true if this is an `EditableInputWithUnits` where the new value must be confirmed
 */
export async function setValueInputByTitle(wrapper: Wrapper, title: string, value: number, openToEdit = false) {
  if (openToEdit) {
    fireEvent.click(wrapper.getByTitle(`Edit ${title}`));
    await waitUntilPass(() => wrapper.getByTitle(`Confirm ${title} edit`));
  }
  const input = getValueInputByTitle(wrapper, title);
  fireEvent.change(input, { target: { value: value.toString() } });
  if (openToEdit) {
    fireEvent.click(wrapper.getByTitle(`Confirm ${title} edit`));
  }
}

export function setUnitSelectByTitle(wrapper: Wrapper, title: string, units: Units) {
  const select = getUnitSelectByTitle(wrapper, title);
  fireEvent.change(select, { target: { value: units.toString() } });
}

/**
 * Sets the value of an input in a `InputWithUnits` or `EditableInputWithUnits`
 * @param wrapper The result to search for the component within
 * @param title The base title of the component (by default the setting name)
 * @param value The value to set
 * @param unit The unit to set
 * @param openToEdit Set true if this is an `EditableInputWithUnits` where the new value must be confirmed
 */
export function setValueAndUnitsByTitle(wrapper: Wrapper, title: string, value: number, units: Units, openToEdit = false) {
  if (openToEdit) {
    fireEvent.click(wrapper.getByTitle(`Edit ${title}`));
  }
  const { valueInput, unitSelect } = getInputAndSelectByTitle(wrapper, title);
  fireEvent.change(unitSelect, { target: { value: units.toString() } });
  fireEvent.change(valueInput, { target: { value: value.toString() } });
  if (openToEdit) {
    fireEvent.click(wrapper.getByTitle(`Confirm ${title} edit`));
  }
}

export function editValueAndUnitsByLabel(wrapper: Wrapper, label: string, value: number, units: Units) {
  const placeholderInput = wrapper.getByRole('spinbutton', { name: label });
  fireEvent.focus(placeholderInput);
  const popUp = placeholderInput.closest('body')?.querySelector('.input-editor.editing-popup') as HTMLElement;
  const popUpWrapper = within(popUp);
  fireEvent.change(popUpWrapper.getByRole('combobox'), { target: { value: units.toString() } });
  fireEvent.change(popUpWrapper.getByRole('spinbutton'), { target: { value: value.toString() } });
  fireEvent.click(popUpWrapper.getByText('Save'));
}

@injectable()
export class UnitInfoServiceMockBase {
  protected mockUnitInfo(dimension: string, conversion: number | null = null, precision?: number): UnitInfo {
    const info = getUnitInfoOfDimension(dimension);
    if (info.supported) {
      return { ...info, conversion, precision: precision ?? info.precision };
    }
    return { ...info, precision: precision ?? info.precision };
  }

  public* getSettingUnitInfo(_entity: EntityKey, _setting: string): SagaIterator<UnitInfo> {
    return unsupportedUnitInfo();
  }

  public* getUnitInfo(_from: UnitFrom): SagaIterator<UnitInfo> {
    return unsupportedUnitInfo();
  }
}
