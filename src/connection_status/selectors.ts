import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectConnectionStatus } from '../store';
import { selectConnections as selectManagerConnections } from '../connection_manager/selectors';
import { tryAccess } from '../utils';

export const selectRouters = createSelector(selectConnectionStatus, state => state.routers);
export const selectConnections = createSelector(selectConnectionStatus, selectManagerConnections,
  (state, connections) => _.mapValues(state.connections, (status, key) => ({
    ...status,
    connection: tryAccess(connections, key),
  }))
);
