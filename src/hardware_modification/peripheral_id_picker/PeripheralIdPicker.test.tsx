import React, { Component } from 'react';
import { Container, injectable } from 'inversify';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import _ from 'lodash';
import type { ascii } from '@zaber/motion';

declare namespace SelectMock {
  interface Item {
    isDisabled?: boolean;
  }
  interface Props {
    placeholder: string;
    value: EnumProtocolDataOption;
    onChange: (item: unknown) => void;
    options: EnumProtocolDataOption[];
    'data-testid': string;
    isDisabled: boolean;
  }
  interface State {
    items: Item[];
    input: string;
  }
}

class SelectMock extends Component<SelectMock.Props, SelectMock.State> {
  render() {
    const { placeholder, isDisabled, options, onChange } = this.props;
    return <input
      placeholder={placeholder}
      onChange={e => onChange(options.find(opt => `${opt.value}` === e.target.value))}
      data-testid={this.props['data-testid']}
      disabled={isDisabled}
    />;
  }

  static createFilter() { return true }
}

jest.mock('react-select', () => SelectMock);

HTMLElement.prototype.scrollIntoView = _.noop;

import { connectionViewMockInstance } from '../../connection_manager/connection_view/mocks';
import { createContainer, destroyContainer } from '../../container';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter, mockStorage, StorageMock } from '../../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase
} from '../../test/mocks/ascii';
import { connectionManagerActions, selectAxes, selectDevices } from '../../connection_manager';
import { MessageRoutersService } from '../../message_router';
import { mockDataForDeviceWithoutPeripherals, mockSingleDeviceWithPeripherals } from '../../connection_manager/mocks';
import type { AxisState, DeviceState } from '../../connection_manager/reducer';
import { HardwareModificationApp } from '../HardwareModificationApp';
import type { EnumProtocolDataOption } from '../types';
import { extractConnectionKey } from '../../keys';

let wrapper: RenderResult;
let container: Container;
let storageMock: StorageMock;

let asciiAxes: AxisMock[];
class AxisMock extends AxisMockBase {
  constructor(axisNumber: number, device: DeviceMockBase<AxisMockBase>) {
    super(axisNumber, device);
    asciiAxes.push(this);
  }

  identity?: Partial<ascii.AxisIdentity>;

  settings = {
    get: jest.fn(async () => 1),
    set: jest.fn(async () => undefined),
  };

  storage = {
    setBool: jest.fn(async () => undefined),
  };
}

let devices: DeviceMock[];
let device: DeviceState;
class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(id: number, connection: ConnectionMock) {
    super(id, connection);
    devices.push(this);
  }

  identity?: Partial<ascii.DeviceIdentity>;
  axisCount: number = 0;
  settings = {
    get: jest.fn(_name => 1),
    set: jest.fn((_name, _value) => 1),
  };

  identify = jest.fn(async () => this.identity);
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {}

jest.spyOn(connectionManagerActions, 'loadDevices')
  .mockImplementation((connectionKey: string) => {
    setImmediate(() => {
      if (device) {
        const store = TestHardwareModificationApp.testStore;
        const devices = [mockDataForDeviceWithoutPeripherals(extractConnectionKey(device.key), device.address)];
        devices[0].identity.isModified = true;
        devices[0].identity.firmwareVersion = { major: 7, minor: 22, build: 8074 };
        store.dispatch(connectionManagerActions.devicesLoaded(connectionKey, devices));
      }
    });
    return ({ type: 'LOAD_DEVICES_MOCK_ACTION', payload: null! });
  });

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const configurableFirmwareVersion = { major: 7, minor: 27, build: 8074 };

const TestHardwareModificationApp = wrapWithNewStore(wrapWithRouter(HardwareModificationApp));

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  storageMock = mockStorage(container);

  asciiAxes = [];
  devices = [];

  wrapper = render(<TestHardwareModificationApp/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  asciiAxes = [];
  devices = [];
  device = null!;

  destroyContainer();
  container = null!;
  storageMock.stored = {};
});

describe('Peripheral ID picker', () => {
  let axis: AxisState;

  let nextButton: HTMLInputElement;

  let motionSelect: HTMLInputElement;
  let motorSelect: HTMLInputElement;
  let encoder1Select: HTMLInputElement;
  let encoder2Select: HTMLInputElement;

  beforeEach(async () => {
    const store = TestHardwareModificationApp.testStore;
    mockSingleDeviceWithPeripherals(store, {
      peripheralCount: 1, type: 'unused', modifier: d => ({
        ...d, identity: { ...d.identity, firmwareVersion: configurableFirmwareVersion }
      })
    });
    device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    axis = _.sample(selectAxes(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);

    await waitUntilPass(() => wrapper.getByText('Hardware is not identified'));
    fireEvent.click(wrapper.getByText('Configure'));
    await waitUntilPass(() => wrapper.getByTestId('motion-trait-picker'));

    nextButton = wrapper.getByText('Next') as HTMLInputElement;

    motionSelect = wrapper.getByTestId('motion-trait-picker') as HTMLInputElement;
    motorSelect = wrapper.getByTestId('motor-trait-picker') as HTMLInputElement;
    encoder1Select = wrapper.getByTestId('singleEncoder-trait-picker') as HTMLInputElement;
    encoder2Select = wrapper.getByTestId('diffEncoder-trait-picker') as HTMLInputElement;
  });

  afterEach(() => {
    axis = null!;

    nextButton = null!;

    motionSelect = null!;
    motorSelect = null!;
    encoder1Select = null!;
    encoder2Select = null!;
  });

  test('Non-null checks', async () => {
    expect(nextButton.disabled).toBe(true);

    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    expect(nextButton.disabled).toBe(true);

    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    expect(nextButton.disabled).toBe(true);

    fireEvent.input(encoder1Select, { target: { value: 'none' } });
    expect(nextButton.disabled).toBe(true);

    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    expect(nextButton.disabled).toBe(false);
  });

  test('Non-null checks', async () => {
    expect(nextButton.disabled).toBe(true);

    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    expect(nextButton.disabled).toBe(true);

    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    expect(nextButton.disabled).toBe(true);

    fireEvent.input(encoder1Select, { target: { value: 'none' } });
    expect(nextButton.disabled).toBe(true);

    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    expect(nextButton.disabled).toBe(false);
  });

  test('Zaber controllers can only support one encoder on stepper motor peripherals', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'direct' } });
    fireEvent.input(encoder2Select, { target: { value: 'direct' } });
    fireEvent.click(nextButton);

    wrapper.getByText('Zaber controllers can only support one encoder on stepper motor peripherals.');
  });

  test('Absolute phase motor encoders are only compatible with three-phase direct motors', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'voice' } });
    fireEvent.input(encoder1Select, { target: { value: 'absolute' } });
    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    fireEvent.click(nextButton);

    wrapper.getByText('Absolute phase motor encoders are only compatible with three-phase direct motors.');
  });

  test('Peripheral ID 88800', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'none' } });
    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88800);
    });
    await waitUntilPass(() => {
      wrapper.getByText('Enabling Hardware Modification...');
    });
  });

  test('Peripheral ID 88801', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'motor' } });
    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88801);
    });
  });

  test('Peripheral ID 88802', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'direct' } });
    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88802);
    });
  });

  test('Peripheral ID 88803', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'none' } });
    fireEvent.input(encoder2Select, { target: { value: 'direct' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88803);
    });
  });

  test('Peripheral ID 88804', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'none' } });
    fireEvent.input(encoder2Select, { target: { value: 'analog' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88804);
    });
  });

  test('Peripheral ID 88805', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'linear' } });
    fireEvent.input(encoder1Select, { target: { value: 'absolute' } });
    fireEvent.input(encoder2Select, { target: { value: 'analog' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88805);
    });
  });

  test('Linear motors require an absolute phase and differential direct encoder', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'linear' } });
    fireEvent.input(encoder1Select, { target: { value: 'direct' } });
    fireEvent.input(encoder2Select, { target: { value: 'analog' } });
    fireEvent.click(nextButton);

    wrapper.getByText(
      'Zaber controllers require a single-ended absolute phase motor encoder ' +
      'and a differential direct analog encoder to support linear motors.'
    );
  });

  test('Peripheral ID 88806', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'voice' } });
    fireEvent.input(encoder1Select, { target: { value: 'none' } });
    fireEvent.input(encoder2Select, { target: { value: 'analog' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88806);
    });
  });

  test('Zaber controllers require a differential analog direct encoder to support voice coil motors', async () => {
    fireEvent.input(motionSelect, { target: { value: 'linear' } });
    fireEvent.input(motorSelect, { target: { value: 'voice' } });
    fireEvent.input(encoder1Select, { target: { value: 'direct' } });
    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    fireEvent.click(nextButton);

    wrapper.getByText('Zaber controllers require a differential analog direct encoder to support voice coil motors.');

    fireEvent.input(encoder2Select, { target: { value: 'analog' } });
    fireEvent.click(nextButton);

    wrapper.getByText('Voice coil motors may not have a single-ended encoder.');
  });

  test('Peripheral ID 88807', async () => {
    fireEvent.input(motionSelect, { target: { value: 'rotary' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'none' } });
    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88807);
    });
  });

  test('Peripheral ID 88808', async () => {
    fireEvent.input(motionSelect, { target: { value: 'rotary' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'motor' } });
    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88808);
    });
  });

  test('Peripheral ID 88809', async () => {
    fireEvent.input(motionSelect, { target: { value: 'rotary' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'direct' } });
    fireEvent.input(encoder2Select, { target: { value: 'none' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88809);
    });
  });

  test('Peripheral ID 88810', async () => {
    fireEvent.input(motionSelect, { target: { value: 'rotary' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'none' } });
    fireEvent.input(encoder2Select, { target: { value: 'direct' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88810);
    });
  });

  test('Peripheral ID 88811', async () => {
    fireEvent.input(motionSelect, { target: { value: 'rotary' } });
    fireEvent.input(motorSelect, { target: { value: 'stepper' } });
    fireEvent.input(encoder1Select, { target: { value: 'none' } });
    fireEvent.input(encoder2Select, { target: { value: 'analog' } });
    fireEvent.click(nextButton);

    await waitUntilPass(() => {
      expect(asciiAxes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88811);
    });
  });

  test('Non-stepper-motor rotary peripherals are not compatible with Zaber controllers', async () => {
    fireEvent.input(motionSelect, { target: { value: 'rotary' } });
    fireEvent.input(motorSelect, { target: { value: 'voice' } });
    fireEvent.input(encoder1Select, { target: { value: 'direct' } });
    fireEvent.input(encoder2Select, { target: { value: 'analog' } });
    fireEvent.click(nextButton);

    wrapper.getByText('Non-stepper-motor rotary peripherals are not compatible with Zaber controllers.');
  });

  test('Can Cancel', async () => {
    expect(wrapper.queryByText('Hardware is not identified')).toBeNull();
    fireEvent.click(wrapper.getByText('Cancel'));
    wrapper.getByText('Hardware is not identified');
  });

  test('Can Show Docs', async () => {
    fireEvent.click(wrapper.getByTitle('Show Motion Type Docs'));
    await waitUntilPass(() => wrapper.getByText('Mocked HTML data'));
  });
});
