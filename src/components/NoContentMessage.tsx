import React from 'react';
import { Loader, NoticeBanner, Text } from '@zaber/react-library';
import { match, P } from 'ts-pattern';

interface Props {
  title?: string;
  message?: React.ReactNode;
  type?: 'error' | 'warning' | 'info' | 'working';
  bannerTitle?: React.ReactNode;
  tryAgain?: { action: () => void; button: string };
}

export const NoContentMessage: React.FC<Props> = ({ title, message, type, children, bannerTitle, tryAgain }) => (
  <div className="no-content">
    {title && <Text t={Text.Type.H2} e={Text.Emphasis.Light}>{title}</Text>}
    {match(type)
      .with(P.union('info', 'warning', 'error'), noticeType =>
        <NoticeBanner type={noticeType} closer={tryAgain}>
          {bannerTitle && <Text className="title" e={Text.Emphasis.Bold}>{bannerTitle}&ensp;</Text>}
          {message}
        </NoticeBanner>)
      .with('working', () =>
        <div className="working">
          <Loader size="large"/>
          {message && <Text>{message}</Text>}
        </div>)
      .otherwise(() => message &&
        <Text t={Text.Type.BodyLg} e={Text.Emphasis.Light}>{message}</Text>)}
    {children}
  </div>
);
