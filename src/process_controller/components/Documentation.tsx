import React from 'react';

import type { EntityKey } from '../../keys';
import { ProtocolManual, ProtocolManualFormatter } from '../../protocol_manual';

import ai from './custom_manuals/analog_input.html';

const customDocs: Record<string, string> = {
  'analog in': ai,
};

interface Props {
  entity: EntityKey;
  setting: string;
}

export const Documentation: React.FC<Props> = ({ entity, setting }) => {
  if (setting in customDocs) {
    return <ProtocolManualFormatter html={customDocs[setting]}/>;
  }

  return <ProtocolManual deviceOrAxisKey={entity} setting={setting}/>;
};
