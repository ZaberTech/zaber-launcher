import fs from 'fs';
import path from 'path';

import moment from 'moment';
import * as remote from '@electron/remote';
import { inject, injectable, preDestroy } from 'inversify';
import { Library, LogOutputMode } from '@zaber/motion';
import { Mutex } from 'async-mutex';

import { environment } from '../environment';
import { PreferencesService } from '../preferences/service';
import { PreferenceKeys } from '../preferences/types';
import { handleNonCriticalError } from '../errors';
import { Paths } from '../paths_util';
import { sleep } from '../utils';

import { loggingSettingsSymbol, LoggingSettings } from './logging_settings';

@injectable()
export class ZMLLoggingService {
  private logToFile: boolean = false;
  private mutex = new Mutex();
  private _logFilePath: string | null = null;
  private browserSuffix: string = `window-${remote.getCurrentWindow().id}`;
  private destroyed = false;

  public static readonly LOG_FOLDER_NAME = 'zmlLogs';
  public static readonly LOG_FOLDER_PATH = path.join(Paths.LOG, ZMLLoggingService.LOG_FOLDER_NAME);

  constructor(
    @inject(PreferencesService) private readonly preferencesService: PreferencesService,
    @inject(loggingSettingsSymbol) private readonly settings: LoggingSettings,
  ) {
    if (!environment.isProduction) {
      Library.setLogOutput(LogOutputMode.STDERR);
    }

    this.preferencesService.registerHandler(
      PreferenceKeys.ZML_LOGGING, value => this.setLogToFile(value)
    );

    this.dayChangeLoop().catch(() => null);
    this.checkLogfileSizeLoop().catch(() => null);
  }

  @preDestroy()
  onDestroy() {
    this.destroyed = true;
  }

  public async setLogToFile(enable: boolean): Promise<void> {
    if (enable === this.logToFile) {
      return;
    }
    this.logToFile = enable;

    try {
      await this.updateLogging(this.logToFile);
    } catch (e) { handleNonCriticalError(e) }
  }

  // This function will remove files from log folder if there are too many files
  // associated with this browser window
  public async requestFilePath(): Promise<string> {
    const folderPath = ZMLLoggingService.LOG_FOLDER_PATH;
    await this.tryUpdateLogFolder(folderPath);
    const currentDate = new Date();
    const formattedDate = currentDate.toISOString().replace(/T/g, '-').replace(/:/g, '_').slice(0, 19);
    return path.join(folderPath, `${formattedDate}-log-${this.browserSuffix}.txt`);
  }

  public get logFilePath(): string | null {
    return this._logFilePath;
  }

  public static computeTimeToMidnight(now: moment.Moment): number {
    const midnight = now.clone().endOf('day').add(1, 'ms');
    return midnight.diff(now);
  }

  public async logSizeExceeded(): Promise<boolean> {
    return await this.mutex.runExclusive(async () => {
      if (this._logFilePath == null) { return false }
      const stats = await fs.promises.stat(this._logFilePath);
      return stats.size > this.settings.maxLogSize;
    });
  }

  private async updateLogging(enable: boolean): Promise<void> {
    await this.mutex.runExclusive(async () => {
      if (enable) {
        this._logFilePath = await this.requestFilePath();
      } else {
        this._logFilePath = null;
      }
      const logMode =
        enable ? LogOutputMode.FILE :
        environment.isProduction ? LogOutputMode.OFF :
        LogOutputMode.STDERR;
      Library.setLogOutput(logMode, this._logFilePath ?? undefined);
    });
  }

  private async tryUpdateLogFolder(folderPath: string): Promise<void> {
    const exists = await fs.promises.access(folderPath).then(() => true).catch(() => false);
    if (!exists) {
      await fs.promises.mkdir(folderPath, { recursive: true });
    }
    const files = await fs.promises.readdir(folderPath);
    const filteredFiles = files.filter(file => file.endsWith(`${this.browserSuffix}.txt`));
    const fileCount = filteredFiles.length;
    if (fileCount >= this.settings.maxLogFiles) {
      const oldestFiles = filteredFiles.sort().slice(0, fileCount - this.settings.maxLogFiles + 1);
      for (const file of oldestFiles) {
        await fs.promises.unlink(path.join(folderPath, file));
      }
    }
  }

  private async dayChangeLoop() {
    for (;;) {
      await sleep(ZMLLoggingService.computeTimeToMidnight(moment()));
      if (this.destroyed) { return }

      try {
        if (this.logToFile) {
          await this.updateLogging(this.logToFile);
        }
      } catch (e) {
        handleNonCriticalError(e);
      }
    }
  };

  private async checkLogfileSizeLoop() {
    for (;;) {
      await sleep(this.settings.sizeCheckInterval);
      if (this.destroyed) { return }

      try {
        if (await this.logSizeExceeded()) {
          await this.updateLogging(this.logToFile);
        }
      } catch (e) {
        handleNonCriticalError(e);
      }
    }
  };
}
