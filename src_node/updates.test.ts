import path from 'path';

import { injectable } from 'inversify';
import { map, Subject } from 'rxjs';
import { waitUntilPass, realTimers } from '@zaber/toolbox/lib/test';

jest.useFakeTimers();

const autoUpdaterMock = {
  quitAndInstall: jest.fn(),
  checkForUpdates: jest.fn().mockResolvedValue({ downloadPromise: null }),
  addListener: jest.fn(),

  _reset() {
    this.checkForUpdates.mockClear();
    this.quitAndInstall.mockClear();
  }
};

jest.mock('electron', () => ({
  BrowserWindow: {
    getFocusedWindow: () => ({ webContents: 'focused-window' }),
  },
  app: {
    getPath: () => 'userData',
    getVersion: () => '1.2.1',
  },
}));
jest.mock('electron-updater', () => ({
  autoUpdater: autoUpdaterMock,
}));
const fs = {
  writeFile: jest.fn().mockResolvedValue(undefined),
  readFile: jest.fn<Promise<string>, [string]>().mockRejectedValue(new Error('File not found')),
  unlink: jest.fn(),

  _reset() {
    this.readFile.mockClear();
    this.writeFile.mockClear();
    this.unlink.mockClear();
  }
};
jest.mock('fs/promises', () => fs);

import { Environment, environment } from '../src/environment';
import type { AnyAction } from '../src/utils';
import { ActionTypes, actions } from '../src/updates/actions';

import { createContainer, destroyContainer } from './container';
import { Ipc } from './ipc';
import { RENDER_PROCESS_DISPLAY_NOTIFICATION_TIMEOUT, STARTUP_CHECK_DELAY, INSTALL_BEGIN_TIMEOUT, Updates } from './updates';

let ipcMock: IpcMock;

@injectable()
class IpcMock {
  channels: Record<string, Subject<AnyAction>> = {};

  broadcastAction = jest.fn();
  sendAction = jest.fn<void, [unknown, AnyAction]>();
  getActions = jest.fn((actionType: string) => {
    if (!this.channels[actionType]) {
      this.channels[actionType] = new Subject<AnyAction>();
    }
    return this.channels[actionType].pipe(map(action => ({ action, sender: 'window' })));
  });
}

let container: ReturnType<typeof createContainer>;
let updates: Updates;

beforeEach(() => {
  fs._reset();

  (environment as Environment).isProduction = true;
  container = createContainer();
  container.bind<unknown>(Ipc).to(IpcMock);
  ipcMock = container.get<unknown>(Ipc) as IpcMock;
  updates = container.get(Updates);
});

afterEach(() => {
  destroyContainer();
  container = null!;
  ipcMock = null!;
  updates = null!;

  autoUpdaterMock._reset();
});

test('checks for updates on start', () => {
  updates.scheduleUpdateChecks();
  jest.advanceTimersByTime(STARTUP_CHECK_DELAY);

  expect(autoUpdaterMock.checkForUpdates).toHaveBeenCalled();
});

test('calls quitAndInstall of updater on action from render processes', () => {
  ipcMock.channels[ActionTypes.QUIT_AND_INSTALL].next(actions.quitAndInstall());
  expect(autoUpdaterMock.quitAndInstall).toHaveBeenCalled();
});

test('broadcast actions once checking is done', async () => {
  updates.scheduleUpdateChecks();
  jest.advanceTimersByTime(STARTUP_CHECK_DELAY);

  await waitUntilPass(() =>
    expect(ipcMock.broadcastAction).toHaveBeenCalledWith(
      { type: ActionTypes.VERSION_CHECK_DONE, payload: expect.anything() }
    )
  );
});

describe('new version', () => {
  beforeEach(() => {
    autoUpdaterMock.checkForUpdates.mockResolvedValueOnce({
      updateInfo: { version: '1.2.3' },
      downloadPromise: Promise.resolve(),
    });
  });

  test('sends action when there is a new version available', async () => {
    ipcMock.sendAction.mockImplementation((windows, action) => {
      if (action.type === ActionTypes.NEW_VERSION_READY) {
        realTimers.setTimeout(() =>
          ipcMock.channels[ActionTypes.NEW_VERSION_NOTIFICATION_DISPLAYED].next(actions.newVersionNotificationDisplayed()));
      }
    });

    updates.scheduleUpdateChecks();
    jest.advanceTimersByTime(STARTUP_CHECK_DELAY);

    await waitUntilPass(() =>
      expect(ipcMock.broadcastAction).toHaveBeenCalled()
    );

    expect(ipcMock.sendAction).toHaveBeenCalledWith(
      'focused-window',
      { type: ActionTypes.NEW_VERSION_READY, payload: { version: '1.2.3', canUpdate: true } }
    );
  });

  test('applies update if there is no reply from window', async () => {
    ipcMock.sendAction.mockImplementation((windows, action) => {
      if (action.type === ActionTypes.NEW_VERSION_READY) {
        realTimers.setTimeout(() => jest.advanceTimersByTime(RENDER_PROCESS_DISPLAY_NOTIFICATION_TIMEOUT));
      }
    });

    updates.scheduleUpdateChecks();
    jest.advanceTimersByTime(STARTUP_CHECK_DELAY);

    await waitUntilPass(() =>
      expect(autoUpdaterMock.quitAndInstall).toHaveBeenCalled()
    );
  });

  test('populates scheduled updated file on ignore update', async () => {
    updates.scheduleUpdateChecks();
    jest.advanceTimersByTime(STARTUP_CHECK_DELAY);
    await waitUntilPass(() => expect(ipcMock.sendAction).toHaveBeenCalled());

    ipcMock.channels[ActionTypes.IGNORE_UPDATE].next(actions.ignoreUpdate(true));
    expect(fs.writeFile).toHaveBeenCalledWith(path.join('userData', 'pending-update.txt'), '1.2.3', 'utf-8');
  });

  test('installs scheduled update on start', async () => {
    fs.readFile.mockImplementation(async file => {
      if (file.includes('pending-update')) {
        return '1.2.3';
      }
      throw new Error('File not found');
    });
    const checkPromise = updates.init();

    await waitUntilPass(() =>
      expect(autoUpdaterMock.quitAndInstall).toHaveBeenCalled());
    expect(fs.unlink).toHaveBeenCalledWith(path.join('userData', 'pending-update.txt'));

    jest.advanceTimersByTime(INSTALL_BEGIN_TIMEOUT);
    await checkPromise;
  });

  test('sends notification with canUpdate=false and does nothing else when updates are disabled', async () => {
    fs.readFile.mockImplementation(async file => {
      if (file.includes('disable-updates')) {
        return 'This file disables updates';
      }
      throw new Error('File not found');
    });
    await updates.init();

    updates.scheduleUpdateChecks();
    jest.advanceTimersByTime(STARTUP_CHECK_DELAY);

    await waitUntilPass(() => {
      expect(ipcMock.sendAction).toHaveBeenCalledWith(
        'focused-window',
        { type: ActionTypes.NEW_VERSION_READY, payload: { version: '1.2.3', canUpdate: false } }
      );
    });
  });
});

test('puts error into action when the update check fails', async () => {
  autoUpdaterMock.checkForUpdates.mockRejectedValueOnce(new Error('No internet'));
  updates.scheduleUpdateChecks();
  jest.advanceTimersByTime(STARTUP_CHECK_DELAY);

  await waitUntilPass(() =>
    expect(ipcMock.broadcastAction).toHaveBeenCalledWith(
      { type: ActionTypes.VERSION_CHECK_DONE, payload: { error: 'No internet' } }
    )
  );
});

test('only one check can be in progress at a time', async () => {
  let checkForUpdatesResolve: (arg: unknown) => void;
  autoUpdaterMock.checkForUpdates.mockReturnValueOnce(new Promise(resolve => { checkForUpdatesResolve = resolve }));

  updates.scheduleUpdateChecks();
  jest.advanceTimersByTime(STARTUP_CHECK_DELAY);

  ipcMock.channels[ActionTypes.CHECK_NEW_VERSION].next(actions.checkNewVersion());
  ipcMock.channels[ActionTypes.CHECK_NEW_VERSION].next(actions.checkNewVersion());

  expect(autoUpdaterMock.checkForUpdates).toHaveBeenCalledTimes(1);

  checkForUpdatesResolve!({ downloadPromise: null });
  await waitUntilPass(() =>
    expect(ipcMock.broadcastAction).toHaveBeenCalled()
  );

  ipcMock.channels[ActionTypes.CHECK_NEW_VERSION].next(actions.checkNewVersion());
  expect(autoUpdaterMock.checkForUpdates).toHaveBeenCalledTimes(2);
});

describe('after initial update check', () => {
  beforeEach(async () => {
    updates.scheduleUpdateChecks();
    jest.advanceTimersByTime(STARTUP_CHECK_DELAY);

    await waitUntilPass(() =>
      expect(ipcMock.broadcastAction).toHaveBeenCalled()
    );
    ipcMock.broadcastAction.mockClear();
    autoUpdaterMock.checkForUpdates.mockClear();
  });

  test('checks for updates on action from render process', () => {
    ipcMock.channels[ActionTypes.CHECK_NEW_VERSION].next(actions.checkNewVersion());
    expect(autoUpdaterMock.checkForUpdates).toHaveBeenCalled();
  });
});
