import React, { Component } from 'react';
import classNames from 'classnames';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Chevron, Icons, NoticeMessage, Text } from '@zaber/react-library';
import _ from 'lodash';

import type { RootState } from '../../store';
import { ConnectionType } from '../../app_components';
import { selectConnections } from '../selectors';
import { connectionName } from '../utils';
import { omit } from '../../utils';
import type { ConnectionState } from '../reducer';
import { EntityKey, tryExtractConnectionKey } from '../../keys';

import { Device } from './Device';
import type { PassedProperties } from './types';
import { AttentionRequester } from './AttentionRequester';
import { ConnectionContextMenu, LoadingDevicesIcon } from './ConnectionParts';
import { selectExpanded } from './selectors';
import { actions as actionsDefinition } from './actions';

interface OwnProps extends PassedProperties {
  connectionKey: EntityKey;
}
interface DispatchProps {
  actions: typeof actionsDefinition;
}
interface StateProps {
  connection: ConnectionState;
  expanded: boolean;
}
type Props = OwnProps & StateProps & DispatchProps;

class ConnectionBase extends Component<Props> {
  onSelect = () => {
    const { selectable, onSelect, connection, actions } = this.props;
    if (selectable.includes('connection')) {
      actions.setExpanded(connection.key, true);
      onSelect(connection.key);
    }
  };
  onToggleExpanded = (e: React.MouseEvent<unknown>) => {
    const { expanded, connection, actions } = this.props;
    actions.setExpanded(connection.key, !expanded);
    e.stopPropagation();
  };

  componentDidUpdate(prevProps: Props) {
    const { selected, connection, expanded, actions } = this.props;
    if (connection.key === tryExtractConnectionKey(selected) && !expanded
      && connection.key !== tryExtractConnectionKey(prevProps.selected)) {
      actions.setExpanded(connection.key, true);
    }
  }

  render(): React.ReactNode {
    const { connection, expanded, ...rest } = this.props;
    const deviceProps = omit(rest, 'actions');
    const { attentionRequests, selectable } = rest;
    const selected = rest.selected === connection.key;
    const canSelect = selectable.includes('connection');
    const canOnlySelect = _.isEqual(selectable, ['connection']);
    const connectionNameText = connectionName(connection);

    return (
      <div className={classNames({ collapsed: !expanded, selected }, 'connection')}>
        <div className="connection-name" onClick={canSelect ? this.onSelect : this.onToggleExpanded}>
          <Chevron
            expanded={expanded}
            titleExpanded="Collapse Connection" titleCollapsed="Expand Connection"
            onClick={canSelect ? this.onToggleExpanded : undefined}/>

          {connection.type === ConnectionType.SERIAL_PORT && <Icons.Usb/>}
          {connection.type === ConnectionType.TCP_IP && <Icons.EthernetSocket/>}

          <Text t={Text.Type.H5} title={connectionNameText}>{connectionNameText}</Text>

          <LoadingDevicesIcon connection={connection}/>

          {!expanded && <AttentionRequester showChildrenCount
            entityKey={connection.key}
            extractor={tryExtractConnectionKey}
            attentionRequests={attentionRequests}/>}
          <ConnectionContextMenu connection={connection}/>
        </div>
        <div className={classNames('devices', { selectable: canOnlySelect })}
          onClick={canOnlySelect ? this.onSelect : undefined}>
          {connection.loadingDevicesErr && <NoticeMessage headline="Cannot load devices">
            {connection.loadingDevicesErr.message}
          </NoticeMessage>}
          {connection.devices?.map((deviceKey, i) => (
            <Device key={deviceKey} deviceKey={deviceKey} {...deviceProps} isLast={i === connection.devices!.length - 1}/>
          ))}
          {connection.devices?.length === 0 &&
            <NoticeMessage type="warning" className="no-devices">No devices have been detected.</NoticeMessage>
          }
        </div>
      </div>
    );
  }
}

export const Connection = connect<StateProps, DispatchProps, OwnProps, RootState>(
  (state: RootState, props: OwnProps): StateProps => ({
    connection: selectConnections(state)[props.connectionKey],
    expanded: selectExpanded(state)[props.connectionKey],
  }),
  (dispatch: Dispatch): DispatchProps => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(ConnectionBase);
