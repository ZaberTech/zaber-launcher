import { all, takeEvery, put, select } from 'redux-saga/effects';
import type { SagaIterator } from 'redux-saga';
import { throwUnexpectedError } from '@zaber/toolbox';
import { duration } from 'moment';

import type { Action, RT, SagaIter } from '../utils';
import { getDevice, getAxis, reloadDevices } from '../connection_manager';
import { extractConnectionKey, getEntityType, EntityKeyType } from '../keys';
import { getSerialNumberInfo, waitForDeviceToRestart } from '../devices';

import { ActionTypes, ActionsToPayloads, actions } from './actions';
import { selectAll } from './selectors';

const WAIT_FOR_RESTART_TIMEOUT_MS = duration(60, 'seconds').asMilliseconds();

export function* rootSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.CONFIRM, confirmed),
  ]);
}

function* confirmed({ payload: { hardReset } }: Action<ActionsToPayloads[typeof ActionTypes.CONFIRM]>): SagaIter {
  try {
    const { currentKey }: RT<typeof selectAll> = yield select(selectAll);
    if (currentKey == null) { return }

    if (getEntityType(currentKey) === EntityKeyType.AXIS) {
      const axis: RT<typeof getAxis> = yield getAxis(currentKey);
      yield axis.restore();
    } else {
      const device: RT<typeof getDevice> = yield getDevice(currentKey);
      if (hardReset) {
        const serialNumberInfo: RT<typeof getSerialNumberInfo> = yield getSerialNumberInfo(device);
        yield device.restore(true);
        yield waitForDeviceToRestart(currentKey, serialNumberInfo, WAIT_FOR_RESTART_TIMEOUT_MS);
      } else {
        yield device.restore();
      }
    }
    yield reloadDevices(extractConnectionKey(currentKey));
    yield put(actions.done());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.done(err.message));
  }
}
