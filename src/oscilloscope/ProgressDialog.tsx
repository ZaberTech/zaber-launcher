import { Icons, Modal, ProgressBar, Text, TextType } from '@zaber/react-library';
import React from 'react';
import { useSelector } from 'react-redux';

import { selectOscilloscope } from '../store';

export const ProgressDialog: React.FC = () => {
  const { progress, progressMessage, showProgressBar } = useSelector(selectOscilloscope);
  return <Modal
    className="progress-dialog"
    headerIcon={<Icons.Oscilloscope/>}
    headerText="Capturing Data"
    small
  >
    <Text t={TextType.H5}>Please wait while we capture the data</Text>
    <Text>{progressMessage}</Text>
    {showProgressBar && <ProgressBar progress={progress}/>}
  </Modal>;
};
