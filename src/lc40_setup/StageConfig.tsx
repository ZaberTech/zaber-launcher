import React from 'react';
import { useSelector } from 'react-redux';
import { useActions } from '@zaber/toolbox/lib/redux';
import { HeaderCard, NoticeBorder, Text } from '@zaber/react-library';

import stageLc40b from './assets/lc40b.png';
import stageLc40c from './assets/lc40c.png';
import { actions as actionsDefinition } from './actions';
import { DEVICE_NAME, StageType } from './types';
import { selectCurrentAxisOrDevice, selectResultError } from './selectors';
import { ImageSelect } from './ImageSelect';
import { ErrorContext } from './exception';


export const StageConfig: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const { config } = useSelector(selectCurrentAxisOrDevice)!;
  const resultError = useSelector(selectResultError);

  const hasError = (resultError?.errContext === ErrorContext.DRIVER_ORIENTATION);

  return <NoticeBorder type={hasError ? 'error' : null}>
    <HeaderCard className="section" header={<Text e={Text.Emphasis.Bold}>Stage Type</Text>}>
      <ImageSelect
        selected={config.stageType ?? ''}
        options={[
          {
            name: StageType.BASE_MOUNT,
            imageSource: stageLc40b,
            altText: `Diagram of ${DEVICE_NAME}B stage when viewed from the side.`,
            label: 'LC40B',
          },
          {
            name: StageType.CARRIAGE_MOUNT,
            imageSource: stageLc40c,
            altText: `Diagram of ${DEVICE_NAME}C stage when viewed from the side.`,
            label: 'LC40C',
          },
        ]}
        onSelect={name => {
          actions.updateConfig({ stageType: name as StageType, motorOrientation: undefined });
          actions.clearResultError();
        }}/>
    </HeaderCard>
  </NoticeBorder>;
};
