import { toBeDeepCloseTo } from 'jest-matcher-deep-close-to';

import { Sign, type Series } from './types';
import { PvtSeriesHelper } from './pvtSeriesHelper';

expect.extend({ toBeDeepCloseTo });


describe('Series helper', () => {
  describe('finds limits', () => {
    const series: Series = {
      name: 'X',
      points: [
        { dt: 1, time: 1, pos: 0, vel: 0 },
        { dt: 5, time: 6, pos: 20, vel: 12 },
        { dt: 5, time: 11, pos: 30, vel: 0 },
        { dt: 5, time: 16, pos: 30, vel: 0 }],
    };


    it('of position', () => {
      const posRange = PvtSeriesHelper.FromScales(series, 1, 1).findPositionRange();
      expect(posRange.max.value).toBeCloseTo(32.5);
    });


    it('of velocity', () => {
      const velRange = PvtSeriesHelper.FromScales(series, 1, 1).findVelocityRange();
      expect(velRange.min.value).toBeCloseTo(-1.5);
      expect(velRange.min.time).toBeCloseTo(9.75);
      expect(velRange.max.value).toBeCloseTo(12);
      expect(velRange.max.time).toBeCloseTo(5 + 1);
    });


    it('of acceleration', () => {
      const accelRange = PvtSeriesHelper.FromScales(series, 1, 1).findAccelerationRange();
      expect(accelRange.min.value).toBeCloseTo(-7.2);
      expect(accelRange.min.time).toBeCloseTo(6);
      expect(accelRange.max.value).toBeCloseTo(4.8);
      expect(accelRange.max.time).toBeCloseTo(6);
    });
  });


  describe('finds limit crossings', () => {
    const series: Series = {
      name: 'X',
      points: [
        { dt: 0, time: 0, pos: 0, vel: 0 },
        { dt: 1, time: 1, pos: 0, vel: 0 },
        { dt: 5, time: 6, pos: 20, vel: 12 },
        { dt: 5, time: 11, pos: 30, vel: 0 },
        { dt: 5, time: 16, pos: 30, vel: 0 }],
    };


    it('of position', () => {
      const helper = PvtSeriesHelper.FromScales(series, 1, 1);
      let times = helper.findPositionLimitCrossings(31, Sign.Positive);
      expect(times).toBeDeepCloseTo([{ index: 3, time: 7.506 }]);
      times = helper.findPositionLimitCrossings(31, Sign.Negative);
      expect(times).toBeDeepCloseTo([{ index: 3, time: 9.918 }]);
    });


    it('of velocity', () => {
      const helper = PvtSeriesHelper.FromScales(series, 1, 1);
      let times = helper.findVelocityLimitCrossings(9, Sign.Positive);
      expect(times).toBeDeepCloseTo([{ index: 2, time: 5.33 }]);
      times = helper.findVelocityLimitCrossings(9, Sign.Negative);
      expect(times).toBeDeepCloseTo([{ index: 3, time: 6.443 }]);
    });


    it('of acceleration', () => {
      const helper = PvtSeriesHelper.FromScales(series, 1, 1);
      let times = helper.findAccelerationLimitCrossings(1, Sign.Positive);
      expect(times).toBeDeepCloseTo([{ index: 2, time: 2.042 }, { index: 3, time: 10.271 }]);
      times = helper.findAccelerationLimitCrossings(1, Sign.Negative);
      // t=0 counts because the path starting outside a limit should be flagged.
      expect(times).toBeDeepCloseTo([{ index: 1, time: 0 }, { index: 3, time: 6 }, { index: 4, time: 11 }]);
    });


    it('of acceleration caused by discontinuities', () => {
      const discontinuous: Series = {
        name: 'X',
        points: [
          { dt: 0, time: 0, pos: 1, vel: 0 },
          { dt: 1, time: 1, pos: 1, vel: 10 },
          { dt: 1, time: 2, pos: 50, vel: 0 }],
      };

      const times = PvtSeriesHelper.FromScales(discontinuous, 1, 1).findAccelerationLimitCrossings(195, Sign.Positive);
      expect(times).toEqual([{ index: 2, time: 1 }]);
    });
  });


  it('finds position limit crossings resulting from deceleration', () => {
    const series: Series = {
      name: 'X',
      points: [
        { dt: 0, time: 0, pos: 0, vel: 0 },
        { dt: 0.5, time: 0.5, pos: 10, vel: 50 },
        { dt: 0.5, time: 1, pos: 10, vel: 0 }],
    };

    const helper = PvtSeriesHelper.FromScales(series, 1, 1);
    const problems = helper.findStopCollisions({ min: 0, max: 10 }, 1);
    expect(problems).toBeDeepCloseTo([{ index: 1, time: 0.5 }]);
  });
});
