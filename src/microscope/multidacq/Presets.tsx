import { HeaderCard, SimpleMultiSelect, Text } from '@zaber/react-library';
import React from 'react';
import { useSelector } from 'react-redux';

import { useActions } from '../../utils';
import { selectPresets } from '../selectors';
import { OpenPresetsDialog } from '../PresetEditor';

import { selectSelectedPresets } from './selectors';
import { actions as actionsDefinition } from './actions';

export const Presets: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const selected = useSelector(selectSelectedPresets);
  const presets = useSelector(selectPresets);

  const update = (selected: string[]) => actions.updateConfig({
    presets: { selected },
  });

  return (
    <HeaderCard className="preset-dimension"
      header={<Text t={Text.Type.H4}>Preset Dimension</Text>}>
      <SimpleMultiSelect
        placeholder="Select presets..."
        values={selected.map(({ name }) => name)}
        options={presets.map(({ name }) => ({ value: name, label: name }))}
        onValuesChange={update}
      />
      <OpenPresetsDialog/>
    </HeaderCard>
  );
};
