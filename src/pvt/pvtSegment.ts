import { getCubicRoots } from 'minimatrix-polyroots';
import _ from 'lodash';

import { type Point, type Peak, Range } from './types';
import { maxPeak, minPeak } from './utils';

/**
 * Helper for evaluating piecewise cubic polynomials of the form
 * position: c3x^3 + c2x^2 + c1x + c0 = 0
 * velocity: 3*c3x^2 + 2*c2x + x = 0  (derivative of above)
 * acceleration: 6*c3x + 2*c2 = 0  (derivative of above)
 * The two provided control points define the piece to be considered.
 * All times are relative to the first control point, and method results are only
 * valid for 0 <= t <= dt, where dt is the time difference between the two control points.
 */
export class PvtSegment {
  private c0 = 0;
  private c1 = 0;
  private c2 = 0;
  private c3 = 0;
  public dt = 0;

  /**
   * Instantiate the helper for one segment of the curve.
   * Note both endpoints must have absolute coordinates.
   * @param p0 The segment endpoint with the lower time value.
   * @param p1 The segment endpoint with the higher time value.
   * @param velocityScale Velocity correction factor if Zaber device units are used.
   * @param accelScale Acceleration correction factor if Zaber device units are used.
   */
  constructor(private p0: Point, private p1: Point, private velocityScale: number, private accelScale: number) {
    this.dt = p1.dt;
    this.calcCoefficients();
  }


  public getPos = (t: number): number => {
    const { c0, c1, c2, c3 } = this;
    return c0 + t * (c1 + t * (c2 + t * c3));
  };


  public getVel = (t: number): number => {
    const { c1, c2, c3, velocityScale } = this;
    return (c1 + t * (2 * c2 + 3 * c3 * t)) / velocityScale;
  };


  public getAccel = (t: number): number => {
    const { c2, c3, accelScale } = this;
    return (2 * c2 + 6 * c3 * t) / accelScale;
  };


  public getJerk = (): number => {
    const { c3, accelScale } = this;
    return (6 * c3) / accelScale;
  };


  public getPosRange(): Range {
    const { c1, c2, c3, dt } = this;

    // This is just solving the quadratic equation of velocity c3x^2 + c2x + c1 = 0,
    // to find points where velocity is zero meaning direction is changing.
    // Position minima and maxima can only occur within a segment where the direction
    // changes and at the endpoints.
    const criticalTimes = [0, dt];
    const a = 3 * c3; // a, b, c are the coefficients of the derivative of the cubic curve.
    const b = 2 * c2;
    const c = c1;
    if (a === 0) {
      criticalTimes.push(-c / b);
    } else {
      const disc = b ** 2 - 4 * a * c;
      if (disc > 0) {
        criticalTimes.push((-b + Math.sqrt(disc)) / (2 * a));
        criticalTimes.push((-b - Math.sqrt(disc)) / (2 * a));
      } else if (disc === 0) {
        criticalTimes.push(-b / (2 * a));
      }
    }

    return this.minmax(criticalTimes, this.getPos);
  }


  public getVelRange(): Range {
    const { c2, c3, dt } = this;
    const criticalTimes = [0, dt];
    if (c3 !== 0) {
      // When acceleration is 0.
      criticalTimes.push(-2 * c2 / (6 * c3));
    }

    return this.minmax(criticalTimes, this.getVel);
  }


  public getAccelRange(): Range {
    const { dt } = this;
    // Acceleration will always be between the endpoint values.
    const criticalTimes = [0, dt];
    return this.minmax(criticalTimes, this.getAccel);
  }


  public getPositionCrossings(position: number): number[] {
    const { c0: c0origin, c1, c2, c3, dt } = this;
    const c0 = c0origin - position;
    const roots = getCubicRoots(c3, c2, c1, c0);
    return roots.filter(r => r.imag === 0).map(r => r.real).filter(t => t >= 0 && t <= dt).sort();
  }


  public getVelocityCrossings(velocity: number): number[] {
    const { c1: c1orig, c2, c3, dt } = this;
    const c1 = c1orig - velocity;
    const det = c2 * c2 - 3 * c3 * c1;
    const result: number[] = [];
    if (c3 !== 0 && det >= 0) {
      if (det > 0) {
        const root = Math.sqrt(det);
        result.push((-c2 - root) / (3 * c3));
        result.push((-c2 + root) / (3 * c3));
      } else {
        result.push(-c2 / (3 * c3));
      }
    }

    return result.filter(t => t >= 0 && t <= dt).sort();
  }


  public getAccelerationCrossings(acceleration: number): number[] {
    const { c2, c3, dt } = this;
    const t = (acceleration - 2 * c2) / (6 * c3);
    return [t].filter(t => t >= 0 && t <= dt).sort();
  }


  // Returns times within the segment at which the final position
  // could reach a maximum if a constant deceleration were applied,
  // with said maximum position values.
  public getStopPositionPeakTimes(deceleration: number): Peak[] {
    const { c2, c3, dt } = this;
    let criticalTimes = [0, dt];
    if (c3 !== 0) {
      const t1 = (-deceleration - (2 * c2)) / 6 * c3;
      if (t1 >= 0 && t1 <= dt && this.getVel(t1) < 0) {
        criticalTimes.push(t1);
      }

      const t2 = (deceleration - (2 * c2)) / 6 * c3;
      if (t2 >= 0 && t2 <= dt && this.getVel(t2) > 0) {
        criticalTimes.push(t2);
      }
    }

    criticalTimes = _.sortedUniq(criticalTimes.sort());
    return criticalTimes.map(t => {
      const vel = this.getVel(t);
      return ({
        time: t,
        value: this.getPos(t) + 0.5 * Math.sign(vel) * vel ** 2 / deceleration,
      });
    });
  }


  private calcCoefficients() {
    const { p0, p1, dt, velocityScale } = this;
    if (p0.relative || p1.relative) {
      throw new Error('Endpoints must be absolute coordinates.');
    }

    // This comes from eq. 7.11 in https://ocw.snu.ac.kr/sites/default/files/NOTE/Chap07_Trajectory%20generation.pdf
    // for cubic polynomials with via points.
    const p0vel = p0.vel * velocityScale; // Velocity units must be the derivative of (ie on the same scale as) position.
    const p1vel = p1.vel * velocityScale;
    const t2 = dt * dt;
    const t3 = t2 * dt;
    this.c0 = p0.pos;
    this.c1 = p0vel;
    this.c2 = 3 * (p1.pos - p0.pos) / t2 - 2 * p0vel / dt - p1vel / dt;
    this.c3 = -2 * (p1.pos - p0.pos) / t3 + (p1vel + p0vel) / t2;
  }


  private minmax(times: number[], f: (t: number) => number): Range {
    if (times.length < 1) {
      throw new Error('No critical points found.');
    }

    const { dt } = this;
    let pMin: Peak | null = null;
    let pMax: Peak | null = null;
    for (const time of times) {
      if (time >= 0.0 && time <= dt) {
        const pCrit = { time, value: f(time) };
        pMin = minPeak(pMin, pCrit);
        pMax = maxPeak(pMax, pCrit);
      }
    }

    return {
      min: pMin!,
      max: pMax!,
    };
  }
}
