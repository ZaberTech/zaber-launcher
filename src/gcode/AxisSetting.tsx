import React from 'react';
import { Checkbox, HeaderCard, Text, SimpleSelect } from '@zaber/react-library';

import type { IdentifiedAxisInfo } from '../connection_manager';
import { useActions } from '../utils';

import { AxisSettings, GCODE_AXES } from './types';
import { actions as actionsDefinition } from './actions';


interface Props {
  axis: IdentifiedAxisInfo;
  axisIndex: number;
  settings: AxisSettings;
}

export const AxisSettingsCard: React.FC<Props> = ({ axis, axisIndex, settings }) => {
  const actions = useActions(actionsDefinition);
  const updateLetter = (letter: string | null) => {
    if (letter === '') {
      letter = null;
    }
    if (settings.lockstep) {
      for (const axisNumber of settings.lockstep.axisNumbers) {
        actions.updateAxisSettings(axisNumber - 1, { letter });
      }
    } else {
      actions.updateAxisSettings(axisIndex, { letter });
    }
  };
  return (<HeaderCard edge="outline" header={<>
    Axis {axisIndex + 1}&emsp;{axis.identity.peripheralName} {settings.lockstep && <>(Lockstep)</>}
  </>}>
    <div className="setting">
      <Text>G-Code Axis:</Text>
      <SimpleSelect
        value={settings.letter ?? ''}
        onValueChange={updateLetter}
        options={[{ value: '', label: 'Disabled' }, ...GCODE_AXES.map(letter => ({ value: letter, label: letter }))]}
        disabled={!settings.mobile} portalToBody/>
    </div>
    <div className="setting">
      <Text>Inverted:</Text>
      <Checkbox checked={settings.inverted} onChecked={value => actions.updateAxisSettings(axisIndex, { inverted: value })}/>
    </div>
  </HeaderCard>);
};
