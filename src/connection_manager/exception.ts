export class NoInternetConnectionException extends Error { }
export class DeviceDatabaseOfflineException extends Error { }
