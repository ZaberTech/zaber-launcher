import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Text, HeaderCard, Icons, NoticeBanner } from '@zaber/react-library';
import _ from 'lodash';
import type { Nullable } from '@zaber/toolbox';
import { match } from 'ts-pattern';

import type { RootState } from '../store';
import { SingleDeviceTable } from '../connection_manager/device_table/SingleDeviceTable';
import { NoContentMessage } from '../components';
import { fwToString } from '../devices';
import { environment } from '../environment';
import { Version } from '../app_components';

import { actions as actionsDefinition } from './actions';
import {
  selectDeviceInfoWithAxes,
  selectNotSkippedNewerVersions,
  selectOlderVersions,
  selectSkippedNewerVersions,
  selectOtherVersions,
  selectCurrentConnection,
} from './selectors';
import { FirmwareVersion } from './FirmwareVersion';
import { UpgradeProgressModal } from './UpgradeProgressModal';
import { DeviceFirmwareInfo } from './DeviceFirmwareInfo';
import { OtherUpgradeOptions } from './OtherUpgradeOptions';

interface DispatchProps {
  actions: typeof actionsDefinition;
}

interface StateProps {
  device: NonNullable<ReturnType<typeof selectDeviceInfoWithAxes>>;
  connection: NonNullable<ReturnType<typeof selectCurrentConnection>>;
  newerVersions: Version[];
  skippedVersions: Version[];
  olderVersions: Version[];
  otherVersions: ReturnType<typeof selectOtherVersions>;
}

class UpgradeDeviceFirmwareBase extends Component<DispatchProps & StateProps> {
  renderListOfVersions(name: string, versions: Nullable<Version[]>, newest?: string) {
    if (!versions?.length) { return null }
    const header = newest ?
      (<Text className="subsection-header" t={Text.Type.H4} e={Text.Emphasis.Red}><Icons.Dot/>{name}</Text>) :
      (<Text className="subsection-header" t={Text.Type.H4}>{name}</Text>);
    return (
      <HeaderCard header={header} edge="outline" data-testid={`section_${_.snakeCase(name)}`}>
        {versions.map(v => (<FirmwareVersion key={v.str} version={v} newest={v.str === newest}/>))}
      </HeaderCard>
    );
  }

  render(): React.ReactNode {
    const {
      device, connection, actions,
      newerVersions, skippedVersions, olderVersions, otherVersions,
    } = this.props;
    if (connection.state === 'loading') {
      return <NoContentMessage message="Checking for updates..." type="working"/>;
    } else if (device.identity.firmwareVersion.major < 7) {
      return <NoContentMessage
        bannerTitle="Update Unavailable" type="info"
        message={`The selected device has version ${fwToString(device.identity.firmwareVersion)}. Zaber Launcher does not
        support upgrading devices below firmware version 7. Use Zaber Console.`}
      />;
    }

    return <>
      <SingleDeviceTable device={device}>
        <DeviceFirmwareInfo/>
      </SingleDeviceTable>
      <hr/>
      <div className="section-header">
        <Text t={Text.Type.H4}>Firmware Versions</Text>
        <div className="spacer"/>
        {!environment.offline && <Icons.Refresh title="Refresh Versions" onClick={() => actions.refreshVersions(device.key)}/>}
      </div>
      {match(connection.state)
        .with('error', () => <NoticeBanner type="error">
          Error loading online firmware updates: {connection.error}
        </NoticeBanner>)
        .with('done', () => <>
          {this.renderListOfVersions('New Versions Available', newerVersions, newerVersions[0]?.str)}
          {this.renderListOfVersions('Skipped Versions', skippedVersions)}
          {this.renderListOfVersions('Older Versions', olderVersions)}
          {this.renderListOfVersions('Public Versions', otherVersions?.public)}
          {this.renderListOfVersions('Other Versions', otherVersions?.other)}
          {this.renderListOfVersions('Internal Versions', otherVersions?.internal)}
        </>).exhaustive()}
      <OtherUpgradeOptions/>
      <UpgradeProgressModal/>
    </>;
  }
}

export const UpgradeDeviceFirmware = connect<StateProps, DispatchProps, unknown, RootState>(
  state => ({
    device: selectDeviceInfoWithAxes(state)!,
    connection: selectCurrentConnection(state)!,
    newerVersions: selectNotSkippedNewerVersions(state),
    skippedVersions: selectSkippedNewerVersions(state),
    olderVersions: selectOlderVersions(state),
    otherVersions: selectOtherVersions(state),
  }),
  dispatch => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(UpgradeDeviceFirmwareBase);
