import { Text, HeaderCard, HelpTooltip, EditableField } from '@zaber/react-library';
import React, { useState } from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { match, P } from 'ts-pattern';
import { Angle, Length } from '@zaber/motion';

import { Thumbnail } from '../components/Thumbnail';
import { peripheralNameWithLabel, type IdentifiedAxisInfo } from '../connection_manager';

import { actions as actionsDefinition } from './actions';
import type { AxisDataLockstep } from './reducer';

/**
 * Sub-card of LockstepGroupCard
 */
export const AxisCard: React.FC<{ axisData: AxisDataLockstep; axis: IdentifiedAxisInfo }> = ({ axisData, axis }) => {
  const actions = useActions(actionsDefinition);
  const [enteredTwistTolerance, setEnteredTwistTolerance] = useState<string | null>(null);

  return (
    <HeaderCard
      header={<Text t={Text.Type.H4}>{`Axis ${axisData.axisNumber} ${peripheralNameWithLabel(axis)}`}</Text>}
      key={axisData.axisNumber}
      className="axis-card"
      title={`Axis ${axisData.axisNumber} Card`}
    >
      <Thumbnail className="picture" deviceOrPeripheralId={axis.identity.peripheralId} title={axis.identity.peripheralName} altExt="axis"/>
      {match(axisData.lockstepInfo)
        .with(P.nullish, () => <>
          <Text t={Text.Type.H5}>Not in Lockstep</Text>
          <HelpTooltip>
            This axis is no longer part of this lockstep group. Reload this device to update lockstep information.
          </HelpTooltip>
        </>)
        .with({ isPrimary: true }, () => <>
          <Text t={Text.Type.H5}>Axis Type</Text>
          <Text>Primary</Text>
        </>)
        .with({ isPrimary: false }, axisLockstepInfo => <>
          <Text t={Text.Type.H5}>Twist Tolerance</Text>
          <HelpTooltip>
            Maximum allowable twist. Twist is unexpected displacement relative to primary axis.
          </HelpTooltip>
          <EditableField
            value={enteredTwistTolerance ?? axisLockstepInfo.twistTolerance ?? ''}
            validate={(value: string) => (parseInt(value, 10) >= 0)}
            data-testid="input-tolerance"
            title=""
            onValueChange={value => {
              setEnteredTwistTolerance(value);
              actions.writeTwistTolerance(parseInt(value, 10), axisData.axisNumber);
            }}/>

          <Text t={Text.Type.H5}>Offset</Text>
          <HelpTooltip>Displacement relative to primary axis at creation.</HelpTooltip>
          {match(axisLockstepInfo.offset)
            .with({ units: Length.mm }, ({ value }) => <Text className="left-padded">{value.toFixed(2)}mm</Text>)
            .with({ units: Angle['°'] }, ({ value }) => <Text className="left-padded">{value.toFixed(2)}°</Text>)
            .otherwise(() => <Text className="left-padded">Undefined Offset</Text>)
          }
        </>).exhaustive()
      }
    </HeaderCard>
  );
};
