import type { Container } from 'inversify';
import type { ascii } from '@zaber/motion';
import _ from 'lodash';
import type { SagaIterator, Task } from 'redux-saga';
import { all, call, fork, select, join, SagaReturnType as SRT, put } from 'redux-saga/effects';

import { createContainer, destroyContainer } from '../container';
import { MessageRoutersService } from '../message_router';
import {
  AxisMockBase, DeviceMockBase, ConnectionMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { AppStore, buildStore } from '../store/build';
import type { RootState } from '../store';
import { makeAxisKey, makeDeviceKey } from '../keys';
import { defer, waitTick, waitUntilPass } from '../test';
import { mockIpc } from '../ipc/mock';

import { getConnection, getDevice, getAxis, getDeviceOrAxis, getAxisOrOnlyAxis, reidentifyDevices, reloadDevices } from './devices';
import { mockNonIdentifiedDevice, mockSingleDevice, mockSingleDeviceWithPeripherals } from './mocks';
import type { AxisState, ConnectionState, DeviceState } from './reducer';
import { actions } from './actions';

const loadDevicesActionMock = jest.spyOn(actions, 'loadDevices');

let container: Container;

class AxisMock extends AxisMockBase {
  identity?: Partial<ascii.AxisIdentity>;
  label = '';
}

class DeviceMock extends DeviceMockBase<AxisMock> {
  isIdentified = false;
  identity?: Partial<ascii.DeviceIdentity>;
  identify = jest.fn(async () => {
    this.isIdentified = true;
    return this.identity;
  });
  settings = { get: async () => 0 };
  label = '';
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  detectDevices = jest.fn(() => Promise.reject<unknown[]>(new Error('Connection broken')));
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

let store: AppStore;

beforeEach(() => {
  container = createContainer();
  store = buildStore();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  mockIpc(container);
});

afterEach(() => {
  destroyContainer();
  container = null!;
  store = null!;

  loadDevicesActionMock.mockClear();
});

describe('getDevice', () => {
  let connectionMock: ConnectionMock;
  let deviceMock: DeviceMock;
  let axisMock: AxisMock;

  let connectionState: ConnectionState;
  let deviceState: DeviceState;
  let axisState: AxisState;

  beforeEach(() => store.saga.run(function* (): SagaIterator {
    mockSingleDevice(store);
    const state = (yield select()) as RootState;
    connectionState = _.sample(state.connectionManager.connections)!;
    deviceState = _.sample(state.connectionManager.devices)!;
    axisState = _.sample(state.connectionManager.axes)!;

    connectionMock = yield call(getConnection, connectionState.key);
    deviceMock = connectionMock.getDevice(deviceState.address);
    axisMock = deviceMock.getAxis(axisState.axisNumber);

    deviceMock.identity = _.cloneDeep(deviceState.identity)!;
    axisMock.identity = _.cloneDeep(axisState.identity)!;
  }).toPromise());

  test('identifies and returns the device when getDevice is called', () =>
    store.saga.run(function* (): SagaIterator {
      const device = yield call(getDevice, deviceState.key);

      expect(device).toBe(deviceMock);
      expect(deviceMock.identify).toHaveBeenCalled();
    }).toPromise()
  );

  test('multiple simultaneous calls of getDevice results in a single identification', () =>
    store.saga.run(function* (): SagaIterator {
      const deferred = defer<DeviceMock['identity']>();
      deviceMock.identify.mockImplementation(() => deferred.promise);

      const tasks: Task[] = [];
      tasks.push(yield fork(getDevice, deviceState.key));
      tasks.push(yield fork(getDevice, deviceState.key));

      deferred.resolve(deviceState.identity!);

      const devices = yield all(tasks.map(task => join(task)));
      expect(devices[0]).toBe(deviceMock);
      expect(devices[1]).toBe(deviceMock);

      expect(deviceMock.identify).toHaveBeenCalledTimes(1);
    }).toPromise()
  );

  test('skips the identification if the device is already identified', () =>
    store.saga.run(function* (): SagaIterator {
      deviceMock.isIdentified = true;

      yield call(getDevice, deviceState.key);

      expect(deviceMock.identify).not.toHaveBeenCalled();
    }).toPromise()
  );

  test('calling reidentifyDevices will identify the device again despite it being already identified', () =>
    store.saga.run(function* (): SagaIterator {
      deviceMock.isIdentified = true;

      reidentifyDevices([deviceState.key]);
      yield call(getDevice, deviceState.key);

      expect(deviceMock.identify).toHaveBeenCalled();
    }).toPromise()
  );

  test('identifies unknown device', () =>
    store.saga.run(function* (): SagaIterator {
      const device: SRT<typeof getDevice> = yield call(getDevice, makeDeviceKey(connectionState.key, 99));
      expect(device.identify).toHaveBeenCalled();
    }).toPromise()
  );

  test('waits for connection manager to finish loading devices', () =>
    store.saga.run(function* (): SagaIterator {
      const deferredDetectDevices = defer<DeviceMock[]>();
      connectionMock.detectDevices.mockReturnValueOnce(deferredDetectDevices.promise);

      yield put(actions.loadDevices(connectionState.key, false));
      yield call(waitUntilPass, () => expect(connectionMock.detectDevices).toHaveBeenCalled());

      const task: Task = yield fork(getDevice, deviceState.key);

      yield call(waitTick, 3);
      expect(deviceMock.identify).not.toHaveBeenCalled(); // waiting on connection manager

      deferredDetectDevices.resolve([deviceMock]);

      const device: SRT<typeof getDevice> = yield join(task);
      expect(device.identify).toHaveBeenCalledTimes(1);
    }).toPromise()
  );

  describe('ZML identity does not match state', () => {
    function* runTest(): SagaIterator {
      try {
        yield call(getDevice, deviceState.key);
        throw new Error('No error thrown');
      } catch (err) {
        expect((err as Error).message).toBe('Connected device does not match expected device. Try again.');
      }

      yield call(waitUntilPass, () => expect(connectionMock.detectDevices).toHaveBeenCalled());
    }

    test('reloads devices and throws exception if the device identity does not match the state', () =>
      store.saga.run(function* (): SagaIterator {
        deviceMock.identity!.deviceId = 7488;

        yield call(runTest);
      }).toPromise()
    );

    test('reloads devices and throws exception if the axis identity does not match the state', () =>
      store.saga.run(function* (): SagaIterator {
        axisMock.identity!.peripheralId = 987;

        yield call(runTest);
      }).toPromise()
    );

    test('reloads devices and throws exception if the device label does not match the state', () =>
      store.saga.run(function* (): SagaIterator {
        deviceMock.label = 'New label';

        yield call(runTest);
      }).toPromise()
    );

    test('reloads devices and throws exception if the axis label does not match the state', () =>
      store.saga.run(function* (): SagaIterator {
        axisMock.label = 'New label';

        yield call(runTest);
      }).toPromise()
    );
  });

  describe('getAxis', () => {
    test('returns axis and identifies device', () =>
      store.saga.run(function* (): SagaIterator {
        const axis = yield call(getAxis, axisState.key);
        expect(axis).toBe(axisMock);
        expect(deviceMock.identify).toHaveBeenCalled();
      }).toPromise()
    );
  });

  describe('getDeviceOrAxis', () => {
    test('returns device or axis depending on the key', () =>
      store.saga.run(function* (): SagaIterator {
        let deviceOrAxis = yield call(getDeviceOrAxis, deviceState.key);
        expect(deviceOrAxis).toBeInstanceOf(DeviceMock);

        deviceOrAxis = yield call(getDeviceOrAxis, axisState.key);
        expect(deviceOrAxis).toBeInstanceOf(AxisMock);
      }).toPromise()
    );
  });

  describe('getAxisOrOnlyAxis', () => {
    test('returns axis for axis key or first axis for device key', () =>
      store.saga.run(function* (): SagaIterator {
        let axis: AxisMock = yield call(getAxisOrOnlyAxis, axisState.key);
        expect(axis).toBeInstanceOf(AxisMock);

        axis = yield call(getAxisOrOnlyAxis, deviceState.key);
        expect(axis).toBeInstanceOf(AxisMock);
        expect(axis.axisNumber).toBe(1);
      }).toPromise()
    );
  });
});

describe('multi-axes device', () => {
  let deviceMock: DeviceMock;
  let deviceState: DeviceState;

  beforeEach(() => store.saga.run(function* (): SagaIterator {
    mockSingleDeviceWithPeripherals(store);
    const state = (yield select()) as RootState;
    const connectionState = _.sample(state.connectionManager.connections)!;
    deviceState = _.sample(state.connectionManager.devices)!;

    const connectionMock = yield call(getConnection, connectionState.key);
    deviceMock = connectionMock.getDevice(deviceState.address);

    deviceMock.identity = _.cloneDeep(deviceState.identity)!;
    for (const axisState of Object.values(state.connectionManager.axes)) {
      deviceMock.getAxis(axisState.axisNumber).identity = _.cloneDeep(axisState.identity)!;
    }
  }).toPromise());

  describe('getAxisOrOnlyAxis', () => {
    test('returns desired axis for axis key', () =>
      store.saga.run(function* (): SagaIterator {
        const axisMock: AxisMock = yield call(getAxisOrOnlyAxis, makeAxisKey(deviceState.key, 3));
        expect(axisMock.axisNumber).toBe(3);
      }).toPromise()
    );
    test('throws error if device key is provided but device has multiple axes', () =>
      store.saga.run(function* (): SagaIterator {
        try {
          yield call(getAxisOrOnlyAxis, deviceState.key);
          throw new Error('No error thrown');
        } catch (err) {
          expect((err as Error).message).toBe('Device key provided for multi-axes device (4)');
        }
      }).toPromise()
    );
  });
});


describe('non-identified device', () => {
  let deviceState: DeviceState;

  beforeEach(() => store.saga.run(function* (): SagaIterator {
    mockNonIdentifiedDevice(store);
    const state = (yield select()) as RootState;
    deviceState = _.sample(state.connectionManager.devices)!;
  }).toPromise());

  test('allows to use non-identified devices when allowNonIdentified is true', () =>
    store.saga.run(function* (): SagaIterator {
      const device: SRT<typeof getDevice> = yield call(getDevice, deviceState.key, { allowNonIdentified: true });
      expect(device.identify).not.toHaveBeenCalled();
    }).toPromise()
  );

  test('calls identify if device is non identified and allowNonIdentified is not provided', () =>
    store.saga.run(function* (): SagaIterator {
      const device: SRT<typeof getDevice> = yield call(getDevice, deviceState.key);
      expect(device.identify).toHaveBeenCalled();
    }).toPromise()
  );
});

describe('reloadDevices', () => {
  let connectionState: ConnectionState;

  beforeEach(() => {
    mockSingleDeviceWithPeripherals(store);
    const state = store.getState();
    connectionState = _.sample(state.connectionManager.connections)!;
  });

  test('waits for connection to be loaded and returns devices', () =>
    store.saga.run(function* (): SagaIterator {
      loadDevicesActionMock.mockImplementationOnce(() => {
        setTimeout(() => mockSingleDeviceWithPeripherals(store));
        return ({ type: 'NOTHING', payload: undefined });
      });
      const devices: SRT<typeof reloadDevices> = yield call(reloadDevices, connectionState.key);
      expect(devices).toHaveLength(1);
      expect(devices![0].address).toBe(1);
    }).toPromise()
  );

  test('returns null if reloading fails', () =>
    store.saga.run(function* (): SagaIterator {
      loadDevicesActionMock.mockImplementationOnce((key: string) => {
        setTimeout(() => store.dispatch(actions.devicesLoadedErr(key, { message: 'Failed horribly', reason: 'other' })));
        return ({ type: 'NOTHING', payload: undefined });
      });
      const devices: SRT<typeof reloadDevices> = yield call(reloadDevices, connectionState.key);
      expect(devices).toBeNull();
    }).toPromise()
  );
});
