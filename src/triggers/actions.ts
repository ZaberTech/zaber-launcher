import type { DeviceIOInfo } from '@zaber/motion/ascii';

import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { TriggerActionState, TriggerConditionState } from './reducer';

export enum ActionTypes {
  DEVICE_MOUNT = 'TRIGGERS_DEVICE_MOUNT',
  DEVICE_UNMOUNT = 'TRIGGERS_DEVICE_UNMOUNT',
  SET_SELECTED_DEVICE = 'TRIGGERS_SET_SELECTED_DEVICE',
  SET_DEVICE_NO_TRIGGERS = 'TRIGGERS_SET_DEVICE_NO_TRIGGERS',
  SET_DEVICE_CAPABILITIES = 'TRIGGERS_SET_DEVICE_CAPABILITIES',
  SET_DEVICE_TRIGGER_EXPANDED = 'TRIGGERS_SET_DEVICE_TRIGGER_EXPANDED',
  SET_DEVICE_ERROR = 'TRIGGERS_SET_DEVICE_ERROR',
  INITIALIZE_TRIGGERS = 'TRIGGERS_INITIALIZE_TRIGGERS',
  SET_TRIGGER_ERROR = 'TRIGGERS_SET_TRIGGER_ERROR',
  ENABLE_TRIGGER = 'TRIGGERS_ENABLE_TRIGGER',
  DISABLE_TRIGGER = 'TRIGGERS_DISABLE_TRIGGER',
  SET_ENABLED_LOADING = 'TRIGGERS_SET_ENABLED_LOADING',
  REQUEST_DETAILS_DEVICE = 'TRIGGERS_REQUEST_DETAILS_DEVICE',
  SEND_DETAILS_DEVICE = 'TRIGGERS_SEND_DETAILS_DEVICE',
  SET_FIRES_TOTAL = 'TRIGGERS_SET_FIRES_TOTAL',
  STORE_LABEL = 'TRIGGERS_STORE_LABEL',
  SET_LABEL = 'TRIGGERS_SET_LABEL',
  SET_CONDITION_DEVICE = 'TRIGGERS_SET_CONDITION_DEVICE',
  SET_CONDITION_INPUT = 'TRIGGERS_SET_CONDITION_INPUT',
  SET_ACTION_DEVICE = 'TRIGGERS_SET_ACTION_DEVICE',
  SET_ACTION_INPUT = 'TRIGGERS_SET_ACTION_INPUT',
  RESET_INPUT = 'TRIGGERS_RESET_INPUT',
}

export interface ActionsToPayloads {
  [ActionTypes.DEVICE_MOUNT]: { deviceKey: EntityKey };
  [ActionTypes.DEVICE_UNMOUNT]: { deviceKey: EntityKey };
  [ActionTypes.SET_SELECTED_DEVICE]: { deviceKey: EntityKey | null };
  [ActionTypes.SET_DEVICE_NO_TRIGGERS]: { deviceKey: EntityKey };
  [ActionTypes.SET_DEVICE_CAPABILITIES]: { deviceKey: EntityKey; isController: boolean; numTriggers: number; ioInfo: DeviceIOInfo };
  [ActionTypes.SET_DEVICE_TRIGGER_EXPANDED]: { deviceKey: EntityKey; triggerIndex: number; expanded: boolean };
  [ActionTypes.SET_DEVICE_ERROR]: { deviceKey: EntityKey; error: string | null };
  [ActionTypes.INITIALIZE_TRIGGERS]: { numTriggers: number};
  [ActionTypes.SET_TRIGGER_ERROR]: { triggerIndex: number; error: string | null };
  [ActionTypes.ENABLE_TRIGGER]: { triggerIndex: number; fires: number };
  [ActionTypes.DISABLE_TRIGGER]: { triggerIndex: number };
  [ActionTypes.SET_ENABLED_LOADING]: { triggerIndex: number; loading: boolean };
  [ActionTypes.REQUEST_DETAILS_DEVICE]: { triggerIndex: number; initialize: boolean };
  [ActionTypes.SEND_DETAILS_DEVICE]: { triggerIndex: number };
  [ActionTypes.SET_FIRES_TOTAL]: { triggerIndex: number; firesTotal: number };
  [ActionTypes.STORE_LABEL]: { triggerIndex: number; label: string };
  [ActionTypes.SET_LABEL]: { triggerIndex: number; label: string };
  [ActionTypes.SET_CONDITION_DEVICE]: { triggerIndex: number; state: TriggerConditionState };
  [ActionTypes.SET_CONDITION_INPUT]: { triggerIndex: number; state: TriggerConditionState; inputChanged: boolean };
  [ActionTypes.SET_ACTION_DEVICE]: { triggerIndex: number; actionIndex: number; state: TriggerActionState };
  [ActionTypes.SET_ACTION_INPUT]: { triggerIndex: number; actionIndex: number; state: TriggerActionState; inputChanged: boolean };
  [ActionTypes.RESET_INPUT]: { triggerIndex: number };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  deviceMount: (deviceKey: EntityKey) => buildAction(ActionTypes.DEVICE_MOUNT, { deviceKey }),
  deviceUnmount: (deviceKey: EntityKey) => buildAction(ActionTypes.DEVICE_UNMOUNT, { deviceKey }),
  setSelectedDevice: (deviceKey: EntityKey | null) => buildAction(ActionTypes.SET_SELECTED_DEVICE, { deviceKey }),
  setDeviceNoTriggers: (deviceKey: EntityKey) => buildAction(ActionTypes.SET_DEVICE_NO_TRIGGERS, { deviceKey }),
  setDeviceCapabilities: (deviceKey: EntityKey, isController: boolean, numTriggers: number, ioInfo: DeviceIOInfo) =>
    buildAction(ActionTypes.SET_DEVICE_CAPABILITIES, { deviceKey, isController, numTriggers, ioInfo }),
  setDeviceTriggerExpanded: (deviceKey: EntityKey, triggerIndex: number, expanded: boolean) =>
    buildAction(ActionTypes.SET_DEVICE_TRIGGER_EXPANDED, { deviceKey, triggerIndex, expanded }),
  setDeviceError: (deviceKey: EntityKey, error: string | null) => buildAction(ActionTypes.SET_DEVICE_ERROR, { deviceKey, error }),
  initializeTriggers: (numTriggers: number) => buildAction(ActionTypes.INITIALIZE_TRIGGERS, { numTriggers }),
  setTriggerError: (triggerIndex: number, error: string | null) => buildAction(ActionTypes.SET_TRIGGER_ERROR, { triggerIndex, error }),
  enableTrigger: (triggerIndex: number, fires: number) => buildAction(ActionTypes.ENABLE_TRIGGER, { triggerIndex, fires }),
  disableTrigger: (triggerIndex: number) => buildAction(ActionTypes.DISABLE_TRIGGER, { triggerIndex }),
  setEnabledLoading: (triggerIndex: number, loading: boolean) => buildAction(ActionTypes.SET_ENABLED_LOADING, { triggerIndex, loading }),
  requestDetailsDevice: (triggerIndex: number, initialize: boolean) =>
    buildAction(ActionTypes.REQUEST_DETAILS_DEVICE, { triggerIndex, initialize }),
  sendDetailsDevice: (triggerIndex: number) => buildAction(ActionTypes.SEND_DETAILS_DEVICE, { triggerIndex }),
  setFiresTotal: (triggerIndex: number, firesTotal: number) => buildAction(ActionTypes.SET_FIRES_TOTAL, { triggerIndex, firesTotal }),
  storeLabel: (triggerIndex: number, label: string) => buildAction(ActionTypes.STORE_LABEL, { triggerIndex, label }),
  setLabel: (triggerIndex: number, label: string) => buildAction(ActionTypes.SET_LABEL, { triggerIndex, label }),
  setConditionDevice: (triggerIndex: number, state: TriggerConditionState) =>
    buildAction(ActionTypes.SET_CONDITION_DEVICE, { triggerIndex, state }),
  setConditionInput: (triggerIndex: number, state: TriggerConditionState, inputChanged: boolean) =>
    buildAction(ActionTypes.SET_CONDITION_INPUT, { triggerIndex, state, inputChanged }),
  setActionDevice: (triggerIndex: number, actionIndex: number, state: TriggerActionState) =>
    buildAction(ActionTypes.SET_ACTION_DEVICE, { triggerIndex, actionIndex, state }),
  setActionInput: (triggerIndex: number, actionIndex: number, state: TriggerActionState, inputChanged: boolean) =>
    buildAction(ActionTypes.SET_ACTION_INPUT, { triggerIndex, actionIndex, state, inputChanged }),
  resetInput: (triggerIndex: number) => buildAction(ActionTypes.RESET_INPUT, { triggerIndex }),
};
