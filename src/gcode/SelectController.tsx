import React from 'react';
import { Button } from '@zaber/react-library';
import { routerActions as routerActionsDefinition } from 'connected-react-router';

import { ConnectionTable } from '../connection_manager';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { Paths } from './types';


export const SelectController: React.FC = () => {
  const routerActions = useActions(routerActionsDefinition);
  const actions = useActions(actionsDefinition);
  return <ConnectionTable
    deviceContent={device => {
      if (device.axes.every(axis => !axis.canMove)) {
        return null;
      }
      return <Button onClick={() => {
        actions.selectDevice(device.key);
        routerActions.replace(Paths.TranslatorSettings);
      }}>Select</Button>;
    }}
  />;
};
