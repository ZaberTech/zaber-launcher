import { call, select, put, race, take, spawn } from 'redux-saga/effects';
import { throwUnexpectedError } from '@zaber/toolbox';
import {
  Length, Velocity, ascii, microscopy, Units,
  MovementInterruptedException, Time, NoValueForKeyException,
} from '@zaber/motion';
import _ from 'lodash';
import type { Task } from 'redux-saga';

import { type SagaIter, type RT, type Action, delay, AnyAction } from '../utils';
import { MeasurementOK, convertBetweenUnits, getDefaultDimensionUnits } from '../units';
import { getContainer } from '../container';
import { TimeMeasuring } from '../time';

import { ActionTypes, ActionsToPayloads, actions } from './actions';
import { selectAxes, selectMonitorWillRefresh, selectTriggerSettingsValues } from './selectors';
import {
  Axes, FOCUS_OFFSET_KEY_PREFIX, INDEX_POS_KEY_PREFIX, MONITOR_BURST_DELAY, MonitorStableInfo, ObjectiveParameters,
  Operations, OperationsArgs, OperationsResult,
  POSITION_DIMENSION, TRIGGER_SETTINGS_FIELDS, TRIGGER_SETTINGS_PREFIX, VELOCITY_DIMENSION,
} from './types';
import { AutofocusProvider } from './utils';

export function* refreshMonitorAndWait(): SagaIter {
  yield put(actions.refreshMonitor());
  while (yield select(selectMonitorWillRefresh)) {
    yield take(ActionTypes.MONITOR_INFO);
  }
}

function takeOperationStart(operation: Operations) {
  return take((action: AnyAction) =>
    action.type === ActionTypes.OPERATION_START && (action as StartAction).payload.name === operation);
}

type OperationsCallback<Key extends keyof OperationsArgs> = (
  context: MicroscopeContext,
  args: OperationsArgs[Key],
) => SagaIter;
const OPERATION_CALLBACKS: { [K in keyof OperationsArgs]: OperationsCallback<K> } = {
  changeFilter,
  changeObjective,
  channelOn,
  channelSetIntensity,
  moveX,
  moveY,
  moveFocus,
  moveAbsolute,
  moveToStored,
  initialize,
  filterChangerSettings,
  objectiveChangerSettings,
  restart,
  autofocusZero,
  autofocusFocusLoop,
  autofocusFocusOnce,
  autofocusSetSettings,
  autofocusGetSettings,
  autofocusStop,
  autofocusLaserOnOff,
  autofocusSetLimit,
  triggerCamera,
  saveTriggerSettings,
};

export interface MicroscopeContext {
  scope: microscopy.Microscope;
  devices: ascii.Device[];
  stableInfo: MonitorStableInfo;
  autofocusProvider: AutofocusProvider | null;
}

type StartAction = Action<ActionsToPayloads[ActionTypes.OPERATION_START]>;

export function* operationStart(
  context: MicroscopeContext,
  { payload: { name, args } }: StartAction,
): SagaIter {
  try {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/no-unsafe-argument
    const result = yield OPERATION_CALLBACKS[name](context, args as any);

    yield put(actions.refreshMonitor());
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    yield put(actions.operationDone(name, result));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.refreshMonitor());
    yield put(actions.operationDoneErr(name, err.message));
  }
}

function* initialize(
  { scope, stableInfo: { xAxis, yAxis } }: MicroscopeContext,
): SagaIter {
  yield scope.initialize();
  if (scope.plate && xAxis && yAxis) {
    const xCenter = (xAxis.max + xAxis.min) / 2;
    const yCenter = (yAxis.max + yAxis.min) / 2;
    yield scope.plate.moveAbsolute({ value: xCenter }, { value: yCenter });
  }
  yield refreshMonitorAndWait();
}

function* changeFilter({ scope }: MicroscopeContext, { filter, home }: OperationsArgs[Operations.changeFilter]): SagaIter {
  const axis = scope.filterChanger!.device.getAxis(1);
  if (home || !(yield axis.isHomed())) {
    yield axis.home();
  }
  yield scope.filterChanger!.change(filter);
}

export async function changeObjectiveBetweenObjectives(
  scope: microscopy.Microscope,
  info: NonNullable<MonitorStableInfo['objectiveChanger']>,
  newObjective: number,
) {
  const changer = scope.objectiveChanger;
  if (changer == null) { throw new Error('Objective changer not available') }

  const focusAxis = changer.focusAxis;
  const { focusOffsets, focusDatum } = info;

  const currentObjective = await changer.getCurrentObjective();
  const focusHomed = await focusAxis.isHomed();

  let positionAfter = 0;
  if (currentObjective > 0 && focusHomed) {
    positionAfter = await focusAxis.getPosition();
    positionAfter -= focusDatum;

    const oldOffset = focusOffsets[currentObjective - 1];
    const oldOffsetNative = focusAxis.settings.convertToNativeUnits('pos', oldOffset.value, oldOffset.units);
    positionAfter -= oldOffsetNative;

    positionAfter = Math.max(positionAfter, 0);
  }

  const newOffset = focusOffsets[newObjective - 1];
  const newOffsetNative = focusAxis.settings.convertToNativeUnits('pos', newOffset.value, newOffset.units);
  positionAfter += newOffsetNative;

  await changer.change(newObjective, { focusOffset: { value: positionAfter } });
}

function* changeObjective(
  { scope, stableInfo }: MicroscopeContext,
  { objective }: OperationsArgs[Operations.changeObjective],
): SagaIter {
  if (stableInfo?.objectiveChanger == null) { return }
  yield changeObjectiveBetweenObjectives(scope, stableInfo.objectiveChanger, objective);
}

function* channelOn({ scope }: MicroscopeContext, { channel, on }: OperationsArgs[Operations.channelOn]): SagaIter {
  yield scope.illuminator!.getChannel(channel).setOn(on);
}

function* channelSetIntensity(
  { scope }: MicroscopeContext,
  { channel, intensity }: OperationsArgs[Operations.channelSetIntensity],
): SagaIter {
  yield scope.illuminator!.getChannel(channel).setIntensity(intensity);
}

function* moveX(ctx: MicroscopeContext, args: OperationsArgs[Operations.moveX]): SagaIter {
  yield moveAxis(ctx, 'xAxis', args);
}
function* moveY(ctx: MicroscopeContext, args: OperationsArgs[Operations.moveY]): SagaIter {
  yield moveAxis(ctx, 'yAxis', args);
}
function* moveFocus(ctx: MicroscopeContext, args: OperationsArgs[Operations.moveFocus]): SagaIter {
  yield moveAxis(ctx, 'focusAxis', args);
}

function* moveAxis(ctx: MicroscopeContext, axisName: Axes, args: OperationsArgs[Operations.moveX]): SagaIter {
  const axis = ctx.scope[axisName];
  if (axis == null) { return }

  try {
    switch  (args.move) {
      case 'gradual':
        yield moveAxisGradual(axis, axisName, args.positive, ctx);
        break;
      case 'steady':
        yield moveAxisSteady(axis, axisName, args.positive, ctx);
        break;
    }
  } catch (err) {
    if (err instanceof MovementInterruptedException) {
      return;
    }
    throw err;
  }
}

const getDefaultUnits = _.memoize(() => ({
  position: getDefaultDimensionUnits(POSITION_DIMENSION) as Length,
  velocity: getDefaultDimensionUnits(VELOCITY_DIMENSION) as Velocity,
}));

export const FIRST_STEP_TIME = 500;
export const SPEED_INCREASE_DELAY = 500;

function* moveAxisGradual(axis: ascii.Axis, axisName: Axes, positive: boolean, { stableInfo }: MicroscopeContext): SagaIter {
  yield race({
    move: call(function* move() {
      const info = stableInfo[axisName]!;
      const defaultUnits = getDefaultUnits();

      const axesStates: RT<typeof selectAxes> = yield select(selectAxes);
      const step: MeasurementOK = axesStates[axisName].step;
      const stepDefault: MeasurementOK = {
        value: convertBetweenUnits(step.value, step.units, defaultUnits.position),
        units: defaultUnits.position,
      };

      const time = getContainer().get(TimeMeasuring);
      const start = time.now();

      const direction = positive ? 1 : -1;
      yield axis.moveRelative(stepDefault.value * direction, stepDefault.units as Length);

      const stepTime = time.now() - start;
      if (stepTime < FIRST_STEP_TIME) {
        yield delay(FIRST_STEP_TIME - stepTime);
      }

      const maxSpeed = axis.settings.convertFromNativeUnits('maxspeed', info.maxSpeed, defaultUnits.velocity);
      let speed = stepDefault.value * (1000 / FIRST_STEP_TIME);
      for (;;) {
        speed = Math.min(speed, maxSpeed);
        yield axis.moveVelocity(speed * direction, defaultUnits.velocity);
        speed *= 1.5;

        yield put(actions.refreshMonitor(1000 / MONITOR_BURST_DELAY));
        yield delay(SPEED_INCREASE_DELAY);
      }
    }),
    stop: take(actions.stopAxisMovement(axisName).type),
  });

  yield axis.stop();
}

export const STEADY_SPEED = 0.10; // as % of the travel range per second

function* moveAxisSteady(axis: ascii.Axis, axisName: Axes, positive: boolean, { stableInfo }: MicroscopeContext): SagaIter {
  yield race({
    move: call(function* move() {
      const info = stableInfo[axisName]!;
      const defaultUnits = getDefaultUnits();
      const maxSpeed = axis.settings.convertFromNativeUnits('maxspeed', info.maxSpeed, defaultUnits.velocity);

      const direction = positive ? 1 : -1;
      const travel = axis.settings.convertFromNativeUnits('pos', info.max - info.min, defaultUnits.position);
      const speed = Math.min(travel * STEADY_SPEED, maxSpeed);
      yield axis.moveVelocity(speed * direction, defaultUnits.velocity);

      for (;;) {
        yield put(actions.refreshMonitor(1000 / MONITOR_BURST_DELAY));
        yield delay(1000);
      }
    }),
    stop: take(actions.stopAxisMovement(axisName).type),
  });

  yield axis.stop();
}

function* moveAbsolute({ scope }: MicroscopeContext, { axis, position }: OperationsArgs[Operations.moveAbsolute]): SagaIter {
  yield scope[axis]!.moveAbsolute(position.value, position.units as Length);
}

function* moveToStored({ scope }: MicroscopeContext, { stored, axes }: OperationsArgs[Operations.moveToStored]): SagaIter {
  yield Promise.all(
    axes.map(axis => scope[axis]?.moveAbsolute(stored[axis].value, stored[axis].units as Length))
  );
}

function* filterChangerSettings(
  { scope }: MicroscopeContext,
  { labels }: OperationsArgs[Operations.filterChangerSettings],
): SagaIter {
  const axis = scope.filterChanger!.device.getAxis(1);
  yield setIndexPositionLabels(axis, labels);

  yield put(actions.restartMonitor());
}

function* objectiveChangerSettings(
  { scope }: MicroscopeContext,
  { labels, focusOffsets }: OperationsArgs[Operations.objectiveChangerSettings],
): SagaIter {
  const changerAxis = scope.objectiveChanger!.turret.getAxis(1);
  yield setIndexPositionLabels(changerAxis, labels);

  const focusAxis = scope.objectiveChanger!.focusAxis;
  yield setFocusOffsets(focusAxis, focusOffsets);

  yield put(actions.restartMonitor());
}

async function setFocusOffsets(axis: ascii.Axis, offsets: MeasurementOK[]) {
  for (let i = 0; i < offsets.length; i++) {
    const offset = offsets[i];
    const offsetNative = Math.round(axis.settings.convertToNativeUnits('pos', offset.value, offset.units));
    if (offsetNative !== 0) {
      await axis.storage.setNumber(FOCUS_OFFSET_KEY_PREFIX + (i + 1), offsetNative);
    } else {
      await axis.storage.eraseKey(FOCUS_OFFSET_KEY_PREFIX + (i + 1));
    }
  }
}

async function setIndexPositionLabels(axis: ascii.Axis, labels: string[]) {
  for (let i = 0; i < labels.length; i++) {
    const label = labels[i];
    if (label) {
      await axis.storage.setString(INDEX_POS_KEY_PREFIX + (i + 1), label, { encode: true });
    } else {
      await axis.storage.eraseKey(INDEX_POS_KEY_PREFIX + (i + 1));
    }
  }
}

async function listIndexedKeys(storage: ascii.AxisStorage, prefix: string, count: number) {
  const keys = await storage.listKeys({ prefix });
  const indices = keys
    .map(key => +key.slice(prefix.length))
    .filter(key => Number.isInteger(key) && key >= 1 && key <= count);
  return indices;
}

export async function getIndexPositionLabels(axis: ascii.Axis, count: number): Promise<string[]> {
  const indices = await listIndexedKeys(axis.storage, INDEX_POS_KEY_PREFIX, count);
  const labels: string[] = _.range(count).map(() => '');
  for (const position of indices) {
    const label = await axis.storage.getString(INDEX_POS_KEY_PREFIX + position, { decode: true });
    labels[position - 1] = label;
  }
  return labels;
}

export async function getFocusOffsets(focusAxis: ascii.Axis, count: number, units: Units): Promise<MeasurementOK[]> {
  const indices = await listIndexedKeys(focusAxis.storage, FOCUS_OFFSET_KEY_PREFIX, count);
  const focusOffsets: MeasurementOK[] = _.range(count).map(() => ({ units, value: 0 }));
  for (const objective of indices) {
    const offsetNative: RT<typeof focusAxis.storage.getNumber> =
      await focusAxis.storage.getNumber(FOCUS_OFFSET_KEY_PREFIX + objective);
    focusOffsets[objective - 1] = {
      value: focusAxis.settings.convertFromNativeUnits('pos', offsetNative, units),
      units,
    };
  }
  return focusOffsets;
}

function* restart({ scope, devices }: MicroscopeContext): SagaIter {
  yield scope.connection.genericCommandNoResponse('system reset');
  // spawning new task as the current saga will be cancelled due to monitor error
  const task: Task = yield spawn(function* () {
    try {
      for (const device of devices) {
        yield device.waitToRespond(5000);
      }
      yield put(actions.restartMonitor());
    } catch (err) {
      throwUnexpectedError(err);
      // ignored
    }
  });
  yield task.toPromise();
}

function* autofocusZero({ scope }: MicroscopeContext): SagaIter {
  yield scope.autofocus!.setFocusZero();
}

function* autofocusFocusLoop({ scope }: MicroscopeContext): SagaIter {
  yield scope.autofocus!.startFocusLoop();
  let isBusy = true;
  do {
    const [slept] = yield race([
      delay(1000),
      takeOperationStart(Operations.autofocusStop),
      takeOperationStart(Operations.moveFocus),
    ]);
    if (!slept) { return }
    isBusy = yield scope.focusAxis!.isBusy();
  } while (isBusy);
  yield scope.autofocus!.stopFocusLoop();
}

function* autofocusFocusOnce(
  { scope }: MicroscopeContext,
  { scan }: OperationsArgs[Operations.autofocusFocusOnce],
): SagaIter {
  try {
    yield scope.autofocus!.focusOnce(scan);
  } catch (err) {
    if (err instanceof MovementInterruptedException) {
      return;
    }
    throw err;
  }
}

function* autofocusStop({ scope }: MicroscopeContext): SagaIter {
  yield scope.autofocus!.stopFocusLoop();
}

function* autofocusGetSettings(
  { scope, stableInfo }: MicroscopeContext,
): SagaIter<OperationsResult<Operations.autofocusGetSettings>> {
  const autofocus = scope.autofocus!;
  const objectiveCount = stableInfo.objectiveChanger?.numberOf ?? 1;
  const objectives: ObjectiveParameters[] = [];

  for (let i = 1; i <= objectiveCount; i++) {
    const namedParams: RT<typeof autofocus.getObjectiveParameters> = yield autofocus.getObjectiveParameters(i);
    const parameters = _.fromPairs(namedParams.map(param => ([param.name, param.value ?? null]))) as ObjectiveParameters;
    objectives.push(parameters);
  }

  return { objectives };
}

function* autofocusSetSettings(
  { scope }: MicroscopeContext,
  { objectives }: OperationsArgs[Operations.autofocusSetSettings],
): SagaIter {
  for (let i = 0; i < objectives.length; i++) {
    const parameters = Object.entries(objectives[i]).map(([name, value]) => ({ name, value }));
    yield scope.autofocus!.setObjectiveParameters(i + 1, _.sortBy(parameters, 'name'));
  }

  yield put(actions.setAutofocusSettingsOpen(false));
}

function* autofocusLaserOnOff(
  { autofocusProvider }: MicroscopeContext,
  { on }: OperationsArgs[Operations.autofocusLaserOnOff]): SagaIter {
  if (on) {
    yield autofocusProvider!.enableLaser();
  } else {
    yield autofocusProvider!.disableLaser();
  }
}

function* autofocusSetLimit(
  { scope }: MicroscopeContext,
  { limit, position }: OperationsArgs[Operations.autofocusSetLimit]): SagaIter {
  const settings = scope.focusAxis!.settings;
  // cannot convert because of missing FW for now
  const native = settings.convertToNativeUnits('pos', position.value, position.units);
  yield settings.set(`motion.tracking.limit.${limit}`, native);
}

function* triggerCamera({ scope }: MicroscopeContext): SagaIter {
  yield triggerScopeCamera(scope);
}

export function* triggerScopeCamera(scope: microscopy.Microscope): SagaIter {
  const { cameraTrigger } = scope;
  if (!cameraTrigger) { return }

  const settings: RT<typeof selectTriggerSettingsValues> = yield select(selectTriggerSettingsValues);
  const { exposure, waitBefore, waitAfter } = settings;

  if (waitBefore.value > 0) {
    const beforeMs = convertBetweenUnits(waitBefore.value, waitBefore.units, Time.MILLISECONDS);
    yield delay(beforeMs);
  }

  yield cameraTrigger.trigger(exposure.value, exposure.units as Time);

  if (waitAfter.value > 0) {
    const afterMs = convertBetweenUnits(waitAfter.value, waitAfter.units, Time.MILLISECONDS);
    yield delay(afterMs);
  }
}

export function* runInitialOperations(context: MicroscopeContext): SagaIter {
  if (context.scope.autofocus) {
    yield put(actions.operationStart(Operations.autofocusGetSettings));
  }
}

const TRIGGER_SETTINGS_STORAGE_UNITS = Time.MILLISECONDS;
const TRIGGER_SETTINGS_STORAGE_KEYS = {
  exposure: `${TRIGGER_SETTINGS_PREFIX}exposure`,
  waitBefore: `${TRIGGER_SETTINGS_PREFIX}wait_before`,
  waitAfter: `${TRIGGER_SETTINGS_PREFIX}wait_after`,
};

export function* loadTriggerSettings(
  scope: microscopy.Microscope,
): SagaIter {
  const trigger = scope.cameraTrigger;
  if (!trigger) { throw new Error('Trigger not available') }

  const storage = trigger.device.storage;
  const settings = {
    exposure: { value: 50, units: TRIGGER_SETTINGS_STORAGE_UNITS },
    waitBefore: { value: 0, units: TRIGGER_SETTINGS_STORAGE_UNITS },
    waitAfter: { value: 0, units: TRIGGER_SETTINGS_STORAGE_UNITS },
  };

  for (const key of TRIGGER_SETTINGS_FIELDS) {
    try {
      const value: RT<typeof storage.getNumber> = yield storage.getNumber(TRIGGER_SETTINGS_STORAGE_KEYS[key]);
      settings[key].value = value;
    } catch (err) {
      if (!(err instanceof NoValueForKeyException)) {
        throw err;
      }
    }
  }

  yield put(actions.updateTriggerSettings(settings));
}

export function* saveTriggerSettings(
  { scope }: MicroscopeContext,
  { settings }: OperationsArgs[Operations.saveTriggerSettings]): SagaIter {
  const trigger = scope.cameraTrigger;
  if (!trigger) { return }

  for (const key of TRIGGER_SETTINGS_FIELDS) {
    const value = convertBetweenUnits(settings[key].value, settings[key].units, TRIGGER_SETTINGS_STORAGE_UNITS);
    yield trigger.device.storage.setNumber(TRIGGER_SETTINGS_STORAGE_KEYS[key], value);
  }

  yield put(actions.setTriggerSettingsOpen('save'));
}
