import React from 'react';
import { useSelector } from 'react-redux';
import { Icons } from '@zaber/react-library';

import { selectProductionMode } from '../selectors';

export const NextPrevious: React.FC<{
  current: number;
  changeTo: (what: number) => void;
  numberOf: number;
}> = ({ changeTo, numberOf, current }) => {
  const productionMode = useSelector(selectProductionMode);
  if (!productionMode) { return null }

  return <div className="next-prev">
    <Icons.LeftNormal title="Previous" onClick={() => changeTo((current - 2 + numberOf) % numberOf + 1)}/>
    <Icons.RightNormal title="Next" onClick={() => changeTo(current /* -1 + 1 */ % numberOf + 1)}/>
  </div>;
};
