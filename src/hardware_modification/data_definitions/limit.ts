import type { SectionValues } from '../types';

type LimitSectionValues = SectionValues<'limitHome'> | SectionValues<'limitC'> | SectionValues<'limitAway'>;

export function limitEnabled(values: LimitSectionValues): boolean {
  return values.type?.is !== 0;
}

export function widthEnabled(values: LimitSectionValues): boolean {
  return values.type?.is !== 0 && values.source?.is !== 0;
}

export function actionEnabled(values: LimitSectionValues): boolean {
  return values.type?.is !== 0 && values.action?.is !== 0;
}

export function actionRequiresPreset(values: LimitSectionValues): boolean {
  const isActionThatRequiresPreset = values.action?.is !== 0 && values.action?.is !== 1;
  return values.type?.is !== 0 && isActionThatRequiresPreset;
}
