import React from 'react';
import { Button, HeaderCard, Text } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { actions as actionsDefinition } from './actions';

export const OtherUpgradeOptions: React.FC = () => {
  const actions = useActions(actionsDefinition);

  return <HeaderCard
    header={<Text t={Text.Type.H4}>Other</Text>}
  >
    <div className="firmware-version">
      <Text t={Text.Type.Body}>Install From File</Text>
      <span className="spacer"/>
      <Button
        color="grey"
        title="Install from File"
        onClick={actions.openFile}
      >Choose File</Button>
    </div>
  </HeaderCard>;
};
