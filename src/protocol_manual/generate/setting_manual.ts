import type { ManualProfilingData, SettingManual } from '../types';

import { parsedManual, profileElement, injectLinks, pointImgToSource, stripNestedArticles, stripZaber } from './profile';

export function generateSettingManual(id: string, profile: ManualProfilingData): SettingManual {
  if (profile.firmwareVersion.major < 7) {
    return { error: `Documentation upsupported for devices with FW${profile.firmwareVersion.major}` };
  }

  const body = parsedManual();

  const selector = `#topic_setting_${id.replace(/\./g, '_')}`;
  const articleInBody = body.querySelector(selector);
  if (!articleInBody) {
    return { error: `Could not find documentation for ${id}` };
  }

  const article = articleInBody.cloneNode(true) as HTMLElement;
  profileElement(article, profile.includes);
  injectLinks(article, profile.baseUrl);
  pointImgToSource(article);
  stripNestedArticles(article);
  stripZaber(article);
  return { html: article.outerHTML };
}
