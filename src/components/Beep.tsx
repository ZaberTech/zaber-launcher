import React, { useLayoutEffect, useRef, useState } from 'react';

import beep from '../assets/beep.mp3';
import { environment } from '../environment';

export const Beep: React.FC<{ play: boolean }> = ({ play }) => {
  const ref = useRef<HTMLMediaElement>(null);
  const [loaded, setLoaded] = useState(false);
  const [error, setError] = useState<string | null>(null);

  useLayoutEffect(() => {
    if (environment.isTest) { return }

    if (play) {
      ref.current?.play().then(() => setError(null), err => setError(`${err}`));
    } else {
      ref.current?.pause();
    }
  }, [play]);

  return <>
    {!loaded && play && <>No sound</>}
    {error != null && <>Audio Error: {error}</>}
    <audio src={beep} ref={ref} loop preload="auto" onCanPlayThrough={() => setLoaded(true)}/>
    {environment.isTest && <>beep-{play ? 'sound' : 'silence'}</>}
  </>;
};
