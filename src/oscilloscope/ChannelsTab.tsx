import _ from 'lodash';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Chevron, ColorPicker, Flex, HeaderCard, iconFromSvg, Icons, Text, TextType } from '@zaber/react-library';

import AxisRightSvg from '../assets/axis_right.svg';
import AxisLeftSvg from '../assets/axis_left.svg';
import { selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import { makeAxisKey } from '../keys';
import { selectOscilloscope } from '../store';
import { compareFirmwareVersions } from '../app_components';
import { useActions } from '../utils';

import { selectAllChannels, selectIsCaptureInProgress, selectSelectedDeviceState, selectSelectedRecording } from './selectors';
import { actions as actionDefinitions } from './actions';
import {
  Channel,
  YAxisId
} from './types';
import { ChannelMathDialog } from './ChannelMathDialog';
import {
  extractAxisChannels,
  extractIoChannels,
  extractMathChannels,
  extractSettingsChannels,
  isIoChannel,
  isMathChannel,
  numFwChannelsUsed
} from './channels';
import { isNewRecTab } from './utils';
import { DataSourceDialog } from './DataSourceDialog';
import { IoChannelDialog } from './IoChannelDialog';
import type { PerDeviceState, Recording } from './reducers';


const AxisLeftIcon = iconFromSvg(AxisLeftSvg);
const AxisRightIcon = iconFromSvg(AxisRightSvg);


interface ChannelRowProps {
  recordingIndex: number;
  channel: Channel;
  closeable?: boolean;
}

const ChannelRow: React.FC<ChannelRowProps> = ({ recordingIndex, channel, closeable }) => {
  const actions = useActions(actionDefinitions);
  const capturing = useSelector(selectIsCaptureInProgress);

  const showClose = (closeable ?? true) && !capturing;
  const isEnabled = !capturing;

  return <>
    {!channel.showInChart &&
      <Icons.Hide className="channel visibility"
        disabled={!isEnabled}
        onClick={() => actions.setChannelChartVisibility(recordingIndex, channel.id, true)}
        data-testid={`show-channel-${channel.id}`}/>}
    {channel.showInChart &&
      <Icons.Show className="channel visibility"
        disabled={!isEnabled}
        onClick={() => actions.setChannelChartVisibility(recordingIndex, channel.id, false)}
        data-testid={`hide-channel-${channel.id}`}/>}

    <ColorPicker color={channel.color}
      disabled={!isEnabled}
      onChange={color => actions.setChannelColor(recordingIndex, channel.id, color)}
      data-testid={`color-channel-${channel.id}`}/>

    <div className="channel label">
      <Text t={Text.Type.BodyLg}>{channel.title}</Text><br/>
      <Text t={Text.Type.BodySm} data-testid={`channel-list-subtitle-${channel.id}`}>{channel.subtitle}</Text>
    </div>

    {isIoChannel(channel) ? <div/> : <AxisLeftIcon className="channel y-left-icon"
      disabled={!isEnabled}
      onClick={() => actions.setYAxis(recordingIndex, channel.id, YAxisId.LEFT)}
      activated={channel.yAxis === YAxisId.LEFT}
      title="Left Y axis"
      data-testid={`left-channel-${channel.id}`}/>}
    <div className={isIoChannel(channel) ? '' : 'channel y-separator'}/>
    {isIoChannel(channel) ? <div/> : <AxisRightIcon className="channel y-right-icon"
      disabled={!isEnabled}
      onClick={() => actions.setYAxis(recordingIndex, channel.id, YAxisId.RIGHT)}
      activated={channel.yAxis === YAxisId.RIGHT}
      title="Right Y axis"
      data-testid={`right-channel-${channel.id}`}/>}

    {showClose && <Icons.Cross className="channel close-icon"
      onClick={() => actions.deleteChannel(channel.id)}
      data-testid={`close-channel-${channel.id}`}/>}
    {!showClose && <div/>}
  </>;
};

interface ChannelGroupProps {
  recordingIndex: number;
  channels: Channel[];
}

const ChannelGroupPanel: React.FC<ChannelGroupProps> = ({ recordingIndex, channels }) =>
  <div className="channel-group">
    {channels.map(channel =>
      <ChannelRow key={channel.id}
        recordingIndex={recordingIndex}
        channel={channel}
        closeable={isNewRecTab(recordingIndex) || isMathChannel(channel)}/>)}
  </div>;

interface ChannelGroup {
  name: string;
  axis: number;
  channels: Channel[];
}

function groupChannels(
  recording: Recording | null,
  deviceState: PerDeviceState,
  filter: (input: Channel[]) => Channel[],
  axisNames: string[]): ChannelGroup[] {
  let groups: ChannelGroup[] = [];
  if (recording) {
    groups = recording.axisNames.map((name, i) => ({
      name,
      axis: i,
      channels: filter(extractAxisChannels(recording.channels, i)).filter(ch => ch.showInUi),
    }));
  } else {
    groups = deviceState.axes.map((axisState, i) => ({
      name: axisNames[i],
      axis: i,
      channels: filter(axisState.channels).filter(ch => ch.showInUi),
    }));
  }

  return groups.filter(group => group.channels.length > 0);
}

interface AxisSectionProps {
  recordingIndex: number;
  axis: number;
  axisName: string;
  channels: Channel[];
}

const AxisSection: React.FC<AxisSectionProps> = ({ recordingIndex, axis, axisName, channels }) =>
  <React.Fragment key={axis}>
    <Text t={TextType.H4} className="channel-group-name">{axis > 0 ? `Axis ${axis}\u00A0\u00A0\u00A0\u00A0${axisName}` : axisName}</Text>
    {channels.length ?
      <ChannelGroupPanel recordingIndex={recordingIndex} channels={channels}/>
      : <div className="left"/>}
  </React.Fragment>;

interface ChannelsCardProps {
  label: string;
  addItemDisabled: boolean;
  onAddItem: () => void;
  channelGroups: ChannelGroup[];
}

const ChannelsCard: React.FC<ChannelsCardProps> = ({ label, addItemDisabled, onAddItem, channelGroups }) => {
  const { selectedRecordingIndex } = useSelector(selectOscilloscope);
  const [expanded, setExpanded] = useState(true);

  const cardEmpty = channelGroups.length === 0;
  return <HeaderCard
    className="channel-card"
    data-testid={`channel-card-${label}`}
    header={<Flex.Row className="channel-card-header">
      <Chevron expanded={expanded && !cardEmpty} disabled={cardEmpty} onClick={() => setExpanded(!expanded)}/>
      <Text t={TextType.H4}>{label}</Text>
      <Flex.Spacer/>
      <Icons.Plus
        className="add"
        disabled={addItemDisabled}
        appearsClickable
        onClick={() => {
          onAddItem();
          setExpanded(true);
        }}
        data-testid={`${label} add`}
      />
    </Flex.Row>}
  >
    {expanded && channelGroups.map((group, i) => <AxisSection key={`${label}${i}`}
      recordingIndex={selectedRecordingIndex}
      axis={group.axis}
      axisName={group.name}
      channels={group.channels}/>)}
  </HeaderCard>;
};

export const ChannelsTab: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const channels = useSelector(selectAllChannels);
  const { selectedRecordingIndex, addingNewSetting, addingNewMath, addingNewIo } = useSelector(selectOscilloscope);
  const capturing = useSelector(selectIsCaptureInProgress);
  const deviceState = useSelector(selectSelectedDeviceState);
  const recording = useSelector(selectSelectedRecording);
  const deviceIdentities = useSelector(selectIdentifiedDevices);
  const axisIdentities = useSelector(selectIdentifiedAxes);

  const axisNames = [...(recording?.axisNames ?? deviceState.axes.map(axisState => axisState.name))];
  if (_.every(axisNames.slice(1), name => name === 'Process')) {
    for (let i = 1; i < axisNames.length; i++) {
      axisNames[i] = `Process ${i}`;
    }
  }

  const isNewRecording = isNewRecTab(selectedRecordingIndex);
  const canAddSettings = numFwChannelsUsed(channels) < deviceState.maxChannels;
  const canAddMath = !recording ||
    (!!deviceIdentities[recording.deviceKey]
      && (recording.deviceId === deviceIdentities[recording.deviceKey]?.identity?.deviceId)
      && (_.isEqual(recording.peripheralIds, recording.peripheralIds.map((_, i) =>
        axisIdentities[makeAxisKey(recording.deviceKey, i)]?.identity?.peripheralId ?? 0))));

  const settingsGroups = groupChannels(recording, deviceState, extractSettingsChannels, axisNames);
  const mathGroups = groupChannels(recording, deviceState, extractMathChannels, axisNames);
  const ioGroups = groupChannels(recording, deviceState, extractIoChannels, axisNames);

  return <Flex.Column className="channel-tab">
    <Flex.Row className="channel-groups-row">
      <Flex.Column className="channel-groups-column">
        <ChannelsCard
          label="Data Source"
          addItemDisabled={capturing || !canAddSettings || !isNewRecording}
          onAddItem={actions.showAddSettingDialog}
          channelGroups={settingsGroups}
        />
      </Flex.Column>

      <Flex.Column className="channel-groups-column" data-testid="channel-list-right">
        {(extractMathChannels(channels).length > 0) &&
          <ChannelsCard
            label="Computed Math"
            addItemDisabled={capturing || !canAddMath}
            onAddItem={actions.showAddMathDialog}
            channelGroups={mathGroups}
          />
        }
        {deviceState.ioPins.total > 0 &&
          compareFirmwareVersions(deviceState.fwVersion, { major: 7, minor: 33, build: 0 }) >= 0 &&
          <ChannelsCard
            label="I/O Pins"
            addItemDisabled={capturing || !canAddSettings || !isNewRecording}
            onAddItem={actions.showAddIoDialog}
            channelGroups={ioGroups}
          />
        }
      </Flex.Column>
    </Flex.Row>

    {addingNewSetting && <DataSourceDialog dialogOpen={true}
      axisNames={axisNames}
      cancel={() => actions.closeAddSettingDialog()}
      apply={newChannels => actions.closeAddSettingDialog(newChannels)}/>}

    {addingNewMath && <ChannelMathDialog dialogOpen={true}
      cancel={() => actions.closeAddMathDialog()}
      axisNames={axisNames}
      apply={newChannels => actions.closeAddMathDialog(newChannels)}/>}

    {addingNewIo && <IoChannelDialog dialogOpen={true}
      cancel={() => actions.closeAddIoDialog()}
      apply={newChannels => actions.closeAddIoDialog(newChannels)}/>}
  </Flex.Column>;
};
