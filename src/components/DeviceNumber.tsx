import React from 'react';

function sanitize(deviceNumber: React.ReactNode): string {
  if (typeof deviceNumber === 'string' || typeof deviceNumber === 'number') {
    return deviceNumber.toString();
  }
  return 'NA';
}

export const DeviceNumber: React.FC = ({ children }) => <>{sanitize(children).padStart(2, '0')}</>;
