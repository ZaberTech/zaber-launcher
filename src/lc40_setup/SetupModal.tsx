import React, { ReactElement } from 'react';
import { useSelector } from 'react-redux';
import { Button, ButtonRowConfirmCancel, Icons, Loader, Modal, Text } from '@zaber/react-library';
import { match, P } from 'ts-pattern';
import { ModalBodyIcon, ModalButtons } from '@zaber/react-library/dist/types/elements/Modal/Modal';

import { useActions } from '../utils';

import { selectSetupStep, selectResultError, selectSetupModalOpen, selectTooManyFailedAttempts } from './selectors';
import { SetupStep } from './types';
import { actions as actionsDefinition } from './actions';
import { ErrorContext } from './exception';


const SaveAndTestButtons: React.FC = () => {
  const actions = useActions(actionsDefinition);

  return <ButtonRowConfirmCancel
    confirmText="Save & Test"
    onConfirm={() => {
      actions.setupStart(true);
      actions.setSetupStep(SetupStep.SAVE_WITH_TEST_IN_PROGRESS);
    }}
    onCancel={() => actions.setSetupModalOpen(false)}
  />;
};

const StopButton: React.FC = () => {
  const actions = useActions(actionsDefinition);

  return <Button
    data-testid="stop-test"
    onClick={() => {
      actions.setupStop();
      actions.setSetupStep(SetupStep.SAVE_WITH_TEST_STOPPING);
    }}
  >
  Stop
  </Button>;
};

const OkButton: React.FC = () => {
  const actions = useActions(actionsDefinition);

  return <Button data-testid="ok-button" onClick={() => actions.setSetupModalOpen(false)}>
    OK
  </Button>;
};

const ErrorContent: React.FC = () => {
  const resultError = useSelector(selectResultError);
  const tooManyFailedAttempts = useSelector(selectTooManyFailedAttempts);

  let errorMessage = 'An error has occured.';
  if (resultError?.errContext === ErrorContext.GENERIC && resultError.message) {
    errorMessage = resultError.message;
  }

  return <>
    {errorMessage}
    {tooManyFailedAttempts &&
      <> Please contact Zaber support if you cannot configure the device successfully.</>
    }
  </>;
};

const getSetupModalContent = (configStep: SetupStep) =>
  match<SetupStep, ReactElement>(configStep)
    .returnType<{content: React.ReactNode; buttons?: ModalButtons; icon?: ModalBodyIcon; allowClose?: boolean}>()
    .with(SetupStep.TEST_PREAMBLE, () => ({
      content: 'Your device will move. Do not mount anything to the stage during this test.',
      buttons: <SaveAndTestButtons/>,
      icon: 'warning',
      allowClose: true
    }))
    .with(SetupStep.SAVE_WITH_TEST_IN_PROGRESS, () => ({
      content: 'Test in progress, the device will move.',
      buttons: <StopButton/>,
      icon: 'loading',
      allowClose: false
    }))
    .with(SetupStep.SAVE_WITH_TEST_STOPPING, () => ({
      content: 'Test is stopping...',
      buttons: <Loader size="small" title="Loading"/>,
      icon: 'warning',
      allowClose: false
    }))
    .with(SetupStep.SAVE_WITH_TEST_STOPPED, () => ({
      content: 'Test has been stopped',
      buttons: <OkButton/>,
      icon: 'error',
      allowClose: true
    }))
    .with(SetupStep.SAVE_WITH_TEST_COMPLETE, () => ({
      content: 'Test completed successfully',
      buttons: <OkButton/>,
      icon: 'success',
      allowClose: true
    }))
    .with(P.union(SetupStep.SAVE_WITH_TEST_ERROR, SetupStep.SAVE_WITHOUT_TEST_ERROR), () => ({
      content: <ErrorContent/>,
      buttons: <OkButton/>,
      icon: 'error',
      allowClose: true
    }))
    .with(SetupStep.SAVE_WITHOUT_TEST_IN_PROGRESS, () => ({
      content: 'Saving configuration ...',
      buttons: <Loader size="small" title="Loading"/>,
      icon: 'info',
      allowClose: false
    }))
    .with(SetupStep.SAVE_WITHOUT_TEST_COMPLETE, () => ({
      content: <>
        <Text e={Text.Emphasis.Bold}>The configuration has been saved successfully, but not tested</Text>
        <br/>
        It is strongly recommended to test the LC40 configuration before using it.
        Skipping the test can result in reversed motion, crashing, or twisting.
      </>,
      buttons: <OkButton/>,
      icon: 'warning',
      allowClose: true
    }))
    .with(SetupStep.NOT_STARTED, () => ({
      content: null,
    }))
    .with(SetupStep.CONFIG_ERROR, () => ({
      content: null,
    }))
    .exhaustive();

export const SetupModal: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const setupModalOpen = useSelector(selectSetupModalOpen);
  const setupStep = useSelector(selectSetupStep);

  if (!setupModalOpen) {
    return null;
  }

  const { content, buttons, icon, allowClose } = getSetupModalContent(setupStep);

  return <Modal
    headerIcon={<Icons.Lc40/>}
    headerText="LC40 Setup"
    small
    bodyIcon={icon}
    onRequestClose={allowClose ? () => actions.setSetupModalOpen(false) : undefined}
    buttons={buttons}
  >
    {content}
  </Modal>;
};
