![Zaber Logo](docs/img/zaber-logo.png)

# Zaber Launcher

More information on [software.zaber.com/zaber-launcher](https://software.zaber.com/zaber-launcher).

## Changelog

You can read our changelog [here](CHANGELOG.md).

## Support

Need assistance with the application?
Follow instructions on [zaber.com](https://www.zaber.com/contact) to contact our Applications Engineering Team.

Have feedback or suggestions for features?
Contact our Software Team by creating an [Issue](https://gitlab.com/ZaberTech/zaber-launcher/issues).
