import React from 'react';

import { getUnitLabel } from '../units';

import { Path } from './types';

export interface Props {
  userPath: Path;
  seriesName: string;
  selectedPointIndex: number;
}


export const TimelineControlPointTooltip: React.FC<Props> = ({ userPath, seriesName, selectedPointIndex }) => {
  if (!userPath) {
    return null;
  }

  const series = userPath.series.find(s => s.name === seriesName);
  if (!series) {
    return null;
  }

  const point = series.points[selectedPointIndex];

  return <div className="control-point-tooltip">
    <table>
      <thead><tr>
        <th>Point {selectedPointIndex + 1}</th>
        <th>{series.name}</th>
      </tr></thead>
      <tbody>
        <tr><td>Position</td><td>{point.pos} {getUnitLabel(userPath.positionUnit)}</td></tr>
        <tr><td>Velocity</td><td>{point.vel} {getUnitLabel(userPath.velocityUnit)}</td></tr>
      </tbody>
    </table>
  </div>;
};
