import React from 'react';
import { ButtonRowConfirmCancel, Flex, Icons, Modal, NumericInput, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../../utils';
import { actionDefinitions as actionDefinitionsBasicControls } from '../actions';
import { Moveable, moveableKey } from '../types';
import { InputWithUnits, measurementOK } from '../../units';

import { actionDefinitions as actionDefinitionsMovement } from './actions';
import { selectAxisStates } from './selectors';

interface MoveSinModalProps {
  moveable: Moveable;
}

export const MoveSinModal: React.FC<MoveSinModalProps> = ({ moveable }) => {
  const actionsBasicControls = useActions(actionDefinitionsBasicControls);
  const actionsMovement = useActions(actionDefinitionsMovement);

  const entity = moveableKey(moveable);

  const axisStates = useSelector(selectAxisStates);
  const axisState = axisStates[entity];

  const { amplitude, period, count } = axisState.moveSinState;

  return <Modal
    className="move-sin-modal"
    headerIcon={<Icons.Oscilloscope/>}
    headerText="Sinusoidal Motion"
    onRequestClose={() => actionsBasicControls.setMoveSinModal(null)}
    buttons={
      <ButtonRowConfirmCancel
        confirmText="Start"
        onCancel={() => actionsBasicControls.setMoveSinModal(null)}
        onConfirm={measurementOK(amplitude) && measurementOK(period) ?
          () => {
            actionsMovement.issueCommand(moveable, { type: 'sin', amplitude, period, count: count ?? undefined });
            actionsBasicControls.setMoveSinModal(null);
          } :
          'disabled'}
      />
    }
  >
    <Text>Move in a Sinusoidal Trajectory</Text>
    <Flex.Column className="sin-inputs">
      <InputWithUnits
        className="sin-input"
        title="Amplitude"
        labelContent={{
          label: 'Amplitude',
          help: 'The amplitude is half of the motion’s peak-to-peak range. '
          + 'A positive number implies that the sinusoidal motion starts at the minimum position '
          + 'and moves in a positive direction from the starting position. '
          + 'A negative number implies that the sinusoidal motion starts at the maximum position '
          + 'and moves in a negative direction from the starting position.'
        }}
        measure={amplitude}
        unitFrom={{ command: 'move sin amplitude', entity }}
        onChange={measurement => actionsMovement.setMoveSinState(entity, { amplitude: measurement })}
      />
      <InputWithUnits
        className="sin-input"
        title="Period"
        labelContent={{
          label: 'Period',
          help: 'The period is the peak-to-peak time of the sinusoidal motion.',
        }}
        measure={period}
        unitFrom={{ command: 'move sin amplitude period', entity }}
        onChange={measurement => actionsMovement.setMoveSinState(entity, { period: measurement })}
      />
      <NumericInput
        className="sin-input"
        title="Cycles"
        labelContent={{
          label: 'Cycles',
          help: 'The number of times the sinusoidal motion repeats. Must be a multiple of 0.5.'
        }}
        value={count}
        displayPrecision={1}
        status={null}
        onNumberChange={value => actionsMovement.setMoveSinState(entity, { count: value })}
      />
    </Flex.Column>
  </Modal>;
};
