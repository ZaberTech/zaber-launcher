import React from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import { Button } from '@zaber/react-library';
import { injectable } from 'inversify';

import { Cache } from '../cache';
import { mockSingleDevice, MOCK_SERIAL_PORT } from '../connection_manager/mocks';
import { createContainer, destroyContainer } from '../container';
import { makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import { waitUntilPass, wrapWithNewStore } from '../test';
import { MessageRoutersServiceMockBase } from '../test/mocks/ascii';

import { ExternalUpgrade } from './ExternalUpgrade';
import type { CachedConnectionFirmwareUpgradeInfo } from './types';
import { testLoadingOfFirmwareVersions } from './sagas';

const MOCK_DEVICE_KEY = makeDeviceKey(makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), MOCK_SERIAL_PORT), 1);

@injectable()
class CacheMock {
  load = jest.fn<CachedConnectionFirmwareUpgradeInfo, []>(() => ({
    info: {
      1: {
        identifiers: { PlatformID: 987343, SerialNumber: 1234, DeviceNumber: 1, DeviceID: 50081, IsKeyed: true },
        versions: [
          { str: '7.1.12', parsed: { major: 7, minor: 1, build: 12 } },
          { str: '7.2.13', parsed: { major: 7, minor: 2, build: 13 } },
        ],
      },
    },
    setAtTimestamp: Date.now(),
  }));
}

let wrapper: RenderResult;

const TestExternalUpgrade = wrapWithNewStore(ExternalUpgrade);

beforeAll(() => {
  testLoadingOfFirmwareVersions();
});

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(Cache).to(CacheMock);
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockBase);
  const router = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMockBase<any, any, any, any>;
  router.getMockConnection.mockRejectedValue(new Error('Cannot connect'));

  wrapper = render(<TestExternalUpgrade
    deviceKey={MOCK_DEVICE_KEY}
    title="Special Upgrade"
    noContent={() => <>No content</>}
    content={(versions, install) => (<>
      {versions.map((version, i) =>
        <Button key={i} onClick={() => install(version)}>{version.str}</Button>)}
    </>)}/>);
});

afterEach(() => {
  wrapper?.unmount();
  wrapper = null!;

  destroyContainer();
});

function loadDevices() {
  mockSingleDevice(TestExternalUpgrade.testStore);
}

test('displays versions once devices are loaded', async () => {
  wrapper.getByText('No content');
  loadDevices();
  await waitUntilPass(() => {
    wrapper.getByText('7.1.12');
    wrapper.getByText('7.2.13');
  });
});

describe('once loaded', () => {
  beforeEach(async () => {
    loadDevices();
    await waitUntilPass(() => wrapper.getByText('7.1.12'));
  });

  test('starts the update process', async () => {
    fireEvent.click(wrapper.getByText('7.1.12'));
    await waitUntilPass(() => {
      wrapper.getByText('Special Upgrade');
      wrapper.getByText(/Cannot connect/);
    });
  });
});
