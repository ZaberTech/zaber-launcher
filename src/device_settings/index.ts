export { reducer as deviceSettingsReducer } from './reducer';
export type { State as DeviceSettingsState } from './reducer';
export {
  actions as deviceSettingsActions,
  ActionTypes as DeviceSettingsActionTypes,
} from './actions';
export type {
  ActionsToPayloads as DeviceSettingsActionPayloads,
} from './actions';
export { deviceSettingsSaga } from './sagas';
export { DeviceSettings } from './DeviceSettings';
