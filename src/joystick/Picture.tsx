import React, { useLayoutEffect, useRef }  from 'react';

import JoystickSvg from '../assets/xjoy.svg';

const KEY_COUNT = 8;
export const PICTURE_ACTIVE_CLASS = 'xjoy_svg__st-active';

interface Props {
  activeButton?: number | null;
  onClick?: (button: number) => void;
}

export const JoystickPicture: React.FC<Props> = ({ activeButton, onClick }) => {
  const ref = useRef<HTMLDivElement>(null);
  const clickRef = useRef<Props['onClick']>();

  clickRef.current = onClick;

  useLayoutEffect(() => {
    const svg = ref.current!.children[0];

    svg.querySelectorAll(`.${PICTURE_ACTIVE_CLASS}`).forEach(
      element => element.classList.remove(PICTURE_ACTIVE_CLASS));

    for (let i = 1; i <= KEY_COUNT; ++i) {
      const handler = () => clickRef.current?.(i);

      svg.querySelectorAll(`.xjoy_svg__button${i}`).forEach(
        element => element.addEventListener('click', handler));
    }
  }, []);

  useLayoutEffect(() => {
    if (activeButton == null) { return }

    const svg = ref.current!.children[0];
    const buttonElements = svg.querySelectorAll(`.xjoy_svg__button${activeButton}`);

    buttonElements.forEach(
      element => element.classList.add(PICTURE_ACTIVE_CLASS));

    return () => buttonElements.forEach(
      element => element.classList.remove(PICTURE_ACTIVE_CLASS));
  }, [activeButton]);

  return <div className="joystick-picture" ref={ref}>
    <JoystickSvg/>
  </div>;
};
