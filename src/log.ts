import { getContainer } from './container';
import { Logger, Log } from './app_components';

export function getLogger(name: string): Logger {
  return getContainer().get(Log).getLogger(name);
}
