import { Button, ButtonRow } from '@zaber/react-library';
import React from 'react';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectValidationErr, selectInLockstep } from './selectors';
import { SetupStep } from './types';
import { SetupModal } from './SetupModal';


interface TestConfigProps {
  validate: boolean;
  onValidate: () => void;
}
export const TestConfig: React.FC<TestConfigProps> = ({ validate, onValidate }) => {
  const actions = useActions(actionsDefinition);
  const validationErr = useSelector(selectValidationErr)!;
  const inLockstep = useSelector(selectInLockstep);

  const onSave = (test: boolean) => {
    if (validationErr.hasError) {
      onValidate();
    } else {
      actions.setSetupModalOpen(true);
      if (test) {
        actions.setSetupStep(SetupStep.TEST_PREAMBLE);
      } else {
        actions.setupStart(false);
        actions.setSetupStep(SetupStep.SAVE_WITHOUT_TEST_IN_PROGRESS);
      }
    }
  };

  return <div className="save-buttons">
    <ButtonRow justify="left">
      <Button
        data-testid="save-and-test"
        onClick={() => onSave(true)}
        disabled={(validate && validationErr.hasError) || inLockstep}
      >
        Save & Test
      </Button>
      <Button
        data-testid="save-without-test"
        color="grey"
        onClick={() => onSave(false)}
        disabled={validate && validationErr.hasError}
      >
        Save without Testing
      </Button>
    </ButtonRow>
    <SetupModal/>
  </div>;
};
