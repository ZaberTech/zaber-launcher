import type { PidTuning, ServoTuningParamset } from '@zaber/motion/ascii';
import _ from 'lodash';

import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';
import { MeasurementInputEditorState } from '../units/MeasurementInputEditor';

import type { StateMessage } from './reducer';
import type { SavedRow } from './saved_rows/saved_rows';
import type {
  AdvancedTuning,
  Chosen,
  ChosenParamset,
  ServoTuningAxisInfo,
  ServoTuningSettings,
  SimpleTuning,
  SimpleTuningInfo,
  TuningInfo,
} from './types';

export enum ActionTypes {
  CHOOSE_ENTITY = 'SERVO_TUNE-CHOOSE_ENTITY',
  CHOOSE_TUNING = 'SERVO_TUNE-CHOOSE_TUNING',
  READ_TUNING_PARAMS = 'SERVO_TUNE-READ_TUNING_PARAMS',

  INFO_FETCHED = 'SERVO_TUNE-INFO_FETCHED',

  CHANGE_STARTUP_PRESET = 'SERVO_TUNE-CHANGE_STARTUP_PRESET',
  CHANGE_STARTUP_PRESET_DONE = 'SERVO_TUNE-CHANGE_STARTUP_PRESET_DONE',
  LOAD_PARAMSET = 'SERVO_TUNE-LOAD_PARAMSET',

  UPDATE_SETTING = 'SERVO_TUNE-UPDATE_SETTING',
  UPDATE_SETTING_ERROR = 'SERVO_TUNE-UPDATE_SETTING_ERROR',
  RECALCULATE_RECOMMENDED_ACCEL = 'SERVO_TUNE-RECALCULATE_ACCEL_RECOMMENDATION',

  SET_MESSAGE = 'SERVO_TUNE-SET_MESSAGE',

  UPDATE_PARAM_TUNING = 'SERVO_TUNE-UPDATE_PARAM_TUNING',
  APPLY_ADVANCED = 'SERVO_TUNE-APPLY_ADVANCED',
  APPLY_PID = 'SERVO_TUNE-APPLY_PID',
  APPLY_SIMPLE = 'SERVO_TUNE-APPLY_SIMPLE',
  APPLY_DONE = 'SERVO_TUNE-APPLY_DONE',

  IMPORT_CSV = 'SERVO_TUNE-IMPORT_CSV',
  IMPORT_CSV_DONE = 'SERVO_TUNE-IMPORT_CSV_DONE',
  ADD_ROW_OF_PARAMSET = 'SERVO_TUNE-ADD_ROW_OF_PARAMSET',
  ADD_ROW = 'SERVO_TUNE-ADD_ROW',
  DELETE_ROW = 'SERVO_TUNE-DELETE_ROW',
  APPLY_SAVED_ROW = 'SERVO_TUNE-APPLY_SAVED_ROW',
  DELETE_TABLE = 'SERVO_TUNE-DELETE_TABLE',
  RENAME_ROW = 'SERVO_TUNE-RENAME_ROW',
  EXPORT_CSV = 'SERVO_TUNE-EXPORT_CSV',

  SET_DRIVER_ENABLED = 'SERVO_TUNE-SET_DRIVER_ENABLED',
  SET_DRIVER_ENABLED_DONE = 'SERVO_TUNE-SET_DRIVER_ENABLED_DONE',
  SET_DRIVER_STATE = 'SERVO_TUNE_SET_DRIVER_STATE',
}

export interface ActionsToPayloads {
  [ActionTypes.CHOOSE_ENTITY]: { chosenKey: EntityKey | null };
  [ActionTypes.CHOOSE_TUNING]: Chosen;
  [ActionTypes.READ_TUNING_PARAMS]: Chosen;

  [ActionTypes.INFO_FETCHED]: ServoTuningAxisInfo;

  [ActionTypes.CHANGE_STARTUP_PRESET]: { key: EntityKey; paramset: ServoTuningParamset };
  [ActionTypes.CHANGE_STARTUP_PRESET_DONE]: { paramset: ServoTuningParamset };
  [ActionTypes.LOAD_PARAMSET]: { key: EntityKey; to: ServoTuningParamset; from: ServoTuningParamset };

  [ActionTypes.UPDATE_SETTING]: { setting: keyof ServoTuningSettings; update: MeasurementInputEditorState };
  [ActionTypes.UPDATE_SETTING_ERROR]: { setting: keyof ServoTuningSettings; error: string | null };
  [ActionTypes.RECALCULATE_RECOMMENDED_ACCEL]: Pick<SimpleTuning, 'loadMass' | 'carriageModified' | 'carriageMass'> | null;

  [ActionTypes.SET_MESSAGE]: { message: StateMessage | null };

  [ActionTypes.UPDATE_PARAM_TUNING]: { tuning: TuningInfo };
  [ActionTypes.APPLY_ADVANCED]: ChosenParamset & { tuning: AdvancedTuning; toLive: boolean; toPowerOn: boolean };
  [ActionTypes.APPLY_PID]: ChosenParamset & { tuning: PidTuning; toLive: boolean; toPowerOn: boolean };
  [ActionTypes.APPLY_SIMPLE]: ChosenParamset & { tuning: SimpleTuning; toLive: boolean; toPowerOn: boolean };
  [ActionTypes.APPLY_DONE]: { paramset: ServoTuningParamset; toLive: boolean; error?: string };

  [ActionTypes.IMPORT_CSV]: null;
  [ActionTypes.IMPORT_CSV_DONE]: {rows: SavedRow[]};
  [ActionTypes.ADD_ROW_OF_PARAMSET]: ChosenParamset;
  [ActionTypes.ADD_ROW]: { row: SavedRow };
  [ActionTypes.DELETE_ROW]: { rowIndex: number };
  [ActionTypes.APPLY_SAVED_ROW]: ChosenParamset & { row: SavedRow };
  [ActionTypes.DELETE_TABLE]: null;
  [ActionTypes.RENAME_ROW]: { rowIndex: number; name: string };
  [ActionTypes.EXPORT_CSV]: null;

  [ActionTypes.SET_DRIVER_ENABLED]:  { key: EntityKey; enable: boolean };
  [ActionTypes.SET_DRIVER_ENABLED_DONE]:  { enable: boolean };
  [ActionTypes.SET_DRIVER_STATE]: { key: EntityKey };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actionDefinitions = {
  chooseEntity: (chosenKey: EntityKey | null) => buildAction(ActionTypes.CHOOSE_ENTITY, { chosenKey }),
  chooseTuning: (chosen: Chosen) => buildAction(ActionTypes.CHOOSE_TUNING, chosen),
  readTuningParams: (chosen: Chosen) => buildAction(ActionTypes.READ_TUNING_PARAMS, chosen),

  changeStartupPreset: (key: EntityKey, paramset: ServoTuningParamset) => buildAction(ActionTypes.CHANGE_STARTUP_PRESET, { key, paramset }),
  changeStartupPresetDone: (paramset: ServoTuningParamset) => buildAction(ActionTypes.CHANGE_STARTUP_PRESET_DONE, { paramset }),
  loadParamset: (key: EntityKey, to: ServoTuningParamset, from: ServoTuningParamset) => buildAction(
    ActionTypes.LOAD_PARAMSET, { key, to, from }
  ),

  updateSetting: (setting: keyof ServoTuningSettings, update: MeasurementInputEditorState) => buildAction(
    ActionTypes.UPDATE_SETTING, { setting, update }
  ),
  updateSettingError: (setting: keyof ServoTuningSettings, error: string | null) => buildAction(
    ActionTypes.UPDATE_SETTING_ERROR, { setting, error }
  ),
  /** Calculate the new recommended acceleration. Pass `null` to clear recommendation. */
  recalculateRecommendedAccel: (info: SimpleTuningInfo | null) => buildAction(
    ActionTypes.RECALCULATE_RECOMMENDED_ACCEL,
    info?.tuning ? _.pick(info.tuning, 'loadMass', 'carriageModified', 'carriageMass') : null
  ),

  infoFetched: (info: ServoTuningAxisInfo) => buildAction(ActionTypes.INFO_FETCHED, info),

  setMessage: (message: StateMessage | null) => buildAction(ActionTypes.SET_MESSAGE, { message }),

  updateParamTuning: (tuning: TuningInfo) => buildAction(ActionTypes.UPDATE_PARAM_TUNING, { tuning }),
  applyAdvanced: (
    entity: EntityKey,
    paramset: ServoTuningParamset,
    tuning: AdvancedTuning,
    toLive: boolean,
    toPowerOn: boolean
  ) => buildAction(ActionTypes.APPLY_ADVANCED, { entity, paramset, tuning, toLive, toPowerOn }),
  applyPid: (
    entity: EntityKey,
    paramset: ServoTuningParamset,
    tuning: PidTuning,
    toLive: boolean,
    toPowerOn: boolean
  ) => buildAction(ActionTypes.APPLY_PID, { entity, paramset, tuning, toLive, toPowerOn }),
  applySimple: (
    entity: EntityKey,
    paramset: ServoTuningParamset,
    tuning: SimpleTuning,
    toLive: boolean,
    toPowerOn: boolean
  ) => buildAction(ActionTypes.APPLY_SIMPLE, { entity, paramset, tuning, toLive, toPowerOn }),
  applyDone: (paramset: ServoTuningParamset, toLive: boolean, error?: string) => buildAction(
    ActionTypes.APPLY_DONE, { paramset, toLive, error }
  ),

  importCSV: () => buildAction(ActionTypes.IMPORT_CSV),
  importCsvDone: (rows: SavedRow[]) => buildAction(ActionTypes.IMPORT_CSV_DONE, { rows }),
  addRowOfParamset: (entity: EntityKey, paramset: ServoTuningParamset) => buildAction(
    ActionTypes.ADD_ROW_OF_PARAMSET, { entity, paramset }
  ),
  addRow: (row: SavedRow) => buildAction(ActionTypes.ADD_ROW, { row }),
  deleteRow: (rowIndex: number) => buildAction(ActionTypes.DELETE_ROW, { rowIndex }),
  applySavedRow: (chosenKey: EntityKey, chosenParamset: ServoTuningParamset, row: SavedRow) => buildAction(
    ActionTypes.APPLY_SAVED_ROW, { entity: chosenKey, paramset: chosenParamset, row }
  ),
  deleteTable: () => buildAction(ActionTypes.DELETE_TABLE),
  renameRow: (rowIndex: number, name: string) => buildAction(ActionTypes.RENAME_ROW, { rowIndex, name }),
  exportCsv: () => buildAction(ActionTypes.EXPORT_CSV),

  setDriverEnabled: (key: EntityKey, enable: boolean) => buildAction(ActionTypes.SET_DRIVER_ENABLED, { key, enable }),
  setDriverEnabledDone: (enable: boolean) => buildAction(ActionTypes.SET_DRIVER_ENABLED_DONE, { enable }),
  setDriverState: (key: EntityKey) => buildAction(ActionTypes.SET_DRIVER_STATE, { key }),
};
