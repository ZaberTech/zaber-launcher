import { createSelector } from 'reselect';
import _ from 'lodash';
import { isMatching } from 'ts-pattern';

import { selectLC40Setup } from '../store';
import {
  selectIdentifiedAxes as selectManagerAxes,
  selectIdentifiedDevices as selectManagerDevices,
  ConnectionManagerIdentifiedAxisState,
  IdentifiedDeviceState,
  selectIdentifiedDevices,
} from '../connection_manager';
import { EntityKeyType, extractDeviceKey, getAxisNumber, getEntityType } from '../keys';
import { MyNumber, tryAccess } from '../utils';
import { selectPendingSetups } from '../device_setup';

import {
  BOSCH_REXROTH_SHB40_IDS,
  CONTACT_SUPPORT_FAILED_ATTEMPTS,
  DISPLAY_SETUP_ALL_TIME,
  MAXIMUM_TRAVEL_LENGTH_MM,
  SUPPORTED_DEVICE_OR_PERIPHERAL_IDS
} from './types';
import { State } from './reducer';

export interface StateComplete extends State {
  axis?: ConnectionManagerIdentifiedAxisState;
  device: IdentifiedDeviceState;
}

export const selectCurrentAxisOrDevice = createSelector(selectLC40Setup, selectManagerAxes, selectManagerDevices,
  (state, axes, devices): StateComplete | null => {
    const key = state.selectedKey;
    if (!key) { return null }

    const device = devices[extractDeviceKey(key)];
    if (!device) { return null }

    const axis = tryAccess(axes, key);
    if (axis == null && getEntityType(key) === EntityKeyType.AXIS) { return null }

    return ({
      device,
      axis,
      ...state,
    });
  }
);

export const selectDeviceOrPeripheralId = createSelector(selectCurrentAxisOrDevice, deviceOrAxis => {
  if (!deviceOrAxis) { return -1 }
  return deviceOrAxis.axis?.identity.peripheralId ?? deviceOrAxis.device.identity.deviceId;
});

export const selectIsCurrentSupported = createSelector(selectDeviceOrPeripheralId, id => SUPPORTED_DEVICE_OR_PERIPHERAL_IDS.includes(id));

export interface ValidationResult {
  hasError: boolean;
  travelDistance: string | null;
  estop: string | null;
}

export const selectValidationErr = createSelector(selectCurrentAxisOrDevice, axisState => {
  if (!axisState) {
    return null;
  }

  const { config } = axisState;
  const validation: Omit<ValidationResult, 'hasError'> = {
    travelDistance: null,
    estop: null,
  };

  if (!MyNumber.isFinite(config.travelDistanceMm)) {
    validation.travelDistance = 'Please enter a valid number.';
  } else if (config.travelDistanceMm <= 0) {
    validation.travelDistance = 'Travel distance must be more than 0.';
  } else if (config.travelDistanceMm > MAXIMUM_TRAVEL_LENGTH_MM) {
    validation.travelDistance = `Travel distance must be less than or equal to ${MAXIMUM_TRAVEL_LENGTH_MM} mm.`;
  }

  if (!isMatching({ status: 'ok', ready: true }, axisState.estop)) {
    validation.estop = 'E-Stop is disconnected.';
  }

  return {
    ...validation,
    hasError: _.values(validation).some(validationProperty => validationProperty !== null),
  };
});

export const selectResultError = createSelector(selectCurrentAxisOrDevice, state => state?.resultError);
export const selectEstop = createSelector(selectCurrentAxisOrDevice, state => state?.estop);

export const selectNeedSetupKeys = createSelector(selectManagerAxes, selectManagerDevices, selectPendingSetups,
  (axes, devices, pendingSetups) => {
    if (DISPLAY_SETUP_ALL_TIME) {
      return [];
    }
    const deviceKeys = _.filter(devices, device =>
      SUPPORTED_DEVICE_OR_PERIPHERAL_IDS.includes(device.identity.deviceId)
      && device.identity.serialNumber in pendingSetups
    ).map(device => device.key);
    const axisKeys = _.filter(axes, axis =>
      SUPPORTED_DEVICE_OR_PERIPHERAL_IDS.includes(axis.identity.peripheralId)
      && (axis.serialNumber ?? 0) in pendingSetups
    ).map(axis => axis.key);
    return deviceKeys.concat(axisKeys);
  });
export const selectNeedsSetup = createSelector(selectLC40Setup, selectNeedSetupKeys, (state, needSetupKeys) =>
  state.selectedKey && needSetupKeys.includes(state.selectedKey)
);

export const selectEstopSupported = createSelector(selectDeviceOrPeripheralId, id => BOSCH_REXROTH_SHB40_IDS.includes(id));

export const selectLoading = createSelector(selectLC40Setup, state => state.loading);
export const selectInLockstep = createSelector(
  selectCurrentAxisOrDevice,
  selectIdentifiedDevices,
  (currentAxisOrDevice, identifiedDevices) => {
    if (currentAxisOrDevice?.selectedKey == null) {
      return false;
    }
    const { selectedKey } = currentAxisOrDevice;
    if (getEntityType(selectedKey) !== EntityKeyType.AXIS) {
      return false;
    }

    const deviceKey = extractDeviceKey(selectedKey);
    const identifiedDevice = identifiedDevices[deviceKey];
    if (identifiedDevice == null) {
      return false;
    }

    const axisNumber = getAxisNumber(selectedKey);
    const lockstepGroups = identifiedDevice.locksteps;

    for (const lockstepGroup of lockstepGroups) {
      if (lockstepGroup.axisNumbers.includes(axisNumber)) {
        return true;
      }
    }

    return false;
  });
export const selectSetupModalOpen = createSelector(selectLC40Setup, state => state.setupModalOpen);
export const selectSetupStep = createSelector(selectLC40Setup, state => state.setupStep);
export const selectTooManyFailedAttempts = createSelector(selectLC40Setup, state =>
  state.failedAttempts >= CONTACT_SUPPORT_FAILED_ATTEMPTS
);
