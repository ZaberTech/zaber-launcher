/* eslint-disable @typescript-eslint/no-this-alias */
import { Subject, ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { injectable } from 'inversify';

jest.useFakeTimers();

let rpcIoTcp: RpcIoTcpMock;
let rpcIoIot: RpcIoIotMock;
let rpcClient: RpcClientMock;
let rpcNotifications: RpcNotificationsMock;

class RpcIoTcpMock {
  isReliable = true;

  constructor() {
    rpcIoTcp = this;
  }

  errors = new Subject<Error>();
  connect = jest.fn().mockResolvedValue(undefined);
  close = jest.fn().mockResolvedValue(undefined);
}

class RpcIoIotMock {
  isReliable = false;
  lastMessageTs = 0;

  constructor() {
    rpcIoIot = this;
  }

  errors = new Subject<Error>();
  connect = jest.fn().mockResolvedValue(undefined);
  close = jest.fn().mockResolvedValue(undefined);
}

class RpcClientMock {
  static API_VERSION = '';

  constructor() {
    rpcClient = this;
  }

  errors = new Subject<Error>();
  handlerErrors = new Subject<Error>();
  end = jest.fn();
  call = jest.fn(() => Promise.resolve<unknown>({}));
  callOpt = jest.fn((options, method) => {
    if (method === 'status_getApiVersion') {
      return Promise.resolve(RpcClientMock.API_VERSION);
    }
    return Promise.resolve<unknown>({});
  });
}

class RpcNotificationsMock {
  constructor() {
    rpcNotifications = this;
  }
  hasSubscriptions = jest.fn().mockReturnValue(false);
}

jest.mock('fs/promises', () => ({
  get access() {
    return FsMock.instance.access;
  },
  constants: { R_OK: 'R' },
}));

class FsMock {
  private static _instance?: FsMock;

  access = jest.fn(async () => undefined);

  static get instance() {
    if (!FsMock._instance) {
      FsMock._instance = new FsMock();
    }
    return FsMock._instance;
  }

  static reset() {
    FsMock._instance = undefined;
  }
}

jest.mock('@zaber/motion', () => ({
  ...jest.requireActual('@zaber/motion'),
  Tools: class ToolsMock {
    static getMessageRouterPipePath() { return 'PIPE' }
    static getDbServicePipePath() { return 'PIPE_DB' }
  },
}));

@injectable()
export class MessageRoutersServiceMock {
}

jest.mock('../app_components', () => ({
  ...jest.requireActual('../app_components'),
  RpcIoTcp: RpcIoTcpMock,
  RpcIoIoT: RpcIoIotMock,
  RpcClient: RpcClientMock,
  RpcNotifications: RpcNotificationsMock,
  RoutedConnection: RoutedConnectionMock
}));

const handleUnexpectedErrorMock = jest.fn();
jest.mock('../errors', () => ({
  handleUnexpectedError: handleUnexpectedErrorMock,
}));

let routedConnections: RoutedConnectionMock[];

class RoutedConnectionMock {
  constructor(public readonly id: string) {
    routedConnections.push(this);
  }

  ended = new ReplaySubject();
  asciiConnectionMock = {};

  get connection(): unknown {
    return this.asciiConnectionMock;
  }

  ensureOpen = jest.fn(() => Promise.resolve());
}

import { defer, waitTick } from '../test';
import { createContainer, destroyContainer } from '../container';
import { MIN_REQUIRED_API_VERSION } from '../app_components';

import { LOCAL_ROUTER_URL } from './constants';
import { MessageRouterConnection, CHECK_UNRELIABLE_CONNECTION_STATUS } from './connection';
import { makeLocalShareUrl } from '../connection_manager';
import { MessageRoutersService } from './service';
import { TimeMeasuring } from '../time';
import { TimeMeasuringMock } from '../test/mocks/times';

RpcClientMock.API_VERSION = MIN_REQUIRED_API_VERSION;

let timeMeasuring: TimeMeasuringMock;

let connection: MessageRouterConnection;

beforeEach(() => {
  routedConnections = [];
  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(TimeMeasuring).to(TimeMeasuringMock);
  timeMeasuring = container.get<unknown>(TimeMeasuring) as TimeMeasuringMock;
});

afterEach(() => {
  connection = null!;
  timeMeasuring = null!;
  FsMock.reset();
});

afterAll(() => {
  destroyContainer();
  routedConnections = null!;

  rpcIoTcp = null!;
  rpcIoIot = null!;
  rpcClient = null!;
  rpcNotifications = null!;
});

describe('TCP IO', () => {
  beforeEach(() => {
    connection = new MessageRouterConnection(LOCAL_ROUTER_URL, { local: 1000, remote: 1000 });
  });

  describe('ensureConnected', () => {
    test('opens io', async () => {
      await connection.ensureConnected();
      expect(rpcIoTcp.connect).toBeCalledTimes(1);
    });

    test('multiple calls opens io once and wait for it to finish', async () => {
      const deferred = defer<void>();
      rpcIoTcp.connect.mockImplementationOnce(() => deferred.promise);

      let resolved = false;
      const connect1Promise = connection.ensureConnected().then(() => resolved = true);
      const connect2Promise = connection.ensureConnected().then(() => resolved = true);

      await waitTick();
      expect(rpcIoTcp.connect).toBeCalledTimes(1);
      expect(resolved).toBe(false);

      deferred.resolve();
      await Promise.all([connect1Promise, connect2Promise]);
    });
  });

  describe('routed connections management', () => {
    test('returns same connection when requested multiple times with the same id', async () => {
      const routedConnection1 = await connection.getRoutedConnection('id1');
      const routedConnection2 = await connection.getRoutedConnection('id1');

      expect(routedConnections).toHaveLength(1);
      expect(routedConnections[0].ensureOpen).toBeCalledTimes(2);

      expect(routedConnection1 === routedConnection2).toBeTruthy();
    });

    test('returns different connections for different ids', async () => {
      const routedConnection1 = await connection.getRoutedConnection('id1');
      const routedConnection2 = await connection.getRoutedConnection('id2');

      expect(routedConnections).toHaveLength(2);

      expect(routedConnection1 !== routedConnection2).toBeTruthy();
    });

    test('removes the connection when the connection ends and returns new one when requested again', async () => {
      const routedConnection = await connection.getRoutedConnection('id1');

      expect(routedConnections).toHaveLength(1);
      routedConnections[0].ended.next(true);

      const routedConnectionRenewed = await connection.getRoutedConnection('id1');
      expect(routedConnections).toHaveLength(2);
      expect(routedConnection !== routedConnectionRenewed).toBeTruthy();
    });

    test('emits new routed connections', async () => {
      const emittedConnection = connection.newRoutedConnection.pipe(take(1)).toPromise();
      const routedConnection = await connection.getRoutedConnection('id');

      expect(await emittedConnection).toBe(routedConnection);
    });
  });

  test('aggregates errors from rpc', async () => {
    const promise1 = connection.errors.toPromise();
    rpcClient.errors.next(new Error('fail'));
    expect(await promise1).toBeInstanceOf(Error);

    const promise3 = connection.errors.toPromise();
    rpcIoTcp.errors.next(new Error('fail'));
    expect(await promise3).toBeInstanceOf(Error);
  });

  test('handles handler errors', async () => {
    rpcClient.handlerErrors.next(new Error('Fail1'));
    expect(handleUnexpectedErrorMock).toHaveBeenCalledTimes(1);
  });

  test('close ends client and closes io', async () => {
    await connection.close();
    expect(rpcIoTcp.close).toBeCalledTimes(1);
    expect(rpcClient.end).toBeCalledTimes(1);
  });

  test('emits end on close', async () => {
    const endPromise = connection.ended.toPromise();

    await connection.close();
    await endPromise;
  });

  test('failed connect gives custom error', async () => {
    rpcIoTcp.connect.mockRejectedValueOnce(new Error('Cannot open pipe'));
    await expect(connection.ensureConnected()).rejects.toMatchObject({
      message: expect.stringMatching(/Zaber Message Router is not running on this computer/),
    });
  });

  test('waits for local router to start', async () => {
    FsMock.instance.access.mockRejectedValueOnce(new Error('File not found'));

    await connection.ensureConnected();

    expect(FsMock.instance.access).toBeCalledWith('PIPE', 'R');
    expect(FsMock.instance.access).toHaveBeenCalledTimes(2);

    expect(rpcIoTcp.connect).toBeCalledTimes(1);
  });

  test('waits for local router to start until timeout and then tries to connect anyway', async () => {
    timeMeasuring.mockTime = 0;
    FsMock.instance.access.mockImplementation(async () => {
      timeMeasuring.mockTime! += 2000;
      throw new Error('File not found');
    });

    await connection.ensureConnected();

    expect(FsMock.instance.access).toHaveBeenCalledTimes(8);
    expect(rpcIoTcp.connect).toBeCalledTimes(1);
  });
});

describe('TCP IO (Network)', () => {
  beforeEach(() => {
    connection = new MessageRouterConnection(makeLocalShareUrl('host', 1234), { local: 1000, remote: 1000 });
  });

  test('failed connect gives custom error', async () => {
    rpcIoTcp.connect.mockRejectedValueOnce(new Error('Cannot connect to host'));
    await expect(connection.ensureConnected()).rejects.toMatchObject({
      message: expect.stringMatching(/Cannot reach network destination/),
    });
  });
});

describe('IoT IO', () => {
  beforeEach(() => {
    connection = new MessageRouterConnection('iot://realm/test_id', { local: 1000, remote: 1000 });
  });

  test('pings the router during connection', async () => {
    await connection.ensureConnected();
    expect(rpcClient.callOpt).toHaveBeenCalledWith(expect.anything(), 'status_ping');
  });

  test('failed initial ping gives custom error', async () => {
    rpcClient.callOpt.mockRejectedValueOnce(new Error('Request timeout'));
    await expect(connection.ensureConnected()).rejects.toMatchObject({
      message: expect.stringMatching(/Cannot reach cloud destination/),
    });
  });

  test('pings the router in regular interval if there is activity', async () => {
    await connection.ensureConnected();
    rpcClient.callOpt.mockClear();

    jest.advanceTimersByTime(CHECK_UNRELIABLE_CONNECTION_STATUS);
    expect(rpcClient.callOpt).toHaveBeenCalledTimes(0);

    rpcIoIot.lastMessageTs = performance.now();
    jest.advanceTimersByTime(CHECK_UNRELIABLE_CONNECTION_STATUS);
    expect(rpcClient.callOpt).toHaveBeenCalledWith(expect.anything(), 'status_ping');
  });

  test('pings the router in regular interval if there are subscriptions', async () => {
    await connection.ensureConnected();
    rpcClient.callOpt.mockClear();

    rpcNotifications.hasSubscriptions.mockReturnValue(true);

    jest.advanceTimersByTime(CHECK_UNRELIABLE_CONNECTION_STATUS);
    expect(rpcClient.callOpt).toHaveBeenCalledWith(expect.anything(), 'status_ping');

    rpcClient.callOpt.mockClear();
    rpcNotifications.hasSubscriptions.mockReturnValue(false);

    jest.advanceTimersByTime(CHECK_UNRELIABLE_CONNECTION_STATUS);
    expect(rpcClient.callOpt).not.toHaveBeenCalled();
  });

  test('failed ping of the router results in error', async () => {
    await connection.ensureConnected();

    const error = connection.errors.toPromise();
    rpcClient.callOpt.mockRejectedValueOnce(new Error('Request failed'));

    rpcIoIot.lastMessageTs = performance.now();
    jest.advanceTimersByTime(CHECK_UNRELIABLE_CONNECTION_STATUS);

    expect(await error).toMatchObject({ message: 'Request failed' });
  });

  test('close cancels the interval', async () => {
    await connection.ensureConnected();
    rpcClient.callOpt.mockClear();

    await connection.close();

    rpcIoIot.lastMessageTs = performance.now();
    jest.advanceTimersByTime(CHECK_UNRELIABLE_CONNECTION_STATUS);
    expect(rpcClient.callOpt).toHaveBeenCalledTimes(0);
  });
});
