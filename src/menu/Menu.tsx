import { Icons, Text } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import classNames from 'classnames';
import React, { useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, NavLink } from 'react-router-dom';

import zaberLauncherLogo from '../assets/zl_logo.svg?url';
import rexrothLogo from '../assets/rexroth.svg?url';
import rexrothIcon from '../assets/rexroth_favicon.svg?url';
import { FeedbackButton } from '../feedback';
import { RecentApps } from '../apps';
import { environment, Flavors } from '../environment';

import { selectMenuState, selectPinned } from './selectors';
import { actions as actionDefinition } from './actions';
import type { MenuState } from './types';

const Section: React.FC = ({ children }) => <div className="section">{children}</div>;
const MenuItem: React.FC = ({ children }) => <div className="item">{children}</div>;

export const Menu: React.FC = () => {
  const pinned = useSelector(selectPinned);
  const actions = useActions(actionDefinition);

  const customScroll = useMemo(() => environment.platform !== 'darwin', []);

  const desiredState = useSelector(selectMenuState);
  const [currentState, setState] = useState<MenuState>(desiredState);
  let state = currentState;
  if (desiredState !== currentState) {
    const shouldNotTransition = desiredState === 'extended' && currentState === 'fixed';
    if (shouldNotTransition) {
      state = currentState;
    } else {
      state = desiredState;
      setState(desiredState);
    }
  }

  return (
    <div className={classNames('red-menu', state, environment.flavor)} data-testid="menu">
      <div className={classNames('container', { 'custom-scroll': customScroll })} data-testid="menu-container"
        onMouseEnter={() => actions.mouseOver(true)}
        onMouseLeave={() => actions.mouseOver(false)}>

        <div className="header">
          {environment.flavor === Flavors.Zaber && <>
            <img className="zaber-launcher-logo" src={zaberLauncherLogo} alt="Zaber Launcher Logo"/>
            <Text t={Text.Type.H4}>{environment.appName}</Text>
          </>}

          {environment.flavor === Flavors.Rexroth && <>
            <div className="rexroth-icon">
              <img src={rexrothIcon} alt="Rexroth favicon"/>
            </div>
            <img className="rexroth-logo" src={rexrothLogo} alt="Rexroth"/>
          </>}

          <div className="spacer"/>
          <Icons.Pin className="pin"
            interactionColor={environment.flavor === Flavors.Zaber ? 'white' : 'grey'}
            activated={pinned} title="Pin Menu"
            onClick={() => actions.pinMenu(!pinned)}/>
        </div>

        <Section>
          Connections
        </Section>
        <NavLink to="/connection-manager">
          <MenuItem><Icons.Connection/>My Connections</MenuItem>
        </NavLink>
        {environment.flavor === Flavors.Zaber && <NavLink to="/local-share">
          <MenuItem><Icons.Network/>Network Sharing</MenuItem>
        </NavLink>}
        {environment.flavor === Flavors.Zaber && !environment.offline && <NavLink to="/cloud">
          <MenuItem><Icons.Cloud/>Cloud</MenuItem>
        </NavLink>}

        <Section>
          Applications
        </Section>
        <NavLink to="/apps">
          <MenuItem><Icons.Applications/>All Applications</MenuItem>
        </NavLink>

        <RecentApps/>

        {state !== 'collapsed' && <>
          <Section>
            My Launcher
          </Section>
          <Link to="" target="_blank">
            <MenuItem><Icons.NewWindow/>New Window</MenuItem>
          </Link>
          {environment.flavor === Flavors.Zaber && <NavLink to="/preferences">
            <MenuItem><Icons.Settings/>Preferences</MenuItem>
          </NavLink>}
          <NavLink to="/contact">
            <MenuItem><Icons.Chat/>Contact Us</MenuItem>
          </NavLink>
          <NavLink to="/about">
            <MenuItem><Icons.Information/>About</MenuItem>
          </NavLink>
        </>}

        <div className="spacer"/>

        {state === 'collapsed' && <Icons.Feedback className="feedback-icon" appearsClickable/>}
        {state !== 'collapsed' && <FeedbackButton/>}

      </div>
      <div className={classNames('width-placeholder', { animate: state === 'collapsed' })}/>
    </div>
  );
};
