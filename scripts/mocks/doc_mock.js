/* eslint-disable max-len */
module.exports = `<html>
  <article id="topic_setting_system_access">
    <p class="shortdesc">The access level of the user</p>` +
// For testing filtering by capability
`
    <p data-capability="foo">This user can see foo</p>
    <p data-capability="bar">This user can see bar</p>` +
// For testing filtering by firmware version
`
    <p data-min-fw="7.10">This user can see min 7.10</p>
    <p data-min-fw="7.11">This user can see min 7.11</p>
    <p data-min-fw="7.09">This user can see min 7.09</p>
    <p data-max-fw="7.10">This user can see max 7.10</p>
    <p data-max-fw="7.11">This user can see max 7.11</p>
    <p data-max-fw="7.09">This user can see max 7.09</p>` +
// For testing link injection
`
    <p>Go look at 
      <a href="#topic_setting_another_setting" id="test-link-in-setting-doc"><span class="keyword asciisetting">another setting</span></a>
    </p>
    <p>But members of the public cannot see 
      <a href="#topic_setting_non_public" id="test-no-link-in-setting-doc"><span class="keyword asciisetting" data-disclosure="pub-hardware">Non-Public Setting</span></a>
    </p>
  </article>
  <article id="topic_setting_pos">
     <p>Go look at
       <a href="#topic_setting_another_setting" id="test-link-in-setting-doc">another setting</a>
     </p>
  </article>
  <article id="topic_setting_peripheral">
     <p>about peripheral in general</p>
     <article id="topic_setting_peripheral_id">
        <p>about peripheral.id</p>
     </article>
  </article>
  <dl>
    <dt class="dt dlterm" data-capability="motion" id="topic_message_format_warning_flags__topic_message_format_warning_flags_WR">
      <code class="ph codeph">WR</code>
    </dt>
    <dd class="dd" data-capability="motion">
      <p class="p"><dfn class="term">Warning</dfn> - <dfn class="term">No Reference Position</dfn></p>
      <p class="p">The axis does not have a reference position.</p>
      <p class="p">This warning persists until the axis position is updated by homing or any command/action that sets position.</p>
    </dd>
    <dt class="dt dlterm" data-capability="motion" id="topic_message_format_warning_flags__topic_message_format_warning_flags_FS">
      <code class="ph codeph">FS</code>
    </dt>
    <dd class="dd" data-capability="motion">
      <p class="p"><dfn class="term">Fault</dfn> - <dfn class="term">Stalled and Stopped</dfn></p>
      <p class="p">The stall is a terrible thing to happen.</p>
    </dd>
  </dl>
</html>`;
