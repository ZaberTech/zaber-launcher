const childProcess = require('child_process');
const fs = require('fs').promises;
const yml = require('yaml');

const editions = {
  DEV: 'dev',
  INTERNAL: 'internal',
  PUBLIC: 'public',
};

const currentEdition = process.env.ZL_EDITION || editions.DEV;
if (!Object.values(editions).includes(currentEdition)) {
  throw new Error(`Edition (ZL_EDITION) is not correct: ${currentEdition}`);
}

const flavors = {
  ZABER: 'zaber',
  REXROTH: 'rexroth',
};

// We replace $ZL_FLAVOR with empty string because CI passes the name when the value is not defined.
const currentFlavor = (process.env.ZL_FLAVOR != null ? process.env.ZL_FLAVOR.replace('$ZL_FLAVOR', '') : null) || flavors.ZABER;
if (!Object.values(flavors).includes(currentFlavor)) {
  throw new Error(`Flavor (ZL_FLAVOR) is not correct: ${currentFlavor}`);
}

if (process.env.ZL_OFFLINE != null && !['true', 'false'].includes(process.env.ZL_OFFLINE)) {
  throw new Error(`Offline Flag (ZL_OFFLINE) is not correct: ${currentFlavor}`);
}
const isOffline = process.env.ZL_OFFLINE === 'true';

const isCI = process.env.CI === 'true';
const shouldSign = isCI;

const exec = (...args) => new Promise((resolve, reject) => {
  const child = childProcess.exec(...args, err => err ? reject(err) : resolve());
  child.stdout.pipe(process.stdout);
  child.stderr.pipe(process.stderr);
});

async function readYaml(file) {
  const content = await fs.readFile(file, 'utf8');
  return yml.parse(content);
}
async function writeYaml(file, content) {
  await fs.writeFile(file, yml.stringify(content), 'utf8');
}

async function readJson(file) {
  const content = await fs.readFile(file, 'utf8');
  return JSON.parse(content);
}
async function writeJson(file, content) {
  await fs.writeFile(file, JSON.stringify(content, null, 2), 'utf8');
}

module.exports = {
  exec,
  isCI,
  shouldSign,
  currentEdition,
  editions,
  currentFlavor,
  flavors,
  isOffline,
  readYaml,
  writeYaml,
  readJson,
  writeJson,
};
