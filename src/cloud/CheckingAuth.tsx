import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Button } from '@zaber/react-library';

import { actions as actionsDefinition } from './actions';

interface Props {
  actions: typeof actionsDefinition;
}

class CheckingAuthBase extends Component<Props> {
  render(): React.ReactNode {
    const { actions } = this.props;

    return (
      <div>
        Cannot connect to Cloud. Make sure you are connected to internet.
        <Button onClick={actions.checkAuthNow}>Try Again</Button>
      </div>
    );
  }
}

export const CheckingAuth = connect(
  null,
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(CheckingAuthBase);
