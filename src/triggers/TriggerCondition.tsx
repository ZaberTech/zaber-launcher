import React from 'react';
import { useSelector } from 'react-redux';
import { useActions } from '@zaber/toolbox/lib/redux';
import _ from 'lodash';
import { SimpleSelect } from '@zaber/react-library';
import { P, match } from 'ts-pattern';

import type { EntityKey } from '../keys';
import { selectDevices } from '../connection_manager';
import { InputWithUnits, Measurement, UnitFrom } from '../units';

import { actions as actionsDefinition } from './actions';
import {
  TriggerConditionDistState,
  TriggerConditionIoState,
  TriggerConditionPosState,
  TriggerConditionSettingState,
  TriggerConditionState,
  TriggerConditionTimeState,
  defaultTriggerConditionDistState,
  defaultTriggerConditionEncoderState,
  defaultTriggerConditionIoState,
  defaultTriggerConditionPosState,
  defaultTriggerConditionSettingAbsoluteState,
  defaultTriggerConditionSettingState,
  defaultTriggerConditionState,
  defaultTriggerConditionTimeState,
  defaultValue
} from './reducer';
import {
  selectIoInfo, selectIsController, selectPosAxes, selectEncoderAxes, selectAxisCount, selectHasConditionSettingAbsolute
} from './selectors';
import { IoType, Operator, CONDITION_TYPES, IO_TYPES, OPERATORS, type ConditionType } from './types';
import { SelectAxis, SelectSetting } from './components';

interface TriggerConditionProps {
  deviceKey: EntityKey;
  triggerIndex: number;
  inputState: TriggerConditionState;
}

export const TriggerCondition: React.FC<TriggerConditionProps> = ({ deviceKey, triggerIndex, inputState }) => {
  const actions = useActions(actionsDefinition);

  const deviceInfo = useSelector(selectDevices)[deviceKey];
  const ioInfo = useSelector(selectIoInfo);
  const isController = useSelector(selectIsController);
  const posAxes = useSelector(selectPosAxes);
  const encoderAxes = useSelector(selectEncoderAxes);
  const hasConditionSettingAbsolute = useSelector(selectHasConditionSettingAbsolute);
  const axisCount = useSelector(selectAxisCount);

  const multiAxis = axisCount > 1 || isController;

  const changeInput = (state: Partial<TriggerConditionState>) =>
    actions.setConditionInput(triggerIndex, { ...inputState, ...state } as TriggerConditionState, true);

  const updateValue = (value: Measurement) => {
    const inputChanged = !(inputState.type !== 'none' && inputState.value.units === 'default');
    actions.setConditionInput(triggerIndex, { ...inputState, value } as TriggerConditionState, inputChanged);
  };

  const conditionTypes = CONDITION_TYPES.filter(type => {
    if (type === 'IO Channel') {
      return ioInfo != null;
    } else if (type === 'Trajectory Position') {
      return posAxes.length !== 0 || inputState.type === 'Trajectory Position';
    } else if (type === 'Distance Interval') {
      return posAxes.length !== 0 || inputState.type === 'Distance Interval';
    } else if (type === 'Encoder Distance Interval') {
      return encoderAxes.length !== 0 || inputState.type === 'Encoder Distance Interval';
    } else if (type === 'Absolute Setting') {
      return hasConditionSettingAbsolute;
    }
    return true;
  });

  return <>
    <SimpleSelect<ConditionType>
      title="Select Condition Type"
      className="select-condition-type"
      value={inputState.type === 'none' ? null : inputState.type}
      isSearchable
      onValueChange={type => {
        if (type === 'Trajectory Position') {
          changeInput(defaultTriggerConditionPosState);
        } else if (type === 'Distance Interval') {
          changeInput(defaultTriggerConditionDistState);
        } else if (type === 'Encoder Distance Interval') {
          changeInput(defaultTriggerConditionEncoderState);
        } else if (type === 'IO Channel') {
          changeInput(defaultTriggerConditionIoState);
        } else if (type === 'Time Elapsed') {
          changeInput(defaultTriggerConditionTimeState);
        } else if (type === 'Setting') {
          changeInput(defaultTriggerConditionSettingState);
        } else if (type === 'Absolute Setting') {
          changeInput(defaultTriggerConditionSettingAbsoluteState);
        } else {
          changeInput(defaultTriggerConditionState);
        }
      }}
      options={conditionTypes
        .map(type => (
          { value: type, label: type }
        ))}
    />
    {match(inputState)
      .with({ type: 'Trajectory Position' }, (state: TriggerConditionPosState) => {
        const axisKey = deviceInfo ? deviceInfo.axes[state.axis > 0 ? state.axis - 1 : 0] : null;
        const unitFrom = { setting: 'pos', entity: axisKey };
        return <>
          {multiAxis &&
            <SelectAxis
              axes={posAxes}
              axis={state.axis}
              onChange={axis => changeInput({ axis })}
            />}
          <SelectOperator
            operator={state.operator}
            onChange={operator => changeInput({ operator })}
          />
          <ConditionInput
            unitFrom={unitFrom}
            value={state.value}
            onChange={updateValue}
          />
        </>;
      })
      .with({ type: 'Distance Interval' }, (state: TriggerConditionDistState) => {
        const axisKey = deviceInfo ? deviceInfo.axes[state.axis > 0 ? state.axis - 1 : 0] : null;
        const unitFrom = { setting: 'pos', entity: axisKey };
        return <>
          {multiAxis &&
            <SelectAxis
              axes={posAxes}
              axis={state.axis}
              onChange={axis => changeInput({ axis })}
            />}
          <ConditionInput
            unitFrom={unitFrom}
            value={state.value}
            onChange={updateValue}
          />
        </>;
      })
      .with({ type: 'Encoder Distance Interval' },
        (state: TriggerConditionDistState) => {
          const axisKey = deviceInfo ? deviceInfo.axes[state.axis > 0 ? state.axis - 1 : 0] : null;
          const unitFrom = { setting: 'encoder.pos', entity: axisKey };
          return <>
            {multiAxis &&
              <SelectAxis
                axes={encoderAxes}
                axis={state.axis}
                onChange={axis => changeInput({ axis })}
              />}
            <ConditionInput
              unitFrom={unitFrom}
              value={state.value}
              onChange={updateValue}
            />
          </>;
        })
      .with({ type: 'IO Channel' }, (state: TriggerConditionIoState) => {
        const isDigital = ['Digital Input', 'Digital Output'].includes(state.ioType);
        return ioInfo && <>
          <SimpleSelect<IoType>
            title="Select IO Type"
            className="select-io-type"
            value={state.ioType}
            isSearchable
            onValueChange={ioType => changeInput({ ioType, ioChannel: 1, value: defaultValue })}
            options={IO_TYPES
              .filter(ioType => ioInfo[ioType] > 0)
              .map(ioType => ({ value: ioType, label: ioType }))}
          />
          <SimpleSelect<number>
            title="Select IO Channel"
            className="select-io-channel"
            value={state.ioChannel}
            isSearchable
            onValueChange={ioChannel => changeInput({ ioChannel })}
            options={_.range(1, ioInfo[state.ioType] + 1).map(i => ({ value: i, label: `Channel ${i}` }))}
          />
          <SelectOperator
            operator={state.operator}
            onChange={operator => changeInput({ operator })}
          />
          {isDigital &&
            <SimpleSelect<number>
              title="Select Digital Value"
              className="select-io-digital"
              value={state.value.value}
              isSearchable
              onValueChange={value => changeInput({ value: { value, units: '' } })}
              options={[{ value: 0, label: 'Off' }, { value: 1, label: 'On' }]}
            />
          }
          {!isDigital &&
            <ConditionInput
              unitFrom={{ command: 'trigger number when io ai channel relationaloperator value', entity: deviceKey }}
              value={state.value}
              onChange={updateValue}
            />
          }
        </>;
      })
      .with({ type: 'Time Elapsed' }, (state: TriggerConditionTimeState) =>
        <ConditionInput
          unitFrom={{ command: 'trigger number when time period', entity: deviceKey }}
          value={state.value}
          onChange={updateValue}
        />
      )
      .with({ type: P.union('Setting', 'Absolute Setting') }, (state: TriggerConditionSettingState) => {
        const axisKey = deviceInfo ? deviceInfo.axes[state.axis > 0 ? state.axis - 1 : 0] : null;
        const unitFrom = {
          setting: state.setting,
          entity: state.axis === 0 ? deviceKey : axisKey,
        };
        return <>
          {multiAxis &&
            <SelectAxis
              axes={axisCount}
              axis={state.axis}
              allowDevice
              onChange={axis => changeInput({
                axis,
                setting: '',
                value: defaultValue
              })}
            />}
          <SelectSetting
            settingsFor={state.axis}
            setting={state.setting}
            onChange={setting => changeInput({
              setting,
              value: defaultValue,
              axis: multiAxis ? state.axis : 0,
            })}
          />
          <SelectOperator
            operator={state.operator}
            onChange={operator => changeInput({ operator })}/>
          <ConditionInput
            unitFrom={unitFrom}
            value={state.value}
            onChange={updateValue}
          />
        </>;
      })
      .with({ type: 'none' }, () => null)
      .exhaustive()
    }
  </>;
};

interface SelectOperatorProps {
  operator: Operator;
  onChange: (operation: Operator) => void;
}

const SelectOperator: React.FC<SelectOperatorProps> = ({ operator, onChange }) =>
  <SimpleSelect<Operator>
    title="Select Operator"
    className="select-operator"
    value={operator}
    isSearchable
    onValueChange={onChange}
    options={OPERATORS.map(op => ({ value: op, label: op }))}
  />;

interface ConditionInputProps {
  unitFrom: UnitFrom;
  value: Measurement;
  onChange: (value: Measurement) => void;
}

const ConditionInput: React.FC<ConditionInputProps> = ({ unitFrom, value, onChange }) =>
  <InputWithUnits
    title="Condition Value"
    className="input-value"
    unitFrom={unitFrom}
    measure={value}
    onChange={onChange}
  />;
