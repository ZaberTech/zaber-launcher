import { Container, injectable } from 'inversify';

import { IPC } from './ipc';

@injectable()
export class IpcMock {
  broadcastAction = jest.fn().mockResolvedValue(undefined);
  sendActionToMainProcess = jest.fn().mockResolvedValue(undefined);
}

export function mockIpc(container: Container): IpcMock {
  container.bind<unknown>(IPC).to(IpcMock);
  return container.get<unknown>(IPC) as IpcMock;
}
