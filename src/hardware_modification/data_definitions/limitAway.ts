import { CommandFailedException } from '@zaber/motion';

import type { SectionDataDefinition } from '../types';

import { actionEnabled, actionRequiresPreset, limitEnabled, widthEnabled } from './limit';

export const limitAway: SectionDataDefinition<'limitAway'>  = {
  initializers: {
    type: { type: 'setting', backing: 'limit.away.type' },
    source: { type: 'setting', backing: 'limit.away.source' },
    edge: { type: 'setting', backing: 'limit.away.edge' },
    width: { type: 'setting', backing: 'limit.away.width' },
    action: { type: 'setting', backing: 'limit.away.action' },
    preset: { type: 'setting', backing: 'limit.away.preset' },
    posUpdate: { type: 'setting', backing: 'limit.away.posupdate' },
  },
  sectionPresent: async device => {
    try {
      await device.ascii.settings.get('limit.away.type');
    } catch (e) {
      if (e instanceof CommandFailedException && e.details.responseData === 'BADCOMMAND') {
        return false;
      }
      throw e;
    }
    return true;
  },
  fieldEnabled: {
    source: limitEnabled,
    edge: limitEnabled,
    width: widthEnabled,
    action: limitEnabled,
    posUpdate: actionEnabled,
    preset: actionRequiresPreset,
  },
};
