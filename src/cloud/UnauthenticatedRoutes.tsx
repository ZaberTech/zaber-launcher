import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { Intro } from './Intro';
import { Login } from './Login';

export const UnauthenticatedRoutes: React.FC = () => (
  <Switch>
    <Route path="/cloud/intro" component={Intro}/>
    <Route path="/cloud/login" component={Login}/>
    <Route><Redirect to="/cloud/intro"/></Route>
  </Switch>
);
