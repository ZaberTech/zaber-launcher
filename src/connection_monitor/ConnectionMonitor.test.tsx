import React, { useState } from 'react';
import { RenderResult, render } from '@testing-library/react';
import { injectable } from 'inversify';
import { ascii } from '@zaber/motion';

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore } from '../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';

import {
  mockSingleDevice,
} from '../connection_manager/mocks';
import { connectionManagerActions } from '../connection_manager';
import { ConnectionMonitor } from './ConnectionMonitor';
import { makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';
import { TEST_SKIP_SAGA_DELAYS } from '../utils';
import { DeviceReachable } from './DeviceReachable';
import { selectDevicesFF } from './selectors';

let reloadAllowed = false;
const loadDevicesActionMock = jest.fn(() => {
  if (!reloadAllowed) { throw new Error('Not allowed') }
  return ({ type: 'NOTHING', payload: undefined });
});
connectionManagerActions.loadDevices = loadDevicesActionMock;

class AxisMock extends AxisMockBase {
}

class DeviceMock extends DeviceMockBase<AxisMock> { }

let connections: ConnectionMock[] = [];
let connectionCallback: (c: ConnectionMock) => void;

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  constructor(id: string, router: RouterConnectionMock) {
    super(id, router);
    connections.push(this);
    connectionCallback?.(this);
  }
  genericCommandMultiResponse = jest.fn<Promise<Partial<ascii.Response>[]>, []>().mockResolvedValue([]);
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const ROUTER_KEY = makeRouterKey(LOCAL_ROUTER_URL);
const CONNECTION_KEY = makeConnectionKey(ROUTER_KEY, 'COM5');
const DEVICE_KEY = makeDeviceKey(CONNECTION_KEY, 1);
const DEVICE_ID = 50081;

const TestMonitor = wrapWithNewStore(() => {
  const [enabled, setEnabled] = useState(true);
  return <>
    <ConnectionMonitor connectionKey={CONNECTION_KEY} enabled={enabled}>
      {error => <span>{error ?? 'No error'}</span>}
    </ConnectionMonitor>

    <DeviceReachable deviceKey={DEVICE_KEY}>
      {reachable => <span>{reachable ? 'Present' : 'Lost'}</span>}
    </DeviceReachable>

    {enabled && <button onClick={() => setEnabled(false)}>Disable</button>}
  </>;
});

let wrapper: RenderResult;

beforeEach(() => {
  reloadAllowed = false;

  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

  wrapper = render(<TestMonitor/>);
  mockSingleDevice(TestMonitor.testStore);
});

afterEach(() => {
  wrapper?.unmount();
  wrapper = null!;

  destroyContainer();

  connectionCallback = null!;
  connections = [];

  loadDevicesActionMock.mockClear();
});

test('displays error when connection fails', async () => {
  await waitUntilPass(() => wrapper.getByText('No error'));

  connections[0].genericCommandMultiResponse.mockRejectedValue(new Error('Connection Failed'));
  TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
  await waitUntilPass(() =>  wrapper.getByText('Connection Failed'));

  connections[0].genericCommandMultiResponse.mockResolvedValue([]);
  TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
  await waitUntilPass(() => wrapper.getByText('No error'));
});

test('can be disabled', async () => {
  await waitUntilPass(() => expect(connections[0].genericCommandMultiResponse).toHaveBeenCalled());
  wrapper.getByText('Disable').click();

  connections[0].genericCommandMultiResponse.mockRejectedValue(new Error('Not allowed'));
  TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
  await waitUntilPass(() => wrapper.getByText('No error'));
});

describe('reload devices', () => {
  beforeEach(() => {
    reloadAllowed = true;
  });

  test('reloads devices when a new device is detected', async () => {
    connections[0].genericCommandMultiResponse.mockResolvedValue([{ deviceAddress: 2, data: `${DEVICE_ID}`, replyFlag: 'OK' }]);
    TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    await waitUntilPass(() => expect(loadDevicesActionMock).toHaveBeenCalled());
  });

  test('reloads devices when device.id mismatch is detected', async () => {
    connections[0].genericCommandMultiResponse.mockResolvedValue([{ deviceAddress: 1, data: `${DEVICE_ID + 1}`, replyFlag: 'OK' }]);
    TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    await waitUntilPass(() => expect(loadDevicesActionMock).toHaveBeenCalled());
  });

  test('reloads devices when connection has a connection issue', async () => {
    TestMonitor.testStore.dispatch(
      connectionManagerActions.devicesLoadedErr(CONNECTION_KEY, { reason: 'connection', message: 'Connection failed' }));

    connections[0].genericCommandMultiResponse.mockRejectedValue(new Error('Connection Failed'));
    TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    await waitUntilPass(() => wrapper.getByText('Connection Failed'));

    connections[0].genericCommandMultiResponse.mockResolvedValue([]);
    TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    await waitUntilPass(() => expect(loadDevicesActionMock).toHaveBeenCalled());
  });

  test('detects devices with FF flag', async () => {
    connections[0].genericCommandMultiResponse.mockResolvedValue([
      { deviceAddress: 1, data: `${DEVICE_ID}`, replyFlag: 'OK', warningFlag: ascii.WarningFlags.CRITICAL_SYSTEM_ERROR },
      { deviceAddress: 2, data: `${DEVICE_ID}`, replyFlag: 'OK' },

    ]);
    TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    await waitUntilPass(() => expect(selectDevicesFF(TestMonitor.testStore.getState())).toEqual({ [DEVICE_KEY]: true }));
  });
});

describe('DeviceReachable', () => {
  test('displays when device is present', async () => {
    await waitUntilPass(() => wrapper.getByText('Lost'));

    connections[0].genericCommandMultiResponse.mockResolvedValueOnce([{ deviceAddress: 1, data: `${DEVICE_ID}`, replyFlag: 'OK' }]);
    TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    await waitUntilPass(() => wrapper.getByText('Present'));

    TestMonitor.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);
    await waitUntilPass(() => wrapper.getByText('Lost'));
  });
});
