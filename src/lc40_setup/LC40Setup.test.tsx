/* eslint-disable @typescript-eslint/no-this-alias */
import React from 'react';
import { injectable } from 'inversify';
import { render, RenderResult, fireEvent } from '@testing-library/react';
import {
  MovementFailedException, Velocity, MotionLibException, Length, CommandFailedException, CommandFailedExceptionData,
} from '@zaber/motion';
import _ from 'lodash';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { wrapWithNewStore, wrapWithRouter, waitUntilPass, defer } from '../test';
import { createContainer, destroyContainer } from '../container';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { MessageRoutersService } from '../message_router';
import { mockSingleDeviceWithPeripherals, mockSingleDevice } from '../connection_manager/mocks';
import { IdentifiedDeviceInfo, selectDevices } from '../connection_manager';
import { makeAxisKey } from '../keys';
import { SettingsMock } from '../test/mocks/settings';
import { WarningsMock } from '../test/mocks/warnings';
import { deviceSetupActions } from '../device_setup';

import { SAFE_APPROACH_SPEED_MM_PER_S, MAX_HOME_SENSOR_DISTANCE_HEURISTIC_MM, onIdentification, CHECK_ESTOP_PERIOD } from './sagas';
import { LC40Setup } from './LC40Setup';
import { BOSCH_REXROTH_SHB40_IDS, MAXIMUM_TRAVEL_LENGTH_MM, SetupStep } from './types';
import { selectSetupStep } from './selectors';

let axis: AxisMock;
let axisCallback: (axis: AxisMock) => void;
const ORIGINAL_APPROACH_SPEED = 5544;

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);
    axis = this;

    this.settings.values['limit.approach.maxspeed'] = { value: ORIGINAL_APPROACH_SPEED };

    if (axisCallback) { axisCallback(this) }
  }

  readonly settings = new SettingsMock();
  readonly warnings = new WarningsMock();

  public storageKeyExists = false;
  public storageValue = '';


  storage = {
    keyExists: jest.fn(() => this.storageKeyExists),
    getString: jest.fn(() => this.storageValue),
    setString: jest.fn((_key, value) => {
      this.storageValue = value;
      return Promise.resolve();
    }),
  };

  _pos = 0;
  getPosition = jest.fn(() => Promise.resolve(this._pos));

  genericCommand = jest.fn().mockResolvedValue({});
  waitUntilIdle = jest.fn().mockResolvedValue(undefined);
  home = jest.fn().mockResolvedValue(undefined);
  moveMin = jest.fn().mockResolvedValue(undefined);
  moveMax = jest.fn().mockResolvedValue(undefined);
  stop = jest.fn().mockResolvedValue(undefined);
  driverEnable = jest.fn().mockResolvedValue(undefined);
  driverDisable = jest.fn().mockResolvedValue(undefined);
}

let device: DeviceMock;
let deviceCallback: (device: DeviceMock) => void;

class DeviceMock extends DeviceMockBase<AxisMock> {
  readonly settings = new SettingsMock();
  readonly warnings = new WarningsMock();

  constructor(readonly deviceAddress: number, readonly connection: ConnectionMock) {
    super(deviceAddress, connection);
    device = this;

    if (deviceCallback) { deviceCallback(this) }
    this.settings.values['system.access'] = { value: 1 };
    this.settings.values['lockstep.numgroups'] = { value: 1 };
  }

  genericCommand = jest.fn().mockResolvedValue({});
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const TestLC40Setup = wrapWithNewStore(wrapWithRouter(LC40Setup));
let wrapper: RenderResult;

beforeAll(() => {
  jest.spyOn(deviceSetupActions, 'setupNeeded').mockImplementation(() => ({ type: 'NO_ACTION', payload: null! }));
  jest.spyOn(deviceSetupActions, 'setupNotNeeded').mockImplementation(() => ({ type: 'NO_ACTION', payload: null! }));
});

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

  wrapper = render(<TestLC40Setup/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  axis = null!;
  device = null!;
  axisCallback = null!;

  destroyContainer();

  jest.clearAllMocks();
});

function mockPeripherals(lockstepAxes: number[] = []) {
  mockSingleDeviceWithPeripherals(TestLC40Setup.testStore, {
    modifier: info => {
      info.locksteps = [{ groupNumber: 1, axisNumbers: lockstepAxes }];
      info.axes[0].identity.peripheralId = 70245;
      info.axes[1].identity.peripheralId = 70245;
      return info;
    },
    peripheralCount: 3,
    type: 'smart',
  });
}

describe('before device selected', () => {
  beforeEach(mockPeripherals);

  test('renders message when device is not supported', () => {
    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(makeAxisKey(device.key, 3));

    wrapper.getByText('The device or axis is not supported by this application.');
  });

  test('renders a message when controller is selected', () => {
    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    wrapper.getByText('You have selected a controller. Please select its axis or an integrated device.');
  });

  test('populates configuration when device is set up', async () => {
    axisCallback = newAxis => {
      newAxis.settings.values['driver.dir'] = { value: 1 };
      newAxis.settings.values['limit.max'] = { value: 250, units: Length.mm };
    };

    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(makeAxisKey(device.key, 1));

    await waitUntilPass(() => {
      expect((wrapper.getByTestId('travel-distance') as HTMLInputElement).value).toBe('250');
      expect(wrapper.getByTitle('Base Mount')).toHaveClass('selected');
      expect(wrapper.getByTitle('Right Orientation')).toHaveClass('selected');
    });
  });

  test('populates configuration when device is set up with carriage-mount stage', async () => {
    axisCallback = newAxis => {
      newAxis.settings.values['driver.dir'] = { value: 1 };
      newAxis.settings.values['limit.max'] = { value: 250, units: Length.mm };
      newAxis.storageKeyExists = true;
      newAxis.storageValue = 'Carriage Mount';
    };

    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(makeAxisKey(device.key, 1));

    await waitUntilPass(() => {
      expect((wrapper.getByTestId('travel-distance') as HTMLInputElement).value).toBe('250');
      expect(wrapper.getByTitle('Carriage Mount')).toHaveClass('selected');
      expect(wrapper.getByTitle('Left Orientation')).toHaveClass('selected');
    });
  });

  test('shows error message when retrieving config fails', async () => {
    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(makeAxisKey(device.key, 1));

    await waitUntilPass(() => {
      wrapper.getByText(/There was an error loading the configuration from the device./);
    });
  });
});

describe('device with peripherals selected', () => {
  beforeEach(async () => {
    mockPeripherals();

    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(makeAxisKey(device.key, 1));

    await waitUntilPass(() => {
      wrapper.getByTitle('Base Mount');
    });
  });

  describe('validation', () => {
    test('clicking test reveals validation and disables the button', () => {
      fireEvent.click(wrapper.getByTitle('Base Mount'));
      fireEvent.click(wrapper.getByTitle('Left Orientation'));
      expect(wrapper.getByTestId('save-and-test')).not.toHaveAttribute('disabled');

      fireEvent.click(wrapper.getByTestId('save-and-test'));

      expect(wrapper.getByTestId('save-and-test')).toHaveAttribute('disabled');
      wrapper.getByText(/Please enter a valid number/);
    });

    test('validates travel distance', () => {
      fireEvent.click(wrapper.getByTitle('Base Mount'));
      fireEvent.click(wrapper.getByTitle('Left Orientation'));
      fireEvent.click(wrapper.getByTestId('save-and-test'));

      fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 50000 } });
      wrapper.getByText('Travel distance must be less than or equal to 7000 mm.');

      fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 0 } });
      wrapper.getByText('Travel distance must be more than 0.');

      expect(wrapper.getByTestId('save-and-test')).toHaveAttribute('disabled');
    });

    test('enables button back once the validation issues are addressed', () => {
      expect(wrapper.queryByTestId('save-and-test')).toBeNull();
      fireEvent.click(wrapper.getByTitle('Base Mount'));
      fireEvent.click(wrapper.getByTitle('Left Orientation'));
      fireEvent.click(wrapper.getByTestId('save-and-test'));

      expect(wrapper.getByTestId('save-and-test')).toHaveAttribute('disabled');
      fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 150 } });

      expect(wrapper.getByTestId('save-and-test')).not.toHaveAttribute('disabled');
    });
  });

  describe('testing configuration', () => {
    beforeEach(() => {
      fireEvent.click(wrapper.getByTitle('Base Mount'));
      fireEvent.click(wrapper.getByTitle('Right Orientation'));
      fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 250 } });
    });

    afterEach(async () => {
      await waitUntilPass(() =>
        expect(
          [
            SetupStep.SAVE_WITH_TEST_COMPLETE,
            SetupStep.SAVE_WITH_TEST_ERROR,
            SetupStep.SAVE_WITHOUT_TEST_ERROR,
            SetupStep.SAVE_WITH_TEST_STOPPED
          ]
        ).toContain(selectSetupStep(TestLC40Setup.testStore.getState()))
      );
    });

    async function runTestToEnd(step: SetupStep): Promise<void> {
      fireEvent.click(wrapper.getByTestId('save-and-test'));
      fireEvent.click(wrapper.getByTitle('Save & Test'));
      await waitUntilPass(() =>
        expect(selectSetupStep(TestLC40Setup.testStore.getState())).toBe(step)
      );
    }

    describe('procedure tests', () => {
      test('writes the correct settings', async () => {
        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.settings.set.mock.calls).toContainEqual(['driver.dir', 1]);
        expect(axis.settings.set.mock.calls).toContainEqual(['encoder.dir', 0]);
        expect(axis.settings.set.mock.calls).toContainEqual(['limit.max', 250, 'Length:millimetres']);
        expect(axis.storageValue).toBe('Base Mount');
      });

      test('writes the correct settings left side', async () => {
        fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 150 } });
        fireEvent.click(wrapper.getByTitle('Left Orientation'));

        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.settings.set.mock.calls).toContainEqual(['driver.dir', 0]);
        expect(axis.settings.set.mock.calls).toContainEqual(['encoder.dir', 1]);
        expect(axis.settings.set.mock.calls).toContainEqual(['limit.max', 150, 'Length:millimetres']);
        expect(axis.storageValue).toBe('Base Mount');
      });

      test('writes the correct settings carriage-mount', async () => {
        fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 150 } });
        fireEvent.click(wrapper.getByTitle('Carriage Mount'));
        fireEvent.click(wrapper.getByTitle('Left Orientation'));

        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.settings.set.mock.calls).toContainEqual(['driver.dir', 1]);
        expect(axis.settings.set.mock.calls).toContainEqual(['encoder.dir', 0]);
        expect(axis.settings.set.mock.calls).toContainEqual(['limit.max', 150, 'Length:millimetres']);
        expect(axis.storageValue).toBe('Carriage Mount');
      });

      test('sets the access level and sets it back to previous level', async () => {
        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(device.settings.set.mock.calls).toEqual([
          ['system.access', 2],
          ['system.access', 1],
        ]);
      });

      test('disables the driver during setting of settings and enables it back', async () => {
        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.driverDisable).toHaveBeenCalled();
        expect(axis.driverEnable).toHaveBeenCalled();
      });

      test('tries to find the sensor', async () => {
        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.genericCommand).toHaveBeenCalledWith('tools gotolimit home neg 1 0');
      });

      test('tries to find the sensor on other side if the first attempt fails', async () => {
        axis.genericCommand.mockImplementation(async (command: string) => {
          if (command === 'tools gotolimit home neg 1 0') {
            axis.waitUntilIdle.mockRejectedValueOnce(new MovementFailedException('Failed', null!));
          }
        });

        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.genericCommand).toHaveBeenCalledWith('tools gotolimit home pos 1 0');
      });

      test('displays error when cannot find the home sensor', async () => {
        axis.genericCommand.mockImplementation(async (command: string) => {
          if (['tools gotolimit home neg 1 0', 'tools gotolimit home pos 1 0'].includes(command)) {
            axis.waitUntilIdle.mockRejectedValueOnce(new MovementFailedException('Failed', null!));
          }
        });

        await runTestToEnd(SetupStep.SAVE_WITH_TEST_ERROR);

        wrapper.getByText(/Cannot find the home sensor/);
        wrapper.getByText(/The configuration was saved successfully, but the test failed. Check the configuration and try again./);
      });

      test('sets safe approach speed and writes the original approach speed back', async () => {
        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.settings.set.mock.calls).toContainEqual(
          ['limit.approach.maxspeed', SAFE_APPROACH_SPEED_MM_PER_S, 'Velocity:millimetres per second']
        );
        expect(axis.settings.set.mock.calls).toContainEqual(['limit.approach.maxspeed', ORIGINAL_APPROACH_SPEED]);
      });

      test('tests the final configuration by homing', async () => {
        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.home).toHaveBeenCalled();
      });

      test('stops the axis as the first command to ensure that device is not moving and to clear possible warning flags', async () => {
        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.stop).toHaveBeenCalled();
      });

      test('moves slowly when checking travel distance', async () => {
        await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

        expect(axis.moveMax).toHaveBeenCalledWith({
          velocity: SAFE_APPROACH_SPEED_MM_PER_S,
          velocityUnit: Velocity.MILLIMETRES_PER_SECOND
        });
      });

      test('displays orientation error when the move from home sensors fails quickly', async () => {
        axis.moveMax.mockRejectedValueOnce(new MovementFailedException('Failed', null!));

        await runTestToEnd(SetupStep.SAVE_WITH_TEST_ERROR);

        expect(wrapper.getAllByText(/Motor orientation appears to be incorrect/)).not.toHaveLength(0);
        wrapper.getByText(/The configuration was saved successfully, but the test failed. Check the configuration and try again./);
      });

      test('displays travel distance error when device travels move than heuristic limit and crashes', async () => {
        axis._pos = 0;
        axis.moveMax.mockImplementationOnce(async () => {
          axis._pos = MAX_HOME_SENSOR_DISTANCE_HEURISTIC_MM + 1;
          await Promise.reject(new MovementFailedException('Failed', null!));
        });

        await runTestToEnd(SetupStep.SAVE_WITH_TEST_ERROR);

        expect(wrapper.getAllByText(/The travel length that you entered seems too long\./)).not.toHaveLength(0);
        wrapper.getByText(/The configuration was saved successfully, but the test failed. Check the configuration and try again./);
      });

      test('displays driver disabled error when driver cannot be enabled', async () => {
        axis.driverEnable.mockRejectedValueOnce(
          new CommandFailedException('', { responseData: 'DRIVERDISABLED' } as CommandFailedExceptionData));

        await runTestToEnd(SetupStep.SAVE_WITHOUT_TEST_ERROR);

        expect(wrapper.getAllByText(/Device has disabled driver\./)).not.toHaveLength(0);
        wrapper.getByText(/The configuration failed to save. Please try again./);
      });
    });

    describe('stop test', () => {
      test('issues stop and display stop message', async () => {
        const moveMaxDefer = defer<void>();
        axis.moveMax.mockImplementation(() => moveMaxDefer.promise);

        fireEvent.click(wrapper.getByTestId('save-and-test'));
        fireEvent.click(wrapper.getByTitle('Save & Test'));
        await waitUntilPass(() => expect(axis.moveMax).toHaveBeenCalled());

        expect(axis.stop).toHaveBeenCalledTimes(1);
        fireEvent.click(wrapper.getByTestId('stop-test'));

        await waitUntilPass(() => expect(selectSetupStep(TestLC40Setup.testStore.getState())).toBe(SetupStep.SAVE_WITH_TEST_STOPPED));
        moveMaxDefer.resolve();

        expect(axis.stop).toHaveBeenCalledTimes(2);
        wrapper.getByText(/The configuration was saved successfully, but the test was manually stopped./);
      });

      test('sets the original approach speed when cancelled', async () => {
        const moveMaxDefer = defer<void>();
        axis.moveMax.mockImplementation(() => moveMaxDefer.promise);

        fireEvent.click(wrapper.getByTestId('save-and-test'));
        fireEvent.click(wrapper.getByTitle('Save & Test'));
        await waitUntilPass(() => expect(axis.moveMax).toHaveBeenCalled());
        fireEvent.click(wrapper.getByTestId('stop-test'));

        await waitUntilPass(() =>
          expect(axis.settings.set.mock.calls).toContainEqual(['limit.approach.maxspeed', ORIGINAL_APPROACH_SPEED])
        );
        moveMaxDefer.resolve();
      });
    });

    test('displays success message', async () => {
      await runTestToEnd(SetupStep.SAVE_WITH_TEST_COMPLETE);

      wrapper.getByText(/Setup completed successfully/);
    });

    test('displays contact Zaber message if fails multiple times', async () => {
      axis.moveMax.mockRejectedValue(new MovementFailedException('Failed', null!));

      await runTestToEnd(SetupStep.SAVE_WITH_TEST_ERROR);
      await runTestToEnd(SetupStep.SAVE_WITH_TEST_ERROR);

      expect(wrapper.queryByText(/contact Zaber support/)).toBeNull();
      await runTestToEnd(SetupStep.SAVE_WITH_TEST_ERROR);
      wrapper.getByText(/contact Zaber support/);
      wrapper.getByText(/The configuration was saved successfully, but the test failed. Check the configuration and try again./);
    });

    test('displays dialog in case of unknown error', async () => {
      axis.moveMax.mockRejectedValueOnce(new MotionLibException('some strange error'));

      await runTestToEnd(SetupStep.SAVE_WITH_TEST_ERROR);
      wrapper.getByText(/some strange error/);

      fireEvent.click(wrapper.getByTitle('Close Dialog'));
      expect(wrapper.queryByText(/some strange error/)).toBeNull();
      wrapper.getByText(/The configuration was saved successfully, but the test failed. Check the configuration and try again./);
    });
  });

  test('shows warnings when saving without testing', async () => {
    fireEvent.click(wrapper.getByTitle('Base Mount'));
    fireEvent.click(wrapper.getByTitle('Right Orientation'));
    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 250 } });
    fireEvent.click(wrapper.getByTestId('save-without-test'));

    await waitUntilPass(() => {
      expect(axis.settings.set.mock.calls).toContainEqual(['driver.dir', 1]);
      expect(axis.settings.set.mock.calls).toContainEqual(['limit.max', 250, 'Length:millimetres']);
      expect(axis.storageValue).toBe('Base Mount');
      expect(wrapper.getByText(/Skipping the test can result in reversed motion/));
      expect(wrapper.getByText(/The configuration was saved successfully, but not tested/));
    });

    expect(axis.home).not.toHaveBeenCalled();
  });
});

describe('integrated device selected', () => {
  beforeEach(async () => {
    mockSingleDevice(TestLC40Setup.testStore, device => {
      device.identity.deviceId = 50911;
      return device;
    });

    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => {
      wrapper.getByTitle('Base Mount');
    });
  });

  test('sets up the device', async () => {
    fireEvent.click(wrapper.getByTitle('Carriage Mount'));
    fireEvent.click(wrapper.getByTitle('Left Orientation'));
    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 150 } });


    fireEvent.click(wrapper.getByTestId('save-and-test'));
    fireEvent.click(wrapper.getByTitle('Save & Test'));

    await waitUntilPass(() => wrapper.getByText('Test completed successfully'));
    expect(axis.settings.set.mock.calls).toContainEqual(['driver.dir', 1]);
    expect(axis.settings.set.mock.calls).toContainEqual(['limit.max', 150, 'Length:millimetres']);
    expect(axis.storageValue).toBe('Carriage Mount');
  });

  test('shows warnings when saving without testing', async () => {
    fireEvent.click(wrapper.getByTitle('Base Mount'));
    fireEvent.click(wrapper.getByTitle('Right Orientation'));
    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 250 } });
    fireEvent.click(wrapper.getByTestId('save-without-test'));

    await waitUntilPass(() => {
      expect(axis.settings.set.mock.calls).toContainEqual(['driver.dir', 1]);
      expect(axis.settings.set.mock.calls).toContainEqual(['limit.max', 250, 'Length:millimetres']);
      expect(axis.storageValue).toBe('Base Mount');
      expect(wrapper.getByText(/Skipping the test can result in reversed motion/));
      expect(wrapper.getByText(/The configuration was saved successfully, but not tested/));
    });

    expect(axis.home).not.toHaveBeenCalled();
  });
});

describe('identification hooks', () => {
  let device1: IdentifiedDeviceInfo;
  let device2: IdentifiedDeviceInfo;

  beforeEach(() => {
    mockSingleDevice(TestLC40Setup.testStore, device => {
      device1 = device as IdentifiedDeviceInfo;
      device.identity.deviceId = 50911;
      return device;
    });
    mockSingleDeviceWithPeripherals(TestLC40Setup.testStore, {
      deviceNumber: 2,
      modifier: device => {
        device2 = device as IdentifiedDeviceInfo;
        device2.axes[0].identity.peripheralId = 70245;
        return device;
      },
      peripheralCount: 2,
      type: 'smart',
    });
  });

  describe('needs setup (limit.max not set)', () => {
    beforeEach(() => {
      axisCallback = newAxis => {
        newAxis.settings.values['driver.dir'] = { value: 1 };
        newAxis.settings.values['limit.max'] = { value: MAXIMUM_TRAVEL_LENGTH_MM + 1, units: Length.mm };
      };
    });

    test('marks integrated device as needing setup', async () => {
      const task = TestLC40Setup.testStore.saga.run(function* () {
        yield onIdentification(device1);
      });
      await task.toPromise();
      expect(deviceSetupActions.setupNeeded).toHaveBeenCalledTimes(1);
    });
    test('marks peripheral as needing setup', async () => {
      const task = TestLC40Setup.testStore.saga.run(function* () {
        yield onIdentification(device2);
      });
      await task.toPromise();
      expect(deviceSetupActions.setupNeeded).toHaveBeenCalledTimes(1);
    });
  });

  describe('does not need setup (limit.max set)', () => {
    beforeEach(() => {
      axisCallback = newAxis => {
        newAxis.settings.values['driver.dir'] = { value: 1 };
        newAxis.settings.values['limit.max'] = { value: 250, units: Length.mm };
      };
    });

    test('marks integrated device as needing setup', async () => {
      const task = TestLC40Setup.testStore.saga.run(function* () {
        yield onIdentification(device1);
      });
      await task.toPromise();
      expect(deviceSetupActions.setupNotNeeded).toHaveBeenCalledTimes(1);
    });
    test('marks peripheral as needing setup', async () => {
      const task = TestLC40Setup.testStore.saga.run(function* () {
        yield onIdentification(device2);
      });
      await task.toPromise();
      expect(deviceSetupActions.setupNotNeeded).toHaveBeenCalledTimes(1);
    });
  });

  describe('does not need setup (internal product)', () => {
    beforeEach(() => {
      axisCallback = newAxis => {
        newAxis.settings.values['driver.dir'] = { value: 1 };
        newAxis.settings.values['limit.max'] = { value: MAXIMUM_TRAVEL_LENGTH_MM + 1, units: Length.mm };
      };
    });

    test('Internal integrated device does not need setup', async () => {
      mockSingleDevice(TestLC40Setup.testStore, device => {
        device1 = device as IdentifiedDeviceInfo;
        device.identity.deviceId = 50911;
        device.identity.serialNumber = 4294967295;
        return device;
      });

      const task = TestLC40Setup.testStore.saga.run(function* () {
        yield onIdentification(device1);
      });
      await task.toPromise();
      expect(deviceSetupActions.setupNotNeeded).toHaveBeenCalledTimes(1);
    });

    test('internal peripheral does not need setup', async () => {
      mockSingleDeviceWithPeripherals(TestLC40Setup.testStore, {
        deviceNumber: 2,
        modifier: device => {
          device2 = device as IdentifiedDeviceInfo;
          device2.axes[0].identity.peripheralId = 70245;
          device2.axes[0].serialNumber = 0;
          return device;
        },
        peripheralCount: 2,
        type: 'smart',
      });

      const task = TestLC40Setup.testStore.saga.run(function* () {
        yield onIdentification(device2);
      });
      await task.toPromise();
      expect(deviceSetupActions.setupNotNeeded).toHaveBeenCalledTimes(1);
    });
  });
});

describe('lockstep', () => {
  test('displays lockstep warning and disables test button', async () => {
    mockPeripherals([1, 2]);

    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(makeAxisKey(device.key, 1));

    await waitUntilPass(() => {
      wrapper.getByTitle('Base Mount');
    });

    wrapper.getByText(/This peripheral is in a lockstep group and can be configured, but not tested./);
    fireEvent.click(wrapper.getByTitle('Base Mount'));
    fireEvent.click(wrapper.getByTitle('Left Orientation'));
    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 100 } });
    expect(wrapper.getByTestId('save-and-test')).toBeDisabled();
  });

  test('does not display lockstep warning when lockstep enabled for different axes', async () => {
    mockPeripherals();

    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(makeAxisKey(device.key, 2));

    await waitUntilPass(() => {
      wrapper.getByTitle('Base Mount');
    });

    expect(wrapper.queryByText(/This peripheral is in a lockstep group and can be configured, but not tested./)).toBeNull();
    fireEvent.click(wrapper.getByTitle('Base Mount'));
    fireEvent.click(wrapper.getByTitle('Left Orientation'));
    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 100 } });
    expect(wrapper.getByTestId('save-and-test')).toBeEnabled();
  });
});

describe('device with E-Stop', () => {
  beforeAll(() => jest.useFakeTimers());
  afterAll(() => jest.useRealTimers());

  beforeEach(async () => {
    mockSingleDevice(TestLC40Setup.testStore, device => {
      device.identity.deviceId = BOSCH_REXROTH_SHB40_IDS[0];
      return device;
    });

    const device = _.sample(selectDevices(TestLC40Setup.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    jest.advanceTimersToNextTimer();
    await waitUntilPass(() => {
      wrapper.getByTitle('Base Mount');
    });

    fireEvent.click(wrapper.getByTitle('Base Mount'));
    fireEvent.click(wrapper.getByTitle('Right Orientation'));
    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 150 } });
  });

  test('test button is disabled when E-Stop is disconnected', async () => {
    axis.warnings.warnings.FH = true;
    jest.advanceTimersByTime(CHECK_ESTOP_PERIOD);
    await waitUntilPass(() => wrapper.getByText(/Disconnected/));

    fireEvent.click(wrapper.getByTestId('save-and-test'));

    await waitUntilPass(() => wrapper.getByText(/E-Stop is disconnected/));
    expect(wrapper.getByTestId('save-and-test')).toHaveAttribute('disabled');
  });

  test('check button enables the driver', async () => {
    axis.warnings.warnings.FH = true;
    jest.advanceTimersByTime(CHECK_ESTOP_PERIOD);
    await waitUntilPass(() => wrapper.getByText(/Disconnected/));

    axis.driverEnable.mockImplementationOnce(async () => {
      axis.warnings.warnings.FH = false;
    });

    fireEvent.click(wrapper.getByText('Check'));
    wrapper.getByText(/Checking\.\.\./);
    expect(wrapper.getByText('Check')).toHaveAttribute('disabled');

    await waitUntilPass(() => wrapper.getByText(/Connected/));
  });

  test('shows error if e-stop cannot be checked', async () => {
    axis.warnings.getFlags.mockRejectedValue(new Error('Cannot connect'));
    jest.advanceTimersByTime(CHECK_ESTOP_PERIOD);
    await waitUntilPass(() => wrapper.getByText(/Cannot connect/));
  });

  test('shows error if driver enable fails', async () => {
    axis.warnings.warnings.FH = true;
    jest.advanceTimersByTime(CHECK_ESTOP_PERIOD);
    await waitUntilPass(() => wrapper.getByText('Check'));

    axis.driverEnable.mockRejectedValueOnce(new Error('Something went wrong'));
    fireEvent.click(wrapper.getByText('Check'));

    await waitUntilPass(() => wrapper.getByText(/Something went wrong/));
  });
});
