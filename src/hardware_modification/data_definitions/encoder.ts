
import type { SectionValues } from '../types';

export enum EncoderMode {
  OFF = 0,
  DIRECT = 2,
  MOTOR_AND_DIRECT = 3,
  ABSOLUTE = 4,
  DISCONNECTED = 5,
}

export enum MotorType {
  STEPPER = 1,
  THREE_PHASE = 2,
  VOICE_COIL = 3,
}

function isOnMode(mode: number | null | undefined): boolean {
  return mode != null && mode !== EncoderMode.OFF && mode !== EncoderMode.DISCONNECTED;
}

type EncoderSectionValues = SectionValues<'encoder1'> | SectionValues<'encoder2'>;

export function encoderConversionRatioEditable(mode: number | null, values: EncoderSectionValues): boolean {
  if (!isOnMode(mode)) { return false }

  const directMode = mode === EncoderMode.DIRECT || mode === EncoderMode.MOTOR_AND_DIRECT;
  if (directMode) {
    const motorType = values.motorType?.is;
    if (motorType === undefined || motorType === MotorType.THREE_PHASE || motorType === MotorType.VOICE_COIL) {
      return false;
    }
  }
  return true;
}

export function encoderIsOn(values: EncoderSectionValues): boolean {
  return isOnMode(values.mode?.is);
}

export function encoderIsAnalog(values: EncoderSectionValues): boolean {
  return values.type?.is === 2;
}
