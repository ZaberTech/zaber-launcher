/* eslint-disable camelcase */
import React, { Component } from 'react';
import {
  ascii, CommandFailedException, CommandFailedExceptionData, NoValueForKeyException,
  CurrentControllerIntegralGain, CurrentControllerProportionalGain, Time, Length, Velocity, Acceleration, Angle,
} from '@zaber/motion';
import { Container, injectable } from 'inversify';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import _ from 'lodash';

declare namespace SelectMock {
  interface Item {
    isDisabled?: boolean;
  }
  interface Props {
    placeholder: string;
    value: EnumProtocolDataOption;
    onChange: (item: unknown) => void;
    options: EnumProtocolDataOption[];
    'data-testid': string;
    isDisabled: boolean;
  }
  interface State {
    items: Item[];
    input: string;
  }
}

class SelectMock extends Component<SelectMock.Props, SelectMock.State> {
  render() {
    const { placeholder, isDisabled, options, value, 'data-testid': testid, onChange } = this.props;

    return <input
      placeholder={placeholder}
      onChange={e => onChange(options.find(opt => `${opt.value}` === e.target.value))}
      value={value?.value ?? ''}
      data-testid={testid}
      data-options={options.map(opt => opt.label).join()}
      disabled={isDisabled}
    />;
  }

  static createFilter() { return true }
}

jest.mock('react-select', () => SelectMock);

HTMLElement.prototype.scrollIntoView = _.noop;

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter, mockStorage, StorageMock } from '../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase
} from '../test/mocks/ascii';
import { DeviceInfoWithAxes, selectAxes, selectDevices } from '../connection_manager';
import { MessageRoutersService } from '../message_router';
import { mockSingleDevice, mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import { IntersectionObserverMockBase } from '../test/mocks/intersection_observer';
import type { AxisState, DeviceState } from '../connection_manager/reducer';
import type { Setting, SettingDefinition, TuningInfo } from '../current_tuner/types';
import { CurrentTunerAPI } from '../current_tuner';
import { mockReloadDevices } from '../test/mocks/reload_devices';
import settingsData from '../protocol_manual/settings.json';
import { extractConnectionKey } from '../keys';

import { HardwareModificationApp } from './HardwareModificationApp';
import type { EnumProtocolDataOption, SettingData } from './types';
import { UNDEFINED_NAME } from './components/fields/SettingField';
import {
  DEFAULT_DRIVER_HOLD_FACTOR,
  DEFAULT_DRIVER_RUN_FACTOR,
  RATED_TO_HOLD_CURRENT,
  RATED_TO_MAXSPEED_FACTOR,
  RATED_TO_MAX_CURRENT,
  RATED_TO_RUN_CURRENT,
} from './data_definitions/motorCharacteristics';
import { EncoderMode } from './data_definitions/encoder';

let wrapper: RenderResult;
let container: Container;
let storageMock: StorageMock;

const EXTENDED_TIMEOUT = 3000;

class IntersectionObserverMock extends IntersectionObserverMockBase { }

(window as unknown as { IntersectionObserver: typeof IntersectionObserverMock }).IntersectionObserver = IntersectionObserverMock;

const scrollMock = jest.fn();
HTMLElement.prototype.scroll = scrollMock;

let axisUseSettings: Record<string, number | null> = {};
let axisUseStorage: Record<string, number | null> = {};
const axisDefaultSettings: Record<string, number | null> = {
  'maxspeed': 25000,
  'peripheral.hw.modified': 0,
  'peripheralid': 70000,
  // Other required settings
  'cloop.enable': 0,
  'cloop.servo.enable': 0,
  'encoder.port.default': 1,
};

let axes: AxisMock[];
let axis: AxisState;
let axisCallback: ((axis: AxisMock) => void) | null = null;
class AxisMock extends AxisMockBase {
  constructor(axisNumber: number, device: DeviceMockBase<AxisMockBase>) {
    super(axisNumber, device);
    axes.push(this);
    axisCallback?.(this);
  }

  identity?: Partial<ascii.AxisIdentity> = {
    axisType: ascii.AxisType.LINEAR,
  };

  settings = {
    get: jest.fn(async name => {
      const settingValue = axisUseSettings[name];
      if (settingValue == null) {
        throw new CommandFailedException(`BADCOMMAND: ${name}`, { responseData: 'BADCOMMAND' } as CommandFailedExceptionData);
      }
      return settingValue;
    }),
    set: jest.fn(async (name, value) => {
      if (failOnSet.includes(name)) {
        throw new CommandFailedException('BADCOMMAND: fail to set', null!);
      }
      if (axisUseSettings[name] == null) {
        throw new CommandFailedException(`BADCOMMAND: ${name}`, { responseData: 'BADCOMMAND' } as CommandFailedExceptionData);
      } else if (value > MOCK_SETTING_MAX_VALUE) {
        throw new CommandFailedException(`Command rejected: BADDATA: value for ${name} out of range.`, null!);
      }
      axisUseSettings[name] = value;
    }),
    canConvertNativeUnits: jest.fn(() => true),
    convertToNativeUnits: jest.fn((settings: string, value: number) => value * 100),
    setCustomUnitConversions: jest.fn(async () => undefined),
  };

  storage = {
    getBool: jest.fn(async name => {
      const settingValue = axisUseStorage[name];
      if (settingValue == null) {
        throw new NoValueForKeyException(`No value has been stored with key '${name}'`);
      }
      return Promise.resolve(!!settingValue);
    }),
    setBool: jest.fn(async () => undefined),
    getNumber: jest.fn(async name => {
      const settingValue = axisUseStorage[name];
      if (settingValue == null) {
        throw new NoValueForKeyException(`No value has been stored with key '${name}'`);
      }
      return settingValue;
    }),
    setNumber: jest.fn(async () => undefined),
    getString: jest.fn(async () => {
      throw new NoValueForKeyException('no value');
    }),
  };

  driverEnable = jest.fn().mockResolvedValue(undefined);
  driverDisable = jest.fn().mockResolvedValue(undefined);
}

let devices: DeviceMock[];
let device: DeviceState;
const MOCK_SETTING_MAX_VALUE = 1000000000;
const defaultSettings: Record<string, number | null> = {
  // General
  'system.access': 1,
  'motor.type': 1,
  // Motion
  'limit.cycle.dist': 0,
  'limit.range.mode': 0,
  'limit.min': 0,
  'limit.max': 1000,
  // Encoder
  'device.hw.modified': 1,
  // C Loop Type
  'cloop.controller.type.calc': 2,
  'cloop.controller.type': 1,
};
const deviceAnalogEncoderValue = 1001;
const axisAnalogEncoderValue = 1002;
const encoderSettings = {
  // Encoder 1
  'encoder.1.mode': 1,
  'encoder.1.type': 0,
  'encoder.1.dir': 1,
  'encoder.1.fault.type': 1,
  'encoder.1.index.mode': 0,
  'encoder.1.filter': 1,
  'encoder.1.ratio.mult': 1,
  'encoder.1.ratio.div': 1,
  'encoder.1.ref.phase': 1,

  // Encoder 2
  'encoder.2.mode': 0,
  'encoder.2.type': 1,
  'encoder.2.dir': 1,
  'encoder.2.fault.type': 1,
  'encoder.2.index.mode': 0,
  'encoder.2.filter': 1,
  'encoder.2.ratio.mult': 1,
  'encoder.2.ratio.div': 1,
  'encoder.2.interpolation': 1,
  'encoder.2.signal.min': 1,
  'encoder.2.cos.dc': axisAnalogEncoderValue,
  'encoder.2.cos.dc.tune': deviceAnalogEncoderValue,
  'encoder.2.cos.gain': axisAnalogEncoderValue,
  'encoder.2.cos.gain.tune': deviceAnalogEncoderValue,
  'encoder.2.sin.dc': axisAnalogEncoderValue,
  'encoder.2.sin.dc.tune': deviceAnalogEncoderValue,
  'encoder.2.sin.gain': axisAnalogEncoderValue,
  'encoder.2.sin.gain.tune': deviceAnalogEncoderValue,

  // Other required settings
  'cloop.enable': 0,
  'cloop.servo.enable': 0,
  'encoder.port.default': 1,
};
const limitSettings = {
  // home limit
  'limit.home.type': 0,
  'limit.home.source': 0,
  'limit.home.edge': 0,
  'limit.home.width': 0,
  'limit.home.bidirectional': 0,
  'limit.home.action': 0,
  'limit.home.preset': 0,
  'limit.home.posupdate': 0,

  // away limit
  'limit.away.type': 0,
  'limit.away.source': 0,
  'limit.away.edge': 0,
  'limit.away.width': 0,
  'limit.away.action': 0,
  'limit.away.preset': 0,
  'limit.away.posupdate': 0,

  // C limit
  'limit.c.type': 0,
  'limit.c.source': 0,
  'limit.c.edge': 0,
  'limit.c.width': 0,
  'limit.c.pos': 0,
  'limit.c.action': 0,
  'limit.c.preset': 0,
  'limit.c.posupdate': 0,
};
let deviceUseSettings: Record<string, number | null> = {};
/** A list of settings that will always fail when set is called */
let failOnSet: string[];
/** A list of settings that should be treated as non-existent for a test */
let nonExistentSettings: string[];
class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(id: number, connection: ConnectionMock) {
    super(id, connection);
    devices.push(this);
  }

  identity?: Partial<ascii.DeviceIdentity>;
  axisCount: number = 0;
  settings = {
    get: jest.fn(async name => {
      const settingValue = deviceUseSettings[name];
      if (settingValue == null || nonExistentSettings.includes(name)) {
        throw new CommandFailedException(`BADCOMMAND: ${name}`, { responseData: 'BADCOMMAND' } as CommandFailedExceptionData);
      }
      return settingValue;
    }),
    set: jest.fn(async (name, value) => {
      if (failOnSet.includes(name) || nonExistentSettings.includes(name)) {
        throw new CommandFailedException('BADCOMMAND: fail to set', null!);
      }
      if (deviceUseSettings[name] == null) {
        throw new CommandFailedException(`BADCOMMAND: ${name}`, { responseData: 'BADCOMMAND' } as CommandFailedExceptionData);
      } else if (value > MOCK_SETTING_MAX_VALUE) {
        throw new CommandFailedException(`Command rejected: BADDATA: value for ${name} out of range.`, null!);
      }
      deviceUseSettings[name] = value;
    }),
  };

  identify = jest.fn(async () => this.identity);

  allAxes = {
    driverEnable: jest.fn().mockResolvedValue(undefined),
    driverDisable: jest.fn().mockResolvedValue(undefined),
  };
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const firmwareVersion = { major: 7, minor: 24, build: 8074 };

const TestHardwareModificationApp = wrapWithNewStore(wrapWithRouter(HardwareModificationApp));

const inputSettings: SettingDefinition[] = [
  { name: 'ictrl.delay', units: Time.SECONDS },
  { name: 'ictrl.period', units: Time.SECONDS },
];

const currentControllerParams: Setting[] = [
  { name: 'ictrl.pi.ki', value: 6108.652381980152, units: CurrentControllerIntegralGain.VOLTS_PER_AMP_PER_SECOND },
  { name: 'ictrl.pi.kp', value: 15.453803969016153, units: CurrentControllerProportionalGain.VOLTS_PER_AMP },
  { name: 'ictrl.type', value: 7 },
];
@injectable()
class CurrentTunerAPIMock {
  getInfo = jest.fn<Promise<TuningInfo>, []>(async () => ({
    defaultSliderPositions: {
      overallGain: -0.5, backEMFCompensation: 3, responseCorrectionSpeed: -0.5
    },
    slidersRange: {
      overallGain: { min: -2, max: 0, step: 0.25 },
      backEMFCompensation: { min: 1, max: 3, step: 0.25 },
      responseCorrectionSpeed: { min: -1.5, max: -0.5, step: 0.25 }
    },
    settings: [],
    inputSettings,
  }));

  getCurrentControllerParams = jest.fn<Promise<Setting[]>, []>(async () => currentControllerParams);
}

const loadDevicesModifier = jest.fn<void, [DeviceInfoWithAxes[]]>();
let reloadDevicesMock: ReturnType<typeof mockReloadDevices>;

beforeEach(() => {
  reloadDevicesMock = mockReloadDevices(() => TestHardwareModificationApp.testStore, loadDevicesModifier);

  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(CurrentTunerAPI).to(CurrentTunerAPIMock);
  storageMock = mockStorage(container);

  failOnSet = [];
  nonExistentSettings = [];
  axes = [];
  devices = [];
  deviceUseSettings = { ...defaultSettings };
  axisUseSettings = { ...axisDefaultSettings };
  axisUseStorage = {};

  wrapper = render(<TestHardwareModificationApp/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  axes = [];
  devices = [];
  deviceUseSettings = {};
  axisUseSettings = {};
  axisUseStorage = {};
  device = null!;

  destroyContainer();
  container = null!;
  storageMock.stored = {};

  scrollMock.mockClear();
  loadDevicesModifier.mockReset();

  axisCallback = null;
});

const deviceSettingsRows = () => Object.keys(deviceUseSettings).filter(name => (
  deviceUseSettings[name] != null
)).map(name => ({
  name,
  param_type: '',
  decimal_places: 0,
  visibility: 'always' as const,
}));

const mockDeviceWithUsedSettings = (isModified = true) => {
  const store = TestHardwareModificationApp.testStore;
  mockSingleDevice(store, d => ({
    ...d,
    identity: { ...d.identity, isModified, firmwareVersion },
    product: {
      ...d.product,
      settings: { ...d.product.settings, rows: deviceSettingsRows() },
      capabilities: [...d.product.capabilities, 'encoder-2-analog'],
    },
  }));
  device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
};

const mockDeviceAndAxisWithUsedSettings = () => {
  const store = TestHardwareModificationApp.testStore;
  const axisSettingRows = Object.keys(axisUseSettings)
    .filter(setting => !nonExistentSettings.includes(setting))
    .map(setting => ({
      name: setting,
      param_type: '',
      decimal_places: 0,
      visibility: 'always' as const,
    }));

  mockSingleDeviceWithPeripherals(
    store,
    {
      modifier: d => ({
        ...d,
        identity: { ...d.identity, firmwareVersion },
        product: {
          ...d.product,
          settings: { rows: deviceSettingsRows() },
          capabilities: [...d.product.capabilities, 'encoder-2-analog'],
        },
      }),
      peripheralCount: 1, type: 'thirdParty',
      axisModifier: a => ({
        ...a,
        identity: { ...a.identity, isModified: true },
        product: {
          ...a.product,
          settings: { rows: axisSettingRows },
        },
      })
    });
  axis = _.sample(selectAxes(TestHardwareModificationApp.testStore.getState()))!;
  device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
};

describe('Check setting data validity', () => {
  test('every enum option set includes each value only once', () => {
    for (const setting in settingsData) {
      const data: SettingData = settingsData[setting as keyof typeof settingsData];
      const alreadySeenValues = new Set<number>();
      for (const option of data.options ?? []) {
        expect(alreadySeenValues.has(option.value)).toBe(false);
        alreadySeenValues.add(option.value);
      }
    }
  });
});

describe('Hardware Modification Initialization', () => {
  beforeEach(() => {
    loadDevicesModifier.mockImplementation(devices => {
      devices[0].identity!.isModified = true;
      devices[0].identity!.firmwareVersion = firmwareVersion;
      devices[0].product!.settings.rows = deviceSettingsRows();
    });
  });

  test('Message when there is no device selected', () => {
    wrapper.getByText('Welcome to Advanced Hardware Setup!');
  });

  test('Message when device is unmodifiable', async () => {
    const store = TestHardwareModificationApp.testStore;
    mockSingleDevice(store, d => ({
      ...d,
      identity: { ...d.identity, firmwareVersion }
    }));
    device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText('Hardware modification not supported'));
  });

  test('Message when device has old firmware', async () => {
    const store = TestHardwareModificationApp.testStore;
    mockSingleDevice(store, d => ({
      ...d,
      identity: { ...d.identity, firmwareVersion: { major: 7, minor: 23, build: 8000 } },
      product: { ...d.product, settings: { ...d.product.settings, rows: deviceSettingsRows() } },
    }));
    device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText(/Advanced Hardware Setup is only support for devices with Firmware 7.24 or later./));
  });

  test('Message when device has really old firmware', async () => {
    const store = TestHardwareModificationApp.testStore;
    mockSingleDevice(store, d => ({
      ...d,
      identity: { ...d.identity, firmwareVersion: { major: 6, minor: 23, build: 8000 } },
      product: { ...d.product, settings: { ...d.product.settings, rows: deviceSettingsRows() } },
    }));
    device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText(/Advanced Hardware Setup is only supported for devices with Firmware 7./));
  });

  test('Message when the device is not hw.modified', async () => {
    deviceUseSettings['device.hw.modified'] = 0;
    mockDeviceWithUsedSettings(false);
    device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText('Enable hardware modification'));
    wrapper.getAllByText(
      `This ${device.identity?.name} has not been configured to support modified hardware (e.g., a new limit sensor or encoder). ` +
      'Click the "Enable" button to allow support for modified hardware.'
    );
    fireEvent.click(wrapper.getByText('Enable'));

    await waitUntilPass(() => wrapper.getAllByText('Apply'));
    expect(devices[0].settings.set).toHaveBeenCalledWith('system.access', 2);
    expect(devices[0].settings.set).toHaveBeenCalledWith('device.hw.modified', 1);
    expect(devices[0].settings.set).toHaveBeenCalledWith('system.access', 1);
  });

  test('Error during enable', async () => {
    deviceUseSettings['system.access'] = null;
    mockDeviceWithUsedSettings(false);
    device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText('Enable hardware modification'));
    fireEvent.click(wrapper.getByText('Enable'));

    await waitUntilPass(() => wrapper.getAllByText('Enabling hardware modification failed'));
    expect(devices[0].settings.set).not.toHaveBeenCalledWith('system.access', 2);
    expect(devices[0].settings.set).not.toHaveBeenCalledWith('device.hw.modified', 1);

    deviceUseSettings['system.access'] = 1;
    fireEvent.click(wrapper.getByText('Try again'));

    await waitUntilPass(() => wrapper.getAllByText('Apply'));
  });

  test('Enable a peripheral for modification', async () => {
    const store = TestHardwareModificationApp.testStore;
    mockSingleDeviceWithPeripherals(store, {
      peripheralCount: 1, type: 'smart', modifier: d => ({
        ...d, identity: { ...d.identity, firmwareVersion }
      })
    });
    device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    const axis = _.sample(selectAxes(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);

    await waitUntilPass(() => wrapper.getByText('Enable hardware modification'));
    wrapper.getAllByText(
      `Axis ${axis.axisNumber} of this ${device.identity?.name} has not been configured to support modified hardware ` +
      '(e.g., a new limit sensor or encoder). Click the "Enable" button to allow support for modified hardware.'
    );
    fireEvent.click(wrapper.getByText('Enable'));

    await waitUntilPass(() => {
      expect(axes[0]).toBeTruthy();
      expect(devices[0].settings.set).toHaveBeenCalledWith('system.access', 2);
      expect(axes[0].settings.set).toHaveBeenCalledWith('peripheral.hw.modified', 1);
      expect(devices[0].settings.set).toHaveBeenCalledWith('system.access', 1);
    });
  });

  test('Catches initialization error', async () => {
    mockDeviceWithUsedSettings();
    deviceUseSettings['limit.min'] = null;
    device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText('This device could not be initialized'));

    // Fix the error
    deviceUseSettings['limit.min'] = 0;
    fireEvent.click(wrapper.getByText('Try again'));
    await waitUntilPass(() => wrapper.getByText('General information about product motion.'));
  });
});

describe('modified product loaded', () => {
  beforeEach(async () => {
    mockDeviceWithUsedSettings();
    const device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getAllByText('Motion'));
  });

  async function remount() {
    const oldStore = TestHardwareModificationApp.testStore;
    wrapper.unmount();

    wrapper = render(<TestHardwareModificationApp store={oldStore}/>);
    await waitUntilPass(() => wrapper.getAllByText('Motion'));
  }

  test('remembers scroll position when remounted', async () => {
    wrapper.getByTestId('scrolling-sections').scrollTop = 1000;

    scrollMock.mockClear();
    await remount();

    await waitUntilPass(() => expect(scrollMock).toHaveBeenCalledWith({ top: 1000 }));
  });

  test('remembers changed values when remounted', async () => {
    let limitMinInput = wrapper.getByTestId('motion-limitMin-input') as HTMLInputElement;
    fireEvent.input(limitMinInput, { target: { value: '12345' } });

    await remount();

    limitMinInput = wrapper.getByTestId('motion-limitMin-input') as HTMLInputElement;
    expect(limitMinInput.value).toBe('12345');
  });

  test('scrolls to errors', async () => {
    const limitMinInput = wrapper.getByTestId('motion-limitMin-input') as HTMLInputElement;
    const limitMaxInput = wrapper.getByTestId('motion-limitMax-input') as HTMLInputElement;
    fireEvent.input(limitMinInput, { target: { value: `${MOCK_SETTING_MAX_VALUE + 2}` } });
    fireEvent.input(limitMaxInput, { target: { value: `${MOCK_SETTING_MAX_VALUE + 1}` } });
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => wrapper.getByText('2 changes could not be applied. Correct the values and press "Apply" again.'));

    const minErrorField = wrapper.getByText('Minimum Position must be less than Maximum Position')
      .parentElement!.parentElement!;
    const maxErrorField = wrapper.getByText('Maximum Position must be greater than Minimum Position')
      .parentElement!.parentElement!;

    minErrorField.scrollIntoView = jest.fn();
    maxErrorField.scrollIntoView = jest.fn();

    fireEvent.click(wrapper.getByTitle('Close Modal'));
    expect(minErrorField.scrollIntoView).toHaveBeenCalledWith({ behavior: 'smooth' });

    fireEvent.click(wrapper.getByText('See next error'));
    expect(maxErrorField.scrollIntoView).toHaveBeenCalledWith({ behavior: 'smooth' });

    fireEvent.click(wrapper.getByText('See next error'));
    expect(minErrorField.scrollIntoView).toHaveBeenCalledTimes(2);
  });
});

describe('Motion Section', () => {
  beforeEach(async () => {
    mockDeviceWithUsedSettings();
    const device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getAllByText('Motion'));
  });

  test('Renders Motion Section', async () => {
    expect(wrapper.getAllByText('Motion').length).toEqual(2);
    wrapper.getByText('General information about product motion.');
    expect(wrapper.queryAllByText(UNDEFINED_NAME)).toEqual([]);
  });

  test('Will not set invalid values', async () => {
    const limitMaxInput = wrapper.getByTestId('motion-limitMax-input') as HTMLInputElement;
    fireEvent.input(limitMaxInput, { target: { value: 'letters!' } });
    const applyButton = wrapper.getByText('Apply') as HTMLButtonElement;
    expect(applyButton.disabled).toBe(true);
    fireEvent.input(limitMaxInput, { target: { value: `${MOCK_SETTING_MAX_VALUE + 1}` } });
    expect(applyButton.disabled).toBe(false);
    fireEvent.click(applyButton);
    await waitUntilPass(() => wrapper.getByText('Command rejected: BADDATA: value for limit.max out of range.'));
  });

  test('Catches error on apply', async () => {
    const limitMinInput = wrapper.getByTestId('motion-limitMin-input') as HTMLInputElement;
    fireEvent.input(limitMinInput, { target: { value: `${deviceUseSettings['limit.min']! + 1}` } });

    deviceUseSettings['system.access'] = null;
    fireEvent.click(wrapper.getByText('Apply'));

    await waitUntilPass(() => {
      wrapper.getByText(/Could not prepare device for setting application: BADCOMMAND: system.access/);
      expect(wrapper.queryByText(/Validating changes/)).toBeNull();
    });
  });

  test('Revert values', async () => {
    const limitMinInput = wrapper.getByTestId('motion-limitMin-input') as HTMLInputElement;
    const newValue = `${deviceUseSettings['limit.min']! + 1}`;
    fireEvent.input(limitMinInput, { target: { value: newValue } });
    expect(limitMinInput.value).toBe(newValue);

    const cancelButton = wrapper.getByText('Cancel') as HTMLInputElement;
    expect(cancelButton.disabled).toBe(false);
    fireEvent.click(cancelButton);
    expect(limitMinInput.value).toBe(`${deviceUseSettings['limit.min']!}`);
  });

  test('Will update locked fields', async () => {
    const limitMinInput = wrapper.getByTestId('motion-limitMin-input') as HTMLInputElement;

    const oldValue = deviceUseSettings['limit.min']!;
    fireEvent.input(limitMinInput, { target: { value: `${oldValue + 1}` } });
    const isInfiniteInput = wrapper.getByTestId('motion-isInfinite-input') as HTMLInputElement;
    fireEvent.input(isInfiniteInput, { target: { value: '1' } });
    expect(limitMinInput.disabled).toBe(true);

    fireEvent.click(wrapper.getByText('Apply'));

    await waitUntilPass(() => expect(deviceUseSettings['limit.min']).toBe(-1000000000));
  });

  test('Cyclic motion toggle', async () => {
    const applyButton = wrapper.getByText('Apply');

    const cyclicMotionToggle = wrapper.getByTestId('motion-isCyclic-toggle');
    const cycleDistInput = wrapper.getByTestId('motion-cyclicDist-input') as HTMLInputElement;
    expect(cycleDistInput.disabled).toBe(true);
    fireEvent.click(cyclicMotionToggle);
    expect(cycleDistInput.disabled).toBe(false);
    fireEvent.click(applyButton);
    await waitUntilPass(() => wrapper.getByText('Cycle distance must be greater than 0 if this is a cyclic device'));
    fireEvent.click(cyclicMotionToggle);
    await waitUntilPass(() => expect(wrapper.queryByText('Cycle distance must be greater than 0 if this is a cyclic device')).toBeNull());
    fireEvent.click(cyclicMotionToggle);
    fireEvent.input(cycleDistInput, { target: { value: '1' } });
    fireEvent.click(applyButton);
    await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);
    fireEvent.click(wrapper.getByTitle('Close Modal'));
    expect(wrapper.queryByText(/Your changes have been successfully applied/)).toBeNull();
  });

  test('override unit conversions', async () => {
    const overrideToggle = wrapper.getByTestId('motion-unitConversionEnabled-toggle');
    const factorInput = wrapper.getByTestId('motion-unitConversion-input') as HTMLInputElement;

    expect(overrideToggle).toHaveAttribute('aria-checked', 'false');
    expect(factorInput).toHaveAttribute('disabled');

    fireEvent.click(overrideToggle);

    expect(factorInput).not.toHaveAttribute('disabled');
    expect(factorInput.value).toBe('100');
    fireEvent.input(factorInput, { target: { value: '1000' } });

    fireEvent.click(wrapper.getByText('Apply'));

    await waitUntilPass(() => expect(axes[0].settings.setCustomUnitConversions).toHaveBeenLastCalledWith([
      { setting: 'pos', value: 0.001, unit: Length.mm },
      { setting: 'maxspeed', value: 0.0006103515625, unit: Velocity['mm/s'] },
      { setting: 'accel', value: 6.103515625, unit: Acceleration['mm/s²'] },
    ]));

    fireEvent.click(overrideToggle);
    fireEvent.click(wrapper.getByText('Apply'));

    await waitUntilPass(() => expect(axes[0].settings.setCustomUnitConversions).toHaveBeenLastCalledWith([]));
  });
});

async function reloadDevices() {
  const connectionKey = extractConnectionKey(device.key);
  reloadDevicesMock.reload(connectionKey);
  await reloadDevicesMock.waitForReload;
  await waitUntilPass(() => wrapper.getAllByText('Motion'));
}

let e1mode: HTMLInputElement;
let e1type: HTMLInputElement;
let e2mode: HTMLInputElement;
let e2type: HTMLInputElement;
let e2ratio: HTMLInputElement;

describe('Encoder Section With Integrated Device', () => {
  beforeEach(async () => {
    deviceUseSettings = { ...defaultSettings, ...encoderSettings };
    mockDeviceWithUsedSettings();
  });

  const mockEncoderSection = async (motorType: number) => {
    deviceUseSettings['motor.type'] = motorType;

    const device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getByText('Encoder 1 (Single-ended Signalling)'));

    e1mode = wrapper.getByTestId('encoder1-mode-input') as HTMLInputElement;
    e1type = wrapper.getByTestId('encoder1-type-input') as HTMLInputElement;
    e2mode = wrapper.getByTestId('encoder2-mode-input') as HTMLInputElement;
    e2type = wrapper.getByTestId('encoder2-type-input') as HTMLInputElement;
    e2ratio = wrapper.getByTestId('encoder2-mult-input') as HTMLInputElement;
  };

  describe('Encoders with motor type 1', () => {
    beforeEach(async () => {
      await mockEncoderSection(1);
    });

    test('Renders Encoder Section', async () => {
      expect(wrapper.queryAllByText(UNDEFINED_NAME)).toEqual([]);
      expect(e1mode.value).toBe('1');
      expect(e2mode.value).toBe('0');
      expect(e1type.disabled).toBe(false);
      expect(e2type.disabled).toBe(true);
      expect(e2ratio.disabled).toBe(true);
    });

    test('Will not allow for both encoders being on', async () => {
      fireEvent.input(e2mode, { target: { value: '1' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => {
        const why = 'Products with a stepper motor may only have one encoder enabled at a time';
        wrapper.getByText(`${why}. Set this or encoder 2 mode to disconnected or disabled`);
        wrapper.getByText(`${why}. Set this or encoder 1 mode to disconnected or disabled`);
      });
    });

    test('Will allow switching to encoder 2', async () => {
      fireEvent.input(e1mode, { target: { value: '5' } });
      fireEvent.input(e2mode, { target: { value: '1' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);
    });

    test('Will allow switching both encoders off', async () => {
      fireEvent.input(e1mode, { target: { value: '5' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);
    });

    test('Error when default port cannot change', async () => {
      deviceUseSettings['encoder.port.default'] = null;
      fireEvent.input(e1mode, { target: { value: '5' } });
      fireEvent.input(e2mode, { target: { value: '1' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => wrapper.getByText('Could not set default encoder port: BADCOMMAND: encoder.port.default'));
    });

    test('Fail to set cloop.enable', async () => {
      deviceUseSettings['cloop.enable'] = 1;
      failOnSet = ['cloop.enable'];
      fireEvent.input(e1mode, { target: { value: '0' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => wrapper.getByText(/Error disabling closed-loop mode: BADCOMMAND: fail to set/));
    });

    test('Fail to set cloop.servo.enable', async () => {
      deviceUseSettings['cloop.servo.enable'] = 1;
      failOnSet = ['cloop.servo.enable'];
      fireEvent.input(e1mode, { target: { value: '0' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => wrapper.getByText(/Error disabling servo mode: BADCOMMAND: fail to set/));
    });

    test('Direct Mode with Motor Type 1 allows editing the ratio', () => {
      fireEvent.input(e1mode, { target: { value: '5' } });
      fireEvent.input(e2mode, { target: { value: `${EncoderMode.DIRECT}` } });
      expect(e2ratio.disabled).toBe(false);
    });

    test('Reads correct values for analog encoder', () => {
      const e2cosDc = wrapper.getByTestId('encoder2-cosDC-input') as HTMLInputElement;
      expect(e2cosDc.disabled).toBe(true);
      fireEvent.input(e2mode, { target: { value: `${EncoderMode.DIRECT}` } });
      fireEvent.input(e2type, { target: { value: '2' } });
      expect(e2cosDc.disabled).toBe(false);
      expect(e2cosDc.value).toBe(`${deviceAnalogEncoderValue}`);
      wrapper.getByText('(encoder.2.cos.dc.tune)');
      wrapper.getByText('(encoder.2.cos.gain.tune)');
      wrapper.getByText('(encoder.2.sin.dc.tune)');
      wrapper.getByText('(encoder.2.sin.gain.tune)');
    });

    test('Hides options based on profile (capability)', async () => {
      const allOptions = settingsData['encoder.1.type'].options.map(option => option.label);
      const options = e1type.getAttribute('data-options')!.split(',');

      expect(allOptions).toHaveLength(2);
      expect(options).toHaveLength(allOptions.length - 1);

      loadDevicesModifier.mockImplementation(devices => {
        devices[0].product!.capabilities.push('encoder-1-triphase');
      });
      await reloadDevices();

      e1type = wrapper.getByTestId('encoder1-type-input') as HTMLInputElement;
      const newOptions = e1type.getAttribute('data-options')!.split(',');
      expect(newOptions).toHaveLength(allOptions.length);
    });
  });

  describe('Encoders with motor type 2', () => {
    beforeEach(async () => {
      await mockEncoderSection(2);
      expect(e2ratio.disabled).toBe(true);
    });

    test('set a direct and absolute encoder', async () => {
      fireEvent.input(e1mode, { target: { value: '4' } });
      fireEvent.input(e2mode, { target: { value: '2' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);
    });

    test('Requires a direct and absolute encoder', async () => {
      fireEvent.input(e2mode, { target: { value: '1' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => {
        const error = 'Products with three-phase direct drive motors must have a direct encoder and an absolute phase encoder';
        expect(wrapper.getAllByText(error).length).toBe(2);
      });
    });

    test('Error when default port cannot change', async () => {
      deviceUseSettings['encoder.port.default'] = null;
      fireEvent.input(e1mode, { target: { value: '4' } });
      fireEvent.input(e2mode, { target: { value: '2' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => {
        expect(wrapper.getAllByText('Could not set default encoder port: BADCOMMAND: encoder.port.default').length).toBe(2);
      });
    });

    test('Disables ratio fields while the encoder is direct', () => {
      const mult1 = wrapper.getByTestId('encoder1-mult-input') as HTMLInputElement;
      const mult2 = wrapper.getByTestId('encoder2-mult-input') as HTMLInputElement;

      // Set to non-direct modes
      fireEvent.input(e1mode, { target: { value: '1' } });
      fireEvent.input(e2mode, { target: { value: '1' } });

      expect(mult1.disabled).toBe(false);
      expect(mult2.disabled).toBe(false);

      // Set to direct modes
      fireEvent.input(e1mode, { target: { value: '2' } });
      fireEvent.input(e2mode, { target: { value: '3' } });

      expect(mult1.disabled).toBe(true);
      expect(mult2.disabled).toBe(true);
    });
  });

  describe('Encoders with motor type 3', () => {
    beforeEach(async () => {
      await mockEncoderSection(3);
      expect(e2ratio.disabled).toBe(true);
    });

    test('set a direct encoder', async () => {
      fireEvent.input(e1mode, { target: { value: '2' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);
    });

    test('Requires a direct encoder', async () => {
      fireEvent.input(e1mode, { target: { value: '3' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => {
        wrapper.getByText('Products with voice coil motors must have a direct encoder enabled. Switch this or encoder 2 mode to direct');
      });
    });

    test('Voice coil only allows one encoder', async () => {
      fireEvent.input(e1mode, { target: { value: '3' } });
      fireEvent.input(e2mode, { target: { value: '2' } });
      fireEvent.click(wrapper.getByText('Apply'));
      await waitUntilPass(() => {
        expect(wrapper.getAllByText(/Products with voice coil motors may only have one encoder enabled at a time/).length).toBe(2);
      });
    });
  });
});

type LimitSectionElements = {
  type: HTMLInputElement;
  source: HTMLInputElement;
  action: HTMLInputElement;
  posUpdate: HTMLInputElement;
  preset: HTMLInputElement;
};

describe('Limit Sensors', () => {
  // Home
  let homeElements: LimitSectionElements;
  let awayElements: LimitSectionElements;
  let cElements: LimitSectionElements;

  beforeEach(async () => {
    deviceUseSettings = { ...defaultSettings, ...encoderSettings, ...limitSettings };
    mockDeviceWithUsedSettings();

    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getAllByText('Home Limit Sensor'));

    const getLimitSectionElements = (sectionName: string): LimitSectionElements => ({
      type: wrapper.getByTestId(`${sectionName}-type-input`) as HTMLInputElement,
      source: wrapper.getByTestId(`${sectionName}-source-input`) as HTMLInputElement,
      action: wrapper.getByTestId(`${sectionName}-action-input`) as HTMLInputElement,
      posUpdate: wrapper.getByTestId(`${sectionName}-posUpdate-input`) as HTMLInputElement,
      preset: wrapper.getByTestId(`${sectionName}-preset-input`) as HTMLInputElement,
    });

    homeElements = getLimitSectionElements('limitHome');
    awayElements = getLimitSectionElements('limitAway');
    cElements = getLimitSectionElements('limitC');
  });

  test('Renders Limit Sections', () => {
    expect(wrapper.queryAllByText(UNDEFINED_NAME)).toEqual([]);

    // Home
    expect(homeElements.type.value).toBe('0');
    expect(homeElements.source.value).toBe('');
    expect(homeElements.type.disabled).toBe(false);
    expect(homeElements.source.disabled).toBe(true);

    // Away
    expect(awayElements.type.value).toBe('0');
    expect(awayElements.source.value).toBe('');
    expect(awayElements.type.disabled).toBe(false);
    expect(awayElements.source.disabled).toBe(true);

    // C
    expect(cElements.type.value).toBe('0');
    expect(cElements.source.value).toBe('');
    expect(cElements.type.disabled).toBe(false);
    expect(cElements.source.disabled).toBe(true);
  });

  test('Enabling Limits', () => {
    for (const elements of [homeElements, awayElements, cElements]) {
      // If type is zero, everything is disabled
      fireEvent.input(elements.type, { target: { value: '0' } });
      expect(elements.action.disabled).toBe(true);
      expect(elements.posUpdate.disabled).toBe(true);
      expect(elements.preset.disabled).toBe(true);
      // Once it is 1 action should be enabled
      fireEvent.input(elements.type, { target: { value: '1' } });
      expect(elements.action.disabled).toBe(false);
      expect(elements.posUpdate.disabled).toBe(true);
      expect(elements.preset.disabled).toBe(true);
      // If action is 1, posupdate is enabled, but preset is not
      fireEvent.input(elements.action, { target: { value: '1' } });
      expect(elements.action.disabled).toBe(false);
      expect(elements.posUpdate.disabled).toBe(false);
      expect(elements.preset.disabled).toBe(true);
      // If action is 2, everything is enabled
      fireEvent.input(elements.action, { target: { value: '2' } });
      expect(elements.action.disabled).toBe(false);
      expect(elements.posUpdate.disabled).toBe(false);
      expect(elements.preset.disabled).toBe(false);
      // If type is set back to 0, everything is disabled
      fireEvent.input(elements.type, { target: { value: '0' } });
      expect(elements.action.disabled).toBe(true);
      expect(elements.posUpdate.disabled).toBe(true);
      expect(elements.preset.disabled).toBe(true);
    }
  });

  test('Will NOT update disabled fields', async () => {
    const applyButton = wrapper.getByText('Apply') as HTMLButtonElement;

    fireEvent.input(homeElements.type, { target: { value: '1' } });
    expect(homeElements.action.disabled).toBe(false);
    fireEvent.input(homeElements.action, { target: { value: '1' } });
    expect(applyButton.disabled).toBe(false);
    fireEvent.click(wrapper.getByTestId('limitHome-type-undo'));

    // Apply some changes to another section
    fireEvent.input(awayElements.type, { target: { value: '1' } });
    fireEvent.click(applyButton);
    await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);

    // Ensure that action is not effected by this
    expect(deviceUseSettings['limit.home.action']).toBe(0);
  });

  test('Index Pulse Form Validation', async () => {
    fireEvent.input(homeElements.type, { target: { value: '1' } });
    fireEvent.input(homeElements.source, { target: { value: '1' } });
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => {
      expect(wrapper.getAllByText('This source requires that at least one encoder has its index pulse mode enabled').length).toBe(3);
    });
    fireEvent.input(wrapper.getByTestId('encoder1-indexMode-input'), { target: { value: '1' } });
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);
  });
});

describe('Third party peripherals', () => {
  let applyButton: HTMLButtonElement;
  const thirdPartyPeripheralSettings = {
    ...axisDefaultSettings,
    ...limitSettings,
    // Third Party Peripheral
    'peripheralid': 88800,
    'peripheral.hw.modified': 1,
    'motor.type': 1,
    // Max speeds
    'accel': 0,
    'maxspeed': 0,
    'limit.detect.maxspeed': 0,
    'limit.approach.maxspeed': 0,
    'knob.maxspeed': 0,
    // Motion
    'limit.cycle.dist': 0,
    'limit.range.mode': 0,
    'limit.min': 0,
    'limit.max': 0,
    // Motor Characteristics
    'driver.dir': 0,
    'motion.accel.ramptime': 0,
    'motor.inductance': 0,
    'motor.resistance': 0,
    'motion.index.dist': 0,
    // Current settings
    'motor.current.max': 0,
    'driver.current.max': 10000,
    'driver.current.run': 0,
    'driver.current.approach': 0,
    'driver.current.hold': 0,
  };

  const steps = 250;
  const rpm = 1000;
  const rated = 0.65;
  const setupThirdPartyPeripheral = () => {
    fireEvent.input(wrapper.getByTestId('motorCharacteristics-stepsPerRotation-input'), { target: { value: `${steps}` } });
    fireEvent.input(wrapper.getByTestId('motorCharacteristics-rpm-input'), { target: { value: `${rpm}` } });
    fireEvent.input(wrapper.getByTestId('motorCharacteristics-ratedCurrent-input'), { target: { value: `${rated}` } });
    fireEvent.input(wrapper.getByTestId('motorCharacteristics-motorInductance-input'), { target: { value: '4' } });
    fireEvent.input(wrapper.getByTestId('motorCharacteristics-motorResistance-input'), { target: { value: '5.6' } });
    fireEvent.input(wrapper.getByTestId('motion-limitMax-input'), { target: { value: '100' } });
  };

  describe('Device With basic settings', () => {
    beforeEach(async () => {
      axisUseSettings = thirdPartyPeripheralSettings;
      mockDeviceAndAxisWithUsedSettings();
      connectionViewMockInstance.props.onSelect(axis.key);
      await waitUntilPass(() => wrapper.getAllByText('Motor Characteristics'));
      applyButton = wrapper.getByText('Apply') as HTMLButtonElement;
    });

    test('Requires certain fields from the start', async () => {
      fireEvent.input(wrapper.getByTestId('motion-limitMin-input'), { target: { value: '1' } });
      expect(applyButton.disabled).toBe(true);
      wrapper.getByText('Please fill in all fields to apply changes to a device');

      fireEvent.input(wrapper.getByTestId('motorCharacteristics-stepsPerRotation-input'), { target: { value: '250' } });
      expect(applyButton.disabled).toBe(true);
      fireEvent.input(wrapper.getByTestId('motorCharacteristics-rpm-input'), { target: { value: '1000' } });
      expect(applyButton.disabled).toBe(true);
      fireEvent.input(wrapper.getByTestId('motorCharacteristics-ratedCurrent-input'), { target: { value: '650' } });
      expect(applyButton.disabled).toBe(true);
      fireEvent.input(wrapper.getByTestId('motorCharacteristics-motorInductance-input'), { target: { value: '4' } });
      expect(applyButton.disabled).toBe(true);
      fireEvent.input(wrapper.getByTestId('motorCharacteristics-motorResistance-input'), { target: { value: '5.6' } });

      expect(applyButton.disabled).toBe(false);
    });

    test('Can set and store provided values', async () => {
      setupThirdPartyPeripheral();
      expect(applyButton.disabled).toBe(false);
      fireEvent.click(wrapper.getByText('Apply'));

      await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);

      expect(axes[0].storage.setBool).toHaveBeenCalledWith('zaber.3pp.setup', true);
      expect(axes[0].storage.setNumber.mock.calls).toEqual([
        ['zaber.3pp.rpm', rpm],
        ['zaber.3pp.steps', steps],
        ['zaber.3pp.rated', rated],
        ['zaber.3pp.run-factor', 70],
        ['zaber.3pp.hold-factor', 35],
      ]);

      const runCurrent = rated * DEFAULT_DRIVER_RUN_FACTOR * RATED_TO_RUN_CURRENT;
      const holdCurrent = rated * DEFAULT_DRIVER_HOLD_FACTOR * RATED_TO_HOLD_CURRENT;
      const speed = steps * rpm * RATED_TO_MAXSPEED_FACTOR;

      expect(axes[0].settings.set).toHaveBeenCalledWith('limit.max', 100);
      expect(axes[0].settings.set).toHaveBeenCalledWith('motor.inductance', 4);
      expect(axes[0].settings.set).toHaveBeenCalledWith('motor.resistance', 5.6);
      expect(axes[0].settings.set).toHaveBeenCalledWith('motor.current.max', rated * RATED_TO_MAX_CURRENT);
      expect(axes[0].settings.set).toHaveBeenCalledWith('driver.current.run', runCurrent);
      expect(axes[0].settings.set).toHaveBeenCalledWith('driver.current.approach', runCurrent);
      expect(axes[0].settings.set).toHaveBeenCalledWith('driver.current.hold', holdCurrent);
      expect(axes[0].settings.set).toHaveBeenCalledWith('maxspeed', speed);
      expect(axes[0].settings.set).toHaveBeenCalledWith('knob.maxspeed', speed);
      expect(axes[0].settings.set).toHaveBeenCalledWith('limit.approach.maxspeed', speed);
      expect(axes[0].settings.set).toHaveBeenCalledWith('limit.detect.maxspeed', speed / 16);
    });

    test('Works when controller has no knob', async () => {
      nonExistentSettings = ['knob.maxspeed'];
      mockDeviceAndAxisWithUsedSettings();
      connectionViewMockInstance.props.onSelect(axis.key);
      await waitUntilPass(() => wrapper.getAllByText('Motor Characteristics'));
      applyButton = wrapper.getByText('Apply') as HTMLButtonElement;
      setupThirdPartyPeripheral();
      expect(applyButton.disabled).toBe(false);
      fireEvent.click(applyButton);

      await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);

      const speed = steps * rpm * RATED_TO_MAXSPEED_FACTOR;

      expect(axes[0].settings.set).toHaveBeenCalledWith('maxspeed', speed);
      expect(axes[0].settings.set).not.toHaveBeenCalledWith('knob.maxspeed', speed);
    });

    test('can revert inputs', async () => {
      const stepsInput = wrapper.getByTestId('motorCharacteristics-stepsPerRotation-input') as HTMLInputElement;
      expect(stepsInput.value).toBe('');
      fireEvent.input(stepsInput, { target: { value: '1' } });
      expect(stepsInput.value).toBe('1');
      fireEvent.click(wrapper.getByTestId('motorCharacteristics-stepsPerRotation-undo'));
      expect(stepsInput.value).toBe('');

      const runSlider = wrapper.getByTestId('motorCharacteristics-runFactor-slider') as HTMLInputElement;
      expect(runSlider.value).toBe(`${DEFAULT_DRIVER_RUN_FACTOR}`);
      fireEvent.input(runSlider, { target: { value: '75' } });
      expect(runSlider.value).toBe('75');
      fireEvent.click(wrapper.getByTestId('motorCharacteristics-runFactor-undo'));
      expect(runSlider.value).toBe(`${DEFAULT_DRIVER_RUN_FACTOR}`);

      const holdSlider = wrapper.getByTestId('motorCharacteristics-holdFactor-slider') as HTMLInputElement;
      expect(holdSlider.value).toBe(`${DEFAULT_DRIVER_HOLD_FACTOR}`);
      fireEvent.input(holdSlider, { target: { value: '25' } });
      expect(holdSlider.value).toBe('25');
      fireEvent.click(wrapper.getByTestId('motorCharacteristics-holdFactor-undo'));
      expect(holdSlider.value).toBe(`${DEFAULT_DRIVER_HOLD_FACTOR}`);
    });

    test('Show custom docs', () => {
      fireEvent.click(wrapper.getByTitle('Show Rated Motor Current Documentation'));
      wrapper.getByText(/Mocked HTML data/);
    });

    test('Can reconfigure peripheral ID', async () => {
      fireEvent.click(wrapper.getByTitle('Open Menu'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText(/Reconfigure Peripheral Specs/)));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Reconfigure')));
      await waitUntilPass(() => wrapper.getByText('Resetting Peripheral...'));
    });
  });

  test('Axis Analog reads the correct settings', async () => {
    axisUseSettings = { ...thirdPartyPeripheralSettings, ...encoderSettings };
    mockDeviceAndAxisWithUsedSettings();
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getAllByText('Encoder 2 (Differential Signalling)'));
    e2mode = wrapper.getByTestId('encoder2-mode-input') as HTMLInputElement;
    e2type = wrapper.getByTestId('encoder2-type-input') as HTMLInputElement;
    const e2cosDc = wrapper.getByTestId('encoder2-cosDC-input') as HTMLInputElement;
    expect(e2cosDc.disabled).toBe(true);
    fireEvent.input(e2mode, { target: { value: `${EncoderMode.DIRECT}` } });
    fireEvent.input(e2type, { target: { value: '2' } });
    expect(e2cosDc.disabled).toBe(false);
    expect(e2cosDc.value).toBe(`${axisAnalogEncoderValue}`);
    wrapper.getByText('(encoder.2.cos.dc)');
    wrapper.getByText('(encoder.2.cos.gain)');
    wrapper.getByText('(encoder.2.sin.dc)');
    wrapper.getByText('(encoder.2.sin.gain)');
  });

  test('Controller with Back-EMF support', async () => {
    axisUseSettings = { ...thirdPartyPeripheralSettings, 'motor.ke': 0 };
    mockDeviceAndAxisWithUsedSettings();
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getAllByText('Holding Torque'));
    applyButton = wrapper.getByText('Apply') as HTMLButtonElement;

    // Holding Torque is required
    expect(applyButton.disabled).toBe(true);
    setupThirdPartyPeripheral();
    expect(applyButton.disabled).toBe(true);
    const torque = 23;
    fireEvent.input(wrapper.getByTestId('motorCharacteristics-holdingTorque-input'), { target: { value: `${torque}` } });
    expect(applyButton.disabled).toBe(false);

    // Stores and sets motor.ke
    fireEvent.click(applyButton);
    await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);
    expect(axes[0].storage.setNumber).toHaveBeenCalledWith('zaber.3pp.hold-torque', 23);
    expect(axes[0].settings.set).toHaveBeenCalledWith('motor.ke', 767.6255076445494);
  });

  test('applies default tuning for controller with current tuning support', async () => {
    axisUseSettings = {
      ...thirdPartyPeripheralSettings,
      'ictrl.type': 0,
      'ictrl.delay': 0,
      'ictrl.period': 0,
      'ictrl.pi.ki': 0,
      'ictrl.pi.kp': 0,
    };
    mockDeviceAndAxisWithUsedSettings();
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getAllByText('Motor Characteristics'));
    setupThirdPartyPeripheral();
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => wrapper.getByText(/Your changes have been successfully applied/), EXTENDED_TIMEOUT);

    for (const setting of inputSettings) {
      expect(axes[0].settings.get).toHaveBeenCalledWith(setting.name, setting.units);
    }

    for (const setting of currentControllerParams) {
      expect(axes[0].settings.set).toHaveBeenCalledWith(setting.name, setting.value, setting.units);
    }
  });

  describe('unit conversions', () => {
    async function preparePeripheral() {
      axisUseSettings = thirdPartyPeripheralSettings;
      mockDeviceAndAxisWithUsedSettings();
      connectionViewMockInstance.props.onSelect(axis.key);
      await waitUntilPass(() => wrapper.getByTestId('motion-unitConversionEnabled-toggle'));
      setupThirdPartyPeripheral();
    }

    test('sets up conversions', async () => {
      axisCallback = axis => {
        axis.identity!.peripheralId = thirdPartyPeripheralSettings.peripheralid;
        axis.settings.canConvertNativeUnits.mockReturnValueOnce(false);
      };

      await preparePeripheral();

      const overrideToggle = wrapper.getByTestId('motion-unitConversionEnabled-toggle');
      const factorInput = wrapper.getByTestId('motion-unitConversion-input') as HTMLInputElement;

      // enabled by default
      expect(factorInput).not.toHaveAttribute('disabled');
      expect(overrideToggle).toHaveAttribute('aria-checked', 'true');

      fireEvent.input(factorInput, { target: { value: '1000' } });

      applyButton = wrapper.getByText('Apply') as HTMLButtonElement;
      fireEvent.click(applyButton);

      await waitUntilPass(() => expect(axes[0].settings.setCustomUnitConversions).toHaveBeenLastCalledWith([
        { setting: 'pos', value: 0.001, unit: Length.mm },
        expect.objectContaining({ setting: 'maxspeed' }),
        expect.objectContaining({ setting: 'accel' }),
      ]));
    });

    test('sets up angular conversions', async () => {
      axisCallback = axis => {
        axis.identity!.peripheralId = thirdPartyPeripheralSettings.peripheralid;
        axis.identity!.axisType = ascii.AxisType.ROTARY;
      };

      await preparePeripheral();

      const factorInput = wrapper.getByTestId('motion-unitConversion-input') as HTMLInputElement;

      fireEvent.input(factorInput, { target: { value: '1000' } });

      applyButton = wrapper.getByText('Apply') as HTMLButtonElement;
      fireEvent.click(applyButton);

      await waitUntilPass(() => expect(axes[0].settings.setCustomUnitConversions).toHaveBeenLastCalledWith([
        { setting: 'pos', value: 0.36, unit: Angle.DEGREES },
        expect.objectContaining({ setting: 'maxspeed' }),
        expect.objectContaining({ setting: 'accel' }),
      ]));
    });
  });
});

describe('Closed Loop Post Validation', () => {
  beforeEach(async () => {
    mockDeviceWithUsedSettings();
    const device = _.sample(selectDevices(TestHardwareModificationApp.testStore.getState()))!;

    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getAllByText('Motion'));
  });

  test('Cannot calc cloop.controller.type', async () => {
    deviceUseSettings['cloop.controller.type.calc'] = 0;
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => {
      wrapper.getByText(/Unable to determine an appropriate target controller \(cloop.controller.type\) for the current configuration./);
    });
  });

  test('Cannot set cloop.controller.type', async () => {
    deviceUseSettings['cloop.controller.type'] = null;
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => wrapper.getByText(/Unable to set target controller \(cloop.controller.type\) to the proper value/));
  });
});
