/* eslint-disable camelcase */
jest.mock('../environment', () => {
  const actual = jest.requireActual('../environment');
  return ({
    ...actual,
    environment: {
      ...actual.environment,
      edition: 'public',
    },
  });
});

import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import {
  ascii, CommandFailedException, ConnectionFailedException, DeviceAddressConflictException, DeviceDbFailedException, NoDeviceFoundException,
  RequestTimeoutException,
} from '@zaber/motion';
import type { RecursivePartial } from '@zaber/toolbox';
import _ from 'lodash';
import { delay } from 'redux-saga/effects';


jest.mock('../add_connection/AddConnection', () => {
  const AddConnection: React.FC = () => <div>AddConnection</div>;
  AddConnection.displayName = 'AddConnection';
  return { AddConnection };
});

const PeripheralMock: React.FC<{ axis: AxisState }> = ({ axis }) => <div>{axis.identity?.peripheralName}</div>;
jest.mock('../peripherals/Axis', () => ({
  get Axis() {
    return PeripheralMock;
  },
}));

import { createContainer, destroyContainer } from '../container';
import { waitTick, waitUntilPass, wrapWithNewStore, wrapWithRouter, mockStorage, StorageMock } from '../test';
import { MessageRoutersService, LOCAL_ROUTER_URL } from '../message_router';
import { Connection, ConnectionType, ZaberApi } from '../app_components';
import {
  AxisMockBase, DeviceMockBase, ConnectionMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { EntityKey, makeAxisKey, makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';
import { Cache } from '../cache';
import { UNAUTHENTICATED_REALM } from '../add_connection/types';
import { IpcMock, mockIpc } from '../ipc/mock';
import { actions as connectionMonitorActions } from '../connection_monitor/actions';

import { ConnectionManager } from './ConnectionManager';
import { actions, ActionTypes } from './actions';
import { DeviceInfoWithAxes, IdentifiedDeviceInfo, ROUTERS_STORAGE_KEY, RouterType, StoredRouter } from './types';
import { makeCloudUrl } from './utils';
import { DevicesContext } from './devices';
import type { AxisState } from './reducer';
import { mockProduct } from './mocks';
import { IdentificationHooks } from './identification_hooks';
import { selectAxes, selectDevices } from './selectors';

let container: Container;

class LockstepMock {
  isEnabled = jest.fn(async () => false);
  getAxisNumbers = jest.fn<Promise<number[]>, []>(async () => []);
}

class AxisMock extends AxisMockBase {
  identity?: Partial<ascii.AxisIdentity>;
  settings = {
    canConvertNativeUnits: () => true,
  };
}

class DeviceMock extends DeviceMockBase<AxisMock> {
  identity?: Partial<ascii.DeviceIdentity>;
  axisCount: number = 0;
  _settings: Record<string, number> = {
    'bootloader.keyed': 0,
    'device.hw.modified': 0,
    'system.platform': 987343,
  };
  settings = {
    get: jest.fn(async name => {
      if (name in this._settings) { return this._settings[name] }
      throw new CommandFailedException('Failed', null!);
    }),
  };

  identify = jest.fn(async () => this.identity);

  genericCommand = jest.fn<Promise<{data: string}>, [string]>(async (cmd: string) => {
    switch (cmd) {
      case 'get peripheral.serial': return { data: _.range(this.axisCount).map(i => `${1000 + i}`).join(' ') };
      case 'get peripheral.id':
        throw new CommandFailedException('Command failed', null!);
      default: throw new Error(`Mock not implemented: ${cmd}`);
    }
  });

  getLockstep = jest.fn<LockstepMock, [number]>();
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  detectDevices = jest.fn<Promise<DeviceMock[]>, []>().mockRejectedValue(new NoDeviceFoundException(''));
  forgetDevices = jest.fn().mockResolvedValue(undefined);
  renumberDevices = jest.fn().mockResolvedValue(2);
  genericCommandMultiResponse = jest.fn().mockRejectedValue(new RequestTimeoutException('Timeout'));
}

const CONNECTION_ID = 'COM1';
const MOCK_CONNECTION_DATA: Connection = {
  id: CONNECTION_ID,
  type: ConnectionType.SERIAL_PORT,
  config: {
    serialPort: 'COM1',
    name: '',
    baudRate: 115200,
  },
};
const MOCK_CONNECTION_DATA2: Connection = {
  id: 'COM2',
  type: ConnectionType.SERIAL_PORT,
  config: {
    serialPort: 'COM2',
    name: '',
    baudRate: 115200,
  },
};
const MOCK_CONNECTION_DATA3: Connection = {
  id: 'COM3',
  type: ConnectionType.SERIAL_PORT,
  config: {
    serialPort: 'COM3',
    name: '',
    baudRate: 115200,
  },
};

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
  api = {
    getConnections: jest.fn().mockResolvedValue([MOCK_CONNECTION_DATA]),
    getName: jest.fn().mockResolvedValue('Router'),
    setConnectionName: jest.fn(),
    removeConnection: jest.fn().mockResolvedValue(undefined),
  };
}

let messageRoutersServiceMock: MessageRoutersServiceMock;

class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;

  dropConnection = jest.fn();
}

const TestConnectionManager = wrapWithNewStore(wrapWithRouter(ConnectionManager));

const loadRouters = () => TestConnectionManager.testStore.dispatch(actions.loadRouters());

let ipcMock: IpcMock;

@injectable()
class CacheMock {
  load = jest.fn().mockReturnValue(null);
  save = jest.fn();
}

let storageMock: StorageMock;

@injectable()
class ZaberApiMock {
  getDeviceInfo = jest.fn<Promise<RecursivePartial<ZaberApi.DeviceInfo>>, Parameters<ZaberApi['getDeviceInfo']>>().mockImplementation(
    ({ deviceID, peripherals }) => Promise.resolve({
      device: {
        product_id: 65123,
      },
      capabilities: ['cap1'],
      command_tree: { nodes: [] },
      conversion_table: { rows: [] },
      settings: deviceID === 31000
        ? {
          rows: [{
            name: 'comm.ethercat.remote',
            value: 1,
          }],
        }
        : { rows: [] },
      peripherals: (peripherals ?? []).map(() => ({
        device: {
          product_id: 11123,
        },
        capabilities: ['cap2'],
        command_tree: { nodes: [] },
        conversion_table: { rows: [] },
        settings: { rows: [] },
      }))
    })
  );
  getWebsiteImageForProducts = jest.fn<Promise<ZaberApi.WebsiteProductImage[]>, []>().mockImplementation(() => Promise.resolve([
    {
      id: 65123,
      thumbnail: '/ufs/thumbnail-image.html',
      image: '/ufs/full-size-image.html',
    },
    {
      id: 11123,
      thumbnail: '/ufs/thumbnail-image.html',
      image: '/ufs/full-size-image.html',
    },
  ]));
}

async function expandConnection(wrapper: RenderResult) {
  await waitUntilPass(() => wrapper.getByTitle('Expand Connection'));
  fireEvent.click(wrapper.getByTitle('Expand Connection'));
}

let wrapper: RenderResult;

beforeEach(() => {
  container = createContainer();
  ipcMock = mockIpc(container);
  storageMock = mockStorage(container);
  container.bind<unknown>(Cache).to(CacheMock);
  container.bind<unknown>(ZaberApi).to(ZaberApiMock);
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  messageRoutersServiceMock = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;

  wrapper = render(<TestConnectionManager/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
  messageRoutersServiceMock = null!;
  ipcMock = null!;
  storageMock = null!;
});

test('renders connection with no devices', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);

  loadRouters();

  await waitUntilPass(() => wrapper.getByText('COM1'));
  expect(connection.detectDevices).toBeCalled();
});

test('can collapse/expand connection', async () => {
  loadRouters();
  await waitUntilPass(() => wrapper.getByText('COM1'));

  fireEvent.click(wrapper.getByTitle('Collapse Connection'));
  fireEvent.click(wrapper.getByTitle('Expand Connection'));
});

const LHM_IDENTITY: ascii.DeviceIdentity = {
  axisCount: 1, deviceId: 50081, name: 'X-LHM', serialNumber: 1234,
  firmwareVersion: { major: 7, minor: 11, build: 8073 }, isModified: false, isIntegrated: true,
};
const LHM_PERIPHERAL_ID = 43211;
const XMCC_DEVICE_ID = 30341;
const EMCC_DEVICE_ID = 31000;

function mockDeviceAsLhm(mock: DeviceMock): void {
  mock.identity = LHM_IDENTITY;
  mock.axisCount = 1;
  mock.getAxis(1).identity = {
    axisType: ascii.AxisType.LINEAR,
    isPeripheral: false,
  };
}

function mockDeviceAsXmcc(mock: DeviceMock): void {
  mock.axisCount = 4;
  mock.identity = {
    axisCount: 4, deviceId: XMCC_DEVICE_ID, name: 'X-MCC4', serialNumber: 1235,
    firmwareVersion: { major: 7, minor: 13, build: 8074 }, isModified: false,
  };
  for (let i = 1; i <= 4; i++) {
    mock.getAxis(i).identity = {
      axisType: ascii.AxisType.LINEAR,
      isPeripheral: true,
      peripheralId: LHM_PERIPHERAL_ID,
      peripheralName: 'LHM025A-T3',
      isModified: false,
    };
  }
}


function mockDeviceAsEmcc(mock: DeviceMock): void {
  mock.axisCount = 1;
  mock.identity = {
    axisCount: 1, deviceId: EMCC_DEVICE_ID, name: 'E-MCC1', serialNumber: 1236,
    firmwareVersion: { major: 7, minor: 99, build: 17299 }, isModified: false,
  };
  mock._settings['comm.ethercat.remote'] = 1;

  mock.getAxis(1).identity = {
    axisType: ascii.AxisType.LINEAR,
    isPeripheral: true,
    peripheralId: LHM_PERIPHERAL_ID,
    peripheralName: 'LHM025A-T3',
    isModified: false,
  };
}


test('renders connection with one integrated and one composite device', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);

  const device1 = connection.getDevice(1);
  mockDeviceAsLhm(device1);

  const device2 = connection.getDevice(2);
  mockDeviceAsXmcc(device2);

  connection.detectDevices.mockImplementation(() => Promise.resolve([device1, device2]));

  loadRouters();
  await waitUntilPass(() => {
    wrapper.getByText(/X-LHM/);
    wrapper.getByText(/1234/);
    wrapper.getByText(/X-MCC4/);
    wrapper.getByText(/1235/);
  });

  expect(wrapper.queryAllByText(/LHM025A-T3/)).toHaveLength(4);
  expect(wrapper.queryAllByText(/EtherCAT/)).toHaveLength(0);
});

test('displays remote mode switch when supported', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);

  const device1 = connection.getDevice(1);
  mockDeviceAsEmcc(device1);

  connection.detectDevices.mockImplementation(() => Promise.resolve([device1]));

  loadRouters();
  await waitUntilPass(() => {
    wrapper.getByText(/E-MCC1/);
    wrapper.getByText(/1236/);
    wrapper.getByText(/EtherCAT/);
  });
});


test('queries lockstep information', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);
  const device = connection.getDevice(1) as DeviceMock;
  mockDeviceAsXmcc(device);
  connection.detectDevices.mockImplementation(() => Promise.resolve([device]));

  device._settings[ascii.SettingConstants.LOCKSTEP_NUMGROUPS] = 2;

  const lockstep = new LockstepMock();
  lockstep.isEnabled.mockResolvedValue(true);
  lockstep.getAxisNumbers.mockResolvedValue([1, 3]);

  device.getLockstep.mockImplementation(group => {
    switch (group) {
      case 1: return new LockstepMock();
      case 2: return lockstep;
      default: throw new Error('Invalid group');
    }
  });

  loadRouters();
  await waitUntilPass(() => wrapper.getByText(/X-MCC/));

  const deviceState = _.sample(selectDevices(TestConnectionManager.testStore.getState()))!;
  expect(deviceState.locksteps).toEqual([{
    groupNumber: 2,
    axisNumbers: [1, 3],
  }]);
});

test('renders a device that cannot be identified', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);

  const device1 = connection.getDevice(1);
  device1._settings.deviceid = 1111;
  device1._settings.version = 7.13;
  device1._settings['version.build'] = 1789;
  device1._settings['system.axiscount'] = 1;
  device1.identify.mockRejectedValueOnce(new DeviceDbFailedException('Cannot find device', { code: 'not-found' }));

  connection.detectDevices.mockImplementation(() => Promise.resolve([device1]));

  loadRouters();

  await waitUntilPass(() => wrapper.getByText(/Unknown Device 1111/));

  const deviceState = _.sample(selectDevices(TestConnectionManager.testStore.getState()))!;
  expect(deviceState.nonIdentifiedInfo).toMatchObject({
    deviceId: 1111,
    firmwareVersion: {
      major: 7,
      minor: 13,
      build: 1789,
    },
  });
  const axisState = _.sample(selectAxes(TestConnectionManager.testStore.getState()))!;
  expect(axisState.nonIdentifiedInfo).toMatchObject({
    peripheralId: null,
  });
});

test('displays device with FF error message', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);
  const device = connection.getDevice(1);
  mockDeviceAsXmcc(device);
  connection.detectDevices.mockImplementation(() => Promise.resolve([device]));

  const connectionKey = makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), CONNECTION_ID);
  const deviceKey = makeDeviceKey(makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), CONNECTION_ID), 1);

  loadRouters();

  await waitUntilPass(() => wrapper.getByText(/X-MCC/));

  TestConnectionManager.testStore.dispatch(connectionMonitorActions.updateDevicesFF(connectionKey, [deviceKey]));
  await waitUntilPass(() => wrapper.getByText(/Fault.*\(FF\).*/));
});


test('renumbers devices when there is conflict', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);
  connection.detectDevices.mockImplementation(() => Promise.resolve([]));
  connection.detectDevices.mockImplementationOnce(() => Promise.reject(new DeviceAddressConflictException('', null!)));

  loadRouters();

  await waitUntilPass(() => wrapper.getByText('COM1'));
  expect(connection.renumberDevices).toBeCalled();
  expect(connection.detectDevices).toBeCalledTimes(2);
});

test('renders router with no connections', async () => {
  const cloudUrl = makeCloudUrl(UNAUTHENTICATED_REALM, 'cloud-machine');
  const router: StoredRouter = { type: RouterType.Cloud, url: cloudUrl };
  storageMock.stored[ROUTERS_STORAGE_KEY] = [router];

  for (const url of [LOCAL_ROUTER_URL, cloudUrl]) {
    const connection = await messageRoutersServiceMock.getConnection(url);
    connection.api.getConnections.mockResolvedValue([]);
  }

  loadRouters();

  await waitUntilPass(() =>
    expect(wrapper.getAllByText('No connections available')).toHaveLength(2));
});

describe('cache usage', () => {
  let connection: ConnectionMock;
  let deviceKey: EntityKey;

  beforeEach(async () => {
    connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);

    const cacheMock = container.get<unknown>(Cache) as CacheMock;
    const routerKey = makeRouterKey(LOCAL_ROUTER_URL);
    const connectionKey = makeConnectionKey(routerKey, CONNECTION_ID);
    deviceKey = makeDeviceKey(connectionKey, 1);

    cacheMock.load.mockImplementation((key: string) => {
      if (key === `${routerKey}_info`) {
        return { connections: [MOCK_CONNECTION_DATA], name: 'router' };
      } else if (key === `${connectionKey}_devices`) {
        const cachedDevices: RecursivePartial<DeviceInfoWithAxes>[] = [{
          key: deviceKey,
          address: 1,
          axisCount: 1,
          axes: [{
            key: makeAxisKey(deviceKey, 1),
            axisNumber: 1,
            identity: {
              axisType: ascii.AxisType.LINEAR,
              isPeripheral: false,
            },
          }],
          identity: LHM_IDENTITY,
          isMultiAxis: false,
          product: mockProduct(),
        }];
        return cachedDevices;
      } else if (key === 'versions_skipped') {
        return {};
      } else {
        throw new Error(`Key ${key} was not expected in tests`);
      }
    });
  });

  test('uses cache if the connection is already known', async () => {
    loadRouters();

    await waitUntilPass(() => wrapper.getByText(/X-LHM/));
    expect(connection.detectDevices).not.toBeCalled();
  });

  test('reidentifies devices when the information is obtained from the cache', async () => {
    loadRouters();

    const devicesContext = container.get(DevicesContext);
    await waitUntilPass(() => expect(Array.from(devicesContext.invalidatedDeviceKeys)).toEqual([deviceKey]));
  });
});

test('displays error if connections cannot be loaded', async () => {
  const routerConnection = await messageRoutersServiceMock.getConnection(LOCAL_ROUTER_URL);
  routerConnection.api.getConnections.mockRejectedValueOnce(new Error('Cannot communicate with message router'));

  loadRouters();

  await waitUntilPass(() => wrapper.getByText(/Cannot communicate with message router/));
});

test('displays no connections message when there are no connection', async () => {
  const routerConnection = await messageRoutersServiceMock.getConnection(LOCAL_ROUTER_URL);
  routerConnection.api.getConnections.mockResolvedValue([]);

  loadRouters();

  await waitUntilPass(() => wrapper.getByText(/You currently have no devices connected/));
});

test('displays error if devices cannot be detected', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);
  connection.detectDevices.mockRejectedValue(new ConnectionFailedException('Cannot connect somewhere'));

  loadRouters();
  await expandConnection(wrapper);

  wrapper.getByText(/Cannot connect somewhere/);
});


test('re-detects devices when refresh is pressed', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);

  loadRouters();

  await waitUntilPass(() => expect(connection.detectDevices).toHaveBeenCalledTimes(1));

  fireEvent.click(wrapper.getByTitle('Refresh Devices'));

  await waitUntilPass(() => expect(connection.detectDevices).toHaveBeenCalledTimes(2));
});

test('adding router makes it render, store it, broadcasts the modified action, and detect the devices', async () => {
  const url = makeCloudUrl(UNAUTHENTICATED_REALM, 'cloud-machine');

  const router = await messageRoutersServiceMock.getConnection(url);
  router.api.getConnections.mockResolvedValue([MOCK_CONNECTION_DATA2]);
  const connection = await messageRoutersServiceMock.getAsciiConnection(url, MOCK_CONNECTION_DATA2.id);

  loadRouters();

  await waitUntilPass(() => wrapper.getByText('COM1'));

  TestConnectionManager.testStore.dispatch(actions.addRouter({
    type: RouterType.Cloud,
    url,
  }));

  await waitUntilPass(() => wrapper.getByText(/cloud-machine/));
  await waitUntilPass(() => wrapper.getByText('COM2'));

  expect(storageMock.save).toHaveBeenCalledWith(ROUTERS_STORAGE_KEY, [{
    type: RouterType.Cloud,
    url,
  }]);

  expect(ipcMock.broadcastAction).toHaveBeenCalledWith({
    type: ActionTypes.ADD_ROUTER,
    payload: {
      router: expect.anything(),
      saveAndBroadcast: false,
    },
  });

  await waitUntilPass(() => expect(connection.detectDevices).toHaveBeenCalledTimes(1));
});

test('retrieves information from ZaberApi and puts it into state', async () => {
  const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);
  const device = connection.getDevice(1);
  mockDeviceAsXmcc(device);
  connection.detectDevices.mockImplementation(() => Promise.resolve([device]));

  loadRouters();

  await waitUntilPass(() => wrapper.getByText(/X-MCC/));

  const api = container.get<unknown>(ZaberApi) as ZaberApiMock;
  expect(api.getDeviceInfo).toHaveBeenCalledWith({
    deviceID: 30341,
    fwVersion: {
      build: 8074,
      major: 7,
      minor: 13,
    },
    modified: false,
    peripherals: [{
      modified: false,
      peripheralID: 43211,
    }, {
      modified: false,
      peripheralID: 43211,
    }, {
      modified: false,
      peripheralID: 43211,
    }, {
      modified: false,
      peripheralID: 43211,
    }],
  });

  const state = TestConnectionManager.testStore.getState().connectionManager;
  expect(_.sample(state.devices)).toMatchObject({
    product: {
      id: 65123,
      capabilities: ['cap1'],
      commandTree: expect.anything(),
      settings: expect.anything(),
      conversionTable: expect.anything(),
    },
  });
  expect(_.sample(state.axes)).toMatchObject({
    product: {
      id: 11123,
      capabilities: ['cap2'],
      commandTree: expect.anything(),
      settings: expect.anything(),
      conversionTable: expect.anything(),
    }
  });
});

describe('with Cloud router', () => {
  const url = makeCloudUrl(UNAUTHENTICATED_REALM, 'cloud-machine');

  beforeEach(async () => {
    const router: StoredRouter = {
      type: RouterType.Cloud,
      url,
    };
    storageMock.stored[ROUTERS_STORAGE_KEY] = [router];

    const routerConnection = await messageRoutersServiceMock.getConnection(url);
    routerConnection.api.getConnections.mockResolvedValue([MOCK_CONNECTION_DATA2]);

    loadRouters();

    await waitUntilPass(() => wrapper.getByText('COM2'));
  });

  test('remove router makes it disappear, remove it from storage, broadcasts the modified action, and drop the connection', async () => {
    fireEvent.click(wrapper.getByTitle('Remove Router'));

    await waitUntilPass(() =>
      expect(wrapper.queryByText(/cloud-machine/)).toBeNull()
    );
    await waitUntilPass(() =>
      expect(storageMock.stored[ROUTERS_STORAGE_KEY]).toHaveLength(0)
    );
    expect(ipcMock.broadcastAction).toHaveBeenCalledWith({
      type: ActionTypes.REMOVE_ROUTER,
      payload: {
        key: makeRouterKey(url),
        saveAndBroadcast: false,
      },
    });
    expect(messageRoutersServiceMock.dropConnection).toHaveBeenCalledWith(url);
  });

  test('click on Refresh Connections reloads connections', async () => {
    const routerConnection = await messageRoutersServiceMock.getConnection(url);
    expect(routerConnection.api.getConnections).toHaveBeenCalledTimes(1);

    routerConnection.api.getConnections.mockResolvedValue([MOCK_CONNECTION_DATA3]);

    fireEvent.click(wrapper.getByTitle('Refresh Connections'));

    await waitUntilPass(() => wrapper.getByText('COM3'));
    expect(routerConnection.api.getConnections).toHaveBeenCalledTimes(2);
  });
});

describe('identification hooks', () => {
  const UNKNOWN_DEVICE_ID = 587894;
  let hooks: IdentificationHooks;

  beforeEach(async () => {
    hooks = container.get(IdentificationHooks);

    const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);

    const device1 = connection.getDevice(1);
    mockDeviceAsLhm(device1);
    const device2 = connection.getDevice(2);
    mockDeviceAsXmcc(device2);

    connection.detectDevices.mockImplementation(() => Promise.resolve([device1, device2]));
  });

  afterEach(() => {
    hooks = null!;
  });

  test('runs correct hooks for integrated device', async () => {
    const hook1Mock = jest.fn();
    function* hook1(device: IdentifiedDeviceInfo) {
      expect(device.identity.deviceId).toBe(LHM_IDENTITY.deviceId);
      yield delay(0);
      hook1Mock();
    }
    const hook2 = jest.fn();
    const hook3 = jest.fn();

    hooks.register([LHM_IDENTITY.deviceId], hook1);
    hooks.register([LHM_IDENTITY.deviceId], hook2);
    hooks.register([UNKNOWN_DEVICE_ID], hook3);

    loadRouters();

    await waitUntilPass(() => expect(hook1Mock).toBeCalled());
    expect(hook2).toBeCalled();
    expect(hook3).not.toBeCalled();
  });

  test('runs correct hook for peripheral', async () => {
    const hook = jest.fn();
    hooks.register([LHM_PERIPHERAL_ID], hook);
    loadRouters();
    await waitUntilPass(() => expect(hook).toBeCalled());
  });

  test('runs hook just once for the entire device', async () => {
    const hook = jest.fn();
    hooks.register([LHM_PERIPHERAL_ID, XMCC_DEVICE_ID], hook);
    loadRouters();

    await waitUntilPass(() => expect(hook).toBeCalled());
    await waitTick();

    expect(hook).toBeCalledTimes(1);
  });
});

describe(('rename connection'), () => {
  beforeEach(async () => {
    const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);

    const device1 = connection.getDevice(1);
    mockDeviceAsLhm(device1);

    connection.detectDevices.mockImplementation(() => Promise.resolve([device1]));

    loadRouters();
  });

  test('renames connection', async () => {
    fireEvent.click(wrapper.getByTitle('Rename Connection'));
    fireEvent.change(wrapper.getByDisplayValue(CONNECTION_ID), { target: { value: 'Controller 1' } });
    fireEvent.click(wrapper.getByTitle('Confirm'));
    expect(wrapper.getByTitle('Rename Connection')).toHaveClass('disabled');

    const routerConnection = await messageRoutersServiceMock.getConnection(LOCAL_ROUTER_URL);

    await waitUntilPass(() => {
      expect(routerConnection.api.setConnectionName).toHaveBeenCalledWith(CONNECTION_ID, 'Controller 1');
    });
  });

  test('handles error', async () => {
    const routerConnection = await messageRoutersServiceMock.getConnection(LOCAL_ROUTER_URL);
    routerConnection.api.setConnectionName.mockRejectedValueOnce(new Error('Something went wrong'));

    fireEvent.click(wrapper.getByTitle('Rename Connection'));
    fireEvent.change(wrapper.getByDisplayValue(CONNECTION_ID), { target: { value: 'Controller 1' } });
    fireEvent.click(wrapper.getByTitle('Confirm'));
    await waitUntilPass(() => expect(wrapper.getByText(/Something went wrong/)));
  });
});

describe(('remove connection'), () => {
  beforeEach(async () => {
    const connection = await messageRoutersServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, CONNECTION_ID);

    const device1 = connection.getDevice(1);
    mockDeviceAsLhm(device1);

    connection.detectDevices.mockImplementation(() => Promise.resolve([device1]));

    loadRouters();
  });

  test('display error if removing connection fails', async () => {
    const routerConnection = await messageRoutersServiceMock.getConnection(LOCAL_ROUTER_URL);
    routerConnection.api.removeConnection.mockRejectedValueOnce(new Error('Cannot be removed'));

    fireEvent.click(wrapper.getByTitle('Remove Connection'));
    fireEvent.click(wrapper.getByText('Remove'));

    await waitUntilPass(() => wrapper.getByText(/Cannot be removed/));
  });
});

describe(('Little Device Sim Notice'), () => {
  test('does not display notice for public version', async () => {
    expect(wrapper.queryAllByText(/Little Device Simulator/)).toHaveLength(0);
  });
});
