const React = require('react');

const Light = ({
  children
}) => React.createElement('div', null, children);
Light.registerLanguage = () => {};

exports.Light = Light;
