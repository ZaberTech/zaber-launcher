import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectCurrentTuner } from '../store';
import {
  ConnectionManagerAxisState,
  ConnectionManagerDeviceState,
  DeviceInfoWithAxes,
  isDeviceStateIdentified,
  selectAxes as selectManagerAxes,
  selectDevices as selectManagerDevices,
} from '../connection_manager';
import { tryAccess } from '../utils';
import { EntityKeyType, extractDeviceKey, getEntityType } from '../keys';

import type { AxisOrDeviceState } from './reducer';
import { isIntegratedMultiAxisDevice, UNUSED_AXIS_PERIPHERAL_ID } from './types';

export const selectLoaded = createSelector(selectCurrentTuner, state => state.loaded);
export const selectLoadingErr = createSelector(selectCurrentTuner, state => state.loadingError);
export const selectSelectedAxisOrDevice = createSelector(selectCurrentTuner, state => state.selectedAxisOrDevice);
export const selectAxesAndDevices = createSelector(selectCurrentTuner, state => state.axesAndDevices);
export const selectCurrentAxisOrDeviceState = createSelector(selectCurrentTuner, selectManagerAxes, selectManagerDevices,
  (state, managerAxes, managerDevices): DeviceOrAxisStateComplete | null => {
    const key = state.selectedAxisOrDevice;
    const currentState = tryAccess(state.axesAndDevices, key);
    if (key == null || currentState == null) { return null }

    const device = tryAccess(managerDevices, extractDeviceKey(key));
    if (device == null) { return null }

    const axis = tryAccess(managerAxes, key);
    if (axis == null && getEntityType(key) === EntityKeyType.AXIS) { return null }

    return ({
      device,
      axis,
      ...currentState,
    });
  });
export const selectDeviceInfoWithAxes = createSelector(selectSelectedAxisOrDevice, selectManagerDevices, selectManagerAxes,
  (deviceKey, devices, axes) => {
    if (!deviceKey) {
      return null;
    }
    const device = tryAccess(devices, extractDeviceKey(deviceKey));
    if (!device) {
      return null;
    }
    const deviceWithAxes: DeviceInfoWithAxes = {
      ...device,
      axes: device.axes.map(axisKey => axes[axisKey]),
    };
    return deviceWithAxes;
  }
);

export const selectResetOrApplyDisabled = createSelector(selectCurrentAxisOrDeviceState,
  state => state?.appliedPositions == null || _.isEqual(state?.appliedPositions, state?.sliderPositions));
export const selectDefaultDisabled = createSelector(selectCurrentAxisOrDeviceState,
  state => state?.defaultPositions == null || _.isEqual(state?.sliderPositions, state?.defaultPositions));

export interface DeviceOrAxisStateComplete extends AxisOrDeviceState {
  axis?: ConnectionManagerAxisState;
  device: ConnectionManagerDeviceState;
}

export type InvalidSelectionError = 'controller' | 'multiAxisIntegrated' | 'unusedAxis';
export const selectInvalidSelectionError = createSelector(selectCurrentTuner, selectManagerAxes, selectManagerDevices,
  (state, managerAxes, managerDevices): InvalidSelectionError | null => {
    const key = state.selectedAxisOrDevice;
    if (key == null) { return null }

    const device = managerDevices[extractDeviceKey(key)];
    if (device == null) { return null }
    const axis = tryAccess(managerAxes, key);

    if (axis == null && device.isController) {
      return 'controller';
    } else if (isDeviceStateIdentified(device) && isIntegratedMultiAxisDevice(device)) {
      return 'multiAxisIntegrated';
    } else if ((axis?.identity ?? axis?.nonIdentifiedInfo)?.peripheralId === UNUSED_AXIS_PERIPHERAL_ID) {
      return 'unusedAxis';
    }
    return null;
  });

export const selectSettingDriver = createSelector(selectCurrentTuner, state => state.settingDriver);
export const selectDriverEnabled = createSelector(selectCurrentTuner, state => state.driverEnabled);
export const selectDriverError = createSelector(selectCurrentTuner, state => state.setDriverErr);
export const selectShowWarning = createSelector(selectCurrentTuner, state => state.showWarning);
export const selectShow3rdPartyWarning = createSelector(selectCurrentTuner, selectCurrentAxisOrDeviceState,
  (state, device) => !state.thirdPartyConfirmed && device?.thirdPartyInfo != null);
