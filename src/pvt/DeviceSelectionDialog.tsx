import React from 'react';
import { useSelector } from 'react-redux';
import { Button, Icons, Modal } from '@zaber/react-library';

import { ConnectionTable, DeviceInfoWithAxes } from '../connection_manager';
import { useActions } from '../utils';

import { actions as actionDefinitions } from './actions';
import { selectUserPath } from './selectors';


interface SelectControllerProps {
  onSelection: (device: DeviceInfoWithAxes) => void;
}

export const ControllerList: React.FC<SelectControllerProps> = ({ onSelection }) =>
  <ConnectionTable
    deviceContent={device => <Button
      data-testid="select-device"
      onClick={() => {
        onSelection(device);
      }}>Select</Button>}
  />;


export const DeviceSelectionDialog: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const path = useSelector(selectUserPath);

  return <Modal className="device-selection-dialog"
    headerIcon={<Icons.Connection/>}
    headerText="Select Device"
    isOpen
    onRequestClose={() => actions.setDeviceSelectionDialogVisible(false)}
  >
    <ControllerList onSelection={deviceInfo => {
      actions.selectDevice(deviceInfo.key);
      actions.setDeviceSelectionDialogVisible(false);
      if ((path?.series.length ?? 0) > 0) {
        actions.setAxisDialogVisible(true, true);
      } else {
        actions.selectAxes({});
        actions.commitDeviceSelection();
      }
    }}/>
  </Modal>;
};
