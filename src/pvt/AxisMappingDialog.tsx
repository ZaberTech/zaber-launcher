import _ from 'lodash';
import React, { useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { Icons, Modal, Text, SimpleSelect, Flex, ButtonPrevious, ButtonRowConfirmCancel } from '@zaber/react-library';

import { deviceLabelOrName, peripheralLabelOrName, selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import type { EntityKey } from '../keys';
import { fwToString, isMobile } from '../devices';
import { useActions } from '../utils';

import { actions as actionDefinitions } from './actions';
import { selectDeviceSelectionDialog, selectDeviceOptions, selectUserPath } from './selectors';
import type { SeriesToAxisMap } from './types';
import { axisHasDimensionOf } from './utils';


export const AXIS_MAPPING_DIALOG_TITLE = 'Axis Mapping';
export const NO_COMPATIBLE_AXES = 'Device has no axes with compatible units.';


interface SelectableAxis {
  key: EntityKey;
  name: string;
  index: number;
}

export const AxisMappingDialog: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const userPath = useSelector(selectUserPath);
  const dialog = useSelector(selectDeviceSelectionDialog);
  const identifiedDevices = useSelector(selectIdentifiedDevices);
  const identifiedAxes = useSelector(selectIdentifiedAxes);
  const { axes: prevSelections } = useSelector(selectDeviceOptions);
  const device = identifiedDevices[dialog.pendingOptions.deviceKey!];

  const selectableAxes: SelectableAxis[] = useMemo(() => ((device?.axes ?? [])
    .filter(key => identifiedAxes[key])
    .map(key => identifiedAxes[key])
    .filter(axis => isMobile(axis) && axisHasDimensionOf(axis, userPath?.positionUnit))
    // Allow selection of only non-lockstepped axes or the first axes in lockstep groups.
    .filter(axis => !_.some(device.locksteps,
      lockstep => lockstep.axisNumbers.includes(axis.axisNumber) && axis.axisNumber !== lockstep.axisNumbers[0]))
    .map(axis => {
      let name = peripheralLabelOrName(axis) ?? `${deviceLabelOrName(device)} axis ${axis.axisNumber}`;
      const lockstep = device.locksteps.find(lockstep => lockstep.axisNumbers.includes(axis.axisNumber));
      if (lockstep) {
        name = `Lockstep group ${lockstep.groupNumber}`;
      }

      return {
        key: axis.key,
        name,
        index: axis.axisNumber - 1,
      };
    })), [device, identifiedAxes]);


  // Strip out any previous axis selections that don't exist on the new device.
  let prev = _.clone(prevSelections);
  _.toPairs(prev).forEach(([key, index]) => {
    const axis = identifiedAxes[device.axes[index]];
    if (!axis || !_.some(selectableAxes, selectable => selectable.key === key)) {
      delete prev[key];
    }
  });

  // If there are no valid selections left, generate defaults.
  if (_.keys(prev).length === 0) {
    prev = _.reduce((userPath?.series ?? []), (map, series, index) => {
      const axis = selectableAxes[index];
      if (axis) {
        map[series.name] = axis.index;
      }
      return map;
    }, {} as SeriesToAxisMap);
  }

  // Axis numbers here are the 0-based index of the axis on the device, with
  // absent meaning no axis. These are not the same as the axis indices in the
  // select component because some device axes may not be selectable.
  // The first axis in a lockstep group stands in for the group.
  const [axes, setAxes] = useState<SeriesToAxisMap>(_.clone(prev));

  function setAxis(seriesName: string, axis: number) {
    const newAxes = _.clone(axes);
    for (const key in newAxes) {
      if (newAxes[key] === axis) {
        delete newAxes[key];
      }
    }

    if (axis < 0) {
      delete newAxes[seriesName];
    } else {
      newAxes[seriesName] = axis;
    }

    setAxes(newAxes);
  }


  return <Modal className="axis-dialog"
    headerIcon={<Icons.Settings/>}
    headerText={AXIS_MAPPING_DIALOG_TITLE}
    isOpen
    buttons={<ButtonRowConfirmCancel
      onConfirm={(device == null || selectableAxes.length < 1) ? 'disabled' : () => {
        actions.selectAxes(axes);
        actions.setAxisDialogVisible(false, false);
        actions.commitDeviceSelection();
      }}
      onCancel={() => actions.setAxisDialogVisible(false, false)}>
      {dialog.showBackButton &&
      <ButtonPrevious
        onClick={() => {
          actions.setAxisDialogVisible(false, false);
          actions.setDeviceSelectionDialogVisible(true);
        }}>
        Back
      </ButtonPrevious>}
      <Flex.Spacer/>
    </ButtonRowConfirmCancel>}
    onRequestClose={() => actions.setAxisDialogVisible(false, false)}>
    <div className="card">
      <Text e={Text.Emphasis.Bold}>{deviceLabelOrName(device)}</Text>
      <Text>{`SN: ${device.identity?.serialNumber}`}&emsp;
        {`FW: ${device.identity?.firmwareVersion ? fwToString(device.identity?.firmwareVersion) : '<unidentified>'}`}
      </Text>
      <div className="row-spacer"/>
      {userPath?.series.map((series, seriesIndex) => <React.Fragment key={seriesIndex}>
        <Text e={Text.Emphasis.Bold}>{series.name}</Text>
        {selectableAxes.length > 0 && <SimpleSelect
          placeholder="Select axis"
          data-testid={`axis-map-select-${seriesIndex}`}
          value={selectableAxes.findIndex(selectable => identifiedAxes[selectable.key].axisNumber === axes[series.name] + 1)}
          onValueChange={selection =>
            setAxis(series.name, selection < selectableAxes.length ? identifiedAxes[selectableAxes[selection].key].axisNumber - 1 : -1)}
          menuPlacement={selectableAxes.length > (userPath.series.length - seriesIndex - 1) ? 'top' : 'bottom'}
          portalToBody
          options={selectableAxes
            .map((selectable, selectionIndex) => ({
              label: selectable.name,
              value: selectionIndex,
            }))}/>}
        {selectableAxes.length < 1 && <div className="notice">
          <Icons.ErrorWarning className="info-icon"/><Text>{NO_COMPATIBLE_AXES}</Text>
        </div>}
      </React.Fragment>)}
    </div>
  </Modal>;
};
