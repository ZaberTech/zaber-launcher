import type { Nullable } from '@zaber/toolbox';

export type EntityKey = string;

export enum EntityKeyType {
  ROUTER = '@router',
  CONNECTION = '@connection',
  DEVICE = '@device',
  AXIS = '@axis',
}

const entityKeyTypeValues = Object.keys(EntityKeyType).reduce(
  (keys, key) => ({ ...keys, [((EntityKeyType as Record<string, string>)[key])]: key }),
  {});

const KEY_SEPARATOR = '|';

function checkId(id: string | number): void {
  if (typeof id === 'string') {
    if (id.length === 0) {
      throw new Error(`Invalid ID "${id}"`);
    }
  } else if (typeof id === 'number') {
    if (!Number.isInteger(id)) {
      throw new Error(`Invalid ID "${id}"`);
    }
  } else {
    throw new Error(`Invalid ID "${typeof id}"`);
  }
}

export function makeKey(type: EntityKeyType, id: string | number): EntityKey {
  if (!(type in entityKeyTypeValues)) {
    throw new Error(`Invalid type ${type}`);
  }
  checkId(id);
  return [type, id].join(KEY_SEPARATOR);
}

export function getEntityType(key: Nullable<EntityKey>): EntityKeyType | null {
  if (!key) {
    return null;
  }
  const type = key.substr(0, key.indexOf(KEY_SEPARATOR));
  if (type in entityKeyTypeValues) {
    return type as EntityKeyType;
  } else {
    return null;
  }
}

function _getIdFromKey(key: EntityKey): string {
  return key.substring(key.indexOf(KEY_SEPARATOR) + 1);
}

export function extendKey(key: EntityKey, mustBeType: EntityKeyType, typeToBecome: EntityKeyType, additionalId: number|string): EntityKey {
  checkId(additionalId);
  if (getEntityType(key) !== mustBeType) {
    throw new Error(`Not a ${mustBeType} key ${key}`);
  }
  return makeKey(typeToBecome, [_getIdFromKey(key), additionalId].join(KEY_SEPARATOR));
}

export function extractKey(key: EntityKey, mustBeType: EntityKeyType[], typeToBecome: EntityKeyType, keyPartsToKeep: number): EntityKey {
  const extractedKey = tryExtractKey(key, mustBeType, typeToBecome, keyPartsToKeep);
  if (!extractedKey) {
    throw new Error(`Not a ${mustBeType.join()} key ${key}`);
  }
  return extractedKey;
}

export function tryExtractKey(
  key: Nullable<EntityKey>,
  mustBeType: EntityKeyType[],
  typeToBecome: EntityKeyType,
  keyPartsToKeep: number
): Nullable<EntityKey> {
  if (!mustBeType.includes(getEntityType(key)!)) {
    return null;
  }
  const ids = _getIdFromKey(key!).split(KEY_SEPARATOR);
  return makeKey(typeToBecome, ids.slice(0, keyPartsToKeep).join(KEY_SEPARATOR));
}

export function getIdFromKey(key: EntityKey, mustBeType: EntityKeyType[], partToReturn: number): string {
  if (!mustBeType.includes(getEntityType(key)!)) {
    throw new Error(`Not a ${mustBeType.join()} key ${key}`);
  }
  const ids = _getIdFromKey(key).split(KEY_SEPARATOR);
  return ids[partToReturn];
}

export function tryGetIdFromKey(key: Nullable<EntityKey>, mustBeType: EntityKeyType[], partToReturn: number): string | null {
  if (key == null || !mustBeType.includes(getEntityType(key)!)) {
    return null;
  }
  const ids = _getIdFromKey(key).split(KEY_SEPARATOR);
  return ids[partToReturn];
}

export function isSubKeyOf(testedKey: Nullable<EntityKey>, parentKey: Nullable<EntityKey>): boolean {
  if (!testedKey || !parentKey) { return false }

  const testedParts = testedKey.split(KEY_SEPARATOR).slice(1);
  const parentParts = parentKey.split(KEY_SEPARATOR).slice(1);
  if (testedParts.length < parentParts.length) { return false }

  return parentParts.every((part, i) => testedParts[i] === part);
}
