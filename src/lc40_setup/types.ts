import _ from 'lodash';

import { environment, Flavors } from '../environment';

import type { ErrorContext } from './exception';


export const BOSCH_REXROTH_SHB40_IDS = _.range(53049, 53087 + 1);
export const SUPPORTED_DEVICE_OR_PERIPHERAL_IDS = [50911, 70245, ...BOSCH_REXROTH_SHB40_IDS, 53131, 70479];
export const DEVICE_NAME = environment.flavor === Flavors.Zaber ? 'LC40' : 'SHB40';
export const LC40_TITLE = `Welcome to ${DEVICE_NAME} Setup!`;

export const MAXIMUM_TRAVEL_LENGTH_MM = 7000;
export const CONTACT_SUPPORT_FAILED_ATTEMPTS = 3;

export enum StageType {
  BASE_MOUNT = 'Base Mount',
  CARRIAGE_MOUNT = 'Carriage Mount',
}

export enum MotorOrientation {
  LEFT = 'Left Orientation',
  RIGHT = 'Right Orientation',
}

export const DriverDirection = {
  [StageType.BASE_MOUNT]: {
    [MotorOrientation.LEFT]: 0,
    [MotorOrientation.RIGHT]: 1,
  },
  [StageType.CARRIAGE_MOUNT]: {
    [MotorOrientation.LEFT]: 1,
    [MotorOrientation.RIGHT]: 0,
  },
};

export enum SetupStep {
  NOT_STARTED = 'NOT_STARTED',
  CONFIG_ERROR = 'CONFIG_ERROR',
  TEST_PREAMBLE = 'TEST_PREAMBLE',
  SAVE_WITH_TEST_IN_PROGRESS = 'SAVE_WITH_TEST_IN_PROGRESS',
  SAVE_WITH_TEST_STOPPING = 'SAVE_WITH_TEST_STOPPING',
  SAVE_WITH_TEST_STOPPED = 'SAVE_WITH_TEST_STOPPED',
  SAVE_WITH_TEST_COMPLETE = 'SAVE_WITH_TEST_COMPLETE',
  SAVE_WITH_TEST_ERROR = 'SAVE_WITH_TEST_ERROR',
  SAVE_WITHOUT_TEST_IN_PROGRESS = 'SAVE_WITHOUT_TEST_IN_PROGRESS',
  SAVE_WITHOUT_TEST_COMPLETE = 'SAVE_WITHOUT_TEST_COMPLETE',
  SAVE_WITHOUT_TEST_ERROR = 'SAVE_WITHOUT_TEST_ERROR',
}

export interface AxisConfigComplete {
  stageType: StageType;
  motorOrientation: MotorOrientation;
  travelDistanceMm: number;
}

export type AxisConfig = Partial<AxisConfigComplete>;

export interface ResultError {
  message?: string;
  errContext?: ErrorContext;
}

export const DISPLAY_SETUP_ALL_TIME = environment.flavor === Flavors.Rexroth;

export type EstopData = { status: 'ok'; ready: boolean } | { status: 'error'; error: string } | { status: 'enabling' };
