import classNames from 'classnames';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Checkbox, ColorPicker, Flex, HeaderCard, MinorMenu, Modal, Text } from '@zaber/react-library';
import { changeCollection } from '@zaber/toolbox';

import { AppIconsNeutral } from '../apps';

import { ADD_MATH_BUTTON_LABEL, Channel, ChannelListMap } from './types';
import { selectAllChannels } from './selectors';
import { extractAxisChannels, extractMathChannels, MATH_DIMENSIONS, MATH_DIMENSION_CATEGORIES } from './channels';
import { PREFERRED_DIMENSION_ORDER } from './utils';
import { getDefaultColor, UNSET_COLOR } from './colors';


interface RowProps {
  channel: Channel;
  setVisible: (visible: boolean) => void;
  setColor: (color: string) => void;
}


const ChannelRow: React.FC<RowProps> = ({ channel, setVisible, setColor }) => <>
  <Checkbox checked={channel.showInUi}
    className="visibility"
    data-testid={`dlg-show-channel ${channel.id}`}
    onChecked={() => setVisible(!channel.showInUi)}/>
  {channel.showInUi ?
    <ColorPicker color={channel.color} onChange={setColor}/>
    : <div className="color-picker-placeholder"/>}
  <div className="label">
    <Text t={Text.Type.Body}>{channel.title}</Text><br/>
    <Text t={Text.Type.BodySm}>{channel.subtitle}</Text>
  </div>
</>;


const groupMathChannels = function(channels: Channel[]): [ChannelListMap, string[]] {
  const groups: ChannelListMap = {};
  const groupOrder: Record<string, number> = {};

  for (const channel of extractMathChannels(channels)) {
    const groupName = MATH_DIMENSION_CATEGORIES[channel.name];
    if (!(groupName in groups)) {
      groups[groupName] = [];
      groupOrder[groupName] = PREFERRED_DIMENSION_ORDER.indexOf(MATH_DIMENSIONS[channel.name][0]);
    }

    groups[groupName].push(channel);
  }

  const groupNames = Array.from(Object.keys(groups));
  groupNames.sort((a, b) => groupOrder[a] - groupOrder[b]);
  return [groups, groupNames];
};

interface Props {
  dialogOpen: boolean;
  axisNames: string[];
  apply: (newMath: Channel[]) => void;
  cancel: () => void;
}


export const ChannelMathDialog: React.FC<Props> = ({ dialogOpen, axisNames, apply, cancel }) => {
  const originalChannels = useSelector(selectAllChannels);
  const [channels, setChannels] = useState([...originalChannels]);
  const [groups, groupNames] = groupMathChannels(channels);

  const nonEmptyAxes: number[] = [];
  axisNames.forEach((_, i) => {
    if (extractAxisChannels(extractMathChannels(originalChannels), i).length > 0) {
      nonEmptyAxes.push(i);
    }
  });

  const defaultAxis = nonEmptyAxes.includes(1) ? 1 : nonEmptyAxes[0];
  const [axis, setAxis] = useState(defaultAxis);

  return <Modal
    className="add-math-dialog"
    data-testid="add-math-dialog"
    headerIcon={<AppIconsNeutral.Oscilloscope/>}
    headerText={ADD_MATH_BUTTON_LABEL}
    isOpen={dialogOpen}
    buttons={<Button className="apply-button" onClick={() => apply(extractMathChannels(channels))}>Apply</Button>}
    onRequestClose={cancel}>
    {axisNames.length && <MinorMenu barStyle="horizontal">
      {axisNames.map((name, i) => nonEmptyAxes.includes(i) ?
        <div key={i} className={classNames({ active: axis === i })}>
          <MinorMenu.Item data-testid={`math-axis-tab-${i}`} onClick={() => setAxis(i)}>{i ? `Axis ${i}` : name}</MinorMenu.Item>
        </div> : null)}</MinorMenu>}
    <Flex.Column className="groups-list">
      {groupNames.map(groupName => {
        const channelsInGroup = extractAxisChannels(groups[groupName], axis);
        return <HeaderCard key={groupName} header={<span>{groupName}</span>}>
          {channelsInGroup.map(channel =>
            <ChannelRow
              key={channel.id}
              channel={channel}
              setVisible={visible => setChannels(changeCollection(channels, 'id', channel.id, ch => ({
                ...ch,
                showInUi: visible,
                color: visible && ch.color === UNSET_COLOR ? getDefaultColor(channel.id) : ch.color,
              })))}
              setColor={color => setChannels(changeCollection(channels, 'id', channel.id, ch => ({ ...ch, color })))}/>)}
        </HeaderCard>;
      })}
    </Flex.Column>
  </Modal>;
};
