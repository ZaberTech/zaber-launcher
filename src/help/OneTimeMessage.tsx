import { useActions } from '@zaber/toolbox/lib/redux';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { selectMessagesVisibility } from './selectors';
import { actions as actionsDefinitions } from './actions';

interface Props {
  messageId: string;
  message: (dismiss: () => void) => React.ReactNode;
}

export const OneTimeMessage: React.FC<Props> = ({ messageId, message }) => {
  const actions = useActions(actionsDefinitions);

  useEffect(() => {
    actions.messageReady(messageId);
  }, [messageId]);

  const visible = useSelector(selectMessagesVisibility)[messageId] ?? false;
  if (!visible) { return null }

  const dismiss = () => actions.dismissMessage(messageId);
  return <>{message(dismiss)}</>;
};
