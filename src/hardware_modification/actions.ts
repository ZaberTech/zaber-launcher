import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { ApplyProgress } from './ApplyModal';
import type { ValidationError, ErrorsOnApply } from './validators';
import type { PeripheralTraits } from './reducer';
import type { HwModDeviceOrAxisState } from './selectors';
import type { NonNullValues, Values, ValueUpdate } from './types';
import type { SectionIds } from './data_definitions';

export enum ActionTypes {
  // Initialize
  PRODUCT_CHOSEN = 'HW_MOD_SET_SELECTED_DEVICE',
  APP_LOADED = 'HW_MOD_APP_LOADED',
  ENABLE_MODIFICATION = 'HW_MOD_ENABLE_MODIFICATION',
  ERROR_ON_ENABLE_MODIFICATION = 'HW_MOD_ERROR_ON_ENABLE_MODIFICATION',
  INITIALIZE_VALUES = 'HW_MOD_INITIALIZE_VALUES',
  ERROR_ON_INITIALIZE = 'HW_MOD_ERROR_ON_INITIALIZE',
  SET_VALUES = 'HW_MOD_SET_VALUES',
  STORE_SCROLL_POSITION = 'HW_MOD_STORE_SCROLL_POSITION',

  // Edit Form
  UPDATE_VALUES = 'HW_MOD_UPDATE_VALUES',
  REVERT_VALUES = 'HW_MOD_REVERT_VALUES',

  // Apply Form
  APPLY_VALUES = 'HW_MOD_APPLY_VALUES',
  SET_APPLY_PROGRESS = 'HW_MOD_SET_APPLY_STATE',
  SCROLL_TO_ERROR = 'HW_MOD_SCROLL_TO_ERROR',
  SET_VALUE_APPLIED = 'HW_MOD_SET_VALUE_APPLIED',
  ERROR_ON_APPLY = 'HW_MOD_ERROR_ON_APPLY',

  // Pick peripheral ID of 3rd party peripheral
  CONFIGURE_PERIPHERAL_ID = 'HW_MOD_CONFIGURE_PERIPHERAL_ID',
  CHOOSE_PERIPHERAL_TRAIT = 'HW_MOD_CHOOSE_PERIPHERAL_TRAIT',
  SET_PERIPHERAL_ID = 'HW_MOD_SET_PERIPHERAL_ID',
  ERROR_SETTING_PERIPHERAL_ID = 'HW_MOD_ERROR_SETTING_PERIPHERAL_ID',
}

export interface ActionsToPayloads {
  // Initialize
  [ActionTypes.PRODUCT_CHOSEN]: { productKey: EntityKey | null };
  [ActionTypes.APP_LOADED]: { info: HwModDeviceOrAxisState };
  [ActionTypes.ENABLE_MODIFICATION]: { info: HwModDeviceOrAxisState };
  [ActionTypes.ERROR_ON_ENABLE_MODIFICATION]: { message: string };
  [ActionTypes.INITIALIZE_VALUES]: { info: HwModDeviceOrAxisState };
  [ActionTypes.ERROR_ON_INITIALIZE]: { message: string };
  [ActionTypes.SET_VALUES]: { values: Values };
  [ActionTypes.STORE_SCROLL_POSITION]: { scrollPosition: number };

  // Edit Form
  [ActionTypes.UPDATE_VALUES]: { updates: ValueUpdate<SectionIds>[] };
  [ActionTypes.REVERT_VALUES]: null;

  // Apply Form
  [ActionTypes.APPLY_VALUES]: { deviceKey: EntityKey; validFieldValues: NonNullValues; info: HwModDeviceOrAxisState };
  [ActionTypes.SET_APPLY_PROGRESS]: { progress?: ApplyProgress };
  [ActionTypes.SCROLL_TO_ERROR]: { error: ValidationError | null };
  [ActionTypes.SET_VALUE_APPLIED]: { sectionId: string; fieldId: string; value: number };
  [ActionTypes.ERROR_ON_APPLY]: { errors: ErrorsOnApply };

  // Pick peripheral ID of 3rd party peripheral
  [ActionTypes.CONFIGURE_PERIPHERAL_ID]: { info: HwModDeviceOrAxisState };
  [ActionTypes.CHOOSE_PERIPHERAL_TRAIT]: { traits: PeripheralTraits | null };
  [ActionTypes.SET_PERIPHERAL_ID]: { info: HwModDeviceOrAxisState; id: number };
  [ActionTypes.ERROR_SETTING_PERIPHERAL_ID]: { message: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  // Initialize
  productChosen: (productKey: EntityKey | null) => buildAction(ActionTypes.PRODUCT_CHOSEN, { productKey }),
  appLoaded: (info: HwModDeviceOrAxisState) => buildAction(ActionTypes.APP_LOADED, { info }),
  makeDeviceModifiable: (info: HwModDeviceOrAxisState) => buildAction(ActionTypes.ENABLE_MODIFICATION, { info }),
  errorOnEnableHwMod: (message: string) => buildAction(ActionTypes.ERROR_ON_ENABLE_MODIFICATION, { message }),
  initializeValues: (info: HwModDeviceOrAxisState) => buildAction(ActionTypes.INITIALIZE_VALUES, { info }),
  errorOnInitialize: (message: string) => buildAction(ActionTypes.ERROR_ON_INITIALIZE, { message }),
  setValues: (values: Values) => buildAction(ActionTypes.SET_VALUES, { values }),
  storeScrollPosition: (scrollPosition: number) => buildAction(ActionTypes.STORE_SCROLL_POSITION, { scrollPosition }),

  // Edit Form
  updateValue: <S extends SectionIds>(update: ValueUpdate<S>) => buildAction(ActionTypes.UPDATE_VALUES, { updates: [update] }),
  updateValues: <S extends SectionIds>(updates: ValueUpdate<S>[]) => buildAction(ActionTypes.UPDATE_VALUES, { updates }),
  revertValues: () => buildAction(ActionTypes.REVERT_VALUES),

  // Apply Form
  applyValues: (deviceKey: EntityKey, validFieldValues: NonNullValues, info: HwModDeviceOrAxisState) => buildAction(
    ActionTypes.APPLY_VALUES, { deviceKey, validFieldValues, info }
  ),
  setApplyProgress: (progress?: ApplyProgress) => buildAction(ActionTypes.SET_APPLY_PROGRESS, { progress }),
  scrollToError: (error: ValidationError | null) => buildAction(ActionTypes.SCROLL_TO_ERROR, { error }),
  setValueApplied: (sectionId: string, fieldId: string, value: number) => (
    buildAction(ActionTypes.SET_VALUE_APPLIED, { sectionId, fieldId, value })
  ),
  errorOnApply: (errors: ErrorsOnApply) => (
    buildAction(ActionTypes.ERROR_ON_APPLY, { errors })
  ),

  // Pick peripheral ID of 3rd party peripheral
  configurePeripheralId: (info: HwModDeviceOrAxisState) => buildAction(ActionTypes.CONFIGURE_PERIPHERAL_ID, { info }),
  choosePeripheralTrait: (traits: PeripheralTraits | null) => buildAction(ActionTypes.CHOOSE_PERIPHERAL_TRAIT, { traits }),
  setPeripheralId: (info: HwModDeviceOrAxisState, id: number) => buildAction(ActionTypes.SET_PERIPHERAL_ID, { info, id }),
  errorSettingPeripheralId: (message: string) => buildAction(ActionTypes.ERROR_SETTING_PERIPHERAL_ID, { message }),
};
