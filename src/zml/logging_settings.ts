export const loggingSettingsSymbol = Symbol('LoggingSettings');

export interface LoggingSettings {
  maxLogFiles: number;
  maxLogSize: number;
  sizeCheckInterval: number;
}

export const DEFAULT_LOGGING_SETTINGS: LoggingSettings = {
  maxLogFiles: 10,           // 10 files per window
  maxLogSize: 2 ** 24,       // 16MB
  sizeCheckInterval: 30000,  // 30s
};
