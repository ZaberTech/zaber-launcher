import { consoleErrorSpy } from '@zaber/toolbox/lib/test/setup';

const sentryMock = {
  init: jest.fn(),
};

function sentryMockRequire(): unknown {
  return sentryMock;
}

jest.mock('@sentry/react', sentryMockRequire);

import { initErrors, errorEmitter } from './errors';

beforeEach(() => {
  sentryMock.init.mockReset();
});

afterEach(() => {
  errorEmitter.removeAllListeners('error');
});

test('sending event with level error emits error', async () => {
  consoleErrorSpy.mockImplementation(() => null);
  initErrors();

  expect(sentryMock.init).toBeCalledTimes(1);
  const initArg = sentryMock.init.mock.calls[0][0];

  const emitterPromise = new Promise(resolve => errorEmitter.once('error', resolve));

  initArg.beforeSend({
    level: 'error',
  });

  await emitterPromise;
  consoleErrorSpy.mockReset();
});
