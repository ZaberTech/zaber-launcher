import { useSelector } from 'react-redux';
import { P, match } from 'ts-pattern';

import { selectAxisCount, selectEncoderAxes, selectPosAxes, selectSettingsLists } from './selectors';
import { TriggerActionState, TriggerConditionState, TriggerState, defaultTriggerConditionState } from './reducer';
import { SettingsList } from './types';
import { combineSettingsLists } from './utils';
import { getActionLetter } from './TriggerAction';


export const useTriggerSaveableError = (trigger: TriggerState): string | null => {
  const settingsLists = useSelector(selectSettingsLists);

  const { conditionInput, actionsInput } = trigger;

  const conditionValidError = getConditionValidError(conditionInput ?? defaultTriggerConditionState);
  const conditionSupportedError = useConditionSupportedError(conditionInput ?? defaultTriggerConditionState, settingsLists);
  const actionsValidError = actionsInput.map((action, i) => getActionValidError(action, getActionLetter(i)));
  const actionsSupportedError = actionsInput.map((action, i) => getActionSupportedError(action, getActionLetter(i), settingsLists));

  if (conditionValidError != null) {
    return conditionValidError;
  }
  if (conditionSupportedError != null) {
    return conditionSupportedError;
  }

  const actionInvalidError = actionsValidError.find(action => action != null);
  if (actionInvalidError) {
    return actionInvalidError;
  }
  const actionSupportedError = actionsSupportedError.find(action => action != null);
  if (actionSupportedError) {
    return actionSupportedError;
  }

  return null;
};

export const useTriggerSupportedError = (trigger: TriggerState): string | null => {
  const settingsLists = useSelector(selectSettingsLists);

  const { conditionInput, actionsInput } = trigger;

  const conditionSupportedError = useConditionSupportedError(conditionInput ?? defaultTriggerConditionState, settingsLists);
  const actionsSupportedError = actionsInput.map((action, i) => getActionSupportedError(action, getActionLetter(i), settingsLists));

  if (conditionSupportedError != null) {
    return conditionSupportedError;
  }

  const actionSupportedError = actionsSupportedError.find(action => action != null);
  if (actionSupportedError) {
    return actionSupportedError;
  }

  return null;
};

const getConditionValidError = (condition: TriggerConditionState): string | null => {
  const isValuePositiveError = (value: number | null): string | null => {
    if (value == null || value <= 0) {
      return 'Condition Value must be greater than 0';
    }
    return null;
  };

  return match(condition)
    .with({ type: 'none' }, () => 'Missing Condition Type')
    .with({ type: P.union('Distance Interval', 'Encoder Distance Interval', 'Time Elapsed') },
      condition => isValuePositiveError(condition.value.value))
    .with({ type: P.union('Setting', 'Absolute Setting') }, condition => {
      if (condition.setting === '') {
        return 'Missing Condition Setting';
      } else {
        return null;
      }
    })
    .otherwise(() => null);
};

const useConditionSupportedError = (condition: TriggerConditionState, settingsLists: SettingsList[]): string | null => {
  const posAxes = useSelector(selectPosAxes);
  const encoderAxes = useSelector(selectEncoderAxes);
  const axisCount = useSelector(selectAxisCount);

  const axis = 'axis' in condition ? condition.axis : 0;
  const settingsList = axisCount === 1 ? combineSettingsLists(settingsLists) : settingsLists[axis] ?? {};

  return match(condition)
    .with({ type: P.union('Trajectory Position', 'Distance Interval') }, condition => {
      if (posAxes.length === 0) {
        return 'Condition Type not supported by device and configured peripherals';
      } else if (!posAxes.includes(condition.axis)) {
        return 'Condition Axis not supported by configured peripheral';
      }
      return null;
    })
    .with({ type: 'Encoder Distance Interval' }, condition => {
      if (encoderAxes.length === 0) {
        return 'Condition Type not supported by device and configured peripherals';
      } else if (!encoderAxes.includes(condition.axis)) {
        return 'Condition Axis not supported by configured peripheral';
      } else {
        return null;
      }
    })
    .with({ type: P.union('Setting', 'Absolute Setting') }, condition => {
      if (!Object.keys(settingsList).includes(condition.setting)) {
        return 'Condition Setting not supported by selected Device or Axis';
      } else {
        return null;
      }
    })
    .otherwise(() => null);
};

const getActionValidError = (action: TriggerActionState, actionLetter: string): string | null =>
  match(action)
    .with({ type: 'Command' }, action => {
      if (action.command === '') {
        return `Missing Command for Action ${actionLetter}`;
      } else if (/<|>|\|/.test(action.command)) {
        return `Missing Command Variables for Action ${actionLetter}`;
      }
      return null;
    })
    .with({ type: 'Set Setting to Value' }, action => {
      if (action.setting === '') {
        return `Missing Setting for Action ${actionLetter}`;
      } else {
        return null;
      }
    })
    .with({ type: 'Set Setting to Setting' }, action => {
      if (action.setting === '') {
        return `Missing Setting for Action ${actionLetter}`;
      } else if (action.fromSetting === '') {
        return `Missing Source Setting for Action ${actionLetter}`;
      } else {
        return null;
      }
    })
    .otherwise(() => null);

const getActionSupportedError = (action: TriggerActionState, actionLetter: string, settingsLists: SettingsList[]): string | null => {
  const axis = 'axis' in action ? action.axis : 0;
  const settingsList = axis === 0 ? combineSettingsLists(settingsLists) : settingsLists[axis] ?? {};
  const axisFrom = 'fromAxis' in action ? action.fromAxis : 0;
  const settingsListFrom = axisFrom === 0 ? combineSettingsLists(settingsLists) : settingsLists[axisFrom] ?? {};

  return match(action)
    .with({ type: 'Set Setting to Value' }, action => {
      if (action.setting && !Object.keys(settingsList).includes(action.setting)) {
        return `Setting not supported by selected Device or Axis for Action ${actionLetter}`;
      } else {
        return null;
      }
    })
    .with({ type: 'Set Setting to Setting' }, action => {
      if (action.setting && !Object.keys(settingsList).includes(action.setting)) {
        return `Setting not supported by selected Device or Axis for Action ${actionLetter}`;
      } else if (action.fromSetting && !Object.keys(settingsListFrom).includes(action.fromSetting)) {
        return `Source Setting not supported by selected Device or Axis for Action ${actionLetter}`;
      } else {
        return null;
      }
    })
    .otherwise(() => null);
};
