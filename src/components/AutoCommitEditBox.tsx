import { Input } from '@zaber/react-library';
import classNames from 'classnames';
import React, { useRef, useState } from 'react';

export interface AutoCommitEditBoxProps {
  initialValue: number;
  label?: string;
  disabled?: boolean;
  onChange?: (newValue: number) => void;
  'data-testid'?: string;
}


/* Numeric input box that only fires the change event when the value is committed. */
export const AutoCommitEditBox: React.FC<AutoCommitEditBoxProps> = props => {
  const { initialValue, label, disabled, onChange } = props;
  const ref = useRef<HTMLInputElement>(null);
  const [currentValue, setCurrentValue] = useState(initialValue);
  const [valid, setValid] = useState(!Number.isNaN(initialValue));

  function commit() {
    if (valid) {
      if (currentValue !== initialValue) {
        onChange?.(currentValue);
      }
    } else {
      setCurrentValue(initialValue);
    }
  }

  function onKeyDown(event: React.KeyboardEvent<HTMLInputElement>) {
    if (event.key === 'Enter' || event.key === 'Tab') {
      ref.current?.blur();
    }
  }

  return <Input
    ref={ref}
    labelContent={label}
    type="number"
    disabled={!!disabled || onChange == null}
    data-testid={props['data-testid']}
    value={currentValue}
    className={classNames('auto-commit-edit-box', { invalid: !valid })}
    onKeyDown={onKeyDown}
    onChange={e => {
      if (!Number.isNaN(+e.target.value)) {
        setCurrentValue(+e.target.value);
        setValid(true);
      } else {
        setValid(false);
      }
    }}
    onBlur={() => commit()}/>;
};
