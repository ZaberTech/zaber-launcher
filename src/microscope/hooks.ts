import { useSelector } from 'react-redux';
import { useRef } from 'react';
import { P } from 'ts-pattern';

import { isMatch } from '../utils';

import { selectMonitorWillRefresh, selectOperations } from './selectors';
import { isOperationError, isOperationInProgress, type Operations, type OperationsResults } from './types';

/** Holds intermediate value while it's being changed by operation and refreshed. */
export function useOperationValue<T>(
  value: T, setOperation?: Operations,
): [intermediateValue: T, setter: (value: T) => void, inProgress: boolean] {
  const operations = useSelector(selectOperations);
  const inProgress = isMatch({ inProgress: true }, setOperation ? operations[setOperation] : null);
  const [intermediate, setter] = useMonitoredValue(value, inProgress);
  return [intermediate, setter, inProgress];
}

/**
 * Holds intermediate value while it's being changed and refreshed.
 * Returns the intermediate value and a setter.
 **/
export function useMonitoredValue<T>(
  value: T, inProgress?: boolean,
): [intermediateValue: T, setter: (value: T) => void] {
  const willRefresh = useSelector(selectMonitorWillRefresh);

  const valueRef = useRef(value);
  if (!inProgress && !willRefresh) {
    valueRef.current = value;
  }

  return [valueRef.current, (value: T) => { valueRef.current = value }];
}

export function useOperationState(operation: Operations): [inProgress: boolean, error: string | null] {
  const state = useSelector(selectOperations)[operation];
  return [isOperationInProgress(state), isOperationError(state) ? state.error : null];
}

export function useOperationResult<Op extends keyof OperationsResults>(
  operation: Operations,
): [result: OperationsResults[Op] | null, inProgress: boolean, error: string | null] {
  const operations = useSelector(selectOperations);
  const state = operations[operation];
  if (isOperationInProgress(state)) {
    return [null, true, null];
  } else if (isOperationError(state)) {
    return [null, false, state.error];
  }  else if (isMatch({ result: P.any }, state)) {
    return [state.result as OperationsResults[Op], false, null];
  } else {
    return [null, false, null];
  }
}
