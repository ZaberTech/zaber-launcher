import React, { useEffect, useState } from 'react';
import { ButtonNext, ButtonRow, Flex, LinkClasses, NoticeBanner, Text } from '@zaber/react-library';
import { ProcessControllerMode } from '@zaber/motion/product';
import _ from 'lodash';

import { makeAxisKey } from '../../keys';
import { convertNullable, EditableInputWithUnits, getUnits, measurementOK } from '../../units';
import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import { POLL_RETENTION_SEC } from '../constants';
import type { ValueUnitState } from '../../units/EditableInputWithUnits';
import { Graph } from '../components/Graph';

import type { AutoTuneSettings, AutoTuneSetup } from './types';

const MAX_MEASURED_CHANGE = 0.05;
const MAX_MEASURED_CHANGE_MARGIN = 0.01;
const MAX_DIFF_FROM_SETPOINT = 1.0;

const SetupWarning: React.FC<{ process: number; measures: number[]; setpoint: ValueUnitState }> = ({ process, measures, setpoint }) => {
  const actions = useActions(actionDefinitions);
  const [unstable, setUnstable] = useState(false);

  const averageMeasuredChange = ((_.max(measures) ?? 0) - (_.min(measures) ?? 0)) / POLL_RETENTION_SEC;
  useEffect(() => {
    if (averageMeasuredChange < MAX_MEASURED_CHANGE) {
      setUnstable(false);
    } else if (averageMeasuredChange > MAX_MEASURED_CHANGE + MAX_MEASURED_CHANGE_MARGIN) {
      setUnstable(true);
    }
  }, [averageMeasuredChange]);

  if (unstable) {
    return <NoticeBanner type="error">
      There has been an average change
      of {averageMeasuredChange.toLocaleString(undefined, { minimumFractionDigits: 3, maximumFractionDigits: 3 })}˚C/s
      over the last {POLL_RETENTION_SEC} seconds. Please wait until the average change has stabilized
      to below {MAX_MEASURED_CHANGE}˚C/s before you continue.
    </NoticeBanner>;
  }

  const lastMeasure = measures.at(-1);
  if (lastMeasure != null && setpoint.value != null && Math.abs(lastMeasure - setpoint.value) > MAX_DIFF_FROM_SETPOINT) {
    const measured = Math.round(lastMeasure * 10) / 10;
    const setSetpointToMeasured = () => {
      actions.updateProcessSetting(process, 'process.control.setpoint.temperature', { ...setpoint, value: measured });
    };
    return <NoticeBanner type="error">
      The system has stabilized at {measured}˚C which is not the setpoint ({setpoint.value}˚C).
      Either choose a new voltage that causes the system to stabilize at the setpoint,
      or <Text className={LinkClasses.Default} onClick={setSetpointToMeasured}>use {measured}˚C as the setpoint</Text>.
    </NoticeBanner>;
  }

  return null;
};

type AutoTuneSetupProps = {
  tune: AutoTuneSetup;
  settings: AutoTuneSettings;
};

export const AutoTuneModalSetup: React.FC<AutoTuneSetupProps> = ({ tune, settings }) => {
  const { 'process.voltage.on': stableVoltage, 'process.control.setpoint.temperature': setpoint } = settings;
  const [maxVoltage, setMaxVoltage] = useState<ValueUnitState>({ ...tune.maxVoltage, mode: 'displaying' });
  const actions = useActions(actionDefinitions);
  const processKey = makeAxisKey(tune.entity, tune.process);

  return <>
    <Text>Set the maximum voltage and a setpoint that is near to the temperature this device will be run at in operation</Text>
    <Flex.Row>
      <EditableInputWithUnits
        labelContent="Maximum Voltage"
        title="Maximum Voltage"
        unitFrom={{ info: tune.converters['process.control.voltage.max'] }}
        mode={maxVoltage.mode}
        measure={maxVoltage}
        onChange={update => {
          if (update.mode === 'writing') {
            if (stableVoltage.value != null && update.value < stableVoltage.value) {
              const message = `Cannot set Max Voltage to ${update.value}V which is less than Voltage (${stableVoltage.value}V)`;
              actions.autoTuneError({ type: 'error', message });
            } else {
              setMaxVoltage({ ...update, mode: 'displaying' });
            }
          } else {
            setMaxVoltage(update);
          }
        }}
        unitsEditable={false}
      />
      <Flex.Spacer/>
      <EditableInputWithUnits
        labelContent="Setpoint"
        unitFrom={{ setting: 'process.control.setpoint.temperature', entity: processKey }}
        mode={setpoint.mode}
        measure={setpoint}
        onChange={update => actions.updateProcessSetting(tune.process, 'process.control.setpoint.temperature', update)}
        unitsEditable={false}
      />
    </Flex.Row>

    <Text>
      In order to tune this system, you must manually find a voltage at which it settles to the desired setpoint.
      Try using different voltages, until you find one that works. If the chosen value is too close to the maximum voltage (within ~10%),
      the identification process may not work optimally. In that case, lower the setpoint and find a lower stabilizing voltage.
    </Text>
    <Flex.Row>
      <EditableInputWithUnits
        title="Stable Voltage"
        labelContent="Voltage"
        unitFrom={{ info: tune.converters['process.voltage.on'] }}
        mode={stableVoltage.mode}
        measure={stableVoltage}
        onChange={update => {
          if (update.mode === 'writing') {
            const maxVoltageValue = convertNullable(
              tune.converters['process.control.voltage.max'], maxVoltage.value, getUnits(maxVoltage), update.units
            );
            if (maxVoltageValue == null) {
              actions.autoTuneError({ type: 'error', message: 'Set Maximum Voltage to a valid value before setting Stable Voltage.' });
            } else if (update.value > maxVoltageValue) {
              const message = `Cannot set Voltage to ${update.value}V which is larger than Maximum Voltage (${maxVoltageValue}V)`;
              actions.autoTuneError({ type: 'error', message });
            } else {
              actions.updateProcessSetting(tune.process, 'process.voltage.on', update);
            }
          } else {
            actions.updateProcessSetting(tune.process, 'process.voltage.on', update);
          }
        }}
        unitsEditable={false}
      />
      <Flex.Spacer/>
    </Flex.Row>

    <Graph
      mode={ProcessControllerMode.MANUAL}
      source={tune.initial.source}
      setpoint={true}
      readings={tune.readings}
      duration={POLL_RETENTION_SEC}
    />

    <SetupWarning process={tune.process} measures={tune.readings.map(r => r.measured)} setpoint={setpoint}/>

    <ButtonRow justify="right">
      <ButtonNext
        title="Go To Identify Step"
        onClick={
          measurementOK(stableVoltage) && measurementOK(maxVoltage) && measurementOK(setpoint)
            ?
            () => {
              actions.autoTuneGoToStep({
                ...tune, step: 'identify',
                stableVoltage,
                maxVoltage,
                setpoint,
                run: null,
                readings: [],
              });
            }
            :
            'disabled'
        }
      >
        Next
      </ButtonNext>
    </ButtonRow>
  </>;
};
