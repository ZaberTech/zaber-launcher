import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';

import type { EntityKey } from '../keys';
import { useActions, tryAccess } from '../utils';

import { actions as actionsDefinition } from './actions';
import { defaultConnectionState } from './reducer';
import { selectConnections } from './selectors';

interface Props {
  connectionKey: EntityKey;
  enabled?: boolean;
  children?: (error: string | null) => React.ReactNode;
}

export const ConnectionMonitor: React.FC<Props> = ({ connectionKey, children, enabled = true }) => {
  const actions = useActions(actionsDefinition);
  const state = tryAccess(useSelector(selectConnections), connectionKey) ?? defaultConnectionState;

  useEffect(() => {
    if (!enabled) { return }

    actions.start(connectionKey);
    return () => { actions.stop(connectionKey) };
  }, [connectionKey, enabled]);

  return <>{children?.(state.error)}</>;
};
