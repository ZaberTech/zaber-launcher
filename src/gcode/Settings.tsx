import { HeaderCard, Input, NoticeBanner, Text, HelpTooltip, Loader, ButtonRowConfirmCancel, Button } from '@zaber/react-library';
import { routerActions as routerActionsDefinition } from 'connected-react-router';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { AxisSettingsCard } from './AxisSetting';
import { selectEditedSettings, selectEditedSettingsValid, selectLoadingDevice, selectSettingsDevice } from './selectors';
import { isSupported, Paths } from './types';

export const Settings: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const routerActions = useActions(routerActionsDefinition);
  const device = useSelector(selectSettingsDevice);
  const settings = useSelector(selectEditedSettings)!;
  const { traverseRate } = settings;
  const loadingDevice = useSelector(selectLoadingDevice);
  const valid = useSelector(selectEditedSettingsValid);

  useEffect(() => {
    if (device == null) {
      routerActions.replace(Paths.SelectController);
    }
  }, [device]);

  const done = () => {
    actions.settingsEditingDone(true);
    routerActions.replace(Paths.Root);
    actions.startTranslator();
  };

  if (device == null) {
    return null;
  }

  return (<div className="settings">
    {loadingDevice.loading && <div className="loading"><Loader size="small"/>&ensp;Loading...</div>}
    {loadingDevice.error != null && <NoticeBanner type="error">
      Cannot load device data: {loadingDevice.error}
    </NoticeBanner>}
    {!isSupported(device.identity.firmwareVersion) && <NoticeBanner type="warning">
      G-Code requires firmware version 7.28 or higher. Some functionality may not be available.
    </NoticeBanner>}

    {traverseRate && <HeaderCard edge="outline" header="General Settings">
      <div className="setting">
        <Text>Traverse rate:</Text>
        <Input type="number"
          value={Number.isFinite(traverseRate.value) ? traverseRate.value : ''}
          onValueChange={value => actions.updateSettings({ traverseRate: { ...traverseRate, value: +value } })}/>
        <Text className="units">mm/s</Text>
        <HelpTooltip>Traverse rate is the maximum combined speed of all axes when executing G0 instruction.</HelpTooltip>
      </div>
    </HeaderCard>}

    {settings.axes?.map((axisSettings, i) =>
      <AxisSettingsCard key={i} axisIndex={i} settings={axisSettings} axis={device.axes[i]}/>
    )}

    <ButtonRowConfirmCancel
      confirmText="Configure"
      onConfirm={valid ? done : 'disabled'}
      onCancel={() => {
        actions.settingsEditingDone(false);
        routerActions.replace(Paths.Root);
      }}>
      <Button color="grey" className="select-different-controller"
        onClick={() => routerActions.replace(Paths.SelectController)}>
        Select Different Controller
      </Button>
    </ButtonRowConfirmCancel>
  </div>);
};
