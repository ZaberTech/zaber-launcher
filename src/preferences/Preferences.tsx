import React, { useEffect, useState } from 'react';
import * as remote from '@electron/remote';
import { useSelector } from 'react-redux';
import { Icons, Text, Flex, HelpTooltip, Toggle, Button, NumericInputEditor, InputEditor } from '@zaber/react-library';
import { match } from 'ts-pattern';

import { ContentHeader } from '../components';
import { useActions } from '../utils';
import { ZMLLoggingService as ZML } from '../zml/logging_service';
import { environment } from '../environment';

import { PreferenceItem, PreferenceKeys, DEFAULT_VALUES } from './types';
import { actions as actionsDefinition } from './actions';
import { selectPreferenceStates } from './selectors';

const folderPath = ZML.LOG_FOLDER_PATH;
const fileExplorerName = environment.platform === 'win32' ? 'file explorer' : 'finder';

function openLogFolder() {
  remote.shell.openPath(folderPath).catch(() => null);
}

// the preferences which will be displayed
// preset values are loaded from app storage --
// if storage doesn't contain a value, the default value is used
export const preferences: PreferenceItem[] = [
  {
    title: 'Log Communication',
    key: PreferenceKeys.ZML_LOGGING,
    type: 'bool',
    tooltip: 'Turn on / off logging to file of communication with connected devices.',
    component: <Button className="preferences-link" color="grey"
      title={`Open log folder in ${fileExplorerName}`} onClick={openLogFolder}>Open Folder</Button>,
  },
  {
    title: 'Local Timeout (ms)',
    key: PreferenceKeys.TIMEOUT_LOCAL,
    type: 'number',
    tooltip: 'The maximum time for a device connected to this computer to respond to a request.'
  },
  {
    title: 'Remote Timeout (ms)',
    key: PreferenceKeys.TIMEOUT_REMOTE,
    type: 'number',
    tooltip: 'The maximum time for a network-connected device to respond to a request.'
  }
];

interface PreferenceBoolProps {
  title: string;
  value: boolean;
  updating: boolean;
  onChange: (value: boolean) => void;
}

const PreferenceBool: React.FC<PreferenceBoolProps> = ({ title, value, updating, onChange }) =>
  <Toggle
    title={title}
    value={value}
    disabled={updating}
    onValueChange={onChange}
  />;

interface PreferenceIntProps {
  title: string;
  value: number;
  onChange: (value: number) => void;
}

const PreferenceInt: React.FC<PreferenceIntProps> = ({ title, value, onChange }) => {
  const [displayValue, setDisplayValue] = useState<number | null>(value);
  const [mode, setMode] = useState<InputEditor.Mode>('display');

  useEffect(() => {
    setDisplayValue(value);
    setMode('display');
  }, [value]);

  return <NumericInputEditor
    title={title}
    value={displayValue}
    mode={mode}
    onChange={({ mode, value: newValue }) => {
      setDisplayValue(newValue);
      if (mode === 'write') {
        onChange(newValue);
      }
      setMode(mode);
    }}
    aria-label={`Set ${title}`}
  />;
};

interface PreferenceProps {
  preference: PreferenceItem;
}

const Preference: React.FC<PreferenceProps> = ({ preference }) => {
  const actions = useActions(actionsDefinition);

  const preferenceStates = useSelector(selectPreferenceStates);
  const preferenceState = preferenceStates[preference.key];

  if (preferenceState == null) {
    return null;
  }

  return <>
    <Flex.Row className="preference-label">
      <Text className="label-text" t={Text.Type.H4}>{preference.title}</Text>
      <HelpTooltip>{preference.tooltip}</HelpTooltip>
    </Flex.Row>
    <div className="preference-custom-component">
      {preference.component}
    </div>
    <Flex.Row className="preference-input">
      {match(preference)
        .with({ type: 'bool' }, ({ key, title }) =>
          <PreferenceBool
            title={title}
            value={preferenceState.value as boolean}
            updating={preferenceState.updating}
            onChange={value => actions.setPreference(key, value)}
          />)
        .with({ type: 'number' }, ({ key, title }) =>
          <PreferenceInt
            title={title}
            value={preferenceState.value as number}
            onChange={value => actions.setPreference(key, value)}
          />)
        .exhaustive()
      }
      <Icons.Restore
        className="restore-icon"
        title={`Reset "${preference.title}" to default`}
        onClick={() => actions.setPreference(preference.key, DEFAULT_VALUES[preference.key])}
      />
    </Flex.Row>
  </>;
};

export const Preferences: React.FC = () => <div className="preferences-page">
  <ContentHeader>Preferences</ContentHeader>
  <div className="preferences">
    {preferences.map((preference, i) => <Preference key={i} preference={preference}/>)}
  </div>
</div>;
