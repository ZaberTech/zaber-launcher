import _ from 'lodash';
import { duration } from 'moment';
import React from 'react';
import { useSelector } from 'react-redux';
import Select from 'react-select';
import { Checkbox, Flex, HelpTooltip, NoticeType, NumericInput, PopUp, Slider, Text, TextType } from '@zaber/react-library';

import { makeAxisKey } from '../keys';
import { InputWithUnits } from '../units';
import { roundedE3Sequence, roundToMultiple, useActions } from '../utils';
import { compareFirmwareVersions } from '../app_components';
import { MS_PER_SEC } from '../types';

import { actions as actionDefinitions } from './actions';
import { numFwChannelsUsed } from './channels';
import { MAX_POLLED_BUFFER_SIZE } from './sagas';
import { ComboBoxOption, getComboBoxItemLabel, Mode, reactSelectStyle, SignalType, START_RECORDING_BUTTON_LABEL } from './types';
import { getDifferencingRange, isNewRecTab } from './utils';
import {
  selectAllChannels,
  selectCaptureSettings,
  selectIsCaptureInProgress,
  selectSelectedConnectionIsSerialPort,
  selectSelectedDeviceIdentity,
  selectSelectedDeviceKey,
  selectSelectedDeviceState,
  selectSelectedRecording,
  selectSelectedRecordingIndex,
  selectSignalGeneratorSettings,
} from './selectors';


const ModeDescription = {
  [Mode.POLLED]: 'Low-resolution capture',
  [Mode.BUFFERED_MANUAL]: 'High-resolution capture',
  [Mode.EXTERNAL]: 'Externally triggered high-resolution capture',
  [Mode.DOWNLOAD]: 'Display data currently stored on the device',
};


const ModeHelp = {
  [Mode.POLLED]: 'Zaber Launcher queries the axis for position or settings as fast as the communication port will allow. '
    + 'Polled mode can run continuously, but the sampling frequency is limited by the communication speed, and '
    + 'is divided by the number of settings being read. Samples will be irregularly spaced in time and samples from '
    + 'multiple channels will not be taken at the same times.',
  [Mode.BUFFERED_MANUAL]: 'You start the capture, and the device firmware stores the samples in an internal buffer '
    + 'before downloading that Zaber Launcher will download when full. '
    + 'Buffered mode can capture up to 6,144 samples across up to six channels simultaneously at up to 10kHz '
    + '(faster than Polled mode and with no jitter). '
    + 'Samples will be regularly spaced in time and samples from multiple channels will be taken at the same times.',
  [Mode.EXTERNAL]: 'Waits for the "scope start" command to be sent by another application or external program. '
    + 'The Zaber Launcher Oscilloscope will then retrieve and display the data. You must configure the oscilloscope '
    + 'channels and settings externally in this mode - for example in the Terminal and Settings apps or in your own program.',
  [Mode.DOWNLOAD]: 'Reads whatever data is currently in the oscilloscope buffer on the selected device, without '
    + 'causing any action or waiting for a trigger. If the device is currently recording data, the app will wait '
    + 'for recording to complete before reading the data.'
};


interface SampleRateProps {
  min: number;
  max: number;
  enabled: boolean;
}


const SampleRate: React.FC<SampleRateProps> = props => {
  const { min, max, enabled } = props;
  const actions = useActions(actionDefinitions);
  const { mode, sampleRates } = useSelector(selectCaptureSettings);

  const sampleRateOptions = roundedE3Sequence(min, max).map(n => ({ value: n, label: n.toLocaleString() }));

  return <>
    <Text className="col1" t={TextType.H5}>Target Sampling Rate (Hz)</Text>

    <Select<ComboBoxOption> className="col2 combo-box"
      value={sampleRateOptions.find(o => o.value === sampleRates[mode])}
      data-testid="rate-selector"
      styles={reactSelectStyle}
      menuPlacement="auto"
      formatOptionLabel={getComboBoxItemLabel}
      onChange={option => option && actions.setTargetSampleRate(+option.value)}
      isDisabled={!enabled}
      options={sampleRateOptions}/>
  </>;
};


const SampleLimit: React.FC<{ enabled: boolean }> = ({ enabled }) => {
  const actions = useActions(actionDefinitions);
  const { samplesToCapture } = useSelector(selectCaptureSettings);
  const { fwBufferSize, bufferReshapable } = useSelector(selectSelectedDeviceState);
  const channels = useSelector(selectAllChannels);
  const numChannelsUsed = Math.max(1, numFwChannelsUsed(channels));

  const maxSamples = bufferReshapable ? Math.floor(fwBufferSize / numChannelsUsed) : 1024;
  let status: NoticeType | null = null;
  let message: string | null = null;
  if (samplesToCapture != null) {
    if (samplesToCapture < 1) {
      status = 'error';
      message = 'Must be at least 1.';
    } else if (samplesToCapture > maxSamples) {
      status = 'warning';
      message = `With the number of channels currently selected, at most ${maxSamples} samples will be captured.`;
    }
  }

  return <>
    <Flex.Row className="col1 item-row">
      <Text t={TextType.H5}>Number of Samples</Text>
      <HelpTooltip>
        Optional number of samples to capture per channel. Use this to trim extraneous data
        from the end of an experiment and speed up data retrieval. If left blank,
        the device will capture until its buffer is full.
      </HelpTooltip>
    </Flex.Row>
    <PopUp className="col2" position="bottom" triggerAction={message ? 'hover' : 'none'} trigger={() =>
      <Flex.Row className="item-row">
        <NumericInput
          value={samplesToCapture}
          disabled={!enabled}
          min={1} max={maxSamples}
          status={status}
          data-testid="sample-limit"
          onNumberChange={actions.setSampleLimit}
        />
        <Text t={TextType.Instruction}>(Optional)</Text>
      </Flex.Row>
    }>
      {message}
    </PopUp>
  </>;
};


const CentralDifferencing: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const { mode, cDiffSpan, sampleRates } = useSelector(selectCaptureSettings);
  const selectedRecording = useSelector(selectSelectedRecording);
  const isCaptureInProgress = useSelector(selectIsCaptureInProgress);

  const sampleRate = selectedRecording?.actualSampleRate ?? sampleRates[mode];
  const [min, max] = getDifferencingRange(sampleRate);

  return <>
    <Flex.Row className="col1 item-row">
      <Text t={TextType.H5}>Differencing Window (ms)</Text>
      <HelpTooltip>
        Controls the spacing of the sample points for central differencing of math channels.
        Larger numbers produce smoother results but will lose details.
      </HelpTooltip>
    </Flex.Row>
    <Slider className="col2"
      data-testid="differencing-window"
      disabled={isCaptureInProgress}
      min={min}
      max={max}
      minLabel={`${min}`}
      maxLabel={`${max}`}
      step={Math.max(roundToMultiple(1 / ((max - min) / min), 0.1), 0.1)}
      value={cDiffSpan}
      onValueChange={actions.setDifferencingSpan}/>
  </>;
};


interface TimeEstimateProps {
  numSamples: number;
  sampleRate: number;
  willExpire?: boolean;
}


const TimeEstimate: React.FC<TimeEstimateProps> = ({ numSamples, sampleRate, willExpire }) => {
  const { fwBufferSize, bufferReshapable } = useSelector(selectSelectedDeviceState);
  const channels = useSelector(selectAllChannels);
  const numChannelsUsed = Math.max(1, numFwChannelsUsed(channels));

  const maxSamples = bufferReshapable ? Math.floor(fwBufferSize / numChannelsUsed) : 1024;

  numSamples = Math.min(maxSamples, numSamples);
  const estimate = duration(numSamples / sampleRate, 'seconds');
  const parts = [] as string[];

  if (estimate.days() > 0) {
    parts.push(`${estimate.days()}d`);
    estimate.subtract(duration(estimate.days(), 'days'));
  }

  if (estimate.hours() > 0) {
    parts.push(`${estimate.hours()}h`);
    estimate.subtract(duration(estimate.hours(), 'hours'));
  }

  if (estimate.minutes() > 0) {
    parts.push(`${estimate.minutes()}m`);
    estimate.subtract(duration(estimate.minutes(), 'minutes'));
  }

  if (estimate.seconds() > 0) {
    parts.push(`${estimate.seconds()}s`);
    estimate.subtract(duration(estimate.seconds(), 'seconds'));
  }

  if (estimate.milliseconds() >= 1) {
    parts.push(`${Math.floor(estimate.milliseconds())}ms`);
  }

  if (parts.length === 0) {
    parts.push('0');
  }

  return <>
    <Flex.Row className="col1 item-row">
      <Text t={TextType.H5}>Recording Length</Text>
      <HelpTooltip className="col2">
        Approximate time span of recorded data that will be displayed.
        { willExpire && ' Older data will be discarded.' }
      </HelpTooltip>
    </Flex.Row>
    <Text className="col2" t={TextType.H5} data-testid="time-estimate">{parts.join(' ')}</Text>
  </>;
};


const Polled: React.FC = () => {
  const selectedRecordingIndex = useSelector(selectSelectedRecordingIndex);
  const isCapturing = useSelector(selectIsCaptureInProgress);
  const enableChanges = isNewRecTab(selectedRecordingIndex) && !isCapturing;
  const selectedRecording = useSelector(selectSelectedRecording);
  const axisSettings = useSelector(selectSelectedDeviceState);

  const sampleRate = selectedRecording
    ? (selectedRecording.actualSampleRate ?? selectedRecording.sampleRates.Polled)
    : axisSettings.sampleRates.Polled;

  return <>
    <SampleRate min={1} max={100} enabled={enableChanges}/>
    {isNewRecTab(selectedRecordingIndex) &&
      <TimeEstimate numSamples={MAX_POLLED_BUFFER_SIZE} sampleRate={sampleRate} willExpire/>}
    <CentralDifferencing/>
  </>;
};

const RecordingDelayTooltip: React.FC = () => (
  <HelpTooltip position="top">
    A delay between between the start of the capture
    and the first data recorded. Applies after the start timer, if any.
  </HelpTooltip>);

const TimerTooltip: React.FC = () => (
  <HelpTooltip position="top">
    A delay between pressing the {START_RECORDING_BUTTON_LABEL}
    &nbsp;button and the actual start of data recording, to allow table vibrations to dampen.
  </HelpTooltip>);

const BufferedManual: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const selectedRecordingIndex = useSelector(selectSelectedRecordingIndex);
  const { startDelay, recordingDelay, sampleRates } = useSelector(selectCaptureSettings);
  const { signalAxis, signalReturnToStart: returnToStart, signalType, signalValue } = useSelector(selectSignalGeneratorSettings);
  const selectedDeviceKey = useSelector(selectSelectedDeviceKey);
  const isCapturing = useSelector(selectIsCaptureInProgress);
  const deviceState = useSelector(selectSelectedDeviceState);
  const deviceIdentity = useSelector(selectSelectedDeviceIdentity);

  const totalSettingsChannels = numFwChannelsUsed(deviceState.axes.map(axisState => axisState.channels).flat());
  const enableChanges = isNewRecTab(selectedRecordingIndex) && !isCapturing;
  const signalTypeOptions = Object.values(SignalType).map(item => ({ value: item, label: item }));
  const numAxes = deviceState.axes.length - 1;
  const axisOptions = _.range(0, numAxes).map((_, i) => ({
    label: `Axis ${i + 1}`,
    value: i + 1,
  }));

  return <>
    <Text className="col1" t={TextType.H5}>Movement</Text>

    <div className="col2 control-group">
      <Select<ComboBoxOption> className="combo-box signal-axis"
        value={axisOptions.find(o => o.value === signalAxis)}
        styles={reactSelectStyle}
        formatOptionLabel={getComboBoxItemLabel}
        onChange={option => option && actions.setSignalAxis(option.value as number)}
        isDisabled={!enableChanges || numAxes < 2}
        data-testid="signal-axis-selector"
        options={axisOptions}/>

      <Select<ComboBoxOption> className="combo-box"
        value={signalTypeOptions.find(o => o.value === signalType)}
        styles={reactSelectStyle}
        formatOptionLabel={getComboBoxItemLabel}
        onChange={option => option && actions.setSignalType(option.value as SignalType)}
        isDisabled={!enableChanges}
        data-testid="signal-type-selector"
        options={signalTypeOptions}/>

      <InputWithUnits disabled={signalType === SignalType.NONE || !enableChanges}
        unitFrom={{ setting: 'pos', entity: selectedDeviceKey ? makeAxisKey(selectedDeviceKey, signalAxis) : null }}
        measure={signalValue}
        title="Position Control"
        onChange={newValue => actions.setSignalValue(newValue)}
      />
    </div>
    <Flex.Row className="col1 item-row">
      <Text t={TextType.H5}>Return to Start</Text>
      <HelpTooltip>
        This checkbox instructs the axis to return to the original position after data has been recorded,
        so that it is ready for another run.
      </HelpTooltip>
    </Flex.Row>
    <div className="col2 pair">
      <Checkbox checked={returnToStart}
        data-testid="enable-return-to-start"
        onChecked={checked => actions.setSignalReturnToStart(checked)}
        disabled={signalType === SignalType.NONE || !enableChanges}/>
    </div>
    <Flex.Row className="col1 item-row">
      <Text t={TextType.H5}>Timer (s)</Text>
      <TimerTooltip/>
    </Flex.Row>
    <NumericInput className="col2"
      value={startDelay / MS_PER_SEC} defaultValue={0}
      disabled={!enableChanges}
      data-testid="start-timer"
      onNumberChange={value => actions.setCaptureDelays({
        startDelay: MS_PER_SEC * (value ?? 0),
      })}/>

    <Flex.Row className="col1 item-row">
      <Text t={TextType.H5}>Recording Delay (ms)</Text>
      <HelpTooltip position="top">
        A delay between the start of the capture
        and the first data recorded. Applies after the start timer, if any.
      </HelpTooltip>
    </Flex.Row>
    <NumericInput className="col2"
      value={recordingDelay} defaultValue={0}
      disabled={!enableChanges}
      data-testid="recording-delay"
      onNumberChange={value => actions.setCaptureDelays({
        recordingDelay: value ?? 0,
      })}/>

    <SampleRate min={1} max={10000} enabled={enableChanges}/>

    {compareFirmwareVersions(deviceIdentity?.identity?.firmwareVersion, { major: 7, minor: 29, build: 0 }) >= 0 &&
      <SampleLimit enabled={enableChanges}/>
    }

    {deviceState &&
      <TimeEstimate
        numSamples={deviceState.samplesToCapture ??
          (deviceState.bufferReshapable ? deviceState.fwBufferSize / totalSettingsChannels : 1024)}
        sampleRate={sampleRates[Mode.BUFFERED_MANUAL]}/>}

    <CentralDifferencing/>
  </>;
};


const ExternallyTriggered: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const selectedRecordingIndex = useSelector(selectSelectedRecordingIndex);
  const { externalWaitForScopePrint } = useSelector(selectCaptureSettings);
  const { startDelay, recordingDelay } = useSelector(selectCaptureSettings);
  const isCapturing = useSelector(selectIsCaptureInProgress);
  const isComplete = !isNewRecTab(selectedRecordingIndex);

  return <>
    <Flex.Row className="col1 item-row">
      <Text>I'm also reading the data</Text>
      <HelpTooltip position="bottom">
        Check this if you are reading the oscilloscope data from the device in your external program
        or interactively; in order to avoid timeouts, Zaber Launcher will wait until you have a copy of
        the data before reading and displaying it.<br/><br/>
        Uncheck this if you are not reading the data externally.
      </HelpTooltip>
    </Flex.Row>
    <div className="col2 pair">
      <Checkbox
        data-testid="wait-for-external-scope-read"
        checked={externalWaitForScopePrint}
        onChecked={() => actions.setExternalWaitForPrint(!externalWaitForScopePrint)}
        disabled={isComplete || isCapturing}/>
    </div>

    {isComplete && <>
      <Flex.Row className="col1 item-row">
        <Text t={TextType.H5}>Timer (s)</Text>
        <TimerTooltip/>
      </Flex.Row>
      <NumericInput className="col2"
        value={startDelay / MS_PER_SEC}
        disabled
        data-testid="start-timer"
        onNumberChange={() => { /* Never editable. */ }}/>

      <Flex.Row className="col1 item-row">
        <Text t={TextType.H5}>Recording Delay (ms)</Text>
        <RecordingDelayTooltip/>
      </Flex.Row>
      <NumericInput
        className="col2"
        value={recordingDelay}
        disabled
        data-testid="recording-delay"
        onNumberChange={() => { /* Never editable. */ }}
      />
    </>}

    {isComplete && <SampleRate min={1} max={10000} enabled={false}/>}

    <CentralDifferencing/>
  </>;
};


const Download: React.FC = () => {
  const selectedRecordingIndex = useSelector(selectSelectedRecordingIndex);
  const { recordingDelay } = useSelector(selectCaptureSettings);
  const isComplete = !isNewRecTab(selectedRecordingIndex);

  return <>
    {isComplete && <>
      <Flex.Row className="col1 item-row">
        <Text t={TextType.H5}>Recording Delay (ms)</Text>
        <RecordingDelayTooltip/>
      </Flex.Row>
      <NumericInput
        className="col2"
        value={recordingDelay}
        disabled
        data-testid="recording-delay"
        onNumberChange={() => { /* Never editable. */ }}
      />
    </>}

    {isComplete && <SampleRate min={1} max={10000} enabled={false}/>}

    <CentralDifferencing/>
  </>;
};


export const SettingsTab: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const selectedRecordingIndex = useSelector(selectSelectedRecordingIndex);
  const isCapturing = useSelector(selectIsCaptureInProgress);
  const { mode } = useSelector(selectCaptureSettings);
  const isSerialPort = useSelector(selectSelectedConnectionIsSerialPort);

  const enableChanges = isNewRecTab(selectedRecordingIndex) && !isCapturing;
  const modeOptions = Object.values(Mode)
    .filter(mode => isSerialPort || mode !== Mode.EXTERNAL)
    .map(item => ({ value: item, label: item }));

  return <div className="settings-tab">
    <Text className="col1" t={TextType.H5}>Capture Mode</Text>

    <Flex.Row className="col2 item-row">
      <Select<ComboBoxOption>
        className="combo-box"
        value={modeOptions.find(o => o.value === mode)}
        styles={reactSelectStyle}
        formatOptionLabel={getComboBoxItemLabel}
        onChange={option => option && actions.setCaptureMode(option.value as Mode)}
        data-testid="mode-selector"
        isDisabled={!enableChanges}
        options={modeOptions}
      />
      <Text t={TextType.Body}>{ModeDescription[mode]}</Text>
      <HelpTooltip>{ModeHelp[mode]}</HelpTooltip>
    </Flex.Row>

    <div className="hr">&nbsp;</div>

    {mode === Mode.POLLED && <Polled/>}
    {mode === Mode.BUFFERED_MANUAL && <BufferedManual/>}
    {mode === Mode.EXTERNAL && <ExternallyTriggered/>}
    {mode === Mode.DOWNLOAD && <Download/>}
  </div>;
};
