import React from 'react';
import { RenderResult, render, getByTitle, fireEvent } from '@testing-library/react';
import { injectable } from 'inversify';

let translator: TranslatorMock;
let translatorCallback: (t: TranslatorMock) => void;
class TranslatorMock {
  constructor() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    translator = this;
    if (translatorCallback) { translatorCallback(this) }
  }

  static setup = jest.fn(async () => new TranslatorMock());

  translate = jest.fn(async (_block: string): Promise<TranslateResult> => ({ warnings: [], commands: [] }));
  flush = jest.fn(async (): Promise<string[]> => ([]));
}

jest.mock('@zaber/motion/gcode', () => ({
  Translator: TranslatorMock,
}));

import { ascii } from '@zaber/motion';
import type { TranslateResult } from '@zaber/motion/gcode';

import { createContainer, destroyContainer } from '../container';
import { wrapWithNewStore, wrapWithRouter, StorageMock, mockStorage, waitUntilPass } from '../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { MessageRoutersService } from '../message_router';
import { mockDataForDeviceWithoutPeripherals, mockDataForDeviceWithPeripherals, mockLocalConnections } from '../connection_manager/mocks';

import { Demo } from './Demo';
import type { DeviceInfoWithAxes } from '../connection_manager';
import { WarningFlags } from '@zaber/motion/ascii';
import { CONTENT_STORAGE_KEY } from './sagas';

async function asyncNoop() { }

let axes: AxisMock[];
let axisCallback: (axis: AxisMock) => void;

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);

    axes.push(this);
    if (axisCallback) { axisCallback(this) }
  }
  axisType = ascii.AxisType.LINEAR;
  _settings: Record<string, number> = {
    'limit.min': 200,
    'limit.max': 1000,
    'maxspeed': 100,
  };
  settings = {
    get: jest.fn(async setting => this._settings[setting]),
  };
  _warnings: string[] = [WarningFlags.NO_REFERENCE_POSITION];
  warnings = {
    clearFlags: jest.fn(async () => new Set(this._warnings)),
  };
  home = jest.fn(asyncNoop);
  stop = jest.fn(asyncNoop);
}

class StreamMock {
  disabled = true;
  disable = jest.fn(async () => {
    this.disabled = true;
  });
  waitUntilIdle = jest.fn(() => new Promise(r => setTimeout(r, 0)));
  setupLiveComposite = jest.fn(async () => {
    this.disabled = false;
  });
}

let devices: DeviceMock[];
class DeviceMock extends DeviceMockBase<AxisMock> {
  stream = new StreamMock();
  lockstep = {
    home: jest.fn(asyncNoop),
    stop: jest.fn(asyncNoop),
  };
  identity = { };
  warnings = {
    clearFlags: jest.fn(),
  };
  allAxes = {
    stop: jest.fn(),
  };

  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    devices.push(this);
  }

  streams = {
    getStream: jest.fn(() => this.stream),
  };

  getLockstep = jest.fn(() => this.lockstep);
}

let connections: ConnectionMock[];

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  constructor(id: string, router: RouterConnectionMock) {
    super(id, router);
    connections.push(this);
  }
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

let storageMock: StorageMock;

const TestDemo = wrapWithNewStore(wrapWithRouter(Demo));

let wrapper: RenderResult;

beforeEach(() => {
  devices = [];
  axes = [];
  connections = [];
  axisCallback = null!;

  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  storageMock = mockStorage(container);

  wrapper = render(<TestDemo/>);

  mockDeviceChain();
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();

  storageMock = null!;

  translator = null!;
  translatorCallback = null!;

  TranslatorMock.setup.mockClear();
});

function getAxis(regex: RegExp | string) {
  return wrapper.getByText(regex).parentElement!;
}

function mockDeviceChain() {
  function addLockstepModifier(info: DeviceInfoWithAxes): DeviceInfoWithAxes {
    info.locksteps = [{
      groupNumber: 1, axisNumbers: [2, 3],
    }];
    return info;
  }

  mockLocalConnections(TestDemo.testStore, {
    COM1: connectionKey => [
      mockDataForDeviceWithoutPeripherals(connectionKey, 1, 1),
      addLockstepModifier(mockDataForDeviceWithPeripherals(connectionKey, 2, 4, 'smart')),
    ],
  });
}

async function runForSomeTime(axis: HTMLElement, count = 5) {
  fireEvent.click(getByTitle(axis, 'Start Axis'));
  await waitUntilPass(() => expect(translator.translate.mock.calls.length).toBeGreaterThan(count));
  fireEvent.click(getByTitle(axis, 'Stop Axis'));
}

test('renders', () => {
  wrapper.getByText(/Stopped 4/);
});

test('moves single axis (stop, home, setup, translation)', async () => {
  const axis = getAxis(/X-LHM/);

  fireEvent.click(getByTitle(axis, 'Start Axis'));

  await waitUntilPass(() => {
    expect(axes[0].stop).toHaveBeenCalled();
    expect(axes[0].home).toHaveBeenCalled();
    expect(devices[0].stream.setupLiveComposite).toHaveBeenCalledWith(expect.objectContaining({ axisNumber: 1 }));
    expect(TranslatorMock.setup).toHaveBeenCalledWith(devices[0].stream);
  });

  await waitUntilPass(() => expect(translator.translate.mock.calls.length).toBeGreaterThan(5));

  axes[0].stop.mockClear();
  fireEvent.click(getByTitle(axis, 'Stop Axis'));
  await waitUntilPass(() => expect(axes[0].stop).toHaveBeenCalled());
});

test('shows errors', async () => {
  const axis = getAxis(/X-LHM/);

  fireEvent.click(getByTitle(axis, 'Start Axis'));

  await waitUntilPass(() => {
    expect(TranslatorMock.setup).toHaveBeenCalled();
  });

  translator.translate.mockImplementation(async () => {
    if (translator.translate.mock.calls.length > 5) {
      throw new Error('Movement interrupted');
    }
    return ({ warnings: [], commands: [] });
  });

  await waitUntilPass(() => wrapper.getByText(/Movement interrupted/));
});

test('works with lockstep', async () => {
  await runForSomeTime(getAxis(/Axis 2/));
  await waitUntilPass(() => wrapper.getByText(/Stopped 4/));

  expect(devices[0].stream.setupLiveComposite).toHaveBeenCalledWith(
    expect.objectContaining({ axisNumber: 1, axisType: ascii.StreamAxisType.LOCKSTEP }));
  expect(devices[0].lockstep.stop).toHaveBeenCalled();
  expect(devices[0].lockstep.home).toHaveBeenCalled();
});

test('changing G-Code in settings', async () => {
  fireEvent.click(wrapper.getByTitle('Settings'));
  fireEvent.input(wrapper.getByPlaceholderText(/G-Code/), { target: { value: 'G999' } });
  fireEvent.click(wrapper.getByText('Save'));

  await runForSomeTime(getAxis(/X-LHM/));

  expect(translator.translate).toHaveBeenCalledWith('G999');
});

test('Start/Stop All', async () => {
  fireEvent.click(wrapper.getByTitle('Start All Axes'));

  await waitUntilPass(() => {
    expect(axes).toHaveLength(4);
    expect(TranslatorMock.setup).toHaveBeenCalledTimes(4);
  });
  wrapper.getByText(/Running 4/);

  await waitUntilPass(() => expect(translator.translate.mock.calls.length).toBeGreaterThan(5));

  fireEvent.click(wrapper.getByTitle('Stop All Axes'));
  await waitUntilPass(() => wrapper.getByText(/Stopped 4/));
});

test('retrieves the code from storage', async () => {
  wrapper.unmount();
  storageMock.stored[CONTENT_STORAGE_KEY] = 'G999';
  wrapper = render(<TestDemo/>);

  fireEvent.click(wrapper.getByTitle('Settings'));
  expect(wrapper.getByPlaceholderText(/G-Code/)).toHaveValue('G999');
});

test('transforms percentage sing in the code to travel and maxspeed fraction', async () => {
  fireEvent.click(wrapper.getByTitle('Settings'));
  fireEvent.input(wrapper.getByPlaceholderText(/G-Code/), { target: { value: 'G1 X50% F20%' } });
  fireEvent.click(wrapper.getByText('Save'));

  const axis = getAxis(/X-LHM/);
  fireEvent.click(getByTitle(axis, 'Start Axis'));
  await waitUntilPass(() => expect(translator.translate).toHaveBeenCalled());
  fireEvent.click(getByTitle(axis, 'Stop Axis'));

  expect(translator.translate).toHaveBeenCalledWith('G1X600F1200');
});
