/** The percentage of tolerance acceptable for process on-off duty cycle, it will be used to check the quality of identification */
const AUTOTUNE_DUTY_CYCLE_TOLERANCE = 12.5;
export const MAX_DUTY_CYCLE = 50 + AUTOTUNE_DUTY_CYCLE_TOLERANCE;
export const MIN_DUTY_CYCLE = 50 - AUTOTUNE_DUTY_CYCLE_TOLERANCE;
