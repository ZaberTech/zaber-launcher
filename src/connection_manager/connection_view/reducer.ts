import { createReducer } from '../../utils';
import { EntityKey, makeRouterKey } from '../../keys';
import { LOCAL_ROUTER_URL } from '../../message_router';
import { ActionTypes as StatusActionTypes, ActionsToPayloads as StatusActionsToPayloads } from '../../connection_status/actions';
import { ActionTypes as ParentsActionTypes, ActionsToPayloads as ParentsActionsToPayloads } from '../actions';

import { ActionsToPayloads, ActionTypes } from './actions';

interface State {
  expanded: Record<string, boolean>;
  lastKey: EntityKey | null;
}

const initialState: State = {
  expanded: {
    [makeRouterKey(LOCAL_ROUTER_URL)]: true,
  },
  lastKey: null,
};

const storeLastKey = (state: State, { key }: ActionsToPayloads[ActionTypes.STORE_LAST_KEY]): State => ({
  ...state,
  lastKey: key,
});

const setExpanded = (state: State, { key, expanded }: ActionsToPayloads[ActionTypes.SET_EXPANDED]): State => ({
  ...state,
  expanded: {
    ...state.expanded,
    [key]: expanded,
  }
});

const devicesLoaded = (state: State, { connectionKey }: ParentsActionsToPayloads[ParentsActionTypes.DEVICES_LOADED]): State => ({
  ...state,
  expanded: {
    ...state.expanded,
    [connectionKey]: true,
  },
});

const routedConnectionDisconnect = (
  state: State,
  { connectionKey }: StatusActionsToPayloads[StatusActionTypes.ROUTED_CONNECTION_DISCONNECT],
): State => ({
  ...state,
  expanded: {
    ...state.expanded,
    [connectionKey]: false,
  },
});

export type ConnectionViewState = State;
type Payloads = ActionsToPayloads & StatusActionsToPayloads & ParentsActionsToPayloads;
export const connectionViewReducer = createReducer<Payloads, typeof initialState>({
  [ActionTypes.STORE_LAST_KEY]: storeLastKey,
  [ActionTypes.SET_EXPANDED]: setExpanded,
  [StatusActionTypes.ROUTED_CONNECTION_DISCONNECT]: routedConnectionDisconnect,
  [ParentsActionTypes.DEVICES_LOADED]: devicesLoaded,
}, initialState);
