import React from 'react';
import { Text } from '@zaber/react-library';

import { sortAdvancedParams } from '../tuners/advanced_metadata';
import type { AdvancedTuning } from '../types';

export const ParamTable: React.FC<{ params: AdvancedTuning }> = ({ params }) => {
  const sortedParams = sortAdvancedParams(params);
  return <table className="servo-tuning-param-table">
    <thead><tr className="servo-tuning-param-row">
      <th><Text t={Text.Type.H5}>Name</Text></th>
      <th><Text t={Text.Type.H5}>Value</Text></th>
      <th><Text t={Text.Type.H5}>At High Speed</Text></th>
    </tr></thead>
    <tbody>
      {sortedParams.described.map(p =>
        <tr className="servo-tuning-param-row" key={p.paramName} data-testid={`param-row-${p.paramName}`}>
          <td>{p.description}</td>
          <td>{p.param.value}</td>
          <td>{p.highSpeedParam?.value}</td>
        </tr>)}
      {sortedParams.undescribed.map(p => <tr key={p.name}>
        <td>{p.name}</td>
        <td>{p.value}</td>
        <td></td>
      </tr>)}
    </tbody>
  </table>;
};
