import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render } from '@testing-library/react';
import { Subject } from 'rxjs';
import type { Message } from '@zaber/app-components/lib/iot';

jest.mock('uuid', () => ({ v4: () => 'uuid' }));

@injectable()
class IoTServiceMock {
  getConnection: jest.Mock = jest.fn(async () => this);

  messages = new Subject<Message>();

  publish = jest.fn().mockResolvedValue(undefined);
  subscribe = jest.fn(async () => this.messages);
}

import { MessageRoutersService } from '../message_router';
import { createContainer, destroyContainer } from '../container';
import { waitTick, waitUntilPass, wrapWithNewStore } from '../test';
import { IoTService } from '../app_components';
import { actions as managerActions } from '../connection_manager/actions';

import { MessageRoutersServiceMock } from './mocks';
import { CloudDiscovery } from './CloudDiscover';
import { BROADCAST_DISCOVERY_REQUEST_PERIOD } from './sagas';

const addRouterMock = jest.fn(() => ({ type: 'NOTHING', payload: undefined }));
managerActions.addRouter = addRouterMock;

let container: Container;
let iot: IoTServiceMock;

const TestCloudDiscovery = wrapWithNewStore(CloudDiscovery);

let wrapper: RenderResult;

beforeAll(() => {
  jest.useFakeTimers();
});
afterAll(() => {
  jest.useRealTimers();
});

const MY_IDENTITY = 'identity_id';

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  const router = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;
  router.mockSharingOn();
  container.bind<unknown>(IoTService).to(IoTServiceMock);
  iot = container.get<unknown>(IoTService) as IoTServiceMock;

  wrapper = render(<TestCloudDiscovery preloadedState={{
    cloud: {
      loggedInUser: { 'sub': 'id', 'custom:identity_id': MY_IDENTITY },
      sharingStatus: { id: 'my_id' },
    },
  }} onRouterAdded={jest.fn()}/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

test('looks for clients', async () => {
  await waitUntilPass(() =>
    expect(iot.publish).toHaveBeenCalledWith(`${MY_IDENTITY}/message-router/discovery-request`, '')
  );
  expect(iot.subscribe).toHaveBeenCalledWith(`${MY_IDENTITY}/message-router/discovery-response`);
});

describe('subscribed', () => {
  beforeEach(async () => {
    await waitUntilPass(() => expect(iot.publish).toHaveBeenCalled());
  });

  test('keeps sending discovery requests', async () => {
    jest.advanceTimersByTime(BROADCAST_DISCOVERY_REQUEST_PERIOD);
    await waitTick();
    expect(iot.publish).toHaveBeenCalledTimes(2);

    jest.advanceTimersByTime(BROADCAST_DISCOVERY_REQUEST_PERIOD);
    await waitTick();
    expect(iot.publish).toHaveBeenCalledTimes(3);
  });

  test('stops looking and publishing once unmounted', async () => {
    wrapper.unmount();
    jest.advanceTimersByTime(BROADCAST_DISCOVERY_REQUEST_PERIOD);

    await waitUntilPass(() => expect(iot.messages.observers.length).toBe(0));
    expect(iot.publish).toHaveBeenCalledTimes(1);
  });

  test('displays client that comes back', async () => {
    iot.messages.next({ payload: JSON.stringify({ id: 'client1', name: 'Client1' }), topic: '' });

    await waitUntilPass(() => wrapper.getByText('Client1'));
  });

  test('filters own message router response and invalid responses', async () => {
    iot.messages.next({ payload: JSON.stringify({ id: 'uuid', name: 'My Router' }), topic: '' });
    iot.messages.next({ payload: 'gibberish', topic: '' });
    iot.messages.next({ payload: JSON.stringify({ id: 'client1', name: 'Client1' }), topic: '' });

    await waitUntilPass(() => wrapper.getByText('Client1'));
    expect(wrapper.queryByText(/My Router/)).toBeNull();
  });

  test('deals with error during subscription', async () => {
    iot.messages.error(new Error('Internet down'));
    await waitUntilPass(() => wrapper.getByText(/Internet down/));
  });

  test('deals with error during publishing', async () => {
    iot.publish.mockRejectedValueOnce(new Error('Internet down'));
    jest.advanceTimersByTime(BROADCAST_DISCOVERY_REQUEST_PERIOD);
    await waitUntilPass(() => wrapper.getByText(/Internet down/));
  });
});
