import React, { useLayoutEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { Icons, Text } from '@zaber/react-library';
import _ from 'lodash';
import classNames from 'classnames';

import { ExternalLink } from '../components';
import { useCommandIdList, useSettingIdList } from '../protocol_manual/hooks';
import { useActions } from '../utils';

import { selectCommandSuggestions, selectCurrentCommand, selectProtocolLink, selectSelectedSuggestion } from './selectors';
import { suggestionToText } from './utils';
import type { Command, CommandPart } from './types';
import { actions as actionDefinitions } from './actions';

function getCommandId(commandParts: CommandPart[], commandIdList: string[]): string {
  const id = commandParts.filter(part => part.type === 'command').map(part => part.text).join('_');

  if (commandIdList.includes(id)) {
    return `#topic_command_${id}`;
  }

  // When no exact command ID match found, find longest matching command ID
  const matchingCommandIdList = commandIdList.filter(element => id.startsWith(element));

  const longestMatchingCommandId = _.maxBy(matchingCommandIdList, id => id.length);

  if (longestMatchingCommandId !== '') {
    return `#topic_command_${longestMatchingCommandId}`;
  }

  return '#topic_commands';
}

function getSettingId(settingNamePart: CommandPart, settingIdList: string[]): string {
  if (settingNamePart == null || settingNamePart.type !== 'command') {
    return '#topic_settings';
  }

  const id = settingNamePart.text.split('.').join('_');

  if (settingIdList.includes(id)) {
    return `#topic_setting_${id}`;
  }

  return '#topic_settings';
}

interface CommandSuggestionsProps {
  onSuggestionClick: (cmd: string) => void;
}

const CommandSuggestion = ({ index, command, onClick }: {
  index: number;
  command: Command;
  onClick: () => void;
}) => {
  const actions = useActions(actionDefinitions);
  const selectedSuggestion = useSelector(selectSelectedSuggestion);
  const selected = selectedSuggestion === index;

  useLayoutEffect(() => {
    if (selected) {
      ref.current?.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
    }
  }, [selected]);

  const { type, parts } = command;

  const protocolLinkPrefix = useSelector(selectProtocolLink);
  const commandIdList = useCommandIdList();
  const settingIdList = useSettingIdList();

  const protocolLink = `${protocolLinkPrefix}${
    type === 'setting' ? getSettingId(parts[1], settingIdList) : getCommandId(parts, commandIdList)}`;

  const ref = useRef<HTMLDivElement>(null);

  return <div
    ref={ref}
    className={classNames('suggestion-option', { selected })}
    onMouseEnter={() => actions.setSelectedSuggestion(index)}
    onMouseLeave={() => actions.setSelectedSuggestion(null)}
  >
    <ExternalLink url={protocolLink}>
      <span className="suggestion-link">
        <Icons.NewWindow title="Link to Protocol Manual"/>
      </span>
    </ExternalLink>
    <span
      className="suggestion-text"
      onClick={onClick}
      role="button">
      {parts.map((part, i) =>
        <Text key={i} className={part.type}>
          {part.type === 'command' && `${part.text} `}
          {part.type === 'param' && `<${part.text}>${part.multiple ? '...' : ''} `}
          {part.type === 'enum' && `${part.text.join('|')}${part.multiple ? '...' : ''} `}
        </Text>
      )}
    </span>
    {type === 'commandWithUnits' && <UnitConversionNote/>}
  </div>;
};

export const CommandSuggestions: React.FC<CommandSuggestionsProps> = ({
  onSuggestionClick,
}) => {
  const currentCommand = useSelector(selectCurrentCommand);
  const commandSuggestions = useSelector(selectCommandSuggestions);

  if (currentCommand.length === 0 || commandSuggestions.length === 0) {
    return null;
  }

  return (
    <div className="command-suggestions">
      {commandSuggestions.map((command, i) =>
        <CommandSuggestion key={i} command={command} index={i}
          onClick={() => onSuggestionClick(suggestionToText(command.parts, currentCommand))}
        />)}
    </div>
  );
};

export const UnitConversionNote: React.FC = () =>
  <span className="unit-conversion-note">
    Unit conversion will apply
  </span>;
