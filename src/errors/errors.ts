import { EventEmitter } from 'events';

import type SentryTypeNode from '@sentry/node';
// eslint-disable-next-line import-x/default
import type SentryTypeReact from '@sentry/react';
import type { Extras, SeverityLevel, Event } from '@sentry/types';
import { ensureError } from '@zaber/toolbox';

import { environment } from '../environment';

type SentryType = typeof SentryTypeNode | typeof SentryTypeReact;
// eslint-disable-next-line @typescript-eslint/no-require-imports
const Sentry: SentryType = typeof window !== 'undefined' ? require('@sentry/react') : require('@sentry/node');

/**
 * Emits an event when sentry captures an error.
 * Serves to allow application react to the event.
 */
export const errorEmitter = new EventEmitter();

const sendSentryEvents = environment.isProduction;
const failures: SeverityLevel[] = ['warning', 'error', 'fatal'];
const BANNED_DEV_INTEGRATIONS = ['Breadcrumbs'];

function beforeSend(event: Event): Event | null {
  if (failures.includes(event.level ?? 'error')) {
    process.nextTick(() => {
      if (event.level !== 'warning') {
        errorEmitter.emit('error');
      }
      console.error(JSON.stringify(event, null, 2)); // eslint-disable-line no-console
    });
  }
  if (!sendSentryEvents) {
    return null;
  }
  return event;
}

export function initErrors(): void {
  const dsn = process.env.SENTRY_DSN;
  if (environment.isProduction && !dsn) {
    throw new Error(`Sentry.io token missing: ${dsn}`);
  }

  /* We keep sentry running in dev to benefit from the error capturing machinery
  but we prevent it from sending events to the server.
  We need a fake DSN to deceive sentry SDK. */
  Sentry.init({
    dsn,
    release: `${environment.releaseName}-${environment.edition}`,
    normalizeDepth: 10,
    beforeSend,
    autoSessionTracking: false,
    attachStacktrace: true,
    integrations: defaultIntegrations => {
      if (!environment.isProduction) {
        // disables console instrumentation in development
        defaultIntegrations = defaultIntegrations.filter(integration => !BANNED_DEV_INTEGRATIONS.includes(integration.name));
      }
      return defaultIntegrations;
    },
  });
}

export function handleUnexpectedError(err: unknown): void {
  if (environment.isTest) { throw err }
  Sentry.captureException(err);
}

/**
 * Should only be used for errors that do not significantly hinder UX but we should still be aware of.
 * @param caught The object caught
 * @param level The severity of the error
 * @returns caught, ensured to be an error
 */
export function handleNonCriticalError(caught: unknown, level: 'warning' | 'info' = 'warning', extra?: Extras): Error {
  Sentry.captureException(caught, { level, extra });
  return ensureError(caught);
}
