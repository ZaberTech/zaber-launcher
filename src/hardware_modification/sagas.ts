import type { SagaIterator } from '@redux-saga/types';
import { all, call, put, SagaReturnType, select, takeEvery, takeLeading } from 'redux-saga/effects';
import { makeString, throwUnexpectedError } from '@zaber/toolbox';
import { AllAxes, Axis } from '@zaber/motion/ascii';

import type { Action, RT } from '../utils';
import { connectionManagerActions, getDevice, getDeviceOrAxis, reloadDevices } from '../connection_manager';
import { extractConnectionKey } from '../keys';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import { initializeValues } from './initializers';
import { selectChangeCount } from './selectors';
import {
  runPreSetValidators, runPostSetValidators, topLevelErrorOnApply,
  ValidationError, fieldErrorsOnApply, customSetters
} from './validators';
import { SETUP_COMPLETE_KEY } from './storageKeys';
import type { NonNullValue } from './types';

export function* configurationManagerSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.APP_LOADED, appLoaded),
    takeEvery(ActionTypes.ENABLE_MODIFICATION, setDeviceModifiable),
    takeEvery(ActionTypes.INITIALIZE_VALUES, initializeValuesAction),
    takeEvery(ActionTypes.APPLY_VALUES, applyValues),
    takeLeading(ActionTypes.CONFIGURE_PERIPHERAL_ID, configurePeripheralId),
    takeLeading(ActionTypes.SET_PERIPHERAL_ID, setPeripheralId),
  ]);
}

type ActionPayload<Type extends ActionTypes> = Action<ActionsToPayloads[Type]>;

function* appLoaded({ payload: { info } }: ActionPayload<ActionTypes.APP_LOADED>) {
  try {
    const ascii: SagaReturnType<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, info.key);
    if (!info.isModifiable) { return }

    let modState: SagaReturnType<typeof ascii.settings.get>;
    if (info.axis == null) {
      modState = yield call([ascii.settings, ascii.settings.get], 'device.hw.modified');
    } else {
      modState = yield call([ascii.settings, ascii.settings.get], 'peripheral.hw.modified');
    }

    if (Boolean(modState) !== info.isModified) {
      yield put(connectionManagerActions.loadDevices(extractConnectionKey(info.key), false));
    } else {
      yield put(actions.initializeValues(info));
    }
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.errorOnInitialize(e.message));
  }
}

function* setDeviceModifiable({ payload: { info } }: ActionPayload<ActionTypes.ENABLE_MODIFICATION>) {
  try {
    const asciiDevice: SagaReturnType<typeof getDevice> = yield call(getDevice, info.key);
    const ascii: SagaReturnType<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, info.key);
    const initialAccessLevel: number = yield call([asciiDevice.settings, asciiDevice.settings.get], 'system.access');
    yield call([asciiDevice.settings, asciiDevice.settings.set], 'system.access', 2);
    if (info.axis == null) {
      yield call([ascii.settings, ascii.settings.set], 'device.hw.modified', 1);
    } else {
      yield call([ascii.settings, ascii.settings.set], 'peripheral.hw.modified', 1);
    }
    yield call([asciiDevice.settings, asciiDevice.settings.set], 'system.access', initialAccessLevel);
    yield put(connectionManagerActions.loadDevices(extractConnectionKey(info.key), false));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.errorOnEnableHwMod(e.message));
  }
}

function* initializeValuesAction({ payload: { info } }: ActionPayload<ActionTypes.INITIALIZE_VALUES>) {
  try {
    const deviceOrAxis: SagaReturnType<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, info.key);
    const values: SagaReturnType<typeof initializeValues> = yield call(initializeValues, deviceOrAxis, info);
    yield put(actions.setValues(values));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.errorOnInitialize(e.message));
  }
}

function* applyValues({ payload: { deviceKey, validFieldValues: values, info } }: ActionPayload<ActionTypes.APPLY_VALUES>) {
  const changeCount: RT<typeof selectChangeCount> = yield select(selectChangeCount);
  yield put(actions.setApplyProgress({ step: 'validating' }));
  let asciiDevice: SagaReturnType<typeof getDevice>;
  let deviceOrAxis: SagaReturnType<typeof getDeviceOrAxis>;
  try {
    asciiDevice = yield call(getDevice, deviceKey);
    deviceOrAxis = yield call(getDeviceOrAxis, deviceKey);
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.errorOnApply(topLevelErrorOnApply(`Could not access device to modify: ${err.message}`)));
    return;
  }

  let axes: Axis | AllAxes;
  if (info.axis != null) {
    axes = asciiDevice.getAxis(info.axis.axisNumber);
  } else {
    axes = asciiDevice.allAxes;
  }

  try {
    const preSetErrs: SagaReturnType<typeof runPreSetValidators> = yield call(runPreSetValidators, values, deviceOrAxis);
    if (preSetErrs != null) {
      yield put(actions.errorOnApply(preSetErrs));
      yield put(actions.setApplyProgress({ step: 'pre-validation-errors', errors: preSetErrs.fieldErrors }));
      return;
    }
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.errorOnApply(topLevelErrorOnApply(`Could not validate the device: ${e.message}`)));
    return;
  }

  let initialAccessLevel: SagaReturnType<typeof deviceOrAxis.settings.get>;
  try {
    initialAccessLevel = yield call([asciiDevice.settings, asciiDevice.settings.get], 'system.access');
    yield call([asciiDevice.settings, asciiDevice.settings.set], 'system.access', 2);
    yield call([axes, axes.driverDisable]);
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.errorOnApply(topLevelErrorOnApply(`Could not prepare device for setting application: ${err.message}`)));
    return;
  }

  yield put(actions.setApplyProgress({ step: 'applying' }));

  const applyErrors: ValidationError[] = [];
  for (const sectionValues of Object.values(values)) {
    for (const field of Object.values(sectionValues)) {
      if (!field.enabled || field.setting == null) { continue }
      try {
        yield call([deviceOrAxis.settings, deviceOrAxis.settings.set], field.setting, field.value);
        if (field.changed) {
          yield put(actions.setValueApplied(field.at.sectionId, field.at.fieldId, field.value));
        }
      } catch (e) {
        throwUnexpectedError(e);
        applyErrors.push({ ...field.at, message: e.message });
      }
    }
  }

  const appliedByCustomSetters = new Set<NonNullValue>();
  for (const setter of customSetters) {
    const update: RT<typeof setter> = yield call(setter, values, deviceOrAxis, info);
    for (const e of update.errors ?? []) { applyErrors.push(e) }
    for (const applied of update.updatesApplied ?? []) { appliedByCustomSetters.add(applied) }
  }
  for (const applied of appliedByCustomSetters) {
    if (!applied.changed) { continue }
    const associatedWithAnError = applyErrors.some(err => err.sectionId === applied.at.sectionId && err.fieldId === applied.at.fieldId);
    if (!associatedWithAnError) {
      yield put(actions.setValueApplied(applied.at.sectionId, applied.at.fieldId, applied.value));
    }
  }

  if (applyErrors.length > 0) {
    yield put(actions.errorOnApply(fieldErrorsOnApply(applyErrors)));
    yield put(actions.setApplyProgress({ step: 'errors-on-apply', total: changeCount, errors: applyErrors }));
    return;
  }

  try {
    const postSetErrs: SagaReturnType<typeof runPostSetValidators> = yield call(runPostSetValidators, values, deviceOrAxis);
    if (postSetErrs != null) {
      yield put(actions.errorOnApply(postSetErrs));
      yield put(actions.setApplyProgress({ step: 'post-validation-errors', errors: postSetErrs.fieldErrors }));
      return;
    }
  } catch (e) {
    yield put(actions.errorOnApply({
      topLevelErrors: [{
        topLevelError: `Values were applied, but some clean-up tasks threw an error: ${makeString(e)}`,
      }],
      fieldErrors: [],
    }));
  }

  try {
    yield call([asciiDevice.settings, asciiDevice.settings.set], 'system.access', initialAccessLevel);
    yield call([axes, axes.driverEnable]);

    yield call(reloadDevices, extractConnectionKey(deviceKey));

    yield put(actions.setApplyProgress({ step: 'complete' }));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.errorOnApply(topLevelErrorOnApply(`Could not revert device to operational state: ${e.message}`)));
  }
}

function* configurePeripheralId({ payload: { info } }: ActionPayload<ActionTypes.CONFIGURE_PERIPHERAL_ID>) {
  try {
    if (info.axis?.identity?.peripheralId === 0) {
      return;
    }
    const ascii: SagaReturnType<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, info.key);
    yield call([ascii.settings, ascii.settings.set], 'peripheral.id', 0);
    yield put(connectionManagerActions.loadDevices(extractConnectionKey(info.key), false));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.errorSettingPeripheralId(e.message));
  }
}

function* setPeripheralId({ payload: { info, id } }: ActionPayload<ActionTypes.SET_PERIPHERAL_ID>) {
  try {
    const ascii: SagaReturnType<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, info.key);
    yield call([ascii.settings, ascii.settings.set], 'peripheral.id', id);
    yield call([ascii.storage, ascii.storage.setBool], SETUP_COMPLETE_KEY, false);
    yield put(actions.makeDeviceModifiable(info));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.errorSettingPeripheralId(e.message));
  }
}
