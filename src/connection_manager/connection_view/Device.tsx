import React from 'react';
import {  useSelector } from 'react-redux';
import classNames from 'classnames';
import { Icons, Text } from '@zaber/react-library';

import { selectDevices } from '../selectors';
import { DeviceNumber } from '../../components';
import { EntityKey, tryExtractDeviceKey } from '../../keys';

import { PassedProperties } from './types';
import { TreeLine } from './TreeLine';
import { AttentionRequester } from './AttentionRequester';
import { Axis } from './Axis';

interface Props extends PassedProperties {
  deviceKey: EntityKey;
  isLast: boolean;
}

export const Device: React.FC<Props> = props => {
  const { deviceKey, isLast, ...passedProperties } = props;
  const { selectable, onSelect, selected, onPinClick, pinnedAxesKeys, attentionRequests, showAxes = true } = passedProperties;
  const device = useSelector(selectDevices)[deviceKey];
  const isIntegrated = !device.isMultiAxis && !device.isController;
  const firstAxis = device.axes[0];
  const isPinned = isIntegrated && pinnedAxesKeys?.includes(firstAxis);
  const onPin = (e: React.MouseEvent<unknown, MouseEvent>) => {
    e.stopPropagation();
    onPinClick?.(firstAxis);
  };
  const canSelect = selectable.includes('device') || (isIntegrated && selectable.includes('integrated'));
  const onClick = canSelect ? (() => onSelect(deviceKey)) : undefined;

  return (
    <div className={classNames({ selected: selected === deviceKey }, 'device')}>
      <div className={classNames({ selectable: canSelect }, 'device-container')} onClick={onClick}>
        <div className="device-level tree-spacer"/>
        <TreeLine connectRight connectDown={!isLast}/>
        <div className="device-name">
          <Text><DeviceNumber>{device.address}</DeviceNumber></Text>
          <Text>{device.identity ? (device.label ?? device.identity.name) : 'Unknown Device'}</Text>
          <div className="spacer"/>
          {isPinned && <Icons.Pin
            title="Pin Axis"
            onClick={onPin}
            activated={true}/>}
          <AttentionRequester entityKey={device.key} extractor={tryExtractDeviceKey} attentionRequests={attentionRequests}/>
        </div>
      </div>
      {!isIntegrated && showAxes && (
        <div className="axes">
          {device.axes.map((axisKey, i) =>
            (<Axis key={axisKey} axisKey={axisKey} isLastDevice={isLast} isLast={i === device.axes.length - 1} {...passedProperties}/>)
          )}
        </div>
      )}
    </div>
  );
};
