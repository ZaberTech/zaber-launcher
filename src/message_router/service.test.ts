import type { Container } from 'inversify';
import { Subject } from 'rxjs';
import { take } from 'rxjs/operators';

let connections: MessageRouterConnectionMock[];

class MessageRouterConnectionMock {
  constructor(public readonly url: string) {
    connections.push(this);
  }

  ensureConnected = jest.fn(() => Promise.resolve());
  close = jest.fn(() => Promise.resolve());
  errors = new Subject<Error>();
  getRoutedConnection = jest.fn(() => Promise.resolve({ connection: 'asciiConnection' }));
}

jest.mock('./connection', () => ({
  MessageRouterConnection: MessageRouterConnectionMock,
}));

import { createContainer, destroyContainer } from '../container';

import { MessageRoutersService } from './service';

let service: MessageRoutersService;
let container: Container;

beforeEach(() => {
  connections = [];

  container = createContainer();
  service = container.get<MessageRoutersService>(MessageRoutersService);
});

afterEach(() => {
  connections = null!;

  service = null!;
  destroyContainer();
  container = null!;
});

test('returns same connection for the same url opening it once', async () => {
  const connection1 = await service.getConnection('url1');
  const connection2 = await service.getConnection('url1');

  expect(connections).toHaveLength(1);
  expect(connections[0].ensureConnected).toHaveBeenCalledTimes(2);

  expect(connection1 === connection2).toBeTruthy();
});

test('returns different connection for different urls', async () => {
  const connection1 = await service.getConnection('url1');
  const connection2 = await service.getConnection('url2');

  expect(connections).toHaveLength(2);
  expect(connections[0].ensureConnected).toHaveBeenCalledTimes(1);
  expect(connections[1].ensureConnected).toHaveBeenCalledTimes(1);

  expect(connection1 !== connection2).toBeTruthy();
});

test('removes connection on error returning new instance when requested again', async () => {
  const connection1 = await service.getConnection('url1');

  expect(connections).toHaveLength(1);
  connections[0].errors.next(new Error('Failed'));
  expect(connections[0].close).toHaveBeenCalledTimes(1);

  const connection2 = await service.getConnection('url1');
  expect(connections).toHaveLength(2);

  expect(connection1 !== connection2).toBeTruthy();
});

test('getAsciiConnection calls getRoutedConnection of the router connection and returns ascii connection', async () => {
  const asciiConnection = await service.getAsciiConnection('url1', 'id1');

  expect(connections).toHaveLength(1);
  expect(connections[0].getRoutedConnection).toHaveBeenCalled();

  expect(asciiConnection).toBe('asciiConnection');
});

test('getRoutedConnection calls getRoutedConnection of the router connection', async () => {
  const routedConnection = await service.getRoutedConnection('url1', 'id1');

  expect(connections).toHaveLength(1);
  expect(connections[0].getRoutedConnection).toHaveBeenCalled();

  expect(routedConnection).toBeTruthy();
});

test('dropConnection removes and closes the connection', async () => {
  const connection = await service.getConnection('url1');

  service.dropConnection('url1');
  expect(connection.close).toHaveBeenCalled();

  const newConnection = await service.getConnection('url1');
  expect(newConnection).not.toBe(connection);
});

test('emits new connection after creation', async () => {
  const emittedConnection = service.newConnections.pipe(take(1)).toPromise();

  const connection = await service.getConnection('url1');

  expect(await emittedConnection).toBe(connection);
});
