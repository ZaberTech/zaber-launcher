import React from 'react';

import type { EntityKey } from '../keys';
import { NoContentMessage } from '../components';

import { ProtocolManualFormatter } from './ProtocolManualFormatter';
import { useSettingManual } from './hooks';

type Props = {
  deviceOrAxisKey: EntityKey | null | undefined;
  setting: string;
  /** true if the manual should be hidden if there are no docs found. By default, shows a generic "No Docs" message. */
  hideWhenNoDocs?: boolean;
};

export const ProtocolManual: React.FC<Props> = ({ deviceOrAxisKey, setting, hideWhenNoDocs }) => {
  const manual = useSettingManual(deviceOrAxisKey, setting);

  if (manual === undefined) {
    return <NoContentMessage message="Preparing Documentation..." type="working"/>;
  }
  if ('error' in manual) {
    if (hideWhenNoDocs) {
      return null;
    } else {
      return <NoContentMessage type="error" bannerTitle="No Documentation" message={manual.error}/>;
    }
  }

  return <ProtocolManualFormatter html={manual.html}/>;
};
