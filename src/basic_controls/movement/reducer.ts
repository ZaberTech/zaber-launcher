import _ from 'lodash';
import { Units } from '@zaber/motion';

import { changeDictionary, createReducer, tryAccess } from '../../utils';
import { EntityKey, extractConnectionKey, removeWithKey } from '../../keys';
import {
  ConnectionManagerActionPayloads,
  ConnectionManagerActionTypes,
  DevicesLoadedPayload, IdentifiedAxisInfo, isIdentified
} from '../../connection_manager';
import type { ConnectionStatusActionPayloads } from '../../connection_status';
import { isMobile } from '../../devices';
import { dimensionsByName, Measurement } from '../../units';
import { moveableKey } from '../types';

import { ActionsToPayloads, ActionTypes } from './actions';
import { KeyPressCombo, MoveSinState, type AxisMovementState } from './types';

function defaultAxisPosUnit(axis: IdentifiedAxisInfo): Units {
  const posInfo = axis.product.settings.rows.find(row => row.name === 'pos');
  const conversionInfo = axis.product.conversionTable.rows.find(row => row.contextual_dimension_id === posInfo?.contextual_dimension_id);
  return tryAccess(dimensionsByName, conversionInfo?.dimension_name)?.defaultUnit as Units ?? Units.NATIVE;
}

function defaultAxisState(axis: IdentifiedAxisInfo): AxisMovementState {
  return {
    key: axis.key,
    lastCommand: null,
    previousCommand: null,
    posUnit: defaultAxisPosUnit(axis),

    preciseMovement: {
      absolute: null,
      relative: null,
      velocity: null,
    },
    preciseMovementExpanded: false,
    shortcut: KeyPressCombo.None,
    moveSinState: initialMoveSinState,
  };
}

export interface State {
  axes: Record<EntityKey, AxisMovementState>;
}

const initialState: State = {
  axes: {},
};

const defaultMeasurement: Measurement = { value: null, units: 'default' };

const initialMoveSinState: MoveSinState = {
  amplitude: defaultMeasurement,
  period: defaultMeasurement,
  count: null,
};

const issueCommand = (state: State, { moveable, command }: ActionsToPayloads[ActionTypes.ISSUE_COMMAND]): State => {
  const key = moveableKey(moveable);
  return ({
    ...state,
    axes: changeDictionary(state.axes, key, axis => ({
      ...axis,
      previousCommand: axis.lastCommand,
      lastCommand: command,
    })),
  });
};

const commandComplete = (state: State, { axisKey }: ActionsToPayloads[ActionTypes.COMMAND_COMPLETE]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axisKey, axis => ({
    ...axis,
    previousCommand: axis.lastCommand,
    lastCommand: null,
  })),
});

const setUnits = (state: State, { axisKey, units }: ActionsToPayloads[ActionTypes.SET_POS_UNITS]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axisKey, axis => ({ ...axis, posUnit: units })),
});

const expandPreciseMovement = (state: State, { axisKey, expanded }: ActionsToPayloads[ActionTypes.EXPAND_PRECISE_MOVEMENT]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axisKey, axis => ({ ...axis, preciseMovementExpanded: expanded })),
});

const setPreciseMovement = (state: State, { axisKey, movement, value }: ActionsToPayloads[ActionTypes.SET_PRECISE_MOVEMENT]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axisKey, axis => ({ ...axis, preciseMovement: { ...axis.preciseMovement, [movement]: value } })),
});

const setKeyboardShortcut = (state: State, { axisKey, keyPress }: ActionsToPayloads[ActionTypes.SET_KEYBOARD_SHORTCUT]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axisKey, axis => ({ ...axis, shortcut: keyPress }))
});

const devicesLoaded = (state: State, { devices, connectionKey }: DevicesLoadedPayload): State => {
  const identified = devices.filter(isIdentified);
  return ({
    ...state,
    axes: {
      ...removeWithKey(state.axes, extractConnectionKey, connectionKey),
      ..._.keyBy(
        _.flatMap(identified, device => device.axes
          .filter(axis => isMobile(axis))
          .map(axis => defaultAxisState(axis))
        ),
        'key'
      ),
    }
  });
};

const setMoveSinState = (state: State, { axisKey, moveSinState }: ActionsToPayloads[ActionTypes.SET_MOVE_SIN_STATE]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axisKey, axis => ({ ...axis, moveSinState: { ...axis.moveSinState, ...moveSinState } })),
});

type CombinedPayloads = ActionsToPayloads & ConnectionManagerActionPayloads & ConnectionStatusActionPayloads;
export const reducer = createReducer<CombinedPayloads, typeof initialState>({
  [ActionTypes.ISSUE_COMMAND]: issueCommand,
  [ActionTypes.COMMAND_COMPLETE]: commandComplete,
  [ActionTypes.SET_POS_UNITS]: setUnits,

  [ActionTypes.EXPAND_PRECISE_MOVEMENT]: expandPreciseMovement,
  [ActionTypes.SET_PRECISE_MOVEMENT]: setPreciseMovement,
  [ActionTypes.SET_KEYBOARD_SHORTCUT]: setKeyboardShortcut,

  [ActionTypes.SET_MOVE_SIN_STATE]: setMoveSinState,

  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
}, initialState);
