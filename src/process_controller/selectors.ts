import { tryAccess } from '@zaber/toolbox';
import { createSelector } from 'reselect';

import { selectIdentifiedDevices, selectIdentifiedAxes } from '../connection_manager';
import { selectProcessController } from '../store';
import { makeAxisKey } from '../keys';

import type { SettingsMap } from './settings';
import { PROCESS_NUMBERS, type ControllerInfo } from './types';

export const selectEntity = createSelector(selectProcessController, state => state.entity);

export const selectControllerInfo = createSelector(selectProcessController, state => state.info);
export const selectControllerInfoOrNull = createSelector(selectControllerInfo, info => info === 'not-process-controller' ? null : info);
export const selectChosenProcess = createSelector(selectProcessController, state => state.process);
export const selectProcesses = createSelector(selectProcessController, state => state.processes);
export const selectProcess = createSelector(selectChosenProcess, selectProcesses, (process, processes) => {
  if (process == null) { return null }
  return processes[process];
});
export const selectProcessSettings = createSelector(selectProcess, (info): SettingsMap => info?.settings ?? {});
export const selectProcessState = createSelector(
  selectControllerInfo, selectChosenProcess, (info, process): ControllerInfo['states'][number] => {
    if (info == null || info === 'not-process-controller' || process == null) { return { on: false, reading: true } }
    return info.states[process];
  }
);
/** Only allow controls to be edited when the process is off */
export const selectProcessOn = createSelector(selectProcessState, state => state.on || state.reading);
export const selectProcessFaults = createSelector(selectControllerInfo, selectChosenProcess, (info, process) => {
  if (info == null || info === 'not-process-controller' || process == null) { return null }
  return info.faults[process] ?? null;
});

export const selectProduct = createSelector(selectProcessController, selectIdentifiedDevices, (state, devices) => {
  const { entity } = state;
  if (entity == null) {
    return null;
  }
  return devices[entity] ?? null;
});

export const selectReadings = createSelector(selectProcessController, state => state.readings);

export const selectError = createSelector(selectProcessController, state => state.error);

export const selectAutoTune = createSelector(selectProcessController, state => state.autoTune);
export const selectAutoTuneData = createSelector(selectAutoTune, tune => tune?.readings ?? []);

export const selectProcessLabels = createSelector(selectProcessController, selectControllerInfoOrNull, selectIdentifiedAxes,
  ({ entity }, info, axes) => Object.fromEntries(PROCESS_NUMBERS.map(process => {
    const axis = tryAccess(axes, entity && makeAxisKey(entity, process));
    const bridged = info?.bridges[process].on;
    let label: string[];
    if (axis?.label) {
      const schema = bridged ? `CH${process} + CH${process + 1}` : `CH${process}`;
      label = [axis.label, `(${schema})`];
    } else {
      label = [bridged ? `Bridge ${process} + ${process + 1}` : `Channel ${process}`];
    }
    return [process, label];
  })));

const DEFAULT_NAME = 'X-SCA4 Driver';
export const selectControllerLabel = createSelector(selectProcessController, selectIdentifiedDevices, (state, devices) => {
  const label = tryAccess(devices, state.entity)?.label;
  return label ? `${label} (${DEFAULT_NAME})` : DEFAULT_NAME;
});
