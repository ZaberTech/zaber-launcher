import _ from 'lodash';
import { CircularBuffer } from 'mnemonist';
import { settingsMetadata } from '@zaber/device-db-metadata';

import { environment } from '../environment';
import { FW_TICK_RATE, TIME_CONSTANT } from '../units';

import {
  Channel,
  CHANNEL_DEFAULTS,
  ChannelMap,
  MathChannelNames,
  StorageBuffer,
  RecordingBuffer,
  Sample,
  YAxisId,
} from './types';
import { UNSET_COLOR } from './colors';
import { derivative, difference, threePhaseCurrent, vectorLength } from './math';
import { MATH_BUFFER_SIZE } from './sagas';


const ACCEL_CONSTANT = 1 / FW_TICK_RATE * TIME_CONSTANT;

export const MostUsedSettings = [
  'pos', 'encoder.pos', 'encoder.count', 'encoder.pos.error', 'vel', 'encoder.vel', 'driver.temperature', 'driver.i2t.measured',
  'io.di.port', 'io.do.port', 'limit.home.state', 'limit.away.state', 'limit.c.state', 'motion.busy', 'motor.i2t.measured',
  'system.temperature', 'system.voltage',
  'lamp.flux', 'lamp.temperature',
  'process.voltage', 'process.current',
];

export const DefaultToRightAxis = new Set([MathChannelNames.FOLLOW_ERR, MathChannelNames.FOLLOW_ERR_VEL, MathChannelNames.FOLLOW_ERR_ACCEL,
  'encoder.pos.error',
]);

export const sortByRelevance = (a: string, b: string) => MostUsedSettings.indexOf(a) - MostUsedSettings.indexOf(b);
export const sortAlphabetic = (a: string, b: string) => a.localeCompare(b);


export const ID_DELIM = '|';
export type ChannelTypePrefix = 'setting' | 'math' | 'io';

export const makeChannelId =
  (type: ChannelTypePrefix, name: string, axis: number) => `${type}${ID_DELIM}${name}${ID_DELIM}${axis}`;
export const makeChannelIdWithoutAxis = (type: ChannelTypePrefix, name: string) => `${type}${ID_DELIM}${name}`;
export const makeSettingChannelId = (name: string, axis: number) => makeChannelId('setting', name, axis);
export const makeMathChannelId = (name: string, axis: number) => makeChannelId('math', name, axis);
export const makeIoChannelId = (name: string, pin: number) => makeChannelId('io', name, pin);
export function splitChannelId(id: string): { type: ChannelTypePrefix; name: string; axis?: string } {
  const parts = id.split(ID_DELIM);
  if (parts[0] !== 'setting' && parts[0] !== 'math' && parts[0] !== 'io') {
    throw new Error(`Invalid channel type prefix on ID ${id}`);
  }

  return {
    type: parts[0] as ChannelTypePrefix,
    name: parts[1],
    axis: parts[2],
  };
}

export function changeChannelIdAxis(id: string, axis: number): string {
  const { type, name } = splitChannelId(id);
  return makeChannelId(type, name, axis);
}

const SETTING_PREFIX = `setting${ID_DELIM}`;
export const isSettingChannelId = (id: string): boolean => id.startsWith(SETTING_PREFIX);
export const isSettingChannel = (channel: Channel): boolean => isSettingChannelId(channel.id);

const MATH_PREFIX = `math${ID_DELIM}`;
export const isMathChannelId = (id: string): boolean => id.startsWith(MATH_PREFIX);
export const isMathChannel = (channel: Channel): boolean => isMathChannelId(channel.id);

const IO_PREFIX = `io${ID_DELIM}`;
export const isIoChannelId = (id: string): boolean => id.startsWith(IO_PREFIX);
export const isIoChannel = (channel: Channel): boolean => isIoChannelId(channel.id);


export const MATH_CHANNELS: Pick<Channel, 'name' | 'dependencyIds' | 'title' | 'subtitle' | 'mathFunc' | 'prescale'>[] = [{
  name: MathChannelNames.TRAJECTORY_VEL,
  dependencyIds: [makeChannelIdWithoutAxis('setting', 'pos')],
  mathFunc: derivative,
  prescale: TIME_CONSTANT,
  title: 'Trajectory Velocity',
  subtitle: 'd(pos)/dt',
}, {
  name: MathChannelNames.TRAJECTORY_ACCEL,
  dependencyIds: [makeChannelIdWithoutAxis('math', MathChannelNames.TRAJECTORY_VEL)],
  mathFunc: derivative,
  prescale: ACCEL_CONSTANT,
  title: 'Trajectory Acceleration',
  subtitle: 'd²(pos)/dt²',
}, {
  name: MathChannelNames.MEASURED_VEL,
  dependencyIds: [makeChannelIdWithoutAxis('setting', 'encoder.pos')],
  mathFunc: derivative,
  prescale: TIME_CONSTANT,
  title: 'Measured Velocity',
  subtitle: 'd(encoder.pos)/dt',
}, {
  name: MathChannelNames.MEASURED_ACCEL,
  dependencyIds: [makeChannelIdWithoutAxis('math', MathChannelNames.MEASURED_VEL)],
  mathFunc: derivative,
  prescale: ACCEL_CONSTANT,
  title: 'Measured Acceleration',
  subtitle: 'd²(encoder.pos)/dt²',
}, {
  name: MathChannelNames.FOLLOW_ERR,
  dependencyIds: [makeChannelIdWithoutAxis('setting', 'encoder.pos'), makeChannelIdWithoutAxis('setting', 'pos')],
  mathFunc: difference,
  prescale: 1.0,
  title: 'Position Following Error',
  subtitle: 'encoder.pos - pos',
}, {
  name: MathChannelNames.FOLLOW_ERR_VEL,
  mathFunc: difference,
  prescale: TIME_CONSTANT,
  dependencyIds: [
    makeChannelIdWithoutAxis('math', MathChannelNames.MEASURED_VEL),
    makeChannelIdWithoutAxis('math', MathChannelNames.TRAJECTORY_VEL)],
  title: 'Velocity Following Error',
  subtitle: 'd(encoder.pos)/dt - d(pos)/dt',
}, {
  name: MathChannelNames.FOLLOW_ERR_ACCEL,
  mathFunc: difference,
  prescale: ACCEL_CONSTANT,
  dependencyIds: [
    makeChannelIdWithoutAxis('math', MathChannelNames.MEASURED_ACCEL),
    makeChannelIdWithoutAxis('math', MathChannelNames.TRAJECTORY_ACCEL)],
  title: 'Acceleration Following Error',
  subtitle: 'd²(encoder.pos)/dt² - d²(pos)/dt²',
}, {
  name: MathChannelNames.ENCODER_SIGNAL_STRENGTH,
  mathFunc: vectorLength,
  prescale: 1.0,
  dependencyIds: [
    makeChannelIdWithoutAxis('setting', 'encoder.2.sin'),
    makeChannelIdWithoutAxis('setting', 'encoder.2.cos')],
  title: 'Encoder Signal Strength',
  subtitle: '√(encoder.2.sin² + encoder.2.cos²)',
}, {
  name: MathChannelNames.MOTOR_CURRENT_3PHASE,
  mathFunc: threePhaseCurrent,
  prescale: 1.0,
  dependencyIds: [
    makeChannelIdWithoutAxis('setting', '_driver.current.a.meas'),
    makeChannelIdWithoutAxis('setting', '_driver.current.b.meas')],
  title: 'Three-Phase Motor Current',
  subtitle: 'max(abs(_driver.current.a.meas), abs(_driver.current.b.meas), abs(_driver.current.a.meas + _driver.current.b.meas))',
}];

const POSITION_DIMENSION_NAMES = ['Length', 'Angle'];
const VELOCITY_DIMENSION_NAMES = ['Velocity', 'Angular Velocity'];
const ACCELERATION_DIMENSION_NAMES = ['Acceleration', 'Angular Acceleration'];

export const MATH_DIMENSIONS: Record<string, string[]> = {
  [MathChannelNames.TRAJECTORY_VEL]: VELOCITY_DIMENSION_NAMES,
  [MathChannelNames.TRAJECTORY_ACCEL]: ACCELERATION_DIMENSION_NAMES,
  [MathChannelNames.MEASURED_VEL]: VELOCITY_DIMENSION_NAMES,
  [MathChannelNames.MEASURED_ACCEL]: ACCELERATION_DIMENSION_NAMES,
  [MathChannelNames.FOLLOW_ERR]: POSITION_DIMENSION_NAMES,
  [MathChannelNames.FOLLOW_ERR_VEL]: VELOCITY_DIMENSION_NAMES,
  [MathChannelNames.FOLLOW_ERR_ACCEL]: ACCELERATION_DIMENSION_NAMES,
  [MathChannelNames.ENCODER_SIGNAL_STRENGTH]: ['Voltage'],
  [MathChannelNames.MOTOR_CURRENT_3PHASE]: ['Current'],
};

export const MATH_DIMENSION_CATEGORIES: Record<string, string> = {
  [MathChannelNames.TRAJECTORY_VEL]: 'Velocity',
  [MathChannelNames.TRAJECTORY_ACCEL]: 'Acceleration',
  [MathChannelNames.MEASURED_VEL]: 'Velocity',
  [MathChannelNames.MEASURED_ACCEL]: 'Acceleration',
  [MathChannelNames.FOLLOW_ERR]: 'Position',
  [MathChannelNames.FOLLOW_ERR_VEL]: 'Velocity',
  [MathChannelNames.FOLLOW_ERR_ACCEL]: 'Acceleration',
  [MathChannelNames.ENCODER_SIGNAL_STRENGTH]: 'Voltage',
  [MathChannelNames.MOTOR_CURRENT_3PHASE]: 'Current',
};


export const createChannelForSetting = (settingName: string, axis: number): Channel => ({
  ..._.cloneDeep(CHANNEL_DEFAULTS),
  id: makeSettingChannelId(settingName, axis),
  name: settingName,
  axis,
  title: settingsMetadata[settingName]?.humanName ?? settingName,
  subtitle: settingName,
  color: UNSET_COLOR,
  showInUi: true,
  yAxis: DefaultToRightAxis.has(settingName) ? YAxisId.RIGHT : YAxisId.LEFT,
});


export const createChannelForIo = (portType: string, pin: number): Channel => ({
  ..._.cloneDeep(CHANNEL_DEFAULTS),
  id: makeIoChannelId(portType, pin),
  name: `${portType} ${pin}`,
  axis: 0,
  title: `${portType.toUpperCase()} - Pin ${pin}`,
  subtitle: '',
  color: UNSET_COLOR,
  showInUi: true,
  yAxis: YAxisId.LEFT,
});


// Sorts channels in preferred display order: User-selected settings
// first in the order they were selected, then math channels in
// dependency order first then in the order defined in the table above.
export function channelSorter(a: Channel, b: Channel) {
  if (a.dependencyIds.length !== b.dependencyIds.length) {
    return a.dependencyIds.length - b.dependencyIds.length;
  }

  if (a.dependencyIds.length === 0) {
    // Note this relies on array.sort() being stable, which it recently is supposed to be.
    return 0;
  }

  const indexDiff = MATH_CHANNELS.findIndex(channel => channel.name === a.name)
    - MATH_CHANNELS.findIndex(channel => channel.name === b.name);
  if (indexDiff !== 0) {
    return indexDiff;
  }

  return a.axis - b.axis;
}


export function addChannelAndDependents(oldChannels: Channel[], newSetting: string, axis: number): Channel[] {
  if (_.some(oldChannels, ch => ch.id === makeSettingChannelId(newSetting, axis))) {
    return oldChannels;
  }

  // Add a channel for the user-selected setting if not already present.
  const result = [...oldChannels, createChannelForSetting(newSetting, axis)];
  return addAllPossibleMathChannels(result);
}


export function addOrReplaceChannelAndUpdateDependents(oldChannels: Channel[], newChannel: Channel): Channel[] {
  let append = true;
  const result = oldChannels.map(ch => {
    if (ch.id === newChannel.id) {
      append = false;
      return newChannel;
    } else {
      return ch;
    }
  });

  if (append) {
    result.push(newChannel);
  }

  return addAllPossibleMathChannels(result);
}


export const addAllPossibleMathChannels = function(channels: Channel[]): Channel[] {
  const axes = Array.from(new Set(channels.map(ch => ch.axis))).sort();
  const idsPerAxis: Record<number, Set<string>> = Object.assign({},
    ...axes.map(axis => ({ [axis]: new Set(channels.filter(ch => ch.axis === axis).map(ch => ch.id)) })));

  // Add all possible math channels resulting from the new addition.
  let stuffAdded = true;
  while (stuffAdded) {
    stuffAdded = false;
    for (const item of MATH_CHANNELS) {
      for (const axis of axes) {
        const newChannelId = makeMathChannelId(item.name, axis);
        if (!idsPerAxis[axis].has(newChannelId)) {
          if (item.dependencyIds.every(dId => idsPerAxis[axis].has(changeChannelIdAxis(dId, axis)))) {
            channels.push({
              ...CHANNEL_DEFAULTS,
              ...item,
              color: UNSET_COLOR,
              id: newChannelId,
              axis,
              yAxis: DefaultToRightAxis.has(item.name) ? YAxisId.RIGHT : YAxisId.LEFT,
            });

            idsPerAxis[axis].add(newChannelId);
            stuffAdded = true;
          }
        }
      }
    }
  }

  channels.sort(channelSorter);

  return channels;
};


export const removeChannelAndDependents = function(channels: Channel[], idToRemove: string): Channel[] {
  const map = _.keyBy(channels, 'id');
  return channels.filter(ch => ch.id !== idToRemove && !channelHasDependency(ch, idToRemove, map));
};


export const channelHasDependency = function(channel: Channel, dependencyId: string, channelMap: ChannelMap): boolean {
  if (!channel) {
    return false;
  }

  for (const dId of channel.dependencyIds) {
    if (dependencyId === changeChannelIdAxis(dId, channel.axis)) {
      return true;
    }
  }

  return channel.dependencyIds.some(
    dId => channelHasDependency(channelMap[changeChannelIdAxis(dId, channel.axis)], dependencyId, channelMap));
};


export const getChannelRootDependencies = function(channel: Channel, channelMap: ChannelMap): string[] {
  if (!channel) {
    return [];
  }

  if (channel.dependencyIds.length === 0) {
    return [channel.id];
  }

  return _.uniq(_.flatten(
    channel.dependencyIds.map(dId => getChannelRootDependencies(channelMap[changeChannelIdAxis(dId, channel.axis)], channelMap))));
};


export const getAllChannelDependencies = function(channel: Channel, channelMap: ChannelMap): Set<string> {
  let result = new Set<string>([channel.id]);
  for (const name of channel.dependencyIds) {
    const child = channelMap[changeChannelIdAxis(name, channel.axis)];
    if (child) {
      result = new Set([...result, ...getAllChannelDependencies(child, channelMap)]);
    }
  }

  return result;
};


export const getMathChannelSubtitle = function(name: string): string | null {
  const definition = MATH_CHANNELS.find(item => item.name === name);
  return definition?.subtitle ?? null;
};


export const extractSettingsChannels = (input: Channel[]): Channel[] => input.filter(isSettingChannel);
export const extractMathChannels = (input: Channel[]): Channel[] => input.filter(isMathChannel);
export const extractIoChannels = (input: Channel[]): Channel[] => input.filter(isIoChannel);
export const extractAxisChannels = (input: Channel[], axis: number): Channel[] => input.filter(ch => ch.axis === axis);

export function extractMathChannelsToCalculateForAllAxes(channels: Channel[]): Channel[] {
  const allMathChannels = extractMathChannels(channels);
  let mathChannelsToUpdate: Channel[] = [];
  for (const axisNumber of _.uniq(allMathChannels.map(ch => ch.axis))) {
    const axisChannels = extractAxisChannels(allMathChannels, axisNumber);
    const channelMap = _.keyBy(axisChannels, 'id');
    const axisMathChannels = extractMathChannelsToCalculate(axisChannels, channelMap);
    mathChannelsToUpdate = mathChannelsToUpdate.concat(axisMathChannels);
  }

  return mathChannelsToUpdate;
}

export function extractMathChannelsToCalculate(mathChannels: Channel[], channelMap: ChannelMap): Channel[] {
  // Only compute visible math channels and their dependencies.
  const visibleMathChannels = mathChannels.filter(ch => ch.showInChart && ch.showInUi);
  let channelsToEvaluate = new Set<string>(visibleMathChannels.map(ch => ch.id));
  for (const channel of visibleMathChannels) {
    channelsToEvaluate = new Set<string>([...channelsToEvaluate, ...getAllChannelDependencies(channel, channelMap)]);
  }

  const result = extractMathChannels(mathChannels).filter(ch => channelsToEvaluate.has(ch.id));
  if (!environment.isProduction) {
    for (const channel of result) {
      if (!channel.mathFunc) {
        throw new Error(`Math channel ${channel.id} is visible or a dependency of a visible channel but has no math function.`);
      }
    }
  }

  return result;
}


export const createEmptyRecordingForChannels = function(channels: Channel[], capacity?: number): RecordingBuffer {
  if (!capacity) {
    capacity = _.max(channels.map(ch => ch.samples.length)) ?? 0;
  }

  return _.reduce(channels, (dict, channel) =>
    ({ ...dict, [channel.id]: new CircularBuffer<Sample>(Array, capacity!) }),
    {} as RecordingBuffer);
};


// Incremental - adds one new calculated sample to each specified math channel after one new
// setting sample has been added to each setting channel.
export function updateMathChannels(channelsToUpdate: Channel[], inputs: RecordingBuffer, window: number, outputs: RecordingBuffer): void {
  const linearizedInputs: StorageBuffer = {};
  for (const key in inputs) {
    linearizedInputs[key] = inputs[key].toArray() as Sample[];
  }

  for (const channel of channelsToUpdate) {
    const updateFunc = channel.mathFunc!;
    const newData = updateFunc(linearizedInputs, channel.dependencyIds.map(dId => changeChannelIdAxis(dId, channel.axis)), window);
    if (newData) {
      const key = channel.id;
      outputs[key].push(newData);
      inputs[key].push(newData); // For use by dependent math channels.
      linearizedInputs[key] = inputs[key].toArray() as Sample[];
    }
  }
}


// Bulk fill - completely fills in all math channels needed for rendering, assuming all
// sample channels are complete. Returns all data in circular buffers.
// Any math channels that already have data are left unmodified - meaning partially filled
// ones may cause errors. Only fills samples - chart data must be done in a saga.
export function fillInMathRecordings(channels: Channel[], window: number): RecordingBuffer {
  const mathChannelsToUpdate = extractMathChannelsToCalculateForAllAxes(channels)
    .filter(ch => ch.samples.length === 0); // Some may already have data.
  const bufferSize = _.max(channels.map(ch => ch.samples.length)) ?? 0;
  const allData = _.reduce(channels, (dict, channel) =>
    ({ ...dict, [channel.id]: CircularBuffer.from(channel.samples, Array, bufferSize) }),
    {} as RecordingBuffer);
  const mathData = createEmptyRecordingForChannels(channels, MATH_BUFFER_SIZE);

  for (let i = 0; i < bufferSize; i++) {
    for (const key in allData) {
      if (allData[key].size > i) {
        mathData[key].push(allData[key].get(i)!);
      }
    }

    updateMathChannels(mathChannelsToUpdate, mathData, window, allData);
  }

  return allData;
}


// Bulk fill - completely fills in all math channels needed for rendering, assuming all
// sample channels are complete. Populates the sample data if needed, but not the chart data.
export function fillInMathAllChannelSamples(channels: Channel[], window: number): Channel[] {
  const newData = fillInMathRecordings(channels, window);
  return channels.map(ch => ({
    ...ch,
    samples: ch.id in newData ? newData[ch.id].toArray() as Sample[] : ch.samples,
  }));
}


export function findNearestSampleIndex(data: Sample[], time: number): number {
  const maxIndex = _.sortedIndexBy(data, { time, value: 0 }, sample => sample.time);
  if (maxIndex > 0) {
    if (maxIndex >= data.length) {
      return maxIndex - 1;
    }

    if (time - data[maxIndex - 1].time < data[maxIndex].time - time) {
      return maxIndex - 1;
    }
  }

  return maxIndex;
}


export function numFwChannelsUsed(channels: Channel[]): number {
  let count = 0;
  let diSeen = false;
  let doSeen = false;

  for (const channel of channels) {
    if (isSettingChannel(channel)) {
      count++;
    } else if (isIoChannel(channel)) {
      const { name: portType } = splitChannelId(channel.id);
      // Because recording I/O is only supported with FW 7.33+, we can assume
      // the existence of the io.[di,do].port gettings because they were
      // introduced in FW 7.22.
      if (portType === 'di' && !diSeen) {
        diSeen = true;
        count++;
      } else if (portType === 'do' && !doSeen) {
        doSeen = true;
        count++;
      } else if (portType.startsWith('a')) {
        count++;
      }
    }
  }

  return count;
}
