import React from 'react';
import classNames from 'classnames';

export interface Props {
  persistent: boolean;
  leftPosition: number;
  children: React.ReactNode;
}

export const TrackHoverInfo: React.FC<Props> = ({ persistent, leftPosition, children }) => (
  <div className={classNames('hover-info', { persistent })} style={{ left: leftPosition }}>
    <div className="above-track">
      <div>{children}</div>
      <svg viewBox="0 0 60 100">
        <path d="M 30 0 v 100" strokeDasharray="10,10"/>
        <path d="M30 100 l -30 -50 l 60 0 l -30 50"/>
      </svg>
    </div>
    <svg className="inside-track" viewBox="0 0 60 100">
      <path d="M30 0 v100" strokeDasharray="10,10"/>
    </svg>
    <svg className="below-track" viewBox="0 0 60 100">
      <path d="M30 0 l -30 50 l 60 0 l -30 -50"/>
    </svg>
  </div>
);
