import { derivative, difference, threePhaseCurrent, vectorLength } from './math';


describe('Derivative operator', () => {
  it('returns nothing if there are less than two sample points', () => {
    let result = derivative({
      x: [],
      dx: [],
    }, ['x'], 1);

    expect(result).toBeNull();

    result = derivative({
      x: [{ time: 0, value: 0 }],
      dx: [],
    }, ['x'], 1);

    expect(result).toBeNull();
  });


  it('returns best effort if the sample point spacing is smaller than the window', () => {
    const result = derivative({
      x: [{ time: 0, value: 0 }, { time: 1, value: 1 }],
      dx: [],
    }, ['x'], 10);

    expect(result).toEqual({ time: 0.5, value: 1000 });
  });


  it('stores time scaled difference at the midpoint time, not the middle sample time', () => {
    const result = derivative({
      x: [{ time: 0, value: 0 }, { time: 50, value: 1 }, { time: 200, value: 2 }],
      dx: [],
    }, ['x'], 200);

    expect(result).toEqual({ time: 100, value: 10 });
  });
});


describe('Difference operator', () => {
  it('returns nothing if there are not enough samples', () => {
    const result = difference({
      x: [{ time: 0, value: 0 }],
      y: [{ time: 100, value: 0 }],
      d: [],
    }, ['x', 'y']);

    expect(result).toBeNull();
  });


  it('straight subtracts if data points are in lockstep', () => {
    const result = difference({
      x: [{ time: 0, value: 0 }, { time: 100, value: 1 }],
      y: [{ time: 0, value: 1 }, { time: 100, value: 0 }],
      d: [],
    }, ['x', 'y']);

    expect(result).toEqual({ time: 100, value: 1 });
  });


  it('interpolates if data points are not in lockstep', () => {
    const data = {
      x: [{ time: 0, value: 0 }, { time: 200, value: 1 }],
      y: [{ time: 100, value: 0 }],
      d: [],
    };

    let result = difference(data, ['x', 'y']);
    expect(result).toEqual({ time: 100, value: 0.5 });

    result = difference(data, ['y', 'x']);
    expect(result).toEqual({ time: 100, value: -0.5 });
  });
});


describe('Vector length operator', () => {
  it('calculates expected values', () => {
    const result = vectorLength({
      x: [{ time: 0, value: -4 }],
      y: [{ time: 0, value: 3 }],
      d: [],
    }, ['x', 'y']);

    expect(result).toEqual({ time: 0, value: 5 });
  });
});


describe('Three-Phase current operator', () => {
  it('calculates expected values', () => {
    let result = threePhaseCurrent({
      x: [{ time: 0, value: -4 }],
      y: [{ time: 0, value: 3 }],
      d: [],
    }, ['x', 'y']);

    expect(result).toEqual({ time: 0, value: 4 });

    result = threePhaseCurrent({
      x: [{ time: 0, value: -4 }],
      y: [{ time: 0, value: 5 }],
      d: [],
    }, ['x', 'y']);

    expect(result).toEqual({ time: 0, value: 5 });

    result = threePhaseCurrent({
      x: [{ time: 0, value: 4 }],
      y: [{ time: 0, value: 2 }],
      d: [],
    }, ['x', 'y']);

    expect(result).toEqual({ time: 0, value: 6 });
  });
});
