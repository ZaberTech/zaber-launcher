import { Units } from '@zaber/motion';

import type { EntityKey } from '../keys';

export type { DimensionDefinition, UnitDefinition } from '../app_components';

export type UnitConvert = {
  /** The name of the dimension from device DB */
  dimension: string;
  /** The default units to use if another isn't specified */
  default: Units;
  /** Default units per native unit */
  conversion: number | null;
  /** The decimal points of precision used by native units */
  precision: number;
};

export type UnitFromSetting = { setting: string; entity: EntityKey | null };
export type UnitFromCommand = { command: string; entity: EntityKey | null };
export type UnitFrom = UnitFromSetting | UnitFromCommand | { dimension: string } | { info: UnitInfo };

export function titleOfUnitFrom(from: UnitFrom) {
  if ('setting' in from) {
    return from.setting;
  } else if ('command' in from) {
    return from.command;
  } else if ('dimension' in from) {
    return from.dimension;
  } else if (from.info.supported) {
    return from.info.dimension;
  }
  return 'Undefined';
}

export type UnitInfo = {
  supported: false;
  default: typeof Units.NATIVE;
  conversion: null;
  precision: number;
} | UnitConvert & {
  supported: true;
};

/** Create a UnitInfo object for a settings with no unit info and optionally a precision if that is know  */
export function unsupportedUnitInfo(precision?: number): UnitInfo {
  return { supported: false, default: Units.NATIVE, conversion: null, precision: precision ?? 0 };
}

export interface Measurement {
  value: number | null;
  units?: Units | 'default' | 'readable';
}

export interface MeasurementWithUnit {
  value: number | null;
  units: Units;
}

export function hasUnits(measure: Measurement | null): measure is MeasurementWithUnit {
  return measure?.units != null && measure.units !== 'default' && measure.units !== 'readable';
}

export function getUnits(vu: Measurement | null): Units {
  if (vu?.units == null || vu.units === 'default' || vu.units === 'readable') {
    return Units.NATIVE;
  }
  return vu.units;
}

export type MeasurementOK = {
  value: number;
  units: Units;
};

export function measurementOK(vu: Measurement | null): vu is MeasurementOK {
  return hasUnits(vu) && vu?.value != null;
}

/** Returns pure measurement (value, units) from extended structure. */
export function pureMeasurement<T extends Measurement>(measure: T): T {
  const { value, units } = measure;
  return { value, units } as T;
}

// TODO (ZL-609): Clean up
export interface MovementDimensions {
  position: string;
  velocity: string;
  acceleration: string;
}
