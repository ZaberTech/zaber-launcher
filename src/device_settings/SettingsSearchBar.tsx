import React, { useEffect } from 'react';
import { Icons, Input } from '@zaber/react-library';
import { useSelector } from 'react-redux';
import { useActions } from '@zaber/toolbox/lib/redux';

import { actions as actionsDefinition } from './actions';
import { selectSearch } from './selectors';

export const SettingsSearchBar: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const search = useSelector(selectSearch);

  return <Input
    placeholder="Search..."
    className="search-bar"
    onValueChange={actions.setSearchString}
    value={search}
    clearable
    icon={<Icons.Search className="search-icon"/>}
  />;
};

export const OnSearchChange: React.FC<{ onChange: () => void }> = ({ onChange }) => {
  const search = useSelector(selectSearch);
  useEffect(onChange, [search]);
  return null;
};
