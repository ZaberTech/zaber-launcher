import { app, BrowserWindow } from 'electron';
import { injectable } from 'inversify';

@injectable()
export class SingleInstance {
  checkIsSingle(): boolean {
    const hasLock = app.requestSingleInstanceLock();
    if (!hasLock) {
      app.quit();
      return false;
    }

    app.on('second-instance', this.onSecondInstance);
    return true;
  }

  private onSecondInstance = () => {
    const firstWindow = BrowserWindow.getAllWindows()[0];
    if (!firstWindow) {
      return;
    }
    if (firstWindow.isMinimized()) {
      firstWindow.restore();
    }
    firstWindow.focus();
  };
}
