import _ from 'lodash';

import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes } from '../connection_manager';
import { isSubKeyOf } from '../keys';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { ProductManuals, ProductManualsList } from './types';

export type State = {
  productManuals: ProductManualsList;
  commandIdList: string[];
  settingIdList: string[];
};

const initialState: State = {
  productManuals: {},
  commandIdList: [],
  settingIdList: [],
};

const initialManuals = (): ProductManuals =>
  ({ settings: {}, warnings: {}, commandRejectedReasons: {} });

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;
type CmReducer<AT extends ConnectionManagerActionTypes> = (state: State, payload: ConnectionManagerActionPayloads[AT]) => State;

const devicesReload: CmReducer<ConnectionManagerActionTypes.DEVICES_LOADED> = (state, { connectionKey }) => ({
  ...state,
  productManuals: _.pickBy(state.productManuals, (_, key) => !isSubKeyOf(key, connectionKey))
});

const storeGenerateSettings: Reducer<ActionTypes.STORE_SETTING_MANUAL> = (state: State, { entity, setting, manual }) => {
  const { productManuals } = state;
  const entry = productManuals[entity] ?? initialManuals();
  return {
    ...state,
    productManuals: {
      ...productManuals,
      [entity]: { ...entry, settings: { ...entry?.settings, [setting]: manual } }
    },
  };
};

const storeGenerateWarning: Reducer<ActionTypes.STORE_WARNING_MANUAL> = (state: State, { entity, flag, manual }) => {
  const { productManuals } = state;
  const entry = productManuals[entity] ?? initialManuals();
  return {
    ...state,
    productManuals: {
      ...productManuals,
      [entity]: { ...entry, warnings: { ...entry?.warnings, [flag]: manual } }
    },
  };
};

const storeGenerateCommandRejectedReason
  : Reducer<ActionTypes.STORE_COMMAND_REJECTED_REASON_MANUAL> = (state: State, { entity, reason, manual }) => {
    const { productManuals } = state;
    const entry = state.productManuals[entity] ?? initialManuals();
    return {
      ...state,
      productManuals: {
        ...productManuals,
        [entity]: { ...entry, commandRejectedReasons: { ...entry?.commandRejectedReasons, [reason]: manual } }
      }
    };
  };

const storeCommandIdList : Reducer<ActionTypes.STORE_COMMAND_ID_LIST> = (state: State, { commandIdList }) => ({
  ...state,
  commandIdList,
});

const storeSettingIdList : Reducer<ActionTypes.STORE_SETTING_ID_LIST> = (state: State, { settingIdList }) => ({
  ...state,
  settingIdList,
});

export const reducer = createReducer<ActionsToPayloads | ConnectionManagerActionPayloads, typeof initialState>({
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesReload,

  [ActionTypes.STORE_SETTING_MANUAL]: storeGenerateSettings,
  [ActionTypes.STORE_WARNING_MANUAL]: storeGenerateWarning,
  [ActionTypes.STORE_COMMAND_REJECTED_REASON_MANUAL]: storeGenerateCommandRejectedReason,
  [ActionTypes.STORE_COMMAND_ID_LIST]: storeCommandIdList,
  [ActionTypes.STORE_SETTING_ID_LIST]: storeSettingIdList,
}, initialState);
