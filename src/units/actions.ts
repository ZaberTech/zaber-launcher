import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { UnitInfo } from './types';

export enum ActionTypes {
  FETCH_SETTING_UNIT_INFO = 'UNITS-FETCH_SETTING_UNIT_INFO',
  FETCH_SETTING_UNIT_INFO_DONE = 'UNITS-FETCH_SETTING_UNIT_INFO_DONE',
  FETCH_COMMAND_UNIT_INFO = 'UNITS-FETCH_COMMAND_UNIT_INFO',
  FETCH_COMMAND_UNIT_INFO_DONE = 'UNITS-FETCH_COMMAND_UNIT_INFO_DONE',
}

export interface ActionsToPayloads {
  [ActionTypes.FETCH_SETTING_UNIT_INFO]: { entityKey: EntityKey; setting: string };
  [ActionTypes.FETCH_SETTING_UNIT_INFO_DONE]: { entityKey: EntityKey; setting: string; info: UnitInfo };
  [ActionTypes.FETCH_COMMAND_UNIT_INFO]: { entityKey: EntityKey; command: string };
  [ActionTypes.FETCH_COMMAND_UNIT_INFO_DONE]: { entityKey: EntityKey; command: string; info: UnitInfo };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actionDefinitions = {
  fetchSettingUnitInfo: (entityKey: EntityKey, setting: string) => buildAction(
    ActionTypes.FETCH_SETTING_UNIT_INFO, { entityKey, setting }
  ),
  fetchSettingUnitInfoDone: (entityKey: EntityKey, setting: string, info: UnitInfo) => buildAction(
    ActionTypes.FETCH_SETTING_UNIT_INFO_DONE, { entityKey, setting, info }),
  fetchCommandUnitInfo: (entityKey: EntityKey, command: string) => buildAction(
    ActionTypes.FETCH_COMMAND_UNIT_INFO, { entityKey, command }
  ),
  fetchCommandUnitInfoDone: (entityKey: EntityKey, command: string, info: UnitInfo) => buildAction(
    ActionTypes.FETCH_COMMAND_UNIT_INFO_DONE, { entityKey, command, info }),
};
