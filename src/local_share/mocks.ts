import { injectable } from 'inversify';

import type { LocalShareConfig, LocalShareStatus } from '../app_components';

@injectable()
export class MessageRoutersServiceMock {
  getConnection: jest.Mock = jest.fn(async () => this);
  get api() {
    return this;
  }

  config: LocalShareConfig | null = null;
  isRunning = false;

  getLocalShareStatus = jest.fn(async (): Promise<LocalShareStatus> => ({
    isRunning: this.isRunning,
    isDiscoverable: this.config?.discoverable ?? false,
  }));
  getLocalShareConfig = jest.fn(async (): Promise<LocalShareConfig | null> => this.config);
  setLocalShareConfig = jest.fn(async (config: LocalShareConfig) => { this.config = config });
  getName = jest.fn().mockResolvedValue('Lola-PC');
  setName = jest.fn().mockResolvedValue(undefined);
}
