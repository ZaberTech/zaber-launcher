import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { ButtonNext, ButtonPrevious, ButtonRow, Flex, HelpTooltip, Input, InputLabel, RadioButton } from '@zaber/react-library';

import { MyNumber } from '../utils';
import type { RootState } from '../store';

import { actions as actionsDefinition } from './actions';
import { MAX_PORT_NUM, Steps } from './types';
import { selectTcpConfig } from './selectors';
import { Editions, environment } from '../environment';

interface Props {
  tcpConfig: ReturnType<typeof selectTcpConfig>;
  actions: typeof actionsDefinition;
}

class ConfigureTcpBase extends Component<Props> {
  isValid(): boolean {
    const { tcpConfig: { hostname, port } } = this.props;
    return !!hostname && Number.isInteger(port);
  }
  render(): React.ReactNode {
    const { actions, tcpConfig: { hostname, port, selectedConnectionType } } = this.props;
    return (
      <div className="configure-tcp">
        <h3>TCP/IP connection</h3>
        <div className="discover-device">
          <div className="selection">
            <Flex.Column className="tcp-connection-type-wrapper">
              <InputLabel content="TCP Connection Type" htmlFor="tcp-connection-type"/>
              <Flex.Column className="tcp-connection-type" id="tcp-connection-type">
                <RadioButton
                  name="connection-type"
                  value="chain"
                  checked={selectedConnectionType === 'chain'}
                  onValueChange={() => actions.changeTcpConnectionType('chain')}>
                  Device Chain
                  <HelpTooltip className="connection-type-tooltip">
                    Connect to a Zaber device over TCP
                  </HelpTooltip>
                </RadioButton>
                <RadioButton
                  name="connection-type"
                  value="single"
                  checked={selectedConnectionType === 'single'}
                  onValueChange={() => actions.changeTcpConnectionType('single')}>
                  Device Only
                  <HelpTooltip className="connection-type-tooltip">
                    Connect to a Zaber device over TCP and do not forward commands to the next device on the chain
                  </HelpTooltip>
                </RadioButton>
                {[Editions.Dev, Editions.Internal].includes(environment.edition) && <RadioButton
                  name="connection-type"
                  value="simulator"
                  checked={selectedConnectionType === 'simulator'}
                  onValueChange={() => actions.changeTcpConnectionType('simulator')}>
                  Little Device Simulator
                  <HelpTooltip className="connection-type-tooltip">
                    Connect to an instance of the Little Device Simulator running at simulator.izaber.com (internal only)
                  </HelpTooltip>
                </RadioButton>}
                <RadioButton
                  name="connection-type"
                  value="custom"
                  checked={selectedConnectionType === 'custom'}
                  onValueChange={() => actions.changeTcpConnectionType('custom')}>
                  Custom
                  <HelpTooltip className="connection-type-tooltip">
                    Connect to a Zaber device over TCP using a custom port
                  </HelpTooltip>
                </RadioButton>
              </Flex.Column>
            </Flex.Column>
            <Input className="hostname"
              labelContent="Hostname/IP Address"
              disabled={selectedConnectionType === 'simulator'}
              value={hostname}
              onValueChange={value => actions.changeTcp({ hostname: value })}/>

            <Input className="tcp-port"
              labelContent="Port"
              max={MAX_PORT_NUM}
              disabled={['single', 'chain'].includes(selectedConnectionType)}
              value={MyNumber.isFinite(port) ? port : ''}
              onValueChange={value => actions.changeTcp({ port: value !== '' ? +value : null })}/>
          </div>
        </div>
        <ButtonRow justify="spread">
          <ButtonPrevious onClick={() => actions.changeStep(Steps.DISCOVER_MDNS)}>Back</ButtonPrevious>
          <ButtonNext disabled={!this.isValid()} onClick={() =>  actions.changeStep(Steps.DETECT_DEVICES)}>Next</ButtonNext>
        </ButtonRow>
      </div>
    );
  }
}

export const ConfigureTcp = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    tcpConfig: selectTcpConfig(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(ConfigureTcpBase);
