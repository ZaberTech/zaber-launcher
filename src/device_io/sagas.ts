import { all, call, race, take, takeEvery, SagaReturnType as SRT, delay, select, put, actionChannel } from 'redux-saga/effects';
import type { Channel, SagaIterator } from 'redux-saga';
import { throwUnexpectedError, tryAccess } from '@zaber/toolbox';
import { CommandFailedException } from '@zaber/motion';
import { DigitalOutputAction } from '@zaber/motion/ascii';

import { Action, EmptyAction, SagaIter, takeIfTrue } from '../utils';
import type { EntityKey } from '../keys';
import { getDevice } from '../connection_manager';
import { DEFAULT_POLL_INTERVAL } from '../types';

import { ActionTypes, ActionsToPayloads, actions } from './actions';
import type { IoState } from './types';
import { selectDevices } from './selectors';
import type { DeviceState } from './reducer';

export function* rootSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.MOUNT, mount),
    takeEvery(ActionTypes.SET_OUTPUT, setOutput),
  ]);
}

function* setOutput(
  { payload: { deviceKey, output, value } }: Action<ActionsToPayloads[ActionTypes.SET_OUTPUT]>
): SagaIter {
  try {
    const device: SRT<typeof getDevice> = yield call(getDevice, deviceKey);
    if (typeof value === 'boolean') {
      yield device.io.setDigitalOutput(output + 1, value ? DigitalOutputAction.ON : DigitalOutputAction.OFF);
    } else {
      yield device.io.setAnalogOutput(output + 1, value);
    }
    yield put(actions.setOutputDone(deviceKey));
    yield put(actions.refreshIo(deviceKey));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setOutputDone(deviceKey, `Cannot set output: ${err.message}`));
  }
}

function* mount({ payload: { deviceKey } }: Action<ActionsToPayloads[ActionTypes.MOUNT]>): SagaIterator {
  const refreshChannel: Channel<EmptyAction> = yield actionChannel(
    (a: Partial<Action<ActionsToPayloads[ActionTypes.REFRESH_IO]>>) =>
      a.type === ActionTypes.REFRESH_IO && a.payload?.deviceKey === deviceKey);
  try {
    yield race([
      call(monitorDevice, deviceKey, refreshChannel),
      takeIfTrue<ActionsToPayloads[ActionTypes.UNMOUNT]>(ActionTypes.UNMOUNT,
        payload => payload.deviceKey === deviceKey)
    ]);
  } finally {
    refreshChannel.close();
  }
}

function* monitorDevice(deviceKey: EntityKey, refreshChannel: Channel<EmptyAction>): SagaIter {
  for (;;) {
    try {
      const device: SRT<typeof getDevice> = yield call(getDevice, deviceKey);

      const currentState = tryAccess((yield select(selectDevices)) as ReturnType<typeof selectDevices>, deviceKey);
      if (currentState == null) { throw new Error('State not accessible') }

      const ioState: Partial<IoState> = {};
      if (shouldQueryIo(currentState, 'digitalInputs')) {
        try {
          ioState.digitalInputs = yield device.io.getAllDigitalInputs();
        } catch (err) {
          throwIfNotBadCommand(err);
          ioState.digitalInputs = [];
        }
      }

      if (shouldQueryIo(currentState, 'analogInputs')) {
        try {
          ioState.analogInputs = yield device.io.getAllAnalogInputs();
        } catch (err) {
          throwIfNotBadCommand(err);
          ioState.analogInputs = [];
        }
      }

      if (shouldQueryIo(currentState, 'digitalOutputs')) {
        try {
          ioState.digitalOutputs = yield device.io.getAllDigitalOutputs();
        } catch (err) {
          throwIfNotBadCommand(err);
          ioState.digitalOutputs = [];
        }
      }

      if (shouldQueryIo(currentState, 'analogOutputs')) {
        try {
          ioState.analogOutputs = yield device.io.getAllAnalogOutputs();
        } catch (err) {
          throwIfNotBadCommand(err);
          ioState.analogOutputs = [];
        }
      }
      yield put(actions.stateRead(deviceKey, ioState));
    } catch (err) {
      throwUnexpectedError(err);
      yield put(actions.stateReadError(deviceKey, err.message ?? String(err)));
    }

    yield race([
      delay(DEFAULT_POLL_INTERVAL),
      take(refreshChannel),
    ]);
  }
}

function shouldQueryIo(currentState: DeviceState, ioType: keyof IoState): boolean {
  if (currentState.ioState == null) {
    return true;
  }
  return currentState.ioState[ioType].length > 0;
}

function throwIfNotBadCommand(err: unknown) {
  if (err instanceof CommandFailedException) {
    if (['BADCOMMAND', 'BADDATA'].includes(err.details.responseData)) {
      return;
    }
  }
  throw err;
}
