import fs from 'fs/promises';
import path from 'path';

import { take, race, delay as defaultDelay } from 'redux-saga/effects';
import type { Action } from '@zaber/toolbox/lib/redux';
import _ from 'lodash';
import { useEffect, useRef, useState } from 'react';
import { isMatching, P } from 'ts-pattern';
import { CANCEL } from 'redux-saga';
import { makeString } from '@zaber/toolbox';

export * from '@zaber/toolbox/lib/redux';
export * from '@zaber/toolbox';

export function splitOnce(str: string, separator: string): [string, string] {
  const index = str.indexOf(separator);
  if (index < 0) {
    return [str, ''];
  }
  return [str.substr(0, index), str.substr(index + 1)];
}

export function takeIfTrue<TPayload>(type: string, condition: (payload: TPayload) => boolean) {
  return take((a: Partial<Action<TPayload>>) =>
    a.type === type && condition(a.payload!));
}

export function htmlColor(color: number): string {
  return `#${color.toString(16).toUpperCase().padStart(6, '0')}`;
}

export async function findFiles(folder: string, pattern: RegExp, ignore?: RegExp) {
  const result: string[] = [];
  const toLook = [folder];
  while (toLook.length) {
    const current = toLook.pop()!;
    for (const entry of await fs.readdir(current)) {
      if (ignore != null && entry.match(ignore)) {
        continue;
      }

      const entryPath = path.join(current, entry);
      const stat = await fs.stat(entryPath);

      if (stat.isFile() && entry.match(pattern)) {
        result.push(entryPath);
      } else if (stat.isDirectory()) {
        toLook.push(entryPath);
      }
    }
  }
  return result;
}

export const TEST_SKIP_SAGA_DELAYS = { type: 'TEST_SKIP_SAGA_DELAYS' };
export function delay(ms: number) {
  if (process.env.JEST_WORKER_ID) {
    return race([
      defaultDelay(ms),
      take(TEST_SKIP_SAGA_DELAYS.type),
    ]);
  } else {
    return defaultDelay(ms);
  }
}

/** Determines whether given element is entirely in view. */
export function isScrolledIntoView(element: HTMLElement) {
  const rect = element.getBoundingClientRect();
  const isVisible = rect.top >= 0 && rect.bottom <= window.innerHeight;
  return isVisible;
}

/** Tests that Left and Right are exactly the same types. */
export type TestExact<Left, Right> =
  (<U>() => U extends Left ? 1 : 0) extends (<U>() => U extends Right ? 1 : 0) ? unknown : never;


export type NullFields<T, K extends keyof T> = {
  [P in keyof T]: P extends K ? T[P] | null : T[P];
};


/** Sorts strings with embedded numbers by comparing the values of the numbers. Letters are sorted case-insensitively. */
export const naturalSort = (a: string, b: string) => a.localeCompare(b, undefined, { numeric: true, sensitivity: 'base' });

export const roundToMultiple = function(x: number, quantum: number): number {
  return quantum * Math.round(x / quantum);
};

export function round<T extends number | null | undefined>(n: T, precision: number = 0): T {
  if (n == null) { return n }
  return _.round(n, precision) as T;
}

export function roundToFixed(value: number, digits: number): string {
  return _.round(value, digits).toFixed(digits);
}

/**
 * Outputs an E3 number sequence (https://en.wikipedia.org/wiki/E_series_of_preferred_numbers)
 * rounded to integer values. This corresponds to the 1-2-5 scaling sequence found on
 * oscilloscope controls.
 * This function does not support negative numbers, but does support values less than 1.
 * @param min Lower bound for the output sequence. If this is not a power of ten times 1, 2 or 5 it will
 * be omitted from the output and the next larger value will be the first output.
 * @param max Upper bound for the output sequence. If this is not a power of ten times 1, 2 or 5 it will
 * be omitted from the output and the next smaller value will be the last output.
 * @returns Zero or more numbers in an E3 sequence between min and max, in ascending order.
 */
export const roundedE3Sequence = function(min: number, max: number): number[] {
  if (Number.isNaN(min) || Number.isNaN(max) || !Number.isFinite(min) || !Number.isFinite(max)) {
    throw new Error('That\'s not a number!');
  }

  const steps = [1, 2, 5];
  const result: number[] = [];
  let exponent = Math.floor(Math.log10(min));
  let i = 0;
  let val;
  for (;;) {
    val = steps[i] * Math.pow(10, exponent);
    if (val >= min) {
      if (val <= max) {
        result.push(val);
      } else {
        break;
      }
    }

    i++;
    if (i >= steps.length) {
      i = 0;
      exponent++;
    }
  }

  return result;
};


export function blankToNull(str: string | null | undefined): string | null {
  if (str && str.trim().length > 0) {
    return str;
  }

  return null;
}

/**
 * Splits typed array at a given index.
 * Credit: https://stackoverflow.com/questions/67605122/obtain-a-slice-of-a-typescript-parameters-tuple
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TupleSplit<T, N extends number, O extends readonly any[] = readonly []> =
    O['length'] extends N ? [O, T] : T extends readonly [infer F, ...infer R] ?
    TupleSplit<readonly [...R], N, readonly [...O, F]> : [O, T];


/** Debounces the callback while returning the intermediate value. */
export function useDebounceValue<TValue>(
  callback: TValue extends null ? never : (value: TValue) => void,
  delay: number,
): [intermediate: TValue | null, callback: ((value: TValue) => void)] {
  const callbackRef = useRef(callback);
  callbackRef.current = callback;

  const [intermediate, setIntermediate] = useState<TValue | null>(null);

  useEffect(() => {
    if (intermediate == null) { return }

    const handler = setTimeout(() => {
      callbackRef.current(intermediate);
      setIntermediate(null);
    }, process.env.JEST_WORKER_ID ? 0 : delay);

    return () => clearTimeout(handler);
  }, [intermediate, delay]);

  return [intermediate, (value: TValue) => setIntermediate(value)];
}

/** Equivalent to isMatching from ts-pattern except it verifies that value overlaps with the pattern. */
// eslint-disable-next-line @typescript-eslint/no-redundant-type-constituents
export function isMatch<T, const PType extends P.Pattern<T | unknown>>(pattern: PType, value: T): value is P.infer<PType> {
  return isMatching(pattern, value);
}

export async function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function prependZeros(n: number, precision: number = 2): string {
  return n.toLocaleString('en-US', {
    minimumIntegerDigits: precision,
    useGrouping: false,
  });
}

export function attachSagaCancel<T>(promise: Promise<T>, cancel: (result: T) => void | Promise<void>) {
  // The catch is necessary to prevent unhandled rejection from the promise created by attaching the cancel handler
  // and also to catch potential errors from cancellation itself.
  // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/no-unsafe-member-access
  (promise as any)[CANCEL] = () => promise.then(cancel).catch(_.noop);
}


export type ComparisonOperatorSymbol = '>' | '<' | '=' | '!=' | '>=' | '<=';
export function compare(a: number, operator: ComparisonOperatorSymbol, b: number): boolean {
  switch (operator) {
    case '=':
      return a === b;
    case '!=':
      return a !== b;
    case '>':
      return a > b;
    case '<':
      return a < b;
    case '>=':
      return a >= b;
    case '<=':
      return a <= b;
    default:
      throw new Error(`Invalid operator ${makeString(operator)}`);
  }
}

export function deepFreeze<T>(obj: T): T {
  if (obj == null || typeof obj !== 'object') {
    return obj;
  }

  const propNames = Object.getOwnPropertyNames(obj);

  for (const name of propNames) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/no-unsafe-member-access
    const value = (obj as any)[name];
    deepFreeze(value);
  }
  return Object.freeze(obj);
}
