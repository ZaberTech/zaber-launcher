import { EventEmitter } from 'events';

import React from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import type { MDNSAnswer, MDNSResponse } from 'multicast-dns';
import type { DeepPartial } from 'redux';

const lookupMock = jest.fn<void, [name: string, family: number, cb: (err: unknown) => void]>();

let mdnsMock: MdnsMock;
const createMdns = jest.fn(() => new MdnsMock());

class MdnsMock extends EventEmitter {
  constructor() {
    super();
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    mdnsMock = this;
  }

  query = jest.fn();
  destroy = jest.fn();

  response(response: DeepPartial<MDNSResponse>) {
    this.emit('response', response);
  }
}

jest.mock('multicast-dns', () => createMdns);
jest.mock('dns', () => ({ lookup: lookupMock }));

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore } from '../test';
import { makeLocalShareUrl } from '../connection_manager';
import { MessageRoutersService } from '../message_router';

import { MessageRoutersServiceMock } from './mocks';
import { LocalShareDiscover } from './LocalShareDiscover';
import { LOCAL_SHARE_DISCOVERY_INACTIVE_TIMEOUT, LOCAL_SHARE_DISCOVERY_SERVICE_SUFFIX } from './types';
import { selectDiscoveryCandidates } from './selectors';

const TestLocalShareDiscover = wrapWithNewStore(LocalShareDiscover);

let wrapper: RenderResult;

beforeAll(() => {
  jest.useFakeTimers();
});
afterAll(() => {
  jest.useRealTimers();
});

beforeEach(() => {
  lookupMock.mockImplementation((name: string, family: number, cb: (err: unknown) => void) => cb(null));

  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

  wrapper = render(<TestLocalShareDiscover onRouterAdded={jest.fn()}/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  mdnsMock = null!;

  lookupMock.mockReset();
});

function createMockRecord(name: string, port: number): DeepPartial<MDNSAnswer[]> {
  const dnsName = `${name.toLocaleLowerCase()}.${LOCAL_SHARE_DISCOVERY_SERVICE_SUFFIX}`;
  return [{
    type: 'SRV',
    name: dnsName,
    data: {
      target: `${name.toLocaleLowerCase()}.local`,
      port,
      priority: 0,
      weight: 0,
    },
  }, {
    type: 'TXT',
    name: dnsName,
    data: [Buffer.from(`name=${name}`)],
  }];
}

test('explores clients', async () => {
  await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  mdnsMock.response({
    answers: createMockRecord('somePC', 4567),
    additionals: [],
  });
  await waitUntilPass(() => wrapper.getByText(/somePC/));
});

test('destroys mDNS after timeout', async () => {
  jest.advanceTimersByTime(LOCAL_SHARE_DISCOVERY_INACTIVE_TIMEOUT);
  await waitUntilPass(() => expect(mdnsMock.destroy).toHaveBeenCalled());
});

test('destroys mDNS on unmount', async () => {
  wrapper.unmount();
  await waitUntilPass(() => expect(mdnsMock.destroy).toHaveBeenCalled());
});

test('filters duplicates and other records', async () => {
  await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  mdnsMock.response({
    answers: [
      { type: 'TXT', name: 'something' },
      { type: 'TXT', name: `broken.${LOCAL_SHARE_DISCOVERY_SERVICE_SUFFIX}`, data: [Buffer.from('invalid')] },
      { type: 'AAAA', name: 'something' },
      ...createMockRecord('SomePC', 4567),
      ...createMockRecord('SomePC', 4567),
    ],
    additionals: [],
  });
  mdnsMock.response({
    answers: [
      ...createMockRecord('SomePC', 4567),
      ...createMockRecord('OtherPC', 4567),
    ],
    additionals: [],
  });
  await waitUntilPass(() => wrapper.getByText(/OtherPC/));
  expect(wrapper.queryAllByText(/SomePC/)).toHaveLength(1);
});

test('removes client if it cannot be resolved', async () => {
  lookupMock.mockImplementation((name, f, callback) => callback(name.includes('some') ? 'error' : null));

  await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  mdnsMock.response({
    answers: [
      ...createMockRecord('SomePC', 4567),
      ...createMockRecord('OtherPC', 4567),
    ],
    additionals: [],
  });

  await waitUntilPass(() => wrapper.getByText(/OtherPC/));
  expect(wrapper.queryByText(/SomePC/)).toBeNull();
});

test('removes .local suffix if it cannot be resolved', async () => {
  lookupMock.mockImplementation((name, f, callback) => callback(name.includes('.local') ? 'error' : null));

  await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  mdnsMock.response({
    answers: createMockRecord('SomePC', 4567),
    additionals: [],
  });

  await waitUntilPass(() => {
    const candidates = selectDiscoveryCandidates(TestLocalShareDiscover.testStore.getState());
    expect(candidates[0].url).toBe(makeLocalShareUrl('somepc', 4567));
  });
});

describe('after first discovery done', () => {
  beforeEach(async () => {
    jest.advanceTimersByTime(LOCAL_SHARE_DISCOVERY_INACTIVE_TIMEOUT);
    await waitUntilPass(() => expect(mdnsMock.destroy).toHaveBeenCalled());
  });

  test('allows to repeat discovery', async () => {
    mdnsMock = null!;
    fireEvent.click(wrapper.getByText(/Discover Again/));
    await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  });

  test('shows error when discovery fails', async () => {
    createMdns.mockImplementation(() => {
      const mdns = new MdnsMock();
      mdns.query.mockImplementationOnce(() => { throw new Error('Cannot query mDNS.') });
      return mdns;
    });

    fireEvent.click(wrapper.getByText(/Discover Again/));

    await waitUntilPass(() => wrapper.getByText(/Cannot query/));
  });
});
