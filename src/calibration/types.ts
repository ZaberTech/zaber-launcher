export const VALUE_COUNT = 64;

export interface CalibrationTable {
  calibrationType: number;
  width: number;
  offset: number;
  values: number[];
}

export interface EditedCalibrationTable {
  calibrationType: number | null;
  width: number | null;
  offset: number | null;
  values: string;
}

export function toEditedTable(table: CalibrationTable): EditedCalibrationTable {
  const values = table.values.reduce<string[][]>((rows, value, i) => {
    if (i % 4 === 0) {
      rows.push([]);
    }
    rows[rows.length - 1].push(`${value}`.padStart(6, ' '));
    return rows;
  }, []).map(row => row.join(' ')).join('\n');

  return {
    calibrationType: table.calibrationType,
    width: table.width,
    offset: table.offset,
    values,
  };
}

export function fromEditedTable(table: EditedCalibrationTable): CalibrationTable | { error: string } {
  if (table.calibrationType == null) {
    return { error: 'Calibration Type is required.' };
  } else if (table.offset == null) {
    return { error: 'Table Offset is required.' };
  } else if (table.width == null) {
    return { error: 'Table Width is required.' };
  }
  const values = table.values.split(/\s+/g).filter(str => !!str).map(str => +str);
  if (values.length === 0) {
    return { error: 'No calibration data.' };
  } else if (values.length !== VALUE_COUNT) {
    return { error: `Expected ${VALUE_COUNT} values, got ${values.length}.` };
  }
  const invalidValue = values.findIndex(v => Number.isNaN(v));
  if (invalidValue >= 0) {
    return { error: `Value at index ${invalidValue} is invalid.` };
  }
  return {
    calibrationType: table.calibrationType,
    width: table.width,
    offset: table.offset,
    values,
  };
}

export function fromEditedTableOrThrow(table: EditedCalibrationTable): CalibrationTable {
  const result = fromEditedTable(table);
  if ('error' in result) {
    throw new Error(`Cannot convert table: ${result.error}`);
  }
  return result;
}
