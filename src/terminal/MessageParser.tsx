import type { Nullable } from '@zaber/toolbox';

import { getEntityType, type EntityKey, EntityKeyType } from '../keys';

import type {
  HistoryItem, HistoryItemParsed, HistoryItemParsedCommand, HistoryItemParsedReply, HistoryItemParsedInfo,
  HistoryItemParsedAlert, ErrorFlagType, HistoryItemParsedUnknown
} from './types';
import { getMostSpecificEntityKey } from './utils';

const REGEX: Record<string, RegExp> = {
  command: new RegExp(
    /^(?<type>\/)((?<device>\d+))?(\s(?<axis>\d+))?/.source
    + /(\s(?<id>[\d-]+))?(\s?(?<data>[^:]+))?(:(?<checksum>\S+))?$/.source),
  reply: new RegExp(
    /^(?<type>@)((?<device>\d+)\s)((?<axis>\d+)\s)/.source
    + /((?<id>\d+)\s)?((?<replyFlag>OK|RJ)\s)((?<status>IDLE|BUSY)\s)/.source
    + /((?<errorFlag>[-FWN]\S)\s)(?<data>[^:]+)(:(?<checksum>\S+))?$/.source),
  info: new RegExp(/^(?<type>#)((?<device>\d+)\s)((?<axis>\d+)\s)/.source
    + /((?<id>\d+)\s)?(?<data>[^:]+)(:(?<checksum>\S+))?$/.source),
  alert: new RegExp(/^(?<type>!)((?<device>\d+)\s)?((?<axis>\d+)\s)?/.source
    + /((?<status>IDLE|BUSY)\s)(?<errorFlag>[-FWN]\S)(\s(?<data>[^:]+))?/.source
    + /(:(?<checksum>\S+))?$/.source),
  unknown: /^((?<data>[^:]+))?(:(?<checksum>\S+))?$/,
};

const PARSERS: Record<string, (match: Nullable<RegExpMatchArray>, text: string) => HistoryItemParsed> = {
  command: (match: Nullable<RegExpMatchArray>, text: string): HistoryItemParsedCommand => ({
    type: 'command',
    device: match?.groups?.device ?? null,
    axis: match?.groups?.axis ?? null,
    id: match?.groups?.id ?? null,
    data: match?.groups?.data ?? null,
    checksum: match?.groups?.checksum ? {
      text: match?.groups?.checksum,
      error: !verifyLRC(text, match?.groups?.checksum),
    } : null,
  }),
  reply: (match: Nullable<RegExpMatchArray>, text: string): HistoryItemParsedReply => ({
    type: 'reply',
    device: match?.groups?.device ?? '',
    axis: match?.groups?.axis ?? '',
    id: match?.groups?.id ?? null,
    replyFlag: match?.groups?.replyFlag ?? '',
    status: match?.groups?.status ?? '',
    errorFlag: {
      text: match?.groups?.errorFlag ?? '',
      type: getErrorFlagType(match?.groups?.errorFlag)
    },
    data: match?.groups?.data ?? '',
    checksum: match?.groups?.checksum ? {
      text: match?.groups?.checksum,
      error: !verifyLRC(text, match?.groups?.checksum),
    } : null,
  }),
  info: (match: Nullable<RegExpMatchArray>, text: string): HistoryItemParsedInfo => ({
    type: 'info',
    device: match?.groups?.device ?? '',
    axis: match?.groups?.axis ?? '',
    id: match?.groups?.id ?? null,
    data: match?.groups?.data ?? '',
    checksum: match?.groups?.checksum ? {
      text: match?.groups?.checksum,
      error: !verifyLRC(text, match?.groups?.checksum),
    } : null,
  }),
  alert: (match: Nullable<RegExpMatchArray>, text: string): HistoryItemParsedAlert => ({
    type: 'alert',
    device: match?.groups?.device ?? '',
    axis: match?.groups?.axis ?? '',
    status: match?.groups?.status ?? '',
    errorFlag: {
      text: match?.groups?.errorFlag ?? '',
      type: getErrorFlagType(match?.groups?.errorFlag)
    },
    data: match?.groups?.data ?? null,
    checksum: match?.groups?.checksum ? {
      text: match?.groups?.checksum,
      error: !verifyLRC(text, match?.groups?.checksum),
    } : null,
  }),
  unknown: (): HistoryItemParsedUnknown => ({
    type: 'unknown'
  }),
};

function verifyLRC(text: string, checksum: string) {
  const content = text.substring(0, text.indexOf(':'));

  let lrc = 0;
  for (let i = 1; i < content.length; i++) {
    lrc -= content.charCodeAt(i);
  }
  lrc = ((lrc % 256) + 256) % 256;

  const claimedLRC = parseInt(checksum, 16);
  return lrc === claimedLRC;
}

function getItemType(typeFlag: string): string {
  switch (typeFlag) {
    case '/':
      return 'command';
    case '@':
      return 'reply';
    case '#':
      return 'info';
    case '!':
      return 'alert';
    default:
      return 'unknown';
  }
}

function getErrorFlagType(flag?: string): ErrorFlagType {
  if (flag == null) {
    return null;
  }
  switch (flag[0]) {
    case 'F': return 'fault';
    case 'W': return 'warn';
    case 'N': return 'note';
    default: return null;
  }
}

export const parseMessage = (text: string, local: boolean, entityKey: EntityKey): HistoryItem => {
  let type: string = getItemType(text[0]);
  const match: Nullable<RegExpMatchArray> = text.match(REGEX[type]);

  if (match == null) {
    type = 'unknown';
  }

  const specificEntityKey = getMostSpecificEntityKey(entityKey, match?.groups?.device, match?.groups?.axis);
  const entityType = getEntityType(specificEntityKey);

  return {
    deviceOrAxisEntityKey: entityType === EntityKeyType.DEVICE || entityType === EntityKeyType.AXIS ? specificEntityKey : null,
    local,
    text,
    parsed: PARSERS[type](match, text),
  };
};
