import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Button, Toast, Flex } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { selectUpdates } from '../store';
import { Editions, environment } from '../environment';

import { actions as actionsDefinition } from './actions';


const NotifyMainProcess: React.FC = () => {
  const actions = useActions(actionsDefinition);
  useEffect(() => {
    actions.newVersionNotificationDisplayed();
  }, []);
  return null;
};

export const UpdateNotification: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const { newVersion, showNewVersionInstalled, currentVersion, showNewVersionAvailable, waitingToQuit } = useSelector(selectUpdates);

  if (newVersion && showNewVersionAvailable) {
    return <Toast
      className="new-version-available"
      type="info"
      closer={{ title: 'Ignore Update' }}
      onClose={() => actions.ignoreUpdate(false)}
    >
      <Flex.Row>
        <NotifyMainProcess key={newVersion.version}/>
        {!waitingToQuit && <>There is a new version ({newVersion.version}) of {environment.appName} available.</>}
        {waitingToQuit && <>Preparing the update, please wait...</>}
        {newVersion.canUpdate && <>
          <Button color="grey" disabled={waitingToQuit} onClick={actions.quitAndInstall}>Update and Restart</Button>
          {environment.edition !== Editions.Public &&
            <Button color="grey" disabled={waitingToQuit} onClick={() => actions.ignoreUpdate(true)}>Install on Startup</Button>}
        </>}
      </Flex.Row>
    </Toast>;
  } else if (showNewVersionInstalled) {
    return <Toast
      type="success"
      duration={10000}
      closer={{ title: 'Close' }}
      onClose={actions.appUpdatedNotificationClose}
    >
      New version ({currentVersion}) of {environment.appName} has been successfully installed.
    </Toast>;
  } else {
    return null;
  }
};
