import { combineReducers } from 'redux';

import { reducer as appReducer, State as AppState } from './reducer';
import { State as MultiDAcqState, reducer as multiDAcqReducer } from './multidacq/reducer';

export interface State {
  app: AppState;
  multiDAcq: MultiDAcqState;
}

export const reducer = combineReducers<State>({
  app: appReducer,
  multiDAcq: multiDAcqReducer,
});
