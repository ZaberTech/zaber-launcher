import type { Units } from '@zaber/motion';

import type { DeviceInfoWithAxes, IdentifiedDeviceState } from '../connection_manager';

export const UNUSED_AXIS_PERIPHERAL_ID = 0;
export const tuningGuidelineUrl = 'https://www.zaber.com/w/Software/Current_Tuner';

export interface Setting {
  name: string;
  value: number;
  units?: Units;
}
export interface SettingDefinition {
  name: string;
  units?: Units;
}

export interface Sliders {
  overallGain: number;
  backEMFCompensation: number;
  responseCorrectionSpeed: number;
}

interface SliderRange {
  min: number;
  max: number;
  step: number;
}

export type SlidersRange = { [key in keyof Sliders]: SliderRange };

export interface TuningInfo {
  defaultSliderPositions: Sliders;
  slidersRange: SlidersRange;
  settings: SettingDefinition[];
  inputSettings: SettingDefinition[];
}

export enum MotorTypes {
  None = 0,
  StepperMotor = 1,
  DirectDriveMotor = 2,
  VoiceCoilMotor = 3,
  BrushlessDCMotor = 4,
}

export enum LoadingErrorEnum {
  NON_STEPPER = 'not-stepper',
  INCOMPATIBLE_PRODUCT = 'incompatible',
  THIRD_PARTY_NOT_COMPLETE = '3rd-party-incomplete',
}

// either enum value or a custom message
export type LoadingError = LoadingErrorEnum | string;

export const SLIDER_APPLIED_CONFIRMATION_MESSAGE = 'Your changes have been applied. ';

export function isIntegratedMultiAxisDevice(device: DeviceInfoWithAxes | IdentifiedDeviceState): boolean {
  return device.isMultiAxis && !device.isController;
}

export function isIntegratedSingleAxisDevice(device: DeviceInfoWithAxes): boolean {
  return !device.isMultiAxis && !device.isController;
}

export interface ThirdPartyInfo {
  ratedCurrent: number;
  stepsPerRotation: number;
  holdingTorque: number;
}
