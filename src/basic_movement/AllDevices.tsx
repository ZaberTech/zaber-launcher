import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Card, Text, Icons } from '@zaber/react-library';

import type { RootState } from '../store';

import { actions as actionsDefinition } from './actions';
import { MovementType } from './types';
import { selectLastInitiatorAll } from './selectors';

interface DispatchProps {
  actions: typeof actionsDefinition;
}

interface Props {
  lastInitiatorAll: ReturnType<typeof selectLastInitiatorAll>;
}

class AllDevicesBase extends Component<Props & DispatchProps> {
  render(): React.ReactNode {
    const { actions, lastInitiatorAll } = this.props;
    return (
      <Card className="all-devices">
        <div className="header"><Text t={Text.Type.H5}>All Devices Below</Text></div>
        <div className="controls">
          <div className="home">
            <Icons.Home title="Home All Devices"
              activated={lastInitiatorAll === 'home all'}
              onClick={() => actions.moveAllAxes(MovementType.Home, 'home all')}/>
          </div>
          <div className="move">
            <Icons.LeftEnd title="Move All Devices to Beginning"
              activated={lastInitiatorAll === 'min all'}
              onClick={() => actions.moveAllAxes(MovementType.Min, 'min all')}/>
            <Icons.Stop title="Stop All Devices"
              activated={lastInitiatorAll === 'stop all'}
              onClick={() => actions.moveAllAxes(MovementType.Stop, 'stop all')}/>
            <Icons.RightEnd title="Move All Devices to End"
              activated={lastInitiatorAll === 'max all'}
              onClick={() => actions.moveAllAxes(MovementType.Max, 'max all')}/>
          </div>
        </div>
      </Card>
    );
  }
}

export const AllDevices = connect<Props, DispatchProps, unknown, RootState>(
  (state: RootState): Props => ({
    lastInitiatorAll: selectLastInitiatorAll(state),
  }),
  (dispatch: Dispatch): DispatchProps => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(AllDevicesBase);
