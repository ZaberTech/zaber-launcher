import { SetStateAxisResponse, SetStateDeviceResponse } from '@zaber/motion/ascii';
import { Text, UnorderedList } from '@zaber/react-library';
import React from 'react';

type SetStateResponseWarningsProps = {
  header: string;
  response: SetStateDeviceResponse | SetStateAxisResponse;
};

export const SetStateResponseWarnings: React.FC<SetStateResponseWarningsProps> = ({ header, response }) => {
  const axisResponses = 'axisResponses' in response ?
    response.axisResponses.filter(({ warnings }) => warnings.length > 0) :
    [];
  const allWarnings = [
    ...response.warnings,
    ...axisResponses.flatMap(({ axisNumber, warnings }) => warnings.map(warning => `On Axis ${axisNumber}: ${warning}`)),
  ];
  if (allWarnings.length <= 0) {
    return null;
  }
  return  <UnorderedList header={<Text t={Text.Type.H5}>{header}</Text>}>{allWarnings}</UnorderedList>;
};
