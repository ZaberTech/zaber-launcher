import { filterDictionary } from '../utils';

import type { EntityKey } from './generic';

/**
 * Remove dictionary items whose values have a property 'key' which matches the key returned by extractKeyFn
 */
export function removeWithKey<T extends { key: EntityKey }>(
  dictionary: Record<string, T>,
  extractKeyFn: (key: EntityKey) => EntityKey, key: EntityKey,
): Record<string, T> {
  return filterDictionary(dictionary, item => extractKeyFn(item.key) !== key);
}

/**
 * Remove dictionary items whose keys match the key returned by extractKeyFn
 */
export function removeEntriesWithKey<T>(
  dictionary: Record<string, T>,
  extractKeyFn: (key: EntityKey) => EntityKey,
  toRemove: EntityKey,
): Record<string, T> {
  return filterDictionary(dictionary, (_, key) => extractKeyFn(key) !== toRemove);
}
