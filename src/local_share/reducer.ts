import type { LocalShareConfig, LocalShareStatus } from '../app_components';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { LocalShareCandidate } from './types';

interface DiscoveryState {
  discovering: boolean;
  candidates: LocalShareCandidate[];
  error: string | null;
}
const defaultDiscoveryState: DiscoveryState = {
  discovering: false,
  candidates: [],
  error: null,
};

export interface State {
  localShare: LocalShareConfig | null;
  localShareMode: 'initialize' | 'write' | 'ready';
  readingConfig: boolean;
  configReadErr: string | null;

  status: LocalShareStatus | null;
  readingStatus: boolean;
  statusReadErr: string | null;

  configUpdating: boolean;
  configUpdateErr: string | null;
  sharingWarningVisible: boolean;

  discovery: DiscoveryState;
}

const initialState: State = {
  localShare: null,
  localShareMode: 'initialize',
  readingConfig: false,
  configReadErr: '',

  status: null,
  readingStatus: false,
  statusReadErr: '',

  configUpdating: false,
  configUpdateErr: null,
  sharingWarningVisible: false,

  discovery: defaultDiscoveryState,
};

const localShareConfig = (state: State): State =>
  ({
    ...state,
    localShareMode: 'initialize',
    readingConfig: true,
  });

const localShareConfigResult = (state: State, { config, err }: ActionsToPayloads[ActionTypes.GET_LOCAL_SHARE_CONFIG_RESULT]): State =>
  ({
    ...state,
    localShareMode: 'ready',
    readingConfig: false,
    localShare: config ?? null,
    configReadErr: err ?? null,
  });

const localShareStatus = (state: State): State =>
  ({
    ...state,
    readingStatus: true,
  });

const localShareStatusResult = (state: State, { status, err }: ActionsToPayloads[ActionTypes.LOCAL_SHARE_STATUS_RESULT]): State =>
  ({
    ...state,
    readingStatus: false,
    status: status ?? null,
    statusReadErr: err ?? null,
  });


const updateConfig = (state: State, { configUpdate }: ActionsToPayloads[ActionTypes.UPDATE_CONFIG]): State =>
  ({
    ...state,
    localShareMode: 'write',
    configUpdating: true,
    configUpdateErr: null,
    localShare: state.localShare && { ...state.localShare, ...configUpdate },
  });

const updateConfigDone = (state: State, { err }: ActionsToPayloads[ActionTypes.UPDATE_CONFIG_DONE]): State =>
  ({
    ...state,
    localShareMode: 'ready',
    configUpdating: false,
    configUpdateErr: err ?? null,
  });

const toggleSharingWarning = (state: State, { on }: ActionsToPayloads[ActionTypes.TOGGLE_SHARING_WARNING]): State =>
  ({
    ...state,
    sharingWarningVisible: on,
  });

const discoveringCandidates = (state: State): State =>
  ({
    ...state,
    discovery: {
      candidates: [],
      discovering: true,
      error: null,
    },
  });

const discoveryDone = (state: State, { error }: ActionsToPayloads[ActionTypes.DISCOVER_DONE]): State =>
  ({
    ...state,
    discovery: {
      ...state.discovery,
      discovering: false,
      error: error ?? null,
    },
  });

const clearCandidates = (state: State): State =>
  ({
    ...state,
    discovery: {
      ...state.discovery,
      candidates: [],
    },
  });

const addCandidate = (state: State, { candidate }: ActionsToPayloads[ActionTypes.DISCOVER_ADD_CANDIDATE]): State =>
  ({
    ...state,
    discovery: {
      ...state.discovery,
      candidates: [
        ...state.discovery.candidates.filter(other => other.serviceName !== candidate.serviceName),
        candidate,
      ],
    },
  });

export const reducer = createReducer<ActionsToPayloads, State>({
  [ActionTypes.UPDATE_CONFIG]: updateConfig,
  [ActionTypes.UPDATE_CONFIG_DONE]: updateConfigDone,
  [ActionTypes.TOGGLE_SHARING_WARNING]: toggleSharingWarning,
  [ActionTypes.GET_LOCAL_SHARE_CONFIG]: localShareConfig,
  [ActionTypes.GET_LOCAL_SHARE_CONFIG_RESULT]: localShareConfigResult,
  [ActionTypes.LOCAL_SHARE_STATUS]: localShareStatus,
  [ActionTypes.LOCAL_SHARE_STATUS_RESULT]: localShareStatusResult,
  [ActionTypes.DISCOVER]: discoveringCandidates,
  [ActionTypes.DISCOVER_CLEAR_CANDIDATES]: clearCandidates,
  [ActionTypes.DISCOVER_ADD_CANDIDATE]: addCandidate,
  [ActionTypes.DISCOVER_DONE]: discoveryDone,
}, initialState);
