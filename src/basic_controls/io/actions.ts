import type { IoPortType } from '@zaber/motion/ascii';
import { InputEditor } from '@zaber/react-library';

import type { EntityKey } from '../../keys';
import { actionBuilder } from '../../utils';

export enum ActionTypes {
  START_SET_DIGITAL = 'BASIC_CONTROLS-IO-START_SET_DIGITAL',
  COMPLETE_SET_DIGITAL = 'BASIC_CONTROLS-IO-COMPLETE_SET_DIGITAL',

  SET_ANALOG = 'BASIC_CONTROLS-IO-SET_ANALOG',

  SET_ERROR = 'BASIC_CONTROLS-IO-SET_ERROR',

  SET_LABEL = 'BASIC_CONTROLS-IO-SET_LABEL',
  SET_LABEL_DEVICE = 'BASIC_CONTROLS-IO-SET_LABEL_DEVICE',
  SET_LABEL_ERROR = 'BASIC_CONTROLS-IO-SET_LABEL_ERROR',
  DELETE_LABEL_ERROR = 'BASIC_CONTROLS-IO-DELETE_LABEL_ERROR',

  LOAD_LABELS_DEVICE = 'BASIC_CONTROLS-IO-LOAD_LABELS_DEVICE',
}

export interface ActionsToPayloads {
  [ActionTypes.START_SET_DIGITAL]: { deviceKey: EntityKey; channel: number; on: boolean };
  [ActionTypes.COMPLETE_SET_DIGITAL]: { deviceKey: EntityKey; channel: number };

  [ActionTypes.SET_ANALOG]: { deviceKey: EntityKey; channel: number; state: InputEditor.State<number | null, number>};

  [ActionTypes.SET_ERROR]: { deviceKey: EntityKey; error: { message: string; details: string } | null };

  [ActionTypes.SET_LABEL]: { deviceKey: EntityKey; portType: IoPortType; channelNumber: number; newValue: string };
  [ActionTypes.SET_LABEL_DEVICE]: {
    deviceKey: EntityKey; portType: IoPortType; channelNumber: number; newValue: string; prevValue: string;
  };
  [ActionTypes.SET_LABEL_ERROR]: {
     deviceKey: EntityKey; portType: IoPortType; channelNumber: number;
     prevValue: string; error: { message: string; details: string } | null;
  };
  [ActionTypes.DELETE_LABEL_ERROR]: { deviceKey: EntityKey; ioErrorKey: string };

  [ActionTypes.LOAD_LABELS_DEVICE]: { deviceKey: EntityKey };
}

type K = keyof ActionsToPayloads;
const buildAction = (type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actionDefinitions = {
  setDigital: (deviceKey: EntityKey, channel: number, on: boolean) => (
    buildAction(ActionTypes.START_SET_DIGITAL, { deviceKey, channel, on })
  ),
  completeSetDigital: (deviceKey: EntityKey, channel: number) => (
    buildAction(ActionTypes.COMPLETE_SET_DIGITAL, { deviceKey, channel })
  ),

  setAnalog: (deviceKey: EntityKey, channel: number, state: InputEditor.State<number | null, number>) => (
    buildAction(ActionTypes.SET_ANALOG, { deviceKey, channel, state })
  ),

  setError: (deviceKey: EntityKey, error: { message: string; details: string } | null) => (
    buildAction(ActionTypes.SET_ERROR, { deviceKey, error })
  ),

  setLabel: (deviceKey: EntityKey, portType: IoPortType, channelNumber: number, newValue: string) => (
    buildAction(ActionTypes.SET_LABEL, { deviceKey, portType, channelNumber, newValue })
  ),
  setLabelDevice: (deviceKey: EntityKey, portType: IoPortType, channelNumber: number, newValue: string, prevValue: string) => (
    buildAction(ActionTypes.SET_LABEL_DEVICE, { deviceKey, portType, channelNumber, newValue, prevValue })
  ),
  setLabelError: (
    deviceKey: EntityKey, portType: IoPortType, channelNumber: number,
    prevValue: string, error: { message: string; details: string } | null
  ) => (
    buildAction(ActionTypes.SET_LABEL_ERROR, { deviceKey, portType, channelNumber, prevValue, error })
  ),
  deleteLabelError: (deviceKey: EntityKey, ioErrorKey: string) => (
    buildAction(ActionTypes.DELETE_LABEL_ERROR, { deviceKey, ioErrorKey })
  ),

  loadLabelsDevice: (deviceKey: EntityKey) => (
    buildAction(ActionTypes.LOAD_LABELS_DEVICE, { deviceKey })
  ),
};
