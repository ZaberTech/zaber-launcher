import React from 'react';
import { Velocity, Length, Angle, AngularVelocity } from '@zaber/motion';
import { Icons, Text } from '@zaber/react-library';

import { ExternalLink } from '../components';
import { getUnitLabel } from '../units';
import { useActions } from '../utils';

import { actions as actionDefinitions } from './actions';
import type { Path } from './types';


const DOC_URL = 'https://www.zaber.com/w/Software/PVT_File_Format';

export const EXAMPLE_DATA_LINEAR: Path = {
  positionUnit: Length.MILLIMETRES,
  velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
  series: [{
    name: 'Horizontal',
    points: [
      { dt: 0, time: 0, pos: 5, vel: 0 },
      { dt: 2, time: 2, pos: 20, vel: 10 },
      { dt: 1, time: 3, pos: 20, vel: -8 },
      { dt: 3, time: 6, pos: 5, vel: -8 },
      { dt: 1, time: 7, pos: 5, vel: 10 },
      { dt: 2, time: 9, pos: 20, vel: 0 },
    ],
  }, {
    name: 'Vertical',
    points: [
      { dt: 0, time: 0, pos: 20, vel: 0 },
      { dt: 2, time: 2, pos: 20, vel: 0 },
      { dt: 1, time: 3, pos: 20, vel: -8 },
      { dt: 3, time: 6, pos: 5, vel: -8 },
      { dt: 1, time: 7, pos: 5, vel: 0 },
      { dt: 2, time: 9, pos: 5, vel: 0 },
    ],
  }],
};


export const EXAMPLE_DATA_ROTARY: Path = {
  positionUnit: Angle.DEGREES,
  velocityUnit: AngularVelocity.DEGREES_PER_SECOND,
  series: [{
    name: 'Pitch',
    points: [
      { dt: 0, time: 0, pos: 0, vel: 0 },
      { dt: 2, time: 2, pos: 15, vel: 5 },
      { dt: 2, time: 4, pos: 0, vel: 0 },
      { dt: 2, time: 6, pos: 0, vel: 0 },
      { dt: 2, time: 8, pos: 0, vel: 0 },
      { dt: 2, time: 10, pos: 0, vel: 0 },
      { dt: 2, time: 12, pos: -15, vel: -5 },
      { dt: 2, time: 14, pos: 0, vel: 0 },
    ],
  }, {
    name: 'Roll',
    points: [
      { dt: 0, time: 0, pos: 0, vel: 0 },
      { dt: 2, time: 2, pos: 0, vel: 0 },
      { dt: 2, time: 4, pos: 0, vel: 0 },
      { dt: 2, time: 6, pos: -45, vel: 0 },
      { dt: 2, time: 7, pos: 45, vel: 0 },
      { dt: 2, time: 10, pos: 0, vel: 0 },
      { dt: 2, time: 12, pos: 0, vel: 0 },
      { dt: 2, time: 14, pos: 0, vel: 0 },
    ],
  }],
};


export const WelcomeScreen: React.FC = () => {
  const actions = useActions(actionDefinitions);

  return <div className="welcome-screen">
    <Text className="link" onClick={actions.fileOpen}>Import a CSV</Text><Text> file to start.</Text>
    <div className="info-grid">
      <Icons.ErrorWarning className="info-icon"/>
      <Text>
        <p>In order to use the PVT Viewer app effectively, the CSV file should contain one row of headers,
          one row defining the starting state, and one row for each point in the sequence.
          Additionally, the file must contain:</p>
        <ul>
          <li>One time column. The corresponding header should contain the word "time" and the bracket-enclosed units.</li>
          <li>One position column for each axis in the sequence. The corresponding headers should contain the chosen
            name for the axis, the word "pos" or "position", and the bracket-enclosed units. All position columns
            must share the same units.</li>
          <li>One velocity column for each axis in the sequence. The corresponding headers should contain the
            chosen name for the axis, the word "vel" or "velocity", and the bracket-enclosed units. All velocity
            columns must share the same units.</li>
        </ul>
        <p>Note that initial and final velocities must be zero, and the initial time must be zero.</p>
        <p>Example:</p>
        <table>
          <thead><tr>
            <th>Time (s)</th>
            {EXAMPLE_DATA_LINEAR.series.map(s => <React.Fragment key={s.name}>
              <th>{`${s.name} position (${getUnitLabel(EXAMPLE_DATA_LINEAR.positionUnit)})`}</th>
              <th>{`${s.name} velocity (${getUnitLabel(EXAMPLE_DATA_LINEAR.velocityUnit)})`}</th>
            </React.Fragment>)}
          </tr></thead>
          <tbody>
            {EXAMPLE_DATA_LINEAR.series[0].points.map((p, i) => <tr key={i}>
              <td>{p.dt}</td>
              {EXAMPLE_DATA_LINEAR.series.map(s => <React.Fragment key={s.name}>
                <td>{s.points[i].pos}</td>
                <td>{s.points[i].vel}</td>
              </React.Fragment>)}
            </tr>)}
          </tbody>
        </table>
        <p>
          <Text className="link" onClick={() => actions.fileSaveAndOpenExample(EXAMPLE_DATA_LINEAR)}>Click here</Text> to
          save this example as a CSV file and open it in this app. If you are using rotary devices,
          use <Text className="link" onClick={() => actions.fileSaveAndOpenExample(EXAMPLE_DATA_ROTARY)}>this example</Text> instead.
        </p>
        <p>More detailed formatting information is available on the <ExternalLink url={DOC_URL}>Zaber website</ExternalLink></p>
      </Text>
    </div>
  </div>;
};
