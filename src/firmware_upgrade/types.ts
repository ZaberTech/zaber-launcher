import { SetStateDeviceResponse } from '@zaber/motion/ascii';
import { DeviceIdentifiers, Version } from '../app_components';
import type { EntityKey } from '../keys';
import type { LoadErrorDetails } from '../save_load';


/** The timeout to use for commands that take longer than usual, in milliseconds */
export const LONG_RUNNING_COMMAND_TIMEOUT = { timeout: 10 * 1000 };

export type UpgradeProgress = {
  step: 'connecting' | 'canceling' | 'cancelled' | 'time_out' | 'checking-device' |
    'downloading-fwu' | 'preparing-stream' | 'applying' | 'reporting-success';
} | {
  step: 'ask-if-save';
  installParams: InstallParams;
} | {
  step: 'ask-if-save' | 'could-not-save';
  installParams: InstallParams;
  error: string;
} | {
  step: 'saving-state' | 'reloading-state';
} | {
  step: 'streaming';
  bytesWritten: number;
  bytesTotal: number;
  remainingTime?: number;
} | {
  step: 'failing';
} | {
  step: 'failed';
  error: string;
  wasReset: boolean;
} | ({
  step: 'failed-reload';
  pathToSaveFile: string;
  error: string;
  details?: React.ComponentProps<typeof LoadErrorDetails>['details'];
}) | {
  step: 'done';
  version?: Version;
  setStateResponse?: SetStateDeviceResponse;
};

export type VersionSkipMap = Record<number, Version>;

export type InstallParams = {
  deviceKey: EntityKey;
  identifiers: DeviceIdentifiers;
  filePath: string;
  version?: Version;
};

export interface CachedConnectionFirmwareUpgradeInfo {
  setAtTimestamp: number;
  info: Record<string, CachedDeviceFirmwareUpgradeInfo>;
}

export interface CachedDeviceFirmwareUpgradeInfo {
  identifiers: DeviceIdentifiers;
  versions: Version[];
}

export type VersionCategory = 'public' | 'other' | 'internal';
