import { Flex, Icons, Input } from '@zaber/react-library';
import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';

import { useActions } from '../utils';
import { EntityKey } from '../keys';

import {
  selectCommandSuggestions,
  selectCommandWithMeasurements,
  selectConnectionState,
  selectContextPrefix,
  selectCurrentCommand,
  selectIsCurrentCommandFromHistory,
  selectSelectedSuggestion,
  selectSuggestedCommand,
} from './selectors';
import { CommandSuggestions, UnitConversionNote } from './CommandSuggestions';
import { ConnectionStates } from './types';
import { actions as actionDefinitions } from './actions';
import { getLongestMatchingSuggestion, splitAndCleanCommands, suggestionToText } from './utils';


interface CommandInputProps {
  selectedKey: EntityKey | null;
}

export const CommandInput: React.FC<CommandInputProps> = ({ selectedKey }) => {
  const actions = useActions(actionDefinitions);

  const connectionState = useSelector(selectConnectionState);
  const contextPrefix = useSelector(selectContextPrefix);
  const currentCommand = useSelector(selectCurrentCommand);
  const isCurrentCommandFromHistory = useSelector(selectIsCurrentCommandFromHistory);
  const commandSuggestions = useSelector(selectCommandSuggestions);
  const commandWithMeasurements = useSelector(selectCommandWithMeasurements);
  const selectedSuggestion = useSelector(selectSelectedSuggestion);
  const suggestedCommand = useSelector(selectSuggestedCommand);

  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputRef.current?.focus();
  }, [selectedKey, inputRef.current]);

  const longestMatchingSuggestion = getLongestMatchingSuggestion(commandSuggestions);
  const longestMatchingSuggestionText = suggestionToText(longestMatchingSuggestion, currentCommand);
  const controlsAreDisabled = connectionState !== ConnectionStates.Connected;
  const hasUnits = commandWithMeasurements.measurements.length > 0;

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    switch (event.key!) {
      case 'Enter':
        event.preventDefault();
        if (suggestedCommand != null) {
          actions.setCommandInput(suggestionToText(suggestedCommand.parts, currentCommand));
        } else {
          actions.sendCommand(currentCommand);
        }
        break;
      case 'ArrowUp':
        event.preventDefault();
        if (currentCommand === '' || isCurrentCommandFromHistory) {
          actions.setCommandHistoryOlder();
        } else {
          const newSuggestion = selectedSuggestion != null ? selectedSuggestion + 1 : 0;
          if (newSuggestion < commandSuggestions.length) {
            actions.setSelectedSuggestion(newSuggestion);
          }
        }
        break;
      case 'ArrowDown':
        event.preventDefault();
        if (isCurrentCommandFromHistory) {
          actions.setCommandHistoryNewer();
        } else if (selectedSuggestion != null) {
          const newSuggestion = selectedSuggestion > 0 ? selectedSuggestion - 1 : null;
          actions.setSelectedSuggestion(newSuggestion);
        }
        break;
      case 'Tab':
        event.preventDefault();
        if (longestMatchingSuggestionText.length > currentCommand.length) {
          actions.setCommandInput(longestMatchingSuggestionText);
        }
        break;
      case 'Escape':
        actions.setCommandInput('');
        break;
    }
  };

  return <div className="controls">
    <div className="command-text">
      <div className="command-prefix" data-testid="command-prefix">
        <span>{contextPrefix}</span>
      </div>
      <Flex.Column className="command-input-column">
        <CommandSuggestions
          onSuggestionClick={command => {
            actions.setCommandInput(command);
            inputRef.current?.focus();
          }}
        />
        <Flex.Row className={classNames('command-input-row', { selected: suggestedCommand == null })}>
          <Input
            className={classNames('command-input', { 'has-units': hasUnits })}
            ref={inputRef}
            placeholder="Type Commands Here... ex. home"
            value={currentCommand}
            onKeyDown={handleKeyDown}
            onValueChange={actions.setCommandInput}
            onPaste={event => {
              const textData = event.clipboardData.getData('text/plain').trim();
              const lines = splitAndCleanCommands(textData);
              if (lines.length > 1) {
                actions.commandBatchEdit(textData);
                event.preventDefault();
              }
              return;
            }}
            disabled={controlsAreDisabled}
            spellCheck={false}
            autoCorrect="off"
            autoCapitalize="none"
            title="Terminal input"
          />
          {hasUnits && <UnitConversionNote/>}
        </Flex.Row>
      </Flex.Column>
    </div>
    <Icons.Enter
      onClick={() => actions.sendCommand(currentCommand)}
      disabled={controlsAreDisabled}
      title="Send"/>
  </div>;
};
