import React from 'react';
import { Toggle, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectHolding } from './selectors';

export const FreeHold: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const holding = useSelector(selectHolding);
  return (<div className="free-hold">
    <Text>Freehold</Text>
    <Toggle
      title="Freehold"
      value={holding}
      onValueChange={actions.setFreeHold}/>
  </div>);
};
