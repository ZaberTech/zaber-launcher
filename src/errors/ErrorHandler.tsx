import React, { Component } from 'react';
import * as Sentry from '@sentry/react';

import { ErrorReport } from './ErrorReport';
import { errorEmitter } from './errors';

interface State {
  hasError?: true;
}

interface Props {
  children: React.ReactNode;
}

export class ErrorHandler extends Component<Props, State> {
  private fallback = () => (<ErrorReport/>);

  constructor(props: Props) {
    super(props);
    this.state = { };

    errorEmitter.on('error', this.onError);
  }

  public componentWillUnmount(): void {
    errorEmitter.off('error', this.onError);
  }

  private onError = (): void => {
    this.setState({
      hasError: true,
    });
  };

  public render(): React.ReactNode {
    const { hasError } = this.state;
    if (hasError) {
      return <ErrorReport/>;
    }

    return (
      <Sentry.ErrorBoundary fallback={this.fallback}>
        {this.props.children}
      </Sentry.ErrorBoundary>
    );
  }
}
