import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { Button, ButtonPrevious, ButtonRow } from '@zaber/react-library';

import { LocalShareDiscover as Discover } from '../local_share/LocalShareDiscover';

import { actions as actionsDefinition } from './actions';
import { Steps } from './types';

export const DiscoverLocalShare: React.FC = () => {
  const actions = useActions(actionsDefinition);
  return (<div className="discover-local-share">
    <Discover onRouterAdded={() => actions.changeDialogOpen(false)}/>
    <ButtonRow justify="spread">
      <ButtonPrevious onClick={() => actions.changeStep(Steps.CHOOSE_CONNECTION_TYPE)}>Back</ButtonPrevious>
      <Button color="grey" onClick={() => actions.changeStep(Steps.CONNECT_LOCAL_SHARE)}>Connect Manually</Button>
    </ButtonRow>
  </div>);
};
