import { Button, Icons, Loader, Modal, Text } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { match, P } from 'ts-pattern';
import { ModalButtons } from '@zaber/react-library/dist/types/elements/Modal/Modal';

import { tryExtractConnectionKey } from '../keys';

import { actions as actionsDefinition } from './actions';
import type { ValidationError } from './validators';
import { HwModDeviceOrAxisState, selectApplyProgress, selectDeviceOrAxisInfo } from './selectors';
import { supportLink, troubleshootingWiki } from './components/links';

export type ApplyProgress = {
  step: 'complete' | 'validating' | 'applying';
} | {
  step: 'pre-validation-errors' | 'post-validation-errors';
  errors: ValidationError[];
} | {
  step: 'errors-on-apply';
  total: number;
  errors: ValidationError[];
};

const getHardwareModificationModalContent = (progress: ApplyProgress, info: HwModDeviceOrAxisState, actions: typeof actionsDefinition) =>
  match(progress)
    .returnType<{content: React.ReactNode; buttons?: ModalButtons}>()
    .with({ step: 'validating' }, () => ({
      content:
        <div className="working">
          <Loader size="large"/>
          <Text>Validating changes...</Text>
        </div>
    }))
    .with({ step: 'applying' }, () => ({
      content:
        <div className="working">
          <Loader size="large"/>
          <Text>Writing the changes to the device...</Text>
        </div>
    }))
    .with({ step: 'complete' }, () => {
      const connectionKey = tryExtractConnectionKey(info.key);
      const basicMovementLink = <NavLink className="link-hover" onClick={() => actions.setApplyProgress()}
        to={`/basic-movement?selected=${connectionKey}`}>
        Basic Movement
      </NavLink>;
      const terminalLink = <NavLink className="link-hover" onClick={() => actions.setApplyProgress()}
        to={`/terminal?selected=${connectionKey}`}>
        Terminal
      </NavLink>;

      let name = `the ${info.device.identity.name}`;
      if (info.axis) {
        name = `axis ${info.axis.axisNumber} of the ${info.device.identity.name}`;
      }

      return {
        content:
          <div className="info-grid">
            <Icons.Confirmation className="success icon"/>
            <Text t={Text.Type.H4} className="title">Completed</Text>
            <div className="info">
              <Text>Your changes have been successfully applied to {name}.</Text>
              <Text>
              Please use {basicMovementLink} and/or {terminalLink} to confirm that it behaves as expected before continuing to use it.
              See {troubleshootingWiki} for suggestions of things to check and troubleshooting information.
              </Text>
              <Text>
              If you need further help configuring this product, please contact {supportLink}.
              </Text>
            </div>
          </div>,
        buttons:
          <Button onClick={() => actions.setApplyProgress()} title="Close Modal">OK</Button>
      };
    })
    .with({ step: 'errors-on-apply' }, ({ errors, total }) => ({
      content:
        <div className="info-grid">
          <Text t={Text.Type.H4} className="title">Could not apply all updates</Text>
          <div className="info">
            <Text>There were errors applying changes to {errors.length} of {total} fields</Text>
            <Text>Please fix the errors and try again</Text>
            <Text>If you need help configuring this product, please contact {supportLink}.</Text>
          </div>
        </div>,
      buttons:
        <Button onClick={() => actions.scrollToError(errors[0])} title="Close Modal">OK</Button>
    }))
    .with({ step: P.union('pre-validation-errors', 'post-validation-errors') }, ({ errors }) => ({
      content:
        <div className="info-grid">
          <Text t={Text.Type.H4} className="title">Invalid update</Text>
          <div className="info">
            <Text>This update would put the device in an invalid state</Text>
            <Text>Please fix the errors and try again</Text>
          </div>
        </div>,
      buttons:
        <Button onClick={() => actions.scrollToError(errors[0])} title="Close Modal">OK</Button>
    }))
    .exhaustive();

export const ApplyHardwareModificationProgressModal: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const info = useSelector(selectDeviceOrAxisInfo);
  const progress = useSelector(selectApplyProgress);

  if (info == null || progress == null) { return null }

  const entityName = info.axis ? `Axis ${info.axis.axisNumber} of ${info.device.identity.name}` : info.device.identity.name;

  const { content, buttons } = getHardwareModificationModalContent(progress, info, actions);

  return (
    <Modal
      headerIcon={<Icons.HardwareModification/>}
      headerText={`${entityName} Hardware Modification`}
      className="apply-progress"
      small={['validating', 'applying'].includes(progress.step)}
      bodyIcon={['errors-on-apply', 'pre-validation-errors', 'post-validation-errors'].includes(progress.step) ? 'error' : undefined}
      buttons={buttons}
    >
      {content}
    </Modal>
  );
};
