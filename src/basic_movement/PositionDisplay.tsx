import React, { Component } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import { Units } from '@zaber/motion';

import { convertNullable, getUnits, UnitInfo, ValueDisplay } from '../units';
import { round } from '../utils';

import { TrackHoverInfo } from './TrackHoverInfo';
import type { AxisStateComplete } from './selectors';


export const INDICATOR_WIDTH_PX = 36;
export const END_STOP_WIDTH_PX = 16;
const TOTAL_TRACK_FILL = INDICATOR_WIDTH_PX + END_STOP_WIDTH_PX * 2;

export interface Props {
  axis: AxisStateComplete;
  posUnitInfo: UnitInfo | null;
  deviceIsMoving?: boolean;
  onRequestMovement: (positionNative: number) => void;
}

export interface State {
  hoverXPosition: number;
  hoverDevicePositionFraction: number;
  clickXPosition: number;
  clickDevicePositionFraction: number;
  deviceMovingDueToTrackClick: boolean;
  touching: boolean;
}

type MouseOrTouch = React.MouseEvent<HTMLDivElement> | React.TouchEvent<HTMLDivElement>;

function getClientX(e: MouseOrTouch) {
  if ('changedTouches' in e) {
    return e.changedTouches[0].clientX;
  } else {
    return e.clientX;
  }
}

export class PositionDisplay extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      hoverXPosition: 0,
      hoverDevicePositionFraction: 0,
      clickXPosition: 0,
      clickDevicePositionFraction: 0,
      deviceMovingDueToTrackClick: false,
      touching: false,
    };
  }

  static getDerivedStateFromProps(nextProps: Props, prevState: State): Partial<State> | null {
    if (prevState.deviceMovingDueToTrackClick && !nextProps.deviceIsMoving) {
      return {
        deviceMovingDueToTrackClick: false,
      };
    }
    return null;
  }

  onMoveToPosition(e: MouseOrTouch) {
    const { axis: { travelInfo }, onRequestMovement } = this.props;
    if (!travelInfo) {
      return;
    }
    const positionFraction = this.devicePositionFraction(e);
    let positionNative: number;
    if (travelInfo.cycleDistance === 'not-cyclic') {
      positionNative = _.round(travelInfo.min + travelInfo.distance * positionFraction);
    } else {
      positionNative = _.round(travelInfo.cycleDistance * positionFraction);
    }
    onRequestMovement(positionNative);

    this.setState({
      clickXPosition: this.getTrackInfoPosition(e),
      clickDevicePositionFraction: positionFraction,
      deviceMovingDueToTrackClick: true,
    });
  }

  getTrackInfoPosition(e: MouseOrTouch) {
    const boundingRect = (e.currentTarget as HTMLElement).getBoundingClientRect();
    return END_STOP_WIDTH_PX + _.clamp(
      getClientX(e) - boundingRect.left - END_STOP_WIDTH_PX,
      0, boundingRect.width - END_STOP_WIDTH_PX * 2);
  }

  onTrackHover = (e: MouseOrTouch) => {
    this.setState({
      hoverXPosition: this.getTrackInfoPosition(e),
      hoverDevicePositionFraction: this.devicePositionFraction(e),
    });
  };

  onMouseUp = (e: React.MouseEvent<HTMLDivElement>) => {
    if (e.button !== 0) {
      return;
    }
    this.onMoveToPosition(e);
  };

  onTouchStart = (e: React.TouchEvent<HTMLDivElement>) => {
    this.setState({ touching: true });
    this.onTrackHover(e);
  };

  onTouchEnd = (e: React.TouchEvent<HTMLDivElement>) => {
    this.setState({ touching: false });
    this.onMoveToPosition(e);
  };

  devicePositionFraction = (e: MouseOrTouch): number => {
    const { posUnitInfo } = this.props;
    // There is reserved space at both ends of the track to accommodate the thickness of the
    // position indicator because the precise location of the device is represented by the
    // center of the position indicator.
    const boundingRect = (e.currentTarget as HTMLElement).getBoundingClientRect();
    const mouseX = Math.max(getClientX(e) - (boundingRect.left + (INDICATOR_WIDTH_PX / 2) + END_STOP_WIDTH_PX), 0);
    let fraction = _.clamp(mouseX / (boundingRect.width - TOTAL_TRACK_FILL), 0, 1);
    const { axis: { position, travelInfo } } = this.props;
    if (travelInfo!.cycleDistance !== 'not-cyclic') {
      const positionNative = round(convertNullable(posUnitInfo, position.value, getUnits(position), Units.NATIVE));
      if (positionNative != null) {
        fraction += Math.floor(positionNative / travelInfo!.cycleDistance);
      }
    }
    return fraction;
  };

  render(): React.ReactNode {
    const { axis: { travelInfo, key, position }, deviceIsMoving, posUnitInfo } = this.props;
    const {
      hoverXPosition,
      hoverDevicePositionFraction,
      clickXPosition,
      clickDevicePositionFraction,
      deviceMovingDueToTrackClick,
      touching,
    } = this.state;

    const units = getUnits(position);
    const positionNative = round(convertNullable(posUnitInfo, position?.value, units, Units.NATIVE));

    if (!travelInfo || positionNative == null) {
      return null;
    }

    let actualPositionFraction: number;
    if (travelInfo.cycleDistance === 'not-cyclic') {
      actualPositionFraction = (positionNative - travelInfo.min) / travelInfo.distance;
    } else {
      let cyclicPosition = positionNative % travelInfo.cycleDistance;
      if (cyclicPosition < 0) {
        cyclicPosition += travelInfo.cycleDistance;
      }
      actualPositionFraction = cyclicPosition / travelInfo.cycleDistance;
    }

    const hoverInfoXPosition = deviceIsMoving ? clickXPosition : hoverXPosition;
    const hoverInfoDevicePosFraction = deviceIsMoving ? clickDevicePositionFraction : hoverDevicePositionFraction;
    const hoverInfoDevicePositionNative = hoverInfoDevicePosFraction * travelInfo.distance;

    return (
      <div
        className={classnames('interactive-track', { activated: !!deviceIsMoving || touching })}
        data-testid="interactive-track"
        onMouseMove={this.onTrackHover} onMouseUp={this.onMouseUp}
        onTouchStart={this.onTouchStart} onTouchEnd={this.onTouchEnd} onTouchMove={this.onTrackHover}>
        {(deviceMovingDueToTrackClick || !deviceIsMoving) && <TrackHoverInfo
          persistent={deviceMovingDueToTrackClick || touching}
          leftPosition={hoverInfoXPosition}>
          <ValueDisplay entityKey={key} setting="pos" value={hoverInfoDevicePositionNative} unit={units}/>
        </TrackHoverInfo>}
        <div className="end-stop left"/>
        <div className="empty-track" style={{
          width: `calc(${actualPositionFraction * 100}% - ${actualPositionFraction * TOTAL_TRACK_FILL}px)`,
        }}/>
        <div className="indicator-fill"/>
        <div className="empty-track" style={{
          width: `calc(${(1 - actualPositionFraction) * 100}% - ${(1 - actualPositionFraction) * TOTAL_TRACK_FILL}px)`,
        }}/>
        <div className="end-stop right"/>
      </div>
    );
  }
}
