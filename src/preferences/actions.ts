import { actionBuilder } from '../utils';

import { PreferenceValue } from './types';

export enum ActionTypes {
  SET_NAME = 'PREFERENCES_SET_NAME',
  SET_NAME_DONE = 'PREFERENCES_SET_NAME_DONE',
  SET_PREFERENCE = 'PREFERENCES_SET_PREFERENCE',
  PREFERENCE_UPDATED = 'PREFERENCES_PREFERENCE_UPDATED',
}

export interface ActionsToPayloads {
  [ActionTypes.SET_NAME]: { name: string | null };
  [ActionTypes.SET_NAME_DONE]: { err?: string };
  [ActionTypes.SET_PREFERENCE]: { key: string; value: PreferenceValue; broadcast: boolean };
  [ActionTypes.PREFERENCE_UPDATED]: { key: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  setName: (name: string) => buildAction(ActionTypes.SET_NAME, { name }),
  setNameDefault: () => buildAction(ActionTypes.SET_NAME, { name: null }),
  setNameDone: (err?: string) => buildAction(ActionTypes.SET_NAME_DONE, { err }),
  setPreference: (key: string, value: PreferenceValue, broadcast: boolean = true) =>
    buildAction(ActionTypes.SET_PREFERENCE, { key, value, broadcast }),
  preferenceUpdated: (key: string) => buildAction(ActionTypes.PREFERENCE_UPDATED, { key }),
};
