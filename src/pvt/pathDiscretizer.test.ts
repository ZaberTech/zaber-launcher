import { toBeDeepCloseTo } from 'jest-matcher-deep-close-to';
import { Length, Velocity } from '@zaber/motion';

import type { Path, Series } from './types';
import { PathDiscretizer } from './pathDiscretizer';
import { PvtSeriesHelper } from './pvtSeriesHelper';

expect.extend({ toBeDeepCloseTo });


describe('Path discretization by time', () => {
  describe.each([{
    velUnit: Velocity.MILLIMETRES_PER_SECOND,
    unitScale: 1,
    series: {
      name: 'X',
      points: [
        { dt: 1, time: 1, pos: 0, vel: 0 },
        { dt: 3, time: 4, pos: 10, vel: 10 },
        { dt: 3, time: 7, pos: 25, vel: 0 },
        { dt: 3, time: 10, pos: 10, vel: -10 },
        { dt: 3, time: 13, pos: 0, vel: 0 }],
    }
  }, {
    velUnit: Velocity.METRES_PER_SECOND,
    unitScale: 0.001,
    series: {
      name: 'X',
      points: [
        { dt: 1, time: 1, pos: 0, vel: 0 },
        { dt: 3, time: 4, pos: 10, vel: 0.01 },
        { dt: 3, time: 7, pos: 25, vel: 0 },
        { dt: 3, time: 10, pos: 10, vel: -0.01 },
        { dt: 3, time: 13, pos: 0, vel: 0 }],
    }
  },
  ])('of U test data with differing units of velocity (params: %p)', ({ velUnit, unitScale, series }) => {
    let output: Series;

    beforeAll(async () => {
      const discretizer = new PathDiscretizer({
        series: [series],
        positionUnit: Length.mm,
        velocityUnit: velUnit,
      });
      output = discretizer.discretizeToPath().series[0];
      const helper = PvtSeriesHelper.FromScales(series, discretizer.velocityScale, discretizer.accelerationScale);
      output.positionRange = helper.findPositionRange();
      output.velocityRange = helper.findVelocityRange();
      output.accelerationRange = helper.findAccelerationRange();
    });


    it('correctly finds the position limits', () => {
      expect(output.positionRange?.min.value).toBeCloseTo(0);
      expect(output.positionRange?.max.value).toBeCloseTo(25);
      expect(output.positionRange?.max.time).toBeCloseTo(3 + 3 + 1);
    });


    it('correctly finds the velocity limits', () => {
      expect(output.velocityRange?.min.value).toBeCloseTo(-10 * unitScale);
      expect(output.velocityRange?.min.time).toBeCloseTo(3 + 3 + 3 + 1);
      expect(output.velocityRange?.max.value).toBeCloseTo(10 * unitScale);
      expect(output.velocityRange?.max.time).toBeCloseTo(3 + 1);
    });


    it('correctly finds the acceleration limits', () => {
      expect(output.accelerationRange?.min.value).toBeCloseTo(-3.333 * unitScale);
      expect(output.accelerationRange?.max.value).toBeCloseTo(6.666 * unitScale);
    });


    it('passes through the control points in order', () => {
      let i = 0;
      for (const x of [0, 10, 25, 10, 0]) {
        while (i < output.points.length) {
          if (Math.abs(output.points[i].pos - x) < 0.01) {
            break;
          }

          i++;
        }

        expect(i).toBeLessThan(output.points.length);
      }
    });
  });
});


test('Path discretization by time finds correct position range when a segment is not a cubic curve (regression test)', async () => {
  const path: Path = {
    positionUnit: Length.mm,
    velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
    series: [{
      name: 'X',
      points: [
        { dt: 0, time: 0, pos: 70, vel: 0 },
        { dt: 1, time: 1, pos: 1, vel: -10 },
        { dt: 1, time: 2, pos: 1, vel: 10 },
        { dt: 1, time: 3, pos: 50, vel: 0 },
      ],
    }],
  };

  const discretizer = new PathDiscretizer(path);
  const output = discretizer.discretizeToPath().series[0];
  const helper = PvtSeriesHelper.FromScales(output, discretizer.velocityScale, discretizer.accelerationScale);
  const positionRange = helper.findPositionRange();
  expect(positionRange).toBeDeepCloseTo({ min: { time: 1.5, value: -1.5 }, max: { time: 0, value: 70 } });
});
