export const internalReducer = (state: unknown = {}) => state;

// eslint-disable-next-line @typescript-eslint/no-empty-function
export function* internalSaga() {
}

export const InternalRouter = () => null;


export type InternalState = object;
