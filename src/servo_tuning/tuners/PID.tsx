import React from 'react';
import { useSelector } from 'react-redux';
import { HelpTooltip, NumericInput, Text } from '@zaber/react-library';
import type { PidTuning } from '@zaber/motion/ascii';

import { NoContentMessage } from '../../components';
import { selectTuningInfo } from '../selectors';
import type { ChosenParamset } from '../types';
import { actionDefinitions } from '../actions';
import { useActions } from '../../utils';
import { StatusMessage } from '../components/StatusMessage';
import { ApplyButtons } from '../components/ApplyButtons';

export const PID: React.FC<ChosenParamset> = ({ entity, paramset }) => {
  const actions = useActions(actionDefinitions);
  const info = useSelector(selectTuningInfo);
  if (info?.method !== 'pid' || info?.paramset !== paramset) {
    return <NoContentMessage message="Getting tuning..." type="working"/>;
  }
  const tuning = info.tuning;
  const setParam = (paramName: keyof PidTuning) => (
    (value: number | null) => {
      actions.updateParamTuning({ ...info, tuning: { ...tuning, [paramName]: value } });
    }
  );
  return <div className="pid">
    <Text>Enter PID parameters to tune your device.</Text>
    <div className="tuning">
      <Text t={Text.Type.H5}>kp</Text>
      <HelpTooltip>
        <Text e={Text.Emphasis.Bold}>Proportional Gain:</Text> This controls how much control effort will be applied
        to correct measured positioning error. A larger number produces stiffer behavior, but can also cause overshoot
        and degrade stability.
      </HelpTooltip>
      <NumericInput value={tuning.p} onNumberChange={setParam('p')} data-testid="kp"/>
      <Text>N/m</Text>

      <Text t={Text.Type.H5}>ki</Text>
      <HelpTooltip>
        <Text e={Text.Emphasis.Bold}>Integral Gain:</Text> Governs control response in proportion to the error history.
        Increasing it will decrease setting time and eliminate steady-state error, but can also cause overshoot and degrade stability.
      </HelpTooltip>
      <NumericInput value={tuning.i} onNumberChange={setParam('i')} data-testid="ki"/>
      <Text>N/m•s</Text>

      <Text t={Text.Type.H5}>kd</Text>
      <HelpTooltip>
        <Text e={Text.Emphasis.Bold}>Derivative Gain:</Text> Governs control response in proportion to the rate at which the stage
        is approaching the target position. Increasing it increases stability, decreases overshoot, and damps oscillations,
        but can also lower setting time and increase noise.
      </HelpTooltip>
      <NumericInput value={tuning.d} onNumberChange={setParam('d')} data-testid="kd"/>
      <Text>N•s/m</Text>

      <Text t={Text.Type.H5}>fc</Text>
      <HelpTooltip>
        <Text e={Text.Emphasis.Bold}>Low Pass Filter Cutoff Frequency:</Text> Lower values produce more stability
        and reduce audible noise but also cause slower settling.
      </HelpTooltip>
      <NumericInput value={tuning.fc} onNumberChange={setParam('fc')} data-testid="fc"/>
      <Text>Hz</Text>
    </div>
    <ApplyButtons paramset={paramset} onClick={(toLive, toPowerOn) => actions.applyPid(entity, paramset, tuning, toLive, toPowerOn)}/>
    <StatusMessage message={info?.message} paramset={paramset}/>
  </div>;
};
