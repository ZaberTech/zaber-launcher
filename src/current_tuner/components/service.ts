import { inject, injectable } from 'inversify';
import type { FirmwareVersion } from '@zaber/motion';

import { Request } from '../../app_components';
import { environment } from '../../environment';
import type { Setting, Sliders, TuningInfo } from '../types';

@injectable()
export class CurrentTunerAPI {
  constructor(@inject(Request) private readonly request: Request) {
  }

  public async getCurrentControllerParams(
    inputSettings: Setting[],
    firmwareVersion: FirmwareVersion,
    sliderPositions: Sliders,
  ): Promise<Setting[]> {
    return await this.request.request<Setting[]>(`${environment.apiUrl}/current-tuner/controller-params`, 'POST', {
      inputSettings,
      firmwareVersion,
      sliderPositions,
    });
  }

  public async getSliderPositions(
    inputSettings: Setting[],
    firmwareVersion: FirmwareVersion,
    tuningParameters: Setting[],
  ): Promise<Sliders | null> {
    return await this.request.request<Sliders | null>(`${environment.apiUrl}/current-tuner/slider-positions`, 'POST', {
      inputSettings,
      firmwareVersion,
      tuningParameters,
    });
  }

  public async getInfo(
    firmwareVersion: FirmwareVersion,
  ): Promise<TuningInfo> {
    return await this.request.request<TuningInfo>(`${environment.apiUrl}/current-tuner/info`, 'POST', {
      firmwareVersion,
    });
  }
}
