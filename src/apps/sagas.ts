import type { SagaIterator } from 'redux-saga';
import { fork, put } from 'redux-saga/effects';

import { getContainer } from '../container';
import { environment } from '../environment';
import { Storage } from '../app_components';
import { reduxStoreSymbol, OurStore } from '../store';

import { actions } from './actions';
import { AppUrls, APP_DICTIONARY, DEFAULT_RECENT_APPS, DEFAULT_PINNED_APPS } from './list';
import { selectAppsToStore } from './selectors';

export function* rootSaga(): SagaIterator {
  if (environment.isTest) { return }

  registerPageUnload();
  yield fork(loadRecent);
}

export const PINNED_APPS_KEY = 'PINNED_APPS';
export const RECENT_APPS_KEY = 'RECENT_APPS';
type StoredApps = AppUrls[];

export function registerPageUnload() {
  window.addEventListener('unload', () => {
    const container = getContainer();
    const storage = container.get(Storage);
    const store = container.get<OurStore>(reduxStoreSymbol);

    const { recent, pinned } = selectAppsToStore(store.getState());
    storage.save<StoredApps>(RECENT_APPS_KEY, recent);
    storage.save<StoredApps>(PINNED_APPS_KEY, pinned);
  }, { once: true });
}

export function* loadRecent(): SagaIterator {
  const container = getContainer();
  const storage = container.get(Storage);

  const recentApps = storage.load<StoredApps>(RECENT_APPS_KEY) ?? DEFAULT_RECENT_APPS;
  const pinnedApps = storage.load<StoredApps>(PINNED_APPS_KEY) ?? DEFAULT_PINNED_APPS;
  yield put(actions.storedAppsLoaded(
    recentApps.filter(url => url in APP_DICTIONARY && !APP_DICTIONARY[url].deprecated),
    pinnedApps.filter(url => url in APP_DICTIONARY && !APP_DICTIONARY[url].deprecated),
  ));
}
