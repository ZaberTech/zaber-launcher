import _ from 'lodash';

import { createReducer } from '../utils';
import type { ConnectionManagerActionPayloads } from '../connection_manager';

import { ActionsToPayloads, ActionTypes } from './actions';

export interface DeviceOrPeripheralSetup {
  serialNo: number;
  path: string;
}

export type PendingSetups = Record<number, DeviceOrPeripheralSetup>;

export interface State {
  pendingSetups: PendingSetups;
}

const initialState: State = {
  pendingSetups: {},
};

const setupNeeded = (state: State, { serialNo, path }: ActionsToPayloads[ActionTypes.SETUP_NEEDED]): State => ({
  ...state,
  pendingSetups: {
    ...state.pendingSetups,
    [serialNo]: { serialNo, path }
  }
});

const setupNotNeeded = (state: State, { serialNo }: ActionsToPayloads[ActionTypes.SETUP_NOT_NEEDED]): State =>
  serialNo in state.pendingSetups ? ({
    ...state,
    pendingSetups: _.omit(state.pendingSetups, serialNo),
  }) : state;

const pendingSetupsLoaded = (state: State, { setups }: ActionsToPayloads[ActionTypes.PENDING_SETUPS_LOADED]): State => ({
  ...state,
  pendingSetups: setups,
});

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.SETUP_NEEDED]: setupNeeded,
  [ActionTypes.SETUP_NOT_NEEDED]: setupNotNeeded,
  [ActionTypes.PENDING_SETUPS_LOADED]: pendingSetupsLoaded,
}, initialState);
