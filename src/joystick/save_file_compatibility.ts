
interface Version1 {
  keys: {
    commands: {
      targetDevice: number;
      targetAxis: number;
      event: number;
      command: string;
    }[];
  }[];
}

export function fromVersion1(oldConfig: unknown): unknown {
  const version1 = oldConfig as Version1;
  return {
    ...version1,
    keys: version1.keys.map(key => ({
      ...key,
      commands: key.commands.map(command => {
        let parts = command.command.split(/\s+/);
        let axisType = 'axis';
        let targetAxis = command.targetAxis;
        if ((parts[0] === 'lockstep' || parts[0] === 'virtual') && parts[1]?.match(/^\d+$/)) {
          axisType = parts[0];
          targetAxis = +parts[1];
          parts = parts.slice(2);
        }

        return ({
          ...command,
          axisType,
          targetAxis,
          command: parts.join(' '),
        });
      }),
    })),
  };
}

const OLD_VERSION_CONVERTORS = [
  fromVersion1,
];

export function convertToCurrentVersion(config: unknown, version: number) {
  let converted = config;
  for (let i = version - 1; i < OLD_VERSION_CONVERTORS.length; i++) {
    converted = OLD_VERSION_CONVERTORS[i](converted);
  }
  return converted;
}
