jest.mock('@electron/remote', () => ({
  shell: {
    openPath: jest.fn(async () => {}),
  },
  app: {
    getPath: (_: string) => '',
    getName: () => 'ElectronMock',
  },
}));

import * as remote from '@electron/remote';
import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import { Container, injectable } from 'inversify';
import _ from 'lodash';

import { createContainer, destroyContainer, getContainer } from '../container';
import { wrapWithNewStore, waitUntil, waitUntilPass } from '../test';
import { mockIpc, IpcMock } from '../ipc/mock';
import { selectPreferences } from '../store';

import { Preferences } from './Preferences';
import { PreferencesService } from './service';
import { DEFAULT_VALUES, PreferenceKeys, PreferenceValue } from './types';
import { actions as preferencesActions } from './actions';
import { initializeService } from './sagas';

const TestPreferences = wrapWithNewStore(Preferences);

let wrapper: RenderResult;
let container: Container;
let ipcMock: IpcMock;
let serviceMock: PreferencesServiceMock;

@injectable()
class PreferencesServiceMock {
  public preferences: _.Dictionary<PreferenceValue> = {
    [PreferenceKeys.ZML_LOGGING]: true,
    [PreferenceKeys.TIMEOUT_LOCAL]: 1000,
    [PreferenceKeys.TIMEOUT_REMOTE]: 5000,
  };

  public retrieveInitialValues = jest.fn(() => this.preferences);
  public setPreferenceValue = jest.fn();
}

beforeEach(async () => {
  container = createContainer();
  ipcMock = mockIpc(container);
  container.bind<unknown>(PreferencesService).to(PreferencesServiceMock);
  serviceMock = getContainer().get<unknown>(PreferencesService) as PreferencesServiceMock;

  wrapper = render(<TestPreferences/>);
  TestPreferences.testStore.saga.run(initializeService);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  jest.clearAllMocks();
});

test('gets preference setting from preferences service', () => {
  expect(serviceMock.retrieveInitialValues).toHaveBeenCalled();
  expect(serviceMock.setPreferenceValue).toHaveBeenCalledWith(PreferenceKeys.ZML_LOGGING, true, false);
  expect(serviceMock.setPreferenceValue).toHaveBeenCalledWith(PreferenceKeys.TIMEOUT_LOCAL, 1000, false);
  expect(serviceMock.setPreferenceValue).toHaveBeenCalledWith(PreferenceKeys.TIMEOUT_REMOTE, 5000, false);
});

describe('ZML Logging', () => {
  test('correctly toggles zml logging', async () => {
    const zmlLogKey = PreferenceKeys.ZML_LOGGING;
    expect(serviceMock.preferences[zmlLogKey]).toEqual(true);

    fireEvent.click(wrapper.getByTitle('Log Communication'));
    await waitUntil(() => selectPreferences(
      TestPreferences.testStore.getState()).preferenceStates[zmlLogKey].value === false);

    // check that preference was set
    expect(serviceMock.setPreferenceValue).toHaveBeenLastCalledWith(zmlLogKey, false, true);
    // check that action was broadcast
    const expectedAction = preferencesActions.setPreference(zmlLogKey, false, false);
    expect(ipcMock.broadcastAction).toHaveBeenCalledWith(expectedAction);
  });

  test('correctly resets value zml logging value to default', async () => {
    const defaultValue = DEFAULT_VALUES[PreferenceKeys.ZML_LOGGING];
    TestPreferences.testStore.dispatch(preferencesActions.setPreference(PreferenceKeys.ZML_LOGGING, !defaultValue));

    fireEvent.click(wrapper.getByTitle('Reset "Log Communication" to default'));
    await waitUntil(() => {
      const states = selectPreferences(TestPreferences.testStore.getState()).preferenceStates;
      return states[PreferenceKeys.ZML_LOGGING].value === defaultValue;
    });

    expect(serviceMock.setPreferenceValue).toHaveBeenLastCalledWith(PreferenceKeys.ZML_LOGGING, defaultValue, true);
  });

  test('calls openPath when log folder link is clicked', () => {
    fireEvent.click(wrapper.getByText('Open Folder'));
    expect(remote.shell.openPath).toHaveBeenCalled();
  });
});

describe('timeouts', () => {
  test('changes local timeout', async () => {
    const localTimeoutKey = PreferenceKeys.TIMEOUT_LOCAL;
    expect(serviceMock.preferences[localTimeoutKey]).toEqual(1000);

    await waitUntilPass(() => {
      fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'Set Local Timeout (ms)' }));
      wrapper.getByText('Save');
      fireEvent.change(wrapper.getByRole('spinbutton', { name: 'Set Local Timeout (ms)' }), { target: { value: '2500' } });
      fireEvent.click(wrapper.getByText('Save'));
    });

    await waitUntil(() => selectPreferences(
      TestPreferences.testStore.getState()).preferenceStates[localTimeoutKey].value === 2500);

    expect(serviceMock.setPreferenceValue).toHaveBeenLastCalledWith(localTimeoutKey, 2500, true);
    const expectedAction = preferencesActions.setPreference(localTimeoutKey, 2500, false);
    expect(ipcMock.broadcastAction).toHaveBeenCalledWith(expectedAction);
  });

  test('changes remote timeout', async () => {
    const remoteTimeoutKey = PreferenceKeys.TIMEOUT_REMOTE;
    expect(serviceMock.preferences[remoteTimeoutKey]).toEqual(5000);

    await waitUntilPass(() => {
      fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'Set Remote Timeout (ms)' }));
      wrapper.getByText('Save');
      fireEvent.change(wrapper.getByRole('spinbutton', { name: 'Set Remote Timeout (ms)' }), { target: { value: '10000' } });
      fireEvent.click(wrapper.getByText('Save'));
    });

    await waitUntil(() => selectPreferences(
      TestPreferences.testStore.getState()).preferenceStates[remoteTimeoutKey].value === 10000);

    expect(serviceMock.setPreferenceValue).toHaveBeenLastCalledWith(remoteTimeoutKey, 10000, true);
    const expectedAction = preferencesActions.setPreference(remoteTimeoutKey, 10000, false);
    expect(ipcMock.broadcastAction).toHaveBeenCalledWith(expectedAction);
  });
});
