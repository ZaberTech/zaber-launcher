import React from 'react';
import { useSelector } from 'react-redux';
import { ContextMenu, Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { AddConnection, RemoveConnection } from '../add_connection';
import noConnectionsImage from '../assets/no_connection.svg?url';
import { getRouterUrl } from '../keys';
import { LOCAL_ROUTER_URL } from '../message_router';
import { Axis } from '../peripherals/Axis';
import { selectAxesErrors } from '../peripherals/selectors';
import { DisconnectButton } from '../connection_status';
import { ContentHeader, InternalOnlyWarning, RemoteSwitch } from '../components';
import { DeviceSetupButton } from '../device_setup/DeviceSetupButton';
import { SaveLoadContextMenuItem } from '../save_load';
import { FirmwareUpgradeContextMenuItem } from '../firmware_upgrade';
import { FactoryResetModal, FactoryResetDeviceMenuItem } from '../factory_reset';
import { DeviceReachable } from '../connection_monitor';
import { IntegratedAxis } from '../peripherals/IntegratedAxis';
import {
  SetLabel,
  SetLabelContextMenuItem,
} from '../connection_manager_ui';
import { hasRemoteMode } from '../devices';
import { DeviceError } from '../connection_manager_ui/DeviceError';
import { selectDevicesFF } from '../connection_monitor/selectors';

import { selectAllRoutersLoading, selectHasConnectionsToManage } from './selectors';
import { actions as actionsDefinition } from './actions';
import { ConnectionTable } from './connection_table';
import { SerialNumber, FirmwareVersion } from './device_table';
import { DeviceInfoWithAxes, RouterType } from './types';
import { isAxisIdentified } from './utils';
import { InternalDevicesWarning } from './components/InternalDevicesWarning';
import { actions as connectionViewActionsDefinition } from './connection_view/actions';
import { selectExpanded as selectExpandedConnections } from './connection_view/selectors';
import { SimulatorOneTimeMessage } from './components/SimulatorMessage';


const DeviceContent: React.FC<{ actions: typeof actionsDefinition; device: DeviceInfoWithAxes }> = ({ actions, device }) => (<>
  {hasRemoteMode(device) && <RemoteSwitch
    isRemote={device.isRemote}
    hasError={false}
    setRemote={remote => actions.setRemoteMode(device.key, remote)}/>}
  <DeviceSetupButton serialNo={device.identity?.serialNumber}/>
  <DeviceReachable deviceKey={device.key}>
    {reachable => !reachable && <Icons.ErrorWarning className="non-reachable" title="Device cannot be reached"/>}
  </DeviceReachable>
  <ContextMenu>
    <FirmwareUpgradeContextMenuItem device={device}/>
    <SaveLoadContextMenuItem productKey={device.key}/>
    <FactoryResetDeviceMenuItem deviceKey={device.key}/>
    <SetLabelContextMenuItem entityKey={device.key} label={device.label}/>
  </ContextMenu>
</>);

export const ConnectionManager: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const connectionViewActions = useActions(connectionViewActionsDefinition);
  const allLoading = useSelector(selectAllRoutersLoading);
  const hasConnections = useSelector(selectHasConnectionsToManage);
  const expandedConnections = useSelector(selectExpandedConnections);
  const axisErrors = useSelector(selectAxesErrors);
  const devicesFF = useSelector(selectDevicesFF);

  return (
    <div className="connection-manager">
      <div className="connection-manager-content">
        <InternalOnlyWarning/>
        <InternalDevicesWarning/>
        <ContentHeader otherContent={hasConnections && <AddConnection/>}>My Connections</ContentHeader>
        <SimulatorOneTimeMessage/>

        {allLoading && <>
          Loading....
        </>}

        {!allLoading && <>
          {hasConnections &&
            <ConnectionTable
              connectionContent={({ key }) => {
                const isLocal = getRouterUrl(key) === LOCAL_ROUTER_URL;
                return <>
                  <DisconnectButton connectionKey={key}/>
                  {isLocal && <>&emsp;<RemoveConnection connectionKey={key}/></>}
                </>;
              }}
              routerContent={router => {
                const isLocal = router.type === RouterType.Local;
                return !isLocal && <Icons.Trash title="Remove Router" onClick={() => actions.removeRouter(router.key)}/>;
              }}
              deviceContent={device => <DeviceContent actions={actions} device={device}/>}
              deviceErrorContent={device => ({
                node: devicesFF[device.key] && <DeviceError deviceKey={device.key}/>,
                noticeType: devicesFF[device.key] ? 'error' : undefined,
              })}
              controllerContent={device => <>
                <SerialNumber device={device}/>
                <FirmwareVersion device={device}/>
              </>}
              axisContent={axis =>
                isAxisIdentified(axis) && (axis.identity.isPeripheral ? <Axis axis={axis}/> : <IntegratedAxis axis={axis}/>)}
              axisErrors={axisErrors}
              expandedConnections={expandedConnections}
              onConnectionExpand={connectionViewActions.setExpanded}
              connectionNameEditable
              displayMultiAxis
            />}

          {!hasConnections && <div className="no-devices-content">
            <img src={noConnectionsImage} alt="No devices connected"/>
            <h4>You currently have no devices connected.</h4>
            <div>Press the button below to set up a new connection.</div>
            <AddConnection/>
          </div>}
        </>}
      </div>
      <FactoryResetModal/>
      <SetLabel/>
    </div>
  );
};
