import type { Units } from '@zaber/motion';

import type { EntityKey } from '../../keys';
import type { Measurement, MeasurementOK } from '../../units';

export enum KeyPressCombo {
  None,
  LeftRight,
  UpDown,
  PageUpPageDown,
}

type AbsMove = {
  type: 'abs';
  initiator: 'home' | 'away' | 'track' | 'precise';
  position: number;
  units?: Units;
};

type RelMove = {
  type: 'rel';
  direction: 'home' | 'away';
  distance: number;
  units: Units;
};

type VelMove = {
  type: 'vel';
  initiator: 'jog' | 'precise';
  direction: 'home' | 'away';
  velocity: number;
  units?: Units;
};

type CycleMove = {
  type: 'cycle';
  velocity: number;
  units?: Units;
};

type IndexMove = {
  type: 'moveIndex';
  direction: 'prev' | 'next';
  units?: Units;
};

type SinMove = {
  type: 'sin';
  amplitude: MeasurementOK;
  period: MeasurementOK;
  count?: number;
};

type OtherAxisCommand = {
  type: 'stop' | 'home' | 'stop sin' | 'findrange hardstop';
};

export type AxisCommand = AbsMove | RelMove | VelMove | CycleMove | IndexMove | SinMove | OtherAxisCommand;

export interface PreciseMovementValues {
  absolute: Measurement | null;
  relative: Measurement | null;
  velocity: Measurement | null;
}

export interface MoveSinState {
  amplitude: Measurement;
  period: Measurement;
  count: number | null;
}

export type AxisMovementState = {
  key: EntityKey;

  lastCommand: AxisCommand | null;
  previousCommand: AxisCommand | null;
  posUnit: Units;

  preciseMovement: PreciseMovementValues;
  preciseMovementExpanded: boolean;
  shortcut: KeyPressCombo;
  moveSinState: MoveSinState;
};
