import _ from 'lodash';
import classNames from 'classnames';
import React from 'react';
import { useSelector } from 'react-redux';
import { match } from 'ts-pattern';
import { AnimationClasses, Button, Checkbox, Colors, ContextMenu, Icons, LinkClasses, MinorMenu, PopUp, Text } from '@zaber/react-library';

import { selectConnections, selectIdentifiedDevices } from '../connection_manager';
import { extractConnectionKey } from '../keys';
import { useActions } from '../utils';
import { Title, useKeyboard } from '../components';

import { actions as actionDefinitions } from './actions';
import { AxisMappingDialog } from './AxisMappingDialog';
import { CsvImportDialog } from './CsvImportDialog';
import { CurveView } from './CurveView/CurveView';
import { DeviceSelectionDialog } from './DeviceSelectionDialog';
import { ExportTrajectoryDialog } from './ExportTrajectoryDialog';
import { PointPropertyPanel } from './PointPropertyPanel';
import {
  selectCsvImportOptions,
  selectCurrentFile,
  selectDeviceSelectionDialog,
  selectDeviceOptions,
  selectDevicePositionNative,
  selectErrors,
  selectExportInProgress,
  selectPlaybackTime,
  selectStartPosition,
  selectUserPath,
  selectViewMode,
  selectDeviceLoadDialog,
  selectIsFileSaved,
  selectCanUndo
} from './selectors';
import { Table } from './Table';
import { TimeSeries } from './TimeSeries';
import { ViewMode } from './types';
import { WelcomeScreen } from './WelcomeScreen';
import { BufferLoadDialog } from './BufferLoadDialog';
import { MessageDialog } from './MessageDialog';

export const FILE_MENU_ID = 'pvt-file-menu';
export const OPEN_FILE_LABEL = 'Open File';
export const SAVE_FILE_LABEL = 'Save';
export const SAVE_AS_FILE_LABEL = 'Save As';
export const CURVE_VIEW_LABEL = 'Path view';
export const TABLE_BUTTON_LABEL = 'Table view';
export const SELECT_DEVICE_BUTTON_LABEL = 'Select Device';
export const SELECT_AXES_TOOLTIP = 'Map device axes';
export const RESET_DEVICE_TOOLTIP = 'Reset device to start position';
export const RUN_TOOLTIP = 'Execute path';
export const UNSAVED_CHANGES_LABEL = 'Unsaved changes made.';


export const PvtApp: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const importOptions = useSelector(selectCsvImportOptions);
  const currentFile = useSelector(selectCurrentFile);
  const userPath = useSelector(selectUserPath);
  const errors = useSelector(selectErrors);
  const mode = useSelector(selectViewMode);
  const deviceSelectionDialog = useSelector(selectDeviceSelectionDialog);
  const deviceLoadDialog = useSelector(selectDeviceLoadDialog);
  const deviceOptions = useSelector(selectDeviceOptions);
  const identifiedDevices = useSelector(selectIdentifiedDevices);
  const connections = useSelector(selectConnections);
  const pos = useSelector(selectDevicePositionNative);
  const startPos = useSelector(selectStartPosition);
  const isPlaying = useSelector(selectPlaybackTime) != null;
  const isSaved = useSelector(selectIsFileSaved);
  const exporting = useSelector(selectExportInProgress);
  const device = deviceOptions.deviceKey ? identifiedDevices[deviceOptions.deviceKey] : null;
  const connection = device ? connections[extractConnectionKey(deviceOptions.deviceKey!)] : null;
  const { canUndo, canRedo } = useSelector(selectCanUndo);

  const closeToStart = pos.length > 0 && _.every(_.values(deviceOptions.axes), axisIndex => pos[axisIndex] === startPos[axisIndex]);

  useKeyboard((event: KeyboardEvent) => {
    if (event.ctrlKey) {
      switch (event.key) {
        case 'z':
          event.preventDefault(); // Chrome has its own undo/redo within input controls.
          actions.undo();
          break;
        case 'y':
          event.preventDefault();
          actions.redo();
          break;
        case 's':
          if (event.shiftKey) {
            actions.fileSaveAs();
          } else {
            actions.fileSave();
          }
          break;
        case 'o':
          actions.fileOpen();
          break;
        default: break;
      }
    }
  });


  return <>
    <div className="pvt-app">
      <Title>PVT Viewer</Title>
      <div className="toolbar">
        {!device && <Button
          color="grey"
          className="select-device-button"
          data-testid="select-device-button"
          onClick={() => actions.setDeviceSelectionDialogVisible(true)}>
          {SELECT_DEVICE_BUTTON_LABEL}
        </Button>}
        {device && <div className={classNames('selected-device', LinkClasses.Default)}>
          <div className="label"
            data-testid="select-device-button"
            onClick={() => actions.setDeviceSelectionDialogVisible(true)}>
            <Text e={Text.Emphasis.Bold}>{connection?.id}</Text>
            <Icons.ArrowCollapsed/>
            <Text e={Text.Emphasis.Bold}>{device.identity.name}</Text>
          </div>
          <div className="underline"/>
        </div>}
        <ContextMenu trigger={() => <Icons.Folder appearsClickable data-testid={FILE_MENU_ID}/>}>
          <ContextMenu.Item onClick={actions.fileOpen} icon={<Icons.OpenFile/>}>{OPEN_FILE_LABEL}</ContextMenu.Item>
          <ContextMenu.Item onClick={actions.fileSave} icon={<Icons.Save/>}>{SAVE_FILE_LABEL}</ContextMenu.Item>
          <ContextMenu.Item onClick={actions.fileSaveAs} icon={<Icons.SaveAs/>}>{SAVE_AS_FILE_LABEL}</ContextMenu.Item>
        </ContextMenu>
        <Icons.Buffer disabled={!device} title="Read a Buffer from the Device" onClick={() => actions.setBufferLoadDialogVisible(true)}/>
        <Icons.Information title="Show file format help" onClick={() => actions.setViewMode(ViewMode.Welcome)}/>
        <div className="divider"/>
        <MinorMenu barStyle="horizontal">
          <div className={mode === ViewMode.TimeSeries ? 'active' : ''}><MinorMenu.Item>
            <Icons.Oscilloscope title="Time series view" onClick={() => actions.setViewMode(ViewMode.TimeSeries)}/>
          </MinorMenu.Item></div>
          <div className={mode === ViewMode.Curve ? 'active' : ''}><MinorMenu.Item>
            <Icons.Path title={CURVE_VIEW_LABEL} onClick={() => actions.setViewMode(ViewMode.Curve)}/>
          </MinorMenu.Item></div>
          <div className={mode === ViewMode.Table ? 'active' : ''}><MinorMenu.Item>
            <Icons.Table title={TABLE_BUTTON_LABEL} onClick={() => actions.setViewMode(ViewMode.Table)}/>
          </MinorMenu.Item></div>
        </MinorMenu>
        <div className="divider"/>
        <Icons.Restore title="Undo (CTRL-Z)" disabled={!canUndo} onClick={actions.undo} data-testid="undo-button"/>
        <Icons.Restore title="Redo (CTRL-Y)" disabled={!canRedo} onClick={actions.redo} className="flipped" data-testid="redo-button"/>
        <div className="divider"/>
        <Icons.Settings title={SELECT_AXES_TOOLTIP}
          disabled={!device || !userPath}
          onClick={() => actions.setAxisDialogVisible(true, false)}/>
        <Icons.LeftEnd title={RESET_DEVICE_TOOLTIP}
          disabled={!device || !userPath}
          onClick={actions.reset}/>
        <PopUp position="bottom"
          triggerAction={!device || !userPath || closeToStart || isPlaying ? 'none' : 'hover'}
          trigger={() => <Icons.RightNormal
            title={RUN_TOOLTIP}
            disabled={!device || !userPath || !closeToStart || isPlaying}
            onClick={actions.run}/>}>
          The current device position is too far from the sequence start position.
          Click the reset to start position button first.
        </PopUp>
        <Icons.Stop title="Stop device motion"
          disabled={!device || !userPath}
          onClick={actions.stop}/>
        <div className="divider"/>
        {!exporting && <>
          <Icons.Export disabled={userPath == null}
            data-testid="trajectory-export-button"
            onClick={() => actions.showTrajectoryExportDialog(true)}/>
          <Text t={Text.Type.Helper}
            className="clickable-text" onClick={() => actions.showTrajectoryExportDialog(true)}>Export Trajectory</Text>
        </>}
        {exporting && <>
          <Icons.Refresh className={AnimationClasses.Rotation}/>
          <Text t={Text.Type.Helper}>Export in progress</Text>
        </>}
        <div className="spacer"/>
        {!isSaved && <>
          <Icons.ErrorWarning className="icon-small" color={Colors.yellow}/>
          <Text t={Text.Type.Helper} className="margined">{UNSAVED_CHANGES_LABEL}</Text>
        </>}
        <Checkbox checked={importOptions.autoReload}
          className="auto-reload-checkbox"
          labelContent="Auto-Reload"
          onChecked={actions.setAutoReload}/>
        <Icons.Refresh title="Reload current file"
          disabled={!currentFile}
          onClick={() => actions.reload(true)}/>
      </div>
      <div className="workspace">
        {match(mode)
          .with(ViewMode.TimeSeries, () => <TimeSeries/>)
          .with(ViewMode.Curve, () => <CurveView/>)
          .with(ViewMode.Table, () => <Table/>)
          .otherwise(() => <WelcomeScreen/>)}
      </div>
      <div className="properties">
        <PointPropertyPanel/>
      </div>
    </div>
    <CsvImportDialog/>
    {deviceSelectionDialog.deviceDialogVisible && <DeviceSelectionDialog/>}
    {deviceLoadDialog.visible && <BufferLoadDialog/>}
    {deviceSelectionDialog.axisDialogVisible && <AxisMappingDialog/>}
    <ExportTrajectoryDialog/>
    {errors.length !== 0 && <MessageDialog messages={errors}/>}
  </>;
};
