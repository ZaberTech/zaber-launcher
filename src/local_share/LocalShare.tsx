import React from 'react';

import { ContentHeader } from '../components';

import { LocalShareConfig } from './LocalShareConfig';

export const LocalShare: React.FC = () => (
  <div className="local-share">
    <ContentHeader>Local Area Network Sharing</ContentHeader>
    <LocalShareConfig/>
  </div>
);
