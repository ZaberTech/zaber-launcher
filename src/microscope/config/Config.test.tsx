import React from 'react';
import {
  RenderResult, fireEvent, render,
} from '@testing-library/react';
import { injectable } from 'inversify';
import EventEmitter from 'events';

async function asyncNoop() { }

let tuners: ServoTunerMock[] = [];

class ServoTunerMock {
  constructor(public readonly axis: unknown) {
    tuners.push(this);
  }
  getSimpleTuningParamDefinitions = jest.fn(async () => [{ name: 'Stiffness' }]);
  setSimpleTuning = jest.fn(asyncNoop);
  setStartupParamset = jest.fn(asyncNoop);
  loadParamset = jest.fn(asyncNoop);
}

jest.mock('@zaber/motion', () => {
  const original = jest.requireActual('@zaber/motion');
  return {
    ...original,
    ascii: {
      ...original.ascii,
      ServoTuner: ServoTunerMock,
    },
  };
});

class SnmpSession extends EventEmitter {
  static createCallback: ((session: SnmpSession) => void) | null = null;

  constructor() {
    super();
    SnmpSession.createCallback?.(this);
  }

  get = jest.fn((iod: string[], callback: (error: Error | null, varbinds: unknown[]) => void) => {
    setTimeout(() => callback(null, [{  value: '169.254.123.45' }]), 0);
  });

  close = jest.fn();
}

jest.mock('net-snmp', () => ({
  createSession: jest.fn(() => new SnmpSession()),
}));


import {
  CommandFailedException, CommandFailedExceptionData, Length, ascii,
} from '@zaber/motion';

import { createContainer, destroyContainer } from '../../container';
import { mockStorage, StorageMock, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../../test/mocks/ascii';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../../message_router';
import {
  MOCK_SERIAL_PORT, mockDataForDeviceWithPeripherals, mockDataForDeviceWithoutPeripherals, mockLocalConnections,
} from '../../connection_manager/mocks';
import { ConfigDialog } from './Config';
import { Button } from '@zaber/react-library';
import { actions } from '../actions';
import { ALT_PART_KEY_PREFIX, AUTOFOCUS_STORAGE_KEY, AXIS_INVERTED_KEY, PART_KEY, X_MOR_DEVICE_ID } from '../types';
import { makeAxisKey, makeConnectionKey, makeDeviceKey, makeRouterKey } from '../../keys';
import { useDispatch } from 'react-redux';
import { ConfigSuccess } from './ConfigSuccess';
import _ from 'lodash';

const ADDRESSES = {
  filterChanger: 1,
  objectiveChanger: 2,
  focus: 3,
  illuminator: 4,
  xyStage: 5,
};

function badCmd() {
  return new CommandFailedException('Rejected', {
    replyFlag: 'RJ',
    responseData: 'BADCOMMAND',
  } as CommandFailedExceptionData);
}

let axes: (AxisMock)[];

class AxisMock extends AxisMockBase {
  axisType = ascii.AxisType.UNKNOWN;
  identity = { peripheralName: '' };

  constructor(id: number, device: DeviceMock) {
    super(id, device);

    axes.push(this);
  }

  storage = {
    setString: jest.fn(asyncNoop),
    setNumber: jest.fn(asyncNoop),
    eraseKey: jest.fn(asyncNoop),
    getBool: jest.fn(async () => false),
    setBool: jest.fn(asyncNoop),
    keyExists: jest.fn(() => true),
  };

  settings = {
    set: jest.fn(asyncNoop),
    canConvertNativeUnits: jest.fn(() => true),
  };
}

let devices: DeviceMock[];

class DeviceMock extends DeviceMockBase<AxisMock> {
  axisCount = 1;
  identity = { name: '' };

  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    devices.push(this);
  }

  storage = {
    setString: jest.fn(asyncNoop),
    canConvertNativeUnits: jest.fn(() => true),
  };
  settings = {
    set: jest.fn(asyncNoop),
  };

  identify = jest.fn(asyncNoop);
}

function makePart(name: string, deviceAddress: number, axisNumber: number, additional?: Record<string, string>) {
  const data = [
    `set ${PART_KEY} ${_.snakeCase(name)}`,
    ...Object.entries(additional ?? {}).map(([key, value]) => `${key}=${value}`),
  ].join(' ');
  return {
    messageType: ascii.MessageType.INFO,
    data,
    deviceAddress,
    axisNumber,
  };
}

let connections: ConnectionMock[];
let connectionCallback: (c: ConnectionMock) => void;

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  constructor(id: string, router: RouterConnectionMock) {
    super(id, router);
    connections.push(this);
    connectionCallback?.(this);
  }

  genericCommandMultiResponse = jest.fn(async (command: string) => {
    switch (command) {
      case `storage all print prefix ${PART_KEY}`:
        return this.microscopeParts();
      case `storage all erase ${PART_KEY}`:
        return [];
      case `storage all print keys prefix ${ALT_PART_KEY_PREFIX}`:
        return [];
      default:
        throw badCmd();
    }
  });
  genericCommand = jest.fn(asyncNoop);

  microscopeParts = jest.fn(() => [
    makePart('filterChanger', ADDRESSES.filterChanger, 0),
    makePart('focus', ADDRESSES.focus, 1),
    makePart('x', ADDRESSES.xyStage, 1),
    makePart('y', ADDRESSES.xyStage, 2),
    makePart('cameraTrigger', ADDRESSES.focus, 0, { do: '2' }),
  ]);
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}


const Component: React.FC = () => {
  const dispatch = useDispatch();
  return (<>
    <ConfigDialog/>
    <Button onClick={() => dispatch(actions.resetConfigDialog(true))}>Open</Button>
    <ConfigSuccess/>
  </>);
};

const TestApp = wrapWithNewStore(wrapWithRouter(Component));

let wrapper: RenderResult;

let storage: StorageMock;

const ROUTER_KEY = makeRouterKey(LOCAL_ROUTER_URL);
const CONNECTION_KEY = makeConnectionKey(ROUTER_KEY, MOCK_SERIAL_PORT);

function makeDeviceData(connectionKey: string) {
  const illuminator = mockDataForDeviceWithPeripherals(connectionKey, ADDRESSES.illuminator, 4, 'smart', axis => {
    axis.identity.axisType = ascii.AxisType.LAMP;
    return axis;
  });
  illuminator.identity.name = 'X-LCA4';

  const objectiveChanger = mockDataForDeviceWithoutPeripherals(connectionKey, ADDRESSES.objectiveChanger, 1);
  objectiveChanger.identity.name = 'X-MOR';
  objectiveChanger.identity.deviceId = X_MOR_DEVICE_ID[0];

  const filterChanger = mockDataForDeviceWithoutPeripherals(connectionKey, ADDRESSES.filterChanger, 1);
  filterChanger.identity.name = 'X-FCR';

  return [
    objectiveChanger,
    filterChanger,
    mockDataForDeviceWithoutPeripherals(connectionKey, ADDRESSES.focus, 1),
    illuminator,
    mockDataForDeviceWithPeripherals(connectionKey, ADDRESSES.xyStage, 2, 'smart'),
  ];
}

beforeEach(() => {
  devices = [];
  axes = [];
  connections = [];
  tuners = [];

  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

  storage = mockStorage(container);
  storage.stored[AUTOFOCUS_STORAGE_KEY + CONNECTION_KEY] = 'wdi://host:1234';

  wrapper = render(<TestApp/>);
  mockLocalConnections(TestApp.testStore, {
    [MOCK_SERIAL_PORT]: makeDeviceData,
  });
  TestApp.testStore.dispatch(actions.setSelectedKey(CONNECTION_KEY));
});

afterEach(() => {
  wrapper?.unmount();
  wrapper = null!;

  destroyContainer();
  storage = null!;

  connectionCallback = null!;
  SnmpSession.createCallback = null;
});

const LOADING = /Loading configuration/;
const SAVED = 'Component setup successful.';

async function openConfigDialog() {
  fireEvent.click(wrapper.getByText('Open'));
  wrapper.getByText(LOADING);
  await waitUntilPass(() => expect(wrapper.queryByText(LOADING)).toBeNull());
}

test('detects all parts when configured', async () => {
  await openConfigDialog();

  expect(wrapper.getByPlaceholderText('Filter Changer Device').getAttribute('data-current')).toBe('01 X-FCR');
  expect(wrapper.getByPlaceholderText('Objective Changer').getAttribute('data-current')).toBe('02 X-MOR');
  expect(wrapper.getByPlaceholderText('Illuminator').getAttribute('data-current')).toBe('04 X-LCA4');
  expect(wrapper.getByPlaceholderText('Focus Axis Device').getAttribute('data-current')).toBe('03 X-LHM');
  expect(wrapper.getByPlaceholderText('X Axis Device').getAttribute('data-current')).toBe('05 X-MCC2');
  expect(wrapper.getByPlaceholderText('X Axis').getAttribute('data-current')).toBe('Axis 1 LRM150A-E03T4A');
  expect(wrapper.getByPlaceholderText('Y Axis Device').getAttribute('data-current')).toBe('05 X-MCC2');
  expect(wrapper.getByPlaceholderText('Y Axis').getAttribute('data-current')).toBe('Axis 2 LRM150A-E03T4A');
  expect(wrapper.getByPlaceholderText('Hostname/IP')).toHaveValue('host:1234');
  expect(wrapper.getByPlaceholderText('Trigger Device').getAttribute('data-current')).toBe('03 X-LHM');
  expect(wrapper.getByPlaceholderText('Trigger Digital Output')).toHaveValue(2);
});

test('handles loading error', async () => {
  connectionCallback = connection => {
    connection.genericCommandMultiResponse.mockRejectedValueOnce(new Error('Bad disconnect'));
  };
  fireEvent.click(wrapper.getByText('Open'));
  await waitUntilPass(() => expect(wrapper.getByText(/Bad disconnect/)).toBeInTheDocument());
});

test('handles config errors', async () => {
  const ADDRESS_OFFSET = 20;

  mockLocalConnections(TestApp.testStore, {
    [MOCK_SERIAL_PORT]: connectionKey => {
      const data = makeDeviceData(CONNECTION_KEY);

      const objectiveChanger2 = mockDataForDeviceWithoutPeripherals(connectionKey, ADDRESSES.objectiveChanger + ADDRESS_OFFSET, 1);
      objectiveChanger2.identity.deviceId = X_MOR_DEVICE_ID[0];
      data.push(objectiveChanger2);

      const illuminator2 = mockDataForDeviceWithPeripherals(connectionKey, ADDRESSES.illuminator + ADDRESS_OFFSET, 1, 'smart', axis => {
        axis.identity.axisType = ascii.AxisType.LAMP;
        return axis;
      });
      data.push(illuminator2);

      return data;
    },
  });
  TestApp.testStore.dispatch(actions.setSelectedKey(CONNECTION_KEY));

  connectionCallback = connection => {
    connection.microscopeParts.mockReturnValueOnce([
      makePart('filterChanger', ADDRESSES.filterChanger, 0),
      makePart('filterChanger', ADDRESSES.filterChanger + ADDRESS_OFFSET, 0),
      makePart('focus', ADDRESSES.focus, 0),
      makePart('x', ADDRESSES.xyStage, 1),
      makePart('x', ADDRESSES.xyStage, 2),
      makePart('cameraTrigger', ADDRESSES.filterChanger, 0),
      makePart('cameraTrigger', ADDRESSES.focus, 0),
    ]);
  };

  await openConfigDialog();

  expect(wrapper.getAllByText('Multiple tags found')).toHaveLength(3);
  expect(wrapper.getByText('Device tagged instead of axis')).toBeInTheDocument();
  expect(wrapper.getByText('Multiple illuminators found')).toBeInTheDocument();
  expect(wrapper.getByText('Multiple objective changers found')).toBeInTheDocument();
});

test('allows to configure all parts', (async () => {
  connectionCallback = connection => {
    connection.microscopeParts.mockReturnValueOnce([]);
  };
  await openConfigDialog();

  const makeDeviceKey2 = (address: number) => makeDeviceKey(CONNECTION_KEY, address);
  const makeAxisKey2 = (device: number, axisNumber: number) => makeAxisKey(makeDeviceKey2(device), axisNumber);

  fireEvent.change(wrapper.getByPlaceholderText('Filter Changer Device'), { target: { value: makeDeviceKey2(ADDRESSES.filterChanger) } });
  fireEvent.change(wrapper.getByPlaceholderText('Focus Axis Device'), { target: { value: makeDeviceKey2(ADDRESSES.focus) } });
  fireEvent.change(wrapper.getByPlaceholderText('X Axis Device'), { target: { value: makeDeviceKey2(ADDRESSES.xyStage) } });
  fireEvent.change(wrapper.getByPlaceholderText('X Axis'), { target: { value: makeAxisKey2(ADDRESSES.xyStage, 1) } });
  fireEvent.change(wrapper.getByPlaceholderText('Y Axis Device'), { target: { value: makeDeviceKey2(ADDRESSES.xyStage) } });
  fireEvent.change(wrapper.getByPlaceholderText('Y Axis'), { target: { value: makeAxisKey2(ADDRESSES.xyStage, 2) } });
  fireEvent.change(wrapper.getByPlaceholderText('Hostname/IP'), { target: { value: 'other:27' } });
  fireEvent.change(wrapper.getByPlaceholderText('Trigger Device'), { target: { value: makeDeviceKey2(ADDRESSES.focus) } });
  fireEvent.change(wrapper.getByPlaceholderText('Trigger Digital Output'), { target: { value: '1' } });

  fireEvent.click(wrapper.getByText('Set up'));
  await waitUntilPass(() => expect(wrapper.getByText(SAVED)).toBeInTheDocument());

  expect(connections[0].getDevice(ADDRESSES.filterChanger).storage.setString)
    .toHaveBeenCalledWith(PART_KEY, 'filter_changer');
  expect(connections[0].getDevice(ADDRESSES.focus).getAxis(1).storage.setString)
    .toHaveBeenCalledWith(PART_KEY, 'focus');
  expect(connections[0].getDevice(ADDRESSES.focus).storage.setString)
    .toHaveBeenCalledWith(`${ALT_PART_KEY_PREFIX}camera_trigger`, 'do=1');
  expect(connections[0].getDevice(ADDRESSES.xyStage).getAxis(1).storage.setString)
    .toHaveBeenCalledWith(PART_KEY, 'x');
  expect(connections[0].getDevice(ADDRESSES.xyStage).getAxis(2).storage.setString)
    .toHaveBeenCalledWith(PART_KEY, 'y');
  expect(storage.stored[AUTOFOCUS_STORAGE_KEY + CONNECTION_KEY]).toBe('wdi://other:27');
}));

test('deletes all the parts', async () => {
  await openConfigDialog();

  connections[0].genericCommandMultiResponse.mockImplementation(async (command: string) => {
    switch (command) {
      case `storage all print keys prefix ${ALT_PART_KEY_PREFIX}`:
        return [
          `${ALT_PART_KEY_PREFIX}lawn_mover`,
          `${ALT_PART_KEY_PREFIX}deli_slicer`,
        ].map((key, i) => ({
          deviceAddress: i + 1,
          axisNumber: 0,
          data: `set ${key}`,
          messageType: ascii.MessageType.INFO,
        }));
      default:
        return [];
    }
  });

  fireEvent.click(wrapper.getByText('Set up'));
  await waitUntilPass(() => expect(wrapper.getByText(SAVED)).toBeInTheDocument());

  expect(connections[0].genericCommandMultiResponse).toHaveBeenCalledWith(
    `storage all erase ${PART_KEY}`, expect.anything());
  expect(connections[0].genericCommand).toBeCalledWith(
    `storage all erase ${ALT_PART_KEY_PREFIX}lawn_mover`, { device: 1 });
  expect(connections[0].genericCommand).toBeCalledWith(
    `storage all erase ${ALT_PART_KEY_PREFIX}deli_slicer`, { device: 2 });
});

test('allows to remove parts', async () => {
  await openConfigDialog();
  fireEvent.change(wrapper.getByPlaceholderText('Hostname/IP'), { target: { value: '' } });

  fireEvent.click(wrapper.getByText('Set up'));
  await waitUntilPass(() => expect(wrapper.getByText(SAVED)).toBeInTheDocument());

  expect(storage.stored[AUTOFOCUS_STORAGE_KEY + CONNECTION_KEY]).toBe(null);
});

test('detects inverted axes', (async () => {
  connectionCallback = connection => {
    connection.getDevice(ADDRESSES.focus).getAxis(1).storage.getBool.mockResolvedValueOnce(true);
    connection.getDevice(ADDRESSES.xyStage).getAxis(2).storage.getBool.mockResolvedValueOnce(true);
  };
  await openConfigDialog();

  const invertedBoxes = wrapper.getAllByLabelText(/Inverted/).map(checkbox => checkbox as HTMLInputElement);
  expect(invertedBoxes.map(checkbox => checkbox.checked)).toEqual([true, false, true]);
}));

test('allows to invert axes', (async () => {
  await openConfigDialog();

  const invertedBoxes = wrapper.getAllByLabelText(/Inverted/).map(checkbox => checkbox as HTMLInputElement);
  for (const checkbox of invertedBoxes) {
    expect(checkbox).not.toBeChecked();
  }
  fireEvent.click(invertedBoxes[0]);
  fireEvent.click(invertedBoxes[2]);

  fireEvent.click(wrapper.getByText('Set up'));
  await waitUntilPass(() => expect(wrapper.getByText(SAVED)).toBeInTheDocument());

  expect(connections[0].getDevice(ADDRESSES.focus).getAxis(1).storage.setBool)
    .toHaveBeenCalledWith(AXIS_INVERTED_KEY, true);
  expect(connections[0].getDevice(ADDRESSES.xyStage).getAxis(1).storage.eraseKey)
    .toHaveBeenCalledWith(AXIS_INVERTED_KEY);
  expect(connections[0].getDevice(ADDRESSES.xyStage).getAxis(2).storage.setBool)
    .toHaveBeenCalledWith(AXIS_INVERTED_KEY, true);
}));

test('handles error while configuring', (async () => {
  await openConfigDialog();
  connections[0].genericCommandMultiResponse.mockRejectedValueOnce(new Error('Bad disconnect'));
  fireEvent.click(wrapper.getByText('Set up'));
  await waitUntilPass(() => expect(wrapper.getByText(/Bad disconnect/)).toBeInTheDocument());
}));

test('makes special changes to certain parts', (async () => {
  await openConfigDialog();

  connections[0].getDevice(ADDRESSES.focus).identity.name = 'X-LDA';
  connections[0].getDevice(ADDRESSES.xyStage).getAxis(1).identity.peripheralName = 'ASR';

  fireEvent.click(wrapper.getByText('Set up'));
  await waitUntilPass(() => expect(wrapper.getByText(SAVED)).toBeInTheDocument());

  const focusSet = connections[0].getDevice(ADDRESSES.focus).getAxis(1).settings.set;
  expect(focusSet).toHaveBeenCalledWith('limit.home.offset', 15, Length.mm);
  expect(focusSet).toHaveBeenCalledWith('motion.tracking.ai', 1);

  expect(connections[0].getDevice(ADDRESSES.xyStage).getAxis(1).settings.set).toHaveBeenCalled();
}));

test('makes special changes to certain parts 2', (async () => {
  await openConfigDialog();

  connections[0].getDevice(ADDRESSES.xyStage).getAxis(1).identity.peripheralName = 'ADR';

  fireEvent.click(wrapper.getByText('Set up'));
  await waitUntilPass(() => expect(wrapper.getByText(SAVED)).toBeInTheDocument());

  expect(tuners[0].setSimpleTuning).toHaveBeenCalled();
}));

describe('WDI search', () => {
  test('allows to search for WDI', async () => {
    await openConfigDialog();
    fireEvent.click(wrapper.getByText('Search'));
    await waitUntilPass(() => expect(wrapper.getByPlaceholderText('Hostname/IP')).toHaveValue('169.254.123.45'));
  });

  test('handles error', async () => {
    SnmpSession.createCallback = session => {
      session.get.mockImplementationOnce((oid, callback) => {
        setTimeout(() => callback(new Error('Timeout'), []), 0);
      });
    };

    await openConfigDialog();
    fireEvent.click(wrapper.getByText('Search'));
    await waitUntilPass(() => expect(wrapper.getByText(/Error: Timeout/)));
  });

  test('handles general error', async () => {
    SnmpSession.createCallback = session => {
      session.get.mockImplementationOnce(() => null);
      setTimeout(() => session.emit('error', new Error('General error')), 0);
    };

    await openConfigDialog();
    fireEvent.click(wrapper.getByText('Search'));
    await waitUntilPass(() => expect(wrapper.getByText(/Error: General error/)));
  });
});
