import React, { ComponentType, Component as ReactComponent } from 'react';
import { ConnectedRouter } from 'connected-react-router';

import { browserHistory } from '../store/history';

export function wrapWithRouter<Props>(Component: ComponentType<Props>) {
  class WrappedWithRouter extends ReactComponent<Props> {
    public render(): React.ReactNode {
      return (
        <ConnectedRouter history={browserHistory}>
          <Component {...this.props}/>
        </ConnectedRouter>
      );
    }
  }

  return WrappedWithRouter;
}
