import { changeCollection } from '@zaber/toolbox';

import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes, DevicesLoadedPayload } from '../connection_manager';
import { EntityKey, tryExtractConnectionKey } from '../keys';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import { KeyConfig, ChainInfo, DEFAULT_COMMAND, JoystickConfig, CalibrationType } from './types';

export interface State {
  selectedKey: EntityKey | null;
  config: JoystickConfig | null;
  deviceConfig: JoystickConfig | null;
  chainInfo: ChainInfo | null;
  readingConfig: boolean;
  configReadError: string | null;
  writingConfig: boolean;
  configWriteOrRestoreError: string | null;
  manualMode: boolean;
  productionMode: boolean;
  saveLoadError: string | null;
  calibrating: CalibrationType | null;
  calibrationError: string | null;
  restoringConfig: boolean;
}

const initialState: State = {
  selectedKey: null,
  config: null,
  deviceConfig: null,
  chainInfo: null,
  readingConfig: false,
  configReadError: null,
  writingConfig: false,
  configWriteOrRestoreError: null,
  manualMode: false,
  productionMode: false,
  saveLoadError: null,
  calibrating: null,
  calibrationError: null,
  restoringConfig: false,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const setSelectedKey: Reducer<ActionTypes.SET_SELECTED_KEY> = ({ manualMode, productionMode }, { key }) => ({
  ...initialState,
  manualMode,
  productionMode,
  selectedKey: key,
  readingConfig: true,
});

const configRead: Reducer<ActionTypes.CONFIG_READ> = (state, { config, chainInfo }) => ({
  ...state,
  config,
  deviceConfig: config,
  chainInfo,
  readingConfig: false,
});

const configReadErr: Reducer<ActionTypes.CONFIG_READ_ERR> = (state, { error }) => ({
  ...state,
  readingConfig: false,
  configReadError: error,
});

const updateAxisConfig: Reducer<ActionTypes.UPDATE_AXIS_CONFIG> = (state, { axisNumber, config }) => ({
  ...state,
  config: state.config ? {
    ...state.config,
    axes: changeCollection(state.config.axes, 'axisNumber', axisNumber, config),
  } : state.config,
});

const keyConfigReducer = <AT extends ActionTypes>(
  change: ((cfg: KeyConfig, payload: ActionsToPayloads[AT]) => KeyConfig),
): Reducer<AT> => (state, payload) => ({
    ...state,
    config: state.config ? {
      ...state.config,
      keys: changeCollection(state.config.keys,
        'keyNumber', (payload as { keyNumber: number }).keyNumber,
        cfg => change(cfg, payload)),
    } : state.config,
  });

const updateKeyConfig: Reducer<ActionTypes.UPDATE_KEY_CONFIG> =
  keyConfigReducer((config, { config: newConfig }) => ({ ...config, ...newConfig }));

const updateKeyCommand: Reducer<ActionTypes.UPDATE_KEY_COMMAND> =
  keyConfigReducer((config, { commandIndex, command }) => ({
    ...config,
    commands: config.commands.map((cmd, i) => (i === commandIndex ? { ...cmd, ...command } : cmd))
  }));

const removeKeyCommand: Reducer<ActionTypes.REMOVE_KEY_COMMAND> =
  keyConfigReducer((config, { commandIndex }) => ({
    ...config,
    commands: config.commands.filter((cmd, i) => i !== commandIndex)
  }));

const addKeyCommand: Reducer<ActionTypes.ADD_KEY_COMMAND> =
  keyConfigReducer(config => ({
    ...config,
    commands: [...config.commands, DEFAULT_COMMAND],
  }));

const writeConfig: Reducer<ActionTypes.WRITE_CONFIG> = state => ({
  ...state,
  writingConfig: true,
  configWriteOrRestoreError: null,
});

const writeConfigDone: Reducer<ActionTypes.WRITE_CONFIG_DONE> = (state, { error }) => ({
  ...state,
  writingConfig: false,
  configWriteOrRestoreError: error ?? null,
  deviceConfig: error == null ? state.config : state.deviceConfig,
});

const setManualMode: Reducer<ActionTypes.SET_MANUAL_MODE> = (state, { mode }) => ({
  ...state,
  manualMode: mode,
});

const setProductionMode: Reducer<ActionTypes.SET_PRODUCTION_MODE> = (state, { mode }) => ({
  ...state,
  productionMode: mode,
});

const setSaveLoadConfigError: Reducer<ActionTypes.SET_SAVE_LOAD_CONFIG_ERR> = (state, { error }) => ({
  ...state,
  saveLoadError: error,
});

const loadConfigDone: Reducer<ActionTypes.LOAD_CONFIG_DONE> = (state, { config }) => ({
  ...state,
  config,
});

const startCalibration: Reducer<ActionTypes.START_CALIBRATION> = (state, { type }) => ({
  ...state,
  calibrating: type,
  calibrationError: null,
});

const stopCalibration: Reducer<ActionTypes.STOP_CALIBRATION> = state => ({
  ...state,
  calibrating: null,
});

const calibrationError: Reducer<ActionTypes.CALIBRATION_ERROR> = (state, { error }) => ({
  ...state,
  calibrating: null,
  calibrationError: error,
});

const restoreConfig: Reducer<ActionTypes.RESTORE_CONFIG> = state => ({
  ...state,
  restoringConfig: true,
  configWriteOrRestoreError: null,
});

const restoreConfigDone: Reducer<ActionTypes.RESTORE_CONFIG_DONE> = (state, { error }) => ({
  ...state,
  restoringConfig: false,
  configWriteOrRestoreError: error ?? null,
});

const devicesLoaded = (state: State, { connectionKey }: DevicesLoadedPayload): State =>
  tryExtractConnectionKey(connectionKey) === connectionKey ? ({
    ...initialState,
    manualMode: state.manualMode,
    productionMode: state.productionMode,
  }) : state;

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.SET_SELECTED_KEY]: setSelectedKey,
  [ActionTypes.CONFIG_READ]: configRead,
  [ActionTypes.CONFIG_READ_ERR]: configReadErr,
  [ActionTypes.UPDATE_AXIS_CONFIG]: updateAxisConfig,
  [ActionTypes.UPDATE_KEY_CONFIG]: updateKeyConfig,
  [ActionTypes.UPDATE_KEY_COMMAND]: updateKeyCommand,
  [ActionTypes.ADD_KEY_COMMAND]: addKeyCommand,
  [ActionTypes.REMOVE_KEY_COMMAND]: removeKeyCommand,
  [ActionTypes.WRITE_CONFIG]: writeConfig,
  [ActionTypes.WRITE_CONFIG_DONE]: writeConfigDone,
  [ActionTypes.SET_MANUAL_MODE]: setManualMode,
  [ActionTypes.SET_PRODUCTION_MODE]: setProductionMode,
  [ActionTypes.SET_SAVE_LOAD_CONFIG_ERR]: setSaveLoadConfigError,
  [ActionTypes.LOAD_CONFIG_DONE]: loadConfigDone,
  [ActionTypes.START_CALIBRATION]: startCalibration,
  [ActionTypes.STOP_CALIBRATION]: stopCalibration,
  [ActionTypes.CALIBRATION_ERROR]: calibrationError,
  [ActionTypes.RESTORE_CONFIG]: restoreConfig,
  [ActionTypes.RESTORE_CONFIG_DONE]: restoreConfigDone,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
}, initialState);
