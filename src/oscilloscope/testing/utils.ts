/* eslint-disable camelcase */
import { fireEvent, RenderResult } from '@testing-library/react';

import type { IdentifiedDeviceInfo } from '../../connection_manager';
import { waitUntilPass } from '../../test';
import { CAPTURE_SETTINGS_TAB_LABEL, Mode } from '../types';


export const selectRecordingTab = async function(wrapper: RenderResult, title: string) {
  fireEvent.click(wrapper.getByTestId(`recording-tab ${title}`));
  await waitUntilPass(() => expect(wrapper.getByTestId(`recording-tab ${title}`)).toHaveClass('selected-tab'));
};


export const selectRecordingControls = async function(wrapper: RenderResult) {
  fireEvent.click(wrapper.getByText(CAPTURE_SETTINGS_TAB_LABEL));
  await waitUntilPass(() => expect(wrapper.getByText(CAPTURE_SETTINGS_TAB_LABEL).parentElement!.parentElement).toHaveClass('active'));
};


export const selectChannelControls = async function(wrapper: RenderResult) {
  fireEvent.click(wrapper.getByTestId('channels-tab').childNodes[0]);
  await waitUntilPass(() => expect(wrapper.getByTestId('channels-tab')).toHaveClass('active'));
};


export const selectRecordMode = function(wrapper: RenderResult, mode: Mode) {
  const selector = wrapper.getByTestId('mode-selector');
  fireEvent.change(selector, { target: { value: mode } });
};


export const setDifferencingWindow = function(wrapper: RenderResult, window: number) {
  const selector = wrapper.getByTestId('differencing-window');
  fireEvent.change(selector, { target: { value: window } });
};


export const setSampleRate = function(wrapper: RenderResult, rate: number) {
  const selector = wrapper.getByTestId('rate-selector');
  fireEvent.change(selector, { target: { value: rate } });
};


export const setNumSamples = function(wrapper: RenderResult, limit: number | null) {
  const selector = wrapper.getByTestId('sample-limit');
  fireEvent.change(selector, { target: { value: limit } });
};


export const openAddSettingsDialog = function(wrapper: RenderResult): HTMLElement {
  fireEvent.click(wrapper.getByTestId('Data Source add'));
  const dialog = wrapper.getByTestId('add-data-source-dialog');
  expect(dialog).not.toHaveClass('hidden');
  return dialog;
};


export const openAddIoDialog = function(wrapper: RenderResult): HTMLElement {
  fireEvent.click(wrapper.getByTestId('I/O Pins add'));
  const dialog = wrapper.getByTestId('add-io-channel-dialog');
  expect(dialog).not.toHaveClass('hidden');
  return dialog;
};


export const addDeviceScopeSettingsInfo = function(info: IdentifiedDeviceInfo): IdentifiedDeviceInfo {
  info.product.settings.rows = info.product.settings.rows.concat([{
    name: 'scope.delay',
    param_type: 'UintDP1',
    decimal_places: 1,
    contextual_dimension_id: 11,
    read_access_level: 1,
    write_access_level: 1,
    visibility: 'always',
    realtime: true,
  }, {
    name: 'scope.timebase',
    param_type: 'UintDP1',
    decimal_places: 1,
    contextual_dimension_id: 11,
    read_access_level: 1,
    write_access_level: 1,
    visibility: 'always',
    realtime: true,
  }]);

  return info;
};
