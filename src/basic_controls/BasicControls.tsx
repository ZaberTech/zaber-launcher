import { Card, ContextMenu, Flex, Icons, NoticeBorder, NoticeMessage, Text } from '@zaber/react-library';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { match, P } from 'ts-pattern';
import { CloserProps } from '@zaber/react-library/dist/types/elements/Notice/NoticeMessage';

import { actions as connectionManagerActionsDefinition } from '../connection_manager/actions';
import { EntityHierarchyLabel, NoContentMessage, RemoteSwitch, Title } from '../components';
import {
  axisDescription,
  ConnectionsView,
  selectAxes,
  selectDevices,
  selectIdentifiedDevices
} from '../connection_manager';
import {
  EntityKey, EntityKeyType, extractConnectionKey, extractDeviceKey,
  getAxisNumber, getEntityType, makeAxisKey
} from '../keys';
import { useActions } from '../utils';
import { PolledAxisWarnings, usePolledProductWarnings } from '../polling/hooks';
import { includesDisablingFlag } from '../polling/warnings';
import { AppUrls, APP_LIST } from '../apps/list';
import { Thumbnail } from '../components/Thumbnail';
import { hasRemoteMode } from '../devices';

import { actionDefinitions } from './actions';
import { useProductInfo } from './hooks';
import { ProductError, worstErrorType } from './errors/error';
import { MovableSection } from './movement/MovableSection';
import { selectEntity, selectErrors, selectMoveSinModal, selectPinned, selectProductKeys, selectTravelLimitModal } from './selectors';
import { WarningFlag } from './errors/WarningFlag';
import { isMoveable, isUnsupportedDevice, moveableKey, productKey, X_ADR_PERIPHERAL_IDS } from './types';
import { selectAxisStates } from './movement/selectors';
import { IoCard } from './io';
import { KeyPressCombo } from './movement/types';
import { ShortCutIcon } from './movement/precise/KeyboardShortcutControl';
import { MoveSinModal } from './movement/MoveSin';
import { TravelLimitModal } from './movement/TravelLimit';

type AxisCardLabelProps = {
  entity: EntityKey;
  lockstepGroup: number | false;
};

const AxisCardLabel: React.FC<AxisCardLabelProps> = ({ entity, lockstepGroup }) => {
  const devices = useSelector(selectDevices);
  const device = devices[extractDeviceKey(entity)];
  const axes = useSelector(selectAxes);

  const axis = axes[entity];
  const axisNumber = getAxisNumber(entity);
  const axisDescriptionText =
      axis.label && axis.identity != null ? `${axis.label} (${axis.identity.peripheralName})` :
      axis.label ? axis.label :
      axis.identity != null ? axis.identity.peripheralName : axisDescription(axis, device);

  return <>
    <Text t={Text.Type.H5} className="axis-detail">Axis {axisNumber}</Text>
    <Text t={Text.Type.H5} className="axis-detail">
      {axisDescriptionText} {lockstepGroup && `(Lockstep Group ${lockstepGroup})`}
    </Text>
  </>;
};

type ProductCardProps = {
  entity: EntityKey;
  axisInController?: boolean;
};

const RedirectedProductCard: React.FC<{ deviceId: number }> = ({ deviceId }) => <div className="redirect">
  <Thumbnail deviceOrPeripheralId={deviceId} altExt="device"/>
</div>;

const RedirectedProductNotice: React.FC<{ entity: EntityKey; appUrl: AppUrls }> = ({ entity, appUrl }) => {
  const appInfo = APP_LIST.find(appInfo => appInfo.url === appUrl);
  let updatedAppInfo = appInfo;
  if (appInfo && appInfo.name === 'X-JOY') {
    updatedAppInfo = { ...appInfo, name: 'Joystick' };
  }

  const closer: CloserProps | undefined = updatedAppInfo && {
    action: () => window.location.hash = `${appUrl}?${ConnectionsView.SELECT_PARAM}=${entity}`,
    title: 'Go To App',
    button: `Go To ${updatedAppInfo.name} App`,
  };

  return <NoticeMessage className="device-error" type="info" closer={closer}>
    {`This device is not supported by Basic Controls.${updatedAppInfo != null ? ` Go to the ${updatedAppInfo.name} app.` : ''}`}
  </NoticeMessage>;
};


interface ProductErrorMessageProps {
  productError: ProductError;
  entity: EntityKey;
  info: ReturnType<typeof useProductInfo>;
}

const ProductErrorMessage: React.FC<ProductErrorMessageProps> = ({ productError, entity, info }) => {
  const actions = useActions(actionDefinitions);
  const connectionActions = useActions(connectionManagerActionsDefinition);

  const closer = productError.context === 'remote-mode'
    ? { action: () => connectionActions.setRemoteMode(entity, false), button: 'Enable USB Control' }
    : { action: () => actions.setError(productKey(info, entity), null), title: 'Dismiss Error' };

  return <NoticeMessage
    headline={productError.headline}
    className="device-error"
    closer={closer}
    collapsible>
    {productError.message}
  </NoticeMessage>;
};


const ProductCard: React.FC<ProductCardProps> = ({ entity, axisInController = false }) => {
  const actions = useActions(actionDefinitions);
  const connectionActions = useActions(connectionManagerActionsDefinition);
  const info = useProductInfo(entity, true);
  const identifiedDevices = useSelector(selectIdentifiedDevices);
  const deviceProductInfo = identifiedDevices[extractDeviceKey(entity)];
  const entityType = getEntityType(entity);
  const movementStates = useSelector(selectAxisStates);

  const state = isMoveable(info) ? movementStates[moveableKey(info)] : null;
  const isMoving = state?.lastCommand != null;
  const keyboardShortcut = state?.shortcut ?? KeyPressCombo.None;
  const unfilteredWarnings = usePolledProductWarnings(entity);
  const warnings: PolledAxisWarnings = isMoving && unfilteredWarnings.type === 'flags' ? {
    type: 'flags', flags: unfilteredWarnings.flags.filter(flag => flag !== 'NI')
  } : unfilteredWarnings;

  const driverDisabled = includesDisablingFlag(warnings);
  const allErrors = useSelector(selectErrors);
  const productError = allErrors[productKey(info, entity)];
  const worstError = worstErrorType(warnings, productError);

  const unsupportedDevice = isUnsupportedDevice(info);
  const unsupportedDeviceError = unsupportedDevice ? 'info' : null;
  const displayErrorType = info?.type === 'controller' || info?.type === 'invalid' ? null : worstError ?? unsupportedDeviceError;

  const productHasRemoteError = productError?.context === 'remote-mode';
  const peripheralHasRemoteError = info?.type === 'controller' && [...info.peripherals.entries()]
    .filter(([, peripheral]) => peripheral.type !== 'invalid')
    .some(([axisNumber]) => allErrors[makeAxisKey(info.key, axisNumber)]?.context === 'remote-mode');
  const hasRemoteError = productHasRemoteError || peripheralHasRemoteError;

  const pinned = useSelector(selectPinned).includes(entity);
  useEffect(() => {
    if (info?.type === 'invalid' && info.reason === 'disconnected') {
      actions.fetchDeviceInfo(entity);
    }
  }, [JSON.stringify(warnings), productError?.message]);

  const driverLabel = driverDisabled ? 'Enable Driver' : 'Disable Driver';
  return <Card className="product-card">
    {(!axisInController && entityType === EntityKeyType.AXIS) &&
      <>
        <Flex.Row className="product-header controller-header">
          <EntityHierarchyLabel entityKey={extractDeviceKey(entity)}/>
          {hasRemoteMode(deviceProductInfo) && <>
            <Flex.Spacer/>
            <RemoteSwitch isRemote={deviceProductInfo.isRemote}
              hasError={hasRemoteError}
              setRemote={remote => connectionActions.setRemoteMode(entity, remote)}/>
          </>}
        </Flex.Row>
        <div className="controller-peripherals">
          <ProductCard key={entity} entity={entity} axisInController={!axisInController}/>
        </div>
      </>}
    {(axisInController || entityType !== EntityKeyType.AXIS) &&
      <NoticeBorder type={displayErrorType}>
        <Flex.Row className={`product-header ${entityType !== EntityKeyType.AXIS && 'controller-header'}`}>
          {axisInController && <AxisCardLabel entity={entity} lockstepGroup={info?.type === 'lockstep' && info?.group}/>}
          {entityType !== EntityKeyType.AXIS && <EntityHierarchyLabel entityKey={entity}/>}
          <Flex.Spacer/>
          {info?.type === 'controller' && hasRemoteMode(deviceProductInfo) &&
            <RemoteSwitch isRemote={deviceProductInfo.isRemote}
              hasError={hasRemoteError}
              setRemote={remote => connectionActions.setRemoteMode(entity, remote)}/>}
          {isMoveable(info) &&
        <>
          <ShortCutIcon shortcut={keyboardShortcut}/>
          <ContextMenu>
            <ContextMenu.Item
              title={driverLabel}
              icon={<Icons.Flash/>}
              onClick={() => actions.enableDriver(info, driverDisabled)}>
              {driverLabel}
            </ContextMenu.Item>
            <ContextMenu.Item
              title="Sinusoidal Motion"
              icon={<Icons.Oscilloscope/>}
              onClick={() => actions.setMoveSinModal(info)}
            >
                Sinusoidal Motion
            </ContextMenu.Item>
            {info.type === 'motion-axis' && X_ADR_PERIPHERAL_IDS.includes(info.identity.peripheralId) &&
            <ContextMenu.Item
              title="Set Travel Limits"
              icon={<Icons.HardwareModification/>}
              onClick={() => actions.setTravelLimitModal(info)}
            >
              Set Travel Limits
            </ContextMenu.Item>
            }
          </ContextMenu>
        </>}
          {pinned ?
            <Icons.Pinned title="Unpin" className="pin-icon" onClick={() => actions.pinProduct(entity, false)}/> :
            <Icons.Pin title="Pin" className="pin-icon" onClick={() => actions.pinProduct(entity, true)}/>
          }
        </Flex.Row>
        {match(info)
          .with(P.nullish, () => <NoContentMessage message="Loading..." type="working"/>)
          .when(isMoveable, info => <>
            <MovableSection moveable={info} warnings={warnings}/>
            {'io' in info && info.io != null && <IoCard deviceKey={entity} info={info.io} type="device"/>}
          </>)
          .with({ type: 'controller' }, info => <>
            <div className="controller-peripherals">
              {[...info.peripherals.entries()]
                .filter(([, peripheral]) => peripheral.type !== 'invalid')
                .map(([axisNumber]) => (
                  <ProductCard key={axisNumber} entity={makeAxisKey(info.key, axisNumber)} axisInController/>
                ))}
            </div>
            {info.io != null && <IoCard deviceKey={entity} info={info.io} type="controller"/>}
          </>)
          .with({ type: 'x-joy' }, ({ deviceId }) => <RedirectedProductCard deviceId={deviceId}/>)
          .with({ type: 'process-controller' }, ({ deviceId }) => <RedirectedProductCard deviceId={deviceId}/>)
          .with({ type: 'lamp-controller' }, ({ deviceId }) => <RedirectedProductCard deviceId={deviceId}/>)
          .with({ type: 'invalid' }, ({ reason, message }) => match(reason)
            .with('disconnected', () => <NoContentMessage title="Not Connected" message={message}/>)
            .with('error', () => <NoContentMessage title="Error" message={message}/>)
            .with('unidentified', () => <NoContentMessage title="Unidentified" message={message}/>)
            .exhaustive()
          )
          .exhaustive()
        }
        {displayErrorType != null && <div className="errors">
          {warnings.type === 'flags' && warnings.flags.map(flag => (
            <WarningFlag info={info} entity={entity} key={flag} flag={flag}/>
          ))}
          {warnings.type === 'error' && (
            <NoticeMessage headline={warnings.message} className="device-error" collapsible>{warnings.details}</NoticeMessage>
          )}
          {productError && <ProductErrorMessage productError={productError} entity={entity} info={info}/>}
          {unsupportedDevice &&
            match(info)
              .with({ type: 'x-joy' }, ({ key }) =>
                <RedirectedProductNotice entity={key} appUrl="/joystick"/>)
              .with({ type: 'process-controller' }, ({ key }) =>
                <RedirectedProductNotice entity={key} appUrl="/process-controller"/>)
              .with({ type: 'lamp-controller' }, ({ key }) =>
                <RedirectedProductNotice entity={extractConnectionKey(key)} appUrl="/microscope"/>)
              .exhaustive()
          }
        </div>}
      </NoticeBorder>
    }
  </Card>;
};

export const BasicControls: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const selectedEntity = useSelector(selectEntity);
  const productKeys = useSelector(selectProductKeys);
  const moveSinModal = useSelector(selectMoveSinModal);
  const travelLimitModal = useSelector(selectTravelLimitModal);

  return <div className="connection-view-and-app">
    <Title>Basic Controls</Title>
    <ConnectionsView
      selectable={['connection', 'device', 'axis']}
      selected={selectedEntity}
      onSelect={actions.chooseEntity}
    />
    <div className="app-ui basic-controls">
      {productKeys.map(key => <ProductCard key={key} entity={key}/>)}
      {productKeys.length === 0 && (
        <NoContentMessage
          title="Welcome to Basic Controls!"
          type={undefined}
          message="Select a connection to start moving the devices."
        />
      )}
      {moveSinModal != null && <MoveSinModal moveable={moveSinModal}/>}
      {travelLimitModal != null && <TravelLimitModal moveable={travelLimitModal}/>}
    </div>
  </div>;
};
