import React from 'react';
import { Flex, InputEditor, NumericInputEditor, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';
import { IoPortType } from '@zaber/motion/ascii';

import type { EntityKey } from '../../keys';
import { usePolledAnalogIo } from '../../polling/hooks';
import { useActions } from '../../utils';

import { selectDeviceStates } from './selectors';
import { actionDefinitions } from './actions';

type AnalogOutputProps = {
  deviceKey: EntityKey;
  channel: number;
};

export const AnalogOutput: React.FC<AnalogOutputProps> = ({ deviceKey, channel }) => {
  const output = usePolledAnalogIo(deviceKey, 'out', channel);
  const change = useSelector(selectDeviceStates)[deviceKey]?.analogChanges[channel];
  const mode: InputEditor.Mode = output == null ? 'initialize' : change?.mode ?? 'display';
  const value = mode === 'edit' || mode === 'write' ? change?.value : output;
  const actions = useActions(actionDefinitions);
  const labels = useSelector(selectDeviceStates)[deviceKey]?.labels;
  const label = labels?.[`${IoPortType.ANALOG_OUTPUT}-${channel}`] ?? '';

  return <Flex.Row className="ao control">
    <Text e={Text.Emphasis.Bold}>{channel}.</Text>
    <NumericInputEditor
      value={value ?? null}
      mode={mode}
      onChange={update => actions.setAnalog(deviceKey, channel, update)}
      title={`Analog Output ${channel}`}
      align="center"
      aria-label={`Set Analog Output ${channel}`}
    />
    <Text>{label}</Text>
  </Flex.Row>;
};
