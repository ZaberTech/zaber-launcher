import type { SagaIterator } from 'redux-saga';
import { all, call, race, take, put, select } from 'redux-saga/effects';
import { RequestTimeoutException } from '@zaber/motion';
import { WarningFlags } from '@zaber/motion/ascii';

import { connectionManagerActions, getConnection, selectConnections, selectDevices } from '../connection_manager';
import { Action, takeLeadingUniq, RT, SagaIter, throwUnexpectedError, tryAccess, delay } from '../utils';
import type { Logger } from '../app_components';
import { getLogger } from '../log';
import { EntityKey, makeDeviceKey } from '../keys';
import { DEFAULT_POLL_INTERVAL } from '../types';

import { ActionTypes, ActionsToPayloads, actions } from './actions';

let logger: Logger;

export function* rootSaga(): SagaIterator {
  logger = getLogger('connectionMonitorSaga');

  yield all([
    takeLeadingUniq(ActionTypes.START,
      (action: Action<ActionsToPayloads[ActionTypes.START]>) => action.payload.connectionKey,
      startMonitoring),
  ]);
}

interface MonitorDevice {
  key: EntityKey;
  deviceId: number;
}

function* startMonitoring(
  { payload: { connectionKey } }: Action<ActionsToPayloads[ActionTypes.START]>,
): SagaIter {
  yield race([
    call(function* () {
      while (true) {
        try {
          const connection: RT<typeof getConnection> = yield getConnection(connectionKey);

          let replies: RT<typeof connection.genericCommandMultiResponse>;
          try {
            replies = yield connection.genericCommandMultiResponse('get deviceid', { checkErrors: false });
          } catch (err) {
            if (err instanceof RequestTimeoutException) {
              replies = [];
            } else {
              throw err;
            }
          }

          const devices = replies
            .filter(reply => reply.replyFlag === 'OK')
            .map(({ deviceAddress, data }): MonitorDevice => ({
              key: makeDeviceKey(connectionKey, deviceAddress),
              deviceId: +data,
            }));
          const deviceKeys = devices.map(device => device.key);
          yield put(actions.monitorData(connectionKey, deviceKeys));

          const deviceFF = replies
            .filter(reply => reply.warningFlag === WarningFlags.CRITICAL_SYSTEM_ERROR)
            .map(({ deviceAddress }): EntityKey => makeDeviceKey(connectionKey, deviceAddress));
          yield put(actions.updateDevicesFF(connectionKey, deviceFF));

          if (yield checkDevicesForReload(connectionKey, devices)) {
            logger.info('Reloading devices');
            yield put(connectionManagerActions.loadDevices(connectionKey, false));
          }
        } catch (err) {
          throwUnexpectedError(err);
          logger.warn('monitoring error', err);
          yield put(actions.monitorErr(connectionKey, err.message ?? String(err)));
        }
        yield delay(DEFAULT_POLL_INTERVAL);
      }
    }),
    take((action: Partial<Action<ActionsToPayloads[ActionTypes.STOP]>>) =>
      action.type === ActionTypes.STOP && action.payload?.connectionKey === connectionKey),
  ]);
}

function* checkDevicesForReload(connectionKey: EntityKey, devices: MonitorDevice[]): SagaIter<boolean> {
  const managerConnections: RT<typeof selectConnections> = yield select(selectConnections);
  const managerDevices: RT<typeof selectDevices> = yield select(selectDevices);

  const connectionState = tryAccess(managerConnections, connectionKey);
  if (connectionState == null || connectionState.loadingDevices) {
    return false;
  }
  if (connectionState.loadingDevicesErr != null) {
    return connectionState.loadingDevicesErr.reason === 'connection';
  }

  for (const detectedDevice of devices) {
    const device = tryAccess(managerDevices, detectedDevice.key);
    if (device == null) {
      logger.info(`New device ${detectedDevice.key} discovered`);
      return true;
    }

    if (
      (device.identity != null && device.identity.deviceId !== detectedDevice.deviceId) ||
      (device.nonIdentifiedInfo != null && device.nonIdentifiedInfo.deviceId !== detectedDevice.deviceId)
    ) {
      logger.info(`Device ${detectedDevice.key} does not match`);
      return true;
    }
  }

  return false;
}
