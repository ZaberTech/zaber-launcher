import React from 'react';
import { Text } from '@zaber/react-library';

import eq1 from '../assets/eq1.png';
import eq2 from '../assets/eq2.png';

export const tuningManualContent = {
  backEMFCompensation: <>
    <Text e={Text.Emphasis.Bold}>
      This slider controls the amount of damping applied to the back EMF compensation term of the controller.
    </Text>
    <div>
      <Text e={Text.Emphasis.Bold}>Increasing this value</Text><Text> can increase the available torque, by bringing
      the actual current amplitude applied to the motor closer to the target current amplitude. However, as
      the current amplitude increases, natural stepper motor resonances can cause decreases in the torque at
      certain speeds. In addition to adjusting this value, it can be useful to apply other resonance
      suppression techniques, such as mechanical damping, lowering the run current, or avoiding problematic
      speeds, to improve performance.</Text>
    </div>
    <div>
      <Text e={Text.Emphasis.Bold}>Lowering this value</Text><Text> increases the damping and can help to improve
        the robustness of the controller. This can make the motion smoother and help to suppress motor resonances.
        However, this can also increase the chance of stalling due to insufficient back EMF compensation and poor setpoint tracking.</Text>
    </div>
    <Text>
      When trying to get rid of mechanical resonances in your motor, in addition to adjusting this value,
      it can be useful to apply other resonance suppression techniques,
      such as mechanical damping, lowering the run current, or avoiding problematic speeds.
    </Text>
  </>,
  overallGain: <>
    <Text e={Text.Emphasis.Bold}>
      This slider affects overall controller gain.
    </Text>
    <div>
      <Text e={Text.Emphasis.Bold}>Higher values</Text><Text> provide stronger correction to measured error in coil current.
        This can increase the available torque.</Text>
    </div>
    <div>
      <Text e={Text.Emphasis.Bold}>A lower value</Text><Text> increases the robustness of the controller
        and decreases the effect of current sensor noise on the motor current and acoustic noise.
        This value should be chosen such that it is high enough to provide acceptable
        setpoint tracking and low enough to avoid instability or excessive noise.</Text>
    </div>
    <Text>
      The gain (K) is set relative to nominal maximum gain (K<sub>max</sub>) where K<sub>max</sub> is calculated such that
      the phase margin of the PI module is 45° (neglecting other parts of the controller). The value of the slider is defined as
      &nbsp;<img src={eq1} alt="log10(K/Kmax)"/>.
    </Text>
  </>,
  responseCorrectionSpeed: <>
    <Text e={Text.Emphasis.Bold}>
      This slider controls the corner frequency of the proportional-integral (PI) module of the current controller.
    </Text>
    <Text>
      Adjusting this value affects the strength of the controller's response to step changes in the current error.
    </Text>
    <Text>
      This value should be set high enough to provide reasonably fast correction without leading to excessive oscillations.
    </Text>
    <Text>
      The corner frequency (ω<sub>PI</sub>) is set relative to the corner frequency of the motor coil (ω<sub>motor</sub>) where the coil
      is modeled as an RL circuit. The value of the slider is defined as&nbsp;<img src={eq2} alt="log10(wPI / wMotor)"/>.
    </Text>
    <Text>
    A value of -0.5 is expected to work well in most cases.
    </Text>
  </>
};
