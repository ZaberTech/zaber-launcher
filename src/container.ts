import { Container } from 'inversify';
import { LocalStorageSymbol } from '@zaber/app-components/lib/storage/storage';

import { environment, processArguments } from './environment';

let container: Container;

export function getContainer(): Container {
  return container;
}

export function createContainer(): Container {
  container = new Container({ autoBindInjectable: true, defaultScope: 'Singleton' });

  if (global.localStorage) {
    const { localStorage } = global;
    container.bind(LocalStorageSymbol).toConstantValue(localStorage);

    if (!environment.isTest && processArguments.clearLocalStorage) {
      localStorage.clear();
    }
  }

  return container;
}

export function destroyContainer(): void {
  if (container) {
    container.unbindAll();
    container = null!;
  }
}
