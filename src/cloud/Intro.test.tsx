import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';

const openExternalLink = jest.fn();
jest.mock('../desktop', () => ({
  openExternalLink,
}));

import { wrapWithNewStore, wrapWithRouter } from '../test';
import { createContainer, destroyContainer } from '../container';

import { Intro } from './Intro';

const TestIntro = wrapWithNewStore(wrapWithRouter(Intro));

let wrapper: RenderResult;

beforeEach(() => {
  createContainer();

  wrapper = render(<TestIntro/>);
});

afterEach(() => {
  destroyContainer();

  wrapper.unmount();
  wrapper = null!;
});

test('opens link for sign up', () => {
  fireEvent.click(wrapper.getByText('Sign Up'));
  expect(openExternalLink).toHaveBeenCalledWith('https://cloud.test.io/signup');
});
