import path from 'path';
import os from 'os';
import url from 'url';

import _ from 'lodash';
import { ReplaySubject } from 'rxjs';
import { app, BrowserWindow, Menu, MenuItem } from 'electron';
import * as electronRemote from '@electron/remote/main';
import windowStateKeeper from 'electron-window-state';
import { injectable } from 'inversify';

import { environment } from '../src/environment';

const DEFAULT_WINDOW_OPTIONS: Electron.BrowserWindowConstructorOptions = {
  minWidth: 960,
  minHeight: 480,
  webPreferences: {
    nodeIntegration: true,
    contextIsolation: false,
  },
};

const INDEX_URL = url.pathToFileURL(path.resolve(path.join(__dirname, '../build/index.html'))).href;

interface CreateWindowOptions {
  url?: string;
  managedState?: boolean;
}

@injectable()
export class Windows {
  private readonly runSubject = new ReplaySubject();
  public readonly runPromise = this.runSubject.toPromise();
  private stopped = false;

  private readonly windowState = windowStateKeeper({
    defaultWidth: 1300,
    defaultHeight: 960
  });

  constructor() {
    app.on('activate', this.onActivate.bind(this));
    app.on('window-all-closed', this.onAllClosed.bind(this));
  }

  private onActivate() {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
      this.createWindow({ managedState: true });
    }
  }

  private onAllClosed() {
    if (!environment.isProduction) {
      this.createWindow({ managedState: true });
      return;
    }

    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      this.stop();
    }
  }

  createWindow(options: CreateWindowOptions) {
    this.createWindowAsync(options).catch(err => this.runSubject.error(err));
  }

  private async createWindowAsync(options: CreateWindowOptions): Promise<void> {
    if (this.stopped) { return }

    const windowOptions: Electron.BrowserWindowConstructorOptions = {
      width: this.windowState.width,
      height: this.windowState.height,
      ...DEFAULT_WINDOW_OPTIONS,
    };
    if (options.managedState) {
      _.merge(windowOptions, {
        x: this.windowState.x,
        y: this.windowState.y,
      });
    }
    if (os.platform() === 'linux') {
      // on linux this icon is used for panel
      windowOptions.icon = path.join(__dirname, '../icons/512x512.png');
    }

    let closeDuringLoading = false;
    const onCloseDuringLoading = () => closeDuringLoading = true;

    const window = new BrowserWindow(windowOptions);
    window.addListener('close', onCloseDuringLoading);

    if (options.managedState) {
      this.windowState.manage(window);
    }
    this.setupWindow(window);

    try {
      await window.loadURL(options.url ?? INDEX_URL);
    } catch (err) {
      if (closeDuringLoading || window.isDestroyed()) {
        return;
      }
      throw err;
    }

    window.removeListener('close', onCloseDuringLoading);

    if (!environment.isProduction) {
      window.webContents.openDevTools();
    }
  }

  private setupWindow(window: BrowserWindow) {
    electronRemote.enable(window.webContents);
    window.webContents.on('did-create-window', childWindow => {
      this.setupWindow(childWindow);
    });
    window.webContents.setWindowOpenHandler(details => {
      // This is a hack that avoid windows sharing a JS context (see window.opener).
      // We used to rely on nativeWindowOpen property that was removed from electron.
      // We had made changes to ZML to support multiple contexts but some electron assertion fails.
      // See https://github.com/electron/electron/issues/33868 .
      this.createWindow({ url: details.url });
      return ({ action: 'deny' });
      /*
      TODO: Enable this code in to allow context sharing once the issue from above is resolved.
      if (this.stopped) {
        return ({ action: 'deny' });
      }
      return ({
        action: 'allow',
        overrideBrowserWindowOptions: {
          width: this.windowState.width,
          height: this.windowState.height,
          ...DEFAULT_WINDOW_OPTIONS,
        },
      });
      */
    });
    window.setMenuBarVisibility(!environment.isProduction);
    window.webContents.on('context-menu', (_, props) => {
      if (!props.selectionText) { return }
      const menu = new Menu();
      menu.append(new MenuItem({ label: 'Copy', role: 'copy' }));
      menu.append(new MenuItem({ label: 'Paste', role: 'paste' }));
      menu.popup();
    });
  }

  stop() {
    if (this.stopped) {
      return;
    }
    this.stopped = true;

    BrowserWindow.getAllWindows().forEach(window => window.close());

    this.runSubject.next(undefined);
    this.runSubject.complete();
  }
}
