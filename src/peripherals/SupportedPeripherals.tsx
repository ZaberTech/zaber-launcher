import React from 'react';
import { Button, Loader, Text, reactSelect } from '@zaber/react-library';
import { createFilter } from 'react-select';
import { useActions } from '@zaber/toolbox/lib/redux';
import Select from 'react-select/async';

import type { AxisStateComplete } from './selectors';
import { actions as actionsDefinition } from './actions';
import type { SupportedPeripheral } from './types';

interface Message {
  text: string;
  isDisabled: true;
}
type OptionType = SupportedPeripheral | Message;

function isMessage(option: OptionType): option is Message {
  return 'text' in option;
}

const formatLabel = (option: OptionType): string => {
  if (isMessage(option)) {
    return option.text;
  }
  return `${option.name} (${option.peripheralId})`;
};
const getOptionValue = (option: OptionType): string => {
  if (isMessage(option)) {
    return 'message';
  }
  return String(option.peripheralId);
};

const styles = reactSelect.getDefaultStyles<OptionType>({
  option: (base, { isDisabled }) => {
    let style = base;
    if (isDisabled) {
      style = { ...style, fontStyle: 'italic' };
    }
    return style;
  },
});

const peripheralFilter = createFilter({
  stringify: formatLabel,
}) as unknown as (p: SupportedPeripheral, input: string) => boolean;

const MAX_OPTIONS_COUNT = 6;

interface Props {
  axis: AxisStateComplete;
  onPick: (peripheral: SupportedPeripheral) => void;
}

export const SupportedPeripherals: React.FC<Props> = ({ axis, onPick }) => {
  const { supportedPeripherals, fetchSupportedPeripheralsErr } = axis;
  const actions = useActions(actionsDefinition);
  if (fetchSupportedPeripheralsErr) {
    return (<>
      Cannot fetch peripheral list.&ensp;
      <Button size="small" onClick={() => actions.fetchSupportedPeripherals(axis.key)}>Try again</Button>
      <Text t={Text.Type.BodySm} e={Text.Emphasis.Light}>({fetchSupportedPeripheralsErr})</Text>
    </>);
  }
  if (!supportedPeripherals) {
    return <Loader size="small"/>;
  }

  return (<Select<OptionType>
    className="select-input"
    noOptionsMessage={({ inputValue }) => inputValue ? 'Peripheral cannot be found' : null}
    placeholder="Enter Peripheral ID or Name"
    styles={styles}
    formatOptionLabel={formatLabel}
    getOptionValue={getOptionValue}
    components={{
      IndicatorsContainer: () => null,
      IndicatorSeparator: () => null,
    }}
    loadOptions={(input, callback) => {
      let options = supportedPeripherals.filter(o => peripheralFilter(o, input)) as OptionType[];
      if (options.length > MAX_OPTIONS_COUNT) {
        const countOver = options.length - MAX_OPTIONS_COUNT;
        options = [
          ...options.slice(0, MAX_OPTIONS_COUNT),
          { text: `...and ${countOver} more`, isDisabled: true }
        ];
      }
      callback(options);
    }}
    onChange={option => onPick(option as SupportedPeripheral)}/>
  );
};
