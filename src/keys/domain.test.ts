import {
  makeRouterKey, makeConnectionKey, makeDeviceKey, makeAxisKey,
  getRouterUrl, getConnectionId, getDeviceAddress, getAxisNumber,
  extractRouterKey, extractConnectionKey, extractDeviceKey, getIncludedKeys,
} from './domain';
import { EntityKeyType } from './generic';

const TEST_ROUTER_KEY = makeRouterKey('localhost');
const TEST_CONNECTION_KEY = makeConnectionKey(TEST_ROUTER_KEY, 'COM1');
const TEST_DEVICE_KEY = makeDeviceKey(TEST_CONNECTION_KEY, 1);
const TEST_AXIS_KEY = makeAxisKey(TEST_DEVICE_KEY, 1);

describe('makeRouterKey', () => {
  test('creates unique a key out of url', () => {
    const key1 = makeRouterKey('localhost');
    const key2 = makeRouterKey('hostlocal');
    expect(key1 !== key2).toBeTruthy();
  });

  test('throws error when router url is invalid', () => {
    expect(() => makeRouterKey('')).toThrow();
    expect(() => makeRouterKey(null!)).toThrow();
  });
});
describe('makeConnectionKey', () => {
  test('creates unique a key out of router key and connection ID', () => {
    const key1 = makeConnectionKey(TEST_ROUTER_KEY, 'COM1');
    const key2 = makeConnectionKey(makeRouterKey('hostlocal'), 'COM1');
    const key3 = makeConnectionKey(TEST_ROUTER_KEY, 'COM2');
    expect(new Set([key1, key2, key3]).size).toBe(3);
  });

  test('throws error when router key is invalid', () => {
    expect(() => makeConnectionKey(TEST_DEVICE_KEY, 'COM1')).toThrow();
    expect(() => makeConnectionKey(null!, 'COM1')).toThrow();
    expect(() => makeConnectionKey(TEST_ROUTER_KEY, '')).toThrow();
  });
});
describe('makeDeviceKey', () => {
  test('creates unique a key out of connection key and device number', () => {
    const key1 = makeDeviceKey(TEST_CONNECTION_KEY, 1);
    const key2 = makeDeviceKey(makeConnectionKey(TEST_ROUTER_KEY, 'COM2'), 1);
    const key3 = makeDeviceKey(TEST_CONNECTION_KEY, 2);
    expect(new Set([key1, key2, key3]).size).toBe(3);
  });

  test('throws error when connection key is invalid', () => {
    expect(() => makeDeviceKey(TEST_DEVICE_KEY, 1)).toThrow();
    expect(() => makeDeviceKey(null!, 1)).toThrow();
    expect(() => makeDeviceKey(TEST_CONNECTION_KEY, null!)).toThrow();
    expect(() => makeDeviceKey(TEST_CONNECTION_KEY, 1.2)).toThrow();
    expect(() => makeDeviceKey(TEST_CONNECTION_KEY, Number.NaN)).toThrow();
  });
});
describe('makeAxisKey', () => {
  test('creates unique a key out of connection key and device number', () => {
    const key1 = makeAxisKey(TEST_DEVICE_KEY, 1);
    const key2 = makeAxisKey(makeDeviceKey(TEST_CONNECTION_KEY, 2), 1);
    const key3 = makeAxisKey(TEST_DEVICE_KEY, 2);
    expect(new Set([key1, key2, key3]).size).toBe(3);
  });

  test('throws error when connection key is invalid', () => {
    expect(() => makeAxisKey(TEST_CONNECTION_KEY, 1)).toThrow();
    expect(() => makeAxisKey(null!, 1)).toThrow();
    expect(() => makeAxisKey(TEST_DEVICE_KEY, null!)).toThrow();
    expect(() => makeDeviceKey(TEST_DEVICE_KEY, 1.2)).toThrow();
    expect(() => makeDeviceKey(TEST_DEVICE_KEY, Number.NaN)).toThrow();
  });
});
describe('getRouterUrl', () => {
  test('extracts router url from higher key', () => {
    expect(getRouterUrl(TEST_ROUTER_KEY)).toBe('localhost');
    expect(getRouterUrl(TEST_CONNECTION_KEY)).toBe('localhost');
    expect(getRouterUrl(TEST_DEVICE_KEY)).toBe('localhost');
    expect(getRouterUrl(TEST_AXIS_KEY)).toBe('localhost');
  });
  test('throws error on invalid key', () => {
    expect(() => getRouterUrl('')).toThrow();
    expect(() => getRouterUrl(null!)).toThrow();
  });
});
describe('getRouterUrl', () => {
  test('extracts router url from higher key', () => {
    expect(getConnectionId(TEST_CONNECTION_KEY)).toBe('COM1');
    expect(getConnectionId(TEST_DEVICE_KEY)).toBe('COM1');
    expect(getConnectionId(TEST_AXIS_KEY)).toBe('COM1');
  });
  test('throws error on invalid key', () => {
    expect(() => getConnectionId('')).toThrow();
    expect(() => getConnectionId(null!)).toThrow();
  });
});
describe('getDeviceAddress', () => {
  test('extracts router url from higher key', () => {
    expect(getDeviceAddress(TEST_DEVICE_KEY)).toBe(1);
    expect(getDeviceAddress(TEST_AXIS_KEY)).toBe(1);
  });
  test('throws error on invalid key', () => {
    expect(() => getDeviceAddress('')).toThrow();
    expect(() => getDeviceAddress(null!)).toThrow();
  });
});
describe('getAxisNumber', () => {
  test('extracts router url from higher key', () => {
    expect(getAxisNumber(TEST_AXIS_KEY)).toBe(1);
  });
  test('throws error on invalid key', () => {
    expect(() => getAxisNumber('')).toThrow();
    expect(() => getAxisNumber(null!)).toThrow();
  });
});
describe('extractRouterKey', () => {
  test('extracts router key from higher key', () => {
    expect(extractRouterKey(TEST_ROUTER_KEY)).toBe(TEST_ROUTER_KEY);
    expect(extractRouterKey(TEST_CONNECTION_KEY)).toBe(TEST_ROUTER_KEY);
    expect(extractRouterKey(TEST_DEVICE_KEY)).toBe(TEST_ROUTER_KEY);
    expect(extractRouterKey(TEST_AXIS_KEY)).toBe(TEST_ROUTER_KEY);
  });
  test('throws error on invalid key', () => {
    expect(() => extractRouterKey('')).toThrow();
    expect(() => extractRouterKey(null!)).toThrow();
  });
});
describe('extractConnectionKey', () => {
  test('extracts router key from higher key', () => {
    expect(extractConnectionKey(TEST_CONNECTION_KEY)).toBe(TEST_CONNECTION_KEY);
    expect(extractConnectionKey(TEST_DEVICE_KEY)).toBe(TEST_CONNECTION_KEY);
    expect(extractConnectionKey(TEST_AXIS_KEY)).toBe(TEST_CONNECTION_KEY);
  });
  test('throws error on invalid key', () => {
    expect(() => extractConnectionKey('')).toThrow();
    expect(() => extractConnectionKey(null!)).toThrow();
  });
});
describe('extractConnectionKey', () => {
  test('extracts router key from higher key', () => {
    expect(extractDeviceKey(TEST_DEVICE_KEY)).toBe(TEST_DEVICE_KEY);
    expect(extractDeviceKey(TEST_AXIS_KEY)).toBe(TEST_DEVICE_KEY);
  });
  test('throws error on invalid key', () => {
    expect(() => extractDeviceKey('')).toThrow();
    expect(() => extractDeviceKey(null!)).toThrow();
  });
});
describe('getIncludedKeys', () => {
  test('returns all the parent keys including the key itself', () => {
    expect(getIncludedKeys(TEST_ROUTER_KEY)).toEqual({
      [EntityKeyType.ROUTER]: TEST_ROUTER_KEY,
    });
    expect(getIncludedKeys(TEST_CONNECTION_KEY)).toEqual({
      [EntityKeyType.ROUTER]: TEST_ROUTER_KEY,
      [EntityKeyType.CONNECTION]: TEST_CONNECTION_KEY,
    });
    expect(getIncludedKeys(TEST_DEVICE_KEY)).toEqual({
      [EntityKeyType.ROUTER]: TEST_ROUTER_KEY,
      [EntityKeyType.CONNECTION]: TEST_CONNECTION_KEY,
      [EntityKeyType.DEVICE]: TEST_DEVICE_KEY,
    });
    expect(getIncludedKeys(TEST_AXIS_KEY)).toEqual({
      [EntityKeyType.ROUTER]: TEST_ROUTER_KEY,
      [EntityKeyType.CONNECTION]: TEST_CONNECTION_KEY,
      [EntityKeyType.DEVICE]: TEST_DEVICE_KEY,
      [EntityKeyType.AXIS]: TEST_AXIS_KEY,
    });
  });
  test('returns empty object for null', () => {
    expect(getIncludedKeys(null)).toEqual({});
  });
});
