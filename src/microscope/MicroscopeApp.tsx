import React, { useEffect }  from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';

import { NoContentMessage, Title } from '../components';
import { ConnectionsView } from '../connection_manager';
import { useActions } from '../utils';
import { SetLabel } from '../connection_manager_ui';

import { actions as actionsDefinition } from './actions';
import { selectConnection, selectProductionMode, selectSelectedKey } from './selectors';
import { ConfigDialog } from './config/Config';
import { ScopeTest } from './ScopeTest';
import { Controls } from './Controls';
import { PresetsDialog } from './PresetEditor';
import { FactoryResetDialog } from './FactoryReset';
import { MultiDAcquisition } from './multidacq/MultiDAcquisition';
import { Paths } from './types';
import { Container } from './Container';
import { DemoMode } from './DemoMode';

const noSelection = <NoContentMessage title="Welcome to Microscope!" message="Select a microscope connection from the menu on the left."/>;

export const MicroscopeApp: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const selectedKey = useSelector(selectSelectedKey);
  const connection = useSelector(selectConnection);
  const productionMode = useSelector(selectProductionMode);

  useEffect(() => {
    actions.mount();
    return () => { actions.unmount() };
  }, []);

  return (
    <div className="connection-view-and-app">
      <Title>Microscope</Title>

      <ConnectionsView
        selectable={['connection']}
        selected={selectedKey}
        onSelect={actions.setSelectedKey}/>
      <div className="app-ui microscope-app">
        {connection == null ? noSelection :
        <Switch>
          <Route path={Paths.Overview} exact component={() => <Container className="microscope-overview">
            <DemoMode/>
            {productionMode && <ScopeTest/>}
            <Controls/>
          </Container>}/>
          <Route path={Paths.MultiDAcq} component={MultiDAcquisition}/>
          <Redirect to={Paths.Overview}/>
        </Switch>}
        <div className="modals">
          <SetLabel/>
          <ConfigDialog/>
          <PresetsDialog/>
          <FactoryResetDialog/>
        </div>
      </div>
    </div>);
};
