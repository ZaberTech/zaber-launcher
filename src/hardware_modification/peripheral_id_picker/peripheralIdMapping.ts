import _ from 'lodash';

import type { NonNullPeripheralTraits } from '../reducer';

const linearStepper = { motion: 'linear' as const, motor: 'stepper' as const };
const rotaryStepper = { motion: 'rotary' as const, motor: 'stepper' as const };

const peripheralIdMapping: (NonNullPeripheralTraits & { peripheralId: number })[] = [
  { ...linearStepper, singleEncoder: 'none', diffEncoder: 'none', peripheralId: 88800 },
  { ...linearStepper, singleEncoder: 'motor', diffEncoder: 'none', peripheralId: 88801 },
  { ...linearStepper, singleEncoder: 'direct', diffEncoder: 'none', peripheralId: 88802 },
  { ...linearStepper, singleEncoder: 'none', diffEncoder: 'direct', peripheralId: 88803 },
  { ...linearStepper, singleEncoder: 'none', diffEncoder: 'analog', peripheralId: 88804 },
  { ...linearStepper, singleEncoder: 'none', diffEncoder: 'motor', peripheralId: 88812 },
  { motion: 'linear', motor: 'linear', singleEncoder: 'absolute', diffEncoder: 'analog', peripheralId: 88805 },
  { motion: 'linear', motor: 'voice', singleEncoder: 'none', diffEncoder: 'analog', peripheralId: 88806 },
  { ...rotaryStepper, singleEncoder: 'none', diffEncoder: 'none', peripheralId: 88807 },
  { ...rotaryStepper, singleEncoder: 'motor', diffEncoder: 'none', peripheralId: 88808 },
  { ...rotaryStepper, singleEncoder: 'direct', diffEncoder: 'none', peripheralId: 88809 },
  { ...rotaryStepper, singleEncoder: 'none', diffEncoder: 'direct', peripheralId: 88810 },
  { ...rotaryStepper, singleEncoder: 'none', diffEncoder: 'analog', peripheralId: 88811 },
  { ...rotaryStepper, singleEncoder: 'none', diffEncoder: 'motor', peripheralId: 88813 },
];

function traitsMatch(t1: NonNullPeripheralTraits, t2: NonNullPeripheralTraits): boolean {
  return t1.motion === t2.motion && t1.motor === t2.motor && t1.singleEncoder === t2.singleEncoder && t1.diffEncoder === t2.diffEncoder;
}

export function matchTraitsToId(traits: NonNullPeripheralTraits): number | undefined {
  return peripheralIdMapping.find(line => traitsMatch(line, traits))?.peripheralId;
}


const get3rdPartyIds = _.once(
  () => peripheralIdMapping.map(line => line.peripheralId)
);

export function is3rdPartyId(id?: number | null): boolean {
  return id != null ? get3rdPartyIds().includes(id) : false;
}

const getRotary3rdPartyIds = _.once(
  () => peripheralIdMapping.filter(line => line.motion === 'rotary').map(line => line.peripheralId)
);

export function isRotary3rdPartyId(id?: number | null): boolean {
  return id != null ? getRotary3rdPartyIds().includes(id) : false;
}

export function getErrorMessageForTraitMapping(traits: NonNullPeripheralTraits): string {
  if (traits.motor === 'stepper' && traits.singleEncoder !== 'none' && traits.diffEncoder !== 'none') {
    return 'Zaber controllers can only support one encoder on stepper motor peripherals.';
  }
  if (traits.singleEncoder === 'absolute' && traits.motor !== 'linear') {
    return 'Absolute phase motor encoders are only compatible with three-phase direct motors.';
  }
  if (traits.motion === 'linear' && traits.motor === 'linear') {
    if (traits.singleEncoder !== 'absolute' || traits.diffEncoder !== 'analog') {
      return [
        'Zaber controllers require a single-ended absolute phase motor encoder',
        'and a differential direct analog encoder to support linear motors.'
      ].join(' ');
    }
  }
  if (traits.motion === 'linear' && traits.motor === 'voice') {
    if (traits.diffEncoder !== 'analog') {
      return 'Zaber controllers require a differential analog direct encoder to support voice coil motors.';
    } else if (traits.singleEncoder !== 'none') {
      return 'Voice coil motors may not have a single-ended encoder.';
    }
  }
  if (traits.motion === 'rotary' && traits.motor !== 'stepper') {
    return 'Non-stepper-motor rotary peripherals are not compatible with Zaber controllers.';
  }

  return 'There is no support for this type of peripheral at this time.';
}
