import React from 'react';
import { Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import type { Moveable } from '../types';

import { actionDefinitions } from './actions';
import type { AxisMovementState } from './types';

interface Props {
  moveable: Moveable;
  direction: 'prev' | 'next';
  state: AxisMovementState;
}

export const MoveIndex: React.FC<Props> = ({ moveable, direction, state }) => {
  const actions = useActions(actionDefinitions);

  const move = () => {
    actions.issueCommand(moveable, { type: 'moveIndex', direction });
  };
  const props = {
    title: direction === 'prev' ? 'Move to Previous Position' : 'Move to Next Position',
    className: `move-index-${direction}`,
    activated: state.lastCommand?.type === 'moveIndex' && state.lastCommand.direction === direction,
    onClick: move,
  };

  return <Icons.Rotate highlight="off" {...props}/>;
};
