import _ from 'lodash';
import {
  ascii, CommandFailedException, ConnectionClosedException, ConnectionFailedException, NoValueForKeyException,
  RequestTimeoutException, Units,
} from '@zaber/motion';
import { call } from 'redux-saga/effects';

import { EntityKey } from '../keys';
import { delay, RT, SagaIter } from '../utils';
import { getConnection } from '../connection_manager';
import { environment } from '../environment';

export async function getNumericSettings(
  deviceOrAxis: ascii.Device, setting: string, expectedCount: number,
): Promise<(number | null)[]> {
  try {
    const response = await deviceOrAxis.genericCommand(`get ${setting}`);
    const values = response.data.split(' ').map(text => {
      const value = parseFloat(text);
      return Number.isNaN(value) ? null : value;
    });
    return values;
  } catch (e) {
    if (e instanceof CommandFailedException) {
      return _.range(expectedCount).map(() => null);
    }
    throw e;
  }
}

export async function getNumericSetting(deviceOrAxis: ascii.Device | ascii.Axis, setting: string, units?: Units): Promise<number | null> {
  try {
    const value = await deviceOrAxis.settings.get(setting, units);
    return value;
  } catch (e) {
    if (e instanceof CommandFailedException) {
      return null;
    }
    throw e;
  }
}

export async function getStoredBoolOrDefault(ascii: ascii.Device | ascii.Axis, key: string, defaultBool: boolean): Promise<boolean> {
  try {
    return await ascii.storage.getBool(key);
  } catch (e) {
    if (e instanceof NoValueForKeyException) {
      return defaultBool;
    }
    throw e;
  }
}

export async function getStoredNumberOrDefault<T>(ascii: ascii.Device | ascii.Axis, key: string, defaultVal: T): Promise<number | T> {
  try {
    return await ascii.storage.getNumber(key);
  } catch (e) {
    if (e instanceof NoValueForKeyException) {
      return defaultVal;
    }
    throw e;
  }
}

export type SerialNumberInfo = {
  /** The serial number of the device being upgraded */
  number: number;
  /** The number of devices on the chain with this serial number (may be more than 0 if there are internal devices with generic SNs) */
  count: number;
};

/**
 * Polls a connection until a device with the given serial number appears
 * @param oldDeviceKey The original device key of the device
 * @param serialNumberInfo the serial number to wait to appear
 * @returns device with specified serial number or null if the device is not found before timing out
 */
export function* waitForDeviceToRestart(
  oldDeviceKey: EntityKey, snInfo: SerialNumberInfo, timeout: number,
): SagaIter<ascii.Device | 'many' | 'timeout'> {
  const startTime = performance.now();
  while (performance.now() - startTime < timeout) {
    try {
      yield delay(environment.isTest ? 1 : 1000);
      const connection: RT<typeof getConnection> = yield call(getConnection, oldDeviceKey);
      const allSerialNumberResponses: RT<typeof connectionSerialNumbers> = yield call(connectionSerialNumbers, connection);
      const responsesWithSerialNumber = allSerialNumberResponses.filter(({ serialNumber }) => serialNumber === snInfo.number);
      if (responsesWithSerialNumber.length >= snInfo.count) {
        if (responsesWithSerialNumber.length === 1) {
          return connection.getDevice(responsesWithSerialNumber[0].device);
        } else {
          return 'many';
        }
      }
    } catch (e) {
      if (![
        RequestTimeoutException,
        ConnectionFailedException,
        ConnectionClosedException,
      ].some(Error => e instanceof Error)) {
        throw e;
      }
    }
  }
  return 'timeout';
}

/**
 * Get the serial number of a device and the number of devices on the chain with that serial number.
 */
export async function getSerialNumberInfo(device: ascii.Device): Promise<SerialNumberInfo> {
  const { serialNumber } = device.identity;
  const serialNumbers = await connectionSerialNumbers(device.connection);
  return {
    number: serialNumber,
    count: serialNumbers.filter(({ serialNumber: other }) => other === serialNumber).length,
  };
}

async function connectionSerialNumbers(connection: ascii.Connection) {
  const responses = await connection.genericCommandMultiResponse('get system.serial');
  return responses.map(resp => ({ device: resp.deviceAddress, serialNumber: parseInt(resp.data, 10) }));
}
