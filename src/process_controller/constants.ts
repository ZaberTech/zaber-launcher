const POLLS_PER_SECOND = 1;
export const POLL_DELAY_MS = 1000 / POLLS_PER_SECOND;
export const POLL_RETENTION_MS = 10_000;
export const POLL_RETENTION_SEC = POLL_RETENTION_MS / 1000;
export const TITLE = 'Process Controller';
