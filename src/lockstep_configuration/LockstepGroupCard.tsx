import { HeaderCard, Text, Button, Flex } from '@zaber/react-library';
import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';

import { AxisCard } from './AxisCard';
import { actions as actionsDefinition } from './actions';
import type { AxisDataLockstep } from './reducer';
import type { DeviceStateLockstep } from './selectors';

interface Props {
  header: React.ReactNode;
  groupNumber: number;
  device: DeviceStateLockstep;
  axesData: AxisDataLockstep[];
}

export const LockstepGroupCard: React.FC<Props> = ({ header, groupNumber, axesData, device }) => {
  const actions = useActions(actionsDefinition);

  return (
    <HeaderCard className="lockstep-group-card" header={header}>
      <Flex.Row>
        <Text t={Text.Type.H4}>{`Lockstep Group ${groupNumber}`}</Text>
        <Flex.Spacer/>
        <Button color="grey" onClick={() => actions.breakLockstepGroup(groupNumber)}>
          Break Lockstep Group
        </Button>
      </Flex.Row>

      {axesData.map(axisData =>
        <AxisCard
          key={axisData.axisNumber}
          axisData={axisData}
          axis={device.axes[axisData.axisNumber - 1]}/>)}
    </HeaderCard>
  );
};
