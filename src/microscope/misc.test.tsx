import React from 'react';
import { RenderResult, render } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { StorageMock, wrapWithNewStore, wrapWithRouter } from '../test';
import { MessageRoutersService } from '../message_router';
import { MOCK_SERIAL_PORT, mockDataForDeviceWithoutPeripherals, mockLocalConnections } from '../connection_manager/mocks';

import { CONNECTION_KEY, MessageRoutersServiceMock } from './mocks';
import { Storage } from '../app_components';
import { TEST_END } from './types';
import { addConnectionActions } from '../add_connection';
import { actions as appsActions } from '../apps/actions';

const TestApp = wrapWithNewStore(wrapWithRouter(() => null));

let wrapper: RenderResult;

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(Storage).to(StorageMock);
});

afterEach(() => {
  const store = TestApp.testStore;
  wrapper?.unmount();
  wrapper = null!;

  if (store) {
    store.dispatch({ type: TEST_END });
  }

  destroyContainer();
});

function microscopeDevices(connectionKey: string) {
  const device1 = mockDataForDeviceWithoutPeripherals(connectionKey, 1, 1);
  device1.identity.name = 'X-MOR';
  const device2 = mockDataForDeviceWithoutPeripherals(connectionKey, 2, 1);
  device2.identity.name = 'X-FCR';
  const device3 = mockDataForDeviceWithoutPeripherals(connectionKey, 3, 1);
  device3.identity.name = 'X-LHM';
  return [device1, device2, device3];
}

describe('pinning microscope App', () => {
  beforeEach(() => {
    appsActions.pinApp = jest.fn(() => ({ type: 'MOCK', payload: null! }));

    wrapper = render(<TestApp/>);

    // simulate adding connection
    TestApp.testStore.dispatch(addConnectionActions.addConnectionDone(CONNECTION_KEY));
  });

  test('pins the app if microscope is detected (but only once)', async () => {
    mockLocalConnections(TestApp.testStore, {
      [MOCK_SERIAL_PORT]: microscopeDevices,
    });

    const pinMock = appsActions.pinApp as jest.Mock;
    expect(pinMock).toHaveBeenLastCalledWith('/microscope', true);
    pinMock.mockClear();

    mockLocalConnections(TestApp.testStore, {
      [MOCK_SERIAL_PORT]: microscopeDevices,
    });
    expect(pinMock).not.toHaveBeenCalled();
  });

  test('does not pin the app for one microscope device', async () => {
    mockLocalConnections(TestApp.testStore, {
      [MOCK_SERIAL_PORT]: connectionKey => {
        const microscope = microscopeDevices(connectionKey);
        return microscope.slice(0, 1);
      },
    });

    expect(appsActions.pinApp).not.toHaveBeenCalled();
  });
});
