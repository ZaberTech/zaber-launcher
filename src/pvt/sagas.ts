import { writeFile, open, FileHandle } from 'fs/promises';

import dataUriToBuffer from 'data-uri-to-buffer';
import type { Store } from 'redux';
import type { SagaIterator } from 'redux-saga';
import _ from 'lodash';
import { SagaReturnType, all, call, delay, put, race, select, take, takeEvery, takeLeading } from 'redux-saga/effects';
import { BrowserWindow } from '@electron/remote';
import {
  Angle,
  AngularVelocity,
  CommandFailedException,
  ConversionFailedException,
  Length,
  PvtExecutionException,
  PvtMovementFailedException,
  PvtMovementInterruptedException,
  Time,
  Velocity,
  ascii
} from '@zaber/motion';
import { AxisType, PvtAxisDefinition, PvtAxisType, type PvtSequence } from '@zaber/motion/ascii';

import { reduxStoreSymbol } from '../store';
import { getContainer } from '../container';
import { getDevice, selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import { environment } from '../environment';
import { FileWatcher } from '../file_watcher';
import { TimeMeasuring } from '../time';
import { MS_PER_SEC } from '../types';
import { convertBetweenUnits, derivativeUnit, getDimensionName, getUnitLabel } from '../units';
import { Action, AsyncReturnType, ensureError, naturalSort, RT, throwUnexpectedError, tryAccess } from '../utils';
import type { IUnitConverter } from '../zml';
import { Dialogs } from '../dialogs';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import { detectSeriesAndUnits, detectHeaders, getDataRows, loadCsv, CsvCellType, saveCsv, formatCsv } from './csv';
import { PathDiscretizer } from './pathDiscretizer';
import { getAccelerationScale, getVelocityScale } from './pvtSeriesHelper';
import { PvtSegment } from './pvtSegment';
import {
  selectCsvImportOptions,
  selectCurrentFile,
  selectUserPath,
  selectPreviousFile,
  selectDeviceOptions,
  selectPositionLimits,
  selectVelocityLimits,
  selectAccelerationLimits,
  selectDevicePath,
  selectDeviceLoadDialog,
  selectPlaybackTime,
  selectPollDefaults,
  selectDevicePositionNative,
  selectDevicePosition,
  selectFile,
  selectIsFileSaved,
  selectFidelity
} from './selectors';
import { Series, Path, Point, Span, ViewMode, Message } from './types';
import {
  clip,
  convertToCsvData,
  expandAxisNumberIfInLockstep,
  findBugs,
  getLockstepGroup,
  getLockstepGroupNumber,
  isFirstPointRelative,
  makeBufferAxisName
} from './utils';
import { discretizePath } from './pvtCurveUtils';


const PVT_STREAM = 1;
const FAST_POLL_INTERVAL = 100;

export function* pvtSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.FILE_OPEN, fileOpenWithDialog),
    takeEvery(ActionTypes.FILE_SAVE, fileSave),
    takeEvery(ActionTypes.FILE_SAVE_AS, fileSaveAs),
    takeEvery(ActionTypes.RELOAD, onReload),
    takeEvery(ActionTypes.UNDO, onPathEdited),
    takeEvery(ActionTypes.REDO, onPathEdited),
    takeEvery(ActionTypes.EDIT_SEGMENT_TIME, onPathEdited),
    takeEvery(ActionTypes.EDIT_POSITION, onPathEdited),
    takeEvery(ActionTypes.EDIT_VELOCITY, onPathEdited),
    takeEvery(ActionTypes.ADD_POINT, onPathEdited),
    takeEvery(ActionTypes.DELETE_POINT, onPathEdited),
    takeEvery(ActionTypes.FILE_SAVE_AND_OPEN_EXAMPLE, saveAndLoadExample),
    takeEvery(ActionTypes.SET_CURRENT_FILE, watchDifferentFile),
    takeEvery(ActionTypes.SET_AUTO_RELOAD, startOrStopFileWatcher),
    takeEvery(ActionTypes.SAVE_TIME_SERIES_IMAGE, saveImageData),
    takeEvery(ActionTypes.EXPORT_TRAJECTORY, onExportTrajectory),
    takeEvery(ActionTypes.COMMIT_DEVICE_SELECTION, onDeviceChanged),
    takeEvery(ActionTypes.RESET, reset),
    takeEvery(ActionTypes.RUN, run),
    takeEvery(ActionTypes.STOP, stop),
    takeEvery(ActionTypes.EXPORT_IMAGE, exportCurveImage),
    takeLeading(ActionTypes.POLL_DEVICE_POSITION, devicePollLoop),
    takeEvery(ActionTypes.SET_BUFFER_LOAD_DIALOG_VISIBLE, getBufferList),
    takeEvery(ActionTypes.GET_BUFFER_AXIS_COUNT, getBufferAxisCount),
    takeEvery(ActionTypes.LOAD_FROM_DEVICE_BUFFER, loadFromBuffer),
    takeEvery(ActionTypes.STORE_RELATIVE_START, storeDevicePosition),
  ]);
}


function* confirmDiscardUnsaved(): SagaIterator<boolean> {
  const isSaved = (yield select(selectIsFileSaved)) as ReturnType<typeof selectIsFileSaved>;
  if (isSaved) {
    return true;
  }

  const dialogResult: SagaReturnType<typeof Dialogs.showMessageBox> = yield call(Dialogs.showMessageBox, {
    type: 'question',
    buttons: ['Save', 'Discard', 'Cancel'],
    defaultId: 0,
    title: 'Unsaved Changes',
    message: 'Discard unsaved changes?',
  });

  switch (dialogResult.response) {
    case 0: {
      const filePath = (yield select(selectFile)) as ReturnType<typeof selectFile>;
      if (filePath != null) {
        return (yield call(fileSave)) as RT<typeof fileSave>;
      } else {
        return (yield call(fileSaveAs)) as RT<typeof fileSaveAs>;
      }
    }
    case 1:
      return true;
    default:
      return false;
  }
}


function* fileOpenWithDialog(): SagaIterator {
  if (!(yield call(confirmDiscardUnsaved))) {
    return;
  }

  const dialogResult: SagaReturnType<typeof Dialogs.showOpenDialog> = yield call(Dialogs.showOpenDialog, {
    title: 'Open CSV file',
    filters: [{ name: 'CSV files', extensions: ['csv'] }, { name: 'All files', extensions: ['*'] }],
  });

  if (dialogResult.canceled) {
    return;
  }

  if (dialogResult.filePaths.length !== 1) {
    return;
  }

  yield call(fileOpen, dialogResult.filePaths[0]);
}


function* fileOpen(filePath: string): SagaIterator {
  try {
    const path = (yield call(loadFile, filePath, true)) as SagaReturnType<typeof loadFile>;
    if (!path) {
      return;
    }

    yield put(actions.pollDevicePosition('stop'));
    yield put(actions.setCurrentFile(filePath));
    yield put(actions.storeUserPath(path));
    yield put(actions.commitEdit());
    yield put(actions.clearUndoHistory());
    yield put(actions.fileSetLastSavedContent(path));
    yield call(checkAxesAndUnits);
    yield call(checkDataValid);
    yield call(discretizeAndStore);
    yield call(convertDeviceLimits);
    yield call(convertStartPosition);
    yield call(scanForBugs);
    yield put(actions.pollDevicePosition('default'));
  } catch (err) {
    yield put(actions.showError(ensureError(err).message));
  }
}


function* fileSave(): SagaIterator<boolean> {
  const filePath = (yield select(selectFile)) as ReturnType<typeof selectFile>;
  if (!filePath) {
    return (yield call(fileSaveAs)) as RT<typeof fileSaveAs>;
  }

  const userPath: Path | null = yield select(selectUserPath);
  if (!userPath) {
    return true;
  }

  const { autoReload } = (yield select(selectCsvImportOptions)) as ReturnType<typeof selectCsvImportOptions>;

  try {
    yield put(actions.setAutoReload(false));
    yield call(saveFile, userPath, filePath);
    yield put(actions.setAutoReload(autoReload));
    yield put(actions.fileSetLastSavedContent(userPath));
    return true;
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.showError(err.message));
  }

  return false;
}


function* fileSaveAs(): SagaIterator<boolean> {
  const dialogResult: SagaReturnType<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
    title: 'Save CSV file',
    filters: [{ name: 'CSV files', extensions: ['csv'] }, { name: 'All files', extensions: ['*'] }],
  });

  if (dialogResult.canceled || dialogResult.filePath == null) {
    return false;
  }

  const userPath: Path | null = yield select(selectUserPath);
  if (!userPath) {
    return true;
  }

  try {
    yield call(saveFile, userPath, dialogResult.filePath);
    yield put(actions.fileSetLastSavedContent(userPath));
    yield put(actions.setCurrentFile(dialogResult.filePath));
    return true;
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.showError(err.message));
  }

  return false;
}


function* onReload({ payload: { forced } }: Action<ActionsToPayloads[ActionTypes.RELOAD]>): SagaIterator {
  const filePath = (yield select(selectCurrentFile)) as ReturnType<typeof selectCurrentFile>;
  const { autoReload } = (yield select(selectCsvImportOptions)) as ReturnType<typeof selectCsvImportOptions>;
  if (!filePath) {
    return;
  }

  if (!autoReload && !forced) {
    return;
  }

  const isSaved = (yield select(selectIsFileSaved)) as ReturnType<typeof selectIsFileSaved>;
  if (!isSaved) {
    const dialogResult: SagaReturnType<typeof Dialogs.showMessageBox> = yield call(Dialogs.showMessageBox, {
      type: 'question',
      buttons: ['Keep current data', 'Reload file'],
      defaultId: 0,
      title: 'Conflicting edits',
      message: 'File has changed on disk but there are also unsaved changes in memory. Reload the file anyway?',
    });

    if (dialogResult.response === 0) {
      return;
    }
  }

  try {
    const path = (yield call(loadFile, filePath, false)) as SagaReturnType<typeof loadFile>;
    if (!path) {
      return;
    }

    yield put(actions.pollDevicePosition('stop'));
    const oldPath = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;
    yield put(actions.storeUserPath(path));
    yield put(actions.commitEdit());
    yield put(actions.clearUndoHistory());
    yield put(actions.fileSetLastSavedContent(path));
    const { deviceKey } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
    if (deviceKey) {
      if (!oldPath
        || !_.isEqual(oldPath.series.map(s => s.name), path.series.map(s => s.name))
        || oldPath.positionUnit !== path.positionUnit) {
        yield put(actions.setAxisDialogVisible(true, false));
      }
    }

    yield call(checkDataValid);
    yield call(discretizeAndStore);
    yield call(convertDeviceLimits);
    yield call(convertStartPosition);
    yield call(scanForBugs);
    yield put(actions.pollDevicePosition('default'));
  } catch (err) {
    yield put(actions.setAutoReload(false));
    yield put(actions.showError(`Auto-reload failed: ${ensureError(err).message}`));
  }
}


function* onPathEdited(): SagaIterator {
  yield call(checkDataValid);
  yield call(discretizeAndStore);
  yield call(convertDeviceLimits);
  yield call(convertStartPosition);
  yield call(scanForBugs);
}


function* saveAndLoadExample(
  { payload: { path } }: Action<ActionsToPayloads[ActionTypes.FILE_SAVE_AND_OPEN_EXAMPLE]>): SagaIterator {
  if (!(yield call(confirmDiscardUnsaved))) {
    return;
  }

  const dialogResult: SagaReturnType<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
    title: 'Save CSV file',
    filters: [{ name: 'CSV files', extensions: ['csv'] }, { name: 'All files', extensions: ['*'] }],
    defaultPath: 'example.csv',
  });

  if (dialogResult.canceled || !dialogResult.filePath) {
    return;
  }


  const { autoReload } = (yield select(selectCsvImportOptions)) as ReturnType<typeof selectCsvImportOptions>;

  try {
    yield put(actions.setAutoReload(false));
    yield call(saveFile, path, dialogResult.filePath);
    yield call(fileOpen, dialogResult.filePath);
    yield put(actions.setViewMode(ViewMode.TimeSeries));
    yield put(actions.setAutoReload(autoReload));
  } catch (err) {
    yield put(actions.showError(ensureError(err).message));
  }
}


function* loadFile(path: string, interactive: boolean): SagaIterator<Path | null> {
  const fileData: RT<typeof loadCsv> = yield call(loadCsv, path);
  if (!fileData?.length) {
    throw new Error(`Could not interpret "${path}" as a CSV file.`);
  }

  const headerRows = detectHeaders(fileData);
  const map = detectSeriesAndUnits(headerRows, fileData[0].length);
  const dataRows = getDataRows(fileData, headerRows.length);
  const absoluteTime = !_.some(dataRows, (row, i) => (i > 0) && row[map.timeColumn] <= dataRows[i - 1][map.timeColumn]);

  if (interactive) {
    yield put(actions.setCsvImportOptions({
      dialogVisible: true,
      allowAbsoluteTime: absoluteTime,
      absoluteTime,
      detectedTimeUnit: map.timeUnit,
      detectedPositionUnit: map.positionUnit,
      detectedVelocityUnit: map.velocityUnit,
      selectedTimeUnit: map.timeUnit ?? Time.MILLISECONDS,
      selectedPositionUnit: map.positionUnit,
      selectedVelocityUnit: map.velocityUnit,
    }));

    // Known issue: If the user has auto-reload enabled and saves the previous file again while the
    // import dialog is open, their unit selections in the dialog could change spontaneously.
    // Not fixing now because it seems unlikely, but if that changes we could buffer the file watcher
    // signals while the dialog is open, and if it fires then reload when the dialog is cancelled.

    const proceed: {
      go: unknown;
      stop: unknown;
    } = yield race({
      go: take(ActionTypes.CONTINUE_IMPORT),
      stop: take(ActionTypes.CANCEL_IMPORT),
    });

    if (!proceed.go) {
      return null;
    }
  } else {
    const options = (yield select(selectCsvImportOptions)) as ReturnType<typeof selectCsvImportOptions>;
    if (options.absoluteTime && !absoluteTime) {
      throw new Error(
        'File was previously imported with absolute time selected but the current data cannot be absolute.');
    }

    yield put(actions.setCsvImportOptions({
      selectedTimeUnit: map.timeUnitDetected ? map.timeUnit! : options.selectedTimeUnit,
      selectedPositionUnit: map.positionUnitDetected ? map.positionUnit : options.selectedPositionUnit,
      selectedVelocityUnit: map.velocityUnitDetected ? map.velocityUnit : options.selectedVelocityUnit,
    }));
    yield put(actions.setAutoReload(options.autoReload));
  }

  const options = (yield select(selectCsvImportOptions)) as ReturnType<typeof selectCsvImportOptions>;
  const velocityScale = getVelocityScale(options.selectedPositionUnit, options.selectedVelocityUnit);
  const accelScale = getAccelerationScale(options.selectedPositionUnit, options.selectedVelocityUnit);

  // Convert the data.
  const series: Series[] = map.series.map(s => {
    const points: Point[] = [];
    let absTime = 0;
    for (let i = 0; i < dataRows.length; i++) {
      const row = dataRows[i];
      let dt, time;
      if (options.absoluteTime) {
        dt = i > 0 ? row[map.timeColumn] - dataRows[i - 1][map.timeColumn] : row[map.timeColumn];
        dt = convertBetweenUnits(dt, options.selectedTimeUnit, Time.SECONDS);
        time = convertBetweenUnits(row[map.timeColumn], options.selectedTimeUnit, Time.SECONDS);
        absTime = time;
      } else {
        dt = convertBetweenUnits(row[map.timeColumn], options.selectedTimeUnit, Time.SECONDS);
        absTime += dt;
        time = absTime;
      }

      const newPoint: Point = {
        dt,
        time,
        pos: row[s.positionColumn],
        vel: row[s.velocityColumn],
      };

      if (points.length) {
        const segment = new PvtSegment(_.last(points)!, newPoint, velocityScale, accelScale);
        newPoint.accel = segment.getAccel(dt);
      }

      points.push(newPoint);
    }

    return {
      name: s.name,
      points,
    };
  });

  return {
    positionUnit: options.selectedPositionUnit,
    velocityUnit: options.selectedVelocityUnit,
    series,
  };
}


function* saveFile(path: Path, fileName: string): SagaIterator {
  const cells = convertToCsvData(path);
  yield call(saveCsv, cells, fileName);
}


function onFileUpdated() {
  const store = getContainer().get<Store>(reduxStoreSymbol);
  store.dispatch(actions.reload());
}


function* watchDifferentFile(
  { payload: { path } }: Action<ActionsToPayloads[ActionTypes.SET_CURRENT_FILE]>): SagaIterator {
  const watcher = getContainer().get(FileWatcher);
  const prevFilePath = (yield select(selectPreviousFile)) as ReturnType<typeof selectPreviousFile>;
  if (prevFilePath) {
    watcher.unregister(prevFilePath);
  }

  const { autoReload } = (yield select(selectCsvImportOptions)) as ReturnType<typeof selectCsvImportOptions>;
  if (path && autoReload) {
    watcher.register(path, onFileUpdated);
  }
}


function* startOrStopFileWatcher(
  { payload: { autoReload } }: Action<ActionsToPayloads[ActionTypes.SET_AUTO_RELOAD]>): SagaIterator {
  const currentFile = (yield select(selectCurrentFile)) as ReturnType<typeof selectCurrentFile>;
  const watcher = getContainer().get(FileWatcher);
  if (currentFile) {
    if (autoReload) {
      watcher.register(currentFile, onFileUpdated);
    } else {
      watcher.unregister(currentFile);
    }
  }
}


function* discretizeAndStore(): SagaIterator {
  const userPath = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;
  if (userPath == null || userPath.series.length < 1 || userPath.series[0].points.length < 2) {
    yield put(actions.storeDevicePath(null));
    return;
  }

  const fidelity = (yield select(selectFidelity)) as ReturnType<typeof selectFidelity>;

  const path: AsyncReturnType<typeof discretizePath> = yield call(discretizePath, userPath, fidelity);
  yield put(actions.storeDevicePath(path));
}


type SaveImageActionPayload = Action<ActionsToPayloads[ActionTypes.SAVE_TIME_SERIES_IMAGE]>;
function* saveImageData({ payload: { format, data } }: SaveImageActionPayload): SagaIterator {
  let fileName: string = (yield select(selectCurrentFile)) ?? 'untitled';
  fileName = fileName.replace(/\.\w*$/, `.${format}`);

  const dialogResult: SagaReturnType<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
    title: `Save Time Series to ${format.toLocaleUpperCase()} File`,
    properties: ['showOverwriteConfirmation'],
    filters: [{ name: `${format.toLocaleUpperCase()} files`, extensions: [format] }, { name: 'All files', extensions: ['*'] }],
    defaultPath: fileName,
  });

  if (dialogResult.canceled) {
    return;
  }

  if (dialogResult.filePath == null) {
    yield put(actions.showError('Could not save the file to the selected location'));
    return;
  }

  try {
    const image = dataUriToBuffer(data);
    yield call(writeFile, dialogResult.filePath, image, { encoding: 'binary', flag: 'w' });
  } catch (err) {
    yield put(actions.showError(`There was an error saving the file: ${ensureError(err)}`));
  }
}


function* onExportTrajectory({ payload: { timeStep } }: Action<ActionsToPayloads[ActionTypes.EXPORT_TRAJECTORY]>): SagaIterator {
  const userPath = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;
  if (userPath == null || userPath.series.length < 1 || userPath.series[0].points.length < 2) {
    return;
  }

  const dialogResult: SagaReturnType<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
    title: 'Export Trajectory',
    filters: [{ name: 'CSV files', extensions: ['csv'] }, { name: 'All files', extensions: ['*'] }],
  });

  if (dialogResult.canceled || dialogResult.filePath == null) {
    return;
  }

  const BUFFER_LINES = 500;
  let fp: FileHandle | null = null;
  try {
    yield put(actions.setExportInProgress(true));
    fp = (yield call(open, dialogResult.filePath, 'w')) as RT<typeof open>;

    const headings = [`Time Step (${getUnitLabel(timeStep.units)})`, `Accumulated Time (${getUnitLabel(timeStep.units)})`];
    userPath.series.forEach(ser => {
      headings.push(`${ser.name} Position (${getUnitLabel(userPath.positionUnit)})`);
      headings.push(`${ser.name} Velocity (${getUnitLabel(userPath.velocityUnit)})`);
    });

    yield call([fp, fp.write], formatCsv([headings]));

    const discretizer = new PathDiscretizer(userPath);
    const timeStepSeconds = convertBetweenUnits(timeStep.value, timeStep.units, Time.SECONDS);

    let rowCount = 0;
    let rows: CsvCellType[][] = [];
    for (const points of discretizer.discretizeToStream(timeStepSeconds)) {
      const row: CsvCellType[] = [];
      row.push(convertBetweenUnits(points[0].dt, Time.SECONDS, timeStep.units));
      row.push(convertBetweenUnits(points[0].time, Time.SECONDS, timeStep.units));
      points.forEach(p => {
        row.push(p.pos);
        row.push(p.vel);
      });

      rows.push(row);
      rowCount++;

      if (rowCount >= BUFFER_LINES) {
        const data = formatCsv(rows);
        rowCount = 0;
        rows = [];
        yield call([fp, fp.write], data);
      }
    }

    if (rowCount > 0) {
      const data = formatCsv(rows);
      yield call([fp, fp.write], data);
    }
  } catch (err) {
    yield put(actions.showError(ensureError(err).message));
  } finally {
    if (fp != null) {
      yield call([fp, fp.close]);
    }

    yield put(actions.setExportInProgress(false));
  }
}


function* onDeviceChanged(): SagaIterator {
  const { deviceKey } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  const pos: (Span | null)[] = [];
  const vel: (Span | null)[] = [];
  const accel: (Span | null)[] = [];

  yield put(actions.pollDevicePosition('stop'));
  if (deviceKey) {
    try {
      const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
      for (let i = 0; i < zmlDevice.axisCount; i++) {
        const zmlAxis = zmlDevice.getAxis(i + 1);
        const warningsApi = zmlAxis.warnings;
        const warnings = (yield call([warningsApi, warningsApi.getFlags])) as SagaReturnType<typeof zmlAxis.warnings.getFlags>;
        if (warnings.has(ascii.WarningFlags.PERIPHERAL_INACTIVE)
          || warnings.has(ascii.WarningFlags.PERIPHERAL_NOT_SUPPORTED)
          || (zmlAxis.isPeripheral && zmlAxis.peripheralId === 0)) {
          pos.push(null);
          vel.push(null);
          accel.push(null);
        } else {
          const settings = zmlAxis.settings;
          let max: number = yield call([settings, settings.get], 'limit.max');
          let min: number = yield call([settings, settings.get], 'limit.min');
          pos.push({ min, max });

          max = yield call([settings, settings.get], 'maxspeed');
          min = -max;
          vel.push({ min, max });

          min = yield call([settings, settings.get], 'motion.decelonly');
          max = yield call([settings, settings.get], 'motion.accelonly');
          accel.push({ min: -min, max });
        }
      }
    } catch (err) {
      throwUnexpectedError(err);
      yield put(actions.showError(`There was an error getting the device limits: ${err.message}`));
      return;
    }
  }

  yield put(actions.setDeviceLimits(pos, vel, accel));
  yield call(checkAxesAndUnits);
  yield call(convertDeviceLimits);
  yield call(convertStartPosition);
  yield call(checkDataValid);
  yield call(scanForBugs);
  yield put(actions.pollDevicePosition('default'));
}


function* convertDeviceLimits(): SagaIterator {
  const { deviceKey, axes } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  const path = (yield select(selectDevicePath)) as ReturnType<typeof selectDevicePath>;

  if (deviceKey && path) {
    const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
    const posNative = (yield select(selectPositionLimits)) as ReturnType<typeof selectPositionLimits>;
    const velNative = (yield select(selectVelocityLimits)) as ReturnType<typeof selectVelocityLimits>;
    const accelNative = (yield select(selectAccelerationLimits)) as ReturnType<typeof selectAccelerationLimits>;
    const deviceInfo = ((yield select(selectIdentifiedDevices)) as ReturnType<typeof selectIdentifiedDevices>)[deviceKey];

    for (let seriesIndex = 0; seriesIndex < path.series.length; seriesIndex++) {
      let posLimits: Span | null = null;
      let velLimits: Span | null = null;
      let accelLimits: Span | null = null;
      const series = path.series[seriesIndex];
      const targetAxisIndex = axes[series.name];

      if (targetAxisIndex != null && targetAxisIndex < posNative.length) {
        for (const axisNumber of expandAxisNumberIfInLockstep(deviceInfo, targetAxisIndex + 1)) {
          const axisIndex = axisNumber - 1;
          const zmlAxis = zmlDevice.getAxis(axisNumber);
          const settings = zmlAxis.settings;

          if (posNative[axisIndex]) {
            try {
              const max = settings.convertFromNativeUnits('limit.max', posNative[axisIndex]!.max, path.positionUnit);
              const min = settings.convertFromNativeUnits('limit.min', posNative[axisIndex]!.min, path.positionUnit);
              posLimits = posLimits ? clip(posLimits, { max, min }) : { max, min };
            } catch (err) {
              if (!(err instanceof ConversionFailedException)) {
                throw err;
              }
            }
          }

          if (velNative[axisIndex]) {
            try {
              const max = settings.convertFromNativeUnits('maxspeed', velNative[axisIndex]!.max, path.velocityUnit);
              const min = settings.convertFromNativeUnits('maxspeed', velNative[axisIndex]!.min, path.velocityUnit);
              velLimits = velLimits ? clip(velLimits, { max, min }) : { max, min };
            } catch (err) {
              if (!(err instanceof ConversionFailedException)) {
                throw err;
              }
            }
          }

          if (accelNative[axisIndex]) {
            try {
              const max =
                settings.convertFromNativeUnits('motion.accelonly', accelNative[axisIndex]!.max, derivativeUnit(path.velocityUnit)!);
              const min =
                settings.convertFromNativeUnits('motion.decelonly', accelNative[axisIndex]!.min, derivativeUnit(path.velocityUnit)!);
              accelLimits = accelLimits ? clip(accelLimits, { max, min }) : { max, min };
            } catch (err) {
              if (!(err instanceof ConversionFailedException)) {
                throw err;
              }
            }
          }
        }
      }

      yield put(actions.setSeriesLimits(seriesIndex, posLimits, velLimits, accelLimits));
    }
  }
}


function* convertStartPosition(): SagaIterator {
  const { deviceKey, axes } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  const path = (yield select(selectDevicePath)) as ReturnType<typeof selectDevicePath>;

  if (!deviceKey || !path) {
    yield put(actions.storeStartPosition([]));
    return;
  }

  try {
    const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
    const positions = _.range(zmlDevice.axisCount).fill(0);
    path.series.forEach(series => {
      const axisIndex = tryAccess(axes, series.name);
      if (axisIndex != null) {
        const zmlAxis = zmlDevice.getAxis(axisIndex + 1);
        positions[axisIndex] = Math.round(zmlAxis.settings.convertToNativeUnits('pos', series.points[0].pos, path.positionUnit));
      }
    });

    yield put(actions.storeStartPosition(positions));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.storeStartPosition([]));
    yield put(actions.showError(`There was an error converting the start position units: ${err.message}`));
  }
}


function* checkAxesAndUnits(): SagaIterator {
  // If there are any mapped series names that are not in the new path, or if any selected axes
  // no longer have units compatible with the path, clear the mappings.
  const path = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;
  const { axes, deviceKey } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  const identifiedDevices = (yield select(selectIdentifiedDevices)) as ReturnType<typeof selectIdentifiedDevices>;
  const identifiedAxes = (yield select(selectIdentifiedAxes)) as ReturnType<typeof selectIdentifiedAxes>;
  const deviceInfo = deviceKey ? identifiedDevices[deviceKey] : null;
  if (!path || !deviceInfo) {
    yield put(actions.selectAxes({}));
    yield put(actions.clearAxisSelection());
  } else {
    const seriesNames = new Set(path.series.map(s => s.name));
    const danglingAxes = _.keys(axes).some(name => !seriesNames.has(name));
    const unitMismatch = (deviceInfo != null) && _.values(axes).some(axisNum => {
      const axisInfo = identifiedAxes[deviceInfo.axes[axisNum]];
      return axisInfo && getDimensionName(path.positionUnit) !== axisInfo.movementDimensions?.position;
    });

    if (danglingAxes || unitMismatch || (path != null && _.keys(axes).length === 0)) {
      yield put(actions.selectAxes({}));
      yield put(actions.clearAxisSelection());
      if (deviceKey) {
        yield put(actions.setAxisDialogVisible(true, false));
      }
    }
  }
}


function* checkDataValid(): SagaIterator {
  const userPath = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;

  if (!userPath) {
    return;
  }

  const errors: string[] = [];
  if (userPath.series[0]?.points[0]?.time !== 0) {
    errors.push('The first point in the file must have time set to zero.');
  }
  if (_.some(userPath.series, ser => ser.points[0]?.vel !== 0)) {
    errors.push('The first point in the file must have zero velocity.');
  }
  if (_.some(userPath.series, ser => _.last(ser.points)?.vel !== 0)) {
    errors.push('The last point in the file should have zero velocity.');
  }

  if (errors.length) {
    errors.push('The above conditions have been automatically corrected.');
    yield put(actions.showError(errors.join(' ')));
    yield put(actions.autoCorrect());
  }
}


function* scanForBugs(): SagaIterator {
  const userPath = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;
  const devicePath = (yield select(selectDevicePath)) as ReturnType<typeof selectDevicePath>;

  if (!userPath || !devicePath) {
    return;
  }

  const bugs = findBugs(userPath, devicePath);
  yield put(actions.storeBugs(bugs));
}


function* reset(): SagaIterator {
  const { deviceKey, axes } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  const path = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;
  if (!deviceKey || !path) {
    return;
  }

  const deviceInfo = ((yield select(selectIdentifiedDevices)) as ReturnType<typeof selectIdentifiedDevices>)[deviceKey];
  const axisNumbers: number[] = [];
  const lockstepGroups: number[] = [];
  for (const axisIndex of _.values(axes)) {
    const axisNumber = axisIndex + 1;
    const group = getLockstepGroupNumber(deviceInfo, axisNumber);
    if (group) {
      lockstepGroups.push(group);
    } else {
      axisNumbers.push(axisNumber);
    }
  }

  try {
    const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
    const pvt = zmlDevice.pvt.getSequence(PVT_STREAM);
    yield call([pvt, pvt.disable]);
    yield put(actions.pollDevicePosition(FAST_POLL_INTERVAL));

    // Stop all relevant axes and home if needed.
    for (const axisNumber of axisNumbers) {
      const zmlAxis = zmlDevice.getAxis(axisNumber);
      const homed = yield call([zmlAxis, zmlAxis.isHomed]);
      if (!homed) {
        yield call([zmlAxis, zmlAxis.home], { waitUntilIdle: false });
      } else {
        yield call([zmlAxis, zmlAxis.stop], { waitUntilIdle: false });
      }
    }

    for (const groupNumber of lockstepGroups) {
      const zmlGroup = zmlDevice.getLockstep(groupNumber);
      // We don't home the lockstep groups because you're supposed to home the individual axes
      // when setting up the group, and there's no biguous way to determine if the
      // group should be homed.
      yield call([zmlGroup, zmlGroup.stop], { waitUntilIdle: true });
    }

    for (const axisNumber of axisNumbers) {
      const zmlAxis = zmlDevice.getAxis(axisNumber);
      yield call([zmlAxis, zmlAxis.waitUntilIdle]);
    }

    // Move axes to start position.
    for (const series of path.series) {
      const axisIndex = axes[series.name];
      if (axisIndex != null) {
        const group = getLockstepGroupNumber(deviceInfo, axisIndex + 1);
        if (group) {
          const lockstep = zmlDevice.getLockstep(group);
          yield call([lockstep, lockstep.moveAbsolute],
            series.points[0].pos,
            path.positionUnit as '' | Length | Angle,
            { waitUntilIdle: false });
        } else {
          const zmlAxis = zmlDevice.getAxis(axisIndex + 1);
          yield call([zmlAxis, zmlAxis.moveAbsolute],
            series.points[0].pos,
            path.positionUnit as '' | Length | Angle,
            { waitUntilIdle: false });
        }
      }
    }

    for (const axisNumber of axisNumbers) {
      const zmlAxis = zmlDevice.getAxis(axisNumber);
      yield call([zmlAxis, zmlAxis.waitUntilIdle]);
    }

    for (const groupNumber of lockstepGroups) {
      const zmlGroup = zmlDevice.getLockstep(groupNumber);
      yield call([zmlGroup, zmlGroup.waitUntilIdle]);
    }

    yield put(actions.pollDevicePosition('default'));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.pollDevicePosition('stop'));
    yield put(actions.showError(`Error moving to start position: ${err.message}`));
  }
}


function* run(): SagaIterator {
  const { deviceKey, axes } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  const path = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;
  if (!deviceKey || !path) {
    return;
  }

  const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
  const pvt = zmlDevice.pvt.getSequence(PVT_STREAM);
  const deviceInfo = ((yield select(selectIdentifiedDevices)) as ReturnType<typeof selectIdentifiedDevices>)[deviceKey];

  const axisDefinitions: PvtAxisDefinition[] = [];
  const unitConverters: IUnitConverter[] = [];
  path.series
    .map(series => axes[series.name])
    .filter(i => i != null)
    .forEach(axisIndex => {
      const axisNumber = axisIndex + 1;
      const group = getLockstepGroup(deviceInfo, axisNumber);
      axisDefinitions.push({
        axisNumber: group?.groupNumber ?? axisNumber,
        axisType: group ? PvtAxisType.LOCKSTEP : PvtAxisType.PHYSICAL,
      });

      if (group) {
        unitConverters.push(zmlDevice.getAxis(group.axisNumbers[0]).settings);
      } else {
        unitConverters.push(zmlDevice.getAxis(axisNumber).settings);
      }
    });

  const timer = getContainer().get(TimeMeasuring);
  let pointIndex = 1;
  try {
    yield put(actions.pollDevicePosition('stop'));
    yield call([pvt, pvt.disable]);
    yield call([pvt, pvt.setupLiveComposite], ...axisDefinitions);
    yield call([pvt, pvt.cork]);

    let moveStartTime: number | null = null;
    const { userStop } = yield race({
      userStop: take(ActionTypes.STOP),
      go: call(function* () {
        let lastUiUpdateTime = 0;
        let totalTimeSent = 0;
        while (pointIndex < path.series[0].points.length) {
          const pos: string[] = [];
          const vel: string[] = [];
          let pointDt: number | null = null;
          let relative = false;
          let paramIndex = 0;
          for (const series of path.series) {
            const axis = axes[series.name];
            if (axis != null) {
              const point = series.points[pointIndex];
              relative = point.relative ?? false;
              pos.push(unitConverters[paramIndex].convertToNativeUnits('pos', point.pos, path.positionUnit).toFixed(0));
              vel.push(unitConverters[paramIndex].convertToNativeUnits('vel', point.vel, path.velocityUnit).toFixed(0));
              if (!pointDt) {
                pointDt = MS_PER_SEC * point.dt;
              }

              paramIndex++;
            }
          }

          const cmd = `pvt ${PVT_STREAM} point ${relative ? 'rel' : 'abs'} p ${pos.join(' ')} v ${vel.join(' ')} t ${pointDt!.toFixed(1)}`;
          while (true) {
            const response = (yield call([zmlDevice, zmlDevice.genericCommand], cmd, { checkErrors: false })) as
              SagaReturnType<typeof zmlDevice.genericCommand>;
            if (response.status === 'BUSY' && !moveStartTime) {
              moveStartTime = timer.now();
            }

            if (response.replyFlag === 'OK') {
              break;
            } else {
              if (response.data === 'BADDATA') {
                throw new Error('The device returned BADDDATA.');
              } else if (response.data !== 'AGAIN') {
                throw new Error(`An unexpected response was received from the device: ${JSON.stringify(response)}`);
              }

              // AGAIN received. Update UI if we are far enough ahead of the device and it has been a while since the last update.
              lastUiUpdateTime = yield call(playbackPositionUpdate, timer, moveStartTime, totalTimeSent, lastUiUpdateTime);
            }
          }

          totalTimeSent += pointDt!;

          // Also try UI updates if the command was not repeated.
          lastUiUpdateTime = yield call(playbackPositionUpdate, timer, moveStartTime, totalTimeSent, lastUiUpdateTime);

          pointIndex++;
        }
      }),
    });

    if (userStop) {
      yield call([pvt, pvt.disable]);
    } else {
      yield call([pvt, pvt.uncork]);
      yield call(waitForPvtEnd, pvt, moveStartTime ?? timer.now()); // Uncorking will start movement if not already moving.
    }
  } catch (err) {
    throwUnexpectedError(err);
    if (err instanceof CommandFailedException && err.details.responseData === 'BADDATA') {
      yield put(actions.setSelection(pointIndex));
      yield put(actions.showError(`The device rejected control point ${pointIndex + 1}; ` +
        'the position, velocity or acceleration may be out of range or would cause the stage to move beyond its travel limits.'));
    } else {
      yield put(actions.showError(`Error while sending the movement sequence to the device: ${err.message}`));
    }
  }

  yield put(actions.pollDevicePosition('default'));
}


function* playbackPositionUpdate(
  timer: TimeMeasuring,
  moveStartTime: number | null,
  totalTimeSent: number,
  lastUiUpdateTime: number): SagaIterator<number> {
  if (moveStartTime != null) {
    const now = timer.now();
    const playbackTime = now - moveStartTime;
    const bufferedTime = totalTimeSent - playbackTime;
    if (now - lastUiUpdateTime > 100 && bufferedTime > 1000) {
      try {
        const poll = (yield call(pollDevicePosition)) as PollReturn | null;
        if (poll != null) {
          yield put(actions.storeDevicePosition(poll.pos, poll.native, playbackTime / MS_PER_SEC));
        }
      } catch {
        // Ignore polling errors during playback; retry at next UI update opportunity.
      }

      lastUiUpdateTime = timer.now();
    }
  }

  return lastUiUpdateTime;
}


function* stop(): SagaIterator {
  const { deviceKey } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  if (deviceKey) {
    try {
      const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
      yield call([zmlDevice.allAxes, zmlDevice.allAxes.stop]);
      yield call([zmlDevice.allAxes, zmlDevice.allAxes.waitUntilIdle]);
      yield put(actions.pollDevicePosition('default'));
    } catch (err) {
      throwUnexpectedError(err);
      yield put(actions.pollDevicePosition('stop'));
      yield put(actions.showError(`Error stopping device: ${err.message}`));
    }
  }
}


function* waitForPvtEnd(pvt: PvtSequence, moveStartTime: number): SagaIterator {
  const timer = getContainer().get(TimeMeasuring);
  try {
    while ((yield call([pvt, pvt.isBusy])) as boolean) {
      const poll = (yield call(pollDevicePosition)) as PollReturn | null;
      if (poll == null) {
        break;
      }

      yield put(actions.storeDevicePosition(poll.pos, poll.native, (timer.now() - moveStartTime) / MS_PER_SEC));
      yield delay(100);
    }

    yield call([pvt, pvt.waitUntilIdle]);
  } catch (err) {
    throwUnexpectedError(err);
    if (err instanceof PvtMovementFailedException) {
      yield put(actions.showError(err.details.reason));
    } else if (err instanceof PvtExecutionException) {
      let points = '';
      if (err.details.invalidPoints.length) {
        points = ` Relevant control points: ${err.details.invalidPoints.map(p => p.index + 1).join(', ')}`;
      }
      yield put(actions.showError(`${err.details.reason}${points}`));
    } else if (err instanceof PvtMovementInterruptedException) {
      yield put(actions.showError(err.details.reason));
    } else {
      yield put(actions.showError(err.message));
    }
  }
}


function* exportCurveImage({ payload: { region } }: Action<ActionsToPayloads[ActionTypes.EXPORT_IMAGE]>): SagaIterator {
  const window = BrowserWindow.getFocusedWindow();
  if (!window) {
    yield put(actions.showError('Could not export image: Could not determine which window is focused.'));
    return;
  }

  try {
    const dialogResult: SagaReturnType<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
      title: 'Export Image',
      filters: [{ name: 'PNG files', extensions: ['png'] }, { name: 'All files', extensions: ['*'] }],
    });

    if (dialogResult.canceled || dialogResult.filePath == null) {
      return;
    }

    const data = (yield call([window, window.capturePage], {
      x: Math.floor(region.x),
      y: Math.floor(region.y),
      width: Math.ceil(region.width),
      height: Math.ceil(region.height),
    })) as SagaReturnType<typeof window.capturePage>;
    const buffer = data.toPNG();
    yield call(writeFile, dialogResult.filePath, buffer, { encoding: 'binary', flag: 'w' });
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.showError(`There was an error exporting the image: ${err.message}`));
  }
}


function* devicePollLoop(
  { payload: { interval } }: Action<ActionsToPayloads[ActionTypes.POLL_DEVICE_POSITION]>): SagaIterator {
  if (interval === 'default') {
    interval = yield select(selectPollDefaults);
  }

  while (interval !== null && interval !== 'stop' && !environment.isTest) {
    let effectiveInterval = interval as number;
    try {
      const poll = (yield call(pollDevicePosition)) as PollReturn | null;
      if (poll == null) {
        break;
      }

      // Always poll at the higher rate if the device is moving.
      const oldPos = yield select(selectDevicePositionNative);
      if (!_.isEqual(oldPos, poll.native)) {
        effectiveInterval = FAST_POLL_INTERVAL;
      }

      yield put(actions.storeDevicePosition(poll.pos, poll.native));
    } catch (err) {
      throwUnexpectedError(err);
      yield put(actions.showError(`Error while polling device: ${err.message}`));
      break;
    }

    const { taken } = yield race({
      taken: take(ActionTypes.POLL_DEVICE_POSITION),
      timer: delay(effectiveInterval),
    });

    if (taken != null) {
      const { interval: newInterval } = (taken as Action<ActionsToPayloads[ActionTypes.POLL_DEVICE_POSITION]>).payload;
      interval = newInterval;
      if (interval === 'default') {
        interval = yield select(selectPollDefaults);
      }
    }
  }
}


interface PollReturn { pos: number[]; native: number[] }

function* pollDevicePosition(): SagaIterator<PollReturn | null> {
  const { deviceKey } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  const path = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;

  if (!deviceKey || !path) {
    return null;
  }

  const { axes } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  const relevantAxes = new Set(_.values(axes));
  if (_.values(axes).length === 0) {
    return null;
  }

  const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
  const native: number[] = yield call([zmlDevice.settings, zmlDevice.settings.getFromAllAxes], 'pos');
  const pos: number[] = Array<number>(native.length).fill(0);
  for (let axis = 0; axis < native.length; axis++) {
    if (!Number.isNaN(native[axis]) && relevantAxes.has(axis)) {
      const zmlAxis = zmlDevice.getAxis(axis + 1);
      pos[axis] = zmlAxis.settings.convertFromNativeUnits('pos', native[axis], path.positionUnit);
    }
  }

  return { pos, native };
}


function* getBufferList({ payload: { visible } }: Action<ActionsToPayloads[ActionTypes.SET_BUFFER_LOAD_DIALOG_VISIBLE]>): SagaIterator {
  const { deviceKey } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  if (!visible || !deviceKey) {
    return;
  }

  try {
    const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
    const responses: ascii.Response[] = yield call([zmlDevice, zmlDevice.genericCommandMultiResponse], 'pvt buffer list');
    const buffers = responses.map(r => r.data.match(/buffer\s+(\d+)/))
      .filter(match => match != null)
      .map(match => match![1]);

    yield put(actions.storeBufferList(buffers.sort(naturalSort)));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setBufferLoadDialogVisible(false));
    yield put(actions.showError(`Error while getting buffer list: ${err.message}`));
  }
}


function* getBufferAxisCount({ payload: { bufferId } }: Action<ActionsToPayloads[ActionTypes.GET_BUFFER_AXIS_COUNT]>): SagaIterator {
  const { deviceKey } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  if (!deviceKey) {
    return;
  }

  const { bufferAxes } = (yield select(selectDeviceLoadDialog)) as ReturnType<typeof selectDeviceLoadDialog>;
  if (bufferId in bufferAxes) {
    return;
  }

  try {
    const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
    const responses: ascii.Response[] = yield call([zmlDevice, zmlDevice.genericCommandMultiResponse], `pvt buffer ${bufferId} print`);
    for (const response of responses) {
      const match = response.data.match(/setup\sstore\s\d+\s(\d+)/);
      if (match) {
        const numAxes = parseInt(match[1], 10);
        yield put(actions.storeBufferAxisCount(bufferId, numAxes));
        break;
      }
    }
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setBufferLoadDialogVisible(false));
    yield put(actions.showError(`Error while getting buffer axis count: ${err.message}`));
  }
}


function* loadFromBuffer({ payload: { bufferId, axes } }: Action<ActionsToPayloads[ActionTypes.LOAD_FROM_DEVICE_BUFFER]>): SagaIterator {
  const { deviceKey } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
  if (!bufferId || !deviceKey) {
    return;
  }

  const polling = (yield select(selectPollDefaults)) as ReturnType<typeof selectPollDefaults>;
  let phase = 'reading the buffer contents';
  try {
    yield put(actions.pollDevicePosition('stop'));
    const zmlDevice: ascii.Device = yield call(getDevice, deviceKey);
    const { rows, warnings } =
      (yield call(readBuffer, zmlDevice, parseInt(bufferId, 10), [])) as SagaReturnType<typeof readBuffer>;

    if (rows.length === 0 || rows[0].length === 0) {
      yield put(actions.showError('No points were found in the buffer.'));
      yield put(actions.pollDevicePosition(polling));
      return;
    }

    if (rows[0].length !== _.keys(axes).length || _.keys(axes).length !== _.uniq(_.values(axes)).length) {
      yield put(actions.showError('A distinct device axis must be selected for each buffer axis.'));
      yield put(actions.pollDevicePosition(polling));
      return;
    }

    if (warnings.length) {
      yield put(actions.showErrors(_.sortedUniqBy(_.sortBy(warnings, w => w.text), w => w.text)));
    }

    // Insert artificial start position. Current device position will overwrite this later.
    rows.splice(0, 0, rows[0].map(() => ({ dt: 0, time: 0, pos: 0, vel: 0 })));

    // Fill in accumulated time and do unit conversion.
    const zmlAxes: ascii.Axis[] = [];
    for (let i = 0; i < zmlDevice.axisCount; i++) {
      const zmlAxis = zmlDevice.getAxis(i + 1);
      zmlAxes.push(zmlAxis);
    }

    if (_.uniq(_.values(axes).map(i => zmlAxes[i].axisType)).length > 1) {
      yield put(actions.showError('Selected axes must have the same motion type.'));
      yield put(actions.pollDevicePosition(polling));
      return;
    }

    phase = 'converting data from device units';
    const someAxis = _.first(_.values(axes))!;
    const posUnit = zmlAxes[someAxis].axisType === AxisType.ROTARY ? Angle.DEGREES : Length.MILLIMETRES;
    const velUnit = zmlAxes[someAxis].axisType === AxisType.ROTARY ? AngularVelocity.DEGREES_PER_SECOND : Velocity.MILLIMETRES_PER_SECOND;

    let time = 0;
    const seriesIndexToZmlAxis = rows[0].map((_, i) => zmlAxes[axes[makeBufferAxisName(bufferId, i)]]);
    for (const row of rows) {
      time += row[0].dt;
      for (let seriesIndex = 0; seriesIndex < row.length; seriesIndex++) {
        const zmlAxis = seriesIndexToZmlAxis[seriesIndex];
        const point = row[seriesIndex];
        point.time = time;
        point.pos = zmlAxis.settings.convertFromNativeUnits('pos', point.pos, posUnit);
        point.vel = zmlAxis.settings.convertFromNativeUnits('maxspeed', point.vel, velUnit);
      }
    }

    const path: Path = {
      positionUnit: posUnit,
      velocityUnit: velUnit,
      series: rows[0].map((_, i) => ({
        name: makeBufferAxisName(bufferId, i),
        points: rows.map(row => row[i]),
      })),
    };

    yield put(actions.setCurrentFile(null));
    yield put(actions.storeUserPath(path));
    yield put(actions.commitEdit());
    yield put(actions.storeAxisSelections(axes));
    yield call(checkDataValid);
    if (isFirstPointRelative(path)) {
      const devicePos = (yield select(selectDevicePosition)) as ReturnType<typeof selectDevicePosition>;
      const { axes } = (yield select(selectDeviceOptions)) as ReturnType<typeof selectDeviceOptions>;
      if (devicePos.length > 0 && _.keys(axes).length > 0) {
        yield put(actions.storeRelativeStart(devicePos, axes));
      }
    }
    yield call(discretizeAndStore);
    yield call(scanForBugs);
    yield put(actions.pollDevicePosition(polling));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.showError(`Error while ${phase}: ${err.message}`));
  }
}


function* readBuffer(zmlDevice: ascii.Device, bufferId: number, bufferStack: number[]):
  SagaIterator<{ rows: Point[][]; warnings: Message[] }> {
  if (bufferStack.includes(bufferId)) {
    return {
      rows: [],
      warnings: [{
        text: `Circular buffer calls detected; stopped loading before first repeat of buffer ${bufferId}.`,
        severity: 'warning',
      }]
    };
  }

  let rows: Point[][] = [];
  let warnings: Message[] = [];

  const buffer = zmlDevice.pvt.getBuffer(bufferId);
  const lines: string[] = yield call([buffer, buffer.getContent]);
  if (lines.length === 0) {
    return {
      rows: [],
      warnings: [{
        text: `Empty or disabled buffer ${bufferId} called.`,
        severity: 'error',
      }]
    };
  }

  const numAxes = parseInt(lines[0].split(' ')[3], 10);
  for (const line of lines.slice(1)) {
    let match = line.match(/point\s(abs|rel)\sp\s([\d-\s]+)\sv\s([\d-\s]+)\st\s([\d.]+)/);
    if (match) {
      const relative = match[1] === 'rel';
      const pos = match[2].split(/\s+/).map(s => parseInt(s, 10));
      const vel = match[3].split(/\s+/).map(s => parseInt(s, 10));
      const time = parseFloat(match[4]) / MS_PER_SEC;
      if (pos.length !== numAxes || vel.length !== numAxes) {
        throw new Error(`Buffer ${bufferId} has inconsistent number of axes.`);
      }

      rows.push(pos.map((p, i) => ({
        dt: time,
        time: 0,
        pos: p,
        vel: vel[i],
        relative,
      } as Point)));

      continue;
    }

    match = line.match(/call\s(\d+)/);
    if (match) {
      const bufNum = parseInt(match[1], 10);
      const { rows: bufRows, warnings: bufWarnings } =
        (yield call(readBuffer, zmlDevice, bufNum, [...bufferStack, bufferId])) as SagaReturnType<typeof readBuffer>;
      if (bufRows.length && rows.length && bufRows[0].length !== rows[0].length) {
        warnings.push({
          text: `Buffer ${bufferId} calls buffer ${bufNum} which has a different number of axes.`,
          severity: 'warning',
        });
        break;
      }

      warnings = warnings.concat(bufWarnings);
      warnings.push({
        text: 'Calls were encountered; results include the contents of multiple buffers.',
        severity: 'warning',
      });
      warnings = _.uniq(warnings);
      rows = rows.concat(bufRows);
    }
  }

  return { rows, warnings };
}


function* storeDevicePosition(): SagaIterator {
  const userPath = (yield select(selectUserPath)) as ReturnType<typeof selectUserPath>;
  const running = (yield select(selectPlaybackTime)) != null;
  // If the first point is relative and we're in edit mode, then changes to the device position
  // are edits to the start position.
  if (userPath && isFirstPointRelative(userPath) && !running) {
    yield call(discretizeAndStore);
    yield call(convertDeviceLimits);
    yield call(convertStartPosition);
    yield call(scanForBugs);
  }
}
