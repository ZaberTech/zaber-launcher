import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Button, Icons, Text, TextType } from '@zaber/react-library';
import { NavLink } from 'react-router-dom';
import { useActions } from '@zaber/toolbox/lib/redux';

import { ExternalLink, NoContentMessage } from '../components';
import { selectHardwareModification } from '../store';
import unknownDevice from '../assets/unknown_device.svg?url';
import modifiedDevice from '../assets/modified_device.svg?url';
import { environment } from '../environment';

import { actions as actionsDefinition } from './actions';
import { HwModDeviceOrAxisState, selectDeviceOrAxisInfo, selectValues } from './selectors';
import { ConfigSections } from './ConfigSections';
import { ThirdPartyPeripheralIdPicker } from './peripheral_id_picker/PeripheralIdPicker';
import { supportLink } from './components/links';
import { TITLE } from './types';

function peripheralSupportLink(info: HwModDeviceOrAxisState) {
  const name = info.device.identity.name;
  if (name.match(/X-MCA/) != null) {
    return 'https://www.zaber.com/manuals/X-MCA#m-9-non-zaber-peripheral-support';
  } else if (name.match(/X-MCB1/) != null) {
    return 'https://www.zaber.com/manuals/X-MCB1#m-10-non-zaber-peripheral-support';
  } else if (name.match(/X-MCB2/) != null) {
    return 'https://www.zaber.com/manuals/X-MCB2#m-10-non-zaber-peripheral-support';
  } else {
    return 'https://www.zaber.com/manuals/X-MCC#m-13-non-zaber-peripheral-support';
  }
}

export const HardwareModification: React.FC = () => {
  const info = useSelector(selectDeviceOrAxisInfo);
  const values = useSelector(selectValues);
  const { error, enablingHwMod, peripheralTraits } = useSelector(selectHardwareModification);
  const actions = useActions(actionsDefinition);

  useEffect(() => {
    if (info?.isModified && values == null) {
      actions.appLoaded(info);
    }
  }, [info?.key, info?.isModified, values == null]);

  if (!info) {
    return <>
      <NoContentMessage
        title="Welcome to Advanced Hardware Setup!"
        message="Select a device or axis to start configuring it."
      />
      <div className="hw-mod-explanation">
        <Text className="header" t={TextType.H4}>This application can help you to configure:</Text>
        <div className="option">
          <img src={unknownDevice} alt="3rd party peripheral"></img>
          <Text t={TextType.H5}>Non-Zaber Peripheral</Text>
          <Text>You can configure non-Zaber peripherals for use with Zaber controllers.</Text>
        </div>
        <div className="option">
          <img src={modifiedDevice} alt="Modified device or peripheral"></img>
          <Text t={TextType.H5}>Modified Hardware</Text>
          <Text>You can configure products with modified hardware, such as a new limit sensor or encoder.</Text>
        </div>
      </div>
    </>;
  }

  if (peripheralTraits != null) {
    return <ThirdPartyPeripheralIdPicker traits={peripheralTraits} info={info}/>;
  }

  let specificName = `This ${info.device.identity.name}`;
  let genericName = `a ${info.device.identity.name}`;
  if (info.axis) {
    specificName = `Axis ${info.axis.axisNumber} of this ${info.device.identity.name}`;
    genericName = `an axis of a ${info.device.identity.name}`;
  }

  if (error.on === 'enable') {
    return (
      <div className="hw-mod-info info-grid">
        <Icons.ErrorFault className="error icon"/>
        <Text t={Text.Type.H4} className="title">Enabling hardware modification failed</Text>
        <div className="info">
          <Text>{error.message}</Text>
        </div>
        <div className="buttons"><Button onClick={() => actions.makeDeviceModifiable(info)}>Try again</Button></div>
      </div>
    );
  }

  if (enablingHwMod) {
    return <NoContentMessage title={TITLE} message="Enabling Hardware Modification..." type="working"/>;
  }

  if (info.device.isController) {
    if (info.axis != null && info.axis.identity?.peripheralId === 0) {
      const supports3rdParties = info.device.identity.firmwareVersion.major >= 7 && info.device.identity.firmwareVersion.minor >= 27;
      if (supports3rdParties && environment.offline) {
        return <NoContentMessage title={TITLE}
          type="info" message="Configuration of 3rd party peripherals is not available in Offline Zaber Launcher."
        />;
      }
      return (
        <div className="hw-mod-info info-grid">
          <Icons.ErrorNote className="note icon"/>
          <Text t={Text.Type.H4} className="title">Hardware is not identified</Text>
          <ol className="info">
            <li>
              Make sure you have your peripheral connected.
            </li>
            <li>
              If you have a Zaber peripheral which is not identified please go
              to <NavLink className="link-hover" to="/connection-manager">My Connections</NavLink> or
              the <NavLink className="link-hover" to={`/device-settings?selected=${info.axis.key}`}>
                Device Settings Application
              </NavLink> and setup a peripheral ID or peripheral name from there.
            </li>
            {supports3rdParties && <li>
              If you have a Non-Zaber peripheral you can configure it in this application.
              Check the <ExternalLink url={peripheralSupportLink(info)}>List Of Supported Peripheral Types</ExternalLink>  to
              make sure your peripheral is compatible.
            </li>}
          </ol>
          {supports3rdParties && <div className="buttons">
            <Button onClick={() => actions.configurePeripheralId(info)}>Configure</Button>
          </div>}
        </div>
      );
    }
  }

  if (!info.isModifiable) {
    return (
      <div className="hw-mod-info info-grid">
        <Icons.ErrorNote className="note icon"/>
        <Text t={Text.Type.H4} className="title">Hardware modification not supported</Text>
        <div className="info">
          <Text>
            In most cases, the Device Settings Application is sufficient to configure {genericName} and modifying hardware is not necessary.
          </Text>
          <Text>
            If you need help configuring this product, please contact Customer Support.
          </Text>
        </div>
      </div>
    );
  }

  if (info.device.identity.firmwareVersion.major < 7) {
    return <NoContentMessage title={TITLE}
      bannerTitle="Cannot Setup This Device" type="info"
      message="Advanced Hardware Setup is only supported for devices with Firmware 7."
    />;
  }

  if (info.device.identity.firmwareVersion.minor < 24) {
    const message =
      'Advanced Hardware Setup is only support for devices with Firmware 7.24 or later. ' +
      'To use Advanced setup on this device, first upgrade the firmware';
    return <NoContentMessage title={TITLE} bannerTitle="Cannot Setup This Device" type="info" message={message}/>;
  }

  if (!info.isModified) {
    return (
      <div className="hw-mod-info info-grid">
        <Icons.ErrorWarning className="warning icon"/>
        <Text t={Text.Type.H4} className="title">Enable hardware modification</Text>
        <div className="info">
          <Text>
            {specificName} has not been configured to support modified hardware (e.g., a new limit sensor or encoder).
            Click the "Enable" button to allow support for modified hardware.
          </Text>
          <Text>
          Please note that this process can only be reversed with a system/axis restore.
          </Text>
          <Text>
          In most cases, the <NavLink className="link-hover" to="/device-settings">Device Settings Application</NavLink> is
          sufficient to configure {genericName} and modifying hardware is not necessary.
          </Text>
          <Text>
          If you need help configuring this product, please contact {supportLink}.
          </Text>
        </div>
        <div className="buttons"><Button onClick={() => actions.makeDeviceModifiable(info)}>Enable</Button></div>
      </div>
    );
  }

  if (error.on === 'init') {
    return (
      <div className="hw-mod-info info-grid">
        <Icons.ErrorFault className="error icon"/>
        <Text t={Text.Type.H4} className="title">This device could not be initialized</Text>
        <div className="info">
          <Text>{error.message}</Text>
          <Text>Ensure your device is connected. If this error persists please contact {supportLink}</Text>
        </div>
        <div className="buttons"><Button onClick={() => actions.initializeValues(info)}>Try again</Button></div>
      </div>
    );
  }

  if (!values) {
    return <NoContentMessage title={TITLE} message="Initializing..." type="working"/>;
  }

  return <ConfigSections values={values} deviceKey={info.key}/>;
};
