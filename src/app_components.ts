import { Config } from '@zaber/app-components';

import { Editions, environment, Flavors } from './environment';

Config.merge({
  aws: {
    region: environment.awsRegion,
    cognitoIdentityPoolId: environment.cognitoIdentityPoolId,
    cognitoUserPoolId: environment.cognitoUserPoolId,
    iotCoreHost: environment.iotCoreHost,
    iotClientIdPrefix: 'zl',
  },
  zaber: {
    apiUrl: environment.apiUrl,
    deviceDB:
      environment.offline ? 'offline' :
      environment.edition === Editions.Public ? 'public' :
      'master',
    firmwareUpgradeServer: environment.edition === Editions.Public || environment.flavor !== Flavors.Zaber ? 'public' : 'internal',
  }
});

export * from '@zaber/app-components/lib/iot';
export * from '@zaber/app-components/lib/aws';
export * from '@zaber/app-components/lib/zaber_api';
export * from '@zaber/app-components/lib/request';
export * from '@zaber/app-components/lib/message_router';
export * from '@zaber/app-components/lib/log';
export * from '@zaber/app-components/lib/units';
export * from '@zaber/app-components/lib/storage';
export * from '@zaber/app-components/lib/firmware_upgrader';
