import type { SagaIterator } from 'redux-saga';
import { all, put, takeEvery } from 'redux-saga/effects';

import { getContainer } from '../container';
import type { Action } from '../utils';
import { Storage } from '../app_components';

import { ActionTypes, ActionsToPayloads, actions } from './actions';

export function* helpSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.MESSAGE_READY, messageReady),
    takeEvery(ActionTypes.DISMISS_MESSAGE, dismissMessage),
  ]);
}

function makeStorageKey(messageId: string) {
  return `HELP_MESSAGE_${messageId}`;
}

function* messageReady({ payload: { messageId } }: Action<ActionsToPayloads[ActionTypes.MESSAGE_READY]>): SagaIterator {
  const storage = getContainer().get<Storage>(Storage);
  const visibility = storage.load<boolean>(makeStorageKey(messageId)) ?? true;
  yield put(actions.updateMessageVisibility(messageId, visibility));
}

function dismissMessage({ payload: { messageId } }: Action<ActionsToPayloads[ActionTypes.DISMISS_MESSAGE]>) {
  const storage = getContainer().get<Storage>(Storage);
  storage.save<boolean>(makeStorageKey(messageId), false);
}
