import { Units, Length, Angle, ascii, Velocity, AngularVelocity, Acceleration, AngularAcceleration, Time, UnitTable } from '@zaber/motion';
import _ from 'lodash';
import { tryAccess } from '@zaber/toolbox';
import { match, P } from 'ts-pattern';

import { MyNumber } from '../utils';
import { dimensions } from '../app_components';

import { MovementDimensions, UnitInfo, UnitConvert, unsupportedUnitInfo } from './types';


const UNITS_DIMENSION_DELIMITER = ':';

export const dimensionsByName = _.keyBy(dimensions, 'DimensionName');
export const unitsById = _.keyBy(_.flatMap(dimensions, dimension => dimension.units), 'id');

type NullableNumber = number | null | undefined;

// default units overrides
dimensionsByName[getDimensionName(Length.mm)!].defaultUnit = Length.mm;
dimensionsByName[getDimensionName(Velocity.MILLIMETRES_PER_SECOND)!].defaultUnit = Velocity.MILLIMETRES_PER_SECOND;
dimensionsByName[getDimensionName(Acceleration.MILLIMETRES_PER_SECOND_SQUARED)!].defaultUnit = Acceleration.MILLIMETRES_PER_SECOND_SQUARED;

export const getUnitLabel = (unit: Units): string => {
  if (unit === Units.NATIVE) {
    return 'Native';
  }
  const unitData = unitsById[unit];
  return unitData?.ShortName;
};

export const getUnitLabelExceptNative = (unit: Units): string => {
  if (unit === Units.NATIVE) {
    return '';
  }
  const unitData = unitsById[unit];
  return unitData?.ShortName;
};

interface MovementUnits {
  position: Length | Angle;
  velocity: Velocity | AngularVelocity;
  acceleration: Acceleration | AngularAcceleration;
}
export const getMovementUnits = (axisType: ascii.AxisType): MovementUnits | null => match(axisType)
  .with(ascii.AxisType.LINEAR, () => ({
    position: Length.mm,
    velocity: Velocity.MILLIMETRES_PER_SECOND,
    acceleration: Acceleration.MILLIMETRES_PER_SECOND_SQUARED,
  }))
  .with(ascii.AxisType.ROTARY, () => ({
    position: Angle.DEGREES,
    velocity: AngularVelocity.DEGREES_PER_SECOND,
    acceleration: AngularAcceleration.DEGREES_PER_SECOND_SQUARED,
  }))
  .otherwise(() => null);

export const getMovementDimensions = (axisType: ascii.AxisType) => match(getMovementUnits(axisType))
  .with(P.not(P.nullish), ({ position, velocity, acceleration }) => ({
    position: getDimensionName(position)!,
    velocity: getDimensionName(velocity)!,
    acceleration: getDimensionName(acceleration)!,
  } as MovementDimensions))
  .otherwise(() => null);

export function getDimensionName(units: Units | null): string | null {
  if (units === Units.NATIVE || units == null) { return null }
  return units.split(UNITS_DIMENSION_DELIMITER, 1)[0];
}

export function dimensionExists(dimension: string): boolean {
  return dimension in dimensionsByName;
}

export function getDefaultDimensionUnits(dimension: string): Units {
  const dimensionDef = tryAccess(dimensionsByName, dimension);
  if (dimensionDef == null) { throw new Error(`Unknown dimension ${dimension}`) }
  return dimensionDef.defaultUnit as Units;
}

export function getUnitInfoOfDimension(dimensionName: string): UnitInfo {
  const dimensionDefinition = dimensionsByName[dimensionName];
  if (dimensionDefinition != null) {
    return  {
      supported: true,
      dimension: dimensionDefinition.DimensionName,
      default: dimensionDefinition.defaultUnit as Units,
      conversion: null,
      precision: 0,
    };
  } else {
    return unsupportedUnitInfo();
  }
}

// TODO: expose static conversion in ZML and use here
/**
 * e.g: convertFromDefaultUnits(123, Velocity.NANOMETRES_PER_SECOND) => 123000000
 * @param value value in default units, for example Velocity.MILLIMETERS_PER_SECOND or Length.MILLIMETRES
 * @param convertTo units that you want to convert to
 * @returns value in new units
 */
export function convertFromDefaultUnits<T extends NullableNumber>(value: T, convertTo: Units): T {
  const dimension = getDimensionName(convertTo);
  if (dimension == null) {
    throw new Error('Cannot convert native units');
  }
  return convertBetweenUnits(value, getDefaultDimensionUnits(dimension), convertTo);
}

export function convertToDefaultUnits<T extends NullableNumber>(value: T, convertFrom: Units): T {
  const dimension = getDimensionName(convertFrom);
  if (dimension == null) {
    throw new Error('Cannot convert native units');
  }
  return convertBetweenUnits(value, convertFrom, getDefaultDimensionUnits(dimension));
}

export function convertBetweenUnits<T extends NullableNumber>(
  value: T, fromUnit: Units, toUnit: Units): T {
  if (fromUnit === Units.NATIVE || toUnit === Units.NATIVE) {
    throw new Error('Cannot convert from/to native units');
  }

  const dimension = tryAccess(dimensionsByName, getDimensionName(fromUnit));
  if (dimension == null) {
    throw new Error(`Unknown units ${fromUnit}`);
  }
  if (dimension.DimensionName !== getDimensionName(toUnit)) {
    throw new Error(`Incompatible conversion ${fromUnit} -> ${toUnit}`);
  }

  if (fromUnit === toUnit || !MyNumber.isFinite(value)) {
    return value;
  }

  const sourceUnit = dimension.units.find(unit => unit.id === fromUnit)!;
  const targetUnit = dimension.units.find(unit => unit.id === toUnit)!;

  const baseValue = (value - sourceUnit.Offset) / sourceUnit.Scale;
  const convertedValue = (baseValue * targetUnit.Scale) + targetUnit.Offset;
  return convertedValue as T;
}

/**
 * Gets the precision a given unit supports, expressed in powers of 10 of native units, rounded up to avoid losing precision.
 * Thus, if a single unit is 100 native units, this function will return 2 (100 => 10^2),
 * and if a single unit is 12,500 native units, it will return 5 (12,500 rounds up to 100,000; 100,000 => 10^5).
 * Further, if the native unit has decimal precision, take that into account
 * Returns null if precision cannot be calculated for this value for any reason
 */
function getPrecision(info: UnitInfo, unit: Units) {
  if (!info.supported || info.conversion == null || unit === Units.NATIVE) { return info.precision }

  const defaultScale = unitsById[info.default]?.Scale;
  if (defaultScale == null) {
    throw new Error(`No unit definition for ${info.default}`);
  }
  const targetUnitScale = unitsById[unit]?.Scale;
  if (targetUnitScale == null) {
    throw new Error(`No unit definition for ${unit}`);
  }

  const defaultUnitsPerTargetUnit = defaultScale / targetUnitScale;
  const nativeUnitsPerTargetUnit = defaultUnitsPerTargetUnit / info.conversion;
  return Math.ceil(Math.log(nativeUnitsPerTargetUnit) / Math.log(10)) + info.precision;
}

/**
 * Truncates `value` to only retain precision that is backed by native units plus one decimal place.
 */
export function truncateValue(info: UnitInfo, value: number, unit: Units) {
  if (info.conversion == null) {
    // This value is not backed by native units. Do not truncate
    return value;
  }
  const precision = getPrecision(info, unit) + 1;
  const raised10 = 10 ** precision;
  if (precision >= 0) {
    return Math.round(value * raised10) / raised10;
  } else {
    return Math.round(Math.round(value * raised10) / raised10);
  }
}

export function convertValueUnits(info: UnitConvert, value: number, fromUnits: Units, toUnits: Units): number {
  if (fromUnits === toUnits) {
    return value;
  } else if (fromUnits === Units.NATIVE) {
    if (info.dimension !== getDimensionName(toUnits) || info.conversion == null) {
      throw new Error(`Incompatible conversion NATIVE ${info.dimension} -> ${toUnits}`);
    }
    return convertFromDefaultUnits(value * info.conversion, toUnits);
  } else if (toUnits === Units.NATIVE) {
    if (info.dimension !== getDimensionName(fromUnits) || info.conversion == null) {
      throw new Error(`Incompatible conversion ${fromUnits} -> NATIVE ${info.dimension}`);
    }
    return convertBetweenUnits(value, fromUnits, info.default) / info.conversion;
  } else {
    return convertBetweenUnits(value, fromUnits, toUnits);
  }
}

export function convertNullable(info: UnitInfo | null, value: number | null | undefined, fromUnits: Units, toUnits: Units): number | null {
  if (fromUnits === toUnits) { return value ?? null }
  if (!info?.supported || value == null) { return null }
  const fromDimension = getDimensionName(fromUnits);
  const toDimension = getDimensionName(toUnits);
  if ((fromDimension != null && fromDimension !== info.dimension) || (toDimension != null && toDimension !== info.dimension)) {
    return null;
  }
  return convertValueUnits(info, value, fromUnits, toUnits);
}

const unitsAlternateSpellings: Record<string, Units> = {
  '': Units.NATIVE,
  'um': Length.MICROMETRES,
  'm/s^2': Acceleration.METRES_PER_SECOND_SQUARED,
  'um/s': Velocity.MICROMETRES_PER_SECOND,
  'cm/s^2': Acceleration.CENTIMETRES_PER_SECOND_SQUARED,
  'mm/s^2': Acceleration.MILLIMETRES_PER_SECOND_SQUARED,
  'um/s²': Acceleration.MICROMETRES_PER_SECOND_SQUARED,
  'µm/s^2': Acceleration.MICROMETRES_PER_SECOND_SQUARED,
  'um/s^2': Acceleration.MICROMETRES_PER_SECOND_SQUARED,
  'nm/s^2': Acceleration.NANOMETRES_PER_SECOND_SQUARED,
  'in/s^2': Acceleration.INCHES_PER_SECOND_SQUARED,
  'deg': Angle.DEGREES,
  'deg/s': AngularVelocity.DEGREES_PER_SECOND,
  'deg/s²': AngularAcceleration.DEGREES_PER_SECOND_SQUARED,
  '°/s^2': AngularAcceleration.DEGREES_PER_SECOND_SQUARED,
  'deg/s^2': AngularAcceleration.DEGREES_PER_SECOND_SQUARED,
  'rad/s^2': AngularAcceleration.RADIANS_PER_SECOND_SQUARED,
  'us': Time.MICROSECONDS,
};

export function unitsFromSymbol(sym: string): Units | null {
  if (sym in unitsAlternateSpellings) {
    return unitsAlternateSpellings[sym];
  } else {
    try {
      return UnitTable.getUnit(sym);
    } catch {
      return null;
    }
  }
}

export function derivativeUnit(unit?: Units): Units | null {
  switch (unit) {
    case Length.CENTIMETRES: return Velocity.CENTIMETRES_PER_SECOND;
    case Length.INCHES: return Velocity.INCHES_PER_SECOND;
    case Length.METRES: return Velocity.METRES_PER_SECOND;
    case Length.MICROMETRES: return Velocity.MICROMETRES_PER_SECOND;
    case Length.MILLIMETRES: return Velocity.MILLIMETRES_PER_SECOND;
    case Length.NANOMETRES: return Velocity.NANOMETRES_PER_SECOND;
    case Angle.DEGREES: return AngularVelocity.DEGREES_PER_SECOND;
    case Angle.RADIANS: return AngularVelocity.RADIANS_PER_SECOND;
    case Velocity.CENTIMETRES_PER_SECOND: return Acceleration.CENTIMETRES_PER_SECOND_SQUARED;
    case Velocity.INCHES_PER_SECOND: return Acceleration.INCHES_PER_SECOND_SQUARED;
    case Velocity.METRES_PER_SECOND: return Acceleration.METRES_PER_SECOND_SQUARED;
    case Velocity.MICROMETRES_PER_SECOND: return Acceleration.MICROMETRES_PER_SECOND_SQUARED;
    case Velocity.MILLIMETRES_PER_SECOND: return Acceleration.MILLIMETRES_PER_SECOND_SQUARED;
    case Velocity.NANOMETRES_PER_SECOND: return Acceleration.NANOMETRES_PER_SECOND_SQUARED;
    case AngularVelocity.DEGREES_PER_SECOND: return AngularAcceleration.DEGREES_PER_SECOND_SQUARED;
    case AngularVelocity.RADIANS_PER_SECOND: return AngularAcceleration.RADIANS_PER_SECOND_SQUARED;
    case Units.NATIVE: return Units.NATIVE;
    default: return null;
  }
}
