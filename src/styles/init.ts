import { Colors, styleObject } from '@zaber/react-library';

import { environment, Flavors } from '../environment';

function initStyles() {
  if (environment.flavor === Flavors.Rexroth) {
    styleObject.generalRadius = '0';

    Colors.orange = '#00CCFF';
    Colors.zaberRed = '#002B49';
    Colors.pink = '#7F95A4';
    Colors.darkRed = '#A3BAC8';
    Colors.darkRedTranslucent = '#A3BAC826';
    Colors.justRed = '#DF0024';
  }
}

initStyles();
