import _ from 'lodash';
import { createSelector } from 'reselect';

import { makeDeviceKey } from '../keys';
import { selectFirmwareUpdate } from '../store';
import { IdentifiedDeviceState, selectIdentifiedDevices as selectManagerDevices } from '../connection_manager/selectors';
import { tryAccess } from '../utils';
import { DeviceIdentifiers, Version } from '../app_components';

import { compareVersions, compareWithDeviceVersion, isPossibleUpgrade } from './utility';

function thereIsAHigherVersionsForDevice(device: IdentifiedDeviceState, versions: Version[], skippedToVersion: Version | undefined) {
  return versions.some(version =>
    isPossibleUpgrade(version, device)
    && compareWithDeviceVersion(version, device) > 0
    && compareVersions(version, skippedToVersion) > 0);
}

export const selectSkippedVersions = createSelector(selectFirmwareUpdate, state => state.versionsSkipped);
export const selectUpgradeInProgress = createSelector(selectFirmwareUpdate, state => state.progress != null);
const selectConnectionStates = createSelector(selectFirmwareUpdate, state => state.connections);

export const selectDevicesWithVersions = createSelector(selectConnectionStates, selectManagerDevices, (connections, managerDevices) => {
  const devices: Record<string, { versions: Version[]; identifier: DeviceIdentifiers }> = {};

  for (const connection of Object.values(connections)) {
    for (const identifier of connection.identifiers) {
      const deviceKey = makeDeviceKey(connection.key, identifier.DeviceNumber);
      const device = tryAccess(managerDevices, deviceKey);
      if (device == null) { continue }

      const versions = tryAccess(connection.versions, device.address);
      if (versions == null) { continue }

      devices[device.key] = { versions, identifier };
    }
  }

  return devices;
});

export const selectDevicesWithHigherVersionsAvailable = createSelector(
  selectDevicesWithVersions, selectManagerDevices, selectSkippedVersions,
  (devicesVersions, managerDevices, skippedMap) =>
    _(managerDevices).filter(device => {
      const versions = tryAccess(devicesVersions, device.key);
      if (versions == null) { return false }
      const skippedToVersion = tryAccess(skippedMap, device.identity.serialNumber);
      return thereIsAHigherVersionsForDevice(device, versions.versions, skippedToVersion);
    }).map(device => device.key).value()
);
