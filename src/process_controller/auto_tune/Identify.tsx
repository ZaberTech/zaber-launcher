import React, { ComponentProps } from 'react';
import { Button, ButtonNext, ButtonPrevious, ButtonRow, Icons, ProgressBar, Text } from '@zaber/react-library';
import { ProcessControllerMode } from '@zaber/motion/product';
import type { MarkLineComponentOption } from 'echarts';

import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import { InputWithFixedUnit } from '../../units/InputWithFixedUnit';
import { EditableInputWithUnits, measurementOK } from '../../units';
import { Editions, environment } from '../../environment';
import { Graph } from '../components/Graph';
import type { ValueUnitState } from '../../units/EditableInputWithUnits';

import type { AutoTuneIdentify, AutoTuneSettings } from './types';

function hysteresisLines(tune: AutoTuneIdentify, hysteresis: ValueUnitState): MarkLineComponentOption['data'] {
  if (!(measurementOK(hysteresis))) {
    return [];
  }

  const high = tune.setpoint.value + hysteresis.value;
  const low = tune.setpoint.value - hysteresis.value;
  const startTimeMs = tune.readings.at(0)?.time ?? 0;
  const endTimeMs = tune.readings.at(-1)?.time ?? 0;
  const xMax = (endTimeMs - startTimeMs) / 1000;
  return [
    [
      {
        xAxis: 0,
        yAxis: high,
        symbol: 'none',
        lineStyle: {
          color: '#F00',
        },
      },
      {
        xAxis: xMax,
        yAxis: high,
        symbol: 'none'
      }
    ], [
      {
        xAxis: 0,
        yAxis: low,
        symbol: 'none',
        lineStyle: {
          color: '#00F'
        },
      },
      {
        xAxis: xMax,
        yAxis: low,
        symbol: 'none'
      }
    ]
  ];
}

const DownloadIdentificationData: React.FC<ComponentProps<typeof Button> & { tune: AutoTuneIdentify }> = ({ tune, ...props }) => {
  const actions = useActions(actionDefinitions);

  if (environment.edition === Editions.Public || tune.criticalFrequency == null || tune.criticalGain == null) {
    return null;
  }

  return <Button {...props} onClick={() => actions.autoTuneDownloadIdentification(tune.readings)}>Download Data</Button>;
};

type AutoTuneSetupProps = {
  tune: AutoTuneIdentify;
  settings: AutoTuneSettings;
};

export const AutoTuneModalIdentify: React.FC<AutoTuneSetupProps> = ({ tune, settings }) => {
  const actions = useActions(actionDefinitions);
  const hysteresisSetting = tune.initial.mode === ProcessControllerMode.PID ?
    'process.control.hysteresis.voltage' :
    'process.control.hysteresis.temperature';
  const hysteresis = settings[hysteresisSetting];
  const { criticalFrequency, criticalGain } = tune;
  const runCompleteSuccess = criticalFrequency != null && criticalGain != null;

  return <>
    <Text>Set the hysteresis to the desired value and start system identification</Text>
    <div className="controls-columns">
      <Text t={Text.Type.H3} className="left">Controls</Text>
      <EditableInputWithUnits
        className="left"
        labelContent="Hysteresis"
        title="Hysteresis"
        unitFrom={{ info: tune.converters[hysteresisSetting] }}
        measure={hysteresis}
        mode={hysteresis.mode}
        onChange={update => actions.updateProcessSetting(tune.process, hysteresisSetting, update)}
        unitsEditable={false}
      />
      {tune.run == null ? (
        <Button
          className="left"
          onClick={measurementOK(hysteresis) ? () => actions.autoTuneRunIdentification(tune, hysteresis) : 'disabled'}
        >
          <Icons.RightNormal/> Start
        </Button>
      ) : (
        <Button className="left" onClick={() => actions.autoTuneCancelIdentification()}>Cancel</Button>
      )}
      <Text t={Text.Type.H3} className="right">Identified Parameters</Text>
      <InputWithFixedUnit
        className="right"
        labelContent="Kc"
        value={criticalGain}
        unit="V²/˚C"
      />
      <InputWithFixedUnit
        className="right"
        labelContent="Fc"
        value={criticalFrequency}
        unit="Hz"
      />
    </div>

    <hr/>

    <div className="controls-columns">
      <InputWithFixedUnit
        className="left"
        labelContent="Output Voltage"
        value={tune.run?.voltage ?? 0}
        unit="V"
      />
      <InputWithFixedUnit
        className="left"
        labelContent="Temperature"
        value={tune.readings.at(-1)?.measured ?? 0}
        unit="˚C"
      />
      <div className="identify-progress right">
        {
          tune.run != null ? <Text>{tune.run.message}</Text> :
          runCompleteSuccess ? <><Icons.Confirmation className="complete"/>&nbsp;<Text>System identification is complete.</Text></> :
          null
        }
      </div>
      <DownloadIdentificationData tune={tune} className="right"/>
    </div>

    <ProgressBar className={tune.run == null ? 'hide' : 'show'} progress={tune.run?.progress ?? 0}/>

    <Graph
      mode={ProcessControllerMode.MANUAL}
      source={tune.initial.source}
      setpoint={true}
      readings={tune.readings}
      lines={hysteresisLines(tune, hysteresis)}
    />

    <ButtonRow justify="spread">
      <ButtonPrevious
        title="Return To Setup Step"
        disabled={tune.run != null}
        onClick={() => actions.autoTuneGoToStep({ ...tune, step: 'setup' })}
      >
        Back
      </ButtonPrevious>
      <ButtonNext
        title="Go To Test Step"
        disabled={tune.run != null}
        onClick={(criticalFrequency != null && criticalGain != null) ? () => {
          actions.processPower(tune.process, false);
          actions.autoTuneGoToStep({ ...tune, step: 'test', criticalFrequency, criticalGain });
        } : 'disabled'}
      >
        Next
      </ButtonNext>
    </ButtonRow>
  </>;
};
