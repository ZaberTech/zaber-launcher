import {
  ProcessControllerMode as Mode,
  ProcessControllerSource,
  ProcessControllerSourceSensor as SourceSensor
} from '@zaber/motion/product';
import _ from 'lodash';

import type { ValueUnitState } from '../units/EditableInputWithUnits';

import type { ProcessInfo } from './types';

const generalSettings = [
  'process.control.setpoint.voltage',
  'process.control.setpoint.temperature',
  'process.voltage.tf',
  'process.current.max',
] as const;

const manualSettings = [
  /** The stead state voltage once the process is on */
  'process.voltage.on',
  /** The initial pulse voltage at startup */
  'process.voltage.start',
  /** How long to hold the pulse voltage for */
  'process.voltage.start.duration',
  ...generalSettings,
] as const;

const closedLoopSettings = [
  'process.control.voltage.max',
  'process.control.voltage.min',
  /** Setpoint Filter Time Constant */
  'process.control.setpoint.tf',
  /** Direction */
  'process.control.dir',
  ...generalSettings,
] as const;

const pidSettings = [
  'process.pid.kp',
  'process.pid.ki',
  'process.pid.kd',
  'process.pid.offset',
] as const;

export const modeSettings = {
  [Mode.MANUAL]: manualSettings,
  [Mode.ON_OFF]: ['process.control.hysteresis.voltage', 'process.control.hysteresis.temperature', ...closedLoopSettings] as const,
  [Mode.PID]: [...closedLoopSettings, ...pidSettings],
  [Mode.PID_HEATER]: [...closedLoopSettings, ...pidSettings],
} satisfies Record<Mode, readonly string[]>;

export type ModeSettingNames<M extends Mode> = (typeof modeSettings)[M] extends readonly (infer T)[] ? T : never;

export type ProcessControllerSettingNames = {[M in Mode]: ModeSettingNames<M>}[Mode];

export type SettingsMap = Partial<Record<ProcessControllerSettingNames, ValueUnitState>>;

export type ModeSettings<M extends Mode> = ModeSettingNames<M> extends string ? Record<ModeSettingNames<M>, ValueUnitState> : never;

export function hasAllModeSettings<M extends Mode>(mode: M, settings: SettingsMap): settings is ModeSettings<M> {
  for (const settingName of modeSettings[mode]) {
    if (!(settingName in settings)) {
      return false;
    }
  }
  return true;
}

/** Lists the settings for a mode separated into general and PID categories */
export const separatedSettings = _.memoize(<M extends Mode>(mode: M): [ModeSettingNames<M>[], ModeSettingNames<M>[]] => {
  const generalSectionSettings: ModeSettingNames<M>[] = [];
  const pidSectionSettings: ModeSettingNames<M>[] = [];
  for (const setting of modeSettings[mode] as ModeSettingNames<M>[]) {
    if ((pidSettings as readonly string[]).includes(setting as string)) {
      pidSectionSettings.push(setting);
    } else {
      generalSectionSettings.push(setting);
    }
  }
  return [generalSectionSettings, pidSectionSettings];
});

export type SettingMeta = {
  name: string | ((info: ProcessInfo) => string);
  toggle?: {
    off: string;
    on: string;
  };
  /** True if the setting should not be displayed in the settings section */
  hide?: (info: ProcessInfo) => boolean;
  disableIfProcessOn?: true;
};

export const settingMeta: Record<ProcessControllerSettingNames, SettingMeta> = {
  'process.voltage.on': { name: 'Steady Voltage' },
  'process.voltage.start': { name: 'Initial Pulse Voltage' },
  'process.voltage.start.duration': { name: 'Voltage Delay' },
  'process.voltage.tf': { name: 'Output Filter Time Constant' },
  'process.current.max': { name: 'Maximum Current' },

  'process.control.voltage.max': { name: info => info.mode === Mode.ON_OFF ? 'On Voltage' : 'Maximum Voltage', disableIfProcessOn: true },
  'process.control.voltage.min': { name: info => info.mode === Mode.ON_OFF ? 'Off Voltage' : 'Minimum Voltage', disableIfProcessOn: true },
  'process.control.setpoint.temperature': { name: 'Setpoint', hide: () => true },
  'process.control.setpoint.voltage': { name: 'Setpoint', hide: () => true },
  'process.control.setpoint.tf': { name: 'Setpoint Filter Time Constant' },
  'process.control.hysteresis.voltage': { name: 'Hysteresis', hide: info => info.source.sensor !== SourceSensor.ANALOG_INPUT },
  'process.control.hysteresis.temperature': { name: 'Hysteresis', hide: info => info.source.sensor !== SourceSensor.THERMISTOR },
  /** Direction */
  'process.control.dir': { name: 'Direction', toggle: { on: 'Reverse', off: 'Direct' } },

  'process.pid.kp': { name: 'Kp' },
  'process.pid.ki': { name: 'Ki' },
  'process.pid.kd': { name: 'Kd' },
  'process.pid.offset': { name: 'Offset' },
};

export function setpointSetting(source: ProcessControllerSource) {
  return source.sensor === SourceSensor.THERMISTOR ? 'process.control.setpoint.temperature' : 'process.control.setpoint.voltage';
}

export function hysteresisSetting(source: ProcessControllerSource) {
  return source.sensor === SourceSensor.THERMISTOR ? 'process.control.hysteresis.temperature' : 'process.control.hysteresis.voltage';
}
