const openExternalLink = jest.fn();
jest.mock('../desktop/index', () => ({
  openExternalLink,
}));

import React, { useState } from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import _ from 'lodash';
import { act } from 'react-dom/test-utils';

import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import { mockSingleDevice } from '../connection_manager/mocks';
import { ConnectionsViewMock, connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { selectDevices } from '../connection_manager';
import type { EntityKey } from '../keys';
import { createContainer, destroyContainer } from '../container';

import { ProtocolManual } from './ProtocolManual';

let wrapper: RenderResult;

const SelectManualTestComponent: React.FC = () => {
  const [deviceKey, setDeviceKey] = useState<EntityKey | null>(null);
  return <>
    <ConnectionsViewMock selected={deviceKey} selectable={['device']} onSelect={d => { act(() => setDeviceKey(d)) }}/>
    <ProtocolManual deviceOrAxisKey={deviceKey} setting="system.access"/>
  </>;
};

const TestProtocolManual = wrapWithNewStore(wrapWithRouter(SelectManualTestComponent));

beforeEach(async () => {
  createContainer();
  wrapper = render(<TestProtocolManual/>);
  const store = TestProtocolManual.testStore;
  mockSingleDevice(store, d => ({ ...d, identity: { ...d.identity, firmwareVersion: { major: 7, minor: 5, build: 0 } } }));
  const device = _.sample(selectDevices(TestProtocolManual.testStore.getState()))!;
  connectionViewMockInstance.props.onSelect(device.key);
  await waitUntilPass(() => wrapper.findByText('The access level of the user'));
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
});

test('opens external links', async () => {
  fireEvent.click(wrapper.getByText('another setting'));
  expect(openExternalLink).lastCalledWith(
    'https://www.zaber.com/protocol-manual?device=X-LHM&version=7.5&protocol=ASCII#topic_setting_another_setting'
  );
});

test('strips out pub-hardware links', async () => {
  const nonPublicSetting = await waitUntilPass(() => {
    const nonPublicSetting = wrapper.container.querySelector('span.asciisetting[data-disclosure="pub-hardware"]');
    if (nonPublicSetting != null) { return nonPublicSetting }
    throw new TypeError('Expected nonPublicSetting to not be null');
  });
  const nonPublicSettingWrapper = nonPublicSetting.parentElement ?? null;
  expect(nonPublicSettingWrapper).not.toBeNull();
  expect(nonPublicSettingWrapper!.hasAttribute('href')).toBe(false);
  expect(nonPublicSettingWrapper!.className).toBe('unlinked');
});
