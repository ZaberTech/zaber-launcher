import { NoticeType } from '@zaber/react-library';

import type { EntityKey } from '../../keys';
import type { ConnectionState, RouterState } from '../reducer';
import type { AxisInfo, DeviceInfoWithAxes, IdentifiedDeviceInfo } from '../types';

export interface ConnectionContext {
  setExpanded: (expanded: boolean) => void;
}

export interface PassedProps {
  routerContent?: (router: RouterState) => React.ReactNode;
  connectionContent?: (connection: ConnectionState, ctx: ConnectionContext) => React.ReactNode;
  deviceContent?: (device: DeviceInfoWithAxes) => React.ReactNode;
  deviceErrorContent?: (device: DeviceInfoWithAxes) => { node: React.ReactNode; noticeType?: NoticeType } ;
  axisContent?: (axis: AxisInfo, device: DeviceInfoWithAxes) => React.ReactNode;
  controllerContent?: (device: IdentifiedDeviceInfo) => React.ReactNode;
  axisErrors?: Record<string, 'error' | 'warning' | 'info' | null>;
  expandedConnections?: Record<EntityKey, boolean>;
  onConnectionExpand?: (connectionKey: EntityKey, expanded: boolean) => void;
  // Display axes of integrated multi-axis devices.
  displayMultiAxis?: boolean;
}

export type ConnectionPassedProps = Omit<PassedProps, 'routerContent' | 'expandedConnections' | 'onConnectionExpand'>;
export type DevicePassedProps = Omit<ConnectionPassedProps, 'connectionContent'>;
