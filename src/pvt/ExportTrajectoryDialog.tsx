import _ from 'lodash';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { ButtonRowConfirmCancel, Colors, Icons, Modal, Text } from '@zaber/react-library';
import { Time } from '@zaber/motion';

import { InputWithUnits, MeasurementWithUnit, convertBetweenUnits } from '../units';
import { useActions } from '../utils';

import { actions as actionDefinitions } from './actions';
import { selectTrajectoryExportDialogVisible, selectUserPath } from './selectors';

export const IMPORT_BUTTON_LABEL = 'Import';
export const ACCUMULATED_TIME_LABEL = 'Accumulated Time';
export const SEGMENT_TIME_LABEL = 'Segment Time';


export const ExportTrajectoryDialog: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const dialogVisible = useSelector(selectTrajectoryExportDialogVisible);
  const userPath = useSelector(selectUserPath);

  const [timeMeasure, setTimeMeasure] = useState<MeasurementWithUnit>({ value: 100, units: Time.MICROSECONDS });

  if (!dialogVisible || !userPath) {
    return null;
  }

  let linesWarning = <div className="file-size"/>;
  const lastPoint = _.last(userPath.series[0]?.points ?? []);
  if (lastPoint && timeMeasure.value != null) {
    const lineCount = Math.round(lastPoint.time / convertBetweenUnits(timeMeasure.value, timeMeasure.units, Time.SECONDS));
    let fileSize = lineCount * (25 + 35 * userPath.series.length); // ~25 for time columns, ~35 per pos/vel pair.
    let sizeUnit = 'bytes';
    if (fileSize > 1000) {
      fileSize = Math.round(fileSize / 1000);
      sizeUnit = 'KB';
    }

    if (fileSize > 1000) {
      fileSize = Math.round(fileSize / 1000);
      sizeUnit = 'MB';
    }

    linesWarning = <div className="file-size">
      {lineCount >= 1000000 ? <Icons.ErrorWarning color={Colors.orange}/> : <Icons.ErrorNote color={Colors.blue}/>}
      Projected file size: {lineCount} lines (approximately {10 * Math.ceil(fileSize / 10)} {sizeUnit}).
    </div>;
  }

  return <Modal className="export-trajectory-dialog"
    headerIcon={<Icons.Download/>}
    headerText="Export Trajectory"
    isOpen
    buttons={<ButtonRowConfirmCancel
      confirmText="Export"
      onConfirm={(timeMeasure.value == null || timeMeasure.value === 0) ? 'disabled' : () => {
        actions.showTrajectoryExportDialog(false);
        actions.exportTrajectory({ value: timeMeasure.value!, units: timeMeasure.units });
      }}
      onCancel={() => actions.showTrajectoryExportDialog(false)}
    />}
    onRequestClose={() => actions.showTrajectoryExportDialog(false)}
  >
    <Text>Time Interval</Text>
    <InputWithUnits
      title="Time Interval"
      unitFrom={{ dimension: 'Time' }}
      measure={timeMeasure}
      displayPrecision={3}
      onChange={setTimeMeasure}/>
    {linesWarning}
  </Modal>;
};
