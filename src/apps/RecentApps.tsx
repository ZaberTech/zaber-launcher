import React from 'react';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';

import { selectRecentApps, selectPinnedApps } from './selectors';
import { APPS_UI_META } from './ui_meta';

export const RecentApps: React.FC = () => {
  const pinnedApps = useSelector(selectPinnedApps);
  const recentApps = useSelector(selectRecentApps);
  const apps = _.uniqBy(pinnedApps.concat(recentApps), 'url');
  return <>{apps.map(app =>
    <NavLink key={app.url} to={app.url}>
      <div className="item">{APPS_UI_META[app.url].icon}{app.name}</div>
    </NavLink>
  )}</>;
};
