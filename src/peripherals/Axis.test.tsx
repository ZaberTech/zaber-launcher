import { render } from '@testing-library/react';
import React, { useEffect } from 'react';
import type { DeepPartial } from 'redux';

import { createContainer, destroyContainer } from '../container';

import { Axis } from './Axis';

let peripheralCreated = 0;

const PeripheralMock: React.FC = () => {
  useEffect(() => {
    peripheralCreated++;
  }, []);
  return <>Created {peripheralCreated}</>;
};

jest.mock('./Peripheral', () => ({
  get Peripheral() {
    return PeripheralMock;
  }
}));

type AxisProps = React.ComponentPropsWithoutRef<typeof Axis>;
const TextAxis: React.FC<DeepPartial<AxisProps>> = props => <Axis {...(props as AxisProps)}/>;

beforeEach(() => {
  peripheralCreated = 0;

  createContainer();
});

afterEach(() => {
  destroyContainer();
});

test('Recreates peripheral when peripheral.id changes to wipe out any state', () => {
  const wrapper = render(<TextAxis axis={{ identity: { peripheralId: 1 } }}/>);
  expect(peripheralCreated).toBe(1);

  wrapper.rerender(<TextAxis axis={{ identity: { peripheralId: 2 } }}/>);
  expect(peripheralCreated).toBe(2);

  wrapper.unmount();
});
