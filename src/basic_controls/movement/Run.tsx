import React from 'react';
import { Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import type { Moveable } from '../types';
import { usePolledAxisSetting } from '../../polling';

import { actionDefinitions } from './actions';
import type { AxisMovementState } from './types';

interface Props {
  moveable: Moveable;
  direction: 'home' | 'away';
  state: AxisMovementState;
}

export const Run: React.FC<Props> = ({ moveable, direction, state }) => {
  const position = usePolledAxisSetting(state.key, { setting: 'pos' });
  const actions = useActions(actionDefinitions);
  const { motion } = moveable;

  const move = () => {
    if (motion.type === 'linear') {
      actions.issueCommand(moveable, { type: 'abs', initiator: direction, position: direction === 'home' ? motion.min : motion.max });
    } else if (position != null) {
      const targetPosition = direction === 'home' ?
        Math.max((Math.ceil(position / motion.travel) - 1) * motion.travel, motion.min) :
        Math.min((Math.floor(position / motion.travel) + 1) * motion.travel, motion.max);
      actions.issueCommand(moveable, { type: 'abs', initiator: direction, position: targetPosition });
    }
  };
  const props = {
    className: `run-${direction}`,
    activated: state.lastCommand?.type === 'abs' && state.lastCommand.initiator === direction,
    disabled: position == null,
    onClick: move,
  };

  if (motion.type === 'cyclic') {
    if (direction === 'away') {
      return <Icons.RightEnd highlight="off" title="Move to End of the Cycle" {...props}/>;
    } else {
      return <Icons.LeftEnd highlight="off" title="Move to Start of the Cycle" {...props}/>;
    }
  } else if (direction === 'home') {
    return <Icons.LeftEnd highlight="off" title="Move to Beginning" {...props}/>;
  } else {
    return <Icons.RightEnd highlight="off" title="Move to End" {...props}/>;
  }
};
