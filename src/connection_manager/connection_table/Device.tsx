import React, { Component } from 'react';
import { connect } from 'react-redux';
import { routerActions as routerActionsDefinition } from 'connected-react-router';
import { bindActionCreators, Dispatch } from 'redux';
import classNames from 'classnames';
import { NoticeBorder } from '@zaber/react-library';

import { selectAxes, selectDevices } from '../selectors';
import type { RootState } from '../../store';
import { AxisHeader, Controller } from '../device_table';
import type { DeviceInfoWithAxes } from '../types';
import { AxisType } from '../../hardware_modification/types';

import type { DevicePassedProps } from './types';

interface Props extends DevicePassedProps {
  deviceKey: string;
}

interface StateProps {
  device: DeviceInfoWithAxes;
}

class DeviceBase extends Component<Props & StateProps> {
  render(): React.ReactNode {
    const { device, controllerContent, deviceContent, deviceErrorContent, axisContent, axisErrors, displayMultiAxis } = this.props;
    const { node, noticeType } = deviceErrorContent?.(device) ?? {};
    return (<NoticeBorder type={noticeType}>
      <Controller device={device} renderContent={controllerContent}>
        {deviceContent?.(device)}
      </Controller>
      {(device.isController || (displayMultiAxis && device.isMultiAxis)) && <>
        {device.axes.map(axis => {
          if (axis.identity?.axisType === AxisType.PROCESS) { return null }
          const content = axisContent?.(axis, device);
          return (<div key={axis.key}
            className={classNames('axis', axisErrors?.[axis.key], {
              integrated: axis.identity?.isPeripheral === false,
            })}>
            <AxisHeader axis={axis} device={device} displayName={content == null}/>
            {content}
          </div>);
        })}
      </>}
      {node}
    </NoticeBorder>);
  }
}

export const Device = connect(
  (state: RootState, props: Props): StateProps => ({
    device: {
      ...selectDevices(state)[props.deviceKey],
      axes: selectDevices(state)[props.deviceKey].axes.map(axisKey => selectAxes(state)[axisKey]),
    },
  }),
  (dispatch: Dispatch) => ({
    routerActions: bindActionCreators(routerActionsDefinition, dispatch),
  }),
)(DeviceBase);
