import { settingsMetadata } from '@zaber/device-db-metadata';
import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';

import type { SectionValues, SettingData } from '../../types';
import { actions as actionsDefinition } from '../../actions';
import type { FieldIds, SectionIds } from '../../data_definitions';
import { selectProfiledSettingsData } from '../../selectors';

import { HwModField } from './HwModField';

export const UNDEFINED_NAME = 'Undefined Name for this Setting';

interface Props<S extends SectionIds> {
  sectionId: S;
  fieldId: FieldIds<S>;
  values: SectionValues<S>;
  /** Makes this field locked to user interaction, even if it's not disabled */
  locked?: boolean;
}

export const SettingField = <S extends SectionIds>({ sectionId, fieldId, values, locked }: React.PropsWithChildren<Props<S>>) => {
  const actions = useActions(actionsDefinition);

  const settingsData = useSelector(selectProfiledSettingsData);

  const field = values[fieldId];
  if (field == null || settingsData == null) {
    return null;
  }
  if (field.setting == null) {
    throw new Error(`Cannot create a SettingField for field ${fieldId} in section ${sectionId} that has no related setting.`);
  }

  const data: SettingData = settingsData[field.setting];

  return <HwModField
    sectionId={sectionId} fieldId={fieldId} values={values} locked={locked}
    name={settingsMetadata[field.setting]?.humanName ?? UNDEFINED_NAME}
    settingName={field.setting}
    description={data.units ? `${data.description} (${data.units})` : data.description}
    onUpdate={value => actions.updateValue({ sectionId, fieldId, value })}
    docs={{ type: 'setting', setting: field.setting }}
    input={data.options ? { type: 'enum', options: data.options } : { type: 'numeric' }}
  />;
};
