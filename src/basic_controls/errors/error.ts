import { NoticeType } from '@zaber/react-library';

import type { usePolledProductWarnings } from '../../polling/hooks';
import type { RT } from '../../utils';


export interface ProductError {
  headline?: string;
  message: string;
  context?: 'remote-mode';
}

export function worstErrorType(warnings: RT<typeof usePolledProductWarnings>, moveError: ProductError | null): NoticeType | null {
  if (warnings.type === 'error' || moveError != null) { return 'error' }
  if (warnings.flags.some(flag => flag.startsWith('F'))) { return 'error' }
  if (warnings.flags.some(flag => flag.startsWith('W'))) { return 'warning' }
  return warnings.flags.length > 0 ? 'info' : null;
}
