import React from 'react';
import { render } from '@testing-library/react';

import { wrapWithNewStore } from '../test';
import { createContainer, destroyContainer } from '../container';

import { Contact } from './Contact';

const TestContact = wrapWithNewStore(Contact);

beforeEach(() => {
  createContainer();
});
afterEach(() => {
  destroyContainer();
});

test('renders', () => {
  const wrapper = render(<TestContact/>);
  wrapper.unmount();
});
