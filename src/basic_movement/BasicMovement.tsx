import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Text } from '@zaber/react-library';

import type { RootState } from '../store';
import { ConnectionsView } from '../connection_manager';
import { NoContentMessage, Title } from '../components';

import { selectSelectedKey, selectPinnedMovables, selectVisibleUnpinnedMovables, getAxisFromMovable } from './selectors';
import { actions as actionsDefinition } from './actions';
import { MovableCard } from './MovableCard';
import { AllDevices } from './AllDevices';

interface Props {
  actions: typeof actionsDefinition;
  selectedKey: ReturnType<typeof selectSelectedKey>;
  pinnedMovables: ReturnType<typeof selectPinnedMovables>;
  unpinnedMovables: ReturnType<typeof selectVisibleUnpinnedMovables>;
}

class BasicMovementBase extends Component<Props> {
  componentDidMount(): void {
    const { actions } = this.props;
    actions.didMount();
  }

  componentWillUnmount(): void {
    const { actions } = this.props;
    actions.willUnmount();
  }

  render(): React.ReactNode {
    const { actions, selectedKey, pinnedMovables, unpinnedMovables } = this.props;
    const noAxes = pinnedMovables.length === 0 && unpinnedMovables.length === 0;
    return (
      <div className="connection-view-and-app">
        <Title>Basic Movement</Title>

        <ConnectionsView
          selectable={['connection', 'device', 'axis']}
          selected={selectedKey}
          onSelect={actions.selectKey}
          pinnedAxesKeys={pinnedMovables.map(movable => getAxisFromMovable(movable).key)}
          onPinClick={axisKey => actions.unpinAxis(axisKey)}/>

        <div className="basic-movement app-ui">
          {!noAxes && <>
            <AllDevices/>
            {pinnedMovables.length === 0 && <div className="spacer"/>}
          </>}

          {pinnedMovables.length !== 0 && <>
            <div className="pinned-axes-container" data-testid="pinned-axes">
              <Text className="section-heading" t={Text.Type.H5}>Pinned Devices</Text>
              {pinnedMovables.map(movable => <MovableCard key={getAxisFromMovable(movable).key} movable={movable}/>)}
            </div>
            {unpinnedMovables.length !== 0 && selectedKey &&
              <Text className="section-heading" t={Text.Type.H5}>
                Current Selection
              </Text>}
          </>}

          {unpinnedMovables.length !== 0 && <>
            {unpinnedMovables.map(movable => <MovableCard key={getAxisFromMovable(movable).key} movable={movable}/>)}
          </>}

          {noAxes && (
            <NoContentMessage
              title="Welcome to Basic Movement!"
              type={selectedKey ? 'info' : undefined}
              message={selectedKey ? 'There are no mobile axes to display.' : 'Select a connection to start moving the devices.'}/>
          )}
        </div>
      </div>
    );
  }
}

export const BasicMovement = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    selectedKey: selectSelectedKey(state),
    pinnedMovables: selectPinnedMovables(state),
    unpinnedMovables: selectVisibleUnpinnedMovables(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(BasicMovementBase);
