import React from 'react';
import { useSelector } from 'react-redux';
import { NoticeBanner } from '@zaber/react-library';

import { isOperationError, type Operations } from '../types';
import { selectOperations } from '../selectors';
import { actions as actionsDefinition } from '../actions';
import { useActions } from '../../utils';

export const OperationError: React.FC<{
  operation: Operations;
  children?: React.ReactNode;
}> = ({ operation, children }) => {
  const actions = useActions(actionsDefinition);
  const state = useSelector(selectOperations)[operation];
  if (!isOperationError(state)) { return null }

  return <NoticeBanner type="error" closer={() => actions.operationClearError(operation)}>
    {children} {state.error}
  </NoticeBanner>;
};
