import { Length } from '@zaber/motion';

import { MeasurementWithUnit } from '../../units';

interface Point {
  x: MeasurementWithUnit;
  y: MeasurementWithUnit;
}

const ZERO_POINT: Point = { x: { value: 0, units: Length.MILLIMETRES }, y: { value: 0, units: Length.MILLIMETRES } };

export type Dimensions = 'xy' | 'focus' | 'presets';

export const DIMENSION_LABELS: Record<Dimensions, string> = {
  xy: 'XY Position',
  focus: 'Focus',
  presets: 'Presets',
};

export interface MultiDAcqConfig {
  xy: {
    source: 'grid';
    grid: {
      reference: Point;
      spacing: Point;
      size: { x: number | null; y: number | null };
      align: 'center' | 'top-left';
      order: 'snake-rows' | 'snake-columns' | 'rows' | 'columns';
    };
  };
  focus: {
    reference: MeasurementWithUnit;
    spacing: MeasurementWithUnit;
    count: { pos: number | null; neg: number | null };
    autofocus: 'reference' | 'previous' | 'off';
  };
  presets: {
    selected: string[];
  };
  autofocus: {
    on: boolean;
  };
  order: Dimensions[];
}

export const defaultMultiDAcqConfig: MultiDAcqConfig = {
  xy: {
    source: 'grid',
    grid: {
      reference: ZERO_POINT,
      spacing: {
        x: { value: 100, units: Length.MICROMETRES },
        y: { value: 100, units: Length.MICROMETRES },
      },
      size: { x: 1, y: 1 },
      align: 'top-left',
      order: 'snake-rows',
    },
  },
  focus: {
    reference: { value: 0, units: Length.MILLIMETRES },
    spacing: { value: 10, units: Length.MICROMETRES },
    count: { pos: 0, neg: 0 },
    autofocus: 'off',
  },
  presets: {
    selected: [],
  },
  autofocus: {
    on: false,
  },
  order: ['presets', 'xy', 'focus'],
};

export interface ProgressReport {
  taken: number;
  total: number;
}

export interface DoneReport {
  elapsedTime: number;
  total: number;
}
