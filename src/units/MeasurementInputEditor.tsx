import { InputEditor, PopUp } from '@zaber/react-library';
import React from 'react';
import classnames from 'classnames';

import { InputWithUnitProps, InputWithUnits } from './InputWithUnits';
import { Measurement, MeasurementOK, measurementOK } from './types';

export type MeasurementInputEditorState = InputEditor.State<Measurement | null, MeasurementOK>;

interface Props extends Omit<InputWithUnitProps, 'onChange' | 'defaultValue'> {
  mode: InputEditor.Mode;
  onChange: (change: MeasurementInputEditorState) => void;
  align?: PopUp.Align;
  /** The message displayed with the placeholder input (for spacing purposes only) */
  placeholderMessage?: React.ReactNode;
  renderExtra?: () => React.ReactNode;
  defaultMeasurement?: MeasurementOK | null;
}

export const MeasurementInputEditor: React.FC<Props> = props => {
  const {
    mode,
    onChange,
    measure,
    displayPrecision,
    align,
    message,
    placeholderMessage,
    className,
    disabled,
    renderExtra,
    defaultMeasurement,
    ...rest
  } = props;

  return <InputEditor<Measurement | null, MeasurementOK>
    value={measure}
    mode={mode}
    onChange={onChange}
    isValid={measurementOK}
    align={align}
    className={classnames('measurement-input-editor', className)}
    defaultValue={defaultMeasurement}
    renderExtra={renderExtra}
    renderInput={({ mode, value, startEditing, ref, setValue, placeholder }) => (
      <InputWithUnits
        {...rest}
        message={placeholder ? placeholderMessage : message}
        measure={value}
        disabled={mode === 'initialize' || mode === 'write' || disabled}
        ref={ref}
        onFocus={startEditing}
        displayPrecision={!placeholder && mode === 'edit' ? undefined : displayPrecision}
        onChange={setValue}
      />
    )}
  />;
};
