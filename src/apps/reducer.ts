import { LocationChangePayload, LOCATION_CHANGE } from 'connected-react-router';
import _ from 'lodash';

import { createReducer } from '../utils';

import { ActionTypes, ActionsToPayloads } from './actions';
import { AppUrls, APP_LIST, MAX_RECENT_APPS } from './list';

export interface State {
  lastAppUrl: string | null;
  recentApps: AppUrls[];
  pinnedApps: Partial<Record<AppUrls, true>>;
  unlockedApps: Record<string, true>;
}

const initialState: State = {
  lastAppUrl: null,
  recentApps: [],
  pinnedApps: {},
  unlockedApps: {},
};

const locationChange = (state: State, { location }: LocationChangePayload): State => {
  const currentApp = APP_LIST.find(app => location.pathname.startsWith(app.url));
  if (!currentApp) {
    if (state.lastAppUrl == null) {
      return state;
    }
    return { ...state, lastAppUrl: null };
  }

  if (currentApp.url === state.lastAppUrl) {
    return state;
  }

  let recentApps = state.recentApps;
  if (!state.pinnedApps[currentApp.url]) {
    recentApps = [currentApp.url, ...state.recentApps.filter(url => currentApp.url !== url)].slice(0, MAX_RECENT_APPS);
  }

  return {
    ...state,
    lastAppUrl: currentApp.url,
    recentApps,
  };
};

const storedAppsLoaded = (state: State, { recent, pinned }: ActionsToPayloads[ActionTypes.STORED_APPS_LOADED]): State => ({
  ...state,
  recentApps: recent,
  pinnedApps: _(pinned).keyBy().mapValues(() => true).value(),
});

const pinApp = (state: State, { app, pinned }: ActionsToPayloads[ActionTypes.PIN_APP]): State => ({
  ...state,
  pinnedApps: pinned ? { ...state.pinnedApps, [app]: true } : _.omit(state.pinnedApps, app),
  recentApps: pinned ? state.recentApps.filter(other => other !== app) : state.recentApps,
});

const unlockApp = (state: State, { url }: ActionsToPayloads[ActionTypes.UNLOCK_APP]): State => ({
  ...state,
  unlockedApps: {
    ...state.unlockedApps,
    [url]: true,
  },
});

type LocationPayloads = {
  [LOCATION_CHANGE]: LocationChangePayload;
};

export const reducer = createReducer<ActionsToPayloads & LocationPayloads, typeof initialState>({
  [LOCATION_CHANGE]: locationChange,
  [ActionTypes.STORED_APPS_LOADED]: storedAppsLoaded,
  [ActionTypes.PIN_APP]: pinApp,
  [ActionTypes.UNLOCK_APP]: unlockApp,
}, initialState);
