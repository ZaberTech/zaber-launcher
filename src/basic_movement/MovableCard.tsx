import React from 'react';
import _ from 'lodash';
import { Card, Text, Icons, ContextMenu, NoticeBorder } from '@zaber/react-library';
import { Link } from 'react-router-dom';
import { ascii } from '@zaber/motion';

import { connectionName, ConnectionsView } from '../connection_manager';
import { getUnits, InputWithUnits, ValueDisplay } from '../units';
import { useActions } from '../utils';
import { DRIVER_DISABLED_FLAGS } from '../protocol_manual';
import { useUnitInfo } from '../units/hooks';

import { actions as actionsDefinition } from './actions';
import { AxisStateComplete, Movable, getAxisFromMovable } from './selectors';
import { getDisplayPrecision, MovementType } from './types';
import { PositionDisplay } from './PositionDisplay';
import { PreciseMovement } from './PreciseMovement';
import { ErrorView } from './Error';
import { MoveMinMax } from './MoveMinMax';
import { MovableLabel } from './MovableLabel';
import { JogLeftRight } from './JogLeftRight';
import { ThumbnailGroup } from './ThumbnailGroup';

interface Props {
  movable: Movable;
}

export const MovableCard: React.FC<Props> = props => {
  const actions = useActions(actionsDefinition);
  const { movable } = props;
  const axis = getAxisFromMovable(movable);
  const { device } = axis;
  const posUnitInfo = useUnitInfo({ setting: 'pos', entity: axis.key });
  const maxError = _.maxBy(axis.errors, err => err.type);
  const homingNeeded = axis.errors.some(err => err.warningFlag === ascii.WarningFlags.NO_REFERENCE_POSITION);
  const driverDisabled = axis.errors.some(error => DRIVER_DISABLED_FLAGS.includes(error.warningFlag!));
  const settingsUrl = `/device-settings?${ConnectionsView.SELECT_PARAM}=${device.isMultiAxis ? axis.key : device.key}`;

  const driverLabel = driverDisabled ? 'Enable Driver' : 'Disable Driver';
  return (
    <Card className="axis-card" data-testid={`movable-card-${axis.key}`}>
      <NoticeBorder type={maxError ? 'error' : null}>
        <div className="axis-grid" data-testid={getAxisGridTestId(axis)}>
          <MovableLabel movable={movable}/>
          <div className="separator"/>
          <div className="position-label"><Text t={Text.Type.BodySm} e={Text.Emphasis.Light}>Position</Text></div>
          <InputWithUnits
            unitFrom={{ setting: 'pos', entity: axis.key }}
            className="position"
            title="Device Position"
            alignValue="left"
            displayPrecision={getDisplayPrecision(axis)}
            disabled="readonly"
            measure={axis.position}
            onChange={newValue => actions.changeUnits(axis.key, newValue.units, newValue.value)}
          />
          <div className="menu">
            <ContextMenu>
              <Link to={settingsUrl}>
                <ContextMenu.Item className="clickable" icon={<Icons.DoubleGear/>}>
                  Settings
                </ContextMenu.Item>
              </Link>
              <ContextMenu.Item title={driverLabel} onClick={() => actions.setDriver(axis.key, driverDisabled)} icon={<Icons.Flash/>}>
                {driverLabel}
              </ContextMenu.Item>
            </ContextMenu>
          </div>
          <div className="pin">
            <Icons.Pin
              title="Pin Axis"
              onClick={() => axis.isPinned ? actions.unpinAxis(axis.key) : actions.pinAxis(axis.key)}
              activated={axis.isPinned}/>
          </div>
          <ThumbnailGroup movable={movable}/>
          {axis.travelInfo && <div className="device-info">
            <Text e={Text.Emphasis.Light}>
                Travel Distance&nbsp;
              <ValueDisplay entityKey={axis.key} setting="pos" value={axis.travelInfo.distance} unit={getUnits(axis.position)} digits={0}/>
            </Text>
          </div>}
          <div className="home">
            <Icons.Home title="Home Axis"
              highlight={homingNeeded ? 'on' : 'off'}
              activated={axis.lastInitiator?.includes('home')}
              onClick={() => actions.moveAxis(
                axis.key, MovementType.Home, 'home')}/>
          </div>
          <div className="jog-controls">
            <MoveMinMax axis={axis} which="min" posUnitInfo={posUnitInfo}/>
            <JogLeftRight axis={axis} which="min"/>
            <Icons.Stop title="Stop Axis"
              activated={axis.lastInitiator?.includes('stop')}
              onClick={() => actions.moveAxis(
                axis.key, MovementType.Stop, 'stop')}/>
            <JogLeftRight axis={axis} which="max"/>
            <MoveMinMax axis={axis} which="max" posUnitInfo={posUnitInfo}/>
          </div>
          <div className="track-and-minmax-controls">
            <PositionDisplay
              axis={axis}
              posUnitInfo={posUnitInfo}
              deviceIsMoving={axis.movementRequests > 0}
              onRequestMovement={positionNative => actions.moveAxis(
                axis.key, MovementType.Absolute, 'track',  positionNative)}/>
          </div>
          <div className="advanced">
            <PreciseMovement axis={axis}/>
          </div>
        </div>
        <div className="errors">
          {axis.errors.map((err, i) =>
            <ErrorView key={i} error={err}
              entity={axis.key}
              onDismiss={() => actions.dismissError(axis.key, i)}
              enableDriver={() => actions.setDriver(axis.key, true)}/>
          )}
        </div>
      </NoticeBorder>
    </Card>
  );
};

const getAxisGridTestId = (axis: AxisStateComplete) => {
  const { device, connection } = axis;
  let testId = `con ${connectionName(connection)} > dev ${device.address}`;
  if (device.isMultiAxis) {
    if (!axis.lockstep) {
      testId += ` > axis ${axis.axisNumber}`;
    } else {
      testId += ` > lockstep ${axis.lockstep.groupNumber}`;
    }
  }
  return testId;
};
