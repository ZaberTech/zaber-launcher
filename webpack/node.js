const path = require('path');
const { shared } = require('./shared');

module.exports = {
  ...shared,
  entry: './src_node/main.ts',
  target: 'node',
  node: false,
  output: {
    filename: 'main.js',
    path: path.join(__dirname, '..', 'build_node'),
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json']
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          configFile: 'tsconfig.node.json'
        }
      },
      { enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' },
    ]
  },
};
