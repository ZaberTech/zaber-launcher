import _ from 'lodash';

import { createReducer, changeDictionary } from '../utils';
import type { Connection } from '../app_components';
import { LOCAL_ROUTER_URL } from '../message_router';
import { EntityKey, makeRouterKey, makeConnectionKey, extractRouterKey, extractConnectionKey, removeWithKey } from '../keys';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { RouterType, DeviceInfo, AxisInfo, StoredRouter, LoadingDevicesError } from './types';

export interface RouterState {
  key: EntityKey;

  url: string;
  type: RouterType;
  name: string | null;

  loadingConnections: boolean;
  connectionsErr: string | null;

  connections: EntityKey[] | null;
}

export interface ConnectionState extends Connection {
  key: EntityKey;

  loadingDevices: boolean;
  identifyingDevices: boolean;
  loadingDevicesErr: LoadingDevicesError | null;

  devices: EntityKey[] | null;
}

export interface DeviceState extends DeviceInfo  {
  key: EntityKey;

  axes: EntityKey[];
}

export interface AxisState extends AxisInfo  {
  key: EntityKey;
}

export interface State {
  loadingRouters: boolean;
  routers: Record<string, RouterState>;
  connections: Record<string, ConnectionState>;
  devices: Record<string, DeviceState>;
  axes: Record<string, AxisState>;
  thumbnails: Record<number, string | undefined>;
}

function defaultRouterState(stored: StoredRouter): RouterState {
  return {
    ...stored,
    key: makeRouterKey(stored.url),
    name: null,
    connections: null,
    loadingConnections: false,
    connectionsErr: null,
  };
}

const initialState: State = {
  loadingRouters: false,
  routers: {},
  connections: {},
  devices: {},
  axes: {},
  thumbnails: {},
};

const loadRouters = (state: State): State =>
  ({
    ...state,
    loadingRouters: true,
  });

const routersLoaded = (state: State, { routers }: ActionsToPayloads[ActionTypes.ROUTERS_LOADED]): State =>
  ({
    ...state,
    loadingRouters: false,
    routers: _.keyBy(routers.map(defaultRouterState), 'key'),
    connections: {},
    devices: {},
    axes: {},
  });

const addRouter = (state: State, { router }: ActionsToPayloads[ActionTypes.ADD_ROUTER]): State =>
  ({
    ...state,
    routers: {
      ...state.routers,
      [makeRouterKey(router.url)]: defaultRouterState(router),
    },
  });
const removeRouter = (state: State, { key }: ActionsToPayloads[ActionTypes.REMOVE_ROUTER]): State =>
  ({
    ...state,
    routers: removeWithKey(state.routers, _.identity, key),
    connections: removeWithKey(state.connections, extractRouterKey, key),
    devices: removeWithKey(state.devices, extractRouterKey, key),
    axes: removeWithKey(state.axes, extractRouterKey, key),
  });

const loadConnections = (state: State, { routerKey }: ActionsToPayloads[ActionTypes.LOAD_CONNECTIONS]): State =>
  ({
    ...state,
    routers: changeDictionary(state.routers, routerKey, { loadingConnections: true, connectionsErr: null }),
  });

const connectionsLoadedErr = (state: State, { routerKey, err }: ActionsToPayloads[ActionTypes.CONNECTIONS_LOADED_ERR]): State =>
  ({
    ...state,
    routers: changeDictionary(state.routers, routerKey, router => ({
      ...router,
      loadingConnections: false,
      connectionsErr: err,
      connections: null,
    })),
    connections: removeWithKey(state.connections, extractRouterKey, routerKey),
    devices: removeWithKey(state.devices, extractRouterKey, routerKey),
    axes: removeWithKey(state.axes, extractRouterKey, routerKey),
  });

const connectionsLoaded = (state: State, { routerKey, info }: ActionsToPayloads[ActionTypes.CONNECTIONS_LOADED]): State =>
  ({
    ...state,
    routers: changeDictionary(state.routers, routerKey, router => ({
      ...router,
      loadingConnections: false,
      connections: info.connections.map(connection => makeConnectionKey(routerKey, connection.id)),
      name: info.name,
    })),
    connections: {
      ...removeWithKey(state.connections, extractRouterKey, routerKey),
      ..._.keyBy(info.connections.map<ConnectionState>(connection =>
        newConnectionState(state, connection, routerKey)), 'key'),
    },
    devices: removeWithKey(state.devices, extractRouterKey, routerKey),
    axes: removeWithKey(state.axes, extractRouterKey, routerKey),
  });

function newConnectionState(previousState: State, connection: Connection, routerKey: string): ConnectionState {
  const key = makeConnectionKey(routerKey, connection.id);
  return ({
    key,
    ...connection,
    devices: null,
    loadingDevices: false,
    identifyingDevices: false,
    loadingDevicesErr: null,
  });
}

const loadDevices = (state: State, { connectionKey }: ActionsToPayloads[ActionTypes.LOAD_DEVICES]): State =>
  ({
    ...state,
    connections: changeDictionary(state.connections, connectionKey, {
      loadingDevices: true,
      identifyingDevices: true,
      loadingDevicesErr: null,
    }),
  });

const devicesLoadedErr = (state: State, { connectionKey, err }: ActionsToPayloads[ActionTypes.DEVICES_LOADED_ERR]): State => ({
  ...state,
  connections: changeDictionary(state.connections, connectionKey, connection => ({
    ...connection,
    devices: null,
    loadingDevices: false,
    identifyingDevices: false,
    loadingDevicesErr: err,
  })),
  devices: removeWithKey(state.devices, extractConnectionKey, connectionKey),
  axes: removeWithKey(state.axes, extractConnectionKey, connectionKey),
});

const devicesLoaded = (state: State, { connectionKey, devices }: ActionsToPayloads[ActionTypes.DEVICES_LOADED]): State => {
  const deviceStates = devices.map<DeviceState>(device => ({
    ...device,
    axes: device.axes.map(axis => axis.key),
  }));
  const axisStates = _.flatMap(devices, device => device.axes);

  return ({
    ...state,
    connections: changeDictionary(state.connections, connectionKey, connection => ({
      ...connection,
      loadingDevices: false,
      identifyingDevices: false,
      devices: deviceStates.map(device => device.key),
    })),
    devices: {
      ...removeWithKey(state.devices, extractConnectionKey, connectionKey),
      ..._.keyBy(deviceStates, 'key'),
    },
    axes: {
      ...removeWithKey(state.axes, extractConnectionKey, connectionKey),
      ..._.keyBy(axisStates, 'key'),
    },
  });
};

const devicesIdentified = (state: State, { connectionKey }: ActionsToPayloads[ActionTypes.DEVICES_IDENTIFIED]): State => ({
  ...state,
  connections: changeDictionary(state.connections, connectionKey, { identifyingDevices: false }),
});

const setThumbnails = (state: State, { thumbnails } : ActionsToPayloads[ActionTypes.SET_THUMBNAILS]): State => ({
  ...state,
  thumbnails: { ...state.thumbnails, ...thumbnails },
});

const updateLocalRouterName = (state: State, { name }: ActionsToPayloads[ActionTypes.UPDATE_LOCAL_ROUTER_NAME]): State =>
  ({
    ...state,
    routers: changeDictionary(state.routers, makeRouterKey(LOCAL_ROUTER_URL), { name }),
  });

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.ADD_ROUTER]: addRouter,
  [ActionTypes.REMOVE_ROUTER]: removeRouter,
  [ActionTypes.LOAD_ROUTERS]: loadRouters,
  [ActionTypes.ROUTERS_LOADED]: routersLoaded,
  [ActionTypes.LOAD_CONNECTIONS]: loadConnections,
  [ActionTypes.CONNECTIONS_LOADED]: connectionsLoaded,
  [ActionTypes.CONNECTIONS_LOADED_ERR]: connectionsLoadedErr,
  [ActionTypes.LOAD_DEVICES]: loadDevices,
  [ActionTypes.DEVICES_LOADED]: devicesLoaded,
  [ActionTypes.DEVICES_IDENTIFIED]: devicesIdentified,
  [ActionTypes.DEVICES_LOADED_ERR]: devicesLoadedErr,
  [ActionTypes.UPDATE_LOCAL_ROUTER_NAME]: updateLocalRouterName,
  [ActionTypes.SET_THUMBNAILS]: setThumbnails,
}, initialState);
