/**
 * jsdom replaces Uint8Array for something not compatible which causes issues in libraries like protobuf.
 * This code reverts Uint8Array to the one provided by environment. */
(function fixUint8Array() {
  let findPrototype = Buffer as any;
  while (findPrototype.__proto__) {
    if (findPrototype.name === 'Uint8Array') {
      global.Uint8Array = findPrototype;
      break;
    }
    findPrototype = findPrototype.__proto__;
  }

  if (!(Buffer.from('test') instanceof Uint8Array)) {
    throw new Error('Buffer not compatible with Uint8Array');
  }
})();

import 'reflect-metadata';
import '@testing-library/jest-dom/extend-expect';
import '@zaber/toolbox/lib/test/setup';
import '@testing-library/react/dont-cleanup-after-each';

jest.mock('../cloud/amplify', () => ({}));
jest.mock('monaco-editor/esm/vs/editor/editor.api', () => ({
  languages: {
    register: jest.fn(),
    setLanguageConfiguration: jest.fn(),
    setMonarchTokensProvider: jest.fn(),
  },
  editor: {
    defineTheme: jest.fn(),
  },
}));

if (typeof global.TextEncoder === 'undefined') {
  // eslint-disable-next-line @typescript-eslint/no-require-imports
  const util = require('util');
  global.TextEncoder = util.TextEncoder;
  global.TextDecoder = util.TextDecoder;
}
