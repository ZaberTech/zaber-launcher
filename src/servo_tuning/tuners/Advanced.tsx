import { NoticeBanner, NumericInput, Text } from '@zaber/react-library';
import React from 'react';
import { useSelector } from 'react-redux';

import { NoContentMessage } from '../../components';
import { OneTimeMessage } from '../../help';
import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import { ApplyButtons } from '../components/ApplyButtons';
import { StatusMessage } from '../components/StatusMessage';
import { selectSortedAdvancedParams, selectTuningInfo } from '../selectors';
import type { ChosenParamset } from '../types';

export const Advanced: React.FC<ChosenParamset> = ({ entity, paramset }) => {
  const actions = useActions(actionDefinitions);
  const info = useSelector(selectTuningInfo);
  const tuning = useSelector(selectSortedAdvancedParams);
  if (info?.method !== 'advanced' || info.paramset !== paramset || tuning == null) {
    return <NoContentMessage message="Getting tuning..." type="working"/>;
  }
  const setParam = (paramName: string) => (
    (value: number | null) => {
      actions.updateParamTuning({ ...info, tuning: { ...info.tuning, [paramName]: value } });
    }
  );
  return <div className="advanced-tuning">
    <OneTimeMessage messageId="youDoNotWantToUseAdvancedTuningInMostCases"
      message={dismiss => (
        <NoticeBanner type="warning" closer={dismiss} className="servo-tuning-warning">
          <p>
            The Advanced tuning method allows direct editing of the raw parameters.
          </p>
          <p>
            We strongly recommend using either the Simple or PID methods to tune the control loop.
            These methods should suffice for almost all applications, and are simpler
            (they do not require in-depth knowledge of the control loop). If you do need details of any of the raw parameters,
            please reach out to us at <Text f={Text.Family.Mono}>contact@zaber.com</Text>
          </p>
        </NoticeBanner>
      )}
    />
    <div className="header table-width">
      <span className="over-descriptions"/>
      <Text t={Text.Type.Helper} className="column">Parameter Value</Text>
      <Text t={Text.Type.Helper} className="column">At High Speed</Text>
    </div>
    {tuning.described.map(described => <div key={described.paramName} className="tuning-param table-width">
      <Text t={Text.Type.H5}>{described.description} ({described.paramName})</Text>
      {described.param && (
        <NumericInput
          onNumberChange={setParam(described.param.name)} value={described.param.value}
          data-testid={`described-param-${described.paramName}`}
        />
      )}
      {described.highSpeedParam && (
        <NumericInput
          onNumberChange={setParam(described.highSpeedParam.name)} value={described.highSpeedParam.value}
          data-testid={`high-speed-param-${described.paramName}`}
        />
      )}
    </div>)}
    {tuning.undescribed.map(param => <div key={param.name} className="tuning-param table-width">
      <Text t={Text.Type.H5}>{param.name}</Text>
      <NumericInput onNumberChange={setParam(param.name)} value={param.value} data-testid={`other-param-${param.name}`}/>
    </div>)}
    <ApplyButtons paramset={paramset} onClick={(toLive, toPowerOn) => {
      actions.applyAdvanced(entity, paramset, tuning.raw, toLive, toPowerOn);
    }}/>
    <StatusMessage message={info?.message} paramset={paramset}/>
  </div>;
};
