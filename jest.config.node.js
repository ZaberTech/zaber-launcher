module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  transform: {
    '^.+\\.(ts|tsx)?$': ['ts-jest', { tsconfig: 'tsconfig.node.json', isolatedModules: true }],
  },
  maxWorkers: '50%',
  setupFilesAfterEnv: ['<rootDir>/src_node/test/setup.ts'],
  moduleDirectories: ['node_modules'],
  testMatch: ['<rootDir>/src_node/**/*.test.ts'],
};
