import { useSelector } from 'react-redux';
import React, { createRef, useEffect, useRef, useState } from 'react';
import { Flex, Input, Text } from '@zaber/react-library';
import classNames from 'classnames';

import { commandPartToText, getCommandSuggestions, suggestionToText } from '../terminal/utils';
import { UnitConversionNote } from '../terminal/CommandSuggestions';

import { selectCommandList } from './selectors';


interface SelectCommandProps {
  command: string;
  onChange: (command: string) => void;
}

export const SelectCommand: React.FC<SelectCommandProps> = ({ command, onChange }) => {
  const commandList = useSelector(selectCommandList);

  const inputParts = command.split(' ');
  const commandSuggestions = getCommandSuggestions(commandList, inputParts);

  const [selected, setSelected] = useState(false);
  const [suggestionSelected, setSuggestionSelected] = useState<number | null>(null);
  const optionRefs = commandSuggestions.map(() => createRef<HTMLDivElement>());
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (suggestionSelected != null) {
      optionRefs[suggestionSelected]?.current?.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
    }
  }, [suggestionSelected]);

  useEffect(() => {
    setSuggestionSelected(null);
  }, [command]);

  const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      if (suggestionSelected == null) {
        setSelected(false);
      } else {
        onChange(suggestionToText(commandSuggestions[suggestionSelected].parts, command));
      }
    } else if (event.key === 'Tab') {
      event.preventDefault();
      if (commandSuggestions.length > 0) {
        onChange(suggestionToText(commandSuggestions[0].parts, command));
      }
    } else if (event.key === 'ArrowDown') {
      event.preventDefault();
      setSuggestionSelected(suggestionSelected === null ? 0 : Math.min(commandSuggestions.length - 1, suggestionSelected + 1));
    } else if (event.key === 'ArrowUp') {
      event.preventDefault();
      if (suggestionSelected != null) {
        if (suggestionSelected === 0) {
          setSuggestionSelected(null);
        } else {
          setSuggestionSelected(suggestionSelected - 1);
        }
      }
    } else {
      setSelected(true);
    }
  };

  return <Flex.Column
    className="select-command"
    onFocus={() => setSelected(true)}
    onBlur={() => setSelected(false)}>
    <Input
      ref={inputRef}
      title="Command"
      className="input-command"
      value={command}
      clearable
      onValueChange={onChange}
      onKeyDown={onKeyDown}
      spellCheck={false}
    />
    {selected && commandSuggestions.length > 0 &&
      <div title="Command Suggestions" className="command-suggestions">
        {commandSuggestions.map((suggestion, i) =>
          <div
            ref={optionRefs[i]}
            className={classNames('suggestion-option', { selected: suggestionSelected === i })}
            key={i}
            onMouseEnter={() => setSuggestionSelected(i)}
            onMouseLeave={() => setSuggestionSelected(null)}
          >
            <div
              className="suggestion-command"
              onMouseDown={() => {
                onChange(suggestionToText(suggestion.parts, command));
                setTimeout(() => inputRef?.current?.focus(), 0);
              }}
            >
              {suggestion.parts.map((part, i) =>
                <Text key={i} className={classNames('part', part.type)}>{commandPartToText(part)}</Text>)}
            </div>
            {suggestion.type === 'commandWithUnits' && <UnitConversionNote/>}
          </div>)}
      </div>
    }
  </Flex.Column>;
};
