import os from 'os';

import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore } from '../test';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import { makeRouterKey } from '../keys';
import { RouterType } from '../connection_manager';

import { LauncherName } from './LauncherName';

@injectable()
export class MessageRoutersServiceMock {
  async getConnection() {
    return this;
  }
  get api() {
    return this;
  }
  setName = jest.fn().mockResolvedValue(undefined);
}

let container: Container;
let messageRouter: MessageRoutersServiceMock;

const TestLauncherName = wrapWithNewStore(LauncherName);

let wrapper: RenderResult;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  messageRouter = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

const setName = async (name: string) => {
  fireEvent.focus(wrapper.getByRole('textbox', { name: 'Launcher Name:' }));
  await waitUntilPass(() => fireEvent.input(wrapper.getByRole('textbox', { name: 'Launcher Name:' }), { target: { value: name } }));
  fireEvent.click(wrapper.getByText('Save'));
};

describe('with name', () => {
  beforeEach(() => {
    wrapper = render(<TestLauncherName preloadedState={{
      connectionManager: {
        routers: {
          [makeRouterKey(LOCAL_ROUTER_URL)]: { type: RouterType.Local, name: 'Old name' },
        }
      }
    }}/>);
  });

  test('sets the name', async () => {
    await setName('New name');

    await waitUntilPass(() => {
      expect(messageRouter.setName).toHaveBeenCalledWith('New name');
    });
    expect(wrapper.queryByText('Save')).toBeNull();
    wrapper.getByDisplayValue(/New name/);
  });

  test('displays error in unlikely event of failure', async () => {
    messageRouter.setName.mockRejectedValueOnce(new Error('very unlikely error'));

    await setName('New name');

    await waitUntilPass(() => wrapper.getByText(/very unlikely error/));
  });
});

describe('name not populated', () => {
  beforeEach(() => {
    wrapper = render(<TestLauncherName preloadedState={{
      connectionManager: {
        routers: {
          [makeRouterKey(LOCAL_ROUTER_URL)]: { type: RouterType.Local, name: '' },
        }
      }
    }}/>);
  });

  test('sets the name to hostname by default', async () => {
    const hostname = os.hostname();
    await waitUntilPass(() => {
      expect(messageRouter.setName).toHaveBeenCalledWith(hostname);
    });
    wrapper.getByDisplayValue(hostname);
  });
});
