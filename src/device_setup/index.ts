export { reducer as deviceSetupReducer } from './reducer';
export type { State as DeviceSetupState } from './reducer';
export {
  actions as deviceSetupActions,
  ActionTypes as DeviceSetupActionTypes,
} from './actions';
export type {
  ActionsToPayloads as DeviceSetupActionPayloads,
} from './actions';
export { rootSaga as deviceSetupSaga } from './sagas';
export { selectPendingSetups } from './selectors';
