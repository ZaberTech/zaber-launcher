import { roundedE3Sequence } from '../utils';

import { sortDimensionNames } from './utils';

describe('E3 preferred number sequence', () => {
  it('works for simple ranges', () => {
    expect(roundedE3Sequence(1, 100)).toEqual([1, 2, 5, 10, 20, 50, 100]);
  });

  it('produces aligned output from misaligned input', () => {
    expect(roundedE3Sequence(7.5, 49.5)).toEqual([10, 20]);
  });

  it('works for small values', () => {
    expect(roundedE3Sequence(0.001, 0.01)).toEqual([0.001, 0.002, 0.005, 0.01]);
  });

  it('works across zero', () => {
    expect(roundedE3Sequence(0.1, 2)).toEqual([0.1, 0.2, 0.5, 1, 2]);
  });
});


describe('Sort dimension names', () => {
  it('preserves hardcoded order', () => {
    const data = ['Acceleration', 'Velocity', 'Length'];
    sortDimensionNames(data);
    expect(data).toEqual(['Length', 'Velocity', 'Acceleration']);
  });

  it('sorts alphabetically when hardcoded order doesn not apply', () => {
    const data = ['Force', 'Current', 'Bobness'];
    sortDimensionNames(data);
    expect(data).toEqual(['Bobness', 'Current', 'Force']);
  });

  it('places alphabetic items after hardcoded items', () => {
    const data = ['Velocity', 'Acceleration', 'Force', 'Current'];
    sortDimensionNames(data);
    expect(data).toEqual(['Velocity', 'Acceleration', 'Current', 'Force']);
  });
});

