import _ from 'lodash';
import { InputEditor } from '@zaber/react-library';

import { createReducer, writeToDictionary } from '../../utils';
import type { EntityKey } from '../../keys';

import { ActionsToPayloads, ActionTypes } from './actions';

export type DeviceOutputsState = {
  /** A record of all of the digital channels with outstanding changes */
  digitalChanges: Partial<Record<number, boolean>>;
  /** A record of all of the analog channels with outstanding changes */
  analogChanges: Partial<Record<number, InputEditor.State<number | null, number>>>;
  error: { message: string; details: string } | null;
  labels: Record<string, string>;
  labelErrors: Record<string, { message: string; details: string } | null>;
};

const initialDeviceOutputsState = {
  digitalChanges: {},
  analogChanges: {},
  error: null,
  labels: {},
  labelErrors: {},
};

export interface State {
  devices: Partial<Record<EntityKey, DeviceOutputsState>>;
}

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const initialState: State = {
  devices: {},
};

const startSetDigital: Reducer<ActionTypes.START_SET_DIGITAL> = (state, { deviceKey, channel, on }) => ({
  ...state,
  devices: writeToDictionary(state.devices, deviceKey, (_, device = initialDeviceOutputsState) => ({
    ...device,
    digitalChanges: { ...device.digitalChanges, [channel]: on },
  }))
});

const completeSetDigital: Reducer<ActionTypes.COMPLETE_SET_DIGITAL> = (state, { deviceKey, channel }) => ({
  ...state,
  devices: writeToDictionary(state.devices, deviceKey, (_key, device = initialDeviceOutputsState) => ({
    ...device,
    digitalChanges: _.omit(device.digitalChanges, channel),
  }))
});

const setAnalog: Reducer<ActionTypes.SET_ANALOG> = (state, { deviceKey, channel, state: output }) => ({
  ...state,
  devices: writeToDictionary(state.devices, deviceKey, (_, device = initialDeviceOutputsState) => ({
    ...device,
    analogChanges: { ...device.analogChanges, [channel]: output },
  }))
});

const setError: Reducer<ActionTypes.SET_ERROR> = (state, { deviceKey, error }) => ({
  ...state,
  devices: writeToDictionary(state.devices, deviceKey, (_, device = initialDeviceOutputsState) => ({ ...device, error }))
});

const setLabel: Reducer<ActionTypes.SET_LABEL> = (state, { deviceKey, portType, channelNumber, newValue }) => ({
  ...state,
  devices: writeToDictionary(state.devices, deviceKey, (_, device = initialDeviceOutputsState) => ({
    ...device,
    labels: { ...device.labels, [`${portType}-${channelNumber}`]: newValue },
  }))
});

const setLabelError: Reducer<ActionTypes.SET_LABEL_ERROR> = (state, { deviceKey, portType, channelNumber, prevValue, error }) => ({
  ...state,
  devices: writeToDictionary(state.devices, deviceKey, (_, device = initialDeviceOutputsState) => ({
    ...device,
    labels: { ...device.labels, [`${portType}-${channelNumber}`]: prevValue },
    labelErrors: { ...device.labelErrors, [`${portType}-${channelNumber}`]: error },
  })),
});

const deleteLabelError: Reducer<ActionTypes.DELETE_LABEL_ERROR> = (state, { deviceKey, ioErrorKey }) => ({
  ...state,
  devices: writeToDictionary(state.devices, deviceKey, (_key, device = initialDeviceOutputsState) => ({
    ...device,
    labelErrors: _.omit(device.labelErrors, ioErrorKey),
  })),
});

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.START_SET_DIGITAL]: startSetDigital,
  [ActionTypes.COMPLETE_SET_DIGITAL]: completeSetDigital,

  [ActionTypes.SET_ANALOG]: setAnalog,

  [ActionTypes.SET_ERROR]: setError,

  [ActionTypes.SET_LABEL]: setLabel,
  [ActionTypes.SET_LABEL_ERROR]: setLabelError,
  [ActionTypes.DELETE_LABEL_ERROR]: deleteLabelError,
}, initialState);
