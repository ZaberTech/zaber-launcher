import type { FirmwareVersion } from '@zaber/motion';
import { changeCollection, changeDictionary } from '@zaber/toolbox';
import _ from 'lodash';

import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes, DevicesLoadedPayload } from '../connection_manager';
import { EntityKey, EntityKeyType, extractConnectionKey, extractDeviceKey, getEntityType } from '../keys';
import type { Measurement } from '../units';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import {
  addChannelAndDependents,
  channelSorter,
  extractSettingsChannels,
  channelHasDependency,
  isMathChannelId,
  fillInMathAllChannelSamples,
  removeChannelAndDependents,
  addOrReplaceChannelAndUpdateDependents,
  extractAxisChannels,
  isIoChannel,
  extractIoChannels,
  isMathChannel,
} from './channels';
import {
  Channel,
  ControlTabId,
  Mode,
  SignalType,
  NEW_RECORDING_TAB,
  UnitMap,
  Sample,
  UnitConversionCache,
  MAX_DEVICE_CHANNELS,
} from './types';
import { getDifferencingRange, isNewRecTab } from './utils';


export interface SignalGeneratorSettings {
  signalAxis: number;
  signalReturnToStart: boolean;
  signalType: SignalType;
  signalValue: Measurement;
}


export const SIGNAL_GENERATOR_DEFAULTS: SignalGeneratorSettings = {
  signalAxis: 1,
  signalReturnToStart: true,
  signalType: SignalType.NONE,
  signalValue: { value: 0, units: 'default' },
};


export interface CaptureSettings {
  mode: Mode;
  sampleRates: Record<Mode, number>;
  startDelay: number; // ms
  recordingDelay: number; // ms
  samplesToCapture: number | null;
  cDiffSpan: number; // ms
  externalWaitForScopePrint: boolean;
}


export const CAPTURE_DEFAULTS: CaptureSettings = {
  mode: Mode.POLLED,
  sampleRates: {
    [Mode.POLLED]: 100,
    [Mode.BUFFERED_MANUAL]: 10000,
    [Mode.EXTERNAL]: 10000,
    [Mode.DOWNLOAD]: 10000,
  },
  startDelay: 0,
  recordingDelay: 0,
  samplesToCapture: null,
  cDiffSpan: 0.1,
  externalWaitForScopePrint: true,
};


export interface ChannelGroup {
  channelIds: string[];
}


export type Recording = SignalGeneratorSettings & CaptureSettings & {
  title: string;
  id: number; // Unique, whereas recording index can be reused.
  deviceName: string;
  axisNames: string[];
  deviceKey: EntityKey;
  deviceId: number;
  peripheralIds: number[];
  actualSampleRate: number | null;

  // Channels are always stored in the order: Settings in user-selected order, then math channels
  // in dependency order and increasing number of dependencies. Channels with the same name are
  // sorted in increasing order of axis number.
  // The array always stores all math channels that can be derived from the selected settings; there
  // is a boolean property that controls whether they are shown in the UI that serves as "delete".
  // Settings channels actually get deleted, and their dependents are really deleted then.
  channels: Channel[];

  // Grouped data for Recharts display.
  groups: ChannelGroup[];

  // Autoranged units for charts. Keys are channel IDs.
  unitMap: UnitMap;
};


export type PerAxisState = {
  name: string;
  peripheralId: number;
  settingNames: string[]; // Cache of all selectable settings on the axis.
  unitConversionCache: UnitConversionCache;
  channels: Channel[];
};


export const DEFAULT_AXIS_STATE: PerAxisState = {
  name: '',
  peripheralId: 0,
  settingNames: [],
  unitConversionCache: {
    baseDimensionScales: {},
    contextualDimensionScales: {},
    settingContextualDimensions: {},
  },
  channels: [],
};


export type PerDeviceState = SignalGeneratorSettings & CaptureSettings & {
  name: string;
  fwVersion: FirmwareVersion;
  deviceId: number;
  maxChannels: number;
  fwBufferSize: number;
  bufferReshapable: boolean;
  ioPins: {
    ai: number;
    ao: number;
    di: number;
    do: number;
    total: number;
  };
  selectedControlTab: ControlTabId;
  axes: PerAxisState[]; // [0] is used for device-level channels.
};


export const PER_DEVICE_DEFAULTS: PerDeviceState = {
  ..._.cloneDeep(CAPTURE_DEFAULTS),
  ..._.cloneDeep(SIGNAL_GENERATOR_DEFAULTS),
  name: '',
  fwVersion: { major: 0, minor: 1, build: 0 },
  deviceId: 0,
  maxChannels: MAX_DEVICE_CHANNELS,
  fwBufferSize: MAX_DEVICE_CHANNELS * 1024,
  bufferReshapable: false,
  ioPins: {
    ai: 0,
    ao: 0,
    di: 0,
    do: 0,
    total: 0,
  },
  selectedControlTab: ControlTabId.CHANNELS,
  axes: [],
};


type DeviceStateMap = Record<string, PerDeviceState>;


export interface State {
  connectionManagerSelectedKey: EntityKey | null;
  selectedDeviceKey: EntityKey | null;

  perDeviceSettings: DeviceStateMap;

  addingNewSetting: boolean;
  addingNewMath: boolean;
  addingNewIo: boolean;
  shareChannelSettings: boolean;
  controlsVisible: boolean;

  activeRecording: Recording | null;
  replacing: boolean;
  progress: number;
  progressMessage: string;
  showProgressBar: boolean;
  stopButtonEnabled: boolean;
  recordings: Recording[];
  nextRecordingNumber: number;
  selectedRecordingIndex: number;
  unitConversionEnabled: boolean;

  errorMessage: string | null;
}


const initialState: State = {
  connectionManagerSelectedKey: null,
  selectedDeviceKey: null,
  perDeviceSettings: {},

  addingNewSetting: false,
  addingNewMath: false,
  addingNewIo: false,
  shareChannelSettings: false,
  controlsVisible: true,

  activeRecording: null,
  replacing: false,
  progress: 0,
  progressMessage: '',
  showProgressBar: false,
  stopButtonEnabled: true,
  recordings: [],
  nextRecordingNumber: 1,
  selectedRecordingIndex: NEW_RECORDING_TAB,
  unitConversionEnabled: true,

  errorMessage: null,
};


const changeDeviceSettings =
  (deviceMap: DeviceStateMap, deviceKey: EntityKey | null, change: (state: PerDeviceState) => PerDeviceState) =>
    (deviceKey ? changeDictionary(deviceMap, deviceKey, change) : deviceMap);


function changeArray<T>(array: T[], index: number, transform: ((item?: T) => T)): T[] {
  let result = [...array];
  if (index >= result.length) {
    result = result.concat(Array(result.length - index));
    result.push(transform());
  } else {
    result[index] = transform(result[index]);
  }

  return result;
}


const selectDevice = (state: State, { key }: ActionsToPayloads[ActionTypes.ON_DEVICE_SELECTED]): State => {
  let deviceKey = key;
  if (key != null) {
    const keyType = getEntityType(key);
    if (keyType === EntityKeyType.AXIS) {
      deviceKey = extractDeviceKey(key);
    }
  }

  return {
    ...state,
    perDeviceSettings: deviceKey === null || deviceKey in state.perDeviceSettings
      ? state.perDeviceSettings
      : {
        ...state.perDeviceSettings,
        [deviceKey]: PER_DEVICE_DEFAULTS,
      },
    connectionManagerSelectedKey: key,
    selectedDeviceKey: deviceKey,
    addingNewSetting: false,
    addingNewMath: false,
  };
};


const devicesLoaded = (state: State, { connectionKey }: DevicesLoadedPayload): State => ({
  ...state,
  perDeviceSettings: _.omitBy(state.perDeviceSettings, (_, key) => extractConnectionKey(key) === connectionKey),
  addingNewSetting: false,
  selectedDeviceKey: null,
});


const storeDeviceIdentity = (state: State, { deviceKey, name, deviceId }: ActionsToPayloads[ActionTypes.STORE_DEVICE_IDENTITY]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, deviceKey, deviceState => ({
    ...deviceState,
    name,
    deviceId,
  })),
});


const storeAxisIdentity =
  (state: State, { deviceKey, axis, name, peripheralId }: ActionsToPayloads[ActionTypes.STORE_AXIS_IDENTITY]): State => ({
    ...state,
    perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, deviceKey, deviceState => ({
      ...deviceState,
      axes: changeArray(deviceState.axes, axis, () => ({
        ...DEFAULT_AXIS_STATE,
        name,
        peripheralId,
      })),
    })),
  });


const storeSelectableSettingNames =
  (state: State, { deviceKey, axis, names }: ActionsToPayloads[ActionTypes.STORE_SELECTABLE_SETTING_NAMES]): State => ({
    ...state,
    perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, deviceKey, deviceState => ({
      ...deviceState,
      axes: changeArray(deviceState.axes, axis, axisState => ({
        ...axisState!,
        settingNames: names,
      })),
    })),
  });


const storeUnitConversions =
  (state: State, { deviceKey, axis, unitConversions }: ActionsToPayloads[ActionTypes.STORE_UNIT_CONVERSIONS]): State => ({
    ...state,
    perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, deviceKey, deviceState => ({
      ...deviceState,
      axes: changeArray(deviceState.axes, axis, axisState => ({
        ...axisState!,
        unitConversionCache: unitConversions,
      })),
    })),
  });


const storeScopeBufferInfo = (
  state: State,
  { deviceKey, maxChannels, bufferSize, reshapable }: ActionsToPayloads[ActionTypes.STORE_SCOPE_BUFFER_INFO]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, deviceKey, axisState => ({
    ...axisState,
    maxChannels,
    fwBufferSize: bufferSize,
    bufferReshapable: reshapable,
  })),
});


const storeDeviceIoInfo = (
  state: State, { deviceKey, fwVersion, io }: ActionsToPayloads[ActionTypes.STORE_DEVICE_IO_INFO]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, deviceKey, axisState => ({
    ...axisState,
    fwVersion,
    ioPins: {
      ai: io.numberAnalogInputs,
      ao: io.numberAnalogOutputs,
      di: io.numberDigitalInputs,
      do: io.numberDigitalOutputs,
      total: io.numberAnalogInputs + io.numberAnalogOutputs + io.numberDigitalInputs + io.numberDigitalOutputs,
    },
  })),
});


const selectControlTab = (state: State, { tab }: ActionsToPayloads[ActionTypes.SELECT_CONTROL_TAB]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
    ...deviceState,
    selectedControlTab: tab,
  })),
  addingNewMath: false,
  addingNewSetting: false,
});


const selectRecording = (state: State, { index }: ActionsToPayloads[ActionTypes.SELECT_RECORDING_TAB]): State => ({
  ...state,
  selectedRecordingIndex: Math.min(index, state.recordings.length - 1),
  addingNewMath: false,
  addingNewSetting: false,
});


const showAddSettingDialog = (state: State): State => ({
  ...state,
  addingNewMath: false,
  addingNewSetting: true,
  addingNewIo: false,
});


const closeAddSettingDialog = (state: State, { newSettings }: ActionsToPayloads[ActionTypes.CLOSE_ADD_SETTING_DIALOG]): State => {
  // Changing the list of settings is only allowed on the New Recording tab.
  if (!isNewRecTab(state.selectedRecordingIndex) || !newSettings) {
    return {
      ...state,
      addingNewSetting: false,
    };
  }

  const updateChannels = (oldChannels: Channel[], axis: number): Channel[] => {
    const currentlyEnabled = new Set<string>(newSettings.map(ch => ch.id));
    const settingsToRemove = extractSettingsChannels(oldChannels).filter(ch => !(currentlyEnabled.has(ch.id))).map(ch => ch.id);
    let channels = [...oldChannels];
    for (const setting of settingsToRemove) {
      channels = removeChannelAndDependents(channels, setting);
    }

    for (const newChannel of newSettings.filter(ch => ch.axis === axis)) {
      channels = addOrReplaceChannelAndUpdateDependents(channels, newChannel);
    }

    return channels;
  };

  return {
    ...state,
    perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
      ...deviceState,
      axes: deviceState.axes.map((axisState, i) => ({
        ...axisState,
        channels: updateChannels(axisState.channels, i),
      })),
    })),
    addingNewSetting: false,
  };
};


const showAddMathDialog = (state: State): State => ({
  ...state,
  addingNewMath: true,
  addingNewSetting: false,
  addingNewIo: false,
});


const closeAddMathDialog = (state: State, { newMath }: ActionsToPayloads[ActionTypes.CLOSE_ADD_MATH_DIALOG]): State => {
  // If the new recording tab is selected, save the new math channel selections on the selected axis.
  if (isNewRecTab(state.selectedRecordingIndex)) {
    return {
      ...state,
      perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
        ...deviceState,
        axes: deviceState.axes.map((axisState, i) => ({
          ...axisState,
          channels: newMath
            ? [...extractSettingsChannels(axisState.channels),
              ...extractAxisChannels(newMath, i).sort(channelSorter),
              ...extractIoChannels(axisState.channels)]
            : axisState.channels,
        })),
      })),
      addingNewMath: false,
    };

  // If a previous recording is selected, change the math channel selection either in
  // that recording or in all previous recordings that it's applicable to.
  } else if (newMath) {
    const allNewMath = _.keyBy(newMath, 'id');
    const recordings = state.recordings.map((rec, index) => ({
      ...rec,
      channels: fillInMathAllChannelSamples(rec.channels.map(ch => ({
        ...ch,
        color: (state.shareChannelSettings || index === state.selectedRecordingIndex) &&
          ch.id in allNewMath ? allNewMath[ch.id].color : ch.color,
        showInUi: (state.shareChannelSettings || index === state.selectedRecordingIndex) &&
          ch.id in allNewMath ? allNewMath[ch.id].showInUi : ch.showInUi,
      })), rec.cDiffSpan),
    }));

    return {
      ...state,
      recordings,
      addingNewMath: false,
    };

  // Otherwise do nothing and close the dialog.
  } else {
    return {
      ...state,
      addingNewMath: false,
    };
  }
};


const showAddIoDialog = (state: State): State => ({
  ...state,
  addingNewMath: false,
  addingNewSetting: false,
  addingNewIo: true,
});


const closeAddIoDialog = (state: State, { newIo }: ActionsToPayloads[ActionTypes.CLOSE_ADD_IO_DIALOG]): State => {
  // Changing the list of I/O channels is only allowed on the New Recording tab.
  if (!isNewRecTab(state.selectedRecordingIndex) || !newIo) {
    return {
      ...state,
      addingNewIo: false,
    };
  }

  return {
    ...state,
    perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
      ...deviceState,
      axes: deviceState.axes.map((axisState, i) => ({
        ...axisState,
        // I/O channels are always on the 0th axis (the device itself), and have no connections to other channels (yet).
        channels: i === 0 ? axisState.channels.filter(ch => !isIoChannel(ch)).concat(newIo) : axisState.channels,
      })),
    })),
    addingNewIo: false,
  };
};


const setControlPanelVisibility = (state: State, { visible }: ActionsToPayloads[ActionTypes.SET_CONTROL_PANEL_VISIBILITY]): State => ({
  ...state,
  controlsVisible: visible,
  addingNewSetting: false,
});


const setStopButtonEnabled = (state: State, { enabled }: ActionsToPayloads[ActionTypes.SET_STOP_BUTTON_ENABLED]): State => ({
  ...state,
  stopButtonEnabled: enabled,
});


const setUnitConversionEnabled = (state: State, { enabled }: ActionsToPayloads[ActionTypes.SET_UNIT_CONVERSION_ENABLED]): State => ({
  ...state,
  unitConversionEnabled: enabled,
});


const setCaptureMode = (state: State, { mode }: ActionsToPayloads[ActionTypes.SET_CAPTURE_MODE]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => {
    const [min, max] = getDifferencingRange(deviceState.sampleRates[mode]);
    return {
      ...deviceState,
      mode,
      cDiffSpan: Math.max(min, Math.min(max, deviceState.cDiffSpan)),
    };
  }),
});


const setTargetSampleRate = (state: State, { sampleRate }: ActionsToPayloads[ActionTypes.SET_TARGET_SAMPLE_RATE]): State => {
  const [min, max] = getDifferencingRange(sampleRate);
  return {
    ...state,
    perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
      ...deviceState,
      sampleRates: {
        ...deviceState.sampleRates,
        [deviceState.mode]: sampleRate,
      },
      cDiffSpan: Math.max(min, Math.min(max, deviceState.cDiffSpan)),
    })),
  };
};


const setDifferencingSpan = (state: State, { span }: ActionsToPayloads[ActionTypes.SET_DIFFERENCING_SPAN]): State => {
  const tab = state.selectedRecordingIndex;
  const perDeviceSettings = isNewRecTab(tab) ?
    changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
      ...deviceState,
      cDiffSpan: span,
    })) : state.perDeviceSettings;

  const recordings = isNewRecTab(tab) ?
    state.recordings :
    changeArray(state.recordings, tab, rec => ({
      ...rec!,
      cDiffSpan: span,
    }));

  return {
    ...state,
    perDeviceSettings,
    recordings,
  };
};


const addChannel = (state: State, { deviceKey, axis, settingName }: ActionsToPayloads[ActionTypes.ADD_CHANNEL]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, deviceKey, deviceState => ({
    ...deviceState,
    axes: changeArray(deviceState.axes, axis, axisState => ({
      ...axisState!,
      channels: addChannelAndDependents(axisState!.channels, settingName, axis),
    })),
  })),
});


const deleteChannel = (state: State, { id }: ActionsToPayloads[ActionTypes.DELETE_CHANNEL]): State => {
  // If the new recording tab is selected, delete the channel from the selected axis.
  if (isNewRecTab(state.selectedRecordingIndex)) {
    return {
      ...state,
      addingNewSetting: false,
      perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
        ...deviceState,
        axes: deviceState.axes.map(axisState => {
          let channels = axisState.channels;
          const channelToDelete = channels.find(ch => ch.id === id);
          if (channelToDelete) {
            if (isMathChannel(channelToDelete)) {
              // Math channel - just turn off showing it in the UI because it might still be needed as an intermediate result.
              channels = changeCollection(axisState.channels, 'id', id, channel => ({
                ...channel,
                showInUi: false,
              }));
            } else { // Non-math channel - remove it and all its dependents.
              const channelMap = _.keyBy(channels, 'id');
              channels = channels.filter(ch => ch !== channelToDelete && !channelHasDependency(ch, id, channelMap));
            }
          }

          return {
            ...axisState,
            channels,
          };
        }),
      })),
    };

  // If a previous recording is selected, only disabling math channels is allowed.
  } else if (isMathChannelId(id)) {
    return {
      ...state,
      addingNewSetting: false,
      recordings: state.recordings.map((rec, index) => (
        state.shareChannelSettings || (index === state.selectedRecordingIndex)
          ? {
            ...rec,
            channels: changeCollection(rec.channels, 'id', id, ch => ({
              ...ch,
              showInUi: false,
            })),
          }
          : rec)),
    };

  // Otherwise do nothing.
  } else {
    return state;
  }
};


const setChannelInCharts = (state: State,
  { recordingIndex, channelId, visible }: ActionsToPayloads[ActionTypes.SET_CHANNEL_CHART_VISIBILITY]): State => {
  // If a previous recording is selected, change the channel's visibility in either the selected
  // recording or in all previous recordings.
  let recordings = state.recordings;
  if (!isNewRecTab(recordingIndex)) {
    recordings = state.recordings.map((rec, i) => ({
      ...rec,
      channels: rec.channels.map(ch => ((state.shareChannelSettings || i === recordingIndex) && ch.id === channelId
        ? {
          ...ch,
          showInChart: visible,
        } : ch)),
    }));
  }

  // If the new recording tab is selected, change the channel visibility in the axis settings.
  const perDeviceSettings = isNewRecTab(recordingIndex)
    ? changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
      ...deviceState,
      axes: deviceState.axes.map(axisState => ({
        ...axisState,
        channels: changeCollection(axisState.channels, 'id', channelId, ch => ({
          ...ch,
          showInChart: visible,
        })),
      })),
    }))
    : state.perDeviceSettings;

  return {
    ...state,
    recordings,
    perDeviceSettings,
  };
};


const setChannelColor = (state: State, { recordingIndex, channelId, color }: ActionsToPayloads[ActionTypes.SET_CHANNEL_COLOR]): State => {
  // If a previous recording is selected, change the channel's color in either the selected
  // recording or in all previous recordings.
  let recordings = state.recordings;
  if (!isNewRecTab(recordingIndex)) {
    recordings = state.recordings.map((rec, i) => ({
      ...rec,
      channels: rec.channels.map(ch => ((state.shareChannelSettings || i === recordingIndex) && ch.id === channelId
        ? {
          ...ch,
          color,
        } : ch)),
    }));
  }

  // If the new recording tab is selected, change the channel color in the axis settings.
  const perDeviceSettings = isNewRecTab(recordingIndex)
    ? changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
      ...deviceState,
      axes: deviceState.axes.map(axisState => ({
        ...axisState,
        channels: changeCollection(axisState.channels, 'id', channelId, ch => ({
          ...ch,
          color,
        }))
      })),
    }))
    : state.perDeviceSettings;

  return {
    ...state,
    recordings,
    perDeviceSettings,
  };
};


const setYAxis = (state: State, { recordingIndex, channelId, axis }: ActionsToPayloads[ActionTypes.SET_Y_AXIS]): State => {
  let recordings = state.recordings;
  // If a previous recording is selected, change the channel's axis in either the selected
  // recording or in all previous recordings.
  if (!isNewRecTab(recordingIndex)) {
    recordings = state.recordings.map((rec, i) => ({
      ...rec,
      channels: rec.channels.map(ch => ((state.shareChannelSettings || i === recordingIndex) && ch.id === channelId
        ? {
          ...ch,
          yAxis: axis,
        } : ch)),
    }));
  }

  // If the new recording tab is selected, change the channel axis in the axis settings.
  const perDeviceSettings = isNewRecTab(recordingIndex)
    ? changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
      ...deviceState,
      axes: deviceState.axes.map(axisState => ({
        ...axisState,
        channels: changeCollection(axisState.channels, 'id', channelId, ch => ({
          ...ch,
          yAxis: axis,
        })),
      })),
    }))
    : state.perDeviceSettings;

  return {
    ...state,
    recordings,
    perDeviceSettings,
  };
};


const setSignalAxis = (state: State, { axis }: ActionsToPayloads[ActionTypes.SET_SIGNAL_AXIS]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
    ...deviceState,
    signalAxis: axis,
  })),
});


const setSignalType = (state: State, { type }: ActionsToPayloads[ActionTypes.SET_SIGNAL_TYPE]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
    ...deviceState,
    signalType: type,
  })),
});


const setSignalValue = (state: State, { value }: ActionsToPayloads[ActionTypes.SET_SIGNAL_VALUE]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
    ...deviceState,
    signalValue: value,
  })),
});


const setSignalReturnToStart = (state: State, { returnToStart }: ActionsToPayloads[ActionTypes.SET_SIGNAL_RETURN_TO_START]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
    ...deviceState,
    signalReturnToStart: returnToStart,
  })),
});


const setCaptureDelays = (state: State, { startDelay, recordingDelay }: ActionsToPayloads[ActionTypes.SET_CAPTURE_DELAYS]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
    ...deviceState,
    startDelay: startDelay ?? deviceState.startDelay,
    recordingDelay: recordingDelay ?? deviceState.recordingDelay,
  })),
});


const setShareChannelSettings = (state: State, { share }: ActionsToPayloads[ActionTypes.SET_SHARED_CHANNEL_SETTINGS]): State => {
  let recordings = state.recordings;
  // If we're turning on setting sharing between recordings and currently have
  // an existing recording selected, copy its settings to all other recordings.
  if (share && !isNewRecTab(state.selectedRecordingIndex)) {
    const selectedRecording = state.recordings[state.selectedRecordingIndex];
    const selectedChannels = _.keyBy(selectedRecording.channels, 'id');
    recordings = state.recordings.map(rec => ({
      ...rec,
      channels: fillInMathAllChannelSamples(rec.channels.map(ch => (
        (ch.id in selectedChannels)
          ? {
            ...ch,
            showInChart: selectedChannels[ch.id].showInChart,
            showInUi: selectedChannels[ch.id].showInUi,
            color: selectedChannels[ch.id].color,
            yAxis: selectedChannels[ch.id].yAxis,
          }
          : ch
      )), rec.cDiffSpan),
    }));
  }

  return {
    ...state,
    shareChannelSettings: share,
    recordings,
    addingNewSetting: false,
  };
};


const setTitle = (state: State, { recordingIndex, title }: ActionsToPayloads[ActionTypes.SET_RECORDING_TITLE]): State => ({
  ...state,
  recordings: state.recordings.map((rec, i) => ({
    ...rec,
    title: i === recordingIndex ? title : rec.title,
  })),
});


const setExternalWaitForPrint = (state: State, { wait }: ActionsToPayloads[ActionTypes.SET_EXTERNAL_WAIT_FOR_PRINT]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
    ...deviceState,
    externalWaitForScopePrint: wait,
  })),
});


const setSampleLimit = (state: State, { numSamples }: ActionsToPayloads[ActionTypes.SET_SAMPLE_LIMIT]): State => ({
  ...state,
  perDeviceSettings: changeDeviceSettings(state.perDeviceSettings, state.selectedDeviceKey, deviceState => ({
    ...deviceState,
    samplesToCapture: numSamples,
  })),
});


const startRecording = (state: State): State => ({
  ...state,
  activeRecording: createBlankRecording(state),
});


function createBlankRecording(state: State): Recording {
  const deviceKey = state.selectedDeviceKey;
  if (!deviceKey) {
    throw new Error('Cannot start recording without a selected device or axis.');
  }

  const deviceState = state.perDeviceSettings[deviceKey];

  return {
    title: `Recording ${state.nextRecordingNumber.toString()}`,
    id: state.nextRecordingNumber,
    deviceName: deviceState.name,
    axisNames: deviceState.axes.map(axis => axis.name),
    deviceId: deviceState.deviceId,
    peripheralIds: deviceState.axes.map(axis => axis.peripheralId),
    deviceKey: deviceKey!,
    actualSampleRate: null,
    channels: _.flatten(deviceState.axes.map(axisState => _.cloneDeep(axisState.channels))).sort(channelSorter),
    ..._.cloneDeep(_.pick(deviceState, Object.keys(SIGNAL_GENERATOR_DEFAULTS)) as SignalGeneratorSettings),
    ..._.cloneDeep(_.pick(deviceState, Object.keys(CAPTURE_DEFAULTS)) as CaptureSettings),
    groups: [],
    unitMap: {},
  };
}


const copyRecordingToActive = (state: State, recordingIndex: number): State => ({
  ...state,
  selectedRecordingIndex: NEW_RECORDING_TAB,
  controlsVisible: true,
  activeRecording: {
    ..._.cloneDeep(state.recordings[recordingIndex]),
    title: `Recording ${state.nextRecordingNumber.toString()}`,
    id: state.nextRecordingNumber,
    actualSampleRate: null,
    channels: state.recordings[recordingIndex].channels.map(ch => ({
      ...ch,
      samples: [],
    })),
    groups: [],
    unitMap: {},
  },
});


const repeatRecording = (state: State, { recordingIndex }: ActionsToPayloads[ActionTypes.REPEAT]): State =>
  copyRecordingToActive(state, recordingIndex);


const replaceRecording = (state: State, { recordingIndex }: ActionsToPayloads[ActionTypes.REPLACE]): State => ({
  ...copyRecordingToActive(state, recordingIndex),
  replacing: true,
});


const prepareForDownload = (state: State, { recordingIndex }: ActionsToPayloads[ActionTypes.DOWNLOAD_EXISTING_DATA]): State => {
  const replacing = recordingIndex != null;

  if (replacing) {
    return {
      ...state,
      ...copyRecordingToActive(state, recordingIndex),
      replacing: true,
    };
  } else {
    return {
      ...state,
      activeRecording: {
        ...createBlankRecording(state),
      },
    };
  }
};


const reportProgress = (state: State, { progress, message, showBar }: ActionsToPayloads[ActionTypes.REPORT_PROGRESS]): State => ({
  ...state,
  progress,
  progressMessage: message,
  showProgressBar: showBar,
});


const storeChannelData = (state: State, { newSamples, recordingIndex }: ActionsToPayloads[ActionTypes.STORE_CHANNEL_DATA]): State => {
  if (isNewRecTab(recordingIndex ?? NEW_RECORDING_TAB)) {
    return {
      ...state,
      activeRecording: {
        ...state.activeRecording!,
        channels: state.activeRecording!.channels.map(c => ({
          ...c,
          samples: (c.id in newSamples) ? newSamples[c.id].toArray() as Sample[] : c.samples,
        })),
      }
    };
  }

  const newRecordings = [...state.recordings];
  const oldRecording = state.recordings[recordingIndex!];
  newRecordings[recordingIndex!] = {
    ...oldRecording,
    channels: oldRecording.channels.map(c => ({
      ...c,
      samples: (c.id in newSamples)
        ? newSamples[c.id].toArray() as Sample[]
        : c.samples,
    })),
  };

  return {
    ...state,
    recordings: newRecordings,
  };
};


const replaceChannelData = (state: State, { newChannels, recordingIndex }: ActionsToPayloads[ActionTypes.REPLACE_CHANNELS]): State => {
  if (isNewRecTab(recordingIndex ?? NEW_RECORDING_TAB)) {
    return {
      ...state,
      activeRecording: {
        ...state.activeRecording!,
        channels: newChannels,
      },
    };
  }

  const newRecordings = [...state.recordings];
  const oldRecording = state.recordings[recordingIndex!];
  newRecordings[recordingIndex!] = {
    ...oldRecording,
    channels: newChannels,
  };

  return {
    ...state,
    recordings: newRecordings,
  };
};


const storeChartData = (state: State, { recordingIndex, groups, units, data }: ActionsToPayloads[ActionTypes.STORE_CHART_DATA]): State => {
  if (isNewRecTab(recordingIndex)) {
    return {
      ...state,
      activeRecording: {
        ...state.activeRecording!,
        groups,
        unitMap: units,
        channels: state.activeRecording!.channels.map(ch => ({
          ...ch,
          convertedData: data[ch.id] ?? ch.convertedData,
        })),
      }
    };
  }

  return {
    ...state,
    recordings: state.recordings.map((rec, i) => (i === recordingIndex
      ? {
        ...rec,
        groups,
        unitMap: units,
        channels: rec.channels.map(ch => ({
          ...ch,
          convertedData: data[ch.id] ?? ch.convertedData,
        })),
      } : rec
    )),
  };
};


const storeActualSampleRate = (state: State, { rate }: ActionsToPayloads[ActionTypes.STORE_ACTUAL_SAMPLE_RATE]): State => ({
  ...state,
  activeRecording: state.activeRecording ? {
    ...state.activeRecording,
    actualSampleRate: rate,
  } : state.activeRecording,
});


const storeExternalSampleSettings =
  (state: State, { sampleRate, startDelay }: ActionsToPayloads[ActionTypes.STORE_EXTERNAL_SAMPLE_SETTINGS]): State => ({
    ...state,
    activeRecording: state.activeRecording ? {
      ...state.activeRecording,
      sampleRates: {
        ...state.activeRecording.sampleRates,
        [state.activeRecording.mode]: sampleRate,
      },
      startDelay,
    } : state.activeRecording,
  });


const saveRecording = (state: State, { recordingIndex }: ActionsToPayloads[ActionTypes.SAVE_RECORDING]): State => {
  const newState = {
    ...state,
    activeRecording: null,
    nextRecordingNumber: state.nextRecordingNumber + 1,
    replacing: false,
  };

  if (recordingIndex === undefined) {
    newState.recordings = [...state.recordings, state.activeRecording!];
    newState.selectedRecordingIndex = state.recordings.length;
  } else {
    newState.recordings = changeArray(state.recordings, recordingIndex, () => state.activeRecording!);
    newState.selectedRecordingIndex = recordingIndex;
  }

  return newState;
};


const discardRecording = (state: State): State => ({
  ...state,
  activeRecording: null,
  replacing: false,
});


const deleteRecording = (state: State, { index }: ActionsToPayloads[ActionTypes.DELETE_RECORDING]): State => ({
  ...state,
  recordings: [...state.recordings.slice(0, index), ...state.recordings.slice(index + 1)],
  selectedRecordingIndex: Math.min(state.selectedRecordingIndex, state.recordings.length - 2),
});


const deleteAllRecordings = (state: State): State => ({
  ...state,
  recordings: [],
  selectedRecordingIndex: NEW_RECORDING_TAB,
});


const setError = (state: State, { message }: ActionsToPayloads[ActionTypes.SET_ERROR]): State => ({
  ...state,
  errorMessage: message,
});


export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.ON_DEVICE_SELECTED]: selectDevice,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
  [ActionTypes.STORE_DEVICE_IDENTITY]: storeDeviceIdentity,
  [ActionTypes.STORE_AXIS_IDENTITY]: storeAxisIdentity,
  [ActionTypes.STORE_SELECTABLE_SETTING_NAMES]: storeSelectableSettingNames,
  [ActionTypes.STORE_UNIT_CONVERSIONS]: storeUnitConversions,
  [ActionTypes.STORE_SCOPE_BUFFER_INFO]: storeScopeBufferInfo,
  [ActionTypes.STORE_DEVICE_IO_INFO]: storeDeviceIoInfo,

  [ActionTypes.SELECT_CONTROL_TAB]: selectControlTab,
  [ActionTypes.SELECT_RECORDING_TAB]: selectRecording,
  [ActionTypes.SHOW_ADD_SETTING_DIALOG]: showAddSettingDialog,
  [ActionTypes.CLOSE_ADD_SETTING_DIALOG]: closeAddSettingDialog,
  [ActionTypes.SHOW_ADD_MATH_DIALOG]: showAddMathDialog,
  [ActionTypes.CLOSE_ADD_MATH_DIALOG]: closeAddMathDialog,
  [ActionTypes.SHOW_ADD_IO_DIALOG]: showAddIoDialog,
  [ActionTypes.CLOSE_ADD_IO_DIALOG]: closeAddIoDialog,
  [ActionTypes.SET_CONTROL_PANEL_VISIBILITY]: setControlPanelVisibility,
  [ActionTypes.SET_STOP_BUTTON_ENABLED]: setStopButtonEnabled,
  [ActionTypes.SET_UNIT_CONVERSION_ENABLED]: setUnitConversionEnabled,

  [ActionTypes.SET_CAPTURE_MODE]: setCaptureMode,
  [ActionTypes.SET_TARGET_SAMPLE_RATE]: setTargetSampleRate,
  [ActionTypes.SET_DIFFERENCING_SPAN]: setDifferencingSpan,
  [ActionTypes.ADD_CHANNEL]: addChannel,
  [ActionTypes.DELETE_CHANNEL]: deleteChannel,
  [ActionTypes.SET_CHANNEL_CHART_VISIBILITY]: setChannelInCharts,
  [ActionTypes.SET_CHANNEL_COLOR]: setChannelColor,
  [ActionTypes.SET_Y_AXIS]: setYAxis,
  [ActionTypes.SET_SIGNAL_AXIS]: setSignalAxis,
  [ActionTypes.SET_SIGNAL_TYPE]: setSignalType,
  [ActionTypes.SET_SIGNAL_VALUE]: setSignalValue,
  [ActionTypes.SET_SIGNAL_RETURN_TO_START]: setSignalReturnToStart,
  [ActionTypes.SET_CAPTURE_DELAYS]: setCaptureDelays,
  [ActionTypes.SET_SHARED_CHANNEL_SETTINGS]: setShareChannelSettings,
  [ActionTypes.SET_RECORDING_TITLE]: setTitle,
  [ActionTypes.SET_EXTERNAL_WAIT_FOR_PRINT]: setExternalWaitForPrint,
  [ActionTypes.SET_SAMPLE_LIMIT]: setSampleLimit,

  [ActionTypes.START]: startRecording,
  [ActionTypes.REPEAT]: repeatRecording,
  [ActionTypes.REPLACE]: replaceRecording,
  [ActionTypes.DOWNLOAD_EXISTING_DATA]: prepareForDownload,
  [ActionTypes.REPORT_PROGRESS]: reportProgress,
  [ActionTypes.STORE_CHANNEL_DATA]: storeChannelData,
  [ActionTypes.REPLACE_CHANNELS]: replaceChannelData,
  [ActionTypes.STORE_CHART_DATA]: storeChartData,
  [ActionTypes.STORE_ACTUAL_SAMPLE_RATE]: storeActualSampleRate,
  [ActionTypes.STORE_EXTERNAL_SAMPLE_SETTINGS]: storeExternalSampleSettings,
  [ActionTypes.SAVE_RECORDING]: saveRecording,
  [ActionTypes.DISCARD_ACTIVE_RECORDING]: discardRecording,
  [ActionTypes.DELETE_RECORDING]: deleteRecording,
  [ActionTypes.DELETE_ALL_RECORDINGS]: deleteAllRecordings,
  [ActionTypes.SET_ERROR]: setError,
}, initialState);
