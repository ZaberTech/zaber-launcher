import React from 'react';
import { Card, Icons, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectCounts } from './selectors';
import { Settings } from './Settings';

export const All: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const counts = useSelector(selectCounts);

  return (<Card className="all">
    <Text t={Text.Type.H5}>All Axes</Text>&emsp;
    {counts.running > 0 && <>(Running {counts.running})&emsp;</>}
    {counts.stopped > 0 && <>(Stopped {counts.stopped})&emsp;</>}

    <div className="spacer"/>
    <Icons.RightNormal title="Start All Axes" onClick={actions.startAll}/>
    <Icons.Stop title="Stop All Axes" onClick={actions.stopAll}/>
    <Settings/>
  </Card>);
};
