import path from 'path';
import os from 'os';

import { app } from 'electron';

export function setPaths(): void {
  if (os.platform() === 'win32') {
    // default is Roaming app data which is not suitable for our application
    const appData = process.env.LOCALAPPDATA;
    if (!appData) {
      throw new Error('LOCALAPPDATA not defined');
    }
    app.setPath('appData', appData);
    app.setPath('userData', path.join(appData, app.getName()));
  }
}
