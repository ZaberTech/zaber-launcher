
import os from 'os';

import type { SagaIterator } from 'redux-saga';
import { all, call, put, takeEvery, takeLatest, fork } from 'redux-saga/effects';

import { environment } from '../environment';
import { connectionManagerActions } from '../connection_manager';
import { getContainer } from '../container';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import { Action, AsyncReturnType, notNil, throwUnexpectedError } from '../utils';
import { ipcActions } from '../ipc';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import { PreferencesService } from './service';

export function* rootSaga(): SagaIterator {
  yield all([
    !environment.isTest ? fork(initializeService) : null,
    takeLatest(ActionTypes.SET_NAME, setName),
    takeEvery(ActionTypes.SET_PREFERENCE, setPreference),
  ].filter(notNil));
}

export function* initializeService(): SagaIterator {
  const service = getContainer().get(PreferencesService);
  const preferences = service.retrieveInitialValues();

  for (const [key, value] of Object.entries(preferences)) {
    yield put(actions.setPreference(key, value, false));
  }
}

function* setName({ payload: { name } }: Action<ActionsToPayloads[ActionTypes.SET_NAME]>): SagaIterator {
  try {
    if (!name) {
      name = os.hostname();
    }

    const service = getContainer().get(MessageRoutersService);
    const { api }: AsyncReturnType<typeof service.getConnection> = yield call([service, service.getConnection], LOCAL_ROUTER_URL);
    yield call([api, api.setName], name);

    yield put(actions.setNameDone());
    yield put(connectionManagerActions.updateLocalRouterName(name));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setNameDone(err.message || String(err)));
  }
}

type SetPreferenceAction = Action<ActionsToPayloads[ActionTypes.SET_PREFERENCE]>;

function* setPreference(action: SetPreferenceAction): SagaIterator {
  if (action.payload.broadcast) {
    const broadCastAction: SetPreferenceAction = { type: action.type, payload: { ...action.payload, broadcast: false } };
    yield put(ipcActions.ipcBroadcast(broadCastAction));
  }

  const service = getContainer().get(PreferencesService);
  yield call([service, service.setPreferenceValue], action.payload.key, action.payload.value, action.payload.broadcast);
  yield put(actions.preferenceUpdated(action.payload.key));
}


