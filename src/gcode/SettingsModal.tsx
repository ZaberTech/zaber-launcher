import { Modal } from '@zaber/react-library';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Route } from 'react-router';
import { routerActions as routerActionsDefinition } from 'connected-react-router';

import { selectRouter } from '../store';
import { useActions } from '../utils';
import { AppIconsNeutral } from '../apps';

import { actions as actionsDefinition } from './actions';
import { SelectController } from './SelectController';
import { Settings } from './Settings';
import { Paths } from './types';
import { selectEditedSettings, selectSettings, selectSettingsDevice } from './selectors';


export const SettingsModal: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const routerActions = useActions(routerActionsDefinition);
  const oldSettings = useSelector(selectSettings);
  const settings = useSelector(selectEditedSettings);
  const router = useSelector(selectRouter);
  const device = useSelector(selectSettingsDevice);

  useEffect(() => {
    actions.updateSettings(oldSettings ?? {});
  }, []);

  if (settings == null) {
    return null;
  }

  let headerText = '';
  if (router.location.pathname === Paths.SelectController) {
    headerText = 'Select Controller';
  } else if (device != null) {
    headerText = `Configure ${device.identity.name}`;
  }

  return (
    <Modal
      className="settings-modal"
      headerIcon={<AppIconsNeutral.GCode/>}
      headerText={headerText}
      onRequestClose={() => {
        actions.settingsEditingDone(false);
        routerActions.replace(Paths.Root);
      }}>
      <Route path={Paths.SelectController}>
        <SelectController/>
      </Route>
      <Route path={Paths.TranslatorSettings}>
        <Settings/>
      </Route>
    </Modal>
  );
};
