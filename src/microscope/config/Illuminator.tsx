import React from 'react';
import { useSelector } from 'react-redux';
import { NoticeBanner, SimpleSelect } from '@zaber/react-library';

import { useDeviceOptions } from './DeviceAndAxisSelect';
import { selectIlluminators } from './selectors';

export const Illuminator: React.FC = () => {
  const illuminators = useSelector(selectIlluminators);
  const options = useDeviceOptions();

  return <>
    <div className="label">Illuminator</div>
    <SimpleSelect className="device" disabled placeholder="Illuminator"
      value={illuminators[0]?.key ?? ''} options={options} onValueChange={() => null}
      portalToBody/>
    {illuminators.length > 1 &&
      <NoticeBanner className="message" type="warning">
        Multiple illuminators found
      </NoticeBanner>}
  </>;
};
