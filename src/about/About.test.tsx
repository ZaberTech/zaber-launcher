import React from 'react';
import { render } from '@testing-library/react';

jest.mock('../updates/UpdateCheck', () => ({
  UpdateCheck: () => null,
}));

import { About } from './About';

test('renders', () => {
  const wrapper = render(<About/>);
  wrapper.unmount();
});
