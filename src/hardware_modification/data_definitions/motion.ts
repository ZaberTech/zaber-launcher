import { Angle, ascii, Length, NoValueForKeyException } from '@zaber/motion';

import { Capabilities, hasCapability } from '../../devices';
import { is3rdPartyId, isRotary3rdPartyId } from '../peripheral_id_picker/peripheralIdMapping';
import { SETUP_COMPLETE_KEY } from '../storageKeys';
import type { SectionDataDefinition } from '../types';
import { getAxis } from '../utils';

export const motion: SectionDataDefinition<'motion'> = {
  initializers: {
    axisType: {
      type: 'custom',
      backing: async comms => {
        const axis = getAxis(comms);
        return axis.identity.axisType;
      }
    },
    isCyclic: {
      type: 'custom',
      requires: ['limit.cycle.dist'],
      backing: async (ascii, device) => {
        const cyclicDist = await ascii.settings.get('limit.cycle.dist');
        if (cyclicDist !== 0 || isRotary3rdPartyId(device.axis?.identity?.peripheralId)) {
          return 1;
        }
        return 0;
      }
    },
    cyclicDist: { type: 'setting', backing: 'limit.cycle.dist' },
    isInfinite: { type: 'setting', backing: 'limit.range.mode' },
    limitMin: { type: 'setting', backing: 'limit.min' },
    limitMax: { type: 'setting', backing: 'limit.max' },
    unitConversionEnabled: {
      type: 'custom',
      backing: async comms => {
        const axis = getAxis(comms);
        try {
          await axis.storage.getString('zaber.unit_conversions');
          return 1;
        } catch (err) {
          if (!(err instanceof NoValueForKeyException)) { throw err }
        }

        if (is3rdPartyId(axis.identity.peripheralId)) {
          let complete = false;
          try {
            complete = await axis.storage.getBool(SETUP_COMPLETE_KEY);
          } catch (err) {
            if (!(err instanceof NoValueForKeyException)) { throw err }
          }
          if (!complete) {
            return 1;
          }
        }

        return 0;
      },
    },
    unitConversion: {
      type: 'custom',
      backing: async comms => {
        const axis = getAxis(comms);
        const { axisType } = axis.identity;
        if (!axis.settings.canConvertNativeUnits('pos') || axisType === ascii.AxisType.UNKNOWN) {
          return null;
        }
        if (axisType === ascii.AxisType.LINEAR) {
          return axis.settings.convertToNativeUnits('pos', 1, Length.mm);
        } else {
          return axis.settings.convertToNativeUnits('pos', 360, Angle.DEGREES);
        }
      },
    }
  },
  sectionPresent: async ctx => {
    if (ctx.info.axis != null) {
      return hasCapability(ctx.info.axis, Capabilities.Motion);
    } else {
      return hasCapability(ctx.info.device, Capabilities.Motion);
    }
  },
  fieldEnabled: {
    unitConversion: values => values.unitConversionEnabled?.is === 1,
  },
};
