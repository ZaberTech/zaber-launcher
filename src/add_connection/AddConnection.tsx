import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Button, Modal, Icons } from '@zaber/react-library';

import type { RootState } from '../store';

import { actions as actionsDefinition } from './actions';
import { selectDialogOpen, selectStep } from './selectors';
import { SelectPort } from './SelectPort';
import { Steps } from './types';
import { ChooseConnectionType } from './ChooseConnectionType';
import { ConfigureTcp } from './ConfigureTcp';
import { ConnectCloud } from './ConnectCloud';
import { DetectDevices } from './DetectDevices';
import { AddToRouter } from './AddToRouter';
import { DiscoverLocalShare } from './DiscoverLocalShare';
import { ConnectLocalShare } from './ConnectLocalShare';
import { DiscoverCloud } from './DiscoverCloud';
import { DeviceDiscovery } from './DeviceDiscovery';

interface Props {
  actions: typeof actionsDefinition;
  dialogOpen: boolean;
  step: Steps | null;
}

class AddConnectionBase extends Component<Props> {
  render(): React.ReactNode {
    const { actions, dialogOpen, step } = this.props;
    return (<>
      <Button className="add-connection-button" onClick={() => actions.changeDialogOpen(true)}>Add New Connection</Button>
      <Modal
        className="add-connection"
        headerIcon={<Icons.Connection/>}
        headerText="Create New Connection"
        isOpen={dialogOpen}
        onRequestClose={() => actions.changeDialogOpen(false)}>
        {step === Steps.CHOOSE_CONNECTION_TYPE && <ChooseConnectionType/>}
        {step === Steps.CONFIGURE_SERIAL && <SelectPort/>}
        {step === Steps.CONFIGURE_TCP && <ConfigureTcp/>}
        {step === Steps.DISCOVER_MDNS && <DeviceDiscovery/>}
        {step === Steps.DETECT_DEVICES && <DetectDevices/>}
        {step === Steps.ADD_TO_ROUTER && <AddToRouter/>}
        {step === Steps.CONNECT_CLOUD && <ConnectCloud/>}
        {step === Steps.DISCOVER_LOCAL_SHARE && <DiscoverLocalShare/>}
        {step === Steps.CONNECT_LOCAL_SHARE && <ConnectLocalShare/>}
        {step === Steps.DISCOVER_CLOUD && <DiscoverCloud/>}
      </Modal>
    </>);
  }
}

export const AddConnection = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    dialogOpen: selectDialogOpen(state),
    step: selectStep(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(AddConnectionBase);
