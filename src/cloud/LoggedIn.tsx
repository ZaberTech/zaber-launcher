import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { ContextMenu, Icons, Text, MinorMenu } from '@zaber/react-library';
import { NavLink, Redirect, Route, Switch } from 'react-router-dom';

import type { RootState } from '../store';

import { selectCanLogout, selectUser } from './selectors';
import { actions as actionsDefinition } from './actions';
import type { LoggedInUser } from './types';
import { CloudSharing } from './CloudSharing';
import { Header } from './Header';
import { CloudDiscovery } from './CloudDiscover';

interface Props {
  actions: typeof actionsDefinition;
  user: LoggedInUser | null;
  logoutEnabled: boolean;
}

class LoggedInBase extends Component<Props> {
  componentDidMount() {
    const { actions } = this.props;
    actions.cloudSharingStatus();
  }

  render(): React.ReactNode {
    const { user, actions, logoutEnabled } = this.props;
    if (!user) {
      return null;
    }

    return (<div className="logged-in">
      <div className="header">
        <Header/>
        <ContextMenu dropStyle="aligned"
          trigger={() => (<>
            <Icons.User className="user"/>&ensp;
            <Text t={Text.Type.H4}>{user.email}</Text>
            <Icons.DropdownArrow/>
          </>)}>
          <ContextMenu.Item disabled={!logoutEnabled} onClick={actions.logout} icon={<Icons.SignOut/>}>
            Sign out
          </ContextMenu.Item>
        </ContextMenu>
      </div>

      <div className="content">
        <MinorMenu>
          <NavLink to="/cloud/sharing"><MinorMenu.Item>Share my Devices</MinorMenu.Item></NavLink>
          <NavLink to="/cloud/discovery"><MinorMenu.Item>Shared with Me</MinorMenu.Item></NavLink>
        </MinorMenu>

        <div className="minor-content">
          <Switch>
            <Route path="/cloud/sharing" component={CloudSharing}/>
            <Route path="/cloud/discovery" component={CloudDiscovery}/>
            <Route><Redirect to="/cloud/sharing"/></Route>
          </Switch>
        </div>
      </div>
    </div>);
  }
}

export const LoggedIn = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    user: selectUser(state),
    logoutEnabled: selectCanLogout(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(LoggedInBase);
