import React from 'react';
import { Flex, HelpTooltip, iconFromSvg, Icons, SimpleSelect, Text } from '@zaber/react-library';
import classNames from 'classnames';

import etherCatSvg from '../assets/etherCAT.svg';


const EtherCatIcon = iconFromSvg(etherCatSvg);

const options = [
  { value: 0, label: 'USB Control', icon: <Icons.Usb/> },
  { value: 1, label: 'EtherCAT Control', icon: <EtherCatIcon/> },
];


export interface Props {
  isRemote: boolean;
  hasError: boolean;
  setRemote: (remote: boolean) => void;
}


export const RemoteSwitch: React.FC<Props> = ({ isRemote, hasError, setRemote }) =>
  <Flex.Row className={classNames('remote-mode-switch', { 'has-error': hasError })}>
    <SimpleSelect<number>
      isSearchable
      portalToBody
      title="Motion control mode"
      blurInputOnSelect
      options={options}
      value={isRemote ? 1 : 0}
      onValueChange={option => setRemote(option === 1)}/>
    <HelpTooltip popupClassName="ethercat-mode-switch-help">
      <div>
        {isRemote && <Text><p>This device is under EtherCAT (remote) control. Motion is controlled by your EtherCAT software
            and motion-related commands and settings cannot be used from Zaber Launcher.</p>
        <p>Select USB Control to enable motion control from Zaber Launcher.</p></Text>}
        {!isRemote && <Text><p>This device is under USB (local) control. Zaber Launcher has full control over the device,
            and EtherCAT software cannot command motion at this time.</p>
        <p>Select EtherCAT Control to enable motion control from EtherCAT software.</p></Text>}
      </div>
    </HelpTooltip>
  </Flex.Row>;
