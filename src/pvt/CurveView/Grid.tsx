/* eslint-disable import-x/namespace */
import React from 'react';
import * as math from 'mathjs';

import { roundedE3Sequence, roundToMultiple } from '../../utils';
import type { Span } from '../types';
import { seriesColor } from '../utils';

import { XAXIS, YAXIS } from './constants';


const LABEL_MARGIN = 20;
const MARK_SIZE = 7;


// Finds a tick interval that fits in the interesting range 5-10 times and follows the 1,2,5 scaling pattern.
function chooseTickInterval(span: Span): number {
  const range = span.max - span.min;
  const sequence = roundedE3Sequence(range / 10, range / 2);
  return sequence[0];
}


// Set the ticks at even multiples of the interval over the full range.
function generateTicks(span: Span, interval: number): number[] {
  const result = [];
  let tick = roundToMultiple(span.min, interval);
  while (tick <= span.max) {
    result.push(tick);
    tick += interval;
  }

  return result;
}


const horizontalLine = (x1: number, x2: number, y: number, className: string, color?: string) =>
  <line
    x1={x1} x2={x2}
    y1={y} y2={y}
    className={className}
    role="horizontal-grid-line"
    style={color ? { stroke: color } : {}}/>;


const verticalLine = (x: number, y1: number, y2: number, className: string, color?: string) =>
  <line
    x1={x} x2={x}
    y1={y1} y2={y2}
    className={className}
    role="vertical-grid-line"
    style={color ? { stroke: color } : {}}/>;


export interface BugMarker {
  x: number;
  y: number;
  text: string;
}


export interface Props {
  // Chart area dimensions in chart coordinates (origin at the center).
  viewPort: {
    x: Span;
    y: Span;
  };
  // Device motion limits in the same units as the grid.
  deviceLimits: {
    x?: Span;
    y?: Span;
  };
  // Transformation from curve coordinates to chart coordinates (3x3 homogeneous matrix).
  projection: math.Matrix;
  axisLabels: {
    x: string;
    y: string;
  };
  bugs: BugMarker[];
  drawingAxes: number[];
}


// Draws the grid and axis labels on the curve view.
export const Grid: React.FC<Props> = ({ viewPort, deviceLimits, projection, axisLabels, bugs, drawingAxes }) => {
  // Project the viewport into curve space to determine how many ticks each axis will have.
  const leftTop = math.matrix([viewPort.x.min, viewPort.y.min, 1]);
  const rightBottom = math.matrix([viewPort.x.max, viewPort.y.max, 1]);
  const projInv = math.inv(projection);
  const gridLeftTop = math.multiply(projInv, leftTop);
  const gridRightBottom = math.multiply(projInv, rightBottom);

  // Use the smaller of the tick intervals for X and Y to create a square grid.
  const xRange = { min: gridLeftTop.get([0]), max: gridRightBottom.get([0]) };
  const yRange = { min: gridRightBottom.get([1]), max: gridLeftTop.get([1]) };
  const tickInterval = math.min(chooseTickInterval(xRange), chooseTickInterval(yRange));
  const xTicks = generateTicks(xRange, tickInterval);
  const yTicks = generateTicks(yRange, tickInterval);

  const projectX = (x: number) => math.multiply(projection, math.matrix([x, 0, 1])).get([0]) as number;
  const projectY = (y: number) => math.multiply(projection, math.matrix([0, y, 1])).get([1]) as number;

  const digits = math.floor(math.log10(tickInterval));
  const round: (n: number) => string = (digits >= 0)
    ? (n: number) => n.toString()
    : (n: number) => n.toFixed(-digits);

  return <>
    {deviceLimits.x && <React.Fragment key="xoob">
      {projectX(deviceLimits.x.max) < viewPort.x.max &&
        <rect className="out-of-bounds"
          role="x-travel-limit"
          x={projectX(deviceLimits.x.max)}
          y={viewPort.y.min}
          width={viewPort.x.max - projectX(deviceLimits.x.max)}
          height={viewPort.y.max - viewPort.y.min}/>}
      {viewPort.x.min < projectX(deviceLimits.x.min) &&
        <rect className="out-of-bounds"
          role="x-travel-limit"
          x={viewPort.x.min}
          y={viewPort.y.min}
          width={projectX(deviceLimits.x.min) - viewPort.x.min}
          height={viewPort.y.max - viewPort.y.min}/>}
    </React.Fragment>}

    {deviceLimits.y && <React.Fragment key="yoob">
      {projectY(deviceLimits.y.max) > viewPort.y.min &&
        <rect className="out-of-bounds"
          role="y-travel-limit"
          x={viewPort.x.min}
          y={viewPort.y.min}
          width={viewPort.x.max - viewPort.x.min}
          height={projectY(deviceLimits.y.max) - viewPort.y.min}/>}
      {viewPort.y.max > projectY(deviceLimits.y.min) &&
        <rect className="out-of-bounds"
          role="y-travel-limit"
          x={viewPort.x.min}
          y={projectY(deviceLimits.y.min)}
          width={viewPort.x.max - viewPort.x.min}
          height={viewPort.y.max - projectY(deviceLimits.y.min)}/>}
    </React.Fragment>}

    <text key="xLabel" className="axis-label"
      x={(viewPort.x.max + viewPort.x.min) / 2}
      y={viewPort.y.max - 2 * LABEL_MARGIN}
      textAnchor="middle"
      role="x-axis-label"
      vectorEffect="non-scaling-size">{axisLabels.x}</text>
    <text key="yLabel" className="axis-label"
      x={viewPort.x.min + 2 * LABEL_MARGIN}
      y={(viewPort.y.max + viewPort.y.min) / 2}
      textAnchor="right"
      role="y-axis-label"
      alignmentBaseline="middle"
      vectorEffect="non-scaling-size">{axisLabels.y}</text>

    {xTicks.map(x => <React.Fragment key={`x-${x}`}>
      {verticalLine(projectX(x), viewPort.y.min, viewPort.y.max, 'grid-line')}
      <text x={projectX(x) + 5} y={viewPort.y.max - LABEL_MARGIN} className="tick-label x" role="x-tick-value">{round(x)}</text>
    </React.Fragment>)}
    {yTicks.map(y => <React.Fragment key={`y-${y}`}>
      {horizontalLine(viewPort.x.min, viewPort.x.max, projectY(y), 'grid-line')}
      <text x={viewPort.x.min + 5} y={projectY(y) + LABEL_MARGIN} className="tick-label y" role="y-tick-value">{round(y)}</text>
    </React.Fragment>)}

    {deviceLimits.x && <React.Fragment key="xlimits">
      {verticalLine(projectX(deviceLimits.x.min), viewPort.y.min, viewPort.y.max, 'position-limit', seriesColor(drawingAxes[XAXIS]))}
      {verticalLine(projectX(deviceLimits.x.max), viewPort.y.min, viewPort.y.max, 'position-limit', seriesColor(drawingAxes[XAXIS]))}
    </React.Fragment>}
    {deviceLimits.y && <React.Fragment key="ylimits">
      {horizontalLine(viewPort.x.min, viewPort.x.max, projectY(deviceLimits.y.min), 'position-limit', seriesColor(drawingAxes[YAXIS]))}
      {horizontalLine(viewPort.x.min, viewPort.x.max, projectY(deviceLimits.y.max), 'position-limit', seriesColor(drawingAxes[YAXIS]))}
    </React.Fragment>}

    {bugs.map((bug, i) => {
      const x = projectX(bug.x);
      const y = projectY(bug.y);
      return <React.Fragment key={i}>
        <line x1={x - MARK_SIZE} x2={x + MARK_SIZE} y1={y - MARK_SIZE} y2={y + MARK_SIZE} className="marker error" role="error-marker"/>
        <line x1={x - MARK_SIZE} x2={x + MARK_SIZE} y1={y + MARK_SIZE} y2={y - MARK_SIZE} className="marker error" role="error-marker"/>
      </React.Fragment>;
    })}
  </>;
};
