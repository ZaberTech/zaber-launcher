import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

import type { SettingManual, WarningFlagManual, CommandRejectedReasonManual } from './types';

export enum ActionTypes {
  GENERATE_SETTING_MANUAL = 'PROTOCOL_MANUAL-GENERATE_SETTING',
  STORE_SETTING_MANUAL = 'PROTOCOL_MANUAL-STORE_SETTING_MANUAL',

  GENERATE_WARNING_MANUAL = 'PROTOCOL_MANUAL-GENERATE_WARNING_MANUAL',
  STORE_WARNING_MANUAL = 'PROTOCOL_MANUAL-STORE_WARNING_MANUAL',

  GENERATE_COMMAND_REJECTED_REASON_MANUAL = 'PROTOCOL_MANUAL-GENERATE_COMMAND_REJECTED_REASON_MANUAL',
  STORE_COMMAND_REJECTED_REASON_MANUAL = 'PROTOCOL_MANUAL-STORE_COMMAND_REJECTED_REASON_MANUAL',

  GENERATE_COMMAND_ID_LIST = 'PROTOCOL_MANUAL-GENERATE-COMMAND-ID-LIST',
  STORE_COMMAND_ID_LIST = 'PROTOCOL_MANUAL-STORE-COMMAND-ID-LIST',

  GENERATE_SETTING_ID_LIST = 'PROTOCOL_MANUAL-GENERATE-SETTING-ID-LIST',
  STORE_SETTING_ID_LIST = 'PROTOCOL_MANUAL-STORE-SETTING-ID-LIST',
}

export interface ActionsToPayloads {
  [ActionTypes.GENERATE_SETTING_MANUAL]: { entity: EntityKey; setting: string };
  [ActionTypes.STORE_SETTING_MANUAL]: { entity: EntityKey; setting: string; manual: SettingManual };

  [ActionTypes.GENERATE_WARNING_MANUAL]: { entity: EntityKey; flag: string };
  [ActionTypes.STORE_WARNING_MANUAL]: { entity: EntityKey; flag: string; manual: WarningFlagManual };

  [ActionTypes.GENERATE_COMMAND_REJECTED_REASON_MANUAL]: { entity: EntityKey;  reason: string };
  [ActionTypes.STORE_COMMAND_REJECTED_REASON_MANUAL]:
  { entity: EntityKey; reason: string; manual: CommandRejectedReasonManual };

  [ActionTypes.GENERATE_COMMAND_ID_LIST]: void;
  [ActionTypes.STORE_COMMAND_ID_LIST]: { commandIdList: string[] };

  [ActionTypes.GENERATE_SETTING_ID_LIST]: void;
  [ActionTypes.STORE_SETTING_ID_LIST]: { settingIdList: string[] };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actionDefinitions = {
  generateSettingManual: (entity: EntityKey, setting: string) => (
    buildAction(ActionTypes.GENERATE_SETTING_MANUAL, { entity, setting })
  ),
  storeSettingManual: (entity: EntityKey, setting: string, manual: SettingManual) => (
    buildAction(ActionTypes.STORE_SETTING_MANUAL, { entity, setting, manual })
  ),

  generateWarningManual: (entity: EntityKey, flag: string) => (
    buildAction(ActionTypes.GENERATE_WARNING_MANUAL, { entity, flag })
  ),
  storeWarningManual: (entity: EntityKey, flag: string, manual: WarningFlagManual) => (
    buildAction(ActionTypes.STORE_WARNING_MANUAL, { entity, flag, manual })
  ),

  generateCommandRejectedReasonManual: (entity: EntityKey, reason: string) => (
    buildAction(ActionTypes.GENERATE_COMMAND_REJECTED_REASON_MANUAL, { entity, reason })
  ),
  storeCommandRejectedReasonManual: (entity: EntityKey, reason: string, manual: WarningFlagManual) => (
    buildAction(ActionTypes.STORE_COMMAND_REJECTED_REASON_MANUAL, { entity, reason, manual })
  ),

  generateCommandIdList: () => buildAction(ActionTypes.GENERATE_COMMAND_ID_LIST),
  storeCommandIdList: (commandIdList: string[]) => buildAction(ActionTypes.STORE_COMMAND_ID_LIST, { commandIdList }),

  generateSettingIdList: () => buildAction(ActionTypes.GENERATE_SETTING_ID_LIST),
  storeSettingIdList: (settingIdList: string[]) => buildAction(ActionTypes.STORE_SETTING_ID_LIST, { settingIdList }),
};
