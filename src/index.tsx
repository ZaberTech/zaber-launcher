/* eslint-disable import-x/first */
// this set of imports is intentionally first
import './environment';
import { initErrors } from './errors';

initErrors();

import 'reflect-metadata';
import React from 'react';
import { render } from 'react-dom';
import type { Store } from 'redux';

import './styles/index.scss';
import './styles/init';
import { App } from './App';
import { createContainer, getContainer } from './container';
import { buildStore } from './store/build';
import { reduxStoreSymbol } from './store';
import { ZML } from './zml';
import { loggingSettingsSymbol, DEFAULT_LOGGING_SETTINGS } from './zml/logging_settings';

const container = createContainer();
container.bind(reduxStoreSymbol).toDynamicValue(() => buildStore());
container.bind(loggingSettingsSymbol).toConstantValue(DEFAULT_LOGGING_SETTINGS);

getContainer().get(ZML);
const store = container.get<Store>(reduxStoreSymbol);

render(<App store={store}/>, document.getElementById('root'));

document.getElementById('app-loading-animation')?.remove();
