import _ from 'lodash';
import { createSelector } from 'reselect';
import { tryAccess } from '@zaber/toolbox';

import { ConnectionType } from '../app_components';
import { selectConnections, selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import { makeAxisKey, tryExtractConnectionKey } from '../keys';
import { selectOscilloscope } from '../store';

import { isNewRecTab } from './utils';
import { CaptureSettings, PER_DEVICE_DEFAULTS, SignalGeneratorSettings } from './reducers';


// Data source rules:
// If the New Recording tab is selected:
// - Capture settings come from the selected device and can be changed.
// - Signal generator settings come from the selected device and can be changed.
// - Channels come from all the axes of the selected device and can be fully edited.
// else:
// - Capture settings come from the selected recording and are read only.
// - Signal generator settings come from the selected recording and are read only.
// - Channels come from the selected recording can only be only shown or hidden.


export const selectSelectedDeviceKey = createSelector(selectOscilloscope, state => state.selectedDeviceKey);
export const selectSelectedDeviceIdentity = createSelector(selectSelectedDeviceKey, selectIdentifiedDevices,
  (key, devices) => devices[key ?? ''] ?? null);


export const selectSelectedConnection = createSelector(selectOscilloscope, selectConnections,
  (state, connections) => tryAccess(connections, tryExtractConnectionKey(state.selectedDeviceKey)));

export const selectSelectedDeviceState = createSelector(selectOscilloscope, state =>
  tryAccess(state.perDeviceSettings, state.selectedDeviceKey) ?? _.cloneDeep(PER_DEVICE_DEFAULTS));

export const selectAllChannels = createSelector(selectOscilloscope, selectSelectedDeviceState, (state, deviceState) =>
  (isNewRecTab(state.selectedRecordingIndex)
    ? (state.activeRecording?.channels ?? deviceState?.axes.map(axisState => axisState.channels)?.flat() ?? [])
    : (state.recordings[state.selectedRecordingIndex]?.channels) ?? []));

export const selectSavedRecordings = createSelector(selectOscilloscope, state => state.recordings);
export const selectActiveRecording = createSelector(selectOscilloscope, state => state.activeRecording);
export const selectSelectedRecording = createSelector(selectOscilloscope, state =>
  isNewRecTab(state.selectedRecordingIndex) ? state.activeRecording : state.recordings[state.selectedRecordingIndex]);
export const selectSelectedRecordingIndex = createSelector(selectOscilloscope, state => state.selectedRecordingIndex);

export const selectCaptureSettings = createSelector(selectSelectedRecording, selectSelectedDeviceState,
  (recording, deviceState) => (recording ?? deviceState) as CaptureSettings);

export const selectNewRecordingCaptureSettings = createSelector(selectSelectedDeviceState,
  deviceState => deviceState as CaptureSettings);

export const selectSignalGeneratorSettings = createSelector(selectSelectedRecording, selectSelectedDeviceState,
  (recording, deviceState) => (recording ?? deviceState) as SignalGeneratorSettings);

export const selectIsCaptureInProgress = createSelector(selectActiveRecording, recording => !!recording);
export const selectStopButtonEnabled = createSelector(selectOscilloscope, state => state.stopButtonEnabled);

export const selectDevices = createSelector(selectOscilloscope, state => state.perDeviceSettings);

export const selectSelectedConnectionIsSerialPort = createSelector(selectSelectedConnection,
  connection => connection?.type === ConnectionType.SERIAL_PORT);
export const selectUnitConversionEnabled = createSelector(selectOscilloscope, state => state.unitConversionEnabled);

export const selectCanRepeatCurrentRecording = createSelector(
  selectSelectedRecording,
  selectIdentifiedDevices,
  selectIdentifiedAxes,
  (selectedRecording, devices, axes) =>
    selectedRecording != null
    && (selectedRecording.deviceId === tryAccess(devices, selectedRecording.deviceKey)?.identity?.deviceId)
    && (_.isEqual(
      selectedRecording.peripheralIds,
      selectedRecording.peripheralIds.map((_, i) =>
        tryAccess(axes, makeAxisKey(selectedRecording.deviceKey, i))?.identity?.peripheralId ?? 0))));
