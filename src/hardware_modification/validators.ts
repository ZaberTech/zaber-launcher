import { Acceleration, Angle, AngularAcceleration, AngularVelocity, ascii, CommandFailedException, Length, Velocity } from '@zaber/motion';

import type { AsciiComms } from '../connection_manager/types';
import { getContainer } from '../container';
import { CurrentTunerAPI, resolveInputSettings } from '../current_tuner';
import { FW_TICK_RATE, TIME_CONSTANT } from '../units';
import { hasSetting } from '../devices';
import { makeString, omit, throwUnexpectedError } from '../utils';

import {
  HOLDING_TORQUE_KEY, HOLD_FACTOR_KEY, RATED_CURRENT_KEY, RPM_KEY, RUN_FACTOR_KEY, SETUP_COMPLETE_KEY, STEPS_PER_ROTATION_KEY
} from './storageKeys';
import { EncoderMode } from './data_definitions/encoder';
import {
  RATED_TO_HOLD_CURRENT,
  RATED_TO_MAXSPEED_FACTOR,
  RATED_TO_MAX_CURRENT,
  RATED_TO_RUN_CURRENT,
} from './data_definitions/motorCharacteristics';
import type { FieldKey, NonNullSectionValues, NonNullValue, NonNullValues, ValueKey } from './types';
import type { FieldIds, SectionIds } from './data_definitions';
import { getAxis } from './utils';
import type { HwModDeviceOrAxisState } from './selectors';

export type ValidationError = ValueKey & { message: string };

type ValidatorResponse = ValidationError[] | { topLevelError: string } | null;

export type FieldError = ValidationError & { correlatedErrors: FieldKey[] };

export type ErrorsOnApply = {
  fieldErrors: FieldError[];
  topLevelErrors: { topLevelError: string }[];
};

/** A section of non-null values, where the specified values have been verified to exist */
type SectionWithFields<S extends SectionIds, FK extends FieldIds<S>> = NonNullSectionValues<S> & Record<FK, NonNullValue>;
/**
 * Ensures that a provided section is defined and all listed fields are non-null
 * @param section The section to check
 * @param fields The fields to verify the existence of
 * @returns true if all the expected fields are present
 */
function hasFields<S extends SectionIds, FK extends FieldIds<S>>(
  section: NonNullSectionValues<S> | undefined, fieldIds: FK[]
): section is SectionWithFields<S, FK> {
  if (section == null) { return false }
  for (const fieldId of fieldIds) {
    if (section[fieldId] == null) { return false }
  }
  return true;
}

export function topLevelErrorOnApply(topLevelError: string): ErrorsOnApply {
  return {
    fieldErrors: [],
    topLevelErrors: [{ topLevelError }],
  };
}

export function fieldErrorsOnApply(errors: ValidationError[]): ErrorsOnApply  {
  return {
    fieldErrors: errors.map(err => ({ ...err, correlatedErrors: [] })),
    topLevelErrors: [],
  };
}

type FormValidator = (values: NonNullValues, device: AsciiComms) => Promise<ValidatorResponse>;

function encoderOn(encoderValues: NonNullSectionValues<'encoder1'> | NonNullSectionValues<'encoder2'> | undefined) {
  if (!hasFields(encoderValues, ['mode'])) { return false }
  const mode = encoderValues.mode.value;
  return mode !== EncoderMode.OFF && mode !== EncoderMode.DISCONNECTED;
}

function encoderModeIs(encoderValues: SectionWithFields<'encoder1' | 'encoder2', 'mode'>, mode: EncoderMode) {
  const encoderMode = encoderValues.mode.value;
  return encoderMode === mode;
}

const minElectricalValues = 0.1;
const maxElectricalValues = 18446744073;

function calculateDriverCurrentRun(values: SectionWithFields<'motorCharacteristics', 'ratedCurrent' | 'runFactor'>) {
  const rated = values.ratedCurrent;
  const factor = values.runFactor;
  return rated.value * factor.value * RATED_TO_RUN_CURRENT;
}

function calculateDriverCurrentHold(values: SectionWithFields<'motorCharacteristics', 'ratedCurrent' | 'holdFactor'>) {
  const rated = values.ratedCurrent;
  const factor = values.holdFactor;
  return rated.value * factor.value * RATED_TO_HOLD_CURRENT;
}

async function runValidators(
  validators: FormValidator[], values: NonNullValues, device: AsciiComms
): Promise<ErrorsOnApply | null> {
  const errors: ErrorsOnApply = {
    fieldErrors: [],
    topLevelErrors: [],
  };
  for (const validator of validators) {
    const validationResponse = await validator(values, device);
    if (validationResponse != null) {
      if (Array.isArray(validationResponse)) {
        const correlatedErrors: FieldKey[] = validationResponse.map(err => omit(err, 'message'));
        for (const validationError of validationResponse) {
          errors.fieldErrors.push({ ...validationError, correlatedErrors });
        }
      } else {
        errors.topLevelErrors.push(validationResponse);
      }
    }
  }

  if (errors.fieldErrors.length > 0 || errors.topLevelErrors.length > 0) {
    return errors;
  } else {
    return null;
  }
}

const preSetValidators: FormValidator[] = [
  // Positive Cycle Distance
  async values => {
    if (!hasFields(values.motion, ['isCyclic', 'cyclicDist'])) { return null }

    const isCyclic = values.motion.isCyclic.value;
    const cyclicDist = values.motion.cyclicDist.value;
    if (isCyclic !== 0 && cyclicDist <= 0) {
      return [
        { sectionId: 'motion', fieldId: 'cyclicDist', message: 'Cycle distance must be greater than 0 if this is a cyclic device' },
        { sectionId: 'motion', fieldId: 'isCyclic', message: '' },
      ];
    }
    return null;
  },
  async values => {
    if (!hasFields(values.motion, ['limitMin', 'limitMax'])) { return null }

    const min = values.motion.limitMin.value;
    const max = values.motion.limitMax.value;

    if (min >= max) {
      return [
        { sectionId: 'motion', fieldId: 'limitMin', message: 'Minimum Position must be less than Maximum Position' },
        { sectionId: 'motion', fieldId: 'limitMax', message: 'Maximum Position must be greater than Minimum Position' },
      ];
    }
    return null;
  },
  async (values): Promise<ValidatorResponse> => {
    if (!hasFields(values.motorCharacteristics, ['ratedCurrent', 'runFactor', 'holdFactor'])) { return null }

    const ratedCurrent = values.motorCharacteristics.ratedCurrent.value;
    const runFactor = values.motorCharacteristics.runFactor.value;
    const holdFactor = values.motorCharacteristics.holdFactor.value;

    const ratedError: ValidationError[] = ratedCurrent > 0 ? [] : [
      { sectionId: 'motorCharacteristics', fieldId: 'ratedCurrent', message: 'Rated current must be a positive value' }
    ];

    const runError: ValidationError[] = runFactor > 0 ? [] : [
      { sectionId: 'motorCharacteristics', fieldId: 'runFactor', message: 'Run current must be a positive value' }
    ];

    const holdError: ValidationError[] = holdFactor > 0 ? [] : [
      { sectionId: 'motorCharacteristics', fieldId: 'holdFactor', message: 'Hold current must be a positive value' }
    ];

    return [...ratedError, ...runError, ...holdError];
  },
  // Limit Index Pulse Mode
  async (values): Promise<ValidatorResponse> => {
    const indexMode1 = values.encoder1?.indexMode?.value;
    const indexMode2 = values.encoder2?.indexMode?.value;
    if (indexMode1 === 1 || indexMode2 === 1) {
      return null;
    }

    for (const sectionId of ['limitHome', 'limitAway', 'limitC'] as const) {
      const source = values[sectionId]?.source?.value;
      if (source != null && (source === 1 || source === 2)) {
        const message = 'This source requires that at least one encoder has its index pulse mode enabled';
        return [
          { sectionId, fieldId: 'source', message },
          { sectionId: 'encoder1', fieldId: 'indexMode', message },
          { sectionId: 'encoder2', fieldId: 'indexMode', message },
        ];
      }
    }

    return null;
  },
  // Motor Type Encoder Verification
  async (values): Promise<ValidatorResponse> => {
    const encoder1 = values.encoder1;
    const encoder2 = values.encoder2;
    if (!hasFields(encoder1, ['mode', 'motorType']) || !hasFields(encoder2, ['mode'])) { return null }
    const motorType = encoder1.motorType.value;

    if (motorType === 1 && encoderOn(encoder1) && encoderOn(encoder2)) {
      const why = 'Products with a stepper motor may only have one encoder enabled at a time';
      return [
        { sectionId: 'encoder1', fieldId: 'mode', message: `${why}. Set this or encoder 2 mode to disconnected or disabled` },
        { sectionId: 'encoder2', fieldId: 'mode', message: `${why}. Set this or encoder 1 mode to disconnected or disabled` },
      ];
    } else if (motorType === 2) {
      const isDirectAndAbsolute = encoderModeIs(encoder1, EncoderMode.DIRECT) && encoderModeIs(encoder2, EncoderMode.ABSOLUTE);
      const isAbsoluteAndDirect = encoderModeIs(encoder1, EncoderMode.ABSOLUTE) && encoderModeIs(encoder2, EncoderMode.DIRECT);
      if (isDirectAndAbsolute || isAbsoluteAndDirect) {
        return null;
      }
      const error = 'Products with three-phase direct drive motors must have a direct encoder and an absolute phase encoder';
      return [
        { sectionId: 'encoder1', fieldId: 'mode', message: error },
        { sectionId: 'encoder2', fieldId: 'mode', message: error },
      ];
    } else if (motorType === 3) {
      if (encoderOn(encoder1) && encoderOn(encoder2)) {
        const why = 'Products with voice coil motors may only have one encoder enabled at a time';
        return [
          { sectionId: 'encoder1', fieldId: 'mode', message: `${why}. Set this or encoder 2 mode to disconnected or disabled` },
          { sectionId: 'encoder2', fieldId: 'mode', message: `${why}. Set this or encoder 1 mode to disconnected or disabled` },
        ];
      }
      if (!encoderModeIs(encoder1, EncoderMode.DIRECT) && !encoderModeIs(encoder2, EncoderMode.DIRECT)) {
        const why = 'Products with voice coil motors must have a direct encoder enabled';
        return [
          { sectionId: 'encoder1', fieldId: 'mode', message: `${why}. Switch this or encoder 2 mode to direct` },
          { sectionId: 'encoder2', fieldId: 'mode', message: `${why}. Switch this or encoder 1 mode to direct` },
        ];
      }
    }
    return null;
  },
  // Computed Settings
  async (values): Promise<ValidatorResponse> => {
    if (!hasFields(values.motorCharacteristics, ['stepsPerRotation', 'rpm'])) { return null }

    const S = values.motorCharacteristics.stepsPerRotation.value;
    const R = values.motorCharacteristics.rpm.value;
    const maxspeed = S * R * RATED_TO_MAXSPEED_FACTOR;
    const errors: ValidationError[] = [];
    if (S <= 0) {
      errors.push({ sectionId: 'motorCharacteristics', fieldId: 'stepsPerRotation', message: 'Steps per rotation must be greater than 0' });
    }
    if (R <= 0) {
      errors.push({ sectionId: 'motorCharacteristics', fieldId: 'rpm', message: 'Rated RPM must be greater than 0' });
    }
    if (errors.length > 0) { return errors }
    const maxMaxSpeed = 1048576;
    if (maxspeed > maxMaxSpeed) {
      const maxStepsTimesRpm = maxMaxSpeed / RATED_TO_MAXSPEED_FACTOR;
      const message = `(Steps Per Rotation) x (RPM) must be less than ${maxStepsTimesRpm}`;
      return [
        { sectionId: 'motorCharacteristics', fieldId: 'stepsPerRotation', message },
        { sectionId: 'motorCharacteristics', fieldId: 'rpm', message },
      ];
    }
    return null;
  },
  async (values): Promise<ValidatorResponse> => {
    if (!hasFields(values.motorCharacteristics, ['motorInductance'])) { return null }

    const inductance = values.motorCharacteristics.motorInductance.value;
    if (inductance < minElectricalValues || inductance > maxElectricalValues) {
      return [{
        sectionId: 'motorCharacteristics', fieldId: 'motorInductance',
        message: `Inductance must be a value between ${minElectricalValues.toFixed(1)} and ${maxElectricalValues.toLocaleString()}`
      }];
    }
    return null;
  },
  async (values): Promise<ValidatorResponse> => {
    if (!hasFields(values.motorCharacteristics, ['motorResistance'])) { return null }

    const resistance = values.motorCharacteristics.motorResistance.value;
    if (resistance < minElectricalValues || resistance > maxElectricalValues) {
      return [{
        sectionId: 'motorCharacteristics', fieldId: 'motorResistance',
        message: `Resistance must be a value between ${minElectricalValues.toFixed(1)} and ${maxElectricalValues.toLocaleString()}`
      }];
    }
    return null;
  },
  async (values, device): Promise<ValidatorResponse> => {
    if (!hasFields(values.motorCharacteristics, ['ratedCurrent', 'holdFactor', 'runFactor'])) { return null }

    const ratedCurrent = values.motorCharacteristics.ratedCurrent.value;
    if (ratedCurrent <= 0) {
      return [{ sectionId: 'motorCharacteristics', fieldId: 'ratedCurrent', message: 'Rated current must be greater than 0' }];
    }

    const maxDriverCurrent = await device.settings.get('driver.current.max');

    // Limited Hold Current
    const errors: ValidationError[] = [];
    const holdFactor = values.motorCharacteristics.holdFactor!.value;
    const driverCurrentHold = calculateDriverCurrentHold(values.motorCharacteristics);
    if (holdFactor <= 0 || holdFactor > 100) {
      errors.push({ sectionId: 'motorCharacteristics', fieldId: 'holdFactor', message: `Invalid value (${holdFactor}%)` });
    } else if (driverCurrentHold > maxDriverCurrent) {
      const maxPct = Math.floor(maxDriverCurrent /  (ratedCurrent * RATED_TO_HOLD_CURRENT));
      errors.push({
        sectionId: 'motorCharacteristics', fieldId: 'holdFactor',
        message: [
          'This hold percentage is too high and could result in the motor being driven with a damaging current.',
          `For a motor with rated current ${ratedCurrent}V it may be at most ${maxPct}%`
        ].join(' '),
      });
    }

    // Limited Run Current
    const runFactor = values.motorCharacteristics.runFactor!.value;
    const driverCurrentRun = calculateDriverCurrentRun(values.motorCharacteristics);
    if (runFactor <= 0) {
      errors.push({ sectionId: 'motorCharacteristics', fieldId: 'runFactor', message: `Invalid value (${runFactor}%)` });
    } else if (driverCurrentRun > maxDriverCurrent) {
      const maxPct = Math.floor(maxDriverCurrent /  (ratedCurrent * RATED_TO_RUN_CURRENT));
      errors.push({
        sectionId: 'motorCharacteristics', fieldId: 'runFactor',
        message: [
          'This run percentage is too high and could result in the motor being driven with a damaging current.',
          `For a motor with rated current ${ratedCurrent}V it may be at most ${maxPct}%`
        ].join(' '),
      });
    }

    return errors;
  }
];

export async function runPreSetValidators(values: NonNullValues, device: AsciiComms) {
  return runValidators(preSetValidators, values, device);
}

/** A custom setter returns either an error or a list of value keys that are applied by this setter */
type CustomSetResponse = { errors?: ValidationError[]; updatesApplied?: NonNullValue[] };
type CustomSetter = (values: NonNullValues, device: AsciiComms, info: HwModDeviceOrAxisState) => Promise<CustomSetResponse>;
/** A collection of custom set methods used for things that are not one-to-one with settings */
export const customSetters: CustomSetter[] = [
  async values => {
    if (values.motion?.isCyclic) {
      return { updatesApplied: [values.motion.isCyclic] };
    }
    return {};
  },
  // motor speed
  async (values, device, info): Promise<CustomSetResponse> => {
    if (!hasFields(values.motorCharacteristics, ['stepsPerRotation', 'rpm'])) { return {} }
    const steps = values.motorCharacteristics.stepsPerRotation;
    const rpm = values.motorCharacteristics.rpm;

    const calcSpeed = rpm.value * steps.value * RATED_TO_MAXSPEED_FACTOR;
    const errors: ValidationError[] = [];

    const toSet: [string, number][] = [
      ['maxspeed', calcSpeed], ['limit.approach.maxspeed', calcSpeed], ['limit.detect.maxspeed', calcSpeed / 16],
    ];
    if (hasSetting('knob.maxspeed', info.axis ?? info.device)) {
      toSet.push(['knob.maxspeed', calcSpeed]);
    }

    for (const [setting, value] of toSet) {
      try {
        await device.settings.set(setting, value);
      } catch (e) {
        throwUnexpectedError(e);
        const message = `Could not set ${setting} to ${value}: ${e.message}`;
        errors.push({ ...steps.at, message });
        errors.push({ ...rpm.at, message });
      }
    }

    return { errors, updatesApplied: [rpm, steps] };
  },
  // Max motor current
  async (values, device): Promise<CustomSetResponse> => {
    if (!hasFields(values.motorCharacteristics, ['ratedCurrent'])) { return {} }
    const rated = values.motorCharacteristics.ratedCurrent;
    const motorCurrentMax = rated.value * RATED_TO_MAX_CURRENT;
    try {
      await device.settings.set('motor.current.max', motorCurrentMax);
    } catch (e) {
      throwUnexpectedError(e);
      const message = `Could not set motor.current.max to ${motorCurrentMax}: ${e.message}`;
      return { errors: [{ ...rated.at, message }] };
    }
    return { updatesApplied: [rated] };
  },
  // motor run current
  async (values, device, info): Promise<CustomSetResponse> => {
    if (!hasFields(values.motorCharacteristics, ['ratedCurrent', 'runFactor'])) { return {} }
    const rated = values.motorCharacteristics.ratedCurrent;
    const factor = values.motorCharacteristics.runFactor;
    const driverCurrentRun = calculateDriverCurrentRun(values.motorCharacteristics);
    try {
      await device.settings.set('driver.current.run', driverCurrentRun);
      if (info.axis != null && hasSetting('driver.current.approach', info.axis)) {
        await device.settings.set('driver.current.approach', driverCurrentRun);
      }
    } catch (e) {
      throwUnexpectedError(e);
      const message = `Could not set driver.current.run to ${driverCurrentRun}: ${e.message}`;
      return { errors: [{ ...rated.at, message }, { ...factor.at, message }] };
    }
    return { updatesApplied: [rated, factor] };
  },
  // motor hold current
  async (values, device): Promise<CustomSetResponse> => {
    if (!hasFields(values.motorCharacteristics, ['ratedCurrent', 'holdFactor'])) { return {} }
    const rated = values.motorCharacteristics.ratedCurrent;
    const factor = values.motorCharacteristics.holdFactor;
    const driverCurrentHold = calculateDriverCurrentHold(values.motorCharacteristics);
    try {
      await device.settings.set('driver.current.hold', driverCurrentHold);
    } catch (e) {
      throwUnexpectedError(e);
      const message = `Could not set driver.current.hold to ${driverCurrentHold}: ${e.message}`;
      return { errors: [{ ...rated.at, message }, { ...factor.at, message }] };
    }
    return { updatesApplied: [rated, factor] };
  },
  // Back EMF
  async (values, device): Promise<CustomSetResponse> => {
    if (!hasFields(values.motorCharacteristics, ['ratedCurrent', 'holdingTorque', 'stepsPerRotation'])) { return {} }
    const holdingTorque = values.motorCharacteristics.holdingTorque;
    const rated = values.motorCharacteristics.ratedCurrent;
    const steps = values.motorCharacteristics.stepsPerRotation;
    const torqueConstant = holdingTorque.value / (rated.value * Math.SQRT2);
    const electricalCyclesPerRev = steps.value / 4;
    const backEmf = torqueConstant / electricalCyclesPerRev;
    const backEmfNativeUnits = backEmf * 10_000_000 * Math.PI / 16_384;
    try {
      await device.settings.set('motor.ke', backEmfNativeUnits);
    } catch (e) {
      throwUnexpectedError(e);
      const message = `Could not set motor.ke to ${backEmfNativeUnits}: ${e.message}`;
      return { errors: [{ ...holdingTorque.at, message }, { ...rated.at, message }, { ...steps.at, message }] };
    }
    return { updatesApplied: [holdingTorque, rated, steps] };
  },
  // Unit conversions
  async (values, comms): Promise<CustomSetResponse> => {
    if (!hasFields(values.motion, ['unitConversion', 'unitConversionEnabled'])) { return {} }
    const enabled = values.motion.unitConversionEnabled.value === 1;
    const conversionField = values.motion.unitConversion;

    try {
      const axis = getAxis(comms);
      const { axisType } = axis.identity;

      if (!enabled || conversionField.value === 0 || axisType === ascii.AxisType.UNKNOWN) {
        await axis.settings.setCustomUnitConversions([]);
      } else if (axis.identity.axisType === ascii.AxisType.LINEAR) {
        const factor = 1 / conversionField.value;
        await axis.settings.setCustomUnitConversions([
          { setting: 'pos', value: factor, unit: Length.mm },
          { setting: 'maxspeed', value: factor / TIME_CONSTANT, unit: Velocity.MILLIMETRES_PER_SECOND },
          { setting: 'accel', value: factor * FW_TICK_RATE / TIME_CONSTANT, unit: Acceleration.MILLIMETRES_PER_SECOND_SQUARED },
        ]);
      } else if (axis.identity.axisType === ascii.AxisType.ROTARY) {
        const factor = 360 / conversionField.value;
        await axis.settings.setCustomUnitConversions([
          { setting: 'pos', value: factor, unit: Angle.DEGREES },
          { setting: 'maxspeed', value: factor / TIME_CONSTANT, unit: AngularVelocity.DEGREES_PER_SECOND },
          { setting: 'accel', value: factor * FW_TICK_RATE / TIME_CONSTANT, unit: AngularAcceleration.DEGREES_PER_SECOND_SQUARED },
        ]);
      }
    } catch (e) {
      throwUnexpectedError(e);
      const message = `Could not set custom unit conversions: ${e.message}`;
      return { errors: [{ ...conversionField.at, message }] };
    }
    return { updatesApplied: [values.motion.unitConversion, values.motion.unitConversionEnabled] };
  },
];


const postSetValidators: FormValidator[] = [
  async (values, device): Promise<ValidatorResponse> => {
    const encoder1 = values.encoder1;
    const encoder2 = values.encoder2;
    const motorType =
      encoder1?.motorType ? encoder1.motorType.value :
      encoder2?.motorType ? encoder2.motorType.value :
      undefined;

    if (motorType === 1 || motorType === 3) {
      const e1On = encoderOn(encoder1);
      const e2On = encoderOn(encoder2);
      if (!e1On && !e2On) { return null }

      const defaultPort = e1On ? 1 : 2;
      try {
        await device.settings.set('encoder.port.default', defaultPort);
      } catch (e) {
        throwUnexpectedError(e);
        return [{ sectionId: `encoder${defaultPort}`, fieldId: 'mode', message: `Could not set default encoder port: ${e.message}` }];
      }
    } else if (motorType === 2) {
      try {
        await device.settings.set('encoder.port.default', 2);
      } catch (e) {
        throwUnexpectedError(e);
        return [
          { sectionId: 'encoder1', fieldId: 'mode', message: `Could not set default encoder port: ${e.message}` },
          { sectionId: 'encoder2', fieldId: 'mode', message: `Could not set default encoder port: ${e.message}` },
        ];
      }
    }
    return null;
  },
  // Set Closed Loop Controller Type
  async (_, device): Promise<ValidatorResponse> => {
    let calcCloopControllerType = 0;
    try {
      calcCloopControllerType = await device.settings.get('cloop.controller.type.calc');
    } catch (e) {
      throwUnexpectedError(e);
      if (e instanceof CommandFailedException && e.details.responseData === 'BADCOMMAND') {
        return null;
      }
      return { topLevelError: `Unable to calculate a target controller: ${e.message}` };
    }

    if (calcCloopControllerType === 0) {
      return {
        topLevelError: 'Unable to determine an appropriate target controller (cloop.controller.type) for the current configuration.'
      };
    }

    try {
      await device.settings.set('cloop.controller.type', calcCloopControllerType);
      return null;
    } catch (e) {
      throwUnexpectedError(e);
      return { topLevelError: `Unable to set target controller (cloop.controller.type) to the proper value: ${e.message}` };
    }
  },
  async (values, device): Promise<ValidatorResponse> => {
    const encoder1 = values.encoder1;
    const encoder2 = values.encoder2;
    if (encoder1 == null && encoder2 == null) { return null }

    const cloopEnabled = await device.settings.get('cloop.enable');
    const cloopServoEnabled = await device.settings.get('cloop.servo.enable');
    const cloopEnabledShouldBe = encoderOn(encoder1) || encoderOn(encoder2) ? 1 : 0;
    if (cloopEnabled !== cloopEnabledShouldBe) {
      try {
        await device.settings.set('cloop.enable', cloopEnabledShouldBe);
      } catch (e) {
        throwUnexpectedError(e);
        const topLevelError = cloopEnabledShouldBe === 1 ?
          `Error enabling closed-loop mode: ${e.message}` :
          `Error disabling closed-loop mode: ${e.message}`;
        return { topLevelError };
      }
    }
    if (cloopServoEnabled !== cloopEnabledShouldBe) {
      try {
        await device.settings.set('cloop.servo.enable', cloopEnabledShouldBe);
      } catch (e) {
        throwUnexpectedError(e);
        const topLevelError = cloopEnabledShouldBe === 1 ?
          `Error enabling servo mode: ${e.message}` :
          `Error disabling servo mode: ${e.message}`;
        return { topLevelError };
      }
    }
    return null;
  },
  async (values, device): Promise<ValidatorResponse> => {
    const motorCharacterSection = values.motorCharacteristics;
    if (motorCharacterSection == null) { return null }

    const setNumberValue = async (storageKey: string, fieldId: FieldIds<'motorCharacteristics'>): Promise<ValidationError[]> => {
      const field = motorCharacterSection[fieldId];
      if (!field) {
        return [];
      }
      try {
        await device.storage.setNumber(storageKey, field.value);
        return [];
      } catch (e) {
        return [{ sectionId: 'motorCharacteristics', fieldId, message: `Error setting ${storageKey}=${field.value}: ${makeString(e)}` }];
      }
    };

    try {
      await device.storage.setBool(SETUP_COMPLETE_KEY, true);
      return [
        await setNumberValue(RPM_KEY, 'rpm'),
        await setNumberValue(STEPS_PER_ROTATION_KEY, 'stepsPerRotation'),
        await setNumberValue(HOLDING_TORQUE_KEY, 'holdingTorque'),
        await setNumberValue(RATED_CURRENT_KEY, 'ratedCurrent'),
        await setNumberValue(RUN_FACTOR_KEY, 'runFactor'),
        await setNumberValue(HOLD_FACTOR_KEY, 'holdFactor'),
      ].flat();
    } catch (e) {
      throwUnexpectedError(e);
      return { topLevelError: `Error Saving 3rd Party Peripheral information: ${e.message}` };
    }
  },
  async (values, axis): Promise<ValidatorResponse> => {
    if (!('device' in axis)) { return null }
    if (values.motorCharacteristics?.ictrlType == null) { return null }

    try {
      const api = getContainer().get(CurrentTunerAPI);
      const fwVersion = axis.device.firmwareVersion;
      const currentTuningInfo = await api.getInfo(fwVersion);
      const inputSettings = await resolveInputSettings(axis, currentTuningInfo.inputSettings);

      const ictrlSettings = await api.getCurrentControllerParams(inputSettings, fwVersion, currentTuningInfo.defaultSliderPositions);
      for (const setting of ictrlSettings) {
        await axis.settings.set(setting.name, setting.value, setting.units);
      }
    } catch (e) {
      throwUnexpectedError(e);
      return { topLevelError: `Failed to set current tuning: ${e}` };
    }
    return null;
  },
];

export async function runPostSetValidators(values: NonNullValues, device: AsciiComms) {
  return runValidators(postSetValidators, values, device);
}
