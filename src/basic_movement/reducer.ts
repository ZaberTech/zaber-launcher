import _ from 'lodash';

import { createReducer, changeDictionary } from '../utils';
import { EntityKey, extractConnectionKey, removeWithKey, tryExtractConnectionKey } from '../keys';
import {
  ConnectionManagerActionTypes, ConnectionManagerActionPayloads, DevicesLoadedPayload, IdentifiedAxisInfo, isIdentified,
} from '../connection_manager';
import type { Measurement } from '../units';
import { isMobile } from '../devices';
import { ConnectionStatusActionPayloads, ConnectionStatusActionTypes } from '../connection_status';

import { ActionsToPayloads, ActionTypes } from './actions';
import { Initiator, AxisError, TravelInfo, MovementType } from './types';

export interface PreciseMovementValues {
  absolute: Measurement | null;
  relative: Measurement | null;
  velocity: Measurement | null;
}

export interface AxisState {
  key: EntityKey;

  movementRequests: number;
  errors: AxisError[];
  warningFlags: string[];

  position: Measurement;
  travelInfo: TravelInfo | null;
  isPinned: boolean;
  lastInitiator: Initiator | null;
  ignoreInterruptFlag: boolean;

  preciseMovement: PreciseMovementValues;
  expanded: boolean;
}

function defaultAxisState(axis: IdentifiedAxisInfo, previousState: AxisState): AxisState {
  return {
    key: axis.key,
    movementRequests: 0,
    errors: [],
    warningFlags: [],

    position: { value: null, units: 'default' },
    travelInfo: null,
    isPinned: previousState ? previousState.isPinned : false,
    lastInitiator: null,
    ignoreInterruptFlag: false,

    preciseMovement: {
      absolute: null,
      relative: null,
      velocity: null,
    },
    expanded: false,
  };
}

export interface State {
  selectedKey: EntityKey | null;
  axes: Record<string, AxisState>;
}

const initialState: State = {
  selectedKey: null,
  axes: {},
};

const moveAxis = (state: State, { axis, initiator, moveType }: ActionsToPayloads[ActionTypes.MOVE_AXIS]): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axis, axisState => ({
      ...axisState,
      lastInitiator: initiator,
      ignoreInterruptFlag: moveType === MovementType.Stop,
      movementRequests: axisState.movementRequests + 1,
      errors: axisState.errors.filter(err => err.origin !== 'movement'),
    })),
  });

const moveAxisDone = (state: State, { axis, errors }: ActionsToPayloads[ActionTypes.MOVE_AXIS_DONE]): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axis, axisState => ({
      ...axisState,
      lastInitiator: axisState.movementRequests <= 1 ? null : axisState.lastInitiator,
      movementRequests: Math.max(axisState.movementRequests - 1, 0),
      errors: errors ? [...axisState.errors, ...errors] : axisState.errors,
    })),
  });

const selectKey = (state: State, { key }: ActionsToPayloads[ActionTypes.SELECT_KEY]): State =>
  ({
    ...state,
    selectedKey: key,
  });

const axisMonitoringData = (state: State, { axis, ...data }: ActionsToPayloads[ActionTypes.AXIS_MONITORING_DATA]): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axis, data),
  });

const changeUnits = (state: State, { axis, units, position }: ActionsToPayloads[ActionTypes.CHANGE_UNITS]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axis, axisState => ({
    ...axisState,
    position: { value: position, units },
  })),
});

const dismissError = (state: State, { axis, index }: ActionsToPayloads[ActionTypes.DISMISS_ERROR]): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axis, axisState => ({
      ...axisState,
      errors: axisState.errors.filter((err, i) => i !== index),
    })),
  });

const updateErrors = (state: State, { axis, origin, errors }: ActionsToPayloads[ActionTypes.UPDATE_ERRORS]): State =>
  ({
    ...state,
    axes: changeDictionary(state.axes, axis, axisState => ({
      ...axisState,
      errors: [...errors, ...axisState.errors.filter(err => err.origin !== origin)],
    })),
  });

const devicesLoaded = (state: State, { devices, connectionKey }: DevicesLoadedPayload): State => {
  const identified = devices.filter(isIdentified);
  return ({
    ...state,
    axes: {
      ...removeWithKey(state.axes, extractConnectionKey, connectionKey),
      ..._.keyBy(
        _.flatMap(identified, device => device.axes
          .filter(axis => isMobile(axis))
          .map(axis => defaultAxisState(axis, state.axes[axis.key]))
        ),
        'key'
      ),
    }
  });
};

type DisconnectPayload = ConnectionStatusActionPayloads[ConnectionStatusActionTypes.ROUTED_CONNECTION_DISCONNECT];
const connectionDisconnect = (state: State, { connectionKey }: DisconnectPayload): State =>
  ({
    ...state,
    selectedKey: tryExtractConnectionKey(state.selectedKey) === connectionKey ? null : state.selectedKey,
    axes: _.mapValues(state.axes, axis => ({
      ...axis,
      isPinned: extractConnectionKey(axis.key) === connectionKey ? false : axis.isPinned,
    }))
  });

const pinAxis = (state: State, { axis }: ActionsToPayloads[ActionTypes.PIN_AXIS]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axis, { isPinned: true }),
});

const unpinAxis = (state: State, { axis }: ActionsToPayloads[ActionTypes.UNPIN_AXIS]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axis, { isPinned: false }),
});

const addWarningFlags = (state: State, { axis, flags }: ActionsToPayloads[ActionTypes.ADD_WARNING_FLAGS]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axis, axis => ({ ...axis, warningFlags: [...axis.warningFlags, ...flags] })),
});

const clearIgnoreInterrupt = (state: State, { axis }: ActionsToPayloads[ActionTypes.CLEAR_IGNORE_INTERRUPT]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axis, { ignoreInterruptFlag: false }),
});

const didMount = (state: State): State => ({
  ...state,
  axes: _.mapValues(state.axes, axis => ({
    ...axis,
    travelInfo: null,
  }))
});


const setPreciseMeasurement =
  (state: State, { axis, movementType, measurement }: ActionsToPayloads[ActionTypes.SET_PRECISE_MEASUREMENT]): State => ({
    ...state,
    axes: changeDictionary(state.axes, axis, axis => ({
      ...axis,
      preciseMovement: {
        ...axis.preciseMovement,
        [movementType]: measurement,
      }
    })),
  });

const setCardExpanded = (state: State, { axis, expanded }: ActionsToPayloads[ActionTypes.SET_CARD_EXPANDED]): State => ({
  ...state,
  axes: changeDictionary(state.axes, axis, axis => ({
    ...axis,
    expanded,
  })),
});

type Payloads = ActionsToPayloads & ConnectionManagerActionPayloads & ConnectionStatusActionPayloads;
export const reducer = createReducer<Payloads, typeof initialState>({
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
  [ConnectionStatusActionTypes.ROUTED_CONNECTION_DISCONNECT]: connectionDisconnect,
  [ActionTypes.MOVE_AXIS]: moveAxis,
  [ActionTypes.MOVE_AXIS_DONE]: moveAxisDone,
  [ActionTypes.SELECT_KEY]: selectKey,
  [ActionTypes.AXIS_MONITORING_DATA]: axisMonitoringData,
  [ActionTypes.CHANGE_UNITS]: changeUnits,
  [ActionTypes.UPDATE_ERRORS]: updateErrors,
  [ActionTypes.DISMISS_ERROR]: dismissError,
  [ActionTypes.PIN_AXIS]: pinAxis,
  [ActionTypes.UNPIN_AXIS]: unpinAxis,
  [ActionTypes.ADD_WARNING_FLAGS]: addWarningFlags,
  [ActionTypes.CLEAR_IGNORE_INTERRUPT]: clearIgnoreInterrupt,
  [ActionTypes.DID_MOUNT]: didMount,
  [ActionTypes.SET_PRECISE_MEASUREMENT]: setPreciseMeasurement,
  [ActionTypes.SET_CARD_EXPANDED]: setCardExpanded,
}, initialState);
