import React from 'react';
import { NoticeBanner, Text } from '@zaber/react-library';

import { OneTimeMessage } from '../help';
import { ExternalLink } from '../components';

import { tuningGuidelineUrl } from './types';

export const TunerOneTimeMessage: React.FC = () => {
  const tuningGuidelines = <ExternalLink url={tuningGuidelineUrl}>Tuning Guidelines</ExternalLink>;

  return (
    <OneTimeMessage messageId="tuning-guidelines" message={dismiss => (
      <NoticeBanner type="info" headline="First time tuning?" closer={{ action: dismiss }} className="one-time-message">
        <Text>
          Check out our {tuningGuidelines} page to familiarize yourself with the process and things to pay attention to.
        </Text>
      </NoticeBanner>
    )}/>
  );
};
