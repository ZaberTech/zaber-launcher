import React, { useState } from 'react';
import { Button, ButtonRow, ButtonRowConfirmCancel, ContextMenu, Icons, Modal, Text } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';

import type { EntityKey } from '../keys';
import { SaveLoadContextMenuItem } from '../save_load';
import { AppIconsNeutral } from '../apps';

import { actions as actionsDefinition } from './actions';
import { selectChangeCount, selectDeviceOrAxisInfo, selectAllFieldsIfValid } from './selectors';
import { troubleshootingWiki } from './components/links';
import { is3rdPartyId } from './peripheral_id_picker/peripheralIdMapping';

export const ConfirmationSection: React.FC<{ deviceKey: EntityKey }> = ({ deviceKey }) => {
  const actions = useActions(actionsDefinition);
  const info = useSelector(selectDeviceOrAxisInfo);
  const changeCount = useSelector(selectChangeCount);
  const validatedFields = useSelector(selectAllFieldsIfValid);
  const [showModal, setShowModal] = useState(false);

  return <div className="confirmation">
    <div className="text-lines">
      <Text>Click "Apply" to check the configuration and update your product.</Text>
      <Text>If you're having trouble, check out our {troubleshootingWiki}.</Text>
    </div>
    {info && <ButtonRow>
      <Button
        disabled={validatedFields == null}
        onClick={() => validatedFields != null ? actions.applyValues(deviceKey, validatedFields, info) : null}
      >
        Apply
      </Button>
      <Button disabled={changeCount <= 0} onClick={actions.revertValues} color="grey">Cancel</Button>
      <ContextMenu position="top" align="end">
        {is3rdPartyId(info.axis?.identity.peripheralId) && (
          <ContextMenu.Item onClick={() => setShowModal(true)} icon={<Icons.Edit/>}>
            Reconfigure Peripheral Specs
          </ContextMenu.Item>
        )}
        <SaveLoadContextMenuItem productKey={info.axis ? info.axis.key : info.device.key}/>
      </ContextMenu>
    </ButtonRow>}

    {showModal && info && (
      <Modal
        headerIcon={<AppIconsNeutral.HardwareModification/>}
        headerText="Confirm Reconfiguration"
        hidden={!showModal}
        small
        bodyIcon="warning"
        buttons={
          <ButtonRowConfirmCancel
            confirmText="Reconfigure"
            onConfirm={() => actions.configurePeripheralId(info)}
            onCancel={() => setShowModal(false)}
          />
        }
        onRequestClose={() => setShowModal(false)}
      >
        <Text>Note that reconfiguring the peripheral will revert this peripheral to defaults, clearing any changes you have made.</Text>
      </Modal>
    )}

  </div>;
};
