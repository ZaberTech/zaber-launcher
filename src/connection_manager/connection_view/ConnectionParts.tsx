import React from 'react';
import { useSelector } from 'react-redux';
import { AnimationClasses, ContextMenu, Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { actions as actionsDefinition } from '../actions';
import type { ConnectionState } from '../reducer';
import { connectionStatusActions as connectionStatusActionsDefinition, selectConnections as selectStatus } from '../../connection_status';
import { ConnectionType } from '../../app_components';


interface Props {
  connection: ConnectionState;
}

export const ConnectionContextMenu: React.FC<Props> = ({ connection }) => {
  const connectionStatusActions = useActions(connectionStatusActionsDefinition);
  const actions = useActions(actionsDefinition);
  const isConnected = useSelector(selectStatus)[connection.key]?.connected ?? false;
  return (
    <ContextMenu align="end" onClick={e => e.stopPropagation()}>
      <ContextMenu.Item disabled={connection.loadingDevices}
        icon={<Icons.Refresh title="Refresh Devices"/>}
        onClick={() => actions.loadDevices(connection.key, false)}>
        Refresh Devices
      </ContextMenu.Item>
      {isConnected && <ContextMenu.Item icon={<Icons.Disconnect title="Disconnect"/>}
        onClick={() => connectionStatusActions.routedConnectionDisconnect(connection.key)}>
        {connection.type === ConnectionType.SERIAL_PORT ? 'Close Serial Port' : 'Disconnect'}
      </ContextMenu.Item>}
    </ContextMenu>
  );
};

export const LoadingDevicesIcon: React.FC<Props> = ({ connection }) => {
  if (!connection.loadingDevices) {
    return <div className="refresh-spacer"/>;
  }
  return <>
    <div className="spacer"/>
    <Icons.Refresh className={AnimationClasses.Rotation} activated/>
  </>;
};
