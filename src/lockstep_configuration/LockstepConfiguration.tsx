import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useActions } from '@zaber/toolbox/lib/redux';
import { Button, Icons, NoticeBanner, Text } from '@zaber/react-library';

import { ConnectionsView, deviceNameWithLabel } from '../connection_manager';
import { NoContentMessage, Title } from '../components';
import { getConnectionId } from '../keys';
import NoLockstepGroupSvg from '../assets/no_lockstep_group.svg';

import { actions as actionsDefinition } from './actions';
import {
  DeviceStateLockstep,
  selectAxesSorted,
  selectCreationModal,
  selectDeviceStateLockstep,
  selectError,
  selectLockstepData
} from './selectors';
import { LockstepContainer } from './LockstepContainer';
import { CreationModal } from './CreationModal';

export const ErrorElements: React.FC<{error: string}> = ({ error }) => {
  let instruction = '';
  if (error.includes('CommandFailedException')) {
    instruction = 'Ensure that you are only trying to lockstep peripherals that use a stepper motor \
      (not direct drive, voice coil, or brushless).';
  } else if (error.includes('ConnectionFailedException')) {
    instruction = 'Make sure that device is connected, then refresh the connection and try again.';
  }
  return <>
    <Text>{error}</Text><br/><br/>
    <Text t={Text.Type.Instruction}>{instruction}</Text>
  </>;
};

export const DeviceHeader: React.FC<{device: DeviceStateLockstep}> = ({ device }) => (
  <>
    <Icons.Usb/>
    <Text e={Text.Emphasis.Bold} t={Text.Type.H4}>{getConnectionId(device.key)}</Text>
    <Icons.ArrowCollapsed/>
    <Text e={Text.Emphasis.Bold} t={Text.Type.H4}>{deviceNameWithLabel(device)}</Text>
  </>
);

export const CreateLockstepButton: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const device = useSelector(selectDeviceStateLockstep);
  const { free } = useSelector(selectAxesSorted);
  const disableCreation = free.length < 2;
  return device && <Button
    className="button-open-modal"
    disabled={disableCreation}
    onClick={actions.beginCreation}
  >
    Create Lockstep Group
  </Button>;
};

const TITLE = 'Welcome to Lockstep Configuration!';

const NoSelection = <NoContentMessage
  title={TITLE}
  message="Select a device to configure its lockstep groups."
/>;
const Loading = <NoContentMessage
  title={TITLE}
  message="Loading Lockstep Information..."
  type="working"/>;

const LockstepConfigAppUI: React.FC<{device: DeviceStateLockstep}> = ({ device }) => {
  const error = useSelector(selectError);
  const lockstepData = useSelector(selectLockstepData);
  const dialogState = useSelector(selectCreationModal);

  if (error?.blocking) {
    return <NoContentMessage title={TITLE} type="error" message={error.message}/>;
  } else if (lockstepData == null || device.connection.loadingDevices) {
    return dialogState == null ? Loading : null;
  } else if (device.locksteps.length > 0) {
    return <>
      <LockstepContainer header={<DeviceHeader device={device}/>} device={device} lockstepData={lockstepData}/>
      { error && <NoticeBanner>
        <ErrorElements error={error.message}/>
      </NoticeBanner>}
      <CreateLockstepButton/>
    </>;
  } else {
    return <div className="no-lockstep-group">
      <NoLockstepGroupSvg className="no-lockstep-svg"/>
      <div>You have no lockstep group created yet.</div>
      <CreateLockstepButton/>
    </div>;
  }
};

export const LockstepConfiguration: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const device = useSelector(selectDeviceStateLockstep);

  useEffect(() => {
    actions.updateFetchedAxisDataRecord();
  }, [device?.key]);

  return (
    <div className="connection-view-and-app">
      <ConnectionsView
        selectable={['device']}
        selected={device?.key ?? null}
        onSelect={actions.setSelectedDevice}
        showAxes={true}
      />
      <div className="app-ui lockstep-configuration">
        <Title>Lockstep Configuration</Title>
        {(device) ? <LockstepConfigAppUI device={device}/> : NoSelection}
        <CreationModal data-testid="creation-modal"/>
      </div>
    </div>
  );
};
