/* eslint-disable camelcase */
import _ from 'lodash';
import { Length, microscopy, Percent, Time, Units, Velocity } from '@zaber/motion';

import type { EntityKey } from '../keys';
import { getDimensionName, type MeasurementOK } from '../units';
import { ComparisonOperatorSymbol } from '../utils';

export const PART_KEY = 'zaber.microscope.part';
export const ALT_PART_KEY_PREFIX = `${PART_KEY}.`;
export const INDEX_POS_KEY_PREFIX = 'zaber.motion.index.label.';
export const FOCUS_OFFSET_KEY_PREFIX = 'zaber.microscope.focus_offset.';
export const TRIGGER_SETTINGS_PREFIX = 'zaber.microscope.trigger.';
export const AXIS_INVERTED_KEY = 'zaber.microscope.axis_inverted';
/** The device ID of X-MOR is hard-coded (same as ZML) because of the specificity of the hardware. */
export const X_MOR_DEVICE_ID = [53115];
export const WDI_CURRENT_OBJECTIVE_REG = 50;

export const POSITION_DIMENSION = getDimensionName(Length.mm)!;
export const VELOCITY_DIMENSION = getDimensionName(Velocity['mm/s'])!;
export const PERCENT_DIMENSION = getDimensionName(Percent.PERCENT)!;
export const TIME_DIMENSION = getDimensionName(Time.ms)!;

/** Represents information as if detected by ZML. Only use in config dialog. */
export interface DetectableConfig {
  filterChanger: { key: EntityKey } | { error: string } | null;
  x: AxisConfig | { error: string } | null;
  y: AxisConfig | { error: string } | null;
  focus: AxisConfig | { error: string } | null;
  cameraTrigger: TriggerConfig | { error: string } | null;
}

export interface AdditionalConfig {
  autofocus: AutofocusConfig | null;
}

export interface AutofocusConfig {
  provider: 'wdi';
  host: string;
  port: number | null;
}

export type MicroscopeConfig = DetectableConfig & AdditionalConfig;

interface AxisConfig { key: EntityKey; inverted: boolean }
export interface TriggerConfig { key: EntityKey; digitalOutput: number | null }

export const AXES = ['focusAxis', 'xAxis', 'yAxis'] as const;
export type Axes = typeof AXES[number];
export const AXES_XYZ: readonly Axes[] = ['xAxis', 'yAxis', 'focusAxis'];
export const AXES_LABELS: Record<Axes, string> = {
  focusAxis: 'Focus',
  xAxis: 'X',
  yAxis: 'Y',
};

export type ObjectiveParameterKeys = 'control_loop_kp' | 'control_loop_ki' | 'magnification' | 'max_exposure' | 'signal_to_noise_ratio';
export type ObjectiveParameters = Record<ObjectiveParameterKeys, number | null>;

export const OBJECTIVE_PARAMETER_LABELS = {
  magnification: 'Magnification',
  control_loop_kp: 'Control Loop Kp',
  control_loop_ki: 'Control Loop Ki',
  max_exposure: 'Max Exposure',
  signal_to_noise_ratio: 'Signal to Noise Ratio',
};

export const EMPTY_OBJECTIVE_PARAMETERS: ObjectiveParameters = {
  magnification: 0,
  control_loop_kp: 0,
  control_loop_ki: 0,
  max_exposure: null,
  signal_to_noise_ratio: null,
};

/** Represents microscope information that does not change as detected by ZML. */
export interface MonitorStableInfo {
  filterChanger: {
    numberOf: number;
    labels: string[];
    hasEncoder: boolean;
  } | null;
  objectiveChanger: {
    numberOf: number;
    labels: string[];
    focusOffsets: MeasurementOK[];
    focusDatum: number;
  } | null;
  illuminator: {
    channelCount: number;
    channels: Record<number, {
      axisKey: EntityKey;
      channel: number;
      wavelength: number;
      name: string;
    }>;
  } | null;
  xAxis: StableAxisInfo | null;
  yAxis: StableAxisInfo | null;
  focusAxis: StableAxisInfo | null;
  autofocus: {
    text: string;
  } | null;
  cameraTrigger: {
    device: number;
    channel: number;
  } | null;
}

interface StableAxisInfo {
  nativeStepSize: number;
  min: number;
  max: number;
  maxSpeed: number;
  inverted: boolean;
}

export interface MonitorInfo {
  needsInitialization: boolean;
  filterChanger: {
    // 1-based index of the filter
    filter: number;
  } | null;
  objectiveChanger: {
    // 1-based index of the objective
    objective: number;
  } | null;
  illuminator: {
    channels: Record<number, {
      channel: number;
      on: boolean;
      intensity: number;
    }>;
  } | null;
  xAxis: { position: MeasurementOK } | null;
  yAxis: { position: MeasurementOK } | null;
  focusAxis: { position: MeasurementOK } | null;
  autofocus: {
    inRange: boolean;
    inFocus: boolean;
    laserOn: boolean;
    limits: { min: MeasurementOK; max: MeasurementOK };
    providerObjective: number | null;
  } | null;
}

export enum Operations {
  initialize = 'initialize',
  restart = 'restart',
  changeFilter = 'changeFilter',
  changeObjective = 'changeObjective',
  channelOn = 'channelOn',
  channelSetIntensity = 'channelSetIntensity',
  moveX = 'moveX',
  moveY = 'moveY',
  moveFocus = 'moveFocus',
  moveAbsolute = 'moveAbsolute',
  moveToStored = 'moveToStored',
  filterChangerSettings = 'filterChangerSettings',
  objectiveChangerSettings = 'objectiveChangerSettings',
  autofocusZero = 'autofocusZero',
  autofocusFocusOnce = 'autofocusFocusOnce',
  autofocusFocusLoop = 'autofocusFocusLoop',
  autofocusGetSettings = 'autofocusGetSettings',
  autofocusSetSettings = 'autofocusSetSettings',
  autofocusStop = 'autofocusStop',
  autofocusLaserOnOff = 'autofocusLaserOnOff',
  autofocusSetLimit = 'autofocusSetLimit',
  triggerCamera = 'triggerCamera',
  saveTriggerSettings = 'saveTriggerSettings',
}

type MovementArgs = { positive: boolean; move: 'steady' | 'gradual' };

export interface OperationsArgs {
  [Operations.initialize]: void;
  [Operations.restart]: void;
  [Operations.changeFilter]: { filter: number; home?: boolean };
  [Operations.changeObjective]: { objective: number };
  [Operations.channelOn]: { channel: number; on: boolean };
  [Operations.channelSetIntensity]: { channel: number; intensity: number };
  [Operations.moveX]: MovementArgs;
  [Operations.moveY]: MovementArgs;
  [Operations.moveFocus]: MovementArgs;
  [Operations.moveAbsolute]: { axis: Axes; position: MeasurementOK };
  [Operations.moveToStored]: { stored: StoredPosition; axes: Axes[] };
  [Operations.filterChangerSettings]: { labels: string[] };
  [Operations.objectiveChangerSettings]: { labels: string[]; focusOffsets: MeasurementOK[] };
  [Operations.autofocusZero]: void;
  [Operations.autofocusFocusOnce]: { scan?: boolean };
  [Operations.autofocusFocusLoop]: void;
  [Operations.autofocusStop]: void;
  [Operations.autofocusGetSettings]: void;
  [Operations.autofocusSetSettings]: { objectives: ObjectiveParameters[] };
  [Operations.autofocusLaserOnOff]: { on: boolean };
  [Operations.autofocusSetLimit]: { limit: 'min' | 'max'; position: MeasurementOK };
  [Operations.triggerCamera]: void;
  [Operations.saveTriggerSettings]: { settings: TriggerSettings };
}

export interface OperationsResults {
  [Operations.autofocusGetSettings]: { objectives: ObjectiveParameters[] };
}

export type OperationsResult<T extends keyof OperationsArgs> = T extends keyof OperationsResults ? OperationsResults[T] : never;

export type OperationState = { error: string } | { inProgress: true } | { result: unknown } | null;

export function isOperationError(state: OperationState | undefined): state is { error: string } {
  return state != null && 'error' in state;
}
export function isOperationInProgress(state: OperationState | undefined): boolean {
  return state != null && 'inProgress' in state;
}
export function getOperationResult<T extends keyof OperationsArgs>(
  state: OperationState | undefined,
  _operation: T, // eslint-disable-line @typescript-eslint/no-unused-vars
): OperationsResult<T> | null {
  return state != null && 'result' in state ? state.result as OperationsResult<T> : null;
}

export const ILLUMINATOR_MAX_CHANNELS = 4 as const;
export const ILLUMINATOR_CHANNELS = _.range(1, ILLUMINATOR_MAX_CHANNELS + 1);

export const MONITOR_DELAY = 1000;
export const MONITOR_BURST_DELAY = 50;

export type TestType = 'initialize' | 'full' | 'force-home';

export type StoredPosition = Record<Axes, MeasurementOK> & {
  label: string;
};

export const TEST_END = 'MICROSCOPE_TEST_END';

export const AUTOFOCUS_STORAGE_KEY = 'microscopeAutofocus_';

export interface SettingsValidator {
  component: Components;
  setting: string;
  comparison: ComparisonOperatorSymbol;
  value: number;
  unit: Units;
  precondition?: (scope: microscopy.Microscope) => boolean;
}

export const SETTINGS_VALIDATORS: SettingsValidator[] = [
  {
    component: 'focusAxis',
    setting: 'limit.hardstop.retraction', comparison: '<=', value: 1, unit: Length.mm,
    precondition: ({ objectiveChanger, focusAxis }) =>
      objectiveChanger != null && focusAxis?.device.identity.name.startsWith('X-LDA') === true,
  },
];

export type Components = keyof typeof COMPONENT_NAMES;

export const COMPONENT_NAMES = {
  focusAxis: 'Focus axis',
  illuminator: 'Illuminator',
  xAxis: 'X axis',
  yAxis: 'Y axis',
  objectiveChanger: 'Objective changer',
  filterChanger: 'Filter changer',
};

export enum Paths {
  Root = '/microscope',
  Overview = '/microscope/overview',
  MultiDAcq = '/microscope/multidacq',
}

export interface TriggerSettings {
  exposure: MeasurementOK;
  waitBefore: MeasurementOK;
  waitAfter: MeasurementOK;
}
export const TRIGGER_SETTINGS_FIELDS = ['exposure', 'waitBefore', 'waitAfter'] as const;
export const TRIGGER_SETTINGS_LABELS = {
  exposure: 'Exposure (Pulse Width)',
  waitBefore: 'Wait before Pulse',
  waitAfter: 'Wait after Pulse',
};

export const EMPTY_TRIGGER_SETTINGS: TriggerSettings = {
  exposure: { value: 0, units: Time.ms },
  waitBefore: { value: 0, units: Time.ms },
  waitAfter: { value: 0, units: Time.ms },
};
