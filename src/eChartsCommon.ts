import { Colors } from '@zaber/react-library';

// Definitions common to eCharts usage (oscilloscope and PVT apps).


export const ECHARTS_YAXIS_FONT_SIZE = 12;

export const ECHARTS_AXIS_NAME_TEXT_STYLE = {
  nameTextStyle: {
    color: Colors.zaberGrey,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
    fontSize: ECHARTS_YAXIS_FONT_SIZE,
    rich: {
      u: {
        color: Colors.zaberGrey,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        fontSize: ECHARTS_YAXIS_FONT_SIZE,
      }
    },
  },
};

export const ECHARTS_X_AXIS_LABEL_TEXT_STYLE = {
  ...ECHARTS_AXIS_NAME_TEXT_STYLE,
  axisLabel: {
    color: Colors.lightGrey,
    fontFamily: 'Roboto',
  },
};

export const ECHARTS_Y_AXIS_LABEL_TEXT_STYLE = {
  ...ECHARTS_AXIS_NAME_TEXT_STYLE,
  axisLabel: {
    color: Colors.lightGrey,
    fontFamily: 'Roboto',
    showMinLabel: false,
    showMaxLabel: false,
  },
};

export const ECHARTS_GRID_LINE_STYLE = {
  splitLine: {
    lineStyle: {
      color: Colors.paleGrey,
      width: 1,
      type: 'solid',
    },
  },
};

export const ECHARTS_Y_AXIS_DEFAULTS = {
  ...ECHARTS_Y_AXIS_LABEL_TEXT_STYLE,
  ...ECHARTS_GRID_LINE_STYLE,
  axisLine: {
    lineStyle: {
      color: Colors.lightGrey,
    },
  },
  nameLocation: 'center',
  splitNumber: 8,
  scale: true,
};

export const ECHARTS_X_AXIS_DEFAULTS = {
  ...ECHARTS_X_AXIS_LABEL_TEXT_STYLE,
  ...ECHARTS_GRID_LINE_STYLE,
  type: 'value',
  splitNumber: 10,
  name: 'Time (ms)',
  nameLocation: 'center',
  nameGap: 30,
};

export const ECHARTS_SLIDER_STYLE = {
  borderColor: Colors.zaberGrey,
  backgroundColor: Colors.white,
  handleSize: '200%',
  handleStyle: {
    color: Colors.paleGrey,
    borderColor: Colors.zaberGrey,
  },
  emphasis: {
    handleStyle: {
      color: Colors.zaberGrey,
    },
  },
  dataBackground: {
    lineStyle: {
      color: Colors.zaberGrey,
      opacity: 1,
    },
    areaStyle: {
      color: Colors.paleGrey,
      opacity: 1,
    }
  },
  textStyle: {
    color: Colors.zaberGrey,
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 14,
  },
};

export const ECHARTS_LEGEND_STYLE = {
  selectedMode: false,
  left: 'center',
  icon: 'pin',
  animation: false,
  padding: 10,
  borderColor: Colors.black,
  borderWidth: 1,
};
