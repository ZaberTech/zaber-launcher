import { injectable } from 'inversify';

@injectable()
export class TimeMeasuring {
  public now(): number {
    return performance.now();
  }
}
