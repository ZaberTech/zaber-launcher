import { DeepPartial } from 'redux';

import { actionBuilder } from '../../utils';

import { DoneReport, MultiDAcqConfig, ProgressReport } from './types';

export enum ActionTypes {
  UPDATE_CONFIG = 'MULTIDACQ_UPDATE_CONFIG',
  START = 'MULTIDACQ_START',
  PROGRESS = 'MULTIDACQ_PROGRESS',
  END = 'MULTIDACQ_END',
  ERROR = 'MULTIDACQ_ERROR',
  CANCEL = 'MULTIDACQ_CANCEL',
  RESET = 'MULTIDACQ_RESET',
}

export interface ActionsToPayloads {
  [ActionTypes.UPDATE_CONFIG]: { config: DeepPartial<MultiDAcqConfig> };
  [ActionTypes.START]: void;
  [ActionTypes.PROGRESS]: { progress: ProgressReport };
  [ActionTypes.END]: { report: DoneReport };
  [ActionTypes.ERROR]: { error: string };
  [ActionTypes.CANCEL]: void;
  [ActionTypes.RESET]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  updateConfig: (config: DeepPartial<MultiDAcqConfig>) => buildAction(ActionTypes.UPDATE_CONFIG, { config }),
  start: () => buildAction(ActionTypes.START),
  progress: (progress: ProgressReport) => buildAction(ActionTypes.PROGRESS, { progress }),
  error: (error: string) => buildAction(ActionTypes.ERROR, { error }),
  end: (report: DoneReport) => buildAction(ActionTypes.END, { report }),
  cancel: () => buildAction(ActionTypes.CANCEL),
  reset: () => buildAction(ActionTypes.RESET),
};
