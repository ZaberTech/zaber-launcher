import React from 'react';
import _ from 'lodash';
import { SimpleSelect } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { selectSettingsLists } from './selectors';
import { combineSettingsLists } from './utils';


interface SelectAxisProps {
    axes: number | number[];
    axis: number;
    onChange: (axis: number) => void;
    allowDevice?: boolean;
    title?: string;
  }

export const SelectAxis: React.FC<SelectAxisProps> = ({ axes, axis, onChange, allowDevice, title }) => {
  const options = allowDevice ? [{ value: 0, label: 'Device' }] : [];

  if (typeof axes === 'number') {
    options.push(..._.range(1, axes + 1).map(i => ({ value: i, label: `Axis ${i}` })));
  } else if (typeof axes === 'object') {
    options.push(...axes.map(i => ({ value: i, label: `Axis ${i}` })));
  }

  if (!options.some(option => option.value === axis)) {
    options.push({ value: axis, label: axis === 0 ? 'Device' : `Axis ${axis}` });
  }

  return <SimpleSelect<number>
    title={title ?? 'Select Axis'}
    className="select-axis"
    value={axis}
    isSearchable
    onValueChange={onChange}
    options={options}
  />;
};

interface SelectSettingProps {
  settingsFor: number | 'all';
  setting: string;
  onChange: (setting: string) => void;
  title?: string;
  write?: boolean;
}

export const SelectSetting: React.FC<SelectSettingProps> = ({ settingsFor, setting, onChange, title, write }) => {
  const settingsLists = useSelector(selectSettingsLists);

  const settingsList = settingsFor === 'all' ?
    combineSettingsLists(settingsLists) :
    (settingsLists[settingsFor] ?? {});

  const filteredSettings = Object.entries(settingsList).filter(([_, info]) => !write || info.writeable);
  const options = filteredSettings.map(([name, _]) => ({ value: name, label: name }));

  if (setting !== '' && !options.some(option => option.value === setting)) {
    options.push({ value: setting, label: setting });
  }

  return <SimpleSelect<string>
    title={title ?? 'Select Setting'}
    className="select-setting"
    value={setting}
    isSearchable
    placeholder="Select Setting..."
    onValueChange={onChange}
    options={options}
  />;
};
