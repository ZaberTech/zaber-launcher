import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Button, ButtonPrevious, NoticeMessage, ButtonRow, Flex, ButtonRowConfirmCancel, Text } from '@zaber/react-library';

import type { RootState } from '../store';
import {
  AxisDescription, AxisHeader, ConnectionName,
  ControllerHeader, DEVICE_TABLE_CLASS,
} from '../connection_manager/device_table';
import { ConnectionType } from '../app_components';

import { actions as actionsDefinition } from './actions';
import { selectConfiguration, selectDetectDevices } from './selectors';
import { Steps } from './types';
import { Loading } from './Elements';

interface Props {
  actions: typeof actionsDefinition;
  state: ReturnType<typeof selectDetectDevices>;
  configuration: ReturnType<typeof selectConfiguration>;
}

class DetectDevicesBase extends Component<Props> {
  componentDidMount(): void {
    const { actions } = this.props;
    actions.detectDevices();
  }

  render(): React.ReactNode {
    const { actions, state: { detecting, error, detected }, configuration } = this.props;
    return (
      <div className="detect-devices">
        {detecting && <Loading type="screen"/>}

        {!!error && <>
          <NoticeMessage headline={error.title ?? undefined}>
            <Text>{error.message}</Text>
            <br/>
            <Text>You can still create the connection and resolve the issue later.</Text>
          </NoticeMessage>
          <ButtonRowConfirmCancel
            confirmText="Create Connection"
            cancelText="Try Again"
            onConfirm={this.addToRouter}
            onCancel={actions.detectDevices}
          >
            <ButtonPrevious className="back" onClick={this.goBack}>Back</ButtonPrevious>
            <Flex.Spacer/>
          </ButtonRowConfirmCancel>
        </>}

        {detected && <>
          <h3>Found devices</h3>
          <div className={DEVICE_TABLE_CLASS}>
            <div className="connection">
              <ConnectionName connection={configuration}/>
            </div>
            {detected.map(device => <React.Fragment key={device.key}>
              <div className="controller">
                <ControllerHeader device={device}/>
              </div>
              {device.isController && device.axes.map(axis => (
                <div key={axis.key} className="axis">
                  <AxisHeader axis={axis} device={device}/>
                  <AxisDescription axis={axis} device={device}/>
                </div>
              ))}
            </React.Fragment>)}
          </div>

          <ButtonRow justify="spread">
            <ButtonPrevious onClick={this.goBack}>Back</ButtonPrevious>
            <Button onClick={this.addToRouter}>Create Connection</Button>
          </ButtonRow>
        </>}
      </div>
    );
  }

  addToRouter = () => {
    const { actions } = this.props;
    actions.changeStep(Steps.ADD_TO_ROUTER);
  };

  goBack = () => {
    const { actions, configuration } = this.props;
    actions.changeStep(configuration.type === ConnectionType.SERIAL_PORT ? Steps.CONFIGURE_SERIAL : Steps.CONFIGURE_TCP);
  };
}

export const DetectDevices = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    state: selectDetectDevices(state),
    configuration: selectConfiguration(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(DetectDevicesBase);
