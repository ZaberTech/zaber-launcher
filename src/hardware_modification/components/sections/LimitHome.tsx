import React from 'react';

import { SettingField } from '../fields/SettingField';

import type { ContentProps, HwModSectionTemplate } from './HwModSection';

const LimitHomeSection: React.FC<ContentProps<'limitHome'>> = ({ values }) => {
  const sectionId = 'limitHome';

  return <>
    <SettingField {...{ values, sectionId, fieldId: 'type' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'source' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'edge' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'width' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'bidirectional' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'action' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'posUpdate' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'preset' }}/>
  </>;
};

export const limitHomeTemplate: HwModSectionTemplate<'limitHome'> = {
  sectionId: 'limitHome',
  titleName: 'Home Limit Sensor',
  description: 'Information about the home limit sensor, which is at the minimum travel position.',
  generateContent: values => (<LimitHomeSection values={values}/>),
};
