import { createReducer } from '../utils';
import type { EntityKey } from '../keys';
import type { ConnectionManagerActionPayloads } from '../connection_manager';

import { ActionsToPayloads, ActionTypes } from './actions';
import { AxisConfig, EstopData, ResultError, SetupStep } from './types';

export interface State {
  selectedKey: EntityKey | null;
  loading: boolean;
  config: AxisConfig;
  failedAttempts: number;
  setupModalOpen: boolean;
  setupStep: SetupStep;
  resultError: ResultError | null;
  estop: EstopData;
}

const initialState: State = {
  selectedKey: null,
  loading: true,
  config: {},
  failedAttempts: 0,
  setupModalOpen: false,
  setupStep: SetupStep.NOT_STARTED,
  resultError: null,
  estop: { status: 'ok', ready: true },
};

const selectDeviceOrAxis = (state: State, { deviceOrAxis }: ActionsToPayloads[ActionTypes.SELECT_DEVICE_OR_AXIS]): State => ({
  ...state,
  selectedKey: deviceOrAxis,
});

const loadDeviceOrAxis = (state: State): State => ({
  ...initialState,
  selectedKey: state.selectedKey,
});

const updateConfig = (state: State, { config }: ActionsToPayloads[ActionTypes.UPDATE_CONFIG]): State => ({
  ...state,
  config: { ...state.config, ...config },
});

const setLoaded = (state: State): State => ({
  ...state,
  loading: false,
});

const setSetupModalOpen = (state: State, { open }: ActionsToPayloads[ActionTypes.SET_SETUP_MODAL_OPEN]): State => ({
  ...state,
  setupModalOpen: open,
});

const setSetupStep = (state: State, { step }: ActionsToPayloads[ActionTypes.SET_SETUP_STEP]): State => ({
  ...state,
  setupStep: step,
});

const setResultError = (state: State, resultError: ActionsToPayloads[ActionTypes.SET_RESULT_ERROR]): State => ({
  ...state,
  resultError,
  failedAttempts: state.failedAttempts + 1,
});

const clearResultError = (state: State): State => ({
  ...state,
  resultError: null,
});

const estopData = (state: State, { data }: ActionsToPayloads[ActionTypes.ESTOP_DATA]): State => ({
  ...state,
  estop: data,
});

const enableDriver = (state: State): State => ({
  ...state,
  estop: { status: 'enabling' },
});

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.SELECT_DEVICE_OR_AXIS]: selectDeviceOrAxis,
  [ActionTypes.LOAD_DEVICE_OR_AXIS]: loadDeviceOrAxis,
  [ActionTypes.UPDATE_CONFIG]: updateConfig,
  [ActionTypes.SET_LOADED]: setLoaded,
  [ActionTypes.SET_SETUP_MODAL_OPEN]: setSetupModalOpen,
  [ActionTypes.SET_SETUP_STEP]: setSetupStep,
  [ActionTypes.SET_RESULT_ERROR]: setResultError,
  [ActionTypes.CLEAR_RESULT_ERROR]: clearResultError,
  [ActionTypes.ESTOP_DATA]: estopData,
  [ActionTypes.ENABLE_DRIVER]: enableDriver,
}, initialState);
