import React from 'react';
import _ from 'lodash';
import type { Container } from 'inversify';
import { render, RenderResult } from '@testing-library/react';
import { Length, Units, Voltage } from '@zaber/motion';

import { selectDevices, selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import { mockProduct, mockSingleDevice, mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import type { DeviceState } from '../connection_manager/reducer';
import { createContainer, destroyContainer } from '../container';
import { MessageRoutersService } from '../message_router';
import { wrapWithNewStore, wrapWithRouter } from '../test';

import { addChannelAndDependents } from './channels';
import { autoSelectUnit, selectGroupUnit, groupByDimension } from './dataConversion';
import { Oscilloscope } from './Oscilloscope';
import { MessageRoutersServiceMockIntegrated } from './testing/mocks';
import { Channel, CHANNEL_DEFAULTS, MathChannelNames, UnitConversionCache } from './types';


describe('Channel grouping', () => {
  let container: Container;
  const TestOscilloscope = wrapWithNewStore(wrapWithRouter(Oscilloscope));
  let wrapper: RenderResult;
  let device: DeviceState;

  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockIntegrated);
    wrapper = render(<TestOscilloscope/>);
    mockSingleDevice(TestOscilloscope.testStore);
    device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  it('groups channels by unit dimension', () => {
    let channels = addChannelAndDependents([], 'pos', 1);
    channels = addChannelAndDependents(channels, 'encoder.pos', 1);
    const axisIdentities = selectIdentifiedAxes(TestOscilloscope.testStore.getState());
    const deviceIdentities = selectIdentifiedDevices(TestOscilloscope.testStore.getState());
    const result = groupByDimension(channels, device.key, deviceIdentities, axisIdentities);
    expect(result.Length.map(ch => ch.name).sort()).toEqual(['pos', 'encoder.pos', MathChannelNames.FOLLOW_ERR].sort());
    expect(result.Velocity.map(ch => ch.name).sort())
      .toEqual([MathChannelNames.TRAJECTORY_VEL, MathChannelNames.MEASURED_VEL, MathChannelNames.FOLLOW_ERR_VEL].sort());
    expect(result.Acceleration.map(ch => ch.name).sort())
      .toEqual([MathChannelNames.TRAJECTORY_ACCEL, MathChannelNames.MEASURED_ACCEL, MathChannelNames.FOLLOW_ERR_ACCEL].sort());
  });
});


describe('Unit auto-selection', () => {
  const device = mockProduct();
  const axis = mockProduct({ mobile: true, encoder: true });

  const conversions: UnitConversionCache = {
    contextualDimensionScales: {
      1: 0.000001,
      14: 1,
      15: 0.001,
      16: 0.000001,
    },
    baseDimensionScales: {
      Length: 0.000001,
      Voltage: 1,
    },
    settingContextualDimensions: {
      'pos': 1,
      'encoder.2.cos': 16,
      'encoder.2.sin': 16,
    }
  };


  it('chooses the unit that results in the most human-friendly range', () => {
    const channel: Channel = {
      ...CHANNEL_DEFAULTS,
      title: 'Trajectory Position',
      subtitle: 'pos',
      name: 'pos',
      axis: 1,
      id: 'pos',
      color: '#000000',
      samples: [{ time: 0, value: 0 }, { time: 1, value: 1000000 }]
    };

    let result = autoSelectUnit(channel, device, axis, conversions);
    expect(result).toEqual(Length.MILLIMETRES); // Because default length unit is mm not m.

    channel.samples = [{ time: 0, value: 0 }, { time: 1, value: 1000 }];
    result = autoSelectUnit(channel, device, axis, conversions);
    expect(result).toEqual(Length.MICROMETRES);

    channel.samples = [{ time: 0, value: 0 }, { time: 1, value: 0.001 }];
    result = autoSelectUnit(channel, device, axis, conversions);
    expect(result).toEqual(Length.NANOMETRES);
  });


  it('correctly chooses units for scaled dimensions', () => {
    const channel: Channel = {
      ...CHANNEL_DEFAULTS,
      title: 'Encoder 2 Cosine',
      subtitle: 'encoder.2.cos',
      name: 'encoder.2.cos',
      axis: 1,
      id: 'encoer.2.cos',
      color: '#000000',
      samples: [{ time: 0, value: 1000 }, { time: 1, value: 10000 }]
    };

    let result = autoSelectUnit(channel, device, axis, conversions);
    expect(result).toEqual(Voltage.MILLIVOLTS);

    channel.samples = [{ time: 0, value: 100 }, { time: 1, value: 10 }];
    result = autoSelectUnit(channel, device, axis, conversions);
    expect(result).toEqual(Voltage.MICROVOLTS);

    channel.samples = [{ time: 0, value: 1000000 }, { time: 1, value: 2000000 }];
    result = autoSelectUnit(channel, device, axis, conversions);
    expect(result).toEqual(Voltage.VOLTS);
  });
});


describe('Selecting a common unit for a group', () => {
  it('chooses native if there are no choices', () => {
    expect(selectGroupUnit([])).toEqual(Units.NATIVE);
  });

  it('chooses native if any of the choices are native', () => {
    expect(selectGroupUnit([Length.MILLIMETRES, Units.NATIVE, Length.METRES])).toEqual(Units.NATIVE);
  });

  it('chooses the unit that produces the largest numbers when converted to base units', () => {
    expect(selectGroupUnit([Length.CENTIMETRES, Length.MILLIMETRES, Length.METRES])).toEqual(Length.METRES);
  });
});


describe('Parent unit dimensions are used when axis does not have them', () => {
  let container: Container;
  const TestOscilloscope = wrapWithNewStore(wrapWithRouter(Oscilloscope));
  let wrapper: RenderResult;
  let device: DeviceState;

  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockIntegrated);
    wrapper = render(<TestOscilloscope/>);
    mockSingleDeviceWithPeripherals(TestOscilloscope.testStore, {
      peripheralCount: 1,
      modifier: info => {
        info.product.conversionTable.rows = info.axes[0].product.conversionTable.rows;
        info.axes[0].product.conversionTable.rows = [];
        return info;
      },
    });

    device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  it('picks up the axis unit dimension from the device', () => {
    const channels = addChannelAndDependents([], 'pos', 1);
    const axisIdentities = selectIdentifiedAxes(TestOscilloscope.testStore.getState());
    const deviceIdentities = selectIdentifiedDevices(TestOscilloscope.testStore.getState());
    const result = groupByDimension(channels, device.key, deviceIdentities, axisIdentities);
    expect(result.Length.map(ch => ch.name).sort()).toEqual(['pos']);
    expect(result.Native).toBeUndefined();
  });
});
