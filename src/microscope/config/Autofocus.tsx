import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Input, Loader } from '@zaber/react-library';
import { P } from 'ts-pattern';
import _ from 'lodash';

import { actions as actionsDefinition } from '../actions';
import { isMatch, useActions } from '../../utils';

import { selectEditedConfig, selectWdiSearch } from './selectors';

function parseConfig(input: string): {
  provider: 'wdi';
  host: string;
  port: number | null;
} | null {
  if (!input) { return null }

  const endpoint = input.split(':');
  return {
    provider: 'wdi',
    host: endpoint[0],
    port: endpoint.at(1) ? +(endpoint[1]) : null,
  };
}

function configToString(autofocus: ReturnType<typeof parseConfig>): string {
  let value = autofocus?.host ?? '';
  if (autofocus?.port != null) {
    value += `:${autofocus.port}`;
  }
  return value;
}

export const Autofocus: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const autofocus = useSelector(selectEditedConfig).autofocus;

  const search = useSelector(selectWdiSearch);
  const searching = isMatch({ searching: true }, search);

  const [input, setInput] = useState(() => configToString(autofocus));
  useEffect(() => {
    if (!_.isEqual(autofocus, parseConfig(input))) {
      setInput(configToString(autofocus));
    }
  }, [autofocus]);

  function onChange(input: string) {
    setInput(input);
    actions.editConfig({ autofocus: parseConfig(input) });
  }

  return <>
    <div className="label">Autofocus</div>
    <Input className="autofocus" placeholder="Hostname/IP"
      disabled={searching}
      value={input} onValueChange={onChange}/>
    <div className="wdi-search">
      <Button color="grey" size="small" disabled={searching} onClick={actions.searchWdi}>Search</Button>
      {searching && <Loader size="small"/>}
      {isMatch({ error: P.any }, search) && <span className="error">Error: {search.error}</span>}
    </div>
  </>;
};
