import type { SagaIterator } from 'redux-saga';
import { all, call, put, select, takeLatest, fork, take } from 'redux-saga/effects';
import { v4 as uuidV4 } from 'uuid';
import { throwUnexpectedError } from '@zaber/toolbox';

import { getContainer } from '../container';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import { LocalShareConfig, Logger, Storage } from '../app_components';
import type { AsyncReturnType } from '../utils';
import { getLogger } from '../log';

import { actions, ActionTypes } from './actions';
import { selectShareConfig } from './selectors';
import { DEFAULT_CONFIG } from './types';
import { discoverySaga } from './discovery';

export const FIRST_TIME_STORAGE_KEY = 'LOCAL_SHARE_FIRST_TIME';

let logger: Logger;

export function* localShareSaga(): SagaIterator {
  logger = getLogger('localShareSaga');

  yield all([
    takeLatest(ActionTypes.GET_LOCAL_SHARE_CONFIG, getLocalShareConfig),
    takeLatest(ActionTypes.LOCAL_SHARE_STATUS, getLocalShareStatus),
    takeLatest(ActionTypes.UPDATE_CONFIG, updateConfig),
    fork(discoverySaga),
  ]);
}

function sanitizeConfig(retrievedConfig: LocalShareConfig | null): LocalShareConfig {
  const config: LocalShareConfig = {
    ...DEFAULT_CONFIG,
    ...retrievedConfig,
    // eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
    instanceName: retrievedConfig?.instanceName || uuidV4(),
  };
  return config;
}

function* getLocalShareConfig(): SagaIterator {
  try {
    const service = getContainer().get(MessageRoutersService);
    const connection: AsyncReturnType<typeof service.getConnection> = yield call([service, service.getConnection], LOCAL_ROUTER_URL);

    const routerConfig: AsyncReturnType<typeof connection.api.getLocalShareConfig> = yield call(
      [connection.api, connection.api.getLocalShareConfig]
    );
    const sanitizedConfig = sanitizeConfig(routerConfig);
    yield put(actions.localShareConfigResult(sanitizedConfig));
  } catch (err) {
    throwUnexpectedError(err);
    logger.warn(err);
    yield put(actions.localShareConfigResultErr(err.message || String(err)));
  }
}

function* getLocalShareStatus(): SagaIterator {
  try {
    const service = getContainer().get(MessageRoutersService);
    const connection: AsyncReturnType<typeof service.getConnection> = yield call([service, service.getConnection], LOCAL_ROUTER_URL);

    const status: AsyncReturnType<typeof connection.api.getLocalShareStatus> = yield call(
      [connection.api, connection.api.getLocalShareStatus]
    );
    yield put(actions.localShareStatusResult(status));
  } catch (err) {
    throwUnexpectedError(err);
    logger.warn(err);
    yield put(actions.localShareStatusResultErr(err.message || String(err)));
  }
}

function* updateConfig(): SagaIterator {
  try {
    const service = getContainer().get(MessageRoutersService);
    const storage = getContainer().get(Storage);
    const connection: AsyncReturnType<typeof service.getConnection> = yield call([service, service.getConnection], LOCAL_ROUTER_URL);

    const { config }: ReturnType<typeof selectShareConfig> = yield select(selectShareConfig);
    if (config?.enabled) {
      const firstTime = storage.load<boolean>(FIRST_TIME_STORAGE_KEY, true);
      if (firstTime) {
        storage.save(FIRST_TIME_STORAGE_KEY, false);
        yield put(actions.toggleSharingWarning(true));
        yield take(ActionTypes.TOGGLE_SHARING_WARNING);
      }
    }

    yield call([connection.api, connection.api.setLocalShareConfig], config);

    yield put(actions.updateConfigDone());
    yield put(actions.getCurrentStatus());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.updateConfigDone(err.message || String(err)));
    yield put(actions.getCurrentConfig());
  }
}
