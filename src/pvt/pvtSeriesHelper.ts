import _ from 'lodash';
import { Units } from '@zaber/motion';

import { convertBetweenUnits, derivativeUnit } from '../units';

import { PvtSegment } from './pvtSegment';
import { type Peak, Series, Range, PositionUnits, VelocityUnits, Sign, LimitCrossing, Point, Span } from './types';
import { maxPeak, minPeak } from './utils';


export function getVelocityScale(positionUnit: PositionUnits, velocityUnit: VelocityUnits): number {
  if (positionUnit === Units.NATIVE) {
    throw new Error('Native units are not yet supported.');
    // TODO: When we enable support for native units, we will need the selected device's unit converter
    // here to get the conversion factor from FW vel units to pos units per second.
  }

  if (velocityUnit !== derivativeUnit(positionUnit)) {
    return convertBetweenUnits(1, velocityUnit, derivativeUnit(positionUnit)!);
  }

  return 1;
}


export function getAccelerationScale(positionUnit: PositionUnits, velocityUnit: VelocityUnits): number {
  if (positionUnit === Units.NATIVE) {
    throw new Error('Native units are not yet supported.');
    // TODO: When we enable support for native units, we will need the selected device's unit converter
    // here to get the conversion factor from FW accel units to pos units per square second.
  }

  return getVelocityScale(positionUnit, velocityUnit);
}


export function insertPoint(
  series: Series, index: number, dt: number | undefined, positionUnit: PositionUnits, velocityUnit: VelocityUnits): Series {
  const points = [...series.points];

  // If the last point is selected, instead of trying to guess a useful position past the
  // present end of the curve, we'll insert between the last and second-last points instead.
  if (index === points.length - 1) {
    index--;
  }

  const helper = PvtSeriesHelper.FromUnits(series, positionUnit, velocityUnit);
  const prevPoint = points[index];
  const nextPoint = points[index + 1];
  const newPointDt = (dt ?? 0.5 * nextPoint.dt);
  const newPointTime = prevPoint.time + newPointDt;
  const { position, velocity } = helper.getValueAtTime(newPointTime);

  points[index + 1] = {
    ...nextPoint,
    dt: Math.max(0, nextPoint.dt - newPointDt),
  };

  points.splice(index + 1, 0, {
    pos: position,
    vel: velocity,
    dt: newPointDt,
    time: newPointTime,
    relative: false,
  });

  const segment = new PvtSegment(points[index], points[index + 1], helper.velocityScale, helper.accelerationScale);
  points[index + 1].accel = segment.getAccel(points[index + 1].dt);

  return {
    ...series,
    points,
  };
}


export function deletePoint(series: Series, index: number, positionUnit: PositionUnits, velocityUnit: VelocityUnits): Series {
  const points = [...series.points];
  let accelUpdateIndex = index;

  if (index === 0) {
    accelUpdateIndex = 1;
    points[1] = {
      ...points[1],
      vel: 0,
      dt: 0,
      time: 0,
      accel: undefined,
    };

    for (let i = 2; i < points.length; i++) {
      points[i] = {
        ...points[i],
        time: points[i - 1].time + points[i].dt,
      };
    }
  } else if (index === points.length - 1) {
    accelUpdateIndex--;
    points[index - 1] = {
      ...points[index - 1],
      vel: 0,
    };
  } else {
    points[index + 1] = {
      ...points[index + 1],
      dt: points[index].dt + points[index + 1].dt,
    };
  }

  points.splice(index, 1);

  const segment = new PvtSegment(points[accelUpdateIndex - 1], points[accelUpdateIndex],
    getVelocityScale(positionUnit, velocityUnit),
    getAccelerationScale(positionUnit, velocityUnit));
  points[accelUpdateIndex] = {
    ...points[accelUpdateIndex],
    accel: segment.getAccel(points[accelUpdateIndex].dt),
  };

  return {
    ...series,
    points,
  };
}


interface LimitCrossingCalculation {
  getCrossings: (seg: PvtSegment) => number[];
  getValue: (seg: PvtSegment, t: number) => number;
  getSlope: (seg: PvtSegment, t: number) => number;
  limit: number;
  direction: Sign | null;
  discontinuous: boolean;
}


/**
 * Helper class for analytically getting numeric properties of one series, considering all segments.
 */
export class PvtSeriesHelper {
  private constructor(private series: Series, public velocityScale: number, public accelerationScale: number) {
  }


  public static FromScales(series: Series, velocityScale: number, accelerationScale: number): PvtSeriesHelper {
    return new PvtSeriesHelper(series, velocityScale, accelerationScale);
  }


  public static FromUnits(series: Series, positionUnits: PositionUnits, velocityUnits: VelocityUnits): PvtSeriesHelper {
    return new PvtSeriesHelper(series,
      getVelocityScale(positionUnits, velocityUnits),
      getAccelerationScale(positionUnits, velocityUnits));
  }


  public findPositionRange(): Range {
    return this.minMax(seg => seg.getPosRange());
  }


  public findVelocityRange(): Range {
    return this.minMax(seg => seg.getVelRange());
  }


  public findAccelerationRange(): Range {
    return this.minMax(seg => seg.getAccelRange());
  }


  private minMax(getRange: (segment: PvtSegment) => Range): Range {
    const { series, velocityScale, accelerationScale } = this;
    if ((series.points.length ?? 0) < 2) {
      throw new Error(`Series ${series.name} has no segments.`);
    }

    let min: (Peak | null) = null;
    let max: (Peak | null) = null;
    let prev = series.points[0];
    if (prev.relative) {
      throw new Error('Series start position must be absolute.');
    }

    let i = 1;
    while (i < series.points.length) {
      let next = series.points[i];
      if (next.relative) {
        next = {
          ...next,
          pos: prev.pos + next.pos,
          relative: false,
        };
      }

      const segment = new PvtSegment(prev, next, velocityScale, accelerationScale);
      const range = getRange(segment);
      range.min.time += prev.time;
      range.max.time += prev.time;
      min = minPeak(min, range.min);
      max = maxPeak(max, range.max);
      prev = next;
      i++;
    }

    return {
      min: min!,
      max: max!,
    };
  }


  public findPositionLimitCrossings(limit: number, direction: Sign | null): LimitCrossing[] {
    const c: LimitCrossingCalculation = {
      getCrossings: segment => segment.getPositionCrossings(limit),
      getValue: (segment, t) => segment.getPos(t),
      getSlope: (segment, t) => segment.getVel(t),
      limit,
      direction,
      discontinuous: false,
    };

    return this.findLimitCrossings(c);
  }


  public findVelocityLimitCrossings(limit: number, direction: Sign | null): LimitCrossing[] {
    const c: LimitCrossingCalculation = {
      getCrossings: segment => segment.getVelocityCrossings(limit),
      getValue: (segment, t) => segment.getVel(t),
      getSlope: (segment, t) => segment.getAccel(t),
      limit,
      direction,
      discontinuous: false
    };

    return this.findLimitCrossings(c);
  }


  public findAccelerationLimitCrossings(limit: number, direction: Sign | null): LimitCrossing[] {
    const c: LimitCrossingCalculation = {
      getCrossings: segment => segment.getAccelerationCrossings(limit),
      getValue: (segment, t) => segment.getAccel(t),
      getSlope: segment => segment.getJerk(),
      limit,
      direction,
      discontinuous: true,
    };

    return this.findLimitCrossings(c);
  }


  /**
   * Finds times and segments at which, if stopped, the device would hit a given position limit while
   * decelerating. The device will reject any such segments.
   */
  public findStopCollisions(posLimits: Span, deceleration: number): LimitCrossing[] {
    const { series, velocityScale, accelerationScale } = this;
    let crossings: LimitCrossing[] = [];

    let prevPoint = series.points[0];
    if (prevPoint.relative) {
      throw new Error('Series start position must be absolute.');
    }

    for (let i = 1; i < series.points.length; i++) {
      let nextPoint = series.points[i];
      if (nextPoint.relative) {
        nextPoint = {
          ...nextPoint,
          pos: prevPoint.pos + nextPoint.pos,
          relative: false,
        };
      }

      const segment = new PvtSegment(prevPoint, nextPoint, velocityScale, accelerationScale);
      const stopPeaks = segment.getStopPositionPeakTimes(deceleration)
        .filter(({ value }) => value < posLimits.min || value > posLimits.max);

      const tPrev = prevPoint.time;
      crossings = crossings.concat(stopPeaks.map(peak => ({ time: peak.time + tPrev, index: i })));
      prevPoint = nextPoint;
    }

    return crossings.filter(({ time }, i) => i === 0 || Math.abs(time - crossings[i - 1].time) >= 0.0001);
  }


  private findLimitCrossings(c: LimitCrossingCalculation): LimitCrossing[] {
    const { series, velocityScale, accelerationScale } = this;
    let crossings: LimitCrossing[] = [];
    let prevSegment: PvtSegment | null = null;
    let prevPoint = series.points[0];
    if (prevPoint.relative) {
      throw new Error('Series start position must be absolute.');
    }

    for (let i = 1; i < series.points.length; i++) {
      let nextPoint = series.points[i];
      if (nextPoint.relative) {
        nextPoint = {
          ...nextPoint,
          pos: prevPoint.pos + nextPoint.pos,
          relative: false,
        };
      }

      const segment = new PvtSegment(prevPoint, nextPoint, velocityScale, accelerationScale);

      const crossingTimes = c.getCrossings(segment).filter(t => {
        if (c.direction == null) {
          return true;
        }

        const slope = c.getSlope(segment, t);
        return (c.direction === Sign.Positive && slope > 0) || (c.direction === Sign.Negative && slope < 0);
      });

      // If the quantity is discontinuous (ie acceleration), the discontinuities can also cause limit crossings at the start of segments.
      if (c.discontinuous && !crossingTimes.includes(0)) {
        const initialValue = c.getValue(segment, 0);
        const outAbove = c.direction === Sign.Positive && initialValue > c.limit;
        const outBelow = c.direction === Sign.Negative && initialValue < c.limit;
        if (outAbove || outBelow) {
          if (!prevSegment) {
            crossingTimes.push(0);
          } else {
            const prevEndValue = c.getValue(prevSegment, prevSegment.dt);
            if ((outAbove && prevEndValue < c.limit) ||
              (outBelow && prevEndValue > c.limit)) {
              crossingTimes.push(0);
            }
          }
        }
      }

      const tPrev = prevPoint.time;
      crossings = crossings.concat(crossingTimes.map(t => ({ time: t + tPrev, index: i })));
      prevSegment = segment;
      prevPoint = nextPoint;
    }

    return crossings;
  }


  public getValueAtTime(time: number): { position: number; velocity: number; acceleration: number } {
    const { series } = this;
    let index = _.sortedIndexBy<Pick<Point, 'time'>>(series.points, { time }, point => point.time) - 1;
    index = Math.max(0, Math.min(series.points.length - 2, index));

    let start = series.points[index];
    if (start.relative) {
      let deltaPos = 0;
      let i = index;
      while (i > 0 && series.points[i].relative) {
        deltaPos += series.points[i].pos;
        i--;
      }

      deltaPos += series.points[i].pos; // The previous absolute point; 0th point is always absolute.
      start = {
        ...start,
        pos: deltaPos,
        relative: false,
      };
    }

    let end = series.points[index + 1];
    if (end.relative) {
      end = {
        ...end,
        pos: start.pos + end.pos,
        relative: false,
      };
    }

    const segment = new PvtSegment(start, end, this.velocityScale, this.accelerationScale);
    const t = time - start.time;
    return {
      position: segment.getPos(t),
      velocity: segment.getVel(t),
      acceleration: segment.getAccel(t),
    };
  }
}

