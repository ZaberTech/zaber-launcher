const tsEslint = require('typescript-eslint');
const { config: zaberEslintConfig } = require('@zaber/eslint-config');
const path = require('node:path');
const { includeIgnoreFile } = require('@eslint/compat');

module.exports = tsEslint.config(
  includeIgnoreFile(path.resolve(__dirname, '.gitignore')),
  {
    files: ['**/*.js'],
    extends: [
      zaberEslintConfig.javascript,
    ],
  },
  {
    files: ['scripts/**/*.js'],
    rules: {
      'no-console': 'off',
    },
  },
  {
    files: ['src/**/*.{ts,tsx}', 'src_node/**/*.ts'],
    extends: [
      zaberEslintConfig.typescript(['./tsconfig.json', './tsconfig.node.json']),
    ],
    rules: {
      '@typescript-eslint/no-unused-vars': [
        'error',
        {
          destructuredArrayIgnorePattern: '^_',
        }
      ],
    }
  },
  {
    files: ['src/**/*.{jsx,tsx}'],
    extends: [
      zaberEslintConfig.react,
    ],
  },
  {
    files: [
      '**/*.test.{ts,tsx}',
      'src/test/**/*.{ts,tsx}',
      'src/**/mocks.{ts,tsx}',
      'src/units/test_utils.ts',
    ],
    extends: [
      zaberEslintConfig.test,
    ],
  }
);
