import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Input, Button, Text, NoticeBanner } from '@zaber/react-library';

import type { RootState } from '../store';
import { environment } from '../environment';
import { ExternalLink } from '../components';

import { selectLoginData, selectLoginError, selectLoginInProgress } from './selectors';
import { actions as actionsDefinition } from './actions';
import type { LoginData } from './types';
import { Header } from './Header';

interface Props {
  actions: typeof actionsDefinition;
  loginData: LoginData;
  inProgress: boolean;
  error: ReturnType<typeof selectLoginError>;
}

class LoginBase extends Component<Props> {
  onPasswordKeyDown: React.ComponentProps<typeof Input>['onKeyDown'] = e => {
    const { inProgress, actions } = this.props;
    if (e.key === 'Enter' && !inProgress) {
      actions.login();
    }
  };

  render(): React.ReactNode {
    const { actions, loginData, inProgress, error } = this.props;

    return (<>
      <Header/>

      <div className="login">
        <div className="center heading">
          <Text t={Text.Type.H2}>Sign In To Account</Text>
        </div>

        <div className="inputs">
          <div className="inner">
            <Input type="text" name="email"
              labelContent="Email Address"
              disabled={inProgress}
              value={loginData.email}
              onChange={e => actions.updateLoginData({ email: e.target.value })}/>

            <Input type="password" name="password"
              labelContent="Password"
              disabled={inProgress}
              value={loginData.password}
              onChange={e => actions.updateLoginData({ password: e.target.value })}
              onKeyDown={this.onPasswordKeyDown}/>
          </div>
          {error && <NoticeBanner>{error}</NoticeBanner>}
        </div>

        <div>
          <Text e={Text.Emphasis.Light}>
            <ExternalLink url={`${environment.cloudPortalUrl}/reset-password`}>Forgot Password?</ExternalLink>
          </Text>
        </div>

        <div>
          <Button disabled={inProgress} onClick={actions.login}>Sign In</Button>
        </div>

        <div>
          <Text e={Text.Emphasis.Light}>
            <ExternalLink url={`${environment.cloudPortalUrl}/signup`}>Create Account</ExternalLink>
          </Text>
        </div>
      </div>
    </>);
  }
}

export const Login = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    loginData: selectLoginData(state),
    inProgress: selectLoginInProgress(state),
    error: selectLoginError(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(LoginBase);
