import { makeAxisKey, makeConnectionKey, makeDeviceKey, makeRouterKey } from './domain';
import { isSubKeyOf } from './generic';

describe('isSubKeyOf', () => {
  const connectionKey = makeConnectionKey(makeRouterKey('local'), 'COM1');
  const deviceKey = makeDeviceKey(connectionKey, 2);
  const axisKey = makeAxisKey(deviceKey, 1);

  test('returns true for derived keys and the key itself', () => {
    expect(isSubKeyOf(connectionKey, connectionKey)).toBe(true);
    expect(isSubKeyOf(deviceKey, connectionKey)).toBe(true);
    expect(isSubKeyOf(axisKey, connectionKey)).toBe(true);

    expect(isSubKeyOf(deviceKey, deviceKey)).toBe(true);
    expect(isSubKeyOf(axisKey, deviceKey)).toBe(true);

    expect(isSubKeyOf(axisKey, axisKey)).toBe(true);
  });

  test('returns false for everything else', () => {
    const deviceKey = makeDeviceKey(connectionKey, 2);
    const connectionKey2 = makeConnectionKey(makeRouterKey('local'), 'COM2');
    const connectionKey3 = makeConnectionKey(makeRouterKey('other'), 'COM1');

    expect(isSubKeyOf(connectionKey, deviceKey)).toBe(false);
    expect(isSubKeyOf(connectionKey, axisKey)).toBe(false);
    expect(isSubKeyOf(deviceKey, axisKey)).toBe(false);

    expect(isSubKeyOf(connectionKey, connectionKey2)).toBe(false);
    expect(isSubKeyOf(connectionKey, connectionKey3)).toBe(false);

    expect(isSubKeyOf(deviceKey, connectionKey2)).toBe(false);
    expect(isSubKeyOf(deviceKey, connectionKey3)).toBe(false);
  });

  test('returns false when one of the keys is falsy', () => {
    for (const falsy of [null, undefined, '']) {
      expect(isSubKeyOf(falsy, connectionKey)).toBe(false);
      expect(isSubKeyOf(connectionKey, falsy)).toBe(false);
      expect(isSubKeyOf(falsy, falsy)).toBe(false);
    }
  });
});
