import { createSelector } from 'reselect';
import type { Nullable } from '@zaber/toolbox';

import { selectTriggers } from '../store';
import { selectAxes, selectDevices } from '../connection_manager';
import type { CommandTree } from '../terminal/types';
import { commandTreeToCommandList } from '../terminal/utils';
import { commandExists } from '../devices';

import { getSettingAxes, getSettingsList } from './utils';
import { SettingsList } from './types';


export const selectSelectedDevice = createSelector(selectTriggers, state => state.selectedDevice);
export const selectHasTriggers = createSelector(selectTriggers,
  state => state.selectedDevice ? state.devices[state.selectedDevice].hasTriggers : null);
export const selectDeviceError = createSelector(selectTriggers,
  state => state.selectedDevice ? state.devices[state.selectedDevice].error : null);
export const selectIsController = createSelector(selectTriggers,
  state => state.selectedDevice ? state.devices[state.selectedDevice].isController : null);
export const selectIoInfo = createSelector(selectTriggers, state =>
  state.selectedDevice ? state.devices[state.selectedDevice].ioInfo : null);
export const selectTriggersExpanded = createSelector(selectTriggers,
  state => state.selectedDevice && state.devices[state.selectedDevice] ? state.devices[state.selectedDevice].triggersExpanded : []);
export const selectTriggersTriggers = createSelector(selectTriggers, state => state.triggers);
export const selectCommandList = createSelector(selectTriggers, selectDevices, (state, allDevices) => {
  const selectedDevice = state.selectedDevice;
  if (selectedDevice == null) {
    return [];
  }
  const deviceInfo = allDevices[selectedDevice];
  if (deviceInfo == null) {
    return [];
  }
  const productInfo = deviceInfo.product;
  if (productInfo == null) {
    return [];
  }

  const getSubCommands = (command: string[], commandTree: Nullable<CommandTree>): CommandTree[] => {
    if (command.length === 0) {
      return commandTree?.nodes ? commandTree.nodes : [];
    }
    if (commandTree?.nodes == null) {
      return [];
    }
    return getSubCommands(command.slice(1), commandTree.nodes.find(node => node.command === command[0]));
  };

  const commandTree = productInfo.commandTree;
  const triggerCommandsTrees = getSubCommands(['trigger', 'number', 'action', 'triggeract'], commandTree);

  return triggerCommandsTrees
    .filter(command => !['none', 'axisNum', 'settingName'].includes(command.command))
    .flatMap(command => commandTreeToCommandList(command, productInfo));
});
export const selectAxisCount = createSelector(selectTriggers, selectDevices, (state, allDevices) => {
  const selectedDevice = state.selectedDevice;
  if (selectedDevice == null) {
    return 1;
  }
  const deviceInfo = allDevices[selectedDevice];
  if (deviceInfo == null) {
    return 1;
  }

  return deviceInfo.axisCount;
});
export const selectPosAxes = createSelector(selectTriggers, selectDevices, selectAxes, (state, allDevices, allAxes) => {
  const selectedDevice = state.selectedDevice;
  if (selectedDevice == null) {
    return [];
  }
  const deviceInfo = allDevices[selectedDevice];
  if (deviceInfo == null) {
    return [];
  }

  return getSettingAxes('pos', deviceInfo.axes.map(axisKey => allAxes[axisKey]));
});
export const selectEncoderAxes = createSelector(selectTriggers, selectDevices, selectAxes, (state, allDevices, allAxes) => {
  const selectedDevice = state.selectedDevice;
  if (selectedDevice == null) {
    return [];
  }
  const deviceInfo = allDevices[selectedDevice];
  if (deviceInfo == null) {
    return [];
  }

  return getSettingAxes('encoder.pos', deviceInfo.axes.map(axisKey => allAxes[axisKey]));
});
export const selectHasConditionSettingAbsolute = createSelector(selectTriggers, selectDevices, (state, allDevices) => {
  const selectedDevice = state.selectedDevice;
  if (selectedDevice == null) {
    return false;
  }
  const deviceInfo = allDevices[selectedDevice];
  if (deviceInfo == null) {
    return false;
  }

  return commandExists('trigger number when abs', deviceInfo, true);
});
export const selectHasActionSettingToSetting = createSelector(selectTriggers, selectDevices, (state, allDevices) => {
  const selectedDevice = state.selectedDevice;
  if (selectedDevice == null) {
    return false;
  }
  const deviceInfo = allDevices[selectedDevice];
  if (deviceInfo == null) {
    return false;
  }

  return commandExists('trigger number action triggeract settingName assignmentoperator setting', deviceInfo, true);
});
export const selectSettingsLists = createSelector(selectTriggers, selectDevices, selectAxes,
  (state, allDevices, allAxes): SettingsList[]  => {
    const selectedDevice = state.selectedDevice;
    if (selectedDevice == null) {
      return [];
    }

    const deviceInfo = allDevices[selectedDevice];
    if (deviceInfo == null) {
      return [];
    }

    const settingsLists: SettingsList[] = [];

    if (deviceInfo.product) {
      settingsLists[0] = getSettingsList(deviceInfo.product);
    }

    deviceInfo.axes.forEach((axisKey, i) => {
      const axis = allAxes[axisKey];
      if (axis.product) {
        settingsLists[i + 1] = getSettingsList(axis.product);
      }
    });

    return settingsLists;
  });
