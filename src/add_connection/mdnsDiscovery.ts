import { eventChannel, EventChannel, SagaIterator } from 'redux-saga';
import { all, put, delay, take, race, takeLeading, spawn, call } from 'redux-saga/effects';
import MDNS, { MDNSAnswer, MDNSResponse, SRVResponseData } from 'multicast-dns';
import _ from 'lodash';
import { AsyncReturnType, throwUnexpectedError } from '@zaber/toolbox';

import { fetchDeviceImagesById } from '../connection_manager/sagas';
import { Action, ensureError, splitOnce, takeLeadingUniq } from '../utils';
import { getContainer } from '../container';
import { ZaberApi } from '../app_components';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import {
  MDNSCandidate, MDNS_DISCOVERY_INACTIVE_TIMEOUT,
  MDNS_DISCOVERY_SERVICE_SUFFIX_SINGLE, MDNS_DISCOVERY_SERVICE_SUFFIX_CHAIN,
} from './types';

export function* discoverySaga(): SagaIterator {
  yield all([
    takeLeading(ActionTypes.MDNS_DISCOVER, discoverCandidates),
    takeLeadingUniq(ActionTypes.MDNS_DISCOVER_ADD_CANDIDATE, action => action.payload!.candidate.hostname, onNewCandidate),
  ]);
}

interface DiscoveryContext {
  candidates: Record<string, Partial<MDNSCandidate>>;
  emit: (candidate: MDNSCandidate) => void;
}

interface TxtRecordData {
  v: string;
  textver: string;
  devid: string;
  sn: string;
}

function processRecord({ candidates, emit }: DiscoveryContext, record: MDNSAnswer) {
  let candidate = candidates[record.name];
  if (!candidate) {
    candidate = {
      hostname: record.name,
    };
    candidates[record.name] = candidate;
  }

  if (record.type === 'SRV') {
    const data = record.data as SRVResponseData;
    candidate.hostname = data.target;
    if (record.name.includes(MDNS_DISCOVERY_SERVICE_SUFFIX_SINGLE)) {
      candidate.singlePort = data.port;
    } else if (record.name.includes(MDNS_DISCOVERY_SERVICE_SUFFIX_CHAIN)) {
      candidate.chainPort = data.port;
    }
  } else if (record.type === 'TXT') {
    const records = (record.data as Buffer[]).map(buffer => splitOnce(buffer.toString('ascii'), '='));
    const info: TxtRecordData = _.fromPairs(records) as unknown as TxtRecordData;

    candidate.deviceId = +info.devid;
    candidate.serialNumber = +info.sn;
  }

  const isComplete = (candidate.singlePort ?? candidate.chainPort) && candidate.deviceId;
  if (isComplete) {
    emit(_.cloneDeep(candidate as MDNSCandidate));
  }
}

function mdnsDiscoveryChannel(search: string[]): EventChannel<MDNSCandidate | Error> {
  return eventChannel<MDNSCandidate | Error>(emit => {
    const context: DiscoveryContext = {
      candidates: {},
      emit,
    };

    const handler = (record: MDNSResponse) => {
      try {
        record.answers.concat(record.additionals).filter(a => search.some(suffix => a.name.endsWith(suffix)))
          .forEach(record => processRecord(context, record));
      } catch (err) {
        emit(ensureError(err));
      }
    };

    const mdns = MDNS();
    mdns.on('response', handler);
    mdns.on('error', err => emit(ensureError(err)));
    mdns.query({ questions: search.map(suffix => ({ name: suffix, type: 'PTR' })) });

    return () => {
      mdns.off('response', handler);
      mdns.destroy();
    };
  });
}

export function* discoverCandidates(): SagaIterator {
  yield put(actions.mdnsDiscoverClearCandidates());

  try {
    const channel = mdnsDiscoveryChannel([MDNS_DISCOVERY_SERVICE_SUFFIX_SINGLE, MDNS_DISCOVERY_SERVICE_SUFFIX_CHAIN]);
    try {
      while (true) {
        const { candidate }: {
          candidate?: MDNSCandidate | Error;
        } = yield race({
          candidate: take(channel),
          timeout: delay(MDNS_DISCOVERY_INACTIVE_TIMEOUT),
          cancelled: take(ActionTypes.MDNS_DISCOVER_CANCEL),
        });

        if (!candidate) { break }

        if (candidate instanceof Error) {
          throw candidate;
        }
        yield put(actions.mdnsDiscoverAddCandidate(candidate));
      }
    } finally {
      channel.close();
    }
    yield put(actions.mdnsDiscoverDone());
  } catch (err) {
    yield put(actions.mdnsDiscoverDone(ensureError(err).message));
  }
}

function* onNewCandidate(
  { payload: { candidate } }: Action<ActionsToPayloads[ActionTypes.MDNS_DISCOVER_ADD_CANDIDATE]>
): SagaIterator {
  if (candidate.name != null) {
    return;
  }
  yield spawn(fetchDeviceImagesById, [candidate.deviceId], []);
  try {
    const api = getContainer().get<ZaberApi>(ZaberApi);
    const request = { deviceID: candidate.deviceId };
    const { device }: AsyncReturnType<typeof api.getDeviceInfo> = yield call(
      [api, api.getDeviceInfo],
      request,
    );

    yield put(actions.mdnsUpdateCandidateName(candidate.hostname, device.name));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.mdnsFetchDeviceInfoDoneErr(candidate, err.message));
  }
}
