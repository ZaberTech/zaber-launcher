import { DigitalOutputAction, IoPortLabel } from '@zaber/motion/ascii';
import { all, call, put, take, takeEvery } from 'redux-saga/effects';

import { getDevice } from '../../connection_manager';
import { Action, RT, throwUnexpectedError } from '../../utils';
import { actionDefinitions as pollingActions, ActionTypes as PollingActionTypes } from '../../polling/actions';

import { ActionsToPayloads, ActionTypes, actionDefinitions as actions } from './actions';


export function* ioSaga() {
  yield all([
    takeEvery(ActionTypes.START_SET_DIGITAL, setDigital),
    takeEvery(ActionTypes.SET_ANALOG, setAnalog),
    takeEvery(ActionTypes.SET_LABEL_DEVICE, setLabel),
    takeEvery(ActionTypes.LOAD_LABELS_DEVICE, getLabel)
  ]);
}

function* setDigital({ payload: { deviceKey, channel, on } }: Action<ActionsToPayloads[ActionTypes.START_SET_DIGITAL]>) {
  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);

    yield call([device.io, device.io.setDigitalOutput], channel, on ? DigitalOutputAction.ON : DigitalOutputAction.OFF);
    yield put(pollingActions.pollNow());
    yield take(PollingActionTypes.RECEIVE_RESULTS);
    yield put(actions.completeSetDigital(deviceKey, channel));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setError(deviceKey, { message: `Failed to set Digital IO ${channel}`, details: e.message }));
    yield put(actions.completeSetDigital(deviceKey, channel));
  }
}

function* setAnalog({ payload: { deviceKey, channel, state } }: Action<ActionsToPayloads[ActionTypes.SET_ANALOG]>) {
  if (state.mode === 'write') {
    try {
      const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);

      yield call([device.io, device.io.setAnalogOutput], channel, state.value);
      yield put(pollingActions.pollNow());
      yield take(PollingActionTypes.RECEIVE_RESULTS);
      yield put(actions.setAnalog(deviceKey, channel, { mode: 'display', value: state.value }));
    } catch (e) {
      throwUnexpectedError(e);
      yield put(actions.setError(deviceKey, { message: `Failed to set Analog IO ${channel}`, details: e.message }));
      yield put(actions.setAnalog(deviceKey, channel, { mode: 'display', value: state.value }));
    }
  }
}

function* setLabel({ payload: { deviceKey, portType, channelNumber, newValue, prevValue } }:
  Action<ActionsToPayloads[ActionTypes.SET_LABEL_DEVICE]>) {
  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
    yield call([device.io, device.io.setLabel], portType, channelNumber, newValue);
    yield put(actions.setLabel(deviceKey, portType, channelNumber, newValue));
    yield put(actions.deleteLabelError(deviceKey, `${portType}-${channelNumber}`));
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setLabelError(
      deviceKey, portType, channelNumber, prevValue, { message: 'Failed to save label', details: e.message }
    ));
  }
}

function* getLabel({ payload: { deviceKey } }: Action<ActionsToPayloads[ActionTypes.LOAD_LABELS_DEVICE]>) {
  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);

    const labels: IoPortLabel[] = yield call([device.io, device.io.getAllLabels]);
    for (const label of labels) {
      yield put(actions.setLabel(deviceKey, label.portType, label.channelNumber, label.label));
    }
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setError(deviceKey, { message: 'Failed to load labels', details: e.message }));
  }
}
