import type { ascii, FirmwareVersion } from '@zaber/motion';

import type { EntityKey } from '../keys';
import type { Connection, ZaberApi } from '../app_components';
import type { MovementDimensions } from '../units';

export interface StoredRouter {
  url: string;
  type: RouterType;
}

export enum RouterType {
  Local = 'Local',
  Cloud = 'Cloud',
  LocalShare = 'LocalShare',
}

export const RouterTypeOrder = {
  [RouterType.Local]: 1,
  [RouterType.LocalShare]: 2,
  [RouterType.Cloud]: 3,
};

export interface DeviceInfo {
  key: EntityKey;
  address: number;

  axisCount: number;
  isMultiAxis: boolean;
  isController: boolean;
  /** EtherCAT or other alternate connection - will reject movement commands if true. */
  isRemote: boolean;
  product: ProductInfo | null;
  platformId: number | null;
  isKeyed: boolean;
  locksteps: LockstepGroup[] | null;
  label: string | null;

  identity: ascii.DeviceIdentity | null;
  nonIdentifiedInfo: {
    deviceId: number;
    firmwareVersion: FirmwareVersion | null;
  } | null;
}

export interface AxisInfo {
  key: EntityKey;
  axisNumber: number;
  isPeripheralLike: boolean;
  serialNumber: number | null;
  label: string | null;

  identity: ascii.AxisIdentity | null;
  product: ProductInfo | null;
  canMove: boolean;
  movementDimensions: MovementDimensions | null;
  nonIdentifiedInfo: {
    peripheralId: number | null;
  } | null;
}

export interface IdentifiedAxisInfo extends AxisInfo {
  identity: NonNullable<AxisInfo['identity']>;
  product: NonNullable<AxisInfo['product']>;
  nonIdentifiedInfo: null;
}

export interface DeviceInfoWithAxes extends DeviceInfo {
  axes: AxisInfo[];
}

export interface IdentifiedDeviceInfo extends DeviceInfoWithAxes {
  identity: NonNullable<DeviceInfoWithAxes['identity']>;
  product: NonNullable<DeviceInfoWithAxes['product']>;
  locksteps: NonNullable<DeviceInfoWithAxes['locksteps']>;
  nonIdentifiedInfo: null;
  axes: IdentifiedAxisInfo[];
}

export interface RouterInfo {
  name: string;
  connections: Connection[];
}

export interface ProductInfo {
  id: number;
  capabilities: string[];
  commandTree: ZaberApi.DeviceInfo['command_tree'];
  conversionTable: ZaberApi.DeviceInfo['conversion_table'];
  settings: ZaberApi.DeviceInfo['settings'];
}

export const ROUTERS_STORAGE_KEY = 'ROUTERS';

export interface LockstepGroup {
  groupNumber: number;
  axisNumbers: number[];
}

export type AsciiComms = ascii.Device | ascii.Axis;

export interface LoadingDevicesError {
  reason: 'connection' | 'other';
  message: string;
}
