import React from 'react';
import { Icons, AnimationClasses, Flex, NoticeBanner } from '@zaber/react-library';
import classNames from 'classnames';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectSettingsBeingRead, selectError } from './selectors';
import { SettingsSearchBar } from './SettingsSearchBar';


export const TopPanel: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const settingsBeingRead = useSelector(selectSettingsBeingRead);
  const error = useSelector(selectError);

  return <div className="top-panel">
    <Flex.Row className="top-panel-row">
      <SettingsSearchBar/>
      <Flex.Spacer/>
      <div className="refresh-all">
        <span>Refresh All Settings</span>
        <Icons.Refresh
          title="Refresh All Settings"
          activated={settingsBeingRead}
          className={classNames({ [AnimationClasses.Rotation]: settingsBeingRead })}
          onClick={() => actions.readAllSettings()}/>
      </div>
    </Flex.Row>
    {error && <NoticeBanner className="device-error">{error}</NoticeBanner>}
  </div>;
};
