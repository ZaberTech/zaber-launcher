import { createSelector } from 'reselect';

import { selectHelp } from '../store';

export const selectMessagesVisibility = createSelector(selectHelp, state => state.messageVisibility);
export const selectFullscreenNoticeVisibility = createSelector(selectHelp, state => state.fullscreenNoticeVisibility);
