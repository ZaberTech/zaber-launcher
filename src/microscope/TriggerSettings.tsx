import React from 'react';
import { useSelector } from 'react-redux';
import { ButtonRowConfirmCancel, HelpTooltip, Icons, Modal, Text } from '@zaber/react-library';

import { useActions } from '../utils';
import { InputWithUnits } from '../units';

import { actions as actionsDefinition } from './actions';
import { selectTriggerSettings } from './selectors';
import { OperationError } from './components';
import { Operations, TIME_DIMENSION, TRIGGER_SETTINGS_FIELDS, TRIGGER_SETTINGS_LABELS } from './types';
import { useOperationState } from './hooks';
import { sanitizeMeasurement } from './utils';

export const TriggerSettings: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const { open: isOpen, settings } = useSelector(selectTriggerSettings);

  const [isSaving] = useOperationState(Operations.saveTriggerSettings);

  const open = () => actions.setTriggerSettingsOpen('open');
  const close = () => actions.setTriggerSettingsOpen('close');

  return <>
    <Modal
      className="trigger-settings"
      headerIcon={<Icons.Settings/>}
      headerText="Trigger Settings"
      isOpen={isOpen}
      onRequestClose={close}
      buttons={<ButtonRowConfirmCancel
        confirmText={isSaving ? 'Saving...' : 'Save'}
        onConfirm={isSaving ? 'disabled' : () => actions.operationStart(Operations.saveTriggerSettings, { settings })}
        onCancel={close}
      />}
    >
      <div className="table">
        <div className="label">
          <Text>Exposure (Pulse Width)</Text>
          <HelpTooltip>
            The time the digital output is high.
            Depending on the camera setting, the pulse width may set the exposure time.
          </HelpTooltip>
        </div>
        <div className="label">
          <Text>Wait before Pulse</Text>
          <HelpTooltip>
            The time to wait before the trigger pulse.
            Set the time if, for example, your camera needs time to adjust the exposure.
            During Multi-D acquisition, the microscope will wait this long
            after arriving to the position before triggering the camera.
          </HelpTooltip>
        </div>
        <div className="label">
          <Text>Wait after Pulse</Text>
          <HelpTooltip>
            The time to wait after the trigger pulse.
            Set the time if you use the pulse to only trigger the camera rather than set the exposure time.
            During Multi-D acquisition, the microscope will wait this long
            before advancing to the next position.
          </HelpTooltip>
        </div>
        {TRIGGER_SETTINGS_FIELDS.map(key => (
          <InputWithUnits
            key={key}
            title={TRIGGER_SETTINGS_LABELS[key]}
            displayPrecision={1}
            unitFrom={{ dimension: TIME_DIMENSION }}
            measure={settings[key]}
            onChange={value => actions.updateTriggerSettings({ [key]: sanitizeMeasurement(value) })}
          />
        ))}
      </div>
      <OperationError operation={Operations.saveTriggerSettings}/>
    </Modal>
    <Icons.Settings className="settings-icon" title="Settings" onClick={open}/>
  </>;
};
