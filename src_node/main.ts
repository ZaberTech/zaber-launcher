// this set of imports is intentionally first
import { environment } from '../src/environment';
import './errors';

import 'reflect-metadata';

import { ReplaySubject } from 'rxjs';
import { app, BrowserWindow, dialog } from 'electron';
import * as electronRemote from '@electron/remote/main';

import { handleUnexpectedError } from '../src/errors/errors';

import { createContainer } from './container';
import { MessageRouter, MessageRouterError, MessageRouterExitCode } from './message_router';
import { setPaths } from './paths';
import { Updates } from './updates';
import { SingleInstance } from './single_instance';
import { Windows } from './windows';
import { Pty } from './pty';
import { DbService } from './db_service';

async function main(): Promise<void> {
  if (!environment.isProduction) {
    // Fixes: https://github.com/electron/electron/issues/38790
    app.commandLine.appendSwitch('disable-features', 'WidgetLayering');
  }
  electronRemote.initialize();

  const shouldQuit = new ReplaySubject();
  let canQuit = true;

  app.on('before-quit', e => {
    if (canQuit) {
      return;
    }

    e.preventDefault();

    if (!shouldQuit.isStopped) {
      shouldQuit.next(undefined);
      shouldQuit.complete();
    }
  });

  const container = createContainer();
  if (!container.get(SingleInstance).checkIsSingle()) {
    return;
  }

  const updates = container.get(Updates);
  await updates.init();

  canQuit = false;

  container.get(Pty);

  const messageRouter = container.get(MessageRouter);
  messageRouter.startRouter();

  const dbService = container.get(DbService);
  dbService.runDbService();
  try {
    await app.whenReady();

    const windows = container.get(Windows);
    try {
      windows.createWindow({ managedState: true });
      updates.scheduleUpdateChecks();

      await Promise.race([
        shouldQuit.toPromise(),
        messageRouter.runPromise,
        dbService.runPromise,
        windows.runPromise,
      ]);
    } catch (err) {
      if (err instanceof MessageRouterError && err.exitCode === MessageRouterExitCode.AnotherInstanceRunning) {
        await dialog.showMessageBox(BrowserWindow.getAllWindows()[0], {
          type: 'error',
          title: 'Zaber Launcher Error',
          buttons: ['OK'],
          message: [
            'Another incompatible application is already running (e.g. another edition of Zaber Launcher).',
            'Please, exit the other application first and then try again.',
          ].join(' '),
        });
      } else {
        throw err;
      }
    } finally {
      windows.stop();
    }
  } finally {
    await messageRouter.stopRouter();
    await dbService.stopProcess();
  }

  canQuit = true;
  app.quit();
}

setPaths();
main().catch(handleUnexpectedError);
