import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Button, MinorMenu } from '@zaber/react-library';
import classNames from 'classnames';
import { match, P } from 'ts-pattern';

import { ConnectionsView } from '../connection_manager';
import { SetLabel } from '../connection_manager_ui';
import { NoContentMessage, Title } from '../components';
import { useActions } from '../utils';

import { actionDefinitions } from './actions';
import { selectEntity, selectControllerInfo, selectChosenProcess, selectError, selectProcessLabels } from './selectors';
import { Header } from './components/Header';
import { Process } from './components/Process';
import { Dashboard } from './components/Dashboard';
import { PROCESS_GROUPS } from './types';
import { TITLE } from './constants';

const AppUi: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const entity = useSelector(selectEntity);
  const controller = useSelector(selectControllerInfo);
  const chosenProcess = useSelector(selectChosenProcess);
  const names = useSelector(selectProcessLabels);
  const error = useSelector(selectError);

  if (error != null) {
    return <NoContentMessage title={TITLE} type="error"
      bannerTitle={error.title}
      message={error.details}>
      <Button onClick={() => actions.chooseEntity(entity, chosenProcess)}>Try Again</Button>
    </NoContentMessage>;
  }
  if (entity == null) {
    return <NoContentMessage title={TITLE} message="Select a process controller."/>;
  }
  if (controller == null) {
    return <NoContentMessage title={TITLE} message="Loading..." type="working"/>;
  }
  if (controller === 'not-process-controller') {
    return <NoContentMessage title={TITLE} type="info" message="This is not a Process Controller"/>;
  }
  return <div className="process-controller">
    <Header {...controller}/>
    <div className="controls-container">
      <MinorMenu barStyle="horizontal">
        <span onClick={() => actions.chooseProcess()} className={classNames(chosenProcess ?? 'active')}>
          <MinorMenu.Item>Dashboard</MinorMenu.Item>
        </span>
        {PROCESS_GROUPS.map(group => {
          const { on: bridged, reading } = controller.bridges[group[0]];
          const processes = bridged ? [group[0]] : group;
          return processes.map(process => (
            <span
              key={process}
              onClick={() => actions.chooseProcess(process)}
              className={classNames(chosenProcess === process && 'active')}
            >
              <MinorMenu.Item disabled={reading}>{names[process].join(' ')}</MinorMenu.Item>
            </span>
          ));
        })}
      </MinorMenu>
      {match(chosenProcess)
        .with(P.number, process => <Process entity={entity} process={process}/>)
        .otherwise(() => <Dashboard entity={entity} info={controller}/>)
      }
    </div>
    <SetLabel/>
  </div>;
};

export const App: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const entity = useSelector(selectEntity);
  useEffect(() => {
    if (entity != null) {
      actions.chooseEntity(entity);
    }
    return () => { actions.unmount() };
  }, []);
  return <div className="connection-view-and-app">
    <Title>{TITLE}</Title>
    <ConnectionsView
      selectable={['device']}
      showAxes={false}
      selected={entity}
      onSelect={actions.chooseEntity}
    />
    <div className="app-ui servo-tuner">
      <AppUi/>
    </div>
  </div>;
};
