import type { EntityKey } from '../keys';

import type { ProfileMap } from './generate/profile';

export interface FirmwareVersion {
  major: number;
  minor: number;
  build: number;
}


export type ProtocolManualError = {
  error: string;
};

export interface ManualProfilingData {
  capabilities: string[];
  settings: string[];
  firmwareVersion: FirmwareVersion;
  deviceName: string;
  peripheralName?: string;
  baseUrl: string;
  includes: ProfileMap;
}

/** a raw html string of the manual, or an error that arose generating this manual */
export type SettingManual = { html: string} | ProtocolManualError;
export type SettingManuals = Partial<Record<string, SettingManual>>;

export type WarningFlagManual = { name: string } & ({ html: string} | ProtocolManualError);
export type WarningFlagManuals = Partial<Record<string, WarningFlagManual>>;

export type CommandRejectedReasonManual = { name: string } & ({ html: string} | ProtocolManualError);
export type CommandRejectedReasonManuals = Partial<Record<string, CommandRejectedReasonManual>>;

export interface ProductManuals {
  settings: SettingManuals;
  warnings: WarningFlagManuals;
  commandRejectedReasons: CommandRejectedReasonManuals;
}

export type ProductManualsList = Partial<Record<EntityKey, ProductManuals>>;
