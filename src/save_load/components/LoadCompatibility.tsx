import { Button, ButtonPrevious, Flex, Icons, RadioButton, Text } from '@zaber/react-library';
import React, { useState } from 'react';
import { match, P } from 'ts-pattern';

import { useActions } from '../../utils';
import type { LoadStateCompatibility, SaveLoadTarget, SaveLoadUiState } from '../types';
import { actions as actionDefinitions } from '../actions';

type Props = {
  target: SaveLoadTarget;
  ui: Extract<SaveLoadUiState, { step: 'load-check-result'}>;
};

const CompatibilityDescription: React.FC<Props> = ({ ui: { compatibility, context: { fileName } } }) => {
  const productDescription = compatibility.type === 'controller' ? 'controller and peripherals' : compatibility.type;
  const choice = compatibility.type === 'controller' ? 'Please choose the load option below.' : '';
  if (compatibility.error != null) {
    return <div className="info-grid">
      <Icons.ErrorFault className="icon error"/>
      <Text t={Text.Type.H4} className="title">Incompatible</Text>
      <div className="info">
        <Text>
        File <Text e={Text.Emphasis.Bold}>{fileName}</Text> is incompatible with
        the chosen {productDescription}. Please choose another file.
        </Text>
      </div>
    </div>;
  } else if (compatibility.peripheralErrors != null) {
    return <div className="info-grid">
      <Icons.ErrorWarning className="icon warning"/>
      <Text t={Text.Type.H4} className="title">Partially compatible</Text>
      <div className="info">
        <Text>
          File <Text e={Text.Emphasis.Bold}>{fileName}</Text> is partially compatible
          with the chosen {productDescription}. {choice}
        </Text>
      </div>
    </div>;
  } else {
    return <div className="info-grid">
      <Icons.Confirmation className="icon success"/>
      <Text t={Text.Type.H4} className="title">Compatible</Text>
      <div className="info">
        <Text>
          File <Text e={Text.Emphasis.Bold}>{fileName}</Text> is compatible
          with the chosen {productDescription}. {choice}
        </Text>
      </div>
    </div>;
  }
};

type CompatibilityGridPeripheralLineProps = {
  name: string;
  axisNumber: number;
  error?: string | null;
};

const CompatibilityGridPeripheralLine: React.FC<CompatibilityGridPeripheralLineProps> = ({ name, axisNumber, error }) => (
  <>
    <Text t={Text.Type.H5} className="axis-number">Axis {axisNumber}</Text>
    <Text className="axis-name">{name}</Text>
    <div className="result-icon">
      {error == null ? <Icons.Confirmation className="success"/> : <Icons.ErrorFault className="error"/>}
    </div>
    <div className="result">
      {error == null ? <Text className="title success">Compatible</Text> : <Text className="title error">Failed</Text>}
      {error != null && <Text className="description">{error}</Text>}
    </div>
  </>
);

type CompatibilityGridProps = {
  target: SaveLoadTarget;
  compatibility: LoadStateCompatibility;
};
const CompatibilityGrid: React.FC<CompatibilityGridProps> = ({ target, compatibility }) => match(target)
  .with({ type: 'axis' }, target => (
    <div className="compatibility-grid">
      <Text className="compatibility-grid-header">Saved Configuration</Text>
      <CompatibilityGridPeripheralLine error={compatibility.error} axisNumber={target.axisNumber} name={target.name}/>
    </div>
  ))
  .with({ type: P.not('axis') }, target => {
    const peripheralErrors = compatibility.peripheralErrors ?? [];
    const sortedAxisErrors = [...peripheralErrors].sort((e1, e2) => e1.axisNumber - e2.axisNumber);
    return <div className="compatibility-grid">
      <Text className="compatibility-grid-header">Saved Configuration</Text>
      {/* */}
      <Text t={Text.Type.H5} className="address">{target.address}</Text>
      <Text className="controller-name">{target.name}</Text>
      <div className="result-icon">
        {compatibility.error ? <Icons.ErrorFault className="error"/> : <Icons.Confirmation className="success"/>}
      </div>
      <div className="result">
        {compatibility.error ? <Text className="title error">Failed</Text> : <Text className="title success">Compatible</Text>}
        {compatibility.error && <Text className="description">{compatibility.error}</Text>}
      </div>
      {sortedAxisErrors.map(e => {
        const axis = target.axes.find(a => a.axisNumber === e.axisNumber);
        const axisName = axis?.identity.peripheralName ?? 'Unknown';
        return <CompatibilityGridPeripheralLine key={e.axisNumber} name={axisName} axisNumber={e.axisNumber} error={e.error}/>;
      })}
    </div>;
  })
  .exhaustive();

export const LoadCompatibility: React.FC<Props> = props => {
  const { target, ui: { compatibility } } = props;
  const [loadType, setLoadType] = useState<'controller' | 'all' | null>(null);
  const canApply = compatibility.type === 'controller' ? loadType != null : compatibility.error == null;
  const actions = useActions(actionDefinitions);

  return <>
    <CompatibilityDescription {...props}/>
    <CompatibilityGrid target={target} compatibility={compatibility}/>
    {compatibility.type === 'controller' && <div className="load-type-section">
      <Text t={Text.Type.H4}>Choose the restore option:</Text>
      <RadioButton
        name="load-type"
        onValueChange={() => setLoadType('controller')}
        checked={loadType === 'controller'}
        disabled={compatibility.error != null}
        data-testid="controller-only"
      >
        Apply the saved state to the controller only
      </RadioButton>
      <RadioButton
        name="load-type"
        onValueChange={() => setLoadType('all')}
        checked={loadType === 'all'}
        disabled={compatibility.error != null || compatibility.peripheralErrors != null}
        data-testid="load-all"
      >
        Apply the saved state to the controller and compatible peripheral(s)
      </RadioButton>
    </div>}
    <Flex.Row className="back-and-apply-buttons">
      <ButtonPrevious onClick={() => actions.beginLoad(target)}>Back</ButtonPrevious>
      <Flex.Spacer/>
      <Button disabled={!canApply} onClick={() => actions.applyLoad(target, props.ui.context, loadType === 'controller')}>Apply</Button>
    </Flex.Row>
  </>;
};
