import React from 'react';
import { HeaderCard } from '@zaber/react-library';

import type { DeviceInfoWithAxes } from '../types';
import { extractConnectionKey } from '../../keys';
import { EntityHierarchyLabel } from '../../components';

import { Controller } from './Controller';
import { DEVICE_TABLE_CLASS } from './types';

interface Props {
  device: DeviceInfoWithAxes;
}

export const SingleDeviceTable: React.FC<Props> = ({ children, device }) => {
  const connectionKey = extractConnectionKey(device.key);

  const header = <EntityHierarchyLabel entityKey={connectionKey}/>;

  return (
    <HeaderCard header={header} className="connection-with-device" edge="shadow">
      <div className={DEVICE_TABLE_CLASS}>
        <Controller key={device.key} device={device}>
          {children}
        </Controller>
      </div>
    </HeaderCard>
  );
};
