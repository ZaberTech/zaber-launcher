import React, { useLayoutEffect, useState } from 'react';
import classNames from 'classnames';
import { Text } from '@zaber/react-library';

import type { HistoryItem } from './types';
import { MessageTagger } from './MessageTagger';
import { ErrorDetails, ErrorDetailsType } from './ErrorDetails';


interface Props {
  historyItem: HistoryItem;
  onLayoutUpdate?: () => void;
}

export const TerminalHistoryItem: React.FC<Props> = ({ historyItem, onLayoutUpdate }) => {
  const [showErrorDetailsType, setShowErrorDetailsType] = useState<ErrorDetailsType>(null);

  useLayoutEffect(() => {
    onLayoutUpdate?.();
  }, [showErrorDetailsType]);

  return <>
    <div
      className={
        classNames('history-item', {
          response: historyItem.parsed.type !== 'command',
          request: historyItem.parsed.type === 'command',
          error: historyItem.parsed.type === 'unknown',
        })}>
      <Text f={Text.Family.Mono}>
        <MessageTagger
          historyItem={historyItem}
          onClickError={setShowErrorDetailsType}
        />
      </Text>
    </div>
    <ErrorDetails
      errorDetailsType={showErrorDetailsType}
      historyItem={historyItem}
      onClose={() => setShowErrorDetailsType(null)}>
    </ErrorDetails>
  </>;
};
