import React from 'react';
import { HelpTooltip, Text } from '@zaber/react-library';

interface Props {
  className?: string;
  label: string;
  children: React.ReactNode;
  help?: React.ReactNode;
}

export const Field: React.FC<Props> = ({ children, label, className, help }) => (
  <div className="field">
    <div>
      <Text t={Text.Type.H5}>{label}</Text>
      {help && <HelpTooltip>{help}</HelpTooltip>}
    </div>
    <div className={className}>{children}</div>
  </div>
);
