import React, { useEffect } from 'react';
import _ from 'lodash';
import {
  Button, Card, Checkbox, Icons, Modal, NoticeBanner, Text, TextArea, ButtonRowConfirmCancel,
  Flex
} from '@zaber/react-library';
import type { ascii } from '@zaber/motion';
import { useSelector } from 'react-redux';
import { match, P } from 'ts-pattern';
import classNames from 'classnames';
import { ModalButtons } from '@zaber/react-library/dist/types/elements/Modal/Modal';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectCommandBatch } from './selectors';
import type { CommandBatchReply } from './types';
import type { CommandBatch } from './reducer';

function replyToStr(reply: ascii.Response) {
  return [
    `@${_.padStart(reply.deviceAddress.toString(), 2, '0')}`,
    reply.axisNumber,
    reply.replyFlag,
    reply.status,
    reply.warningFlag,
    reply.data,
  ].join(' ');
}

const CommandList: React.FC = () => {
  const state = useSelector(selectCommandBatch);

  return <>
    <Card edge="outline" className="command-list" data-testid="command-batch">
      {state.commands.map((cmd, i) => {
        const replies: CommandBatchReply[] | undefined = state.replies[i];
        return <React.Fragment key={i}>
          <Text className={classNames('cmd', { odd: i % 2 })} f={Text.Family.Mono}>{cmd}</Text>
          <div className={classNames('replies', { odd: i % 2 })}>
            {(replies ?? []).map((reply, i) =>
              <Text key={i} f={Text.Family.Mono} className={classNames('reply', { error: reply.replyFlag !== 'OK' })}>
                {match(reply)
                  .with({ replyFlag: 'TIMEOUT' }, reply => reply.replyFlag)
                  .with({ data: P.string }, replyToStr)
                  .exhaustive()}
              </Text>)}
          </div>
        </React.Fragment>;
      })}
    </Card>
    {state.error != null && <NoticeBanner>{state.error}</NoticeBanner>}
  </>;
};

const getCommandBatchContent = (
  state: CommandBatch,
  actions: typeof actionsDefinition,
) =>
  match(state.state)
    .returnType<{content: React.ReactNode; buttons?: ModalButtons}>()
    .with('edit', () => ({
      content:
        <>
          <TextArea value={state.text} onValueChange={actions.commandBatchEdit}/>
          <Checkbox
            checked={state.settings.waitUntilIdle}
            onChecked={waitUntilIdle => actions.commandBatchSetSettings({ waitUntilIdle })}
            labelContent={<>Wait until IDLE after every command</>}/>
          <Checkbox
            checked={state.settings.stopOnError}
            onChecked={stopOnError => actions.commandBatchSetSettings({ stopOnError })}
            labelContent={<>Stop on error</>}/>
        </>,
      buttons: <ButtonRowConfirmCancel
        confirmText="Execute"
        onConfirm={state.text.trim() ? actions.commandBatchExecute : () => null}
        onCancel={actions.commandBatchClose}
      />
    }))
    .with('executing', () => ({
      content: <CommandList/>,
      buttons: <Button onClick={actions.commandBatchStop}>Stop</Button>
    }))
    .with('done', () => ({
      content: <CommandList/>,
      buttons: <ButtonRowConfirmCancel
        confirmText="Execute Again"
        cancelText="Close"
        onConfirm={actions.commandBatchExecute}
        onCancel={actions.commandBatchClose}
      >
        <Button color="grey" onClick={() => actions.commandBatchEdit()}>Edit</Button>
        <Flex.Spacer/>
      </ButtonRowConfirmCancel>
    }))
    .with('closed', () => ({ content: null }))
    .exhaustive();


export const CommandBatchModal: React.FC = () => {
  const state = useSelector(selectCommandBatch);
  const actions = useActions(actionsDefinition);

  useEffect(() => () => {
    actions.commandBatchClose();
  }, []);

  if (state.state === 'closed') { return null }

  const { content, buttons } = getCommandBatchContent(state, actions);

  return <Modal className="command-batch-modal"
    headerIcon={<Icons.Notes/>}
    headerText="Command Batch"
    onRequestClose={actions.commandBatchClose}
    buttons={buttons}
  >
    {content}
  </Modal>;
};
