import type { DeepPartial } from 'redux';

import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { AxesLiveStates } from './reducer';
import type { AxisSettings, BlockMessage, MoveType, Settings } from './types';

export enum ActionTypes {
  MOUNT = 'GCODE_MOUNT',
  UNMOUNT = 'GCODE_UNMOUNT',
  EVALUATE = 'GCODE_EVALUATE',
  EVALUATE_PROGRESS = 'GCODE_EVALUATE_PROGRESS',
  EVALUATE_DONE = 'GCODE_EVALUATE_DONE',
  PERFORM = 'GCODE_PERFORM',
  PERFORMING = 'GCODE_PERFORMING',
  PERFORMING_DONE = 'GCODE_PERFORMING_DONE',
  REWIND = 'GCODE_REWIND',
  ADD_WARNINGS = 'GCODE_ADD_WARNINGS',
  SET_PERFORMING_BLOCK = 'GCODE_SET_PERFORMING_BLOCK',
  STOP = 'GCODE_STOP',
  HARD_STOP = 'GCODE_HARD_STOP',
  UPDATE_SETTINGS = 'GCODE_UPDATE_SETTINGS',
  UPDATE_AXIS_SETTINGS = 'GCODE_UPDATE_AXIS_SETTINGS',
  SELECT_DEVICE = 'GCODE_SELECT_DEVICE',
  LOADING_DEVICE_DONE = 'GCODE_LOADING_DEVICE_DONE',
  SETTINGS_EDITING_DONE = 'GCODE_SETTINGS_EDITING_DONE',
  START_TRANSLATOR = 'GCODE_START_TRANSLATOR',
  STOP_TRANSLATOR = 'GCODE_STOP_TRANSLATOR',
  TRANSLATOR_RUNNING = 'GCODE_TRANSLATOR_RUNNING',
  TRANSLATOR_ERR = 'GCODE_TRANSLATOR_ERR',
  LOAD_CONTENT = 'GCODE_LOAD_CONTENT',
  STORE_CONTENT = 'GCODE_STORE_CONTENT',
  MOVE_AXIS = 'GCODE_MOVE_AXIS',
  MOVE_AXIS_DONE = 'GCODE_MOVE_AXIS_DONE',
  STOP_AXIS = 'GCODE_STOP_AXIS',
  ZERO_AXIS = 'GCODE_ZERO_AXIS',
  SET_FEED_RATE_OVERRIDE = 'GCODE_SET_FEED_RATE_OVERRIDE',
  COMMAND = 'GCODE_COMMAND',
  COMMAND_DONE = 'GCODE_COMMAND_DONE',
  COMMAND_CLEAR = 'GCODE_COMMAND_CLEAR',
  SET_STEP = 'GCODE_SET_STEP',
  SET_PERFORMING_BLOCK_ERROR = 'GCODE_SET_PERFORMING_BLOCK_ERROR',
  UPDATE_POSITION = 'GCODE_UPDATE_POSITION',
  UPDATE_POSITION_DONE = 'GCODE_UPDATE_POSITION_DONE',
  SET_FREE_HOLD = 'GCODE_SET_FREE_HOLD',
}

export interface ActionsToPayloads {
  [ActionTypes.MOUNT]: void;
  [ActionTypes.UNMOUNT]: void;
  [ActionTypes.EVALUATE]: { code: string[] };
  [ActionTypes.EVALUATE_PROGRESS]: { progress: number };
  [ActionTypes.EVALUATE_DONE]: { warnings: BlockMessage[]; error?: BlockMessage };
  [ActionTypes.PERFORM]: { code: string[]; beginIndex: number; endIndex: number; loop: boolean };
  [ActionTypes.PERFORMING]: void;
  [ActionTypes.PERFORMING_DONE]: { error?: BlockMessage };
  [ActionTypes.REWIND]: void;
  [ActionTypes.ADD_WARNINGS]: { warnings: BlockMessage[] };
  [ActionTypes.SET_PERFORMING_BLOCK]: { blockIndex: number };
  [ActionTypes.STOP]: void;
  [ActionTypes.HARD_STOP]: void;
  [ActionTypes.UPDATE_SETTINGS]: { settings: Partial<Settings> };
  [ActionTypes.UPDATE_AXIS_SETTINGS]: { axisIndex: number; settings: Partial<AxisSettings> };
  [ActionTypes.SELECT_DEVICE]: { deviceKey: EntityKey };
  [ActionTypes.LOADING_DEVICE_DONE]: { error?: string };
  [ActionTypes.SETTINGS_EDITING_DONE]: { apply: boolean };
  [ActionTypes.START_TRANSLATOR]: void;
  [ActionTypes.STOP_TRANSLATOR]: void;
  [ActionTypes.TRANSLATOR_RUNNING]: void;
  [ActionTypes.TRANSLATOR_ERR]: { error: string };
  [ActionTypes.LOAD_CONTENT]: { content: string };
  [ActionTypes.STORE_CONTENT]: { content: string };
  [ActionTypes.MOVE_AXIS]: { axisLetter: string; move: MoveType };
  [ActionTypes.MOVE_AXIS_DONE]: { axisLetter: string };
  [ActionTypes.STOP_AXIS]: { axisLetter: string };
  [ActionTypes.ZERO_AXIS]: { axisLetter: string };
  [ActionTypes.SET_FEED_RATE_OVERRIDE]: { override: number };
  [ActionTypes.COMMAND]: { cmd: string };
  [ActionTypes.COMMAND_DONE]: { error?: string; warnings: string[] };
  [ActionTypes.COMMAND_CLEAR]: void;
  [ActionTypes.SET_STEP]: { step: boolean };
  [ActionTypes.SET_PERFORMING_BLOCK_ERROR]: { error: string };
  [ActionTypes.UPDATE_POSITION]: void;
  [ActionTypes.UPDATE_POSITION_DONE]: { updates: DeepPartial<AxesLiveStates>; coordinateSystem: string };
  [ActionTypes.SET_FREE_HOLD]: { hold: boolean };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  mount: () => buildAction(ActionTypes.MOUNT),
  unmount: () => buildAction(ActionTypes.UNMOUNT),
  evaluate: (code: string[]) => buildAction(ActionTypes.EVALUATE, { code }),
  evaluateProgress: (progress: number) => buildAction(ActionTypes.EVALUATE_PROGRESS, { progress }),
  evaluateDone: (warnings: BlockMessage[], error?: BlockMessage) => buildAction(ActionTypes.EVALUATE_DONE, { warnings, error }),
  perform: (code: string[], beginIndex: number, endIndex: number, loop: boolean) =>
    buildAction(ActionTypes.PERFORM, { code, beginIndex, endIndex, loop }),
  performing: () => buildAction(ActionTypes.PERFORMING),
  performingDone: (error?: BlockMessage) => buildAction(ActionTypes.PERFORMING_DONE, { error }),
  rewind: () => buildAction(ActionTypes.REWIND),
  addWarnings: (warnings: BlockMessage[]) => buildAction(ActionTypes.ADD_WARNINGS, { warnings }),
  setPerformingBlock: (blockIndex: number) => buildAction(ActionTypes.SET_PERFORMING_BLOCK, { blockIndex }),
  stop: () => buildAction(ActionTypes.STOP),
  hardStop: () => buildAction(ActionTypes.HARD_STOP),
  updateSettings: (settings: Partial<Settings>) => buildAction(ActionTypes.UPDATE_SETTINGS, { settings }),
  updateAxisSettings: (axisIndex: number, settings: Partial<AxisSettings>) =>
    buildAction(ActionTypes.UPDATE_AXIS_SETTINGS, { axisIndex, settings }),
  selectDevice: (deviceKey: EntityKey) => buildAction(ActionTypes.SELECT_DEVICE, { deviceKey }),
  loadingDeviceDone: (error?: string) => buildAction(ActionTypes.LOADING_DEVICE_DONE, { error }),
  settingsEditingDone: (apply: boolean) => buildAction(ActionTypes.SETTINGS_EDITING_DONE, { apply }),
  startTranslator: () => buildAction(ActionTypes.START_TRANSLATOR),
  stopTranslator: () => buildAction(ActionTypes.STOP_TRANSLATOR),
  translatorRunning: () => buildAction(ActionTypes.TRANSLATOR_RUNNING),
  translatorErr: (error: string) => buildAction(ActionTypes.TRANSLATOR_ERR, { error }),
  loadContent: (content: string) => buildAction(ActionTypes.LOAD_CONTENT, { content }),
  storeContent: (content: string) => buildAction(ActionTypes.STORE_CONTENT, { content }),
  moveAxis: (axisLetter: string, move: MoveType) => buildAction(ActionTypes.MOVE_AXIS, { axisLetter, move }),
  moveAxisDone: (axisLetter: string) => buildAction(ActionTypes.MOVE_AXIS_DONE, { axisLetter }),
  stopAxis: (axisLetter: string) => buildAction(ActionTypes.STOP_AXIS, { axisLetter }),
  zeroAxis: (axisLetter: string) => buildAction(ActionTypes.ZERO_AXIS, { axisLetter }),
  setFeedRateOverride: (override: number) => buildAction(ActionTypes.SET_FEED_RATE_OVERRIDE, { override }),
  command: (cmd: string) => buildAction(ActionTypes.COMMAND, { cmd }),
  commandDone: (warnings: string[], error: string) => buildAction(ActionTypes.COMMAND_DONE, { warnings, error }),
  commandClear: () => buildAction(ActionTypes.COMMAND_CLEAR),
  setStep: (step: boolean) => buildAction(ActionTypes.SET_STEP, { step }),
  setPerformingBlockError: (error: string) => buildAction(ActionTypes.SET_PERFORMING_BLOCK_ERROR, { error }),
  updatePosition: () => buildAction(ActionTypes.UPDATE_POSITION),
  updatePositionDone: (updates: DeepPartial<AxesLiveStates>, coordinateSystem: string) =>
    buildAction(ActionTypes.UPDATE_POSITION_DONE, { updates, coordinateSystem }),
  setFreeHold: (hold: boolean) => buildAction(ActionTypes.SET_FREE_HOLD, { hold }),
};
