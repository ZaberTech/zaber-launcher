import React from 'react';
import { connect } from 'react-redux';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

class User {
  attributes = [{
    Name: 'email',
    Value: 'email@test.io',
  }];
}

class AuthMock {
  static currentUserPoolUser = jest.fn(() => Promise.resolve(new User()));
  static userAttributes = jest.fn((user: User) => user.attributes);
}

jest.mock('aws-amplify', () => ({
  Auth: AuthMock,
}));

const LoggedIn = connect((state: RootState) => ({ email: state.cloud.loggedInUser!.email }))(
  ({ email }) => <>LoggedIn {email}</>
);
const UnauthenticatedRoutes = () => <>Unauthenticated</>;

jest.mock('./LoggedIn', () => ({
  LoggedIn,
}));
jest.mock('./UnauthenticatedRoutes', () => ({
  UnauthenticatedRoutes,
}));

@injectable()
class AwsCredentialsServiceMock {
  setUser = jest.fn().mockResolvedValue(undefined);
}

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore } from '../test';
import { AwsCredentialsService } from '../app_components';
import type { RootState } from '../store';

import { Cloud } from './Cloud';
import { TEST_CHECK_LOGGED_IN } from './sagas';

let container: Container;
let credentialsMock: AwsCredentialsServiceMock;

const TestCloud = wrapWithNewStore(Cloud);

let wrapper: RenderResult;

beforeEach(() => {
  AuthMock.currentUserPoolUser.mockClear();
  AuthMock.userAttributes.mockClear();

  container = createContainer();
  container.bind<unknown>(AwsCredentialsService).to(AwsCredentialsServiceMock);
  credentialsMock = container.get<unknown>(AwsCredentialsService) as AwsCredentialsServiceMock;

  wrapper = render(<TestCloud/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

test('displays a message while waiting for cloud', () => {
  wrapper.getByText(/Cannot connect to Cloud/);
});

test('checks for logged in user and sets credentials', async () => {
  TestCloud.testStore.dispatch({ type: TEST_CHECK_LOGGED_IN });

  await waitUntilPass(() => wrapper.getByText('LoggedIn email@test.io'));

  expect(credentialsMock.setUser).toHaveBeenCalledWith(expect.any(User));
});

test('error leads to user being unauthenticated', async () => {
  AuthMock.currentUserPoolUser.mockRejectedValueOnce(new Error('No user'));
  TestCloud.testStore.dispatch({ type: TEST_CHECK_LOGGED_IN });

  await waitUntilPass(() => wrapper.getByText('Unauthenticated'));

  expect(credentialsMock.setUser).toHaveBeenCalledWith('unauthenticated');
});

describe('keeps trying on network error', () => {
  beforeEach(() => {
    jest.useFakeTimers();

    AuthMock.currentUserPoolUser.mockRejectedValueOnce({ code: 'NetworkError' });
    TestCloud.testStore.dispatch({ type: TEST_CHECK_LOGGED_IN });
  });
  afterEach(() => {
    jest.useRealTimers();
  });

  test('try again buttons tries again', async () => {
    await waitUntilPass(() => expect(AuthMock.currentUserPoolUser).toHaveBeenCalledTimes(1));

    fireEvent.click(wrapper.getByText('Try Again'));

    await waitUntilPass(() => wrapper.getByText('LoggedIn email@test.io'));
    expect(AuthMock.currentUserPoolUser).toHaveBeenCalledTimes(2);
  });

  test('keeps trying with timeout', async () => {
    await waitUntilPass(() => expect(AuthMock.currentUserPoolUser).toHaveBeenCalledTimes(1));

    AuthMock.currentUserPoolUser.mockRejectedValueOnce({ code: 'NetworkError' });
    jest.advanceTimersByTime(1000);
    await waitUntilPass(() => expect(AuthMock.currentUserPoolUser).toHaveBeenCalledTimes(2));

    jest.advanceTimersByTime(2000);
    await waitUntilPass(() => wrapper.getByText('LoggedIn email@test.io'));
    expect(AuthMock.currentUserPoolUser).toHaveBeenCalledTimes(3);
  });
});
