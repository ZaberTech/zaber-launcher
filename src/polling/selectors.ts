import { buildDictionary } from '@zaber/toolbox';
import _ from 'lodash';
import { createSelector } from 'reselect';

import { selectPolling } from '../store';

import { PollRequest, Polled, PolledGetter, POLLED } from './types';

export const selectRequests = createSelector(selectPolling, state => state.requests);
export const selectResults = createSelector(selectPolling, state => state.results);

export type ActivePollGetter<P extends Polled> = { getter: PolledGetter[P]; fast: boolean };
function activeGetters<P extends Polled>(entries: PollRequest[P]): ActivePollGetter<P>[] {
  const activeRequests = _.pickBy(entries, entry => entry.fast > 0 || entry.slow > 0);
  return Object.entries(activeRequests).map(([_, entry]): ActivePollGetter<P> => (
    { getter: entry.get as PolledGetter[P], fast: entry.fast > 0 }
  ));
}

const selectPollGetters = createSelector(selectRequests, requests => buildDictionary(Object.keys(requests), deviceKey => ({
  settings: activeGetters<'settings'>(requests[deviceKey].settings),
  analogIo: activeGetters<'analogIo'>(requests[deviceKey].analogIo),
  warnings: activeGetters<'warnings'>(requests[deviceKey].warnings),
  triggers: activeGetters<'triggers'>(requests[deviceKey].triggers),
})));

export const selectActivePolls = createSelector(selectPollGetters, getters => (
  _.pickBy(getters, getter => POLLED
    .map(p => getter[p].length)
    .some(l => l > 0)
  )
));
