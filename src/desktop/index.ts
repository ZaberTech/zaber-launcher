import * as remote from '@electron/remote';


import { handleNonCriticalError } from '../errors/errors';

export function openExternalLink(url: string): void {
  remote.shell.openExternal(url, { activate: true }).catch(handleNonCriticalError);
}

export function restart(): void {
  remote.app.relaunch();
  remote.app.exit();
}

// using alerts break all input boxes
// https://stackoverflow.com/questions/56805920/cant-edit-input-text-field-after-window-alert
window.alert = () => { throw new Error('alert not available in electron') };
