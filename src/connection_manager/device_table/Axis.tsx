import React, { HTMLProps } from 'react';
import classNames from 'classnames';

import type { DeviceInfo, AxisInfo } from '../types';

import { AxisHeader } from './AxisHeader';

interface Props extends HTMLProps<HTMLDivElement> {
  device: DeviceInfo;
  axis: AxisInfo;
}

export const Axis: React.FC<Props> = ({ children, device, axis, className, ...rest }) => (
  <div className={classNames('axis', className)} {...rest}>
    <AxisHeader device={device} axis={axis}/>
    <span className="spacer"/>
    {children}
  </div>);
