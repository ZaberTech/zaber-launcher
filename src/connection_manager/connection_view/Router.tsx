import React from 'react';
import classNames from 'classnames';
import { Chevron, Text, NoticeMessage } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { tryExtractRouterKey } from '../../keys';
import type { RouterState } from '../reducer';
import { RouterName } from '../components/RouterName';
import { naturalSort, useActions } from '../../utils';

import type { PassedProperties } from './types';
import { Connection } from './Connection';
import { AttentionRequester } from './AttentionRequester';
import { selectExpanded } from './selectors';
import { actions as actionsCreators } from './actions';

interface Props extends PassedProperties {
  router: RouterState;
}

export const Router: React.FC<Props> = ({ router, ...passedProperties }) => {
  const actions = useActions(actionsCreators);
  const expanded = useSelector(selectExpanded)[router.key] ?? false;
  if (router.connections?.length === 0) {
    return null;
  }
  return (
    <div className={classNames({ collapsed: !expanded }, 'router')}>
      <div className="router-info">
        <div className="router-name" onClick={() => actions.setExpanded(router.key, !expanded)}>
          <Chevron
            expanded={expanded}
            titleExpanded="Collapse Router" titleCollapsed="Expand Router"/>
          <RouterName router={router} textType={Text.Type.H5}/>
          <div className="spacer"/>
          {!expanded && <AttentionRequester showChildrenCount
            entityKey={router.key}
            extractor={tryExtractRouterKey}
            attentionRequests={passedProperties.attentionRequests}
          />}
        </div>
        {expanded && <>
          {router.loadingConnections && <div>Loading Connections...</div>}
          {router.connectionsErr && <NoticeMessage headline="Cannot connect">
            {router.connectionsErr}
          </NoticeMessage>}
        </>}
      </div>
      <div className="connections">
        {[...(router.connections ?? [])]
          .sort(naturalSort)
          .map(connectionKey => (
            <Connection
              key={connectionKey}
              connectionKey={connectionKey}
              {...passedProperties}
            />
          ))}
      </div>
    </div>
  );
};
