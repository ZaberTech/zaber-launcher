import React from 'react';
import { HeaderCard, NoticeBorder, NoticeMessage } from '@zaber/react-library';
import type { TriggerEnabledState } from '@zaber/motion/ascii';

import type { EntityKey } from '../keys';

import { TriggerDetails } from './TriggerDetails';
import type { TriggerState } from './reducer';
import { TriggerHeader } from './TriggerHeader';

interface TriggerProps {
  deviceKey: EntityKey;
  triggerIndex: number;
  enabledState: TriggerEnabledState | null;
  triggerState: TriggerState;
  expanded: boolean;
}

export const Trigger: React.FC<TriggerProps> = ({ deviceKey, triggerIndex, enabledState, triggerState, expanded }) => {
  const { error } = triggerState;

  return <NoticeBorder className="trigger-card" type={error != null ? 'error' : null}>
    <HeaderCard
      header={<TriggerHeader
        deviceKey={deviceKey}
        triggerIndex={triggerIndex}
        enabledState={enabledState}
        triggerState={triggerState}
        expanded={expanded}
      />}
    >
      {expanded && <TriggerDetails deviceKey={deviceKey} triggerIndex={triggerIndex} state={triggerState}/>}
    </HeaderCard>
    {error != null &&
      <NoticeMessage className="error-message">{error}</NoticeMessage>
    }
  </NoticeBorder>;
};
