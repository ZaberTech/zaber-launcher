const CURRENT_VERSION = '1.2.3';
jest.mock('@electron/remote', () => ({
  app: {
    getVersion: jest.fn(() => CURRENT_VERSION),
    getName: () => 'ElectronMock',
    getPath: (_: string) => '',
  },
}));


import React from 'react';
import type { Container } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore, mockStorage, StorageMock } from '../test';
import { IpcMock, mockIpc } from '../ipc/mock';

import { UpdateNotification } from './UpdateNotification';
import { actions } from './actions';
import { checkIfNewVersionInstalled, LATEST_CHECKED_VERSION_KEY } from './sagas';
import { selectCurrentAppVersion } from './selectors';

const TestUpdateNotification = wrapWithNewStore(UpdateNotification);

let container: Container;
let wrapper: RenderResult;

let ipcMock: IpcMock;
let storageMock: StorageMock;

beforeAll(() => {
  jest.useFakeTimers();
});

afterAll(() => {
  jest.useRealTimers();
});

beforeEach(() => {
  container = createContainer();
  ipcMock = mockIpc(container);
  storageMock = mockStorage(container);

  wrapper = render(<TestUpdateNotification/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

test('renders nothing when there is no new version', async () => {
  expect(wrapper.baseElement.textContent).toBeFalsy();
});

describe('on new version', () => {
  beforeEach(() => {
    TestUpdateNotification.testStore.dispatch(actions.newVersionReady('1.2.3'));
  });

  test('shows new version when is available', async () => {
    wrapper.getByText(/1\.2\.3/);
  });

  test('ignoring version hides the notification', async () => {
    fireEvent.click(wrapper.getByTitle('Ignore Update'));
    expect(wrapper.queryByTitle('Ignore Update')).toBeNull();
  });

  test('clicking on update sends action to the main process', async () => {
    fireEvent.click(wrapper.getByText('Update and Restart'));
    expect(ipcMock.sendActionToMainProcess).toHaveBeenCalled();
    wrapper.getByText('Preparing the update, please wait...');
  });
});

describe('checkIfNewVersionInstalled', () => {
  test('stores the new version and invokes app updated action if the stored version differs', async () => {
    storageMock.stored[LATEST_CHECKED_VERSION_KEY] = '1.2.2';
    await TestUpdateNotification.testStore.saga.run(checkIfNewVersionInstalled).toPromise();

    expect(storageMock.save).toHaveBeenLastCalledWith(LATEST_CHECKED_VERSION_KEY, CURRENT_VERSION);
    expect(selectCurrentAppVersion(TestUpdateNotification.testStore.getState())).toBe(CURRENT_VERSION);
  });
  test('does not store the new version or invoke app updated action if the stored version is the same', async () => {
    storageMock.stored[LATEST_CHECKED_VERSION_KEY] = CURRENT_VERSION;
    await TestUpdateNotification.testStore.saga.run(checkIfNewVersionInstalled).toPromise();

    expect(storageMock.save).not.toHaveBeenCalled();
  });
});

describe('on new version installed', () => {
  beforeEach(() => {
    TestUpdateNotification.testStore.dispatch(actions.setCurrentVersion(CURRENT_VERSION, true));
  });

  test('displays notification on new version that can be closed', async () => {
    const message = `New version (${CURRENT_VERSION}) of Zaber Launcher has been successfully installed.`;
    wrapper.getByText(message);
    fireEvent.click(wrapper.getByTitle('Close'));
    await waitUntilPass(() =>
      expect(wrapper.queryByText(message)).toBeNull()
    );
  });

  test('notification closes itself after timeout', async () => {
    wrapper.getByText(/successfully installed/);
    jest.runAllTimers();
    await waitUntilPass(() =>
      expect(wrapper.queryByText(/successfully installed/)).toBeNull()
    );
  });
});
