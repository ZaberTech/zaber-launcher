import 'reflect-metadata';
import type { Container } from 'inversify';

import { createContainer, destroyContainer } from '../container';
import { mockStorage, StorageMock } from '../test';

import { Cache } from '.';

const TEST_KEY = 'test_key';

let container: Container;
let storageMock: StorageMock;

let cache: Cache;

beforeEach(() => {
  container = createContainer();
  storageMock = mockStorage(container);

  cache = container.get(Cache);
});

afterEach(() => {
  destroyContainer();
  container = null!;
});

test('saves and retrieves the data', () => {
  cache.save(TEST_KEY, [{ id: 1 }, { id: 2 }]);
  const cached = cache.load(TEST_KEY);

  expect(cached).toEqual([{ id: 1 }, { id: 2 }]);
});

test('returns null when the cache data are for a different version', () => {
  cache.save(TEST_KEY, [{ id: 1 }, { id: 2 }]);

  (storageMock.stored.cache_test_key as { version: string }).version = 'Release 1.2.3';

  const cached = cache.load(TEST_KEY);
  expect(cached).toBe(null);
});
