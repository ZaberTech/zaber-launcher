import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { injectable } from 'inversify';
import { act } from 'react-dom/test-utils';
import { Subject } from 'rxjs';

import { createContainer, destroyContainer } from '../container';
import { makeConnectionKey, makeRouterKey } from '../keys';
import { MessageRoutersService } from '../message_router';
import { buildStore } from '../store/build';
import { waitUntilPass, wrapWithNewStore } from '../test';
import {
  MessageRoutersServiceMockBase, RouterConnectionMockBase, AxisMockBase,
  DeviceMockBase, ConnectionMockBase,
} from '../test/mocks/ascii';

import { DisconnectButton, DISPLAY_DELAY } from './DisconnectButton';
import { monitorRouters } from './sagas';
import { selectConnections, selectRouters } from './selectors';

type AxisMock = AxisMockBase;
type DeviceMock = DeviceMockBase<AxisMockBase>;

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  readonly ended = new Subject<true>();
  ensureOpen = jest.fn().mockResolvedValue(undefined);
  close = jest.fn().mockResolvedValue(undefined);
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
  readonly ended = new Subject<true>();
  readonly newRoutedConnection = new Subject<ConnectionMock>();
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMockBase, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMockBase;
  DeviceCtor = DeviceMockBase;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;

  readonly newConnections = new Subject<RouterConnectionMock>();
}

let store: ReturnType<typeof buildStore>;
let serviceMock: MessageRoutersServiceMock;

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  serviceMock = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;

  store = buildStore();
  store.saga.run(monitorRouters);
});

afterEach(() => {
  destroyContainer();

  store = null!;
  serviceMock = null!;
});

test('monitors router connections maintaining the status', async () => {
  const URL = 'url1';
  const key = makeRouterKey(URL);

  const connection = await serviceMock.getConnection(URL);
  serviceMock.newConnections.next(connection);

  await waitUntilPass(() => {
    const status = selectRouters(store.getState())[key];
    expect(status).toMatchObject({ connected: true });
  });

  connection.ended.next(true);
  connection.ended.complete();

  await waitUntilPass(() => {
    const status = selectRouters(store.getState())[key];
    expect(status).toMatchObject({ connected: false });
  });
});

describe('routed connections', () => {
  const URL = 'url1';
  const routerKey = makeRouterKey(URL);
  let routerConnection: RouterConnectionMock;

  const COM = 'COM1';
  const key = makeConnectionKey(routerKey, COM);

  beforeEach(async () => {
    routerConnection = await serviceMock.getConnection(URL);
    serviceMock.newConnections.next(routerConnection);
  });

  beforeAll(() => {
    jest.useFakeTimers();
  });
  afterAll(() => {
    jest.useRealTimers();
  });

  test('monitors routed connections maintaining the status', async () => {
    const connection = await routerConnection.getConnection(COM);
    routerConnection.newRoutedConnection.next(connection);

    await waitUntilPass(() => {
      const status = selectConnections(store.getState())[key];
      expect(status).toMatchObject({ connected: true });
    });

    connection.ended.next(true);
    connection.ended.complete();

    await waitUntilPass(() => {
      const status = selectConnections(store.getState())[key];
      expect(status).toMatchObject({ connected: false });
    });
  });

  test('disconnects routed connection upon request', async () => {
    const connection = await routerConnection.getConnection(COM);
    routerConnection.newRoutedConnection.next(connection);

    await waitUntilPass(() => {
      const status = selectConnections(store.getState())[key];
      expect(status).toMatchObject({ connected: true });
    });

    const TestButton = wrapWithNewStore(DisconnectButton);
    const wrapper = render(<TestButton connectionKey={key} store={store}/>);
    act(() => { jest.advanceTimersByTime(DISPLAY_DELAY) });
    fireEvent.click(wrapper.getByRole('button'));
    wrapper.unmount();

    await waitUntilPass(() => expect(connection.close).toHaveBeenCalled());
  });

  test('does not do anything if the opening fails', async () => {
    const connection = await routerConnection.getConnection(COM);
    connection.ensureOpen.mockRejectedValue(new Error('Failed to open'));
    routerConnection.newRoutedConnection.next(connection);

    await waitUntilPass(() => {
      expect(connection.ensureOpen).toHaveBeenCalled();

      const status = selectConnections(store.getState())[key] ?? { connected: false };
      expect(status).toMatchObject({ connected: false });
    });
  });
});
