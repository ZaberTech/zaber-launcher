import type { Length } from '@zaber/motion';
import type { DeepPartial } from 'redux';

import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';
import type { MeasurementOK } from '../units';

import type {
  Axes, MicroscopeConfig, MonitorInfo, MonitorStableInfo, OperationsArgs, OperationsResult, TriggerSettings, StoredPosition, TestType,
} from './types';
import type { Preset, PresetData } from './presets';
import type { State } from './reducer';

type ValueOf<a> = a extends readonly unknown[] ? a[number] : a[keyof a];

export enum ActionTypes {
  SET_SELECTED_KEY = 'MICROSCOPE_SET_SELECTED_KEY',

  LOAD_CONFIG = 'MICROSCOPE_LOAD_CONFIG',
  LOAD_CONFIG_DONE = 'MICROSCOPE_LOAD_CONFIG_DONE',
  EDIT_CONFIG = 'MICROSCOPE_EDIT_CONFIG',
  RESET_CONFIG_DIALOG = 'MICROSCOPE_RESET_CONFIG_DIALOG',
  WRITE_CONFIG = 'MICROSCOPE_WRITE_CONFIG',
  WRITE_CONFIG_DONE = 'MICROSCOPE_WRITE_CONFIG_DONE',
  SEARCH_WDI = 'MICROSCOPE_SEARCH_WDI',
  SEARCH_WDI_DONE = 'MICROSCOPE_SEARCH_WDI_DONE',

  TEST = 'MICROSCOPE_TEST',
  TEST_PROGRESS = 'MICROSCOPE_TEST_PROGRESS',
  TEST_DONE = 'MICROSCOPE_TEST_DONE',
  CLEAR_TEST_RESULT = 'MICROSCOPE_CLEAR_TEST_RESULT',

  MOUNT = 'MICROSCOPE_MOUNT',
  UNMOUNT = 'MICROSCOPE_UNMOUNT',

  RESTART_MONITOR = 'MICROSCOPE_RESTART_MONITOR',
  MONITOR_ERROR = 'MICROSCOPE_MONITOR_ERROR',
  MONITOR_STABLE_INFO = 'MICROSCOPE_MONITOR_STABLE_INFO',
  MONITOR_INFO = 'MICROSCOPE_MONITOR_INFO',
  MONITOR_REFRESH = 'MICROSCOPE_REFRESH',
  MONITOR_REFRESH_STARTED = 'MICROSCOPE_REFRESH_STARTED',
  BLOCK_MONITOR = 'MICROSCOPE_BLOCK_MONITOR',

  OPERATION_START = 'MICROSCOPE_OPERATION_START',
  OPERATION_DONE = 'MICROSCOPE_OPERATION_DONE',
  OPERATION_CLEAR_ERROR = 'MICROSCOPE_OPERATION_CLEAR_ERROR',

  FACTORY_RESET_SET_STEP = 'MICROSCOPE_FACTORY_RESET_SET_STEP',
  FACTORY_RESET_ADD_ERROR = 'MICROSCOPE_FACTORY_RESET_ADD_ERROR',

  UPDATE_UNITS = 'MICROSCOPE_UPDATE_UNITS',
  UPDATE_AXIS_STEP = 'MICROSCOPE_UPDATE_AXIS_STEP',

  STOP_AXIS_MOVEMENT = 'MICROSCOPE_STOP_AXIS_MOVEMENT',

  ADD_PRESET = 'MICROSCOPE_ADD_PRESET',
  REMOVE_PRESET = 'MICROSCOPE_REMOVE_PRESET',
  APPLY_PRESET = 'MICROSCOPE_APPLY_PRESET',
  APPLY_PRESET_DATA = 'MICROSCOPE_APPLY_PRESET_DATA',
  APPLY_PRESET_DONE = 'MICROSCOPE_APPLY_PRESET_DONE',
  PRESETS_LOADED = 'MICROSCOPE_PRESETS_LOADED',

  PRESET_EDITOR_OPEN = 'MICROSCOPE_PRESET_EDITOR_OPEN',
  PRESET_EDITOR_CANCEL = 'MICROSCOPE_PRESET_EDITOR_CANCEL',
  PRESET_EDITOR_SAVE = 'MICROSCOPE_PRESET_EDITOR_SAVE',
  UPDATE_PRESET = 'MICROSCOPE_UPDATE_PRESET',

  LOAD_STORED_POSITIONS = 'MICROSCOPE_LOAD_STORED_POSITIONS',
  UPDATE_STORED_POSITION = 'MICROSCOPE_UPDATE_STORED_POSITION',
  DELETE_STORED_POSITION = 'MICROSCOPE_DELETE_STORED_POSITION',
  ADD_STORED_POSITION = 'MICROSCOPE_ADD_STORED_POSITION',

  SET_PRODUCTION_MODE = 'MICROSCOPE_SET_PRODUCTION_MODE',
  SET_DEMO_MODE = 'MICROSCOPE_SET_DEMO_MODE',

  SET_AUTOFOCUS_SETTINGS_OPEN = 'MICROSCOPE_SET_AUTOFOCUS_SETTINGS_OPEN',

  CLEAR_VALIDATION_ERRORS = 'MICROSCOPE_CLEAR_VALIDATION_ERRORS',
  ADD_VALIDATION_ERROR = 'MICROSCOPE_ADD_VALIDATION_ERROR',

  SET_TRIGGER_SETTINGS_OPEN = 'MICROSCOPE_SET_TRIGGER_SETTINGS_OPEN',
  UPDATE_TRIGGER_SETTINGS = 'MICROSCOPE_UPDATE_TRIGGER_SETTINGS',
}

export interface ActionsToPayloads {
  [ActionTypes.SET_SELECTED_KEY]: { key: EntityKey | null };
  [ActionTypes.LOAD_CONFIG]:  void;
  [ActionTypes.LOAD_CONFIG_DONE]: { loaded: MicroscopeConfig } | { error: string };
  [ActionTypes.EDIT_CONFIG]: { config: Partial<MicroscopeConfig> };
  [ActionTypes.RESET_CONFIG_DIALOG]: { open: boolean };
  [ActionTypes.WRITE_CONFIG]: void;
  [ActionTypes.WRITE_CONFIG_DONE]: { error?: string };
  [ActionTypes.TEST]: { testType: TestType };
  [ActionTypes.TEST_PROGRESS]: { progress: string };
  [ActionTypes.TEST_DONE]: { error?: string };
  [ActionTypes.CLEAR_TEST_RESULT]: void;
  [ActionTypes.SEARCH_WDI]: void;
  [ActionTypes.SEARCH_WDI_DONE]: { error: string} | { host: string };

  [ActionTypes.MOUNT]: void;
  [ActionTypes.UNMOUNT]: void;

  [ActionTypes.RESTART_MONITOR]: void;
  [ActionTypes.MONITOR_ERROR]: { error: string };
  [ActionTypes.MONITOR_STABLE_INFO]: { info: MonitorStableInfo };
  [ActionTypes.MONITOR_INFO]: { info: MonitorInfo };
  [ActionTypes.MONITOR_REFRESH]: { burst?: number };
  [ActionTypes.MONITOR_REFRESH_STARTED]: void;
  [ActionTypes.BLOCK_MONITOR]: { process: string; block: boolean };

  [ActionTypes.OPERATION_START]: { name: keyof OperationsArgs; args: ValueOf<OperationsArgs> };
  [ActionTypes.OPERATION_DONE]: { name: keyof OperationsArgs; error?: string; result?: unknown };
  [ActionTypes.OPERATION_CLEAR_ERROR]: { name: keyof OperationsArgs };

  [ActionTypes.FACTORY_RESET_SET_STEP]: { step: State['factoryReset']['step'] };
  [ActionTypes.FACTORY_RESET_ADD_ERROR]: { error: string };

  [ActionTypes.UPDATE_UNITS]: { axis: Axes; units: Length };
  [ActionTypes.UPDATE_AXIS_STEP]: { axis: Axes; step: MeasurementOK };

  [ActionTypes.STOP_AXIS_MOVEMENT]: { axis: Axes };

  [ActionTypes.ADD_PRESET]: { preset: PresetData };
  [ActionTypes.REMOVE_PRESET]: { preset: number };
  [ActionTypes.APPLY_PRESET]: { preset: number };
  [ActionTypes.APPLY_PRESET_DATA]: { preset: PresetData };
  [ActionTypes.APPLY_PRESET_DONE]: { preset: number; error?: string };
  [ActionTypes.PRESETS_LOADED]: { presets: Preset[] };

  [ActionTypes.UPDATE_PRESET]: { preset: number; update: DeepPartial<Preset> };
  [ActionTypes.PRESET_EDITOR_OPEN]: void;
  [ActionTypes.PRESET_EDITOR_CANCEL]: void;
  [ActionTypes.PRESET_EDITOR_SAVE]: void;

  [ActionTypes.LOAD_STORED_POSITIONS]: { positions: StoredPosition[] };
  [ActionTypes.UPDATE_STORED_POSITION]: { index: number; position: Partial<StoredPosition> };
  [ActionTypes.DELETE_STORED_POSITION]: { index: number };
  [ActionTypes.ADD_STORED_POSITION]: { position: StoredPosition };

  [ActionTypes.SET_PRODUCTION_MODE]: { production: boolean; store: boolean };
  [ActionTypes.SET_DEMO_MODE]: { demo: boolean };

  [ActionTypes.SET_AUTOFOCUS_SETTINGS_OPEN]: { open: boolean };

  [ActionTypes.CLEAR_VALIDATION_ERRORS]: void;
  [ActionTypes.ADD_VALIDATION_ERROR]: { error: string };

  [ActionTypes.SET_TRIGGER_SETTINGS_OPEN]: { change: 'open' | 'close' | 'save' };
  [ActionTypes.UPDATE_TRIGGER_SETTINGS]: { settings: Partial<TriggerSettings> };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  setSelectedKey: (key: EntityKey | null) => buildAction(ActionTypes.SET_SELECTED_KEY, { key }),
  loadConfig: () => buildAction(ActionTypes.LOAD_CONFIG),
  loadConfigDone: (loaded: MicroscopeConfig) => buildAction(ActionTypes.LOAD_CONFIG_DONE, { loaded }),
  loadConfigError: (error: string) => buildAction(ActionTypes.LOAD_CONFIG_DONE, { error }),
  editConfig: (config: Partial<MicroscopeConfig>) => buildAction(ActionTypes.EDIT_CONFIG, { config }),
  resetConfigDialog: (open: boolean = false) => buildAction(ActionTypes.RESET_CONFIG_DIALOG, { open }),
  writeConfig: () => buildAction(ActionTypes.WRITE_CONFIG),
  writeConfigDone: (error?: string) => buildAction(ActionTypes.WRITE_CONFIG_DONE, { error }),
  test: (testType: TestType) => buildAction(ActionTypes.TEST, { testType }),
  testProgress: (progress: string) => buildAction(ActionTypes.TEST_PROGRESS, { progress }),
  testDone: (error?: string) => buildAction(ActionTypes.TEST_DONE, { error }),
  clearTestResult: () => buildAction(ActionTypes.CLEAR_TEST_RESULT),
  searchWdi: () => buildAction(ActionTypes.SEARCH_WDI),
  searchWdiDone: (payload: { error: string } | { host: string }) => buildAction(ActionTypes.SEARCH_WDI_DONE, payload),

  mount: () => buildAction(ActionTypes.MOUNT),
  unmount: () => buildAction(ActionTypes.UNMOUNT),

  restartMonitor: () => buildAction(ActionTypes.RESTART_MONITOR),
  monitorError: (error: string) => buildAction(ActionTypes.MONITOR_ERROR, { error }),
  monitorStableInfo: (info: MonitorStableInfo) => buildAction(ActionTypes.MONITOR_STABLE_INFO, { info }),
  monitorInfo: (info: MonitorInfo) => buildAction(ActionTypes.MONITOR_INFO, { info }),
  refreshMonitor: (burst?: number) => buildAction(ActionTypes.MONITOR_REFRESH, { burst }),
  monitorRefreshStarted: () => buildAction(ActionTypes.MONITOR_REFRESH_STARTED),
  blockMonitor: (process: 'multidacq', block: boolean) => buildAction(ActionTypes.BLOCK_MONITOR, { process, block }),

  operationStart: <Op extends keyof OperationsArgs>(name: Op, args?: OperationsArgs[Op]) =>
    buildAction(ActionTypes.OPERATION_START, { name, args }),
  operationDoneErr: <Op extends keyof OperationsArgs>(name: Op, error: string) =>
    buildAction(ActionTypes.OPERATION_DONE, { name, error }),
  operationDone: <Op extends keyof OperationsArgs>(name: Op, result: OperationsResult<Op>) =>
    buildAction(ActionTypes.OPERATION_DONE, { name, result }),
  operationClearError: <Op extends keyof OperationsArgs>(name: Op) =>
    buildAction(ActionTypes.OPERATION_CLEAR_ERROR, { name }),

  factoryResetSetStep: (step: State['factoryReset']['step']) => buildAction(ActionTypes.FACTORY_RESET_SET_STEP, { step }),
  factoryResetAddError: (error: string) => buildAction(ActionTypes.FACTORY_RESET_ADD_ERROR, { error }),

  updateUnits: (axis: Axes, units: Length) => buildAction(ActionTypes.UPDATE_UNITS, { axis, units }),
  updateAxisStep: (axis: Axes, step: MeasurementOK) => buildAction(ActionTypes.UPDATE_AXIS_STEP, { axis, step }),

  stopAxisMovement: (axis: Axes) => buildAction(ActionTypes.STOP_AXIS_MOVEMENT + axis as ActionTypes),

  addPreset: (preset: PresetData) => buildAction(ActionTypes.ADD_PRESET, { preset }),
  updatePreset: (preset: number, update: DeepPartial<Preset>) => buildAction(ActionTypes.UPDATE_PRESET, { preset, update }),
  removePreset: (preset: number) => buildAction(ActionTypes.REMOVE_PRESET, { preset }),
  applyPreset: (preset: number) => buildAction(ActionTypes.APPLY_PRESET, { preset }),
  applyPresetData: (preset: PresetData) => buildAction(ActionTypes.APPLY_PRESET_DATA, { preset }),
  applyPresetDone: (preset: number, error?: string) => buildAction(ActionTypes.APPLY_PRESET_DONE, { preset, error }),
  presetsLoaded: (presets: Preset[]) => buildAction(ActionTypes.PRESETS_LOADED, { presets }),

  presetEditorOpen: () => buildAction(ActionTypes.PRESET_EDITOR_OPEN),
  presetEditorCancel: () => buildAction(ActionTypes.PRESET_EDITOR_CANCEL),
  presetEditorSave: () => buildAction(ActionTypes.PRESET_EDITOR_SAVE),

  loadStoredPositions: (positions: StoredPosition[]) =>
    buildAction(ActionTypes.LOAD_STORED_POSITIONS, { positions }),
  updateStoredPosition: (index: number, position: Partial<StoredPosition>) =>
    buildAction(ActionTypes.UPDATE_STORED_POSITION, { index, position }),
  deleteStoredPosition: (index: number) => buildAction(ActionTypes.DELETE_STORED_POSITION, { index }),
  addStoredPosition: (position: StoredPosition) => buildAction(ActionTypes.ADD_STORED_POSITION, { position }),

  setProductionMode: (production: boolean, store = true) => buildAction(ActionTypes.SET_PRODUCTION_MODE, { production, store }),
  setDemoMode: (demo: boolean) => buildAction(ActionTypes.SET_DEMO_MODE, { demo }),

  setAutofocusSettingsOpen: (open: boolean) => buildAction(ActionTypes.SET_AUTOFOCUS_SETTINGS_OPEN, { open }),

  clearValidationErrors: () => buildAction(ActionTypes.CLEAR_VALIDATION_ERRORS),
  addValidationError: (error: string) => buildAction(ActionTypes.ADD_VALIDATION_ERROR, { error }),

  setTriggerSettingsOpen: (change: 'open' | 'close' | 'save') => buildAction(ActionTypes.SET_TRIGGER_SETTINGS_OPEN, { change }),
  updateTriggerSettings: (settings: Partial<TriggerSettings>) => buildAction(ActionTypes.UPDATE_TRIGGER_SETTINGS, { settings }),
};
