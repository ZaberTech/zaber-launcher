import React from 'react';
import { NumericInput, Text, TextArea } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectEditValidation, selectEditedTable } from './selectors';

export const Table: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const table = useSelector(selectEditedTable);
  const validation = useSelector(selectEditValidation);
  return <div className="table">
    <Text>Calibration Type</Text>
    <NumericInput
      value={table.calibrationType}
      onNumberChange={calibrationType => actions.updateTable({ calibrationType })}/>
    <Text>Table Width</Text>
    <NumericInput
      value={table.width}
      onNumberChange={width => actions.updateTable({ width })}/>
    <Text>Table Offset</Text>
    <NumericInput
      value={table.offset}
      onNumberChange={offset => actions.updateTable({ offset })}/>
    <Text className="values-label">Values</Text>
    <TextArea
      className="values"
      value={table.values}
      onChange={e => actions.updateTable({ values: e.target.value })}/>
    {validation != null && <Text t={Text.Type.Helper} className="validation-error">{validation}</Text>}
  </div>;
};
