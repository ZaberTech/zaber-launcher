import React from 'react';
import { Chevron, Icons, Text } from '@zaber/react-library';

import { useActions } from '../utils';
import { commandExists } from '../devices';

import { actions as actionsDefinition } from './actions';
import { PreciseMovementControl } from './PreciseMovementControl';
import type { AxisStateComplete } from './selectors';
import { MovementType } from './types';

export const PreciseMovement: React.FC<{axis: AxisStateComplete}> = ({ axis }) => {
  const actions = useActions(actionsDefinition);
  const fwVersion = axis.device?.identity.firmwareVersion;
  const cyclable = commandExists('move rel ? ?', axis);

  const cycleTip = fwVersion.major < 7
    ? 'Cycling is not supported with Firmware 6'
    : fwVersion.minor < 25
      ? 'Please update Firmware in order to use the cycling feature'
      : 'Cycle at specified velocity';

  return (
    <div data-testid="precise-movements">
      <div className="title-container">
        <div className="title" onClick={() => actions.setCardExpanded(axis.key, !axis.expanded)}>
          <Chevron
            expanded={axis.expanded}
            titleExpanded="Collapse Precise Movement section"
            titleCollapsed="Expand Precise Movement section"
          />
          <span>Precise Movement</span>
        </div>
      </div>
      {axis.expanded && axis.travelInfo != null && <div className="section-content">
        <PreciseMovementControl
          axis={axis}
          setting="pos"
          title="Absolute Position Control"
          label={{
            label: 'Move to Absolute Position',
            help: <Text>Moves the stage to a specific position relative to the stage's zero.</Text>
          }}
          controls={t => <Icons.RightNormal
            title="Move to Absolute Position"
            activated={axis.lastInitiator === 'absolute'}
            onClick={t ? () => actions.moveAxis(axis.key, MovementType.Absolute, 'absolute', t.value, t.units) : 'disabled'}
          />}
          onEnterPress={t => actions.moveAxis(axis.key, MovementType.Absolute, 'absolute', t.value, t.units)}
          limits={axis.travelInfo}
          movementType="absolute"
        />
        <PreciseMovementControl
          axis={axis}
          setting="pos"
          title="Relative Movement Control"
          label={{
            label: 'Move by Relative Distance',
            help: <Text>Moves the stage relative to its current location by the amount you enter, either forward or backward.</Text>
          }}
          controls={t => <>
            <Icons.LeftNormal
              title="Move by Relative Distance towards Beginning"
              activated={axis.lastInitiator === 'relative left'}
              onClick={t ? () => actions.moveAxis(axis.key, MovementType.Relative, 'relative left', -t.value, t.units) : 'disabled'}
            />
            <Icons.RightNormal
              title="Move by Relative Distance towards End"
              activated={axis.lastInitiator === 'relative right'}
              onClick={t ? () => actions.moveAxis(axis.key, MovementType.Relative, 'relative right', t.value, t.units) : 'disabled'}
            />
          </>}
          onEnterPress={t => actions.moveAxis(axis.key, MovementType.Relative, 'relative right', t.value, t.units)}
          movementType="relative"
        />
        <PreciseMovementControl
          axis={axis}
          setting="maxspeed"
          title="Move at Velocity Control"
          label={{
            label: 'Move at Velocity',
            help: <>
              <Text>Starts the stage moving at the velocity you enter, in either direction. This does not change the
                device's <Text f={Text.Family.Mono}>maxspeed</Text> setting.</Text><br/><br/>
              <Text>The cycle button will cause the stage to move back and forth at the velocity you enter until stopped.</Text>
            </>
          }}
          controls={t => <>
            <Icons.LeftNormal
              title="Move at Velocity towards Beginning"
              activated={axis.lastInitiator === 'velocity left'}
              onClick={t ? () => actions.moveAxis(axis.key, MovementType.Velocity, 'velocity left', -t.value, t.units) : 'disabled'}
            />
            <Icons.RightNormal
              title="Move at Velocity towards End"
              activated={axis.lastInitiator === 'velocity right'}
              onClick={t ? () => actions.moveAxis(axis.key, MovementType.Velocity, 'velocity right', t.value, t.units) : 'disabled'}
            />
            <Icons.CycleVelocity
              title={cycleTip}
              activated={axis.lastInitiator === 'cycle'}
              disabled={!cyclable}
              onClick={t ? () => actions.moveAxis(axis.key, MovementType.Cycle, 'cycle', t.value, t.units) : 'disabled'}
            />
          </>}
          onEnterPress={t => actions.moveAxis(axis.key, MovementType.Velocity, 'velocity right', t.value, t.units)}
          movementType="velocity"
          defaultValue={axis.travelInfo.maxSpeed}
        />
      </div>}
    </div>
  );
};
