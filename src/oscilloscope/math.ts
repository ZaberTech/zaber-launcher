import _ from 'lodash';

import { MS_PER_SEC } from '../types';

import type { Sample, StorageBuffer } from './types';


function findTimeIndex(data: Sample[], time: number): number {
  const index = _.sortedIndexBy(data, { time, value: 0 }, sample => sample.time);
  if (index > 0) {
    if (index >= data.length) {
      return index - 1;
    }
  }

  return index;
}


function interpolate(s1: Sample, s2: Sample, t: number): number {
  const x = (t - s1.time) / (s2.time - s1.time);
  return (1 - x) * s1.value + x * s2.value;
}


export function derivative(dataSet: StorageBuffer, dependencies: string[], window: number): Sample | null {
  const dep = dataSet[dependencies[0]];
  if (dep.length < 2) {
    return null;
  }

  const x1 = _.last(dep)!;
  let i = findTimeIndex(dep, x1.time - window);
  if (i === dep.length - 1 && i > 0) {
    i--;
  }

  let x0 = dep[i];
  if (x1.time - x0.time < 0.1 * window && i > 0) {
    x0 = dep[i - 1];
  }

  const midpointTime = (x0.time + x1.time) / 2;
  return {
    time: midpointTime,
    value: (x1.value - x0.value) / ((x1.time - x0.time) / MS_PER_SEC),
  };
}


function binaryFunction(dataSet: StorageBuffer, dependencies: string[], op: (a: number, b: number) => number): Sample | null {
  const arg1 = dataSet[dependencies[0]];
  const arg2 = dataSet[dependencies[1]];

  if (arg1.length < 1 || arg2.length < 1) {
    return null;
  }

  // Note the interpolated cases assume even sampling of all channels even
  // when the times are staggered - that is, the timestamp of the latest sample
  // in any channel will never be less than that of the second-latest in any other.
  const x1 = _.last(arg1)!;
  const x2 = _.last(arg2)!;
  if (x2.time > x1.time) {
    if (arg2.length < 2) {
      return null;
    }

    return {
      time: x1.time,
      value: op(x1.value, interpolate(arg2[arg2.length - 2], x2, x1.time)),
    };
  } else if (x2.time < x1.time) {
    if (arg1.length < 2) {
      return null;
    }

    return {
      time: x2.time,
      value: op(interpolate(arg1[arg1.length - 2], x1, x2.time), x2.value),
    };
  }

  return {
    time: x2.time,
    value: op(x1.value, x2.value),
  };
}


export function difference(dataSet: StorageBuffer, dependencies: string[]): Sample | null {
  return binaryFunction(dataSet, dependencies, (a, b) => a - b);
}


export function vectorLength(dataSet: StorageBuffer, dependencies: string[]): Sample | null {
  return binaryFunction(dataSet, dependencies, (a, b) => Math.sqrt(a * a + b * b));
}


export function threePhaseCurrent(dataSet: StorageBuffer, dependencies: string[]): Sample | null {
  return binaryFunction(dataSet, dependencies, (a, b) => Math.max(Math.abs(a), Math.abs(b), Math.abs(a + b)));
}
