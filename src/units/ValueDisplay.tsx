import { Units } from '@zaber/motion';
import React from 'react';
import { match } from 'ts-pattern';

import type { EntityKey } from '../keys';
import { roundToFixed } from '../utils';

import { useUnitInfo } from './hooks';
import { getUnits, MeasurementOK } from './types';
import { convertValueUnits, getUnitLabel } from './utils';

interface Props {
  entityKey: EntityKey;
  setting: string;
  /** The value in native units */
  value: number;
  /** The unit to display this value in */
  unit: Units;
  /** How many digits to display. Defaults to 0 for Native and 2 for everything else. */
  digits?: number;
}

export const ValueDisplay: React.FC<Props> = ({ entityKey, setting, value, unit, digits }) => {
  const unitInfo = useUnitInfo({ setting, entity: entityKey });
  const valueAndUnits = match(unitInfo).returnType<MeasurementOK>()
    .with({ supported: true }, info => ({ value: convertValueUnits(info, value, Units.NATIVE, unit), units: unit }))
    .otherwise(() => ({ value, units: Units.NATIVE }));
  const showNative = valueAndUnits.units === Units.NATIVE;
  const showDigits = digits ?? (showNative ? 0 : 2);
  return (
    <>
      {valueAndUnits.value == null ? 'unknown' : roundToFixed(valueAndUnits.value, showDigits)}
      &nbsp;{getUnitLabel(getUnits(valueAndUnits))}
    </>
  );
};
