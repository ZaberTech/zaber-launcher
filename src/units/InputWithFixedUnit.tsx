import { NumericInput, Text } from '@zaber/react-library';
import classNames from 'classnames';
import React, { forwardRef } from 'react';

import type { InputWithUnitProps } from './InputWithUnits';

export type FixedUnitInputProps = Omit<InputWithUnitProps, 'measure' | 'unitFrom' | 'onChange' | 'valueEditable' | 'unitsEditable'> & {
  value: number | null;
  unit: string;
  onChange?: (update: number | null) => void;
};

/**
 * A component that looks visually like an `InputWithUnits` but showing a fixed defined unit.
 * Use this when the unit is unchangeable and known ahead of time, or something complex and unsupported.
 * */
export const InputWithFixedUnit = forwardRef<HTMLInputElement, FixedUnitInputProps>((props, ref) => {
  const { value, unit, className, onChange, disabled: disabledProp, ...rest } = props;
  const disabled = disabledProp ?? (onChange == null);
  const classes = classNames('input-with-units', className, 'align-right');
  return (
    <div className={classes}>
      <NumericInput
        ref={ref}
        value={value} {...rest}
        disabled={disabled}
        onNumberChange={newValue => onChange && onChange(newValue)}
        inputLineChildren={<>
          <div className="value-unit-separator"/>
          <Text className="fixed-unit">{unit}</Text>
        </>}
      />
    </div>
  );
});

InputWithFixedUnit.displayName = 'InputWithFixedUnit';
