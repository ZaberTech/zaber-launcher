import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Text, Button, Loader } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { NewRouters } from '../connection_manager';

import { selectDiscoveredClients, selectDiscoveryError, selectNothingDiscovered } from './selectors';
import { actions as actionsDefinition } from './actions';

interface Props {
  onRouterAdded: () => void;
}

export const CloudDiscovery: React.FC<Props> = ({ onRouterAdded }) => {
  const actions = useActions(actionsDefinition);
  const error = useSelector(selectDiscoveryError);
  const clients = useSelector(selectDiscoveredClients);
  const nothingDiscovered = useSelector(selectNothingDiscovered);

  useEffect(() => {
    actions.cloudSharingStatus();
    actions.startDiscovery();
    return () => { actions.stopDiscovery() };
  }, []);

  return (<div className="cloud-discovery">
    {!!error && <div>Error discovering: {error}. <Button onClick={actions.startDiscovery}>Try Again</Button></div>}

    <Text className="subheading" t={Text.Type.BodyLg}>
      The following are Zaber Launcher instances that are shared using Zaber Cloud under the same account:
    </Text>

    {!error && <div className="searching">
      <Loader size="small"/>&ensp;<Text t={Text.Type.BodySm}>Searching...</Text>
    </div>}

    <NewRouters routers={clients} onAdded={onRouterAdded}/>

    {nothingDiscovered && <div className="nothing-found">
      <Text t={Text.Type.Instruction}>
        Make sure that the other Zaber Launcher is running and device cloud sharing is enabled.
      </Text>
    </div>}
  </div>);
};
