export type WarningFlags = Record<string, true>;
export interface SupportedPeripheral {
  peripheralId: number;
  name: string;
}

export interface AxisMonitorInfo {
  peripheralIdPending: number;
  warningFlags: WarningFlags;
}
