import React from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import _ from 'lodash';
import { onLocationChanged } from 'connected-react-router';
import { injectable } from 'inversify';

import { waitTick, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../../test';
import { createContainer, destroyContainer } from '../../container';
import {
  mockConnectionError,
  mockConnections,
  mockDataForDeviceWithoutPeripherals,
  mockDataForDeviceWithPeripherals,
  mockLocalConnections,
  mockRouterConnectionError,
  mockSingleDevice,
  mockSingleDeviceWithPeripherals,
} from '../mocks';
import { buildStore } from '../../store/build';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../../message_router';
import { EntityKey, extractRouterKey, makeAxisKey, makeConnectionKey, makeDeviceKey, makeRouterKey } from '../../keys';
import { selectAxes, selectConnections } from '../selectors';
import { connectionStatusActions } from '../../connection_status';
import { RouterType } from '../types';
import { makeCloudUrl } from '../utils';
import { LOCAL_ROUTER_NAME } from '../components/RouterName';
import { mockReloadDevices } from '../../test/mocks/reload_devices';
import { actions } from '../actions';

import { ConnectionsView } from './ConnectionsView';
import { actions as connectionsViewActions } from './actions';

jest.spyOn(connectionStatusActions, 'routedConnectionDisconnect');

const TestConnectionsView = wrapWithNewStore(wrapWithRouter(ConnectionsView));
type Props = React.ComponentProps<typeof TestConnectionsView>;

let wrapper: RenderResult;
const onSelect = jest.fn<void, [string | null]>();

let mockMessageRouterService: MockMessageRoutersService;
@injectable()
class MockMessageRoutersService {
  constructor() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    mockMessageRouterService = this;
  }
  getAsciiConnection = jest.fn().mockRejectedValue(new Error('Cannot connect'));
}

beforeEach(() => {
  onSelect.mockReset();

  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MockMessageRoutersService);
});

afterEach(() => {
  destroyContainer();
  mockMessageRouterService = null!;
});

describe('selectable=[device, axis]', () => {
  beforeEach(() => {
    wrapper = render(<TestConnectionsView
      selectable={['device', 'axis']}
      selected={null}
      onSelect={onSelect}
    />);
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });

  test('renders with mock data - single-device', () => {
    mockSingleDevice(TestConnectionsView.testStore);

    wrapper.getByText(/X-LHM/);
  });

  test('renders with mock data - peripheral', () => {
    mockSingleDeviceWithPeripherals(TestConnectionsView.testStore);

    wrapper.getByText(/X-MCC4/);
    expect(wrapper.queryAllByText(/LHM025A-T3/)).toHaveLength(4);
  });

  test('renders with mock data - router error', () => {
    mockRouterConnectionError(TestConnectionsView.testStore);

    wrapper.getByText(/router is broken/);
  });

  test('renders with mock data - connection error', () => {
    mockConnectionError(TestConnectionsView.testStore);

    wrapper.getByText(/Cannot open serial port/);
  });

  test('does not render router without connections (while there are some other connections)', () => {
    mockConnections(TestConnectionsView.testStore, [{
      router: { type: RouterType.Local, url: LOCAL_ROUTER_URL },
      connectionsData: {},
    }, {
      router: { type: RouterType.Cloud, url: makeCloudUrl('realm', 'id') },
      connectionsData: { COM1: () => [] },
    }]);
    wrapper.getByText(/router_name/);
    expect(wrapper.queryByText(LOCAL_ROUTER_NAME)).toBeNull();
  });

  describe('with peripherals', () => {
    beforeEach(() => {
      mockSingleDeviceWithPeripherals(TestConnectionsView.testStore);
    });

    test('expands and collapses connection', () => {
      wrapper.getByTitle('Collapse Connection');
      fireEvent.click(wrapper.getByText('COM5'));
      wrapper.getByTitle('Expand Connection');
      fireEvent.click(wrapper.getByText('COM5'));
      wrapper.getByTitle('Collapse Connection');
    });

    test('expands and collapses router', () => {
      fireEvent.click(wrapper.getByTitle('Collapse Router'));
      fireEvent.click(wrapper.getByTitle('Expand Router'));
      fireEvent.click(wrapper.getByTitle('Collapse Router'));
    });

    test('invokes onSelect callback when axis is selected', () => {
      fireEvent.click(wrapper.getByText(/Axis 2/));
      expect(onSelect).toHaveBeenCalledWith('@axis|local|COM5|1|2');
    });

    test('invokes onSelect callback when device is selected', () => {
      fireEvent.click(wrapper.getByText(/X-MCC4/));
      expect(onSelect).toHaveBeenCalledWith('@device|local|COM5|1');
    });

    test('click on refresh devices tries to query devices', () => {
      fireEvent.click(wrapper.getByTitle('Open Menu'));
      fireEvent.click(wrapper.getByText('Refresh Devices'));
      expect(mockMessageRouterService.getAsciiConnection).toHaveBeenCalled();
    });

    test('allows to disconnect from connection', () => {
      const store = TestConnectionsView.testStore;

      const connection = _.sample(selectConnections(store.getState()))!;
      store.dispatch(connectionStatusActions.routedConnectionConnected(connection.key));

      fireEvent.click(wrapper.getByTitle('Open Menu'));
      fireEvent.click(wrapper.getByText('Close Serial Port'));

      expect(connectionStatusActions.routedConnectionDisconnect).toHaveBeenCalled();
    });
  });
});

describe('Attention Requests', () => {
  const serialPort = 'COM40';
  const D1 = 1;
  const D2 = 2;
  const D3 = 3;

  const routerKey = makeRouterKey(LOCAL_ROUTER_URL);
  const connectionKey = makeConnectionKey(routerKey, serialPort);
  const deviceKey1 = makeDeviceKey(connectionKey, D1);
  const deviceKey2 = makeDeviceKey(connectionKey, D2);
  const deviceKey3 = makeDeviceKey(connectionKey, D3);

  const renderTestConnectionsView = (devicesRequestingAttention: EntityKey[]) => {
    wrapper = render(<TestConnectionsView
      selectable={['connection']}
      selected={null}
      onSelect={onSelect}
      attentionRequests={{ requestAttention: devicesRequestingAttention, showIcon: <span>Attention Icon</span> }}
    />);

    const store = TestConnectionsView.testStore;
    mockLocalConnections(store, {
      [serialPort]: connectionKey => [
        mockDataForDeviceWithoutPeripherals(connectionKey, D1),
        mockDataForDeviceWithoutPeripherals(connectionKey, D2),
        mockDataForDeviceWithPeripherals(connectionKey, D3, 4, 'smart'),
      ],
    });
  };

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });

  test('Attention request appears when collapsed', async () => {
    renderTestConnectionsView([deviceKey1]);

    await waitUntilPass(() => wrapper.getByText('Attention Icon'));
    fireEvent.click(wrapper.getByTitle('Collapse Router'));
    fireEvent.click(wrapper.getByTitle('Collapse Connection'));

    expect(wrapper.queryAllByTitle('1 sub-entity is actionable')).toHaveLength(2);
    fireEvent.click(wrapper.getByTitle('Expand Router'));
    expect(wrapper.queryAllByTitle('1 sub-entity is actionable')).toHaveLength(1);
    fireEvent.click(wrapper.getByTitle('Expand Connection'));
    expect(wrapper.queryAllByTitle('1 sub-entity is actionable')).toHaveLength(0);
  });

  test('Multiple attention requests appears', async () => {
    renderTestConnectionsView([deviceKey1, deviceKey2]);

    fireEvent.click(wrapper.getByTitle('Collapse Connection'));

    await waitUntilPass(() => wrapper.getByTitle('2 sub-entities are actionable'));
    expect(wrapper.getAllByText('Attention Icon')).toHaveLength(2);
  });

  test('Attention requests appears for axes', async () => {
    renderTestConnectionsView([makeAxisKey(deviceKey3, 2), makeAxisKey(deviceKey3, 3)]);

    fireEvent.click(wrapper.getByTitle('Collapse Connection'));

    await waitUntilPass(() => wrapper.getByTitle('2 sub-entities are actionable'));
    expect(wrapper.getAllByText('Attention Icon')).toHaveLength(2);
  });
});

describe('selection mode Connection', () => {
  beforeEach(() => {
    wrapper = render(<TestConnectionsView
      selectable={['connection']}
      selected={null}
      onSelect={onSelect}

    />);
    mockSingleDevice(TestConnectionsView.testStore);
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });

  test('invokes onSelect callback when device is selected and expands connection', () => {
    fireEvent.click(wrapper.getByText(/X-LHM/));
    expect(onSelect).toHaveBeenCalledWith('@connection|local|COM5');
    wrapper.getByTitle('Collapse Connection');
  });

  test('expands and collapses connection', () => {
    fireEvent.click(wrapper.getByTitle('Collapse Connection'));
    fireEvent.click(wrapper.getByTitle('Expand Connection'));
    fireEvent.click(wrapper.getByTitle('Collapse Connection'));
    fireEvent.click(wrapper.getByTitle('Expand Connection'));
  });
});

describe('selectable=[connection] (populated store)', () => {
  beforeEach(() => {
    const store = buildStore();
    mockSingleDevice(store);

    wrapper = render(<TestConnectionsView
      selectable={['connection']}
      selected={null}
      onSelect={onSelect}
      store={store}
    />);
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });

  test('invokes onSelect callback when there is only one connection', async () => {
    await waitUntilPass(() => {
      expect(onSelect).toHaveBeenCalledWith('@connection|local|COM5');
    });
  });
});

describe('selectable=[device] (populated store)', () => {
  beforeEach(() => {
    const store = buildStore();
    mockSingleDevice(store);

    wrapper = render(<TestConnectionsView
      selectable={['device']}
      selected={null}
      onSelect={onSelect}
      store={store}
    />);
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });

  test('invokes onSelect callback when there is only one device to select', async () => {
    await waitUntilPass(() => {
      expect(onSelect).toHaveBeenCalledWith('@device|local|COM5|1');
    });
  });
});


describe('selectable=[axis, integrated]', () => {
  const serialPort = 'COM40';

  beforeEach(() => {
    wrapper = render(<TestConnectionsView
      selectable={['axis', 'integrated']}
      selected={null}
      onSelect={onSelect}
    />);

    const store = TestConnectionsView.testStore;
    mockLocalConnections(store, {
      [serialPort]: connectionKey => [
        mockDataForDeviceWithoutPeripherals(connectionKey, 1),
        mockDataForDeviceWithPeripherals(connectionKey, 2, 1, 'smart'),
      ],
    });
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });


  it('allows selection of peripheral', async () => {
    fireEvent.click(wrapper.getByText(/LRM150A/));
    await waitUntilPass(() => {
      expect(onSelect).toHaveBeenCalledWith('@axis|local|COM40|2|1');
    });
  });


  it('does not allow selection of controller', async () => {
    fireEvent.click(wrapper.getByText(/X-MCC/));
    await waitTick(10);
    expect(onSelect).not.toHaveBeenCalled();
  });
});


describe('selected passed in URL', () => {
  const expectedKey = '@axis|local|COM5|1|3';

  beforeEach(() => {
    const store = buildStore();
    mockSingleDeviceWithPeripherals(store);
    const passedAxis = _.find(store.getState().connectionManager.axes, axis => axis.axisNumber === 3)!;

    store.dispatch(onLocationChanged({
      pathname: '/device-settings',
      hash: '',
      search: `?${ConnectionsView.SELECT_PARAM}=${passedAxis.key}`,
      state: null
    }, 'PUSH'));

    store.dispatch(connectionsViewActions.setExpanded(extractRouterKey(passedAxis.key), false));

    wrapper = render(<TestConnectionsView
      store={store}
      selectable={['axis']}
      selected={null}
      onSelect={onSelect}
    />);
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });

  test('invokes onSelected if param is present', () => {
    expect(onSelect).toHaveBeenCalledWith(expectedKey);
  });

  test('expands router if param is present', () => {
    expect(TestConnectionsView.testStore.getState()
      .connectionView.expanded[extractRouterKey(expectedKey)]).toBe(true);
    expect(wrapper.getAllByTitle('Collapse Router')).toHaveLength(1);
  });
});

describe('Manage device pinning', () => {
  let store: ReturnType<typeof buildStore>;
  const onPinClick = jest.fn<void, [EntityKey]>();

  const renderView = (pinnedAxesKeys: EntityKey[]) => (<TestConnectionsView
    store={store}
    selectable={['device', 'axis']}
    selected={null}
    onSelect={onSelect}
    pinnedAxesKeys={pinnedAxesKeys}
    onPinClick={onPinClick}
  />);

  beforeEach(() => {
    store = buildStore();
    wrapper = render(renderView([]));
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    onPinClick.mockClear();
  });

  test('A pinned axis can be unpinned from the connection view panel', async () => {
    mockSingleDeviceWithPeripherals(store);
    const axisKey = _.sample(selectAxes(store.getState()))!.key;

    wrapper.rerender(renderView([axisKey]));
    fireEvent.click(wrapper.getByTitle('Pin Axis'));
    expect(onPinClick).toHaveBeenCalledTimes(1);
  });

  test('A pinned device with 1 axis can be unpinned from the connection view panel', async () => {
    mockSingleDevice(store);
    const axisKey = _.sample(selectAxes(store.getState()))!.key;

    wrapper.rerender(renderView([axisKey]));
    fireEvent.click(wrapper.getByTitle('Pin Axis'));
    expect(onPinClick).toHaveBeenCalledTimes(1);
  });
});

describe('Connection view with no connections', () => {
  let store: ReturnType<typeof buildStore>;

  beforeEach(() => {
    store = buildStore();
    wrapper = render(<TestConnectionsView
      store={store}
      selectable={['axis']}
      selected={null}
      onSelect={onSelect}
    />);
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });

  test('Show a message when there are no connections', () => {
    wrapper.getByText('No devices connected.');
    wrapper.getByText('Please setup your connections first through My Connections and come back here.');
  });
});

describe('switching between views in different apps', () => {
  let store: ReturnType<typeof buildStore>;

  const routerKey = makeRouterKey(LOCAL_ROUTER_URL);
  const connection1Key = makeConnectionKey(routerKey, 'COM1');
  const device1Key = makeDeviceKey(connection1Key, 1);
  const axisKey = makeAxisKey(device1Key, 1);
  const connection2Key = makeConnectionKey(routerKey, 'COM2');
  const device2Key = makeDeviceKey(connection2Key, 1);

  beforeEach(() => {
    store = buildStore();

    mockLocalConnections(store, {
      COM1: connectionKey => [
        mockDataForDeviceWithPeripherals(connectionKey, 1, 4, 'smart'),
      ],
      COM2: connectionKey => [
        mockDataForDeviceWithPeripherals(connectionKey, 1, 4, 'smart'),
      ],
    });
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });

  test('selection gets carried over if views have same selectable', () => {
    const props: Props = {
      store,
      selectable: ['axis'],
      onSelect,
      selected: null,
    };
    wrapper = render(<TestConnectionsView
      {...props}
    />);
    wrapper.rerender(<TestConnectionsView
      {...props}
      selected={axisKey}
    />);
    wrapper.unmount();

    wrapper = render(<TestConnectionsView
      {...props}
    />);
    expect(onSelect).toHaveBeenCalledTimes(1);
    expect(onSelect).toHaveBeenLastCalledWith(axisKey);
  });

  test('selection gets carried over when views have compatible selectable', () => {
    const props: Props = {
      store,
      selectable: ['axis'],
      onSelect,
      selected: null,
    };
    wrapper = render(<TestConnectionsView
      {...props}
    />);
    wrapper.rerender(<TestConnectionsView
      {...props}
      selected={axisKey}
    />);
    wrapper.unmount();

    wrapper = render(<TestConnectionsView
      {...props}
      selectable={['connection']}
    />);
    expect(onSelect).toHaveBeenCalledTimes(1);
    expect(onSelect).toHaveBeenLastCalledWith(connection1Key);
    wrapper.rerender(<TestConnectionsView
      {...props}
      selectable={['connection']}
      selected={connection1Key}
    />);
    wrapper.unmount();

    wrapper = render(<TestConnectionsView
      {...props}
      selectable={['device']}
    />);
    expect(onSelect).toHaveBeenCalledTimes(2);
    expect(onSelect).toHaveBeenLastCalledWith(device1Key);
    wrapper.rerender(<TestConnectionsView
      {...props}
      selectable={['device']}
      selected={device1Key}
    />);
    wrapper.unmount();

    wrapper = render(<TestConnectionsView
      {...props}
    />);
    expect(onSelect).toHaveBeenCalledTimes(3);
    expect(onSelect).toHaveBeenLastCalledWith(axisKey);
  });

  test('selection stays the same when selectable is not compatible but selected key is derived', () => {
    const props: Props = {
      store,
      selectable: ['connection'],
      onSelect,
      selected: null,
    };
    wrapper = render(<TestConnectionsView
      {...props}
    />);
    wrapper.rerender(<TestConnectionsView
      {...props}
      selected={connection1Key}
    />);
    wrapper.unmount();

    wrapper = render(<TestConnectionsView
      {...props}
      selectable={['device']}
      selected={device1Key}
    />);
    expect(onSelect).toHaveBeenCalledTimes(0);
    wrapper.unmount();

    wrapper = render(<TestConnectionsView
      {...props}
      selectable={['axis']}
      selected={axisKey}
    />);
    expect(onSelect).toHaveBeenCalledTimes(0);
  });

  test('selection gets reset when selectable is not compatible and selected key is not derived', () => {
    const props: Props = {
      store,
      selectable: ['connection'],
      onSelect,
      selected: null,
    };
    wrapper = render(<TestConnectionsView
      {...props}
    />);
    wrapper.rerender(<TestConnectionsView
      {...props}
      selected={connection1Key}
    />);
    wrapper.unmount();

    wrapper = render(<TestConnectionsView
      {...props}
      selectable={['device']}
      selected={device2Key}
    />);
    expect(onSelect).toHaveBeenCalledTimes(1);
    expect(onSelect).toHaveBeenLastCalledWith(null);
    wrapper.unmount();

    wrapper = render(<TestConnectionsView
      {...props}
      selectable={['axis']}
      selected={makeAxisKey(device2Key, 1)}
    />);
    expect(onSelect).toHaveBeenCalledTimes(2);
    expect(onSelect).toHaveBeenLastCalledWith(null);
  });

  test('onSelect does not fire when the selection is the same', () => {
    const props: Props = {
      store,
      selectable: ['device'],
      onSelect,
      selected: null,
    };
    wrapper = render(<TestConnectionsView
      {...props}
    />);
    wrapper.rerender(<TestConnectionsView
      {...props}
      selected={device1Key}
    />);
    wrapper.unmount();

    wrapper = render(<TestConnectionsView
      {...props}
      selected={device1Key}
    />);
    expect(onSelect).toHaveBeenCalledTimes(0);
  });

  test('selection does not get renew if the key is no longer valid', async () => {
    mockReloadDevices(() => store, devices => {
      devices.pop();
    });

    const props: Props = {
      store,
      selectable: ['device'],
      onSelect,
      selected: null,
    };
    wrapper = render(<TestConnectionsView
      {...props}
    />);
    wrapper.rerender(<TestConnectionsView
      {...props}
      selected={device1Key}
    />);
    wrapper.unmount();

    store.dispatch(actions.loadDevices(connection1Key, false));
    await waitTick(2);

    wrapper = render(<TestConnectionsView
      {...props}
    />);
    expect(onSelect).toHaveBeenCalledTimes(0);
  });
});
