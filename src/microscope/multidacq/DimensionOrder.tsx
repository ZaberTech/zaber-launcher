import React from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, Icons, Text } from '@zaber/react-library';

import { useActions } from '../../utils';

import { actions as actionsDefinition } from './actions';
import { selectConfig } from './selectors';
import { DIMENSION_LABELS } from './types';

export const DimensionOrder: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const config = useSelector(selectConfig);

  const moveDimension = (i: number, offset: number) => {
    const newIndex = i + offset;
    if (newIndex < 0 || newIndex >= config.order.length) {
      return;
    }

    const newOrder = [...config.order];
    newOrder[i] = config.order[newIndex];
    newOrder[newIndex] = config.order[i];

    actions.updateConfig({ order: newOrder });
  };

  return <HeaderCard className="dimension-order"
    header={<Text t={Text.Type.H4}>Dimension Order</Text>}>
    <div className="order">
      {config.order.map((dimension, i) =>
        <React.Fragment key={dimension}>
          <Text>{DIMENSION_LABELS[dimension]}</Text>
          <Icons.ArrowLeft
            title={`Move ${DIMENSION_LABELS[dimension]} Up`}
            disabled={i === 0}
            onClick={() => moveDimension(i, -1)}/>
          <Icons.ArrowRight
            title={`Move ${DIMENSION_LABELS[dimension]} Down`}
            disabled={i === config.order.length - 1}
            onClick={() => moveDimension(i, 1)}/>
        </React.Fragment>)}
    </div>
  </HeaderCard>;
};
