import React from 'react';

import type { Path } from '../types';
import { getControlPointPosition } from '../utils';

import { XAXIS, YAXIS } from './constants';

const HANDLE_SIZE = 3;

export interface Props {
  userPath: Path;
  selectedPointIndex: number;
  drawingAxes: number[];
  scale: number;
  onSelected: (i: number) => void;
}


// Velocity editing handle attached to the selected control point in the curve view.
export const VelocityControl: React.FC<Props> = ({ userPath, selectedPointIndex, drawingAxes, scale, onSelected }) => {
  const pointPos = getControlPointPosition(userPath, selectedPointIndex);
  if (pointPos == null) {
    return null;
  }

  const x1 = pointPos[drawingAxes[XAXIS]];
  const y1 = pointPos[drawingAxes[YAXIS]];
  const x2 = x1 + userPath.series[drawingAxes[XAXIS]].points[selectedPointIndex].vel;
  const y2 = y1 + userPath.series[drawingAxes[YAXIS]].points[selectedPointIndex].vel;

  return <>
    <line x1={x1} y1={y1} x2={x2} y2={y2}
      className="velocity-vector"
      role="velocity-control"
      vectorEffect="non-scaling-stroke"/>
    <circle cx={x2} cy={y2} r={HANDLE_SIZE / scale}
      className="control-point velocity"
      role="velocity-control"
      vectorEffect="non-scaling-stroke"
      onMouseDown={() => onSelected(selectedPointIndex)}/>
  </>;
};
