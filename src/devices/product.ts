import type { FirmwareVersion } from '@zaber/motion';

import type { ProductInfo } from '../connection_manager';

const INVALID_SERIAL_NUMBERS = [0, Math.pow(2, 32) - 1];

export enum Capabilities {
  Motion = 'motion',
  DeviceCanAutodetect = 'auto-detector',
  PeripheralIsAutodetect = 'auto-detect',
  Controller = 'controller',
  Multiaxis = 'multiaxis',
  Lamp = 'lamp',
  MotorStepper = 'motor-stepper',
}

const PARAM_CHAR = '?';
/**
 * Determines if the command exists by traversing the command tree.
 * @param command Command (e.g. 'move abs ?')
 * @param partial If true matches part of the command tree (e.g. 'move')
 * @returns true if the command exists on the product
 */
export function commandExists(command: string, deviceOrAxis: { product: ProductInfo | null }, partial?: boolean): boolean {
  if (!deviceOrAxis.product) {
    return false;
  }
  const parts = command.split(' ');
  let currentNode = deviceOrAxis.product.commandTree;
  while (parts.length > 0) {
    const part = parts.shift();
    const nextNode = currentNode.nodes?.find(node => part === PARAM_CHAR ? node.is_param : node.command === part);
    if (!nextNode) {
      return false;
    }
    currentNode = nextNode;
  }

  return !!partial || !!currentNode.is_command;
}

export function productHasSetting(setting: string, productSettingsTable: ProductInfo['settings']): boolean {
  return productSettingsTable.rows.some(row => row.name === setting) ?? false;
}

export function hasSetting(setting: string, deviceOrAxis: { product: ProductInfo | null }): boolean {
  return deviceOrAxis.product != null ? productHasSetting(setting, deviceOrAxis.product.settings) : false;
}

export function hasCapability(deviceOrAxis: { product: ProductInfo | null }, capability: Capabilities): boolean {
  return deviceOrAxis.product?.capabilities.includes(capability) ?? false;
}

export function isMobile(axis: { product: ProductInfo | null }): boolean {
  return commandExists('move abs ?', axis);
}

export const REMOTE_MODE_SETTING = 'comm.ethercat.remote';

export function hasRemoteMode(device?: { product: ProductInfo | null }): boolean {
  if (!device?.product) {
    return false;
  }

  return hasSetting(REMOTE_MODE_SETTING, device);
}

export const isSerialNumberValid = (serialNumber: number) => !INVALID_SERIAL_NUMBERS.includes(serialNumber);

export function fwToString(firmware: FirmwareVersion, includeBuild = true) {
  let str = `${firmware.major}.${firmware.minor.toString().padStart(2, '0')}`;
  if (includeBuild) {
    str += `.${firmware.build}`;
  }
  return str;
}
