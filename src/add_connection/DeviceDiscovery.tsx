import React, { useEffect } from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';
import { Button, ButtonPrevious, ButtonRow, Icons, Loader, Text } from '@zaber/react-library';

import { DEVICE_TABLE_CLASS } from '../connection_manager/device_table';
import { Thumbnail } from '../components/Thumbnail';

import { actions as actionsDefinition } from './actions';
import { selectTcpConfig } from './selectors';
import { Steps } from './types';

export const DeviceDiscovery = () => {
  const actions = useActions(actionsDefinition);
  const { discovering, error, mdnsCandidates } = useSelector(selectTcpConfig);
  useEffect(() => {
    actions.mdnsDiscover();
    return () => { actions.mdnsDiscoverCancel() };
  }, []);

  return (
    <div className="discover-mdns">
      <div className="header-label">
        <h3>Found Devices</h3>

        <div className="search">
          {discovering && <><Loader/>&ensp;Discovering...</>}
          {!discovering && <Button color="grey" onClick={actions.mdnsDiscover}>Discover Again</Button>}
          {error && <>Error discovering devices: {error}</>}
        </div>
      </div>
      <div className="candidates">{
        mdnsCandidates.map(candidate =>
          <div key={candidate.hostname} className={DEVICE_TABLE_CLASS}>
            <div className="connection">
              <Icons.EthernetSocket className="connection-type-icon"/>&nbsp;<Text t={Text.Type.H4}>{candidate.hostname}</Text>
            </div>
            <div className="controller">
              <div className="controller-header">
                <div className="device-picture">
                  <Thumbnail deviceOrPeripheralId={candidate.deviceId} title={candidate.name ?? 'Unknown Device'} altExt="device"/>
                </div>
                <div className="device-name">
                  <Text t={Text.Type.H5}>
                    {candidate.name ?? `Device ${candidate.deviceId}`}
                  </Text>
                  <span>
                    SN: {candidate.serialNumber}&ensp;
                  </span>
                </div>
              </div>
              <Button className="button-select" onClick={() => {
                actions.changeTcpConnectionType('chain');
                actions.changeTcp({ hostname: candidate.hostname, port: candidate.chainPort });
                actions.changeStep(Steps.CONFIGURE_TCP);
              }}>Select</Button>
            </div>
          </div>)
      }</div>

      <ButtonRow justify="spread">
        <ButtonPrevious onClick={() => actions.changeStep(Steps.CHOOSE_CONNECTION_TYPE)}>Back</ButtonPrevious>
        <Button color="grey" onClick={() => actions.changeStep(Steps.CONFIGURE_TCP)}>Connect Manually</Button>
      </ButtonRow>
    </div>
  );
};
