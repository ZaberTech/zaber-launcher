import _ from 'lodash';
import { createSelector } from 'reselect';
import { tryAccess } from '@zaber/toolbox';
import { Units } from '@zaber/motion';

import { selectJoystick as selectState } from '../store';
import {
  selectIdentifiedDevices,
  selectIdentifiedAxes,
  IdentifiedAxisState,
  IdentifiedDeviceState,
  LockstepGroup,
} from '../connection_manager';
import { extractConnectionKey, extractDeviceKey, isSubKeyOf, makeAxisKey, makeDeviceKey, tryExtractConnectionKey } from '../keys';
import { isMobile } from '../devices';
import { getDefaultDimensionUnits } from '../units';

import { AxisChainInfo, JOYSTICK_DEVICE_IDS, AxisConfig } from './types';

export const selectJoystick = createSelector(selectState, selectIdentifiedDevices,
  (state, managerDevices) => {
    const device = tryAccess(managerDevices, state.selectedKey);
    if (device == null) {
      return null;
    }
    const { deviceId } = device.identity;
    if (!(deviceId in JOYSTICK_DEVICE_IDS)) {
      return null;
    }
    return {
      ...device,
      ...JOYSTICK_DEVICE_IDS[deviceId],
    };
  });

export const selectSelectedKey = createSelector(selectState, state => state.selectedKey);
export const selectManualMode = createSelector(selectState, state => state.manualMode);
export const selectProductionMode = createSelector(selectState, state => state.productionMode);
export const selectConfigReading = createSelector(selectState, state => ({
  reading: state.readingConfig,
  error: state.configReadError,
  config: state.config,
}));
export const selectConfigWriting = createSelector(selectState, state => state.writingConfig);
export const selectConfigRestoring = createSelector(selectState, state => state.restoringConfig);
export const selectConfigWriteOrRestoreError = createSelector(selectState, state => state.configWriteOrRestoreError);
export const selectConfigNeedsApply = createSelector(selectState, state => !_.isEqual(state.config, state.deviceConfig));

export const selectDevices = createSelector(selectState, selectIdentifiedDevices,
  (state, devices) => _.chain(devices)
    .pickBy(device => isSubKeyOf(device.key, tryExtractConnectionKey(state.selectedKey))
      && !(device.identity.deviceId in JOYSTICK_DEVICE_IDS))
    .value());


export const selectChainInfo = createSelector(selectState, state => {
  if (state.chainInfo == null) { throw new Error('Chain info missing') }
  return state.chainInfo;
});

export interface PlainAxis extends IdentifiedAxisState {
  velocityUnits: Units;
  device: IdentifiedDeviceState;
  lockstep: LockstepGroup | null;
  isPrimary: boolean;
}

export const selectPlainAxes = createSelector(selectState, selectIdentifiedAxes, selectDevices,
  (state, axes, devices) => _.chain(axes)
    .pickBy(axis => isSubKeyOf(axis.key, tryExtractConnectionKey(state.selectedKey)) && isMobile(axis))
    .mapValues((axis): PlainAxis => {
      const device = devices[extractDeviceKey(axis.key)];
      const lockstep = device.locksteps.find(lockstep => lockstep.axisNumbers.includes(axis.axisNumber)) ?? null;
      return ({
        ...axis,
        velocityUnits: axis.movementDimensions ? getDefaultDimensionUnits(axis.movementDimensions.velocity) : Units.NATIVE,
        device,
        lockstep,
        isPrimary: lockstep == null || lockstep.axisNumbers[0] === axis.axisNumber,
      });
    })
    .value());

export const selectPrimaryAxes = createSelector(selectPlainAxes, axes =>
  _.pickBy(axes, axis => axis.isPrimary));

interface CompleteAxis extends PlainAxis, AxisChainInfo {
}

export const selectAxes = createSelector(selectPrimaryAxes, selectChainInfo,
  (axes, chainInfo) => _.chain(axes)
    .mapValues((axis): CompleteAxis => ({
      ...axis,
      ...chainInfo.axes[axis.key],
    }))
    .value());

export interface EnhancedAxisConfig extends AxisConfig {
  axis: CompleteAxis | null;
  device: IdentifiedDeviceState | null;
}

export const selectConfig = createSelector(selectState, selectAxes, selectDevices, ({ config, selectedKey }, axes, devices) => {
  if (config == null) { throw new Error('Config not read') }
  if (selectedKey == null) { throw new Error('Key not selected') }
  const connectionKey = extractConnectionKey(selectedKey);
  return {
    ...config,
    axes: config.axes.map((axisConfig): EnhancedAxisConfig => {
      const deviceKey = makeDeviceKey(connectionKey, axisConfig.targetDevice ?? 0);

      const device = tryAccess(devices, deviceKey) ?? null;
      let axis: CompleteAxis | null = null;

      switch (axisConfig.axisType) {
        case 'axis': {
          const axisKey = makeAxisKey(deviceKey, axisConfig.targetAxis ?? 0);
          axis = tryAccess(axes, axisKey) ?? null;
          if (axis?.lockstep != null) {
            axis = null; // don't accept lockstep axis as regular one
          }
        } break;
        case 'lockstep': {
          const lockstep = device?.locksteps.find(lockstep => lockstep.groupNumber === axisConfig.targetAxis);
          if (lockstep) {
            axis = tryAccess(axes, makeAxisKey(deviceKey, lockstep.axisNumbers[0])) ?? null;
          }
        } break;
      }
      return ({
        ...axisConfig,
        axis,
        device,
      });
    })
  };
});

export const selectSaveLoadConfigError = createSelector(selectState, state => state.saveLoadError);
export const selectCalibration = createSelector(selectState, state => _.pick(state, 'calibrating', 'calibrationError'));
