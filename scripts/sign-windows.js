const { isCI } = require('./common');

const { signFile } = require('@zaber/tools');

exports.default = async function(configuration) {
  if (!isCI) {
    return;
  }

  if (configuration.isNest) { return; }
  console.log(`Signing ${configuration.path}`);

  await signFile(configuration.path);
};
