import React from 'react';
import { Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { actions as actionsDefinition } from './actions';
import type { AxisStateComplete } from './selectors';
import { MovementType } from './types';


interface Props {
  axis: AxisStateComplete;
  which: 'min' | 'max';
}

export const JogLeftRight: React.FC<Props> = ({ axis, which }) => {
  const actions = useActions(actionsDefinition);
  const maxSpeed = axis.travelInfo?.maxSpeed;
  if (maxSpeed == null) {
    return null;
  }

  const initiator = which === 'min' ? 'jog left' : 'jog right';
  const move = () => {
    const velocity = which === 'min' ? -maxSpeed : maxSpeed;
    actions.moveAxis(axis.key, MovementType.Velocity, initiator, velocity);
  };
  const jogDone = () => {
    if (axis.lastInitiator === initiator) {
      actions.moveAxis(axis.key, MovementType.Stop, 'jog done');
    }
  };
  if (which === 'min') {
    return <Icons.LeftNormal
      title="Move towards Beginning"
      appearsClickable
      onMouseDown={move}
      onTouchStart={move}
      onTouchEnd={jogDone}
      onMouseLeave={jogDone}
      onDragLeave={jogDone}
      onMouseUp={jogDone}/>;
  } else {
    return <Icons.RightNormal
      title="Move towards End"
      appearsClickable
      onMouseDown={move}
      onTouchStart={move}
      onTouchEnd={jogDone}
      onMouseLeave={jogDone}
      onDragLeave={jogDone}
      onMouseUp={jogDone}/>;
  }
};
