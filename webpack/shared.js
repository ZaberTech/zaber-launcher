const webpack = require('webpack');
const dotenv = require('dotenv');
const path = require('path');
const _ = require('lodash');
const nodeExternals = require('webpack-node-externals');

const { editions, currentEdition, currentFlavor, isOffline } = require('../scripts/common');

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'development';
}
const isDevelopment = process.env.NODE_ENV === 'development';
let releaseName = process.env.CI_COMMIT_REF_NAME || 'dev';
if (process.env.CI_PIPELINE_IID) {
  releaseName += `-${process.env.CI_PIPELINE_IID}`;
}

const envFiles = [];
if ([editions.PUBLIC, editions.INTERNAL].includes(currentEdition)) {
  envFiles.push('.env.production');
} else {
  envFiles.push('.env.staging');
  envFiles.push('.env.local');
}

const loadedEnv = {};
for (const file of envFiles) {
  const env = dotenv.config({
    path: path.join(__dirname, '..', file)
  });

  if (!env.error) {
    Object.assign(loadedEnv, env.parsed);
  }
}

// the following libraries are bundled because we require processing of assets
const BUNDLED_LIBRARIES = [
  '@zaber/react-library',
  'monaco-editor',
];

const shared = {
  mode: process.env.NODE_ENV,
  devtool: 'inline-source-map',
  externals: [nodeExternals({ allowlist: BUNDLED_LIBRARIES })],
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'process.env.RELEASE_NAME': JSON.stringify(releaseName),
      'process.env.ZL_EDITION': JSON.stringify(currentEdition),
      'process.env.ZL_FLAVOR': JSON.stringify(currentFlavor),
      'process.env.ZL_OFFLINE': JSON.stringify(isOffline.toString()),
      'process.env.JEST_WORKER_ID': JSON.stringify(''),
      ..._.transform(loadedEnv, (result, value, key) => result[`process.env.${key}`] = JSON.stringify(value)),
    }),
  ],
};


module.exports = {
  shared,
  isDevelopment,
};
