/**
 * 'fixed': menu is pinned (and is at extended width).
 *
 * 'collapsed': menu width is collapsed width.
 *
 * 'extended': menu width is extended width.
 *
 * Note that MenuState is 'extended' only when it is moused over and unpinned.
 */
export type MenuState = 'fixed' | 'collapsed' | 'extended';
