import React  from 'react';
import { useSelector } from 'react-redux';

import { NoContentMessage, Title } from '../components';
import { ConnectionsView } from '../connection_manager';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { DeviceIo } from './DeviceIo';
import { selectSelectedDevicesWithIo, selectSelectedKey } from './selectors';

const TITLE = 'Welcome to Device IO!';
const noSelection = <NoContentMessage title={TITLE} message="Select a connection or device from the menu on the left."/>;
const noIo = <NoContentMessage title={TITLE} type="info" message="No device in your selection has inputs or outputs."/>;

export const DeviceIoApp: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const devices = useSelector(selectSelectedDevicesWithIo);
  const selectedKey = useSelector(selectSelectedKey);

  return (<div className="connection-view-and-app">
    <Title>Device I/O</Title>

    <ConnectionsView
      selectable={['connection', 'device']}
      selected={selectedKey}
      onSelect={actions.setSelectedKey}
      showAxes={false}/>
    <div className="app-ui device-io-app">
      {devices.map(device => <DeviceIo key={device.key} deviceKey={device.key}/>)}
      {devices.length === 0 && (selectedKey ? noIo : noSelection)}
    </div>
  </div>);
};
