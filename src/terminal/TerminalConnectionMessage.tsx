import { Button } from '@zaber/react-library';
import React from 'react';

import { ConnectionStates } from './types';

interface Props {
  connectionState: ConnectionStates;
  reconnect: () => void;
}

export const TerminalConnectionMessage: React.FC<Props> = ({ connectionState, reconnect }) => {
  switch (connectionState) {
    case ConnectionStates.Connected: return null;
    case ConnectionStates.NotConnected: return <div className="connection-state">Not Connected</div>;
    case ConnectionStates.Connecting: return <div className="connection-state">Connecting</div>;
    case ConnectionStates.Ended: return <div className="connection-state">
      Disconnected. <Button onClick={reconnect} color="grey" size="small">Reconnect</Button>
    </div>;
  }
};
