/* eslint-disable import-x/namespace */
import _ from 'lodash';
import * as math from 'mathjs';

import { getAccelerationScale, getVelocityScale } from './pvtSeriesHelper';
import { PvtSegment } from './pvtSegment';
import type { Point, Path } from './types';


export class PathDiscretizer {
  private timeStep = 0.1;
  private fixedTimeStep = true;
  private curvatureSizeScale = 1.0;
  private prevPoints: Point[] = [];
  private nextPoints: Point[] = [];
  private absPositions: number[] = [];
  private time = 0;
  private pointIndex = 0;
  public velocityScale = 1.0;
  public accelerationScale = 1.0;

  constructor(private input: Path) {
    if (input.series.length < 1 || input.series[0].points.length < 1) {
      throw new Error('Path has no data.');
    }

    this.velocityScale = getVelocityScale(input.positionUnit, input.velocityUnit);
    this.accelerationScale = getAccelerationScale(input.positionUnit, input.velocityUnit);
  }


  public discretizeToPath(timeStep?: number): Path {
    const output = this.newOutput();
    for (const points of this.discretizeToStream(timeStep)) {
      points.forEach((p, i) => {
        output.series[i].points.push(p);
      });
    }
    return output;
  }

  public* discretizeToStream(timeStep?: number) {
    this.fixedTimeStep = false;
    if (timeStep !== undefined) {
      if (timeStep < 0.000001) {
        throw new Error('Specified time step is too small.');
      }

      this.timeStep = timeStep;
      this.fixedTimeStep = true;
    } else {
      // Try to limit output to approximately 500 points at most, knowing the
      // adaptive time step may overshoot this.
      this.fixedTimeStep = false;
      this.timeStep = 0.1;
      const lastPoint = _.last(this.input.series[0].points);
      if (lastPoint) {
        if (lastPoint.time / this.timeStep > 500) {
          this.timeStep = lastPoint.time / 500;
        }
      }

      // Use the order of magnitude of the path's maximum dimension to determine what size of
      // curvature in path units should be considered significant.
      const minPos = this.input.series.map(s => _.min(s.points.map(p => p.pos)));
      const maxPos = this.input.series.map(s => _.max(s.points.map(p => p.pos)));
      const maxRange = _.max(maxPos.map((p, i) => p! - minPos[i]!));
      this.curvatureSizeScale = Math.pow(10, Math.max(1, Math.ceil(Math.log10(maxRange!))));
    }

    this.prevPoints = this.input.series.map(s => s.points[0]);
    this.absPositions = this.prevPoints.map(p => p.pos);
    if (_.some(this.prevPoints, p => p.relative)) {
      throw new Error('Start points must have absolute coordinates.');
    }

    // Program must guarantee all peer control points with the same index have the same times (not checked).
    this.time = this.fixedTimeStep ? 0 : this.prevPoints[0].time;
    this.pointIndex = 1;
    while (this.pointIndex < this.input.series[0].points.length) {
      this.nextPoints = this.input.series.map(s => s.points[this.pointIndex]);
      yield* (this.fixedTimeStep ? this.processSegmentFixedStep() : this.processSegment());
      this.prevPoints = this.nextPoints;
      this.absPositions = this.nextPoints.map((p, i) => p.relative ? p.pos + this.absPositions[i] : p.pos);
      if (!this.fixedTimeStep) {
        this.time = this.nextPoints[0].time;
      }

      this.pointIndex++;
    }
  }


  private* processSegment() {
    const segments = this.generateAbsoluteSegments();

    // Determine times to evaluate within this segment.
    let times: number[] = [];

    // Include the start time. This creates double points at the segment boundaries but
    // makes instantaneous acceleration changes look correct in the time series chart.
    times.push(0);

    // If there are any velocity zeroes, include them because it will help accurately determine the position limits.
    segments.forEach(seg => {
      times = times.concat(seg.getVelocityCrossings(0));
    });

    // Must include the end time else the curve will not pass through the control points.
    times.push(this.nextPoints[0].dt);

    // Add more sample points to approximate the desired time step.
    times = _.sortedUniq(times.sort((a, b) => a - b));
    let i = 0;
    let t0 = 0;
    while (i < times.length) {
      const t1 = times[i];
      let timeStep = this.timeStep;

      const vel = math.matrix(segments.map(s => s.getVel(t0)));
      const velMag = Number(math.norm(vel));
      if (Math.abs(velMag) > 0.001) {
        const accel = math.matrix(segments.map(s => s.getAccel(t0)));
        const accelMag = Number(math.norm(accel));
        const dot = math.dot(vel, accel);
        // This curvature is 1/r, where r is the radius of the osculating circle in whatever units the curve is in.
        // Adapted from https://en.wikipedia.org/wiki/Curvature#Curvature_of_space_curves
        const curvature = Number(math.sqrt(velMag * velMag * accelMag * accelMag - dot * dot)) / (velMag * velMag * velMag);
        if (!isNaN(curvature) && isFinite(curvature)) {
          timeStep = _.clamp(this.timeStep / (this.curvatureSizeScale * curvature), this.timeStep / 100, this.timeStep);
        }
      }

      let tNew = t1;
      if (t0 + timeStep < t1) {
        if (timeStep > (t1 - t0) / 2) {
          tNew = (t0 + t1) / 2;
          times.splice(i, 0, tNew);
        } else {
          tNew = t0 + timeStep;
          times.splice(i, 0, tNew);
        }
      }

      i++;
      t0 = tNew;
    }

    for (let i = 0; i < times.length; i++) {
      const timeInSegment = times[i];
      const row: Point[] = [];
      for (let seriesIndex = 0; seriesIndex < this.nextPoints.length; seriesIndex++) {
        row.push({
          dt: i > 0 ? timeInSegment - times[i - 1] : timeInSegment,
          time: this.time + timeInSegment,
          pos: segments[seriesIndex].getPos(timeInSegment),
          vel: segments[seriesIndex].getVel(timeInSegment),
          accel: segments[seriesIndex].getAccel(timeInSegment),
        });
      }

      yield row;
    }
  }


  private* processSegmentFixedStep() {
    const segments = this.generateAbsoluteSegments();

    while (this.time <= this.nextPoints[0].time) {
      const dt = this.time - this.prevPoints[0].time;
      const row: Point[] = [];
      for (let seriesIndex = 0; seriesIndex < this.nextPoints.length; seriesIndex++) {
        row.push({
          dt: this.time > 0 ? this.timeStep : 0,
          time: this.time,
          pos: segments[seriesIndex].getPos(dt),
          vel: segments[seriesIndex].getVel(dt),
          accel: segments[seriesIndex].getAccel(dt),
        });
      }

      yield row;

      // Note fixed time step means part of the last segment may be omitted if the end time
      // is not a multiple of the time step.
      // Also means the curve may not pass through the control points if their times are not multiple of the step.
      // Also it's possible very short segments could be skipped entirely.
      this.time += this.timeStep;
    }
  }


  private generateAbsoluteSegments(): PvtSegment[] {
    return this.nextPoints.map((nextPoint, i) => {
      let start = this.prevPoints[i];
      if (start.relative) {
        start = {
          ...start,
          pos: this.absPositions[i],
          relative: false,
        };
      }

      let end = nextPoint;
      if (end.relative) {
        end = {
          ...end,
          pos: start.pos + end.pos,
          relative: false,
        };
      }

      return new PvtSegment(start, end, this.velocityScale, this.accelerationScale);
    });
  }


  private newOutput(): Path {
    return {
      positionUnit: this.input.positionUnit,
      velocityUnit: this.input.velocityUnit,
      series: this.input.series.map(s => ({
        name: s.name,
        points: [],
      })),
    };
  }
}
