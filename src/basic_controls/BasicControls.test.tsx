
import React from 'react';
import { Container, injectable } from 'inversify';
import { fireEvent, render, RenderResult, within } from '@testing-library/react';
import _ from 'lodash';
import {
  Angle,
  CommandFailedException,
  CommandFailedExceptionData,
  Length,
  MovementFailedException,
  MovementInterruptedException,
  RemoteModeException,
  Time,
  Units,
  Velocity
} from '@zaber/motion';
import { GetSetting, GetSettingResult, IoPortType, type IoPortLabel } from '@zaber/motion/ascii';
import { match, P } from 'ts-pattern';
import type { SagaIterator } from 'redux-saga';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { Defer, defer, waitTick, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import {
  AxisMockBase,
  ConnectionMockBase,
  DeviceMockBase,
  MessageRoutersServiceMockBase,
  RouterConnectionMockBase
} from '../test/mocks/ascii';
import { MessageRoutersService } from '../message_router';
import { createContainer, destroyContainer } from '../container';
import { mockSingleDevice, mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import { selectConnections } from '../connection_manager';
import { getInputAndSelectByTitle, getSelectByTestId, UnitInfoServiceMockBase } from '../units/test_utils';
import { UnitInfo, UnitInfoService, unsupportedUnitInfo } from '../units';
import { actionDefinitions as pollingActions } from '../polling/actions';
import type { RT } from '../utils';
import { EntityKey, makeAxisKey, makeDeviceKey } from '../keys';

import { BasicControls } from './BasicControls';
import { TrackCyclic } from './movement/TrackCyclic';
import { TrackLinear } from './movement/TrackLinear';
import { KeyPressCombo } from './movement/types';

jest.mock('../devices', () => ({
  ...jest.requireActual('../devices'),
  commandExists: jest.fn((command: string) => {
    if (command === 'tools findrange hardstop save') {
      return true;
    }
    return false;
  }),
}));

const STEPS_PER_MM = 10;
const STEPS_PER_DEGREE = 60;
const STEPS_PER_SECOND = 1000;

let container: Container;

type AxisMockState = {
  persistentFlags: string[];
  clearableFlags: string[];
  settings: Record<string, number>;
  move?: { pos: number; vel: number };
};
let axisStates: Map<number, AxisMockState>;
const getAxisState = (axisNumber: number) => match(axisStates.get(axisNumber))
  .with(P.not(undefined), state => state)
  .otherwise(() => {
    const newSate: AxisMockState = {
      clearableFlags: [],
      persistentFlags: [],
      settings: {},
    };
    axisStates.set(axisNumber, newSate);
    return newSate;
  });

const getSettingValue = (state: AxisMockState, setting: string) => {
  if (typeof state.settings[setting] !== 'number') {
    throw new CommandFailedException(`Could not get setting ${setting}`, null!);
  }
  return state.settings[setting];
};

const getFlags = (state: AxisMockState) => new Set([...state.clearableFlags, ...state.persistentFlags]);

const clearFlags = (state: AxisMockState) => {
  const flags = new Set([...state.clearableFlags, ...state.persistentFlags]);
  state.clearableFlags = [];
  return flags;
};

let axes: AxisMock[];

function setLinearAxisMockState(axisNumber: number, pos: number, min: number, max: number, maxSpeed: number) {
  const axisState = getAxisState(axisNumber);
  axisState.settings.pos = pos;
  axisState.settings['limit.min'] = min;
  axisState.settings['limit.max'] = max;
  axisState.settings.maxspeed = maxSpeed;
}

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);

    this._state = getAxisState(id);
    axes.push(this);
  }
  _state: AxisMockState;

  getPosition = jest.fn(async (units: Units) => {
    if (units === Length.mm) {
      return this._state.settings.pos / 10;
    } else if (units === Angle.DEGREES) {
      return this._state.settings.pos / 30;
    }
    return this._state.settings.pos;
  });
  warnings = {
    getFlags: jest.fn(async () => getFlags(this._state)),
    clearFlags: jest.fn(async () => clearFlags(this._state)),
  };
  settings = {
    get: jest.fn(setting => getSettingValue(this._state, setting)),
    convertToNativeUnits: jest.fn().mockImplementation(() => 360),
  };
  home = jest.fn().mockResolvedValue(undefined);
  stop = jest.fn().mockResolvedValue(undefined);
  moveAbsolute = jest.fn((position: number, units: Units) => {
    if (units === Length.cm) {
      this._state.settings.pos = position * 100;
    } else {
      this._state.settings.pos = position;
    }
  });
  moveRelative = jest.fn((position: number, _units: Units) => {
    this._state.settings.pos = _.clamp(
      this._state.settings.pos + position,
      this._state.settings['limit.min'],
      this._state.settings['limit.max']
    );
  });
  moveVelocity = jest.fn(async (vel: number, _units: Units) => {
    if (vel > 0) {
      this._state.settings.pos = this._state.settings['limit.max'];
    } else {
      this._state.settings.pos = this._state.settings['limit.min'];
    }
  });
  moveMin = jest.fn().mockResolvedValue(undefined);
  moveMax = jest.fn().mockResolvedValue(undefined);
  moveSin = jest.fn().mockResolvedValue(undefined);
  moveSinStop = jest.fn().mockResolvedValue(undefined);
  waitUntilIdle = jest.fn().mockResolvedValue(undefined);
  getNumberOfIndexPositions = jest.fn().mockResolvedValue(6);
  genericCommand = jest.fn().mockResolvedValue(undefined);
  driverEnable = jest.fn().mockResolvedValue(undefined);
  driverDisable = jest.fn().mockResolvedValue(undefined);
}

let mockIo: {
  ai: number[];
  ao: number[];
  di: boolean[];
  do: boolean[];
};

let mockLabelsAll: IoPortLabel[];

const getDeviceSettingValue = (state: AxisMockState, setting: string) => {
  if (setting === 'io.di.port') {
    return mockIo.di.reduce((acc, val, i) => acc + (val ? 1 << i : 0), 0);
  } else if (setting === 'io.do.port') {
    return mockIo.do.reduce((acc, val, i) => acc + (val ? 1 << i : 0), 0);
  }
  return getSettingValue(state, setting);
};

let device: DeviceMock;
class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    this._axisState = getAxisState(1);
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    device = this;
  }
  _axisState: AxisMockState;

  lockstep = {
    home: jest.fn().mockResolvedValue(undefined),
    moveMin: jest.fn().mockResolvedValue(undefined),
    moveMax: jest.fn().mockResolvedValue(undefined),
    moveSin: jest.fn().mockResolvedValue(undefined),
    stop: jest.fn().mockResolvedValue(undefined),
    moveAbsolute: jest.fn((position: number, units: Units) => {
      if (units === Length.cm) {
        position = position * 100;
      }
      this.axes.get(0)._state.settings.pos = position;
      this.axes.get(1)._state.settings.pos = position;
    }),
    getOffsets: jest.fn().mockResolvedValue([299]),
  };
  io = {
    getChannelsInfo: jest.fn(async () => ({
      numberAnalogOutputs: mockIo.ao.length,
      numberAnalogInputs: mockIo.ai.length,
      numberDigitalOutputs: mockIo.do.length,
      numberDigitalInputs: mockIo.di.length,
    })),
    getAllAnalogInputs: jest.fn(async () => mockIo.ai),
    getAnalogOutput: jest.fn(async (channel: number) => mockIo.ao[channel - 1]),
    getAllAnalogOutputs: jest.fn(async () => mockIo.ao),
    setDigitalOutput: jest.fn(async (channel: number, value: boolean) => { mockIo.do[channel - 1] = value }),
    setAnalogOutput: jest.fn(async (channel: number, value: number) => { mockIo.ao[channel - 1] = value }),

    getAllLabels: jest.fn(async () => mockLabelsAll),
    getLabel: jest.fn(async (portType: IoPortType, channel: number) => mockLabelsAll.find(
      label => label.portType === portType && label.channelNumber === channel
    )),
    setLabel: jest.fn(async (portType: IoPortType, channel: number, label: string) => {
      mockLabelsAll.push({ portType, channelNumber: channel, label });
    }),
  };
  settings = {
    getMany: jest.fn(async (...settings: GetSetting[]) => (
      settings.map(({ setting, axes, unit }): GetSettingResult => {
        const values = (axes ?? [0]).map(axisNumber => {
          const state = getAxisState(axisNumber);
          const value = getDeviceSettingValue(state, setting);
          return match(unit)
            .with(Length.MILLIMETRES, () => value / STEPS_PER_MM)
            .with(Angle.DEGREES, () => value / STEPS_PER_DEGREE)
            .otherwise(() => value);
        });
        return { setting, values, unit: unit ?? Units.NATIVE };
      })
    ))
  };

  warnings = {
    getFlags: jest.fn(async () => getFlags(this._axisState)),
    clearFlags: jest.fn(async () => clearFlags(this._axisState)),
  };

  getLockstep = jest.fn(() => this.lockstep);
  genericCommand = jest.fn().mockResolvedValue(undefined);
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  genericCommandNoResponse = jest.fn().mockResolvedValue(undefined);
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const TestBasicControls = wrapWithNewStore(wrapWithRouter(BasicControls));

let wrapper: RenderResult;

@injectable()
class UnitInfoServiceMock extends UnitInfoServiceMockBase {
  public* getSettingUnitInfo(entity: string, setting: string): SagaIterator<UnitInfo> {
    switch (setting) {
      case 'pos': return this.mockUnitInfo('Length', 1 / STEPS_PER_MM, 1);
      case 'maxspeed': return this.mockUnitInfo('Velocity', 1 / STEPS_PER_MM, 1);
      default: return unsupportedUnitInfo();
    }
  }
  public* getCommandUnitInfo(entity: string, command: string): SagaIterator<UnitInfo> {
    switch (command) {
      case 'move sin amplitude': return this.mockUnitInfo('Length', 1 / STEPS_PER_MM, 1);
      case 'move sin amplitude period': return this.mockUnitInfo('Time', 1 / STEPS_PER_SECOND, 1);
      default: return unsupportedUnitInfo();
    }
  }
}

describe('BasicControls', () => {
  let position: HTMLElement;

  beforeEach(() => {
    device = null!;
    axisStates = new Map();
    axes = [];
    mockIo = { ai: [], ao: [], di: [], do: [] };
    mockLabelsAll = [];

    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

    wrapper = render(<TestBasicControls/>);
  });

  function remount() {
    const store = TestBasicControls.testStore;
    wrapper.unmount();
    wrapper = render(<TestBasicControls store={store}/>);
  }

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
    mockIo = null!;
    mockLabelsAll = null!;

    destroyContainer();
    container = null!;

    axes = null!;
    axisStates = null!;
    device = null!;
  });

  describe('pinning', () => {
    let pins: HTMLElement[];
    let deviceKey: EntityKey;

    beforeEach(async () => {
      setLinearAxisMockState(1, 0, 0, 1000, 100);
      setLinearAxisMockState(2, 0, 0, 1000, 100);

      container.bind<unknown>(UnitInfoService).to(UnitInfoServiceMock);
      const store = TestBasicControls.testStore;
      mockSingleDevice(store);
      mockSingleDeviceWithPeripherals(store, { peripheralCount: 2, type: 'smart' });
      const connection = _.sample(selectConnections(TestBasicControls.testStore.getState()))!;
      deviceKey = makeDeviceKey(connection.key, 1);
      connectionViewMockInstance.props.onSelect(connection.key);
      await waitUntilPass(() => expect(wrapper.queryAllByText(/Loading/).length).toBe(0));
      wrapper.getByText(/X-MCC2/);
      pins = wrapper.queryAllByTitle('Pin');
      expect(pins).toHaveLength(3);
    });

    afterEach(() => {
      pins = null!;
      deviceKey = null!;
    });

    test('pinned devices appear even when not selected', async () => {
      fireEvent.click(pins[0]);
      connectionViewMockInstance.props.onSelect(null);
      await waitUntilPass(() => wrapper.getByTitle('Selected: null'));
      wrapper.getByText(/X-MCC2/);
    });

    test('pinned peripherals appear even when not selected', async () => {
      fireEvent.click(pins[1]);
      const peripheralKey = makeAxisKey(deviceKey, 2);
      connectionViewMockInstance.props.onSelect(peripheralKey);
      await waitUntilPass(() => wrapper.getByTitle(`Selected: ${peripheralKey}`));
      wrapper.getAllByText(/LRM150A-E03T4A/);
      wrapper.getByText(/Axis 1/);
      wrapper.getByText(/Axis 2/);
    });

    test('pinned peripherals appears only once when selected', async () => {
      fireEvent.click(pins[2]);
      const peripheralKey = makeAxisKey(deviceKey, 2);
      connectionViewMockInstance.props.onSelect(peripheralKey);
      await waitUntilPass(() => wrapper.getByTitle(`Selected: ${peripheralKey}`));
      expect(wrapper.queryAllByText(/LRM150A-E03T4A/)).toHaveLength(1);
    });

    test('pinned peripherals appears only once when their device is selected or pinned', async () => {
      fireEvent.click(pins[0]);
      fireEvent.click(pins[1]);
      connectionViewMockInstance.props.onSelect(deviceKey);
      await waitUntilPass(() => wrapper.getByTitle(`Selected: ${deviceKey}`));
      expect(wrapper.queryAllByText(/Axis 1/)).toHaveLength(1);
    });
  });

  describe('integrated linear device', () => {
    const INITIAL_POS = 310;
    const LIMIT_MIN = 200;
    const LIMIT_MAX = 1000;
    const MAX_SPEED = 100;
    let connectionKey: string;

    beforeEach(async () => {
      setLinearAxisMockState(1, INITIAL_POS, LIMIT_MIN, LIMIT_MAX, MAX_SPEED);

      container.bind<unknown>(UnitInfoService).to(UnitInfoServiceMock);
      const store = TestBasicControls.testStore;
      mockSingleDevice(store);
      const connection = _.sample(selectConnections(TestBasicControls.testStore.getState()))!;
      connectionKey = connection.key;
      connectionViewMockInstance.props.onSelect(connectionKey);
      await waitUntilPass(() => wrapper.getByTitle('Home Axis'));
      wrapper.getByText(/X-LHM/);
      position = wrapper.getByTitle('Linear Axis Position');
      await waitUntilPass(() => {
        wrapper.getByText(/20\.00\s+mm/);
        wrapper.getByText(/100\.00\s+mm/);
        expect(position.innerHTML).toBe('31');
      });
    });
    test('recalculates values when units change', async () => {
      await waitUntilPass(() => {
        expect(position.innerHTML).toBe('31');
      });

      const positionUnit = wrapper.getByTitle('Select Position Units');
      fireEvent.change(positionUnit, { target: { value: Length.CENTIMETRES } });

      await waitUntilPass(() => {
        wrapper.getByText(/2\.00\s+cm/);
        wrapper.getByText(/10\.00\s+cm/);
        expect(position.innerHTML).toBe('3.1');
      });
    });

    test('moves the device when clicked on buttons', async () => {
      fireEvent.click(wrapper.getByTitle('Home Axis'));
      await waitUntilPass(() => {
        expect(axes).toHaveLength(1);
        expect(axes[0].home).toHaveBeenCalledTimes(1);
      });

      fireEvent.click(wrapper.getByTitle('Move to Beginning'));
      await waitUntilPass(() => {
        expect(axes[0].moveAbsolute).toHaveBeenCalledWith(200, undefined);
      });

      fireEvent.click(wrapper.getByTitle('Move to End'));
      await waitUntilPass(() => {
        expect(axes[0].moveAbsolute).toHaveBeenCalledWith(1000, undefined);
      });

      fireEvent.click(wrapper.getByTitle('Stop Axis'));
      await waitUntilPass(() => {
        expect(axes[0].stop).toHaveBeenCalledTimes(1);
      });
    });

    test('movement button is held until movement finishes', async () => {
      fireEvent.click(wrapper.getByTitle('Home Axis'));
      expect(wrapper.getByTitle('Home Axis')).toHaveClass('activated');
      await waitUntilPass(() => {
        expect(wrapper.getByTitle('Home Axis')).not.toHaveClass('activated');
      });

      fireEvent.click(wrapper.getByTitle('Move to Beginning'));
      expect(wrapper.getByTitle('Move to Beginning')).toHaveClass('activated');
      await waitUntilPass(() => {
        expect(wrapper.getByTitle('Move to Beginning')).not.toHaveClass('activated');
      });

      fireEvent.click(wrapper.getByTitle('Move to End'));
      expect(wrapper.getByTitle('Move to End')).toHaveClass('activated');
      await waitUntilPass(() => {
        expect(wrapper.getByTitle('Move to End')).not.toHaveClass('activated');
      });
    });

    test('clears selection when devices reload unless the whole connection is selected', async () => {
      const store = TestBasicControls.testStore;

      connectionViewMockInstance.props.onSelect(makeDeviceKey(connectionKey, 1));
      wrapper.getByText(/X-LHM/);
      mockSingleDevice(store);
      // selection cleared
      await waitUntilPass(() => wrapper.getByText(/Select a connection to start moving/));

      connectionViewMockInstance.props.onSelect(connectionKey);
      mockSingleDevice(store);
      // selection preserved
      await waitUntilPass(() => wrapper.getByText(/X-LHM/));
    });

    describe('jog button', () => {
      let deferredMoveCompletion: Defer<void>;

      beforeEach(async () => {
        deferredMoveCompletion = defer<void>();
        axes[0].moveVelocity.mockReturnValue(deferredMoveCompletion.promise);
      });

      afterEach(async () => {
        deferredMoveCompletion.resolve();
      });

      test('left button moves axis by negative maxspeed', async () => {
        fireEvent.mouseDown(wrapper.getByTitle('Move towards Beginning'));
        await waitUntilPass(() => {
          expect(axes[0].moveVelocity).toHaveBeenCalledWith(-MAX_SPEED, undefined);
        });
        fireEvent.mouseUp(wrapper.getByTitle('Move towards Beginning'));
        await waitUntilPass(() => {
          expect(axes[0].stop).toHaveBeenCalled();
        });
      });

      test('right button moves axis by maxspeed', async () => {
        fireEvent.mouseDown(wrapper.getByTitle('Move towards End'));
        await waitUntilPass(() => {
          expect(axes[0].moveVelocity).toHaveBeenCalledWith(MAX_SPEED, undefined);
        });
        fireEvent.mouseUp(wrapper.getByTitle('Move towards End'));
        await waitUntilPass(() => {
          expect(axes[0].stop).toHaveBeenCalled();
        });
      });

      test('leaving the button will not stop the movement after it finished', async () => {
        fireEvent.mouseDown(wrapper.getByTitle('Move towards End'));
        await waitUntilPass(() => {
          expect(axes[0].moveVelocity).toHaveBeenCalled();
        });

        deferredMoveCompletion.resolve();
        await waitTick();

        fireEvent.mouseLeave(wrapper.getByTitle('Move towards End'));
        expect(axes[0].stop).not.toHaveBeenCalled();
      });

      test('reloads maxspeed on re-mount', async () => {
        const axisState = getAxisState(1);
        axisState.settings.maxspeed = MAX_SPEED * 2;
        axisState.settings['limit.max'] = LIMIT_MAX * 2;
        remount();

        await waitUntilPass(() => wrapper.getByText(/200\.00 mm/));

        fireEvent.mouseDown(wrapper.getByTitle('Move towards End'));
        await waitUntilPass(() => {
          expect(axes[0].moveVelocity).toHaveBeenCalledWith(MAX_SPEED * 2, undefined);
        });
      });
    });

    describe('interactive track', () => {
      TrackLinear.prototype.interactiveBoundingBox = jest.fn(() => (
        { width: 12_000, height: 10, top: 0, bottom: 10, left: 0, right: 12_000 } as DOMRect
      ));
      TrackLinear.prototype.indicatorBoundingBox = jest.fn(() => (
        { width: 10_000, height: 10, top: 0, bottom: 10, left: 1_000, right: 11_000 } as DOMRect
      ));
      const generateMouseHover = (x: number) => fireEvent.mouseMove(
        wrapper.getByTestId('interactive-linear-track'), { clientX: x, clientY: 5 }
      );

      test('shows projected device position on hover', async () => {
        generateMouseHover(500);
        expect(wrapper.getAllByText(/20\.00 mm/).length).toBe(2);
        generateMouseHover(11_500);
        expect(wrapper.getAllByText(/100\.00 mm/).length).toBe(2);
        generateMouseHover(6_000);
        wrapper.getByText(/60\.00 mm/);
      });

      test('rounds device position to whole number when native units are used', async () => {
        fireEvent.change(wrapper.getByTitle('Select Position Units'), { target: { value: Units.NATIVE } });
        generateMouseHover(6_002);
        wrapper.getByText(/600 Native/);
      });

      test('moves the device when clicked on position display', async () => {
        generateMouseHover(6_000);
        fireEvent.mouseUp(wrapper.getByTestId('interactive-linear-track'), { button: 0 });
        await waitUntilPass(() => {
          expect(axes[0].moveAbsolute).toHaveBeenCalledWith(600, undefined);
        });
      });

      test('reloads limits on re-mount', async () => {
        const axisState = getAxisState(1);
        axisState.settings['limit.min'] = 130;
        axisState.settings['limit.max'] = 1050;
        remount();

        await waitUntilPass(() => {
          wrapper.getByText(/13\.00 mm/);
          wrapper.getByText(/105\.00 mm/);
        });
      });
    });

    describe('precise movement', () => {
      let moveAbs: RT<typeof getInputAndSelectByTitle>;
      let moveVel: RT<typeof getInputAndSelectByTitle>;
      let moveRel: RT<typeof getInputAndSelectByTitle>;

      beforeEach(async () => {
        await waitUntilPass(() => {
          expect(position.innerHTML).toBe('31');
        });
        fireEvent.click(wrapper.getByTitle('Expand Precise Movement section'));
        wrapper.getByTitle('Collapse Precise Movement section');
        await waitUntilPass(() => {
          moveAbs = getInputAndSelectByTitle(wrapper, 'Absolute Position Control');
          moveVel = getInputAndSelectByTitle(wrapper, 'Move at Velocity Control');
          moveRel = getInputAndSelectByTitle(wrapper, 'Relative Movement Control');
        });
      });

      test('input unit conversions', () => {
        expect(moveVel.unitSelect.value).toBe(Velocity.MILLIMETRES_PER_SECOND);
        expect(moveVel.valueInput.value).toBe(`${MAX_SPEED / STEPS_PER_MM}`);

        fireEvent.change(moveVel.unitSelect,
          { target: { value: Velocity.CENTIMETRES_PER_SECOND } });
        expect(moveVel.unitSelect.value).toBe(Velocity.CENTIMETRES_PER_SECOND);
        expect(moveVel.valueInput.value).toBe(`${MAX_SPEED / (10 * STEPS_PER_MM)}`);
      });

      test('move to absolute position', async () => {
        fireEvent.change(moveAbs.unitSelect, { target: { value: Length.CENTIMETRES } });
        fireEvent.change(moveAbs.valueInput, { target: { value: '1' } });

        fireEvent.click(wrapper.getByTitle('Move to Absolute Position'));
        await waitUntilPass(() => expect(axes[0].moveAbsolute).toHaveBeenCalledWith(1, Length.CENTIMETRES));
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => expect(position.innerHTML).toBe('10'));
      });

      test('absolute position error appears and disappears', async () => {
        const preciseWrapper = within(wrapper.getByText('Precise Movement').parentElement!.parentElement!);
        const queryLimitsMM = 'Value must be between 20 and 100.';
        fireEvent.input(moveAbs.valueInput, { target: { value: '60' } });
        expect(preciseWrapper.queryByText(queryLimitsMM)).toBeNull();
        fireEvent.input(moveAbs.valueInput, { target: { value: '100.1' } });
        preciseWrapper.getByText(queryLimitsMM);
        fireEvent.input(moveAbs.valueInput, { target: { value: '60' } });
        expect(preciseWrapper.queryByText(queryLimitsMM)).toBeNull();
      });

      test('absolute position error converts units', async () => {
        fireEvent.change(moveAbs.unitSelect, { target: { value: Length.CENTIMETRES } });
        fireEvent.input(moveAbs.valueInput, { target: { value: '10.1' } });
        wrapper.getByText('Value must be between 2 and 10.');
      });

      test('move by relative distance', async () => {
        fireEvent.change(moveRel.valueInput, { target: { value: '5' } });
        fireEvent.click(wrapper.getByTitle('Move by Relative Distance towards Beginning'));
        await waitUntilPass(() => expect(axes[0].moveRelative).toHaveBeenCalledTimes(1));
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => expect(position.innerHTML).toBe('30.5'));
        expect(position.innerHTML).toBe('30.5');

        fireEvent.change(moveRel.valueInput, { target: { value: '10' } });
        fireEvent.click(wrapper.getByTitle('Move by Relative Distance towards End'));
        await waitUntilPass(() => expect(axes[0].moveRelative).toHaveBeenCalledTimes(2));
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => expect(position.innerHTML).toBe('31.5'));
      });


      test('move at a given velocity', async () => {
        fireEvent.click(wrapper.getByTitle('Move at Velocity towards Beginning'));
        await waitUntilPass(() => expect(axes[0].moveVelocity).lastCalledWith(-MAX_SPEED / STEPS_PER_MM, Velocity.MILLIMETRES_PER_SECOND));
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => expect(position.innerHTML).toBe('20'));

        fireEvent.change(moveVel.unitSelect, { target: { value: Velocity.METRES_PER_SECOND } });
        fireEvent.change(moveVel.valueInput, { target: { value: '5' } });
        fireEvent.click(wrapper.getByTitle('Move at Velocity towards End'));
        await waitUntilPass(() => expect(axes[0].moveVelocity).lastCalledWith(5, Velocity.METRES_PER_SECOND));
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => expect(position.innerHTML).toBe('100'));
      });

      test('absolute position reacts to Enter key', async () => {
        fireEvent.change(moveAbs.valueInput, { target: { value: '73' } });
        fireEvent.keyDown(moveAbs.valueInput, { key: 'Enter' });
        await waitUntilPass(() => expect(axes[0].moveAbsolute).toHaveBeenCalledTimes(1));
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => expect(position.innerHTML).toBe('7.3'));
      });

      test('reloads maxspeed on re-mounting', async () => {
        expect(moveVel.valueInput).toHaveValue(MAX_SPEED / 10);

        const axisState = getAxisState(1);
        axisState.settings.maxspeed = MAX_SPEED * 2;
        remount();

        await waitUntilPass(() => {
          moveVel = getInputAndSelectByTitle(wrapper, 'Move at Velocity Control');
          expect(moveVel.valueInput).toHaveValue(2 * MAX_SPEED / 10);
        });
      });
    });

    describe('sinusoidal movement', () => {
      beforeEach(async () => {
        await waitUntilPass(() => {
          expect(position.innerHTML).toBe('31');
        });
        fireEvent.click(wrapper.getByTitle('Open Menu'));
        fireEvent.click(wrapper.getByTitle('Sinusoidal Motion'));
      });

      test('start move without count', async () => {
        fireEvent.change(wrapper.getByTitle('Value of Amplitude'), { target: { value: '1' } });
        fireEvent.change(wrapper.getByTitle('Value of Period'), { target: { value: '1' } });
        fireEvent.click(wrapper.getByTitle('Start'));
        await waitUntilPass(() => {
          expect(axes[0].stop).toHaveBeenCalledTimes(1);
          expect(axes[0].moveSin).lastCalledWith(1, Length.MILLIMETRES, 1, Time.SECONDS, { count: undefined, waitUntilIdle: false });
        });
      });

      test('start move with count', async () => {
        fireEvent.change(wrapper.getByTitle('Value of Amplitude'), { target: { value: '2' } });
        fireEvent.change(wrapper.getByTitle('Value of Period'), { target: { value: '3' } });
        fireEvent.change(wrapper.getByTitle('Cycles'), { target: { value: '4' } });
        fireEvent.click(wrapper.getByTitle('Start'));
        await waitUntilPass(() => {
          expect(axes[0].stop).toHaveBeenCalledTimes(1);
          expect(axes[0].moveSin).lastCalledWith(2, Length.MILLIMETRES, 3, Time.SECONDS, { count: 4, waitUntilIdle: false });
        });
      });

      test('stops movement gracefully', async () => {
        const deferred = defer<void>();
        axes[0].moveSin.mockImplementationOnce(() => deferred.promise);

        fireEvent.change(wrapper.getByTitle('Value of Amplitude'), { target: { value: '1' } });
        fireEvent.change(wrapper.getByTitle('Value of Period'), { target: { value: '1' } });
        fireEvent.click(wrapper.getByTitle('Start'));
        await waitUntilPass(() => expect(axes[0].moveSin).toHaveBeenCalled());
        axes[0].stop.mockClear();

        fireEvent.click(wrapper.getByText('End Sinusoidal Motion'));
        await waitUntilPass(() => expect(axes[0].moveSinStop).toHaveBeenCalled());
        expect(axes[0].stop).not.toHaveBeenCalled();
      });

      test('Values for the same axis are stored', async () => {
        fireEvent.change(wrapper.getByTitle('Value of Amplitude'), { target: { value: '3' } });
        fireEvent.change(wrapper.getByTitle('Value of Period'), { target: { value: '4' } });
        fireEvent.change(wrapper.getByTitle('Cycles'), { target: { value: '5' } });
        fireEvent.click(wrapper.getByTitle('Cancel'));
        await waitUntilPass(() => {
          expect(wrapper.queryByTitle('Value of Amplitude')).toBeNull();
        });
        fireEvent.click(wrapper.getByTitle('Open Menu'));
        fireEvent.click(wrapper.getByTitle('Sinusoidal Motion'));
        await waitUntilPass(() => {
          expect(wrapper.getByTitle('Value of Amplitude')).toHaveValue(3);
          expect(wrapper.getByTitle('Value of Period')).toHaveValue(4);
          expect(wrapper.getByTitle('Cycles')).toHaveValue(5);
        });
      });
    });

    describe('findrange hardstop', () => {
      test('does not appear for non-adr devices', async () => {
        await waitUntilPass(() => {
          expect(position.innerHTML).toBe('31');
        });
        fireEvent.click(wrapper.getByTitle('Open Menu'));
        expect(wrapper.queryByTitle('Set Travel Limits')).toBeNull();
      });
    });

    describe('keyboard shortcuts', () => {
      let selectDropdown: RT<typeof getSelectByTestId>;
      let moveVel: RT<typeof getInputAndSelectByTitle>;
      let moveRel: RT<typeof getInputAndSelectByTitle>;

      beforeEach(async () => {
        await waitUntilPass(() => {
          expect(position.innerHTML).toBe('31');
        });
        fireEvent.click(wrapper.getByTitle('Expand Precise Movement section'));
        wrapper.getByTitle('Collapse Precise Movement section');
        await waitUntilPass(() => {
          selectDropdown = getSelectByTestId(wrapper, 'basic-controls-shortcut-select');
          moveVel = getInputAndSelectByTitle(wrapper, 'Move at Velocity Control');
          moveRel = getInputAndSelectByTitle(wrapper, 'Relative Movement Control');
        });
      });

      test('select left / right control option', () => {
        expect(selectDropdown.value).toBe(KeyPressCombo.None.toString());
        fireEvent.change(selectDropdown, { target: { value: KeyPressCombo.LeftRight } });
        expect(selectDropdown.value).toBe(KeyPressCombo.LeftRight.toString());
      });

      test('unmodified key press performs move vel at 1/4 speed', async () => {
        fireEvent.change(selectDropdown, { target: { value: KeyPressCombo.LeftRight } });
        fireEvent.change(moveVel.unitSelect, { target: { value: Velocity.MILLIMETRES_PER_SECOND } });
        fireEvent.change(moveVel.valueInput, { target: { value: '8' } });

        fireEvent.keyDown(document.body, { key: 'ArrowRight' });

        await waitUntilPass(() => expect(axes[0].moveVelocity).toHaveBeenCalledWith(2, Velocity.MILLIMETRES_PER_SECOND));
      });

      test('shift + key press performs move vel at full speed', async () => {
        fireEvent.change(selectDropdown, { target: { value: KeyPressCombo.LeftRight } });
        fireEvent.change(moveVel.unitSelect, { target: { value: Velocity.MILLIMETRES_PER_SECOND } });
        fireEvent.change(moveVel.valueInput, { target: { value: '8' } });

        fireEvent.keyDown(document.body, { key: 'ArrowRight', shiftKey: true });

        await waitUntilPass(() => expect(axes[0].moveVelocity).toHaveBeenCalledWith(8, Velocity.MILLIMETRES_PER_SECOND));
      });

      test('ctrl + key press performs move rel', async () => {
        fireEvent.change(selectDropdown, { target: { value: KeyPressCombo.LeftRight } });
        fireEvent.change(moveRel.unitSelect, { target: { value: Length.MILLIMETRES } });
        fireEvent.change(moveRel.valueInput, { target: { value: '15' } });

        fireEvent.keyDown(document.body, { key: 'ArrowRight', ctrlKey: true });

        await waitUntilPass(() => expect(axes[0].moveRelative).toHaveBeenCalledWith(15, Length.MILLIMETRES));
      });

      test('up / down inputs are functional', async () => {
        fireEvent.change(selectDropdown, { target: { value: KeyPressCombo.UpDown } });
        fireEvent.change(moveVel.unitSelect, { target: { value: Velocity.MILLIMETRES_PER_SECOND } });
        fireEvent.change(moveVel.valueInput, { target: { value: '8' } });

        fireEvent.keyDown(document.body, { key: 'ArrowUp' });
        await waitUntilPass(() => expect(axes[0].moveVelocity).toHaveBeenCalledWith(2, Velocity.MILLIMETRES_PER_SECOND));

        fireEvent.keyDown(document.body, { key: 'ArrowDown' });
        await waitUntilPass(() => expect(axes[0].moveVelocity).toHaveBeenCalledWith(2, Velocity.MILLIMETRES_PER_SECOND));
      });

      test('page up / page down inputs are functional', async () => {
        fireEvent.change(selectDropdown, { target: { value: KeyPressCombo.PageUpPageDown } });
        fireEvent.change(moveVel.unitSelect, { target: { value: Velocity.MILLIMETRES_PER_SECOND } });
        fireEvent.change(moveVel.valueInput, { target: { value: '8' } });

        fireEvent.keyDown(document.body, { key: 'PageUp' });
        await waitUntilPass(() => expect(axes[0].moveVelocity).toHaveBeenCalledWith(2, Velocity.MILLIMETRES_PER_SECOND));

        fireEvent.keyDown(document.body, { key: 'PageDown' });
        await waitUntilPass(() => expect(axes[0].moveVelocity).toHaveBeenCalledWith(2, Velocity.MILLIMETRES_PER_SECOND));
      });

      test('key presses do nothing when shortcuts are off', async () => {
        fireEvent.change(selectDropdown, { target: { value: KeyPressCombo.None } });

        fireEvent.keyDown(document.body, { key: 'ArrowRight', ctrlKey: true });
        fireEvent.keyDown(document.body, { key: 'ArrowUp', ctrlKey: true });
        fireEvent.keyDown(document.body, { key: 'PageUp', ctrlKey: true });
        fireEvent.keyDown(document.body, { key: 'ArrowRight', shiftKey: true });
        fireEvent.keyDown(document.body, { key: 'ArrowUp', shiftKey: true });
        fireEvent.keyDown(document.body, { key: 'PageUp', ctrlKey: true });

        await waitUntilPass(() => expect(axes[0].moveVelocity).toBeCalledTimes(0));
        await waitUntilPass(() => expect(axes[0].moveRelative).toBeCalledTimes(0));
      });
    });

    describe('failed movement with flag', () => {
      beforeEach(async () => {
        const axisState = getAxisState(1);
        axisState.clearableFlags.push('FS');
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => wrapper.getByText(/Stalled and Stopped/));
      });

      test('shows details', () => {
        fireEvent.click(wrapper.getByText(/Show details/i));
        wrapper.getByText('The stall is a terrible thing to happen.');
      });

      test('allows to dismiss error', async () => {
        fireEvent.click(wrapper.getByTitle('Clear FS Flag'));
        expect(wrapper.queryByTitle('Clear FS Flag')).toBeNull();
      });

      test('clears movement error when new movement is initiated', async () => {
        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => expect(wrapper.queryByText(/Stalled and Stopped/)).toBeNull());
      });
    });

    describe('failed movement - other cases', () => {
      test('displays error when movement fails with no warning flags', async () => {
        axes[0].home.mockRejectedValueOnce(new MovementFailedException('Movement has failed', {
          reason: '',
          warnings: [],
          device: 0,
          axis: 0,
        }));

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => {
          wrapper.getByText(/Movement Failed/);
        });
      });

      test('displays custom error when movement fails with BADDATA', async () => {
        axes[0].home.mockRejectedValueOnce(new CommandFailedException('Command has failed', {
          responseData: 'BADDATA', replyFlag: 'RJ',
        } as CommandFailedExceptionData));

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => {
          wrapper.getByText(/Invalid Position/);
        });
      });

      test('displays custom error when movement fails with REMOTE', async () => {
        axes[0].home.mockRejectedValueOnce(new RemoteModeException('Cannot execute command', {
          responseData: 'REMOTE', replyFlag: 'RJ',
        } as CommandFailedExceptionData));

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => {
          wrapper.getByText(/Enable USB Control/);
        });
      });

      test('displays error when movement fails with other errors', async () => {
        axes[0].home.mockRejectedValueOnce(new CommandFailedException('Command has failed', {
          responseData: 'BADCOMMAND', replyFlag: 'RJ',
        } as CommandFailedExceptionData));

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => {
          wrapper.getByText(/Movement Failed/);
        });
      });

      test('displays only flags that are not persistent', async () => {
        axes[0]._state.persistentFlags.push('FR');
        axes[0].home.mockImplementationOnce(async () => {
          throw new MovementFailedException('Movement has failed', {
            reason: '',
            warnings: ['FR', 'WS'],
            device: 0,
            axis: 0,
          });
        });

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => wrapper.getByText(/Movement Failed with Flag WS/));
      });

      test('displays note when movement is interrupted', async () => {
        axes[0].waitUntilIdle.mockImplementationOnce(() => {
          axes[0]._state.persistentFlags.push('NI');
          return Promise.reject(new MovementInterruptedException('Interrupted', null!));
        });

        fireEvent.click(wrapper.getByTitle('Expand Precise Movement section'));
        await waitUntilPass(() =>
          fireEvent.click(wrapper.getByTitle('Move at Velocity towards End')));

        await waitUntilPass(() => {
          wrapper.getByText(/Warning Flag NI/);
        });
      });

      test('Does not show the Movement Interrupted flag when the user interrupts the movement', async () => {
        axes[0].home.mockImplementationOnce(async () => {
          axes[0]._state.persistentFlags.push('NI');
        });
        axes[0].stop.mockImplementationOnce(async () => {
          axes[0]._state.persistentFlags = [];
        });
        const waitUntilIdle = defer<void>();
        axes[0].waitUntilIdle.mockImplementationOnce(() => waitUntilIdle.promise);

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => expect(axes[0]._state.persistentFlags[0]).toBe('NI'));
        expect(wrapper.queryByText(/NI/)).toBeNull();
        waitUntilIdle.resolve();
        await waitUntilPass(() => expect(axes[0]._state.persistentFlags.length).toBe(0));
        expect(wrapper.queryByText(/NI/)).toBeNull();
      });

      test('shows warnings for the present flags and description', async () => {
        axes[0]._state.persistentFlags = ['FO'];

        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => {
          wrapper.getByText(/Warning Flag FO/);
        });

        axes[0]._state.persistentFlags = ['WR'];

        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => {
          wrapper.getByText(/No Reference Position \(WR\) - Homing Required/);
          expect(wrapper.queryByText(/Warning Flag FO/)).toBeNull();
        });

        fireEvent.click(wrapper.getByText(/Show details/i));
        wrapper.getByText(/The axis does not have a reference position/);
      });
    });

    describe('enable/disable driver', () => {
      test('disable driver', async () => {
        fireEvent.click(wrapper.getByTitle('Open Menu'));
        fireEvent.click(wrapper.getByTitle('Disable Driver'));

        await waitUntilPass(() => expect(device.getAxis(1).driverDisable).toHaveBeenCalled());
      });

      test('enable driver from menu', async () => {
        axes[0]._state.persistentFlags.push('FO');
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => wrapper.getByText('Enable Driver'));

        fireEvent.click(wrapper.getByTitle('Open Menu'));
        fireEvent.click(wrapper.getByTitle('Enable Driver'));
        await waitUntilPass(() => expect(device.getAxis(1).driverEnable).toHaveBeenCalled());
      });

      test('enable driver from warning', async () => {
        axes[0]._state.persistentFlags.push('FH');
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => wrapper.getByText('Enable Driver'));

        fireEvent.click(wrapper.getByText('Enable Driver'));
        await waitUntilPass(() => expect(device.getAxis(1).driverEnable).toHaveBeenCalled());
      });

      test('enable driver fails', async () => {
        axes[0]._state.persistentFlags.push('FH');
        TestBasicControls.testStore.dispatch(pollingActions.pollNow());
        await waitUntilPass(() => wrapper.getByText('Enable Driver'));

        device.getAxis(1).driverEnable.mockRejectedValueOnce(new RemoteModeException('Cannot enable driver', {
          responseData: 'REMOTE', replyFlag: 'RJ',
        } as CommandFailedExceptionData));
        fireEvent.click(wrapper.getByText('Enable Driver'));
        await waitUntilPass(() => wrapper.getByText(/Enable USB Control/));
      });
    });
  });

  describe('integrated multiaxis device', () => {
    const INITIAL_POS = 310;
    const LIMIT_MIN = 200;
    const LIMIT_MAX = 1000;
    const MAX_SPEED = 100;

    beforeEach(async () => {
      setLinearAxisMockState(1, INITIAL_POS, LIMIT_MIN, LIMIT_MAX, MAX_SPEED);
      setLinearAxisMockState(2, INITIAL_POS, LIMIT_MIN, LIMIT_MAX, MAX_SPEED);

      container.bind<unknown>(UnitInfoService).to(UnitInfoServiceMock);
      const store = TestBasicControls.testStore;
      mockSingleDeviceWithPeripherals(store, {
        axisModifier: axis => {
          if (axis.axisNumber === 1) {
            axis.identity.peripheralId = 70365;
          } else if (axis.axisNumber === 2) {
            axis.identity.peripheralId = 70366;
          }
          return axis;
        },
        peripheralCount: 2,
      });
      const connection = _.sample(selectConnections(TestBasicControls.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(connection.key);
      await waitUntilPass(() => wrapper.getAllByTitle('Home Axis').length > 0);
      // wrapper.getByText(/X-LHM/);
      position = wrapper.getAllByTitle('Linear Axis Position')[0];
      await waitUntilPass(() => {
        wrapper.getAllByText(/20\.00\s+mm/);
        wrapper.getAllByText(/100\.00\s+mm/);
        expect(position.innerHTML).toBe('31');
      });
    });

    describe('findrange hardstop', () => {
      beforeEach(async () => {
        await waitUntilPass(() => {
          expect(position.innerHTML).toBe('31');
        });
        fireEvent.click(wrapper.getAllByTitle('Open Menu')[0]);
        fireEvent.click(wrapper.getByTitle('Set Travel Limits'));
      });

      test('detect travel limits', async () => {
        fireEvent.click(wrapper.getByTitle('Detect Travel Limits'));
        await waitUntilPass(() => {
          expect(axes[0].genericCommand).toHaveBeenCalledWith('tools findrange hardstop save');
          expect(axes[0].settings.get).toHaveBeenCalledWith('limit.min');
          expect(axes[0].settings.get).toHaveBeenCalledWith('limit.max');
        });
      });

      test('detect travel limits fails and modal closes', async () => {
        fireEvent.click(wrapper.getByTitle('Detect Travel Limits'));
        axes[0].genericCommand.mockRejectedValueOnce(new MovementFailedException('Movement has failed', {
          reason: '',
          warnings: [],
          device: 0,
          axis: 0,
        }));
        await waitUntilPass(() => {
          wrapper.getByText(/Movement Failed/);
          expect(wrapper.queryByText(/Detect Travel Limits/)).toBeNull();
        });
      });
    });
  });

  describe('rotary peripheral', () => {
    // The radius of the track svg. Very large in the test since fireEvent.mouseMove rounds clientX and clientY to integers
    // And this is needed to get a good precision
    const RADIUS = 1_000_000;
    TrackCyclic.prototype.getBoundingRect = jest.fn(() => ({ width: RADIUS * 2, height: RADIUS * 2, top: 0, left: 0 } as DOMRect));
    const generateMouseHover = (theta: number, r = RADIUS / 2) => {
      const x = RADIUS + r * Math.sin(theta);
      const y = RADIUS - r * Math.cos(theta);
      fireEvent.mouseMove(wrapper.getByTestId('interactive-cyclic-track'), { clientX: x, clientY: y, button: 0 });
    };

    @injectable()
    class UnitInfoServiceMock extends UnitInfoServiceMockBase {
      public* getSettingUnitInfo(entity: string, setting: string): SagaIterator<UnitInfo> {
        switch (setting) {
          case 'pos': return this.mockUnitInfo('Angle', 1 / STEPS_PER_DEGREE, 1);
          default: return unsupportedUnitInfo();
        }
      }
      public* getCommandUnitInfo(entity: string, command: string): SagaIterator<UnitInfo> {
        switch (command) {
          case 'move sin amplitude': return this.mockUnitInfo('Angle', 1 / STEPS_PER_DEGREE, 1);
          case 'move sin amplitude period': return this.mockUnitInfo('Time', 1 / STEPS_PER_SECOND, 1);
          default: return unsupportedUnitInfo();
        }
      }
    }

    const TOTAL_CYCLES = 2;
    beforeEach(async () => {
      const axisState = getAxisState(1);
      const stepsPerCycle = STEPS_PER_DEGREE * 360;
      axisState.settings.pos = 0;
      axisState.settings['limit.cycle.dist'] = stepsPerCycle;
      axisState.settings['limit.min'] = -stepsPerCycle * TOTAL_CYCLES;
      axisState.settings['limit.max'] = stepsPerCycle * TOTAL_CYCLES;
      axisState.settings.maxspeed = STEPS_PER_DEGREE * 90;
      axisState.settings['motion.index.num'] = 0;
      container.bind<unknown>(UnitInfoService).to(UnitInfoServiceMock);
      const store = TestBasicControls.testStore;
      mockSingleDeviceWithPeripherals(store, { peripheralCount: 1, type: 'rotary' });
      const connection = _.sample(selectConnections(TestBasicControls.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(connection.key);
      await waitUntilPass(() => wrapper.getByTitle('Home Axis'));
      wrapper.getByText(/RSW60A-E03T3/);
      position = wrapper.getByTitle('Rotary Axis Position');
      await waitUntilPass(() => expect(position.innerHTML).toBe('0'));
    });

    test('hover over rotary track', () => {
      generateMouseHover(0);
      wrapper.getByText(/0\.00°/);
      const positionUnit = wrapper.getByTitle('Select Position Units');
      fireEvent.change(positionUnit, { target: { value: Angle.RADIANS } });
      generateMouseHover(0.75 * Math.PI);
      wrapper.getByText(/0.75π/);
    });

    test('Makes the shortest move in either direction', () => {
      generateMouseHover(0.5 * Math.PI);
      wrapper.getByText(/90\.00°/);
      generateMouseHover(1.5 * Math.PI);
      wrapper.getByText(/-90\.00°/);
    });

    test('Will not go further than the end of travel', async () => {
      const axisState = getAxisState(1);
      const endOfTravel = TOTAL_CYCLES * 360;
      const closeToEndOfTravel = endOfTravel - 10;
      const closerToEndOfTravel = endOfTravel - 5;
      const pastEndOfTravel = endOfTravel + 5;
      axisState.settings.pos = closeToEndOfTravel * STEPS_PER_DEGREE;
      const store = TestBasicControls.testStore;
      store.dispatch(pollingActions.pollNow());
      await waitUntilPass(() => expect(position.innerHTML).toBe(`${closeToEndOfTravel}`));
      generateMouseHover((closerToEndOfTravel / 360) * 2 * Math.PI);
      wrapper.getByText(`${closerToEndOfTravel}.00°`);
      generateMouseHover((pastEndOfTravel / 360) * 2 * Math.PI);
      wrapper.getByText(`${pastEndOfTravel - 360}.00°`);
    });

    test('Move to next index position', async () => {
      fireEvent.click(wrapper.getByTitle('Move to Next Position'));
      await waitUntilPass(() => {
        expect(axes[0].genericCommand).toHaveBeenCalledWith('move index next');
      });
    });

    test('Move to previous index position', async () => {
      fireEvent.click(wrapper.getByTitle('Move to Previous Position'));
      await waitUntilPass(() => {
        expect(axes[0].genericCommand).toHaveBeenCalledWith('move index prev');
      });
    });

    test('sinusoidal movement', async () => {
      fireEvent.click(wrapper.getByTitle('Open Menu'));
      fireEvent.click(wrapper.getByTitle('Sinusoidal Motion'));
      fireEvent.change(wrapper.getByTitle('Value of Amplitude'), { target: { value: '2' } });
      fireEvent.change(wrapper.getByTitle('Value of Period'), { target: { value: '3' } });
      fireEvent.change(wrapper.getByTitle('Cycles'), { target: { value: '4' } });
      fireEvent.click(wrapper.getByTitle('Start'));
      await waitUntilPass(() => {
        expect(axes[0].stop).toHaveBeenCalledTimes(1);
        expect(axes[0].moveSin).lastCalledWith(2, Angle.DEGREES, 3, Time.SECONDS, { count: 4, waitUntilIdle: false });
      });
    });
  });

  describe('rotary non-cyclic peripherals', () => {
    const RADIUS = 1_000_000;
    TrackCyclic.prototype.getBoundingRect = jest.fn(() => ({ width: RADIUS * 2, height: RADIUS * 2, top: 0, left: 0 } as DOMRect));

    beforeEach(async () => {
      const axisState = getAxisState(1);
      axisState.settings.pos = 0;
      axisState.settings['limit.cycle.dist'] = 0;
      axisState.settings['limit.min'] = 0;
      axisState.settings['limit.max'] = 20;
      axisState.settings.maxspeed = STEPS_PER_DEGREE * 90;
      axisState.settings['motion.index.num'] = 0;
      const store = TestBasicControls.testStore;
      mockSingleDeviceWithPeripherals(store, { peripheralCount: 1, type: 'rotary' });
      const connection = _.sample(selectConnections(TestBasicControls.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(connection.key);
      await waitUntilPass(() => wrapper.getByTitle('Home Axis'));
      wrapper.getByText(/RSW60A-E03T3/);
      position = wrapper.getByTitle('Linear Axis Position');
      await waitUntilPass(() => expect(position.innerHTML).toBe('0'));
    });

    test('show the axis as linear', async () => {
      wrapper.getAllByText(/20\.00/);
      expect(wrapper.queryByTestId('interactive-linear-track')).not.toBeNull();
      expect(wrapper.queryByTestId('interactive-cyclic-track')).toBeNull();
    });
  });

  describe('device with IO', () => {
    beforeEach(async () => {
      mockIo = { ai: [1.234, 2.222], ao: [4.8], di: [true, false], do: [true, false] };
      mockLabelsAll = [
        { portType: IoPortType.ANALOG_INPUT, channelNumber: 1, label: 'Lorem Ipsum' },
        { portType: IoPortType.ANALOG_INPUT, channelNumber: 2, label: 'High lorem' },
        { portType: IoPortType.ANALOG_INPUT, channelNumber: 3, label: 'Asdf' },
        { portType: IoPortType.ANALOG_OUTPUT, channelNumber: 1, label: 'Always Off' },
        { portType: IoPortType.ANALOG_OUTPUT, channelNumber: 2, label: '3' },
        { portType: IoPortType.DIGITAL_INPUT, channelNumber: 1, label: 'b' },
        { portType: IoPortType.DIGITAL_INPUT, channelNumber: 2, label: 'ai label 123' },
        { portType: IoPortType.DIGITAL_OUTPUT, channelNumber: 1, label: 'Water' },
      ];

      const store = TestBasicControls.testStore;
      mockSingleDevice(store);
      mockSingleDeviceWithPeripherals(store, { peripheralCount: 0 });
      const connection = _.sample(selectConnections(TestBasicControls.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(connection.key);
      await waitUntilPass(() => wrapper.getByText(/X-MCC0/));
      await waitUntilPass(() => wrapper.getByTitle('Open Menu'));
      fireEvent.click(wrapper.getByTitle('Open Menu'));
      fireEvent.click(wrapper.getByTitle('Expand Io Section'));
    });

    test('digital in', async () => {
      await waitUntilPass(() => {
        wrapper.getByTitle('Digital Input 1 On');
        wrapper.getByTitle('Digital Input 2 Off');
      });
      mockIo.di = [false, true];
      TestBasicControls.testStore.dispatch(pollingActions.pollNow());
      await waitUntilPass(() => {
        wrapper.getByTitle('Digital Input 1 Off');
        wrapper.getByTitle('Digital Input 2 On');
      });
    });

    test('digital out', async () => {
      await waitUntilPass(() => {
        wrapper.getByTitle('Turn Digital Output 1 Off');
        wrapper.getByTitle('Turn Digital Output 2 On');
      });
      fireEvent.click(wrapper.getByTitle('Turn Digital Output 1 Off'));
      wrapper.getByTitle('Digital Output 1 Loading...');
      await waitUntilPass(() => {
        wrapper.getByTitle('Turn Digital Output 1 On');
      });
    });

    test('analog in', async () => {
      await waitUntilPass(() => {
        wrapper.getByText(/1.23/);
        wrapper.getByText(/2.22/);
      });
      mockIo.ai = [3.333, 4.321];
      TestBasicControls.testStore.dispatch(pollingActions.pollNow());
      await waitUntilPass(() => {
        wrapper.getByText(/3.33/);
        wrapper.getByText(/4.32/);
      });
    });

    test('analog out', async () => {
      await waitUntilPass(() => {
        const input = wrapper.getByTitle('Analog Output 1') as HTMLInputElement;
        expect(input.value).toBe('4.8');
      });
      fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'Set Analog Output 1' }));
      await waitUntilPass(() => {
        fireEvent.change(wrapper.getByRole('spinbutton', { name: 'Set Analog Output 1' }), { target: { value: '3.2' } });
        fireEvent.click(wrapper.getByText('Save'));
      });
      await waitUntilPass(() => expect(device.io.setAnalogOutput).toHaveBeenCalledWith(1, 3.2));
      TestBasicControls.testStore.dispatch(pollingActions.pollNow());
      await waitUntilPass(() => {
        const input = wrapper.getByRole('spinbutton', { name: 'Set Analog Output 1' }) as HTMLInputElement;
        expect(input.value).toBe('3.2');
      });
    });

    test('io errors are displayed', async () => {
      await waitUntilPass(() => {
        const input = wrapper.getByTitle('Analog Output 1') as HTMLInputElement;
        expect(input.value).toBe('4.8');
      });
      const aoException = new CommandFailedException('Setting IO Failed', { responseData: 'BADDATA' } as CommandFailedExceptionData);
      await waitUntilPass(() => device.io.setAnalogOutput.mockRejectedValueOnce(aoException));
      fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'Set Analog Output 1' }));
      await waitUntilPass(() => {
        fireEvent.change(wrapper.getByRole('spinbutton', { name: 'Set Analog Output 1' }), { target: { value: '300000' } });
      });
      fireEvent.click(wrapper.getByText('Save'));
      await waitUntilPass(() => wrapper.getByText('Failed to set Analog IO 1'));
    });

    test('labels are displayed', async () => {
      await waitUntilPass(() => {
        wrapper.getByText('Always Off');
        wrapper.getByText('ai label 123');
        wrapper.getByText('Water');
      });
    });

    test('labels can be edited on pop-up page', async () => {
      fireEvent.click(wrapper.getByTitle('Edit Labels'));
      await waitUntilPass(() => {
        wrapper.getByText('Lorem Ipsum');
        wrapper.getByText('Always Off');
        wrapper.getByText('ai label 123');
        wrapper.getByText('Water');
      });
      await waitUntilPass(() => {
        fireEvent.change(wrapper.getByLabelText(/ai-1/), { target: { value: 'Hello' } });
        fireEvent.click(wrapper.getByText('Save'));
      });
      await waitUntilPass(() => {
        expect(device.io.setLabel).toHaveBeenCalledWith(1, 1, 'Hello');
      });
      TestBasicControls.testStore.dispatch(pollingActions.pollNow());
      await waitUntilPass(() => {
        wrapper.getByTitle('Open Menu');
        fireEvent.click(wrapper.getByTitle('Open Menu'));
        wrapper.getByTitle('Edit Labels');
        wrapper.getByText('Hello');
      });
    });

    test('cancelling label edit won\'t change the label', async () => {
      fireEvent.click(wrapper.getByTitle('Edit Labels'));
      await waitUntilPass(() => {
        fireEvent.change(wrapper.getByLabelText(/ai-1/), { target: { value: 'Hello World' } });
        fireEvent.click(wrapper.getByText('Cancel'));
      });
      await waitUntilPass(() => {
        expect(device.io.setLabel).not.toHaveBeenCalled();
      });
      await waitUntilPass(() => {
        wrapper.getByText('Lorem Ipsum');
        wrapper.getByText('Always Off');
        wrapper.getByText('ai label 123');
        wrapper.getByText('Water');
      });
    });
  });

  describe('device with a lockstep group', () => {
    beforeEach(async () => {
      setLinearAxisMockState(1, 0, 0, 1000, 100);
      setLinearAxisMockState(2, 0, 0, 1000, 100);
      container.bind<unknown>(UnitInfoService).to(UnitInfoServiceMock);

      mockSingleDeviceWithPeripherals(TestBasicControls.testStore, {
        modifier: device => {
          device.locksteps = [{
            groupNumber: 1,
            axisNumbers: [1, 2],
          }];
          return device;
        },
        peripheralCount: 2,
        deviceNumber: 1,
      });
      const connection = _.sample(selectConnections(TestBasicControls.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(connection.key);

      await waitUntilPass(() => wrapper.getByText(/X-MCC2/));
      await waitUntilPass(() => wrapper.getByTitle('Open Menu'));
    });

    test('renders lockstep groups', () => {
      wrapper.getByText(/Lockstep Group 1/);
      wrapper.getByText(/Axis 1/);
    });

    test('moves lockstep group when clicked on buttons', async () => {
      await waitUntilPass(() => {
        expect(wrapper.getByTitle('Move to Beginning')).not.toHaveClass('disabled');
        expect(wrapper.getByTitle('Move to End')).not.toHaveClass('disabled');
        position = wrapper.getByTitle('Linear Axis Position');
      });

      fireEvent.click(wrapper.getByTitle('Home Axis'));
      await waitUntilPass(() => {
        expect(device.lockstep.home).toHaveBeenCalled();
      });

      fireEvent.click(wrapper.getByTitle('Move to Beginning'));
      await waitUntilPass(() => {
        expect(device.lockstep.moveAbsolute).toHaveBeenCalledWith(0, undefined);
      });
      TestBasicControls.testStore.dispatch(pollingActions.pollNow());
      await waitUntilPass(() => expect(position.innerHTML).toBe('0'));

      fireEvent.click(wrapper.getByTitle('Move to End'));
      await waitUntilPass(() => {
        expect(device.lockstep.moveAbsolute).toHaveBeenCalledWith(701, undefined);
      });
      TestBasicControls.testStore.dispatch(pollingActions.pollNow());
      await waitUntilPass(() => expect(position.innerHTML).toBe('70.1'));

      fireEvent.click(wrapper.getByTitle('Stop Axis'));
      await waitUntilPass(() => {
        expect(device.lockstep.stop).toHaveBeenCalled();
      });
    });

    test('sinusoidal movement', async () => {
      fireEvent.click(wrapper.getByTitle('Open Menu'));
      fireEvent.click(wrapper.getByTitle('Sinusoidal Motion'));
      fireEvent.change(wrapper.getByTitle('Value of Amplitude'), { target: { value: '2' } });
      fireEvent.change(wrapper.getByTitle('Value of Period'), { target: { value: '3' } });
      fireEvent.change(wrapper.getByTitle('Cycles'), { target: { value: '4' } });
      fireEvent.click(wrapper.getByTitle('Start'));
      await waitUntilPass(() => {
        expect(device.lockstep.stop).toHaveBeenCalledTimes(1);
        expect(device.lockstep.moveSin).lastCalledWith(2, Length.MILLIMETRES, 3, Time.SECONDS, { count: 4, waitUntilIdle: false });
      });
    });

    test('show 2 lockstepped axis images', async () => {
      await waitUntilPass(() => {
        expect(wrapper.getByTitle('Linear stage peripheral (Primary Lockstep Axis)'));
        expect(wrapper.getByTitle('Linear stage peripheral (Secondary Lockstep Axis 1)'));
      });
    });

    test('disable all drivers in lockstep group', async () => {
      fireEvent.click(wrapper.getByTitle('Open Menu'));
      fireEvent.click(wrapper.getByTitle('Disable Driver'));
      await waitUntilPass(() => {
        expect(device.getAxis(1).driverDisable).toHaveBeenCalled();
        expect(device.getAxis(2).driverDisable).toHaveBeenCalled();
      });
    });

    test('enable all drivers in lockstep group', async () => {
      axes[0]._state.persistentFlags.push('FO');
      TestBasicControls.testStore.dispatch(pollingActions.pollNow());
      await waitUntilPass(() => wrapper.getByText('Enable Driver'));
      fireEvent.click(wrapper.getByText('Enable Driver'));
      await waitUntilPass(() => {
        expect(device.getAxis(1).driverEnable).toHaveBeenCalled();
        expect(device.getAxis(2).driverEnable).toHaveBeenCalled();
      });
    });
  });
});
