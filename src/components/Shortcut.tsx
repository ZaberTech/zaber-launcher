import { environment } from '../environment';

import { useKeyboard } from './hooks';

function isGlobalEvent(e: KeyboardEvent) {
  return (e.target as Element)?.nodeName === 'BODY';
}

function matchEvent({ k, ctrl = false, alt = false, shift = false }: Props, event: KeyboardEvent) {
  return k.toLowerCase() === event.key.toLowerCase()
    && (ctrl === 'ignore' || ctrl === (environment.platform === 'darwin' ? event.metaKey : event.ctrlKey))
    && (shift === 'ignore' || shift === event.shiftKey)
    && (alt === 'ignore' || alt === event.altKey);
}

interface Props {
  /** The key to capture. */
  k: string;
  /** Fires when the given key is pressed down. */
  onDown?: (e: KeyboardEvent) => void;
  /** Fires when the given key is released. Fires even when disabled. */
  onUp?: (e: KeyboardEvent) => void;
  disabled?: boolean;
  ctrl?: boolean | 'ignore';
  shift?: boolean | 'ignore';
  alt?: boolean | 'ignore';
  /** onDown keeps firing repeatedly as long as the user holds the key. */
  repeatable?: boolean;
  preventDefault?: boolean;
}

export const Shortcut: React.FC<Props> = props => {
  const { onUp, onDown, disabled, repeatable, preventDefault } = props;

  useKeyboard(e => {
    if (!isGlobalEvent(e) || disabled) { return }

    if (matchEvent(props, e)) {
      if (preventDefault) {
        e.preventDefault();
      }

      if (environment.platform === 'darwin' && e.metaKey) {
        e = new KeyboardEvent(e.type, { ...e, ctrlKey: true });
      }

      if (!e.repeat || repeatable) {
        onDown?.(e);
      }
    }
  }, e => {
    if (!isGlobalEvent(e)) {
      return;
    }

    if (matchEvent(props, e)) {
      if (preventDefault) {
        e.preventDefault();
      }

      if (environment.platform === 'darwin' && e.metaKey) {
        e = new KeyboardEvent(e.type, { ...e, ctrlKey: true });
      }

      onUp?.(e);
    }
  });

  return null;
};
