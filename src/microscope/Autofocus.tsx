import React from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, Text, Button, Toggle, Icons, HelpTooltip } from '@zaber/react-library';

import { useActions } from '../utils';
import { Editions, environment } from '../environment';
import { Shortcut } from '../components';

import { selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations } from './types';
import { useOperationState, useOperationValue } from './hooks';
import { Indicator, OperationError } from './components';
import { AutofocusSettings } from './AutofocusSettings';
import { ExternalSoftwareWarnings, Limit } from './AutofocusComponents';


export const Autofocus: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.autofocus;
  const info = monitor.info?.autofocus;

  const [inFocus, setInFocus, zeroInProgress] = useOperationValue(info?.inFocus ?? false, Operations.autofocusZero);
  const [laserOn, setLaserOn, settingLaser] = useOperationValue(info?.laserOn ?? true, Operations.autofocusLaserOnOff);

  const [onceInProgress] = useOperationState(Operations.autofocusFocusOnce);
  const [loopInProgress] = useOperationState(Operations.autofocusFocusLoop);
  const [stopInProgress] = useOperationState(Operations.autofocusStop);

  const disabled = zeroInProgress || onceInProgress || loopInProgress || !laserOn;
  const disabledOutOfRange = disabled || !info?.inRange;

  if (stableInfo == null || info == null) {
    return null;
  }

  const setZero = () => {
    setInFocus(true);
    actions.operationStart(Operations.autofocusZero);
  };
  const focusOnce = () => actions.operationStart(Operations.autofocusFocusOnce, { });
  const focusLoop = () => actions.operationStart(Operations.autofocusFocusLoop);
  const stop = () => actions.operationStart(Operations.autofocusStop);
  const scan = () => actions.operationStart(Operations.autofocusFocusOnce, { scan: true });

  return <HeaderCard
    className="controls autofocus"
    header={<>
      <Text t={Text.Type.H4}>Autofocus</Text>
      <div className="laser-control">
        <Text>Laser</Text>
        <Toggle
          value={laserOn}
          onValueChange={value => {
            setLaserOn(value);
            actions.operationStart(Operations.autofocusLaserOnOff, { on: value });
          }}
          loading={settingLaser}/>
      </div>
      <div className="icons">
        <AutofocusSettings/>
        <HelpTooltip>
          Autofocus is a feature that automatically adjusts the focus of the microscope to keep the sample in focus.
          First, put the sample in focus manually, then press "Set Focus" to set the reference focus.
          Adjust the limits to prevent the objective from accidentally touching the sample.<br/>
          Press "Focus Once" to later focus the sample, or "Hold Focus" to keep the sample in focus continuously.<br/>
          Use shortcuts Ctrl+F to set focus, and F to focus once.
        </HelpTooltip>
      </div>
    </>}>
    <div className="row">
      <Button disabled={disabledOutOfRange} color="grey"
        onClick={focusOnce}>
        Focus Once
      </Button>
      <Shortcut disabled={disabledOutOfRange} k="F" onDown={focusOnce}/>

      <Button disabled={disabledOutOfRange} color="grey"
        onClick={focusLoop}>
        Hold Focus
      </Button>

      {(onceInProgress || loopInProgress) && <Button
        disabled={stopInProgress} className="stop"
        onClick={stop}>
        <Icons.Stop/> Stop
      </Button>}

      {onceInProgress && <Text>Focusing...</Text>}
      {loopInProgress && <Text>Maintaining focus...</Text>}

      <Button onClick={setZero} disabled={disabledOutOfRange} color="grey" className="set-zero">
        Set Focus
      </Button>
      <Shortcut disabled={disabledOutOfRange} k="F" ctrl onDown={setZero}/>
    </div>

    <div className="limit-table">
      <Text>In Focus</Text>
      <Text>In Range</Text>
      <Text className="limit-label">Limit Min</Text>
      <Text className="limit-label">Limit Max</Text>
      <Indicator titles={['Out of Focus', 'In Focus']} disabled={!laserOn} value={inFocus}/>
      <Indicator titles={['Out of Range', 'In Range']} disabled={!laserOn} value={info.inRange}/>
      <Limit limit="min" info={info}/>
      <Limit limit="max" info={info}/>
    </div>

    {environment.edition !== Editions.Public && <div className="row">
      <Button disabled={disabled} color="grey"
        onClick={scan}>
        Scan (Internal only)
      </Button>
    </div>}

    <ExternalSoftwareWarnings/>

    {[
      Operations.autofocusZero,
      Operations.autofocusFocusOnce,
      Operations.autofocusFocusLoop,
      Operations.autofocusStop,
      Operations.autofocusSetLimit,
      Operations.autofocusLaserOnOff,
    ].map(op => (
      <OperationError key={op} operation={op}/>
    ))}
  </HeaderCard>;
};
