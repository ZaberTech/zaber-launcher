import { createSelector } from 'reselect';

import { environment, Editions } from '../environment';
import { selectFeedback } from '../store';

export const selectModalState = createSelector(selectFeedback, feedback => feedback.modalState);

export const selectAnonymous = createSelector(selectFeedback, feedback => feedback.anonymous);
export const selectText = createSelector(selectFeedback, feedback => feedback.text);
export const selectError = createSelector(selectFeedback, feedback => feedback.sendError);
export const selectEmail = createSelector(selectFeedback, selectAnonymous, (feedback, anonymous) => {
  if (!anonymous || (environment.edition === Editions.Internal)) {
    return feedback.email;
  }
  return;
});
