import type { SagaIterator, Task } from 'redux-saga';
import { all, call, delay, join, put, race, select, take, fork, cancel } from 'redux-saga/effects';
import type { Device, TriggerEnabledState } from '@zaber/motion/ascii';
import _ from 'lodash';
import { match, P } from 'ts-pattern';
import { ConnectionClosedException, ConnectionFailedException } from '@zaber/motion';

import type { Logger } from '../app_components';
import { getDevice, getDeviceOrAxis } from '../connection_manager';
import { safeSpawn, RT, throwUnexpectedError, Action, takeLeadingUniq } from '../utils';
import { getLogger } from '../log';
import type { EntityKey } from '../keys';

import { actionDefinitions as actions, ActionsToPayloads, ActionTypes } from './actions';
import { ActivePollGetter, selectActivePolls, selectResults } from './selectors';
import { Polled, PollResultSuccess, keyGenerators, PollResult } from './types';
import type { State } from './reducer';

const FAST_POLL_DELAY_MS = 100;
const FAST_POLLS_PER_SLOW_POLL = 10;
const CONNECTION_FAILED_DELAY_MS = 1000;

let logger: Logger;
export function* saga(): SagaIterator {
  logger = getLogger('Poller');

  const loop: Task = yield safeSpawn(pollLoopManager);
  yield all([
    takeLeadingUniq(ActionTypes.CLEAR_FLAGS, ({ payload: { deviceOrAxisKey } }) => deviceOrAxisKey, clearFlags),
    join(loop),
  ]);
}

const POLL_STARTERS: Record<Polled, ActionTypes> = {
  settings: ActionTypes.START_POLLING_SETTING,
  analogIo: ActionTypes.START_POLLING_ANALOG_IO,
  warnings: ActionTypes.START_POLLING_FLAGS,
  triggers: ActionTypes.START_POLLING_TRIGGERS,
};

function* pollLoopManager(): SagaIterator {
  while (true) {
    yield race([
      call(pollLoop),
      take(ActionTypes.POLL_NOW),
      ...Object.values(POLL_STARTERS).map(action => take(action)),
    ]);
  }
}

type DeviceRequests = RT<typeof selectActivePolls>[string];

const initialPollResult: PollResultSuccess = { type: 'success', settings: {}, analogIo: {}, warnings: {}, triggers: {} };

/** A map from poll type to a function that will poll the given device and return new polling results */
type PollerMap = {
  [P in Polled]: (device: Device, requests: ActivePollGetter<P>[], prevResults: PollResultSuccess[P]) => Promise<PollResultSuccess[P]>
};
const pollers: PollerMap = {
  settings: async (device, requests) => {
    const gets = requests.map(p => p.getter);
    const results = await device.settings.getMany(...gets);
    return Object.fromEntries(gets.map((get, i) => [keyGenerators.settings(get), results[i]]));
  },
  analogIo: async (device, requests) => {
    const [inGetters, outGetters] = _.partition(requests, g => g.getter.type === 'in');
    const inResults = await match<ActivePollGetter<'analogIo'>[], Promise<PollResultSuccess['analogIo']>>(inGetters)
      .with([], async () => ({}))
      .with([P._], async ([singleGetter]) => {
        const result = await device.io.getAnalogInput(singleGetter.getter.channel);
        return { [keyGenerators.analogIo(singleGetter.getter)]: result };
      })
      .otherwise(async () => {
        const results = await device.io.getAllAnalogInputs();
        return Object.fromEntries(inGetters.map(getter => [keyGenerators.analogIo(getter.getter), results[getter.getter.channel - 1]]));
      });
    const outResults = await match<ActivePollGetter<'analogIo'>[], Promise<PollResultSuccess['analogIo']>>(outGetters)
      .with([], async () => ({}))
      .with([P._], async ([singleGetter]) => {
        const result = await device.io.getAnalogOutput(singleGetter.getter.channel);
        return { [keyGenerators.analogIo(singleGetter.getter)]: result };
      })
      .otherwise(async () => {
        const results = await device.io.getAllAnalogOutputs();
        return Object.fromEntries(outGetters.map(getter => [keyGenerators.analogIo(getter.getter), results[getter.getter.channel - 1]]));
      });
    return { ...inResults, ...outResults };
  },
  warnings: async (device, requests, prevResults) => {
    const gets = requests.map(p => p.getter);
    const getDeviceFlags  = gets.some(g => g.axisNumber === 0);
    const getAxesFlags = gets.filter(g => g.axisNumber !== 0);
    const newResults: PollResultSuccess['warnings'] = {};
    /** Axes need to be checked one by one if there are existing errors. Otherwise, first see if there are errors on the device. */
    const alwaysCheckAxes = getAxesFlags.length === 1 || Object.keys(prevResults).length > 0;
    const deviceWarnings: RT<typeof device.warnings.getFlags> = getDeviceFlags || !alwaysCheckAxes ?
      await device.warnings.getFlags() :
      new Set();
    if (getDeviceFlags) {
      newResults['0'] = [...deviceWarnings];
    }
    if (alwaysCheckAxes || deviceWarnings.size > 0) {
      for (const getter of getAxesFlags) {
        const flags = await device.getAxis(getter.axisNumber).warnings.getFlags();
        newResults[keyGenerators.warnings(getter)] = [...flags];
      }
    }
    return { ...newResults };
  },
  triggers: async device => {
    const result: TriggerEnabledState[] = await device.triggers.getEnabledStates();
    return { all: result };
  },
};

/**
 * re-polls anything that should be polled on this run, and returns a combination of these results and results from the previous run
 * @param polled The type of thing to poll
 * @param device The device to poll
 * @param requests The polling requests for this device
 * @param prevResults The results from the previous run
 * @param fast True if this is a fast poll
 * @returns The new results added to the previous results
 */
async function getPollerResults<P extends Polled>(
  polled: P, device: Device, requests: DeviceRequests, prevResults: PollResultSuccess, fast: boolean
): Promise<PollResultSuccess[P]> {
  // TODO ZL-1015: This coercion is needed so the filter knows the possible types. This is fixed in ts > 5.2
  const allGetters = requests[polled] as ActivePollGetter<P>[];
  const getters = fast ? allGetters.filter(p => p.fast) : allGetters;
  const retainedResults = fast ? prevResults[polled] : {};
  if (getters.length === 0) { return { ...retainedResults } }
  const newResults = await pollers[polled](device, getters, prevResults[polled]);
  return { ...retainedResults, ...newResults };
}

function* pollDevice(
  deviceKey: string, isFast: boolean, requests: DeviceRequests, prevResults: PollResult | undefined
): SagaIterator<PollResult> {
  const prevResultsOrInitial = prevResults?.type === 'success' ? prevResults : initialPollResult;
  try {
    const device: RT<typeof getDevice> = yield call(getDevice, deviceKey);
    const result: PollResult = {
      type: 'success',
      settings: yield call(getPollerResults, 'settings', device, requests, prevResultsOrInitial, isFast),
      analogIo: yield call(getPollerResults, 'analogIo', device, requests, prevResultsOrInitial, isFast),
      warnings: yield call(getPollerResults, 'warnings', device, requests, prevResultsOrInitial, isFast),
      triggers: yield call(getPollerResults, 'triggers', device, requests, prevResultsOrInitial, isFast),
    };
    return result;
  } catch (e) {
    throwUnexpectedError(e);
    if (e instanceof ConnectionFailedException || e instanceof ConnectionClosedException) {
      yield delay(CONNECTION_FAILED_DELAY_MS);
    }
    logger.warn(`Could not poll device "${deviceKey}": ${e.message}`);
    return { type: 'error', message: 'Getting information from this product failed', details: e.message };
  }
}

function* pollLoop() {
  let firstPoll = true;
  const activePolls: Partial<Record<EntityKey, { task: Task; isFast: boolean }>> = {};
  for (let pollCount = 0; true; pollCount = (pollCount + 1) % FAST_POLLS_PER_SLOW_POLL) {
    const isFast = pollCount !== 0;
    const requests: RT<typeof selectActivePolls> = yield select(selectActivePolls);
    const prevResults: RT<typeof selectResults> = yield select(selectResults);
    for (const [deviceKey, deviceRequests] of Object.entries(requests)) {
      const activePoll = activePolls[deviceKey];
      const prevDeviceResults = prevResults[deviceKey];
      if (activePoll == null) {
        activePolls[deviceKey] = { task: yield fork(pollDevice, deviceKey, isFast, deviceRequests, prevDeviceResults), isFast };
      } else if (!isFast && activePoll.isFast) {
        yield cancel(activePoll.task);
        activePolls[deviceKey] = { task: yield fork(pollDevice, deviceKey, isFast, deviceRequests, prevDeviceResults), isFast };
      }
    }

    yield delay(FAST_POLL_DELAY_MS);

    const newResults: State['results'] = {};
    for (const deviceKey of Object.keys(requests)) {
      const activePoll = activePolls[deviceKey];
      const result = activePoll?.task.result<PollResult>();
      if (result != null) {
        newResults[deviceKey] = result;
        delete activePolls[deviceKey];
      } else {
        newResults[deviceKey] = prevResults[deviceKey];
      }
    }
    if (!_.isEqual(newResults, prevResults) || firstPoll) {
      yield put(actions.receiveResults(newResults));
    }

    firstPoll = false;
  }
}

function* clearFlags({ payload: { deviceOrAxisKey } }: Action<ActionsToPayloads[ActionTypes.CLEAR_FLAGS]>) {
  try {
    const asciiDevice: RT<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, deviceOrAxisKey);
    yield call([asciiDevice.warnings, asciiDevice.warnings.clearFlags]);
  } catch (e) {
    throwUnexpectedError(e);
    logger.warn(`Could not clear flags for device "${deviceOrAxisKey}": ${e.message}`);
  } finally {
    yield put(actions.pollNow());
  }
}
