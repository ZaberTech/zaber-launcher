import { Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import { tryAccess } from '@zaber/toolbox';
import React, { useLayoutEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import type { EntityKey } from '../keys';
import { ConnectionType } from '../app_components';

import { actions as actionsDefinition } from './actions';
import { selectConnections } from './selectors';

interface Props {
  connectionKey: EntityKey;
  onDisconnect?: () => void;
}

export const DISPLAY_DELAY = 500;

export const DisconnectButton: React.FC<Props> = ({ connectionKey, onDisconnect }) => {
  const actions = useActions(actionsDefinition);
  const status = tryAccess(useSelector(selectConnections), connectionKey);
  const connected = status?.connected ?? false;

  // delayed displaying avoids flickering
  const [display, setDisplay] = useState(connected);
  useLayoutEffect(() => {
    if (!connected) {
      setDisplay(false);
      return;
    }
    const timeout = setTimeout(() => setDisplay(true), DISPLAY_DELAY);
    return () => clearTimeout(timeout);
  }, [connected]);

  if (!connected || !display) {
    return null;
  }
  const title = status?.connection?.type === ConnectionType.SERIAL_PORT ? 'Close Serial Port' : 'Disconnect';
  return <Icons.Disconnect title={title} onClick={() => {
    actions.routedConnectionDisconnect(connectionKey);
    onDisconnect?.();
  }}/>;
};
