import type { SagaIterator } from 'redux-saga';
import { all, takeEvery, put, call, race, take, select, delay } from 'redux-saga/effects';
import {
  ascii, MovementInterruptedException, Length, Angle, Velocity, AngularVelocity, CommandFailedException, MovementFailedException, Units,
} from '@zaber/motion';

import { Action, throwUnexpectedError, tryAccess, SagaIter, RT } from '../utils';
import { getAxis, getDevice } from '../connection_manager';
import type { Logger } from '../app_components';
import { getLogger } from '../log';
import type { EntityKey } from '../keys';
import { getUnits } from '../units';
import { DEFAULT_POLL_INTERVAL } from '../types';

import { ActionTypes, actions, ActionsToPayloads } from './actions';
import { MovementType, AxisErrorType, warningFlagToErrorType, AxisError } from './types';
import { selectVisibleAxes, AxisStateComplete, selectAxes } from './selectors';

let logger: Logger;

export function* basicMovementSaga(): SagaIterator {
  logger = getLogger('basicMovementSaga');

  yield all([
    takeEvery(ActionTypes.MOVE_AXIS, moveAxis),
    takeEvery(ActionTypes.MOVE_ALL_AXES, moveAllAxes),
    takeEvery(ActionTypes.SET_DRIVER, setDriver),
    takeEvery(ActionTypes.DID_MOUNT, didMount),
  ]);
}

function* getAxisState(axisKey: EntityKey): SagaIter<ReturnType<typeof selectAxes>[string] | undefined> {
  return tryAccess((yield select(selectAxes)) as ReturnType<typeof selectAxes>, axisKey);
}

function* didMount(): SagaIterator {
  yield race({
    monitor: call(monitorDevices),
    cancel: take(ActionTypes.WILL_UNMOUNT),
  });
}

function* monitorDevices(): SagaIterator {
  while (true) {
    const axes: ReturnType<typeof selectVisibleAxes> = yield select(selectVisibleAxes);
    yield all(axes.map(axis => call(monitorQueryAxis, axis)));

    yield race({
      wait: delay(DEFAULT_POLL_INTERVAL),
      refresh: take([ActionTypes.REFRESH_VISIBLE_AXES, ActionTypes.SELECT_KEY]),
    });
  }
}

export function* queryTravelInfo({ settings }: ascii.Axis) {
  const min: number = yield call([settings, settings.get], 'limit.min');
  const max: number = yield call([settings, settings.get], 'limit.max');
  const maxSpeedNative: number = yield call([settings, settings.get], 'maxspeed');
  let cycleDistance = 0;
  try {
    cycleDistance = yield call([settings, settings.get], 'limit.cycle.dist');
  } catch (err) {
    if (!(err instanceof CommandFailedException)) {
      throw err;
    }
  }
  const isCyclic = cycleDistance > 0;
  const distance = isCyclic ? cycleDistance : max - min;
  return {
    min,
    max,
    cycleDistance: isCyclic ? cycleDistance : 'not-cyclic' as const,
    distance,
    maxSpeed: maxSpeedNative,
  };
}

function* putTravelInfo(axis: AxisStateComplete, asciiAxis: ascii.Axis): SagaIterator {
  const travelInfo: RT<typeof queryTravelInfo> = yield call(queryTravelInfo, asciiAxis);
  yield put(actions.axisMonitoringData({
    axis: axis.key,
    travelInfo,
  }));
}

function* queryPosition(axis: AxisStateComplete, asciiAxis: ascii.Axis): SagaIterator {
  const units = getUnits(axis.position) as Length | Angle;
  const position : number = yield call([asciiAxis, asciiAxis.getPosition], units);
  yield put(actions.axisMonitoringData({
    axis: axis.key,
    position: { value: position, units: axis.position.units },
  }));
}

function* queryWarningFlags(axis: AxisStateComplete, asciiAxis: ascii.Axis): SagaIter {
  const warnings: RT<typeof getWarningFlags> = yield getWarningFlags(asciiAxis, axis);
  if (axis.ignoreInterruptFlag) {
    if (!warnings.delete(ascii.WarningFlags.MOVEMENT_INTERRUPTED)) {
      yield put(actions.clearIgnoreInterrupt(axis.key));
    }
  }
  const flagErrors = axis.errors.filter(err => err.warningFlag);
  const flagsUnchanged = warnings.size === flagErrors.length && flagErrors.every(err => warnings.has(err.warningFlag!));
  if (flagsUnchanged) {
    return;
  }

  const flagsArray = Array.from(warnings.values());
  yield put(actions.addWarningFlags(axis.key, flagsArray));

  const errors: AxisError[] = flagsArray.map(warningFlag => ({
    type: warningFlagToErrorType(warningFlag),
    warningFlag,
    message: `Warning flag: ${warningFlag}`,
    origin: 'warning_flag',
  }));
  yield put(actions.updateErrors(axis.key, 'warning_flag', errors));
}

function* getWarningFlags(asciiAxis: ascii.Axis, axis: AxisStateComplete): SagaIter<Set<string>> {
  let warnings: RT<typeof asciiAxis.warnings.getFlags>;
  if (axis.lockstep) {
    warnings = new Set();
    for (const axisNumber of axis.lockstep.axisNumbers) {
      const asciiWarnings = asciiAxis.device.getAxis(axisNumber).warnings;
      const axisWarnings: RT<typeof asciiWarnings.getFlags> = yield call([asciiWarnings, asciiWarnings.getFlags]);
      for (const flag of axisWarnings) {
        warnings.add(flag);
      }
    }
  } else {
    warnings = yield call([asciiAxis.warnings, asciiAxis.warnings.getFlags]);
  }
  return warnings;
}

function* monitorQueryAxis(axis: AxisStateComplete): SagaIterator {
  try {
    const asciiAxis: ascii.Axis = yield call(getAxis, axis.key);
    if (!axis.travelInfo) {
      yield call(putTravelInfo, axis, asciiAxis);
    }

    yield call(queryPosition, axis, asciiAxis);

    yield call(queryWarningFlags, axis, asciiAxis);

    if (axis.errors.some(err => err.origin === 'connection')) {
      yield put(actions.updateErrors(axis.key, 'connection'));
    }
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.updateErrors(axis.key, 'connection', [{ type: AxisErrorType.Fault, message: err.message, origin: 'connection' }]));
    logger.warn('monitorQueryAxis failed', err);
  }
}

function* moveAllAxes(
  { payload: { moveType, initiator } }: Action<ActionsToPayloads[ActionTypes.MOVE_ALL_AXES]>
): SagaIterator {
  const axes: ReturnType<typeof selectVisibleAxes> = yield select(selectVisibleAxes);
  if (axes.length === 0) {
    return;
  }

  for (const axis of axes) {
    yield put(actions.moveAxis(axis.key, moveType, initiator, axis.lockstep?.groupNumber));
  }
}

function defaultMovementError(err: Error): AxisError[] {
  return [{ type: AxisErrorType.Fault, message: err.message ?? String(err), origin: 'movement' }];
}

function* moveAxis(
  { payload: { axis, moveType, position, units = Units.NATIVE } }: Action<ActionsToPayloads[ActionTypes.MOVE_AXIS]>
): SagaIter {
  const axisState: RT<typeof getAxisState> = yield getAxisState(axis);
  if (axisState == null) { throw new Error('axisState == null') }

  try {
    const asciiAxis: ascii.Axis = yield call(getAxis, axis);
    const lockstepGroup = axisState.lockstep?.groupNumber;
    const moveable = lockstepGroup ? asciiAxis.device.getLockstep(lockstepGroup) : asciiAxis;

    // TODO: replace with waitUntilReady() with no timeout
    for (const axisNumber of axisState.lockstep?.axisNumbers ?? [asciiAxis.axisNumber]) {
      yield asciiAxis.device.getAxis(axisNumber).warnings.clearFlags();
    }

    switch (moveType) {
      case MovementType.Home:
        yield call([moveable, moveable.home]);
        break;
      case MovementType.Min:
        yield call([moveable, moveable.moveMin]);
        break;
      case MovementType.Max:
        yield call([moveable, moveable.moveMax]);
        break;
      case MovementType.Stop:
        yield call([moveable, moveable.stop]);
        break;
      case MovementType.Absolute:
        yield call([moveable, moveable.moveAbsolute], position!, units as Length | Angle | '');
        break;
      case MovementType.Velocity:
        yield call([moveable, moveable.moveVelocity], position!, units as Velocity | AngularVelocity);
        yield call([moveable, moveable.waitUntilIdle]);
        break;
      case MovementType.Relative:
        yield call([moveable, moveable.moveRelative], position!, units as Length | Angle);
        break;
      case MovementType.WaitUntilIdle:
        yield call([moveable, moveable.waitUntilIdle]);
        break;
      case MovementType.Cycle: {
        const cycleDist = axisState?.travelInfo?.cycleDistance;
        const velocity: Parameters<typeof moveable.moveMax>[0] = { velocity: position, velocityUnit: units as Velocity | AngularVelocity };
        if (cycleDist === 'not-cyclic') {
          for (;;) {
            yield call([moveable, moveable.moveMax], velocity);
            yield call([moveable, moveable.moveMin], velocity);
          }
        } else if (typeof cycleDist === 'number') {
          for (;;) {
            yield call([moveable, moveable.moveRelative], cycleDist, Units.NATIVE, velocity);
            yield call([moveable, moveable.moveRelative], -cycleDist, Units.NATIVE, velocity);
          }
        }
      }
    }

    yield put(actions.moveAxisDone(axis));
  } catch (err) {
    throwUnexpectedError(err);

    yield call(handleMovementError, err, axis);
  }

  yield put(actions.refreshVisibleAxes());
}

function* handleMovementError(movementErr: Error, axisKey: EntityKey) {
  const axis: RT<typeof getAxisState> = yield getAxisState(axisKey);
  if (axis == null) { return }

  if (movementErr instanceof MovementInterruptedException) {
    // handled by monitoring of flags
    yield put(actions.moveAxisDone(axisKey));
  } else if (movementErr instanceof MovementFailedException) {
    const { warnings } = movementErr.details;

    yield put(actions.addWarningFlags(axis.key, warnings));

    let persistentFlags: Set<string> | undefined;
    try {
      const asciiAxis: RT<typeof getAxis> = yield call(getAxis, axisKey);
      persistentFlags = yield call(getWarningFlags, asciiAxis, axis);
    } catch (err) {
      throwUnexpectedError(err);
      // ignored, all errors displayed
    }

    const errors: AxisError[] = warnings
      .filter(warningFlag => persistentFlags?.has(warningFlag) !== true)
      .map(warningFlag => ({
        type: warningFlagToErrorType(warningFlag),
        warningFlag,
        message: `Warning flag: ${warningFlag}`,
        origin: 'movement',
      }));
    if (errors.length === 0) {
      errors.push({ type: AxisErrorType.Fault, message: movementErr.message, origin: 'movement' });
    }

    yield put(actions.moveAxisDone(axisKey, errors));
  } else if (movementErr instanceof CommandFailedException
    && movementErr.details.responseData === 'BADDATA') {
    yield put(actions.moveAxisDone(axisKey, [{
      type: AxisErrorType.Fault,
      title: 'Invalid Position or Velocity',
      message: 'The axis cannot move to requested position or at requested velocity.',
      origin: 'movement',
    }]));
  } else {
    yield put(actions.moveAxisDone(axisKey, defaultMovementError(movementErr)));
  }
}

function* setDriver(
  { payload: { axis, enabled } }: Action<ActionsToPayloads[ActionTypes.SET_DRIVER]>
): SagaIter {
  try {
    const axisState: RT<typeof getAxisState> = yield call(getAxisState, axis);
    if (axisState == null) { return }
    const axisNumbers = axisState.lockstep?.axisNumbers ?? [axisState.axisNumber];

    const device: RT<typeof getDevice> = yield call(getDevice, axis);

    for (const axisNumber of axisNumbers) {
      yield device.genericCommand(`driver ${enabled ? 'enable' : 'disable'}`, { axis: axisNumber });
    }

    yield put(actions.updateErrors(axis, 'set_driver'));
    yield put(actions.refreshVisibleAxes());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.updateErrors(axis, 'set_driver',
      [{ type: AxisErrorType.Fault, message: err.message, origin: 'set_driver' }]));
  }
}
