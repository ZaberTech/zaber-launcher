import React from 'react';
import { Icons, Text } from '@zaber/react-library';

import { connectionName } from '../utils';
import { RawTcpConfig, SerialPortConfig, ConnectionType } from '../../app_components';

interface Props {
  connection: { config: SerialPortConfig | RawTcpConfig; type: ConnectionType };
}

export const ConnectionName: React.FC<Props> = ({ connection }) => (<>
  {connection.type === ConnectionType.TCP_IP && <Icons.EthernetSocket className="connection-type-icon"/>}
  {connection.type === ConnectionType.SERIAL_PORT && <Icons.Usb className="connection-type-icon"/>}
  &nbsp;
  <Text t={Text.Type.H4}>{connectionName(connection)}</Text>
</>);
