import React from 'react';
import type { Container } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { wrapWithNewStore } from '../test';
import { IpcMock, mockIpc } from '../ipc/mock';

import { actions } from './actions';
import { UpdateCheck } from './UpdateCheck';

const TestUpdateCheck = wrapWithNewStore(UpdateCheck);

let container: Container;
let wrapper: RenderResult;

let ipcMock: IpcMock;

beforeEach(() => {
  container = createContainer();
  ipcMock = mockIpc(container);

  wrapper = render(<TestUpdateCheck/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

const openMenu = () => fireEvent.click(wrapper.getByTitle('Open Menu'));

beforeEach(() => {
  openMenu();
  fireEvent.click(wrapper.getByText(/Check For Updates/));
});

test('clicking on checkUpdates sends action to main process and disables the menu item', async () => {
  wrapper.getByText(/Checking for Updates/);
  openMenu();
  expect(wrapper.getByText(/Check For Updates/)).toHaveClass('disabled');

  expect(ipcMock.sendActionToMainProcess).toHaveBeenCalled();
});

test('on update check done the menu item gets enabled and no updates are displayed', async () => {
  TestUpdateCheck.testStore.dispatch(actions.versionCheckDone());

  wrapper.getByText(/Up to date/);
  openMenu();
  expect(wrapper.getByText(/Check For Updates/)).not.toHaveClass('disabled');
});

test('displays error when version check ends with error', async () => {
  TestUpdateCheck.testStore.dispatch(actions.versionCheckDone('No internet'));
  wrapper.getByText(/No internet/);
});

test('displays new version when it\'s available', async () => {
  TestUpdateCheck.testStore.dispatch(actions.newVersionReady('3.4.5'));
  TestUpdateCheck.testStore.dispatch(actions.versionCheckDone());

  wrapper.getByText(/New version 3\.4\.5 available/);

  openMenu();
  wrapper.getByText(/Update and Restart/);
});
