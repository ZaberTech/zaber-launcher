import React from 'react';
import { Container, injectable } from 'inversify';
import {
  RenderResult, render, fireEvent, getByText, getByTitle, getByTestId, queryByText, queryAllByTestId, queryByTestId, within,
} from '@testing-library/react';
import _ from 'lodash';
import {
  Length, Units, MovementFailedException, MovementInterruptedException, CommandFailedException, CommandFailedExceptionData,
  Angle, ascii, Velocity, AngularVelocity,
} from '@zaber/motion';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { createContainer, destroyContainer } from '../container';
import { defer, wrapWithNewStore, waitUntilPass, wrapWithRouter, waitTick, Defer } from '../test';
import { selectConnections } from '../connection_manager';
import { MessageRoutersService } from '../message_router';
import {
  mockDataForDeviceWithPeripherals, mockLocalConnections, mockSingleDevice, mockSingleDeviceWithPeripherals,
} from '../connection_manager/mocks';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { connectionStatusActions } from '../connection_status';
import type { RT } from '../utils';
import { getInputAndSelectByTitle, getUnitSelectByTitle, getValueInputByTitle } from '../units/test_utils';

import { BasicMovement } from './BasicMovement';
import { actions } from './actions';
import { INDICATOR_WIDTH_PX, END_STOP_WIDTH_PX } from './PositionDisplay';

const POSITION_DISPLAY_WIDTH = 100;

function mockBoundingRectangle() {
  let boundingRectFuncBackup: () => DOMRect;

  beforeAll(() => {
    // Temporarily change the default function used by `HTMLElement`
    // Set the bounding rectangle for the position track so that the component can calculate the
    // correct hover positions
    boundingRectFuncBackup = HTMLElement.prototype.getBoundingClientRect;
    HTMLElement.prototype.getBoundingClientRect = jest.fn(() => ({
      width: POSITION_DISPLAY_WIDTH + INDICATOR_WIDTH_PX + END_STOP_WIDTH_PX * 2,
      height: 5,
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      x: 0,
      y: 0,
      toJSON: () => '',
    }));
  });

  afterAll(() => {
    HTMLElement.prototype.getBoundingClientRect = boundingRectFuncBackup;
  });
}

function generateFractionMouseEvent(fraction: number) {
  return { clientX: POSITION_DISPLAY_WIDTH * fraction + INDICATOR_WIDTH_PX / 2 + END_STOP_WIDTH_PX, button: 0 };
}

let container: Container;

let axes: AxisMock[];
let axisCallback: (axis: AxisMock) => void;

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);

    axes.push(this);
    if (axisCallback) { axisCallback(this) }
  }
  _flags = [] as string[];
  _settings: Record<string, number> = {
    'limit.min': 200,
    'limit.max': 1000,
    'maxspeed': 100000,
  };
  _pos = 0;
  getPosition = jest.fn(async (units: Units) => {
    if (units === Length.mm) {
      return this._pos / 10;
    } else if (units === Angle.DEGREES) {
      return this._pos / 30;
    }
    return this._pos;
  });
  warnings = {
    getFlags: jest.fn(async () => new Set(this._flags)),
    clearFlags: jest.fn(async () => {
      const flags = new Set(this._flags);
      this._flags = [];
      return flags;
    }),
  };
  settings = {
    get: jest.fn(setting => {
      if (typeof this._settings[setting] !== 'number') {
        throw new CommandFailedException('BADCOMMAND', null!);
      }
      return Promise.resolve(this._settings[setting]);
    }),
    convertFromNativeUnits: jest.fn((_setting, value, units) => {
      switch (units) {
        case Length.mm: return value / 10;
        case Velocity['mm/s']: return value / 10;
        case Angle.DEGREES: return value / 30;
        case AngularVelocity.DEGREES_PER_SECOND: return value / 10;
      }
      throw new Error(`Unknown units ${units}`);
    }),
    canConvertNativeUnits: jest.fn(() => true)
  };
  home = jest.fn().mockResolvedValue(undefined);
  stop = jest.fn().mockResolvedValue(undefined);
  moveAbsolute = jest.fn((position: number, units: Units) => {
    if (units === Length.cm) {
      this._pos = position * 100;
    } else {
      this._pos = position;
    }
  });
  moveRelative = jest.fn((position: number, _units: Units) => {
    this._pos = _.clamp(this._pos + position, this._settings['limit.min'], this._settings['limit.max']);
  });
  moveVelocity = jest.fn((position: number, _units: Units) => {
    if (position > 0) {
      this._pos = this._settings['limit.max'];
    } else {
      this._pos = this._settings['limit.min'];
    }
  });
  moveMin = jest.fn().mockResolvedValue(undefined);
  moveMax = jest.fn().mockResolvedValue(undefined);
  waitUntilIdle = jest.fn().mockResolvedValue(undefined);
}

let device: DeviceMock;
class DeviceMock extends DeviceMockBase<AxisMock> {
  lockstep = {
    home: jest.fn().mockResolvedValue(undefined),
    moveMin: jest.fn().mockResolvedValue(undefined),
    moveMax: jest.fn().mockResolvedValue(undefined),
    stop: jest.fn().mockResolvedValue(undefined),
  };

  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    device = this;
  }

  getLockstep = jest.fn(() => this.lockstep);
  genericCommand = jest.fn().mockResolvedValue(undefined);
}

let connections: ConnectionMock[];

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  genericCommandNoResponse = jest.fn().mockResolvedValue(undefined);

  constructor(id: string, router: RouterConnectionMock) {
    super(id, router);
    connections.push(this);
  }
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const TestBasicMovement = wrapWithNewStore(wrapWithRouter(BasicMovement));

let wrapper: RenderResult;

let position: RT<typeof getInputAndSelectByTitle>;

beforeEach(() => {
  device = null!;
  axes = [];
  connections = [];

  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

  wrapper = render(<TestBasicMovement/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;

  axes = null!;
  connections = null!;
  axisCallback = null!;
});

describe('single linear device', () => {
  beforeEach(() => {
    axisCallback = axis => axis._pos = 310;
    const store = TestBasicMovement.testStore;
    mockSingleDevice(store);
  });

  test('renders devices upon selection', () => {
    const connection = _.sample(selectConnections(TestBasicMovement.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    wrapper.getByText(/X-LHM/);
  });

  describe('connection selected', () => {
    beforeEach(async () => {
      const connection = _.sample(selectConnections(TestBasicMovement.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(connection.key);
      await waitUntilPass(() => {
        position = getInputAndSelectByTitle(wrapper, 'Device Position');
      });
    });

    test('queries device upon selection and displays values', async () => {
      await waitUntilPass(() => {
        within(wrapper.getByTestId('movable-card-@axis|local|COM5|1|1')).getByText(/Travel Distance\s+80\s+mm/i);
        expect(position.valueInput.value).toBe('31');
      });
    });

    test('recalculates values when units change', async () => {
      await waitUntilPass(() => {
        expect(position.valueInput.value).toBe('31');
      });

      fireEvent.change(position.unitSelect, { target: { value: Length.CENTIMETRES } });

      await waitUntilPass(() => {
        wrapper.getByText(/Travel Distance\s+8\s+cm/i);
        expect(position.valueInput.value).toBe('3.1');
      });
    });

    test('moves the device when clicked on buttons', async () => {
      fireEvent.click(wrapper.getByTitle('Home Axis'));
      await waitUntilPass(() => {
        expect(axes).toHaveLength(1);
        expect(axes[0].home).toHaveBeenCalledTimes(1);
      });

      fireEvent.click(wrapper.getByTitle('Move to Beginning'));
      await waitUntilPass(() => {
        expect(axes[0].moveMin).toHaveBeenCalledTimes(1);
      });

      fireEvent.click(wrapper.getByTitle('Move to End'));
      await waitUntilPass(() => {
        expect(axes[0].moveMax).toHaveBeenCalledTimes(1);
      });

      fireEvent.click(wrapper.getByTitle('Stop Axis'));
      await waitUntilPass(() => {
        expect(axes[0].stop).toHaveBeenCalledTimes(1);
      });
    });

    test('movement button is held until movement finishes', async () => {
      fireEvent.click(wrapper.getByTitle('Home Axis'));
      expect(wrapper.getByTitle('Home Axis')).toHaveClass('activated');
      await waitUntilPass(() => {
        expect(wrapper.getByTitle('Home Axis')).not.toHaveClass('activated');
      });

      fireEvent.click(wrapper.getByTitle('Move to Beginning'));
      expect(wrapper.getByTitle('Move to Beginning')).toHaveClass('activated');
      await waitUntilPass(() => {
        expect(wrapper.getByTitle('Move to Beginning')).not.toHaveClass('activated');
      });

      fireEvent.click(wrapper.getByTitle('Move to End'));
      expect(wrapper.getByTitle('Move to End')).toHaveClass('activated');
      await waitUntilPass(() => {
        expect(wrapper.getByTitle('Move to End')).not.toHaveClass('activated');
      });
    });

    describe('jog button', () => {
      let waitUntilIdle: Defer<void>;

      beforeEach(async () => {
        waitUntilIdle = defer<void>();
        await waitUntilPass(() => {
          wrapper.getByTitle(/Move towards Beginning/);
        });
        axes[0].waitUntilIdle.mockReturnValue(waitUntilIdle.promise);
      });

      afterEach(async () => {
        waitUntilIdle.resolve();
      });

      test('left button moves axis by negative maxspeed', async () => {
        fireEvent.mouseDown(wrapper.getByTitle('Move towards Beginning'));
        await waitUntilPass(() => {
          expect(axes[0].moveVelocity).toHaveBeenCalledWith(-100000, Units.NATIVE);
        });
      });

      test('right button moves axis by maxspeed', async () => {
        fireEvent.mouseDown(wrapper.getByTitle('Move towards End'));
        await waitUntilPass(() => {
          expect(axes[0].moveVelocity).toHaveBeenCalledWith(100000, Units.NATIVE);
        });
      });

      test('leaving the button will stop the movement', async () => {
        fireEvent.mouseDown(wrapper.getByTitle('Move towards Beginning'));
        await waitUntilPass(() => {
          expect(axes[0].waitUntilIdle).toHaveBeenCalled();
        });
        fireEvent.mouseUp(wrapper.getByTitle('Move towards Beginning'));
        await waitUntilPass(() => {
          expect(axes[0].stop).toHaveBeenCalled();
        });
      });

      test('leaving the button won\'t stop the movement after it finished', async () => {
        fireEvent.mouseDown(wrapper.getByTitle('Move towards Beginning'));
        await waitUntilPass(() => {
          expect(axes[0].waitUntilIdle).toHaveBeenCalled();
        });

        waitUntilIdle.resolve();
        await waitTick();

        fireEvent.mouseLeave(wrapper.getByTitle('Move towards Beginning'));
        expect(axes[0].stop).not.toHaveBeenCalled();
      });
    });

    describe('position display', () => {
      mockBoundingRectangle();

      beforeEach(async () => {
        await waitUntilPass(() => {
          expect(position.valueInput.value).toBe('31');
        });
      });

      test('shows projected device position on hover', async () => {
        fireEvent.mouseMove(wrapper.getByTestId('interactive-track'), generateFractionMouseEvent(0.25));
        await waitUntilPass(() => {
          getByText(wrapper.getByTestId('interactive-track'), /20\.00 mm/);
        });
      });

      test('rounds device position to whole number when native units are used', async () => {
        fireEvent.change(getUnitSelectByTitle(wrapper, 'Device Position'), { target: { value: Units.NATIVE } });
        fireEvent.mouseMove(wrapper.getByTestId('interactive-track'), generateFractionMouseEvent(0.5));
        await waitUntilPass(() => {
          getByText(wrapper.getByTestId('interactive-track'), /400 Native/);
        });
      });

      test('moves the device when clicked on position display', async () => {
        fireEvent.mouseUp(wrapper.getByTestId('interactive-track'), generateFractionMouseEvent(0.75));
        await waitUntilPass(() => {
          expect(axes[0].moveAbsolute).toHaveBeenCalledWith(800, Units.NATIVE);
        });
      });
    });

    describe('precise movement', () => {
      let moveAbs: RT<typeof getInputAndSelectByTitle>;
      let moveVel: RT<typeof getInputAndSelectByTitle>;
      let moveRel: RT<typeof getInputAndSelectByTitle>;

      beforeEach(async () => {
        await waitUntilPass(() => {
          expect(position.valueInput.value).toBe('31');
        });
        fireEvent.click(wrapper.getByTitle('Expand Precise Movement section'));
        wrapper.getByTitle('Collapse Precise Movement section');
        await waitUntilPass(() => {
          moveAbs = getInputAndSelectByTitle(wrapper, 'Absolute Position Control');
          moveVel = getInputAndSelectByTitle(wrapper, 'Move at Velocity Control');
          moveRel = getInputAndSelectByTitle(wrapper, 'Relative Movement Control');
        });
      });

      test('input unit conversions', () => {
        expect(moveVel.unitSelect.value).toBe(Velocity.MILLIMETRES_PER_SECOND);
        expect(moveVel.valueInput.value).toBe('10000');

        fireEvent.change(moveVel.unitSelect,
          { target: { value: Velocity.CENTIMETRES_PER_SECOND } });
        expect(moveVel.unitSelect.value).toBe(Velocity.CENTIMETRES_PER_SECOND);
        expect(moveVel.valueInput.value).toBe('1000');
      });

      describe('absolute move errors', () => {
        test('absolute position error appears and disappears', async () => {
          const preciseWrapper = within(wrapper.getByTestId('precise-movements'));
          const queryLimitsMM = 'Value must be between 20 and 100.';
          fireEvent.input(moveAbs.valueInput, { target: { value: '60' } });
          expect(preciseWrapper.queryByText(queryLimitsMM)).toBeNull();
          fireEvent.input(moveAbs.valueInput, { target: { value: '100.1' } });
          preciseWrapper.getByText(queryLimitsMM);
          fireEvent.input(moveAbs.valueInput, { target: { value: '60' } });
          expect(preciseWrapper.queryByText(queryLimitsMM)).toBeNull();
        });

        test('absolute position error converts units', async () => {
          fireEvent.change(moveAbs.unitSelect, { target: { value: Length.CENTIMETRES } });
          fireEvent.input(moveAbs.valueInput, { target: { value: '10.1' } });
          wrapper.getByText('Value must be between 2 and 10.');
        });
      });

      test('move to absolute position', async () => {
        fireEvent.change(moveAbs.unitSelect, { target: { value: Length.CENTIMETRES } });
        fireEvent.change(moveAbs.valueInput, { target: { value: '1' } });

        fireEvent.click(wrapper.getByTitle('Move to Absolute Position'));
        await waitUntilPass(() => {
          expect(axes[0].moveAbsolute).toHaveBeenCalledTimes(1);
        });
        await waitUntilPass(() => {
          expect(position.valueInput.value).toBe('10');
        });
      });

      test('move by relative distance', async () => {
        fireEvent.change(moveRel.valueInput, { target: { value: '5' } });
        fireEvent.click(wrapper.getByTitle('Move by Relative Distance towards Beginning'));
        await waitUntilPass(() => {
          expect(axes[0].moveRelative).toHaveBeenCalledTimes(1);
        });
        await waitUntilPass(() => {
          expect(position.valueInput.value).toBe('30.5');
        });

        fireEvent.change(moveRel.valueInput, { target: { value: '10' } });
        fireEvent.click(wrapper.getByTitle('Move by Relative Distance towards End'));
        await waitUntilPass(() => {
          expect(axes[0].moveRelative).toHaveBeenCalledTimes(2);
        });
        await waitUntilPass(() => {
          expect(position.valueInput.value).toBe('31.5');
        });
      });

      test('move at a given velocity from default state', async () => {
        const controlTitle = 'Move at Velocity';

        fireEvent.click(wrapper.getByTitle(`${controlTitle} towards Beginning`));
        await waitUntilPass(() => {
          expect(axes[0].moveVelocity).lastCalledWith(-10000, Velocity.MILLIMETRES_PER_SECOND);
        });
      });

      test('move at a given velocity', async () => {
        fireEvent.change(moveVel.valueInput, { target: { value: '5' } });

        fireEvent.click(wrapper.getByTitle('Move at Velocity towards Beginning'));
        await waitUntilPass(() => {
          expect(axes[0].moveVelocity).toHaveBeenCalledTimes(1);
        });
        await waitUntilPass(() => {
          expect(position.valueInput.value).toBe('20');
        });

        fireEvent.click(wrapper.getByTitle('Move at Velocity towards End'));
        await waitUntilPass(() => {
          expect(axes[0].moveVelocity).toHaveBeenCalledTimes(2);
        });
        await waitUntilPass(() => {
          expect(position.valueInput.value).toBe('100');
        });
      });

      test('absolute position reacts to Enter key', async () => {
        fireEvent.change(moveAbs.valueInput, { target: { value: '73' } });
        fireEvent.keyDown(moveAbs.valueInput, { key: 'Enter' });

        await waitUntilPass(() => {
          expect(position.valueInput.value).toBe('7.3');
        });
        expect(axes[0].moveAbsolute).toHaveBeenCalledTimes(1);
      });
    });

    describe('failed movement', () => {
      beforeEach(() => {
        axes[0].home.mockRejectedValueOnce(new MovementFailedException('Movement has failed', {
          reason: '',
          warnings: ['FS'],
          device: 0,
          axis: 0,
        }));

        fireEvent.click(wrapper.getByTitle('Home Axis'));
      });

      test('displays error when movement fails', async () => {
        await waitUntilPass(() => {
          wrapper.getByText(/Stalled and Stopped/);
        });
      });

      test('shows details', async () => {
        await waitUntilPass(() => {
          fireEvent.click(wrapper.getByText(/Show details/i));
        });
        wrapper.getByText(/stall is a terrible thing/);
      });

      test('allows to dismiss error', async () => {
        await waitUntilPass(() => {
          fireEvent.click(wrapper.getByText(/Dismiss/i));
        });
        expect(wrapper.queryByText(/Dismiss/)).toBeNull();
      });

      test('clears movement error when new movement is initiated', async () => {
        fireEvent.click(wrapper.getByTitle('Home Axis'));
        expect(wrapper.queryByText(/Stalled and Stopped/)).toBeNull();
      });
    });

    describe('failed movement - other cases', () => {
      test('displays error when movement fails (no warning flags)', async () => {
        axes[0].home.mockRejectedValueOnce(new MovementFailedException('Movement has failed', {
          reason: '',
          warnings: [],
          device: 0,
          axis: 0,
        }));

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => {
          wrapper.getByText(/Movement Failed/);
        });
      });
      test('displays custom error when movement fails with BADDATA', async () => {
        axes[0].home.mockRejectedValueOnce(new CommandFailedException('Command has failed', {
          responseData: 'BADDATA', replyFlag: 'RJ',
        } as CommandFailedExceptionData));

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => {
          wrapper.getByText(/Invalid Position/);
        });
      });
      test('displays error when movement fails (other errors)', async () => {
        axes[0].home.mockRejectedValueOnce(new CommandFailedException('Command has failed', {
          responseData: 'BADCOMMAND', replyFlag: 'RJ',
        } as CommandFailedExceptionData));

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => {
          wrapper.getByText(/Movement Failed/);
        });
      });
      test('displays only flags that are not persistent', async () => {
        axes[0].home.mockImplementationOnce(async () => {
          axes[0]._flags = ['FR'];
          throw new MovementFailedException('Movement has failed', {
            reason: '',
            warnings: ['FR', 'WS'],
            device: 0,
            axis: 0,
          });
        });

        fireEvent.click(wrapper.getByTitle('Home Axis'));
        await waitUntilPass(() => wrapper.getByText(/Movement Failed: Warning Flag WS/));
        expect(wrapper.queryAllByText(/Movement Failed: Warning Flag/)).toHaveLength(1);
      });
    });

    test('displays note when movement is interrupted', async () => {
      axes[0].waitUntilIdle.mockImplementationOnce(() => {
        axes[0]._flags.push('NI');
        return Promise.reject(new MovementInterruptedException('Interrupted', null!));
      });

      fireEvent.click(wrapper.getByTitle('Expand Precise Movement section'));
      await waitUntilPass(() =>
        fireEvent.click(wrapper.getByTitle('Move at Velocity towards End')));

      await waitUntilPass(() => {
        wrapper.getByText(/Warning Flag NI/);
      });
    });

    test('does not display NI when movement is interrupted by stopping', async () => {
      const stopped = defer<void>();
      axes[0].moveMax.mockImplementationOnce(async () => {
        await stopped.promise;
        return Promise.reject(new MovementInterruptedException('Interrupted', null!));
      });
      axes[0].stop.mockImplementationOnce(async () => {
        axes[0]._flags.push('NI');
        axes[0]._flags.push('WS'); // just for validation
        stopped.resolve();
      });

      fireEvent.click(wrapper.getByTitle('Move to End'));
      await waitUntilPass(() => expect(axes[0].moveMax).toHaveBeenCalled());
      fireEvent.click(wrapper.getByTitle('Stop Axis'));

      await waitUntilPass(() => {
        wrapper.getByText(/Warning Flag WS/);
        expect(wrapper.queryByText(/Warning Flag NI/)).toBeNull();
      });
    });

    test('shows warnings for the present flags and description', async () => {
      axes[0]._flags.push('FO');

      TestBasicMovement.testStore.dispatch(actions.refreshVisibleAxes());
      await waitUntilPass(() => {
        wrapper.getByText(/Warning Flag FO/);
      });

      axes[0]._flags.pop();
      axes[0]._flags.push('WR');

      TestBasicMovement.testStore.dispatch(actions.refreshVisibleAxes());
      await waitUntilPass(() => {
        wrapper.getByText(/No Reference Position \(WR\) - Homing Required/);
        expect(wrapper.queryByText(/Warning Flag FO/)).toBeNull();
      });

      fireEvent.click(wrapper.getByText(/Show details/i));
      wrapper.getByText(/The axis does not have a reference position/);
    });

    test('shows/hides error when device cannot be reached', async () => {
      axes[0].warnings.getFlags.mockRejectedValueOnce(new Error('Cannot open port'));
      TestBasicMovement.testStore.dispatch(actions.refreshVisibleAxes());

      await waitUntilPass(() =>
        wrapper.getByText(/Cannot reach the device/)
      );
      fireEvent.click(wrapper.getByText(/Show details/i));
      wrapper.getByText('Cannot open port');

      TestBasicMovement.testStore.dispatch(actions.refreshVisibleAxes());
      await waitUntilPass(() =>
        expect(wrapper.queryByText(/Cannot reach the device/)).toBeNull()
      );
    });

    test('all devices controls click moves all axes', async () => {
      fireEvent.click(wrapper.getByTitle('Home All Devices'));
      await waitUntilPass(() => {
        for (const axis of axes) {
          expect(axis.home).toHaveBeenCalledTimes(1);
        }
      });

      fireEvent.click(wrapper.getByTitle('Move All Devices to Beginning'));
      await waitUntilPass(() => {
        for (const axis of axes) {
          expect(axis.moveMin).toHaveBeenCalledTimes(1);
        }
      });

      fireEvent.click(wrapper.getByTitle('Move All Devices to End'));
      await waitUntilPass(() => {
        for (const axis of axes) {
          expect(axis.moveMax).toHaveBeenCalledTimes(1);
        }
      });

      fireEvent.click(wrapper.getByTitle('Stop All Devices'));
      await waitUntilPass(() => {
        for (const axis of axes) {
          expect(axis.stop).toHaveBeenCalledTimes(1);
        }
      });
    });

    test('all devices movement button is held until all movements finish', async () => {
      const deferred = defer<void>();
      for (const axis of axes) {
        axis.home.mockReturnValue(deferred.promise);
      }

      fireEvent.click(wrapper.getByTitle('Home All Devices'));
      await waitUntilPass(() => {
        for (const homeAxis of wrapper.getAllByTitle('Home Axis')) {
          expect(homeAxis).toHaveClass('activated');
        }
      });

      deferred.resolve();
      await waitUntilPass(() => {
        for (const homeAxis of wrapper.getAllByTitle('Home Axis')) {
          expect(homeAxis).not.toHaveClass('activated');
        }
      });
    });

    test('unselects connection upon disconnect', () => {
      expect(wrapper.queryByText(/X-LHM/)).not.toBeNull();

      const connection = _.sample(selectConnections(TestBasicMovement.testStore.getState()))!;
      TestBasicMovement.testStore.dispatch(connectionStatusActions.routedConnectionDisconnect(connection.key));

      expect(wrapper.queryByText(/X-LHM/)).toBeNull();
    });

    describe('enable/disable driver', () => {
      test('disable driver', async () => {
        fireEvent.click(wrapper.getByTitle('Open Menu'));
        fireEvent.click(wrapper.getByText('Disable Driver'));

        await waitUntilPass(() => expect(device.genericCommand).toHaveBeenLastCalledWith('driver disable', { axis: 1 }));
      });

      test('enable driver (from menu)', async () => {
        axes[0]._flags.push('FO');
        TestBasicMovement.testStore.dispatch(actions.refreshVisibleAxes());
        await waitUntilPass(() => wrapper.getByText('Enable Driver'));

        fireEvent.click(wrapper.getByTitle('Open Menu'));
        fireEvent.click(wrapper.getByTitle('Enable Driver'));
        await waitUntilPass(() => expect(device.genericCommand).toHaveBeenLastCalledWith('driver enable', { axis: 1 }));
      });

      test('enable driver (from warning)', async () => {
        axes[0]._flags.push('FH');
        TestBasicMovement.testStore.dispatch(actions.refreshVisibleAxes());
        await waitUntilPass(() => wrapper.getByText('Enable Driver'));

        fireEvent.click(wrapper.getByText('Enable Driver'));
        await waitUntilPass(() => expect(device.genericCommand).toHaveBeenLastCalledWith('driver enable', { axis: 1 }));
      });

      test('shows error when disabling driver', async () => {
        device.genericCommand.mockRejectedValueOnce(new Error('BADCOMMAND'));

        fireEvent.click(wrapper.getByTitle('Open Menu'));
        fireEvent.click(wrapper.getByText('Disable Driver'));

        await waitUntilPass(() => wrapper.getByText('Cannot enable/disable driver'));
        fireEvent.click(wrapper.getByText(/Show Details/));
        wrapper.getByText('BADCOMMAND');
      });
    });
  });
});

describe('device with 2 lockstep groups', () => {
  beforeEach(async () => {
    mockSingleDeviceWithPeripherals(TestBasicMovement.testStore, {
      modifier: device => {
        device.locksteps = [{
          groupNumber: 1,
          axisNumbers: [1, 2],
        }, {
          groupNumber: 2,
          axisNumbers: [3, 4]
        }];
        return device;
      },
      peripheralCount: 4,
      deviceNumber: 1,
    });
    const connection = _.sample(selectConnections(TestBasicMovement.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
  });

  test('renders lockstep groups', () => {
    wrapper.getByText(/Lockstep Group 1/);
    wrapper.getByText(/Lockstep Group 2/);
  });

  test('lockstep dropdown info expands and retracts', () => {
    let dropdownInfo = wrapper.queryByTestId(/lockstep info 1/);
    expect(dropdownInfo).toBeNull();
    const dropdownInitiator = wrapper.getByTestId(/lockstep initiator 1/);
    fireEvent.click(dropdownInitiator);
    dropdownInfo = wrapper.queryByTestId(/lockstep info 1/);
    expect(dropdownInfo).not.toBeNull();
    fireEvent.click(dropdownInitiator);
    dropdownInfo = wrapper.queryByTestId(/lockstep info 1/);
    expect(dropdownInfo).toBeNull();
  });

  test('lockstep groups contain the proper axes', () => {
    const dropdownInitiator1 = wrapper.getByTestId(/lockstep initiator 1/);
    fireEvent.click(dropdownInitiator1);
    const dropdownInfo1 = wrapper.getByTestId(/lockstep info 1/);
    expect(dropdownInfo1.childElementCount).toBe(2);
    expect(/Axis 1.*LHM025A-T3/.test(dropdownInfo1.innerHTML)).toBe(true);
    expect(/Axis 2.*LHM025A-T3/.test(dropdownInfo1.innerHTML)).toBe(true);

    const dropdownInitiator2 = wrapper.getByTestId(/lockstep initiator 2/);
    fireEvent.click(dropdownInitiator2);
    const dropdownInfo2 = wrapper.getByTestId(/lockstep info 2/);
    expect(dropdownInfo2.childElementCount).toBe(2);
    expect(/Axis 3.*LHM025A-T3/.test(dropdownInfo2.innerHTML)).toBe(true);
    expect(/Axis 4.*LHM025A-T3/.test(dropdownInfo2.innerHTML)).toBe(true);
  });
});

describe('single linear device with one lockstep group', () => {
  beforeEach(async () => {
    axisCallback = axis => axis._pos = 310;
    mockSingleDeviceWithPeripherals(TestBasicMovement.testStore, {
      modifier: device => {
        device.locksteps = [{
          groupNumber: 1,
          axisNumbers: [1, 2],
        }];
        return device;
      },
      peripheralCount: 2,
      deviceNumber: 1,
    });
  });

  test('renders lockstep group upon selection', () => {
    const connection = _.sample(selectConnections(TestBasicMovement.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    wrapper.getByText(/Lockstep Group 1/);
  });

  describe('connection selected', () => {
    beforeEach(() => {
      const connection = _.sample(selectConnections(TestBasicMovement.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(connection.key);
    });

    test('moves the lockstep group when clicked on buttons', async () => {
      fireEvent.click(wrapper.getByTitle('Home Axis'));
      await waitUntilPass(() => {
        expect(device.lockstep.home).toHaveBeenCalled();
      });

      fireEvent.click(wrapper.getByTitle('Home Axis'));
      await waitUntilPass(() => {
        expect(device.lockstep.home).toHaveBeenCalled();
      });

      fireEvent.click(wrapper.getByTitle('Move to Beginning'));
      await waitUntilPass(() => {
        expect(device.lockstep.moveMin).toHaveBeenCalledTimes(1);
      });

      fireEvent.click(wrapper.getByTitle('Move to End'));
      await waitUntilPass(() => {
        expect(device.lockstep.moveMax).toHaveBeenCalledTimes(1);
      });

      fireEvent.click(wrapper.getByTitle('Stop Axis'));
      await waitUntilPass(() => {
        expect(device.lockstep.stop).toHaveBeenCalledTimes(1);
      });
    });

    test('each lockstepped axis has its image shown',  () => {
      expect(wrapper.getAllByTitle('Linear stage peripheral')).toHaveLength(2);
    });

    test('clicking on the lockstep group causes axis names to appear', () => {
      expect(wrapper.queryByText(/Axis 1 LHM025A-T3/)).toBeNull();
      fireEvent.click(wrapper.getByText(/Lockstep Group 1/).parentElement!);
      wrapper.getByText(/Axis 1 LHM025A-T3/);
    });

    test('disables all drivers', async () => {
      fireEvent.click(wrapper.getByTitle('Open Menu'));
      fireEvent.click(wrapper.getByText('Disable Driver'));

      await waitUntilPass(() => {
        expect(device.genericCommand).toHaveBeenCalledWith('driver disable', { axis: 1 });
        expect(device.genericCommand).toHaveBeenCalledWith('driver disable', { axis: 2 });
      });
    });

    test('displays warning flags from all lockstep axes', async () => {
      axes[0]._flags.push('FO');
      axes[1]._flags.push('FC');
      TestBasicMovement.testStore.dispatch(actions.refreshVisibleAxes());
      await waitUntilPass(() => {
        wrapper.getByText(/Warning Flag FO/);
        wrapper.getByText(/Warning Flag FC/);
      });
    });
  });
});

describe('device pinning', () => {
  beforeEach(() => {
    mockLocalConnections(TestBasicMovement.testStore, {
      COM5: connectionKey => [mockDataForDeviceWithPeripherals(connectionKey, 1, 4, 'legacy')],
      COM6: connectionKey => [mockDataForDeviceWithPeripherals(connectionKey, 1, 4, 'legacy')],
    });
  });

  test('pinning a device moves it into the "Pinned Devices" section at the top of the page', async () => {
    const connectionsList = selectConnections(TestBasicMovement.testStore.getState());
    connectionViewMockInstance.props.onSelect(_.keys(connectionsList)[0]);

    expect(wrapper.queryByText('Pinned Devices')).toBeNull();

    const axisCard = wrapper.getByTestId(/axis 1/);
    fireEvent.click(getByTitle(axisCard, 'Pin Axis'));
    await waitUntilPass(() => {
      wrapper.getByText('Pinned Devices');
    });

    const pinnedAxesSection = wrapper.getByTestId('pinned-axes');
    getByTestId(pinnedAxesSection, /axis 1/);
  });

  test('pinning a device changes its display title to include the connection name', async () => {
    const connectionsList = selectConnections(TestBasicMovement.testStore.getState());
    connectionViewMockInstance.props.onSelect(_.keys(connectionsList)[0]);

    expect(queryByText(wrapper.getByTestId(/axis 1/), /COM5/)).toBeNull();

    const axisCard = wrapper.getByTestId(/axis 1/);
    fireEvent.click(getByTitle(axisCard, 'Pin Axis'));

    expect(queryByText(wrapper.getByTestId(/axis 1/), /COM5/)).toBeTruthy();
  });

  test('pinning all axes', async () => {
    const connectionsList = selectConnections(TestBasicMovement.testStore.getState());
    connectionViewMockInstance.props.onSelect(_.keys(connectionsList)[0]);

    for (let i = 1; i <= 4; i++) {
      const axisCard = wrapper.getByTestId(new RegExp(`axis ${i}`));
      fireEvent.click(getByTitle(axisCard, 'Pin Axis'));
    }
  });

  test('pinning multiple devices moves them to "Pinned Devices" section in the order seen in connections view panel', async () => {
    const connectionsList = selectConnections(TestBasicMovement.testStore.getState());
    connectionViewMockInstance.props.onSelect(_.keys(connectionsList)[0]);

    const axis1Card = wrapper.getByTestId(/axis 4/);
    const axis2Card = wrapper.getByTestId(/axis 2/);

    fireEvent.click(getByTitle(axis2Card, 'Pin Axis'));
    fireEvent.click(getByTitle(axis1Card, 'Pin Axis'));

    const pinnedAxesSection = wrapper.getByTestId('pinned-axes');
    const axesInPinnedSection = queryAllByTestId(pinnedAxesSection, /axis\s\d/);
    expect(axesInPinnedSection.length).toEqual(2);
    getByText(axesInPinnedSection[0], 'Axis 2 LHM025A-T3');
    getByText(axesInPinnedSection[1], 'Axis 4 LHM025A-T3');
  });

  test('pinned devices remain visible even when another connection is opened', async () => {
    const connectionsList = selectConnections(TestBasicMovement.testStore.getState());

    connectionViewMockInstance.props.onSelect(_.keys(connectionsList)[0]);
    const axisCard = wrapper.getByTestId(/axis 1/);
    fireEvent.click(getByTitle(axisCard, 'Pin Axis'));

    connectionViewMockInstance.props.onSelect(_.keys(connectionsList)[1]);

    const pinnedAxesSection = wrapper.getByTestId('pinned-axes');
    const axesInPinnedSection = queryAllByTestId(pinnedAxesSection, /axis\s\d/);
    expect(axesInPinnedSection.length).toEqual(1);
    getByText(axesInPinnedSection[0], 'Axis 1 LHM025A-T3');
  });

  test('unpinning a pinned device moves it out of the "Pinned Devices" section', async () => {
    const connectionsList = selectConnections(TestBasicMovement.testStore.getState());
    connectionViewMockInstance.props.onSelect(_.keys(connectionsList)[0]);

    fireEvent.click(getByTitle(wrapper.getByTestId(/axis 1/), 'Pin Axis'));

    await waitUntilPass(() => {
      getByTestId(wrapper.getByTestId('pinned-axes'), /axis 1/);
    });

    fireEvent.click(getByTitle(wrapper.getByTestId(/axis 1/), 'Pin Axis'));

    expect(wrapper.queryByText('Pinned Devices')).toBeNull();
    wrapper.getByText(/Axis 1 LHM025A-T3/);
  });

  test('unpinning a pinned device while in a different connection removes that device from view', async () => {
    const connectionsList = selectConnections(TestBasicMovement.testStore.getState());

    connectionViewMockInstance.props.onSelect(_.keys(connectionsList)[0]);
    fireEvent.click(getByTitle(wrapper.getByTestId(/axis 1/), 'Pin Axis'));

    connectionViewMockInstance.props.onSelect(_.keys(connectionsList)[1]);

    await waitUntilPass(() => {
      getByTestId(wrapper.getByTestId('pinned-axes'), /axis 1/);
    });

    fireEvent.click(getByTitle(wrapper.getByTestId(/COM5.*axis 1/), 'Pin Axis'));

    expect(wrapper.queryByText('Pinned Devices')).toBeNull();
    expect(wrapper.queryAllByText(/COM5/)).toHaveLength(0);
  });

  test('unpins devices after disconnect', () => {
    const connectionsList = selectConnections(TestBasicMovement.testStore.getState());
    const [connection1, connection2] = _.keys(connectionsList);

    connectionViewMockInstance.props.onSelect(connection1);
    fireEvent.click(getByTitle(wrapper.getByTestId(/axis 1/), 'Pin Axis'));
    connectionViewMockInstance.props.onSelect(connection2);
    fireEvent.click(getByTitle(wrapper.getByTestId(/axis 2/), 'Pin Axis'));

    const pinnedAxes = wrapper.getByTestId('pinned-axes');
    getByTestId(pinnedAxes, /axis 1/);
    getByTestId(pinnedAxes, /axis 2/);

    TestBasicMovement.testStore.dispatch(
      connectionStatusActions.routedConnectionDisconnect(connection1)
    );

    expect(queryByTestId(pinnedAxes, /axis 1/)).toBeNull();
    getByTestId(pinnedAxes, /axis 2/);
  });
});

describe('cyclic device', () => {
  const CYCLE = 10800;
  beforeEach(async () => {
    const store = TestBasicMovement.testStore;
    axisCallback = axis => {
      axis._settings['limit.cycle.dist'] = CYCLE;
      axis._pos = -CYCLE / 3; // -120°
    };
    mockSingleDeviceWithPeripherals(store, { type: 'rotary', peripheralCount: 1 });

    const connection = _.sample(selectConnections(TestBasicMovement.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);

    await waitUntilPass(() => {
      position = getInputAndSelectByTitle(wrapper, 'Device Position');
      expect(position.valueInput.value).toBe('-120');
    });
  });

  test('renders cyclic travel distance', async () => {
    await waitUntilPass(() => wrapper.getByText(/Travel Distance\s+360\s+°/));
  });

  describe('hover over position track', () => {
    mockBoundingRectangle();

    test('shows projected device position in the current period', async () => {
      fireEvent.mouseMove(wrapper.getByTestId('interactive-track'), generateFractionMouseEvent(0.25));
      await waitUntilPass(() => {
        getByText(wrapper.getByTestId('interactive-track'), /-270\.00\s+°/);
      });
    });

    test('moves the device within the current period when click on position track', async () => {
      const CLICK_FRACTION = 0.5;
      fireEvent.mouseUp(wrapper.getByTestId('interactive-track'), generateFractionMouseEvent(CLICK_FRACTION));
      await waitUntilPass(() => {
        expect(axes[0].moveAbsolute).toHaveBeenCalledWith(-CYCLE * CLICK_FRACTION, Units.NATIVE);
      });
    });
  });

  describe('move min/max buttons', () => {
    test('min button moves towards start of the period', async () => {
      const button = wrapper.getByTitle('Move to Beginning');

      fireEvent.click(button);
      await waitUntilPass(() => {
        expect(axes[0].moveAbsolute).toHaveBeenLastCalledWith(-CYCLE, Units.NATIVE);
      });

      fireEvent.click(button);
      await waitUntilPass(() => {
        expect(axes[0].moveAbsolute).toHaveBeenLastCalledWith(-CYCLE * 2, Units.NATIVE);
      });
    });

    test('max button moves towards start of the next period', async () => {
      const button = wrapper.getByTitle('Move to End');

      fireEvent.click(button);
      await waitUntilPass(() => {
        expect(axes[0].moveAbsolute).toHaveBeenLastCalledWith(0, Units.NATIVE);
      });

      fireEvent.click(button);
      await waitUntilPass(() => {
        expect(axes[0].moveAbsolute).toHaveBeenLastCalledWith(CYCLE, Units.NATIVE);
      });
    });
  });
});

describe('device without units', () => {
  let nativePositionInput: HTMLInputElement;
  beforeEach(async () => {
    const store = TestBasicMovement.testStore;
    axisCallback = axis => {
      axis._pos = 12345;
    };
    mockSingleDevice(store, info => {
      info.axes[0].identity.axisType = ascii.AxisType.UNKNOWN;
      info.axes[0].movementDimensions = null;
      info.axes[0].product.conversionTable.rows = [];
      return info;
    });

    const connection = _.sample(selectConnections(TestBasicMovement.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);

    await waitUntilPass(() => {
      nativePositionInput = getValueInputByTitle(wrapper, 'Device Position');
      expect(nativePositionInput.value).toBe('12345');
    });
  });

  test('moves with native units', async () => {
    fireEvent.click(wrapper.getByTitle('Expand Precise Movement section'));
    const moveAbs = getValueInputByTitle(wrapper, 'Absolute Position Control');
    fireEvent.change(moveAbs, { target: { value: '-150' } });
    fireEvent.click(wrapper.getByTitle('Move to Absolute Position'));

    await waitUntilPass(() => expect(nativePositionInput.value).toBe('-150'));
    expect(axes[0].moveAbsolute).toHaveBeenCalledWith(-150, Units.NATIVE);
  });
});
