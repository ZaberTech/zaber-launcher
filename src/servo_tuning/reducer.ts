import { Inertia } from '@zaber/motion';
import type { ServoTuningParamset } from '@zaber/motion/ascii';

import { EntityKey, tryExtractConnectionKey } from '../keys';
import { convertBetweenUnits } from '../units';
import { createReducer } from '../utils';
import { ConnectionManagerActionTypes, ConnectionManagerActionPayloads, DevicesLoadedPayload } from '../connection_manager';


import { ActionsToPayloads, ActionTypes } from './actions';
import type { SavedRow } from './saved_rows/saved_rows';
import type {
  ServoTuningAxisInfo,
  ServoTuningSetting,
  ServoTuningSettings,
  TuningInfo,
  TuningMessage,
  TuningMethod,
} from './types';

export type StateMessage = {
  type: 'top-error' | 'top-note' | 'modal-working' | 'modal-error' | 'modal-success';
  message: string;
} | {
  type: 'modal-screen';
};

export type State = {
  entity: EntityKey | null;
  paramset: ServoTuningParamset | null;
  method: TuningMethod | null;
  deviceInfo: ServoTuningAxisInfo | null;
  tuning: TuningInfo;
  message: StateMessage | null;
  savedRows: SavedRow[];
};

const newKeyState: Omit<State, 'entity' | 'savedRows'> = {
  paramset: null,
  method: null,
  deviceInfo: null,
  tuning: null,
  message: null,
};

const initialState: State = {
  entity: null,
  ...newKeyState,
  savedRows: [],
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const chooseEntity: Reducer<ActionTypes.CHOOSE_ENTITY> = (state, { chosenKey }) => ({
  entity: chosenKey,
  ...newKeyState,
  savedRows: state.savedRows,
});

const chooseTuning: Reducer<ActionTypes.CHOOSE_TUNING> = (state, chosen) => ({ ...state, ...chosen, tuning: null });

const devicesLoaded = (state: State, { connectionKey }: DevicesLoadedPayload) => {
  if (connectionKey === tryExtractConnectionKey(state.entity)) {
    return { ...initialState };
  }
  return state;
};

const infoFetched: Reducer<ActionTypes.INFO_FETCHED> = (state, info) => ({ ...state, deviceInfo: info });

const changeStartupPresetDone: Reducer<ActionTypes.CHANGE_STARTUP_PRESET_DONE> = (state, { paramset }) => ({
  ...state,
  deviceInfo: state.deviceInfo ? { ...state.deviceInfo, startupParamset: paramset } : null,
});

export function calculateRecommendedAccel(
  deviceInfo: ServoTuningAxisInfo, tuning: ActionsToPayloads[ActionTypes.RECALCULATE_RECOMMENDED_ACCEL]
): number | null {
  if (tuning == null) {
    return null;
  }
  const loadMassKg = convertBetweenUnits(tuning.loadMass.value, tuning.loadMass.units, Inertia.KILOGRAMS);
  const carriageMassKg = tuning.carriageModified ?
    convertBetweenUnits(tuning.carriageMass.value, tuning.carriageMass.units, Inertia.KILOGRAMS) :
    deviceInfo.defaultCarriageMassKg;
  if (loadMassKg == null || carriageMassKg == null) {
    return null;
  }
  const mass = loadMassKg + carriageMassKg;
  if (mass === 0) {
    return null;
  }
  // This constant is meant to provide room for acceleration to increase as a result
  // of feedback during a move. It might become a per-device-type value in the database later.
  const RECOMMENDED_ACCEL_SCALE = 0.6;
  const recommendedAccel = RECOMMENDED_ACCEL_SCALE * deviceInfo.maxForceN / mass;
  return recommendedAccel;
}

const recalculateRecommendedAccel: Reducer<ActionTypes.RECALCULATE_RECOMMENDED_ACCEL> = (state, tuning) => {
  const recommendedAccel = state.deviceInfo ? calculateRecommendedAccel(state.deviceInfo, tuning) : null;
  return {
    ...state,
    deviceInfo: state.deviceInfo ? { ...state.deviceInfo, recommendedAccel } : null,
  };
};

function updateSettingOnDeviceSettings(
  deviceInfo: State['deviceInfo'], setting: keyof ServoTuningSettings, update: Partial<ServoTuningSetting>
): State['deviceInfo'] {
  if (deviceInfo == null) { return null }
  return {
    ...deviceInfo,
    settings: {
      ...deviceInfo.settings,
      [setting]: { ...deviceInfo.settings[setting], ...update }
    }
  };
}

const updateSetting: Reducer<ActionTypes.UPDATE_SETTING> = (state, { setting, update }) => {
  const { value: measure, mode } = update;
  if (mode === 'display') {
    return { ...state, deviceInfo: updateSettingOnDeviceSettings(state.deviceInfo, setting, { measure, mode, error: null }) };
  } else {
    return { ...state, deviceInfo: updateSettingOnDeviceSettings(state.deviceInfo, setting, { measure, mode }) };
  }
};

const updateSettingError: Reducer<ActionTypes.UPDATE_SETTING_ERROR> = (state, { setting, error }) => ({
  ...state, deviceInfo: updateSettingOnDeviceSettings(state.deviceInfo, setting, { error })
});

const updateParamTuning: Reducer<ActionTypes.UPDATE_PARAM_TUNING> = (state, { tuning }) => ({ ...state, tuning });

const applyAdvanced: Reducer<ActionTypes.APPLY_ADVANCED> = (state, { paramset, tuning }) => ({
  ...state,
  tuning: { method: 'advanced', paramset, tuning, message: { status: 'writing' } }
});

const applyPid: Reducer<ActionTypes.APPLY_PID> = (state, { paramset, tuning }) => ({
  ...state,
  tuning: { method: 'pid', paramset, tuning, message: { status: 'writing' } }
});

const applySimple: Reducer<ActionTypes.APPLY_SIMPLE> = (state, { paramset, tuning }) => ({
  ...state,
  tuning: { method: 'simple', paramset, supported: true, tuning, message: { status: 'writing' } }
});

const applyDone: Reducer<ActionTypes.APPLY_DONE> = (state, { toLive, error }) => {
  const message: TuningMessage = error ?
    { status: 'error', message: `Could not write this tuning: ${error}` } :
    { status: 'success', toLive };
  return { ...state, tuning: state.tuning && { ...state.tuning, message } };
};

const setMessage: Reducer<ActionTypes.SET_MESSAGE> = (state, { message }) => ({ ...state, message });

const importCsvDone: Reducer<ActionTypes.IMPORT_CSV_DONE> = (state, { rows }) => ({ ...state, savedRows: rows });

const addRow: Reducer<ActionTypes.ADD_ROW> = (state, { row }) => ({ ...state, savedRows: [...state.savedRows, row] });

const deleteRow: Reducer<ActionTypes.DELETE_ROW> = (state, { rowIndex }) => {
  const beforeDeletion = state.savedRows.slice(0, rowIndex);
  const iAfter = rowIndex + 1;
  const afterDeletion = iAfter >= state.savedRows.length ? [] : state.savedRows.slice(iAfter);
  return { ...state, savedRows: [...beforeDeletion, ...afterDeletion] };
};

const deleteTable: Reducer<ActionTypes.DELETE_TABLE> = state => ({ ...state, savedRows: [] });

const renameRow: Reducer<ActionTypes.RENAME_ROW> = (state, { rowIndex, name }) => {
  const savedRows = [...state.savedRows];
  savedRows[rowIndex] = { ...savedRows[rowIndex], name };
  return { ...state, savedRows };
};

const setDriverEnabledDone: Reducer<ActionTypes.SET_DRIVER_ENABLED_DONE> = (state, { enable }) => ({
  ...state,
  deviceInfo: state.deviceInfo ? { ...state.deviceInfo, driverEnabled: enable } : null,
});

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.CHOOSE_ENTITY]: chooseEntity,
  [ActionTypes.CHOOSE_TUNING]: chooseTuning,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,

  [ActionTypes.INFO_FETCHED]: infoFetched,

  [ActionTypes.CHANGE_STARTUP_PRESET_DONE]: changeStartupPresetDone,

  [ActionTypes.RECALCULATE_RECOMMENDED_ACCEL]: recalculateRecommendedAccel,
  [ActionTypes.UPDATE_SETTING]: updateSetting,
  [ActionTypes.UPDATE_SETTING_ERROR]: updateSettingError,

  [ActionTypes.SET_MESSAGE]: setMessage,

  [ActionTypes.UPDATE_PARAM_TUNING]: updateParamTuning,
  [ActionTypes.APPLY_ADVANCED]: applyAdvanced,
  [ActionTypes.APPLY_PID]: applyPid,
  [ActionTypes.APPLY_SIMPLE]: applySimple,
  [ActionTypes.APPLY_DONE]: applyDone,

  [ActionTypes.IMPORT_CSV_DONE]: importCsvDone,
  [ActionTypes.ADD_ROW]: addRow,
  [ActionTypes.DELETE_ROW]: deleteRow,
  [ActionTypes.DELETE_TABLE]: deleteTable,
  [ActionTypes.RENAME_ROW]: renameRow,

  [ActionTypes.SET_DRIVER_ENABLED_DONE]: setDriverEnabledDone,
}, initialState);
