import type { ProcessControllerMode, ProcessControllerSource } from '@zaber/motion/product';

import type { EntityKey } from '../../keys';
import type { MeasurementOK, UnitInfo } from '../../units';
import type { ValueUnitState } from '../../units/EditableInputWithUnits';
import type { ProcessControllerSettingNames, SettingsMap } from '../settings';
import type { Reading } from '../types';

export const autoTuneSettings = [
  'process.voltage.on',
  'process.control.setpoint.temperature',
  'process.control.hysteresis.voltage',
  'process.control.hysteresis.temperature',
  'process.control.setpoint.tf',
] as const satisfies readonly ProcessControllerSettingNames[];

type AutoTuneSettingNames = typeof autoTuneSettings extends readonly (infer T)[] ? T : never;

export type AutoTuneSettings = Record<AutoTuneSettingNames, ValueUnitState>;

export function hasAutoTuneSettings(settings: SettingsMap): settings is AutoTuneSettings {
  for (const settingName of autoTuneSettings) {
    if (!(settingName in settings)) {
      return false;
    }
  }
  return true;
}

/** The state of the process before the auto tune process begins */
export type InitialState = {
  mode: ProcessControllerMode;
  source: ProcessControllerSource;
  voltageOn: number;
  minVoltage: number;
  maxVoltage: number;
  pulseVoltage: number;
  pulseDuration: number;
  kp: number;
  ki: number;
  kd: number;
};

export type AutoTuneUnitConvertersUnchecked = {
  'process.voltage.on': UnitInfo;
  'process.control.voltage.max': UnitInfo;
  'process.control.hysteresis.voltage': UnitInfo;
  'process.control.hysteresis.temperature': UnitInfo;
};

export type AutoTuneUnitConverters = {
  [setting in keyof AutoTuneUnitConvertersUnchecked]: Extract<UnitInfo, { supported: true }>;
};

export function autoTuneUnitConvertersValid(converters: AutoTuneUnitConvertersUnchecked): converters is AutoTuneUnitConverters {
  for (const converter of Object.values(converters)) {
    if (!converter.supported) {
      return false;
    }
  }
  return true;
}

export type AutoTuneError = {
  type: 'error';
  message: string;
  details?: string;
} | {
  type: 'invalid-duty-cycle';
  /** The percent of each period spent on the duty cycle */
  dutyCycle: number;
};

type AutoTuneBegin = {
  step: 'begin';
  /** A history of the value of the input  */
  readings: Reading[];
  entity: EntityKey;
  process: number;
  error?: AutoTuneError;
};

type AutoTuneTransition = Omit<AutoTuneBegin, 'step'> & {
  step: 'transition';
  initial: InitialState;
};

export type AutoTuneSetup = Omit<AutoTuneTransition, 'step'> & {
  step: 'setup';
  converters: AutoTuneUnitConverters;
  maxVoltage: MeasurementOK;
  /** The voltage at which the system settles at the setpoint */
  stableVoltage: MeasurementOK;
  criticalGain: number | null;
  criticalFrequency: number | null;
};

/** Information about the state of an identification run */
type AutoTuneIdentificationRun = {
  /** The progress (0-1) of the identification complete */
  progress: number;
  message: string;
  voltage: number;
  /** The hysteresis to use for this identification run */
  hysteresis: MeasurementOK;
};

export type AutoTuneIdentify = Omit<AutoTuneSetup, 'step'> & {
  step: 'identify';
  setpoint: MeasurementOK;
  /** Run state, or null if not running */
  run: AutoTuneIdentificationRun | null;
};

export type AutoTuneTestRun = {
  kp: number;
  ki: number;
  kd: number;
};

export type AutoTuneTest = Omit<AutoTuneIdentify, 'step' | 'run' | 'complete'> & {
  step: 'test';
  criticalFrequency: number;
  criticalGain: number;
};

export type AutoTuneUserStep = AutoTuneSetup | AutoTuneIdentify | AutoTuneTest;
export type AutoTune = AutoTuneBegin | AutoTuneTransition | AutoTuneUserStep;
