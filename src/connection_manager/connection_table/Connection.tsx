import React, { Component } from 'react';
import { AnimationClasses, NoticeMessage, HeaderCard, Icons, NoticeBorder, Text } from '@zaber/react-library';
import classNames from 'classnames';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';
import { tryAccess } from '@zaber/toolbox';

import type { RootState } from '../../store';
import { selectConnections } from '../selectors';
import { actions as actionsDefinition } from '../actions';
import type { ConnectionState } from '../reducer';
import { DEVICE_TABLE_CLASS, ConnectionName } from '../device_table';
import { ConnectionNameInput } from '../../connection_manager_ui/ConnectionNameInput';
import { ConnectionMonitor } from '../../connection_monitor';
import { selectConnectionsError } from '../../connection_manager_ui/selectors';

import { Device } from './Device';
import type { ConnectionPassedProps, ConnectionContext } from './types';

interface Props extends ConnectionPassedProps {
  connectionKey: string;
  expanded?: boolean;
  onExpand?: (expanded: boolean) => void;
  editable?: boolean;
}
interface StateProps {
  connection: ConnectionState;
  connectionsError: ReturnType<typeof selectConnectionsError>;
}
interface DispatchProps {
  actions: typeof actionsDefinition;
}
interface State {
  expanded: boolean;
}

type AllProps = Props & StateProps & DispatchProps;

class ConnectionBase extends Component<AllProps, State> {
  private readonly passedContext: ConnectionContext = {
    setExpanded: (expanded: boolean) => this.setExpanded(expanded),
  };

  constructor(props: AllProps) {
    super(props);
    this.state = {
      expanded: props.expanded ?? false,
    };
  }

  static getDerivedStateFromProps(props: AllProps, state: State): State | null {
    if (props.expanded != null && props.expanded !== state.expanded) {
      return { ...state, expanded: props.expanded };
    }
    return null;
  }

  refresh = () => {
    const { connection, actions } = this.props;
    actions.loadDevices(connection.key, false);
  };

  setExpanded = (expanded: boolean) => {
    const { onExpand, expanded: propsExpanded } = this.props;
    if (propsExpanded == null) {
      this.setState({ expanded });
    }
    onExpand?.(expanded);
  };

  renderCard() {
    const { connection, editable, connectionContent, ...rest } = this.props;
    const { expanded } = this.state;

    const header = <div className="connection-header">
      {editable && <ConnectionNameInput
        connection={connection}
        onExpandConnection={() => this.setExpanded(!expanded)}/>}
      {!editable && <div className="connection-name"
        onClick={() => this.setExpanded(!expanded)}>
        <ConnectionName connection={connection}/>
      </div>}
        &ensp;

      <Icons.Refresh
        title="Refresh Devices"
        className={classNames({ [AnimationClasses.Rotation]: connection.loadingDevices })}
        disabled={connection.loadingDevices}
        activated={connection.loadingDevices}
        onClick={this.refresh}/>
      <div className="spacer"/>
      {connectionContent?.(connection, this.passedContext)}
    </div>;

    return <HeaderCard
      header={header}
      edge="outline"
      collapsible expanded={expanded} onToggle={this.setExpanded}
      titleCollapsed="Expand Connection"
      titleExpanded="Collapse Connection"
    >
      <div className={DEVICE_TABLE_CLASS}>
        {this.renderMessage(connection.devices?.map(deviceKey =>
          <Device key={deviceKey} deviceKey={deviceKey}
            {..._.pick(rest,
              'axisContent',
              'deviceContent',
              'deviceErrorContent',
              'controllerContent',
              'axisErrors',
              'displayMultiAxis',
            )}/>
        )
        )}
      </div>
    </HeaderCard>;
  }

  renderMessage(children: React.ReactNode) {
    const { connection } = this.props;
    const noDevices = (connection.devices?.length ?? 0) ===  0;
    if (connection.loadingDevices && noDevices) {
      return <Text className="no-devices">Loading devices...</Text>;
    } else if (noDevices) {
      return <Text className="no-devices">No devices have been detected. Make sure devices are powered on.</Text>;
    } else {
      return children;
    }
  }

  render(): JSX.Element {
    const { connection, connectionsError } = this.props;
    const { expanded } = this.state;
    const connectionError = tryAccess(connectionsError, connection.key);

    return <div className="connection-with-device">
      <ConnectionMonitor connectionKey={connection.key} enabled={expanded}>
        {monitorError => {
          const error = monitorError ?? connectionError;

          if (error != null) {
            return <NoticeBorder type="error">
              {this.renderCard()}
              <NoticeMessage>{error}</NoticeMessage>
            </NoticeBorder>;
          } else {
            return this.renderCard();
          }
        }}
      </ConnectionMonitor>
    </div>;
  }
}

export const Connection = connect(
  (state: RootState, props: Props): StateProps => ({
    connection: selectConnections(state)[props.connectionKey],
    connectionsError: selectConnectionsError(state),
  }),
  (dispatch: Dispatch): DispatchProps => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(ConnectionBase);
