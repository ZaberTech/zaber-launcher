import * as path from 'path';
import * as fs from 'fs/promises';

import type { SagaIterator } from 'redux-saga';
import {
  all,
  call,
  cancelled,
  fork,
  put,
  race,
  SagaReturnType as SRT,
  select,
  take,
  takeEvery,
  takeLeading,
} from 'redux-saga/effects';
import {
  ascii,
  CommandFailedException,
  SetDeviceStateFailedException,
  SetPeripheralStateFailedException
} from '@zaber/motion';
import { AsyncReturnType, notNil, throwUnexpectedError } from '@zaber/toolbox';
import _ from 'lodash';
import moment, { duration } from 'moment';

import type { Action, AsyncReturnType as ART, EmptyAction, RT, SagaIter } from '../utils';
import {
  ConnectionManagerActionTypes,
  getDevice,
  isIdentified,
  IdentifiedDeviceInfo,
  DevicesLoadedAction,
  DeviceInfoWithAxes,
  selectDevicesWithAxes,
  reloadDevices,
} from '../connection_manager';
import { EntityKey, extractConnectionKey } from '../keys';
import { getContainer } from '../container';
import { Cache } from '../cache';
import { DeviceIdentifiers, FirmwareUpgradeStreamer, FirmwareWebQuery, Storage, VersionList } from '../app_components';
import { handleNonCriticalError } from '../errors';
import { Editions, environment } from '../environment';
import { getSerialNumberInfo, isSerialNumberValid, SerialNumberInfo, waitForDeviceToRestart } from '../devices';
import { Dialogs } from '../dialogs';
import { Paths } from '../paths_util';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import {
  CachedConnectionFirmwareUpgradeInfo,
  CachedDeviceFirmwareUpgradeInfo,
  VersionSkipMap,
  LONG_RUNNING_COMMAND_TIMEOUT,
  InstallParams
} from './types';
import { canUpgrade, compareVersions, isInternal, makeIdentifiers } from './utility';
import { selectDeviceIdentifiers, selectDeviceInfoWithAxes } from './selectors';

/** How long to hold on to the cache in milliseconds */
const CACHE_EXPIRY_WINDOW = duration(2, 'days').asMilliseconds();
const SKIPPED_VERSIONS_STORAGE_KEY = 'FWU_SKIPPED_VERSIONS';

let loadFirmwareVersions = !environment.isTest;
export function testLoadingOfFirmwareVersions() {
  if (!environment.isTest) { throw new Error('Can only be called in tests') }
  loadFirmwareVersions = true;
}

export function* firmwareUpdateSaga(): SagaIterator {
  yield all([
    !environment.isTest && !environment.offline ? fork(loadVersionsSkipped) : null,
    loadFirmwareVersions ? takeEvery(ConnectionManagerActionTypes.DEVICES_LOADED, devicesLoaded) : null,
    takeLeading(ActionTypes.BEGIN_SELF_SERVE_UPGRADE, beginSelfServeUpgrade),
    takeLeading(ActionTypes.BEGIN_FROM_FILE_UPGRADE, beginFromFileUpgrade),
    takeLeading(ActionTypes.INSTALL_FIRMWARE, takeInstallDispatch),
    takeLeading(ActionTypes.REFRESH_VERSIONS, refreshVersions),
    takeEvery(ActionTypes.OPEN_FILE, openFile),
    takeEvery(ActionTypes.SKIP_VERSION, skipVersion),
  ].filter(notNil));
}

function* openFile() {
  const device: SRT<typeof selectDeviceInfoWithAxes> = yield select(selectDeviceInfoWithAxes);
  const identifiers: SRT<typeof selectDeviceIdentifiers> = yield select(selectDeviceIdentifiers);
  if (device == null || identifiers == null) { return }

  const dialogResult: SRT<typeof Dialogs.showOpenDialog> = yield call(Dialogs.showOpenDialog, {
    title: 'Open Firmware Upgrade File',
    filters: [{ name: 'Firmware Upgrade File', extensions: ['fwu'] }, { name: 'All Files', extensions: ['*'] }],
  });
  const file = dialogResult.filePaths[0];
  if (file != null) {
    yield put(actions.beginInstall(device.key, identifiers, file));
  }
}

function cacheHit(identifiers: DeviceIdentifiers, cache: CachedConnectionFirmwareUpgradeInfo | null) {
  const cachedDevice = cache?.info?.[identifiers.DeviceNumber];
  if (!cachedDevice) {
    return false;
  }
  return (
    cachedDevice.identifiers.DeviceNumber === identifiers.DeviceNumber &&
    cachedDevice.identifiers.PlatformID === identifiers.PlatformID &&
    cachedDevice.identifiers.SerialNumber === identifiers.SerialNumber
  );
}

function buildCacheData(
  setAtTimestamp: number,
  identifiersList: DeviceIdentifiers[],
  versionsList: VersionList,
): CachedConnectionFirmwareUpgradeInfo {
  const info: Record<string, CachedDeviceFirmwareUpgradeInfo> = {};
  for (const identifiers of identifiersList) {
    const address = identifiers.DeviceNumber;
    const versions = versionsList[address];
    info[address] = { identifiers, versions };
  }
  return { setAtTimestamp, info };
}

function* loadVersionsSkipped() {
  const storage = getContainer().get(Storage);

  const versionsSkipped = storage.load<VersionSkipMap>(SKIPPED_VERSIONS_STORAGE_KEY) ?? {};
  yield put(actions.setVersionsSkipped(versionsSkipped));
}

type SkipVersionActionPayload = Action<ActionsToPayloads[ActionTypes.SKIP_VERSION]>;
function* skipVersion({ payload: { deviceSerialNumber, version } }: SkipVersionActionPayload) {
  const storage = getContainer().get(Storage);

  const versionsSkipped = storage.load<VersionSkipMap>(SKIPPED_VERSIONS_STORAGE_KEY) ?? {};
  const currentlySkippedVersion = versionsSkipped[deviceSerialNumber];
  if (compareVersions(currentlySkippedVersion, version) < 0) {
    versionsSkipped[deviceSerialNumber] = version;
  }
  storage.save(SKIPPED_VERSIONS_STORAGE_KEY, versionsSkipped);
  yield put(actions.setVersionsSkipped(versionsSkipped));
}

function* devicesLoaded({ payload: { connectionKey, devices } }: DevicesLoadedAction): SagaIterator {
  yield call(loadVersions, connectionKey, devices, false, 'background');
}

function* loadVersions(
  connectionKey: EntityKey, devices: DeviceInfoWithAxes[],
  ignoreCache: boolean, ctx: 'background' | 'ui',
): SagaIterator {
  if ((environment.edition === Editions.Public || environment.isTest) && devices.some(isInternal)) {
    yield put(actions.firmwareVersionsErr(
      connectionKey,
      'Updating Zaber Launcher Internal devices is not supported by Zaber Launcher Public edition.',
    ));
    return;
  }

  const upgradeableDevices = devices.filter(isIdentified).filter(canUpgrade);
  const fwuIdentifiers = upgradeableDevices.map(makeIdentifiers);

  if (environment.offline) {
    yield put(actions.firmwareVersionsLoaded(connectionKey, fwuIdentifiers, {}));
    return;
  }

  const cache = getContainer().get(Cache);
  const cacheKey = `${connectionKey}_available_firmware_versions`;
  const cachedUpgradeInfo = cache.load<CachedConnectionFirmwareUpgradeInfo>(cacheKey);
  const useCache = !ignoreCache && cachedUpgradeInfo != null && Date.now() - cachedUpgradeInfo.setAtTimestamp < CACHE_EXPIRY_WINDOW;

  try {
    const [cachedIdentifiers, uncachedIdentifiers] = _.partition(fwuIdentifiers, identifiers =>
      useCache && cacheHit(identifiers, cachedUpgradeInfo));

    const cachedVersions: VersionList = {};
    for (const identifier of cachedIdentifiers) {
      const address = identifier.DeviceNumber;
      cachedVersions[address] = cachedUpgradeInfo!.info[address].versions;
    }

    const api = getContainer().get<FirmwareWebQuery>(FirmwareWebQuery);
    const uncachedVersions: AsyncReturnType<typeof api.getVersions> = yield call([api, api.getVersions], uncachedIdentifiers);

    const versions = { ...cachedVersions, ...uncachedVersions };
    yield put(actions.firmwareVersionsLoaded(connectionKey, fwuIdentifiers, versions));

    if (uncachedIdentifiers.length > 0) {
      const timestamp = useCache ? cachedUpgradeInfo!.setAtTimestamp : Date.now();
      cache.save(cacheKey, buildCacheData(timestamp, fwuIdentifiers, versions));
    }
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.firmwareVersionsErr(connectionKey, e.message));
    if (environment.edition === Editions.Public && ctx === 'ui') {
      handleNonCriticalError(e, 'warning', { fwuIdentifiers });
    }
  }
}

type SelfServeParams = Action<ActionsToPayloads[ActionTypes.BEGIN_SELF_SERVE_UPGRADE]>;

function* beginSelfServeUpgrade(params: SelfServeParams) {
  const checkDevice: { deviceIsValid?: boolean } = yield race({
    deviceIsValid: call(deviceIsValid, params),
    cancel: take(ActionTypes.CANCEL_INSTALL_NEW_FIRMWARE),
  });

  if (!checkDevice.deviceIsValid) {
    return;
  }

  const downloadResult: { filePath: string | null | undefined } = yield race({
    filePath: call(downloadNewFirmware, params),
    cancel: take(ActionTypes.CANCEL_INSTALL_NEW_FIRMWARE),
  });
  if (downloadResult.filePath == null) { return }

  const installParams = { ...params.payload, filePath: downloadResult.filePath };
  if (params.payload.offerSaveState) {
    yield put(actions.setInstallProgress({ step: 'ask-if-save', installParams }));
  } else {
    yield put(actions.installFirmware(installParams, false));
  }
}

type FromFileUpgradeParams = Action<ActionsToPayloads[ActionTypes.BEGIN_FROM_FILE_UPGRADE]>;

function* beginFromFileUpgrade(params: FromFileUpgradeParams) {
  yield put(actions.setInstallProgress({ step: 'ask-if-save', installParams: params.payload }));
}

interface UpgradeContext {
  device: ascii.Device;
  installParams: InstallParams;
  saveParams: SaveParams | null;
  commNextOwnerInitial: CommNextOwner | null;
  serialNumberInfo: SerialNumberInfo;
}

function* takeInstallDispatch(params: Action<ActionsToPayloads[ActionTypes.INSTALL_FIRMWARE]>) {
  const { installParams, withSave } = params.payload;
  const deviceInfo: IdentifiedDeviceInfo = yield select(selectDeviceInfoWithAxes);

  let device: ascii.Device;
  let ctx: UpgradeContext;
  try {
    yield put(actions.setInstallProgress({ step: 'connecting' }));
    device = yield call(getDevice, installParams.deviceKey);
    const serialNumberInfo: RT<typeof getSerialNumberInfo> = yield call(getSerialNumberInfo, device);

    ctx = {
      installParams,
      saveParams: null,
      commNextOwnerInitial: null,
      device,
      serialNumberInfo,
    };
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setInstallProgress({ step: 'failed', error: err.message, wasReset: false }));
    return;
  }

  if (withSave) {
    if (ctx.serialNumberInfo.count > 1) {
      yield put(actions.setInstallProgress({
        step: 'could-not-save',
        installParams,
        error: `There are multiple devices with serial number ${ctx.serialNumberInfo.number}. We do not support saving in this case.`,
      }));
      return;
    }
    try {
      yield put(actions.setInstallProgress({ step: 'saving-state' }));
      const deviceState: SRT<typeof device.getState> = yield call([device, device.getState]);
      if (installParams.version != undefined) {
        const canApplyAfterUpgrade: RT<typeof device.canSetState> = yield call(
          [device, device.canSetState], deviceState, { firmwareVersion: installParams.version.parsed }
        );
        if (canApplyAfterUpgrade.error != null || canApplyAfterUpgrade.axisErrors.length > 0) {
          const message = canApplyAfterUpgrade.error ?? canApplyAfterUpgrade.axisErrors
            .filter(e => e.error != null)
            .map(e => `"${e.error}" on axis ${e.axisNumber}`)
            .join(' and ');
          yield put(actions.setInstallProgress({
            step: 'could-not-save',
            installParams,
            error: [
              `After the upgrade to ${installParams.version.str}, Zaber Launcher will be unable to recreate your device's current state`,
              `because ${message}.`,
              'If you would like to persist the state of this device, please upgrade to the newest version of Launcher and try again.',
            ].join(' '),
          }));
          return;
        }
      }
      const peripherals = deviceInfo.axes.map(a => ({ axisNumber: a.axisNumber, id: a.identity.peripheralId }));
      const fileName = `device-${device.identity.serialNumber}-${moment().format('YYYY-MM-DD-HHmm')}`;
      const dirPath = path.join(Paths.CACHE, 'pre-upgrade-saves');
      const fullPath = path.join(dirPath, fileName);
      ctx.saveParams = { state: deviceState, path: fullPath, peripherals };
      yield call(fs.mkdir, dirPath, { recursive: true });
      yield call(fs.writeFile, fullPath, deviceState);
    } catch (caught) {
      const err = handleNonCriticalError(caught);
      yield put(actions.setInstallProgress({ step: 'could-not-save', installParams, error: err.message }));
      return;
    }
  }

  yield call(installFirmware, ctx);
}

const WAIT_FOR_RESTART_TIMEOUT_MS = duration(3, 'minutes').asMilliseconds();

function* reloadDevicesAndFindUpgradedDevice(
  oldDeviceKey: EntityKey,
  snInfo: SerialNumberInfo,
): SagaIterator<IdentifiedDeviceInfo | null> {
  const connectionKey = extractConnectionKey(oldDeviceKey);
  const devices: SRT<typeof reloadDevices> = yield call(reloadDevices, connectionKey);

  if (snInfo.count > 1) { return null }
  const upgradedDevice = devices?.find(device => device.identity?.serialNumber === snInfo.number);
  if (!upgradedDevice) { return null }

  if (upgradedDevice.key !== oldDeviceKey) {
    yield put(actions.setSelectedDevice(upgradedDevice.key));
  }

  return upgradedDevice as IdentifiedDeviceInfo;
}

function* restoreDeviceState(ctx: UpgradeContext) {
  const { device } = ctx;
  const { state, peripherals, path: pathToSaveFile } = ctx.saveParams!;
  yield put(actions.setInstallProgress({ step: 'reloading-state' }));

  try {
    for (const { axisNumber, id } of peripherals) {
      const axis = device.getAxis(axisNumber);
      if (id !== 0 && axis.peripheralId !== id) {
        yield call([axis.settings, axis.settings.set], 'peripheral.id', id);
      }
    }
  } catch (err) {
    throwUnexpectedError(err);
    const error = `Could not restore peripheral IDs: ${err.message}`;
    yield put(actions.setInstallProgress({ step: 'failed-reload', pathToSaveFile, error }));
    return;
  }

  let setStateResponse: RT<typeof device.setState> | undefined = undefined;
  try {
    setStateResponse = yield call([device, device.setState], state);
  } catch (err) {
    throwUnexpectedError(err);
    const error = err.message;
    const details = (err instanceof SetDeviceStateFailedException || err instanceof SetPeripheralStateFailedException)
      ? err.details
      : undefined;
    yield put(actions.setInstallProgress({ step: 'failed-reload', error, pathToSaveFile, details }));
    return;
  }

  yield reloadDevices(extractConnectionKey(ctx.installParams.deviceKey));
  yield put(actions.setInstallProgress({ step: 'done', version: ctx.installParams.version, setStateResponse }));
}

function* deviceIsValid({ payload: { deviceKey } }: SelfServeParams) {
  try {
    yield put(actions.setInstallProgress({ step: 'checking-device' }));
    const device: SRT<typeof getDevice> = yield call(getDevice, deviceKey);
    const lockstepGroups: number = yield call([device.settings, device.settings.get], 'lockstep.numgroups');

    for (let i = 1; i <= lockstepGroups; i++) {
      const lockstep = device.getLockstep(i);
      const enabled: boolean = yield call([lockstep, lockstep.isEnabled]);
      if (enabled) {
        const axes: number[] = yield call([lockstep, lockstep.getAxisNumbers]);
        const length = axes.length;
        const axesList = `axes ${axes.slice(0, length - 1).join(', ')} and ${axes[length - 1]}`;
        const error = `The device has lockstep enabled on ${axesList}.
          Please mechanically decouple and disable the lockstep before upgrading.`;
        throw new Error(error);
      }
    }
    return true;
  } catch (e) {
    if (e instanceof CommandFailedException) {
      return true;
    }
    throwUnexpectedError(e);
    yield put(actions.setInstallProgress({ step: 'failed', error: e.message, wasReset: false }));
    return false;
  } finally {
    if ((yield cancelled()) as boolean) {
      yield put(actions.setInstallProgress({ step: 'cancelled' }));
    }
  }
}

function* downloadNewFirmware({ payload: { identifiers, version } }: SelfServeParams) {
  try {
    yield put(actions.setInstallProgress({ step: 'downloading-fwu' }));
    const api = getContainer().get<FirmwareWebQuery>(FirmwareWebQuery);
    const fwuFile: ART<FirmwareWebQuery['getUpdatePackage']> = yield call([api, api.getUpdatePackage], version, identifiers);
    return fwuFile;
  } catch (caught) {
    const e = handleNonCriticalError(caught);
    yield put(actions.setInstallProgress({ step: 'failed', error: `Downloading the upgrade file failed: ${e.message}`, wasReset: false }));
    return null;
  } finally {
    if ((yield cancelled()) as boolean) {
      yield put(actions.setInstallProgress({ step: 'cancelled' }));
    }
  }
}

interface SaveParams {
  state: string;
  path: string;
  peripherals: {
    axisNumber: number;
    id: number;
  }[];
}

enum CommNextOwner {
  Automatic = 0,
  RS232,
  USB,
  Ethernet
}

async function getCommNextOwner(device: ascii.Device): Promise<null | CommNextOwner> {
  const { firmwareVersion } = device;
  if (firmwareVersion.major === 7 && firmwareVersion.minor === 23)  {
    // In version 7.23 setting comm.next.owner cause temporary communication failure.
    return null;
  }
  try {
    return await device.settings.get('comm.next.owner');
  } catch (err) {
    if (!(err instanceof CommandFailedException)) {
      throw err;
    }
    return null;
  }
}

function* streamFirmware(ctx: UpgradeContext): SagaIter {
  const { device, installParams: { identifiers, filePath } } = ctx;
  yield put(actions.setInstallProgress({ step: 'preparing-stream' }));

  ctx.commNextOwnerInitial = yield getCommNextOwner(device);
  if (ctx.commNextOwnerInitial != null) {
    yield device.settings.set('comm.next.owner', CommNextOwner.RS232);
  }

  const byteStreamer = new FirmwareUpgradeStreamer(device, identifiers, filePath);
  const bytesTotal: number = yield call([byteStreamer, byteStreamer.beginUpgrade]);
  let bytesWritten: AsyncReturnType<typeof byteStreamer.stream> = 0;
  let progressLastUpdatedTime = 0;
  const startTime = performance.now();

  while (bytesWritten !== 'done') {
    const now = performance.now();
    if (now - progressLastUpdatedTime >= 1000) {
      progressLastUpdatedTime = now;
      const elapsed = now - startTime;
      const bytesPerSecond = bytesWritten / elapsed;
      const remainingTime = bytesPerSecond > 0 ? ((bytesTotal - bytesWritten) / bytesPerSecond) / 1000 : undefined;
      yield put(actions.setInstallProgress({ step: 'streaming', bytesWritten, bytesTotal, remainingTime }));
    }
    bytesWritten = yield call([byteStreamer, byteStreamer.stream]);
  }
}

function* installFirmware(ctx: UpgradeContext): SagaIter {
  const { installParams: { deviceKey } } = ctx;

  let failedWithError: Error | null = null;
  let wasCanceled = false;
  try {
    const { cancelAction }: { cancelAction: EmptyAction } = yield race({
      stream: call(streamFirmware, ctx),
      cancelAction: take(ActionTypes.CANCEL_INSTALL_NEW_FIRMWARE),
    });
    if (cancelAction) {
      wasCanceled = true;
      yield put(actions.setInstallProgress({ step: 'canceling' }));
    } else {
      yield put(actions.setInstallProgress({ step: 'applying' }));
    }
  } catch (caught) {
    const err = handleNonCriticalError(caught, 'info');
    failedWithError = err;
    yield put(actions.setInstallProgress({ step: 'failing' }));
  }

  try {
    yield ctx.device.genericCommand('system reset', LONG_RUNNING_COMMAND_TIMEOUT);
    const deviceAfterRestart: RT<typeof waitForDeviceToRestart> =
      yield waitForDeviceToRestart(deviceKey, ctx.serialNumberInfo, WAIT_FOR_RESTART_TIMEOUT_MS);
    if (deviceAfterRestart === 'timeout') {
      yield put(actions.setInstallProgress({ step: 'time_out' }));
      return;
    } else if (deviceAfterRestart === 'many') {
      if (ctx.serialNumberInfo.count === 1) {
        throw new Error('Multiple devices with the same serial number were unexpectedly found.');
      }
    } else {
      ctx.device = deviceAfterRestart;

      if ((failedWithError != null || wasCanceled) && ctx.commNextOwnerInitial != null) {
        yield ctx.device.settings.set('comm.next.owner', ctx.commNextOwnerInitial);
      }
    }
  } catch (caught) {
    const err = handleNonCriticalError(caught, 'info');
    if (failedWithError == null) {
      failedWithError = err;
    }
  }

  if (failedWithError != null) {
    yield put(actions.setInstallProgress({ step: 'failed', error: failedWithError.message, wasReset: true }));
    return;
  } if (wasCanceled) {
    yield put(actions.setInstallProgress({ step: 'cancelled' }));
    return;
  }

  const newDeviceInfo: SRT<typeof reloadDevicesAndFindUpgradedDevice> =
    yield reloadDevicesAndFindUpgradedDevice(deviceKey, ctx.serialNumberInfo);
  if (newDeviceInfo != null) {
    yield put(actions.setInstallProgress({ step: 'reporting-success' }));
    yield call(reportUpgradeSuccess, newDeviceInfo, ctx.installParams);
  }

  if (ctx.saveParams) {
    yield restoreDeviceState(ctx);
  } else {
    yield put(actions.setInstallProgress({ step: 'done', version: ctx.installParams.version }));
  }
}

function* reportUpgradeSuccess(device: IdentifiedDeviceInfo, params: InstallParams): SagaIter {
  try {
    const { firmwareVersion, serialNumber } = device.identity;
    if (!isSerialNumberValid(serialNumber)) {
      return;
    }

    const isUnlock = (params.version?.str ?? '').toLowerCase().includes('unlock')
      || (params.filePath ?? '').toLowerCase().includes('unlock');

    const api = getContainer().get<FirmwareWebQuery>(FirmwareWebQuery);
    yield call([api, api.reportInstall], [{
      ...makeIdentifiers(device),
      FWVersion: isUnlock ? 'unlock ONLY (UNLOCK)' : `${firmwareVersion.major}.${firmwareVersion.minor}`,
      BuildNumber: firmwareVersion.build,
    }]);
  } catch (err) {
    throwUnexpectedError(err);
    handleNonCriticalError(err, 'info');
  }
}

function* refreshVersions({ payload: { deviceKey } }: Action<ActionsToPayloads[ActionTypes.REFRESH_VERSIONS]>): SagaIterator {
  const connectionKey = extractConnectionKey(deviceKey);
  const devices: ReturnType<typeof selectDevicesWithAxes> = yield select(selectDevicesWithAxes);
  const connectionDevices = _.filter(devices, device => extractConnectionKey(device.key) === connectionKey);
  yield call(loadVersions, connectionKey, connectionDevices, true, 'ui');
}
