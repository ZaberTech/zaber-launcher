import React, { Component, useMemo } from 'react';
import { Colors } from '@zaber/react-library';
import { Angle, Units } from '@zaber/motion';
import { match } from 'ts-pattern';

import type { MotionInfo } from '../types';
import { convertNullable, convertValueUnits, getUnitLabel, useUnitInfo } from '../../units';
import { roundToFixed } from '../../utils';

import type { AxisMovementState } from './types';

interface PolarCoord {
  /** The distance from the center in units of rem */
  r: number;
  theta: number;
}

const TAU = 2 * Math.PI;
const TRACK_WIDTH = 0.2;
const TRACK_RADIUS = 3.25;

const MARKER_RADIUS = 0.5;

const SVG_OVERFLOW = 0.5;
const SVG_RADIUS = TRACK_RADIUS + SVG_OVERFLOW;
const SVG_SIZE = SVG_RADIUS * 2;

export interface Props {
  motion: MotionInfo;
  axis: AxisMovementState;
  /** The position in native units */
  position: number | null;
  onRequestMovement: (positionNative: number) => void;
}

export interface State {
  moveOnClick: number | null;
  positionRadians: number | null;
}

type MouseOrTouch = React.MouseEvent<SVGElement> | React.TouchEvent<SVGElement>;

function getClientCoords(e: MouseOrTouch): {x: number; y: number } {
  if ('changedTouches' in e) {
    return { x: e.changedTouches[0].clientX, y: e.changedTouches[0].clientY };
  } else {
    return { x: e.clientX, y: e.clientY };
  }
}

export class TrackCyclic extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      moveOnClick: null,
      positionRadians: props.position && props.position * TAU / props.motion.travel,
    };
  }

  static getDerivedStateFromProps(props: Props, state: State): Partial<State> {
    const positionRadians = props.position && props.position * TAU / props.motion.travel;
    return {
      moveOnClick: state.moveOnClick,
      positionRadians
    };
  }

  makeMovePlan = (from: number, to: number): number | null => {
    const { motion } = this.props;
    const fromMod = from % TAU;
    const clockwiseMove = to > fromMod ? to - fromMod : to - fromMod + TAU;
    const widdershinsMove = to < fromMod ? to - fromMod : to - fromMod - TAU;
    const clockwiseEndPos = (from + clockwiseMove) * motion.travel / TAU;
    const widdershinsEndPos = (from + widdershinsMove) * motion.travel / TAU;
    if (clockwiseEndPos > motion.max && widdershinsEndPos < motion.min) {
      return null;
    } else if (clockwiseEndPos > motion.max) {
      return from + widdershinsMove;
    } else if (widdershinsEndPos < motion.min) {
      return from + clockwiseMove;
    } else if (clockwiseMove < Math.PI) {
      return from + clockwiseMove;
    } else {
      return from + widdershinsMove;
    }
  };

  moveToHoverTarget = () => {
    const { motion, onRequestMovement } = this.props;
    const { moveOnClick } = this.state;
    if (moveOnClick != null) {
      const toNative = moveOnClick * motion.travel / TAU;
      onRequestMovement(toNative);
    }
  };

  getBoundingRect(e: MouseOrTouch) {
    const svg = e.currentTarget as SVGElement;
    return svg.getBoundingClientRect();
  }

  getClientPolarCoords(e: MouseOrTouch): PolarCoord {
    const coords = getClientCoords(e);
    const rect = this.getBoundingRect(e);
    const x = coords.x - rect.width / 2 - rect.left;
    const y = coords.y - rect.height / 2 - rect.top;
    const r = Math.sqrt(x * x + y * y) * SVG_SIZE / rect.width;
    const theta = Math.atan2(x, -y);
    return { r, theta };
  }

  onTrackHover = (e: MouseOrTouch) => {
    const { positionRadians } = this.state;
    const coords = this.getClientPolarCoords(e);
    if (positionRadians == null || coords.r > TRACK_RADIUS + 1 || coords.r < 0.75) {
      this.setState({ moveOnClick: null });
    } else {
      this.setState({ moveOnClick: this.makeMovePlan(positionRadians, coords.theta) });
    }
  };

  hoverEnd = () => {
    this.setState({ moveOnClick: null });
  };

  onMouseUp = (e: React.MouseEvent<HTMLOrSVGElement>) => {
    if (e.button !== 0) {
      return;
    }
    this.moveToHoverTarget();
  };

  touchEnd = (e: React.TouchEvent<HTMLOrSVGElement>) => {
    this.moveToHoverTarget();
    this.setState({ moveOnClick: null });
    // by default, chrome simulates a mousemove after every touchend. If allowed, this will leave the controls in an activated state.
    e.preventDefault();
  };

  render(): React.ReactNode {
    const { axis, motion } = this.props;

    const { moveOnClick, positionRadians } = this.state;

    return (
      <svg
        viewBox={`-${SVG_RADIUS} -${SVG_RADIUS} ${SVG_SIZE} ${SVG_SIZE}`} style={{ width: `${SVG_SIZE}rem` }}
        className="track rotary" data-testid="interactive-cyclic-track"
        color={moveOnClick != null ? Colors.zaberGrey : Colors.lightGrey}
        onMouseMove={this.onTrackHover} onMouseUp={this.onMouseUp} onMouseLeave={this.hoverEnd}
        onTouchStart={this.onTrackHover} onTouchMove={this.onTrackHover} onTouchEnd={this.touchEnd}
      >
        {/* The Track */}
        <circle x={0} y={0} r={TRACK_RADIUS} stroke="currentcolor" strokeWidth={TRACK_WIDTH} fill="none"/>
        {/* The Position Indicator */}
        {positionRadians != null && (
          <circle
            cx={TRACK_RADIUS * Math.sin(positionRadians)}
            cy={-TRACK_RADIUS * Math.cos(positionRadians)}
            r={MARKER_RADIUS}
            fill="currentcolor"
          />
        )}
        {positionRadians != null && <TrackHoverInfo from={positionRadians} to={moveOnClick} axis={axis}/>}
        <MoveTarget axis={axis} to={moveOnClick} motion={motion}/>
        {this.props.position != null && <CycleEdgeMarkers motion={motion} position={this.props.position}/>}
      </svg>
    );
  }
}

const INSIDE_MARKER = TRACK_RADIUS - MARKER_RADIUS;
const OUTSIDE_MARKER = TRACK_RADIUS + MARKER_RADIUS;
const PLAN_RADIUS = INSIDE_MARKER - 0.25;
const ARROW_INNER_RADIUS = PLAN_RADIUS - 0.25;
const ARROW_OUTER_RADIUS = PLAN_RADIUS + 0.275;
const TEXT_RADIUS = PLAN_RADIUS / 2;
const TrackHoverInfo: React.FC<{ from: number; to: number | null; axis: AxisMovementState }> = ({ from, to, axis }) => {
  const unitInfo = useUnitInfo({ entity: axis.key, setting: 'pos' });
  if (to == null) {
    return null;
  }
  const move = to - from;
  const dir = move > 0 ? 1 : -1;
  const toDisplay = !unitInfo?.supported || axis.posUnit === Angle.rad ?
    `${roundToFixed(to / Math.PI, 2)}π` :
    `${roundToFixed(convertValueUnits(unitInfo, to, Angle.rad, axis.posUnit), 2)}${getUnitLabel(axis.posUnit)}`;
  const fromUnitX = Math.sin(from);
  const fromUnitY = -Math.cos(from);
  const toUnitX = Math.sin(to);
  const toUnitY = -Math.cos(to);
  const arrowheadUnitX = Math.sin(to - dir * 0.2);
  const arrowheadUnitY = -Math.cos(to - dir * 0.2);
  return <>
    {/* The Target Position Line */}
    <line
      x1={0} x2={TRACK_RADIUS * toUnitX}
      y1={0} y2={TRACK_RADIUS * toUnitY}
      stroke={Colors.lightGrey} strokeWidth={0.05} strokeDasharray="0.3 0.2"
    />
    {/* Line connecting the Current Position Marker to the Target Position Line */}
    <path d={`
      M ${INSIDE_MARKER * fromUnitX} ${INSIDE_MARKER * fromUnitY}
      L ${PLAN_RADIUS * fromUnitX} ${PLAN_RADIUS * fromUnitY}
      A ${PLAN_RADIUS} ${PLAN_RADIUS}
        0 ${Math.abs(move) > Math.PI ? 1 : 0} ${dir > 0 ? 1 : 0}
        ${PLAN_RADIUS * arrowheadUnitX} ${PLAN_RADIUS * arrowheadUnitY}
    `} stroke={Colors.lightGrey} strokeWidth={0.1} fill="none"/>
    {/* An arrowhead pointing to the Target Position Line */}
    <path d={`
      M ${ARROW_OUTER_RADIUS * arrowheadUnitX} ${ARROW_OUTER_RADIUS * arrowheadUnitY}
      A ${PLAN_RADIUS} ${PLAN_RADIUS} 0 0 ${dir > 0 ? 1 : 0} ${PLAN_RADIUS * toUnitX} ${PLAN_RADIUS * toUnitY}
      A ${PLAN_RADIUS} ${PLAN_RADIUS} 0 0 ${dir > 0 ? 0 : 1} ${ARROW_INNER_RADIUS * arrowheadUnitX} ${ARROW_INNER_RADIUS  * arrowheadUnitY}
      Z
    `} stroke="none" fill={Colors.lightGrey}/>
    {/* The Target Position Readout */}
    <text
      x={TEXT_RADIUS * toUnitX} y={TEXT_RADIUS * toUnitY}
      textAnchor="middle" dominantBaseline="middle"
      fill={Colors.grey} fontSize={0.75}
    >{toDisplay}</text>
  </>;
};

const MOVE_TARGET_OUTER_RADIUS = TRACK_RADIUS + 0.5;
const MoveTarget: React.FC<{ axis: AxisMovementState; to: number | null; motion: MotionInfo}> = ({ axis, to, motion }) => {
  const unitInfo = useUnitInfo({ entity: axis.key, setting: 'pos' });
  const moveTo = match(axis.lastCommand)
    .with({ type: 'abs' }, cmd => {
      const pos = convertNullable(unitInfo, cmd.position, cmd.units ?? Units.NATIVE, Units.NATIVE);
      return pos && pos * TAU / motion.travel;
    })
    .otherwise(() => to);

  if (moveTo == null) {
    return null;
  }

  return <path d={`
    M ${TRACK_RADIUS * Math.sin(moveTo)} ${TRACK_RADIUS * -Math.cos(moveTo)}
    L ${MOVE_TARGET_OUTER_RADIUS * Math.sin(moveTo + 0.05)} ${MOVE_TARGET_OUTER_RADIUS * -Math.cos(moveTo + 0.05)}
    L ${MOVE_TARGET_OUTER_RADIUS * Math.sin(moveTo - 0.05)} ${MOVE_TARGET_OUTER_RADIUS * -Math.cos(moveTo - 0.05)}
    Z
  `} stroke={Colors.lightGrey} strokeWidth="0.2" fill="none"/>;
};

const CycleEdgeMarkers: React.FC<{ motion: MotionInfo; position: number }> = ({ motion, position }) => {
  const { minRadians, maxRadians } = useMemo(() => ({
    minRadians: motion.min * TAU / motion.travel, maxRadians: motion.max * TAU / motion.travel
  }), [motion.min, motion.max, motion.travel]);
  const cycleStartPosRadians = Math.floor(position / motion.travel) * TAU;
  const cycleEndPosRadians = Math.ceil(position / motion.travel) * TAU;

  const displayAbsoluteStart = cycleStartPosRadians <= minRadians;
  const displayAbsoluteEnd = cycleEndPosRadians >= maxRadians;

  return <>
    {(!displayAbsoluteStart || !displayAbsoluteEnd) && <path d={`
      M 0 -${INSIDE_MARKER}
      L 0 -${OUTSIDE_MARKER}
    `} stroke="currentcolor" strokeWidth={0.1} fill="none"/>}
    {displayAbsoluteStart && <path d={`
      M ${(OUTSIDE_MARKER + 0.25) * Math.sin(minRadians - 0.1)} ${(OUTSIDE_MARKER + 0.25) * -Math.cos(minRadians - 0.1)}
      L ${OUTSIDE_MARKER * Math.sin(minRadians)} ${OUTSIDE_MARKER * -Math.cos(minRadians)}
      L ${INSIDE_MARKER * Math.sin(minRadians)} ${INSIDE_MARKER * -Math.cos(minRadians)}
      L ${(INSIDE_MARKER - 0.25) * Math.sin(minRadians - 0.1)} ${(INSIDE_MARKER - 0.25) * -Math.cos(minRadians - 0.1)}
    `} stroke="currentcolor" strokeWidth={0.1} fill="none"/>}
    {displayAbsoluteEnd && <path d={`
      M ${(OUTSIDE_MARKER + 0.25) * Math.sin(maxRadians + 0.1)} ${(OUTSIDE_MARKER + 0.25) * -Math.cos(maxRadians + 0.1)}
      L ${OUTSIDE_MARKER * Math.sin(maxRadians)} ${OUTSIDE_MARKER * -Math.cos(maxRadians)}
      L ${INSIDE_MARKER * Math.sin(maxRadians)} ${INSIDE_MARKER * -Math.cos(maxRadians)}
      L ${(INSIDE_MARKER - 0.25) * Math.sin(maxRadians + 0.1)} ${(INSIDE_MARKER - 0.25) * -Math.cos(maxRadians + 0.1)}
    `} stroke="currentcolor" strokeWidth={0.1} fill="none"/>}
  </>;
};
