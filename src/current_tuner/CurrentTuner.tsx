import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { NoticeMessage } from '@zaber/react-library';
import { Link } from 'react-router-dom';
import { match } from 'ts-pattern';

import type { RootState } from '../store';
import { ConnectionsView } from '../connection_manager';
import { NoContentMessage, Title } from '../components';
import { environment } from '../environment';

import { actions as actionsDefinition } from './actions';
import {
  selectCurrentAxisOrDeviceState,
  selectDriverError,
  selectInvalidSelectionError,
  selectLoaded,
  selectLoadingErr,
  selectShow3rdPartyWarning,
  selectShowWarning,
} from './selectors';
import { Tuner } from './Tuner';
import { LoadingErrorEnum } from './types';
import { SuggestedTuningMessage } from './components/SuggestedTuningMessage';
import { DriverError } from './DriverError';
import { WarningMessage } from './components/WarningMessage';
import { ThirdPartyWarning } from './components/ThirdPartyWarning';

const title = { title: 'Welcome to Current Tuner!' };

interface StateProps {
  axisOrDevice: ReturnType<typeof selectCurrentAxisOrDeviceState>;
  loaded: ReturnType<typeof selectLoaded>;
  loadingErr: ReturnType<typeof selectLoadingErr>;
  invalidSelectionError: ReturnType<typeof selectInvalidSelectionError>;
  setDriverError: ReturnType<typeof selectDriverError>;
  showWarning: ReturnType<typeof selectShowWarning>;
  show3rdPartyWarning: ReturnType<typeof selectShow3rdPartyWarning>;
}

interface DispatchProps {
  actions: typeof actionsDefinition;
}

class CurrentTunerBase extends Component<StateProps & DispatchProps> {
  componentDidMount(): void {
    const { axisOrDevice, showWarning, actions } = this.props;
    if (showWarning == null) {
      actions.onFirstMount();
    }
    if (axisOrDevice != null) {
      actions.selectAxisOrDevice(axisOrDevice.key);
    }
  }

  componentWillUnmount(): void {
    const { actions } = this.props;
    actions.updateLoaded(false);
  }

  renderContent() {
    const { axisOrDevice, loaded, loadingErr, invalidSelectionError, showWarning, show3rdPartyWarning } = this.props;

    if (environment.offline) {
      return <NoContentMessage type="info" message="Current Tuner is not available in Offline Zaber Launcher." {...title}/>;
    } else if (invalidSelectionError === 'multiAxisIntegrated') {
      return <NoContentMessage type="info" message="Multi-axis device is not supported by current tuner." {...title}/>;
    } else if (invalidSelectionError === 'controller') {
      return <NoContentMessage type="info" message="Controller is selected. Select its axis instead." {...title}/>;
    } else if (invalidSelectionError === 'unusedAxis') {
      return <NoContentMessage type="info" message="This axis is unused." {...title}/>;
    } else if (axisOrDevice && showWarning) {
      return <WarningMessage/>;
    } else if (!axisOrDevice) {
      return <NoContentMessage message="Use this application to tune an axis's current controller, if it has one." {...title}/>;
    } else if (loadingErr) {
      return match(loadingErr)
        .with(LoadingErrorEnum.INCOMPATIBLE_PRODUCT, () => (
          <NoticeMessage type="info" headline="Unsupported product">
            The current controller cannot be tuned on this axis.
            Consult the axis's product manual for more information.
          </NoticeMessage>
        ))
        .with(LoadingErrorEnum.NON_STEPPER, () => (
          <NoticeMessage type="info" headline="Unsupported motor type">
            This device is not a stepper motor device.
          </NoticeMessage>
        ))
        .with(LoadingErrorEnum.THIRD_PARTY_NOT_COMPLETE, () => (
          <NoticeMessage type="info" headline="Non-Zaber peripheral is not setup">
            The selected peripheral is a Non-Zaber peripheral, but has not been fully set up. It cannot be tuned yet.
            Please complete the set up process with the <Link to="/hardware-modification">Advanced Hardware Setup</Link> app.
          </NoticeMessage>
        ))
        .otherwise(() => <NoContentMessage type="error" message={loadingErr} {...title}/>);
    } else if (!loaded) {
      return <NoContentMessage message="Loading..." type="working" {...title}/>;
    } else if (show3rdPartyWarning) {
      return <ThirdPartyWarning axisOrDevice={axisOrDevice}/>;
    } else if (!axisOrDevice.sliderPositions) {
      return <SuggestedTuningMessage axisOrDevice={axisOrDevice}/>;
    } else {
      return <div className="tune-steps"><Tuner/></div>;
    }
  }

  render(): React.ReactNode {
    const { actions, axisOrDevice, setDriverError } = this.props;

    return (
      <div className="connection-view-and-app">
        <ConnectionsView
          selectable={['device', 'axis']}
          selected={axisOrDevice?.key ?? null}
          onSelect={actions.selectAxisOrDevice}/>
        <div className="current-tuner">
          <Title>Current Tuner</Title>
          {setDriverError && <DriverError/>}
          {this.renderContent()}
        </div>
      </div>
    );
  }
}

export const CurrentTuner = connect<StateProps, DispatchProps, unknown, RootState>(
  state => ({
    axisOrDevice: selectCurrentAxisOrDeviceState(state),
    loaded: selectLoaded(state),
    loadingErr: selectLoadingErr(state),
    invalidSelectionError: selectInvalidSelectionError(state),
    setDriverError: selectDriverError(state),
    showWarning: selectShowWarning(state),
    show3rdPartyWarning: selectShow3rdPartyWarning(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(CurrentTunerBase);
