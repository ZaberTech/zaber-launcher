
import React from 'react';
import { Button, Icons, Modal, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../../utils';
import { selectMessage } from '../selectors';
import { actionDefinitions } from '../actions';
import { NoContentMessage } from '../../components';


export const ServoTuningModal: React.FC = () => {
  const message = useSelector(selectMessage);
  const actions = useActions(actionDefinitions);

  if (message == null || message.type === 'top-error' || message.type === 'top-note') {
    return null;
  }

  if (message.type === 'modal-screen') {
    return <div className="modal-background-overlay"/>;
  }

  return (
    <Modal
      className="servo-tune-modal"
      headerIcon={<Icons.Gear/>}
      headerText="Servo Tuning"
      small
      bodyIcon={message.type === 'modal-error' ? 'error' : undefined}
      buttons={
        ['modal-error', 'modal-success'].includes(message.type) &&
        <Button onClick={() => actions.setMessage(null)}>Ok</Button>
      }>
      {message.type === 'modal-working' &&
        <NoContentMessage message={message.message} type="working"/>}
      {['modal-error', 'modal-success'].includes(message.type) &&
        <Text>{message.message}</Text>
      }
    </Modal>
  );
};
