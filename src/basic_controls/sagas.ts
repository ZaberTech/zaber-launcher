import {
  Angle,
  CommandFailedException,
  ConnectionFailedException,
  RemoteModeException,
  RequestTimeoutException,
  TimeoutException
} from '@zaber/motion';
import { AxisType } from '@zaber/motion/ascii';
import type { SagaIterator } from 'redux-saga';
import { all, call, fork, put, select, takeEvery } from 'redux-saga/effects';
import { match, P } from 'ts-pattern';

import type { Logger } from '../app_components';
import {
  getAxis,
  getDevice,
  isAxisIdentified,
  LockstepGroup as ConnectionLockstepGroup,
  selectAxes,
  selectDevices,
  selectIdentifiedAxes
} from '../connection_manager';
import { commandExists, hasSetting } from '../devices';
import { JOYSTICK_DEVICE_IDS } from '../joystick/types';
import { EntityKey, getAxisNumber, makeAxisKey } from '../keys';
import { getLogger } from '../log';
import { Action, RT, takeLeadingUniq, throwUnexpectedError, tryAccess } from '../utils';

import { ActionTypes, ActionsToPayloads, actionDefinitions as actions } from './actions';
import { ioSaga } from './io/sagas';
import { movementSaga } from './movement/sagas';
import {
  AxisInfo, ControllerInfo, DeviceControlsInfo, INDEXED_POSITIONS_LIMIT, IoInfo, LockstepGroup, MotionInfo, moveableKey,
  PeripheralInfo,
} from './types';

let logger: Logger;
export function* basicControlsSaga(): SagaIterator {
  logger = getLogger('BasicControlsSaga');

  yield all([
    takeLeadingUniq(ActionTypes.FETCH_DEVICE_INFO, action => action.payload.key, fetchDeviceInfo),
    takeEvery(ActionTypes.ENABLE_DRIVER, enableDriver),
    fork(ioSaga),
    fork(movementSaga),
  ]);
}

function motion(type: 'linear' | 'cyclic', min: number, max: number, maxSpeed: number, cycleDist = 0, isIndexed = false): MotionInfo {
  return { type, min, max, maxSpeed, travel: type === 'linear' ? max - min : cycleDist, isIndexed };
}

function* getAxisInfo(key: EntityKey): SagaIterator<AxisInfo> {
  const axes: RT<typeof selectAxes> = yield select(selectIdentifiedAxes);
  const axis = axes[key];
  if (!isAxisIdentified(axis)) {
    return { type: 'invalid', key, reason: 'unidentified', message: 'Axis is not identified' };
  }

  const zmlAxis: RT<typeof getAxis> = yield call(getAxis, key);
  const settings = zmlAxis.settings;

  let motionType: 'linear' | 'cyclic' | 'unknown' =
    axis.identity.axisType === AxisType.LINEAR ? 'linear' :
    axis.identity.axisType === AxisType.ROTARY ? 'cyclic' :
    'unknown';

  if (motionType === 'unknown') {
    return { type: 'invalid', key, reason: 'unidentified', message: 'Unused axis' };
  }

  let isIndexed = false;
  if (motionType === 'cyclic') {
    const indexNumber = yield call([zmlAxis, zmlAxis.getNumberOfIndexPositions]);
    if (indexNumber > 1 && indexNumber <= INDEXED_POSITIONS_LIMIT) {
      isIndexed = true;
    }
  }

  const min: number = hasSetting('limit.min', axis) ? yield call([settings, settings.get], 'limit.min') : 0;
  const max: number = hasSetting('limit.max', axis) ? yield call([settings, settings.get], 'limit.max') : 0;
  const maxSpeed: number = hasSetting('maxspeed', axis) ? yield call([settings, settings.get], 'maxspeed') : 0;
  let cycle: number | undefined = undefined;
  if (motionType === 'cyclic') {
    try {
      cycle = yield call([settings, settings.get], 'limit.cycle.dist');
    } catch (err) {
      if (!(err instanceof CommandFailedException)) {
        throw err;
      }
    }

    if (cycle == null || cycle === 0) {
      const fullRotation = zmlAxis.settings.convertToNativeUnits('pos', 360, Angle.DEGREES);

      cycle = Math.min(max - min, fullRotation);
      if (max - min < fullRotation) {
        motionType = 'linear';
      }
    }
  }

  return match(axis.identity.axisType)
    .with(P.union(AxisType.LINEAR, AxisType.ROTARY), (): AxisInfo => ({
      type: 'motion-axis',
      key,
      number: getAxisNumber(key),
      motion: motion(motionType, min, max, maxSpeed, cycle, isIndexed),
      identity: axis.identity,
    }))
    .otherwise((): AxisInfo => ({ type: 'invalid', key, reason: 'error', message: 'Unsupported axis type' }));
}

function* getLockstepGroupInfo(key: EntityKey, lockstep: ConnectionLockstepGroup): SagaIterator<PeripheralInfo> {
  const primaryAxisNumber = lockstep.axisNumbers[0];
  const zmlDevice: RT<typeof getDevice> = yield call(getDevice, key);
  const zmlLockstep = zmlDevice.getLockstep(lockstep.groupNumber);
  const offsets: RT<typeof zmlLockstep.getOffsets> = yield call([zmlLockstep, zmlLockstep.getOffsets]);
  const primary: RT<typeof getAxisInfo> = yield call(getAxisInfo, makeAxisKey(key, primaryAxisNumber));
  if (primary.type === 'invalid') {
    return { ...primary, message: 'Primary axis is invalid' };
  }
  const secondaries: LockstepGroup['secondaries'] = [];
  for (const n of lockstep.axisNumbers.slice(1)) {
    const secondary: RT<typeof getAxisInfo> = yield call(getAxisInfo, makeAxisKey(key, n));
    if (secondary.type === 'invalid') {
      return { type: 'invalid', key: primary.key, reason: secondary.reason, message: `Secondary axis ${n} is invalid` };
    }
    secondaries.push(secondary);
  }
  const min = Math.max(primary.motion.min, ...secondaries.map((secondary, i) => secondary.motion.min - offsets[i]));
  const max = Math.min(primary.motion.max, ...secondaries.map((secondary, i) => secondary.motion.max - offsets[i]));
  const maxSpeed = Math.min(primary.motion.maxSpeed, ...secondaries.map(s => s.motion.maxSpeed));

  return {
    type: 'lockstep',
    group: lockstep.groupNumber,
    primary,
    secondaries,
    motion: motion(primary.motion.type, min, max, maxSpeed, primary.motion.travel),
  };
}

function* getIoInfo(key: EntityKey): SagaIterator<IoInfo | null> {
  const zmlDevice: RT<typeof getDevice> = yield call(getDevice, key);
  const zmlIoInfo: RT<typeof zmlDevice.io.getChannelsInfo> = yield call([zmlDevice.io, zmlDevice.io.getChannelsInfo]);
  const ioInfo = {
    ao: zmlIoInfo.numberAnalogOutputs,
    ai: zmlIoInfo.numberAnalogInputs,
    do: zmlIoInfo.numberDigitalOutputs,
    di: zmlIoInfo.numberDigitalInputs,
  };
  if (ioInfo.ao < 1 && ioInfo.ai < 1 && ioInfo.do < 1 && ioInfo.di < 1) {
    return null;
  } else {
    return ioInfo;
  }
}

function* getDeviceInfo(key: EntityKey): SagaIterator<DeviceControlsInfo> {
  const devices: RT<typeof selectDevices> = yield select(selectDevices);
  const device = devices[key];
  if (device?.identity == null) {
    return { type: 'invalid', key, reason: 'unidentified', message: 'Device is not identified' };
  }
  const joystickInfo = tryAccess(JOYSTICK_DEVICE_IDS, device.identity.deviceId);
  if (joystickInfo != null) {
    return { type: 'x-joy', key, deviceId: device.identity.deviceId };
  }

  const axes: RT<typeof selectAxes> = yield select(selectAxes);
  const firstAxis = tryAccess(axes, makeAxisKey(key, 1));
  if (firstAxis && commandExists('process on', firstAxis)) {
    return { type: 'process-controller', key, deviceId: device.identity.deviceId };
  }
  if (firstAxis && commandExists('lamp on', firstAxis)) {
    return { type: 'lamp-controller', key, deviceId: device.identity.deviceId };
  }
  if (device.isController || device.isMultiAxis) {
    const peripherals: ControllerInfo['peripherals'] = new Map();
    for (let i = 1; i <= device.axisCount; i++) {
      const lockstep = device.locksteps?.find(l => l.axisNumbers.includes(i));
      if (lockstep != null) {
        if (lockstep.axisNumbers[0] === i) {
          const lockstepInfo: RT<typeof getLockstepGroupInfo> = yield call(getLockstepGroupInfo, key, lockstep);
          peripherals.set(i, lockstepInfo);
        }
      } else {
        const axisInfo: RT<typeof getAxisInfo> = yield call(getAxisInfo, makeAxisKey(key, i));
        peripherals.set(i, axisInfo);
      }
    }
    return {
      type: 'controller',
      key,
      peripherals,
      io: yield call(getIoInfo, key),
    };
  } else {
    const integratedAxis: RT<typeof getAxisInfo> = yield call(getAxisInfo, makeAxisKey(key, 1));
    if (integratedAxis.type === 'invalid') {
      return { type: 'invalid', key, reason: 'error', message: `Could not read motion info (${integratedAxis.message})` };
    } else {
      return { type: 'integrated', key, io: yield call(getIoInfo, key), identity: device.identity, motion: integratedAxis.motion };
    }
  }
}

function* fetchDeviceInfo({ payload: { key } }: Action<ActionsToPayloads[ActionTypes.FETCH_DEVICE_INFO]>) {
  try {
    const info: RT<typeof getDeviceInfo> = yield call(getDeviceInfo, key);
    yield put(actions.fetchDeviceInfoDone(key, info));
  } catch (e) {
    throwUnexpectedError(e);
    logger.warn(e);
    if (e instanceof ConnectionFailedException || e instanceof TimeoutException || e instanceof RequestTimeoutException) {
      yield put(actions.fetchDeviceInfoDone(key, { type: 'invalid', key, reason: 'disconnected', message: e.message }));
    } else {
      yield put(actions.fetchDeviceInfoDone(key, { type: 'invalid', key, reason: 'error', message: `Device Read Failed: ${e.message}` }));
    }
  }
}

function* enableDriver({ payload: { moveable, enable } }: Action<ActionsToPayloads[ActionTypes.ENABLE_DRIVER]>) {
  try {
    const zmlDevice: RT<typeof getDevice> = yield call(getDevice, moveableKey(moveable));
    const axisNumbers =
      moveable.type === 'lockstep' ? [moveable.primary.number, ...moveable.secondaries.map(s => s.number)] :
      moveable.type === 'integrated' ? [1] :
      [moveable.number];
    for (const axisNumber of axisNumbers) {
      const axis = zmlDevice.getAxis(axisNumber);
      if (enable) {
        yield axis.driverEnable();
      } else {
        yield axis.driverDisable();
      }
    }
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.setError(moveableKey(moveable), {
      headline: `Could not ${enable ? 'enable' : 'disable'} this driver`,
      message: e.message,
      context: e instanceof RemoteModeException ? 'remote-mode' : undefined,
    }));
  }
}
