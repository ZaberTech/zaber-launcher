# Development setup

The following instructions describe how to setup a development environment for Zaber Launcher on a Windows device:
- Install node.js LTS
- Make sure to update to the latest npm version
- Clone the necessary repos

    ```
    git clone git@gitlab.izaber.com:software-public/zaber-launcher.git --recurse-submodules
    git clone git@gitlab.izaber.com:software-internal/little-device-sim.git
    ```

    - `zaber-launcher` is the main repo where the Zaber launcher code is developed
    - `little-device-sim` is a simulated X-MCC which can act as a device on a computer and can be accessed as a device over a TCP port
- In a new terminal window, get the simulated X-MCC running

    ```
    cd little-device-sim
    npm i
    set PORT=11321
    npm start
    ```
- Finally, in another new terminal window, run the Zaber Launcher electron app

    ```
    cd zaber-launcher
    npm i
    npm start
    ```

# Release

- Create a new release branch e.g. `release-v2024.8.20-1`
- Write changelog entry.
- Update the version by running `npm run setversion`.
    - This automatically updates the npm package version, creates a git commit, and tags the version number in git
- Push the latest changes (including the new tag) to gitlab with `git push --follow-tags`
- Run a manual pipeline on the release branch. This will trigger pipelines for internal, public and offline. These pipelines are all manual, start those that should be run.
    - Dev releases are done automatically at midnight from the master branch, but they can also be triggered manually from any branch.
        - Note that publishing a Dev release from a non-master branch will be short-lived, as a Dev release will be automatically published again at midnight from the master branch.
    - Internal releases are intended for getting internal feedback, publishing a new internal feature, or to complement a public release.
    - Public releases are for customers and are usually be accompanied by Internal and Offline releases.
    - Offline releases are public releases with a bundled device database, for customers that want to run Zaber Launcher on a machine without internet access.
        - Internal releases are usually published with Public releases.
- Merge the release branch into master.

## QA

Any updates to core libraries (e.g. `electron`, `react`) must be tested on dev or internal editions before releasing.
After updating of `electron-updater` library, the update process itself must be **tested on all platforms** (Win, Linux, Mac).
You can test the update process by two consecutive CI pipelines of dev edition.

# Testing
- Run the tests like this

    ```
    npx gulp test
    ```
- Run code coverage like this

    ```
    npm run coverage
    ```

    - This will generate the coverage report in the `./coverage` directory. You can see the coverage report by opening `<path_to_repo>/coverage/lcov-report/index.html` in a browser

- If you want to only run a single test, your can do that with:
    ```
    npx jest src/path/to/Component.test.tsx -t 'Name of the test here'
    ```

- In some cases it may be helpful to see more output from the wrapper (when the section you're interested in is too far down to find). In this case you can try:
    ```
    DEBUG_PRINT_LIMIT=10000 npx jest src/path/to/Component.test.tsx -t 'Name of the test here'
    ```
    Another way to achieve this if you know where the problematic element will be is to add code such as this to only print out a subsection of the body:
    ```
    const subsection: within(wrapper.getSomehow('text to select with'));
    wrapper.debug(subsection);
    ```

- Run custom build of message router
    - `message-router` is a service that is meant to run on the computer in the background and manage the packets traveling from the user to the devices
    - Clone and run the message router
    ```
    git clone git@gitlab.izaber.com:software-public/message-router.git
    cd message-router
    npm i
    npm start -- run
    ```
    - In terminal set environment variable NO_MESSAGE_ROUTER to `1`
    ```
    set NO_MESSAGE_ROUTER=1
    ```
    or in UNIX systems:
    ```
    export NO_MESSAGE_ROUTER=1
    ```
    - Restart Zaber Launcher
    - Zaber Launcher won't start message router process and use the already running one
