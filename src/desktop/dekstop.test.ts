const openExternal = jest.fn().mockResolvedValue(undefined);
const relaunch = jest.fn().mockResolvedValue(undefined);
const exit = jest.fn().mockResolvedValue(undefined);
jest.mock('@electron/remote', () => ({
  shell: {
    openExternal,
  },
  app: {
    relaunch,
    exit,
  }
}));

import { openExternalLink, restart } from '.';

describe('Test desktop functions', () => {
  test('openExternalLink', () => {
    openExternalLink('url');
    expect(openExternal).toHaveBeenCalledWith('url', { activate: true });
  });

  test('restart', () => {
    restart();
    expect(relaunch).toHaveBeenCalled();
    expect(exit).toHaveBeenCalled();
  });
});
