import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { Button, ButtonRow, NoticeMessage } from '@zaber/react-library';
import { NavLink } from 'react-router-dom';
import { Inductance } from '@zaber/motion';

import { actions as actionsDefinition } from '../actions';
import type { DeviceOrAxisStateComplete } from '../selectors';
import { convertBetweenUnits, getUnitLabel } from '../../units';

interface Props {
  axisOrDevice: DeviceOrAxisStateComplete;
}

export const ThirdPartyWarning: React.FC<Props> = ({ axisOrDevice }) => {
  const actions = useActions(actionsDefinition);
  const { thirdPartyInfo, inputSettings } = axisOrDevice;

  const resistance = inputSettings?.find(setting => setting.name === 'motor.resistance');
  const inductance = inputSettings?.find(setting => setting.name === 'motor.inductance');

  return (
    <NoticeMessage type="warning" headline="Metadata confirmation">
      <div className="message-div">
          The selected Non-Zaber product has been set up using the following motor parameters:
      </div>

      <div className="params message-div">
        <div>Rated Current:&emsp;<b>{thirdPartyInfo?.ratedCurrent} A</b></div>
        <div>Holding Torque:&emsp;<b>{thirdPartyInfo?.holdingTorque} Nm</b></div>
        {resistance && <div>
            Resistance:&emsp;
          <b>{resistance.value} {resistance.units ? getUnitLabel(resistance.units) : ''}</b>
        </div>}
        {inductance && <div>
            Inductance:&emsp;
          <b>{convertBetweenUnits(inductance.value, inductance.units!, Inductance.mH)} {getUnitLabel(Inductance.mH)}</b>
        </div>}
        <div>Steps per Rotation:&emsp;<b>{thirdPartyInfo?.stepsPerRotation}</b></div>
      </div>

      <div className="message-div">
          If these are not correct, please update them with the Advanced Hardware Setup app before continuing.
      </div>

      <ButtonRow className="button-container">
        <NavLink to="/hardware-modification">
          <Button color="grey">Go to Advanced Hardware Setup</Button>
        </NavLink>
        <Button onClick={actions.confirm3rdParty}>
            OK
        </Button>
      </ButtonRow>
    </NoticeMessage>
  );
};
