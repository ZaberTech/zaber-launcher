import React, { Component, createRef } from 'react';

import { openExternalLink } from '../desktop';

interface Props {
  html: string;
}
export class ProtocolManualFormatter extends Component<Props> {
  manualContainerReference: React.RefObject<HTMLDivElement> = createRef();

  render(): React.ReactNode {
    const { html } = this.props;
    return (
      <div
        ref={this.manualContainerReference}
        className="protocol-manual"
        dangerouslySetInnerHTML={{ __html: html }}
      />
    );
  }

  componentDidMount() {
    const linkElements = this.manualContainerReference.current!.querySelectorAll('[href]');
    for (const el of linkElements) {
      const noPublicDocs = el.querySelector(':scope > *[data-disclosure="pub-hardware"]') != null;
      if (noPublicDocs) {
        const newSpan = document.createElement('span');
        newSpan.className = 'unlinked';
        el.childNodes.forEach(child => {
          newSpan.appendChild(child);
        });
        el.replaceWith(newSpan);
      } else {
        const href = el.getAttribute('href')!;
        el.addEventListener('click', event => {
          event.preventDefault();
          openExternalLink(href);
        });
      }
    }
  }
}
