import { call, select, put, all, takeLeading, StrictEffect, cancelled, race, take } from 'redux-saga/effects';
import { ensureError, makeString, throwUnexpectedError } from '@zaber/toolbox';
import _ from 'lodash';
import { ascii, Length, microscopy, Units } from '@zaber/motion';
import moment, { invalid } from 'moment';

import { SagaIter, RT } from '../../utils';
import {
  AXES,
  Axes,
  MonitorStableInfo,
} from '../types';
import { selectPresetDataFromState } from '../selectors';
import { applyPresetToScope } from '../preset_sagas';
import { compareAndMarkForApply, PresetData } from '../presets';
import { refreshMonitorAndWait, triggerScopeCamera } from '../operation_sagas';
import { convertBetweenUnits, MeasurementOK } from '../../units';
import { actions as rootActions } from '../actions';

import { Dimensions } from './types';
import { selectConfig, selectFocusPositions, selectGridCoordinates, selectAppliedPresets } from './selectors';
import { actions, ActionTypes } from './actions';

const PROCESS = 'multidacq';

interface AcquisitionState {
  start: moment.Moment;
  picturesTaken: number;
  dimensionsCount: Partial<Record<Dimensions, number>>;
  filter: {
    index: number;
  };
  objective: {
    index: number;
  };
  illuminator: {
    channels: Record<number, {
      on: boolean;
      intensity: number;
    }>;
  };
  axes: Record<Axes, {
    // Focus axis position is specified without the focus offset.
    // It's because the presets can still change objectives after focus dimension.
    position: MeasurementOK;
  }>;
  autofocus: {
    reference: number | null;
  };
}

function createState(stableInfo: MonitorStableInfo): AcquisitionState {
  return {
    start: invalid(),
    picturesTaken: 0,
    dimensionsCount: { },
    filter: {
      index: 0,
    },
    objective: {
      index: 0,
    },
    illuminator: {
      channels: _.mapValues(stableInfo.illuminator?.channels ?? {}, () => ({
        on: false,
        intensity: 0,
      })),
    },
    axes: {
      focusAxis: {
        position: {
          value: 0,
          units: Length.MICROMETRES,
        },
      },
      xAxis: {
        position: {
          value: 0,
          units: Length.MICROMETRES,
        },
      },
      yAxis: {
        position: {
          value: 0,
          units: Length.MICROMETRES,
        },
      },
    },
    autofocus: {
      reference: null,
    },
  };
}

function loadMetadataFromPreset(state: AcquisitionState, info: MonitorStableInfo, preset: PresetData, onlyApplied = true) {
  if (!onlyApplied || preset.filter.apply) {
    state.filter.index = preset.filter.value;
  }
  if (!onlyApplied || preset.objective.apply) {
    state.objective.index = preset.objective.value;
  }

  for (const [channel, channelPreset] of Object.entries(preset.illuminator)) {
    const channelMeta = state.illuminator.channels[+channel];
    if (channelMeta == null) { continue }

    if (!onlyApplied || channelPreset.on.apply) {
      channelMeta.on = channelPreset.on.value;
    }
    if (!onlyApplied || channelPreset.intensity.apply) {
      channelMeta.intensity = channelPreset.intensity.value;
    }
  }

  for (const axisKey of AXES) {
    const axis = preset.axes[axisKey];

    if (!onlyApplied || axis.position.apply) {
      const position = axis.position.value;
      const metaPosition = state.axes[axisKey].position;
      metaPosition.value = convertBetweenUnits(position.value, position.units, metaPosition.units);

      if (axisKey === 'focusAxis' && info.objectiveChanger) {
        const objective =  state.objective.index;
        if (objective > 0) {
          const offset = info.objectiveChanger.focusOffsets[objective - 1];
          metaPosition.value -= convertBetweenUnits(offset.value, offset.units, metaPosition.units);
        }
      }
    }
  }
}

export function* multiDAcqSaga(scope: microscopy.Microscope, info: MonitorStableInfo): SagaIter {
  yield put(actions.reset());
  yield all([
    takeLeading(ActionTypes.START, multiDAcq, scope, info),
  ]);
}

function* multiDAcq(
  scope: microscopy.Microscope,
  info: MonitorStableInfo,
): SagaIter {
  try {
    yield call(refreshMonitorAndWait);
    yield put(rootActions.blockMonitor(PROCESS, true));
    const beforeState: RT<typeof selectPresetDataFromState> = yield select(selectPresetDataFromState);

    const state = createState(info);
    loadMetadataFromPreset(state, info, beforeState, false);

    const config: RT<typeof selectConfig> = yield select(selectConfig);

    state.start = moment();
    const result: {
      cancel?: unknown;
      acquisition?: RT<typeof runAllDimensions>;
    } = yield race({
      acquisition: call(runAllDimensions, { scope, info, state }, config.order),
      cancel: take(ActionTypes.CANCEL),
    });

    if (result.cancel || result.acquisition?.error) {
      yield call(waitForAllIdle, scope.connection);
    }

    yield put(rootActions.blockMonitor(PROCESS, false));
    yield call(refreshMonitorAndWait);
    const afterState: RT<typeof selectPresetDataFromState> = yield select(selectPresetDataFromState);

    const diffState = compareAndMarkForApply(afterState, beforeState);
    yield call(applyPresetToScope, scope, info, diffState);

    if (result.cancel) {
      yield put(actions.reset());
    } else if (result.acquisition?.error) {
      throw ensureError(result.acquisition.error);
    } else {
      yield put(actions.end({
        total: state.picturesTaken,
        elapsedTime: moment().diff(state.start, 'milliseconds'),
      }));
    }
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.error(err.message));
  } finally {
    if (yield cancelled()) {
      yield put(actions.error('Error when communicating with the microscope.'));
    }
    yield put(rootActions.blockMonitor(PROCESS, false));
  }
}

interface RunDimensionContext {
  scope: microscopy.Microscope;
  info: MonitorStableInfo;
  state: AcquisitionState;
}

function* runAllDimensions(
  context: RunDimensionContext,
  dimensions: Dimensions[],
): Generator<StrictEffect, void | { error: unknown }, unknown> {
  try {
    yield* runDimension(context, dimensions);
  } catch (error) {
    return { error };
  }
}

function* runDimension(
  context: RunDimensionContext,
  dimensions: Dimensions[],
): Generator<StrictEffect, void, unknown> {
  const { scope, info, state } = context;
  if (dimensions.length === 0) {
    const { cameraTrigger } = info;
    if (cameraTrigger) {
      yield call(triggerScopeCamera, scope);
    }

    state.picturesTaken++;
    const total = Object.values(state.dimensionsCount).reduce((total, count) => total * count, 1);
    yield put(actions.progress({
      taken: state.picturesTaken,
      total,
    }));
    return;
  }

  let iterator: DimensionIterator;
  switch (dimensions[0]) {
    case 'xy':
      iterator = xyDimension(context);
      break;
    case 'focus':
      iterator = zDimension(context);
      break;
    case 'presets':
      iterator = presetsDimension(context);
      break;
    default:
      throw new Error(`Unknown dimension: ${makeString(dimensions[0])}`);
  }

  let returnValue: unknown;
  for (;;) {
    const { value, done } = iterator.next(returnValue);
    if (done) {
      break;
    }
    if (value === 'step') {
      yield* runDimension(context, dimensions.slice(1));
    } else {
      returnValue = yield value;
    }
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type DimensionIterator = Iterator<StrictEffect | 'step', void, any>;

function* xyDimension({ scope, state }: RunDimensionContext): DimensionIterator {
  const coordinates: RT<typeof selectGridCoordinates> = yield select(selectGridCoordinates);
  const { plate } = scope;
  if (plate == null) { return }

  state.dimensionsCount.xy = coordinates.points.length;

  for (const point of coordinates.points) {
    yield call([plate, plate.moveAbsolute],
      { value: point.x, unit: coordinates.units },
      { value: point.y, unit: coordinates.units },
    );

    const metaPosition = state.axes.yAxis.position;
    metaPosition.value = convertBetweenUnits(point.y, coordinates.units, metaPosition.units);

    yield 'step';
  }
}

function* zDimension({ scope, info, state }: RunDimensionContext): DimensionIterator {
  const positions: RT<typeof selectFocusPositions> = yield select(selectFocusPositions);
  const { focusAxis } = scope;
  if (focusAxis == null) {
    return;
  }

  state.dimensionsCount.focus = positions.points.length;

  let focusOffset = getFocusOffset(info, state, positions.units);
  let reference = positions.reference;

  if (scope.autofocus && positions.autofocus !== 'off') {
    if (positions.autofocus === 'previous') {
      reference = state.autofocus.reference ?? reference;
    }

    yield call([focusAxis, focusAxis.moveAbsolute],
      reference + focusOffset,
      positions.units as Length,
    );

    const { autofocus } = scope;

    yield call([autofocus, autofocus.focusOnce]);
    reference = yield call([focusAxis, focusAxis.getPosition], positions.units as Length);
    reference -= focusOffset;

    state.autofocus.reference = reference;
  }

  for (const position of positions.points) {
    focusOffset = getFocusOffset(info, state, positions.units);
    yield call([focusAxis, focusAxis.moveAbsolute],
      reference + position + focusOffset,
      positions.units as Length,
    );

    const statePosition = state.axes.focusAxis.position;
    statePosition.value = convertBetweenUnits(reference + position, positions.units, statePosition.units);

    yield 'step';
  }
}

function getFocusOffset(info: MonitorStableInfo, state: AcquisitionState, units: Units) {
  let focusOffset = 0;
  if (info.objectiveChanger) {
    const objective = state.objective.index;
    if (objective > 0) {
      const offset = info.objectiveChanger.focusOffsets[objective - 1];
      focusOffset = convertBetweenUnits(offset.value, offset.units, units);
    }
  }
  return focusOffset;
}

function* presetsDimension({ scope, info, state }: RunDimensionContext): DimensionIterator {
  const presets: RT<typeof selectAppliedPresets> = yield select(selectAppliedPresets);
  if (presets.length === 0) {
    yield 'step';
    return;
  }

  state.dimensionsCount.presets = presets.length;

  for (const preset of presets) {
    yield call(applyPresetToScope, scope, info, preset.data);
    loadMetadataFromPreset(state, info, preset.data);

    yield 'step';
  }
}

function* waitForAllIdle(connection: ascii.Connection): SagaIter {
  for (;;) {
    const replies: RT<typeof connection.genericCommandMultiResponse> = yield connection.genericCommandMultiResponse('');
    if (replies.every(reply => reply.status === 'IDLE')) {
      return;
    }
  }
}
