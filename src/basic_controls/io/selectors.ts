import { createSelector } from 'reselect';

import { selectBasicControls } from '../../store';

export const selectBasicControlsIo = createSelector(selectBasicControls, state => state.io);

export const selectDeviceStates = createSelector(selectBasicControlsIo, state => state.devices);
