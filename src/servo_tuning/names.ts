import { ServoTuningParamset } from '@zaber/motion/ascii';

import type { TuningMethod } from './types';

export const paramsetNames: Record<ServoTuningParamset, string> = {
  [ServoTuningParamset.LIVE]: 'Live Tuning',
  [ServoTuningParamset.P_1]: 'Preset 1',
  [ServoTuningParamset.P_2]: 'Preset 2',
  [ServoTuningParamset.P_3]: 'Preset 3',
  [ServoTuningParamset.P_4]: 'Preset 4',
  [ServoTuningParamset.P_5]: 'Preset 5',
  [ServoTuningParamset.P_6]: 'Preset 6',
  [ServoTuningParamset.P_7]: 'Preset 7',
  [ServoTuningParamset.P_8]: 'Preset 8',
  [ServoTuningParamset.P_9]: 'Preset 9',
  [ServoTuningParamset.DEFAULT]: 'Default',
  [ServoTuningParamset.STAGING]: 'Staging',
};

/** The paramsets that the user can tune and interact with (excludes staging and default) */
export const userParamsets: ServoTuningParamset[] = [
  ServoTuningParamset.LIVE,  ServoTuningParamset.P_1,  ServoTuningParamset.P_2,  ServoTuningParamset.P_3,  ServoTuningParamset.P_4,
  ServoTuningParamset.P_5,  ServoTuningParamset.P_6,  ServoTuningParamset.P_7,  ServoTuningParamset.P_8,  ServoTuningParamset.P_9,
];

export const methodNames: Record<TuningMethod, string> = {
  simple: 'Simple',
  pid: 'PID',
  advanced: 'Advanced',
  load: 'Load Preset',
  csv: 'Import/Export',
};

export const methods = Object.keys(methodNames) as TuningMethod[];
