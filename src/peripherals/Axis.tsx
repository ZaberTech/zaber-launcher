import React from 'react';

import type { IdentifiedAxisState } from '../connection_manager';

import { Peripheral } from './Peripheral';

interface Props {
  axis: IdentifiedAxisState;
}

export const Axis: React.FC<Props> = ({ axis }) =>
  // Key-ing by peripheralId ensures entire state destruction on peripheral change.
  <Peripheral key={axis.identity.peripheralId} axisKey={axis.key}/>
;
