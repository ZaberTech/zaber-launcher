import _ from 'lodash';

import { PvtSegment } from './pvtSegment';
import { getAccelerationScale, getVelocityScale, PvtSeriesHelper } from './pvtSeriesHelper';
import { Path } from './types';
import { PathDiscretizer } from './pathDiscretizer';


const COARSE_SEARCH_STEP = 0.1;
const FINE_SEARCH_STEP = 0.01;

export interface PointOnCurve {
  segment: number;
  timeWithinSegment: number;
  coordinates: (number | null)[];
  distanceToTarget?: number;
}


// Target positions should be in the same order as the series in the path; use null to ignore a series.
export function closestPointOnCurve(path: Path, target: (number | null)[], refinement: number): PointOnCurve | null {
  let best: PointOnCurve | null = null;
  if (path.series.length < 1 || path.series[0].points.length < 2) {
    return null;
  }

  const velocityScale = getVelocityScale(path.positionUnit, path.velocityUnit);
  const accelerationScale = getAccelerationScale(path.positionUnit, path.velocityUnit);

  // Using a brute force search (coarse first then fine). Analytical solution will require some research.
  // TODO: Consider switching to an analytical solution. The current search approach can miss selecting
  // the closest line segment because one of the coarse search test points on the second-closest segment
  // may be closer than the two nearest test points on the closest segment. This can easily happen
  // if the curve crosses itself and you zoom in.
  for (let pointIndex = 0; pointIndex < path.series[0].points.length - 1; pointIndex++) {
    const targetSegments = path.series.map((series, seriesIndex) =>
      target[seriesIndex] == null
        ? null
        : new PvtSegment(series.points[pointIndex], series.points[pointIndex + 1], velocityScale, accelerationScale));

    const firstSegment = _.first(targetSegments.filter(_.identity));
    if (firstSegment == null) {
      return null;
    }

    const coarseResult = segmentSearch(targetSegments, target, 0, firstSegment.dt, COARSE_SEARCH_STEP * firstSegment.dt / refinement);
    if (coarseResult != null && (best == null || coarseResult.distanceToTarget! < best.distanceToTarget!)) {
      best = {
        ...coarseResult,
        segment: pointIndex,
        coordinates: Array(targetSegments.length).fill(null),
      };
    }
  }

  // Refine the search using smaller time steps near the best result from the coarse search.
  // May still miss the best result by up to 10% of a segment's time if the best result is near the edge of a segment.
  if (best != null) {
    const segments = path.series.map((series, seriesIndex) =>
      target[seriesIndex] == null
        ? null
        : new PvtSegment(series.points[best!.segment], series.points[best!.segment + 1], velocityScale, accelerationScale));

    const firstSegment = _.first(segments.filter(_.identity))!;
    const fineResult = segmentSearch(
      segments,
      target,
      Math.max(0, best.timeWithinSegment - COARSE_SEARCH_STEP * firstSegment.dt),
      Math.min(firstSegment.dt, best.timeWithinSegment + COARSE_SEARCH_STEP * firstSegment.dt),
      FINE_SEARCH_STEP * firstSegment.dt / refinement);
    if (fineResult != null && fineResult.distanceToTarget! < best.distanceToTarget!) {
      // Ensure result has coordinates for all series, not just the ones being drawn.
      const allSegments = path.series.map(series =>
        new PvtSegment(series.points[best!.segment], series.points[best!.segment + 1], velocityScale, accelerationScale));
      best = {
        ...best,
        ...fineResult,
        coordinates: allSegments.map(segment => segment == null ? null : segment.getPos(fineResult.timeWithinSegment)),
      };
    }
  }

  return best;
}


function segmentSearch(
  segments: (PvtSegment | null)[],
  target: (number | null)[],
  startTime: number,
  endTime: number,
  step: number): Omit<PointOnCurve, 'segment' | 'coordinates'> | null {
  let best: Omit<PointOnCurve, 'segment' | 'coordinates'> | null = null;
  for (let t = startTime; t <= endTime; t += step) {
    let distance: number | null = null;
    segments.forEach((segment, seriesIndex) => {
      if (segment != null) {
        const d = segment!.getPos(t) - target[seriesIndex]!;
        distance = (distance ?? 0) + d * d;
      }
    });

    if (distance != null) {
      if (best == null || distance < best.distanceToTarget!) {
        best = {
          timeWithinSegment: t,
          distanceToTarget: distance,
        };
      }
    }
  }

  if (best != null) {
    best.distanceToTarget = Math.sqrt(best.distanceToTarget!);
  }

  return best;
}


export async function discretizePath(path: Path, fidelity: 'low' | 'high'): Promise<Path | null> {
  if (path.series[0]?.points.length < 2) {
    return null;
  }

  let timeStep: number | undefined = undefined;
  if (fidelity === 'low') {
    const totalTime = _.last(path.series[0].points)!.time;
    timeStep = Math.max(0.1, totalTime / 100);
  }

  const discretizer = new PathDiscretizer(path);
  const result = discretizer.discretizeToPath(timeStep);
  for (let i = 0; i < path.series.length; i++) {
    const helper = PvtSeriesHelper.FromScales(path.series[i], discretizer.velocityScale, discretizer.accelerationScale);
    result.series[i].positionRange = helper.findPositionRange();
    result.series[i].velocityRange = helper.findVelocityRange();
    result.series[i].accelerationRange = helper.findAccelerationRange();
  }

  return result;
}
