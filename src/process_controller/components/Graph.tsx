import ReactECharts from 'echarts-for-react';
import type { EChartsOption, LineSeriesOption, MarkLineComponentOption } from 'echarts';
import React from 'react';
import { ProcessControllerMode, ProcessControllerSource, ProcessControllerSourceSensor } from '@zaber/motion/product';
import { Colors } from '@zaber/react-library';

import type { Reading } from '../types';
import { environment } from '../../environment';

type Props = {
  readings: Reading[];
  mode: ProcessControllerMode;
  source: ProcessControllerSource;
  /** If this graph should show the setpoint on the graph */
  setpoint: boolean;
  /** How often polls come in */
  pollDelay?: number;
  /** If provided, only shows points for the last `duration` seconds, scrolling backwards */
  duration?: number;
  lines?: MarkLineComponentOption['data'];
};

const lineSeries: (color: string, style?: 'dashed') => LineSeriesOption = (color, style) => ({
  type: 'line',
  symbol: 'none',
  lineStyle: { color, type: style },
  itemStyle: { color }
} as const);

/**
 * Calculates the value at the top of the axis
 * @param max The greatest value on an axis
 * @param step The step at which the graph should grow by
 */
function axisMax(max: number, step: number) {
  return Math.floor(max / step) * step + step;
}

/**
 * Calculates the value at the bottom of the axis
 * @param min The least value on an axis
 * @param step The step at which the graph should grow by
 */
function axisMin(max: number, step: number) {
  return Math.ceil(max / step) * step - step;
}

function toOptions({ readings, mode, source, setpoint, pollDelay, duration, lines }: Props): EChartsOption {
  // If a duration is provided, runs backwards from the most recent point, otherwise, run forward from the first point
  const startTime = duration != null ? (readings.at(-1)?.time ?? 0) : (readings.at(0)?.time ?? 0);
  const msToSecs = duration != null ? -1000 : 1000;
  const fieldData = (field: keyof Reading) => readings.map(reading => ({
    name: `${reading.time}`,
    value: [(reading.time - startTime) / msToSecs, reading[field]],
  }));

  const setpointData = setpoint ? fieldData('setpoint') : [];
  const outputStep = mode === ProcessControllerMode.MANUAL || mode === ProcessControllerMode.ON_OFF ? 'start' : undefined;

  const series: EChartsOption['series'] = [
    { ...lineSeries(Colors.blue), name: 'measured', data: fieldData('measured'), yAxisId: 'input', z: 3 },
    { ...lineSeries(Colors.yellow, 'dashed'), name: 'setpoint', data: setpointData, yAxisId: 'input', z: 4, markLine: { data: lines } },
    { ...lineSeries(Colors.green), name: 'output', data: fieldData('output'), yAxisId: 'output', step: outputStep },
  ];
  return {
    legend: {
      type: 'plain',
    },
    xAxis: {
      mainType: 'xAxis',
      type: 'value',
      name: 'Time (s)',
      nameLocation: 'middle',
      nameGap: 30,
      ...(duration && {
        min: 0,
        max: duration,
        interval: 1,
        inverse: true,
      })
    },
    yAxis: [
      {
        id: 'input',
        type: 'value',
        name: source.sensor === ProcessControllerSourceSensor.ANALOG_INPUT ? 'V' : '˚C',
        nameLocation: 'middle',
        nameGap: 40,
        min: value => axisMin(value.min, 10),
        max: value => axisMax(value.max, 10),
        alignTicks: true,
      },
      {
        id: 'output',
        type: 'value',
        name: 'Output (V)',
        nameLocation: 'middle',
        nameGap: 40,
        min: value => value.min === 0 ? 0 : Math.min(0, axisMin(value.min, 5)),
        max: value => Math.max(0, axisMax(value.max, 5)),
      }
    ],
    series,
    grid: {
      bottom: 80,
    },
    animation: pollDelay != null,
    animationEasingUpdate: 'linear',
    animationDurationUpdate: pollDelay,
  };
}

export const Graph: React.FC<Props> = props => {
  if (environment.isTest) {
    return <div className="echarts-graph-placeholder"></div>;
  }
  return <ReactECharts
    option={toOptions(props)}
    opts={{ renderer: 'canvas' }}
  />;
};
