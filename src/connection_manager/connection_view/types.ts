import { EntityKey, EntityKeyType } from '../../keys';

export interface PassedProperties {
  selectable: SelectableEntities[];
  selected: EntityKey | null;
  onSelect: (selected: EntityKey | null) => void;
  pinnedAxesKeys?: EntityKey[];
  onPinClick?: (axisKey: EntityKey) => void;
  attentionRequests?: AttentionRequests;
  showAxes?: boolean;
}

export type SelectableEntities = 'axis' | 'integrated' | 'device' | 'connection';
export const selectableEntitiesToEntityType: [SelectableEntities, EntityKeyType][] = [
  ['axis', EntityKeyType.AXIS],
  ['integrated', EntityKeyType.DEVICE],
  ['device', EntityKeyType.DEVICE],
  ['connection', EntityKeyType.CONNECTION],
];

/** Indicates that there is some action to be taken for a specific entity */
export interface AttentionRequests {
  /** The list of entities that request attention */
  requestAttention: EntityKey[];
  /** The icon to show if the entity requests attention */
  showIcon: JSX.Element;
}
