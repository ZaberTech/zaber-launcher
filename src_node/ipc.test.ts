import { EventEmitter } from 'events';

let ipcMain: IpcMainMock;
class IpcMainMock extends EventEmitter {
  constructor() {
    super();
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    ipcMain = this;
  }
}

class BrowserWindowMock {
  webContents = {
    send: jest.fn(),
    isDestroyed: () => false,
  };
  static windows: BrowserWindowMock[];
  static getAllWindows = jest.fn(() => BrowserWindowMock.windows);
}

jest.mock('electron', () => ({
  ipcMain: new IpcMainMock(),
  BrowserWindow: BrowserWindowMock,
}));

import type { WebContents } from 'electron';

import { REDUX_BROADCAST_CHANNEL, REDUX_MAIN_PROCESS_CHANNEL } from '../src/ipc/types';
import type { AnyAction } from '../src/utils';

import { createContainer } from './container';
import { Ipc } from './ipc';

let container: ReturnType<typeof createContainer>;
let ipc: Ipc;

const TEST_ACTION: AnyAction = { type: 'test_action' };

beforeEach(() => {
  BrowserWindowMock.windows = [new BrowserWindowMock(), new BrowserWindowMock()];

  container = createContainer();
  ipc = container.get(Ipc);
});

afterEach(() => {
  container = null!;
});

describe('sendAction', () => {
  test('sends action to a window', () => {
    const window = BrowserWindowMock.windows[0];
    ipc.sendAction(window.webContents as unknown as WebContents, TEST_ACTION);

    expect(window.webContents.send).toHaveBeenLastCalledWith(REDUX_BROADCAST_CHANNEL, TEST_ACTION);
  });
});

describe('broadcastAction', () => {
  test('sends action to all windows', () => {
    ipc.broadcastAction(TEST_ACTION);

    for (const windows of BrowserWindowMock.windows) {
      expect(windows.webContents.send).toHaveBeenLastCalledWith(REDUX_BROADCAST_CHANNEL, TEST_ACTION);
    }
  });
});

describe('IPC broadcast of redux actions between windows', () => {
  test('broadcast the received action to all windows except the sender', () => {
    ipcMain.emit(REDUX_BROADCAST_CHANNEL, { sender: BrowserWindowMock.windows[0].webContents }, TEST_ACTION);

    expect(BrowserWindowMock.windows[0].webContents.send).not.toHaveBeenCalled();
    expect(BrowserWindowMock.windows[1].webContents.send).toHaveBeenLastCalledWith(REDUX_BROADCAST_CHANNEL, TEST_ACTION);
  });
});

describe('actions for the main process', () => {
  test('delivers the actions according to their types', () => {
    const actions1: AnyAction[] = [];
    const actions2: AnyAction[] = [];
    ipc.getActions('action1').subscribe(action => actions1.push(action.action));
    ipc.getActions('action2').subscribe(action => actions2.push(action.action));

    ipcMain.emit(REDUX_MAIN_PROCESS_CHANNEL, {}, TEST_ACTION);
    ipcMain.emit(REDUX_MAIN_PROCESS_CHANNEL, {}, { type: 'action1' });
    ipcMain.emit(REDUX_MAIN_PROCESS_CHANNEL, {}, { type: 'action2' });

    expect(actions1).toEqual([{ type: 'action1' }]);
    expect(actions2).toEqual([{ type: 'action2' }]);
  });
});
