import React, { useEffect } from 'react';
import { P, match } from 'ts-pattern';
import { useSelector } from 'react-redux';
import { Button, Icons, NoticeBanner, Modal, Loader, ButtonRow } from '@zaber/react-library';

import { isMatch, useActions } from '../../utils';
import { Editions, environment } from '../../environment';
import { actions as actionsDefinition } from '../actions';
import { PART_KEY } from '../types';

import { selectLoading, selectDialogOpen, selectHasConfigChanges, selectWriting } from './selectors';
import { DeviceAndAxisSelect, MotionAxisSelect } from './DeviceAndAxisSelect';
import { Illuminator } from './Illuminator';
import { ObjectiveChanger } from './ObjectiveChanger';
import { Autofocus } from './Autofocus';
import { Trigger } from './Trigger';

const ConfigEdit: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const writing = useSelector(selectWriting);
  const hasChanges = useSelector(selectHasConfigChanges);
  const isWriting = isMatch({ writing: true }, writing);
  return <>
    <div className="table">
      <Illuminator/>
      <ObjectiveChanger/>
      <DeviceAndAxisSelect<'filterChanger'> configKey="filterChanger" label="Filter Changer" deviceOnly defaultConfig={{}}/>
      <MotionAxisSelect configKey="focus" label="Focus Axis"/>
      <MotionAxisSelect configKey="x" label="X Axis"/>
      <MotionAxisSelect configKey="y" label="Y Axis"/>
      <Autofocus/>
      <Trigger/>
    </div>
    <ButtonRow className="save">
      <Button
        onClick={actions.writeConfig}
        disabled={isWriting}>
        Set up {hasChanges && <Icons.ErrorWarning title="Configuration has changes"/>}
      </Button>
      <Button color="grey"
        disabled={isWriting}
        onClick={() => actions.resetConfigDialog()}>
        Cancel
      </Button>
    </ButtonRow>
    <div className="result">
      {isMatch({ error: P.string }, writing) && <NoticeBanner type="error">
        {writing.error}
      </NoticeBanner>}
      {isMatch({ success: true }, writing) && <NoticeBanner type="success">
        Setup successful.
      </NoticeBanner>}
    </div>
    {environment.edition !== Editions.Public && <NoticeBanner type="info">
      <b>Internal note:</b> The component setup tags the microscope components using storage key "{PART_KEY}".
      It also changes certain settings/tuning on specific devices like LDA, ASR, ADR.
      The setup is necessary for ZML microscope API component detection to work properly.
    </NoticeBanner>}
  </>;
};

const Config: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const loading = useSelector(selectLoading);

  useEffect(() => {
    actions.loadConfig();
  }, []);

  return (<div className="config">
    {match(loading)
      .with({ error: P.string }, ({ error }) =>
        <NoticeBanner type="error">
          Error loading configuration: {error}
        </NoticeBanner>)
      .with({ loading: true }, () => <div className="loading"><Loader size="small"/>Loading configuration...</div>)
      .with({ loaded: P.any }, () => <ConfigEdit/>)
      .with(P.nullish, () => null)
      .exhaustive()}
  </div>);
};

export const ConfigDialog: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const isOpen = useSelector(selectDialogOpen);
  return (
    <Modal
      className="component-setup"
      isOpen={isOpen}
      headerIcon={<Icons.Settings/>}
      headerText="Component Setup"
      onRequestClose={() => actions.resetConfigDialog()}>
      {isOpen && <Config/>}
    </Modal>);
};
