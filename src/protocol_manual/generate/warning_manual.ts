import { ascii } from '@zaber/motion';

import type { ManualProfilingData, WarningFlagManual } from '../types';
import noDocs from '../no-docs.html';
import noDocsFw from '../no-docs-fw.html';

import { injectLinks, parsedManual, pointImgToSource, profileElement, stripZaber } from './profile';

export const DRIVER_DISABLED_FLAGS = [
  ascii.WarningFlags.HARDWARE_EMERGENCY_STOP,
  ascii.WarningFlags.OVERVOLTAGE_OR_UNDERVOLTAGE,
  ascii.WarningFlags.DRIVER_DISABLED_NO_FAULT,
  ascii.WarningFlags.CURRENT_INRUSH_ERROR,
  ascii.WarningFlags.MOTOR_TEMPERATURE_ERROR,
  ascii.WarningFlags.DRIVER_DISABLED,
] as string[];

export const CLEARABLE_FLAGS = [
  ascii.WarningFlags.ENCODER_ERROR,
  ascii.WarningFlags.INDEX_ERROR,
  ascii.WarningFlags.ANALOG_ENCODER_SYNC_ERROR,
  ascii.WarningFlags.OVERDRIVE_LIMIT_EXCEEDED,
  ascii.WarningFlags.STALLED_AND_STOPPED,
  ascii.WarningFlags.STREAM_BOUNDS_ERROR,
  ascii.WarningFlags.INTERPOLATED_PATH_DEVIATION,
  ascii.WarningFlags.LIMIT_ERROR,
  ascii.WarningFlags.EXCESSIVE_TWIST,
  ascii.WarningFlags.UNEXPECTED_LIMIT_TRIGGER,
  ascii.WarningFlags.STALLED_WITH_RECOVERY,
  ascii.WarningFlags.DISPLACED_WHEN_STATIONARY,
  ascii.WarningFlags.VALUE_ROUNDED,
  ascii.WarningFlags.VALUE_TRUNCATED,
] as string[];

export function generateWarningFlagManual(
  flag: string, { firmwareVersion, baseUrl, includes }: ManualProfilingData
): WarningFlagManual {
  const body = parsedManual();
  let name = `Warning Flag ${flag}`;
  if (firmwareVersion.major < 7) {
    return { html: noDocsFw, name };
  }
  const selector = `#topic_message_format_warning_flags__topic_message_format_warning_flags_${flag}`;
  let articleInBody = body.querySelector(selector);
  if (!articleInBody) {
    return { html: noDocs, name };
  }

  if (articleInBody.tagName === 'DT' && articleInBody.nextElementSibling?.tagName === 'DD') {
    articleInBody = articleInBody.nextElementSibling;
  }
  const article = articleInBody.cloneNode(true) as HTMLElement;
  profileElement(article, includes);
  injectLinks(article, baseUrl);
  pointImgToSource(article);
  stripZaber(article);
  const titleElement = article.firstElementChild;
  if (titleElement) {
    name = titleElement.textContent ?? 'No name';
    titleElement.remove();
  }

  return { html: article.outerHTML, name };
}
