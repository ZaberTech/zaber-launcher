import os from 'os';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import {
  Button,
  CopyToClipboard,
  HelpTooltip,
  Icons,
  InputEditor,
  Modal,
  NoticeBanner,
  NumericInputEditor,
  Text,
  Toggle
} from '@zaber/react-library';
import { match, P } from 'ts-pattern';

import type { RootState } from '../store';
import type { LocalShareConfig as Config } from '../app_components';
import { ExternalLink } from '../components';
import { ConnectionTable, RouterType } from '../connection_manager';
import { LauncherName } from '../preferences';
import { OneTimeMessage } from '../help';

import { selectShareConfig, selectUpdateConfig, selectShareStatus } from './selectors';
import { actions as actionsDefinition } from './actions';
import { DEFAULT_CONFIG, MAX_PORT_NUM } from './types';

interface StateProps {
  share: ReturnType<typeof selectShareConfig>;
  update: ReturnType<typeof selectUpdateConfig>;
  status: ReturnType<typeof selectShareStatus>;
}

interface DispatchProps {
  actions: typeof actionsDefinition;
}

interface State {
  editedPortValue: number | null | 'not-editing';
}

export const GET_STATUS_INTERVAL = 5000;

class LocalShareConfigBase extends Component<StateProps & DispatchProps, State> {
  private lastUpdated?: keyof Config;
  private getStatusInterval?: ReturnType<typeof setInterval>;
  private hostname: string;

  constructor(props: StateProps & DispatchProps) {
    super(props);
    this.hostname = os.hostname();
    this.state = { editedPortValue: 'not-editing' };
  }

  componentDidMount() {
    const { actions } = this.props;
    actions.getCurrentConfig();
    actions.getCurrentStatus();

    this.getStatusInterval = setInterval(() => actions.getCurrentStatus(), GET_STATUS_INTERVAL);
  }

  componentWillUnmount() {
    if (this.getStatusInterval != null) {
      clearInterval(this.getStatusInterval);
    }
  }

  render() {
    const { share, update, status, actions } = this.props;
    if (!share.config || !status.status) {
      return <>
        {(share.reading || status.reading) && 'Loading...'}
        {share.error && <>Error: {share.error}</>}
        {status.error && <>Error: {status.error}</>}
      </>;
    }

    const pendingToggle = share.config.enabled !== status.status.isRunning && !status.status.lastRunError;

    const port = match(share.mode)
      .returnType<{ mode: InputEditor.Mode; value: number | null}>()
      .with(P.union('initialize', 'write'), mode => ({ mode, value: share.config?.port ?? null }))
      .otherwise(() => {
        if (this.state.editedPortValue === 'not-editing') {
          return { mode: 'display', value: share.config?.port ?? null };
        } else {
          return { mode: 'edit', value: this.state.editedPortValue };
        }
      });

    return <div className="share-config">
      <div className="config-section">
        <Icons.Network/>
        <div>
          <div className="status">
            <Text t={Text.Type.H4}>Share my Devices over Local Area Network&emsp;</Text>
            <Toggle
              title="Toggle Sharing"
              loading={pendingToggle}
              disabled={update.updating && this.lastUpdated !== 'enabled'}
              value={share.config.enabled}
              onValueChange={value => this.updateConfig({ enabled: value })}/>&ensp;
            <HelpTooltip>Allow others on your local network to connect and use your devices.</HelpTooltip>
          </div>

          <LauncherName/>

          <div className="discoverable">
            <Text className="label">Discoverable&emsp;</Text>
            <Toggle
              title="Toggle Discoverable"
              loading={update.updating && this.lastUpdated === 'discoverable'}
              disabled={update.updating && this.lastUpdated !== 'discoverable'}
              value={share.config.discoverable}
              onValueChange={value => this.updateConfig({ discoverable: value })}/>&ensp;
            <HelpTooltip>Allow others on your local network to discover this Zaber Launcher from "Add New Connection" dialog.</HelpTooltip>
          </div>

          <div className="hostname">
            <Text className="label">Hostname:&emsp;</Text>
            <CopyToClipboard copyText={this.hostname}>{this.hostname}</CopyToClipboard>
            <HelpTooltip>
              The name of the computer on the local area network.
              You can use it to identify this computer when adding new connections
              or in Zaber Motion Library programs on other computers.
            </HelpTooltip>
          </div>

          <div className="port">
            <label htmlFor="tcp-ip-port"><Text className="label">TCP/IP Port:</Text></label>
            <NumericInputEditor
              value={port.value}
              id="tcp-ip-port"
              min={1} max={MAX_PORT_NUM}
              defaultValue={DEFAULT_CONFIG.port}
              isValid={value => value >= 1 && value <= MAX_PORT_NUM}
              mode={port.mode}
              onChange={change => {
                if (change.mode === 'edit') {
                  this.setState({ editedPortValue: change.value });
                } else {
                  if (change.mode === 'write') {
                    this.updateConfig({ port: change.value });
                  }
                  this.setState({ editedPortValue: 'not-editing' });
                }
              }}
            />
          </div>

          {status.status.lastRunError && <NoticeBanner>Error: {status.status.lastRunError}</NoticeBanner>}
          {status.status.lastDiscoverabilityError && <NoticeBanner>Error: {status.status.lastDiscoverabilityError}</NoticeBanner>}
          {update.error && <NoticeBanner>Error updating configuration: {update.error}</NoticeBanner>}
        </div>
      </div>

      <NoticeBanner type="warning">
        Share your devices only in networks that you trust.
      </NoticeBanner>

      <Text t={Text.Type.Instruction}>Below are your connections that will be shared over the network:</Text>

      <ConnectionTable filter={router => router.type === RouterType.Local}/>

      {update.warningVisible &&
      <Modal
        headerIcon={<Icons.Network/>}
        headerText="Sharing devices for the first time"
        buttons={
          <Button onClick={() => actions.toggleSharingWarning(false)}>OK</Button>
        }
        onRequestClose={() => actions.toggleSharingWarning(false)}>
        <div>When you share your devices for the first time,
        you may need to allow the zaber-message-router program
        to access your network through your firewall.</div>

        <div>The firewall rule is required for sharing to work.
        Add the rule only for networks that you trust.</div>
      </Modal>}

      <OneTimeMessage messageId="localShareZML" message={dismiss =>
        <NoticeBanner type="info" closer={dismiss}>
          Tip: Use Network Sharing to control your devices remotely from&nbsp;
          <ExternalLink
            url="https://www.zaber.com/software/docs/motion-library/ascii/references/python/#open95network95share">
            Zaber Motion Library
          </ExternalLink>
          &nbsp;programs running on a different computer.
        </NoticeBanner>
      }/>
    </div>;
  }

  updateConfig(update: Partial<Config>) {
    const { actions } = this.props;
    actions.updateConfig(update);
    this.lastUpdated = Object.keys(update)[0] as keyof Config;
  }
}


export const LocalShareConfig = connect(
  (state: RootState): StateProps => ({
    share: selectShareConfig(state),
    update: selectUpdateConfig(state),
    status: selectShareStatus(state),
  }),
  (dispatch: Dispatch): DispatchProps => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(LocalShareConfigBase);
