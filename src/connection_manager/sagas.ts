import { all, takeLatest, put, takeEvery, call, fork, SagaReturnType as ART, spawn } from 'redux-saga/effects';
import type { SagaIterator } from 'redux-saga';
import _ from 'lodash';
import {
  ascii, binary, DeviceAddressConflictException, DeviceDbFailedException, CommandFailedException, NoDeviceFoundException,
  FirmwareVersion, SettingNotFoundException, ConnectionFailedException, ConnectionClosedException,
  DeviceFailedException,
} from '@zaber/motion';
import { castAny, notNil, Nullable } from '@zaber/toolbox';

import { getContainer } from '../container';
import { Action, takeLeadingUniq, throwUnexpectedError } from '../utils';
import { LOCAL_ROUTER_URL } from '../message_router';
import { MessageRoutersService } from '../message_router/service';
import { makeConnectionKey, getRouterUrl, makeRouterKey, makeDeviceKey, EntityKey, makeAxisKey, extractConnectionKey } from '../keys';
import { Cache } from '../cache';
import { ipcActions } from '../ipc';
import { Editions, environment, Flavors } from '../environment';
import { ZaberApi, Connection, Storage, ConnectionError, HttpError } from '../app_components';
import { handleNonCriticalError } from '../errors/errors';
import { MovementDimensions, getMovementDimensions } from '../units';
import { getNumericSetting, getNumericSettings, REMOTE_MODE_SETTING, productHasSetting } from '../devices';

import { ActionTypes, actions, ActionsToPayloads } from './actions';
import {
  StoredRouter, RouterType, DeviceInfoWithAxes, RouterInfo, ROUTERS_STORAGE_KEY, IdentifiedAxisInfo, AxisInfo,
  LockstepGroup,
  LoadingDevicesError,
} from './types';
import { getConnection, getDevice, reidentifyDevices } from './devices';
import { runIdentificationHooks } from './identification_hooks';
import { BOSCH_REXROTH_SUPPORTED_DEVICES, isInternalDevice } from './utils';
import { DeviceDatabaseOfflineException, NoInternetConnectionException } from './exception';

export function* connectionManagerSaga(): SagaIterator {
  yield all([
    !environment.isTest ? fork(loadRouters) : null,
    takeEvery(ActionTypes.ADD_ROUTER, addRouter),
    takeEvery(ActionTypes.REMOVE_ROUTER, removeRouter),
    takeLatest(ActionTypes.LOAD_ROUTERS, loadRouters),
    takeLeadingUniq(ActionTypes.LOAD_CONNECTIONS, action => action.payload!.routerKey, loadConnections),
    takeLeadingUniq(ActionTypes.LOAD_DEVICES, action => action.payload!.connectionKey, loadDevices),
    takeEvery(ActionTypes.SET_REMOTE_MODE, setRemoteMode),
  ].filter(notNil));
}

type AddRouterAction = Action<ActionsToPayloads[ActionTypes.ADD_ROUTER]>;

function* addRouter(action: AddRouterAction): SagaIterator {
  const { payload: { router, saveAndBroadcast } } = action;
  if (!saveAndBroadcast) {
    return;
  }

  const storage = getContainer().get(Storage);

  const routers = storage.load<StoredRouter[]>(ROUTERS_STORAGE_KEY, []);
  routers.push(router);
  storage.save<StoredRouter[]>(ROUTERS_STORAGE_KEY, routers);

  const broadCastAction: AddRouterAction = { type: action.type, payload: { ...action.payload, saveAndBroadcast: false } };
  yield put(ipcActions.ipcBroadcast(broadCastAction));

  yield put(actions.loadConnections(makeRouterKey(router.url), false));
}

type RemoveRouterAction = Action<ActionsToPayloads[ActionTypes.REMOVE_ROUTER]>;

function* removeRouter(action: RemoveRouterAction): SagaIterator {
  const { payload: { key, saveAndBroadcast } } = action;

  if (saveAndBroadcast) {
    const url = getRouterUrl(key);
    const storage = getContainer().get(Storage);

    let routers = storage.load<StoredRouter[]>(ROUTERS_STORAGE_KEY, []);
    routers = routers.filter(router => router.url !== url);
    storage.save<StoredRouter[]>(ROUTERS_STORAGE_KEY, routers);

    const broadCastAction: RemoveRouterAction = { type: action.type, payload: { ...action.payload, saveAndBroadcast: false } };
    yield put(ipcActions.ipcBroadcast(broadCastAction));
  }

  const service = getContainer().get(MessageRoutersService);
  service.dropConnection(getRouterUrl(key));
}

function* loadRouters(): SagaIterator {
  const storage = getContainer().get(Storage);

  const routers = storage.load<StoredRouter[]>(ROUTERS_STORAGE_KEY, []);

  routers.push({
    type: RouterType.Local,
    url: LOCAL_ROUTER_URL,
  });

  yield put(actions.routersLoaded(routers));

  for (const router of routers) {
    const useCache = router.type !== RouterType.Local;
    yield put(actions.loadConnections(makeRouterKey(router.url), useCache));
  }
}

function isSameConnectionList(list1: Connection[], list2: Nullable<Connection[]>): boolean {
  return _.isEqual(list1.map(c => c.id), list2?.map(c => c.id));
}

function* loadConnections({ payload: { routerKey, useCache } }: Action<ActionsToPayloads[ActionTypes.LOAD_CONNECTIONS]>): SagaIterator {
  const cacheKey = `${routerKey}_info`;
  const cache = getContainer().get(Cache);

  const infoCached = cache.load<RouterInfo>(cacheKey);
  let info = useCache ? infoCached : null;

  try {
    if (!info) {
      const service = getContainer().get(MessageRoutersService);
      const { api }: ART<typeof service.getConnection> = yield call([service, service.getConnection], getRouterUrl(routerKey));

      const connections: ART<typeof api.getConnections> = yield call([api, api.getConnections]);
      const name: ART<typeof api.getName> = yield call([api, api.getName]);

      info = { connections, name };
      cache.save(cacheKey, info);

      if (!isSameConnectionList(connections, infoCached?.connections)) {
        yield put(ipcActions.ipcBroadcast(actions.loadConnections(routerKey, true)));
      }
    }
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.connectionsLoadedErr(routerKey, (String)(err)));
    return;
  }

  yield put(actions.connectionsLoaded(routerKey, info));

  for (const connection of info.connections) {
    const useCacheDevices = !!(infoCached?.connections.some(cachedConnection => cachedConnection.id === connection.id));
    yield put(actions.loadDevices(makeConnectionKey(routerKey, connection.id), useCacheDevices));
  }
}

function* loadDevices(
  { payload: { connectionKey, useCache } }: Action<ActionsToPayloads[ActionTypes.LOAD_DEVICES]>
): SagaIterator {
  const cacheKey = `${connectionKey}_devices`;
  const cache = getContainer().get(Cache);

  let devices: DeviceInfoWithAxes[] | null = null;
  if (useCache) {
    devices = cache.load(cacheKey);
  }

  try {
    if (!devices) {
      const connection: ART<typeof getConnection> = yield call(getConnection, connectionKey);

      devices = (yield call(queryDevices, connection, connectionKey)) as ART<typeof queryDevices>;
      cache.save(cacheKey, devices);

      yield put(actions.devicesIdentified(connectionKey));
      yield put(ipcActions.ipcBroadcast(actions.loadDevices(connectionKey, true)));

      yield call(runIdentificationHooks, devices);
    } else {
      reidentifyDevices(devices.map(device => device.key));
    }
  } catch (err) {
    throwUnexpectedError(err);

    const error: LoadingDevicesError = {
      reason: [ConnectionFailedException, ConnectionClosedException].some(ErrType => err instanceof ErrType) ? 'connection' : 'other',
      message: err.message,
    };

    yield put(actions.devicesLoadedErr(connectionKey, error));
    return;
  }

  yield put(actions.devicesLoaded(connectionKey, devices));
  yield spawn(fetchDeviceImages, devices);
}

interface QueryDevicesOptions {
  noDevicesOk?: boolean;
}

export function* queryBinaryDevices(
  connection: binary.Connection,
): SagaIterator<number> {
  try {
    const devices: ART<typeof connection.detectDevices> = yield call([connection, connection.detectDevices], { identifyDevices: false });
    return devices.length;
  } catch (err) {
    if (err instanceof DeviceAddressConflictException) {
      return err.details.deviceAddresses.length;
    } else if (err instanceof NoDeviceFoundException) {
      return 0;
    }
    throw err;
  }
}

export function* queryDevices(
  connection: ascii.Connection,
  connectionKey: EntityKey,
  { noDevicesOk = true }: QueryDevicesOptions = {},
): SagaIterator<DeviceInfoWithAxes[]> {
  const devices: DeviceInfoWithAxes[] = [];
  const api = getContainer().get(ZaberApi);

  let asciiDevices: ascii.Device[];
  try {
    asciiDevices = yield call([connection, connection.detectDevices], { identifyDevices: false });
  } catch (err) {
    if (err instanceof DeviceAddressConflictException) {
      yield call([connection, connection.renumberDevices]);
      asciiDevices = yield call([connection, connection.detectDevices], { identifyDevices: false });
    } else if (err instanceof NoDeviceFoundException && noDevicesOk) {
      asciiDevices = [];
    } else {
      throw err;
    }
  }

  yield call([connection, connection.forgetDevices], asciiDevices.map(device => device.deviceAddress));

  for (const asciiDevice of asciiDevices) {
    const deviceKey = makeDeviceKey(connectionKey, asciiDevice.deviceAddress);
    try {
      const identity: ascii.DeviceIdentity = yield call([asciiDevice, asciiDevice.identify]);
      const asciiAxes = _.range(1, identity.axisCount + 1).map(axisNumber => asciiDevice.getAxis(axisNumber));
      const peripheralLike = asciiAxes
        .filter(axis => axis.identity.peripheralId > 0 || axis.identity.isPeripheral)
        .map(axis => ({
          peripheralID: axis.identity.peripheralId,
          modified: axis.identity.isModified,
        }));

      const apiInfo: ART<typeof api.getDeviceInfo> = yield call(
        [api, api.getDeviceInfo],
        {
          deviceID: identity.deviceId,
          fwVersion: identity.firmwareVersion,
          modified: identity.isModified,
          peripherals: peripheralLike,
        },
      );

      if (environment.flavor === Flavors.Rexroth && !apiInfo.device.name.match(BOSCH_REXROTH_SUPPORTED_DEVICES)) {
        throw new DeviceDbFailedException('Zaber devices are not supported in this application.', { code: 'not-found' });
      }

      const platformId: ART<typeof getNumericSetting> = yield call(getNumericSetting, asciiDevice, 'system.platform');
      const isKeyed: ART<typeof getNumericSetting> = yield call(getNumericSetting, asciiDevice, 'bootloader.keyed');
      let isRemote: boolean = false;
      if (productHasSetting(REMOTE_MODE_SETTING, apiInfo.settings)) {
        const remote: ART<typeof getNumericSetting> = yield call(getNumericSetting, asciiDevice, REMOTE_MODE_SETTING);
        isRemote = remote !== 0;
      }

      let axisSerialNumbers: ART<typeof getNumericSettings> | undefined;
      if (peripheralLike.length > 0) {
        axisSerialNumbers = yield call(getNumericSettings, asciiDevice, 'peripheral.serial', identity.axisCount);
      }

      const axes = asciiAxes.map((axis): IdentifiedAxisInfo => {
        const { axisNumber, identity } = axis;
        const peripheral = apiInfo.peripherals?.[axisNumber - 1];
        return {
          key: makeAxisKey(deviceKey, axisNumber),
          axisNumber,
          isPeripheralLike: peripheral != null,
          serialNumber: axisSerialNumbers?.[axisNumber - 1] ?? null,
          label: axis.label || null,
          identity,
          product: {
            id: peripheral?.device.product_id ?? 0,
            capabilities: peripheral?.capabilities ?? apiInfo.capabilities,
            commandTree: peripheral?.command_tree ?? apiInfo.command_tree,
            conversionTable: peripheral?.conversion_table ?? apiInfo.conversion_table,
            settings: peripheral?.settings ?? apiInfo.settings,
          },
          nonIdentifiedInfo: null,
          canMove: [ascii.AxisType.LINEAR, ascii.AxisType.ROTARY].includes(axis.identity.axisType),
          movementDimensions: determineMovementDimensions(axis),
        };
      });

      const locksteps: ART<typeof queryLocksteps> = yield call(queryLocksteps, asciiDevice);

      devices.push({
        address: asciiDevice.deviceAddress,
        key: deviceKey,
        identity,
        product: {
          id: apiInfo.device.product_id,
          capabilities: apiInfo.capabilities,
          commandTree: apiInfo.command_tree,
          conversionTable: apiInfo.conversion_table,
          settings: apiInfo.settings,
        },
        platformId,
        isKeyed: !!isKeyed,
        label: asciiDevice.label || null,
        axes,
        axisCount: axes.length,
        isMultiAxis: axes.length > 1,
        isController: axes.some(axis => axis.identity.isPeripheral),
        isRemote,
        nonIdentifiedInfo: null,
        locksteps,
      });
    } catch (err) {
      const handleableExceptions = [DeviceDbFailedException, CommandFailedException, ZaberApi.Error, DeviceFailedException];
      if (!handleableExceptions.some(Exception => err instanceof Exception)) {
        throw err;
      }
      if (castAny(err, ZaberApi.Error)?.innerError instanceof ConnectionError
        || castAny(err, DeviceDbFailedException)?.details.code === 'no-access') {
        if (environment.offline) {
          throw new DeviceDatabaseOfflineException('Error accessing offline device database. Please contact Zaber for support.');
        } else {
          throw new NoInternetConnectionException([
            'No internet connection.',
            'A connection is required to download device data and identify Zaber devices.',
            'Zaber devices may not be detected until an internet connection is established.',
            'If your computer is connected to the internet, please check your firewall settings.',
            'If an internet connection is not available, consider installing Zaber Launcher Offline Edition instead.'
          ].join(' '));
        }
      }

      const device: ART<typeof queryUnidentifiableDevice> = yield call(queryUnidentifiableDevice, deviceKey, asciiDevice);
      devices.push(device);

      if (environment.edition === Editions.Public && !isInternalDevice(device)) {
        handleNonCriticalError(err, undefined, device.nonIdentifiedInfo!);
      }
    }
  }

  return devices;
}

function* queryLocksteps(asciiDevice: ascii.Device): SagaIterator<LockstepGroup[]> {
  let groupCount: number;
  try {
    groupCount = yield call([asciiDevice.settings, asciiDevice.settings.get], ascii.SettingConstants.LOCKSTEP_NUMGROUPS);
  } catch (err) {
    if (err instanceof CommandFailedException) {
      return [];
    }
    throw err;
  }

  const groups: LockstepGroup[] = [];
  for (let groupNumber = 1; groupNumber <= groupCount; groupNumber++) {
    const lockstep = asciiDevice.getLockstep(groupNumber);
    const isEnabled: ART<typeof lockstep.isEnabled> = yield call([lockstep, lockstep.isEnabled]);
    if (!isEnabled) {
      continue;
    }

    const axisNumbers: ART<typeof lockstep.getAxisNumbers> = yield call([lockstep, lockstep.getAxisNumbers]);
    groups.push({
      groupNumber,
      axisNumbers,
    });
  }
  return groups;
}

export function* fetchDeviceImages(devices: DeviceInfoWithAxes[]) {
  if (environment.offline) { return }

  const deviceIdSet = new Set<number>();
  const peripheralIdSet = new Set<number>();
  for (const device of devices) {
    if (device.identity) {
      deviceIdSet.add(device.identity.deviceId);
    }
    for (const axis of device.axes) {
      if (axis.identity != null && axis.identity.peripheralId > 0) {
        peripheralIdSet.add(axis.identity.peripheralId);
      }
    }
  }

  yield call(fetchDeviceImagesById, Array.from(deviceIdSet), Array.from(peripheralIdSet));
}

export function* fetchDeviceImagesById(deviceIds: number[], peripheralIds: number[]) {
  try {
    const zaberApi = getContainer().get(ZaberApi);
    const images: ZaberApi.WebsiteProductImage[] = yield call(
      [zaberApi, zaberApi.getWebsiteImageForProducts], deviceIds, peripheralIds
    );
    const thumbnails = Object.fromEntries(
      images.map((image): [number, string] => [image.id, `https://www.zaber.com${image.thumbnail}`])
    );
    yield put(actions.setThumbnails(thumbnails));
  } catch (err) {
    const innerError = castAny(err, ZaberApi.Error)?.innerError;
    if (innerError instanceof HttpError) {
      if (innerError.status !== 404) {
        handleNonCriticalError(err, 'info');
      }
    } else if (!(innerError instanceof ConnectionError)) {
      handleNonCriticalError(err, 'info');
    }
  }
}

function determineMovementDimensions(axis: ascii.Axis): MovementDimensions | null {
  try {
    const { identity } = axis;
    if (identity.axisType === ascii.AxisType.UNKNOWN) {
      return null;
    }

    for (const setting of ['pos', 'maxspeed', 'accel']) {
      if (!axis.settings.canConvertNativeUnits(setting)) {
        return null;
      }
    }
    return getMovementDimensions(identity.axisType);
  } catch (err) {
    if (err instanceof SettingNotFoundException) {
      return null;
    }
    throw err;
  }
}

function* queryUnidentifiableDevice(deviceKey: EntityKey, asciiDevice: ascii.Device): SagaIterator<DeviceInfoWithAxes> {
  const deviceId: ART<typeof getNumericSetting> = yield call(getNumericSetting, asciiDevice, 'deviceid');

  let firmwareVersion: FirmwareVersion | null = null;
  const version: ART<typeof getNumericSetting> = yield call(getNumericSetting, asciiDevice, 'version');
  const build: ART<typeof getNumericSetting> = yield call(getNumericSetting, asciiDevice, 'version.build');
  if (version != null) {
    firmwareVersion = {
      major: Math.floor(version),
      minor: (version * 100) % 100,
      build: build ?? 0,
    };
  }

  let axisCount: ART<typeof getNumericSetting> = yield call(getNumericSetting, asciiDevice, 'system.axiscount');
  if (axisCount == null) {
    axisCount = 0;
  }

  let peripheralIds: ART<typeof getNumericSettings> = [];
  if (axisCount > 0) {
    peripheralIds = yield call(getNumericSettings, asciiDevice, 'peripheral.id', axisCount);
  }

  return {
    key: deviceKey,
    address: asciiDevice.deviceAddress,
    identity: null,
    axes: _.range(axisCount).map((i): AxisInfo => ({
      key: makeAxisKey(deviceKey, i + 1),
      axisNumber: i + 1,
      isPeripheralLike: peripheralIds[i] != null,
      nonIdentifiedInfo: {
        peripheralId: peripheralIds[i] ?? null,
      },
      identity: null,
      product: null,
      serialNumber: null,
      canMove: false,
      movementDimensions: null,
      label: null,
    })),
    axisCount,
    isMultiAxis: axisCount > 1,
    isController: peripheralIds.some(id => id != null),
    isRemote: false,
    platformId: 0,
    isKeyed: false,
    label: null,
    product: null,
    nonIdentifiedInfo: {
      deviceId: deviceId ?? 0,
      firmwareVersion,
    },
    locksteps: null,
  };
}


function* setRemoteMode(
  { payload: { key, isRemote } }: Action<ActionsToPayloads[ActionTypes.SET_REMOTE_MODE]>
): SagaIterator {
  try {
    const zmlDevice: ascii.Device = yield call(getDevice, key);
    yield call([zmlDevice.settings, zmlDevice.settings.set], REMOTE_MODE_SETTING, isRemote ? 1 : 0);
    yield put(actions.loadDevices(extractConnectionKey(key), false));
  } catch (err) {
    throwUnexpectedError(err);
  }
}
