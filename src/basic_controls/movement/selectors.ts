import { createSelector } from 'reselect';

import { selectBasicControls } from '../../store';

export const selectBasicControlsMovement = createSelector(selectBasicControls, state => state.movement);

export const selectAxisStates = createSelector(selectBasicControlsMovement, state => state.axes);
