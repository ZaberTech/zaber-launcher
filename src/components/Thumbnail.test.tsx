import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import type { Container } from 'inversify';

import { mockStorage, StorageMock, waitUntilPass, wrapWithNewStore } from '../test';
import { connectionManagerActions } from '../connection_manager';
import { createContainer } from '../container';

import { Thumbnail } from './Thumbnail';

let container: Container;
let storageMock: StorageMock;
let wrapper: RenderResult;

const TestThumbnail = wrapWithNewStore(Thumbnail);

const title = 'testThumbnail';

beforeEach(() => {
  container = createContainer();
  storageMock = mockStorage(container);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;
  container = null!;
  storageMock.stored = {};
});

test('thumbnail renders', async () => {
  const src = 'thumbnailSrc';
  wrapper = render(<TestThumbnail deviceOrPeripheralId={0} title={title} altExt="device"/>);
  TestThumbnail.testStore.dispatch(connectionManagerActions.setThumbnails({ 0: src }));
  await waitUntilPass(() => {
    const thumbnailElement = wrapper.getByTitle(title);
    expect(thumbnailElement.nodeName).toBe('OBJECT');
    expect(thumbnailElement.getAttribute('data')).toEqual(src);
  });
});

test('thumbnail uses fallback', () => {
  wrapper = render(<TestThumbnail deviceOrPeripheralId={0} title={title} altExt="device"/>);
  const thumbnailElement = wrapper.getByTitle(title);
  expect(thumbnailElement.nodeName).toBe('IMG');
  expect(thumbnailElement.getAttribute('src')).toEqual('FILE_PATH');
  expect(thumbnailElement.getAttribute('alt')).toEqual('Unknown device');
});

describe('Generic Third Party Thumbnails', () => {
  test('Linear', () => {
    wrapper = render(<TestThumbnail deviceOrPeripheralId={88800} title={title} altExt="device"/>);
    const thumbnailElement = wrapper.getByTitle(title);
    expect(thumbnailElement.nodeName).toBe('IMG');
    expect(thumbnailElement.getAttribute('src')).toEqual('FILE_PATH');
    expect(thumbnailElement.getAttribute('alt')).toEqual('Linear third party peripheral');
  });

  test('Rotary', () => {
    wrapper = render(<TestThumbnail deviceOrPeripheralId={88807} title={title} altExt="device"/>);
    const thumbnailElement = wrapper.getByTitle(title);
    expect(thumbnailElement.nodeName).toBe('IMG');
    expect(thumbnailElement.getAttribute('src')).toEqual('FILE_PATH');
    expect(thumbnailElement.getAttribute('alt')).toEqual('Rotary third party peripheral');
  });
});
