import React, { useLayoutEffect, useRef } from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';

import { Section, SectionMenu } from '../components';

import {
  selectDisplayedSettingsByCategories,
  selectCurrentEntity,
} from './selectors';
import { Setting } from './Setting';
import { Category } from './Category';
import { OnSearchChange } from './SettingsSearchBar';
import { actions as actionsDefinition } from './actions';

export const SettingsList: React.FC = () => {
  const sectionsRef = useRef<HTMLDivElement>(null);
  const settingsByCategories = useSelector(selectDisplayedSettingsByCategories);
  const current = useSelector(selectCurrentEntity)!;
  const actions = useActions(actionsDefinition);

  useLayoutEffect(() => {
    const sectionDiv = sectionsRef.current!;
    setTimeout(() => sectionDiv.scroll({ top: current.scrollPosition }));
    return () => { actions.updateScrollPosition(current.key, sectionDiv.scrollTop) };
  }, []);

  return (<div className="settings-container">
    <SectionMenu
      refToScrollingSections={sectionsRef}
      menuItems={settingsByCategories.map(category => ({
        id: category.key,
        name: category.meta[0].humanName,
        disabled: category.settings.length === 0,
        isSub: category.meta.length > 1,
      }))}
      content={(on, off) => <>
        {settingsByCategories
          .filter(category => category.settings.length > 0)
          .map(category => (
            <Section className="sections-subgrid" key={category.key} sectionId={category.key} on={on} off={off}>
              <Category category={category}>
                {category.settings.map(setting =>
                  <Setting key={setting.id} setting={setting}/>
                )}
              </Category>
            </Section>
          ))
        }
        <div className="space-fill"/>
      </>}
    />

    <OnSearchChange onChange={() => {
      sectionsRef.current?.scroll({ top: 0 });
    }}/>
  </div>);
};
