import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { EditableField, Icons, Text } from '@zaber/react-library';

import { ConnectionType } from '../app_components';
import { tryAccess } from '../utils';
import type { ConnectionManagerConnectionState } from '../connection_manager';
import { originalConnectionName, connectionName } from '../connection_manager/utils';

import { actions } from './actions';
import { selectSetConnectionsName } from './selectors';
import { setConnectionNameDefault } from './reducer';

interface Props {
  connection: ConnectionManagerConnectionState;
  onExpandConnection: () => void;
}

export const ConnectionNameInput: React.FC<Props> = ({ connection, onExpandConnection }) => {
  const dispatch = useDispatch();
  const state = tryAccess(useSelector(selectSetConnectionsName), connection.key) ?? setConnectionNameDefault;
  const name = connectionName(connection);
  const port = originalConnectionName(connection);
  const tip = port !== name ? `Rename Connection (originally ${port})` : 'Rename Connection';

  return <div className="connection-name-input">
    <div onClick={onExpandConnection}>
      {connection.type === ConnectionType.TCP_IP && <Icons.EthernetSocket className="connection-type-icon"/>}
      {connection.type === ConnectionType.SERIAL_PORT && <Icons.Usb className="connection-type-icon"/>}
    </div>
    &nbsp;
    <EditableField
      type="string"
      editTitle={tip}
      disabled={state.writing}
      defaultValue={port !== name ? port : undefined}
      value={name}
      textType={Text.Type.H4}
      onClickComponent={onExpandConnection}
      onValueChange={value => dispatch(actions.setConnectionName(connection.key, value.trim()))}/>
  </div>;
};
