import { CommandFailedException } from '@zaber/motion';

import type { SectionDataDefinition } from '../types';

import { actionEnabled, actionRequiresPreset, limitEnabled, widthEnabled } from './limit';

export const limitHome: SectionDataDefinition<'limitHome'> = {
  initializers: {
    type: { type: 'setting', backing: 'limit.home.type' },
    source: { type: 'setting', backing: 'limit.home.source' },
    edge: { type: 'setting', backing: 'limit.home.edge' },
    width: { type: 'setting', backing: 'limit.home.width' },
    bidirectional: { type: 'setting', backing: 'limit.home.bidirectional' },
    action: { type: 'setting', backing: 'limit.home.action' },
    preset: { type: 'setting', backing: 'limit.home.preset' },
    posUpdate: { type: 'setting', backing: 'limit.home.posupdate' },
  },
  sectionPresent: async device => {
    try {
      await device.ascii.settings.get('limit.home.type');
    } catch (e) {
      if (e instanceof CommandFailedException && e.details.responseData === 'BADCOMMAND') {
        return false;
      }
      throw e;
    }
    return true;
  },
  fieldEnabled: {
    source: limitEnabled,
    edge: limitEnabled,
    width: widthEnabled,
    bidirectional: limitEnabled,
    action: limitEnabled,
    posUpdate: actionEnabled,
    preset: actionRequiresPreset,
  },
};
