import { ButtonRowConfirmCancel, ContextMenu, Icons, Input, Loader, Modal, NoticeBanner, Text } from '@zaber/react-library';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';
import type { EntityKey } from '../keys';

import { selectIsLabelValid, selectSetLabel, selectSetLabelSupported } from './selectors';
import { actions as actionsDefinition } from './actions';


export const SetLabel: React.FC = () => {
  const state = useSelector(selectSetLabel);
  const isValid = useSelector(selectIsLabelValid);
  const actions = useActions(actionsDefinition);
  const supported = useSelector(selectSetLabelSupported);
  const eraseDisabled = state.inProgress || !supported;

  const [initialLabel, setInitialLabel] = useState('');
  useEffect(() => {
    if (state.isOpen) {
      setInitialLabel(state.label);
    }
  }, [state.isOpen]);

  const writeDisabled = eraseDisabled || !isValid || state.label === initialLabel;

  if (!state.isOpen) { return null }

  return (
    <Modal
      className="connection-manager-ui set-label"
      headerIcon={<Icons.Edit/>}
      headerText="Set Label"
      buttons={
        <ButtonRowConfirmCancel
          confirmText="Set Label"
          onConfirm={writeDisabled ? 'disabled' : actions.setLabelWrite}
          onCancel={state.inProgress ? 'disabled' : actions.setLabelClose}
        >
          {state.inProgress && <Loader size="small" title="Loading"/>}
        </ButtonRowConfirmCancel>
      }
      onRequestClose={!state.inProgress ? actions.setLabelClose : undefined}>
      <Input
        title="Enter Label"
        clearable={state.label !== '' && !eraseDisabled}
        disabled={state.inProgress}
        autoFocus={true}
        value={state.label} onValueChange={actions.setLabelSetLabel}
        status={isValid ? undefined : 'error'}
        onKeyDown={e => {
          if (e.key === 'Enter' && !writeDisabled) {
            actions.setLabelWrite();
          }
        }}/>
      <div><Text t={Text.Type.Helper}>
      The label may be up to 32 characters long.
      </Text></div>
      {!supported && <NoticeBanner type="warning">
      Setting the label is not supported by your device.
      Please upgrade your firmware.
      </NoticeBanner>}
      {state.error && <NoticeBanner type="error">{state.error}</NoticeBanner>}
    </Modal>);
};

interface SetLabelContextMenuItemProps {
  entityKey: EntityKey;
  label: string | null;
}
export const SetLabelContextMenuItem: React.FC<SetLabelContextMenuItemProps> = ({ entityKey, label }) => (
  <SetLabelClick entityKey={entityKey} label={label}>{onClick =>
    <ContextMenu.Item icon={<Icons.Edit/>}
      onClick={onClick}>
      Set Label
    </ContextMenu.Item>
  }</SetLabelClick>);

interface SetLabelClickProps {
  entityKey: EntityKey;
  label: string | null;
  children: (onClick: () => void) => React.ReactNode;
}

export const SetLabelClick: React.FC<SetLabelClickProps> = ({ entityKey, label, children }) => {
  const actions = useActions(actionsDefinition);
  return (<>{children(() => actions.setLabelOpen(entityKey, label ?? ''))}</>);
};
