import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { ButtonRowConfirmCancel, Modal, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { AppIconsNeutral } from '../apps/ui_meta';

import { actions as actionDefinitions } from './actions';
import { selectDriverEnabled, selectDriverError } from './selectors';

export const DriverError: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const driverEnabled = useSelector(selectDriverEnabled);
  const driverError = useSelector(selectDriverError);

  return (
    <Modal
      className="error-modal"
      headerIcon={<AppIconsNeutral.CurrentTuner/>}
      headerText={`Error: Cannot ${driverEnabled ? 'Disable' : 'Enable'} Driver`}
      small
      bodyIcon="error"
      buttons={
        <ButtonRowConfirmCancel
          confirmText="Try Again"
          onConfirm={() => actions.setDriver(!driverEnabled)}
          onCancel={() => actions.setDriverDone()}
        />
      }
      onRequestClose={actions.setDriverDone}>
      <Text>{driverError}</Text>
    </Modal>
  );
};
