/* eslint-disable camelcase */
import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent, getByTitle, Matcher } from '@testing-library/react';
import _ from 'lodash';
import {
  Acceleration, CommandFailedException, CommandFailedExceptionData, InvalidDataException, Length, RequestTimeoutException, Units, Velocity,
} from '@zaber/motion';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { createContainer, destroyContainer } from '../container';
import { getParentWithClass, waitUntil, waitUntilPass, wrapWithNewStore } from '../test';
import { selectDevices, selectAxes, ProductInfo, IdentifiedDeviceInfo, connectionManagerActions } from '../connection_manager';
import { MessageRoutersService } from '../message_router';
import { mockSingleDevice, mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import {
  AxisMockBase, DeviceMockBase, ConnectionMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { RejectReasons } from '../protocol';
import { IntersectionObserverMockBase } from '../test/mocks/intersection_observer';

import { DeviceSettings } from './DeviceSettings';
import { selectSettingsBeingRead, selectConfiguringNetwork } from './selectors';

const ERROR_CLASS = 'status-error';

let intersectionObserverMock: IntersectionObserverMock;
class IntersectionObserverMock extends IntersectionObserverMockBase {
  constructor(callback: IntersectionObserverCallback, init: IntersectionObserverInit) {
    super(callback, init);
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    intersectionObserverMock = this;
  }
}

(window as unknown as { IntersectionObserver: typeof IntersectionObserverMock }).IntersectionObserver = IntersectionObserverMock;

HTMLElement.prototype.scroll = _.noop;
HTMLElement.prototype.scrollIntoView = _.noop;

let initialSettings: Record<string, number | string>;
const mockSettingGetFunc = jest.fn((settings, setting) => Promise.resolve(settings[setting]));
let canConvertNativeUnits = true;

let container: Container;

const fakeUnitConversion = (value: number, toUnits: Units): number => {
  switch (toUnits) {
    case Length.mm:
    case Velocity.MILLIMETRES_PER_SECOND:
    case Acceleration.MILLIMETRES_PER_SECOND_SQUARED:
      return value / 10;
    case Length.cm:
    case Velocity.CENTIMETRES_PER_SECOND:
    case Acceleration.CENTIMETRES_PER_SECOND_SQUARED:
      return value / 100;
    case Units.NATIVE: return value;
  }
  throw new Error(`Unknown units ${toUnits}`);
};
class SettingsMock {
  _settings: Record<string, number | string> = _.cloneDeep(initialSettings);
  get = jest.fn(async (setting, units) => {
    const rawValue = await mockSettingGetFunc(this._settings, setting);
    return fakeUnitConversion(rawValue, units);
  });
  getString = jest.fn(setting => mockSettingGetFunc(this._settings, setting));
  set = jest.fn((setting: string, value: number) => {
    this._settings[setting] = value;
    return Promise.resolve();
  });
  setString = jest.fn((setting, value) => {
    this._settings[setting] = value;
    return Promise.resolve();
  });
  convertFromNativeUnits = jest.fn((setting, value, units) => fakeUnitConversion(value, units));
  canConvertNativeUnits = jest.fn(() => canConvertNativeUnits);

  updateMockSettings = (settings: Record<string, number | string>) => {
    this._settings = {
      ...this._settings,
      ...settings
    };
  };
}

const loadDevicesActionMock = jest.fn(() => ({ type: 'NOTHING', payload: undefined }));
connectionManagerActions.loadDevices = loadDevicesActionMock;

let axes: AxisMock[];

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);
    axes.push(this);
  }
  settings = new SettingsMock();
}

let devices: DeviceMock[];

class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(id: number, connection: ConnectionMock) {
    super(id, connection);
    devices.push(this);
  }
  settings = new SettingsMock();
  genericCommand = jest.fn(async (_cmd: string) => null);
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const SETTING_ROW_BASE = { read_access_level: 1, write_access_level: 1, visibility: 'always', decimal_places: 0 };
const CONVERSION_TABLE = {
  rows: [
    { contextual_dimension_id: 1, dimension_name: 'Length' },
    { contextual_dimension_id: 2, dimension_name: 'Velocity' },
    { contextual_dimension_id: 3, dimension_name: 'Acceleration' },
  ]
} as ProductInfo['conversionTable'];

const INTEGRATED_SETTINGS = {
  rows: [
    { name: 'comm.address', ...SETTING_ROW_BASE },
    { contextual_dimension_id: 1, name: 'pos', ...SETTING_ROW_BASE },
    { contextual_dimension_id: 2, name: 'maxspeed', ...SETTING_ROW_BASE },
    { name: 'system.access', read_access_level: 1, write_access_level: 1, visibility: 'advanced' },
    { name: 'system.axiscount', ...SETTING_ROW_BASE },
    { name: 'version', ...SETTING_ROW_BASE, decimal_places: 2 },
    { name: 'system.hidden', read_access_level: 1, write_access_level: 1, visibility: 'hardware_modification' },
  ]
} as ProductInfo['settings'];

const DEVICE_SETTINGS = {
  rows: [
    { name: 'system.axiscount', ...SETTING_ROW_BASE },
    { name: 'deviceid', ...SETTING_ROW_BASE },
    { name: 'version', ...SETTING_ROW_BASE, decimal_places: 2 },
    { name: 'system.access', read_access_level: 1, write_access_level: 1, visibility: 'advanced'  },
  ]
} as ProductInfo['settings'];

const defaultPeripheralsModifier = (device: IdentifiedDeviceInfo): IdentifiedDeviceInfo => {
  device.product.conversionTable = CONVERSION_TABLE;
  device.product.settings = DEVICE_SETTINGS;

  device.axes.forEach(axis => {
    axis.product.conversionTable = CONVERSION_TABLE;
    axis.product.settings = {
      rows: [
        { contextual_dimension_id: 1, name: 'pos', ...SETTING_ROW_BASE },
        { contextual_dimension_id: 2, name: 'maxspeed', ...SETTING_ROW_BASE },
        { name: 'peripheral.id', ...SETTING_ROW_BASE },
      ]
    } as ProductInfo['settings'];
  });
  return device;
};

const TestDeviceSettings = wrapWithNewStore(DeviceSettings);
let wrapper: RenderResult;

const waitTillEnd = () => waitUntil(() => !selectSettingsBeingRead(TestDeviceSettings.testStore.getState()));
const waitAfterLoading = () => waitUntilPass(() => expect(wrapper.queryByText(/Loading/)).toBeNull());

function getSettingElement(text: Matcher) {
  return wrapper.getByText(text).parentElement!.parentElement!.parentElement!.parentElement!;
}

const inputHasValue = (setting: string, value: number | string) => {
  const input = wrapper.getByRole('spinbutton', { name: setting }) as HTMLInputElement;
  expect(input.value).toBe(value.toString());
};

const setValue = (setting: string, value: number | string) => {
  const input = wrapper.getByRole('spinbutton', { name: setting });
  fireEvent.change(input, { target: { value: value.toString() } });
};

const checkInputDisabled = (setting: string, disabled: boolean) => {
  const input = wrapper.getByRole('spinbutton', { name: setting }) as HTMLInputElement;
  expect(input.disabled).toBe(disabled);
};

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

  axes = [];
  devices = [];
  initialSettings = {};
  loadDevicesActionMock.mockClear();

  wrapper = render(<TestDeviceSettings/>);
});

afterEach(async () => {
  if (wrapper) {
    await waitTillEnd();
  }

  wrapper?.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;

  axes = null!;
  devices = null!;
  initialSettings = null!;
  intersectionObserverMock = null!;
  canConvertNativeUnits = true;
});

describe('settings autorefresh', () => {
  beforeEach(() => {
    const store = TestDeviceSettings.testStore;
    mockSingleDeviceWithPeripherals(store, { modifier: defaultPeripheralsModifier });
  });

  test('when user returns to app, all settings are refreshed', async () => {
    const store = TestDeviceSettings.testStore;
    const device = _.sample(selectDevices(store.getState()))!;
    initialSettings = {
      'deviceid': 30,
      'system.axiscount': 4,
      'system.access': 1,
    };
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => inputHasValue('deviceid', 30), 500);
    expect(devices[0].settings.get).toHaveBeenCalledTimes(DEVICE_SETTINGS.rows.length);

    devices[0].settings.get.mockClear();
    wrapper.unmount();

    devices[0].settings._settings.deviceid = 31;
    devices[0].settings._settings['system.axiscount'] = 12;
    wrapper = render(<TestDeviceSettings store={store}/>);
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => {
      inputHasValue('deviceid', 31);
      inputHasValue('system.axiscount', 12);
      inputHasValue('system.access', 1);
    });
    expect(devices[0].settings.get).toHaveBeenCalledTimes(DEVICE_SETTINGS.rows.length);
  });
});

describe('integrated device', () => {
  beforeEach(() => {
    const store = TestDeviceSettings.testStore;
    mockSingleDevice(store, device => {
      device.product!.conversionTable = CONVERSION_TABLE;
      device.product!.settings = INTEGRATED_SETTINGS;
      return device;
    });
  });

  test('renders visible device settings', async () => {
    const store = TestDeviceSettings.testStore;
    const device = _.sample(selectDevices(store.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    initialSettings = {
      'system.access': 1,
    };

    await waitUntilPass(() => {
      wrapper.getByText(/maxspeed/);
      wrapper.getByText(/pos/);
      wrapper.getByText(/system.axiscount/);
      expect(wrapper.queryByText(/system.hidden/)).toBeNull();
    });
  });

  test('reads all settings after selection', async () => {
    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;

    initialSettings = {
      'maxspeed': 333,
      'pos': 222,
      'system.axiscount': 1,
      'system.access': 2,
      'version': 7.22,
    };

    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => {
      inputHasValue('maxspeed', 33.3);
      inputHasValue('pos', 22.2);
      inputHasValue('system.axiscount', 1);
      inputHasValue('system.access', 2);
      inputHasValue('version', 7.22);
    });
  });

  test('reads all the settings after refresh all is pressed', async () => {
    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;

    initialSettings = {
      'maxspeed': 333,
      'pos': 222,
      'system.access': 1,
    };

    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => {
      inputHasValue('maxspeed', 33.3);
      inputHasValue('pos', 22.2);
    });

    const rawSettings = devices[0].settings._settings;
    rawSettings.maxspeed = 444;
    rawSettings.pos = 555;

    fireEvent.click(wrapper.getByTitle('Refresh All Settings'));

    await waitUntilPass(() => {
      inputHasValue('maxspeed', 44.4);
      inputHasValue('pos', 55.5);
    });
  });

  test('calls loadDevices when comm.address is changed', async () => {
    const NEW_ADDRESS = 14;

    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
    initialSettings = {
      'maxspeed': 333,
      'system.access': 1,
      'comm.address': 1,
    };
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => inputHasValue('maxspeed', 33.3));

    fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'comm.address' }));
    await waitUntilPass(() => {
      fireEvent.input(wrapper.getByRole('spinbutton', { name: 'comm.address' }), { target: { value: `${NEW_ADDRESS}` } });
    });
    devices[0].settings.set.mockRejectedValueOnce(new InvalidDataException('address field in reply does not match'));
    fireEvent.click(wrapper.getByText('Save'));

    await waitUntilPass(() => {
      expect(loadDevicesActionMock).toHaveBeenCalled();
    });

    initialSettings['comm.address'] = NEW_ADDRESS;
    mockSingleDevice(TestDeviceSettings.testStore, device => {
      device.product!.conversionTable = CONVERSION_TABLE;
      device.product!.settings = INTEGRATED_SETTINGS;
      return device;
    }, NEW_ADDRESS);

    await waitUntilPass(() => inputHasValue('comm.address', NEW_ADDRESS));
  });

  test('displays setting in native units when they cannot be converted', async () => {
    canConvertNativeUnits = false;
    initialSettings = {
      'maxspeed': 12345,
      'system.access': 1,
    };

    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => inputHasValue('maxspeed', 12345));
  });
});

describe('single editable setting', () => {
  beforeEach(async () => {
    initialSettings = {
      'maxspeed': 123,
      'system.access': 1,
    };

    const store = TestDeviceSettings.testStore;
    mockSingleDevice(store, device => {
      device.product!.conversionTable = CONVERSION_TABLE;
      device.product!.settings = {
        rows: [
          { name: 'system.access', read_access_level: 1, visibility: 'advanced' },
          {
            name: 'maxspeed', contextual_dimension_id: 2, default_value: '9900',
            ...SETTING_ROW_BASE,
          },
        ],
      } as ProductInfo['settings'];
      return device;
    });

    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => {
      inputHasValue('maxspeed', 12.3);
    });
  });

  test('changing unit to Native will render the native value', () => {
    fireEvent.change(wrapper.getByText(/Native/).parentElement!, { target: { value: Units.NATIVE } });

    inputHasValue('maxspeed', 123);
  });

  test('changing unit will render the converted value', () => {
    fireEvent.change(wrapper.getByText(/mm\/s/).parentElement!, { target: { value: Velocity.CENTIMETRES_PER_SECOND } });

    inputHasValue('maxspeed', 1.23);
  });

  test('editing value will write the setting', async () => {
    fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'maxspeed' }));
    setValue('maxspeed', 56.7);
    fireEvent.click(wrapper.getByText('Save'));

    await waitUntilPass(() => {
      expect(devices[0].settings.set).toHaveBeenCalledTimes(1);
    });
    expect(devices[0].settings.set).toHaveBeenCalledWith('maxspeed', 56.7, Velocity.MILLIMETRES_PER_SECOND);

    expect(wrapper.queryByPlaceholderText('Setting Value')).toBeFalsy();
  });

  test('Canceling setting reverses changes made', async () => {
    fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'maxspeed' }));
    await waitUntilPass(() => wrapper.getByText('Save'));
    inputHasValue('maxspeed', 12.3);
    setValue('maxspeed', 56.7);
    fireEvent.click(wrapper.getByText('Cancel'));
    inputHasValue('maxspeed', 12.3);
    expect(wrapper.queryByText('Save')).toBeFalsy();
  });

  test('clicking refresh reads the settings again', async () => {
    devices[0].settings._settings.maxspeed = 777;

    const settingElement = getSettingElement(/maxspeed/);
    fireEvent.click(getByTitle(settingElement, 'Refresh Setting'));
    expect(getByTitle(settingElement, 'Refresh Setting')).toHaveClass('disabled');

    await waitUntilPass(() => {
      inputHasValue('maxspeed', 77.7);
    });
  });

  test('renders error when setting fails to read', async () => {
    devices[0].settings.get.mockRejectedValueOnce(new CommandFailedException('Command failed', {
      responseData: RejectReasons.BADCOMMAND, replyFlag: 'RJ',
    } as CommandFailedExceptionData));

    const settingElement = getSettingElement(/maxspeed/);
    fireEvent.click(getByTitle(settingElement, 'Refresh Setting'));

    await waitUntilPass(() => {
      wrapper.getByText(/BADCOMMAND/);
    });
  });

  test('renders error when setting fails to write and hides it on success', async () => {
    devices[0].settings.set.mockRejectedValueOnce(new CommandFailedException('Command failed', {
      responseData: RejectReasons.BADDATA, replyFlag: 'RJ',
    } as CommandFailedExceptionData));

    fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'maxspeed' }));
    await waitUntilPass(() => {
      fireEvent.click(wrapper.getByText('Save'));
    });

    await waitUntilPass(() => {
      wrapper.getByText(/BADDATA/);
    });

    fireEvent.click(wrapper.getByText('Save'));

    await waitUntilPass(() => {
      expect(wrapper.queryByText(/BADDATA/)).toBeNull();
    });

    await waitUntil(() => !selectSettingsBeingRead(TestDeviceSettings.testStore.getState()));
  });
});

describe('single editable string setting', () => {
  beforeEach(async () => {
    initialSettings = {
      'fake.str.setting': 'fakeSetting',
      'system.access': 1,
    };

    const store = TestDeviceSettings.testStore;
    mockSingleDevice(store, device => {
      device.product!.conversionTable = CONVERSION_TABLE;
      device.product!.settings = {
        rows: [
          { name: 'system.access', read_access_level: 1, visibility: 'advanced' },
          { name: 'fake.str.setting', param_type: 'IPv4Address', ...SETTING_ROW_BASE }
        ]
      } as ProductInfo['settings'];
      return device;
    });

    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => {
      const input = wrapper.getByRole('textbox', { name: 'fake.str.setting' }) as HTMLInputElement;
      expect(input.value).toBe('fakeSetting');
    });
  });

  test('editing value will write the string setting', async () => {
    fireEvent.focus(wrapper.getByRole('textbox', { name: 'fake.str.setting' }));
    fireEvent.change(wrapper.getByRole('textbox', { name: 'fake.str.setting' }), { target: { value: 'fakeSettingNew' } });
    fireEvent.click(wrapper.getByText('Save'));

    await waitUntilPass(() => {
      expect(devices[0].settings.setString).toHaveBeenCalledTimes(1);
    });
    expect(devices[0].settings.setString).toHaveBeenCalledWith('fake.str.setting', 'fakeSettingNew');
    expect(wrapper.queryByPlaceholderText('Setting Value')).toBeNull();
  });
});

describe('configure network settings', () => {
  beforeEach(async () => {
    initialSettings = {
      'comm.en.ipv4.address': '192.168.149.16',
      'comm.en.ipv4.dhcp.enabled': '1',
      'comm.en.ipv4.gateway': '192.168.254.254',
      'comm.en.ipv4.netmask': '255.255.0.0',
      'system.access': 1,
    };

    const store = TestDeviceSettings.testStore;
    mockSingleDevice(store, device => {
      device.product!.conversionTable = CONVERSION_TABLE;
      device.product!.settings = {
        rows: [
          { name: 'system.access', read_access_level: 1, visibility: 'advanced' },
          { name: 'comm.en.ipv4.address', param_type: 'IPv4Address', read_access_level: 1, visibility: 'always' },
          { name: 'comm.en.ipv4.dhcp.enabled', param_type: 'Bool', read_access_level: 1, visibility: 'always' },
          { name: 'comm.en.ipv4.gateway', param_type: 'IPv4Address', read_access_level: 1, visibility: 'always' },
          { name: 'comm.en.ipv4.netmask', param_type: 'IPv4Mask', read_access_level: 1, visibility: 'always' }
        ]
      } as ProductInfo['settings'];
      return device;
    });

    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => {
      wrapper.getByText(/255.255.0.0/);
      fireEvent.click(wrapper.getAllByTitle('Edit Setting')[0]);
      wrapper.getByText(/Network Settings/);
    });
  });

  afterEach(async () => {
    await waitUntilPass(() => {
      const state = TestDeviceSettings.testStore.getState();
      expect(selectSettingsBeingRead(state)).toBe(false);
      expect(selectConfiguringNetwork(state)).toBe(false);
    });
  });

  test('selecting automatic disables all inputs', async () => {
    fireEvent.click(wrapper.getByLabelText(/Obtain an IP address automatically/));
    expect(wrapper.getByLabelText(/IP Address/)).toBeDisabled();
    expect(wrapper.getByLabelText(/Subnet Mask/)).toBeDisabled();
    expect(wrapper.getByLabelText(/Use default gateway/)).toBeDisabled();
    expect(wrapper.getByLabelText(/Default Gateway/)).toBeDisabled();
  });

  test('selecting disable networking disables all inputs and sets IP address to 0.0.0.0', async () => {
    fireEvent.click(wrapper.getByLabelText(/Disable networking/));
    const ipInput = wrapper.getByLabelText(/IP Address/);
    expect(ipInput).toBeDisabled();
    expect(wrapper.getByLabelText(/Subnet Mask/)).toBeDisabled();
    expect(wrapper.getByLabelText(/Use default gateway/)).toBeDisabled();
    expect(wrapper.getByLabelText(/Default Gateway/)).toBeDisabled();
    expect(ipInput).toHaveValue('0.0.0.0');
  });

  test('validation is shown only after confirm is clicked for the first time', async () => {
    fireEvent.click(wrapper.getByLabelText(/Use the following IP address/));
    const ipInput = wrapper.getByLabelText(/IP Address/);
    const ipInputComponent = getParentWithClass(ipInput, 'input');
    fireEvent.change(ipInput, { target: { value: 'invalidIP' } });
    expect(ipInputComponent).not.toHaveClass(ERROR_CLASS);

    fireEvent.click(wrapper.getByText(/Confirm/));
    expect(ipInputComponent).toHaveClass(ERROR_CLASS);
  });

  test('confirm button is disabled when there are invalid fields', async () => {
    fireEvent.click(wrapper.getByLabelText(/Use the following IP address/));
    const ipInput = wrapper.getByLabelText(/IP Address/);
    const confirmButton = wrapper.getByText(/Confirm/);

    fireEvent.change(ipInput, { target: { value: 'invalidIP' } });
    fireEvent.click(confirmButton);
    expect(confirmButton).toBeDisabled();

    fireEvent.change(ipInput, { target: { value: '123.123.123.123' } });
    expect(confirmButton).not.toBeDisabled();
  });

  test('IP address, subnet mask and default gateway inputs are validated', async () => {
    fireEvent.click(wrapper.getByLabelText(/Use the following IP address/));

    const ipInput = wrapper.getByLabelText(/IP Address/);
    fireEvent.change(ipInput, { target: { value: '123.43.23.258' } });
    fireEvent.click(wrapper.getByText(/Confirm/));
    expect(getParentWithClass(ipInput, 'input')).toHaveClass(ERROR_CLASS);

    const netmask = wrapper.getByLabelText(/Subnet Mask/);
    fireEvent.change(netmask, { target: { value: '255.255.0.1' } });
    expect(getParentWithClass(netmask, 'input')).toHaveClass(ERROR_CLASS);

    const gateway = wrapper.getByLabelText(/Default Gateway/);
    fireEvent.change(gateway, { target: { value: '0.0.3.1234' } });
    expect(getParentWithClass(gateway, 'input')).toHaveClass(ERROR_CLASS);
  });

  test('clicking confirm configures the network', async () => {
    fireEvent.click(wrapper.getByLabelText(/Disable networking/));
    fireEvent.click(wrapper.getByText(/Confirm/));
    await waitUntilPass(() => {
      expect(devices[0].genericCommand).toHaveBeenCalledWith('comm en ipv4 static 0.0.0.0 255.255.0.0 0.0.0.0');
    });
  });

  test('clicking confirm with automatic configures the network', async () => {
    fireEvent.click(wrapper.getAllByTitle('Edit Setting')[0]);
    fireEvent.click(wrapper.getByLabelText(/Obtain an IP address automatically/));
    fireEvent.click(wrapper.getByText(/Confirm/));
    await waitUntilPass(() => {
      expect(devices[0].genericCommand).toHaveBeenCalledWith('comm en ipv4 dhcp');
    });
  });

  test('confirm and cancel buttons are disabled during configuration', () => {
    fireEvent.click(wrapper.getByLabelText(/Use the following IP address/));
    fireEvent.change(wrapper.getByLabelText(/IP Address/), { target: { value: '123.123.123.123' } });
    fireEvent.change(wrapper.getByLabelText(/Subnet Mask/), { target: { value: '255.255.0.0' } });
    fireEvent.change(wrapper.getByLabelText(/Default Gateway/), { target: { value: '123.123.123.123' } });
    fireEvent.click(wrapper.getByText(/Confirm/));

    expect(wrapper.getByText(/Confirm/)).toBeDisabled();
    expect(wrapper.getByText(/Cancel/)).toBeDisabled();
  });
});

describe('controller and peripheral', () => {
  beforeEach(() => {
    const store = TestDeviceSettings.testStore;
    mockSingleDeviceWithPeripherals(store, { modifier: defaultPeripheralsModifier });
  });

  test('reads all settings after selection of axis', async () => {
    const axis = _.sample(selectAxes(TestDeviceSettings.testStore.getState()))!;

    initialSettings = {
      'maxspeed': 333,
      'pos': 222,
      'system.access': 1,
      'peripheral.id': 45789,
    };

    connectionViewMockInstance.props.onSelect(axis.key);

    await waitUntilPass(() => {
      inputHasValue('maxspeed', 33.3);
      inputHasValue('pos', 22.2);
      inputHasValue('peripheral.id', 45789);
    });
    expect(axes.find(a => a.axisNumber === axis.axisNumber)!.settings.get).toHaveBeenCalledTimes(3);
  });

  test('calls loadDevices when peripheral.id is changed', async () => {
    const axis = _.sample(selectAxes(TestDeviceSettings.testStore.getState()))!;

    initialSettings = {
      'system.access': 1,
      'peripheral.id': 45789,
    };

    connectionViewMockInstance.props.onSelect(axis.key);

    await waitUntilPass(() => {
      inputHasValue('peripheral.id', 45789);
    });

    fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'peripheral.id' }));
    setValue('peripheral.id', 45678);
    fireEvent.click(wrapper.getByText('Save'));

    await waitUntilPass(() => {
      expect(loadDevicesActionMock).toHaveBeenCalled();
    });

    mockSingleDeviceWithPeripherals(TestDeviceSettings.testStore, { modifier: defaultPeripheralsModifier });
    await waitUntilPass(() => {
      inputHasValue('peripheral.id', 45678);
    });
  });

  test('reads all settings after selection of device', async () => {
    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;

    initialSettings = {
      'deviceid': 30341,
      'system.axiscount': 4,
      'system.access': 1,
    };

    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => {
      inputHasValue('deviceid', 30341);
    });
    expect(devices[0].settings.get).toHaveBeenCalledTimes(DEVICE_SETTINGS.rows.length);
  });

  test('remembers scroll position', async () => {
    const axis = _.sample(selectAxes(TestDeviceSettings.testStore.getState()))!;
    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;

    connectionViewMockInstance.props.onSelect(axis.key);
    await waitAfterLoading();
    wrapper.getByTestId('scrolling-sections').scrollTop = 1000;

    connectionViewMockInstance.props.onSelect(device.key);
    await waitAfterLoading();

    connectionViewMockInstance.props.onSelect(axis.key);
    const scroll = jest.fn();
    wrapper.getByTestId('scrolling-sections').scroll = scroll;

    await waitUntilPass(() =>
      expect(scroll).toHaveBeenCalledWith({ top: 1000 })
    );
  });
});

describe('Respect "system.access" setting', () => {
  beforeEach(() => {
    const store = TestDeviceSettings.testStore;
    mockSingleDevice(store, device => {
      device.product!.conversionTable = CONVERSION_TABLE;
      device.product!.settings = {
        rows: [
          { name: 'system.access', read_access_level: 1, write_access_level: 1, visibility: 'advanced' },
          { name: 'comm.rs232.baud', read_access_level: 1, write_access_level: 2, visibility: 'always' },
          { name: 'limit.home.tune', read_access_level: 1, write_access_level: 3, visibility: 'always' },
          { name: 'device.serial', read_access_level: 2, visibility: 'always' },
        ]
      } as ProductInfo['settings'];
      return device;
    });

    initialSettings = {
      'system.access': 1,
      'comm.rs232.baud': 115200,
      'device.serial': 2345,
      'limit.home.tune': 532,
    };
  });

  const changeAccessLevelInUI = async (newAccessLevel: number) => {
    await waitUntilPass(() => fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'system.access' })));
    setValue('system.access', newAccessLevel);
    fireEvent.click(wrapper.getByText('Save'));
  };

  const changeAccessLevelInBackgroundAndRefresh = (newAccessLevel: number) => {
    devices[0].settings.updateMockSettings({
      'system.access': newAccessLevel,
    });
    const systemAccessElement = getSettingElement(/system.access/);
    fireEvent.click(getByTitle(systemAccessElement, 'Refresh Setting'));
  };

  describe('Readability of settings', () => {
    test('"system.access" setting gets read before any other setting', async () => {
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => inputHasValue('comm.rs232.baud', 115200));

      expect(devices[0].settings.get).toBeCalledTimes(3);
      expect(devices[0].settings.get.mock.calls[0][0]).toBe('system.access');
    });

    test('Only readable settings are displayed and loaded during first load', async () => {
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);

      await waitUntilPass(() => {
        wrapper.getByText(/system.access/);
        wrapper.getByText(/comm.rs232.baud/);
      });
      expect(wrapper.queryByText('device.serial')).toBeNull();
    });

    test('No other settings get loaded if the initial "system.access" read fails', async () => {
      mockSettingGetFunc.mockRejectedValueOnce(new CommandFailedException('Command failed', {
        responseData: RejectReasons.BADCOMMAND, replyFlag: 'RJ',
      } as CommandFailedExceptionData));
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => wrapper.getAllByText(/BADCOMMAND/));

      wrapper.getByText(/system.access/);
      expect(devices[0].settings.get).toBeCalledTimes(1);
    });

    test('Settings get loaded if "system.access" setting is refreshed and it succeeds after having failed in first read', async () => {
      mockSettingGetFunc.mockRejectedValueOnce(new CommandFailedException('Command failed', {
        responseData: RejectReasons.BADCOMMAND, replyFlag: 'RJ',
      } as CommandFailedExceptionData));
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);

      await waitUntilPass(() => wrapper.getAllByText(/BADCOMMAND/));
      expect(devices[0].settings.get).toBeCalledTimes(1);

      fireEvent.click(wrapper.getByTitle('Refresh All Settings'));

      await waitUntilPass(() => inputHasValue('comm.rs232.baud', 115200));
      const readSettings = devices[0].settings.get.mock.calls.map(args => args[0]);
      expect(readSettings).toEqual([
        'system.access',
        'system.access',
        'comm.rs232.baud',
        'limit.home.tune',
      ]);
    });

    test('Readability of settings is updated when "system.access" is changed', async () => {
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => expect(wrapper.queryByText('system.access')).not.toBeNull());

      await changeAccessLevelInUI(2);
      await waitUntilPass(() => {
        wrapper.getByText('device.serial');
        inputHasValue('device.serial', 2345);
      });

      await changeAccessLevelInUI(1);
      await waitUntilPass(() => expect(wrapper.queryByText('device.serial')).toBeNull());
    });

    test('Readability of settings is updated when "system.access" is refreshed and is now different than before', async () => {
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => expect(wrapper.queryByText('system.access')).not.toBeNull());

      await changeAccessLevelInUI(2);
      await waitUntilPass(() => {
        wrapper.getByText('device.serial');
        inputHasValue('device.serial', 2345);
      });

      await changeAccessLevelInUI(1);
      await waitUntilPass(() => expect(wrapper.queryByText('device.serial')).toBeNull());
    });
  });

  describe('Writability of settings', () => {
    test('Only writable settings can be edited', async () => {
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => {
        checkInputDisabled('system.access', false);
        checkInputDisabled('comm.rs232.baud', true);
      });
    });

    test('settings with write access of 2 are locked', async () => {
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => {
        fireEvent.mouseOver(wrapper.getByLabelText('comm.rs232.baud locked'));
        wrapper.getByText('Set system.access to at least 2 to edit comm.rs232.baud.');
      });
    });

    test('Writability of settings is updated when "system.access" is changed', async () => {
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => checkInputDisabled('system.access', false));

      checkInputDisabled('comm.rs232.baud', true);

      await changeAccessLevelInUI(2);
      await waitUntilPass(() => {
        checkInputDisabled('comm.rs232.baud', false);
        checkInputDisabled('limit.home.tune', true);
      });

      await changeAccessLevelInUI(1);
      await waitUntilPass(() => {
        checkInputDisabled('comm.rs232.baud', true);
      });

      await waitUntil(() => !selectSettingsBeingRead(TestDeviceSettings.testStore.getState()));
    });

    test('Writability of settings is updated when "system.access" is refreshed and is now different than before', async () => {
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => inputHasValue('comm.rs232.baud', 115200));

      changeAccessLevelInBackgroundAndRefresh(2);
      await waitUntilPass(() => {
        checkInputDisabled('system.access', false);
        checkInputDisabled('comm.rs232.baud', false);
      });

      changeAccessLevelInBackgroundAndRefresh(1);
      await waitUntilPass(() => {
        expect(wrapper.queryByTitle('Edit comm.rs232.baud')).toBeNull();
      });
    });
  });

  describe('Peripherals without "system.access" setting of their own inherit it from parent device', () => {
    beforeEach(() => {
      const store = TestDeviceSettings.testStore;
      mockSingleDeviceWithPeripherals(store, {
        modifier: device => {
          defaultPeripheralsModifier(device);
          device.axes.forEach(axis => {
          axis.product!.settings = {
            rows: [
              { name: 'comm.rs232.baud', ...SETTING_ROW_BASE },
              { name: 'knob.enabled', read_access_level: 1, write_access_level: 2, visibility: 'always' },
              { name: 'device.serial', read_access_level: 2, visibility: 'always' },
            ]
          } as ProductInfo['settings'];
          });
          return device;
        }
      });

      initialSettings = {
        'system.access': 2,
        'system.axiscount': 4,
        'deviceid': 30341,

        'comm.rs232.baud': 115200,
        'knob.enabled': 1,
        'device.serial': 2345,
      };
    });

    const checkPeripheralSettingsForLevel = async (accessLevel: number) => {
      switch (accessLevel) {
        case 1:
          await waitUntilPass(() => {
            wrapper.getByText(/comm.rs232.baud/);
            wrapper.getByText(/knob.enabled/);
            expect(wrapper.queryByText(/device.serial/)).toBeNull();
          });
          checkInputDisabled('comm.rs232.baud', false);
          checkInputDisabled('knob.enabled', true);
          break;
        case 2:
          await waitUntilPass(() => {
            wrapper.getByText(/comm.rs232.baud/);
            wrapper.getByText(/knob.enabled/);
            wrapper.getByText(/device.serial/);
          });
          checkInputDisabled('comm.rs232.baud', false);
          checkInputDisabled('knob.enabled', false);
          checkInputDisabled('device.serial', true);
          break;
      }
    };

    test('Peripheral inherits access level from parent device if parent device settings had already been loaded', async () => {
      const axis = _.sample(selectAxes(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(axis.key);
      await checkPeripheralSettingsForLevel(2);
    });

    test('Parent device is forced to load its system.access if user loads peripheral first', async () => {
      const axis = _.sample(selectAxes(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(axis.key);
      await checkPeripheralSettingsForLevel(2);

      expect(devices[0].settings.get.mock.calls).toHaveLength(1);
      expect(devices[0].settings.get.mock.calls[0][0]).toBe('system.access');
    });

    test('Opening parent device after peripheral already forced it to load its access level, loads its other settings too', async () => {
      const axis = _.sample(selectAxes(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(axis.key);
      await checkPeripheralSettingsForLevel(2);

      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      wrapper.getByText(/deviceid/);
      expect(wrapper.queryByTestId('deviceid-value')).toBeNull();
      await waitUntilPass(() => inputHasValue('deviceid', 30341));
    });

    test('Changing access level of parent device changes readability and writability of settings in peripheral', async () => {
      const axis = _.sample(selectAxes(TestDeviceSettings.testStore.getState()))!;
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;

      connectionViewMockInstance.props.onSelect(axis.key);
      await checkPeripheralSettingsForLevel(2);

      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => inputHasValue('deviceid', 30341));
      await changeAccessLevelInUI(1);

      connectionViewMockInstance.props.onSelect(axis.key);
      await checkPeripheralSettingsForLevel(1);

      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => inputHasValue('deviceid', 30341));
      await waitUntilPass(() => checkInputDisabled('system.access', false));
      await changeAccessLevelInUI(2);

      connectionViewMockInstance.props.onSelect(axis.key);
      await checkPeripheralSettingsForLevel(2);
    });
  });
});

describe('Show documentation', () => {
  beforeEach(() => {
    const store = TestDeviceSettings.testStore;
    mockSingleDeviceWithPeripherals(store, {
      modifier: device => {
        defaultPeripheralsModifier(device);
        device.identity.firmwareVersion = { major: 7, minor: 10, build: 0 };
        device.product.capabilities.push('foo');
        return device;
      }
    });

    initialSettings = {
      'system.access': 1,
    };
  });

  describe('devices', () => {
    beforeEach(async () => {
      const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);

      await waitUntilPass(() => wrapper.getByText(/system.access/));
      fireEvent.click(wrapper.queryAllByTitle('Show documentation')[0]);
    });

    test('displays documentation', async () => {
      wrapper.getByText('The access level of the user');
    });

    test('gates information on data-capability', async () => {
      wrapper.getByText('This user can see foo');
      expect(wrapper.queryByText('This user can see bar')).toBeFalsy();
    });

    test('gates information on firmware version', async () => {
      wrapper.getByText('This user can see min 7.10');
      wrapper.getByText('This user can see min 7.09');
      wrapper.getByText('This user can see max 7.10');
      wrapper.getByText('This user can see max 7.11');
      expect(wrapper.queryByText('This user can see min 7.11')).toBeFalsy();
      expect(wrapper.queryByText('This user can see max 7.09')).toBeFalsy();
    });

    test('formats external URLs according to the current device', async () => {
      const linkElement = wrapper.getByText('another setting').parentElement!;
      expect(linkElement).toHaveAttribute(
        'href',
        'https://www.zaber.com/protocol-manual?' +
        'device=X-MCC4&version=7.10&protocol=ASCII&peripheral=LHM025A-T3#topic_setting_another_setting'
      );
    });
  });

  describe('peripherals', () => {
    beforeEach(async () => {
      const axis = _.sample(selectAxes(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(axis.key);

      await waitUntilPass(() => wrapper.getByText('pos'));
      fireEvent.click(wrapper.queryAllByTitle('Show documentation')[1]);
    });

    test('formats external URLs according to the current peripheral', async () => {
      expect(wrapper.getByText('another setting')).toHaveAttribute(
        'href',
        'https://www.zaber.com/protocol-manual?' +
        'device=X-MCC4&version=7.10&protocol=ASCII&peripheral=LHM025A-T3#topic_setting_another_setting',
      );
    });
  });

  describe('categories', () => {
    beforeEach(async () => {
      const axis = _.sample(selectAxes(TestDeviceSettings.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(axis.key);

      await waitUntilPass(() => wrapper.getByText('peripheral.id'));
    });

    test('shows/hides category documentation stripped of settings inside', async () => {
      const categoryTitle = wrapper.getAllByText('Peripheral')[1].parentElement!.parentElement!;

      fireEvent.click(getByTitle(categoryTitle, 'Show documentation'));
      wrapper.getByText('about peripheral in general');
      expect(wrapper.queryByText('about peripheral.id')).toBeNull();

      fireEvent.click(getByTitle(categoryTitle, 'Hide documentation'));
      expect(wrapper.queryByText('about peripheral in general')).toBeNull();
    });
  });
});

describe('Search bar can narrow the results', () => {
  function getHighlighted(partial: RegExp | string, fullText: string) {
    expect(wrapper.getByText(partial).parentElement!.textContent).toBe(fullText);
  }

  beforeEach(async () => {
    const store = TestDeviceSettings.testStore;
    mockSingleDevice(store, device => {
      device.product!.conversionTable = CONVERSION_TABLE;
      device.product!.settings = INTEGRATED_SETTINGS;
      return device;
    });

    initialSettings = {
      'system.access': 1,
    };

    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getByText('maxspeed'));

    wrapper.getByText('maxspeed');
    wrapper.getByText('Maximum Speed');
    wrapper.getByText('system.access');
    wrapper.getByText('Access Level');
    wrapper.getByText('system.axiscount');
    wrapper.getByText('Number of Axes');
  });

  test('filters settings by expression', async () => {
    const searcher = wrapper.getByPlaceholderText('Search...');
    fireEvent.input(searcher, { target: { value: 'syst' } });

    getHighlighted('em.access', 'system.access');
    wrapper.getByText('Access Level');
    getHighlighted('em.axiscount', 'system.axiscount');
    wrapper.getByText('Number of Axes');
    expect(wrapper.queryByText('Maximum Speed')).toBeNull();
  });

  test('filters settings by human name', async () => {
    const searcher = wrapper.getByPlaceholderText('Search...');
    fireEvent.input(searcher, { target: { value: 'of axes' } });

    wrapper.getByText('system.axiscount');
    getHighlighted('of Axes', 'Number of Axes');
    expect(wrapper.queryByText('system.access')).toBeNull();
    expect(wrapper.queryByText('maxspeed')).toBeNull();
  });

  test('scrolls to the top when searched', () => {
    wrapper.getByTestId('scrolling-sections').scroll = jest.fn();

    const searcher = wrapper.getByPlaceholderText('Search...');
    fireEvent.input(searcher, { target: { value: 'syst' } });

    expect(wrapper.getByTestId('scrolling-sections').scroll).toHaveBeenCalledWith({ top: 0 });
  });
});

describe('categories', () => {
  beforeEach(async () => {
    initialSettings = {
      'system.access': 1,
      'maxspeed': 123,
      'comm.address': 1,
      'limit.max': 1000,
    };

    const store = TestDeviceSettings.testStore;
    mockSingleDevice(store, device => {
      device.product!.conversionTable = CONVERSION_TABLE;
      device.product!.settings = {
        rows: [
          { name: 'system.access', read_access_level: 1, visibility: 'advanced' },
          { name: 'maxspeed', contextual_dimension_id: 2, ...SETTING_ROW_BASE },
          { name: 'comm.address',  ...SETTING_ROW_BASE, decimal_places: 0 },
          { name: 'limit.max', ...SETTING_ROW_BASE, contextual_dimension_id: 2, write_access_level: 2 },
        ]
      } as ProductInfo['settings'];
      return device;
    });

    const device = _.sample(selectDevices(store.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => inputHasValue('maxspeed', 12.3));
    expect(intersectionObserverMock.elements).toHaveLength(4);
  });

  function getCategoryInMenu(text: Matcher) {
    return wrapper.getAllByText(text)[0].parentElement!.parentElement!;
  }

  test('disconnects observer when unmounted', () => {
    wrapper.unmount();
    wrapper = null!;
    expect(intersectionObserverMock.disconnect).toHaveBeenCalled();
  });

  test('registers categories to observer', () => {
    expect(intersectionObserverMock.elements.map(e => e.querySelector('.title')!.textContent)).toStrictEqual([
      'Communication',
      'Limits',
      'Motion',
      'System',
    ]);
  });

  test('removes categories from observer', async () => {
    connectionViewMockInstance.props.onSelect('');
    await waitUntilPass(() =>
      expect(intersectionObserverMock.elements).toHaveLength(0)
    );
  });

  test('keeps track of the active category', async () => {
    intersectionObserverMock.callCallback([{
      target: intersectionObserverMock.elements[0], // Communication
      intersectionRatio: 0.2,
    }, {
      target: intersectionObserverMock.elements[1], // Limits
      intersectionRatio: 0.4,
    }]);

    expect(getCategoryInMenu('Limits')).toHaveClass('active');

    intersectionObserverMock.callCallback([{
      target: intersectionObserverMock.elements[1], // Limits
      intersectionRatio: 0.2,
    }, {
      target: intersectionObserverMock.elements[2], // Motion
      intersectionRatio: 1,
    }, {
      target: intersectionObserverMock.elements[3], // System
      intersectionRatio: 0.1,
    }]);

    expect(getCategoryInMenu('Motion')).toHaveClass('active');
  });

  test('scrolls to category after clicking the menu item', async () => {
    const category = wrapper.getAllByText('Limits')[1].parentElement!.parentElement!.parentElement!.parentElement!;
    category.scrollIntoView = jest.fn();

    fireEvent.click(wrapper.getAllByText('Limits')[0]);

    expect(category.scrollIntoView).toHaveBeenCalled();
  });
});

describe('dependent settings', () => {
  beforeEach(() => {
    const store = TestDeviceSettings.testStore;
    mockSingleDevice(store, device => {
      device.product!.conversionTable = CONVERSION_TABLE;
      device.product!.settings = {
        rows: [
          { name: 'system.access', read_access_level: 1, write_access_level: 1, visibility: 'advanced', decimal_places: 0 },
          { name: 'accel', contextual_dimension_id: 3, ...SETTING_ROW_BASE },
          { name: 'motion.accelonly', contextual_dimension_id: 3, ...SETTING_ROW_BASE },
          { name: 'motion.decelonly', contextual_dimension_id: 3, ...SETTING_ROW_BASE },
        ],
      } as ProductInfo['settings'];
      return device;
    });
  });

  test('reads dependent settings after writing', async () => {
    const device = _.sample(selectDevices(TestDeviceSettings.testStore.getState()))!;

    initialSettings = {
      'system.access': 1,
      'accel': 123,
      'motion.accelonly': 123,
      'motion.decelonly': 123,
    };

    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => {
      inputHasValue('accel', 12.3);
      inputHasValue('motion.accelonly', 12.3);
      inputHasValue('motion.decelonly', 12.3);
    });

    devices[0].settings.get.mockClear();
    devices[0].settings.set.mockImplementationOnce(async (setting, value) => {
      devices[0].settings._settings.accel = value;
      devices[0].settings._settings['motion.accelonly'] = value;
      devices[0].settings._settings['motion.decelonly'] = value;
    });

    fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'accel' }));
    setValue('accel', 456);
    fireEvent.click(wrapper.getByText('Save'));

    await waitUntilPass(() => {
      inputHasValue('accel', 45.6);
      inputHasValue('motion.accelonly', 45.6);
      inputHasValue('motion.decelonly', 45.6);
    });

    expect(devices[0].settings.get).toHaveBeenCalledWith('accel', Acceleration['mm/s²']);
    expect(devices[0].settings.get).toHaveBeenCalledWith('motion.accelonly', Acceleration['mm/s²']);
    expect(devices[0].settings.get).toHaveBeenCalledWith('motion.decelonly', Acceleration['mm/s²']);
  });
});

describe('global read errors', () => {
  beforeEach(() => {
    const store = TestDeviceSettings.testStore;
    mockSingleDeviceWithPeripherals(store, { modifier: defaultPeripheralsModifier });
  });

  test('shows global error message when timeout error', async () => {
    mockSettingGetFunc.mockRejectedValue(new RequestTimeoutException('Device has not responded in given timeout'));

    const store = TestDeviceSettings.testStore;
    const device = _.sample(selectDevices(store.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => {
      wrapper.getByText('Device has not responded in given timeout');
      expect(wrapper.queryByTitle('Read Error')).toBeNull();
    });
  });
});
