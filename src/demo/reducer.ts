import { changeDictionary } from '@zaber/toolbox';

import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes, DevicesLoadedPayload } from '../connection_manager';
import { EntityKey, extractConnectionKey, removeWithKey } from '../keys';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';

export interface State {
  mounted: boolean;
  demoCode: string;
  axes: AxesLiveStates;
}

export type AxesLiveStates = Record<string, AxisState>;

export interface AxisState {
  key: EntityKey;
  running: boolean;
  error: string | null;
}

export const defaultAxisState: Omit<AxisState, 'key'> = {
  running: false,
  error: null,
};

const initialState: State = {
  mounted: false,
  demoCode: '',
  axes: {},
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const mount: Reducer<ActionTypes.MOUNT> = state => ({
  ...state,
  mounted: true,
});

const unmount: Reducer<ActionTypes.UNMOUNT> = state => ({
  ...state,
  mounted: false,
});

const startAxis: Reducer<ActionTypes.START_AXIS> = (state, { key }) => ({
  ...state,
  axes: changeDictionary(state.axes, key, {
    running: true,
    error: null,
  }, () => ({
    key,
    ...defaultAxisState,
  })),
});

const axisStopped: Reducer<ActionTypes.AXIS_STOPPED> = (state, { key, error }) => ({
  ...state,
  axes: changeDictionary(state.axes, key, ({
    running: false,
    error: error ?? null,
  })),
});

const updateSettings: Reducer<ActionTypes.UPDATE_SETTINGS> = (state, { demoCode }) => ({
  ...state,
  demoCode: demoCode ?? state.demoCode,
});

const devicesLoaded = (state: State, { connectionKey }: DevicesLoadedPayload): State => ({
  ...state,
  axes: removeWithKey(state.axes, extractConnectionKey, connectionKey),
});

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.MOUNT]: mount,
  [ActionTypes.UNMOUNT]: unmount,
  [ActionTypes.START_AXIS]: startAxis,
  [ActionTypes.AXIS_STOPPED]: axisStopped,
  [ActionTypes.UPDATE_SETTINGS]: updateSettings,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
}, initialState);
