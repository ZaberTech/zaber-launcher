import React from 'react';

import { SettingField } from '../fields/SettingField';

import type { ContentProps, HwModSectionTemplate } from './HwModSection';

const LimitAwaySection: React.FC<ContentProps<'limitAway'>> = ({ values }) => {
  const sectionId = 'limitAway';

  return <>
    <SettingField {...{ values, sectionId, fieldId: 'type' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'source' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'edge' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'width' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'action' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'posUpdate' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'preset' }}/>
  </>;
};

export const limitAwayTemplate: HwModSectionTemplate<'limitAway'> = {
  sectionId: 'limitAway',
  titleName: 'Away Limit Sensor',
  description: 'Information about the away limit sensor, which is at the maximum travel position.',
  generateContent: values => (<LimitAwaySection values={values}/>),
};
