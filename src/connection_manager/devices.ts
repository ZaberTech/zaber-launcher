import type { ascii } from '@zaber/motion';
import type { SagaIterator, Task } from 'redux-saga';
import { call, join, put, select, take } from 'redux-saga/effects';
import _ from 'lodash';
import { tryAccess } from '@zaber/toolbox';
import { injectable } from 'inversify';

import {
  EntityKey, getRouterUrl, getConnectionId, getDeviceAddress, getAxisNumber, extractConnectionKey, extractDeviceKey,
  getEntityType, EntityKeyType,
} from '../keys';
import { getContainer } from '../container';
import { MessageRoutersService } from '../message_router/service';
import { RT, safeSpawn } from '../utils';

import {
  selectDevices, selectAxes, IdentifiedAxisState, isAxisStateIdentified, IdentifiedDeviceState, selectConnections,
} from './selectors';
import {
  actions, ActionTypes, allConnectionManagerActions, DevicesLoadedAction, DevicesLoadedErrAction, DevicesLoadedPayload,
} from './actions';

interface GetAxisOrDeviceOptions {
  allowNonIdentified?: boolean;
}

@injectable()
export class DevicesContext {
  ongoingIdentifications = new Map<EntityKey, Task>();
  invalidatedDeviceKeys = new Set<EntityKey>();
}

function getContext(): DevicesContext {
  return getContainer().get(DevicesContext);
}

/**
 * Gets ZML connection for entity key.
 */
export function* getConnection(key: EntityKey): SagaIterator<ascii.Connection> {
  const service = getContainer().get(MessageRoutersService);
  const connection: ascii.Connection = yield call([service, service.getAsciiConnection],
    getRouterUrl(key), getConnectionId(key));
  return connection;
}

export function reidentifyDevices(deviceKeys: EntityKey[]): void {
  const { invalidatedDeviceKeys } = getContext();
  for (const key of deviceKeys) {
    if (getEntityType(key) !== EntityKeyType.DEVICE) {
      throw new Error(`Key ${key} is not a device key`);
    }
    invalidatedDeviceKeys.add(key);
  }
}

function* compareStateToZmlInternals(deviceKey: EntityKey, device: ascii.Device): SagaIterator<boolean> {
  /* Because the identification is async it's important to select a fresh state here.
  The fresh state ensures that possible race conditions ends with exception for the caller. */
  const deviceState = tryAccess((yield select(selectDevices)) as ReturnType<typeof selectDevices>, deviceKey);
  if (deviceState?.identity == null) {
    return true;
  }

  const allAxes: ReturnType<typeof selectAxes> = yield select(selectAxes);
  const axesStates = deviceState.axes.map(axisKey => allAxes[axisKey]);

  const { identity: deviceIdentity } = device;
  const axes = _.range(deviceIdentity.axisCount).map(axisIndex => device.getAxis(axisIndex + 1));
  const axisIdentities = axes.map(axis => axis.identity);

  const identitiesEqual = _.isEqual(deviceIdentity, deviceState.identity)
    && _.isEqual(axisIdentities, axesStates.map(axisState => axisState.identity));
  if (!identitiesEqual) {
    return false;
  }

  const labelsEqual = (deviceState.label ?? '') === device.label
    && axesStates.every((axisState, i) => (axisState.label ?? '') === axes[i].label);
  if (!labelsEqual) {
    return false;
  }

  return true;
}

function* identifiedAndVerify(deviceKey: EntityKey, device: ascii.Device): SagaIterator {
  try {
    yield call([device, device.identify]);

    const isExpectedDevice = yield call(compareStateToZmlInternals, deviceKey, device);
    if (!isExpectedDevice) {
      yield put(actions.loadDevices(extractConnectionKey(deviceKey), false));
      throw new Error('Connected device does not match expected device. Try again.');
    }
  } finally {
    getContext().ongoingIdentifications.delete(deviceKey);
  }
}

function* skipIdentification(deviceKey: EntityKey, options?: GetAxisOrDeviceOptions): SagaIterator<boolean> {
  const deviceState = tryAccess((yield select(selectDevices)) as ReturnType<typeof selectDevices>, deviceKey);
  const isNonIdentified = deviceState?.nonIdentifiedInfo != null;
  return isNonIdentified && options?.allowNonIdentified === true;
}

export function* waitForDeviceIdentification(connectionKey: EntityKey) {
  for (;;) {
    const connectionState = tryAccess((yield select(selectConnections)) as ReturnType<typeof selectConnections>, connectionKey);
    if (!connectionState?.identifyingDevices) { return }
    yield take(allConnectionManagerActions);
  }
}

/**
 * Gets ZML device for entity key.
 */
export function* getDevice(key: EntityKey, options?: GetAxisOrDeviceOptions): SagaIterator<ascii.Device> {
  const deviceKey = extractDeviceKey(key);
  const connectionKey = extractConnectionKey(key);
  const connection: ascii.Connection = yield call(getConnection, key);
  const device = connection.getDevice(getDeviceAddress(key));

  yield call(waitForDeviceIdentification, connectionKey);

  if (yield call(skipIdentification, deviceKey, options)) {
    return device;
  }

  const { invalidatedDeviceKeys, ongoingIdentifications } = getContext();

  let identificationTask = ongoingIdentifications.get(deviceKey);
  if (identificationTask) {
    yield join(identificationTask);
  } else {
    const identificationNeeded = !device.isIdentified || invalidatedDeviceKeys.has(deviceKey);
    if (identificationNeeded) {
      identificationTask = (yield safeSpawn(identifiedAndVerify, deviceKey, device)) as Task;
      ongoingIdentifications.set(deviceKey, identificationTask);

      yield join(identificationTask);

      invalidatedDeviceKeys.delete(deviceKey);
    }
  }

  return device;
}

/**
 * Gets ZML axis for axis entity key.
 */
export function* getAxis(key: EntityKey, options?: GetAxisOrDeviceOptions): SagaIterator<ascii.Axis> {
  const device: ascii.Device = yield call(getDevice, key, options);
  return device.getAxis(getAxisNumber(key));
}

/**
 * Gets ZML axis for axis entity key or ZML device for device entity key.
 */
export function* getDeviceOrAxis(key: EntityKey, options?: GetAxisOrDeviceOptions): SagaIterator<ascii.Device | ascii.Axis> {
  const keyType = getEntityType(key);
  if (keyType === EntityKeyType.AXIS) {
    const axis: RT<typeof getAxis> = yield call(getAxis, key, options);
    return axis;
  } else {
    const device: RT<typeof getDevice> = yield call(getDevice, key, options);
    return device;
  }
}

/**
 * Gets ZML axis for axis entity key or fist ZML axis for device entity key.
 * Throws an error if device key is provided for device with multiple axes.
 */
export function* getAxisOrOnlyAxis(key: EntityKey, options?: GetAxisOrDeviceOptions): SagaIterator<ascii.Axis> {
  const keyType = getEntityType(key);
  if (keyType === EntityKeyType.AXIS) {
    const axis: RT<typeof getAxis> = yield call(getAxis, key, options);
    return axis;
  } else {
    const deviceState = tryAccess((yield select(selectDevices)) as ReturnType<typeof selectDevices>, key);
    if ((deviceState?.axisCount ?? 1) !== 1) {
      throw new Error(`Device key provided for multi-axes device (${deviceState?.axisCount})`);
    }
    const device: ascii.Device = yield call(getDevice, key, options);
    return device.getAxis(1);
  }
}

export function* peripheralLikeAxesOfDevice(device: IdentifiedDeviceState): SagaIterator<IdentifiedAxisState[]> {
  const allAxes: ReturnType<typeof selectAxes> = yield select(selectAxes);
  return device.axes
    .map(axisKey => allAxes[axisKey])
    .filter(isAxisStateIdentified)
    .filter(axis => axis.isPeripheralLike);
}

export function takeDevicesLoaded(connectionKey: EntityKey) {
  return take(
    (a: Partial<DevicesLoadedAction | DevicesLoadedErrAction>) =>
      [ActionTypes.DEVICES_LOADED, ActionTypes.DEVICES_LOADED_ERR].includes(a.type as ActionTypes) &&
      a.payload?.connectionKey === connectionKey);
}

export function* reloadDevices(connectionKey: EntityKey): SagaIterator<DevicesLoadedPayload['devices'] | null> {
  if (getEntityType(connectionKey) !== EntityKeyType.CONNECTION) {
    throw new Error(`Key ${connectionKey} is not a connection key`);
  }
  yield put(actions.loadDevices(connectionKey, false));

  const action: DevicesLoadedAction | DevicesLoadedErrAction = yield takeDevicesLoaded(connectionKey);
  if (action.type === ActionTypes.DEVICES_LOADED) {
    return (action as DevicesLoadedAction).payload.devices;
  } else {
    return null;
  }
}
