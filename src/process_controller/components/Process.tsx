import React, { createContext, memo, useContext, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { Toggle, Text, reactSelect, Icons, NoticeBanner, Flex, ContextMenu, SimpleSelect } from '@zaber/react-library';
import { match, P } from 'ts-pattern';
import {
  ProcessControllerMode as Mode,
  ProcessControllerSource as Source,
  ProcessControllerSourceSensor as SourceSensor
} from '@zaber/motion/product';
import Select from 'react-select';
import _ from 'lodash';
import { Units } from '@zaber/motion';

import { EditableInputWithUnits, InputWithUnits } from '../../units';
import { NoContentMessage } from '../../components';
import { EntityKey, makeAxisKey } from '../../keys';
import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import { DRIVER_DISABLED_FLAGS, ProtocolManual, ProtocolManualFormatter, ProtocolManualToggle } from '../../protocol_manual';
import {
  selectControllerInfo,
  selectProcess,
  selectProcessOn,
  selectProcessFaults,
  selectProcessState,
  selectReadings,
  selectProcessLabels
} from '../selectors';
import type { State } from '../reducer';
import {
  hasAllModeSettings, ModeSettingNames, ModeSettings, separatedSettings, setpointSetting, SettingMeta, settingMeta, SettingsMap
} from '../settings';
import type { ProcessInfo } from '../types';
import { AutoTuneButton } from '../auto_tune/AutoTuneModal';
import { InputWithFixedUnit } from '../../units/InputWithFixedUnit';
import { POLL_DELAY_MS, POLL_RETENTION_SEC } from '../constants';
import { useWarningFlagManual } from '../../protocol_manual/hooks';
import { SetLabelContextMenuItem } from '../../connection_manager_ui';

import { Graph } from './Graph';
import { Documentation } from './Documentation';

type ProcessContextType = {
  entity: EntityKey;
  process: number;
  shownSetting: string | null;
  setShownSetting: React.Dispatch<React.SetStateAction<string | null>>;
};

const ProcessContext = createContext<ProcessContextType>({ entity: '', process: 0, shownSetting: null, setShownSetting: () => null });

type GeneralSectionInputProps = {
  name: string;
  setting: string;
  contents: (label: string) => React.ReactNode;
};

const GeneralSectionInput: React.FC<GeneralSectionInputProps> = ({ name, setting, contents }) => {
  const { entity, shownSetting, setShownSetting } = useContext(ProcessContext);
  const labelName = _.uniqueId(_.kebabCase(name));
  const showDocumentation = setting === shownSetting;
  return <>
    <div className="general-input">
      <label htmlFor={labelName}><Text t={Text.Type.H5}>{name}</Text></label>
      <div className="line">
        {contents(labelName)}
        <ProtocolManualToggle
          setting={setting}
          deviceOrAxisKey={entity}
          activated={showDocumentation}
          onClick={() => setShownSetting(showDocumentation ? null : setting)}
        />
      </div>
    </div>
    {showDocumentation && <Documentation entity={entity} setting={setting}/>}
  </>
  ;
};

const ProcessInput: React.FC<{ source: Source }> = ({ source }) => {
  const info = useSelector(selectControllerInfo);
  const input = match<[Source, State['info']]>([source, info])
    .with([{ sensor: SourceSensor.ANALOG_INPUT }, { analogIns: P.any }], ([{ port }, { analogIns }]) => analogIns[port])
    .with([{ sensor: SourceSensor.THERMISTOR }, { temperatures: P.any }], ([{ port }, { temperatures }]) => temperatures[port])
    .otherwise(() => null);
  const setting = source.sensor === SourceSensor.ANALOG_INPUT ? 'analog in' : `sensor.temperature.${source.port}`;
  return <GeneralSectionInput name="Measured" setting={setting} contents={label => (
    <InputWithUnits
      measure={input}
      unitFrom={{ dimension: source.sensor === SourceSensor.THERMISTOR ? 'Absolute Temperature' : 'Voltage' }}
      onChange={() => null}
      disabled="readonly"
      unitsEditable={false}
      id={label}
    />
  )}/>;
};

const ProcessVoltsOutput: React.FC<{ process: number }> = ({ process }) => {
  const info = useSelector(selectControllerInfo);
  const value = match(info)
    .with({ voltsOut: P.any }, ({ voltsOut }) => voltsOut[process])
    .otherwise(() => null);
  return <GeneralSectionInput name="Output Voltage" setting="process.voltage" contents={label => (
    <InputWithFixedUnit
      value={value}
      unit="V"
      disabled="readonly"
      id={label}
    />
  )}/>;
};

const ProcessAmpsOutput: React.FC<{ process: number }> = ({ process }) => {
  const info = useSelector(selectControllerInfo);
  const value = match(info)
    .with({ currentOut: P.any }, ({ currentOut }) => currentOut[process])
    .otherwise(() => null);
  return <GeneralSectionInput name="Output Current" setting="process.current" contents={label => (
    <InputWithFixedUnit
      value={value}
      unit="A"
      disabled="readonly"
      id={label}
    />
  )}/>;
};

const Setpoint: React.FC<{ process: number; info: ProcessInfo }> = ({ process, info }) => {
  const { entity } = useContext(ProcessContext);
  const actions = useActions(actionDefinitions);
  const setpoint = setpointSetting(info.source);
  return  <GeneralSectionInput name="Setpoint" setting={setpoint} contents={label => (
    <EditableInputWithUnits
      measure={info.settings[setpoint] ?? null}
      onChange={update => actions.updateProcessSetting(process, setpoint, update)}
      mode={info.settings[setpoint]?.mode ?? 'displaying'}
      unitFrom={{ setting: setpoint, entity: makeAxisKey(entity, process) }}
      unitsEditable={false}
      id={label}
    />
  )}/>;
};

type SettingRowProps = {
  meta: SettingMeta;
  settings: ModeSettings<Mode>;
  setting: ModeSettingNames<Mode>;
  info: ProcessInfo;
};

const SettingRow: React.FC<SettingRowProps> = ({ meta, settings, setting, info }) => {
  const { entity, process } = useContext(ProcessContext);
  const [showProtocolManual, setShowProtocolManual] = useState(false);
  const actions = useActions(actionDefinitions);
  const processOn = useSelector(selectProcessOn);
  return <div className="setting">
    <Flex.Row>
      <label htmlFor={setting}><Text>{
        match(meta.name)
          .with(P.not(P.string), namer => namer(info))
          .otherwise(name => name)
      }</Text></label>
      <Flex.Spacer/>
      {match(meta)
        .with({ toggle: P.not(P.nullish) }, ({ toggle: { off, on } }) => <div className="toggle-group">
          <Text>{off}</Text>
          <Toggle
            value={settings[setting].value !== 0}
            loading={settings[setting].mode === 'reading' || settings[setting].mode === 'writing'}
            onValueChange={on => {
              actions.updateProcessSetting(process, setting, {
                value: on ? 1 : 0,
                units: Units.NATIVE,
                mode: 'writing',
                preEditMeasure: settings[setting],
              });
            }}
            id={setting}
            disabled={meta.disableIfProcessOn && processOn}
          />
          <Text>{on}</Text>
        </div>)
        .otherwise(() => <EditableInputWithUnits
          measure={settings[setting]}
          onChange={update => actions.updateProcessSetting(process, setting, update)}
          mode={settings[setting].mode}
          unitFrom={{ setting, entity: makeAxisKey(entity, process) }}
          id={setting}
          disabled={meta.disableIfProcessOn && processOn}
        />)
      }
      <Icons.Refresh
        className="refresh-setting"
        onClick={() => actions.updateProcessSetting(process, setting, { ...settings[setting], mode: 'reading' })}
      />
      <ProtocolManualToggle
        deviceOrAxisKey={makeAxisKey(entity, process)} setting={setting}
        activated={showProtocolManual} onClick={() => setShowProtocolManual(!showProtocolManual)}
      />
    </Flex.Row>
    {showProtocolManual && <ProtocolManual deviceOrAxisKey={makeAxisKey(entity, process)} setting={setting}/>}
  </div>;
};

type SettingsSectionProps = {
  names: ModeSettingNames<Mode>[];
  settings: ModeSettings<Mode>;
  info: ProcessInfo;
};

const SettingsSection: React.FC<SettingsSectionProps> = ({ names, settings, info }) => <div>
  {names.map(setting => match(settingMeta[setting])
    .when(({ hide }) => hide?.(info), () => null)
    .otherwise(meta => <SettingRow key={setting} meta={meta} settings={settings} setting={setting} info={info}/>)
  )}
</div>;

const SettingsSections: React.FC<{ mode: Mode; settings: SettingsMap; info: ProcessInfo}> = ({ mode, settings, info }) => {
  if (!hasAllModeSettings(mode, settings)) {
    return <><hr/><NoContentMessage message="Loading Settings..." type="working"/></>;
  }
  const [generalSettings, pidSettings] = separatedSettings(mode);
  return <>
    <hr/>
    <div className="section settings">
      <Text t={Text.Type.H4}>Control Parameters</Text>
      <SettingsSection names={generalSettings} settings={settings} info={info}/>
      {pidSettings.length > 0 && <>
        <Text t={Text.Type.H4}>PID Parameters</Text>
        <SettingsSection names={pidSettings} settings={settings} info={info}/>
      </>}
    </div>
  </>;
};

const ProcessGraph: React.FC<{ process: number; info: ProcessInfo }> = ({ process, info }) => {
  const readings = useSelector(selectReadings)[process];

  if (typeof readings === 'string') {
    return <NoContentMessage bannerTitle="No Graph Available" message={readings} type="error"/>;
  }

  return <Graph
    readings={readings}
    mode={info.mode}
    source={info.source}
    setpoint={info.mode !== Mode.MANUAL}
    pollDelay={POLL_DELAY_MS}
    duration={POLL_RETENTION_SEC}
  />;
};

const ProcessErrorNotice: React.FC<{ entity: EntityKey; process: number; flag: string }> = ({ entity, process, flag }) => {
  const manual = useWarningFlagManual(entity, flag);
  const actions = useActions(actionDefinitions);
  const closer = DRIVER_DISABLED_FLAGS.includes(flag) ? { action: () => actions.reenableDriver(process), button: 'Re-enable' } : undefined;

  return <NoticeBanner key={flag} type="error" headline={manual?.name ?? flag} closer={closer} collapsible>
    {match(manual)
      .with({ html: P._ }, ({ html }) => <ProtocolManualFormatter html={html}/>)
      .with({ error: P._ }, ({ error }) => <Text>{error}</Text>)
      .otherwise(() => <NoContentMessage message="Loading details..." type="working"/>)}
  </NoticeBanner>;
};

type ProcessParams = {
  entity: EntityKey;
  process: number;
};

const controlOptions: SimpleSelect.Option<Mode>[] = [
  { value: Mode.MANUAL, label: 'Manual' },
  { value: Mode.ON_OFF, label: 'Two-State (On/Off)' },
  { value: Mode.PID, label: 'PID Standard' },
  { value: Mode.PID_HEATER, label: 'PID Heater' },
];

const ProcessComponent: React.FC<ProcessParams> = ({ entity, process }) => {
  const info = useSelector(selectProcess);
  const state = useSelector(selectProcessState);
  const processOn = useSelector(selectProcessOn);
  const faults = useSelector(selectProcessFaults);
  const actions = useActions(actionDefinitions);
  const label = useSelector(selectProcessLabels)[process];
  const [shownSetting, setShownSetting] = useState<string | null>(null);
  const selectStyles = useMemo(() => reactSelect.getDefaultStyles<Source>(), []);

  if (info == null) {
    return <NoContentMessage message="Loading process..." type="working"/>;
  }

  return <div className="process-controls">
    <div className="section errors">
      {match(faults)
        .with(P.not(P.nullish), flags => [...flags].map(flag => <ProcessErrorNotice key={flag} {...{ entity, process, flag }}/>))
        .otherwise(() => null)
      }
      {match(info.error)
        .with(({ title: P.string }), ({ title, message }) => (
          <NoticeBanner type="error" headline={title} closer={() => actions.clearProcessError(process)} collapsible>
            {message}
          </NoticeBanner>
        ))
        .otherwise(() => null)
      }
    </div>
    <div className="section top">
      <Text e={state.on ? Text.Emphasis.Regular : Text.Emphasis.Bold}>Off</Text>
      <Toggle value={state.on} loading={state.reading} onValueChange={on => actions.processPower(process, on)}/>
      <Text e={state.on ? Text.Emphasis.Bold : Text.Emphasis.Regular}>On</Text>
      <div className="spacer"/>
      <ContextMenu>
        <SetLabelContextMenuItem entityKey={makeAxisKey(entity, process)} label={label[0]}/>
      </ContextMenu>
    </div>
    <hr/>
    <ProcessContext.Provider value={{ entity, process, shownSetting, setShownSetting }}>
      <div className="section general">
        <GeneralSectionInput name="Control Type" setting="process.control.mode" contents={label => (
          <SimpleSelect
            options={controlOptions}
            value={info.mode}
            onValueChange={mode => actions.updateMode(process, mode)}
            id={label}
            disabled={processOn}
          />
        )}/>
        <GeneralSectionInput name="Input Source" setting="process.control.source" contents={label => (
          <Select<Source>
            options={[
              { sensor: SourceSensor.ANALOG_INPUT, port: 1 },
              { sensor: SourceSensor.ANALOG_INPUT, port: 2 },
              { sensor: SourceSensor.THERMISTOR, port: 1 },
              { sensor: SourceSensor.THERMISTOR, port: 2 },
            ]}
            styles={selectStyles}
            className="source-select"
            value={info.source}
            formatOptionLabel={s => `${s.sensor === SourceSensor.ANALOG_INPUT ? 'Analog Input' : 'Thermistor'} ${s.port}`}
            isOptionSelected={s => s.sensor === info.source.sensor && s.port === info.source.port}
            onChange={source => source && actions.updateSource(process, source)}
            id={label}
          />
        )}/>

        <ProcessVoltsOutput process={process}/>
        <ProcessInput source={info.source}/>

        <ProcessAmpsOutput process={process}/>
        {info.mode !== Mode.MANUAL && <Setpoint process={process} info={info}/>}
      </div>
      <hr/>
      <div className="section graph">
        <Text t={Text.Type.H4}>Oscilloscope Graph</Text>
        <ProcessGraph process={process} info={info}/>
      </div>
      {info.mode === Mode.PID_HEATER && info.source.sensor === SourceSensor.THERMISTOR && (
        <AutoTuneButton entity={entity} process={process}/>
      )}
      <SettingsSections mode={info.mode} settings={info.settings} info={info}/>
    </ProcessContext.Provider>
  </div>;
};

export const Process = memo(ProcessComponent);
