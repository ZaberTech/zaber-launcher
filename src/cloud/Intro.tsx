import React from 'react';
import { Button, ButtonRow, NoticeBanner, Text, UnorderedList } from '@zaber/react-library';
import { NavLink } from 'react-router-dom';

import { openExternalLink } from '../desktop';
import { environment } from '../environment';

import { Header } from './Header';

export const Intro: React.FC = () => (<>
  <Header/>

  <div className="intro">
    <div className="heading">
      <Text t={Text.Type.H2}>Streamline your work with Zaber&nbsp;Cloud</Text>
    </div>

    <NoticeBanner className="closed-testing" type="warning">
      Zaber Cloud is currently in early access.
    </NoticeBanner>

    <div className="description">
      <h4>Zaber Cloud is a set of services that allows you to:</h4>
      <UnorderedList header="">
        <Text>Manage private virtual devices</Text>
        <Text>Set up secured remote access to physical devices</Text>
      </UnorderedList>
    </div>

    <ButtonRow justify="center">
      <NavLink to="/cloud/login"><Button>Sign In</Button></NavLink>
      <Button onClick={() => openExternalLink(`${environment.cloudPortalUrl}/signup`)}>Sign Up</Button>
    </ButtonRow>

    <div>
      * You need an internet connection for Cloud Service.
    </div>
  </div>
</>);
