import type { CircularBuffer } from 'mnemonist';
import type { Units } from '@zaber/motion';
import { reactSelect } from '@zaber/react-library';

export interface Sample {
  time: number; // All times are in milliseconds and not rounded.
  value: number;
}


export type StorageBuffer = Record<string, Sample[]>;
export type RecordingBuffer = Record<string, CircularBuffer<Sample>>;


export enum YAxisId {
  LEFT,
  RIGHT,
}


export type MathFunction = (dataSet: StorageBuffer, dependencies: string[], window: number) => Sample | null;


export interface Channel {
  name: string; // Setting name or math channel ID.
  axis: number;
  id: string; // Unique ID (within device context) that combines name and axis.
  dependencyIds: string[];
  title: string;
  subtitle: string;
  color: string;
  showInChart: boolean;
  showInUi: boolean;
  yAxis: YAxisId;
  mathFunc?: MathFunction;
  prescale: number; // Used to compensate derivatives for FW 1.6384 time scale.
  samples: Sample[];
  convertedData: Sample[];
}


export const CHANNEL_DEFAULTS:
  Pick<Channel, 'dependencyIds' | 'showInChart' | 'showInUi' | 'yAxis' | 'samples' | 'convertedData' | 'prescale'> = {
    dependencyIds: [],
    showInChart: true,
    showInUi: false,
    yAxis: YAxisId.LEFT,
    prescale: 1.0,
    samples: [],
    convertedData: [],
  };


export type ChannelMap = Record<string, Channel>;
export type ChannelListMap = Record<string, Channel[]>;
export type UnitMap = Record<string, Units>;

export enum ControlTabId {
  CHANNELS,
  SETTINGS,
}


export enum Mode {
  POLLED = 'Polled',
  BUFFERED_MANUAL = 'Buffered Manual',
  EXTERNAL = 'Externally Triggered',
  DOWNLOAD = 'Download',
}


export enum MathChannelNames {
  TRAJECTORY_VEL = 'trajectory_vel',
  TRAJECTORY_ACCEL = 'trajectory_accel',
  MEASURED_VEL = 'measured_vel',
  MEASURED_ACCEL = 'measured_accel',
  FOLLOW_ERR = 'follow_err',
  FOLLOW_ERR_VEL = 'follow_err_vel',
  FOLLOW_ERR_ACCEL = 'follow_err_accel',
  ENCODER_SIGNAL_STRENGTH = 'encoder_signal_strength',
  MOTOR_CURRENT_3PHASE = 'motor_current_3phase',
}


export enum SignalType {
  NONE = 'None',
  MOVE_ABSOLUTE = 'Absolute Move',
  MOVE_RELATIVE = 'Relative Move',
}


export const NEW_RECORDING_TAB = -1;


export interface UnitConversionCache {
  contextualDimensionScales: Record<number, number>;
  baseDimensionScales: Record<string, number>;
  settingContextualDimensions: Record<string, number>;
}


export const NEW_RECORDING_TAB_LABEL = 'New Recording';
export const ADD_SETTING_BUTTON_LABEL = 'Add Data Source';
export const ADD_MATH_BUTTON_LABEL = 'Add Computed Math';
export const ADD_IO_BUTTON_LABEL = 'Add I/O Pins';
export const START_RECORDING_BUTTON_LABEL = 'Start';
export const STOP_RECORDING_BUTTON_LABEL = 'Stop';
export const REPEAT_RECORDING_BUTTON_LABEL = 'Repeat';
export const REPLACE_RECORDING_BUTTON_LABEL = 'Overwrite';
export const DOWNLOAD_BUTTON_LABEL = 'Download';
export const SHARE_SETTINGS_LABEL = 'Apply settings to all recordings';
export const CAPTURE_SETTINGS_TAB_LABEL = 'Capture Settings';
export const DELETE_ALL_BUTTON_LABEL = 'Delete All';
export const MAX_DEVICE_CHANNELS_TOOLTIP = 'Maximum number of device data sources reached';
export const WAITING_FOR_TRIGGER_MESSAGE = 'Waiting for external "scope start" command...';
export const WAITING_FOR_EXTERNAL_PRINT_MESSAGE = 'Waiting for external program to read scope data...';


export interface ComboBoxOption {
  value: number | string;
  label: string;
}

export const reactSelectStyle = reactSelect.getDefaultStyles<ComboBoxOption>();

export const getComboBoxItemLabel = (option: ComboBoxOption) => option.label;

export const MAX_DEVICE_CHANNELS = 6;
