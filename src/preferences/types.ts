import { environment } from '../environment';

export type PreferenceValueType = 'bool' | 'number';
export type PreferenceValue = boolean | number;

export interface PreferenceState {
  value: PreferenceValue;
  updating: boolean;
}

export interface PreferenceItem {
  title: string;
  key: string;
  type: PreferenceValueType;
  tooltip: string;
  component?: JSX.Element;
}

export enum PreferenceKeys {
  ZML_LOGGING = 'logZmlCommands',
  TIMEOUT_LOCAL = 'timeoutLocal',
  TIMEOUT_REMOTE = 'timeoutRemote',
}

export interface PreferenceTypes {
  [PreferenceKeys.ZML_LOGGING]: boolean;
  [PreferenceKeys.TIMEOUT_LOCAL]: number;
  [PreferenceKeys.TIMEOUT_REMOTE]: number;
}

export const DEFAULT_VALUES: _.Dictionary<PreferenceValue> = {
  [PreferenceKeys.ZML_LOGGING]: environment.isProduction,
  [PreferenceKeys.TIMEOUT_LOCAL]: 1000,
  [PreferenceKeys.TIMEOUT_REMOTE]: 5000,
};

export type PreferenceModifiedCallback<T extends PreferenceValue = PreferenceValue> = (value: T) => Promise<void>;
