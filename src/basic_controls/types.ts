import type { AxisIdentity, DeviceIdentity } from '@zaber/motion/ascii';

import { EntityKey, makeAxisKey } from '../keys';
import type { Nullable } from '../utils';

type Invalid =  {
  type: 'invalid';
  key: EntityKey;
  reason: 'disconnected' | 'unidentified' | 'error';
  message: string;
};

export type MotionInfo = {
  type: 'linear' | 'cyclic';
  isIndexed: boolean;
  min: number;
  max: number;
  /** Total travel for linear axes, or distance of one cycle for cyclic axes */
  travel: number;
  maxSpeed: number;
};

export type MotionAxisInfo = {
  type: 'motion-axis';
  key: EntityKey;
  number: number;
  identity: AxisIdentity;
  motion: MotionInfo;
};

export type AxisInfo = MotionAxisInfo | Invalid;

export type LockstepGroup = {
  type: 'lockstep';
  group: number;
  primary: MotionAxisInfo;
  secondaries: MotionAxisInfo[];
  motion: MotionInfo;
};

export type MovablePeripheral = MotionAxisInfo | LockstepGroup;

/** An axis' unchanging info */
export type PeripheralInfo = AxisInfo | LockstepGroup;

/** The numbers of each type of IO channel on this device */
export type IoInfo = {
  ao: number;
  ai: number;
  do: number;
  di: number;
};

type IntegratedDeviceInfo = {
  type: 'integrated';
  key: EntityKey;
  io: IoInfo | null;
  identity: DeviceIdentity;
  motion: MotionInfo;
};

export type Moveable = Extract<ProductInfo, { motion: MotionInfo }>;

export const moveableKey = (moveable: Moveable): EntityKey => (
  moveable.type === 'lockstep' ? moveable.primary.key :
  moveable.type === 'integrated' ? makeAxisKey(moveable.key, 1) :
  moveable.key
);

export function isMoveable(info?: ProductInfo): info is Moveable {
  return info != null && (info.type === 'motion-axis' || info.type === 'lockstep' || info.type === 'integrated');
}

export type ControllerInfo = {
  type: 'controller';
  key: EntityKey;
  peripherals: Map<number, PeripheralInfo>;
  io: IoInfo | null;
};

type ProcessControllerInfo = {
  type: 'process-controller';
  key: EntityKey;
  deviceId: number;
};

type LampControllerInfo = {
  type: 'lamp-controller';
  key: EntityKey;
  deviceId: number;
};

type XJoyInfo = {
  type: 'x-joy';
  key: EntityKey;
  deviceId: number;
};

export function isUnsupportedDevice(info?: ProductInfo): info is ProcessControllerInfo | LampControllerInfo | XJoyInfo {
  return info != null && (info.type === 'process-controller' || info.type === 'lamp-controller' || info.type === 'x-joy');
}

/** A collection of the device's unchanging information */
export type DeviceControlsInfo = IntegratedDeviceInfo | ControllerInfo | ProcessControllerInfo | LampControllerInfo | XJoyInfo | Invalid;

export type ProductInfo = PeripheralInfo | DeviceControlsInfo;

export const productKey = (product: Nullable<ProductInfo>, defaultKey: string): EntityKey => (
  product?.type === 'integrated' || product?.type === 'lockstep' ? moveableKey(product) :
  product != null ? product.key :
  defaultKey
);

/** The maximum number of indexed positions when we still consider a rotary device indexable. */
export const INDEXED_POSITIONS_LIMIT = 36;

export const X_ADR_PERIPHERAL_IDS = [70365, 70366, 70367, 70368];
