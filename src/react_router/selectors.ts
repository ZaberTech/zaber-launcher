import { createSelector } from 'reselect';

import { selectRouter } from '../store';

export const selectSearchString = createSelector(selectRouter, state => new URLSearchParams(state.location.search));
