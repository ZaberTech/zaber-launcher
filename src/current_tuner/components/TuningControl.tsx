import React, { ReactNode, useState } from 'react';
import { Icons, Text, Slider } from '@zaber/react-library';

interface Props {
  name: string;
  sliderValue: number;
  disabled: boolean;
  onChange: (value: number) => void;
  min: number;
  max: number;
  step: number;
  manual: ReactNode;
}

export const TuningControl: React.FC<Props> = ({ name, sliderValue, disabled, onChange, min, max, step, manual }) => {
  const [showManual, setShowManual] = useState(false);

  return (<>
    <div className="tuning-control">
      <Text t={Text.Type.H5} className="tuning-attribute">
        {name}
      </Text>
      <Slider min={min} max={max} step={step} showTicks title={name}
        value={sliderValue} onValueChange={onChange} disabled={disabled}/>
      <div className="tooltip">
        <Icons.QuestionMark activated={showManual} onClick={() => setShowManual(!showManual)}
          title={showManual ? 'Hide documentation' : 'Show documentation'}/>
      </div>
    </div>
    {showManual && <div className="tuning-manual">{manual}</div>}
  </>);
};
