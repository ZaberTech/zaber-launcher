import { Units } from '@zaber/motion';
import _ from 'lodash';

import { hashStringToColor } from '../colors';
import type { IdentifiedAxisState, IdentifiedDeviceState, LockstepGroup } from '../connection_manager';
import { derivativeUnit, getDimensionName, getUnitLabel } from '../units';
import { htmlColor } from '../utils';


import type { CsvCellType } from './csv';
import { PvtSeriesHelper } from './pvtSeriesHelper';
import { Sign, type Bug, type Path, Span, LimitType, Peak } from './types';


export function findBugs(userPath: Path, devicePath: Path): Bug[] {
  let bugs: Bug[] = [];
  devicePath.series.forEach((ds, seriesIndex) => {
    const us = userPath.series[seriesIndex];
    const helper = PvtSeriesHelper.FromUnits(us, userPath.positionUnit, userPath.velocityUnit);
    if (ds.positionRange && ds.positionLimits) {
      if (ds.points[0].pos > ds.positionLimits.max || ds.points[0].pos < ds.positionLimits.min) {
        bugs.push({
          time: 0,
          value: ds.points[0].pos,
          dimension: getDimensionName(userPath.positionUnit) ?? 'Length',
          pointIndex: 0,
          seriesIndex,
          message: `${ds.name} start position exceeds device limits`,
          type: LimitType.HARD_LIMIT,
        });
      }

      if (ds.positionRange.max.value > ds.positionLimits.max) {
        bugs = bugs.concat(helper.findPositionLimitCrossings(ds.positionLimits.max, Sign.Positive)
          .map(({ time, index }) => ({
            time,
            value: ds.positionLimits!.max,
            dimension: getDimensionName(userPath.positionUnit) ?? 'Length',
            pointIndex: index,
            seriesIndex,
            message: `${ds.name} exceeds device maximum position at t=${time.toFixed(3)}s`,
            type: LimitType.HARD_LIMIT,
          })));
      }

      if (ds.positionRange.min.value < ds.positionLimits.min) {
        bugs = bugs.concat(helper.findPositionLimitCrossings(ds.positionLimits.min, Sign.Negative)
          .map(({ time, index }) => ({
            time,
            value: ds.positionLimits!.min,
            dimension: getDimensionName(userPath.positionUnit) ?? 'Length',
            pointIndex: index,
            seriesIndex,
            message: `${ds.name} passes device minimum position at t=${time.toFixed(3)}s`,
            type: LimitType.HARD_LIMIT,
          })));
      }
    }

    if (ds.velocityRange && ds.velocityLimits) {
      if (ds.velocityRange.max.value > ds.velocityLimits.max) {
        bugs = bugs.concat(helper.findVelocityLimitCrossings(ds.velocityLimits.max, Sign.Positive)
          .map(({ time, index }) => ({
            time,
            value: ds.velocityLimits!.max,
            dimension: getDimensionName(userPath.velocityUnit) ?? 'Velocity',
            pointIndex: index,
            seriesIndex,
            message: `${ds.name} exceeds device maximum velocity at t=${time.toFixed(3)}s`,
            type: LimitType.HARD_LIMIT,
          })));
      }

      if (ds.velocityRange.min.value < ds.velocityLimits.min) {
        bugs = bugs.concat(helper.findVelocityLimitCrossings(ds.velocityLimits.min, Sign.Negative)
          .map(({ time, index }) => ({
            time,
            value: ds.velocityLimits!.min,
            dimension: getDimensionName(userPath.velocityUnit) ?? 'Velocity',
            pointIndex: index,
            seriesIndex,
            message: `${ds.name} exceeds device maximum velocity at t=${time.toFixed(3)}s`,
            type: LimitType.HARD_LIMIT,
          })));
      }
    }

    if (ds.accelerationRange && ds.accelerationLimits) {
      let decelHardCrashTimes: number[] = [];
      if (ds.positionLimits) {
        const stoppingProblems = helper.findStopCollisions(ds.positionLimits, -ds.accelerationLimits.min);
        decelHardCrashTimes = decelHardCrashTimes.concat(stoppingProblems.map(({ time }) => time));
        bugs = bugs.concat(stoppingProblems.map(({ time, index }) => ({
          time,
          value: helper.getValueAtTime(time).acceleration < 0 ? ds.accelerationLimits!.min : -ds.accelerationLimits!.min,
          dimension: getDimensionName(derivativeUnit(userPath.velocityUnit)) ?? 'Acceleration',
          pointIndex: index,
          seriesIndex,
          message: `${ds.name} could exceed device position limit if stopped at t=${time.toFixed(3)}s`,
          type: LimitType.HARD_LIMIT,
        })));
      }

      if (ds.accelerationRange.max.value > ds.accelerationLimits.max) {
        bugs = bugs.concat(helper.findAccelerationLimitCrossings(ds.accelerationLimits.max, Sign.Positive)
          .filter(({ time }) => _.every(decelHardCrashTimes, t => Math.abs(time - t) >= 0.0001))
          .map(({ time, index }) => ({
            time,
            value: ds.accelerationLimits!.max,
            dimension: getDimensionName(derivativeUnit(userPath.velocityUnit)) ?? 'Acceleration',
            pointIndex: index,
            seriesIndex,
            message: `${ds.name} exceeds device maximum acceleration at t=${time.toFixed(3)}s`,
            type: LimitType.SOFT_LIMIT,
          })));
      }

      if (ds.accelerationRange.min.value < ds.accelerationLimits.min) {
        bugs = bugs.concat(helper.findAccelerationLimitCrossings(ds.accelerationLimits.min, Sign.Negative)
          .filter(({ time }) => _.every(decelHardCrashTimes, t => Math.abs(time - t) >= 0.0001))
          .map(({ time, index }) => ({
            time,
            value: ds.accelerationLimits!.min,
            dimension: getDimensionName(derivativeUnit(userPath.velocityUnit)) ?? 'Acceleration',
            pointIndex: index,
            seriesIndex,
            message: `${ds.name} exceeds device maximum deceleration at t=${time.toFixed(3)}s`,
            type: LimitType.SOFT_LIMIT,
          })));
      }
    }
  });

  return bugs;
}


export function convertToCsvData(path: Path): CsvCellType[][] {
  const cells: CsvCellType[][] = [];

  const headers: CsvCellType[] = ['Time (s)'];
  path.series.forEach(series => {
    headers.push(`${series.name} position (${getUnitLabel(path.positionUnit)})`);
    headers.push(`${series.name} velocity (${getUnitLabel(path.velocityUnit)})`);
  });

  cells.push(headers);

  path.series[0].points.forEach((point, i) => {
    const row: CsvCellType[] = [];
    row.push(point.dt);
    path.series.forEach(series => {
      row.push(series.points[i].pos);
      row.push(series.points[i].vel);
    });

    cells.push(row);
  });

  return cells;
}


const SERIES_COLORS: string[] = [
  0xFF00FF,
  0xFF6600,
  0x00CC80,
  0x00BBFF,
  0xD32024,
  0x0000FF,
].map(c => htmlColor(c));

export function seriesColor(i: number): string {
  return SERIES_COLORS[i] ?? hashStringToColor(SERIES_COLORS[i % SERIES_COLORS.length]);
}


export function makeBufferAxisName(bufferId: string, axis: number): string {
  return `Buffer ${bufferId} Axis ${axis + 1}`;
}


export function axisHasDimensionOf(axis: IdentifiedAxisState, unit: Units | null | undefined): boolean {
  if (unit == null || unit === Units.NATIVE) {
    return false; // Because native units are not supported.
  }

  return getDimensionName(unit) === axis.movementDimensions?.position;
}


export function isFirstPointRelative(path: Path): boolean {
  return path.series.some(s => s.points.length > 1 && s.points[1].relative);
}


export function clip(...spans: Span[]): Span {
  const min = Math.max(...spans.map(s => s.min));
  const max = Math.min(...spans.map(s => s.max));
  return { min, max };
}


export function minPeak(p1: Peak | null, p2: Peak | null): Peak | null {
  if (p1 == null) {
    return p2;
  }

  return p2 && p2.value < p1.value ? p2 : p1;
}


export function maxPeak(p1: Peak | null, p2: Peak | null): Peak | null {
  if (p1 == null) {
    return p2;
  }

  return p2 && p2.value > p1.value ? p2 : p1;
}


export function getLockstepGroup(deviceInfo: IdentifiedDeviceState, axisNumber: number): LockstepGroup | null {
  return (deviceInfo?.locksteps ?? []).find(group => group.axisNumbers.includes(axisNumber)) ?? null;
}


export function getLockstepGroupNumber(deviceInfo: IdentifiedDeviceState, axisNumber: number): number | null {
  const lockstep = getLockstepGroup(deviceInfo, axisNumber);
  if (lockstep) {
    return lockstep.groupNumber;
  }

  return null;
}


export function expandAxisNumberIfInLockstep(deviceInfo: IdentifiedDeviceState, axisNumber: number): number[] {
  const lockstep = (deviceInfo?.locksteps ?? []).find(group => group.axisNumbers.includes(axisNumber));
  if (lockstep) {
    return lockstep.axisNumbers;
  }

  return [axisNumber];
}


export function getControlPointPosition(path: Path, index: number): number[] | null {
  if (index < 0 || index >= path.series[0].points.length) {
    return null;
  }

  const pos = Array<number>(path.series.length).fill(0);

  path.series.forEach((series, i) => {
    for (let j = 0; j <= index; j++) {
      const point = series.points[j];
      pos[i] = point.relative ? point.pos + pos[i] : point.pos;
    }
  });

  return pos;
}
