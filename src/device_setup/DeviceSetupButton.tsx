import { Button } from '@zaber/react-library';
import type { Nullable } from '@zaber/toolbox';
import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { tryAccess } from '../utils';

import { selectPendingSetups } from './selectors';

interface Props {
  serialNo: Nullable<number>;
}

export const DeviceSetupButton: React.FC<Props> = ({ serialNo }) => {
  const pendingSetup = tryAccess(useSelector(selectPendingSetups), serialNo);
  if (!pendingSetup) { return null }
  return (
    <Link className="device-setup" to={pendingSetup.path}>
      <Button>Setup Device</Button>
    </Link>
  );
};
