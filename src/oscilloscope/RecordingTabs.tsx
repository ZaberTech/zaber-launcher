import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { ScrollMenu, VisibilityContext } from 'react-horizontal-scrolling-menu';
import classNames from 'classnames';
import { Icons, InputEditor, MinorMenu, Text, TextInputEditor } from '@zaber/react-library';

import { selectOscilloscope } from '../store';
import { useActions } from '../utils';

import { actions as actionDefinitions } from './actions';
import { NEW_RECORDING_TAB, NEW_RECORDING_TAB_LABEL } from './types';
import { isNewRecTab, selectedTabClass } from './utils';
import { selectIsCaptureInProgress } from './selectors';


interface TabHeaderProps {
  title: string;
  itemId: string;
  tabId: number;
  selected: boolean;
  recordingIndex: number;
  onClick: () => void;
}


const TabHeader: React.FC<TabHeaderProps> = ({ title, tabId, selected, recordingIndex, onClick }) => {
  const isRecordingInProgress = useSelector(selectIsCaptureInProgress);
  const actions = useActions(actionDefinitions);

  const [value, setValue] = useState(title);
  const [mode, setMode] = useState<InputEditor.Mode>('display');

  return <div className={classNames({ active: selected })}>
    <MinorMenu.Item className={classNames('recording-tab', selectedTabClass(selected))}
      data-testid={`recording-tab ${title}`}
      onClick={onClick}>
      <TextInputEditor
        value={value}
        mode={mode}
        onChange={({ value, mode }) => {
          setValue(value);
          setMode(mode);
          if (mode === 'write') {
            actions.setRecordingTitle(recordingIndex, value);
            setMode('display');
          }
        }}
      />
      <Icons.Cross
        className="close-button"
        title="Delete this recording"
        data-testid="delete-recording"
        disabled={isRecordingInProgress}
        onClick={() => actions.deleteRecording(tabId)}/>
    </MinorMenu.Item>
  </div>;
};

const LeftArrow: React.FC = () => {
  const {
    isFirstItemVisible,
    isLastItemVisible,
    scrollPrev,
    visibleElements,
    initComplete,
  } = useContext(VisibilityContext);

  const [disabled, setDisabled] = useState(
    !initComplete || (initComplete && isFirstItemVisible)
  );

  useEffect(() => {
    if (visibleElements.length) {
      setDisabled(isFirstItemVisible);
    }
  }, [isFirstItemVisible, visibleElements.length]);

  if (!(isFirstItemVisible && isLastItemVisible)) {
    return <Icons.ArrowCollapsed style={{ transform: 'rotate(180deg)' }}
      className="menu-scroll-left"
      disabled={disabled}
      onClick={() => scrollPrev()}/>;
  }

  return null;
};


const RightArrow: React.FC = () => {
  const {
    isFirstItemVisible,
    isLastItemVisible,
    scrollNext,
    visibleElements,
  } = useContext(VisibilityContext);

  const [disabled, setDisabled] = useState(
    !visibleElements.length && isLastItemVisible
  );

  useEffect(() => {
    if (visibleElements.length) {
      setDisabled(isLastItemVisible);
    }
  }, [isLastItemVisible, visibleElements.length]);

  if (!(isFirstItemVisible && isLastItemVisible)) {
    return <Icons.ArrowCollapsed className="menu-scroll-right" disabled={disabled} onClick={() => scrollNext()}/>;
  }

  return null;
};


type ScrollVisibilityApiType = React.ContextType<typeof VisibilityContext>;

function onMouseWheel(visApi: ScrollVisibilityApiType, e: React.WheelEvent): void {
  // Special case for touch screens - from scroll menu example code.
  if (Math.abs(e.deltaX) !== 0 || Math.abs(e.deltaY) < 15) {
    e.stopPropagation();
    return;
  }

  if (e.deltaY < 0) {
    visApi.scrollNext();
  } else if (e.deltaY > 0) {
    visApi.scrollPrev();
  }
}


function useDrag() {
  const [clicked, setClicked] = useState(false);
  const [dragging, setDragging] = useState(false);
  const position = useRef(0);

  const dragStart = useCallback((ev: React.MouseEvent) => {
    position.current = ev.clientX;
    setClicked(true);
  }, []);

  const dragStop = useCallback(() =>
    // Prevent item under cursor from being clicked.
    window.requestAnimationFrame(() => {
      setDragging(false);
      setClicked(false);
    }), []
  );

  const dragMove = (ev: React.MouseEvent, cb: (posDif: number) => void) => {
    const newDiff = position.current - ev.clientX;
    const movedEnough = Math.abs(newDiff) > 5;

    if (clicked && movedEnough) {
      setDragging(true);
    }

    if (dragging && movedEnough) {
      position.current = ev.clientX;
      cb(newDiff);
    }
  };

  return {
    dragStart,
    dragStop,
    dragMove,
    dragging,
    position,
    setDragging
  };
}


export const RecordingTabs: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const { recordings, selectedRecordingIndex } = useSelector(selectOscilloscope);
  const { dragStart, dragStop, dragMove, dragging } = useDrag();
  const apiRef = useRef({} as ScrollVisibilityApiType);

  useEffect(() => {
    if (selectedRecordingIndex !== NEW_RECORDING_TAB) {
      const element = apiRef.current?.getItemElementById(selectedRecordingIndex.toString());
      if (element) {
        apiRef.current?.scrollToItem?.(element, 'smooth', 'center', 'nearest');
      }
    }
  }, [selectedRecordingIndex]);

  const handleDrag = ({ scrollContainer }: ScrollVisibilityApiType) => (ev: React.MouseEvent) =>
    dragMove(ev, posDiff => {
      if (scrollContainer.current) {
        scrollContainer.current.scrollLeft += posDiff;
      }
    });

  const handleItemClick = (recordingIndex: number) => {
    if (!dragging) {
      actions.selectRecordingTab(recordingIndex);
    }
  };

  return <MinorMenu barStyle="horizontal" className="recording-tabs">
    <ScrollMenu
      LeftArrow={recordings.length > 1 ? LeftArrow : null}
      RightArrow={recordings.length > 1 ? RightArrow : null}
      apiRef={apiRef}
      onWheel={onMouseWheel}
      onMouseDown={() => dragStart}
      onMouseUp={() => dragStop}
      onMouseMove={handleDrag}>
      {recordings.map((recording, i) => <TabHeader
        title={recording.title}
        itemId={i.toString()}
        tabId={i}
        key={i}
        recordingIndex={i}
        onClick={() => handleItemClick(i)}
        selected={i === selectedRecordingIndex}/>
      )}
    </ScrollMenu>
    <div className={classNames({ active: isNewRecTab(selectedRecordingIndex) })}>
      <MinorMenu.Item
        className={classNames('recording-tab', 'new-recording-tab', selectedTabClass(isNewRecTab(selectedRecordingIndex)))}
        data-testid="recording-tab New Recording"
        onClick={() => actions.selectRecordingTab(NEW_RECORDING_TAB)}
      >
        <Text>{NEW_RECORDING_TAB_LABEL}</Text>
      </MinorMenu.Item>
    </div>
  </MinorMenu>;
};
