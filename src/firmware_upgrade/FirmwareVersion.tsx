import { Button, ContextMenu, Icons, Text } from '@zaber/react-library';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import type { IdentifiedDeviceInfo } from '../connection_manager/types';
import { environment, Flavors } from '../environment';
import type { RootState } from '../store';
import { DeviceIdentifiers, Version } from '../app_components';

import { actions as actionsDefinition } from './actions';
import { ReleaseNotes } from './ReleaseNotes';
import { selectDeviceIdentifiers, selectDeviceInfoWithAxes } from './selectors';

interface Props {
  version: Version;
  newest?: boolean;
}

interface DispatchProps {
  actions: typeof actionsDefinition;
}

interface StateProps {
  device: IdentifiedDeviceInfo;
  identifiers: DeviceIdentifiers;
}

class FirmwareVersionBase extends Component<Props & DispatchProps & StateProps> {
  render() {
    const { version, newest, actions, device, identifiers } = this.props;
    return <div className="firmware-version">
      <Text t={Text.Type.Body}>{version.str}</Text>
      <span className="spacer"/>

      {newest && <Button
        color="grey"
        title={`Skip Version ${version.str}`}
        onClick={() => { actions.skipVersion(device.identity.serialNumber, version) }}
      >
        Skip
      </Button>}

      <Button
        color={newest ? 'red' : 'grey'}
        title={`Install Firmware ${version.str}`}
        onClick={() => { actions.downloadAndInstall(device.key, identifiers, version) }}
      >
        Install
      </Button>

      {environment.flavor === Flavors.Zaber && <ReleaseNotes
        version={version}
        trigger={openReleaseNotes => (
          <ContextMenu>
            <ContextMenu.Item onClick={openReleaseNotes} icon={<Icons.Notes/>}>
              Read Release Notes
            </ContextMenu.Item>
          </ContextMenu>
        )}/>}
    </div>;
  }
}

export const FirmwareVersion = connect<StateProps, DispatchProps, unknown, RootState>(
  (state: RootState): StateProps => ({
    device: selectDeviceInfoWithAxes(state)!,
    identifiers: selectDeviceIdentifiers(state)!,
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(FirmwareVersionBase);
