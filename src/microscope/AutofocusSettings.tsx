/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import { ButtonRowConfirmCancel, Icons, Loader, Modal, NoticeBanner, NumericInput, Select } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { OBJECTIVE_PARAMETER_LABELS, ObjectiveParameters, Operations } from './types';
import { OperationError } from './components';
import { useOperationResult, useOperationState } from './hooks';
import { selectAutofocusSettings, selectObjectiveLabels } from './selectors';

export const WDI_MAGNIFICATIONS = [1, 2, 5, 10, 20, 50, 100];
const MAGNIFICATION_OPTIONS = [
  { value: 0, label: 'Disabled' },
  ...WDI_MAGNIFICATIONS.map(mag => ({ value: mag, label: `${mag}x` })),
];

const EMPTY_PARAMS: ObjectiveParameters = {
  magnification: 0,
  control_loop_ki: null,
  control_loop_kp: null,
  max_exposure: null,
  signal_to_noise_ratio: null,
};

const GENERIC_DEFAULT_PARAMS: Omit<ObjectiveParameters, 'magnification'> = {
  control_loop_ki: 250,
  control_loop_kp: null,
  max_exposure: 600,
  signal_to_noise_ratio: 0.02,
};

const DEFAULT_PARAMS: Record<number, Omit<ObjectiveParameters, 'magnification'>> = {
  0: EMPTY_PARAMS,
  1: GENERIC_DEFAULT_PARAMS,
  2: GENERIC_DEFAULT_PARAMS,
  5: GENERIC_DEFAULT_PARAMS,
  10: GENERIC_DEFAULT_PARAMS,
  20: GENERIC_DEFAULT_PARAMS,
  50: { ...GENERIC_DEFAULT_PARAMS, signal_to_noise_ratio: 0.015 },
  100: { ...GENERIC_DEFAULT_PARAMS, signal_to_noise_ratio: 0.015 },
};

export const AutofocusSettings: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const { open } = useSelector(selectAutofocusSettings);
  const [loadedSettings, isLoading] = useOperationResult(Operations.autofocusGetSettings);
  const [objectives, setObjectives] = useState<ObjectiveParameters[]>([]);
  const objectiveLabels = useSelector(selectObjectiveLabels);

  useEffect(() => {
    if (loadedSettings) {
      setObjectives(loadedSettings.objectives);
    }
  }, [loadedSettings]);

  const [isSaving] = useOperationState(Operations.autofocusSetSettings);

  function changeObjective(index: number, name: keyof ObjectiveParameters, value: number | null) {
    const newObjectives = objectives.map((parameters, i) => index === i ? ({
      ...parameters,
      [name]: value,
    }) : parameters);
    setObjectives(newObjectives);
  }

  function changeMagnification(index: number, value: number) {
    const newObjectives = objectives.map((parameters, i) => index === i ? ({
      magnification: value,
      ...DEFAULT_PARAMS[value],
    }) : parameters);
    setObjectives(newObjectives);
  }

  const [objective, setObjective] = useState(0);
  const parameters = objectives.at(objective);

  return <>
    <Modal
      className="autofocus-settings"
      headerIcon={<Icons.Settings/>}
      headerText="Autofocus Settings"
      onRequestClose={() => actions.setAutofocusSettingsOpen(false)}
      isOpen={open}
      buttons={<ButtonRowConfirmCancel
        confirmText={isSaving ? 'Saving...' : 'Save'}
        onConfirm={isSaving ? 'disabled' : () => actions.operationStart(Operations.autofocusSetSettings, {
          objectives,
        })}
        onCancel={() => actions.setAutofocusSettingsOpen(false)}
      />}
    >
      <NoticeBanner type="warning">
        Changing the following setting may affect the autofocus performance to the point where it may not work as expected.
      </NoticeBanner>

      <div className="table">
        {objectiveLabels.length > 1 && <>
          <Select portalToBody
            labelContent="Objective"
            className="objective-select"
            placeholder="Select Objective"
            options={objectiveLabels.map((label, value) => ({ value, label }))}
            value={objective}
            onValueChange={setObjective}/>
          <div/>
        </>}

        {!isLoading && parameters && <>
          <Select portalToBody
            labelContent={{
              label: 'Magnification',
              help: <>
                Determines default values for the rest of the settings.
                Choose next highest magnification if the exact magnification is not available.
              </>,
            }}
            data-testid={OBJECTIVE_PARAMETER_LABELS.magnification}
            options={MAGNIFICATION_OPTIONS}
            value={parameters.magnification}
            onValueChange={value => changeMagnification(objective, value)}/>
          <div/>
          <NumericInput
            labelContent={{
              label: 'Control Loop Ki',
              help: 'Derivative gain for the PI control loop.',
            }}
            data-testid={OBJECTIVE_PARAMETER_LABELS.control_loop_ki} status={null}
            value={parameters.control_loop_ki} onNumberChange={value => {
              changeObjective(objective, 'control_loop_ki', value);
            }}/>
          <NumericInput
            labelContent={{
              label: 'Control Loop Kp',
              help: 'Proportional gain for the PI control loop.',
            }}
            data-testid={OBJECTIVE_PARAMETER_LABELS.control_loop_kp} status={null}
            value={parameters.control_loop_kp} onNumberChange={value => {
              changeObjective(objective, 'control_loop_kp', value);
            }}/>
          <NumericInput
            labelContent={{
              label: 'Max Exposure',
              help: 'Maximum exposure time in microseconds.',
            }}
            data-testid={OBJECTIVE_PARAMETER_LABELS.max_exposure} status={null}
            disabled={!parameters.magnification}
            value={parameters.max_exposure} onNumberChange={value => {
              changeObjective(objective, 'max_exposure', value);
            }}/>
          <NumericInput displayPrecision={3}
            labelContent={{
              label: 'Signal to Noise Ratio',
              help: 'The signal to noise ratio threshold that determines the relative signal height used to reject noise.',
            }}
            data-testid={OBJECTIVE_PARAMETER_LABELS.signal_to_noise_ratio} status={null}
            disabled={!parameters.magnification}
            value={parameters.signal_to_noise_ratio} onNumberChange={value => {
              changeObjective(objective, 'signal_to_noise_ratio', value);
            }}/>
        </>}
      </div>
      {isLoading && <div><Loader size="medium"/></div>}

      <OperationError operation={Operations.autofocusGetSettings}/>
      <OperationError operation={Operations.autofocusSetSettings}/>
    </Modal>

    <Icons.Settings className="settings-icon" title="Settings" onClick={() => {
      actions.setAutofocusSettingsOpen(true);
      actions.operationStart(Operations.autofocusGetSettings);
    }}/>
  </>;
};
