/* eslint-disable camelcase */

import { ConnectionType } from '../app_components';
import type { selectAxes, selectConnections, selectDevices } from '../connection_manager';

import { generateCommandList } from './GenerateCommandList';
import type { Command } from './types';

const connections: ReturnType<typeof selectConnections> = {
  '@connection|connection1': {
    key: '@connection|connection1',
    id: 'connection1',
    type: ConnectionType.SERIAL_PORT,
    config: {
      serialPort: 'COM1',
      name: '',
      baudRate: null
    },
    loadingDevices: false,
    identifyingDevices: false,
    loadingDevicesErr: null,
    devices: [
      '@device|device1',
      '@device|device2',
    ]
  }
};

const devices: ReturnType<typeof selectDevices> = {
  '@device|device1': {
    key: '@device|device1',
    axes: [
      '@axis|axis1',
      '@axis|axis2'
    ],
    address: 1,
    axisCount: 1,
    isMultiAxis: true,
    isController: false,
    isRemote: false,
    platformId: null,
    isKeyed: false,
    locksteps: null,
    identity: null,
    nonIdentifiedInfo: null,
    label: null,
    product: {
      id: 1,
      capabilities: [],
      conversionTable: {
        rows: [
          {
            contextual_dimension_id: 1,
            dimension_name: 'Length',
            contextual_dimension_name: '',
            function_name: '',
            scale: null
          }
        ]
      },
      commandTree: {
        command: '',
        nodes: [
          {
            command: 'move',
            nodes: [
              {
                command: 'abs',
                nodes: [
                  {
                    command: 'position',
                    is_command: true,
                    is_param: true,
                    param_arity: 1,
                    contextual_dimension_id: 1,
                  }
                ]
              }
            ]
          },
          {
            command: 'pvt',
            nodes: [
              {
                command: 'number',
                is_param: true,
                nodes: [
                  {
                    command: 'info',
                    is_command: true
                  }
                ]
              }
            ]
          },
        ]
      },
      settings: {
        rows: [
          {
            name: 'accel',
            param_type: 'Uint',
            decimal_places: 0,
            visibility: 'advanced',
          },
          {
            name: 'version',
            param_type: 'Uint',
            decimal_places: 0,
            visibility: 'advanced',
          }
        ]
      }
    }
  },
  '@device|device2': {
    key: '@device|device2',
    axes: [],
    address: 2,
    axisCount: 0,
    isMultiAxis: false,
    isController: true,
    isRemote: false,
    platformId: null,
    isKeyed: false,
    locksteps: null,
    identity: null,
    nonIdentifiedInfo: null,
    label: null,
    product: {
      id: 2,
      capabilities: [],
      conversionTable: {
        rows: []
      },
      commandTree: {
        command: ''
      },
      settings: {
        rows: [
          {
            name: 'maxspeed',
            param_type: 'Uint',
            decimal_places: 0,
            visibility: 'advanced',
          }
        ]
      }
    }
  }
};

const axes: ReturnType<typeof selectAxes> = {
  '@axis|axis1': {
    key: '@axis|axis1',
    axisNumber: 1,
    isPeripheralLike: true,
    serialNumber: 0,
    label: null,
    identity: null,
    canMove: false,
    movementDimensions: null,
    nonIdentifiedInfo: null,
    product: {
      id: 1,
      capabilities: [],
      conversionTable: {
        rows: []
      },
      commandTree: {
        command: '',
        nodes: [
          {
            command: 'stop',
            is_command: true,
          },
          {
            command: 'move',
            nodes: [
              {
                command: 'minmax',
                is_param: true,
                param_type: 'Enum',
                is_command: true,
                param_arity: 1,
                values: ['min', 'max']
              }
            ]
          },
        ]
      },
      settings: {
        rows: [
          {
            name: 'peripheral.id',
            param_type: 'Uint',
            decimal_places: 0,
            visibility: 'advanced',
          }
        ]
      }
    }
  },
  '@axis|axis2': {
    key: '@axis|axis2',
    axisNumber: 2,
    isPeripheralLike: true,
    serialNumber: 1,
    label: null,
    identity: null,
    canMove: false,
    movementDimensions: null,
    nonIdentifiedInfo: null,
    product: {
      id: 2,
      capabilities: [],
      conversionTable: {
        rows: [
          {
            contextual_dimension_id: 1,
            dimension_name: 'Length',
            contextual_dimension_name: '',
            function_name: '',
            scale: null
          }
        ]
      },
      commandTree: {
        command: '',
        nodes: [
          {
            command: 'test',
            is_command: true,
          }
        ]
      },
      settings: {
        rows: [
          {
            name: 'pos',
            param_type: 'Uint',
            decimal_places: 0,
            visibility: 'always',
            contextual_dimension_id: 1,
          }
        ]
      }
    }
  },
};

test('get all possible commands for connection', () => {
  const commandList: Command[] = generateCommandList(connections, devices, axes, '@connection|connection1');
  expect(commandList).toHaveLength(15);
  expect(commandList).toContainEqual({
    type: 'command',
    parts: [
      {
        text: 'move',
        type: 'command'
      },
      {
        text: 'abs',
        type: 'command',
      },
      {
        text: 'position',
        type: 'param',
        multiple: false,
        units: [],
      }
    ]
  });
  expect(commandList).toContainEqual({
    type: 'setting',
    parts: [
      {
        text: 'get',
        type: 'command'
      },
      {
        text: 'accel',
        type: 'command'
      }
    ]
  });
  expect(commandList).toContainEqual({
    type: 'command',
    parts: [
      {
        text: 'test',
        type: 'command'
      }
    ]
  });
});

test('get all possible commands for device', () => {
  const commandList: Command[] = generateCommandList(connections, devices, axes, '@device|device1');
  expect(commandList).toHaveLength(13);
  expect(commandList).toContainEqual({
    type: 'command',
    parts: [
      {
        text: 'pvt',
        type: 'command'
      },
      {
        text: 'number',
        type: 'param',
        multiple: true,
        units: [],
      },
      {
        text: 'info',
        type: 'command'
      }
    ]
  });
  expect(commandList).toContainEqual({
    type: 'command',
    parts: [
      {
        text: 'move',
        type: 'command'
      },
      {
        text: 'abs',
        type: 'command',
      },
      {
        text: 'position',
        type: 'param',
        multiple: false,
        units: [
          { LongName: 'metres', Offset: 0, Scale: 1, ShortName: 'm', id: 'Length:metres' },
          { LongName: 'centimetres', Offset: 0, Scale: 100, ShortName: 'cm', id: 'Length:centimetres' },
          { LongName: 'millimetres', Offset: 0, Scale: 1000, ShortName: 'mm', id: 'Length:millimetres' },
          { LongName: 'micrometres', Offset: 0, Scale: 1000000, ShortName: 'µm', id: 'Length:micrometres' },
          { LongName: 'nanometres', Offset: 0, Scale: 1000000000, ShortName: 'nm', id: 'Length:nanometres' },
          { LongName: 'inches', Offset: 0, Scale: 39.37007874015748, ShortName: 'in', id: 'Length:inches' }
        ],
      }
    ]
  });
  expect(commandList).toContainEqual({
    type: 'setting',
    parts: [
      {
        text: 'set',
        type: 'command'
      },
      {
        text: 'peripheral.id',
        type: 'command'
      },
      {
        text: 'value',
        type: 'param',
        multiple: false,
        units: [],
      }
    ]
  });
});

test('get all possible commands for axis', () => {
  const commandList: Command[] = generateCommandList(connections, devices, axes, '@axis|axis1');
  expect(commandList).toHaveLength(4);
  expect(commandList).toContainEqual({
    type: 'command',
    parts: [
      {
        text: 'move',
        type: 'command'
      },
      {
        text: ['min', 'max'],
        type: 'enum',
        multiple: false,
      }
    ]
  });
});

test('get all possible commands for axis with units', () => {
  const commandList: Command[] = generateCommandList(connections, devices, axes, '@axis|axis2');
  expect(commandList).toHaveLength(3);
  expect(commandList).toContainEqual({
    type: 'setting',
    parts: [
      {
        text: 'set',
        type: 'command'
      },
      {
        text: 'pos',
        type: 'command'
      },
      {
        text: 'value',
        type: 'param',
        multiple: false,
        units: [
          { LongName: 'metres', Offset: 0, Scale: 1, ShortName: 'm', id: 'Length:metres' },
          { LongName: 'centimetres', Offset: 0, Scale: 100, ShortName: 'cm', id: 'Length:centimetres' },
          { LongName: 'millimetres', Offset: 0, Scale: 1000, ShortName: 'mm', id: 'Length:millimetres' },
          { LongName: 'micrometres', Offset: 0, Scale: 1000000, ShortName: 'µm', id: 'Length:micrometres' },
          { LongName: 'nanometres', Offset: 0, Scale: 1000000000, ShortName: 'nm', id: 'Length:nanometres' },
          { LongName: 'inches', Offset: 0, Scale: 39.37007874015748, ShortName: 'in', id: 'Length:inches' }
        ],
      }
    ]
  });
});

test('get empty set of commands from non-existent device', () => {
  const commandList: Command[] = generateCommandList(connections, devices, axes, '@device|device3');
  expect(commandList).toHaveLength(0);
});

test('get empty set of commands from non-existent axis', () => {
  const commandList: Command[] = generateCommandList(connections, devices, axes, '@axis|axis3');
  expect(commandList).toHaveLength(0);
});
