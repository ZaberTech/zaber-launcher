import { Units } from '@zaber/motion';
import { ConfirmCancel, Icons } from '@zaber/react-library';
import classNames from 'classnames';
import React, { Component, createRef } from 'react';
import { match, P } from 'ts-pattern';

import { InputWithUnitProps, InputWithUnits } from './InputWithUnits';
import { hasUnits, Measurement, MeasurementOK, measurementOK, titleOfUnitFrom } from './types';

export type EditableInputMode = 'displaying' | 'reading' | 'editing' | 'writing';

type NonWritingState = Measurement & { mode: 'displaying' | 'reading' };
type EditingState = Measurement & { mode: 'editing'; preEditMeasure: Measurement };
type WritingState = MeasurementOK & { mode: 'writing'; preEditMeasure: Measurement };

export type ValueUnitState = NonWritingState | EditingState | WritingState;

export function isEditingState(state: EditableInputMode): state is 'editing' | 'writing' {
  return ['editing', 'writing'].includes(state);
}

interface Props extends Omit<InputWithUnitProps, 'onChange'> {
  mode: EditableInputMode;
  onChange: (change: ValueUnitState) => void;
}

interface State {
  preEditMeasure: Measurement;
}

export class EditableInputWithUnits extends Component<Props, State> {
  private inputRef: React.RefObject<HTMLInputElement>;

  constructor(props: Props) {
    super(props);
    this.state = {
      preEditMeasure: hasUnits(props.measure) ? props.measure : { value: 0, units: Units.NATIVE },
    };
    this.inputRef = createRef<HTMLInputElement>();
  }

  onConfirm = () => {
    const { measure, onChange } = this.props;
    if (!measurementOK(measure)) { return }
    onChange({ ...measure, mode: 'writing', preEditMeasure: this.state.preEditMeasure });
  };

  onInputKeyDown = (e: React.KeyboardEvent<HTMLInputElement>): void => {
    if (this.props.mode === 'editing') {
      switch (e.key) {
        case 'Enter': this.onConfirm(); break;
        case 'Escape': this.cancelEditing(); break;
      }
    }
  };

  componentDidUpdate(prevProps: Props) {
    if (this.inputRef.current && this.props.mode === 'editing' && prevProps.mode !== 'editing') {
      this.inputRef.current.focus();
      this.inputRef.current.select();
    }
  }

  beginEditing = (currentValueAndUnits: Measurement) => {
    this.setState({ preEditMeasure: currentValueAndUnits });
    this.props.onChange({ ...currentValueAndUnits, mode: 'editing', preEditMeasure: currentValueAndUnits });
  };

  cancelEditing = () => this.props.onChange({ ...this.state.preEditMeasure, mode: 'displaying' });

  render() {
    const { className, displayPrecision, mode, measure, onChange, disabled, title, ...inputProps } = this.props;
    const rect = this.inputRef.current?.getBoundingClientRect();

    const descriptionInTitle = title ?? titleOfUnitFrom(inputProps.unitFrom);
    const editing = isEditingState(mode);

    return (
      <InputWithUnits
        {...inputProps}
        className={classNames(className, 'editable-input-with-unit')}
        disabled={
          disabled ? disabled :
          mode === 'reading' ? true :
          mode === 'displaying' ? 'readonly' :
          false
        }
        ref={this.inputRef}
        displayPrecision={!editing ? displayPrecision : undefined}
        measure={measure}
        onChange={newValueAndUnit => match(mode)
          .with(P.union('reading', 'displaying'), mode => onChange({ ...newValueAndUnit, mode }))
          .with('editing', mode => onChange({ ...newValueAndUnit, mode, preEditMeasure: this.state.preEditMeasure }))
          .otherwise(() => null)}
        onKeyDown={this.onInputKeyDown}
        title={descriptionInTitle}
      >
        <div className="edit-controls">
          {match(mode)
            .with(P.union('displaying', 'reading'), () => {
              if (!disabled && hasUnits(measure)) {
                return <Icons.Edit
                  title={`Edit ${descriptionInTitle}`}
                  className="edit"
                  disabled={!!disabled || mode === 'reading'}
                  onClick={() => this.beginEditing(measure)}
                />;
              }
              return null;
            })
            .with(P.union('editing', 'writing'), () => (
              <ConfirmCancel
                confirmTitle={`Confirm ${descriptionInTitle} edit`} cancelTitle={`Cancel editing ${descriptionInTitle}`}
                confirmDisabled={mode === 'writing' || measure == null || !Number.isFinite(measure.value)}
                onConfirm={this.onConfirm} onCancel={this.cancelEditing}
              />
            ))
            .exhaustive()
          }
        </div>
        {rect && mode === 'displaying' && !disabled && hasUnits(measure) &&
          <div
            className="click-to-edit"
            style={{ width: `calc(${rect.width}px + var(--value-separator-margin) + var(--input-line-padding))` }}
            onClick={() => this.beginEditing(measure)}
          />
        }
      </InputWithUnits>);
  }
}
