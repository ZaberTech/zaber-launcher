import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, HeaderCard, HelpTooltip, Icons, Loader, NoticeBanner, Text, SimpleSelect } from '@zaber/react-library';
import _ from 'lodash';

import { useActions } from '../utils';
import { Disappearing, Shortcut } from '../components';

import { selectPreset, selectPresets, selectAppliedPresetMatches } from './selectors';
import { actions as actionsDefinition } from './actions';
import { OpenPresetsDialog } from './PresetEditor';
import { useMonitoredValue } from './hooks';
import type { Preset } from './presets';

const Help: React.FC<{ presets: Preset[] }> = ({ presets }) => (<HelpTooltip>
  <Text t={Text.Type.Body}>
    Switch between presets to quickly apply a set of settings.<br/>
    Use Ctrl+[1-9] to apply a preset.<br/>
    {presets.map((preset, i) => <div key={i}>Ctrl+{i + 1}: {preset.name}</div>)}
  </Text>
</HelpTooltip>);

export const PresetsPanel: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const presets = useSelector(selectPresets);
  const { current: currentBeforeRefresh, error, applying } = useSelector(selectPreset);
  const [current, setCurrent] = useMonitoredValue(currentBeforeRefresh, applying);

  const appliedMatchesBeforeRefresh = useSelector(selectAppliedPresetMatches);
  const [appliedMatches, setAppliedMatches] = useMonitoredValue(appliedMatchesBeforeRefresh, applying);

  const [lastConfirmedMessage, setLastConfirmedMessage] = useState(current);

  const applyPreset = (preset: number) => {
    setAppliedMatches(true);
    setCurrent(preset);
    actions.applyPreset(preset);
  };

  let helper: React.ReactNode;
  if (applying) {
    helper = <><Loader/>&ensp;Applying...</>;
  } else if (!appliedMatches) {
    helper = <>The preset has additional changes.&ensp;
      <Button size="small" color="grey" onClick={() => applyPreset(current ?? 0)}>
        Restore <Icons.Restore title="Restore"/>
      </Button>
    </>;
  } else if (current != null && lastConfirmedMessage !== current) {
    helper = <Disappearing timeout={3000} onDisappear={() => setLastConfirmedMessage(current)}>
      <Icons.Confirmation className="confirm"/>&ensp;Applied.
    </Disappearing>;
  }

  const presetOptions = presets.map((preset, i) => ({ value: i, label: preset.name }));

  return <>
    <HeaderCard
      className="controls presets"
      header={<>
        <Text t={Text.Type.H4}>Presets</Text>
        <div className="icons">
          <Help presets={presets}/>
        </div>
      </>}>
      <div className="select-row">
        <SimpleSelect
          disabled={applying}
          placeholder={presetOptions.length > 0 ? 'Select preset' : 'No presets'}
          value={current}
          onValueChange={applyPreset}
          options={presetOptions}
          portalToBody/>
        {helper && <Text className="status-text">{helper}</Text>}
        <OpenPresetsDialog/>
      </div>
      {error && <NoticeBanner type="error">
        {error}
      </NoticeBanner>}
    </HeaderCard>
    {_.take(presets, 9).map((_, i) =>
      <Shortcut key={i} disabled={applying} ctrl k={`${i + 1}`} onDown={() => applyPreset(i)}/>)}
  </>;
};
