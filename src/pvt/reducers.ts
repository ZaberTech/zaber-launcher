import _ from 'lodash';
import { Time, Units } from '@zaber/motion';
import { combineReducers } from 'redux';
import undoable, { includeAction, StateWithHistory } from 'redux-undo';

import { changeDictionary, createReducer, ensureArray } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import { Bug, CsvImportOptions, DeviceSelection, Message, Path, Point, Span, ViewMode } from './types';
import { deletePoint, getAccelerationScale, getVelocityScale, insertPoint } from './pvtSeriesHelper';
import { PvtSegment } from './pvtSegment';


export interface AppState {
  viewMode: ViewMode;
  filePath: string | null;
  prevFilePath: string | null;
  lastSavedContent: Path | null;
  errors: Message[];

  importOptions: CsvImportOptions;
  deviceSelectionDialog: {
    deviceDialogVisible: boolean;
    axisDialogVisible: boolean;
    showBackButton: boolean;
    pendingOptions: DeviceSelection;
  };
  bufferLoadDialog: {
    visible: boolean;
    buffers: string[] | null;
    bufferAxes: Record<string, number>;
  };
  deviceSelections: DeviceSelection;
  trajectoryExportDialogVisible: boolean;

  devicePath: Path | null; // Contains discretized path plus limits, range and acceleration data.
  fidelity: 'low' | 'high';
  startPosition: number[]; // Native units, in axis order.
  selectedPoint: number | null;
  visibleSeries: Record<string, boolean>;
  posLimits: (Span | null)[]; // All limits are in device units. Arrays are in device axis order with entries for missing axes.
  velLimits: (Span | null)[];
  accelLimits: (Span | null)[];
  devicePosition: number[]; // Path position units, in axis order.
  devicePositionNative: number[];
  playbackTime: number | null;
  bugs: Bug[];

  drawingAxes: number[]; // Used by curve view only.
  timeSeriesVisible: boolean[];

  exportInProgress: boolean;

  defaultPolling: number | 'stop';
}


export type UndoableState = StateWithHistory<Path | null>;

export interface State {
  app: AppState;
  userPath: UndoableState;   // Undoable part. Contains only path data from the opened file.
}


const initialState: State = {
  app: {
    viewMode: ViewMode.Welcome,
    filePath: null,
    prevFilePath: null,
    lastSavedContent: null,
    errors: [],

    importOptions: {
      absoluteTime: false,
      autoReload: true,
      allowAbsoluteTime: false,
      dialogVisible: false,
      detectedPositionUnit: Units.NATIVE,
      detectedVelocityUnit: Units.NATIVE,
      detectedTimeUnit: Time.MILLISECONDS,
      selectedPositionUnit: Units.NATIVE,
      selectedVelocityUnit: Units.NATIVE,
      selectedTimeUnit: Time.MILLISECONDS,
    },

    deviceSelectionDialog: {
      deviceDialogVisible: false,
      axisDialogVisible: false,
      showBackButton: false,
      pendingOptions: {
        deviceKey: null,
        axes: {},
      },
    },
    bufferLoadDialog: {
      visible: false,
      buffers: null,
      bufferAxes: {},
    },
    deviceSelections: {
      deviceKey: null,
      axes: {},
    },
    trajectoryExportDialogVisible: false,

    devicePath: null,
    fidelity: 'high',
    startPosition: [],
    selectedPoint: null,
    visibleSeries: {},
    posLimits: [],
    velLimits: [],
    accelLimits: [],
    devicePosition: [],
    devicePositionNative: [],
    playbackTime: null,
    bugs: [],

    drawingAxes: [],
    timeSeriesVisible: [],

    exportInProgress: false,

    defaultPolling: 'stop',
  },
  userPath: {
    past: [],
    present: null,
    future: [],
  },
};


const setAutoReload = (state: AppState, { autoReload }: ActionsToPayloads[ActionTypes.SET_AUTO_RELOAD]): AppState => ({
  ...state,
  importOptions: {
    ...state.importOptions,
    autoReload,
  }
});


const showErrors = (state: AppState, { errors }: ActionsToPayloads[ActionTypes.SHOW_ERRORS]): AppState => ({
  ...state,
  errors: errors != null ? ensureArray(errors) : [],
});


const setCsvImportDialogOptions =
  (state: AppState, { options }: ActionsToPayloads[ActionTypes.SET_CSV_IMPORT_OPTIONS]): AppState => ({
    ...state,
    importOptions: {
      ...state.importOptions,
      ...options,
    },
  });


const setCurrentFile = (state: AppState, { path }: ActionsToPayloads[ActionTypes.SET_CURRENT_FILE]): AppState => ({
  ...state,
  filePath: path,
  prevFilePath: state.filePath,
});


const setLastSavedContent = (state: AppState, { path }: ActionsToPayloads[ActionTypes.FILE_SET_LAST_SAVED]): AppState => ({
  ...state,
  lastSavedContent: path,
});


const defaultVisibleAxes = (state: AppState, { path }: ActionsToPayloads[ActionTypes.STORE_USER_PATH]): AppState => ({
  ...state,
  drawingAxes: _.range(0, path.series.length),
  timeSeriesVisible: Array(path.series.length).fill(true),
});


const storeDevicePath = (state: AppState, { path }: ActionsToPayloads[ActionTypes.STORE_DEVICE_PATH]): AppState => ({
  ...state,
  devicePath: path,
});


const setSelection = (state: AppState, { index }: ActionsToPayloads[ActionTypes.SET_SELECTION]): AppState => ({
  ...state,
  selectedPoint: index,
});


const setSeriesVisible = (state: AppState, { series, visible }: ActionsToPayloads[ActionTypes.SET_SERIES_DETAILS_VISIBLE]): AppState => ({
  ...state,
  visibleSeries: {
    ...state.visibleSeries,
    [series]: visible,
  },
});


const setViewMode = (state: AppState, { mode }: ActionsToPayloads[ActionTypes.SET_VIEW_MODE]): AppState => ({
  ...state,
  viewMode: mode,
});


const setDrawingAxis = (state: AppState, { axis, series }: ActionsToPayloads[ActionTypes.SET_DRAWING_AXIS]): AppState => {
  const drawingAxes = [...state.drawingAxes];
  drawingAxes[axis] = series;
  const available = _.without(_.range(0, state.drawingAxes.length), ...drawingAxes);
  for (let i = 0; i < drawingAxes.length; i++) {
    if (i !== axis && drawingAxes[i] === series) {
      drawingAxes[i] = available.shift()!;
    }
  }

  return {
    ...state,
    drawingAxes,
  };
};


const setTimeSeriesVisible = (state: AppState, { index, visible }: ActionsToPayloads[ActionTypes.SET_TIME_SERIES_VISIBLE]): AppState => ({
  ...state,
  timeSeriesVisible: state.timeSeriesVisible.map((v, i) => i === index ? visible : v),
});


const setTrajectoryExportDialogVisible =
  (state: AppState, { visible }: ActionsToPayloads[ActionTypes.SET_TRAJECTORY_EXPORT_DIALOG_VISIBLE]): AppState => ({
    ...state,
    trajectoryExportDialogVisible: visible,
  });


const setDeviceSelectionDialogVisible =
  (state: AppState, { visible }: ActionsToPayloads[ActionTypes.SET_DEVICE_SELECTION_DIALOG_VISIBLE]): AppState => ({
    ...state,
    deviceSelectionDialog: {
      ...state.deviceSelectionDialog,
      deviceDialogVisible: visible,
    },
  });


const setAxisDialogVisible =
  (state: AppState, { visible, showBackButton }: ActionsToPayloads[ActionTypes.SET_AXIS_DIALOG_VISIBLE]): AppState => ({
    ...state,
    deviceSelectionDialog: {
      ...state.deviceSelectionDialog,
      axisDialogVisible: visible,
      showBackButton,
    },
  });


const selectDevice = (state: AppState, { deviceKey }: ActionsToPayloads[ActionTypes.SELECT_DEVICE]): AppState => ({
  ...state,
  deviceSelectionDialog: {
    ...state.deviceSelectionDialog,
    pendingOptions: {
      deviceKey,
      axes: deviceKey === state.deviceSelectionDialog.pendingOptions.deviceKey ? state.deviceSelectionDialog.pendingOptions.axes : {},
    },
  },
  bufferLoadDialog: {
    ...state.bufferLoadDialog,
    bufferAxes: {},
  }
});


const selectAxes = (state: AppState, { axes }: ActionsToPayloads[ActionTypes.SELECT_AXES]): AppState => ({
  ...state,
  deviceSelectionDialog: {
    ...state.deviceSelectionDialog,
    pendingOptions: {
      ...state.deviceSelectionDialog.pendingOptions,
      axes,
    },
  }
});


const commitDeviceSelection = (state: AppState): AppState => ({
  ...state,
  deviceSelections: _.cloneDeep(state.deviceSelectionDialog.pendingOptions),
});


const clearAxisSelection = (state: AppState): AppState => ({
  ...state,
  deviceSelections: {
    ...state.deviceSelections,
    axes: {},
  }
});


const storeAxisSelections = (state: AppState, { selections }: ActionsToPayloads[ActionTypes.STORE_AXIS_SELECTIONS]): AppState => ({
  ...state,
  deviceSelections: {
    ...state.deviceSelections,
    axes: selections,
  }
});


const setDeviceLimits =
(state: AppState, { position, velocity, acceleration }: ActionsToPayloads[ActionTypes.SET_DEVICE_LIMITS]): AppState => ({
  ...state,
  posLimits: position,
  velLimits: velocity,
  accelLimits: acceleration,
});


const setSeriesLimits =
  (state: AppState, { seriesIndex, posLimits, velLimits, accelLimits }: ActionsToPayloads[ActionTypes.SET_SERIES_LIMITS]): AppState => ({
    ...state,
    devicePath: state.devicePath ? {
      ...state.devicePath,
      series: state.devicePath.series.map((series, i) => (i === seriesIndex) ? ({
        ...series,
        positionLimits: posLimits ?? undefined,
        velocityLimits: velLimits ?? undefined,
        accelerationLimits: accelLimits ?? undefined,
      }) : series),
    } : null,
  });


const storeDevicePosition =
  (state: AppState, { pos, native, playbackTime }: ActionsToPayloads[ActionTypes.STORE_DEVICE_POSITION]): AppState => ({
    ...state,
    devicePosition: pos,
    devicePositionNative: native,
    playbackTime,
  });


const storeStartPosition = (state: AppState, { startPosition }: ActionsToPayloads[ActionTypes.STORE_START_POSITION]): AppState => ({
  ...state,
  startPosition,
});


const storeBugs = (state: AppState, { bugs }: ActionsToPayloads[ActionTypes.STORE_BUGS]): AppState => ({
  ...state,
  bugs,
});


const setExportInProgress = (state: AppState, { exporting }: ActionsToPayloads[ActionTypes.SET_EXPORT_IN_PROGRESS]): AppState => ({
  ...state,
  exportInProgress: exporting,
});


const setBufferLoadDialogVisible =
  (state: AppState, { visible }: ActionsToPayloads[ActionTypes.SET_BUFFER_LOAD_DIALOG_VISIBLE]): AppState => ({
    ...state,
    bufferLoadDialog: {
      ...state.bufferLoadDialog,
      visible,
      buffers: [],
    }
  });


const storeBufferList = (state: AppState, { buffers }: ActionsToPayloads[ActionTypes.STORE_BUFFER_LIST]): AppState => ({
  ...state,
  bufferLoadDialog: {
    ...state.bufferLoadDialog,
    buffers,
  }
});


const storeBufferAxisCount =
  (state: AppState, { bufferId, axisCount }: ActionsToPayloads[ActionTypes.STORE_BUFFER_AXIS_COUNT]): AppState => ({
    ...state,
    bufferLoadDialog: {
      ...state.bufferLoadDialog,
      bufferAxes: changeDictionary(state.bufferLoadDialog.bufferAxes, bufferId, () => axisCount, () => axisCount),
    }
  });


const appReducer = createReducer<ActionsToPayloads, typeof initialState.app>({
  [ActionTypes.SET_AUTO_RELOAD]: setAutoReload,
  [ActionTypes.SHOW_ERRORS]: showErrors,
  [ActionTypes.SET_CSV_IMPORT_OPTIONS]: setCsvImportDialogOptions,
  [ActionTypes.SET_CURRENT_FILE]: setCurrentFile,
  [ActionTypes.FILE_SET_LAST_SAVED]: setLastSavedContent,
  [ActionTypes.STORE_USER_PATH]: defaultVisibleAxes,
  [ActionTypes.SET_SELECTION]: setSelection,
  [ActionTypes.SET_SERIES_DETAILS_VISIBLE]: setSeriesVisible,
  [ActionTypes.SET_VIEW_MODE]: setViewMode,
  [ActionTypes.SET_DRAWING_AXIS]: setDrawingAxis,
  [ActionTypes.SET_FIDELITY]: (state, { fidelity }) => ({ ...state, fidelity }),
  [ActionTypes.STORE_DEVICE_PATH]: storeDevicePath,
  [ActionTypes.SET_TIME_SERIES_VISIBLE]: setTimeSeriesVisible,
  [ActionTypes.SET_TRAJECTORY_EXPORT_DIALOG_VISIBLE]: setTrajectoryExportDialogVisible,
  [ActionTypes.SET_DEVICE_SELECTION_DIALOG_VISIBLE]: setDeviceSelectionDialogVisible,
  [ActionTypes.SET_AXIS_DIALOG_VISIBLE]: setAxisDialogVisible,
  [ActionTypes.SELECT_DEVICE]: selectDevice,
  [ActionTypes.SELECT_AXES]: selectAxes,
  [ActionTypes.COMMIT_DEVICE_SELECTION]: commitDeviceSelection,
  [ActionTypes.CLEAR_AXIS_SELECTION]: clearAxisSelection,
  [ActionTypes.SET_DEVICE_LIMITS]: setDeviceLimits,
  [ActionTypes.SET_SERIES_LIMITS]: setSeriesLimits,
  [ActionTypes.STORE_DEVICE_POSITION]: storeDevicePosition,
  [ActionTypes.STORE_START_POSITION]: storeStartPosition,
  [ActionTypes.STORE_BUGS]: storeBugs,
  [ActionTypes.SET_EXPORT_IN_PROGRESS]: setExportInProgress,
  [ActionTypes.SET_POLL_DEFAULT]: (state, { interval }) => ({ ...state, defaultPolling: interval }),

  [ActionTypes.SET_BUFFER_LOAD_DIALOG_VISIBLE]: setBufferLoadDialogVisible,
  [ActionTypes.STORE_BUFFER_LIST]: storeBufferList,
  [ActionTypes.STORE_BUFFER_AXIS_COUNT]: storeBufferAxisCount,
  [ActionTypes.STORE_AXIS_SELECTIONS]: storeAxisSelections,
}, initialState.app);


const storeStartPositionFromDevice =
  (state: Path | null, { pos, axisSelections }: ActionsToPayloads[ActionTypes.STORE_RELATIVE_START]): Path | null => {
    let path = state;
    if (path) {
      path = {
        ...path,
        series: path.series.map(series => ({
          ...series,
          points: series.points.map((point, i) => (i === 0 && series.name in axisSelections) ? ({
            ...point,
            pos: pos[axisSelections[series.name]],
          }) : point),
        })),
      };
    }

    return path;
  };


function recalculateAccel(path: Path, prevPoint: Point, nextPoint: Point): number {
  const velocityScale = getVelocityScale(path.positionUnit, path.velocityUnit);
  const accelScale = getAccelerationScale(path.positionUnit, path.velocityUnit);
  const segment = new PvtSegment(prevPoint, nextPoint, velocityScale, accelScale);
  return segment.getAccel(nextPoint.dt);
}


const editSegmentTime = (state: Path | null, { index, value }: ActionsToPayloads[ActionTypes.EDIT_SEGMENT_TIME]): Path | null => {
  if (!state) {
    return state;
  }

  const path = {
    ...state,
    series: state.series.map(series => ({
      ...series,
      points: series.points.map((point, i) => i < index ? point : ({ ...point })),
    })),
  };

  path.series.forEach(series => {
    let time = (index > 0) ? series.points[index - 1].time : 0;
    series.points[index].dt = value;
    if (index > 0) {
      series.points[index].accel = recalculateAccel(state, series.points[index - 1], series.points[index]);
    }

    for (let i = index; i < series.points.length; i++) {
      time += series.points[i].dt;
      series.points[i].time = time;
    }
  });

  return path;
};


const editPointPosition = (state: Path | null, { index, values }: ActionsToPayloads[ActionTypes.EDIT_POSITION]): Path | null => {
  if (!state) {
    return state;
  }

  const path: Path = {
    ...state,
    series: state.series.map(series => ({
      ...series,
      points: series.points.map((point, i) => {
        if (i === index) {
          const newValue = values[series.name];
          if (newValue !== undefined) {
            const newPoint = {
              ...point,
              pos: newValue,
            };

            if (index > 0) {
              newPoint.accel = recalculateAccel(state, series.points[index - 1], newPoint);
            }

            return newPoint;
          }
        }

        return point;
      }),
    })),
  };

  return path;
};


const editPointVelocity = (state: Path | null, { index, values }: ActionsToPayloads[ActionTypes.EDIT_VELOCITY]): Path | null => {
  if (!state) {
    return state;
  }


  const path: Path = {
    ...state,
    series: state.series.map(series => ({
      ...series,
      points: series.points.map((point, i) => {
        if (i === index) {
          const newValue = values[series.name];
          if (newValue !== undefined) {
            const newPoint = {
              ...point,
              vel: newValue,
            };

            if (index > 0) {
              newPoint.accel = recalculateAccel(state, series.points[index - 1], newPoint);
            }

            return newPoint;
          }
        }

        return point;
      }),
    })),
  };

  return path;
};


const addPoint = (state: Path | null, { index, dt }: ActionsToPayloads[ActionTypes.ADD_POINT]): Path | null => {
  if (!state) {
    return state;
  }

  return {
    ...state,
    series: state.series.map(series => insertPoint(series, index, dt, state.positionUnit, state.velocityUnit)),
  };
};


const removePoint = (state: Path | null, { index }: ActionsToPayloads[ActionTypes.DELETE_POINT]): Path | null => {
  if (!state) {
    return state;
  }

  return {
    ...state,
    series: state.series.map(series => deletePoint(series, index, state.positionUnit, state.velocityUnit)),
  };
};

const autoCorrect = (state: Path | null): Path | null => state && {
  ...state,
  series: state.series.map(series => {
    const points = series.points.map((oldPoint, i) => {
      let point = oldPoint;
      if (i === 0 && point.time !== 0) {
        point = { ...point, time: 0, dt: 0 };
      }
      if ((i === 0 || i === series.points.length - 1) && point.vel !== 0) {
        point = { ...point, vel: 0 };
      }
      return point;
    });
    return {
      ...series,
      points,
    };
  }),
};

const editReducer = createReducer<ActionsToPayloads, Path | null>({
  [ActionTypes.STORE_USER_PATH]: (_, { path }) => path,
  [ActionTypes.STORE_RELATIVE_START]: storeStartPositionFromDevice,
  [ActionTypes.EDIT_SEGMENT_TIME]: editSegmentTime,
  [ActionTypes.EDIT_POSITION]: editPointPosition,
  [ActionTypes.EDIT_VELOCITY]: editPointVelocity,
  [ActionTypes.ADD_POINT]: addPoint,
  [ActionTypes.DELETE_POINT]: removePoint,
  [ActionTypes.AUTO_CORRECT]: autoCorrect,
}, initialState.userPath.present);


export const reducer = combineReducers<State>({
  app: appReducer,
  userPath: undoable(editReducer, {
    limit: 10,
    undoType: ActionTypes.UNDO,
    redoType: ActionTypes.REDO,
    clearHistoryType: ActionTypes.CLEAR_UNDO_HISTORY,
    filter: includeAction(ActionTypes.COMMIT_EDIT), // Explicitly control history generation in code.
  }),
});
