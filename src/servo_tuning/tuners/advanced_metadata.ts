import type { AdvancedTuning, ParamValue } from '../types';

type AdvancedTuningMetaData = {
  paramName: string;
  description: string;
  highSpeedVariant?: string;
};

export const advancedTuningMetadata: AdvancedTuningMetaData[] = [
  { description: 'Velocity Feedforward', paramName: 'finv1' },
  { description: 'Acceleration Feedforward', paramName: 'finv2' },
  { description: 'Feedforward Filter, First Order', paramName: 'finvfc1' },
  { description: 'Feedforward Filter, Second Order', paramName: 'finvfc2' },
  { description: 'Static Friction Feedforward, Reversing', paramName: 'finvr' },
  { description: 'Static Friction Feedforward', paramName: 'finvst' },
  { description: 'Static Friction Feedforward Distance', paramName: 'finvstx' },
  { description: 'Smoothing Decay Rate', paramName: 'fcla', highSpeedVariant: 'fcla.hs' },
  { description: 'Smoothing Threshold', paramName: 'fclg', highSpeedVariant: 'fclg.hs' },
  { description: 'Integral Gain', paramName: 'ki', highSpeedVariant: 'ki.hs' },
  { description: 'Proportional Gain', paramName: 'kp', highSpeedVariant: 'kp.hs' },
  { description: 'Derivative Gain', paramName: 'kd', highSpeedVariant: 'kd.hs' },
  { description: 'Feedback Filter, First Order', paramName: 'fc1', highSpeedVariant: 'fc1.hs' },
  { description: 'Feedback Filter, Second Order', paramName: 'fc2', highSpeedVariant: 'fc2.hs' },
  { description: 'Global Gain', paramName: 'gain' },
];

/** Used to check if a given param is in the metadata list */
const knownParams = advancedTuningMetadata.reduce<Record<string, true>>(
  (accumulator, { paramName, highSpeedVariant }) => (
    highSpeedVariant == null ? { ...accumulator, [paramName]: true } : { ...accumulator, [paramName]: true, [highSpeedVariant]: true }
  ),
  {},
);

export type SortedAdvancedParams = {
  described: {
    description: string;
    paramName: string;
    param: ParamValue;
    highSpeedParam?: ParamValue;
  }[];
  undescribed: ParamValue[];
  raw: AdvancedTuning;
};

export function sortAdvancedParams(params: AdvancedTuning): SortedAdvancedParams {
  const sorted: SortedAdvancedParams = {
    described: [],
    undescribed: [],
    raw: params,
  };

  for (const meta of advancedTuningMetadata) {
    const hs = meta.highSpeedVariant;
    sorted.described.push({
      ...meta,
      param: { name: meta.paramName, value: params[meta.paramName] },
      highSpeedParam: hs ? { name: hs, value: params[hs] } : undefined,
    });
  }

  for (const [name, value] of Object.entries(params)) {
    if (!knownParams[name]) {
      sorted.undescribed.push({ name, value });
    }
  }

  return sorted;
}
