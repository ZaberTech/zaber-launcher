import React from 'react';
import { render } from '@testing-library/react';

import { SearchMatch } from './SearchMatch';

function renderText(el: JSX.Element) {
  const wrapper = render(el);
  const text = wrapper.baseElement.lastElementChild!.innerHTML;
  wrapper.unmount();
  return text;
}

test('highlights all matches (case ignored)', () => {
  expect(renderText(<SearchMatch search="stuff">There is Stuff, a lot of stuFF.</SearchMatch>)).toBe(
    'There is <em>Stuff</em>, a lot of <em>stuFF</em>.');
});

test('highlights matches at edges', () => {
  expect(renderText(<SearchMatch search="stuff">Stuff at the beginning and at the end stuff</SearchMatch>)).toBe(
    '<em>Stuff</em> at the beginning and at the end <em>stuff</em>');
});

test('does not highlight nested matches', () => {
  expect(renderText(<SearchMatch search="popo">PoPoPoPoPo</SearchMatch>)).toBe(
    '<em>PoPo</em><em>PoPo</em>Po');
});
