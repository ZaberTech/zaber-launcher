export { reducer as lc40SetupReducer } from './reducer';
export type { State as LC40SetupState } from './reducer';
export {
  actions as lc40SetupActions,
  ActionTypes as LC40SetupActionTypes,
} from './actions';
export type {
  ActionsToPayloads as LC40SetupActionPayloads,
} from './actions';
export { rootSaga as lc40SetupSaga } from './sagas';
export { LC40Setup } from './LC40Setup';
