import { parsedManual } from './profile';

const SETTING_ID_PREFIX = 'topic_setting_';

export function generateSettingIdList()
  : string[] {
  const body = parsedManual();
  return Array.from(body.querySelectorAll(`[id*="${SETTING_ID_PREFIX}"]`))
    .filter(node =>
      node.classList.contains('reference') && !node.hasAttribute('data-disclosure')
    ).map(node =>
      node.id.substring(SETTING_ID_PREFIX.length));
}
