import { all, takeEvery, select, call, take, race, SagaReturnType as SRT, put, takeLatest, delay } from 'redux-saga/effects';
import type { SagaIterator } from 'redux-saga';
import { ascii, gcode, Units } from '@zaber/motion';
import _ from 'lodash';
import { throwUnexpectedError, tryAccess } from '@zaber/toolbox';

import { getContainer } from '../container';
import { Storage } from '../app_components';
import type { Action, RT, SagaIter } from '../utils';
import { takeDevicesLoaded, getAxis } from '../connection_manager';
import { extractConnectionKey } from '../keys';
import { getDefaultDimensionUnits, getMovementDimensions } from '../units';
import { environment } from '../environment';

import { ActionTypes, ActionsToPayloads, actions } from './actions';
import { AxisStateComplete, selectAxes, selectAxesInOrder, selectDemoCode } from './selectors';
import { DEFAULT_PROGRAM } from './types';

export function* rootSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.MOUNT, mount),
    takeEvery(ActionTypes.START_AXIS, startAxis),
    takeLatest(ActionTypes.START_ALL, startAll),
    takeLatest(ActionTypes.UPDATE_SETTINGS, updateSettings),
  ]);
}

export const CONTENT_STORAGE_KEY = 'demo_content';

function* mount(): SagaIterator {
  const storage = getContainer().get(Storage);
  const demoCode = storage.load<string>(CONTENT_STORAGE_KEY) ?? DEFAULT_PROGRAM;
  yield put(actions.updateSettings({ demoCode }));
}

function updateSettings({ payload }: Action<ActionsToPayloads[ActionTypes.UPDATE_SETTINGS]>) {
  const storage = getContainer().get(Storage);
  if (payload.demoCode != null) {
    storage.save(CONTENT_STORAGE_KEY, payload.demoCode);
  }
}

function* startAxis({ payload: { key } }: Action<ActionsToPayloads[ActionTypes.START_AXIS]>): SagaIter {
  try {
    const axes: ReturnType<typeof selectAxes> = yield select(selectAxes);
    const axisState = axes[key];
    const axis: SRT<typeof getAxis> = yield call(getAxis, key);

    yield race({
      end: call(cycle, axisState, axis),
      stop: take((a: Partial<Action<ActionsToPayloads[ActionTypes.STOP_AXIS]>>) =>
        a.type === ActionTypes.STOP_AXIS && a.payload?.key === key),
      stopAll: take(ActionTypes.STOP_ALL),
      reload: takeDevicesLoaded(extractConnectionKey(key)),
    });

    yield axis.stop();
    yield put(actions.axisStopped(key));
  } catch (err) {
    throwUnexpectedError(err);
    const message = err.message ?? String(err);
    yield put(actions.axisStopped(key, message));
  }
}

interface Info {
  posUnits: Units;
  velocityUnits: Units;
  minTravel: number;
  maxTravel: number;
  maxSpeed: number;
}

async function getInfo(axis: SRT<typeof getAxis>): Promise<Info> {
  const dimensions = getMovementDimensions(axis.axisType);
  const posUnits = dimensions != null ? getDefaultDimensionUnits(dimensions.position) : Units.NATIVE;
  const velocityUnits = dimensions != null ? getDefaultDimensionUnits(dimensions.velocity) : Units.NATIVE;

  const minTravel = await axis.settings.get('limit.min', posUnits);
  const maxTravel = await axis.settings.get('limit.max', posUnits);
  const maxSpeed = await axis.settings.get('maxspeed', velocityUnits);

  return {
    posUnits,
    velocityUnits,
    minTravel,
    maxTravel,
    maxSpeed,
  };
}

function transformLine(line: string, info: Info): string {
  line = line.replace(/\s/g, '');
  line = line.replace(/X([0-9.]+)%/g, (substring, value) => `X${info.minTravel + value / 100 * (info.maxTravel - info.minTravel)}`);
  line = line.replace(/F([0-9.]+)%/g, (substring, value) => `F${value / 100 * info.maxSpeed * 60}`); // mm per minute
  return line;
}

function* cycle(axisState: AxisStateComplete, axis: SRT<typeof getAxis>): SagaIter {
  const info: SRT<typeof getInfo> = yield call(getInfo, axis);

  const movable = axisState.lockstep ? axis.device.getLockstep(axisState.lockstep.groupNumber) : axis;
  yield movable.stop();

  const flags: SRT<typeof axis.warnings.clearFlags> = yield axis.warnings.clearFlags();
  if (flags.has(ascii.WarningFlags.NO_REFERENCE_POSITION)) {
    yield movable.home();
  }

  const stream = axis.device.streams.getStream(axis.axisNumber);

  yield stream.disable();
  const streamAxes = axisState.lockstep ? {
    axisNumber: axisState.lockstep.groupNumber,
    axisType: ascii.StreamAxisType.LOCKSTEP
  } : {
    axisNumber: axis.axisNumber
  };
  yield stream.setupLiveComposite(streamAxes);

  const translator: SRT<typeof gcode.Translator.setup> = yield gcode.Translator.setup(stream);

  for (;;) {
    const program: ReturnType<typeof selectDemoCode> = yield select(selectDemoCode);
    const lines = program.trim().split('\n');
    for (const line of lines) {
      const transformedLine = transformLine(line, info);
      yield translator.translate(transformedLine);
    }

    yield stream.waitUntilIdle();
  }
}

function* startAll(): SagaIter {
  yield race({
    start: call(function* (): SagaIter {
      const axes: ReturnType<typeof selectAxesInOrder> = yield select(selectAxesInOrder);
      for (const axis of _.filter(axes, axis => !axis.running)) {
        const currentState = tryAccess(((yield select(selectAxes)) as RT<typeof selectAxes>), axis.key);
        if (currentState == null || currentState.running) { continue }
        yield put(actions.startAxis(axis.key));
        yield delay(!environment.isTest ? 500 : 0);
      }
    }),
    stopAll: take(ActionTypes.STOP_ALL)
  });
}
