import classNames from 'classnames';
import {
  AnimationClasses,
  Button,
  ButtonNext,
  ButtonPrevious,
  ButtonRow,
  ButtonRowConfirmCancel,
  Card,
  Checkbox,
  HeaderCard,
  HelpTooltip,
  Icons,
  Modal,
  NoticeBorder,
  RadioButton,
  Text,
} from '@zaber/react-library';
import React, { useEffect, useState } from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { match } from 'ts-pattern';
import { Length, Angle } from '@zaber/motion';

import { Thumbnail } from '../components/Thumbnail';
import { axisDescription, peripheralNameWithLabel } from '../connection_manager';
import { NoContentMessage } from '../components';
import { AppIconsNeutral } from '../apps';

import {
  DeviceStateLockstep,
  selectAxesSorted,
  selectCreationModal, selectDeviceStateLockstep,
  selectError,
} from './selectors';
import { actions as actionsDefinition } from './actions';
import { DeviceHeader, ErrorElements } from './LockstepConfiguration';
import type { CreationModalState, LockstepCreateAxisInfo } from './reducer';

const Notice: React.FC = ({ children }) => (<div className="home-info">
  <NoticeBorder type="info">
    <Card>
      <Icons.ErrorNote className="note-icon"/>
      <Text>{children}</Text>
    </Card>
  </NoticeBorder>
</div>);

interface AxisSelectionProps {
  device: DeviceStateLockstep;
  state: Extract<CreationModalState, { step: 'choose'}>;
}
const AxisSelection: React.FC<AxisSelectionProps> = ({ device, state: { axesInfo } }) => {
  const actions = useActions(actionsDefinition);
  const { lockstepped, free } = useSelector(selectAxesSorted);
  const existsNotSupportedAxis = axesInfo.some(a => !a.supported);
  const existsNotHomedAxis = axesInfo.some(a => a.supported && !a.homed);

  const [primary, setPrimary] = useState<LockstepCreateAxisInfo | null>(null);
  const [chosenAxes, setChosenAxes] = useState<LockstepCreateAxisInfo[]>([]);

  return <>
    <div className="scrollable">
      <Text t={Text.Type.H4}>Choose Axes</Text>
      <div className="info-text">
        Choose which axes to include in the lockstep group.<br/>Please mark which axis will be <b>Primary</b>.
        <HelpTooltip>
          <div className="help-tooltip-width-limiter">
            The primary axis is used as the reference that other axes will match their position to.
            A group's displayed position and units will be those of its primary axis.
          </div>
        </HelpTooltip>
      </div>

      <HeaderCard header={<DeviceHeader device={device}/>}>
        <div className="axis-display">
          {lockstepped.map(axis => (
            <div key={axis.axisNumber} className={classNames('axis-row', 'lockstepped', axis.axisNumber)}>
              <span className="axis-number">
                <Text t={Text.Type.H5}>{`Axis ${axis.axisNumber}`}</Text>
              </span>
              <span className="thumbnail">
                <Thumbnail deviceOrPeripheralId={axis.identity.peripheralId} title={axisDescription(axis, device)} altExt="axis"/>
              </span>
              <span className="name-serial">
                <Text t={Text.Type.H5}>{peripheralNameWithLabel(axis)}</Text>
                <br/>
                <Text>{`SN: ${axis.serialNumber}`}</Text>
              </span>
              <span></span>
              <span className="group-number">
                {`Group ${device.locksteps.find(lockstep => lockstep.axisNumbers.includes(axis.axisNumber))?.groupNumber}`}
              </span>
            </div>
          ))}
          {axesInfo.map(axisInfo => {
            const axis = free.find(f => f.axisNumber === axisInfo.axisNumber);
            if (axis == null) { return null }
            const homed = axisInfo.supported && axisInfo.homed;
            return (
              <NoticeBorder key={axis.axisNumber} type={!homed ? 'info' : null}>
                <div className={classNames('axis-row', 'free', { disabled: !homed }, axis.axisNumber)}>
                  <span className="axis-number checkbox">
                    <Checkbox
                      data-testid={`axis-number-checkbox${axis.axisNumber}`}
                      labelContent={`Axis ${axis.axisNumber}`}
                      checked={homed && chosenAxes.some(chosen => chosen.axisNumber === axisInfo.axisNumber)}
                      onChecked={checked => {
                        if (checked) {
                          if (axisInfo.supported) {
                            setChosenAxes([...chosenAxes, axisInfo]);
                          }
                        } else {
                          const isPrimary = primary?.axisNumber === axisInfo.axisNumber;
                          if (!isPrimary) {
                            setChosenAxes(chosenAxes.filter(a => a.axisNumber !== axisInfo.axisNumber));
                          }
                        }
                      }}
                      disabled={!homed}
                      bold={true}>
                    </Checkbox>
                  </span>
                  <span className="thumbnail">
                    <Thumbnail deviceOrPeripheralId={axis.identity.peripheralId} title={axisDescription(axis, device)} altExt="axis"/>
                  </span>
                  <span className="name-serial">
                    <Text t={Text.Type.H5}>{peripheralNameWithLabel(axis)}</Text>
                    <br/>
                    {axis.serialNumber != null && <Text>{`SN: ${axis.serialNumber}`}</Text>}
                  </span>
                  {!homed && <span className="disabled-notice"><Icons.ErrorNote className="note-icon"/>
                    {!axisInfo.supported ? 'Not supported' : 'Home required'}
                  </span>}
                  {homed && <div/>}
                  <span className="primary-radio">
                    <RadioButton
                      data-testid={`primary-radio${axis.axisNumber}`}
                      onValueChange={axisInfo.supported ? () => { setPrimary(axisInfo) } : undefined}
                      checked={primary?.axisNumber === axis.axisNumber}
                      disabled={!homed || !chosenAxes.some(chosen => chosen.axisNumber === axisInfo.axisNumber)}
                    >Primary</RadioButton>
                  </span>
                </div>
              </NoticeBorder>);
          })}
        </div>
      </HeaderCard>
      {existsNotHomedAxis && <Notice>
        Please note that an axis has to be homed to be included in a lockstep group. <br/><br/>
        Use the <Link to="/basic-controls">Basic Controls</Link> application to home axes. <br/><br/>
        Alternatively, if your axes are already mechanically connected, you can manually set the position
        in <Link to="/device-settings">Device Settings</Link>.
        Make sure to correctly represent the offset between the axes (if there is any).
        You should home the entire group after creating it.
      </Notice>}
      {existsNotSupportedAxis && <Notice>
        Only axes with a stepper motor can be included in a lockstep group.
      </Notice>}
    </div>
    <ButtonRow>
      <ButtonNext
        onClick={() => {
          actions.choseCreationAxes(primary!, chosenAxes, axesInfo);
        }}
        disabled={chosenAxes.length < 2 || primary == null}
      >
        Next
      </ButtonNext>
    </ButtonRow>
  </>;
};

interface AxisViewingProps {
  device: DeviceStateLockstep;
  state: Extract<CreationModalState, { step: 'confirm'}>;
}
const AxisViewing: React.FC<AxisViewingProps> = ({ device, state: { primary, chosen, axesInfo } }) => {
  const actions = useActions(actionsDefinition);
  const [showCreatingSpinner, setShowCreatingSpinner] = useState(false);

  return <>
    <div className="info-text">
      You're about to setup this lockstep group.
      Make sure that all axes are correctly offset relative to each other.
      Once the group is created, these offsets cannot be changed.<br/>
      <b>Tip:</b> Consider mounting the axes loosely at this time to assist with the final alignment.
    </div>
    <HeaderCard header={<DeviceHeader device={device}/>}>
      <div className="axis-display">
        {chosen.map(axisInfo => {
          const isPrimary = axisInfo.axisNumber === primary.axisNumber;
          const axis = device.axes.find(a => a.axisNumber === axisInfo.axisNumber)!;
          return <div key={axis.axisNumber} className="axis-row">
            <span className="axis-number">
              <Text t={Text.Type.H5}>{`Axis ${axis.axisNumber}`}</Text>
            </span>
            <Thumbnail deviceOrPeripheralId={axis.identity.peripheralId} title={axisDescription(axis, device)} altExt="axis"/>
            <span>
              <Text t={Text.Type.H5}>{peripheralNameWithLabel(axis)}</Text>
              <br/>
              <Text>{`SN: ${axis.serialNumber}`}</Text>
            </span>
            <span className={classNames(
              'primary-secondary-specific',
              { primary: isPrimary, secondary: !isPrimary }
            )}>
              {match(isPrimary)
                .with(true, () => <><Icons.Enable className="greencheck"/>Primary axis</>)
                .with(false, () =>  {
                  const units = match([primary.position.units, axisInfo.position.units])
                    .with([Length.MILLIMETRES, Length.MILLIMETRES], () => 'mm')
                    .with([Angle.DEGREES, Angle.DEGREES], () => '˚')
                    .otherwise(() => null);
                  if (units == null) {
                    return <div><Text t={Text.Type.H5}>Offset Undefined</Text></div>;
                  }
                  const offset = axisInfo.position.value - primary.position.value;
                  return <div><Text t={Text.Type.H5}>Offset</Text>&ensp;{offset.toFixed(2)}{units}</div>;
                }).exhaustive()
              }
            </span>
          </div>;
        })}
      </div>
    </HeaderCard>
    <ButtonRow justify="spread">
      <ButtonPrevious onClick={() => actions.receivedCreationInfo(axesInfo)}>Back</ButtonPrevious>
      {showCreatingSpinner ?
        <Icons.Refresh className={classNames(AnimationClasses.Rotation, 'bottom-right')}/> :
        <Button
          onClick={() => {
            const secondaryAxisNumbers = chosen.map(c => c.axisNumber).filter(n => n !== primary.axisNumber);
            actions.createLockstepGroup([primary.axisNumber, ...secondaryAxisNumbers]);
            setShowCreatingSpinner(true);
          }}
          title="Enable Lockstep Group"
        >Create Lockstep Group</Button>}
    </ButtonRow>
  </>;
};

interface LockstepCreationSuccessProps {
  state: Extract<CreationModalState, { step: 'complete'}>;
}
const LockstepCreationSuccess: React.FC<LockstepCreationSuccessProps> = ({ state }) => {
  const actions = useActions(actionsDefinition);

  return <>
    <div className="indented">
    Lockstep <b>{`Group ${state.groupNumber}`}</b> has been created successfully.<br/><br/>
    It's safe to physically couple the axes at this time.
    Once the axes are coupled and properly aligned,
    make sure to re-tighten all the axes' fastenings.
    </div>
    <ButtonRow>
      <Button onClick={actions.endLockstepCreation}>OK</Button>
    </ButtonRow>
  </>;
};

const LockstepCreationFailure: React.FC<{ error: string }> = ({ error }) => {
  const actions = useActions(actionsDefinition);

  return <>
    <div className="indented">
      <b>Unable to create lockstep group.</b><br/><br/>
      <ErrorElements error={error}/>
    </div>
    <ButtonRowConfirmCancel cancelText="Try Again" onConfirm={actions.endLockstepCreation} onCancel={actions.beginCreation}/>
  </>;
};

export const CreationModal: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const device = useSelector(selectDeviceStateLockstep);
  const state = useSelector(selectCreationModal);
  const error = useSelector(selectError);

  useEffect(() => {
    const shouldReloadData = state?.step === 'choose';
    if (shouldReloadData) {
      actions.beginCreation();
    }
  }, []);

  if (state == null || device == null) {
    return null;
  }

  const errorModal = !!error;

  return <Modal
    headerIcon={<AppIconsNeutral.LockstepConfiguration/>}
    headerText="Create Lockstep Group"
    className={`step-${state.step}`}
    small={errorModal}
    bodyIcon={errorModal ? 'error' : undefined}
    onRequestClose={actions.endLockstepCreation}
  >
    {match(state)
      .with({ step: 'preparing' }, () => <NoContentMessage message="Getting Axis Data..." type="working"/>)
      .with({ step: 'choose' }, s => <AxisSelection device={device} state={s}/>)
      .with({ step: 'confirm' }, s => <AxisViewing device={device} state={s}/>)
      .with({ step: 'creating' }, () => <NoContentMessage message="Creating the Lockstep Group..." type="working"/>)
      .with({ step: 'complete' }, s => <LockstepCreationSuccess state={s}/>)
      .with({ step: 'error' }, ({ message }) => <LockstepCreationFailure error={message}/>)
      .exhaustive()
    }
  </Modal>;
};
