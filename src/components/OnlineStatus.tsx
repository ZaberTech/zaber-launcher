import { Icons } from '@zaber/react-library';
import React, { DetailedHTMLProps } from 'react';
import classnames from 'classnames';

interface Props extends  DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  online: boolean;
}

export const OnlineStatus: React.FC<Props> = ({ online, className, ...rest }) => (
  <div className={classnames(className, 'online-status', { online })} {...rest}>{online ? 'Online' : 'Offline'}&nbsp;<Icons.Dot/></div>
);
