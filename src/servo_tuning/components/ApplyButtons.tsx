import { ServoTuningParamset } from '@zaber/motion/ascii';
import { Button, ButtonRow, Checkbox, NoticeBanner } from '@zaber/react-library';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { selectIsLiveTuning, selectIsWriting, selectIsStartupParamset } from '../selectors';

interface Props {
  paramset: ServoTuningParamset;
  onClick: (toLive: boolean, toPowerOn: boolean) => void;
}

export const ApplyButtons: React.FC<Props> = ({ paramset, onClick }) => {
  const isLive = useSelector(selectIsLiveTuning);
  const isWriting = useSelector(selectIsWriting);
  const isStartupParamset = useSelector(selectIsStartupParamset);
  const [toLive, setToLive] = useState(paramset !== ServoTuningParamset.LIVE);
  const [toPowerOn, setToPowerOn] = useState(paramset !== ServoTuningParamset.LIVE);

  return <>
    {isLive && <NoticeBanner type="warning">
      Warning: Any changes applied only to the live tuning will be lost when the device is powered down.
      To use a persistent tuning add it to one of the presets.
    </NoticeBanner>}
    <ButtonRow>
      {!isLive && <>
        <Checkbox labelContent="Use as the Live Tuning" checked={toLive} onChecked={checked => setToLive(checked)}/>
        {!isStartupParamset && <Checkbox labelContent="Use at Power On" checked={toPowerOn} onChecked={checked => setToPowerOn(checked)}/>}
      </>}
      <Button disabled={isWriting} onClick={() => onClick(toLive, toPowerOn)}>
        {isWriting ? 'Applying...' : 'Apply Tuning'}
      </Button>
    </ButtonRow>
  </>;
};
