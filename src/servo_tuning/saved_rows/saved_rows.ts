import * as CSV from 'csv/sync';

import type { AdvancedTuning } from '../types';

export type SavedRow = {
  name: string;
  controller: {
    type: string;
    version: number;
  };
  params: AdvancedTuning;
};

export class SavedRowsErrors extends Error {}

/** The type of a row returned by CSV.parse */
type CSVRow = {
  Name: string;
  'Controller Type': string;
  'Controller Version': string;
  [paramName: string]: string | undefined;
};

const requiredColumns = ['Name', 'Controller Type', 'Controller Version'];

type CsvRowsValidation = {
  valid: false; error: string;
} | {
  valid: true; rows: CSVRow[];
};
function validateCSVRows(rows: unknown): CsvRowsValidation {
  if (!Array.isArray(rows) || rows.length <= 0) {
    return { valid: false, error: 'This CSV file is empty.' };
  }
  for (const row of rows) {
    if (typeof row !== 'object') {
      return { valid: false, error: 'Invalid row format.' };
    }
    for (const col of requiredColumns) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      if (typeof row[col] !== 'string') {
        return { valid: false, error: `This CSV does not include required column "${col}"` };
      }
    }
  }
  return { valid: true, rows };
}

function parseRow(row: CSVRow): SavedRow {
  const params: SavedRow['params'] = {};

  for (const paramName in row) {
    if (!requiredColumns.includes(paramName)) {
      const rawValue = row[paramName];
      const value = rawValue ? parseFloat(rawValue) : undefined;
      if (Number.isNaN(value)) {
        throw new SavedRowsErrors(`Could not parse "${rawValue}" into a value for param ${paramName}`);
      }
      params[paramName] = value ?? null;
    }
  }

  const version = parseInt(row['Controller Version'], 10);
  if (Number.isNaN(version)) {
    throw new SavedRowsErrors(`Could not parse "${row['Controller Version']}" into a version number`);
  }

  return {
    name: row.Name,
    controller: {
      type: row['Controller Type'],
      version,
    },
    params,
  };
}

export function parseSavedRows(csvFileContents: string): SavedRow[] {
  const tuning = CSV.parse(csvFileContents, { columns: true });
  const validation = validateCSVRows(tuning);
  if (!validation.valid) {
    throw new SavedRowsErrors(`Could not parse this CSV: ${validation.error}`);
  }

  return validation.rows.map(parseRow);
}

export function generateCsv(rows: SavedRow[]): string {
  const csvRows: CSVRow[] = rows.map((row): CSVRow => {
    const csvRow: CSVRow = {
      'Name': row.name,
      'Controller Type': row.controller.type,
      'Controller Version': `${row.controller.version}`,
    };

    for (const paramName in row.params) {
      csvRow[paramName] = `${row.params[paramName]}`;
    }

    return csvRow;
  });

  return CSV.stringify(csvRows, { header: true });
}
