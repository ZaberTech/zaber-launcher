import React from 'react';
import { Button, Colors, Icons, Modal, Text } from '@zaber/react-library';
import { match } from 'ts-pattern';

import { useActions } from '../utils';
import { AppIconsNeutral } from '../apps';

import { actions as actionDefinitions } from './actions';
import type { Message } from './types';


export const MessageDialog: React.FC<{ messages: Message[] }> = ({ messages }) => {
  const actions = useActions(actionDefinitions);

  return <Modal
    className="message-dialog"
    headerIcon={<AppIconsNeutral.Pvt/>}
    headerText="Messages"
    isOpen
    small
    buttons={<Button className="close-button" onClick={actions.clearError}>Close</Button>}
    onRequestClose={actions.clearError}>
    <div className="messages">
      {messages.map((message, i) => <React.Fragment key={i}>
        {match(message.severity)
          .with('error', () => <Icons.ErrorFault color={Colors.justRed}/>)
          .with('warning', () => <Icons.ErrorWarning color={Colors.yellow}/>)
          .with('info', () => <Icons.ErrorNote color={Colors.blue}/>)
          .exhaustive()}
        <Text>{message.text}</Text>
      </React.Fragment>)}
    </div>
  </Modal>;
};
