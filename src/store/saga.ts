import { all, fork } from 'redux-saga/effects';
import type { SagaIterator } from 'redux-saga';

import { connectionManagerSaga } from '../connection_manager';
import { basicControlsSaga } from '../basic_controls';
import { pollingSaga } from '../polling';
import { basicMovementSaga } from '../basic_movement';
import { ipcSaga } from '../ipc';
import { addConnectionSaga } from '../add_connection';
import { deviceSettingsSaga } from '../device_settings';
import { feedbackSaga } from '../feedback';
import { firmwareUpdateSaga } from '../firmware_upgrade';
import { currentTunerSaga } from '../current_tuner';
import { terminalSaga } from '../terminal';
import { cloudSaga } from '../cloud';
import { updateSaga } from '../updates';
import { localShareSaga } from '../local_share';
import { peripheralsSaga } from '../peripherals';
import { preferencesSaga } from '../preferences';
import { servoTunerSaga } from '../servo_tuning';
import { connectionStatusSaga } from '../connection_status';
import { helpSaga } from '../help';
import { menuSaga } from '../menu';
import { configurationManagerSaga } from '../hardware_modification';
import { lc40SetupSaga } from '../lc40_setup';
import { processControllerSaga } from '../process_controller';
import { protocolManualSaga } from '../protocol_manual';
import { deviceSetupSaga } from '../device_setup';
import { appsSaga } from '../apps';
import { saveLoadSaga } from '../save_load';
import { gcodeSaga } from '../gcode';
import { lockstepConfigurationSaga } from '../lockstep_configuration';
import { demoSaga } from '../demo';
import { deviceIoSaga } from '../device_io';
import { joystickSaga } from '../joystick';
import { factoryResetSaga } from '../factory_reset';
import { oscilloscopeSaga } from '../oscilloscope';
import { unitSaga } from '../units';
import { internalSaga } from '../internal';
import { connectionMonitorSaga } from '../connection_monitor';
import { pvtSaga } from '../pvt';
import { cManagerUiSaga } from '../connection_manager_ui';
import { calibrationSaga } from '../calibration';
import { microscopeSaga } from '../microscope';
import { triggersSaga } from '../triggers/sagas';

export function* rootSaga(): SagaIterator {
  yield all([
    fork(connectionManagerSaga),
    fork(basicControlsSaga),
    fork(pollingSaga),
    fork(basicMovementSaga),
    fork(ipcSaga),
    fork(addConnectionSaga),
    fork(configurationManagerSaga),
    fork(saveLoadSaga),
    fork(deviceSettingsSaga),
    fork(processControllerSaga),
    fork(protocolManualSaga),
    fork(feedbackSaga),
    fork(firmwareUpdateSaga),
    fork(currentTunerSaga),
    fork(terminalSaga),
    fork(cloudSaga),
    fork(updateSaga),
    fork(localShareSaga),
    fork(peripheralsSaga),
    fork(preferencesSaga),
    fork(servoTunerSaga),
    fork(connectionStatusSaga),
    fork(helpSaga),
    fork(menuSaga),
    fork(lc40SetupSaga),
    fork(deviceSetupSaga),
    fork(appsSaga),
    fork(gcodeSaga),
    fork(lockstepConfigurationSaga),
    fork(demoSaga),
    fork(deviceIoSaga),
    fork(joystickSaga),
    fork(factoryResetSaga),
    fork(oscilloscopeSaga),
    fork(unitSaga),
    fork(internalSaga),
    fork(connectionMonitorSaga),
    fork(pvtSaga),
    fork(cManagerUiSaga),
    fork(calibrationSaga),
    fork(microscopeSaga),
    fork(triggersSaga),
  ]);
}
