import React from 'react';
import { HeaderCard } from '@zaber/react-library';

import type { AxisInfo, DeviceInfo } from '../types';
import { EntityHierarchyLabel } from '../../components';

import { Axis } from './Axis';
import { DEVICE_TABLE_CLASS } from './types';

interface Props {
  axis: AxisInfo;
  device: DeviceInfo;
}

export const SingleAxisTable: React.FC<Props> = ({ axis, device, children }) => {
  const header = <EntityHierarchyLabel entityKey={device.key}/>;

  return (<HeaderCard header={header} className="connection-with-device" edge="shadow">
    <div className={DEVICE_TABLE_CLASS}>
      <Axis key={axis.key} device={device} axis={axis}>
        {children}
      </Axis>
    </div>
  </HeaderCard>
  );
};
