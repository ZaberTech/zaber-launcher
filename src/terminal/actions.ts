import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { Command, ConnectionStates, HistoryItem, CommandBatchReply, CommandBatchSettings } from './types';

export enum ActionTypes {
  SELECT_ENTITY = 'TERMINAL_SELECT_ENTITY',
  SET_CONNECTION_STATE = 'TERMINAL_SET_CONNECTION_STATE',
  MONITOR_CONNECTION = 'TERMINAL_MONITOR_CONNECTION',
  SET_COMMAND_INPUT = 'TERMINAL_SET_COMMAND_INPUT',
  SET_COMMAND_HISTORY_OLDER = 'TERMINAL_SET_COMMAND_HISTORY_OLDER',
  SET_COMMAND_HISTORY_NEWER = 'TERMINAL_SET_COMMAND_HISTORY_NEWER',
  SET_SELECTED_SUGGESTION = 'TERMINAL_SET_SELECTED_SUGGESTION',
  SEND_COMMAND = 'TERMINAL_SEND_COMMAND',
  APPEND_TO_HISTORY = 'TERMINAL_APPEND_TO_HISTORY',
  WILL_UNMOUNT = 'TERMINAL_WILL_UNMOUNT',
  CLEAR_CONNECTION_HISTORY = 'TERMINAL_CLEAR_CONNECTION_HISTORY',
  SET_SHOW_EXTERNAL = 'TERMINAL_SET_SHOW_EXTERNAL',
  COMMAND_BATCH_EDIT = 'TERMINAL_COMMAND_BATCH_EDIT',
  COMMAND_BATCH_CLOSE = 'TERMINAL_COMMAND_BATCH_CLOSE',
  COMMAND_BATCH_STOP = 'TERMINAL_COMMAND_BATCH_STOP',
  COMMAND_BATCH_EXECUTE = 'TERMINAL_COMMAND_BATCH_EXECUTE',
  COMMAND_BATCH_DONE = 'TERMINAL_COMMAND_BATCH_DONE',
  COMMAND_BATCH_REPLY = 'TERMINAL_COMMAND_BATCH_REPLY',
  COMMAND_BATCH_SET_SETTINGS = 'TERMINAL_COMMAND_BATCH_SET_SETTINGS',
  GENERATE_COMMAND_LIST = 'TERMINAL_GENERATE_COMMAND_LIST',
  STORE_COMMAND_LIST = 'TERMINAL_STORE_COMMAND_LIST',
}

export interface ActionsToPayloads {
  [ActionTypes.SELECT_ENTITY]: { key: EntityKey | null };
  [ActionTypes.SET_CONNECTION_STATE]: { connection: EntityKey; newState: ConnectionStates };
  [ActionTypes.MONITOR_CONNECTION]: void;
  [ActionTypes.SET_COMMAND_INPUT]: { input: string };
  [ActionTypes.SET_COMMAND_HISTORY_OLDER]: void;
  [ActionTypes.SET_COMMAND_HISTORY_NEWER]: void;
  [ActionTypes.SET_SELECTED_SUGGESTION]: { suggestion: number | null };
  [ActionTypes.SEND_COMMAND]: { command: string };
  [ActionTypes.APPEND_TO_HISTORY]: { connection: EntityKey; messages: Omit<HistoryItem, 'key'>[] };
  [ActionTypes.WILL_UNMOUNT]: void;
  [ActionTypes.CLEAR_CONNECTION_HISTORY]: { connection: EntityKey };
  [ActionTypes.SET_SHOW_EXTERNAL]: { showExternal: boolean };
  [ActionTypes.COMMAND_BATCH_EDIT]: { text?: string };
  [ActionTypes.COMMAND_BATCH_CLOSE]: void;
  [ActionTypes.COMMAND_BATCH_STOP]: void;
  [ActionTypes.COMMAND_BATCH_EXECUTE]: void;
  [ActionTypes.COMMAND_BATCH_DONE]: { error?: string };
  [ActionTypes.COMMAND_BATCH_REPLY]: { replies: CommandBatchReply[] };
  [ActionTypes.COMMAND_BATCH_SET_SETTINGS]: Partial<CommandBatchSettings>;
  [ActionTypes.GENERATE_COMMAND_LIST]: { entityKey: EntityKey };
  [ActionTypes.STORE_COMMAND_LIST]: { entityKey: EntityKey; commandList: Command[] };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  selectEntity: (key: EntityKey | null) => buildAction(ActionTypes.SELECT_ENTITY, { key }),
  setConnectionState: (connection: EntityKey, newState: ConnectionStates) =>
    buildAction(ActionTypes.SET_CONNECTION_STATE, { connection, newState }),
  monitorConnection: () => buildAction(ActionTypes.MONITOR_CONNECTION),
  setCommandInput: (input: string) => buildAction(ActionTypes.SET_COMMAND_INPUT, { input }),
  setCommandHistoryOlder: () => buildAction(ActionTypes.SET_COMMAND_HISTORY_OLDER),
  setCommandHistoryNewer: () => buildAction(ActionTypes.SET_COMMAND_HISTORY_NEWER),
  setSelectedSuggestion: (suggestion: number | null) => buildAction(ActionTypes.SET_SELECTED_SUGGESTION, { suggestion }),
  sendCommand: (command: string) => buildAction(ActionTypes.SEND_COMMAND, { command }),
  appendToHistory: (connection: EntityKey, messages: Omit<HistoryItem, 'key'>[]) => buildAction(
    ActionTypes.APPEND_TO_HISTORY,
    { connection, messages }
  ),
  willUnmount: () => buildAction(ActionTypes.WILL_UNMOUNT),
  clearConnectionHistory: (connection: EntityKey) => buildAction(ActionTypes.CLEAR_CONNECTION_HISTORY, { connection }),
  setShowExternal: (showExternal: boolean) => buildAction(ActionTypes.SET_SHOW_EXTERNAL, { showExternal }),
  commandBatchEdit: (text?: string) => buildAction(ActionTypes.COMMAND_BATCH_EDIT, { text }),
  commandBatchClose: () => buildAction(ActionTypes.COMMAND_BATCH_CLOSE),
  commandBatchExecute: () => buildAction(ActionTypes.COMMAND_BATCH_EXECUTE),
  commandBatchStop: () => buildAction(ActionTypes.COMMAND_BATCH_STOP),
  commandBatchDone: (error?: string) => buildAction(ActionTypes.COMMAND_BATCH_DONE, { error }),
  commandBatchReply: (replies: CommandBatchReply[]) => buildAction(ActionTypes.COMMAND_BATCH_REPLY, { replies }),
  commandBatchSetSettings: (settings: Partial<CommandBatchSettings>) => buildAction(ActionTypes.COMMAND_BATCH_SET_SETTINGS, settings),
  generateCommandList: (entityKey: EntityKey) => buildAction(ActionTypes.GENERATE_COMMAND_LIST, { entityKey }),
  storeCommandList: (entityKey: EntityKey, commandList: Command[]) => buildAction(
    ActionTypes.STORE_COMMAND_LIST, { entityKey, commandList }),
};
