import React, { useMemo } from 'react';
import { Units } from '@zaber/motion';
import { Colors, reactSelect, SimpleSelect } from '@zaber/react-library';
import { ensureArray } from '@zaber/toolbox';
import type { Styles } from 'react-select';

import { dimensionsByName } from './utils';

export interface Props {
  dimensions: string | string[];
  includeNative: boolean;
  /** If this control is disabled while waiting for data */
  disabled?: boolean;
  value: Units;
  onChange: (units: Units) => void;
  /** If this control should allow the user to select new units */
  editable?: boolean;
  title?: string;
  styles?: Styles<SimpleSelect.Option<Units>, false>;
  portalToBody?: boolean;
  borderless?: boolean;
}

export const UnitSelect: React.FC<Props> = ({
  value, onChange, dimensions, includeNative,
  disabled = false, editable = true, title, styles = {},
  portalToBody, borderless
}) => {
  const dimensionDefinitions = ensureArray(dimensions).map(dimension => {
    const dimDef = dimensionsByName[dimension];
    if (!dimDef) {
      throw new Error(`Unknown dimension: ${dimension}`);
    }

    return dimDef;
  });

  const options = [
    ...includeNative ? [{ value: Units.NATIVE, label: 'Native' }] : [],
    ...dimensionDefinitions.map(dimDef => dimDef.units.map(unit => ({ value: unit.id as Units, label: unit.ShortName }))).flat(),
  ];

  const mergedStyles = useMemo(() => {
    if (!borderless) {
      return styles;
    }

    const defaultStyles = reactSelect.getDefaultStyles<SimpleSelect.Option<Units>>({
      control: base => ({
        ...base,
        width: SimpleSelect.calculateWidthFromItems(options, editable),
        height: '2rem',
        border: 'none',
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        background: 'initial'
      }),
      menu: base => ({ ...base, zIndex: 10 }),
      indicatorSeparator: () => ({ display: 'none' }),
      dropdownIndicator: base => ({
        ...base,
        padding: 0,
        display: editable ? 'initial' : 'none',
      }),
      singleValue: base => ({ ...base, color: disabled ? Colors.paleGrey : Colors.zaberGrey }),
    });
    return reactSelect.mergeStyles(defaultStyles, styles);
  }, [styles, editable, disabled, borderless]);

  return (
    <SimpleSelect value={value} onValueChange={onChange} options={options}
      calculateMinWidth={!borderless} title={title} portalToBody={portalToBody}
      disabled={disabled || !editable} styles={mergedStyles} className="unit-select"/>
  );
};
