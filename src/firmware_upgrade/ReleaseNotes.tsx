import React, { useState } from 'react';
import { Modal } from '@zaber/react-library';

import { openExternalLink } from '../desktop';
import { AppIconsNeutral } from '../apps';
import { Version } from '../app_components';


interface Props {
  version: Version;
  trigger: (open: () => void) => void;
}

export function isReleaseNotesLink(version: Version): version is Version & { notes: string } {
  return version.notes?.startsWith('http') ?? false;
}

export const ReleaseNotes: React.FC<Props> = ({ version, trigger }) => {
  const [open, setOpen] = useState(false);

  const openNotes = () => {
    if (isReleaseNotesLink(version)) {
      openExternalLink(version.notes);
    } else {
      setOpen(true);
    }
  };

  return (<>
    {trigger(openNotes)}
    {open && <Modal headerIcon={<AppIconsNeutral.FirmwareUpgrader/>} headerText={version.str} onRequestClose={() => setOpen(false)}>
      {version.notes?.split('\n').map((line, i) => <React.Fragment key={i}>{line}<br/></React.Fragment>)}
    </Modal>}
  </>);
};
