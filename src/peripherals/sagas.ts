import type { SagaIterator } from 'redux-saga';
import { all, takeEvery,  call, race, take, select, delay, put, SagaReturnType, takeLeading } from 'redux-saga/effects';
import {
  ascii, CommandFailedException, ConnectionClosedException, ConnectionFailedException, RequestTimeoutException,
} from '@zaber/motion';
import _ from 'lodash';

import { getContainer } from '../container';
import { connectionManagerActions, getAxis } from '../connection_manager';
import { Action, AsyncReturnType, throwUnexpectedError, takeLeadingUniq } from '../utils';
import { extractConnectionKey } from '../keys';
import { ZaberApi, Logger } from '../app_components';
import { Capabilities, hasCapability } from '../devices';
import { getLogger } from '../log';
import { DEFAULT_POLL_INTERVAL } from '../types';

import type { SupportedPeripheral, WarningFlags } from './types';
import { ActionTypes, ActionsToPayloads, actions } from './actions';
import { selectAxes, AxisStateComplete, selectAxesCount } from './selectors';

let logger: Logger;

export function* peripheralsSaga(): SagaIterator {
  logger = getLogger('peripheralsSaga');

  yield all([
    takeEvery(ActionTypes.WILL_UNMOUNT, willUnmount),
    takeLeading(ActionTypes.DID_MOUNT, monitorAxes),
    takeEvery(ActionTypes.SET_PERIPHERAL_ID, setPeripheralId),
    takeEvery(ActionTypes.ACTIVATE, activate),
    takeLeadingUniq(ActionTypes.FETCH_SUPPORTED_PERIPHERALS, action => action.payload.axisKey, fetchSupportedPeripherals),
  ]);
}

function* willUnmount(): SagaIterator {
  const count: ReturnType<typeof selectAxesCount> = yield select(selectAxesCount);
  if (count === 0) {
    yield put(actions.stopMonitoring());
  }
}

function* monitorAxes(): SagaIterator {
  yield delay(0); // wait for burst of DID_MOUNT before monitoring

  yield race({
    monitor: call(monitorAxesLoop),
    cancel: take(ActionTypes.STOP_MONITORING),
  });
}


function* monitorAxesLoop(): SagaIterator {
  while (true) {
    const axes: ReturnType<typeof selectAxes> = yield select(selectAxes);
    if (axes) {
      yield all(_.map(axes, axis => call(monitorQueryAxis, axis)));
    }

    yield race({
      wait: delay(DEFAULT_POLL_INTERVAL),
      refresh: take([ActionTypes.REFRESH_AXES]),
    });
  }
}

function* monitorQueryAxis(axis: AxisStateComplete): SagaIterator {
  try {
    const asciiAxis: SagaReturnType<typeof getAxis> = yield call(getAxis, axis.key);
    const { settings, warnings } = asciiAxis;

    const peripheralId: number = yield call([settings, settings.get], getPeripheralIdSetting(asciiAxis));
    if (asciiAxis.identity.peripheralId !== peripheralId) {
      yield put(connectionManagerActions.loadDevices(extractConnectionKey(axis.key), false));
      return;
    }

    const warningFlagsSet: AsyncReturnType<typeof warnings.getFlags> = yield call([warnings, warnings.getFlags]);
    const warningFlags: WarningFlags = {};
    for (const flag of warningFlagsSet.keys()) {
      warningFlags[flag] = true;
    }

    let peripheralIdPending = 0;
    if (hasCapability(axis.device, Capabilities.DeviceCanAutodetect)) {
      peripheralIdPending = yield call([settings, settings.get], ascii.SettingConstants.PERIPHERAL_ID_PENDING);
    }

    yield put(actions.axisMonitorData(axis.key, {
      peripheralIdPending,
      warningFlags,
    }));

    const newPeripheralConnected = peripheralIdPending > 0 &&  peripheralId !== peripheralIdPending;
    if (newPeripheralConnected && !axis.supportedPeripherals) {
      yield put(actions.fetchSupportedPeripherals(axis.key));
    }
  } catch (err) {
    throwUnexpectedError(err);
    logger.warn('monitorQueryAxis failed', err);
    yield put(actions.axisMonitorDataErr(axis.key, {
      reason: [
        ConnectionFailedException,
        ConnectionClosedException,
        RequestTimeoutException,
      ].some(ErrType => err instanceof ErrType) ? 'connection' : 'other',
      message: err.message ?? String(err)
    }));
  }
}

function* fetchSupportedPeripherals(
  { payload: { axisKey } }: Action<ActionsToPayloads[ActionTypes.FETCH_SUPPORTED_PERIPHERALS]>
): SagaIterator {
  yield race([
    call(function* () {
      try {
        const axes = ((yield select(selectAxes)) as ReturnType<typeof selectAxes>);
        const { device } = axes[axisKey];
        const api = getContainer().get(ZaberApi);

        const { peripherals }: AsyncReturnType<typeof api.getSupportedPeripherals> = yield call(
          [api, api.getSupportedPeripherals],
          device.product.id,
        );
        const supportedPeripherals = peripherals.map<SupportedPeripheral>(peripheral =>
          ({ name: peripheral.name, peripheralId: peripheral.peripheral_id })
        ).sort((a, b) => a.name.localeCompare(b.name));

        yield put(actions.fetchSupportedPeripheralsDone(axisKey, supportedPeripherals));
      } catch (err) {
        throwUnexpectedError(err);
        yield put(actions.fetchSupportedPeripheralsDoneErr(axisKey, err.message));
        logger.warn('fetchSupportedPeripherals', err);
      }
    }),
    take((action: Partial<Action<ActionsToPayloads[ActionTypes.WILL_UNMOUNT]>>): boolean =>
      action.type === ActionTypes.WILL_UNMOUNT && action.payload?.axisKey === axisKey
    )
  ]);
}

function* setPeripheralId({ payload: { axisKey, peripheralId } }: Action<ActionsToPayloads[ActionTypes.SET_PERIPHERAL_ID]>): SagaIterator {
  try {
    const asciiAxis: SagaReturnType<typeof getAxis> = yield call(getAxis, axisKey);
    const { settings } = asciiAxis;

    yield call([settings, settings.set], getPeripheralIdSetting(asciiAxis), peripheralId);

    yield put(actions.axisCommandDone(axisKey));
    yield put(actions.refreshAxes());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.axisCommandDone(axisKey, err.message));
    logger.warn('setPeripheralId failed', err);
  }
}

function* activate({ payload: { axisKey } }: Action<ActionsToPayloads[ActionTypes.ACTIVATE]>): SagaIterator {
  try {
    const asciiAxis: SagaReturnType<typeof getAxis> = yield call(getAxis, axisKey);

    yield call([asciiAxis, asciiAxis.genericCommand], 'activate');

    yield put(actions.axisCommandDone(axisKey));
    yield put(actions.refreshAxes());
  } catch (err) {
    throwUnexpectedError(err);
    if (err instanceof CommandFailedException && err.details.responseData === 'INACTIVE') {
      yield put(actions.axisCommandDone(axisKey));
      return;
    }
    yield put(actions.axisCommandDone(axisKey, err.message));
    logger.warn('activate failed', err);
  }
}

function getPeripheralIdSetting(axis: ascii.Axis) {
  return axis.device.identity.firmwareVersion.major >= 7 ?
    ascii.SettingConstants.PERIPHERAL_ID : ascii.SettingConstants.PERIPHERAL_ID_LEGACY;
}
