import React from 'react';
import { NoticeBanner, Text } from '@zaber/react-library';

import { OneTimeMessage } from '../help';
import { environment, Flavors } from '../environment';

import { AsciiProtocolReferenceLink } from './AsciiProtocolReferenceLink';

export const TerminalOneTimeMessage: React.FC = () => {
  if (environment.flavor !== Flavors.Zaber) {
    return null;
  }

  return (
    <OneTimeMessage messageId="terminal" message={dismiss => (
      <NoticeBanner type="info" headline="New to Terminal?" closer={dismiss} className="one-time-message">
        <Text>Check out the <AsciiProtocolReferenceLink/> page to familiarize yourself with the most common commands.</Text>
        <div className="pro-tip">
          <Text t={Text.Type.H5}>Pro tip:</Text><Text> press up-arrow to see the history of your commands.</Text>
        </div>
      </NoticeBanner>
    )}/>
  );
};
