import React, { useMemo, Fragment } from 'react';

/** Splits children string to emphasized matches of search. */
export const SearchMatch: React.FC<{ children: string; search?: string | null }> = ({ children, search }) => {
  const fragments = useMemo(() => {
    if (!search) { return [children] }

    const searchExpr = search.toLowerCase();
    const childExpr = children.toLowerCase();

    const fragments: string[] = [];
    let i = 0;
    for (;;) {
      const index = childExpr.indexOf(searchExpr, i);
      if (index < 0) {
        fragments.push(children.substring(i));
        break;
      }
      fragments.push(children.substring(i, index));
      const end = index + search.length;
      fragments.push(children.substring(index, end));
      i = end;
    }
    return fragments;
  }, [children, search]);

  return <>{fragments.map(
    (fragment, i) => i % 2 === 0 ? <Fragment key={i}>{fragment}</Fragment> : <em key={i}>{fragment}</em>)
  }</>;
};
