import type { ascii, FirmwareVersion } from '@zaber/motion';

import type { EntityKey } from '../keys';
import type { Measurement } from '../units';
import { actionBuilder } from '../utils';

import type { ChannelGroup } from './reducers';
import type {
  Channel,
  Mode,
  StorageBuffer,
  SignalType,
  ControlTabId,
  YAxisId,
  UnitMap,
  RecordingBuffer,
  UnitConversionCache,
} from './types';


export enum ActionTypes {
  // Device selection related
  ON_DEVICE_SELECTED = 'SCOPE_ON_DEVICE_SELECTED',
  STORE_DEVICE_IDENTITY = 'SCOPE_STORE_DEVICE_IDENTITY',
  STORE_AXIS_IDENTITY = 'SCOPE_STORE_AXIS_IDENTITY',
  STORE_SELECTABLE_SETTING_NAMES = 'SCOPE_STORE_SELECTABLE_SETTING_NAMES',
  STORE_UNIT_CONVERSIONS = 'SCOPE_STORE_UNIT_CONVERSIONS',
  STORE_SCOPE_BUFFER_INFO = 'SCOPE_STORE_SCOPE_BUFFER_INFO',
  STORE_DEVICE_IO_INFO = 'SCOPE_STORE_DEVICE_IO_INFO',

  // UI selection and show/hide stuff
  SELECT_CONTROL_TAB = 'SCOPE_SELECT_CONTROL_TAB',
  SELECT_RECORDING_TAB = 'SCOPE_SELECT_RECORDING_TAB',
  TOGGLE_CHANNEL_MATH = 'SCOPE_TOGGLE_CHANNEL_MATH',
  SHOW_ADD_SETTING_DIALOG = 'SCOPE_SHOW_ADD_SETTING_DIALOG',
  CLOSE_ADD_SETTING_DIALOG = 'SCOPE_CLOSE_ADD_SETTING_DIALOG',
  SHOW_ADD_MATH_DIALOG = 'SCOPE_SHOW_ADD_MATH_DIALOG',
  CLOSE_ADD_MATH_DIALOG = 'SCOPE_CLOSE_ADD_MATH_DIALOG',
  SHOW_ADD_IO_DIALOG = 'SCOPE_SHOW_ADD_IO_DIALOG',
  CLOSE_ADD_IO_DIALOG = 'SCOPE_CLOSE_ADD_IO_DIALOG',
  SET_CONTROL_PANEL_VISIBILITY = 'SCOPE_SET_CONTROL_PANEL_VISIBILITY',
  SET_STOP_BUTTON_ENABLED = 'SCOPE_SET_START_STOP_BUTTON_ENABLED',
  SET_UNIT_CONVERSION_ENABLED = 'SCOPE_SET_UNIT_CONVERSION_ENABLED',

  // Control data editing
  SET_CAPTURE_MODE = 'SCOPE_SET_CAPTURE_MODE',
  SET_TARGET_SAMPLE_RATE = 'SCOPE_SET_TARGET_SAMPLE_RATE',
  SET_DIFFERENCING_SPAN = 'SCOPE_SET_DIFFERENCING_SPAN',
  ADD_CHANNEL = 'SCOPE_ADD_CHANNEL',
  DELETE_CHANNEL = 'SCOPE_DELETE_CHANNEL',
  SET_CHANNEL_CHART_VISIBILITY = 'SCOPE_SET_CHANNEL_CHART_VISIBILITY',
  SET_CHANNEL_COLOR = 'SCOPE_SET_CHANNEL_COLOR',
  SET_Y_AXIS = 'SCOPE_SET_Y_AXIS',
  SET_SIGNAL_AXIS = 'SET_SIGNAL_AXIS',
  SET_SIGNAL_TYPE = 'SCOPE_SET_SIGNAL_TYPE',
  SET_SIGNAL_VALUE = 'SCOPE_SET_SIGNAL_MAGNITUDE',
  SET_SIGNAL_RETURN_TO_START = 'SCOPE_SET_SIGNAL_RETURN_TO_START',
  SET_CAPTURE_DELAYS = 'SCOPE_SET_CAPTURE_DELAYS',
  SET_SHARED_CHANNEL_SETTINGS = 'SCOPE_SET_SHARED_CHANNEL_SETTINGS',
  SET_RECORDING_TITLE = 'SCOPE_SET_RECORDING_TITLE',
  SET_EXTERNAL_WAIT_FOR_PRINT = 'SCOPE_SET_EXTERNAL_WAIT_FOR_PRINT',
  SET_SAMPLE_LIMIT = 'SCOPE_SET_SAMPLE_LIMIT',

  // Data recording and storage
  START = 'SCOPE_START',
  REPEAT = 'SCOPE_REPEAT',
  REPLACE = 'SCOPE_REPLACE',
  DOWNLOAD_EXISTING_DATA = 'SCOPE_DOWNLOAD_EXISTING_DATA',
  STOP = 'SCOPE_STOP',
  REPORT_PROGRESS = 'SCOPE_REPORT_PROGRESS',
  STORE_CHANNEL_DATA = 'SCOPE_STORE_CHANNEL_DATA',
  REPLACE_CHANNELS = 'SCOPE_REPLACE_CHANNELS',
  STORE_CHART_DATA = 'SCOPE_STORE_CHART_DATA',
  STORE_ACTUAL_SAMPLE_RATE = 'SCOPE_STORE_ACTUAL_SAMPLE_RATE',
  STORE_EXTERNAL_SAMPLE_SETTINGS = 'SCOPE_STORE_EXTERNAL_SAMPLE_SETTINGS',
  SAVE_RECORDING = 'SCOPE_SAVE_RECORDING',
  SAVE_RECORDING_TO_SPREADSHEET = 'SCOPE_SAVE_RECORDING_TO_SPREADSHEET',
  SAVE_RECORDING_TO_IMAGE = 'SCOPE_SAVE_RECORDING_TO_IMAGE',
  DISCARD_ACTIVE_RECORDING = 'SCOPE_DISCARD_ACTIVE_RECORDING',
  DELETE_RECORDING = 'SCOPE_DELETE_RECORDING',
  DELETE_ALL_RECORDINGS = 'SCOPE_DELETE_ALL_RECORDINGS',
  SET_ERROR = 'SCOPE_SET_ERROR',
}


export interface ActionsToPayloads {
  // Device selection related
  [ActionTypes.ON_DEVICE_SELECTED]: { key: EntityKey | null };
  [ActionTypes.STORE_DEVICE_IDENTITY]: { deviceKey: EntityKey; name: string; deviceId: number };
  [ActionTypes.STORE_AXIS_IDENTITY]: { deviceKey: EntityKey; axis: number; name: string; peripheralId: number };
  [ActionTypes.STORE_SELECTABLE_SETTING_NAMES]: { deviceKey: EntityKey; axis: number; names: string[] };
  [ActionTypes.STORE_UNIT_CONVERSIONS]: { deviceKey: EntityKey; axis: number; unitConversions: UnitConversionCache };
  [ActionTypes.STORE_SCOPE_BUFFER_INFO]: { deviceKey: EntityKey; maxChannels: number; bufferSize: number; reshapable: boolean };
  [ActionTypes.STORE_DEVICE_IO_INFO]: { deviceKey: EntityKey; fwVersion: FirmwareVersion; io: ascii.DeviceIOInfo };

  // UI selection and show/hide stuff
  [ActionTypes.SELECT_CONTROL_TAB]: { tab: ControlTabId };
  [ActionTypes.SELECT_RECORDING_TAB]: { index: number };
  [ActionTypes.SHOW_ADD_SETTING_DIALOG]: void;
  [ActionTypes.CLOSE_ADD_SETTING_DIALOG]: { newSettings?: Channel[] };
  [ActionTypes.SHOW_ADD_MATH_DIALOG]: void;
  [ActionTypes.CLOSE_ADD_MATH_DIALOG]: { newMath?: Channel[] };
  [ActionTypes.SHOW_ADD_IO_DIALOG]: void;
  [ActionTypes.CLOSE_ADD_IO_DIALOG]: { newIo?: Channel[] };
  [ActionTypes.SET_CONTROL_PANEL_VISIBILITY]: { visible: boolean };
  [ActionTypes.SET_STOP_BUTTON_ENABLED]: { enabled: boolean };
  [ActionTypes.SET_UNIT_CONVERSION_ENABLED]: { enabled: boolean };

  // Control data editing
  [ActionTypes.SET_CAPTURE_MODE]: { mode: Mode };
  [ActionTypes.SET_TARGET_SAMPLE_RATE]: { sampleRate: number };
  [ActionTypes.SET_DIFFERENCING_SPAN]: { span: number };
  [ActionTypes.ADD_CHANNEL]: { deviceKey: EntityKey; axis: number; settingName: string };
  [ActionTypes.DELETE_CHANNEL]: { id: string };
  [ActionTypes.SET_CHANNEL_CHART_VISIBILITY]: { recordingIndex: number; channelId: string; visible: boolean };
  [ActionTypes.SET_CHANNEL_COLOR]: { recordingIndex: number; channelId: string; color: string };
  [ActionTypes.SET_Y_AXIS]: { recordingIndex: number; channelId: string; axis: YAxisId };
  [ActionTypes.SET_SIGNAL_AXIS]: { axis: number };
  [ActionTypes.SET_SIGNAL_TYPE]: { type: SignalType };
  [ActionTypes.SET_SIGNAL_VALUE]: { value: Measurement };
  [ActionTypes.SET_SIGNAL_RETURN_TO_START]: { returnToStart: boolean };
  [ActionTypes.SET_CAPTURE_DELAYS]: { startDelay?: number; recordingDelay?: number };
  [ActionTypes.SET_SHARED_CHANNEL_SETTINGS]: { share: boolean };
  [ActionTypes.SET_RECORDING_TITLE]: { recordingIndex: number; title: string };
  [ActionTypes.SET_EXTERNAL_WAIT_FOR_PRINT]: { wait: boolean };
  [ActionTypes.SET_SAMPLE_LIMIT]: { numSamples: number | null };

  // Data recording and storage
  [ActionTypes.START]: void;
  [ActionTypes.REPEAT]: { recordingIndex: number };
  [ActionTypes.REPLACE]: { recordingIndex: number };
  [ActionTypes.DOWNLOAD_EXISTING_DATA]: { recordingIndex: number | null };
  [ActionTypes.STOP]: void;
  [ActionTypes.REPORT_PROGRESS]: { progress: number; message: string; showBar: boolean };
  [ActionTypes.STORE_CHANNEL_DATA]: { newSamples: RecordingBuffer; recordingIndex?: number };
  [ActionTypes.REPLACE_CHANNELS]: { newChannels: Channel[]; recordingIndex?: number };
  [ActionTypes.STORE_CHART_DATA]: { recordingIndex: number; groups: ChannelGroup[]; units: UnitMap; data: StorageBuffer };
  [ActionTypes.STORE_ACTUAL_SAMPLE_RATE]: { rate: number | null };
  [ActionTypes.STORE_EXTERNAL_SAMPLE_SETTINGS]: { sampleRate: number; startDelay: number };
  [ActionTypes.SAVE_RECORDING]: { recordingIndex?: number };
  [ActionTypes.SAVE_RECORDING_TO_SPREADSHEET]: { recordingIndex: number };
  [ActionTypes.SAVE_RECORDING_TO_IMAGE]: { format: 'png' | 'jpg'; data: string };
  [ActionTypes.DISCARD_ACTIVE_RECORDING]: void;
  [ActionTypes.DELETE_RECORDING]: { index: number };
  [ActionTypes.DELETE_ALL_RECORDINGS]: void;
  [ActionTypes.SET_ERROR]: { message: string | null };
}


const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  // Device selection related
  onDeviceSelected: (key: EntityKey | null) => buildAction(ActionTypes.ON_DEVICE_SELECTED, { key }),
  storeDeviceIdentity: (deviceKey: EntityKey, name: string, deviceId: number) =>
    buildAction(ActionTypes.STORE_DEVICE_IDENTITY, { deviceKey, name, deviceId }),
  storeAxisIdentity: (deviceKey: EntityKey, axis: number, name: string, peripheralId: number) =>
    buildAction(ActionTypes.STORE_AXIS_IDENTITY, { deviceKey, axis, name, peripheralId }),
  storeSelectableSettingNames: (deviceKey: EntityKey, axis: number, names: string[]) =>
    buildAction(ActionTypes.STORE_SELECTABLE_SETTING_NAMES, { deviceKey, axis, names }),
  storeAxisUnitConversions: (deviceKey: EntityKey, axis: number, unitConversions: UnitConversionCache) =>
    buildAction(ActionTypes.STORE_UNIT_CONVERSIONS, { deviceKey, axis, unitConversions }),
  storeAxisBufferInfo: (deviceKey: EntityKey, maxChannels: number, bufferSize: number, reshapable: boolean) =>
    buildAction(ActionTypes.STORE_SCOPE_BUFFER_INFO, { deviceKey, maxChannels, bufferSize, reshapable }),
  storeDeviceIoInfo: (deviceKey: EntityKey, fwVersion: FirmwareVersion, io: ascii.DeviceIOInfo) =>
    buildAction(ActionTypes.STORE_DEVICE_IO_INFO, { deviceKey, fwVersion, io }),

  // UI selection and show/hide stuff
  selectControlTab: (tab: ControlTabId) => buildAction(ActionTypes.SELECT_CONTROL_TAB, { tab }),
  selectRecordingTab: (index: number) => buildAction(ActionTypes.SELECT_RECORDING_TAB, { index }),
  showAddSettingDialog: () => buildAction(ActionTypes.SHOW_ADD_SETTING_DIALOG),
  closeAddSettingDialog: (newSettings?: Channel[]) => buildAction(ActionTypes.CLOSE_ADD_SETTING_DIALOG, { newSettings }),
  showAddMathDialog: () => buildAction(ActionTypes.SHOW_ADD_MATH_DIALOG),
  closeAddMathDialog: (newMath?: Channel[]) => buildAction(ActionTypes.CLOSE_ADD_MATH_DIALOG, { newMath }),
  showAddIoDialog: () => buildAction(ActionTypes.SHOW_ADD_IO_DIALOG),
  closeAddIoDialog: (newIo?: Channel[]) => buildAction(ActionTypes.CLOSE_ADD_IO_DIALOG, { newIo }),
  setControlPanelVisibility: (visible: boolean) => buildAction(ActionTypes.SET_CONTROL_PANEL_VISIBILITY, { visible }),
  setStopButtonEnabled: (enabled: boolean) => buildAction(ActionTypes.SET_STOP_BUTTON_ENABLED, { enabled }),
  setUnitConversionEnabled: (enabled: boolean) => buildAction(ActionTypes.SET_UNIT_CONVERSION_ENABLED, { enabled }),

  // Control data editing
  setCaptureMode: (mode: Mode) => buildAction(ActionTypes.SET_CAPTURE_MODE, { mode }),
  setTargetSampleRate: (sampleRate: number) => buildAction(ActionTypes.SET_TARGET_SAMPLE_RATE, { sampleRate }),
  setDifferencingSpan: (span: number) => buildAction(ActionTypes.SET_DIFFERENCING_SPAN, { span }),
  addChannel: (deviceKey: EntityKey, axis: number, settingName: string) =>
    buildAction(ActionTypes.ADD_CHANNEL, { deviceKey, axis, settingName }),
  deleteChannel: (id: string) => buildAction(ActionTypes.DELETE_CHANNEL, { id }),
  setChannelChartVisibility: (recordingIndex: number, channelId: string, visible: boolean) =>
    buildAction(ActionTypes.SET_CHANNEL_CHART_VISIBILITY, { recordingIndex, channelId, visible }),
  setChannelColor: (recordingIndex: number, channelId: string, color: string) =>
    buildAction(ActionTypes.SET_CHANNEL_COLOR, { recordingIndex, channelId, color }),
  setYAxis: (recordingIndex: number, channelId: string, axis: YAxisId) =>
    buildAction(ActionTypes.SET_Y_AXIS, { recordingIndex, channelId, axis }),
  setSignalAxis: (axis: number) => buildAction(ActionTypes.SET_SIGNAL_AXIS, { axis }),
  setSignalType: (type: SignalType) => buildAction(ActionTypes.SET_SIGNAL_TYPE, { type }),
  setSignalValue: (value: Measurement) => buildAction(ActionTypes.SET_SIGNAL_VALUE, { value }),
  setSignalReturnToStart: (returnToStart: boolean) => buildAction(ActionTypes.SET_SIGNAL_RETURN_TO_START, { returnToStart }),
  setCaptureDelays: (delays: ActionsToPayloads[ActionTypes.SET_CAPTURE_DELAYS]) =>
    buildAction(ActionTypes.SET_CAPTURE_DELAYS, { ...delays }),
  setSharedChannelSettings: (share: boolean) => buildAction(ActionTypes.SET_SHARED_CHANNEL_SETTINGS, { share }),
  setRecordingTitle: (recordingIndex: number, title: string) => buildAction(ActionTypes.SET_RECORDING_TITLE, { recordingIndex, title }),
  setExternalWaitForPrint: (wait: boolean) => buildAction(ActionTypes.SET_EXTERNAL_WAIT_FOR_PRINT, { wait }),
  setSampleLimit: (numSamples: number | null) => buildAction(ActionTypes.SET_SAMPLE_LIMIT, { numSamples }),

  // Data recording and storage
  startRecording: () => buildAction(ActionTypes.START),
  repeatRecording: (recordingIndex: number) => buildAction(ActionTypes.REPEAT, { recordingIndex }),
  replaceRecording: (recordingIndex: number) => buildAction(ActionTypes.REPLACE, { recordingIndex }),
  downloadExistingData: (recordingIndex: number | null = null) => buildAction(ActionTypes.DOWNLOAD_EXISTING_DATA, { recordingIndex }),
  stopRecording: () => buildAction(ActionTypes.STOP),
  reportProgress: (progress: number, message: string, showBar: boolean) =>
    buildAction(ActionTypes.REPORT_PROGRESS, { progress, message, showBar }),
  storeChannelData: (newSamples: RecordingBuffer, recordingIndex?: number) =>
    buildAction(ActionTypes.STORE_CHANNEL_DATA, { newSamples, recordingIndex }),
  replaceChannelData: (newChannels: Channel[], recordingIndex?: number) =>
    buildAction(ActionTypes.REPLACE_CHANNELS, { newChannels, recordingIndex }),
  storeChartData: (recordingIndex: number, groups: ChannelGroup[], units: UnitMap, data: StorageBuffer) =>
    buildAction(ActionTypes.STORE_CHART_DATA, { recordingIndex, groups, units, data }),
  storeActualSampleRate: (rate: number | null) => buildAction(ActionTypes.STORE_ACTUAL_SAMPLE_RATE, { rate }),
  storeExternalSampleSettings: (sampleRate: number, startDelay: number) =>
    buildAction(ActionTypes.STORE_EXTERNAL_SAMPLE_SETTINGS, { sampleRate, startDelay }),
  saveRecording: (recordingIndex? : number) => buildAction(ActionTypes.SAVE_RECORDING, { recordingIndex }),
  saveRecordingToSpreadsheet: (recordingIndex: number) =>
    buildAction(ActionTypes.SAVE_RECORDING_TO_SPREADSHEET, { recordingIndex }),
  saveRecordingToImage: (format: 'png' | 'jpg', data: string) =>
    buildAction(ActionTypes.SAVE_RECORDING_TO_IMAGE, { format, data }),
  discardRecording: () => buildAction(ActionTypes.DISCARD_ACTIVE_RECORDING),
  deleteRecording: (index: number) => buildAction(ActionTypes.DELETE_RECORDING, { index }),
  deleteAllRecordings: () => buildAction(ActionTypes.DELETE_ALL_RECORDINGS),
  setError: (message: string | null) => buildAction(ActionTypes.SET_ERROR, { message }),
};
