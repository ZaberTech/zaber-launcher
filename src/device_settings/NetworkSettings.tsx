import React, { useState } from 'react';
import { RadioButton, Checkbox, Input, Modal, Icons, Text, NoticeBanner, ButtonRowConfirmCancel } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';

import { selectConfigureNetworkErr, selectConfiguringNetwork, selectNetworkSettings } from './selectors';
import { actions as actionsDefinition } from './actions';
import type { NetworkConfig } from './types';

function parseDotNotation(addressToParse: string) {
  if (!addressToParse) { return null }
  const match = addressToParse.trim().match(/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/);
  if (match == null) { return null }
  const values = match.slice(1).map(value => Number(value));
  return values;
}

function validateIPAddressOrDefaultGateway(addressOrGateway: string) {
  if (addressOrGateway === '0.0.0.0') { return true }
  const values = parseDotNotation(addressOrGateway);
  if (!values) { return false }

  if (values[0] <= 0 || values[0] > 223) {
    return false;
  }
  for (let i = 1; i <= 3; i++) {
    if (values[i] < 0 || values[i] > 255) {
      return false;
    }
  }
  return true;
}

function validateSubnetMask(subnetMask: string) {
  if (!subnetMask) {
    return false;
  }

  if (Number.isInteger(+subnetMask)) {
    return +subnetMask >= 8 && +subnetMask <= 31;
  }

  const values = parseDotNotation(subnetMask);
  if (!values) {
    return false;
  }

  return values.map(n => n.toString(2)).join('').match(/^1+0+$/) != null;
}

const ValidationError: React.FC<{error: string}> = ({ error }) => (
  <div className="error-message">
    <Icons.ErrorWarning/><Text t={Text.Type.Helper}>{error}</Text>
  </div>);

export const NetworkSettings: React.FC = () => {
  const settings = useSelector(selectNetworkSettings)!;
  const configuringNetwork = useSelector(selectConfiguringNetwork);
  const configureNetworkErr = useSelector(selectConfigureNetworkErr);

  const getInitialConnectionType = () => {
    if (!settings.dhcp || settings.dhcp === '1') {
      return 'automatic';
    } else if (settings.ipAddress === '0.0.0.0') {
      return 'disabled';
    }
    return 'manual';
  };

  const [connectionType, setConnectionType] = useState(getInitialConnectionType);
  const [ipAddress, setIpAddress] = useState(settings.ipAddress);
  const [subnetMask, setSubnetMask] = useState(settings.subnetMask);
  const [defaultGateway, setDefaultGateway] = useState(settings.defaultGateway);
  const useDefaultGateway = defaultGateway !== '0.0.0.0';
  const [showValidation, setShowValidation] = useState(false);

  const actions = useActions(actionsDefinition);

  const ipAddressValid = connectionType !== 'manual' || validateIPAddressOrDefaultGateway(ipAddress);
  const subnetMaskValid = connectionType !== 'manual' || validateSubnetMask(subnetMask);
  const defaultGatewayValid = connectionType !== 'manual' || validateIPAddressOrDefaultGateway(defaultGateway);
  const allValid = (ipAddressValid && subnetMaskValid && defaultGatewayValid);

  const onConfirm = () => {
    if (!allValid) {
      setShowValidation(true);
      return;
    }
    sendCommand();
  };

  const sendCommand = () => {
    const config: NetworkConfig = {
      dhcp: true,
    };
    if (connectionType !== 'automatic') {
      config.dhcp = false;
      config.IPAddress = ipAddress;
      config.defaultGateway = defaultGateway;
      config.subnetMask = subnetMask;
    }
    actions.configureNetwork(config);
  };

  return (
    <Modal
      className="choose-network-settings"
      headerIcon={<Icons.Network/>}
      headerText="Network Settings"
      buttons={<ButtonRowConfirmCancel
        confirmText="Confirm"
        onConfirm={(configuringNetwork || (showValidation && !allValid)) ? 'disabled' : onConfirm}
        onCancel={configuringNetwork ? 'disabled' : () => actions.setCustomDialog(null)}
      />}
      onRequestClose={() => actions.setCustomDialog(null)}>
      <div className="network-settings">
        {configureNetworkErr && <NoticeBanner type="error">
          Error: {configureNetworkErr}
        </NoticeBanner>}
        <RadioButton
          name="connection-type"
          onValueChange={() => setConnectionType('automatic')}
          checked={connectionType === 'automatic'}>
          Obtain an IP address automatically
        </RadioButton>
        <RadioButton
          name="connection-type"
          onValueChange={() => {
            setConnectionType('disabled');
            setIpAddress('0.0.0.0');
            setSubnetMask('255.255.0.0');
            setDefaultGateway('0.0.0.0');
          }}
          checked={connectionType === 'disabled'}>
          Disable networking
        </RadioButton>
        <RadioButton
          name="connection-type"
          onValueChange={() => setConnectionType('manual')}
          checked={connectionType === 'manual'}>
          Use the following IP address:
        </RadioButton>
        <div>
          <Input
            value={ipAddress}
            labelContent="IP Address"
            status={showValidation && !ipAddressValid ? 'error' : null}
            disabled={connectionType !== 'manual'}
            onValueChange={value => { setIpAddress(value) }}/>
          {(showValidation && !ipAddressValid) && <ValidationError error="Invalid IP address"/>}
        </div>
        <div>
          <Input
            value={subnetMask}
            labelContent="Subnet Mask"
            status={showValidation && !subnetMaskValid ? 'error' : null}
            disabled={connectionType !== 'manual'}
            onValueChange={value => { setSubnetMask(value) }}/>
          {(showValidation && !subnetMaskValid) && <ValidationError error="Invalid subnet mask"/>}
        </div>
        <div>
          <Checkbox labelContent="Use default gateway" checked={useDefaultGateway}
            onChecked={() => setDefaultGateway(useDefaultGateway ? '0.0.0.0' : '')}
            disabled={connectionType !== 'manual'}/>
          <Input
            value={defaultGateway}
            labelContent="Default Gateway"
            status={showValidation && !defaultGatewayValid ? 'error' : null}
            disabled={connectionType === 'automatic' || !useDefaultGateway}
            onValueChange={value => { setDefaultGateway(value) }}/>
          {(showValidation && !defaultGatewayValid) && <ValidationError error="Invalid default gateway"/>}
        </div>
      </div>
    </Modal>
  );
};
