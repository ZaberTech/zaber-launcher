import { ascii } from '@zaber/motion';

import { MS_PER_SEC } from '../types';
import { roundedE3Sequence } from '../utils';

import { NEW_RECORDING_TAB } from './types';


export const getDifferencingRange = function(sampleRate: number): [number, number] {
  const interval = MS_PER_SEC / sampleRate;
  const logRange = roundedE3Sequence(interval / 1.9, interval * 19);
  const min = logRange[0];
  const max = logRange[logRange.length - 1];
  return [min, max];
};


export const isNewRecTab = function(recordingIndex: number): boolean {
  return recordingIndex === NEW_RECORDING_TAB;
};


export const PREFERRED_DIMENSION_ORDER = ['Length', 'Angle', 'Velocity', 'Angular Velocity', 'Acceleration', 'Angular Acceleration'];

/**
 * In-place sorts names of unit dimensions in a preferred order for the oscilloscope.
 * Dimension names that don't have a preferred order will be alphabetically sorted and placed
 * after those that do.
 * @param names Array to sort - will be modified in place.
 */
export const sortDimensionNames = function(names: string[]): void {
  names.sort((a, b) => {
    let ia = PREFERRED_DIMENSION_ORDER.indexOf(a);
    if (ia < 0) {
      ia = PREFERRED_DIMENSION_ORDER.length;
    }

    let ib = PREFERRED_DIMENSION_ORDER.indexOf(b);
    if (ib < 0) {
      ib = PREFERRED_DIMENSION_ORDER.length;
    }

    if (ia !== ib) {
      return ia - ib;
    }

    return a.localeCompare(b);
  });
};


export const selectedTabClass = (selected: boolean) => selected ? 'selected-tab' : 'not-selected';

export const ioPortTypeToZml = (portType: string): ascii.IoPortType => {
  switch (portType) {
    case 'ai': return ascii.IoPortType.ANALOG_INPUT;
    case 'ao': return ascii.IoPortType.ANALOG_OUTPUT;
    case 'di': return ascii.IoPortType.DIGITAL_INPUT;
    case 'do': return ascii.IoPortType.DIGITAL_OUTPUT;
    default:
      throw new Error(`Illegal I/O port type ${portType}`);
  }
};


export const zmlIoPortTypeToString = (portType: ascii.IoPortType): string => {
  switch (portType) {
    case ascii.IoPortType.ANALOG_INPUT: return 'ai';
    case ascii.IoPortType.ANALOG_OUTPUT: return 'ao';
    case ascii.IoPortType.DIGITAL_INPUT: return 'di';
    case ascii.IoPortType.DIGITAL_OUTPUT: return 'do';
    default:
      throw new Error(`Illegal I/O port type ${portType}`);
  }
};
