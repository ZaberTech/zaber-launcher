import React from 'react';
import { useSelector } from 'react-redux';

import { ExternalLink } from '../components';

import { selectProtocolLink } from './selectors';

export const AsciiProtocolReferenceLink: React.FC = () => {
  const protocolLinkUrl = useSelector(selectProtocolLink);

  return <ExternalLink url={protocolLinkUrl}>ASCII Protocol Reference</ExternalLink>;
};
