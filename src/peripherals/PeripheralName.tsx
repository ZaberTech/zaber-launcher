import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { ConfirmCancel, Icons, Loader, Text } from '@zaber/react-library';

import type { RootState } from '../store';

import type { AxisStateComplete } from './selectors';
import { actions as actionsDefinition } from './actions';
import { SupportedPeripherals } from './SupportedPeripherals';

interface Props {
  axis: AxisStateComplete;
}
interface DispatchProps {
  actions: typeof actionsDefinition;
}
interface State {
  newPeripheralId: number | null;
  editing: boolean;
}

class PeripheralNameBase extends Component<Props & DispatchProps, State> {
  constructor(props: Props & DispatchProps) {
    super(props);
    this.state = {
      editing: false,
      newPeripheralId: null,
    };
  }

  static getDerivedStateFromProps({ axis }: Props, state: State): Partial<State> | null {
    if (state.editing && !axis.derived.canSetManually) {
      return { editing: false };
    }
    return null;
  }

  startEditing = () => {
    const { axis: { key, supportedPeripherals }, actions } = this.props;
    this.setState({ editing: true, newPeripheralId: null });
    if (!supportedPeripherals) {
      actions.fetchSupportedPeripherals(key);
    }
  };

  cancelEditing = () => {
    this.setState({ editing: false });
  };

  changePeripheralId = () => {
    const { actions, axis } = this.props;
    const { newPeripheralId } = this.state;
    this.setState({ editing: false });
    if (newPeripheralId !== null) {
      actions.setPeripheralId(axis.key, +newPeripheralId);
    }
  };

  render(): React.ReactNode {
    const { axis } = this.props;
    const { editing } = this.state;
    const {
      commandInProgress,
      connectionLoading,
      monitorInfo,
      derived: { canSetManually },
    } = axis;
    const uiDisabled = commandInProgress || connectionLoading;

    return (
      <div className="peripheral-select">
        {!editing && <>
          <div className="names">
            <Text t={Text.Type.H5}>{axis.label ?? axis.identity.peripheralName}</Text>
            {axis.label && <Text t={Text.Type.Helper}>{axis.identity.peripheralName}</Text>}
          </div>

          {monitorInfo && canSetManually && <>
            &ensp;
            <Icons.Screwdriver title="Change Peripheral" disabled={uiDisabled} onClick={this.startEditing}/>
          </>}
          {commandInProgress && <Loader size="small"/>}
        </>}

        {editing && <>
          <SupportedPeripherals axis={axis} onPick={({ peripheralId }) => this.setState({ newPeripheralId: peripheralId })}/>
          <ConfirmCancel onCancel={this.cancelEditing} onConfirm={this.changePeripheralId} confirmDisabled={uiDisabled}/>
        </>}
      </div>
    );
  }
}

export const PeripheralName = connect<unknown, DispatchProps, Props, RootState>(
  null,
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(PeripheralNameBase);
