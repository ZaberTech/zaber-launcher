export enum TestSetupErrors {
  CANNOT_FIND_SENSOR = 'CANNOT_FIND_SENSOR',
  CANNOT_HOME = 'CANNOT_HOME',
  WRONG_ORIENTATION = 'WRONG_ORIENTATION',
  WRONG_DISTANCE = 'WRONG_DISTANCE',
  WRONG_ORIENTATION_OR_DISTANCE = 'WRONG_ORIENTATION_OR_DISTANCE',
  DRIVER_DISABLED = 'DRIVER_DISABLED',
}

export enum ErrorContext {
  GENERIC = 1,
  DRIVER_ORIENTATION,
  TRAVEL_DISTANCE,
}

const testSetupErrorsContext: Partial<Record<TestSetupErrors, ErrorContext>> = {
  [TestSetupErrors.WRONG_ORIENTATION]: ErrorContext.DRIVER_ORIENTATION,
  [TestSetupErrors.WRONG_DISTANCE]: ErrorContext.TRAVEL_DISTANCE,
};

export const getErrorContext = (error: TestSetupErrors) => testSetupErrorsContext[error] ?? ErrorContext.GENERIC;

export const errorMessages: Record<TestSetupErrors, string> = {
  [TestSetupErrors.CANNOT_FIND_SENSOR]: [
    'Cannot find the home sensor.',
    'Make sure the home sensor is mounted and connected.',
  ].join(' '),
  [TestSetupErrors.CANNOT_HOME]: [
    'Device cannot home.',
    'Make sure that the home sensor is mounted and connected properly.'
  ].join(' '),
  [TestSetupErrors.WRONG_ORIENTATION]: [
    'Motor orientation appears to be incorrect.',
    'Make sure that the correct stage type and motor configuration is selected.'
  ].join(' '),
  [TestSetupErrors.WRONG_DISTANCE]: [
    'The travel length that you entered seems too long.',
    'Make sure that the travel distance matches your product.'
  ].join(' '),
  [TestSetupErrors.WRONG_ORIENTATION_OR_DISTANCE]: [
    'The travel length or orientation is incorrect.',
    'Make sure that the correct stage type and motor configuration is selected.',
    'Make sure that the travel distance matches your product.'
  ].join(' '),
  [TestSetupErrors.DRIVER_DISABLED]: [
    'Device has disabled driver.',
    'Enable the driver in Basic Movement and repeat the process.',
  ].join(' '),
};

export class TestSetupError extends Error {
  constructor(
    public readonly error: TestSetupErrors
  ) {
    super(error);
    Object.setPrototypeOf(this, TestSetupError.prototype);
  }
}
