import type { AnyAction } from '../utils';

export const REDUX_BROADCAST_CHANNEL = 'REDUX_BROADCAST';
export const REDUX_MAIN_PROCESS_CHANNEL = 'REDUX_MAIN_PROCESS_CHANNEL';

export interface IpcReceivedAction extends AnyAction {
  __ipcReceived?: true;
}
