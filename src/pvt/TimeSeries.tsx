/* eslint-disable @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-unsafe-call */

import _ from 'lodash';
import React, { Component, createRef } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import ReactECharts, { EChartsOption } from 'echarts-for-react';
import { Units } from '@zaber/motion';
import { Colors, Icons, Text } from '@zaber/react-library';

import {
  ChartZoomPanController,
  DEFAULT_ZOOM_PAN,
  MouseZoomRegion,
  ZoomEventMultiAxis,
  ZoomEventRectangle,
  ZoomEventSingleAxis,
  ZoomPanCollection,
  ZoomPanSet,
  type ZoomPanState
} from '../components';
import { ECHARTS_SLIDER_STYLE, ECHARTS_X_AXIS_DEFAULTS, ECHARTS_Y_AXIS_DEFAULTS } from '../eChartsCommon';
import { RootState } from '../store';
import { derivativeUnit, getUnitLabel } from '../units';

import { actions as actionDefinitions } from './actions';
import { Bug, DeviceSelection, LimitType, Path, Point, Range, Series, Span } from './types';
import {
  selectBugs,
  selectDeviceOptions,
  selectDevicePath,
  selectDevicePosition,
  selectPlaybackTime,
  selectSelectedPointIndex,
  selectTimeSeriesVisibility,
  selectUserPath
} from './selectors';
import { seriesColor } from './utils';
import { TimelineControlPointTooltip } from './TimelineControlPointTooltip';
import { Filename } from './Filename';


const TOP_MARGIN = 50;
const CHART_HEIGHT = 170;
const TIMELINE_MARGIN = 50;
const TIMELINE_HEIGHT = 20;
const LEFT_MARGIN = 80;
const RIGHT_MARGIN = 40;
const Y_AXIS_LABEL_MARGIN = 10;


const CHART_NAMES = ['Position', 'Velocity', 'Acceleration'];

const chartTop = function(chartIndex: number): number {
  return TOP_MARGIN + chartIndex * (CHART_HEIGHT + TOP_MARGIN + TIMELINE_MARGIN);
};

const timelineTop = function(): number {
  return chartTop(2) + CHART_HEIGHT + TIMELINE_MARGIN;
};


interface Props {
  actions: typeof actionDefinitions;
  userPath: Path | null;
  devicePath: Path | null;
  visibleSeries: boolean[];
  selectedPoint: number | null;
  bugs: Bug[];
  devicePosition: number[];
  deviceOptions: DeviceSelection;
  playbackTime: number | null;
}


class TimeSeriesBase extends Component<Props> {
  private viewPort = createRef<HTMLDivElement>();
  private echarts = createRef<ReactECharts>();
  private seriesByIndex: Record<number, string> = {};

  private zoomPan = new ZoomPanCollection(this.notifyZoomPan.bind(this));
  private zoomController = new ChartZoomPanController(
    this.zoomPan,
    TimeSeriesBase.determineZoomRegion,
    TimeSeriesBase.getMouseChartIndex,
    TimeSeriesBase.getNormalizedMouseCoords,
    this.getZoomRangeFromData.bind(this));

  // This stores the last known range of the data associated with each chart axis (identified by zoom and pan control ID).
  // Unlike normal zoom and pan, the start and end values can be anything.
  // This exists because the ECharts rectangle zoom returns ranges in terms of data values instead of percentages.
  private dataRange: ZoomPanSet = {};


  private readonly baseOptions: EChartsOption = {
    animation: false,
    axisPointer: {
      link: [{
        xAxisIndex: 'all'
      }],
      label: {
        backgroundColor: '#777'
      },
    },
  };


  constructor(props: Props) {
    super(props);

    for (const i of _.range(3)) {
      this.zoomPan.get(this.zoomController.makeControlId(i, 'x', 'bottom', 0));
      this.zoomPan.get(this.zoomController.makeControlId(i, 'y', 'right', 0));
    }
  }


  componentDidMount(): void {
    if (this.echarts.current) {
      const echarts = this.echarts.current.getEchartsInstance();
      const renderer = echarts?.getZr();
      renderer?.on('contextmenu', this.resetZoom);
      window.addEventListener('keydown', this.onKeyDown);
      window.addEventListener('keyup', this.onKeyUp);
    }

    this.props.actions.setPollDefault('stop');
  }


  componentWillUnmount(): void {
    window.removeEventListener('keydown', this.onKeyDown);
    window.removeEventListener('keyup', this.onKeyUp);
  }


  onKeyDown = (event: KeyboardEvent) => {
    if (event.key === 'Shift' && !this.zoomController.enableMouseZoomAndPan) {
      event.preventDefault();
      this.zoomController.enableMouseZoomAndPan = true;
    }
  };


  onKeyUp = (event: KeyboardEvent) => {
    if (event.key === 'Shift' && this.zoomController.enableMouseZoomAndPan) {
      event.preventDefault();
      this.zoomController.enableMouseZoomAndPan = false;
    }
  };


  onMouseDownCapture = () => {
    // Activate rectangle select zoom when not in zoom & pan mode.
    this.echarts.current?.getEchartsInstance().dispatchAction({
      type: 'takeGlobalCursor',
      key: 'dataZoomSelect',
      dataZoomSelectActive: !this.zoomController.enableMouseZoomAndPan,
    });
  };


  onMouseMove = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    const canvasBounds = event?.currentTarget?.getBoundingClientRect();
    if (canvasBounds && this.viewPort.current) {
      this.zoomController.onMouseMove(event, this.viewPort.current, LEFT_MARGIN + RIGHT_MARGIN, CHART_HEIGHT);
    }
  };


  onMouseWheel = (event: React.WheelEvent) => {
    this.zoomController.onMouseWheel(event);
  };


  private notifyZoomPan(changedZoomIds: string[]): void {
    if (changedZoomIds.length) {
      this.echarts.current?.getEchartsInstance()?.dispatchAction({
        type: 'dataZoom',
        batch: changedZoomIds.map(id => ({
          dataZoomId: id,
          ...this.zoomPan.get(id),
        })),
      });
    }
  }


  private static determineZoomRegion(x: number, y: number, width: number): MouseZoomRegion {
    const yMod = (y - TOP_MARGIN) % (CHART_HEIGHT + TIMELINE_MARGIN + TOP_MARGIN);

    let zoomRegion = MouseZoomRegion.NONE;
    if (yMod < CHART_HEIGHT && y < timelineTop()) {
      if (x > width - RIGHT_MARGIN) {
        zoomRegion = MouseZoomRegion.RIGHTAXIS;
      } else {
        zoomRegion = MouseZoomRegion.CHART;
      }
    } else if (y >= timelineTop()) {
      if (x >= LEFT_MARGIN && x <= width - RIGHT_MARGIN) {
        zoomRegion = MouseZoomRegion.TIMELINE;
      }
    }

    return zoomRegion;
  }


  private static getNormalizedMouseCoords(x: number, y: number, width: number): { x: number; y: number } {
    const yMod = (y - TOP_MARGIN) % (CHART_HEIGHT + TIMELINE_MARGIN + TOP_MARGIN);
    return {
      x: (x - LEFT_MARGIN) / (width - LEFT_MARGIN - RIGHT_MARGIN),
      y: 1 - yMod / CHART_HEIGHT,
    };
  }


  private static getMouseChartIndex(y: number): number {
    return Math.floor((y - TOP_MARGIN) / (CHART_HEIGHT + TIMELINE_MARGIN + TOP_MARGIN));
  }


  private getZoomRangeFromData(start: number, end: number, axisId: string): ZoomPanState {
    const dataRange = this.dataRange[axisId];
    if (!dataRange) {
      return DEFAULT_ZOOM_PAN;
    }

    const diff = dataRange.end - dataRange.start;
    return {
      start: 100 * (start - dataRange.start) / diff,
      end: 100 * (end - dataRange.start) / diff,
    };
  }


  onDataZoom = (event: ZoomEventSingleAxis | ZoomEventMultiAxis | ZoomEventRectangle) => {
    this.zoomController.onDataZoom(event);
  };


  resetZoom = () => {
    this.echarts.current?.getEchartsInstance()?.dispatchAction({
      type: 'dataZoom',
      batch: this.zoomPan.getIds(() => true).map(id => ({
        dataZoomId: id,
        ...DEFAULT_ZOOM_PAN,
      })),
    });
  };


  onChartDataSelected = (event: { fromActionPayload: { dataIndexInside: number; seriesIndex: number } }) => {
    setTimeout(() => {
      const seriesIndex = event.fromActionPayload.seriesIndex;
      const pointIndex = event.fromActionPayload.dataIndexInside;
      this.props.actions.setSelection(pointIndex);
      const seriesName = this.seriesByIndex[seriesIndex];
      if (seriesName) {
        this.props.actions.setSeriesDetailsVisible(seriesName, true);
      }
    }, 0);
  };


  saveToImage = () => {
    const data = this.echarts.current?.getEchartsInstance()?.getDataURL({ type: 'png' });
    if (data) {
      this.props.actions.saveTimeSeriesImage('png', data);
    }
  };


  render(): React.ReactNode {
    const { actions, userPath, devicePath, selectedPoint, visibleSeries } = this.props;

    const minTime = Math.floor(_.first(devicePath?.series[0]?.points)?.time ?? 0);
    const maxTime = Math.ceil(_.last(devicePath?.series[0]?.points)?.time ?? 10);
    this.dataRange[this.zoomController.makeControlId(0, 'x', 'bottom', 0)] = { start: minTime, end: maxTime };
    const chartUnits = [
      userPath?.positionUnit ?? Units.NATIVE,
      userPath?.velocityUnit ?? Units.NATIVE,
      derivativeUnit(userPath?.velocityUnit) ?? Units.NATIVE,
    ];
    this.seriesByIndex = {};

    const options: EChartsOption = {
      ...this.baseOptions,
      dataZoom: [{
        ...ECHARTS_SLIDER_STYLE,
        ...this.zoomPan.get(this.zoomController.makeControlId(0, 'x', 'bottom', 0)),
        id: this.zoomController.makeControlId(0, 'x', 'bottom', 0),
        type: 'slider',
        filterMode: 'none',
        brushSelect: false,
        xAxisIndex: _.range(3),
        top: timelineTop(),
        height: TIMELINE_HEIGHT,
        throttle: 0,
      }, ..._.range(3).map(i => ({
        ...ECHARTS_SLIDER_STYLE,
        ...this.zoomPan.get(this.zoomController.makeControlId(i, 'y', 'right', 0)),
        id: this.zoomController.makeControlId(i, 'y', 'right', 0),
        type: 'slider',
        filterMode: 'none',
        orient: 'vertical',
        brushSelect: false,
        showDataShadow: false,
        width: 15,
        yAxisIndex: i,
        throttle: 0,
      }))],
      xAxis: _.range(3).map(i => ({
        ...ECHARTS_X_AXIS_DEFAULTS,
        name: 'Time (s)',
        gridIndex: i,
        min: minTime,
        max: maxTime,
      })),
      yAxis: _.range(3).map(i => ({
        ..._.cloneDeep(ECHARTS_Y_AXIS_DEFAULTS),
        nameGap: Y_AXIS_LABEL_MARGIN,
        gridIndex: i,
        name: `${CHART_NAMES[i]} (${getUnitLabel(chartUnits[i])})`,
        nameLocation: 'end',
      })),
      dataset: [],
      series: [],
      grid: _.range(3).map(i => ({
        top: chartTop(i),
        height: CHART_HEIGHT,
        left: LEFT_MARGIN,
        right: RIGHT_MARGIN,
      })),
      tooltip: {
        trigger: 'item',
        formatter: this.formatToolTip,
      },
      // Create rectangle select zoom tool when not in pan & zoom mode, and hide it.
      toolbox: this.zoomController.enableMouseZoomAndPan ? undefined : {
        feature: {
          dataZoom: {
            brushStyle: {
              color: `${Colors.blue}80`,
            },
            icon: {
              // Hack to not display icons.
              zoom: 'path://',
              back: 'path://',
            },
          },
        },
      },
    };

    // Position, velocity and acceleration curves for the discretized path.
    function addDataSet(getValue: (p: Point) => number | null) {
      options.dataset = options.dataset.concat((devicePath?.series ?? []).map(series => ({
        source: series.points.map(point => ([point.time, getValue(point)])),
      })));
    }

    addDataSet(p => p.pos);
    addDataSet(p => p.vel);
    addDataSet(p => p.accel ?? null);

    function setDataRange(that: TimeSeriesBase, i: number, getRange: (s: Series) => Range | null) {
      that.dataRange[that.zoomController.makeControlId(i, 'y', 'right', 0)] = {
        start: _.min(devicePath?.series.map(series => getRange(series)?.min.value)) ?? 0,
        end: _.max(devicePath?.series.map(series => getRange(series)?.max.value)) ?? 100,
      };
    }

    setDataRange(this, 0, s => s.positionRange ?? null);
    setDataRange(this, 1, s => s.velocityRange ?? null);
    setDataRange(this, 2, s => s.accelerationRange ?? null);

    const controlDataSetIndex = options.dataset.length;

    // Position, velocity and acceleration control points.
    function addControlDataSet(selectedIndex: number | null, getValue: (p: Point, s: number) => number | null) {
      options.dataset = options.dataset.concat((userPath?.series ?? []).map((series, seriesIndex) => ({
        source: series.points.map((point, i) => ([point.time, getValue(point, seriesIndex), i === selectedIndex])),
      })));
    }

    const lastPos = Array(userPath?.series.length ?? 0).fill(0);
    addControlDataSet(selectedPoint, (p, s) => {
      let pos = p.pos;
      if (p.relative) {
        pos += lastPos[s];
      }
      lastPos[s] = pos;
      return pos;
    });
    addControlDataSet(selectedPoint, p => p.vel);
    addControlDataSet(selectedPoint, p => p.accel ?? null);


    visibleSeries.forEach((visible, i) => {
      const series = devicePath?.series[i];
      if (visible && series != null) {
        // Data series for the discretized curves.
        this.seriesByIndex[options.series.length] = series.name;
        options.series.push({
          ...this.discreteSeriesDefaults(0, i),
          name: `${series!.name} position`,
          datasetIndex: i,
          markLine: this.markLine('position', i, series.positionLimits),
          markPoint: this.markers(i, 'position'),
        });

        this.seriesByIndex[options.series.length] = series.name;
        options.series.push({
          ...this.discreteSeriesDefaults(1, i),
          name: `${series.name} velocity`,
          datasetIndex: devicePath!.series.length + i,
          markLine: this.markLine('velocity', i, series.velocityLimits),
          markPoint: this.markers(i, 'velocity'),
        });

        this.seriesByIndex[options.series.length] = series.name;
        options.series.push({
          ...this.discreteSeriesDefaults(2, i),
          name: `${series.name} acceleration`,
          datasetIndex: 2 * devicePath!.series.length + i,
          markLine: this.markLine('acceleration', i, series.accelerationLimits),
          markPoint: this.markers(i, 'acceleration'),
        });

        // Data series for the clickable control points.
        this.seriesByIndex[options.series.length] = series.name;
        options.series.push({
          ...this.controlSeriesDefaults(0, i),
          name: `${series!.name} position control points`,
          datasetIndex: controlDataSetIndex + i,
        });

        this.seriesByIndex[options.series.length] = series.name;
        options.series.push({
          ...this.controlSeriesDefaults(1, i),
          name: `${series!.name} velocity control points`,
          datasetIndex: controlDataSetIndex + devicePath!.series.length + i,
        });

        this.seriesByIndex[options.series.length] = series.name;
        options.series.push({
          ...this.controlSeriesDefaults(2, i),
          name: `${series!.name} acceleration control points`,
          datasetIndex: controlDataSetIndex + 2 * devicePath!.series.length + i,
        });
      }
    });


    const style: React.CSSProperties = { width: '100%', height: chartTop(3) + 4 * TOP_MARGIN };

    return <div className="time-series-view">
      <div className="menu-bar">
        <Filename/>
        <div className="visibility">
          {userPath?.series.map((series, i) => <div key={i} className="series-visibility">
            {visibleSeries[i]
              ? <Icons.Show onClick={() => actions.setTimeSeriesVisible(i, false)}/>
              : <Icons.Hide onClick={() => actions.setTimeSeriesVisible(i, true)}/>}
            <div className="swatch" style={{ backgroundColor: seriesColor(i) }}/>
            <Text>{series.name}</Text>
          </div>)}
        </div>
        <div className="icon-menu">
          <Icons.Scale title="Reset Zoom" onClick={this.resetZoom}/>
          <Icons.Download title="Save Image" onClick={this.saveToImage}/>
        </div>
      </div>
      <div className="chart-area"
        ref={this.viewPort}
        onMouseDownCapture={this.onMouseDownCapture}
        onMouseMove={this.onMouseMove}
        onWheel={this.onMouseWheel}>

        <ReactECharts
          ref={this.echarts}
          option={options}
          notMerge={true}
          style={style}
          opts={{
            renderer: 'canvas',
          }}
          onEvents={{
            dataZoom: this.onDataZoom,
            selectchanged: this.onChartDataSelected,
          }}/>
      </div>
    </div>;
  }


  private discreteSeriesDefaults(chart: number, seriesIndex: number): object {
    return {
      type: 'line',
      xAxisIndex: chart,
      yAxisIndex: chart,
      color: seriesColor(seriesIndex),
      symbol: 'none',
    };
  }


  private controlSeriesDefaults(chart: number, seriesIndex: number): object {
    return {
      type: 'scatter',
      xAxisIndex: chart,
      yAxisIndex: chart,
      color: seriesColor(seriesIndex),
      symbol: 'circle',
      symbolSize: (data: number[]) => data[2] ? 15 : 10,
      z: 3,
      tooltip: {
        formatter: this.formatControlPointToolTip,
        trigger: 'item',
      },
    };
  }


  private markLine(quantity: 'position' | 'velocity' | 'acceleration', styleIndex: number, limits?: Span): unknown {
    const { userPath, playbackTime } = this.props;
    if (!limits || !userPath) {
      return null;
    }

    let units: Units = Units.NATIVE;
    switch (quantity) {
      case 'position':
        units = userPath.positionUnit ?? Units.NATIVE;
        break;
      case 'velocity':
        units = userPath.velocityUnit ?? Units.NATIVE;
        break;
      case 'acceleration':
        units = derivativeUnit(userPath.velocityUnit) ?? Units.NATIVE;
        break;
    }

    const data: unknown[] = [];
    if (quantity === 'acceleration' && limits.min !== limits.max) {
      for (const sign of [-1, 1]) {
        data.push({
          yAxis: sign * limits.max,
          name: `device acceleration limit: ${(sign * limits.max).toFixed(2)} ${getUnitLabel(units)}`,
        });
        data.push({
          yAxis: sign * limits.min,
          name: `device deceleration limit: ${(sign * limits.min).toFixed(2)} ${getUnitLabel(units)}`,
        });
      }
    } else {
      const quantityLabel = quantity === 'acceleration' ? 'acceleration/deceleration' : quantity;
      data.push({
        yAxis: limits.max,
        name: `device ${quantityLabel} limit: ${limits.max.toFixed(2)} ${getUnitLabel(units)}`,
      });
      data.push({
        yAxis: limits.min,
        name: `device ${quantityLabel} limit: ${limits.min.toFixed(2)} ${getUnitLabel(units)}`,
      });
    }

    if (playbackTime !== null) {
      data.push({
        xAxis: playbackTime!,
        lineStyle: {
          color: Colors.grey,
          width: 1,
          type: 'dotted',
        },
      });
    }

    return {
      symbol: 'none',
      label: {
        show: false,
      },
      lineStyle: {
        type: [4, 12],
        dashOffset: 4 * (styleIndex % 4),
        width: 2,
      },
      data,
    };
  }


  private markers(seriesIndex: number, quantity: 'position' | 'velocity' | 'acceleration'): unknown {
    const { userPath, bugs, deviceOptions, devicePosition, playbackTime } = this.props;

    const filters = {
      position: ['Length', 'Angle'],
      velocity: ['Velocity', 'Angular Velocity'],
      acceleration: ['Acceleration', 'Angular Acceleration'],
    };

    const matchingBugs = bugs.filter(bug => bug.seriesIndex === seriesIndex &&
      _.some(filters[quantity], filter => bug.dimension === filter));

    let deviceCursor: unknown = null;
    if (userPath && this.props.playbackTime !== null && quantity === 'position') {
      deviceCursor = {
        coord: [playbackTime!.toString(), devicePosition[deviceOptions.axes[userPath?.series[seriesIndex].name]]],
        symbol: 'rect',
        symbolSize: 6,
      };
    }

    if (!matchingBugs.length && !deviceCursor) {
      return null;
    }

    const data: unknown[] = matchingBugs.map(bug => ({
      coord: [bug.time.toString(), bug.value],
      name: bug.message,
      value: bug.type === LimitType.SOFT_LIMIT ? '!' : '×',
      itemStyle: {
        color: bug.type === LimitType.SOFT_LIMIT ? Colors.yellow : Colors.justRed,
      },
    }));

    if (deviceCursor) {
      data.push(deviceCursor);
    }

    return {
      symbolSize: 30,
      label: {
        color: 'white',
        fontWeight: 'bold',
      },
      data,
    };
  }


  private formatToolTip = (params: { seriesIndex: number; componentType: string; data: { name: string } }): string | null => {
    const seriesName = this.seriesByIndex[params.seriesIndex];
    if (params.componentType === 'markLine') {
      return `${seriesName} ${params.data.name}`;
    } else if (params.componentType === 'markPoint') {
      return params.data.name;
    }
    return null;
  };


  private formatControlPointToolTip = (params: { seriesIndex: number; dataIndex: number }): string | null => {
    const { userPath } = this.props;
    if (!userPath) {
      return null;
    }

    const component = <TimelineControlPointTooltip
      userPath={userPath}
      seriesName={this.seriesByIndex[params.seriesIndex]}
      selectedPointIndex={params.dataIndex}/>;

    if (!component) {
      return null;
    }

    return renderToStaticMarkup(component);
  };
}


export const TimeSeries = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    userPath: selectUserPath(state),
    devicePath: selectDevicePath(state),
    visibleSeries: selectTimeSeriesVisibility(state),
    selectedPoint: selectSelectedPointIndex(state),
    bugs: selectBugs(state),
    devicePosition: selectDevicePosition(state),
    deviceOptions: selectDeviceOptions(state),
    playbackTime: selectPlaybackTime(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionDefinitions, dispatch),
  }),
  null,
  { forwardRef: true }
)(TimeSeriesBase);
