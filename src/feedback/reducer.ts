import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { FeedbackModalState } from './types';

export interface State {
  anonymous: boolean;
  email: string;
  text: string;
  modalState: FeedbackModalState;
  sendError: string | null;
}

const initialState: State = {
  anonymous: false,
  email: '',
  text: '',
  modalState: 'closed',
  sendError: null,
};

const setAnonymous = (state: State, { anonymous }: ActionsToPayloads[ActionTypes.SET_ANONYMOUS]): State => ({
  ...state,
  anonymous,
});

const setEmail = (state: State, { email }: ActionsToPayloads[ActionTypes.SET_EMAIL]): State => ({
  ...state,
  email,
});

const setText = (state: State, { text }: ActionsToPayloads[ActionTypes.SET_TEXT]): State => ({
  ...state,
  text,
});

const setModalState = (state: State, { modalState }: ActionsToPayloads[ActionTypes.SET_MODAL_STATE]) => ({
  ...state,
  modalState,
});

const sendFeedback = (state: State): State => ({
  ...state,
  modalState: 'sending',
  sendError: null,
});

const sendFeedbackDone = (state: State): State => ({
  ...state,
  modalState: 'sent',
  text: '',
});

const sendFeedbackErr = (state: State, { error }: ActionsToPayloads[ActionTypes.SEND_FEEDBACK_ERR]): State => ({
  ...state,
  modalState: 'error',
  sendError: error,
});

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.SET_MODAL_STATE]: setModalState,
  [ActionTypes.SEND_FEEDBACK]: sendFeedback,
  [ActionTypes.SEND_FEEDBACK_DONE]: sendFeedbackDone,
  [ActionTypes.SEND_FEEDBACK_ERR]: sendFeedbackErr,

  [ActionTypes.SET_ANONYMOUS]: setAnonymous,
  [ActionTypes.SET_EMAIL]: setEmail,
  [ActionTypes.SET_TEXT]: setText,
}, initialState);
