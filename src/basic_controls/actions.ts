import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

import type { ProductInfo, Moveable, MotionAxisInfo } from './types';
import type { ProductError } from './errors/error';

export enum ActionTypes {
  FETCH_DEVICE_INFO = 'BASIC_CONTROL-FETCH_DEVICE_INFO',
  FETCH_DEVICE_INFO_DONE = 'BASIC_CONTROL-FETCH_DEVICE_INFO_DONE',

  CHOOSE_ENTITY = 'BASIC_CONTROL-CHOOSE_ENTITY',
  SET_ERROR = 'BASIC_CONTROLS-SET_ERROR',
  PIN_PRODUCT = 'BASIC_CONTROLS-PIN_PRODUCT',
  SET_IO_EXPANDED = 'BASIC_CONTROLS-SET_IO_EXPANDED',

  ENABLE_DRIVER = 'BASIC_CONTROLS-ENABLE_DRIVER',

  SET_MOVE_SIN_MODAL = 'BASIC_CONTROLS-SET_MOVE_SIN_MODAL',

  SET_TRAVEL_LIMIT_MODAL = 'BASIC_CONTROLS-SET_TRAVEL_LIMIT_MODAL',
}

export interface ActionsToPayloads {
  [ActionTypes.FETCH_DEVICE_INFO]: { key: EntityKey };
  [ActionTypes.FETCH_DEVICE_INFO_DONE]: { key: EntityKey; info: ProductInfo };

  [ActionTypes.CHOOSE_ENTITY]: { key: EntityKey | null };
  [ActionTypes.SET_ERROR]: { entity: EntityKey; error: ProductError | null };
  [ActionTypes.PIN_PRODUCT]: { key: EntityKey; pin: boolean };
  [ActionTypes.SET_IO_EXPANDED]: { key: EntityKey; expanded: boolean };

  [ActionTypes.ENABLE_DRIVER]: { moveable: Moveable; enable: boolean };
  [ActionTypes.SET_MOVE_SIN_MODAL]: { moveable: Moveable | null };
  [ActionTypes.SET_TRAVEL_LIMIT_MODAL]: { moveable: MotionAxisInfo | null };
}

type K = keyof ActionsToPayloads;
const buildAction = (type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actionDefinitions = {
  fetchDeviceInfo: (key: EntityKey) => buildAction(ActionTypes.FETCH_DEVICE_INFO, { key }),
  fetchDeviceInfoDone: (key: EntityKey, info: ProductInfo) => buildAction(ActionTypes.FETCH_DEVICE_INFO_DONE, { key, info }),

  chooseEntity: (key: EntityKey | null) => buildAction(ActionTypes.CHOOSE_ENTITY, { key }),
  setError: (entity: EntityKey, error: ProductError | null) => buildAction(ActionTypes.SET_ERROR, { entity, error }),
  pinProduct: (key: EntityKey, pin = true) => buildAction(ActionTypes.PIN_PRODUCT, { key, pin }),
  setIoExpanded: (key: EntityKey, expanded = true) => buildAction(ActionTypes.SET_IO_EXPANDED, { key, expanded }),

  enableDriver: (moveable: Moveable, enable = true) => buildAction(ActionTypes.ENABLE_DRIVER, { moveable, enable }),

  setMoveSinModal: (moveable: Moveable | null) => buildAction(ActionTypes.SET_MOVE_SIN_MODAL, { moveable }),
  setTravelLimitModal: (moveable: MotionAxisInfo | null) => buildAction(ActionTypes.SET_TRAVEL_LIMIT_MODAL, { moveable }),
};
