import _ from 'lodash';
import type { ProcessControllerMode as Mode, ProcessControllerSource } from '@zaber/motion/product';

import type { MeasurementOK } from '../units';

import type { SettingsMap } from './settings';

export type ProcessInfo = {
  number: number;
  mode: Mode;
  /** The input source to provide feedback for this process. */
  source: ProcessControllerSource;
  /** The settings that are relevant to this process */
  settings: SettingsMap;
  error: { title: string; message: string } | null;
};

export type ControllerInfo = {
  temperatures: Record<number, MeasurementOK>;
  analogIns: Record<number, MeasurementOK>;
  digitalIns: boolean[];
  digitalOuts: boolean[];
  bridges: Record<number, { on: boolean; reading: boolean }>;
  /** Whether each process is on */
  states: Record<number, { on: boolean; reading: boolean }>;
  /** The error flags on a given process */
  faults: Partial<Record<number, Set<string>>>;
  /** The voltage output per process */
  voltsOut: Record<number, number>;
  /** The amps output per process */
  currentOut: Record<number, number>;
};

/** A reading at a given time */
export type Reading = {
  time: number;
  measured: number;
  setpoint: number;
  output: number;
};

export const PROCESS_COUNT = 4;
export const PROCESS_NUMBERS = _.range(1, PROCESS_COUNT + 1);
export const PROCESS_GROUPS = _.range(1, PROCESS_COUNT + 1, 2).map(process => [process, process + 1] as const);
