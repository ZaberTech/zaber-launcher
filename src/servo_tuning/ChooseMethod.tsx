import React from 'react';
import { MinorMenu } from '@zaber/react-library';
import classNames from 'classnames';
import { match } from 'ts-pattern';

import { useActions } from '../utils';

import { methodNames, methods } from './names';
import { actionDefinitions } from './actions';
import { Advanced } from './tuners/Advanced';
import { PID } from './tuners/PID';
import type { Chosen, ServoTuningAxisInfo } from './types';
import { Simple } from './tuners/Simple';
import { LoadPreset } from './tuners/LoadPreset';
import { ImportExport } from './saved_rows/ImportExport';

interface ChooseMethodProps {
  chosen: Chosen;
  info: ServoTuningAxisInfo;
}

export const ChooseMethod: React.FC<ChooseMethodProps> = ({ chosen }) => {
  const actions = useActions(actionDefinitions);

  return <div className="choose-method">
    <div className="methods">
      <MinorMenu barStyle="horizontal">
        {methods.map(method => (
          <span key={method} className={classNames({ active: method === chosen.method })}>
            <MinorMenu.Item onClick={() => actions.chooseTuning({ ...chosen, method })}>{methodNames[method]}</MinorMenu.Item>
          </span>
        ))}
      </MinorMenu>
    </div>
    <div className="servo-tuning">
      {match(chosen.method)
        .with('advanced', () => <Advanced {...chosen}/>)
        .with('pid', () => <PID {...chosen}/>)
        .with('simple', () => <Simple {...chosen}/>)
        .with('load', () => <LoadPreset {...chosen}/>)
        .with('csv', () => <ImportExport {...chosen}/>)
        .exhaustive()
      }
    </div>
  </div>;
};
