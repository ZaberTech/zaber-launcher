import { createSelector } from 'reselect';
import { tryAccess } from '@zaber/toolbox';

import { selectConnections, selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import { selectTerminal } from '../store';
import { EntityKeyType, extractDeviceKey, getAxisNumber, getDeviceAddress, getEntityType, tryExtractConnectionKey } from '../keys';

import { getCommandSuggestions, getCommandWithMeasurements, makeProtocolLink } from './utils';


export const selectConnection = createSelector(selectTerminal, selectConnections,
  (state, connections) => tryAccess(connections, tryExtractConnectionKey(state.selectedKey)));
export const selectSelectedKey = createSelector(selectTerminal, state => state.selectedKey);
export const selectConnectionState = createSelector(selectTerminal, state => state.connectionState);
export const selectShowExternal = createSelector(selectTerminal, state => state.showExternal);
export const selectCurrentCommand = createSelector(selectTerminal, state =>
  state.commandHistoryIndex == null ? state.commandInput : state.commandHistory[state.commandHistoryIndex]);
export const selectSelectedSuggestion = createSelector(selectTerminal, state => state.selectedSuggestion);
export const selectIsCurrentCommandFromHistory = createSelector(selectTerminal, state => state.commandHistoryIndex != null);
export const selectConnectionHistory = createSelector(selectTerminal, selectConnection, selectShowExternal,
  (state, connection, showExternal) =>
    tryAccess(state.history, connection?.key)?.filter(historyItem => showExternal || historyItem.local) ?? []);
export const selectKeyHistory = createSelector(selectConnectionHistory, selectSelectedKey,
  (history, selectedKey) =>
    history.filter(historyItem => {
      switch (getEntityType(selectedKey)) {
        case EntityKeyType.AXIS: {
          if (getEntityType(historyItem.deviceOrAxisEntityKey) === EntityKeyType.AXIS) {
            return historyItem.deviceOrAxisEntityKey === selectedKey;
          } else if (historyItem.deviceOrAxisEntityKey != null && selectedKey != null) {
            return historyItem.deviceOrAxisEntityKey === extractDeviceKey(selectedKey);
          }
          return true;
        }
        case EntityKeyType.DEVICE:
          if (historyItem.deviceOrAxisEntityKey != null) {
            return extractDeviceKey(historyItem.deviceOrAxisEntityKey) === selectedKey;
          }
          return true;
        default:
          return true;
      }
    }));
export const selectCommandBatch = createSelector(selectTerminal, state => state.commandBatch);
export const selectProtocolLink = createSelector(selectSelectedKey, selectIdentifiedDevices, selectIdentifiedAxes, makeProtocolLink);
export const selectCommandLists = createSelector(selectTerminal, state => state.commandLists);
export const selectCommandList = createSelector(selectSelectedKey, selectCommandLists, (selectedKey, commandLists) =>
  tryAccess(commandLists, selectedKey) ?? []);
export const selectCommandWithMeasurements = createSelector(selectCurrentCommand, selectCommandList, (command, commandList) =>
  getCommandWithMeasurements(commandList, command));
export const selectCommandSuggestions = createSelector(
  selectCommandList,
  selectCurrentCommand,
  (commandList, currentCommand) => getCommandSuggestions(commandList, currentCommand.split(/\s+/))
);
export const selectSuggestedCommand = createSelector(selectSelectedSuggestion, selectCommandSuggestions,
  (index, commands) => commands.at(index ?? commands.length));
export const selectContextPrefix = createSelector(selectSelectedKey, entityKey => {
  if (entityKey == null) {
    return '/ ';
  }
  switch (getEntityType(entityKey)) {
    case EntityKeyType.DEVICE:
      return `/${getDeviceAddress(entityKey)} `;
    case EntityKeyType.AXIS:
      return `/${getDeviceAddress(entityKey)} ${getAxisNumber(entityKey)} `;
    default:
      return '/ ';
  }
});
