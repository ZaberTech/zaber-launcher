import React from 'react';
import { RenderResult, render } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { wrapWithNewStore, wrapWithRouter } from '../test';
import { MessageRoutersService } from '../message_router';

import { MessageRoutersServiceMock } from './mocks';
import { LocalShare } from './LocalShare';

const TestLocalShare = wrapWithNewStore(wrapWithRouter(LocalShare));

let wrapper: RenderResult;

let container: ReturnType<typeof createContainer>;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

test('renders', () => {
  wrapper = render(<TestLocalShare/>);
});
