import _ from 'lodash';
import React from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';

const JoySvg = () => (<div>
  {_.range(8).map(i => <div key={i} className={`xjoy_svg__button${i + 1}`}>Key {i + 1}</div>)}
</div>);

jest.mock('../assets/xjoy.svg', () => JoySvg);

import { JoystickPicture, PICTURE_ACTIVE_CLASS } from './Picture';

const onClickMock = jest.fn((_n: number) => undefined);

let wrapper: RenderResult;

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  onClickMock.mockClear();
});

test('marks the selected key', () => {
  wrapper = render(<JoystickPicture activeButton={1}/>);
  expect(wrapper.getByText('Key 1')).toHaveClass(PICTURE_ACTIVE_CLASS);
  expect(wrapper.getByText('Key 2')).not.toHaveClass(PICTURE_ACTIVE_CLASS);

  wrapper.rerender(<JoystickPicture activeButton={2}/>);
  expect(wrapper.getByText('Key 1')).not.toHaveClass(PICTURE_ACTIVE_CLASS);
  expect(wrapper.getByText('Key 2')).toHaveClass(PICTURE_ACTIVE_CLASS);
});

test('it calls onClick handler when button gets pressed', () => {
  wrapper = render(<JoystickPicture onClick={onClickMock}/>);

  fireEvent.click(wrapper.getByText('Key 1'));
  expect(onClickMock).toHaveBeenCalledWith(1);
  fireEvent.click(wrapper.getByText('Key 2'));
  expect(onClickMock).toHaveBeenCalledWith(2);
});
