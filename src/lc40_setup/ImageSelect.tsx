import { Flex, Icons } from '@zaber/react-library';
import classNames from 'classnames';
import React from 'react';


interface ImageOptionProps {
  name: string;
  selected: boolean;
  onSelect: (name: string) => void;
  imageSource: string;
  altText: string;
  label: string;
}

const ImageOption: React.FC<ImageOptionProps> = ({ name, selected, onSelect, imageSource, altText, label }) =>
  <button
    title={name}
    className={classNames({ selected }, 'image-select-button')}
    onClick={() => onSelect(name)}>
    <img src={imageSource} alt={altText}/>
    <div className="label">{selected && <Icons.Confirmation/>}{label}</div>
  </button>;

interface ImageSelectOption {
  name: string;
  imageSource: string;
  altText: string;
  label: string;
}

interface ImageSelectProps {
  selected: string;
  options: ImageSelectOption[];
  onSelect: (name: string) => void;
}

export const ImageSelect: React.FC<ImageSelectProps> = ({ selected, options, onSelect }) =>
  <Flex.Row className="image-select-options">
    {options.map((option, i) =>
      <ImageOption
        key={i}
        name={option.name}
        selected={selected === option.name}
        onSelect={onSelect}
        imageSource={option.imageSource}
        altText={option.altText}
        label={option.label}
      />)
    }
  </Flex.Row>;
