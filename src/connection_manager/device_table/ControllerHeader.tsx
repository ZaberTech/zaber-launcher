import React, { HTMLProps } from 'react';
import { Text } from '@zaber/react-library';
import classNames from 'classnames';

import { DeviceNumber } from '../../components';
import type { DeviceInfo, DeviceInfoWithAxes, IdentifiedDeviceInfo } from '../types';
import { deviceDescription, isIdentified } from '../utils';
import { Thumbnail } from '../../components/Thumbnail';
import { fwToString } from '../../devices';

interface Props extends HTMLProps<HTMLDivElement> {
  device: DeviceInfoWithAxes;
  renderContent?: (device: IdentifiedDeviceInfo) => React.ReactNode;
}

export const deviceLabel: (device: DeviceInfo) => React.ReactNode = ({ nonIdentifiedInfo, identity, label }) => {
  if (identity) {
    return label ?? identity.name;
  } else {
    return `Unknown Device ${nonIdentifiedInfo!.deviceId}`;
  }
};

export const SerialNumber: React.FC<{ device: IdentifiedDeviceInfo }> =
({ device }) => <>SN: {device.identity.serialNumber}&ensp;</>;

export const FirmwareVersion: React.FC<{ device: IdentifiedDeviceInfo }> =
({ device }) => <>FW: {fwToString(device.identity.firmwareVersion)}&ensp;</>;

const defaultContent = (device: IdentifiedDeviceInfo) => <SerialNumber device={device}/>;

export const ControllerHeader: React.FC<Props> = ({ device, className, renderContent = defaultContent, ...rest }) => {
  const description = deviceDescription(device);
  return (
    <div className={classNames('controller-header', className)} {...rest}>
      <div className="address">
        <Text t={Text.Type.H5}><DeviceNumber>{device.address}</DeviceNumber></Text>
        <span/>
      </div>
      <div className="device-picture">
        <Thumbnail deviceOrPeripheralId={device.identity?.deviceId} title={description} altExt="device"/>
      </div>
      <div className="device-name">
        <div className="names">
          <Text t={Text.Type.H5}>
            {deviceLabel(device)}
          </Text>
          {device.label && device.identity && <Text t={Text.Type.Helper}>{device.identity.name}</Text>}
        </div>
        <span>
          {isIdentified(device) && renderContent(device)}
        </span>
      </div>
    </div>
  );
};
