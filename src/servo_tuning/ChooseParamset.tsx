import React, { useCallback, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Button, ButtonRow, ContextMenu, Flex, Icons, MinorMenu, NoticeBanner, PopUp, Text } from '@zaber/react-library';
import classNames from 'classnames';
import { match } from 'ts-pattern';
import { ServoTuningParamset } from '@zaber/motion/ascii';

import type { EntityKey } from '../keys';
import { useActions } from '../utils';
import { NoContentMessage } from '../components';
import { OneTimeMessage } from '../help';
import { ConnectionsView } from '../connection_manager';

import { actionDefinitions } from './actions';
import { ChooseMethod } from './ChooseMethod';
import { TITLE, type Chosen } from './types';
import { selectDeviceInfo } from './selectors';
import { Settings } from './components/Settings';
import { paramsetNames, userParamsets } from './names';

function onOpenOscilloscope(key: EntityKey) {
  const urlKey = encodeURIComponent(key);
  const currentUrl = new URL(window.location.href);
  currentUrl.hash = `#/oscilloscope?${ConnectionsView.SELECT_PARAM}=${urlKey}`;
  window.open(currentUrl.toString());
}

export const ChooseParamset: React.FC<Chosen> = chosen => {
  const actions = useActions(actionDefinitions);
  const deviceInfo = useSelector(selectDeviceInfo);
  const enabled = deviceInfo?.driverEnabled;

  const catchKey = useCallback((event: KeyboardEvent) => {
    if (enabled != null && event.key === ' ') {
      actions.setDriverEnabled(chosen.entity, !enabled);
    }
  }, [enabled]);

  const checkDriverEnabled = () => {
    actions.setDriverState(chosen.entity);
  };

  useEffect(() => {
    checkDriverEnabled();
    window.addEventListener('focus', checkDriverEnabled);
    return () => { window.removeEventListener('focus', checkDriverEnabled) };
  }, []);

  const openOscilloscope = () => { onOpenOscilloscope(chosen.entity) };

  useEffect(() => {
    document.addEventListener('keydown', catchKey);
    return () => document.removeEventListener('keydown', catchKey);
  }, [catchKey]);

  if (deviceInfo == null) {
    return <NoContentMessage title={TITLE} message="Fetching Device Servo Tuning Information..." type="working"/>;
  }

  const { startupParamset, settings } = deviceInfo;
  return <div className="choose-paramset">
    <OneTimeMessage messageId="servoTuningCanCauseBadThings"
      message={dismiss => (
        <NoticeBanner type="warning" closer={dismiss} className="servo-tuning-warning">
          Some tuning values can cause the device to make loud noise, move erratically, fail to move, or oscillate.
          It is recommended you start by reading some known-good values from the device and then modifying them.
          Use the "Disable driver" button to stop the device if it behaves unexpectedly.
        </NoticeBanner>
      )}
    />
    <Settings {...{ entity: chosen.entity, settings }}/>
    <MinorMenu className="paramset-list">
      {userParamsets.map(menuParamset => {
        const name = paramsetNames[menuParamset];
        return <div key={menuParamset} className={classNames('paramset', { active: menuParamset === chosen.paramset })}>
          <MinorMenu.Item onClick={() => actions.chooseTuning({ ...chosen, paramset: menuParamset })}>
            <Text className="paramset-label">{name}</Text>
            {startupParamset === menuParamset && (
              <PopUp className="power-on-popup"
                triggerAction="hover"
                trigger={() => <Icons.DotHollow className="power-on"/>}>Power-on Preset</PopUp>
            )}
            <ContextMenu position="bottom" align="end" title={`More Actions for ${name}`}>
              {match(menuParamset)
                .with(ServoTuningParamset.LIVE, () => null)
                .with(startupParamset, () => <ContextMenu.Item disabled>Is Power-on Preset</ContextMenu.Item>)
                .otherwise(() => (
                  <ContextMenu.Item
                    onClick={() => actions.changeStartupPreset(chosen.entity, menuParamset)}
                    title={`Sets ${name} to be the one used when the device is powered up`}
                  >
                  Set Power-on Preset
                  </ContextMenu.Item>
                ))
              }
              {menuParamset !== ServoTuningParamset.LIVE && (
                <ContextMenu.Item
                  onClick={() => actions.loadParamset(chosen.entity, ServoTuningParamset.LIVE, menuParamset)}
                  title={`Copy the values from ${name} to the live set`}
                >
                Copy Tuning to Live
                </ContextMenu.Item>
              )}
              <ContextMenu.Item
                onClick={() => actions.loadParamset(chosen.entity, menuParamset, ServoTuningParamset.DEFAULT)}
                title={`Fill ${name} with the default values for the selected device`}
              >
              Reset To Default
              </ContextMenu.Item>
            </ContextMenu>
          </MinorMenu.Item>
        </div>;
      })}
    </MinorMenu>
    <div className="paramset-pane">
      {chosen.paramset == null ?
        <NoContentMessage title="Select a Paramset"/> :
        <ChooseMethod chosen={chosen} info={deviceInfo}/>
      }
    </div>
    <div className="available-controls" data-testid="servo-tuning-buttons">
      <Text t={Text.Type.Helper}>Press the spacebar to quickly { deviceInfo.driverEnabled ? 'disable' : 'enable' } the driver</Text>
      <ButtonRow>
        <Flex.Spacer/>
        {deviceInfo.driverEnabled ?
          <Button onClick={() => actions.setDriverEnabled(chosen.entity, false)}>Disable Driver</Button> :
          <Button onClick={() => actions.setDriverEnabled(chosen.entity, true)}>Enable Driver</Button>
        }
        <Button onClick={openOscilloscope} color="grey">Open Oscilloscope</Button>
      </ButtonRow>
    </div>
  </div>;
};
