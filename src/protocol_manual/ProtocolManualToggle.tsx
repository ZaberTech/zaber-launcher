import React from 'react';
import { Icons, SvgIconProps } from '@zaber/react-library';

import type { EntityKey } from '../keys';
import { omit } from '../utils';

import { useSettingManual } from './hooks';

type Props = {
  deviceOrAxisKey: EntityKey | null | undefined;
  setting: string;
} & SvgIconProps;

export const ProtocolManualToggle: React.FC<Props> = ({ activated, ...rest }) => (
  <Icons.QuestionMark
    className="protocol-manual-trigger"
    title={activated ? 'Hide documentation' : 'Show documentation'}
    activated={activated}
    {...omit(rest, 'deviceOrAxisKey', 'setting')}
  />
);

/** Returns a Protocol Manual if there are docs for this setting and renders nothing otherwise */
export const ToggleIfProtocolManual: React.FC<Props> = ({ deviceOrAxisKey, setting, ...rest }) => {
  const manual = useSettingManual(deviceOrAxisKey, setting);
  if (manual == null || 'error' in manual) {
    return null;
  }
  return <ProtocolManualToggle deviceOrAxisKey={deviceOrAxisKey} setting={setting} {...rest}/>;
};
