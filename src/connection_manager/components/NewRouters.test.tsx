
import React from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../../container';
import { wrapWithNewStore } from '../../test';
import { actions as managerActions } from '../actions';
import { RouterType } from '../types';
import { makeCloudUrl } from '../utils';

import { NewRouters } from './NewRouters';

const addRouterMock = jest.fn(() => ({ type: 'NOTHING', payload: undefined }));
managerActions.addRouter = addRouterMock;
const onAdded = jest.fn();

const TestNewRouters = wrapWithNewStore(NewRouters);

let wrapper: RenderResult;

const ROUTER_URL = makeCloudUrl('realm1', 'id1');

beforeEach(() => {
  createContainer();

  const router2Url = makeCloudUrl('realm2', 'id2');
  wrapper = render(<TestNewRouters routers={[{
    name: 'Router1',
    type: RouterType.Cloud,
    url: ROUTER_URL,
  }, {
    name: 'Router2',
    type: RouterType.Cloud,
    url: router2Url,
  }]}
  onAdded={onAdded}
  preloadedState={{
    connectionManager: {
      routers: {
        [router2Url]: { type: RouterType.Cloud, url: router2Url },
      },
    },
  }}/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();

  onAdded.mockClear();
});

test('adds router upon clicking', async () => {
  fireEvent.click(wrapper.getByText('Create Connection'));

  const router = expect.objectContaining({
    type: RouterType.Cloud,
    url: ROUTER_URL,
  });
  expect(addRouterMock).toHaveBeenCalledWith(router);
  expect(onAdded).toHaveBeenCalledWith(router);
});

test('indicates already added routers', async () => {
  wrapper.getByText('Already exists');
});
