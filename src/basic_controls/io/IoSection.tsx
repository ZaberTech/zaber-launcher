import _ from 'lodash';
import React, { useEffect } from 'react';
import { NoticeBanner, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import type { EntityKey } from '../../keys';
import type { IoInfo } from '../types';
import { useActions } from '../../utils';
import { usePolledDigitalIo } from '../../polling/hooks';

import { DigitalInput } from './DigitalInput';
import { DigitalOutput } from './DigitalOutput';
import { AnalogInput } from './AnalogInput';
import { AnalogOutput } from './AnalogOutput';
import { selectDeviceStates } from './selectors';
import { actionDefinitions } from './actions';

type IoSectionProps = {
  deviceKey: EntityKey;
  info: IoInfo;
};

const portVisualization = (portType: string) => {
  switch (portType) {
    case '3': return 'Digital Input';
    case '4': return 'Digital Output';
    case '1': return 'Analog Input';
    case '2': return 'Analog Output';
    default: return portType;
  }
};

export const IoSection: React.FC<IoSectionProps> = ({ deviceKey, info }) => {
  const error = useSelector(selectDeviceStates)[deviceKey]?.error;
  const labelErrors = useSelector(selectDeviceStates)[deviceKey]?.labelErrors;
  const actions = useActions(actionDefinitions);
  const inPorts = usePolledDigitalIo(deviceKey, 'in', info.di);
  const outPorts = usePolledDigitalIo(deviceKey, 'out', info.do);

  useEffect(() => {
    actions.loadLabelsDevice(deviceKey);
  }, []);

  return <div className="io">
    <div className="device">
      {info.di > 0 && <>
        <Text t={Text.Type.H5} className="di">Digital Inputs</Text>
        {_.range(1, info.di + 1).map(channel => (
          <DigitalInput key={channel} deviceKey={deviceKey} channel={channel} ports={inPorts}/>
        ))}
      </>}
      {info.do > 0 && <>
        <Text t={Text.Type.H5} className="do">Digital Outputs</Text>
        {_.range(1, info.do + 1).map(channel => (
          <DigitalOutput key={channel} deviceKey={deviceKey} channel={channel} ports={outPorts}/>
        ))}
      </>}
      {info.ai > 0 && <>
        <Text t={Text.Type.H5} className="ai">Analog Inputs (V)</Text>
        {_.range(1, info.ai + 1).map(channel => (
          <AnalogInput key={channel} deviceKey={deviceKey} channel={channel}/>
        ))}
      </>}
      {info.ao > 0 && <>
        <Text t={Text.Type.H5} className="ao">Analog Outputs (V)</Text>
        {_.range(1, info.ao + 1).map(channel => (
          <AnalogOutput key={channel} deviceKey={deviceKey} channel={channel}/>
        ))}
      </>}
      {(error != null || labelErrors != null) && <div className="errors">
        {error != null && (
          <NoticeBanner type="error" headline={error.message} closer={() => actions.setError(deviceKey, null)} collapsible>
            {error.details}
          </NoticeBanner>
        )}
        {labelErrors && Object.keys(labelErrors).length > 0 &&
        Object.entries(labelErrors).map(([key, errorObj]) => errorObj && (
          <NoticeBanner key={key} type="error" closer={() => actions.deleteLabelError(deviceKey, key)} collapsible>
            {errorObj.message} {portVisualization(key.split('-')[0])} channel {parseInt(key.split('-')[1], 10)}. {errorObj.details}.
          </NoticeBanner>
        ))}
      </div>}
    </div>
  </div>;
};
