import { CommandFailedException, type ascii } from '@zaber/motion';
import { throwUnexpectedError } from '@zaber/toolbox';
import type { SagaIterator } from 'redux-saga';
import { all, call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import { Axis, SettingConstants } from '@zaber/motion/ascii';

import {
  getAxis, getDevice, reloadDevices, ConnectionManagerActionTypes, IdentifiedAxisState, connectionManagerActions,
} from '../connection_manager';
import type { Action, RT } from '../utils';
import { makeAxisKey } from '../keys';
import { getNumericSetting } from '../devices';
import { getMovementUnits, MeasurementOK } from '../units';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import type { AxisDataLockstep, AxisLockstepInfo, LockstepAxesData, LockstepSelectAxisInfo } from './reducer';
import { DeviceStateLockstep, selectDeviceStateLockstep, selectLockstepHierarchy } from './selectors';

const letterArray = ['a', 'b', 'c', 'd'];

export function* lockstepConfigurationSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.WRITE_TWIST_TOLERANCE, writeTwistTolerance),
    takeEvery(ActionTypes.UPDATE_FETCHED_AXIS_DATA, updateFetchedAxisData),
    takeEvery(ConnectionManagerActionTypes.DEVICES_LOADED, updateFetchedAxisData),
    takeEvery(ActionTypes.BEGIN_CREATION, beginCreation),
    takeEvery(ActionTypes.BREAK_LOCKSTEP_GROUP, breakLockstepGroup),
    takeEvery(ActionTypes.CREATE_LOCKSTEP_GROUP, createLockstepGroup),
  ]);
}

function getNewGroupNumber(lockstepHierarchy: RT<typeof selectLockstepHierarchy>) {
  if (lockstepHierarchy == null || _.isEmpty(lockstepHierarchy)) {
    return 1;
  }
  const groupNumbers = Object.keys(lockstepHierarchy).map(key => +key);
  const maxGroupNumber = Math.max(...groupNumbers);
  for (let groupNumber = 1; groupNumber <= maxGroupNumber; groupNumber++) {
    if (!groupNumbers.includes(groupNumber)) {
      return groupNumber;
    }
  }
  return maxGroupNumber + 1;
}

async function fetchTwistTolerance(asciiDevice: ascii.Device, lockstepGroupNumber: number, axisIndex: number): Promise<number> {
  const response = await asciiDevice.genericCommand(`lockstep ${lockstepGroupNumber} get ${letterArray[axisIndex]} tolerance`);
  return parseInt(response.data, 10);
}

async function fetchOffset(asciiDevice: ascii.Device, axisNumber: number, primaryPosition: MeasurementOK): Promise<MeasurementOK | null> {
  const axis = asciiDevice.getAxis(axisNumber);
  const units = getMovementUnits(axis.identity.axisType)?.position;
  if (units !== primaryPosition.units) {
    return null;
  }
  const position = await asciiDevice.getAxis(axisNumber).getPosition(units);
  return { value: position - primaryPosition.value, units };
}

async function verifyLockstepState(device: DeviceStateLockstep, asciiDevice: ascii.Device): Promise<boolean> {
  let numGroups = 0;
  try {
    numGroups = await asciiDevice.settings.get(SettingConstants.LOCKSTEP_NUMGROUPS);
  } catch (err) {
    if (err instanceof CommandFailedException) {
      return true;
    }
    throw err;
  }

  for (let groupNumber = 1; groupNumber <= numGroups; groupNumber++) {
    const lockstep = asciiDevice.getLockstep(groupNumber);
    const isEnabled = await lockstep.isEnabled();
    const state = device.locksteps.find(group => group.groupNumber === groupNumber);
    if (isEnabled !== (state != null)) {
      return false;
    }

    if (state) {
      const axes = await lockstep.getAxisNumbers();
      if (!_.isEqual(axes, state.axisNumbers)) {
        return false;
      }
    }
  }

  return true;
}

async function getAxesLockstepInfo(device: DeviceStateLockstep, asciiDevice: ascii.Device): Promise<LockstepAxesData> {
  const lockstepDataEntriesPromise = device.locksteps.map(async (group): Promise<[number, AxisDataLockstep[]]> =>  {
    const primaryAxis = asciiDevice.getAxis(group.axisNumbers[0]);
    const primaryUnits = getMovementUnits(primaryAxis.identity.axisType)?.position;
    const primaryPosition = primaryUnits == null ? null : {
      value: await asciiDevice.getAxis(group.axisNumbers[0]).getPosition(primaryUnits), units: primaryUnits
    };
    const axesDataPromise: Promise<AxisDataLockstep>[] = group.axisNumbers.map(async (axisNumber, i) => {
      const lockstepInfo: AxisLockstepInfo = i === 0 ? {
        isPrimary: true,
      } : {
        isPrimary: false,
        offset: primaryPosition ? await fetchOffset(asciiDevice, axisNumber, primaryPosition) : null,
        twistTolerance: await fetchTwistTolerance(asciiDevice, group.groupNumber, i),
      };
      return {
        axisNumber,
        lockstepInfo,
      };
    });
    return [group.groupNumber, await Promise.all(axesDataPromise)];
  });
  return Object.fromEntries(await Promise.all(lockstepDataEntriesPromise));
}

function* updateFetchedAxisData(): SagaIterator {
  const device: ReturnType<typeof selectDeviceStateLockstep> = yield select(selectDeviceStateLockstep);
  if (device == null) { return }
  try {
    const asciiDevice: ascii.Device = yield call(getDevice, device.key);

    const isValid: RT<typeof verifyLockstepState> = yield call(verifyLockstepState, device, asciiDevice);
    if (!isValid) {
      yield put(connectionManagerActions.loadDevices(device.connection.key, false));
      return;
    }

    const lockstepData: RT<typeof getAxesLockstepInfo> = yield call(getAxesLockstepInfo, device, asciiDevice);
    yield put(actions.setAxesData(lockstepData));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setError(`Failed to fetch Axis Information: ${err}`, true));
  }
}

type BreakLockstepParams = Action<ActionsToPayloads[ActionTypes.BREAK_LOCKSTEP_GROUP]>;
function* breakLockstepGroup({ payload: { groupNumber } }: BreakLockstepParams): SagaIterator {
  const device: DeviceStateLockstep = yield select(selectDeviceStateLockstep);
  try {
    const asciiDevice: ascii.Device = yield call(getDevice, device.key);
    const lockstepGroup = asciiDevice.getLockstep(groupNumber);
    yield call([lockstepGroup, lockstepGroup.disable]);
    yield call(reloadDevices, device.connection.key);
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setError(`${err}`));
  }
}

type CreateLockstepParams = Action<ActionsToPayloads[ActionTypes.CREATE_LOCKSTEP_GROUP]>;
function* createLockstepGroup(createLockstepParams: CreateLockstepParams): SagaIterator {
  const device: DeviceStateLockstep = yield select(selectDeviceStateLockstep);
  const lockstepHierarchy: RT<typeof selectLockstepHierarchy> = yield select(selectLockstepHierarchy)!;
  const newGroupNumber = getNewGroupNumber(lockstepHierarchy);
  try {
    const asciiDevice: ascii.Device = yield call(getDevice, device.key);
    const lockstepGroup = asciiDevice.getLockstep(newGroupNumber);
    yield call([lockstepGroup, lockstepGroup.enable], ...createLockstepParams.payload.axisNumbers);
    yield put(actions.createLockstepDone(newGroupNumber));
    yield call(reloadDevices, device.connection.key);
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.createLockstepError(`${err}`));
  }
}

export const MOTOR_TYPE_STEPPER = 1;

async function getLockstepSelectAxisInfo(axis: IdentifiedAxisState, asciiAxis: Axis): Promise<LockstepSelectAxisInfo> {
  const units = getMovementUnits(axis.identity.axisType)?.position;
  const motorType = await getNumericSetting(asciiAxis, 'motor.type');
  if (units == null || motorType !== MOTOR_TYPE_STEPPER) {
    return { supported: false, axisNumber: axis.axisNumber };
  }
  const value = await getNumericSetting(asciiAxis, 'pos', units);
  if (value == null) {
    return { supported: false, axisNumber: axis.axisNumber };
  }
  const homed = await asciiAxis.isHomed();

  return { supported: true, axisNumber: axis.axisNumber, position: { value, units }, homed };
}

function* beginCreation(): SagaIterator {
  try {
    const device: DeviceStateLockstep = yield select(selectDeviceStateLockstep);

    const creationInfo: LockstepSelectAxisInfo[] = [];
    for (const axis of device.axes) {
      const axisNumber = axis.axisNumber;
      const asciiAxis: RT<typeof getAxis> = yield call(getAxis, makeAxisKey(device.key, axisNumber));
      const lockstepInfo: RT<typeof getLockstepSelectAxisInfo> = yield call(getLockstepSelectAxisInfo, axis, asciiAxis);
      creationInfo.push(lockstepInfo);
    }
    yield put(actions.receivedCreationInfo(creationInfo));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.createLockstepError(`${err}`));
  }
}

type WriteTwistToleranceParams = Action<ActionsToPayloads[ActionTypes.WRITE_TWIST_TOLERANCE]>;
function* writeTwistTolerance(writeTwistToleranceParams: WriteTwistToleranceParams): SagaIterator {
  try {
    const { twistTolerance, axisNumber } = writeTwistToleranceParams.payload;
    const device: DeviceStateLockstep = yield select(selectDeviceStateLockstep);
    const asciiDevice: ascii.Device = yield call(getDevice, device.key);
    const lockstepHierarchy: RT<typeof selectLockstepHierarchy> = yield select(selectLockstepHierarchy);
    const groupNumber = +_.findKey(lockstepHierarchy, axisNumbers => axisNumbers.includes(axisNumber))!;
    const lockstepGroup = asciiDevice.getLockstep(groupNumber);
    const axisNumbers: number[] = yield call([lockstepGroup, lockstepGroup.getAxisNumbers]);
    const command = `lockstep ${groupNumber} set ${letterArray[axisNumbers.indexOf(axisNumber)]} tolerance ${twistTolerance}`;
    yield call([asciiDevice, asciiDevice.genericCommand], command);
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setError(`${err}`));
  }
}
