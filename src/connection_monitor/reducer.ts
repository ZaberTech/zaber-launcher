import _ from 'lodash';

import { extractConnectionKey, removeEntriesWithKey, type EntityKey } from '../keys';
import { createReducer, changeDictionary } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';

interface ConnectionState {
  key: EntityKey;
  detected: boolean;
  detectedDevices: EntityKey[];
  error: string | null;
}

export const defaultConnectionState: Omit<ConnectionState, 'key'> = {
  detected: false,
  detectedDevices: [],
  error: null,
};

export interface State {
  connections: Record<string, ConnectionState>;
  devicesFF: Record<EntityKey, boolean>;
}

const initialState: State = {
  connections: {},
  devicesFF: {},
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const start: Reducer<ActionTypes.START> = (state, { connectionKey }) => ({
  ...state,
  connections: {
    ...state.connections,
    [connectionKey]: { key: connectionKey, ...defaultConnectionState },
  },
});
const stop: Reducer<ActionTypes.STOP> = (state, { connectionKey }) => ({
  ...state,
  connections: _.omit(state.connections, connectionKey),
});

const monitorData: Reducer<ActionTypes.MONITOR_DATA> = (state, { connectionKey, detectedDevices }) => ({
  ...state,
  connections: changeDictionary(state.connections, connectionKey,
    { detected: true, detectedDevices, error: null }),
});
const monitorErr: Reducer<ActionTypes.MONITOR_ERR> = (state, { connectionKey, error }) => ({
  ...state,
  connections: changeDictionary(state.connections, connectionKey,
    { detected: true, detectedDevices: [], error }),
});

const updateDevicesFF = (state: State, { connectionKey, deviceKeys }: ActionsToPayloads[ActionTypes.UPDATE_DEVICES_FF]) => ({
  ...state,
  devicesFF: {
    ...removeEntriesWithKey(state.devicesFF, extractConnectionKey, connectionKey),
    ...deviceKeys.reduce((acc, deviceKey) => ({
      ...acc,
      [deviceKey]: true,
    }), {})
  },
});


export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.START]: start,
  [ActionTypes.STOP]: stop,
  [ActionTypes.MONITOR_DATA]: monitorData,
  [ActionTypes.MONITOR_ERR]: monitorErr,
  [ActionTypes.UPDATE_DEVICES_FF]: updateDevicesFF,
}, initialState);
