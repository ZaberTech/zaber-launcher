import React, { useState } from 'react';
import { ButtonRowConfirmCancel, Icons, Loader, Modal, NoticeBanner, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../../utils';
import { actionDefinitions as actionDefinitionsBasicControls } from '../actions';
import { MotionAxisInfo } from '../types';
import { ExternalLink } from '../../components';
import { selectIdentifiedAxes } from '../../connection_manager';
import { commandExists } from '../../devices';

import { actionDefinitions as actionDefinitionsMovement } from './actions';


interface TravelLimitModalProps {
  moveable: MotionAxisInfo;
}

export const TravelLimitModal: React.FC<TravelLimitModalProps> = ({ moveable }) => {
  const actionsBasicControls = useActions(actionDefinitionsBasicControls);
  const actionsMovement = useActions(actionDefinitionsMovement);

  const identifiedAxes = useSelector(selectIdentifiedAxes);

  const [moving, setMoving] = useState(false);

  const identifiedAxis = identifiedAxes[moveable.key];
  const supportsCommand = identifiedAxes == null ? false : commandExists('tools findrange hardstop save', identifiedAxis);

  return <Modal
    className="travel-limit-modal"
    headerIcon={<Icons.HardwareModification/>}
    headerText="Set Travel Limits"
    small
    onRequestClose={moving ? undefined : () => actionsBasicControls.setTravelLimitModal(null)}
    buttons={<ButtonRowConfirmCancel
      confirmText="Detect Travel Limits"
      onConfirm={moving || !supportsCommand ?
        'disabled' :
        () => {
          setMoving(true);
          actionsMovement.issueCommand(moveable, { type: 'findrange hardstop' });
        }
      }
      onCancel={moving ? 'disabled' : () => actionsBasicControls.setTravelLimitModal(null)}
    >
      {moving && <Loader/>}
    </ButtonRowConfirmCancel>}
  >
    <Text>
      The X-ADR-AE stage features adjustable positive and negative bump stops on each axis,
      allowing the physical axis travel range to be restricted.
      You can use this tool to detect the new travel limits of the axis
      after changing the position of the bump stops.
    </Text>
    <Text>
      Please see the <ExternalLink url="https://www.zaber.com/manuals/X-ADR-AE#m-8-12-setting-travel-limits">
        X-ADR-AE manual
      </ExternalLink> for more information.
    </Text>
    <NoticeBanner type="warning">
      This action will move the axis until it collides with the bump stops.
      Ensure the bump stops are securely installed, otherwise the bearings may be damaged.
    </NoticeBanner>
    {!supportsCommand &&
      <NoticeBanner type="error">
        The current axis does not support this feature. A firmware update may be required.
      </NoticeBanner>
    }
  </Modal>;
};
