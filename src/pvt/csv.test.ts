import { Angle, AngularVelocity, Length, Time, Units, Velocity } from '@zaber/motion';

import { detectSeriesAndUnits, detectHeaders, getDataRows, parseCsv, formatCsv } from './csv';


describe('Parse', () => {
  it('converts numeric data to numbers', () => {
    const data = 'foo,bar,baz\r\n1,"2.3",four\r\n';
    const result = parseCsv(data);
    expect(result).toEqual([['foo', 'bar', 'baz'], [1, 2.3, 'four']]);
  });
});


describe('Format', () => {
  it('handles strings and numbers', () => {
    const data = [['foo', 'bar', 'baz'], [1, 2.3, 'four']];
    const result = formatCsv(data);
    expect(result).toMatch(/foo,bar,baz\s+1,2.3,four\s+/);
  });
});


describe('Header detection', () => {
  it('identifies initial consecutive rows with only non-numeric data', () => {
    const data = [['foo', 'bar', 'baz'], ['one', 'two', 'three'], [1, 2, 'three']];
    const result = detectHeaders(data);
    expect(result).toEqual([['foo', 'bar', 'baz'], ['one', 'two', 'three']]);
  });
});


describe('Data detection', () => {
  it('ignores specified number of rows', () => {
    const data = [['foo', 'bar', 'baz'], ['one', 'two', 'three'], [1, 2, 3]];
    const result = getDataRows(data, 2);
    expect(result).toEqual([[1, 2, 3]]);
  });

  it('throws if there is non-numeric data', () => {
    const data = [['foo', 'bar', 'baz'], [1, 'two', 3]];
    expect(() => getDataRows(data, 1)).toThrow(/Non-numeric/);
  });
});


describe('Data mapping', () => {
  it('produces a default mapping when there are no headers', () => {
    const map = detectSeriesAndUnits([], 5);
    expect(map).toEqual({
      timeColumn: 0,
      timeUnit: null,
      timeUnitDetected: false,
      positionUnit: Units.NATIVE,
      positionUnitDetected: false,
      velocityUnit: Units.NATIVE,
      velocityUnitDetected: false,
      series: [{
        name: 'Series A',
        positionColumn: 1,
        velocityColumn: 2,
      }, {
        name: 'Series B',
        positionColumn: 3,
        velocityColumn: 4,
      }]
    });
  });


  it('maps a fully specified header', () => {
    const map = detectSeriesAndUnits(
      [['Time (s)', 'Foo position [m]', 'Foo velocity (m/s)', 'Bar position (m)', 'Bar velocity (m/s)']],
      5);
    expect(map).toEqual({
      timeColumn: 0,
      timeUnit: Time.SECONDS,
      timeUnitDetected: true,
      positionUnit: Length.METRES,
      positionUnitDetected: true,
      velocityUnit: Velocity.METRES_PER_SECOND,
      velocityUnitDetected: true,
      series: [{
        name: 'Foo',
        positionColumn: 1,
        velocityColumn: 2,
      }, {
        name: 'Bar',
        positionColumn: 3,
        velocityColumn: 4,
      }]
    });
  });


  it('maps rearranged columns when named', () => {
    const map = detectSeriesAndUnits(
      [['Foo position (µm)', 'Bar position (um)', 'Foo velocity (mm/s)', 'Time (us)', 'Bar velocity (mm/s)']],
      5);
    expect(map).toEqual({
      timeColumn: 3,
      timeUnit: Time.MICROSECONDS,
      timeUnitDetected: true,
      positionUnit: Length.MICROMETRES,
      positionUnitDetected: true,
      velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
      velocityUnitDetected: true,
      series: [{
        name: 'Foo',
        positionColumn: 0,
        velocityColumn: 2,
      }, {
        name: 'Bar',
        positionColumn: 1,
        velocityColumn: 4,
      }]
    });
  });


  it('allows sparse unit specification', () => {
    const map = detectSeriesAndUnits(
      [['Time', 'Position (mm)', 'Velocity', 'Position', 'Velocity (mm/s)']],
      5);
    expect(map).toEqual({
      timeColumn: 0,
      timeUnit: null,
      timeUnitDetected: false,
      positionUnit: Length.MILLIMETRES,
      positionUnitDetected: true,
      velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
      velocityUnitDetected: true,
      series: [{
        name: 'Series A',
        positionColumn: 1,
        velocityColumn: 2,
      }, {
        name: 'Series B',
        positionColumn: 3,
        velocityColumn: 4,
      }]
    });
  });


  it('supports angular units', () => {
    const map = detectSeriesAndUnits([['Time', 'Foo position (°)', 'Foo velocity (°/s)', 'Bar position (deg)', 'Bar velocity (deg/s)']], 5);
    expect(map).toMatchObject({
      positionUnit: Angle.DEGREES,
      positionUnitDetected: true,
      velocityUnit: AngularVelocity.DEGREES_PER_SECOND,
      velocityUnitDetected: true,
    });
  });


  it('throws if there is no time column', () => {
    const headers = [['Foo', 'Position', 'Velocity', 'Position', 'Velocity']];
    expect(() => detectSeriesAndUnits(headers, 5)).toThrow(/no time column/);
  });


  it('throws if there are conflicting position units', () => {
    const headers = [['Time', 'Foo position (m)', 'Foo velocity (m/s)', 'Bar position (mm)', 'Bar velocity (m/s)']];
    expect(() => detectSeriesAndUnits(headers, 5)).toThrow(/All position/);
  });


  it('throws if there are conflicting velocity units', () => {
    const headers = [['Time', 'Foo position (m)', 'Foo velocity (mm/s)', 'Bar position (m)', 'Bar velocity (m/s)']];
    expect(() => detectSeriesAndUnits(headers, 5)).toThrow(/All velocity/);
  });


  it('throws if there are unequal numbers of position and velocity columns', () => {
    const headers = [['Foo position', 'Foo velocity', 'Bar position', 'Baz Position', 'Time']];
    expect(() => detectSeriesAndUnits(headers, 5)).toThrow(/equal numbers/);
  });


  it('throws if there are an even number of columns with no headings', () => {
    expect(() => detectSeriesAndUnits([], 4)).toThrow(/even/);
  });


  it('does not support two rows of headings', () => {
    expect(() => detectSeriesAndUnits([['Time', 'Axis 1', ''], ['(ms)', 'Position', 'Velocity']], 3))
      .toThrow(/zero or one/);
  });


  it('throws if there are unpaired named columns', () => {
    const headers = [['Foo position', 'Foo velocity', 'Bar position', 'Baz Velocity', 'Time']];
    expect(() => detectSeriesAndUnits(headers, 5)).toThrow(/Could not identify position and velocity/);
  });
});
