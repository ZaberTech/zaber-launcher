import React from 'react';
import { useSelector } from 'react-redux';
import { Card, Text, ContextMenu, Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { selectUpdates } from '../store';
import { environment } from '../environment';

import { actions as actionsDefinition } from './actions';

export const UpdateCheck: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const { newVersion, checkVersionError, checkingNewVersion, currentVersion } = useSelector(selectUpdates);

  if (environment.offline) {
    return <Card edge="outline" className="update-check">
      <Text t={Text.Type.H5}>{environment.appName} {currentVersion}</Text>

      <div className="message">
        Offline Zaber Launcher cannot be updated automatically.
        To get the newest features and database support, reinstall the app.
      </div>
      <div className="spacer"/>
    </Card>;
  }

  let message: React.ReactNode;
  if (checkingNewVersion) {
    message = <>Checking for Updates...</>;
  } else if (checkVersionError) {
    message = <>Cannot check for updates: {checkVersionError}</>;
  } else if (newVersion) {
    message = <><Icons.Dot className="red"/>&nbsp;New version {newVersion.version} available</>;
  } else {
    message = <><Icons.Dot className="green"/>&nbsp;Up to date</>;
  }

  return (<Card edge="outline" className="update-check">
    <Text t={Text.Type.H5}>{environment.appName} {currentVersion}</Text>

    <div className="message">{message}</div>

    <ContextMenu>
      {newVersion?.canUpdate &&
        <ContextMenu.Item disabled={checkingNewVersion} onClick={actions.quitAndInstall} icon={<Icons.Upgrade/>}>
          Update and Restart
        </ContextMenu.Item>}
      <ContextMenu.Item disabled={checkingNewVersion} onClick={actions.checkNewVersion} icon={<Icons.Refresh/>}>
        Check For Updates
      </ContextMenu.Item>
    </ContextMenu>
  </Card>);
};
