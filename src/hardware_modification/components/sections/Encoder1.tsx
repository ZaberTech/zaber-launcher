import React from 'react';

import { SettingField } from '../fields/SettingField';

import type { ContentProps, HwModSectionTemplate } from './HwModSection';

const Encoder1Section: React.FC<ContentProps<'encoder1'>> = ({ values }) => {
  const sectionId = 'encoder1';

  return <>
    <SettingField {...{ values, sectionId, fieldId: 'mode' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'type' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'dir' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'fault' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'indexMode' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'filter' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'div' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'mult' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'phase' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'powerUpDelay' }}/>
  </>;
};

export const encoder1Template: HwModSectionTemplate<'encoder1'> = {
  sectionId: 'encoder1',
  titleName: 'Encoder 1 (Single-ended Signalling)',
  menuName: 'Encoder 1',
  description: 'Information about the encoder connected on port 1, which only supports encoders with single-ended signalling.',
  generateContent: values => (<Encoder1Section values={values}/>),
};
