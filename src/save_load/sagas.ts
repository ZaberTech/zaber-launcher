import fs from 'fs';
import path from 'path';

import { ascii, SetDeviceStateFailedException, SetPeripheralStateFailedException } from '@zaber/motion';
import type { SagaIterator } from 'redux-saga';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import { isMatching, P } from 'ts-pattern';
import moment from 'moment';

import { getAxis, getDevice, getDeviceOrAxis, reloadDevices } from '../connection_manager';
import { Action, makeString, RT, throwUnexpectedError } from '../utils';
import { extractConnectionKey } from '../keys';
import { getContainer } from '../container';
import { ZaberApi } from '../app_components';
import { Dialogs } from '../dialogs';

import { actions, ActionsToPayloads, ActionTypes } from './actions';

export function* saveLoadSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.BEGIN_SAVE, saveState),
    takeLatest(ActionTypes.BEGIN_LOAD, loadState),
    takeLatest(ActionTypes.APPLY_LOAD, applyLoad),
    takeLatest(ActionTypes.RESTORE, restore),
  ]);
}

type ActionPayload<Type extends ActionTypes> = Action<ActionsToPayloads[Type]>;

function* saveState({ payload: { target } }: ActionPayload<ActionTypes.BEGIN_SAVE>) {
  try {
    const deviceOrAxis: RT<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, target.key);

    const saveState: RT<typeof deviceOrAxis.getState> = yield call([deviceOrAxis, deviceOrAxis.getState]);
    yield put(actions.chooseFile());
    const saveLocation: RT<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
      title: `Save ${target.name} State`,
      defaultPath: `save-state-${target.serialNumber}-${moment().format('YYYY-MM-DD-HHmm')}.json`,
    });
    if (saveLocation.canceled) {
      yield put(actions.cancelFileSelect());
      return;
    }
    yield put(actions.working(`Writing the ${target.name} state to a file...`));
    yield call(fs.promises.writeFile, saveLocation.filePath!, saveState);
    yield put(actions.saveDone());
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.saveError(e.message));
  }
}

function* loadState({ payload: { target } }: ActionPayload<ActionTypes.BEGIN_LOAD>) {
  try {
    const saveLocation: RT<typeof Dialogs.showOpenDialog> = yield call(Dialogs.showOpenDialog, {
      properties: ['openFile']
    });
    if (saveLocation.canceled) {
      yield put(actions.cancelFileSelect());
      return;
    }

    const filePath = saveLocation.filePaths[0];
    const fileName = path.basename(filePath);
    yield put(actions.working(`Checking compatibility of ${fileName}...`));

    const saveState: string = yield call(fs.promises.readFile, filePath, 'utf8');
    let toLoad: unknown;
    try {
      toLoad = JSON.parse(saveState);
    } catch (err) {
      throw new Error(`Cannot parse saved state: ${makeString(err)}`);
    }

    if (target.type === 'axis') {
      if (!isMatching({
        context: { type: 'peripheral' },
        axis: { peripheralId: P.string },
      }, toLoad)) {
        throw new Error('This state you are trying to load is not for a peripheral');
      }

      if (target.peripheralId === 0) {
        const toPeripheralId = parseInt(toLoad.axis.peripheralId, 10);
        const api = getContainer().get(ZaberApi);
        const { peripherals }: RT<typeof api.getSupportedPeripherals> = yield call([api, api.getSupportedPeripherals],
          target.deviceProductId,
        );
        const peripheralIdValidOnDevice = peripherals.some(peripheral => peripheral.peripheral_id === toPeripheralId);
        if (peripheralIdValidOnDevice) {
          yield put(actions.checkLoadDone({ fileName, saveState, setPeripheralId: toPeripheralId }, ''));
        } else {
          const checkResult = `This controller does not support peripherals with ID ${toPeripheralId}`;
          yield put(actions.checkLoadDone({ fileName, saveState }, checkResult));
        }
      } else {
        const axis: RT<typeof getAxis> = yield call(getAxis, target.key);
        const checkResult: RT<typeof axis.canSetState> = yield call([axis, axis.canSetState], saveState);
        yield put(actions.checkLoadDone({ fileName, saveState }, checkResult));
      }
    } else {
      if (!isMatching({ context: { type: P.union('device', 'controller'), fw: { major: P.number, minor: P.number } } }, toLoad)) {
        throw new Error('This state you are trying to load is not for a device');
      }

      const device: RT<typeof getDevice> = yield call(getDevice, target.key);
      const checkResult: RT<typeof device.canSetState> = yield call([device, device.canSetState], saveState);
      yield put(actions.checkLoadDone({ fileName, saveState }, checkResult));
    }
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.loadError({ message: e.message }));
  }
}

function* applyLoad({ payload: { target, context, deviceOnly } }: ActionPayload<ActionTypes.APPLY_LOAD>) {
  try {
    const deviceOrAxis: RT<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, target.key);

    if (context.setPeripheralId != null) {
      yield call([deviceOrAxis.settings, deviceOrAxis.settings.set], ascii.SettingConstants.PERIPHERAL_ID, context.setPeripheralId);
    }

    const response: RT<typeof deviceOrAxis.setState> = yield call([deviceOrAxis, deviceOrAxis.setState], context.saveState, { deviceOnly });

    yield reloadDevices(extractConnectionKey(target.key));

    yield put(actions.loadDone(target.key, context, response));
  } catch (e) {
    throwUnexpectedError(e);
    if (e instanceof SetDeviceStateFailedException || e instanceof SetPeripheralStateFailedException) {
      yield put(actions.loadError({ message: e.message, details: e.details, recommendRestore: true }));
    } else {
      yield put(actions.loadError({ message: e.message }));
    }
  }
}

function* restore({ payload: { target } }: ActionPayload<ActionTypes.RESTORE>) {
  try {
    const deviceOrAxis: RT<typeof getDeviceOrAxis> = yield call(getDeviceOrAxis, target.key);

    yield call([deviceOrAxis, deviceOrAxis.restore]);

    yield reloadDevices(extractConnectionKey(target.key));

    yield put(actions.restoreDone());
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.restoreError(e.message));
  }
}
