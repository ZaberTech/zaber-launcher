import type { Units } from '@zaber/motion';

import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';
import type { UnitConvert, Measurement } from '../units';

import type { Initiator, AxisError, TravelInfo, MovementType, AxisErrorOrigin } from './types';
import type { PreciseMovementValues } from './reducer';

export enum ActionTypes {
  MOVE_AXIS = 'BASIC_MOVEMENT_MOVE_AXIS',
  MOVE_AXIS_DONE = 'BASIC_MOVEMENT_MOVE_AXIS_DONE',
  MOVE_ALL_AXES = 'BASIC_MOVEMENT_MOVE_ALL_AXES',
  SELECT_KEY = 'BASIC_MOVEMENT_SELECT_KEY',
  DID_MOUNT = 'BASIC_MOVEMENT_DID_MOUNT',
  WILL_UNMOUNT = 'BASIC_MOVEMENT_WILL_UNMOUNT',
  AXIS_MONITORING_DATA = 'BASIC_MOVEMENT_AXIS_MONITORING_DATA',
  CHANGE_UNITS = 'BASIC_MOVEMENT_CHANGE_UNITS',
  DISMISS_ERROR = 'BASIC_MOVEMENT_DISMISS_ERROR',
  UPDATE_ERRORS = 'BASIC_MOVEMENT_UPDATE_ERRORS',
  REFRESH_VISIBLE_AXES = 'BASIC_MOVEMENT_REFRESH_VISIBLE_AXES',
  PIN_AXIS = 'BASIC_MOVEMENT_PIN_AXIS',
  UNPIN_AXIS = 'BASIC_MOVEMENT_UNPIN_AXIS',
  ADD_WARNING_FLAGS = 'BASIC_MOVEMENT_ADD_WARNING_FLAGS',
  CLEAR_IGNORE_INTERRUPT = 'BASIC_MOVEMENT_CLEAR_IGNORE_INTERRUPT',
  SET_DRIVER = 'BASIC_MOVEMENT_SET_DRIVER',
  SET_PRECISE_MEASUREMENT = 'BASIC_MOVEMENT_SET_PRECISE_MEASUREMENT',
  SET_CARD_EXPANDED = 'BASIC_MOVEMENT_SET_CARD_EXPANDED',
}

export interface ActionsToPayloads {
  [ActionTypes.MOVE_AXIS]: {
    axis: EntityKey; moveType: MovementType; initiator: Initiator;
    position?: number; units?: Units; };
  [ActionTypes.MOVE_AXIS_DONE]: { axis: EntityKey; errors?: AxisError[] };
  [ActionTypes.MOVE_ALL_AXES]: { moveType: MovementType; initiator: Initiator };
  [ActionTypes.SELECT_KEY]: { key: EntityKey | null };
  [ActionTypes.DID_MOUNT]: void;
  [ActionTypes.WILL_UNMOUNT]: void;
  [ActionTypes.REFRESH_VISIBLE_AXES]: void;
  [ActionTypes.DISMISS_ERROR]: { axis: EntityKey; index: number };
  [ActionTypes.AXIS_MONITORING_DATA]: {
    axis: EntityKey;
    position?: Measurement;
    travelInfo?: TravelInfo;
    positionUnitInfo?: UnitConvert;
  };
  [ActionTypes.UPDATE_ERRORS]: { axis: EntityKey; origin: AxisErrorOrigin; errors: AxisError[] };
  [ActionTypes.CHANGE_UNITS]: { axis: EntityKey; units: Units; position: number | null };
  [ActionTypes.PIN_AXIS]: { axis: EntityKey };
  [ActionTypes.UNPIN_AXIS]: { axis: EntityKey };
  [ActionTypes.ADD_WARNING_FLAGS]: { axis: EntityKey; flags: string[] };
  [ActionTypes.CLEAR_IGNORE_INTERRUPT]: { axis: EntityKey };
  [ActionTypes.SET_DRIVER]: { axis: EntityKey; enabled: boolean };
  [ActionTypes.SET_PRECISE_MEASUREMENT]: { axis: EntityKey; movementType: keyof PreciseMovementValues; measurement: Measurement };
  [ActionTypes.SET_CARD_EXPANDED]: { axis: EntityKey; expanded: boolean };
}

type K = keyof ActionsToPayloads;
const buildAction = (type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  moveAxis: (axis: EntityKey, moveType: MovementType, initiator: Initiator,
    position?: number, units?: Units) =>
    buildAction(ActionTypes.MOVE_AXIS, { axis, moveType, initiator, position, units }),
  moveAllAxes: (moveType: MovementType, initiator: Initiator) =>
    buildAction(ActionTypes.MOVE_ALL_AXES, { moveType, initiator }),
  moveAxisDone: (axis: EntityKey, errors?: AxisError[]) => buildAction(ActionTypes.MOVE_AXIS_DONE, { axis, errors }),
  selectKey: (key: EntityKey | null) => buildAction(ActionTypes.SELECT_KEY, { key }),
  didMount: () => buildAction(ActionTypes.DID_MOUNT),
  willUnmount: () => buildAction(ActionTypes.WILL_UNMOUNT),
  axisMonitoringData: (data: ActionsToPayloads[ActionTypes.AXIS_MONITORING_DATA]) => buildAction(ActionTypes.AXIS_MONITORING_DATA, data),
  changeUnits: (axis: EntityKey, units: Units, position: number | null) => buildAction(ActionTypes.CHANGE_UNITS, { axis, units, position }),
  updateErrors: (axis: EntityKey, origin: AxisErrorOrigin, errors: AxisError[] = []) =>
    buildAction(ActionTypes.UPDATE_ERRORS, { axis, origin, errors }),
  dismissError: (axis: EntityKey, index: number) => buildAction(ActionTypes.DISMISS_ERROR, { axis, index }),
  refreshVisibleAxes: () => buildAction(ActionTypes.REFRESH_VISIBLE_AXES),
  pinAxis: (axis: EntityKey) => buildAction(ActionTypes.PIN_AXIS, { axis }),
  unpinAxis: (axis: EntityKey) => buildAction(ActionTypes.UNPIN_AXIS, { axis }),
  addWarningFlags: (axis: EntityKey, flags: string[]) => buildAction(ActionTypes.ADD_WARNING_FLAGS, { axis, flags }),
  clearIgnoreInterrupt: (axis: EntityKey) => buildAction(ActionTypes.CLEAR_IGNORE_INTERRUPT, { axis }),
  setDriver: (axis: EntityKey, enabled: boolean) => buildAction(ActionTypes.SET_DRIVER, { axis, enabled }),
  setPreciseMeasurement: (axis: EntityKey, movementType: keyof PreciseMovementValues, measurement: Measurement) =>
    buildAction(ActionTypes.SET_PRECISE_MEASUREMENT, { axis, movementType, measurement }),
  setCardExpanded: (axis: EntityKey, expanded: boolean) => buildAction(ActionTypes.SET_CARD_EXPANDED, { axis, expanded }),
};
