import type { SagaIterator } from 'redux-saga';
import { all, put, select, takeLeading } from 'redux-saga/effects';

import { Action, RT, SagaIter, takeLeadingUniq, tryAccess } from '../utils';

import { actionDefinitions as actions, ActionsToPayloads, ActionTypes } from './actions';
import { selectProfilingData } from './selectors';
import { generateSettingManual } from './generate/setting_manual';
import { generateWarningFlagManual } from './generate/warning_manual';
import { generateCommandRejectedReasonManual } from './generate/command_rejected_reason_manual';
import { generateCommandIdList } from './generate/command_id_list';
import { generateSettingIdList } from './generate/setting_id_list';

export function* protocolManualSaga(): SagaIterator {
  yield all([
    takeLeadingUniq(ActionTypes.GENERATE_SETTING_MANUAL, ({ payload: { entity, setting } }) => `${entity}:${setting}`, settingManual),
    takeLeadingUniq(ActionTypes.GENERATE_WARNING_MANUAL, ({ payload: { entity, flag } }) => `${entity}:${flag}`, warningManual),
    takeLeadingUniq(
      ActionTypes.GENERATE_COMMAND_REJECTED_REASON_MANUAL,
      ({ payload: { entity, reason } }) => `${entity}:${reason}`,
      commandRejectedReasonManual),
    takeLeading(ActionTypes.GENERATE_COMMAND_ID_LIST, commandIdList),
    takeLeading(ActionTypes.GENERATE_SETTING_ID_LIST, settingIdList),
  ]);
}


function* settingManual({ payload: { entity, setting } }: Action<ActionsToPayloads[ActionTypes.GENERATE_SETTING_MANUAL]>) {
  const allProfiles: RT<typeof selectProfilingData> = yield select(selectProfilingData);
  const profile = tryAccess(allProfiles, entity);
  if (profile == null) {
    yield put(actions.storeSettingManual(entity, setting, { error: `No documentation for ${setting}` }));
  } else {
    const manual = generateSettingManual(setting, profile);
    yield put(actions.storeSettingManual(entity, setting, manual));
  }
}

function* warningManual({ payload: { entity, flag } }: Action<ActionsToPayloads[ActionTypes.GENERATE_WARNING_MANUAL]>) {
  const allProfiles: RT<typeof selectProfilingData> = yield select(selectProfilingData);
  const profile = tryAccess(allProfiles, entity);
  if (profile == null) {
    yield put(actions.storeWarningManual(entity, flag, { name: flag, error: `No documentation for ${flag}` }));
  } else {
    const manual = generateWarningFlagManual(flag, profile);
    yield put(actions.storeWarningManual(entity, flag, manual));
  }
}

function* commandRejectedReasonManual({ payload: { entity, reason } }
  : Action<ActionsToPayloads[ActionTypes.GENERATE_COMMAND_REJECTED_REASON_MANUAL]>) {
  const allProfiles: RT<typeof selectProfilingData> = yield select(selectProfilingData);
  const profile = tryAccess(allProfiles, entity);
  if (profile == null) {
    yield put(actions.storeCommandRejectedReasonManual(
      entity,
      reason,
      { name: reason, error: `No documentation for ${reason}` }));
  } else {
    const manual = generateCommandRejectedReasonManual(reason, profile);
    yield put(actions.storeCommandRejectedReasonManual(entity, reason, manual));
  }
}

function* commandIdList(): SagaIter {
  const commandIdList = generateCommandIdList();
  yield put(actions.storeCommandIdList(commandIdList));
}

function* settingIdList(): SagaIter {
  const settingIdList = generateSettingIdList();
  yield put(actions.storeSettingIdList(settingIdList));
}
