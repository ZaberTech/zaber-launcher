import { fireEvent, render, RenderResult, within } from '@testing-library/react';
import { ascii, CommandFailedException } from '@zaber/motion';
import { Container, injectable } from 'inversify';
import _ from 'lodash';
import React from 'react';
import type { AxisIdentity } from '@zaber/motion/ascii';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { DeviceInfoWithAxes, selectConnections, selectDevices } from '../connection_manager';
import { mockSingleDevice, mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import { createContainer, destroyContainer } from '../container';
import { MessageRoutersService } from '../message_router';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import {
  AxisMockBase,
  ConnectionMockBase,
  DeviceMockBase,
  MessageRoutersServiceMockBase,
  RouterConnectionMockBase
} from '../test/mocks/ascii';
import { mockReloadDevices } from '../test/mocks/reload_devices';

import { LockstepConfiguration } from './LockstepConfiguration';
import { MOTOR_TYPE_STEPPER } from './sagas';

let container: Container;

let axesMock: AxisMock[];
let axisCallback: (axis: AxisMock) => void;

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);

    if (axisCallback) { axisCallback(this) }
    axesMock.push(this);
  }
  _settings: Record<string, number> = {
    'limit.min': 200,
    'limit.max': 1000,
    'motor.type': MOTOR_TYPE_STEPPER,
    'maxspeed': 100000,
    'pos': 0,
  };
  identity: Partial<AxisIdentity> = {
    axisType: ascii.AxisType.LINEAR,
  };
  settings = {
    get: jest.fn(setting => {
      if (typeof this._settings[setting] !== 'number') {
        throw new CommandFailedException('BADCOMMAND', null!);
      }
      return Promise.resolve(this._settings[setting]);
    }),
  };
  getPosition = jest.fn(() => this._settings.pos);
  isHomed = jest.fn(async () => true);
}

class LockstepMock {
  enabled = false;
  lockstepAxisNumbers: number[] = [];
  isEnabled = jest.fn(async () => this.enabled);
  enable = jest.fn(async (...axisNumbers: number[]) => {
    this.lockstepAxisNumbers = axisNumbers;
    this.enabled = true;
  });
  getAxisNumbers = jest.fn(async () => this.lockstepAxisNumbers);
  getOffsets = jest.fn(async () => {
    const offsets = [229, 229, 229, 229];
    return offsets.slice(0, this.lockstepAxisNumbers.length - 1);
  });
  disable = jest.fn(async () => {
    this.enabled = false;
  });
}

let lockstepMocks: Record<number, LockstepMock> = {};

class DeviceMock extends DeviceMockBase<AxisMock> {
  getLockstep = jest.fn((groupNumber: number) => {
    if (!lockstepMocks[groupNumber]) {
      lockstepMocks[groupNumber] = new LockstepMock();
    }
    return lockstepMocks[groupNumber];
  });

  genericCommand = jest.fn((command: string) => {
    if (/lockstep \d get [abcd] tolerance/.test(command)) {
      return { data: 128 };
    } else if (/lockstep \d get [abcd] offset/.test(command)) {
      return { data: 10000 };
    }
    throw new Error(`No mock for this use of genericCommand: "${command}"`);
  });

  settings = {
    get: jest.fn(async setting => {
      switch (setting) {
        case 'lockstep.numgroups':
          return 2;
        default:
          throw new Error(`No mock for this use of settings.get: "${setting}"`);
      }
    }),
  };
}

let connections: ConnectionMock[];

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  genericCommandNoResponse = jest.fn().mockResolvedValue(undefined);

  constructor(id: string, router: RouterConnectionMock) {
    super(id, router);
    connections.push(this);
  }
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const TestLockstepConfiguration = wrapWithNewStore(wrapWithRouter(LockstepConfiguration));

let reloadModifier: (device: DeviceInfoWithAxes) => void;
const reloadDevicesMock = mockReloadDevices(() => TestLockstepConfiguration.testStore, devices => {
  devices[0].locksteps = [{
    groupNumber: 1,
    axisNumbers: [1, 2],
  }];
  reloadModifier?.(devices[0]);
});

let wrapper: RenderResult;

beforeEach(() => {
  axesMock = [];
  connections = [];
  lockstepMocks = [];

  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);

  wrapper = render(<TestLockstepConfiguration/>);
});

const cleanup = () => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;

  axesMock = null!;
  connections = null!;
  axisCallback = null!;
  lockstepMocks = null!;

  reloadModifier = null!;
  reloadDevicesMock.spy.mockClear();
};

afterEach(() => {
  cleanup();
});

describe('single linear device with 1 axis', () => {
  beforeEach(() => {
    axisCallback = axis => axis._settings.pos = 310;
    const store = TestLockstepConfiguration.testStore;
    mockSingleDevice(store, undefined, 1, 1);
  });

  test('prompts to select device', () => {
    wrapper.getByText(/Select a device/);
    const connection = _.sample(selectConnections(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(connection.key);
    wrapper.getByText(/Select a device/);
  });

  test('create lockstep button is disabled', async () => {
    const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getAllByText(/Create Lockstep Group/));
    const createLockstepButton = wrapper.getAllByText('Create Lockstep Group').find(
      element => element.classList.contains('button-open-modal'));
    expect(createLockstepButton).toBeDisabled();
  });
});

test('disables axes that are not mobile and do not have right motor.type', async () => {
  const store = TestLockstepConfiguration.testStore;
  mockSingleDeviceWithPeripherals(store, {
    deviceNumber: 1,
    peripheralCount: 3,
    axisModifier: info => {
      if (info.axisNumber === 1) {
        return { ...info, identity: { ...info.identity, axisType: ascii.AxisType.UNKNOWN } };
      }
      return info;
    },
  });
  const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
  connectionViewMockInstance.props.onSelect(device.key);
  let createLockstepButton: HTMLElement;
  await waitUntilPass(() => {
    createLockstepButton = wrapper.getByText(/Create Lockstep Group/);
  });
  axisCallback = axis => {
    switch (axis.axisNumber) {
      case 2:
        axis._settings['motor.type'] = MOTOR_TYPE_STEPPER + 1;
        break;
    }
  };
  fireEvent.click(createLockstepButton!);
  await waitUntilPass(() => wrapper.getByText(/Choose which axes/));

  expect(wrapper.getByTestId('axis-number-checkbox1')).toBeDisabled();
  expect(wrapper.getByTestId('axis-number-checkbox2')).toBeDisabled();
  expect(wrapper.getByTestId('axis-number-checkbox3')).not.toBeDisabled();

  expect(wrapper.queryAllByText('Not supported')).toHaveLength(2);
  wrapper.getByText('Only axes with a stepper motor can be included in a lockstep group.');
});

describe('single controller with 3 axes, no lockstep', () => {
  let createLockstepButton: HTMLElement;
  beforeEach(async () => {
    axisCallback = axis => axis._settings.pos = 310;
    const store = TestLockstepConfiguration.testStore;
    mockSingleDevice(store, undefined, 1, 3);
    const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => {
      createLockstepButton = wrapper.getByText(/Create Lockstep Group/);
    });
  });

  test('create lockstep button is enabled', async () => {
    expect(createLockstepButton).not.toBeDisabled();
  });

  test('disabled axes that are not homed, display info', async () => {
    axisCallback = axis => {
      if (axis.axisNumber === 1) {
        axis.isHomed.mockResolvedValue(false);
      }
    };

    fireEvent.click(createLockstepButton);
    await waitUntilPass(() => wrapper.getByText(/Choose which axes/));

    expect(wrapper.getByTestId('axis-number-checkbox1')).toBeDisabled();
    wrapper.getByText('Home required');
    wrapper.getByText(/axis has to be homed/);
  });

  describe('modal tests', () => {
    beforeEach(async () => {
      expect(wrapper.baseElement.querySelector('.modal')).toBeNull();
      fireEvent.click(createLockstepButton);
      await waitUntilPass(() => wrapper.getByText(/Choose which axes to include in the lockstep group./));
    });

    test('enables next button once two axes are selected, one being primary', async () => {
      wrapper.getByText(/Choose which axes to include in the lockstep group./);
      const nextButton = wrapper.getByText('Next');
      expect(nextButton).toBeDisabled();

      fireEvent.click(wrapper.getByTestId('axis-number-checkbox1'));
      fireEvent.click(wrapper.getByTestId('axis-number-checkbox2'));
      expect(nextButton).toBeDisabled();

      fireEvent.click(wrapper.getByTestId('primary-radio1'));
      expect(nextButton).not.toBeDisabled();
    });

    test('reloads data when coming back to the app', async () => {
      axesMock[2].isHomed.mockResolvedValue(false);

      const store = TestLockstepConfiguration.testStore;
      wrapper.unmount();
      wrapper = render(<TestLockstepConfiguration store={store}/>);

      await waitUntilPass(() => wrapper.getByText('Home required'));
    });

    describe('axis viewing', () => {
      beforeEach(async () => {
        // reload the modal with an offset on one axis
        const modalCloser = wrapper.baseElement.querySelector('.modal-close')!;
        expect(modalCloser).not.toBeNull();
        fireEvent.click(modalCloser);
      });

      test('correct text displayed', async () => {
        const axesMock2 = axesMock.find(axis => axis.axisNumber === 2)!;
        axesMock2._settings.pos += 229;
        fireEvent.click(createLockstepButton);
        await waitUntilPass(() => wrapper.getByText(/Choose which axes to include in the lockstep group./));

        // Make selections
        fireEvent.click(wrapper.getByTestId('axis-number-checkbox1'));
        fireEvent.click(wrapper.getByTestId('axis-number-checkbox2'));
        fireEvent.click(wrapper.getByTestId('primary-radio1'));

        fireEvent.click(wrapper.getByText('Next'));
        wrapper.getByText(/You're about to setup this lockstep group./);

        expect(wrapper.queryByText(/Axis 3/)).toBeNull();
        wrapper.getByText(/Axis 1/);
        wrapper.getByText(/Axis 2/);
        wrapper.getByText('229.00mm');
        expect(wrapper.queryByText(/-229.00mm/)).toBeNull();

        await waitUntilPass(() => { wrapper.getByText(/229/) });
      });

      test('correct text displayed for negative offset', async () => {
        const axesMock2 = axesMock.find(axis => axis.axisNumber === 2)!;
        axesMock2._settings.pos -= 12;
        fireEvent.click(createLockstepButton);
        await waitUntilPass(() => wrapper.getByText(/Choose which axes to include in the lockstep group./));

        // Make selections
        fireEvent.click(wrapper.getByTestId('axis-number-checkbox1'));
        fireEvent.click(wrapper.getByTestId('axis-number-checkbox2'));
        fireEvent.click(wrapper.getByTestId('primary-radio1'));

        fireEvent.click(wrapper.getByText('Next'));


        wrapper.getByText(/You're about to setup this lockstep group./);

        expect(wrapper.queryByText(/Axis 3/)).toBeNull();
        wrapper.getByText(/Axis 1/);
        wrapper.getByText(/Axis 2/);
        wrapper.getByText('-12.00mm');

        await waitUntilPass(() => { wrapper.getByText(/-12/) });
      });

      test('create lockstep group', async () => {
        const axesMock2 = axesMock.find(axis => axis.axisNumber === 2)!;
        axesMock2._settings.pos += 229;
        fireEvent.click(createLockstepButton);
        await waitUntilPass(() => wrapper.getByText(/Choose which axes to include in the lockstep group./));

        // Make selections
        fireEvent.click(wrapper.getByTestId('axis-number-checkbox1'));
        fireEvent.click(wrapper.getByTestId('axis-number-checkbox2'));
        fireEvent.click(wrapper.getByTestId('primary-radio1'));

        fireEvent.click(wrapper.getByText('Next'));
        fireEvent.click(wrapper.getByTitle('Enable Lockstep Group'));
        await waitUntilPass(() => { expect(lockstepMocks[1].enable).toHaveBeenLastCalledWith(1, 2) });
        await waitUntilPass(() => { wrapper.getByText(/created successfully/) });

        const closeModalButton = wrapper.getByTitle('Close Dialog');
        fireEvent.click(closeModalButton);
        expect(wrapper.baseElement.querySelector('.modal')).toBeNull();
      });
    });
  });
});

describe('single controller with 4 axes, 1 pre-existing lockstep', () => {
  let createLockstepButton: HTMLElement;
  beforeEach(async () => {
    const existingLockstepped = [1, 2];
    lockstepMocks[1] = new LockstepMock();
    lockstepMocks[1].enabled = true;
    lockstepMocks[1].getAxisNumbers = jest.fn(async () => existingLockstepped);
    const store = TestLockstepConfiguration.testStore;
    mockSingleDevice(store, device => ({
      ...device,
      locksteps: [{
        groupNumber: 1,
        axisNumbers: existingLockstepped,
      }],
    }), 1, 4);
  });

  test('shows the existing lockstep group', async () => {
    axisCallback = axis => {
      if (axis.axisNumber === 1) {
        axis._settings.pos = 310;
      } else if (axis.axisNumber === 2) {
        axis._settings.pos = 330;
      }
    };
    const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => {
      createLockstepButton = wrapper.getByText(/Create Lockstep Group/);
    });
    wrapper.getByText('Lockstep Group 1');

    await waitUntilPass(() => {
      wrapper.getByText(/20.00mm/);
    });
    expect(wrapper.queryByText(/-20.00mm/)).toBeNull();
  });

  test('shows the existing lockstep group with negative offset', async () => {
    axisCallback = axis => {
      if (axis.axisNumber === 1) {
        axis._settings.pos = 310;
      } else if (axis.axisNumber === 2) {
        axis._settings.pos = 280;
      }
    };
    const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => {
      createLockstepButton = wrapper.getByText(/Create Lockstep Group/);
    });
    wrapper.getByText('Lockstep Group 1');

    await waitUntilPass(() => {
      wrapper.getByText(/-30.00mm/);
    });
  });

  test('make a 2nd lockstep group', async () => {
    const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => {
      createLockstepButton = wrapper.getByText(/Create Lockstep Group/);
    });
    fireEvent.click(createLockstepButton);

    // Chose axes in the lockstep
    await waitUntilPass(() => wrapper.getByText(/Choose which axes to include in the lockstep group./));
    expect(wrapper.queryByTestId('axis-number-checkbox1')).toBeNull();
    expect(wrapper.queryByTestId('axis-number-checkbox2')).toBeNull();
    fireEvent.click(wrapper.getByTestId('axis-number-checkbox3'));
    fireEvent.click(wrapper.getByTestId('axis-number-checkbox4'));
    fireEvent.click(wrapper.getByTestId('primary-radio3'));
    fireEvent.click(wrapper.getByText('Next'));

    // confirm and enable
    fireEvent.click(wrapper.getByTitle('Enable Lockstep Group'));
    await waitUntilPass(() => {
      wrapper.getByText(/Group 2/);
      wrapper.getByText(/has been created successfully/);
    });
  });

  test('break a lockstep group', async () => {
    const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => {
      createLockstepButton = wrapper.getByText(/Create Lockstep Group/);
    });
    fireEvent.click(wrapper.getByText('Break Lockstep Group'));
    await waitUntilPass(() => {
      expect(lockstepMocks[1].disable).toHaveBeenCalledTimes(1);
    });
  });

  test('edit twist tolerance', async () => {
    const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => {
      createLockstepButton = wrapper.getByText(/Create Lockstep Group/);
    });
    const card2 = within(wrapper.getByTitle('Axis 2 Card'));
    fireEvent.click(card2.getByTitle('Edit Value'));
    fireEvent.input(card2.getByTestId('input-tolerance'), { target: { value: '101' } });
    fireEvent.click(wrapper.getByTitle('Axis 2 Card').querySelector('.confirm-cancel .confirm')!);
    wrapper.getByText('101');
  });

  test('discrepancy in the groups causes device reload', async () => {
    reloadModifier = device => {
      device.locksteps = [];
    };
    lockstepMocks[1].enabled = false;

    const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText(/Create Lockstep Group/));
    expect(reloadDevicesMock.spy).toHaveBeenCalledTimes(1);
  });

  test('discrepancy in the groups causes device reload (axes differ)', async () => {
    const axisNumbers = [2, 1];
    lockstepMocks[1].getAxisNumbers.mockResolvedValue(axisNumbers);
    reloadModifier = device => {
      device.locksteps = [{
        groupNumber: 1,
        axisNumbers,
      }];
    };

    const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText('Lockstep Group 1'));
    expect(reloadDevicesMock.spy).toHaveBeenCalledTimes(1);
  });
});


test('rotary axes', async () => {
  let createLockstepButton: HTMLElement;
  const store = TestLockstepConfiguration.testStore;
  mockSingleDeviceWithPeripherals(store, {
    deviceNumber: 1,
    peripheralCount: 3,
    axisModifier: info => {
      if (info.axisNumber === 1 || info.axisNumber === 2) {
        return { ...info, identity: { ...info.identity, axisType: ascii.AxisType.ROTARY } };
      }
      return info;
    },
  });
  const device = _.sample(selectDevices(TestLockstepConfiguration.testStore.getState()))!;
  connectionViewMockInstance.props.onSelect(device.key);
  await waitUntilPass(() => {
    createLockstepButton = wrapper.getByText(/Create Lockstep Group/);
  });

  fireEvent.click(createLockstepButton!);
  await waitUntilPass(() => wrapper.getByText(/Choose which axes to include in the lockstep group./));

  // Make selections
  fireEvent.click(wrapper.getByTestId('axis-number-checkbox1'));
  fireEvent.click(wrapper.getByTestId('axis-number-checkbox2'));
  fireEvent.click(wrapper.getByTestId('axis-number-checkbox3'));
  fireEvent.click(wrapper.getByTestId('primary-radio1'));
  fireEvent.click(wrapper.getByText('Next'));

  // Offset with the other rotary device
  wrapper.getByText(/0.00˚/);
  // Offset with the linear device
  wrapper.getByText(/Offset Undefined/);
});
