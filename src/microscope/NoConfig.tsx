import React from 'react';
import { useSelector } from 'react-redux';
import { Button, NoticeBanner } from '@zaber/react-library';

import { useActions } from '../utils';

import { selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';

export const NoConfig: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo;

  if (stableInfo == null || stableInfo.xAxis || stableInfo.yAxis || stableInfo.focusAxis) {
    return null;
  }

  return <NoticeBanner type="warning" className="no-config-warning">
    It appears that the microscope is not set up. Click the button below to set up the microscope components.
    <br/>
    <Button onClick={() => actions.resetConfigDialog(true)}>
      Set up Components
    </Button>
  </NoticeBanner>;
};
