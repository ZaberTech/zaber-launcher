const ipcRenderer = {
  on: jest.fn(),
  send: jest.fn(),
};

jest.mock('electron', () => ({
  ipcRenderer,
}));

import type { Container } from 'inversify';

import { createContainer, destroyContainer } from '../container';
import { buildStore } from '../store/build';
import { waitUntilPass } from '../test';
import { reduxStoreSymbol } from '../store';

import { actions } from './actions';
import { IPC } from './ipc';

let container: Container;
let store: ReturnType<typeof buildStore>;

beforeEach(async () => {
  container = createContainer();

  ipcRenderer.on.mockClear();
  ipcRenderer.send.mockClear();

  store = buildStore();
  container.bind(reduxStoreSymbol).toConstantValue(store);

  container.get(IPC);
});

afterEach(() => {
  store = null!;

  destroyContainer();
  container = null!;
});

test('sends broadcasted action to IPC', async () => {
  await waitUntilPass(() => expect(ipcRenderer.on).toHaveBeenCalled());

  store.dispatch(actions.ipcBroadcast({ type: 'ACTION' }));

  await waitUntilPass(() =>
    expect(ipcRenderer.send).toHaveBeenCalledTimes(1)
  );
  expect(ipcRenderer.send).toBeCalledWith('REDUX_BROADCAST', { type: 'ACTION' });
});

test('sends broadcasted action locally if requested', async () => {
  await waitUntilPass(() => expect(ipcRenderer.on).toHaveBeenCalled());

  let actionsDispatched = 0;
  store.subscribe(() => ++actionsDispatched);

  store.dispatch(actions.ipcBroadcast({ type: 'ACTION' }, true));

  await waitUntilPass(() =>
    expect(actionsDispatched).toBe(2)
  );
});

test('dispatches action that comes from IPC', async () => {
  await waitUntilPass(() => expect(ipcRenderer.on).toHaveBeenCalled());

  const dispatch = jest.spyOn(store, 'dispatch');

  const handle = ipcRenderer.on.mock.calls[0][1];
  handle({}, { type: 'ACTION' });

  expect(dispatch).toHaveBeenCalledWith({ type: 'ACTION', __ipcReceived: true });
});
