import React from 'react';
import { EditableField, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectFeedRateOverride } from './selectors';

const validators = [
  EditableField.validators.float,
  (value: string) => +value > 0,
];

export const FeedRateOverride: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const feedRateOverride = useSelector(selectFeedRateOverride);
  return (<div className="feed-rate-override">
    <EditableField
      validate={validators}
      labelContent={<Text>Feed Rate Override [%]:</Text>} type="number"
      value={feedRateOverride}
      onValueChange={value => actions.setFeedRateOverride(+value)}/>
  </div>);
};
