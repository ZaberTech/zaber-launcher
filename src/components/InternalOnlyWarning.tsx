import { NoticeBanner } from '@zaber/react-library';
import React from 'react';

import { Editions, environment, Flavors } from '../environment';

const CUSTOMER = {
  [Flavors.Rexroth]: 'Bosch Rexroth',
};

export const InternalOnlyWarning: React.FC = () => {
  if (environment.flavor !== Flavors.Zaber && environment.edition === Editions.Dev) {
    return <NoticeBanner className="internal-only-warning" type="warning">
      This edition of {environment.appName} is for Zaber use only.
      Do not provide this edition to {CUSTOMER[environment.flavor]}.
    </NoticeBanner>;
  } else if (environment.flavor !== Flavors.Zaber && environment.edition === Editions.Internal) {
    return <NoticeBanner className="internal-only-warning" type="warning">
      This edition of {environment.appName} is for {CUSTOMER[environment.flavor]} internal testing only.
      Do not provide this edition to customers.
    </NoticeBanner>;
  } else if (environment.edition === Editions.Dev && environment.isProduction) {
    return <NoticeBanner className="internal-only-warning" type="warning">
      This edition of {environment.appName} is for use by Software Team only unless authorized otherwise.
      The application was not properly tested and can result in unexpected behavior.
    </NoticeBanner>;
  }
  return null;
};
