import { createSelector } from 'reselect';

import { selectConnectionMonitor } from '../store';

export const selectConnections = createSelector(selectConnectionMonitor, state => state.connections);
export const selectDevicesFF = createSelector(selectConnectionMonitor, state => state.devicesFF);
