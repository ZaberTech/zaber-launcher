import {
  all, call, put, race, SagaReturnType as SRT, take, select, takeLatest, takeEvery, spawn, fork, delay,
} from '@redux-saga/core/effects';
import {
  ascii, gcode, CommandFailedException, GCodeExecutionException, GCodeSyntaxException, Velocity,
  MovementInterruptedException, Units, MovementFailedException, StreamMovementFailedException, StreamModeException,
  StreamMovementInterruptedException,
  InvalidOperationException,
} from '@zaber/motion';
import { AsyncReturnType as ART, castAny, throwUnexpectedError } from '@zaber/toolbox';
import type { SagaIterator } from 'redux-saga';
import _ from 'lodash';
import type { DeepPartial } from 'redux';

import { getDevice, takeDevicesLoaded } from '../connection_manager';
import { extractConnectionKey } from '../keys';
import { Action, SagaIter, SagaLock } from '../utils';
import { RejectReasons } from '../protocol';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import { AxisSettings, BlockMessage, defaultPositionUnits, isPrimaryAxis, MANUAL_JOG_SCALE, Settings, sortAxes } from './types';
import {
  selectPositionUpdateInterval, selectCurrentDevice, selectSettings, selectStatus, selectStep, selectLiveStates,
  selectFeedRateOverride, selectIsBusy, Device,
  selectHolding,
} from './selectors';
import type { AxesLiveStates, AxisLiveState } from './reducer';

const STREAM_NUM = 1;
const STREAMING_INTERRUPTED = 'Streaming was unexpectedly interrupted. Some movements were likely unfinished.';

interface Context {
  lock: SagaLock;
}

export function* translatorSaga(): SagaIterator {
  const ctx: Context = {
    lock: new SagaLock(),
  };
  yield all([
    takeLatest(ActionTypes.START_TRANSLATOR, startTranslator, ctx),
  ]);
}

function* startTranslator(ctx: Context): SagaIterator {
  const settings: ReturnType<typeof selectSettings> = yield select(selectSettings);
  const device: ReturnType<typeof selectCurrentDevice> = yield select(selectCurrentDevice);
  if (settings == null || device == null) {
    return;
  }

  try {
    yield race({
      loop: call(translatorLoop, ctx, device, settings),
      wasStopped: take(ActionTypes.STOP_TRANSLATOR),
      devicesReloaded: takeDevicesLoaded(extractConnectionKey(device.key)),
    });
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.translatorErr(err.message ?? String(err)));
  }
}

function* translatorLoop(ctx: Context, device: Device, settings: Settings): SagaIterator {
  const { unlock }: ART<typeof ctx.lock.lock> = yield call(ctx.lock.lock);
  unlock();

  const asciiDevice: SRT<typeof getDevice> = yield call(getDevice, device.key);

  const stream = asciiDevice.streams.getStream(STREAM_NUM);
  yield call([stream, stream.disable]);

  const primaryAxes = sortAxes(settings.axes.filter(isPrimaryAxis));

  const streamAxes: ascii.StreamAxisDefinition[] = [];
  const axisMappings: gcode.AxisMapping[] = [];
  const axisTransformations: gcode.AxisTransformation[] = [];
  const axisNumbers: number[] = [];
  for (const axis of primaryAxes) {
    const axisIndex = streamAxes.length;

    if (axis.lockstep) {
      streamAxes.push({
        axisType: ascii.StreamAxisType.LOCKSTEP,
        axisNumber: axis.lockstep.groupNumber,
      });
    } else {
      streamAxes.push({ axisNumber: axis.axisNumber });
    }

    axisNumbers.push(axis.axisNumber);
    axisMappings.push({
      axisIndex,
      axisLetter: axis.letter!,
    });
    if (axis.inverted) {
      axisTransformations.push({
        axisLetter: axis.letter!,
        scaling: -1,
      });
    }
  }

  const translator: gcode.OfflineTranslator = yield call(gcode.OfflineTranslator.setupFromDevice, asciiDevice, axisNumbers, {
    axisMappings,
    axisTransformations,
  });
  translator.setTraverseRate(settings.traverseRate.value, settings.traverseRate.unit as Velocity);

  yield call(setFeedRateOverride, translator);

  yield fork(pollPositionLoop, asciiDevice, translator, primaryAxes);
  yield fork(function* () {
    yield all([
      takeEvery(ActionTypes.HARD_STOP, hardStop, asciiDevice, stream),
      takeEvery(ActionTypes.ZERO_AXIS, zeroAxis, translator),
      takeEvery(ActionTypes.SET_FEED_RATE_OVERRIDE, setFeedRateOverride, translator),
      takeEvery(ActionTypes.SET_FREE_HOLD, setFreeHold, stream),
    ]);
  });
  yield put(actions.translatorRunning());

  try {
    for (;;) {
      const action: {
        perform?: ReturnType<typeof actions.perform>;
        moveAxis?: ReturnType<typeof actions.moveAxis>;
        command?: ReturnType<typeof actions.command>;
      } = yield race({
        perform: take(ActionTypes.PERFORM),
        moveAxis: take(ActionTypes.MOVE_AXIS),
        command: take(ActionTypes.COMMAND),
      });

      if (action.perform || action.command) {
        if (yield call([stream, stream.checkDisabled])) {
          yield call([asciiDevice.warnings, asciiDevice.warnings.clearFlags]);
          yield call([stream, stream.setupLiveComposite], ...streamAxes);

          if (yield select(selectHolding)) {
            yield call(setStreamHold, stream, true);
          }
        }
        yield call(setTranslatorPosition, translator, primaryAxes, asciiDevice);

        if (action.perform) {
          yield race({
            perform: call(perform, ctx, translator, stream, action.perform.payload!),
            stop: take(ActionTypes.STOP),
            hardStop: take(ActionTypes.HARD_STOP),
          });
        } else if (action.command) {
          yield race({
            perform: call(command, ctx, translator, stream, action.command.payload!.cmd),
            hardStop: take(ActionTypes.HARD_STOP),
          });
        }
      } else if (action.moveAxis) {
        if (stream.mode !== ascii.StreamMode.DISABLED) {
          yield call([stream, stream.disable]);
        }
        yield fork(move, settings, asciiDevice, action.moveAxis.payload!);
      }
    }
  } finally {
    try {
      yield call([stream, stream.disable]);
    } catch { /* error is irrelevant */ }
  }
}

function* perform(
  ctx: Context,
  translator: gcode.OfflineTranslator,
  stream: ascii.Stream,
  { code, beginIndex, endIndex, loop }: ActionsToPayloads[ActionTypes.PERFORM],
): SagaIterator {
  let blockIndex = beginIndex;
  let lastCommandBlockIndex = beginIndex;
  let blockError: BlockError | undefined;
  let isStepping: ReturnType<typeof selectStep> = false;

  const { unlock }: ART<typeof ctx.lock.lock> = yield call(ctx.lock.lock);
  yield put(actions.performing());

  try {
    for (; blockIndex < endIndex; blockIndex++) {
      isStepping = yield select(selectStep);
      yield put(actions.setPerformingBlock(blockIndex));

      const line = code[blockIndex];
      const result = translator.translate(line);
      if (isStepping) {
        result.commands.push(...translator.flush());
      }


      const warnings = result.warnings.map<BlockMessage>(w => ({ ...w, blockIndex, blockIndexEnd: blockIndex }));
      if (warnings.length > 0) {
        yield put(actions.addWarnings(warnings));
      }

      yield call([stream, stream.genericCommandBatch], result.commands);

      if (isStepping) {
        yield put(actions.updatePosition());
        yield call([stream, stream.waitUntilIdle]);
      }

      if (result.commands.length > 0) {
        lastCommandBlockIndex = blockIndex;
      }

      if (loop && blockIndex + 1 >= endIndex) {
        blockIndex = beginIndex - 1;
      }
    }
  } catch (err) {
    throwUnexpectedError(err);
    if (err instanceof GCodeExecutionException || err instanceof GCodeSyntaxException) {
      blockError = { ...err.details, blockIndex, blockIndexEnd: blockIndex, message: err.message };
    } else {
      blockError = makeStreamError(err, blockIndex, lastCommandBlockIndex);
    }

    throwIfNotRecoverable(err);
  } finally {
    // spawn is needed because finally gets sometimes mysteriously cancelled on async operation
    yield spawn(function* () {
      yield delay(1); // allows parent's stream.disable to run first

      try {
        const restOfCommands = translator.flush();
        if (blockError?.streamError !== true) {
          yield call([stream, stream.genericCommandBatch], restOfCommands);
        }
      } catch (err) {
        throwUnexpectedError(err);
        if (!blockError) {
          blockError = makeStreamError(err, blockIndex, lastCommandBlockIndex);
        }
      }

      try {
        yield call([stream, stream.waitUntilIdle]);
      } catch (err) {
        throwUnexpectedError(err);
        if (!blockError) {
          blockError = makeStreamError(err, blockIndex, lastCommandBlockIndex);
        }
      }

      if (blockError?.streamError) {
        try {
          yield call([stream, stream.disable]);
        } catch { /* error is irrelevant */ }
      }

      if (blockError == null) {
        yield put(actions.setPerformingBlock(Math.min(blockIndex + 1, endIndex)));
      }

      yield put(actions.performingDone(blockError));
      yield put(actions.updatePosition());
      unlock();
    });
  }
}

function* move(
  settings: Settings,
  device: ascii.Device,
  { axisLetter, move }: ActionsToPayloads[ActionTypes.MOVE_AXIS],
): SagaIterator {
  const axis = settings.axes.find(axis => axis.letter === axisLetter)!;
  const asciiAxis = device.getAxis(axis.primaryAxisNumber);
  const { maxSpeed } = axis;
  const units = defaultPositionUnits(axis.axisType);
  const moveAxis = axis.lockstep ? device.getLockstep(axis.lockstep.groupNumber) : asciiAxis;

  yield put(actions.updatePosition());

  try {
    const { stop }: { stop?: true } = yield race({
      move: call(function* () {
        if (move === 'home') {
          yield call([moveAxis, moveAxis.home]);
        } else if (move.startsWith('jog')) {
          let value = move === 'jog-left' ? -maxSpeed.value : maxSpeed.value;
          if (axis.inverted) { value *= -1 }
          yield call([moveAxis, moveAxis.moveVelocity], value * MANUAL_JOG_SCALE, maxSpeed.unit as Velocity);
          yield call([moveAxis, moveAxis.waitUntilIdle]);
        } else if (move.startsWith('step')) {
          let step = 1;
          if (move.endsWith('-small')) {
            step /= 10;
          } else if (move.endsWith('-xsmall')) {
            step /= 100;
          }
          if (move.startsWith('step-left')) { step *= -1 }
          if (axis.inverted) { step *= -1 }
          yield call([moveAxis, moveAxis.moveRelative], step, units);
        }
      }),
      stop: take((a: Partial<Action<ActionsToPayloads[ActionTypes.STOP_AXIS]>>) =>
        a.type === ActionTypes.STOP_AXIS && a.payload!.axisLetter === axisLetter),
    });
    if (stop) {
      yield call([moveAxis, moveAxis.stop]);
    }
  } catch (err) {
    if ([
      MovementInterruptedException,
      MovementFailedException,
      CommandFailedException,
    ].some(errType => err instanceof errType) === false) {
      throw err;
    }
  }

  yield put(actions.moveAxisDone(axis.letter!));
  yield put(actions.updatePosition());
}

function* setTranslatorPosition(translator: gcode.OfflineTranslator, primaryAxes: AxisSettings[], device: ascii.Device) {
  translator.flush();

  const positions: SRT<typeof getPositionsFromDevice> = yield call(getPositionsFromDevice, device);
  for (const axis of primaryAxes) {
    const devicePosition = positions[axis.axisNumber - 1];
    let translatorPosition: number | undefined;
    try {
      translatorPosition = translator.getAxisPosition(axis.letter!, Units.NATIVE);
    } catch (err) {
      if (!(err instanceof InvalidOperationException)) {
        throw err;
      }
    }
    // only set the position if it differs to prevent of loss of internal decimal places
    if (translatorPosition == null || Math.round(translatorPosition) !== devicePosition) {
      translator.setAxisPosition(axis.letter!, devicePosition, Units.NATIVE);
    }
  }
}

function* hardStop(device: ascii.Device, stream: ascii.Stream): SagaIterator {
  const { performing: wasPerforming }: ReturnType<typeof selectStatus> = yield select(selectStatus);

  try {
    yield call([device.allAxes, device.allAxes.stop], { waitUntilIdle: false });
    yield call([stream, stream.disable]);
  } catch { /* error is irrelevant */ }

  if (wasPerforming) {
    yield put(actions.setPerformingBlockError(STREAMING_INTERRUPTED));
  }
}

function* pollPositionLoop(device: ascii.Device, translator: gcode.OfflineTranslator, primaryAxes: AxisSettings[]): SagaIter {
  const axes = _.zip(primaryAxes, primaryAxes.map(axis => device.getAxis(axis.axisNumber))) as [AxisSettings, ascii.Axis][];

  while (true) {
    const isBusy: ReturnType<typeof selectIsBusy> = yield select(selectIsBusy);
    const positions: SRT<typeof getPositionsFromDevice> = yield call(getPositionsFromDevice, device);
    const { coordinateSystem } = translator;

    const updates: DeepPartial<AxesLiveStates> = {};
    for (const [axis, asciiAxis] of axes) {
      const units = defaultPositionUnits(axis.axisType);

      let positionNative = positions[axis.axisNumber - 1];
      if (axis.inverted) { positionNative *= -1 }

      let position = asciiAxis.settings.convertFromNativeUnits('pos', positionNative, units);
      position -= translator.getAxisCoordinateSystemOffset('G92', axis.letter!, units);

      const axisOffset = translator.getAxisCoordinateSystemOffset(coordinateSystem, axis.letter!, units);
      const positionCoordinateSystem = position - axisOffset;

      const update: Partial<AxisLiveState> = {
        position,
        positionCoordinateSystem,
      };
      if (!isBusy) {
        const flags: SRT<typeof asciiAxis.warnings.getFlags> = yield asciiAxis.warnings.getFlags();
        update.needsHoming = flags.has('WR');
      }

      updates[axis.letter!] = update;
    }

    yield put(actions.updatePositionDone(updates, coordinateSystem));

    const interval: ReturnType<typeof selectPositionUpdateInterval> = yield select(selectPositionUpdateInterval);
    yield race([
      take(ActionTypes.UPDATE_POSITION),
      delay(interval),
    ]);
  }
}

function* zeroAxis(translator: gcode.OfflineTranslator, action: ReturnType<typeof actions.zeroAxis>): SagaIterator {
  const { axisLetter } = action.payload!;
  const liveStates: ReturnType<typeof selectLiveStates> = yield select(selectLiveStates);
  translator.translate(`G10 L2 P0 ${action.payload!.axisLetter}${liveStates[axisLetter]?.position}`);
  yield put(actions.updatePosition());
}

function* setFeedRateOverride(translator: gcode.OfflineTranslator): SagaIterator {
  const feedRateOverride: ReturnType<typeof selectFeedRateOverride> = yield select(selectFeedRateOverride);
  translator.setFeedRateOverride(feedRateOverride / 100);
}

function* command(
  ctx: Context,
  translator: gcode.OfflineTranslator,
  stream: ascii.Stream,
  cmd: string,
): SagaIterator {
  let cmdError: string;
  const cmdWarnings: string[] = [];

  const { unlock }: ART<typeof ctx.lock.lock> = yield call(ctx.lock.lock);
  try {
    const result = translator.translate(cmd);
    for (const warning of result.warnings) {
      cmdWarnings.push(`<${warning.fromBlock}, ${warning.toBlock - 1}> ${warning.message}`);
    }
    result.commands.push(...translator.flush());

    yield call([stream, stream.genericCommandBatch], result.commands);
    yield call([stream, stream.waitUntilIdle]);
  } catch (err) {
    throwUnexpectedError(err);
    if (err instanceof GCodeExecutionException || err instanceof GCodeSyntaxException) {
      cmdError = `<${err.details.fromBlock},${err.details.toBlock - 1}> ${err.message}`;
    } else {
      cmdError = err.message ?? String(err);
      translator.resetAfterStreamError();
    }

    throwIfNotRecoverable(err);
  } finally {
    yield spawn(function* () {
      if (cmdError) {
        try {
          yield call([stream, stream.disable]);
        } catch { /* error is irrelevant */ }
      }
      yield put(actions.commandDone(cmdWarnings, cmdError));
      yield put(actions.updatePosition());
      unlock();
    });
  }
}

function* getPositionsFromDevice(device: ascii.Device): SagaIterator<number[]> {
  const reply: ART<typeof device.genericCommand> = yield call([device, device.genericCommand], 'get pos');
  return reply.data.split(/\s+/).map(pos => +pos);
}

function throwIfNotRecoverable(err: unknown) {
  if ([
    GCodeExecutionException,
    GCodeSyntaxException,
    CommandFailedException,
    StreamMovementFailedException,
    StreamMovementInterruptedException,
    StreamModeException,
  ].some(errType => err instanceof errType) === false) {
    throw err;
  }
}

interface BlockError extends BlockMessage {
  streamError?: boolean;
}

function makeStreamError(err: Error, blockIndex: number, lastCommandBlockIndex: number): BlockError {
  let message = err.message ?? String(err);
  if (castAny(err, CommandFailedException)?.details.responseData === RejectReasons.BADDATA) {
    message = 'Movement is outside of axis limits (BADDATA).';
  }
  return {
    blockIndex: lastCommandBlockIndex,
    blockIndexEnd: blockIndex,
    message,
    fromBlock: -1,
    toBlock: -1,
    streamError: true,
  };
}

function* setFreeHold(stream: ascii.Stream, action: ReturnType<typeof actions.setFreeHold>): SagaIterator {
  yield call(setStreamHold, stream, action.payload!.hold);
}

async function setStreamHold(stream: ascii.Stream, hold: boolean) {
  try {
    await stream.setHold(hold);
  } catch (err) {
    if (err instanceof StreamModeException) {
      // stream disabled
      return;
    } else if (err instanceof CommandFailedException && err.details.responseData === RejectReasons.BADCOMMAND) {
      throw new Error('Hold is not supported by the device. Upgrade your firmware to enable this feature.');
    }
    throw err;
  }
}
