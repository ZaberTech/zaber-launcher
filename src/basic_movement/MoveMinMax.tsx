import React from 'react';
import { Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import { Units } from '@zaber/motion';

import { round } from '../utils';
import { UnitInfo, convertNullable, getUnits, measurementOK } from '../units';

import { actions as actionsDefinition } from './actions';
import type { AxisStateComplete } from './selectors';
import { MovementType } from './types';

interface Props {
  axis: AxisStateComplete;
  which: 'min' | 'max';
  posUnitInfo: UnitInfo | null;
}

export const MoveMinMax: React.FC<Props> = ({ axis, which, posUnitInfo }) => {
  const actions = useActions(actionsDefinition);
  const { position, travelInfo } = axis;
  if (!travelInfo || !measurementOK(position) || posUnitInfo == null) {
    return null;
  }
  const move = () => {
    if (travelInfo.cycleDistance === 'not-cyclic') {
      if (which === 'min') {
        actions.moveAxis(axis.key, MovementType.Min, 'min');
      } else {
        actions.moveAxis(axis.key, MovementType.Max, 'max');
      }
    } else {
      const positionNative = round(convertNullable(posUnitInfo, position.value, getUnits(position), Units.NATIVE));
      if (positionNative == null) { return }
      let periodStart: number;
      if (which === 'min') {
        periodStart = (Math.ceil(positionNative / travelInfo.cycleDistance) - 1) * travelInfo.cycleDistance;
      } else {
        periodStart = (Math.floor(positionNative / travelInfo.cycleDistance) + 1) * travelInfo.cycleDistance;
      }
      actions.moveAxis(axis.key, MovementType.Absolute, 'absolute', periodStart);
    }
  };
  if (which === 'min') {
    return <Icons.LeftEnd
      title="Move to Beginning"
      activated={axis.lastInitiator?.includes('min')}
      onClick={move}/>;
  } else {
    return <Icons.RightEnd
      title="Move to End"
      activated={axis.lastInitiator?.includes('max')}
      onClick={move}/>;
  }
};
