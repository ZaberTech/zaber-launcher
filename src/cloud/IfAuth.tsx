import React, { Component } from 'react';
import { connect } from 'react-redux';

import type { RootState } from '../store';

import { AuthState } from './types';
import { selectAuthState } from './selectors';

interface Props {
  children: React.ReactNode;
  desiredState?: AuthState;
}
interface StateProps {
  authState: AuthState;
}

export class IfAuthBase extends Component<Props & StateProps> {
  static AuthState = AuthState;

  render(): React.ReactNode {
    const { children, authState, desiredState = AuthState.Authenticated } = this.props;
    if (authState !== desiredState) {
      return null;
    }
    return children;
  }
}

export const IfAuth = connect<StateProps, unknown, Props, RootState>(
  (state: RootState) => ({
    authState: selectAuthState(state),
  }),
)(IfAuthBase);
