import { injectable } from 'inversify';
import type { SagaIterator } from 'redux-saga';
import { all, fork, put, select, takeEvery } from 'redux-saga/effects';
import _ from 'lodash';

import { Cache } from '../cache';
import { getContainer } from '../container';
import { environment } from '../environment';
import { ipcActions } from '../ipc';
import { notNil } from '../utils';

import { actions, ActionTypes } from './actions';
import type { PendingSetups } from './reducer';
import { selectPendingSetups } from './selectors';

export function* rootSaga(): SagaIterator {
  yield all([
    takeEvery([ActionTypes.SETUP_NEEDED, ActionTypes.SETUP_NOT_NEEDED], cacheSetups),
    takeEvery(ActionTypes.LOAD_PENDING_SETUPS, loadPendingSetups),
    !environment.isTest ? fork(loadPendingSetups) : null,
  ].filter(notNil));
}

export const CACHE_KEY = 'PENDING_DEVICE_SETUPS';

@injectable()
class ComparisonContext {
  pendingSetup: ReturnType<typeof selectPendingSetups> = {};
}

function* cacheSetups(): SagaIterator {
  const cache = getContainer().get(Cache);
  const comparisonContext = getContainer().get(ComparisonContext);

  const pendingSetups: ReturnType<typeof selectPendingSetups> = yield select(selectPendingSetups);

  if (_.isEqual(comparisonContext.pendingSetup, pendingSetups)) { return }
  comparisonContext.pendingSetup = pendingSetups;

  cache.save(CACHE_KEY, pendingSetups);
  yield put(ipcActions.ipcBroadcast(actions.loadPendingSetups()));
}

function* loadPendingSetups(): SagaIterator {
  const cache = getContainer().get(Cache);
  const pendingSetups = cache.load<PendingSetups>(CACHE_KEY);
  if (pendingSetups == null) { return }

  const comparisonContext = getContainer().get(ComparisonContext);
  comparisonContext.pendingSetup = pendingSetups;

  yield put(actions.pendingSetupsLoaded(pendingSetups));
}
