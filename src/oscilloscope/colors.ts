import { Queue } from 'mnemonist';

import { generateColorWheels, hashStringToColor } from '../colors';
import { htmlColor } from '../utils';


export const UNSET_COLOR = '';

const predefinedColors = Queue.from(generateColorWheels());
const allocatedColors: _.Dictionary<number> = {};


export function getDefaultColor(channelId: string): string {
  if (!(channelId in allocatedColors)) {
    let color = predefinedColors.dequeue();
    if (color) {
      allocatedColors[channelId] = color;
    } else {
      color = hashStringToColor(channelId);
      allocatedColors[channelId] = color;
    }
  }

  return htmlColor(allocatedColors[channelId]);
}
