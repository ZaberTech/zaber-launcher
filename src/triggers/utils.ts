import { ProductInfo } from '../connection_manager';
import { AxisState } from '../connection_manager/reducer';

import { SettingsList } from './types';

export const getSettingsList = (productInfo: ProductInfo) => {
  const settingsList: SettingsList = {};
  productInfo.settings.rows.forEach(setting => {
    if (setting.realtime) {
      settingsList[setting.name] = {
        writeable: setting.write_access_level != null && [1, 2].includes(setting.write_access_level),
      };
    }
  });
  return sortSettingsList(settingsList);
};

export const getSettingAxes = (settingName: string, axesInfo: AxisState[]) => {
  const axes: number[] = [];

  axesInfo.forEach((axisInfo, i) => {
    if (axisInfo?.product?.settings.rows.find(setting => setting.name === settingName) != null) {
      axes.push(i + 1);
    }
  });

  return axes;
};

export const sortSettingsList = (settingsList: SettingsList): SettingsList =>
  Object.keys(settingsList).sort((a, b) => a.localeCompare(b, undefined, { numeric: true, sensitivity: 'base' }))
    .reduce((acc: SettingsList, key) => {
      acc[key] = settingsList[key];
      return acc;
    }, {});

export const combineSettingsLists = (settingsLists: SettingsList[]): SettingsList =>
  sortSettingsList(settingsLists.reduce((acc, settingsList) => {
    Object.keys(settingsList).forEach(key => {
      acc[key] = settingsList[key];
    });
    return acc;
  }, {}));
