import React from 'react';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import { Disappearing } from './Disappearing';

beforeAll(() => {
  jest.useFakeTimers();
});

test('disappears after timeout', () => {
  const onDisappear = jest.fn();

  render(<Disappearing timeout={1000} onDisappear={onDisappear}>Hello</Disappearing>);
  expect(screen.getByText('Hello')).toBeInTheDocument();
  expect(onDisappear).not.toHaveBeenCalled();

  act(() => { jest.advanceTimersByTime(1000) });

  expect(screen.queryByText('Hello')).not.toBeInTheDocument();
  expect(onDisappear).toHaveBeenCalled();
});
