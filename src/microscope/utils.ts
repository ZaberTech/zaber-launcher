import { Length, Units, ascii, microscopy } from '@zaber/motion';
import { parseURL } from 'whatwg-url';

import {
  convertFromDefaultUnits, convertToDefaultUnits,
  getDefaultDimensionUnits,
  getDimensionName,
  getUnitLabel,
  type MeasurementOK, type MeasurementWithUnit,
} from '../units';

import { AutofocusConfig } from './types';

export type AutofocusProvider = microscopy.WdiAutofocusProvider;

export function sanitizeMeasurement(m: MeasurementWithUnit): MeasurementOK {
  return {
    value: m.value ?? Number.NaN,
    units: m.units,
  };
}


export function getPrecision(value: { units?: string }): number {
  switch (value.units) {
    case Length.MICROMETRES: return 1;
    case Length.NANOMETRES: return 0;
    default: return 3;
  }
}

export function displayMeasurement(value: MeasurementWithUnit, precision?: number): string {
  if (value.value == null) {
    return 'N/A';
  }
  return `${value.value.toFixed(precision ?? getPrecision(value))} ${getUnitLabel(value.units)}`;
}

export async function getStorageBool(
  deviceOrAxis: ascii.Axis | ascii.Device,
  key: string,
  defaultValue = false,
): Promise<boolean> {
  if (await deviceOrAxis.storage.keyExists(key)) {
    return await deviceOrAxis.storage.getBool(key);
  }
  return defaultValue;
}

export function autofocusConfigFromUrl(url: string): AutofocusConfig | null {
  const parsed = parseURL(url);
  if (parsed == null) { return null }

  switch (parsed.scheme) {
    case 'wdi': {
      return {
        provider: 'wdi',
        host: parsed.host?.toString() ?? '',
        // eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
        port: parsed.port || null,
      };
    }
    default:
      throw new Error(`Unknown autofocus type: ${url}`);
  }
}

export function autofocusConfigToUrl(config: AutofocusConfig): string {
  switch (config.provider) {
    case 'wdi': {
      let url = `wdi://${config.host}`;
      if (config.port != null) {
        url += `:${config.port}`;
      }
      return url;
    }
    default:
      throw new Error(`Unknown autofocus type: ${JSON.stringify(config)}`);
  }
}


export class MeasurementMath {
  static toNumber(value: MeasurementWithUnit, defaultValue = 0): number {
    return convertToDefaultUnits(value.value ?? defaultValue, value.units);
  }

  static fromNumber(value: number, toUnits: Units): MeasurementOK {
    return {
      value: convertFromDefaultUnits(value, toUnits),
      units: toUnits,
    };
  }

  static mathUnits(value: MeasurementWithUnit): Units {
    return getDefaultDimensionUnits(getDimensionName(value.units)!);
  }
}
