import _ from 'lodash';

import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes } from '../connection_manager';
import { EntityKey, tryExtractConnectionKey } from '../keys';
import { SaveLoadActionPayloads, SaveLoadActions } from '../save_load';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { ApplyProgress } from './ApplyModal';
import type { FieldValue, Values } from './types';

type HwModError = {
  on: 'enable';
  message: string;
} | {
  on: 'init';
  message: string;
} | {
  on: 'apply';
  messages: string[];
} | {
  on: 'none';
};

export type PeripheralTraits = {
  motion: 'linear' | 'rotary' | null;
  motor: 'stepper' | 'voice' | 'linear' | null;
  /** The type of single ended encoder used */
  singleEncoder: 'none' | 'direct' | 'motor' | 'absolute' | null;
  /** The type of differential encoder used */
  diffEncoder: 'none' | 'direct' | 'analog' | 'motor' | null;
};

export type NonNullPeripheralTraits = {
  [Trait in keyof PeripheralTraits]: NonNullable<PeripheralTraits[Trait]>
};
export interface State {
  chosenProductKey: EntityKey | null;
  applyProgress: ApplyProgress | null;
  error: HwModError;
  enablingHwMod: boolean;
  values: Values | null;
  scrollPosition: number;
  scrollToError: { sectionId: string; fieldId: string } | null;
  peripheralTraits: PeripheralTraits | null;
  setPeripheralIdError: string | null;
}

const initialState: State = {
  chosenProductKey: null,
  applyProgress: null,
  error: { on: 'none' },
  enablingHwMod: false,
  values: null,
  scrollPosition: 0,
  scrollToError: null,
  peripheralTraits: null,
  setPeripheralIdError: null,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const productChosen: Reducer<ActionTypes.PRODUCT_CHOSEN> = (state, { productKey }) => {
  if (productKey === state.chosenProductKey) { return state }
  return {
    ...initialState,
    chosenProductKey: productKey,
  };
};

const beginEnablingDevice: Reducer<ActionTypes.ENABLE_MODIFICATION> = state => ({ ...state, enablingHwMod: true, peripheralTraits: null });

const errorEnablingHwMod: Reducer<ActionTypes.ERROR_ON_ENABLE_MODIFICATION> = (state, { message }) => ({
  ...state, enablingHwMod: false, error: { on: 'enable', message }
});

const appLoaded: Reducer<ActionTypes.APP_LOADED> = state => ({ ...state, values: null });

const beginInitialization: Reducer<ActionTypes.INITIALIZE_VALUES> = state => ({
  ...state, enablingHwMod: false, values: null, error: { on: 'none' }
});

const errorOnInitialization: Reducer<ActionTypes.ERROR_ON_INITIALIZE> = (state, { message }) => ({
  ...state, error: { on: 'init', message }
});

const setValues: Reducer<ActionTypes.SET_VALUES> = (state, { values }) => (
  { ...state, values }
);

type UnkeyedValues = Record<string, Record<string, FieldValue>>;
/** runs a specified function on each element in the values */
function updateFields(
  values: Values | null,
  update: (sectionId: string, fieldId: string, field: FieldValue) => Partial<FieldValue>
): Values | null {
  if (values == null) { return null }

  const updatedValues = _.mapValues(values as UnkeyedValues, (sectionValues, sectionId) => {
    if (sectionValues == null) { return null }
    return _.mapValues(sectionValues, (fieldValue, fieldId) => ({
      ...fieldValue, ...update(sectionId, fieldId, fieldValue)
    }));
  });

  return updatedValues as Values;
}

const updateValues: Reducer<ActionTypes.UPDATE_VALUES> = (state, { updates }) => {
  const values = updateFields(state.values, (sectionId, fieldId, field) => {
    const update = updates.find(up => up.sectionId === sectionId && up.fieldId === fieldId);
    if (update) {
      if (update.value === 'revert') {
        return { is: field.was, error: null, correlatedErrors: null, updateApplied: false };
      } else {
        return { is: update.value, error: null, correlatedErrors: null, updateApplied: false };
      }
    } else if (field.error?.correlatedErrors) {
      for (const correlatedError of field.error.correlatedErrors) {
        const isCorrelated = !!updates.find(up => up.sectionId === correlatedError.sectionId && up.fieldId === correlatedError.fieldId);
        if (isCorrelated) {
          return { error: null, correlatedErrors: null };
        }
      }
    }
    return {};
  });

  return { ...state, values };
};

const revertValues: Reducer<ActionTypes.REVERT_VALUES> = state => ({
  ...state,
  values: updateFields(state.values, (_s, _f, field) => ({ ...field, is: field.was, error: null })),
  error: { on: 'none' },
});

const beginningToApplyValues: Reducer<ActionTypes.APPLY_VALUES> = state => ({
  ...state,
  error: { on: 'none' },
  values: updateFields(state.values, (_s, _f, field) => ({ ...field, updateApplied: false, error: null, correlatedErrors: null }))
});

const setValueApplied: Reducer<ActionTypes.SET_VALUE_APPLIED> = (state, { sectionId, fieldId, value }) => {
  const values = updateFields(state.values, (prevSectionId, prevFieldId) => {
    if (sectionId === prevSectionId && fieldId === prevFieldId) {
      return { was: value, is: value, updateApplied: true, error: null };
    }
    return {};
  });

  return { ...state, values };
};

const errorOnApply: Reducer<ActionTypes.ERROR_ON_APPLY> = (state, { errors }) => {
  const values = updateFields(state.values, (sectionId, fieldId) => {
    const error = errors.fieldErrors.find(err => err.sectionId === sectionId && err.fieldId === fieldId);
    if (error) {
      return { error };
    }
    return {};
  });

  const topLevelError: HwModError = errors.topLevelErrors.length > 0 ? {
    on: 'apply', messages: errors.topLevelErrors.map(tle => tle.topLevelError)
  } : {
    on: 'none'
  };

  return { ...state, values, applyProgress: null, error: topLevelError, scrollToError: null };
};

const setApplyProgress: Reducer<ActionTypes.SET_APPLY_PROGRESS> = (state, { progress }) => ({
  ...state, applyProgress: progress ?? null
});

const scrollToError: Reducer<ActionTypes.SCROLL_TO_ERROR> = (state, { error }) => ({
  ...state, applyProgress: null, scrollToError: error
});

const storeScrollPosition: Reducer<ActionTypes.STORE_SCROLL_POSITION> = (state: State, { scrollPosition }) => ({
  ...state, scrollPosition,
});

const configurePeripheralId: Reducer<ActionTypes.CONFIGURE_PERIPHERAL_ID> = state => ({
  ...initialState,
  chosenProductKey: state.chosenProductKey,
  peripheralTraits: {
    motion: null,
    motor: null,
    singleEncoder: null,
    diffEncoder: null,
  }
});

const choosePeripheralTrait: Reducer<ActionTypes.CHOOSE_PERIPHERAL_TRAIT> = (state, { traits }) => ({
  ...state, peripheralTraits: traits, setPeripheralIdError: null
});

const errorSettingPeripheralId: Reducer<ActionTypes.ERROR_SETTING_PERIPHERAL_ID> = (state, { message }) => ({
  ...state, setPeripheralIdError: message
});

type CmReducer<AT extends ConnectionManagerActionTypes> = (state: State, payload: ConnectionManagerActionPayloads[AT]) => State;
const devicesReload: CmReducer<ConnectionManagerActionTypes.DEVICES_LOADED> = (state, { connectionKey }) => {
  if (tryExtractConnectionKey(state.chosenProductKey) !== connectionKey) { return state }
  return { ...state, values: null };
};

type SaveLoadReducer<AT extends SaveLoadActions> = (state: State, payload: SaveLoadActionPayloads[AT]) => State;
const deviceStateLoaded: SaveLoadReducer<SaveLoadActions.LOAD_DONE> = (state, { entity }) => {
  if (entity !== state.chosenProductKey) { return state }
  return { ...state, values: null };
};

export const reducer = createReducer<ActionsToPayloads & SaveLoadActionPayloads & ConnectionManagerActionPayloads, State>({
  [ActionTypes.PRODUCT_CHOSEN]: productChosen,
  [ActionTypes.APP_LOADED]: appLoaded,
  [ActionTypes.ENABLE_MODIFICATION]: beginEnablingDevice,
  [ActionTypes.ERROR_ON_ENABLE_MODIFICATION]: errorEnablingHwMod,
  [ActionTypes.ERROR_ON_INITIALIZE]: errorOnInitialization,
  [ActionTypes.INITIALIZE_VALUES]: beginInitialization,
  [ActionTypes.UPDATE_VALUES]: updateValues,
  [ActionTypes.SET_VALUES]: setValues,
  [ActionTypes.REVERT_VALUES]: revertValues,
  [ActionTypes.APPLY_VALUES]: beginningToApplyValues,
  [ActionTypes.SET_VALUE_APPLIED]: setValueApplied,
  [ActionTypes.ERROR_ON_APPLY]: errorOnApply,
  [ActionTypes.SET_APPLY_PROGRESS]: setApplyProgress,
  [ActionTypes.SCROLL_TO_ERROR]: scrollToError,
  [ActionTypes.STORE_SCROLL_POSITION]: storeScrollPosition,
  [ActionTypes.CONFIGURE_PERIPHERAL_ID]: configurePeripheralId,
  [ActionTypes.CHOOSE_PERIPHERAL_TRAIT]: choosePeripheralTrait,
  [ActionTypes.ERROR_SETTING_PERIPHERAL_ID]: errorSettingPeripheralId,

  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesReload,
  [SaveLoadActions.LOAD_DONE]: deviceStateLoaded,
}, initialState);
