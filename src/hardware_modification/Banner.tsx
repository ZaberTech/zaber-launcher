import { Flex, LinkClasses, NoticeBanner, NoticeType, Text } from '@zaber/react-library';
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { selectHardwareModification } from '../store';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { supportLink } from './components/links';
import { selectShouldShowBanner, selectErrorsOnForm, selectUnsetCount } from './selectors';

export const Banner: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const [headline, setHeadline] = useState<string | undefined>(undefined);
  const [message, setMessage] = useState<React.ReactNode | null>(null);
  const [noticeType, setNoticeType] = useState<NoticeType>('error');
  const [scrollToErrorIndex, setScrollToErrorIndex] = useState(0);

  const { error } = useSelector(selectHardwareModification);
  const hidden = !useSelector(selectShouldShowBanner);
  const formErrors = useSelector(selectErrorsOnForm);
  const unsetCount = useSelector(selectUnsetCount);

  useEffect(() => {
    if (error.on === 'apply') {
      setHeadline('Update Could Not Be Applied');
      setMessage(<Flex.Column className="top-level-error">
        {error.messages.map((message, i) => (<div key={i}>{message}</div>))}
        <div>Contact {supportLink} for assistance.</div>
      </Flex.Column>);
      setNoticeType('error');
    } else if (formErrors.length > 0) {
      const errorMessage = `${formErrors.length} changes could not be applied. Correct the values and press "Apply" again.`;
      const scrollToNextError = () => {
        const nextErrorIndex = (scrollToErrorIndex + 1) % formErrors.length;
        const nextError = formErrors[nextErrorIndex];
        setScrollToErrorIndex(nextErrorIndex);
        actions.scrollToError(nextError);
      };
      setHeadline(undefined);
      setMessage(<>
        <Text className="form-error-count">{errorMessage}</Text>
        <div className={LinkClasses.Default} onClick={scrollToNextError}>See next error</div>
      </>);
      setNoticeType('error');
    } else if (unsetCount > 0) {
      setHeadline(undefined);
      setMessage('Please fill in all fields to apply changes to a device');
      setNoticeType('warning');
    }
  }, [error.on, formErrors, scrollToErrorIndex, unsetCount]);

  return <NoticeBanner type={noticeType} headline={headline} className={classNames({ hidden })}>
    {message}
  </NoticeBanner>;
};
