import { reactSelect, Text } from '@zaber/react-library';
import React, { useState } from 'react';
import Select from 'react-select';
import { useActions } from '@zaber/toolbox/lib/redux';
import _ from 'lodash';

import type { PeripheralTraits } from '../reducer';
import { actions as actionsDefinition } from '../actions';
import { ProtocolManualToggle, ProtocolManualFormatter } from '../../protocol_manual';

type PeripheralTraitEnumOptions<Trait extends keyof PeripheralTraits> = {
  value: NonNullable<PeripheralTraits[Trait]>;
  label: string;
  disabled?: boolean;
};

interface Props<Trait extends keyof PeripheralTraits> {
  trait: Trait;
  traits: PeripheralTraits;
  options: PeripheralTraitEnumOptions<Trait>[];
  title: string;
  description: string;
  docs: string;
}

const getPickerEnumStyles = _.once(<Trait extends keyof PeripheralTraits>() => {
  const styles = reactSelect.getDefaultStyles<PeripheralTraitEnumOptions<Trait>>({
    control: base => ({
      ...base,
      width: '9rem',
    }),
    menu: base => ({
      ...base,
      width: '8rem',
      maxWidth: '27rem',
      right: '0',
      cursor: 'pointer',
    }),
    option: (base, props) => ({
      ...base,
      opacity: props.isDisabled ? 0.5 : 1,
      cursor: props.isDisabled ? 'not-allowed' : 'pointer',
    }),
  });
  return styles;
});


export const PeripheralTraitField = <Trait extends keyof PeripheralTraits>(props: Props<Trait>) => {
  const { trait, traits, options, title, description, docs } = props;
  const [showManual, setShowManual] = useState(false);
  const actions = useActions(actionsDefinition);

  return <div className="boxed-field">
    <div className="top-line">
      <div className="title">
        <div><Text t={Text.Type.H5}>{title}</Text></div>
        <Text e={Text.Emphasis.Light}>{description}</Text>
      </div>
      <Select<PeripheralTraitEnumOptions<Trait>>
        options={options}
        styles={getPickerEnumStyles<Trait>()}
        onChange={opt => actions.choosePeripheralTrait({ ...traits, [trait]: opt?.value ?? null })}
        menuPlacement="auto"
        data-testid={`${trait}-trait-picker`}
        isOptionDisabled={o => !!o.disabled}
      />
      <div className="toggle">
        <ProtocolManualToggle
          deviceOrAxisKey={null} setting="" activated={showManual} onClick={() => setShowManual(!showManual)} title={`Show ${title} Docs`}
        />
      </div>
    </div>
    {showManual && <ProtocolManualFormatter html={docs}/>}
  </div>;
};
