import { Component } from 'react';

import { environment, Editions } from '../environment';
import { handleNonCriticalError } from '../errors/errors';

interface Props {
  children: string;
}

export class Title extends Component<Props> {
  static instanceCount = 0;

  static get baseTitle(): string {
    let title = environment.appName;
    if (environment.edition !== Editions.Public) {
      title += ` (${environment.edition})`;
    }
    return title;
  }

  static setBaseTitle() {
    document.title = Title.baseTitle;
  }

  private setTitle() {
    const { children } = this.props;
    document.title = `${Title.baseTitle} - ${children}`;
  }

  componentDidMount() {
    if (Title.instanceCount !== 0) {
      handleNonCriticalError(new Error('There can be only single title in the application.'));
    }
    Title.instanceCount++;

    this.setTitle();
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.children !== prevProps.children) {
      this.setTitle();
    }
  }

  componentWillUnmount() {
    Title.instanceCount--;
    Title.setBaseTitle();
  }

  render() { return null }
}
