import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { ButtonPrevious, ButtonRow } from '@zaber/react-library';

import { CloudDiscovery as Discover } from '../cloud/CloudDiscover';

import { actions as actionsDefinition } from './actions';
import { Steps } from './types';

export const DiscoverCloud: React.FC = () => {
  const actions = useActions(actionsDefinition);
  return (<>
    <Discover onRouterAdded={() => actions.changeDialogOpen(false)}/>
    <ButtonRow justify="left">
      <ButtonPrevious className="back" onClick={() => actions.changeStep(Steps.CHOOSE_CONNECTION_TYPE)}>Back</ButtonPrevious>
    </ButtonRow>
  </>);
};
