export class MocksDictionary<T> {
  private map = new Map<string | number, T>();

  constructor(private readonly createCallback: (id: string | number) => T) {
  }

  public get(id: string | number): T {
    if (id == null) {
      throw new Error(`Invalid ID ${String(id)}`);
    }
    let instance = this.map.get(id);
    if (!instance) {
      instance = this.createCallback(id);
      this.map.set(id, instance);
    }
    return instance;
  }

  public set(id: string | number, instance: T): void {
    this.map.set(id, instance);
  }
}
