import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore } from '../test';
import { makeConnectionKey, makeRouterKey } from '../keys';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import { IPC } from '../ipc';
import { IpcMock, mockIpc } from '../ipc/mock';

import { RemoveConnection } from './RemoveConnection';

const TestRemoveConnection = wrapWithNewStore(RemoveConnection);

let container: Container;
let wrapper: RenderResult;

const CONNECTION_ID = 'COM5';
const ROUTER_KEY = makeRouterKey(LOCAL_ROUTER_URL);
const CONNECTION_KEY = makeConnectionKey(ROUTER_KEY, CONNECTION_ID);

class RouterConnectionMock {
  api = {
    removeConnection: jest.fn().mockResolvedValue(undefined),
  };
}

let routerService: MessageRoutersServiceMock;

@injectable()
class MessageRoutersServiceMock {
  routerMock = new RouterConnectionMock();

  async getConnection(): Promise<RouterConnectionMock> {
    return this.routerMock;
  }
}

beforeEach(() => {
  container = createContainer();
  mockIpc(container);
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  routerService = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;

  wrapper = render(<TestRemoveConnection connectionKey={CONNECTION_KEY}/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

test('calls removeConnection of router and broadcasts reload action', async () => {
  fireEvent.click(wrapper.getByTitle('Remove Connection'));
  fireEvent.click(wrapper.getByText('Remove'));

  expect(wrapper.getByTitle('Remove Connection')).toHaveClass('disabled');

  await waitUntilPass(() => expect(routerService.routerMock.api.removeConnection).toHaveBeenCalledTimes(1));
  expect(routerService.routerMock.api.removeConnection).toHaveBeenCalledWith(CONNECTION_ID);

  const ipc = container.get<unknown>(IPC) as IpcMock;
  expect(ipc.broadcastAction).toHaveBeenCalled();
});

test('Do not remove connection if user selects cancel', async () => {
  fireEvent.click(wrapper.getByTitle('Remove Connection'));
  wrapper.getByText('Are you sure you want to remove the connection?');

  fireEvent.click(wrapper.getByText('Cancel'));
  expect(wrapper.queryByText('Are you sure you want to remove the connection?')).toBeNull();
});
