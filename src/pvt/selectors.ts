import _ from 'lodash';
import { createSelector } from 'reselect';

import { selectPvt } from '../store';

export const selectAppState = createSelector(selectPvt, state => state.app);

// File operations
export const selectFile = createSelector(selectAppState, state => state.filePath);
export const selectCurrentFile = createSelector(selectAppState, state => state.filePath);
export const selectPreviousFile = createSelector(selectAppState, state => state.prevFilePath);
export const selectCsvImportOptions = createSelector(selectAppState, state => state.importOptions);
export const selectIsFileSaved = createSelector(selectPvt, state => _.isEqual(state.app.lastSavedContent, state.userPath.present));
export const selectCanUndo = createSelector(selectPvt, state =>
  ({ canUndo: state.userPath.past.length > 0, canRedo: state.userPath.future.length > 0 }));

// Document
export const selectUserPath = createSelector(selectPvt, state => state.userPath.present);
export const selectPointCount = createSelector(selectPvt, state => state.userPath.present?.series[0]?.points?.length ?? null);
export const selectSelectedPointIndex = createSelector(selectPvt, selectAppState,
  (pvt, state) => state.selectedPoint == null || pvt.userPath.present?.series[0]?.points?.length == null
    ? null
    : Math.max(0, Math.min(state.selectedPoint, pvt.userPath.present.series[0].points.length - 1)));

// Display
export const selectDevicePath = createSelector(selectAppState, state => state.devicePath);
export const selectViewMode = createSelector(selectAppState, state => state.viewMode);
export const selectSeriesDetailsVisibility = createSelector(selectAppState, state =>
  new Set(_.toPairs(state.visibleSeries).filter(([_, visible]) => visible).map(([seriesName, _]) => seriesName)));
export const selectTimeSeriesVisibility = createSelector(selectAppState, state => state.timeSeriesVisible);
export const selectDrawingAxes = createSelector(selectAppState, state => state.drawingAxes);
export const selectErrors = createSelector(selectAppState, state => state.errors);
export const selectBugs = createSelector(selectAppState, state => state.bugs);
export const selectFidelity = createSelector(selectAppState, state => state.fidelity);

// Device-related
export const selectDeviceOptions = createSelector(selectAppState, state => state.deviceSelections);
export const selectDevicePosition = createSelector(selectAppState, state => state.devicePosition);
export const selectDevicePositionNative = createSelector(selectAppState, state => state.devicePositionNative);
export const selectPlaybackTime = createSelector(selectAppState, state => state.playbackTime);
export const selectStartPosition = createSelector(selectAppState, state => state.startPosition);
export const selectPositionLimits = createSelector(selectAppState, state => state.posLimits);
export const selectVelocityLimits = createSelector(selectAppState, state => state.velLimits);
export const selectAccelerationLimits = createSelector(selectAppState, state => state.accelLimits);
export const selectPollDefaults = createSelector(selectAppState, state => state.defaultPolling);

// Dialog-related
export const selectTrajectoryExportDialogVisible = createSelector(selectAppState, state => state.trajectoryExportDialogVisible);
export const selectDeviceSelectionDialog = createSelector(selectAppState, state => state.deviceSelectionDialog);
export const selectDeviceLoadDialog = createSelector(selectAppState, state => state.bufferLoadDialog);
export const selectExportInProgress = createSelector(selectAppState, state => state.exportInProgress);

