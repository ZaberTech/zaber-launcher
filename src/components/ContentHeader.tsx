import { Text } from '@zaber/react-library';
import React from 'react';

import { Title } from './Title';

export const ContentHeader: React.FC<{ children: string; otherContent?: React.ReactNode }> = ({ children, otherContent })  => (
  <div className="content-header">
    <Text t={Text.Type.H3} e={Text.Emphasis.Light}>{children}</Text>
    <Title>{children}</Title>
    {otherContent}
  </div>
);
