import React, { Component, createRef } from 'react';
import * as monaco from 'monaco-editor';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Checkbox, ContextMenu, Icons, Modal, ProgressBar, Text, HelpTooltip, Colors } from '@zaber/react-library';
import classNames from 'classnames';

import { Editions, environment } from '../environment';
import type { RootState } from '../store';

import './language';
import { actions as actionsDefinition } from './actions';
import {
  selectPerformingBlockIndex, selectEvaluatingProgress, selectTranslatingDisabled,
  selectStatus, selectLoadedContent, selectStatusText, selectCurrentDevice,
} from './selectors';
import { Cmd } from './Cmd';
import type { BlockMessage } from './types';
import { EditorContext } from './EditorContext';
import { Errors, Warnings } from './ErrorsAndWarnings';

interface StateProps {
  performingBlockIndex: ReturnType<typeof selectPerformingBlockIndex>;
  evaluatingProgress: ReturnType<typeof selectEvaluatingProgress>;
  translatingDisabled: ReturnType<typeof selectTranslatingDisabled>;
  status: ReturnType<typeof selectStatus>;
  loadedContent: ReturnType<typeof selectLoadedContent>;
  statusText: ReturnType<typeof selectStatusText>;
  device: ReturnType<typeof selectCurrentDevice>;
}

interface DispatchProps {
  actions: typeof actionsDefinition;
}

type Props = StateProps & DispatchProps;

class EditorBase extends Component<Props> {
  private editorRef = createRef<HTMLDivElement>();
  private containerRef = createRef<HTMLDivElement>();
  private resizeObserver!:ResizeObserver;
  private editor!: monaco.editor.IStandaloneCodeEditor;
  private model!: monaco.editor.ITextModel;

  private performingBlockDecorations: string[] = [];

  private readonly editorContext = {
    parent: this,
    get editor() { return this.parent.editor },
    get model() { return this.parent.model },
    setPositionToMessage(message: BlockMessage, immediateScroll: boolean) {
      this.parent.setPositionToMessage(message, immediateScroll);
    }
  };

  onResize = () => this.editor.layout();
  beforeQuit = () => {
    const { actions } = this.props;
    actions.storeContent(this.model.getValue());
  };

  componentDidMount() {
    const { actions, loadedContent, status } = this.props;
    this.model = monaco.editor.createModel(loadedContent.content, 'gcode');

    this.editor = monaco.editor.create(this.editorRef.current!, {
      model: this.model,
      minimap: { enabled: false },
      glyphMargin: true,
      selectionHighlight: false,
      theme: 'zaber-gcode',
      fixedOverflowWidgets: true,
      occurrencesHighlight: false,
    });

    window.addEventListener('beforeunload', this.beforeQuit);

    this.resizeObserver = new ResizeObserver(this.onResize);
    this.resizeObserver.observe(this.editorRef.current!);

    if (!status.translatorRunning) {
      actions.startTranslator();
    }
  }

  componentWillUnmount() {
    const { actions } = this.props;

    this.resizeObserver.disconnect();

    window.removeEventListener('beforeunload', this.beforeQuit);
    actions.storeContent(this.model.getValue());

    this.editor.dispose();
    this.model.dispose();
  }

  componentDidUpdate(prevProps: Props) {
    const { performingBlockIndex, status, translatingDisabled, loadedContent } = this.props;
    if (loadedContent !== prevProps.loadedContent) {
      this.model.setValue(loadedContent.content);
    }

    if (performingBlockIndex !== prevProps.performingBlockIndex) {
      const decorations: monaco.editor.IModelDeltaDecoration[] = [];
      if (performingBlockIndex != null) {
        decorations.push({
          range: new monaco.Range(performingBlockIndex + 1, -1, performingBlockIndex + 1, -1),
          options: {
            glyphMarginClassName: 'block-performing-margin',
            overviewRuler: {
              color: Colors.blue,
              position: monaco.editor.OverviewRulerLane.Full,
            },
            stickiness: monaco.editor.TrackedRangeStickiness.NeverGrowsWhenTypingAtEdges,
          },
        });
      }
      this.performingBlockDecorations = this.model.deltaDecorations(this.performingBlockDecorations, decorations);

      if (performingBlockIndex != null) {
        this.editor.setPosition({
          lineNumber: performingBlockIndex + 1,
          column: 1,
        });
        const top = this.editor.getTopForLineNumber(performingBlockIndex + 1) - this.editor.getLayoutInfo().height / 2;
        this.editor.setScrollPosition({ scrollTop: Math.max(0, top) }, monaco.editor.ScrollType.Immediate);
      }
    }

    if (status.performing !== prevProps.status.performing && !translatingDisabled) {
      this.editor.focus();
    }
  }

  setPositionToMessage(message: BlockMessage, immediateScroll: boolean) {
    this.editor.setPosition({
      lineNumber: message.blockIndex + 1,
      column: message.fromBlock + 1,
    });
    this.editor.revealLinesInCenterIfOutsideViewport(
      message.blockIndex + 1, message.blockIndex + 1,
      immediateScroll ? monaco.editor.ScrollType.Immediate : monaco.editor.ScrollType.Smooth);
  }

  perform(stop: 'end' | 'next' | 'never') {
    const { actions } = this.props;
    const lines = this.model.getLinesContent();
    let lineIndex = (this.editor.getPosition()?.lineNumber ?? 1) - 1;
    if (lineIndex + 1 >= lines.length) {
      lineIndex = 0;
    }
    const endIndex = stop === 'next' ? lineIndex + 1 : lines.length;
    actions.perform(lines, lineIndex, endIndex, stop === 'never');
  }

  rewind() {
    const { actions } = this.props;
    actions.rewind();
    this.editor.setPosition({
      lineNumber: 1,
      column: 1,
    });
    this.editor.revealLinesInCenterIfOutsideViewport(1, 1, monaco.editor.ScrollType.Immediate);
    this.editor.focus();
  }

  render() {
    const { actions, evaluatingProgress, status, statusText, translatingDisabled, device } = this.props;
    const { translatorRunning, performing } = status;
    return (
      <EditorContext.Provider value={this.editorContext}>
        <div className="editor-container" ref={this.containerRef}>
          <div className="top-panel">
            <div className="menu">
              <Icons.ValidateCode title="Validate" disabled={translatingDisabled}
                onClick={() => actions.evaluate(this.model.getLinesContent())}/>
              <div className="separator"/>
              {!performing && <Icons.RightNormal title="Execute from cursor position" disabled={translatingDisabled}
                onClick={() => this.perform('end')}/>}
              {performing && <Icons.Pause title="Pause" disabled={!translatorRunning}
                onClick={() => actions.stop()}/>}
              <Icons.Stop title="Immediate Stop" disabled={!translatorRunning}
                onClick={() => actions.hardStop()}/>
              <Icons.Restore title="Go to the beginning" disabled={performing || translatingDisabled}
                onClick={() => this.rewind()}/>
              <div className="separator"/>
              <Icons.PlayOneLine title="Step one line" disabled={performing || translatingDisabled}
                onClick={() => this.perform('next')}/>
              <Checkbox labelContent="Step Mode" disabled={!translatorRunning}
                checked={status.step} onChecked={actions.setStep}/>
              <HelpTooltip className={classNames({ disabled: !translatorRunning })} popupClassName="gcode-popup">
                Turning on "Step Mode" or repeatedly clicking on "Step one line" executes the G-Code line by line,
                waiting for the device to finish the movements before advancing. This makes it easier to identify
                and resolve errors than during normal operation. Note that when stepping, the translator
                does not smooth the trajectory.
              </HelpTooltip>

              <div className="flex-spacer"/>

              {device && <Text className="status-text" title={statusText}>{statusText}</Text>}
              <div className="separator"/>
              <ContextMenu popupClassName="gcode-popup">
                <ContextMenu.Item onClick={actions.startTranslator}>Restart Translator</ContextMenu.Item>
                <ContextMenu.Item onClick={actions.stopTranslator}>Stop Translator</ContextMenu.Item>

                {environment.edition !== Editions.Public &&
                  <ContextMenu.Item disabled={performing || translatingDisabled}
                    onClick={() => {
                      this.rewind();
                      this.perform('never');
                    }}>
                    Demo Loop
                  </ContextMenu.Item>}
              </ContextMenu>
            </div>

            <Warnings/>
            <Errors/>
          </div>

          <div className="editor" ref={this.editorRef}/>

          <Cmd/>

          {evaluatingProgress != null && <Modal headerIcon={<Icons.ValidateCode/>} headerText="Validating...">
            <ProgressBar progress={evaluatingProgress}/>
          </Modal>}
        </div>
      </EditorContext.Provider>);
  }
}

export const Editor = connect<StateProps, DispatchProps, unknown, RootState>(
  (state): StateProps => ({
    performingBlockIndex: selectPerformingBlockIndex(state),
    evaluatingProgress: selectEvaluatingProgress(state),
    translatingDisabled: selectTranslatingDisabled(state),
    status: selectStatus(state),
    loadedContent: selectLoadedContent(state),
    statusText: selectStatusText(state),
    device: selectCurrentDevice(state),
  }),
  dispatch => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(EditorBase);
