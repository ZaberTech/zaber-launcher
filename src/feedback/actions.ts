import { actionBuilder } from '../utils';

import type { FeedbackModalState } from './types';

export enum ActionTypes {
  SET_MODAL_STATE = 'FEEDBACK_SET_MODAL_STATE',

  SEND_FEEDBACK = 'FEEDBACK_SEND',
  SEND_FEEDBACK_DONE = 'SEND_FEEDBACK_DONE',
  SEND_FEEDBACK_ERR = 'SEND_FEEDBACK_ERR',

  SET_ANONYMOUS = 'FEEDBACK_SET_ANONYMOUS',
  SET_EMAIL = 'FEEDBACK_SET_REPLY_EMAIL',
  SET_TEXT = 'FEEDBACK_SET_TEXT',
}

export interface ActionsToPayloads {
  [ActionTypes.SET_MODAL_STATE]: { modalState: FeedbackModalState };
  [ActionTypes.SEND_FEEDBACK]: void;
  [ActionTypes.SEND_FEEDBACK_DONE]: void;
  [ActionTypes.SEND_FEEDBACK_ERR]: { error: string };

  [ActionTypes.SET_ANONYMOUS]: { anonymous: boolean };
  [ActionTypes.SET_EMAIL]: { email: string };
  [ActionTypes.SET_TEXT]: { text: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  open: () => buildAction(ActionTypes.SET_MODAL_STATE, { modalState: 'gather' }),
  close: () => buildAction(ActionTypes.SET_MODAL_STATE, { modalState: 'closed' }),

  sendFeedback: () => buildAction(ActionTypes.SEND_FEEDBACK),
  sendFeedbackDone: () => buildAction(ActionTypes.SEND_FEEDBACK_DONE),
  sendFeedbackErr: (error: string) => buildAction(ActionTypes.SEND_FEEDBACK_ERR, { error }),

  setAnonymous: (anonymous: boolean) => buildAction(ActionTypes.SET_ANONYMOUS, { anonymous }),
  setEmail: (email: string) => buildAction(ActionTypes.SET_EMAIL, { email }),
  setText: (text: string) => buildAction(ActionTypes.SET_TEXT, { text }),
};
