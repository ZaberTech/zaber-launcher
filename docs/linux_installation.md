# Linux Installation Instructions

This document provides instructions for getting Zaber Launcher running on a Linux system.

For Linux, we distribute Zaber Launcher packed as AppImage, which is a self-contained executable that requires minimal dependencies to run.
You can copy the AppImage to any location on your system and execute it.
Optionally, you can improve the user experience by using [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher)
that copies the AppImage to a designated location and creates a menu entry for it.

## Making the AppImage executable

Before running the AppImage, you may need to make it executable.
Open the file properties dialog and check the _Executable as Program_ checkbox.

Alternatively, you can run the following command in the terminal:

```bash
chmod +x ZaberLauncher.AppImage
```

## Dependencies

In order to run the Zaber Launcher AppImage, you may need to install [FUSE](https://github.com/AppImage/AppImageKit/wiki/FUSE).
For Ubuntu 24.04 and higher, you can install FUSE with the following command:

```bash
sudo add-apt-repository universe
sudo apt install libfuse2t64
```

## Obtaining permissions to access serial ports

In most Linux distributions, you need to be a member of the `dialout` group to access serial ports.

On debian-based systems (e.g. Ubuntu), you can add your user to the `dialout` group by running the following command:

```bash
sudo usermod -a -G dialout $USER
```

## Adding AppArmor profile (Ubuntu 23.10 and higher)

AppArmor is a mandatory access control system that restricts the application's access to system resources.
If you are using Ubuntu 23.10 or higher, the application won't start unless you add an AppArmor profile for it.

To add an AppArmor profile, create `/etc/apparmor.d/zaber-launcher` file with the content below.
Note that you need to change the absolute path to the AppImage file in the profile.

Start the editor with the following command:

```bash
sudo gnome-text-editor /etc/apparmor.d/zaber-launcher
```

Then paste the following content into the editor and change the path to the AppImage file:

```txt
# This profile allows everything and only exists to give the
# application a name instead of having the label "unconfined"

abi <abi/4.0>,
include <tunables/global>

profile zaber-launcher "/home/user/Downloads/ZaberLauncher.AppImage" flags=(unconfined) {
  userns,

  # Site-specific additions and overrides. See local/README for details.
  include if exists <local/zaber-launcher>
}
```

After saving the file, reload the AppArmor profiles with the following command:

```bash
sudo service apparmor restart
```

Now you should be able to run the AppImage.

## Support

If you encounter any issues, please contact our support [contact@zaber.com](mailto:contact@zaber.com).
