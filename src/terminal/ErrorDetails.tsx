import React from 'react';
import { NoticeBanner, NoticeType } from '@zaber/react-library';
import { match, P } from 'ts-pattern';

import { ProtocolManualFormatter, useCommandRejectedReasonManual, useWarningFlagManual } from '../protocol_manual';

import type { ErrorFlagType, HistoryItem } from './types';


export type ErrorDetailsType = 'rejectedCommand' | 'errorFlag' | 'checksum' | null;

function getNoticeBannerType(errorFlagType: ErrorFlagType): NoticeType {
  switch (errorFlagType) {
    case 'fault': return 'error';
    case 'warn': return 'warning';
    case 'note': return 'info';
    default: return 'error';
  }
}

interface ErrorDetailsHistoryItemProps {
  historyItem: HistoryItem;
  onClose: () => void;
}

const ErrorDetailsRejectedCommand: React.FC<ErrorDetailsHistoryItemProps> = ({ historyItem, onClose }) => {
  const { deviceOrAxisEntityKey } = historyItem;
  const { type } = historyItem.parsed;
  if (type !== 'reply') {
    throw new Error('Rejected Command details expected reply message');
  }

  const { data } = historyItem.parsed;
  return <NoticeBanner type="error" headline={`RJ: Rejected Command - ${data}`} closer={onClose}>
    {match(useCommandRejectedReasonManual(deviceOrAxisEntityKey, data))
      .with({ html: P.string }, ({ html }) => <ProtocolManualFormatter html={html}/>)
      .otherwise(() => <p></p>)}
  </NoticeBanner>;
};

const ErrorDetailsErrorFlag: React.FC<ErrorDetailsHistoryItemProps> = ({ historyItem, onClose }) => {
  const { type } = historyItem.parsed;
  if (type !== 'reply' && type !== 'alert') {
    throw new Error('Error flag details expected reply or alert message');
  }

  const { deviceOrAxisEntityKey } = historyItem;
  const { errorFlag } = historyItem.parsed;
  const warningFlagManualEntry = useWarningFlagManual(deviceOrAxisEntityKey, errorFlag.text);
  const subtitle =
    warningFlagManualEntry?.name ? warningFlagManualEntry.name :
    errorFlag.type === 'fault' ? 'Fault - Unknown' :
    errorFlag.type === 'warn' ? 'Warning - Unknown' :
    errorFlag.type === 'note' ? 'Note - Unknown' :
    'Unknown';
  return <NoticeBanner type={getNoticeBannerType(errorFlag.type)} headline={`${errorFlag.text}: ${subtitle}`} closer={onClose}>
    {match(warningFlagManualEntry)
      .with({ html: P.string }, ({ html }) => <ProtocolManualFormatter html={html}/>)
      .otherwise(() => <p></p>)}
  </NoticeBanner>;
};

interface ErrorDetailsGenericProps {
  onClose: () => void;
}

const ErrorDetailsChecksumError: React.FC<ErrorDetailsGenericProps> = ({ onClose }) =>
  <NoticeBanner type="error" headline="Checksum Error" closer={onClose}>
    <p>Checksum value is invalid. Message is corrupt or uses reserved character ':' incorrectly.</p>
  </NoticeBanner>;

interface ErrorDetailsProps {
  historyItem: HistoryItem;
  errorDetailsType: ErrorDetailsType;
  onClose: () => void;
}

export const ErrorDetails: React.FC<ErrorDetailsProps> = ({ historyItem, errorDetailsType, onClose }) => {
  switch (errorDetailsType) {
    case 'rejectedCommand':
      return <ErrorDetailsRejectedCommand historyItem={historyItem} onClose={onClose}/>;
    case 'errorFlag':
      return <ErrorDetailsErrorFlag historyItem={historyItem} onClose={onClose}/>;
    case 'checksum':
      return <ErrorDetailsChecksumError onClose={onClose}/>;
    default:
      return null;
  }
};
