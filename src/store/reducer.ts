import { combineReducers } from 'redux';
import { connectRouter, RouterState } from 'connected-react-router';

import { ConnectionManagerState, connectionManagerReducer } from '../connection_manager';
import { ConnectionViewState, connectionViewReducer } from '../connection_manager/connection_view/reducer';
import { basicControlsReducer } from '../basic_controls';
import { pollingReducer } from '../polling';
import { BasicMovementState, basicMovementReducer } from '../basic_movement';
import { AddConnectionState, addConnectionReducer } from '../add_connection';
import { HardwareModificationState, hardwareModificationReducer } from '../hardware_modification';
import { DeviceSettingsState, deviceSettingsReducer } from '../device_settings';
import { firmwareUpgradeReducer, FirmwareUpgradeState } from '../firmware_upgrade';
import { saveLoadReducer, SaveLoadState } from '../save_load';
import { currentTunerReducer, CurrentTunerState } from '../current_tuner';
import { feedbackReducer, FeedbackState } from '../feedback';
import { TerminalState, terminalReducer } from '../terminal';
import { MenuState, menuReducer } from '../menu';
import { ProtocolManualState, protocolManualReducer } from '../protocol_manual';
import { cloudReducer, CloudState } from '../cloud';
import { peripheralsReducer, PeripheralsState } from '../peripherals';
import { updatesReducer, UpdatesState } from '../updates';
import { localShareReducer, LocalShareState } from '../local_share';
import { preferencesReducer, PreferencesState } from '../preferences';
import { servoTunerReducer, ServoTunerState } from '../servo_tuning';
import { connectionStatusReducer, ConnectionStatusState } from '../connection_status';
import { helpReducer, HelpState } from '../help';
import { lc40SetupReducer, LC40SetupState } from '../lc40_setup';
import { deviceSetupReducer, DeviceSetupState } from '../device_setup';
import { appsReducer, AppsState } from '../apps';
import { gcodeReducer, GCodeState } from '../gcode';
import { lockstepConfigurationReducer, LockstepConfigurationState } from '../lockstep_configuration';
import { demoReducer, DemoState } from '../demo';
import { deviceIoReducer, DeviceIoState } from '../device_io';
import { joystickReducer, JoystickState } from '../joystick';
import { factoryResetReducer, FactoryResetState } from '../factory_reset';
import { oscilloscopeReducer, OscilloscopeState } from '../oscilloscope';
import { unitReducer, UnitState } from '../units';
import { internalReducer, InternalState } from '../internal';
import { connectionMonitorReducer, ConnectionMonitorState } from '../connection_monitor';
import { processControllerReducer, ProcessControllerState } from '../process_controller';
import { pvtReducer, PvtState } from '../pvt';
import { cManagerUiReducer, CManagerUiState } from '../connection_manager_ui';
import { calibrationReducer, CalibrationState } from '../calibration';
import { microscopeReducer, MicroscopeState } from '../microscope';
import { triggersReducer, TriggersState } from '../triggers';

import { browserHistory } from './history';

export interface State {
  router: RouterState;
  connectionManager: ConnectionManagerState;
  connectionView: ConnectionViewState;
  basicControls: ReturnType<typeof basicControlsReducer>;
  polling: ReturnType<typeof pollingReducer>;
  basicMovement: BasicMovementState;
  addConnection: AddConnectionState;
  hardwareModification: HardwareModificationState;
  deviceSettings: DeviceSettingsState;
  feedback: FeedbackState;
  firmwareUpgrade: FirmwareUpgradeState;
  saveLoad: SaveLoadState;
  currentTuner: CurrentTunerState;
  terminal: TerminalState;
  processController: ProcessControllerState;
  protocolManuals: ProtocolManualState;
  menu: MenuState;
  cloud: CloudState;
  updates: UpdatesState;
  localShare: LocalShareState;
  peripherals: PeripheralsState;
  preferences: PreferencesState;
  servoTuning: ServoTunerState;
  connectionStatus: ConnectionStatusState;
  help: HelpState;
  lc40Setup: LC40SetupState;
  deviceSetup: DeviceSetupState;
  apps: AppsState;
  gcode: GCodeState;
  lockstepConfiguration: LockstepConfigurationState;
  demo: DemoState;
  deviceIo: DeviceIoState;
  joystick: JoystickState;
  factoryReset: FactoryResetState;
  oscilloscope: OscilloscopeState;
  units: UnitState;
  internal: InternalState;
  connectionMonitor: ConnectionMonitorState;
  pvt: PvtState;
  cManagerUi: CManagerUiState;
  calibration: CalibrationState;
  microscope: MicroscopeState;
  triggers: TriggersState;
}

export const rootReducer = combineReducers<State>({
  router: connectRouter(browserHistory),
  connectionManager: connectionManagerReducer,
  connectionView: connectionViewReducer,
  basicControls: basicControlsReducer,
  polling: pollingReducer,
  basicMovement: basicMovementReducer,
  hardwareModification: hardwareModificationReducer,
  feedback: feedbackReducer,
  firmwareUpgrade: firmwareUpgradeReducer,
  saveLoad: saveLoadReducer,
  currentTuner: currentTunerReducer,
  addConnection: addConnectionReducer,
  deviceSettings: deviceSettingsReducer,
  terminal: terminalReducer,
  menu: menuReducer,
  processController: processControllerReducer,
  protocolManuals: protocolManualReducer,
  cloud: cloudReducer,
  peripherals: peripheralsReducer,
  updates: updatesReducer,
  localShare: localShareReducer,
  preferences: preferencesReducer,
  servoTuning: servoTunerReducer,
  connectionStatus: connectionStatusReducer,
  help: helpReducer,
  lc40Setup: lc40SetupReducer,
  deviceSetup: deviceSetupReducer,
  apps: appsReducer,
  gcode: gcodeReducer,
  lockstepConfiguration: lockstepConfigurationReducer,
  demo: demoReducer,
  deviceIo: deviceIoReducer,
  joystick: joystickReducer,
  factoryReset: factoryResetReducer,
  oscilloscope: oscilloscopeReducer,
  units: unitReducer,
  internal: internalReducer,
  connectionMonitor: connectionMonitorReducer,
  pvt: pvtReducer,
  cManagerUi: cManagerUiReducer,
  calibration: calibrationReducer,
  microscope: microscopeReducer,
  triggers: triggersReducer,
});
