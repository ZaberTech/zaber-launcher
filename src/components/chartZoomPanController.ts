import type React from 'react';

import type { ZoomPanCollection, ZoomPanState } from './';


export enum MouseButtons {
  PRIMARY = 1,
  SECONDARY = 2,
  AUX = 4,
  FOURTH = 8,
  FIFTH = 16,
}


export interface ZoomEventSingleAxis extends ZoomPanState {
  dataZoomId: string;
}

export interface ZoomEventMultiAxis {
  batch: ZoomEventSingleAxis[];
}

export interface ZoomEventRectAxis {
  dataZoomId: string;
  startValue: number;
  endValue: number;
}

export interface ZoomEventRectangle {
  batch: ZoomEventRectAxis[];
}

export enum MouseZoomRegion {
  NONE,
  CHART,
  TIMELINE,
  LEFTAXIS,
  RIGHTAXIS,
}


export type GetRegionCallback = (x: number, y: number, width: number) => MouseZoomRegion;
export type GetChartIndexCallback = (y: number) => number;
export type GetNormalizedChartCoordinatesCallback = (x: number, y: number, width: number) => { x: number; y: number };
export type GetZoomRangeFromDataCallback = (start: number, end: number, axisId: string) => ZoomPanState;


/**
 * Encapsulates zoom and pan behavior common to ECharts usage in the Oscilloscope and PVT apps.
 * Some baked-in assumptions:
 * - Each chart will only have at most one X axis and it's always at the bottom.
 * - Each chart can have zero or more Y axes on the left and right.
 * - All X axes zoom and pan together.
 * - Y axes zoom and pan together when the mouse is over the chart, else individually.
 */
export class ChartZoomPanController {
  private mouseZoomRegion = MouseZoomRegion.NONE;
  private mouseChartIndex = 0;
  private mouseChartXNorm = 0;
  private mouseChartYNorm = 0;
  private preventRecursiveEvents = false;
  private enableMouseZoomPan = false;


  constructor(public zoomPan: ZoomPanCollection,
    private getRegion: GetRegionCallback,
    private getChartIndex: GetChartIndexCallback,
    private getNormalizedCoords: GetNormalizedChartCoordinatesCallback,
    private getZoomRangeFromData: GetZoomRangeFromDataCallback) {
  }


  public get enableMouseZoomAndPan() {
    return this.enableMouseZoomPan;
  }


  public set enableMouseZoomAndPan(enable: boolean) {
    this.enableMouseZoomPan = enable;
  }


  public makeControlId(chartIndex: number, axis: 'x' | 'y', side: 'left' | 'right' | 'bottom', axisIndex: number): string {
    return `${chartIndex}_${axis}_${side}_${axisIndex}`;
  }


  public makeControlIdFilter(chartIndex: number | null,
    axis: 'x' | 'y' | null,
    side: 'left' | 'right' | 'bottom' | null,
    axisIndex: number | null): RegExp {
    const parts = [
      `${chartIndex ?? '[^_]*'}`,
      `${axis ?? '[^_]*'}`,
      `${side ?? '[^_]*'}`,
      `${axisIndex ?? '.*'}`,
    ];

    return new RegExp(parts.join('_'));
  }


  public onMouseMove(event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    viewPort: HTMLDivElement,
    hMargins: number,
    chartHeight: number): void {
    const canvasBounds = event?.currentTarget?.getBoundingClientRect();
    if (canvasBounds && viewPort) {
      const x = event.clientX - canvasBounds.left;
      const y = event.clientY - canvasBounds.top;
      this.mouseZoomRegion = this.getRegion(x, y + viewPort.scrollTop, canvasBounds.width);
      const { x: xNorm, y: yNorm } = this.getNormalizedCoords(x, y + viewPort.scrollTop, canvasBounds.width);
      this.mouseChartXNorm = xNorm;
      this.mouseChartYNorm = yNorm;
      this.mouseChartIndex = this.getChartIndex(y + viewPort.scrollTop);

      // Handle mouse panning within the chart area.
      if (this.enableMouseZoomAndPan && this.mouseZoomRegion === MouseZoomRegion.CHART && (event.buttons & MouseButtons.PRIMARY) !== 0) {
        event.preventDefault();
        const xScale = 1 / (window.devicePixelRatio * (viewPort.clientWidth - hMargins));
        const yScale = 1 / (window.devicePixelRatio * chartHeight);
        const dx = xScale * event.movementX;
        const dy = yScale * event.movementY;

        const xFilter = this.makeControlIdFilter(null, 'x', null, null);
        this.zoomPan.pan(id => !!id.match(xFilter), -dx);
        const yFilter = this.makeControlIdFilter(this.mouseChartIndex, 'y', null, null);
        this.zoomPan.pan(id => !!id.match(yFilter), dy);
        this.zoomPan.fireNotifications();
      }
    }
  }


  public onMouseWheel(event: React.WheelEvent) {
    if (!this.enableMouseZoomAndPan) {
      // Mouse wheel should always scroll the chart area unless the zoom & pan modifier key is pressed.
      const target = event?.currentTarget;
      if (target) {
        target.scrollBy(event.deltaX, event.deltaY);
      }
    } else if (this.mouseZoomRegion !== MouseZoomRegion.NONE) {
      // Handle mouse zooming.
      // We do not use ECharts's "inside" dataZoom control for this because it
      // will override the slider dataZooms without overwriting their states, and
      // will lock them to the same scale and position - both are poor UX for our use case.
      const delta = (Math.abs(event.deltaX) > Math.abs(event.deltaY)) ? event.deltaX : event.deltaY;
      const proportion = (delta > 0) ? -0.1 : 0.1;

      switch (this.mouseZoomRegion) {
        case MouseZoomRegion.TIMELINE:
          {
            const id = this.makeControlId(this.mouseChartIndex, 'x', 'bottom', 0);
            this.zoomPan.zoom(id, this.mouseChartXNorm, proportion);
            const xFilter = this.makeControlIdFilter(null, 'x', null, null);
            this.zoomPan.copy(id, key => !!key.match(xFilter));
          }
          break;

        case MouseZoomRegion.LEFTAXIS:
          {
            const filter = this.makeControlIdFilter(this.mouseChartIndex, 'y', 'left', null);
            this.zoomPan.zoom(id => !!id.match(filter), this.mouseChartYNorm, proportion);
          }
          break;

        case MouseZoomRegion.RIGHTAXIS:
          {
            const filter = this.makeControlIdFilter(this.mouseChartIndex, 'y', 'right', null);
            this.zoomPan.zoom(id => !!id.match(filter), this.mouseChartYNorm, proportion);
          }
          break;

        case MouseZoomRegion.CHART:
          {
            // When the mouse is over the chart itself, zoom all Y axes associated with
            // the individual chart separately, and all X axes together.
            const id = this.makeControlId(this.mouseChartIndex, 'x', 'bottom', 0);
            this.zoomPan.zoom(id, this.mouseChartXNorm, proportion);
            this.zoomPan.copy(id, id => !!id.match(this.makeControlIdFilter(null, 'x', null, null)));

            const filter = this.makeControlIdFilter(this.mouseChartIndex, 'y', null, null);
            this.zoomPan.zoom(id => !!id.match(filter), this.mouseChartYNorm, proportion);
          }
          break;

        default:
          break;
      }

      this.zoomPan.fireNotifications();
    }
  }


  public onDataZoom(event: ZoomEventSingleAxis | ZoomEventMultiAxis | ZoomEventRectangle) {
    const reRectZoomX = /.*toolbox-dataZoom_x.*/; // These names set fixed by eCharts.
    const reRectZoomY = /.*toolbox-dataZoom_y.*/;

    const axes = 'batch' in event ? event.batch : [event];
    for (const axis of axes) {
      // Special handling for rectangle select zoom.
      if (axis.dataZoomId.match(reRectZoomX)) {
        const rectAxis = axis as ZoomEventRectAxis;
        const range = this.getZoomRangeFromData(rectAxis.startValue, rectAxis.endValue, this.makeControlId(0, 'x', 'bottom', 0));
        if (!this.preventRecursiveEvents) {
          this.preventRecursiveEvents = true;
          const xFilter = this.makeControlIdFilter(null, 'x', null, null);
          this.zoomPan.set(key => !!key.match(xFilter), range);
          this.zoomPan.fireNotifications();
          this.preventRecursiveEvents = false;
        }
      } else if (axis.dataZoomId.match(reRectZoomY)) {
        const yFilter = this.makeControlIdFilter(this.mouseChartIndex, 'y', null, null);
        const rectAxis = axis as ZoomEventRectAxis;
        this.zoomPan.getIds(id => !!id.match(yFilter)).forEach(id => {
          const range = this.getZoomRangeFromData(rectAxis.startValue, rectAxis.endValue, id);
          this.zoomPan.set(id, range);
        });
        this.zoomPan.clearNotifications();
      } else {
        // Not a rectangle select event - must have come from a slider control.
        const singleAxis = axis as ZoomEventSingleAxis;
        const range = { start: singleAxis.start, end: singleAxis.end };
        // Copy any X slider changes to the others.
        const xFilter = this.makeControlIdFilter(null, 'x', null, null);
        if (axis.dataZoomId.match(xFilter)) {
          if (!this.preventRecursiveEvents) {
            this.preventRecursiveEvents = true;
            this.zoomPan.set(key => !!key.match(xFilter), range);
            this.zoomPan.fireNotifications();
            this.preventRecursiveEvents = false;
          }
        } else {
          this.zoomPan.set(axis.dataZoomId, range);
          this.zoomPan.clearNotifications();
        }
      }
    }
  }
}
