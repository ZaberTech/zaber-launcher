import React from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';

jest.mock('uuid', () => ({ v4: () => 'uuid' }));

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore, mockStorage, StorageMock } from '../test';
import { MessageRoutersService } from '../message_router';

import { LocalShareConfig, GET_STATUS_INTERVAL } from './LocalShareConfig';
import { MessageRoutersServiceMock } from './mocks';
import { FIRST_TIME_STORAGE_KEY } from './sagas';

const TestLocalShareConfig = wrapWithNewStore(LocalShareConfig);

let wrapper: RenderResult;

beforeAll(() => {
  jest.useFakeTimers();
});
afterAll(() => {
  jest.useRealTimers();
});

let container: ReturnType<typeof createContainer>;
let messageRouter: MessageRoutersServiceMock;
let storage: StorageMock;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  messageRouter = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;

  storage = mockStorage(container);
  storage.stored[FIRST_TIME_STORAGE_KEY] = false;
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

test('displays error if router fails', async () => {
  messageRouter.getLocalShareConfig.mockRejectedValueOnce(new Error('Config broken'));
  messageRouter.getLocalShareStatus.mockRejectedValueOnce(new Error('Status broken'));
  wrapper = render(<TestLocalShareConfig/>);

  await waitUntilPass(() => wrapper.getByText(/Config broken/));
  await waitUntilPass(() => wrapper.getByText(/Status broken/));
});

describe('after config and status retrieved', () => {
  beforeEach(async () => {
    wrapper = render(<TestLocalShareConfig/>);
    await waitUntilPass(() => wrapper.getByText(/Share my Devices/));
  });

  test('turns on the sharing', async () => {
    storage.stored[FIRST_TIME_STORAGE_KEY] = null;

    fireEvent.click(wrapper.getByTitle('Toggle Sharing'));

    await waitUntilPass(() => wrapper.getAllByText(/first time/));
    fireEvent.click(wrapper.getByText('OK'));

    await waitUntilPass(() =>
      expect(messageRouter.setLocalShareConfig).toHaveBeenCalledWith(expect.objectContaining({
        discoverable: true, enabled: true, port: 11421, instanceName: 'uuid',
      }))
    );
    expect(wrapper.getByTitle('Toggle Sharing')).toHaveClass('loading');

    messageRouter.isRunning = true;
    jest.advanceTimersByTime(GET_STATUS_INTERVAL);

    await waitUntilPass(() =>
      expect(wrapper.getByTitle('Toggle Sharing')).not.toHaveClass('loading')
    );
    expect(wrapper.getByTitle('Toggle Sharing')).toHaveAttribute('aria-checked', 'true');
  });

  test('updates discoverable', async () => {
    fireEvent.click(wrapper.getByTitle('Toggle Discoverable'));

    await waitUntilPass(() =>
      expect(messageRouter.setLocalShareConfig).toHaveBeenCalledWith(expect.objectContaining({
        discoverable: false,
      }))
    );

    fireEvent.click(wrapper.getByTitle('Toggle Discoverable'));

    await waitUntilPass(() =>
      expect(messageRouter.setLocalShareConfig).toHaveBeenCalledWith(expect.objectContaining({
        discoverable: true,
      }))
    );
  });

  test('updates port', async () => {
    fireEvent.focus(wrapper.getByRole('spinbutton', { name: 'TCP/IP Port:' }));
    fireEvent.input(wrapper.getByRole('spinbutton', { name: 'TCP/IP Port:' }), { target: { value: '999' } });
    fireEvent.click(wrapper.getByText('Save'));
    await waitUntilPass(() =>
      expect(messageRouter.setLocalShareConfig).toHaveBeenCalledWith(expect.objectContaining({
        port: 999,
      }))
    );
  });

  test('displays an error if the update fails', async () => {
    messageRouter.setLocalShareConfig.mockRejectedValueOnce(new Error('Config is broken'));
    fireEvent.click(wrapper.getByTitle('Toggle Sharing'));

    await waitUntilPass(() =>
      wrapper.getByText(/Config is broken/)
    );
  });

  test('displays an error if the message router cannot turn sharing on', async () => {
    messageRouter.getLocalShareStatus.mockResolvedValue({
      isRunning: false,
      isDiscoverable: false,
      lastRunError: 'Port 12345 is taken',
      lastDiscoverabilityError: 'mDNS just does not work',
    });

    jest.advanceTimersByTime(GET_STATUS_INTERVAL);
    await waitUntilPass(() => {
      wrapper.getByText(/Port 12345 is taken/);
      wrapper.getByText(/mDNS just does not work/);
    });
  });
});

describe('after config and status retrieved (turned on)', () => {
  beforeEach(async () => {
    messageRouter.isRunning = true;
    messageRouter.config = {
      discoverable: true,
      enabled: true,
      port: 1234,
      instanceName: 'name',
    };
    wrapper = render(<TestLocalShareConfig/>);
    await waitUntilPass(() => wrapper.getByText(/Share my Devices/));
  });

  test('turns-off sharing', async () => {
    fireEvent.click(wrapper.getByTitle('Toggle Sharing'));

    await waitUntilPass(() =>
      expect(messageRouter.setLocalShareConfig).toHaveBeenCalledWith(expect.objectContaining({
        enabled: false,
      }))
    );
    expect(wrapper.getByTitle('Toggle Sharing')).toHaveClass('loading');

    messageRouter.isRunning = false;
    jest.advanceTimersByTime(GET_STATUS_INTERVAL);

    await waitUntilPass(() =>
      expect(wrapper.getByTitle('Toggle Sharing')).not.toHaveClass('loading')
    );
    expect(wrapper.getByTitle('Toggle Sharing')).toHaveAttribute('aria-checked', 'false');
  });
});
