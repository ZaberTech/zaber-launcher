import _ from 'lodash';
import React from 'react';
import { useSelector } from 'react-redux';
import { Button, HeaderCard, Icons, SimpleSelect, Text } from '@zaber/react-library';

import { AutoCommitEditBox } from '../components/AutoCommitEditBox';
import { getUnitLabel } from '../units';
import { useActions } from '../utils';

import { actions as actionDefinitions } from './actions';
import { selectUserPath, selectSelectedPointIndex, selectSeriesDetailsVisibility } from './selectors';


const PointHeader: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const selectedPointIndex = useSelector(selectSelectedPointIndex);
  const userPath = useSelector(selectUserPath);

  const canDelete = selectedPointIndex != null && userPath != null && userPath.series[0].points.length > 2;

  return (<div className="properties-header">
    <Text t={Text.Type.H4}>Data Point</Text>
    {canDelete && <Icons.Trash
      data-testid="properties-delete-point-button"
      title="Delete this point"
      onClick={() => {
        actions.setSelection(Math.max(0, selectedPointIndex - 1));
        actions.deletePoint(selectedPointIndex);
        actions.commitEdit();
      }}/>}
  </div>);
};


export const PointPropertyPanel: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const selectedPointIndex = useSelector(selectSelectedPointIndex) ?? 0;
  const userPath = useSelector(selectUserPath);
  const expanded = useSelector(selectSeriesDetailsVisibility);

  if (userPath == null || userPath.series.length < 1) {
    return null;
  }

  const indexOptions = _.range(0, userPath.series[0].points.length).map(i => ({ value: i, label: `${i + 1}` }));

  return <div className="point-properties">
    <HeaderCard header={<PointHeader/>} data-testid="point-header-time">
      <SimpleSelect
        className="index-selector"
        data-testid="index-selector"
        options={indexOptions}
        value={selectedPointIndex}
        onValueChange={num => actions.setSelection(num)}/>
      <AutoCommitEditBox label="Segment Time (s)"
        key={`${selectedPointIndex}-dt-${userPath!.series[0].points[selectedPointIndex].dt}`}
        data-testid="edit-dt"
        initialValue={userPath.series[0].points[selectedPointIndex].dt}
        disabled={selectedPointIndex === 0}
        onChange={value => {
          actions.editSegmentTime(selectedPointIndex, value);
          actions.commitEdit();
        }}/>
      <AutoCommitEditBox label="Accumulated Time (s)"
        key={`${selectedPointIndex}-time-${userPath!.series[0].points[selectedPointIndex].time}`}
        initialValue={userPath.series[0].points[selectedPointIndex].time}/>
      {selectedPointIndex < userPath.series[0].points.length - 1 &&
        <Button color="grey"
          data-testid="properties-add-point-button"
          onClick={() => {
            actions.addPoint(selectedPointIndex);
            actions.commitEdit();
            actions.setSelection(selectedPointIndex + 1);
          }}><Icons.Plus/><Text>Add Point</Text></Button>}
    </HeaderCard>
    <div className="series-cards">
      {userPath.series.map((series, i) =>
        <HeaderCard key={i}
          header={series.name}
          collapsible
          expanded={expanded.has(series.name)}
          onToggle={val => actions.setSeriesDetailsVisible(series.name, val)}
          data-testid={`point-header-${series.name}`}>
          <AutoCommitEditBox label={`Position (${getUnitLabel(userPath.positionUnit)})`}
            key={`${selectedPointIndex}-${i}-pos-${series.points[selectedPointIndex].pos}`}
            data-testid={`edit-${series.name}-pos`}
            initialValue={series.points[selectedPointIndex].pos}
            onChange={value => {
              actions.editPosition(selectedPointIndex, { [series.name]: value });
              actions.commitEdit();
            }}/>
          <AutoCommitEditBox label={`Velocity (${getUnitLabel(userPath.velocityUnit)})`}
            key={`${selectedPointIndex}-${i}-vel-${series.points[selectedPointIndex].vel}`}
            data-testid={`edit-${series.name}-vel`}
            initialValue={series.points[selectedPointIndex].vel}
            disabled={selectedPointIndex === 0 || selectedPointIndex === series.points.length - 1}
            onChange={value => {
              actions.editVelocity(selectedPointIndex, { [series.name]: value });
              actions.commitEdit();
            }}/>
        </HeaderCard>)}
    </div>
  </div>;
};
