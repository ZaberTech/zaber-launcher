import React, { useEffect }  from 'react';
import { useSelector } from 'react-redux';

import { NoContentMessage, Title } from '../components';
import { ConnectionsView } from '../connection_manager';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { Joystick } from './Joystick';
import { selectJoystick, selectConfigReading, selectSelectedKey } from './selectors';

const TITLE = 'Welcome to Joystick!';

const noSelection = <NoContentMessage title={TITLE} message="Select a X-JOY device from the menu on the left."/>;
const notJoystick = <NoContentMessage title={TITLE} message="Selected device is not a joystick." type="info"/>;
const readingConfig = <NoContentMessage title={TITLE} message="Reading configuration..." type="working"/>;

export const JoystickApp: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const joystick = useSelector(selectJoystick);
  const config = useSelector(selectConfigReading);
  const selectedKey = useSelector(selectSelectedKey);

  useEffect(() => {
    actions.mount();
  }, []);

  return (<div className="connection-view-and-app">
    <Title>X-JOY</Title>

    <ConnectionsView
      selectable={['device']}
      selected={selectedKey}
      onSelect={actions.setSelectedKey}/>
    <div className="app-ui joystick-app">
      {joystick == null && (selectedKey ? notJoystick : noSelection)}
      {joystick != null && <>
        {config.reading && readingConfig}
        {config.error != null && <NoContentMessage title={TITLE} type="error"
          bannerTitle="Reading Configuration Error"
          tryAgain={{ action: () => actions.setSelectedKey(selectedKey), button: 'Try Again' }}
          message={config.error}/>}
        {config.config != null && <Joystick key={selectedKey}/>}
      </>}
    </div>
  </div>);
};
