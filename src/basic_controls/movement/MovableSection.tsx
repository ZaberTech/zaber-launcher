import React from 'react';
import _ from 'lodash';
import { Button, Icons, Text } from '@zaber/react-library';
import { ascii, Units } from '@zaber/motion';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { P } from 'ts-pattern';

import { convertNullable, MeasurementOK, measurementOK, UnitInfo, UnitSelect, useUnitInfo } from '../../units';
import { isMatch, useActions } from '../../utils';
import { usePolledAxisSetting } from '../../polling';
import { Moveable, moveableKey } from '../types';
import { Shortcut, NoContentMessage } from '../../components';
import type { EntityKey } from '../../keys';
import type { PolledAxisWarnings } from '../../polling/hooks';

import { AxisMovementState, KeyPressCombo } from './types';
import { actionDefinitions } from './actions';
import { Track } from './Track';
import { Jog } from './Jog';
import { MoveableThumbnails } from './MoveableThumbnail';
import { selectAxisStates } from './selectors';
import { Run } from './Run';
import { PreciseMovement, preciseCommand } from './precise/PreciseMovement';
import { MoveIndex } from './MoveIndex';

const VELOCITY_FACTOR = 1.0 / 4.0;

const SHORTCUTS: Record<KeyPressCombo, [string, string]> = {
  [KeyPressCombo.None]: ['', ''],
  [KeyPressCombo.LeftRight]: ['ArrowLeft', 'ArrowRight'],
  [KeyPressCombo.UpDown]: ['ArrowDown', 'ArrowUp'],
  [KeyPressCombo.PageUpPageDown]: ['PageDown', 'PageUp'],
};

type MoveableUnitSelectProps = {
  axisKey: EntityKey;
  unitInfo: UnitInfo;
  value: Units;
  title?: string;
};

function moveTypeFromEvent(e: { shiftKey: boolean; ctrlKey: boolean }): 'vel' | 'rel' | 'vel-reduced' {
  return e.shiftKey ? 'vel' : e.ctrlKey ? 'rel' : 'vel-reduced';
}

const MoveableUnitSelect: React.FC<MoveableUnitSelectProps> = ({ axisKey, unitInfo, value, title }) => {
  const actions = useActions(actionDefinitions);
  if (unitInfo.supported) {
    return <UnitSelect
      title={title}
      dimensions={unitInfo.dimension}
      includeNative={true}
      value={value}
      borderless={true}
      onChange={unit => actions.setPosUnits(axisKey, unit)}/>;
  }
  return <Text>Native</Text>;
};

export const MovableSection: React.FC<{ moveable: Moveable; warnings: PolledAxisWarnings }> = ({ moveable, warnings }) => {
  const actions = useActions(actionDefinitions);
  const key = moveableKey(moveable);
  const { motion } = moveable;
  const state = useSelector(selectAxisStates)[key];
  const unitInfo = useUnitInfo({ entity: key, setting: 'pos' });
  const position = usePolledAxisSetting(key, { setting: 'pos', unit: Units.NATIVE }, state.lastCommand != null);
  const homingNeeded = warnings.type === 'flags' && warnings.flags.some(flag => flag === ascii.WarningFlags.NO_REFERENCE_POSITION);
  const { isIndexed } = motion;

  if (unitInfo == null) {
    return <NoContentMessage message="Loading..." type="working"/>;
  }

  const displayValue = (value: number | null) => {
    const convertedValue = convertNullable(unitInfo, value, Units.NATIVE, state.posUnit);
    if (convertedValue != null) {
      return _.round(convertedValue, 2);
    } else if (value != null && state.posUnit === Units.NATIVE) {
      return value;
    }
    return '-';
  };

  const move = (isReleased: boolean, direction: 'home' | 'away', type: 'vel' | 'rel' | 'vel-reduced') => {
    if (isReleased && state.lastCommand?.type === 'rel') {
      return;
    }
    let measurement: MeasurementOK = { value: 0.0, units: Units.NATIVE };
    const valueAndUnits = type === 'rel' ? state.preciseMovement.relative : state.preciseMovement.velocity;
    if (!isReleased && measurementOK(valueAndUnits)) {
      measurement =  valueAndUnits;
    }
    if (type === 'vel-reduced') {
      measurement = { value: VELOCITY_FACTOR * measurement.value, units: measurement.units };
      type = 'vel';
    }
    actions.issueCommand(moveable, preciseCommand(type, measurement, direction));
  };

  return (<>
    <div className={classNames('moveable-section', motion.type, moveable.type)} title="Moveable Section">
      <MoveableThumbnails moveable={moveable}/>

      <Track moveable={moveable} axisState={state}/>

      <Icons.Home className="home" title="Home Axis"
        highlight={homingNeeded ? 'on' : 'off'}
        activated={state.lastCommand?.type === 'home'}
        onClick={() => actions.issueCommand(moveable, { type: 'home' })}
      />
      <Run moveable={moveable} direction="home" state={state}/>
      {!isIndexed && <Jog moveable={moveable} direction="home" state={state}/>}
      {isIndexed && <MoveIndex moveable={moveable} direction="prev" state={state}/>}
      <Icons.Stop highlight="off" title="Stop Axis" className="stop" onClick={() => actions.issueCommand(moveable, { type: 'stop' })}/>
      {!isIndexed && <Jog moveable={moveable} direction="away" state={state}/>}
      {isIndexed && <MoveIndex moveable={moveable} direction="next" state={state}/>}
      <Run moveable={moveable} direction="away" state={state}/>
      {motion.type === 'linear' ? (<div className="position">
        <Text t={Text.Type.H5}>Position:</Text>
        <span className="info-line">
          <Text title="Linear Axis Position" className="info-text">{displayValue(position)}</Text>
          <MoveableUnitSelect title="Select Position Units" axisKey={key} unitInfo={unitInfo} value={state.posUnit}/>
        </span>
      </div>) : (<div className="position">
        <Text t={Text.Type.H5}>Position:</Text>
        <span className="info-line">
          <Text title="Rotary Axis Position" className="info-text">{displayValue(position)}</Text>
          <MoveableUnitSelect title="Select Position Units" axisKey={key} unitInfo={unitInfo} value={state.posUnit}/>
        </span>
        <Text t={Text.Type.H5}>Cycle:</Text>
        <span className="info-line">
          <Text className="info-text">{displayValue(position && position % motion.travel)}</Text>
          &nbsp;<Text className="info-text">of</Text>&nbsp;
          <Text className="info-text">{displayValue(motion.travel)}</Text>
        </span>
      </div>)}
      <AdditionalControls moveable={moveable} state={state}/>
    </div>
    <PreciseMovement moveable={moveable} state={state}/>
    {state.shortcut !== KeyPressCombo.None &&
    [
      <Shortcut key={0}
        k={SHORTCUTS[state.shortcut][0]} shift="ignore" ctrl="ignore"
        onDown={e => (move(false, 'home', moveTypeFromEvent(e)))}
        onUp={e => (move(true, 'home', moveTypeFromEvent(e)))}
        preventDefault/>,
      <Shortcut key={1}
        k={SHORTCUTS[state.shortcut][1]} shift="ignore" ctrl="ignore"
        onDown={e => move(false, 'away', moveTypeFromEvent(e))}
        onUp={e => move(true, 'away', moveTypeFromEvent(e))}
        preventDefault/>
    ]}
  </>);
};

const AdditionalControls: React.FC<{ moveable: Moveable; state: AxisMovementState }> = ({ moveable, state }) => {
  const actions = useActions(actionDefinitions);

  const repenting = state.lastCommand?.type === 'stop sin';

  return <div className="additional-controls">
    {isMatch(P.union('sin', 'stop sin'), state.lastCommand?.type) &&
      <Button color="grey"
        title="Gracefully stops at the end of the current period"
        disabled={repenting}
        onClick={() => actions.issueCommand(moveable, { type: 'stop sin' })}>
        End{repenting && <>ing</>} Sinusoidal Motion{repenting && <>...</>}
      </Button>}
  </div>;
};
