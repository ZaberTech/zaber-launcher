/* eslint-disable camelcase */
let openFileName: string;
let cancelFileDialog: boolean;

jest.mock('../dialogs', () => ({
  Dialogs: {
    showSaveDialog: jest.fn((opts: {defaultPath: string}) => ({
      canceled: cancelFileDialog,
      filePath: opts.defaultPath,
    })),
    showOpenDialog: jest.fn(() => ({
      canceled: cancelFileDialog,
      filePaths: [openFileName],
    }))
  }
}));

const ffpid4Csv = { 'Controller Type': 'ffpid', 'Controller Version': '4' };

jest.mock('csv/sync', () => ({
  parse: jest.fn(() => [
    { 'Name': 'First Tuning', ...ffpid4Csv, 'gain': 1.23, 'fcla': 2, 'fcla.hs': 1.9, 'gloop': 3 },
    { 'Name': 'Second Tuning', ...ffpid4Csv, 'gain': 3.21, 'fcla': 3, 'fcla.hs': 2.9, 'gloop': 55 },
  ]),
  stringify: jest.fn(),
}));

import fs from 'fs';

import * as CSV from 'csv/sync';
import { fireEvent, render, RenderResult, within } from '@testing-library/react';
import { Container, injectable } from 'inversify';
import React from 'react';
import _ from 'lodash';
import { ServoTuner, ServoTuningParamset } from '@zaber/motion/ascii';
import { Acceleration, Length, MotionLibException, NoValueForKeyException, Time, Units } from '@zaber/motion';
import type { SagaIterator } from 'redux-saga';
import { settingsMetadata } from '@zaber/device-db-metadata';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { mockStorage, StorageMock, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase
} from '../test/mocks/ascii';
import { selectDevices } from '../connection_manager';
import { createContainer, destroyContainer } from '../container';
import { MessageRoutersService } from '../message_router';
import { mockSingleDevice } from '../connection_manager/mocks';
import { ZaberApi } from '../app_components';
import {
  editValueAndUnitsByLabel,
  getValueInputByTitle,
  setUnitSelectByTitle,
  UnitInfoServiceMockBase,
} from '../units/test_utils';
import { UnitInfo, UnitInfoService, unsupportedUnitInfo } from '../units';

import { App } from './App';
import { paramsetNames } from './names';

jest.spyOn(fs.promises, 'writeFile').mockImplementation(() => Promise.resolve());

jest.spyOn(fs.promises, 'readFile').mockImplementation(() => Promise.resolve('load_file_contents'));

jest.spyOn(ZaberApi.prototype, 'getServoTuningInfo').mockImplementation(async () => ({
  Algorithm: 'ffpid',
  AlgorithmVersion: 4,
  Forque: 15.83,
  CarriageInertia: 1500,
  Guidance: [],
}));

jest.spyOn(ServoTuner.prototype, 'getStartupParamset').mockImplementation(() => Promise.resolve(1));
const defaultParams = [
  { name: 'finv1', value: 1 },
  { name: 'fcla', value: 2 },
  { name: 'fcla.hs', value: 1.9 },
  { name: 'gloop', value: 3 },
];
let tuningParams: typeof defaultParams = defaultParams;
jest.spyOn(ServoTuner.prototype, 'getTuning').mockImplementation(() => Promise.resolve({
  type: 'ffpid',
  version: 4,
  params: tuningParams,
}));
const setTuningSpy = jest.spyOn(ServoTuner.prototype, 'setTuning').mockImplementation(() => Promise.resolve());
const loadSpy = jest.spyOn(ServoTuner.prototype, 'loadParamset').mockImplementation(() => Promise.resolve());

let wrapper: RenderResult;
let container: Container;
let storageMock: StorageMock;


let axisStorage: Record<string, number>;
let axes: AxisMock[];
const defaultAccel = 20;
const defaultSettleTolerance = 0.5;
const defaultSettlePeriod = 0.05;
class AxisMock extends AxisMockBase {
  constructor(axisNumber: number, device: DeviceMockBase<AxisMockBase>) {
    super(axisNumber, device);
    axes.push(this);
  }
  enabled = 1;

  settings = {
    get: jest.fn(async (setting: string) => {
      switch (setting) {
        case 'accel': return defaultAccel;
        case 'cloop.settle.tolerance': return defaultSettleTolerance;
        case 'cloop.settle.period': return defaultSettlePeriod;
        case 'driver.current.servo': return 4.24;
        case 'driver.enabled': return this.enabled;
        default: return 0;
      }
    }),
    set: jest.fn(async (setting, value) => {
      if (value < 0) {
        throw new MotionLibException(`Command "set accel ${value}" rejected: BADDATA: The command's data was incorrect or out of range.`);
      } else if (setting === 'driver.enabled') {
        this.enabled = value;
      }
    }),
  };
  storage = {
    getNumber: jest.fn((key: string) => {
      if (axisStorage[key] != null) { return axisStorage[key] }
      throw new NoValueForKeyException(`No stored value with key ${key}`);
    }),
    setNumber: jest.fn(),
    eraseKey: jest.fn(),
  };
  driverEnable = jest.fn().mockResolvedValue(undefined);
  driverDisable = jest.fn().mockResolvedValue(undefined);
}

let devices: DeviceMock[];
class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(id: number, connection: ConnectionMock) {
    super(id, connection);
    devices.push(this);
  }
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

@injectable()
class UnitInfoServiceMock extends UnitInfoServiceMockBase {
  public* getSettingUnitInfo(entity: string, setting: string): SagaIterator<UnitInfo> {
    switch (setting) {
      case 'accel': return this.mockUnitInfo('Acceleration', 1, 100);
      case 'cloop.settle.period': return this.mockUnitInfo('Time', 1, 100);
      case 'cloop.settle.tolerance': return this.mockUnitInfo('Length', 1, 100);
      default: return unsupportedUnitInfo();
    }
  }
}

const TestServoTuningApp = wrapWithNewStore(wrapWithRouter(App));

const checkInput = (testId: string, expectedValue: number | string) => {
  const input = wrapper.getByTestId(testId) as HTMLInputElement;
  expect(input.value).toBe(expectedValue.toString());
};

const setInput = (testId: string, value: number | string) => {
  const input = wrapper.getByTestId(testId) as HTMLInputElement;
  fireEvent.change(input, { target: { value: value.toString() } });
};

const checkInputOfLabel = (label: string, expectedValue: number | string) => {
  const input = wrapper.getByLabelText(label) as HTMLInputElement;
  expect(input.value).toBe(expectedValue.toString());
};

const setInputOfLabel = (label: string, value: number | string) => {
  const input = wrapper.getByLabelText(label) as HTMLInputElement;
  fireEvent.input(input, { target: { value: value.toString() } });
};

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(UnitInfoService).to(UnitInfoServiceMock);
  storageMock = mockStorage(container);

  axisStorage = {};
  axes = [];
  devices = [];

  setTuningSpy.mockReset();
  loadSpy.mockReset();

  wrapper = render(<TestServoTuningApp/>);

  tuningParams = defaultParams;
  openFileName = '';
  cancelFileDialog = false;
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  axes = [];
  devices = [];

  destroyContainer();
  container = null!;
  storageMock.stored = {};
});

describe('Servo Tuning', () => {
  beforeEach(() => {
    jest.spyOn(ServoTuner.prototype, 'getSimpleTuning').mockImplementation(async () => ({
      loadMass: 5,
      tuningParams: [{ name: 'Stiffness', defaultValue: 0.5, value: 0.2 }],
      isUsed: false,
    }));

    jest.spyOn(ServoTuner.prototype, 'getSimpleTuningParamDefinitions').mockImplementation(async () => [{
      name: 'Stiffness',
      minLabel: 'Smooth',
      maxLabel: 'Stiff',
      dataType: '',
      defaultValue: 0.1,
    },
    {
      name: 'Overshoot Compensation',
      minLabel: 'Off',
      maxLabel: 'Max',
      dataType: '',
    }]);
  });


  const servoGetStartupCommand = { command: 'servo', nodes: [{ command: 'get', nodes: [{ command: 'startup', is_command: true }] }] };

  const storageCommand =  { command: 'storage', nodes: [{ command: 'axis', nodes: [{ command: 'print', is_command: true }] }] };
  const initServoTunableDevice = async () => {
    const store = TestServoTuningApp.testStore;
    mockSingleDevice(store, d => {
      d.product.commandTree.nodes = [...(d.product.commandTree.nodes ?? []), servoGetStartupCommand, storageCommand];
      d.product.settings.rows = [
        ...d.product.settings.rows, {
          name: 'cloop.settle.period',
          contextual_dimension_id: 11,
          param_type: 'Uint32',
          decimal_places: 0,
          read_access_level: 1,
          write_access_level: 1,
          visibility: 'advanced',
          default_value: '50',
          realtime: true
        }, {
          name: 'cloop.settle.tolerance',
          contextual_dimension_id: 1,
          param_type: 'Uint64',
          decimal_places: 0,
          read_access_level: 1,
          write_access_level: 1,
          visibility: 'advanced',
          default_value: '500',
          realtime: true
        },
      ];
      return d;
    });
    const device = _.sample(selectDevices(TestServoTuningApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getByText('Live Tuning'));
  };

  describe('Basics', () => {
    test('Product that cannot be tuned', () => {
      const store = TestServoTuningApp.testStore;
      mockSingleDevice(store);
      wrapper.getByText('Select a device or peripheral to tune.');
      const device = _.sample(selectDevices(TestServoTuningApp.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      wrapper.getByText('This product cannot be servo tuned');
    });

    test('Product that cannot be simple tuned', async () => {
      const store = TestServoTuningApp.testStore;
      mockSingleDevice(store, d => {
        d.product.commandTree.nodes = [...(d.product.commandTree.nodes ?? []), servoGetStartupCommand];
        return d;
      });
      const device = _.sample(selectDevices(TestServoTuningApp.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => wrapper.getByText(
        'This product does not support Simple Tuning in Zaber Launcher. Please update the device or controller firmware and try again.'
      ));
      expect(axes[0].storage.getNumber).toHaveBeenCalledTimes(0);
    });

    test('Disables Driver', async () => {
      await initServoTunableDevice();
      const buttons = within(wrapper.getByTestId('servo-tuning-buttons'));
      fireEvent.click(buttons.getByText('Disable Driver'));
      await waitUntilPass(() => buttons.getByText('Enable Driver'), 4000);
      expect(axes[0].driverDisable).toHaveBeenCalled();
    });

    test('Checks for driver state on window focus', async () => {
      await initServoTunableDevice();
      const buttons = within(wrapper.getByTestId('servo-tuning-buttons'));
      await waitUntilPass(() => buttons.getByText('Disable Driver'));
      fireEvent.blur(window);
      axes[0].enabled = 0;
      fireEvent.focus(window);
      await waitUntilPass(() => buttons.getByText('Enable Driver'));
    });
  });

  describe('Advanced Tuning', () => {
    const selectParamset = async (paramset: ServoTuningParamset) => {
      const paramsetName = paramsetNames[paramset];
      fireEvent.click(wrapper.getByText(paramsetName));
      fireEvent.click(wrapper.getByText('Advanced'));
      await waitUntilPass(() => {
        wrapper.getByText('Parameter Value');
        wrapper.getByText('At High Speed');
      });
    };


    beforeEach(initServoTunableDevice);

    test('load tuning', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      checkInput('described-param-finv1', 1);
      checkInput('described-param-fcla', 2);
      checkInput('high-speed-param-fcla', 1.9);
      checkInput('other-param-gloop', 3);
    });

    test('apply live tuning', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      setInput('described-param-fcla', 2.2);
      setInput('high-speed-param-fcla', 1.7);
      fireEvent.click(wrapper.getByText('Apply Tuning'));
      await waitUntilPass(() => wrapper.getAllByText('Applying...'));
      await waitUntilPass(() => expect(wrapper.queryByText('Applying...')).toBeNull());
      expect(setTuningSpy).toHaveBeenCalledWith(ServoTuningParamset.STAGING, [
        { name: 'finv1', value: 1 },
        { name: 'fcla', value: 2.2 },
        { name: 'fcla.hs', value: 1.7 },
        { name: 'gloop', value: 3 },
      ]);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.LIVE, ServoTuningParamset.STAGING);
    });

    test('save preset tuning', async () => {
      await selectParamset(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByLabelText('Use as the Live Tuning'));
      fireEvent.click(wrapper.getByText('Apply Tuning'));
      await waitUntilPass(() => wrapper.getByText('Applying...'));
      await waitUntilPass(() => expect(wrapper.queryByText('Applying...')).toBeNull());
      expect(setTuningSpy).toHaveBeenCalledWith(ServoTuningParamset.STAGING, [...defaultParams]);
      expect(loadSpy).toHaveBeenCalledTimes(1);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.P_1, ServoTuningParamset.STAGING);
    });

    test('save and apply tuning', async () => {
      await selectParamset(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('Apply Tuning'));
      await waitUntilPass(() => wrapper.getByText('Applying...'));
      await waitUntilPass(() => expect(wrapper.queryByText('Applying...')).toBeNull());
      expect(setTuningSpy).toHaveBeenCalledWith(ServoTuningParamset.STAGING, [...defaultParams]);
      expect(loadSpy).toHaveBeenCalledTimes(2);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.P_1, ServoTuningParamset.STAGING);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.LIVE, ServoTuningParamset.STAGING);
    });
  });

  describe('PID Tuning', () => {
    const selectParamset = async (paramset: ServoTuningParamset) => {
      const paramsetName = paramsetNames[paramset];
      fireEvent.click(wrapper.getByText(paramsetName));
      fireEvent.click(wrapper.getByText('PID'));
      await waitUntilPass(() => wrapper.getByText('Enter PID parameters to tune your device.'));
    };

    const defaultTuning = {
      type: 'ffpid',
      version: 4,
      p: 70_000,
      i: 6_500_000,
      d: 630,
      fc: 590,
    };
    jest.spyOn(ServoTuner.prototype, 'getPidTuning').mockImplementation(() => Promise.resolve(defaultTuning));
    const setPidSpy = jest.spyOn(ServoTuner.prototype, 'setPidTuning').mockImplementation(() => Promise.resolve(defaultTuning));

    beforeEach(async () => {
      setPidSpy.mockReset();
      await initServoTunableDevice();
    });

    test('load pid tuning', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      checkInput('kp', 70_000);
      checkInput('ki', 6_500_000);
      checkInput('kd', 630);
      checkInput('fc', 590);
    });

    test('apply pid tuning live', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      setInput('kp', 1);
      setInput('fc', 2);
      fireEvent.click(wrapper.getByText('Apply Tuning'));
      await waitUntilPass(() => wrapper.getAllByText('Applying...'));
      await waitUntilPass(() => expect(wrapper.queryByText('Applying...')).toBeNull());
      expect(setPidSpy).toHaveBeenCalledWith(ServoTuningParamset.STAGING, 1, 6_500_000, 630, 2);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.LIVE, ServoTuningParamset.STAGING);
    });

    test('save pid tuning to preset', async () => {
      await selectParamset(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByLabelText('Use as the Live Tuning'));
      fireEvent.click(wrapper.getByText('Apply Tuning'));
      await waitUntilPass(() => wrapper.getByText('Applying...'));
      await waitUntilPass(() => expect(wrapper.queryByText('Applying...')).toBeNull());
      expect(setPidSpy).toHaveBeenCalledWith(ServoTuningParamset.STAGING, 70_000, 6_500_000, 630, 590);
      expect(loadSpy).toHaveBeenCalledTimes(1);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.P_1, ServoTuningParamset.STAGING);
    });

    test('save pid tuning and apply', async () => {
      await selectParamset(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('Apply Tuning'));
      await waitUntilPass(() => wrapper.getByText('Applying...'));
      await waitUntilPass(() => expect(wrapper.queryByText('Applying...')).toBeNull());
      expect(setPidSpy).toHaveBeenCalledWith(ServoTuningParamset.STAGING, 70_000, 6_500_000, 630, 590);
      expect(loadSpy).toHaveBeenCalledTimes(2);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.P_1, ServoTuningParamset.STAGING);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.LIVE, ServoTuningParamset.STAGING);
    });
  });

  describe('Simple Tuning', () => {
    const selectParamset = async (paramset: ServoTuningParamset) => {
      const paramsetName = paramsetNames[paramset];
      fireEvent.click(wrapper.getByText(paramsetName));
      fireEvent.click(wrapper.getByText('Simple'));
      await waitUntilPass(() => wrapper.getByText(/Input the load mass being carried by this device, and adjust the slider/));
    };
    const setSimpleSpy = jest.spyOn(ServoTuner.prototype, 'setSimpleTuning').mockImplementation(() => Promise.resolve());

    beforeEach(async () => {
      setSimpleSpy.mockReset();
      await initServoTunableDevice();
    });

    test('load simple tuning', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      checkInputOfLabel('Load Mass', 5);
      checkInput('Stiffness-slider', 0.2);
      checkInput('Overshoot Compensation-slider', 0.5);

      expect(wrapper.queryByLabelText('Carriage Mass')).toBeNull();
      expect(wrapper.queryByTestId('Stiffness-default')).toHaveTextContent('Default: 0.1');
      expect(wrapper.queryByTestId('Overshoot Compensation-default')).toHaveTextContent('Default: 0.5');
      fireEvent.click(wrapper.getByText('I\'m using a custom carriage mass'));
      wrapper.getByLabelText('Carriage Mass');
    });

    test('displays a non-blocking error if getting tuning fails', async () => {
      jest.spyOn(ServoTuner.prototype, 'getSimpleTuning').mockImplementation(async () => {
        throw new MotionLibException('Fail for test');
      });
      await selectParamset(ServoTuningParamset.P_3);
      await waitUntilPass(() => wrapper.getByText(
        'The simple tuning saved for this paramset is invalid (Fail for test). To fix this set a valid simple tuning.'
      ));
      checkInput('Stiffness-slider', 0.1);
    });

    test('apply simple tuning', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      setInputOfLabel('Load Mass', 3);
      setInput('Stiffness-slider', 0.75);
      fireEvent.click(wrapper.getByText('Apply Tuning'));
      await waitUntilPass(() => wrapper.getAllByText('Applying...'));
      await waitUntilPass(() => expect(wrapper.queryByText('Applying...')).toBeNull());
      expect(setSimpleSpy).toHaveBeenCalledWith(
        ServoTuningParamset.STAGING,
        [{ name: 'Stiffness', value: 0.75 }, { name: 'Overshoot Compensation', value: 0.5 }],
        3,
        { });
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.LIVE, ServoTuningParamset.STAGING);
    });

    test('Warns when tuning would wipe out changes', async () => {
      await selectParamset(ServoTuningParamset.P_1);
      checkInputOfLabel('Load Mass', 5);
      checkInput('Stiffness-slider', 0.2);
      wrapper.getByText(/The tuning saved to Preset 1 was changed somewhere else since the last time this Simple Tuning was applied/);
      wrapper.getByText(/If you use this tuning, those changes will be lost/);
    });

    test('Requires modified carriage mass when unchecked', async () => {
      await selectParamset(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('I\'m using a custom carriage mass'));
      setInputOfLabel('Carriage Mass', '');
      fireEvent.click(wrapper.getByLabelText('Use as the Live Tuning'));
      fireEvent.click(wrapper.getByText('Apply Tuning'));
      const errMsg = 'Invalid Carriage Mass. If your carriage is not modified, uncheck the "Custom Carriage" control';
      wrapper.getByText(errMsg);
      fireEvent.click(wrapper.getByText('Ok'));
      expect(wrapper.queryByText(errMsg)).toBeNull();
    });

    test('save simple tuning', async () => {
      await selectParamset(ServoTuningParamset.P_1);
      setInputOfLabel('Load Mass', 4);
      fireEvent.click(wrapper.getByText('I\'m using a custom carriage mass'));
      setInputOfLabel('Carriage Mass', 5);
      fireEvent.click(wrapper.getByText('Apply Tuning'));
      await waitUntilPass(() => wrapper.getByText('Applying...'));
      await waitUntilPass(() => expect(wrapper.queryByText('Applying...')).toBeNull());
      expect(setSimpleSpy).toHaveBeenCalledWith(
        ServoTuningParamset.STAGING,
        [{ name: 'Stiffness', value: 0.2 }, { name: 'Overshoot Compensation', value: 0.5 }],
        4,
        { carriageMass: 5 });
      expect(loadSpy).toHaveBeenCalledTimes(2);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.P_1, ServoTuningParamset.STAGING);
      expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.LIVE, ServoTuningParamset.STAGING);
    });
  });

  describe('Load Preset', () => {
    const selectParamset = async (paramset: ServoTuningParamset) => {
      const paramsetName = paramsetNames[paramset];
      fireEvent.click(wrapper.getByText(paramsetName));
      fireEvent.click(wrapper.getByText('Load Preset'));
      await waitUntilPass(() => wrapper.getByText(`Select a preset to load into ${paramsetName}:`));
    };

    beforeEach(initServoTunableDevice);

    test('load preset to live', async () => {
      await selectParamset(ServoTuningParamset.P_1);
      const loadButton = wrapper.getByText('Load') as HTMLButtonElement;
      expect(loadButton.disabled).toBe(true);
      fireEvent.change(wrapper.getByTestId('paramset-to-load'), { target: { value: ServoTuningParamset.P_2 } });
      expect(loadButton.disabled).toBe(false);
      fireEvent.click(loadButton);
      await waitUntilPass(() => {
        expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.P_1, ServoTuningParamset.P_2);
      });
    });

    test('shows preview of tuning before load', async () => {
      await selectParamset(ServoTuningParamset.P_1);
      fireEvent.change(wrapper.getByTestId('paramset-to-load'), { target: { value: ServoTuningParamset.P_2 } });
      await waitUntilPass(() => wrapper.getByText('Velocity Feedforward'));
      tuningParams = [{ name: 'ki.hs', value: 1.23 }];
      fireEvent.change(wrapper.getByTestId('paramset-to-load'), { target: { value: ServoTuningParamset.P_3 } });
      await waitUntilPass(() => wrapper.getByText('Integral Gain'));
    });
  });

  describe('Accel/Settling Settings', () => {
    const selectParamset = async (paramset: ServoTuningParamset) => {
      const paramsetName = paramsetNames[paramset];
      fireEvent.click(wrapper.getByText(paramsetName));
      fireEvent.click(wrapper.getByText('Simple'));
      await waitUntilPass(() => wrapper.getByText(/Input the load mass being carried by this device, and adjust the slider/));
    };

    jest.spyOn(ServoTuner.prototype, 'getSimpleTuningParamDefinitions').mockImplementation(async () => [{
      name: 'Stiffness',
      minLabel: 'Smooth',
      maxLabel: 'Stiff',
      dataType: '',
      defaultValue: 0.1,
    },
    {
      name: 'Overshoot Compensation',
      minLabel: 'Off',
      maxLabel: 'Max',
      dataType: '',
    }]);
    jest.spyOn(ServoTuner.prototype, 'isUsingSimpleTuning').mockImplementation(async () => false);

    beforeEach(async () => {
      await initServoTunableDevice();
    });

    test('load settings', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      const mmToNm = 1000;
      const secondsToMs = 1000;
      expect(getValueInputByTitle(wrapper, 'accel').value).toBe(`${defaultAccel}`);
      expect(getValueInputByTitle(wrapper, 'cloop.settle.tolerance').value).toBe(`${defaultSettleTolerance * mmToNm}`);
      expect(getValueInputByTitle(wrapper, 'cloop.settle.period').value).toBe(`${defaultSettlePeriod * secondsToMs}`);
    });

    const setSetting = async (setting: string, value: number, unit: Units) => {
      const label = settingsMetadata[setting].humanName;
      editValueAndUnitsByLabel(wrapper, label!, value, unit);
      await waitUntilPass(() => expect(axes[0].settings.set).toHaveBeenCalledWith(setting, value, unit));
    };

    test('change settings', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      await setSetting('cloop.settle.tolerance', 1, Length.MICROMETRES);
      await setSetting('cloop.settle.period', 0.1, Time.SECONDS);
      await setSetting('accel', 25, Acceleration.METRES_PER_SECOND_SQUARED);
      expect(axes[0].settings.set).toHaveBeenCalledTimes(3);
    });

    test('Accel recommendations', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      expect(getValueInputByTitle(wrapper, 'accel').value).toBe(`${defaultAccel}`);
      setInputOfLabel('Load Mass', 1);
      const recommendedAccel = wrapper.getByTestId('recommended-accel');
      expect(recommendedAccel.innerHTML).toBe('1.611e+4 mm/s²');
      setUnitSelectByTitle(wrapper, 'accel', Acceleration.METRES_PER_SECOND_SQUARED);
      expect(recommendedAccel.innerHTML).toBe('16.11 m/s²');
      fireEvent.click(wrapper.getByTestId('recommended-accel'));
      await waitUntilPass(() => expect(axes[0].settings.set).toHaveBeenCalledWith('accel', 16.11, Acceleration.METRES_PER_SECOND_SQUARED));
      expect(getValueInputByTitle(wrapper, 'accel').value).toBe('16.11');
    });

    test('Show errors if set fails', async () => {
      await selectParamset(ServoTuningParamset.LIVE);
      editValueAndUnitsByLabel(wrapper, 'Acceleration & Deceleration', -5, Acceleration.METRES_PER_SECOND_SQUARED);
      await waitUntilPass(() => wrapper.getByText(/The command's data was incorrect or out of range/));
    });
  });

  describe('Context Menu', () => {
    const openContextMenu = (paramset: ServoTuningParamset) => {
      const paramsetName = paramsetNames[paramset];
      fireEvent.click(wrapper.getByTitle(`More Actions for ${paramsetName}`));
    };

    jest.spyOn(ServoTuner.prototype, 'setStartupParamset').mockImplementation(() => Promise.resolve());

    beforeEach(initServoTunableDevice);

    test('Set Power on Preset', async () => {
      openContextMenu(ServoTuningParamset.P_1);
      wrapper.getByText('Is Power-on Preset');
      openContextMenu(ServoTuningParamset.P_2);
      fireEvent.click(wrapper.getByText('Set Power-on Preset'));
      await waitUntilPass(() => {
        openContextMenu(ServoTuningParamset.P_1);
        wrapper.getByText('Set Power-on Preset');
      });
      openContextMenu(ServoTuningParamset.P_2);
      wrapper.getByText('Is Power-on Preset');
    });

    test('copy to live', async () => {
      openContextMenu(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('Copy Tuning to Live'));
      await waitUntilPass(() => {
        expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.LIVE, ServoTuningParamset.P_1);
      });
    });

    test('Reset To Default', async () => {
      openContextMenu(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('Reset To Default'));
      await waitUntilPass(() => {
        expect(loadSpy).toHaveBeenCalledWith(ServoTuningParamset.P_1, ServoTuningParamset.DEFAULT);
      });
    });
  });

  describe('Import/Export', () => {
    const openImportExport = async (paramset: ServoTuningParamset) => {
      const paramsetName = paramsetNames[paramset];
      fireEvent.click(wrapper.getByText(paramsetName));
      fireEvent.click(wrapper.getByText('Import/Export'));
      await waitUntilPass(() => wrapper.getByText('Save and restore tuning parameters from spreadsheet files.'));
    };

    jest.spyOn(ServoTuner.prototype, 'setStartupParamset').mockImplementation(() => Promise.resolve());

    beforeEach(initServoTunableDevice);

    test('save a row', async () => {
      await openImportExport(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('New Row From Preset 1'));
      await waitUntilPass(() => checkInput('row-name-0', 'Preset 1'));
    });

    test('open from file', async () => {
      await openImportExport(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('Open From File'));
      await waitUntilPass(() => {
        checkInput('row-name-0', 'First Tuning');
        checkInput('row-name-1', 'Second Tuning');
      });

      fireEvent.click(wrapper.getByTestId('row-raw-1'));
      const fclaRow = within(wrapper.getByTestId('param-row-fcla'));
      expect(fclaRow.getByText('Smoothing Decay Rate'));
      expect(fclaRow.getByText('3'));
      expect(fclaRow.getByText('2.9'));
    });

    test('save to file', async () => {
      await openImportExport(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('New Row From Preset 1'));
      await waitUntilPass(() => {
        checkInput('row-name-0', 'Preset 1');
        setInput('row-name-0', 'A Good Tuning');
      });

      await openImportExport(ServoTuningParamset.P_2);
      fireEvent.click(wrapper.getByText('New Row From Preset 2'));
      await waitUntilPass(() => {
        checkInput('row-name-1', 'Preset 2');
        setInput('row-name-1', 'A Bad Tuning');
      });

      fireEvent.click(wrapper.getByText('Save To File'));
      expect(CSV.stringify).toHaveBeenCalledWith(
        [
          { 'Name': 'A Good Tuning', ...ffpid4Csv, 'fcla': '2', 'fcla.hs': '1.9', 'finv1': '1', 'gloop': '3' },
          { 'Name': 'A Bad Tuning', ...ffpid4Csv, 'fcla': '2', 'fcla.hs': '1.9', 'finv1': '1', 'gloop': '3' },
        ],
        { header: true }
      );
    });

    test('delete row', async () => {
      await openImportExport(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('New Row From Preset 1'));
      await waitUntilPass(() => checkInput('row-name-0', 'Preset 1'));

      fireEvent.click(wrapper.getByTestId('delete-row-0'));
      expect(wrapper.queryByTestId('row-name-0')).toBeNull();
    });

    test('delete table', async () => {
      await openImportExport(ServoTuningParamset.P_1);
      fireEvent.click(wrapper.getByText('New Row From Preset 1'));
      await waitUntilPass(() => wrapper.getByTestId('row-name-0'));
      fireEvent.click(wrapper.getByText('New Row From Preset 1'));
      await waitUntilPass(() => wrapper.getByTestId('row-name-1'));

      fireEvent.click(wrapper.getByTestId('delete-table'));
      expect(wrapper.queryByTestId('row-name-0')).toBeNull();
      expect(wrapper.queryByTestId('row-name-1')).toBeNull();
    });
  });
});
