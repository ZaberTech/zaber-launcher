import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { ButtonRowConfirmCancel, Icons, Modal, Text } from '@zaber/react-library';

import type { EntityKey } from '../keys';
import type { RootState } from '../store';

import { actions as actionsDefinition } from './actions';
import { selectConnectionsBeingRemoved } from './selectors';


interface ConfirmationModalProps {
  confirmRemove: () => void;
  cancelRemove: () => void;
}

const ConfirmRemovalModal: React.FC<ConfirmationModalProps> = ({ confirmRemove, cancelRemove }) => (
  <Modal
    headerIcon={<Icons.Connection/>}
    headerText="Confirm Removing Connection"
    buttons={<ButtonRowConfirmCancel confirmText="Remove" onConfirm={confirmRemove} onCancel={cancelRemove}/>}
    small
    bodyIcon="warning"
  >
    <Text>Are you sure you want to remove the connection?</Text>
  </Modal>
);

interface Props {
  connectionKey: EntityKey;
}
interface StateProps {
  removing: boolean;
  error: string | null;
}
interface DispatchProps {
  actions: typeof actionsDefinition;
}

interface State {
  confirmingRemoval: boolean;
}

class RemoveConnectionBase extends Component<Props & StateProps & DispatchProps, State> {
  constructor(props: Props & StateProps & DispatchProps) {
    super(props);
    this.state = {
      confirmingRemoval: false
    };
  }

  render(): React.ReactNode {
    const { actions, connectionKey, removing = false } = this.props;
    const { confirmingRemoval } = this.state;
    return <>
      { confirmingRemoval && <ConfirmRemovalModal
        confirmRemove={() => actions.removeConnection(connectionKey)}
        cancelRemove={() => this.setState({ confirmingRemoval: false })}
      />}
      <Icons.Trash title="Remove Connection" disabled={removing} onClick={() => this.setState({ confirmingRemoval: true })}/>
    </>;
  }
}

export const RemoveConnection = connect<StateProps, DispatchProps, Props, RootState>(
  (state: RootState, props: Props): StateProps => ({
    ...selectConnectionsBeingRemoved(state)[props.connectionKey],
  }),
  (dispatch: Dispatch): DispatchProps => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(RemoveConnectionBase);
