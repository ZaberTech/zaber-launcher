import { Text, TextType, NoticeBanner, ButtonRowConfirmCancel } from '@zaber/react-library';
import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';

import type { NonNullPeripheralTraits, PeripheralTraits } from '../reducer';
import { actions as actionsDefinition } from '../actions';
import { selectHardwareModification } from '../../store';
import type { HwModDeviceOrAxisState } from '../selectors';
import { NoContentMessage } from '../../components';
import { supportLink } from '../components/links';
import { TITLE } from '../types';

import { PeripheralTraitField } from './PeripheralTraitField';
import { getErrorMessageForTraitMapping, matchTraitsToId } from './peripheralIdMapping';
import motionDocs from './custom_manuals/motion.html';
import motorDocs from './custom_manuals/motor.html';
import encoder1Docs from './custom_manuals/encoder1.html';
import encoder2Docs from './custom_manuals/encoder2.html';

interface Params {
  info: HwModDeviceOrAxisState;
  traits: PeripheralTraits;
}

function isNonNullTraits(traits: PeripheralTraits): traits is NonNullPeripheralTraits {
  if (traits.motion == null || traits.motor == null || traits.singleEncoder == null || traits.diffEncoder == null) {
    return false;
  }

  return true;
}

export const ThirdPartyPeripheralIdPicker: React.FC<Params> = ({ info, traits }) => {
  const { setPeripheralIdError: error } = useSelector(selectHardwareModification);
  const actions = useActions(actionsDefinition);
  const setPeripheralId = () => {
    if (isNonNullTraits(traits)) {
      const peripheralId = matchTraitsToId(traits);
      if (peripheralId != null) {
        actions.setPeripheralId(info, peripheralId);
      } else {
        actions.errorSettingPeripheralId(getErrorMessageForTraitMapping(traits));
      }
    }
  };

  const peripheralid = info.axis?.identity?.peripheralId;
  if (peripheralid == null || peripheralid !== 0) {
    return <NoContentMessage title={TITLE} message="Resetting Peripheral..." type="working"/>;
  }

  return <div className="peripheral-id-picker">
    <div className="selections">
      <p><Text t={TextType.H4}>Peripheral Specs</Text></p>
      <p><Text e={Text.Emphasis.Light}>Please fill in the specs for your peripheral.</Text></p>
      <PeripheralTraitField<'motion'> trait="motion" traits={traits}
        title="Motion Type" description="The way the peripheral moves when powered"
        options={[{ value: 'linear', label: 'Linear' }, { value: 'rotary', label: 'Angular/Rotary' }]}
        docs={motionDocs}
      />
      <PeripheralTraitField<'motor'> trait="motor" traits={traits}
        title="Motor Type" description=" The type of motor that powers the peripheral"
        options={[
          { value: 'linear', label: 'Linear Motor', disabled: true },
          { value: 'stepper', label: 'Stepper' },
          { value: 'voice', label: 'Voice Coil', disabled: true },
        ]}
        docs={motorDocs}
      />
      <PeripheralTraitField<'singleEncoder'> trait="singleEncoder" traits={traits}
        title="Single-Ended Encoder Type" description="The type of single-ended encoder that the peripheral uses, if any."
        options={[
          { value: 'none', label: 'None' },
          { value: 'direct', label: 'Direct' },
          { value: 'motor', label: 'Motor' },
          { value: 'absolute', label: 'Absolute Phase Motor', disabled: true },
        ]}
        docs={encoder1Docs}
      />
      <PeripheralTraitField<'diffEncoder'> trait="diffEncoder" traits={traits}
        title="Differential Encoder Type" description="The type of differential encoder that the peripheral uses, if any."
        options={[
          { value: 'none', label: 'None' },
          { value: 'direct', label: 'Direct' },
          { value: 'analog', label: 'Analog Direct' },
          { value: 'motor', label: 'Motor' },
        ]}
        docs={encoder2Docs}
      />

      {error && <NoticeBanner headline="Hardware is not compatible">
        <Text>{error}</Text>
        <Text>If you need help configuring this product, please contact {supportLink}.</Text>
      </NoticeBanner>}
    </div>

    <ButtonRowConfirmCancel
      className="buttons"
      confirmText="Next"
      onConfirm={(!isNonNullTraits(traits) || error != null) ? 'disabled' : setPeripheralId}
      onCancel={() => actions.choosePeripheralTrait(null)}
    />
  </div>;
};
