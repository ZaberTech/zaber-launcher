import { all, takeLatest, put, call, select } from 'redux-saga/effects';
import type { SagaIterator } from 'redux-saga';
import { ensureError } from '@zaber/toolbox';

import { ZaberApi } from '../app_components';
import { getContainer } from '../container';
import { environment } from '../environment';

import { ActionTypes, actions } from './actions';
import { selectEmail, selectText } from './selectors';


export function* feedbackSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.SEND_FEEDBACK, sendFeedback),
  ]);
}

function* sendFeedback(): SagaIterator {
  const email: ReturnType<typeof selectEmail> = yield select(selectEmail);
  const text: ReturnType<typeof selectText> = yield select(selectText);
  try {
    const api = getContainer().get<ZaberApi>(ZaberApi);

    yield call([api, api.sendFeedback], {
      product: environment.appName,
      version: environment.releaseName,
      comment: text,
      meta: {
        email,
        edition: environment.edition,
        flavor: environment.flavor,
        os: environment.platform,
      },
    });

    yield put(actions.sendFeedbackDone());
  } catch (err) {
    yield put(actions.sendFeedbackErr(ensureError(err).message));
  }
}
