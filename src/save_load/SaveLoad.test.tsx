let openFileName: string;
let cancelFileDialog: boolean;

jest.mock('../dialogs', () => ({
  Dialogs: {
    showSaveDialog: jest.fn((opts: {defaultPath: string}) => ({
      canceled: cancelFileDialog,
      filePath: opts.defaultPath,
    })),
    showOpenDialog: jest.fn(() => ({
      canceled: cancelFileDialog,
      filePaths: [openFileName],
    }))
  }
}));

import fs from 'fs';

let rejectWriteWith: string;
jest.spyOn(fs.promises, 'writeFile').mockImplementation(() => {
  if (rejectWriteWith === '') {
    return Promise.resolve();
  } else {
    return Promise.reject(new Error(rejectWriteWith));
  }
});

let fileContents = {};
let rejectReadWith: string;
jest.spyOn(fs.promises, 'readFile').mockImplementation(() => {
  if (rejectReadWith === '') {
    return Promise.resolve(JSON.stringify(fileContents));
  } else {
    return Promise.reject(new Error(rejectReadWith));
  }
});

import React from 'react';
import {
  SetDeviceStateFailedException,
  SetPeripheralStateFailedException,
} from '@zaber/motion';
import { Container, injectable } from 'inversify';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import _ from 'lodash';
import { ContextMenu } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter, mockStorage, StorageMock, waitTick } from '../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase
} from '../test/mocks/ascii';
import { DeviceInfoWithAxes, selectAxes, selectDevices } from '../connection_manager';
import { MessageRoutersService } from '../message_router';
import { mockNonIdentifiedDevice, mockSingleDevice, mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import { mockReloadDevices } from '../test/mocks/reload_devices';
import { getAxisNumber } from '../keys';
import { ZaberApi } from '../app_components';
import { environment } from '../environment';

import { SaveLoadContextMenuItem } from './SaveLoadContextMenuItem';
import { SaveLoadModal } from './SaveLoadModal';

let wrapper: RenderResult;
let container: Container;
let storageMock: StorageMock;

@injectable()
class ZaberApiMock {
  // eslint-disable-next-line camelcase
  getSupportedPeripherals = jest.fn(() => ({ peripherals: [{ peripheral_id: 88801 }] }));
}


let axes: AxisMock[];
class AxisMock extends AxisMockBase {
  constructor(axisNumber: number, device: DeviceMockBase<AxisMockBase>) {
    super(axisNumber, device);
    axes.push(this);
  }

  identity = { firmwareVersion: { major: 7, minor: 20, build: 1234 } };

  getState = jest.fn(() => 'device_state');
  canSetState = jest.fn(() => '');
  setState = jest.fn(async () => ({ warnings: [] }));
  settings = {
    set: jest.fn(),
  };
  restore = jest.fn(async () => undefined);
}

let devices: DeviceMock[];

let canSetError: string | null = null;
let canSetPeripheralErrors: (string | null)[] = [];
let setException: SetDeviceStateFailedException | SetPeripheralStateFailedException | null = null;
class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(id: number, connection: ConnectionMock) {
    super(id, connection);
    devices.push(this);
  }

  identity = { firmwareVersion: { major: 7, minor: 20, build: 1234 } };
  axisCount: number = 0;
  getState = jest.fn(() => 'device_state');
  canSetState = jest.fn(() => ({
    error: canSetError,
    axisErrors: canSetPeripheralErrors.map((e, i) => ({
      axisNumber: i + 1,
      error: e,
    }))
  }));
  setState = jest.fn(() => {
    if (setException != null) {
      throw setException;
    }
    return { warnings: [] };
  });

  identify = jest.fn(async () => this.identity);
  restore = jest.fn(async () => undefined);
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

let device: DeviceInfoWithAxes;

const SaveLoadTestComponent: React.FC = () => {
  const theDevice = _.sample(useSelector(selectDevices))!;
  const theAxes = useSelector(selectAxes);
  device = {
    ...theDevice,
    axes: _.toArray(theAxes),
  };
  return <>
    <ContextMenu data-testid="device-context-menu">
      <SaveLoadContextMenuItem productKey={device.key}/>
    </ContextMenu>
    <ContextMenu data-testid="axis-context-menu">
      {Object.keys(theAxes).map(key => (
        <SaveLoadContextMenuItem key={key} productKey={key} title={`Save or Load State of Axis ${getAxisNumber(key)}`}/>
      ))}
    </ContextMenu>
    <SaveLoadModal/>
  </>;
};

const TestSaveLoad = wrapWithNewStore(wrapWithRouter(SaveLoadTestComponent));

mockReloadDevices(() => TestSaveLoad.testStore);

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(ZaberApi).to(ZaberApiMock);
  storageMock = mockStorage(container);

  axes = [];
  devices = [];

  wrapper = render(<TestSaveLoad/>);

  openFileName = '';
  rejectWriteWith = '';
  rejectReadWith = '';
  fileContents = {};
  canSetError = null;
  canSetPeripheralErrors = [];
  setException = null;
  cancelFileDialog = false;
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  axes = [];
  devices = [];
  device = null!;

  destroyContainer();
  container = null!;
  storageMock.stored = {};
});

describe('The context menu item', () => {
  test('The context menu item appears', async () => {
    const store = TestSaveLoad.testStore;
    mockSingleDevice(store);

    await waitUntilPass(() => wrapper.getByTestId('device-context-menu'));
    fireEvent.click(wrapper.getByTestId('device-context-menu').firstChild!);
    wrapper.getByText('Save/Load State');
  });

  test('Axis context menu', async () => {
    const store = TestSaveLoad.testStore;
    mockSingleDeviceWithPeripherals(store);

    await waitUntilPass(() => wrapper.getByTestId('axis-context-menu'));
    fireEvent.click(wrapper.getByTestId('axis-context-menu').firstChild!);
    expect(wrapper.getAllByText('Save/Load State').length).toBe(4);
    wrapper.getByTitle('Save or Load State of Axis 1');
  });

  test('Cannot save state of unidentified device', async () => {
    const store = TestSaveLoad.testStore;
    mockNonIdentifiedDevice(store);

    await waitUntilPass(() => wrapper.getByTestId('device-context-menu'));
    fireEvent.click(wrapper.getByTestId('device-context-menu').firstChild!);
    fireEvent.click(wrapper.getByText('Save/Load State'));

    await waitUntilPass(() => wrapper.getByText('Cannot load state to an unidentified device.'));
  });
});

describe('Save/Load Device', () => {
  beforeEach(() => {
    const store = TestSaveLoad.testStore;
    mockSingleDevice(store);

    fireEvent.click(wrapper.getByTestId('device-context-menu').firstChild!);
    fireEvent.click(wrapper.getByText('Save/Load State'));
    fileContents = {
      context: {
        type: 'device',
        fw: { major: 7, minor: 20 },
      },
    };
    wrapper.getByText(
      'Would you like to create a new save file from this product\'s state, or load a previously created save file to this device?'
    );
  });

  test('Device modal', () => {
    wrapper.getByText('Device Settings');
    wrapper.getByText('Stream Buffers');
    wrapper.getByText('Triggers');
    wrapper.getByText('Servo Tuning Parameters');
    expect(wrapper.queryByText('The full state of all connected peripherals')).toBeNull();
  });

  test('Save device state', async () => {
    fireEvent.click(wrapper.getByText('Create Save File'));
    await waitUntilPass(() => {
      wrapper.getByText(
        'The custom state of X-LHM has been successfully saved to a file and can be reloaded to the device at any time.'
      );
    });

    fireEvent.click(wrapper.getByText('Done'));
    expect(wrapper.queryByText('Save/Load State')).toBeNull();
  });

  test('User cancels save in file selection', async () => {
    cancelFileDialog = true;
    fireEvent.click(wrapper.getByText('Create Save File'));
    await waitTick();
    wrapper.getByText(
      'Would you like to create a new save file from this product\'s state, or load a previously created save file to this device?'
    );
  });

  test('Error saving device state', async () => {
    rejectWriteWith = 'Could not write for some reason';
    fireEvent.click(wrapper.getByText('Create Save File'));
    await waitUntilPass(() => wrapper.getByText('Could not write for some reason'));
  });

  test('Load device save state', async () => {
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => wrapper.getByText(/is compatible with the chosen device/));
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => wrapper.getByText(/file have been successfully applied to this/));
  });

  test('Parse file names', async () => {
    openFileName = environment.platform === 'win32' ? 'C:\\windows\\style\\path\\save-file.json' : '/user/dir/save-file.json';
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => wrapper.getByText('save-file.json'));
  });

  test('Read error', async () => {
    rejectReadWith = 'Cannot read for whatever reason';
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => wrapper.getByText('Cannot read for whatever reason'));
  });

  test('Set error', async () => {
    setException = new SetDeviceStateFailedException('Cannot set for whatever reason', {
      settings: ['ctl.go'],
      streamBuffers: ['stream 1 bad command'],
      pvtBuffers: ['pvt 1 bad command'],
      triggers: ['trigger 45676 bad command'],
      servoTuning: 'badly tuned servo',
      storage: ['storage failed'],
      storedPositions: ['invalid position'],
      peripherals: [],
    });
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => fireEvent.click(wrapper.getByText('Apply')));
    await waitUntilPass(() => {
      wrapper.getByText('Cannot set for whatever reason');
      wrapper.getByText('ctl.go');
      wrapper.getByText('stream 1 bad command');
      wrapper.getByText('trigger 45676 bad command');
      wrapper.getByText('Error setting servo tuning: badly tuned servo');
      wrapper.getByText('storage failed');
      wrapper.getByText('invalid position');
    });
  });

  const SIMPLE_EXCEPTION = new SetDeviceStateFailedException('Cannot set', {
    settings: ['ctl.go'],
    streamBuffers: [],
    pvtBuffers: [],
    triggers: [],
    servoTuning: '',
    storage: [],
    storedPositions: [],
    peripherals: [],
  });

  test('allows to restore if load fails', async () => {
    setException = SIMPLE_EXCEPTION;
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => fireEvent.click(wrapper.getByText('Apply')));
    await waitUntilPass(() => wrapper.getByText(/Could not set/));

    fireEvent.click(wrapper.getByText('Factory Reset'));
    await waitUntilPass(() => wrapper.getByText(/the device has been factory reset/));
    expect(devices[0].restore).toHaveBeenCalled();
  });

  test('displays error if restore fails', async () => {
    setException = SIMPLE_EXCEPTION;
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => fireEvent.click(wrapper.getByText('Apply')));
    await waitUntilPass(() => wrapper.getByText(/Could not set/));

    devices[0].restore.mockRejectedValueOnce(new Error('Cannot restore'));
    fireEvent.click(wrapper.getByText('Factory Reset'));
    await waitUntilPass(() => wrapper.getByText(/Cannot restore/));
  });
});

describe('Save/Load Controller with Peripherals', () => {
  beforeEach(() => {
    const store = TestSaveLoad.testStore;
    mockSingleDeviceWithPeripherals(store);

    fileContents = {
      context: {
        type: 'controller',
        fw: { major: 7, minor: 20 },
      },
    };
    fireEvent.click(wrapper.getByTestId('device-context-menu').firstChild!);
    fireEvent.click(wrapper.getByText('Save/Load State'));
    wrapper.getByText(
      'Would you like to create a new save file from this product\'s state, ' +
      'or load a previously created save file to this controller and peripherals?'
    );
  });

  test('Controller modal', () => {
    wrapper.getByText('Device Settings');
    wrapper.getByText('Stream Buffers');
    wrapper.getByText('Triggers');
    wrapper.getByText('Servo Tuning Parameters');
    wrapper.getByText('The full state of all connected peripherals');
  });

  test('Load partial save state', async () => {
    canSetPeripheralErrors = [null, null, null, null];
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => {
      wrapper.getByText(/is compatible with the chosen controller and peripherals. Please choose the load option below./);
    });
    const compatibleControllerOnlyButton = wrapper.getByTestId('controller-only') as HTMLInputElement;
    const compatibleLoadAllButton = wrapper.getByTestId('load-all') as HTMLInputElement;
    const compatibleApplyButton = wrapper.getByText('Apply') as HTMLInputElement;
    expect(compatibleControllerOnlyButton.disabled).toBe(false);
    expect(compatibleLoadAllButton.disabled).toBe(false);
    expect(compatibleApplyButton.disabled).toBe(true);
    fireEvent.click(compatibleLoadAllButton);
    expect(compatibleApplyButton.disabled).toBe(false);

    canSetError = 'Cannot apply anything to this controller';
    fireEvent.click(wrapper.getByText('Back'));
    await waitUntilPass(() => {
      wrapper.getByText(/is incompatible with the chosen controller and peripherals. Please choose another file./);
      wrapper.getByText(/Cannot apply anything to this controller/);
    });
    const incompatibleControllerOnlyButton = wrapper.getByTestId('controller-only') as HTMLInputElement;
    const incompatibleLoadAllButton = wrapper.getByTestId('load-all') as HTMLInputElement;
    expect(incompatibleControllerOnlyButton.disabled).toBe(true);
    expect(incompatibleLoadAllButton.disabled).toBe(true);

    canSetError = null;
    canSetPeripheralErrors = ['Cannot set a peripheral', null, null, null];
    fireEvent.click(wrapper.getByText('Back'));
    await waitUntilPass(() => {
      wrapper.getByText(/is partially compatible with the chosen controller and peripherals. Please choose the load option below./);
      wrapper.getByText(/Cannot set a peripheral/);
    });
    const partCompatibleControllerOnlyButton = wrapper.getByTestId('controller-only') as HTMLInputElement;
    const partCompatibleLoadAllButton = wrapper.getByTestId('load-all') as HTMLInputElement;
    expect(partCompatibleControllerOnlyButton.disabled).toBe(false);
    expect(partCompatibleLoadAllButton.disabled).toBe(true);

    fireEvent.click(partCompatibleControllerOnlyButton);
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => wrapper.getByText(/file have been successfully applied to this/));
    expect(devices[0].setState).toHaveBeenCalledWith(JSON.stringify(fileContents), { deviceOnly: true });
  });

  test('Set error', async () => {
    setException = new SetDeviceStateFailedException('Cannot set for whatever reason', {
      settings: ['ctl.go'],
      streamBuffers: ['stream 1 bad command'],
      pvtBuffers: ['pvt 1 bad command'],
      triggers: ['trigger 45676 bad command'],
      servoTuning: '',
      storedPositions: [],
      storage: [],
      peripherals: [{
        axisNumber: 1,
        settings: ['motor.go', 'motor.stop'],
        servoTuning: 'badly tuned servo',
        storage: ['storage failed'],
        storedPositions: ['invalid position'],
      }],
    });
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => fireEvent.click(wrapper.getByText('Apply')));
    await waitUntilPass(() => {
      wrapper.getByText('Cannot set for whatever reason');
      wrapper.getByText('ctl.go');
      wrapper.getByText('stream 1 bad command');
      wrapper.getByText('pvt 1 bad command');
      wrapper.getByText('trigger 45676 bad command');
      wrapper.getByText('Axis 1');
      wrapper.getByText('motor.go');
      wrapper.getByText('motor.stop');
      wrapper.getByText('Error setting servo tuning: badly tuned servo');
      wrapper.getByText('storage failed');
      wrapper.getByText('invalid position');
    });
  });
});

describe('Save/Load Axis', () => {
  beforeEach(() => {
    const store = TestSaveLoad.testStore;
    mockSingleDeviceWithPeripherals(store);

    fileContents = {
      context: { type: 'peripheral' },
      axis: { peripheralId: '88801' }
    };
    fireEvent.click(wrapper.getByTestId('axis-context-menu').firstChild!);
    fireEvent.click(wrapper.getByTitle('Save or Load State of Axis 1'));
    wrapper.getByText(
      'Would you like to create a new save file from this product\'s state, or load a previously created save file to this axis?'
    );
  });

  test('Axis modal', () => {
    wrapper.getByText('Axis Settings');
    wrapper.getByText('Servo Tuning Parameters');
    expect(wrapper.queryByText('Stream Buffers')).toBeNull();
    expect(wrapper.queryByText('The full state of all connected peripherals')).toBeNull();
  });

  test('Save axis state', async () => {
    fireEvent.click(wrapper.getByText('Create Save File'));
    await waitUntilPass(() => wrapper.getByText(/has been successfully saved to a file and can be reloaded to the axis at any time./));
  });

  test('Load axis state', async () => {
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => wrapper.getByText(/is compatible with the chosen axis/));
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => wrapper.getByText(/file have been successfully applied to this/));
  });

  test('allows to restore axis if load fails', async () => {
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => wrapper.getByText(/is compatible with the chosen axis/));

    const axis = devices[0].getAxis(1);
    axis.setState.mockRejectedValueOnce(
      new SetPeripheralStateFailedException('Cannot set', {
        axisNumber: 1,
        settings: ['maxspeed'],
        servoTuning: '',
        storedPositions: [],
        storage: [],
      }));
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => wrapper.getByText(/Could not set/));

    fireEvent.click(wrapper.getByText('Factory Reset'));
    await waitUntilPass(() => wrapper.getByText(/the axis has been factory reset/));
    expect(axis.restore).toHaveBeenCalled();
  });
});

describe('Load to unused peripheral', () => {
  beforeEach(() => {
    const store = TestSaveLoad.testStore;
    mockSingleDeviceWithPeripherals(store, { peripheralCount: 1, type: 'unused' });

    fileContents = {
      context: { type: 'peripheral' },
      axis: { peripheralId: '88801' }
    };

    fireEvent.click(wrapper.getByTestId('axis-context-menu').firstChild!);
    fireEvent.click(wrapper.getByTitle('Save or Load State of Axis 1'));
    wrapper.getByText(
      'Would you like to create a new save file from this product\'s state, or load a previously created save file to this axis?'
    );
  });

  test('Load state to unused axis', async () => {
    fireEvent.click(wrapper.getByText('Load Save File'));
    await waitUntilPass(() => wrapper.getByText(/is compatible with the chosen axis/));
    fireEvent.click(wrapper.getByText('Apply'));
    await waitUntilPass(() => wrapper.getByText(/file have been successfully applied to this/));
    expect(axes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 88801);
  });
});
