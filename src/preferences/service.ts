import { injectable, inject } from 'inversify';
import _ from 'lodash';

import { Storage } from '../app_components';

import {
  PreferenceModifiedCallback,
  PreferenceValue,
  DEFAULT_VALUES,
  PreferenceKeys,
  PreferenceTypes,
} from './types';

export const PREFERENCES_STORAGE_KEYS: _.Dictionary<string> = {
  [PreferenceKeys.ZML_LOGGING]: 'PREFERENCE_LOG_ZML_COMMANDS',
  [PreferenceKeys.TIMEOUT_LOCAL]: 'PREFERENCE_TIMEOUT_LOCAL',
  [PreferenceKeys.TIMEOUT_REMOTE]: 'PREFERENCE_TIMEOUT_REMOTE',
};

@injectable()
export class PreferencesService {
  private handlers: _.Dictionary<PreferenceModifiedCallback[]> = {};

  constructor(@inject(Storage) private readonly storage: Storage) { }

  public retrieveInitialValues(): _.Dictionary<PreferenceValue> {
    const preferences: _.Dictionary<PreferenceValue> = {};

    _.forEach(DEFAULT_VALUES, (value, key) => {
      const stored: PreferenceValue | null = this.storage.load(PREFERENCES_STORAGE_KEYS[key]);
      preferences[key] = stored ?? value;
    });
    return preferences;
  }

  public registerHandler<TKey extends keyof PreferenceTypes>(key: TKey, handler: PreferenceModifiedCallback<PreferenceTypes[TKey]>): void {
    if (!this.handlers[key]) {
      this.handlers[key] = [];
    }
    this.handlers[key].push(handler as PreferenceModifiedCallback);
  }

  public async setPreferenceValue(key: string, value: PreferenceValue, store: boolean = true): Promise<void> {
    if (store) {
      this.storage.save(PREFERENCES_STORAGE_KEYS[key], value);
    }

    for (const handler of this.handlers[key] ?? []) {
      await handler(value);
    }
  }
}
