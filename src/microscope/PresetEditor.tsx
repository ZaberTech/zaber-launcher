import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
  Button, Checkbox, ContextMenu, Icons, Input, Modal, NoticeBanner, NumericInput, Text, SimpleSelect, ButtonRowConfirmCancel
} from '@zaber/react-library';
import _ from 'lodash';
import type { DeepPartial } from 'redux';

import { useActions } from '../utils';
import { OneTimeMessage } from '../help';
import { InputWithUnits  } from '../units';
import { AppIconsNeutral } from '../apps';

import { selectMonitorState, selectPresetDataFromState, selectPreset, selectPresetEditor } from './selectors';
import { actions as actionsDefinition } from './actions';
import { overrideValues, type PresetData } from './presets';
import {
  AXES, AXES_LABELS, Axes, ILLUMINATOR_CHANNELS, OBJECTIVE_PARAMETER_LABELS, POSITION_DIMENSION, TIME_DIMENSION,
  TRIGGER_SETTINGS_FIELDS, TRIGGER_SETTINGS_LABELS,
} from './types';
import { sanitizeMeasurement } from './utils';

const Section: React.FC<{ children: React.ReactNode }> = ({ children }) => <>
  <span/>
  <div className="section">{children}</div>
</>;

const Label: React.FC<{ children: React.ReactNode }> = ({ children }) => <Text>{children}</Text>;

interface UpdateProps {
  preset: PresetData;
  update: (data: DeepPartial<PresetData>) => void;
}

const Objective: React.FC<UpdateProps> = ({ preset, update }) => {
  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.objectiveChanger;
  if (stableInfo == null) {
    return null;
  }

  const options = _.range(1, stableInfo.numberOf + 1).map(filter =>
    ({ label: stableInfo.labels[filter - 1] || `Objective ${filter}`, value: filter }));

  return <>
    <Section>Objective Changer</Section>
    <Checkbox checked={preset.objective.apply} onChecked={apply => update({ objective: { apply } })}/>
    <Label>Objective</Label>
    <SimpleSelect
      title="Objective"
      value={preset.objective.value}
      onValueChange={value => update({ objective: { value, apply: true } })}
      options={options} portalToBody/>
  </>;
};

const Filter: React.FC<UpdateProps> = ({ preset, update }) => {
  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.filterChanger;
  if (stableInfo == null) {
    return null;
  }

  const options = _.range(1, stableInfo.numberOf + 1).map(filter =>
    ({ label: stableInfo.labels[filter - 1] || `Filter ${filter}`, value: filter }));

  return <>
    <Section>Filter Changer</Section>
    <Checkbox checked={preset.filter.apply} onChecked={apply => update({ filter: { apply } })}/>
    <Label>Filter</Label>
    <SimpleSelect
      title="Filter"
      value={preset.filter.value}
      onValueChange={value => update({ filter: { value, apply: true } })}
      options={options} portalToBody/>
  </>;
};

const IlluminatorChannel: React.FC<UpdateProps & { channel: number }> = ({ preset, update: updateRoot, channel }) => {
  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.illuminator?.channels[channel];
  if (stableInfo == null) {
    return null;
  }

  const channelPreset = preset.illuminator[channel];
  const update = (data: DeepPartial<PresetData['illuminator'][number]>) =>
    updateRoot({ illuminator: { [channel]: data } });

  return <>
    <Section>Illuminator Channel {channel} {stableInfo.name}</Section>
    <Checkbox
      checked={channelPreset.on.apply}
      onChecked={apply => update({ on: { apply } })}/>
    <Label>Power</Label>
    <SimpleSelect
      title={`Channel ${channel} Power`}
      value={Number(channelPreset.on.value)}
      onValueChange={value => update({ on: { value: value > 0, apply: true } })}
      options={[
        { label: 'Off', value: 0 },
        { label: 'On', value: 1 },
      ]}
      portalToBody/>

    <Checkbox
      checked={channelPreset.intensity.apply}
      onChecked={apply => update({ intensity: { apply } })}/>
    <Label>Intensity</Label>
    <NumericInput
      placeholder={`Channel ${channel} Intensity`}
      displayPrecision={0}
      value={Math.round(channelPreset.intensity.value * 100)}
      onNumberChange={value => update({
        intensity: { value: (value ?? Number.NaN) / 100, apply: true }
      })}
      min={0}
      max={100}/>
  </>;
};

const Axis: React.FC<UpdateProps & { axis: Axes }> = ({ preset, update: updateRoot, axis }) => {
  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.[axis];

  if (stableInfo == null) {
    return null;
  }

  const axisPreset = preset.axes[axis];
  const update = (data: DeepPartial<PresetData['axes']['xAxis']>) =>
    updateRoot({ axes: { [axis]: data } });

  return <>
    <Section>{AXES_LABELS[axis]} Axis</Section>
    <Checkbox
      checked={axisPreset.step.apply}
      onChecked={apply => update({ step: { apply } })}/>
    <Label>Step</Label>
    <InputWithUnits
      className="step"
      title={`${AXES_LABELS[axis]} Step`}
      displayPrecision={3}
      unitFrom={{ dimension: POSITION_DIMENSION }}
      measure={axisPreset.step.value}
      onChange={value => update({ step: { value: sanitizeMeasurement(value), apply: true } })}/>

    <Checkbox
      checked={axisPreset.position.apply}
      onChecked={apply => update({ position: { apply } })}/>
    <Label>Position</Label>
    <InputWithUnits
      className="position"
      title={`${AXES_LABELS[axis]} Position`}
      displayPrecision={3}
      unitFrom={{ dimension: POSITION_DIMENSION }}
      measure={axisPreset.position.value}
      onChange={value => update({ position: { value: sanitizeMeasurement(value), apply: true } })}/>
  </>;
};

const Autofocus: React.FC<UpdateProps> = ({ preset, update: updateRoot }) => {
  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.autofocus;
  if (stableInfo == null) {
    return null;
  }
  const { autofocus } = preset;

  const update = (data: DeepPartial<PresetData['autofocus']>) =>
    updateRoot({ autofocus: data });
  const updateSettings = (data: DeepPartial<PresetData['autofocus']['settings']>) =>
    update({ settings: data });

  return <>
    <Section>Autofocus</Section>
    {(['limitMin', 'limitMax'] as const)
      .map(limit => <React.Fragment key={limit}>
        <Checkbox checked={autofocus[limit].apply} onChecked={apply => update({ [limit]: { apply } })}/>
        <Label>Limit {limit === 'limitMin' ? 'Min' : 'Max'}</Label>
        <InputWithUnits
          className="position"
          title={`Autofocus Limit ${limit === 'limitMin' ? 'Min' : 'Max'}`}
          displayPrecision={3}
          unitFrom={{ dimension: POSITION_DIMENSION }}
          measure={autofocus[limit].value}
          onChange={value => update({ [limit]: { value: sanitizeMeasurement(value), apply: true } })}/>
      </React.Fragment>)}
    {(['magnification', 'control_loop_kp', 'control_loop_ki', 'max_exposure', 'signal_to_noise_ratio'] as const)
      .map(setting => <React.Fragment key={setting}>
        <Checkbox checked={autofocus.settings[setting].apply} onChecked={apply => updateSettings({ [setting]: { apply } })}/>
        <Label>{OBJECTIVE_PARAMETER_LABELS[setting]}</Label>
        <NumericInput
          placeholder={`Autofocus ${OBJECTIVE_PARAMETER_LABELS[setting]}`}
          displayPrecision={3}
          value={autofocus.settings[setting].value}
          onNumberChange={value => updateSettings({ [setting]: { value: value ?? Number.NaN, apply: true } })}/>
      </React.Fragment>)}
  </>;
};

const CameraTrigger: React.FC<UpdateProps> = ({ preset, update: updateRoot }) => {
  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.cameraTrigger;
  if (stableInfo == null) {
    return null;
  }
  const { cameraTrigger } = preset;

  const update = (data: DeepPartial<PresetData['cameraTrigger']>) => updateRoot({ cameraTrigger: data });

  return <>
    <Section>Camera Trigger</Section>
    {TRIGGER_SETTINGS_FIELDS.map(field => <React.Fragment key={field}>
      <Checkbox
        checked={cameraTrigger[field].apply}
        onChecked={apply => update({ [field]: { apply } })}/>
      <Label>{TRIGGER_SETTINGS_LABELS[field]}</Label>
      <InputWithUnits
        title={TRIGGER_SETTINGS_LABELS[field]}
        displayPrecision={1}
        unitFrom={{ dimension: TIME_DIMENSION }}
        measure={cameraTrigger[field].value}
        onChange={value => update({ [field]: { value: sanitizeMeasurement(value), apply: true } })}/>
    </React.Fragment>)}
  </>;
};

const DeletePreset: React.FC<{ index: number }> = ({ index }) => {
  const actions = useActions(actionsDefinition);
  const [confirm, setConfirm] = useState(false);
  return <>
    {!confirm && <Icons.Trash title="Delete Preset" onClick={() => setConfirm(true)}/>}
    {confirm && <>
      <Button onClick={() => {
        actions.removePreset(index);
        setConfirm(false);
      }}>Delete</Button>
      <Button color="grey" onClick={() => setConfirm(false)}>Cancel</Button>
    </>}
  </>;
};

const PresetEditor: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const { presets } = useSelector(selectPresetEditor);
  const presetFromState = useSelector(selectPresetDataFromState);
  const appliedPreset = useSelector(selectPreset).current;

  const [presetIndex, setPreset] = useState(appliedPreset ?? 0);
  const preset = presets[presetIndex];

  const update = (data: DeepPartial<PresetData>) => actions.updatePreset(presetIndex, { data });

  return <div className="preset-editor">
    <div className="select-preset">
      <SimpleSelect
        placeholder={presets.length ? 'Select preset' : 'No presets'}
        value={presetIndex}
        onValueChange={setPreset}
        options={presets.map((preset, index) => ({ label: preset.name, value: index }))}
        portalToBody/>
      <Icons.Plus className="plus" title="Add Preset" onClick={() => {
        actions.addPreset(presetFromState);
        setPreset(presets.length);
      }}/>
      {preset != null && <DeletePreset key={presetIndex} index={presetIndex}/>}
    </div>

    {preset != null && <div className="scroll-container">
      <div className="name">
        <Label>Name</Label>
        <Input
          placeholder="Preset Name"
          value={preset.name}
          onValueChange={name =>  actions.updatePreset(presetIndex, { name })}/>
        <ContextMenu align="end">
          <ContextMenu.Item
            onClick={() => actions.updatePreset(presetIndex, {
              data: overrideValues(preset.data, presetFromState),
            })}>
            Load Current Values
          </ContextMenu.Item>
        </ContextMenu>
      </div>
      <OneTimeMessage messageId="microscope-preset-editor" message={dismiss =>
        <NoticeBanner type="info" closer={dismiss}>
          Click on the checkboxes preceding the values to apply them.
          Only the values with checked checkboxes will be changed when the preset is applied.
        </NoticeBanner>
      }/>

      <div className="table">
        {['Apply', 'Section/Property', 'Value'].map(label =>
          <div key={label} className="header-label">{label}</div>)}
        <div className="divider"/>

        <Objective preset={preset.data} update={update}/>
        <Filter preset={preset.data} update={update}/>
        {ILLUMINATOR_CHANNELS.map(channel =>
          <IlluminatorChannel key={channel} channel={channel}
            preset={preset.data} update={update}/>)}
        {AXES.map(axis => <Axis key={axis} axis={axis} preset={preset.data} update={update}/>)}
        <Autofocus preset={preset.data} update={update}/>
        <CameraTrigger preset={preset.data} update={update}/>
      </div>
    </div>}

    {presets.length === 0 && <div className="no-presets">
      You don't have any presets yet. Click the icon above to add one.
    </div>}
  </div>;
};

export const PresetsDialog: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const { open } = useSelector(selectPresetEditor);

  return (
    <Modal
      className="presets-dialog"
      isOpen={open}
      headerIcon={<AppIconsNeutral.Microscope/>}
      headerText="Presets"
      onRequestClose={actions.presetEditorCancel}
      buttons={
        <ButtonRowConfirmCancel confirmText="Save" onConfirm={actions.presetEditorSave} onCancel={actions.presetEditorCancel}/>
      }
    >
      {open && <PresetEditor/>}
    </Modal>);
};

export const OpenPresetsDialog: React.FC = () => {
  const actions = useActions(actionsDefinition);
  return <Button className="edit-presets" color="grey" onClick={actions.presetEditorOpen}>Edit Presets</Button>;
};
