import { ServoTuningParamset } from '@zaber/motion/ascii';
import { createSelector } from 'reselect';
import { match } from 'ts-pattern';

import { selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import { getEntityType, EntityKeyType } from '../keys';
import { selectServoTuning } from '../store';

import { sortAdvancedParams } from './tuners/advanced_metadata';
import type { ChosenTuning } from './types';

export const selectEntity = createSelector(selectServoTuning, state => state.entity);

export const selectChosen = createSelector(selectServoTuning, (state): ChosenTuning | null => {
  if (state.entity != null && state.paramset != null && state.method != null) {
    return { paramset: state.paramset, method: state.method };
  }
  return null;
});

export const selectMessage = createSelector(selectServoTuning, state => state.message);

export const selectDeviceInfo = createSelector(selectServoTuning, state => state.deviceInfo);

export const selectRecommendedAccelMetersPerSecond2 = createSelector(selectDeviceInfo, info => info?.recommendedAccel ?? null);

export const selectSavedRows = createSelector(selectServoTuning, state => state.savedRows);

export const selectTuningInfo = createSelector(selectServoTuning, state => state.tuning);

export const selectIsLiveTuning = createSelector(selectChosen, chosen => chosen?.paramset === ServoTuningParamset.LIVE);
export const selectIsStartupParamset = createSelector(
  selectServoTuning, selectChosen, (state, chosen) => chosen?.paramset === state.deviceInfo?.startupParamset
);
export const selectIsWriting = createSelector(selectTuningInfo, info => info?.message?.status === 'writing');

export const selectSimpleTuningInfo = createSelector(selectTuningInfo, info => info?.method === 'simple' ? info : null);
export const selectAdvancedTuningInfo = createSelector(selectTuningInfo, info => info?.method === 'advanced' ? info : null);

export const selectProduct = createSelector(selectEntity, selectIdentifiedAxes, selectIdentifiedDevices, (entity, axes, devices) => {
  if (entity == null) { return null }
  return  match(getEntityType(entity))
    .with(EntityKeyType.AXIS, () => axes[entity])
    .with(EntityKeyType.DEVICE, () => devices[entity])
    .otherwise(() => null);
});

export const selectSortedAdvancedParams = createSelector(selectAdvancedTuningInfo, info => {
  if (info == null) { return null }
  return sortAdvancedParams(info.tuning);
});
