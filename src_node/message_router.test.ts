import { EventEmitter } from 'events';

import { waitUntil } from '@zaber/toolbox/lib/test';

class AppMock {
  getPath(type: string): string {
    return `/home/zaber/path-${type}`;
  }
}

let processMock: ProcessMock;
class ProcessMock extends EventEmitter {
  constructor() {
    super();
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    processMock = this;
  }
  kill = jest.fn();
}

let socketMock: SocketMock;
class SocketMock extends EventEmitter {
  constructor() {
    super();
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    socketMock = this;
  }
  end = jest.fn();
}

const createConnectionMock = jest.fn<unknown, []>().mockImplementation(() => new SocketMock());
const spawnMock = jest.fn<unknown, [string, string, { env: Record<string, string> }]>(() => new ProcessMock());
const handleNonCriticalErrorMock = jest.fn<unknown, [Error]>();

jest.mock('electron', () => ({
  app: new AppMock(),
}));
jest.mock('child_process', () => ({
  spawn: spawnMock,
}));
jest.mock('net', () => ({
  createConnection: createConnectionMock,
}));
jest.mock('../src/errors/errors', () => ({
  handleNonCriticalError: handleNonCriticalErrorMock,
}));
jest.mock('@zaber/motion', () => ({
  Tools: class ToolsMock {
    static getMessageRouterPipePath() { return 'PIPE' }
    static getDbServicePipePath() { return 'PIPE' }
  },
}));

jest.useFakeTimers();

import { environment, Environment } from '../src/environment';

import { createContainer, destroyContainer } from './container';
import { MessageRouter, EXIT_TIMEOUT } from './message_router';

let container: ReturnType<typeof createContainer>;
let messageRouter: MessageRouter;

beforeEach(() => {
  (environment as Environment).isProduction = true;
  container = createContainer();
  messageRouter = container.get(MessageRouter);
});

afterEach(() => {
  destroyContainer();
  container = null!;
  processMock = null!;
  socketMock = null!;
  spawnMock.mockClear();
  handleNonCriticalErrorMock.mockClear();
  createConnectionMock.mockClear();
});

test('spawns the message router process and provides correct arguments', () => {
  messageRouter.startRouter();
  expect(spawnMock).toHaveBeenCalled();
  const [exe, args, options] = spawnMock.mock.calls[0];
  expect(exe).toContain('zaber-message-router');
  expect(args).toEqual(['run']);
  expect(options.env.ZABER_MESSAGE_ROUTER_CONFIG_FILE).toContain('message_router_config.yml');
});

test('reports error on exit if not stopped', async () => {
  messageRouter.startRouter();

  processMock.emit('exit', 1, 'SIGKILL');
  await expect(messageRouter.runPromise).rejects.toEqual(expect.objectContaining({
    message: 'Unexpected exit',
    exitCode: 1,
  }));
});

test('reports an error if process cannot be started', async () => {
  messageRouter.startRouter();

  processMock.emit('error', new Error('Cannot spawn process'));

  await expect(messageRouter.runPromise).rejects.toEqual(expect.objectContaining({
    message: 'Cannot start: Cannot spawn process',
    exitCode: 1,
  }));
});

test('stops gracefully', async () => {
  messageRouter.startRouter();

  const stopPromise = messageRouter.stopRouter();
  socketMock.emit('connect');
  processMock.emit('exit', 0, null);

  await Promise.all([
    stopPromise,
    messageRouter.runPromise,
  ]);
});

test('reports error on non-zero exit code', async () => {
  messageRouter.startRouter();

  const promise = messageRouter.stopRouter();
  socketMock.emit('connect');

  processMock.emit('exit', 1, '');
  await promise;

  expect(handleNonCriticalErrorMock).toHaveBeenCalledWith(expect.objectContaining({
    message: 'Non-zero exit code: 1',
  }));
  expect(processMock.kill).not.toHaveBeenCalled();
});

describe('stopRouter', () => {
  beforeEach(() => {
    messageRouter.startRouter();
  });

  test('terminates the process', async () => {
    const promise = messageRouter.stopRouter();

    socketMock.emit('connect');
    processMock.emit('exit', 0);
    await promise;

    expect(socketMock.end).toHaveBeenCalledWith(expect.stringMatching(/control_exit/));
  });

  test('kills process if it does not exit', async () => {
    const promise = messageRouter.stopRouter();

    socketMock.emit('connect');
    await waitUntil(() => socketMock.end.mock.calls.length > 0);

    jest.advanceTimersByTime(EXIT_TIMEOUT);
    await promise;

    expect(handleNonCriticalErrorMock).toHaveBeenCalledWith(expect.objectContaining({
      message: 'Message router has not exit',
    }));
    expect(processMock.kill).toHaveBeenLastCalledWith('SIGKILL');
  });

  test('kills process if there is an error', async () => {
    const promise = messageRouter.stopRouter();

    socketMock.emit('error', new Error('pipe does not exist'));
    await promise;

    expect(handleNonCriticalErrorMock).toHaveBeenCalledWith(expect.objectContaining({
      message: 'pipe does not exist',
    }));
    expect(processMock.kill).toHaveBeenLastCalledWith('SIGKILL');
  });
});
