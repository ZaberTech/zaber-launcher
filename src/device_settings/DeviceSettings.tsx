import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useActions } from '@zaber/toolbox/lib/redux';
import { match, P } from 'ts-pattern';

import { ConnectionsView } from '../connection_manager';
import { NoContentMessage, Title } from '../components';

import { actions as actionsDefinition } from './actions';
import { selectCurrentEntity, selectCustomDialog } from './selectors';
import { TopPanel } from './TopPanel';
import { SettingsList } from './SettingsList';
import { NetworkSettings } from './NetworkSettings';

const TITLE = 'Welcome to Device Settings!';

const NoneSelected = () => <NoContentMessage title={TITLE} message="Select device or axis to edit settings."/>;
const Loading = () => <NoContentMessage title={TITLE} type="working" message="Loading settings..."/>;

export const DeviceSettings: React.FC = () => {
  const current = useSelector(selectCurrentEntity);
  const actions = useActions(actionsDefinition);
  const customDialog = useSelector(selectCustomDialog);
  useEffect(() => {
    actions.readAllSettings();
  }, []);
  return (
    <div className="device-settings connection-view-and-app">
      <Title>Device Settings</Title>
      <ConnectionsView
        selectable={['device', 'axis']}
        selected={current?.key ?? null}
        onSelect={actions.selectDeviceOrAxis}/>
      <div className="settings app-ui">
        {match(current)
          .with(P.nullish, () => <NoneSelected/>)
          .with({ settingIDsSorted: P.nullish }, () => <Loading/>)
          .with({ settingIDsSorted: P.not(P.nullish) }, current => <>
            <TopPanel/>
            <SettingsList key={current.key}/>
            {customDialog === 'network settings' && <NetworkSettings/>}
          </>)
          .exhaustive()}
      </div>
    </div>
  );
};
