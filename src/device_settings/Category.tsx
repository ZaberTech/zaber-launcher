import React, { useState } from 'react';
import { Flex, Icons, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { ProtocolManual, ToggleIfProtocolManual } from '../protocol_manual';
import { environment, Flavors } from '../environment';

import { Category as CategoryData, selectCurrentKey } from './selectors';

interface CategoryProps {
  category: CategoryData;
}

export const Category: React.FC<CategoryProps> = ({ category, children }) => {
  const key = useSelector(selectCurrentKey);
  const [showManual, setShowManual] = useState(false);
  return (
    <div className="category sections-subgrid" data-category={category.key}>
      <div className="title sections-subgrid">
        <Flex.Row>
          {category.meta.reverse().map(({ humanName }, i) => (
            <React.Fragment key={i}>
              {i > 0 && <Icons.ArrowCollapsed/>}
              <Text t={Text.Type.H4}>{humanName}</Text>
            </React.Fragment>
          ))}
        </Flex.Row>
        {environment.flavor === Flavors.Zaber && <ToggleIfProtocolManual
          deviceOrAxisKey={key} setting={category.key} className="show-documentation"
          activated={showManual} onClick={() => setShowManual(!showManual)}
        />}
      </div>
      {showManual && <ProtocolManual deviceOrAxisKey={key} setting={category.key} hideWhenNoDocs/>}
      {children}
    </div>
  );
};
