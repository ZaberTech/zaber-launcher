import React from 'react';

import { SettingField } from '../fields/SettingField';

import type { ContentProps, HwModSectionTemplate } from './HwModSection';

const LimitCSection: React.FC<ContentProps<'limitC'>> = ({ values }) => {
  const sectionId = 'limitC';

  return <>
    <SettingField {...{ values, sectionId, fieldId: 'type' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'source' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'edge' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'width' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'action' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'posUpdate' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'preset' }}/>
  </>;
};

export const limitCTemplate: HwModSectionTemplate<'limitC'> = {
  sectionId: 'limitC',
  titleName: 'C Limit Sensor',
  description: 'Information about the C limit sensor, which is usually between the home and away limit sensors.',
  generateContent: values => (<LimitCSection values={values}/>),
};
