import React from 'react';

import { LockstepGroupCard } from './LockstepGroupCard';
import type { LockstepAxesData } from './reducer';
import type { DeviceStateLockstep } from './selectors';

interface Props {
  header: React.ReactNode;
  device: DeviceStateLockstep;
  lockstepData: LockstepAxesData;
}

export const LockstepContainer: React.FC<Props> = ({ header, lockstepData, device }) => (
  <>
    {Object.entries(lockstepData).map(([groupNumber, axes]) => (
      <LockstepGroupCard
        key={groupNumber}
        header={header}
        groupNumber={+groupNumber}
        device={device}
        axesData={axes}
      />))}
  </>
);
