import _ from 'lodash';
import { createSelector } from 'reselect';
import { tryAccess } from '@zaber/toolbox';

import { selectDeviceIo } from '../store';
import {
  selectIdentifiedDevices,
} from '../connection_manager';
import { isSubKeyOf } from '../keys';

import { maybeHasIO } from './types';

export const selectDevices = createSelector(selectDeviceIo, selectIdentifiedDevices,
  (state, managerDevices) => _.mapValues(state.devices, device => ({
    ...device,
    ...managerDevices[device.key],
  })));

export const selectSelectedDevicesWithIo = createSelector(selectDeviceIo, selectIdentifiedDevices,
  (state, devices) =>
    _.filter(devices, device => isSubKeyOf(device.key, state.selectedKey)
    && maybeHasIO(tryAccess(state.devices, device.key)?.ioState)));

export const selectSelectedKey = createSelector(selectDeviceIo, state => state.selectedKey);
