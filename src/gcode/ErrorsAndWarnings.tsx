import React, { useEffect, useRef, useState } from 'react';
import _ from 'lodash';
import * as monaco from 'monaco-editor';
import { useSelector } from 'react-redux';
import { Icons, NoticeBanner, Colors, Button } from '@zaber/react-library';

import { useActions } from '../utils';

import './language';
import { actions as actionsDefinition } from './actions';
import {
  selectError, selectWarnings,
  selectStatus,
} from './selectors';
import type { BlockMessage } from './types';
import { EditorContextType, useEditor } from './EditorContext';

function getWarningIndex(
  editor: EditorContextType['editor'],
  warnings: BlockMessage[],
  which: 'next' | 'prev' | 'current',
) {
  const position = editor.getPosition() ?? { lineNumber: 1, column: 1 };
  const blockPosition = { blockIndex: position.lineNumber - 1, fromBlock: position.column - 1 };
  let nextWarningIndex = _.sortedIndexBy<Pick<BlockMessage, 'blockIndex' | 'fromBlock'>>(
    warnings,
    blockPosition,
    warning => warning.blockIndex * 1000 + warning.fromBlock);

  if (which === 'next') {
    const current = warnings.at(nextWarningIndex);
    if (current?.blockIndex === blockPosition.blockIndex && current?.fromBlock === blockPosition.fromBlock) {
      nextWarningIndex++;
    }
  } else if (which === 'prev') {
    nextWarningIndex--;
  }

  if (nextWarningIndex >= warnings.length) {
    nextWarningIndex = 0;
  } else if (nextWarningIndex < 0) {
    nextWarningIndex = warnings.length - 1;
  }

  return nextWarningIndex;
}

export const Warnings: React.FC = () => {
  const warnings = useSelector(selectWarnings);
  const [current, setCurrent] = useState(0);

  const context = useEditor();

  useEffect(() => {
    if (warnings.length === 0) { return }

    setCurrent(getWarningIndex(context.editor, warnings, 'current'));
  }, [warnings[0]]);

  const onWarningsClick = (which: 'next' | 'prev') => {
    if (warnings.length === 0) { return }

    const nextIndex = getWarningIndex(context.editor, warnings, which);
    const nextWarning = warnings[nextIndex];
    setCurrent(nextIndex);
    context.setPositionToMessage(nextWarning, false);
    context.editor.focus();
  };

  const warningDecorations = useRef<string[]>([]);
  const previousWarningsRef = useRef<typeof warnings>([]);

  useEffect(() => {
    const previousWarnings = previousWarningsRef.current;
    previousWarningsRef.current = warnings;

    const append = warnings.length > previousWarnings.length && previousWarnings.length > 0 && previousWarnings[0] === warnings[0];

    const decorations = (append ? warnings.slice(previousWarnings.length) : warnings)
      .map<monaco.editor.IModelDeltaDecoration>(message => ({
        range: new monaco.Range(message.blockIndex + 1, message.fromBlock + 1, message.blockIndexEnd + 1, message.toBlock + 1),
        options: {
          inlineClassName: 'block-warning',
          hoverMessage: { value: `Warning: ${message.message}` },
          stickiness: monaco.editor.TrackedRangeStickiness.NeverGrowsWhenTypingAtEdges,
          overviewRuler: {
            color: Colors.yellow,
            position: monaco.editor.OverviewRulerLane.Right,
          },
        },
      }));

    if (append) {
      warningDecorations.current.push(...context.model.deltaDecorations([], decorations));
    } else {
      warningDecorations.current = context.model.deltaDecorations(warningDecorations.current, decorations);
    }
  }, [warnings]);

  if (warnings.at(current) == null) {
    return null;
  }

  return <NoticeBanner type="warning">
    <div>Warning ({current + 1}/{warnings.length}): {warnings[current].message}</div>
    <Icons.ArrowExpanded className="prev" onClick={() => onWarningsClick('prev')} title="Previous Warning"/>
    <Icons.ArrowExpanded className="next" onClick={() => onWarningsClick('next')} title="Next Warning"/>
  </NoticeBanner>;
};

export const Errors: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const error = useSelector(selectError);
  const status = useSelector(selectStatus);

  const context = useEditor();

  const onErrorClick = () => {
    if (!error) { return }

    context.setPositionToMessage(error, false);
    context.editor.focus();
  };

  const errorDecorations = useRef<string[]>([]);

  useEffect(() => {
    const decorations: monaco.editor.IModelDeltaDecoration[] = [];
    if (error != null) {
      decorations.push({
        range: new monaco.Range(error.blockIndex + 1, error.fromBlock + 1, error.blockIndexEnd + 1, error.toBlock + 1),
        options: {
          isWholeLine: error.fromBlock < 0,
          inlineClassName: 'block-error',
          hoverMessage: { value: `Error: ${error.message}` },
          marginClassName: 'block-error-margin',
          glyphMarginHoverMessage: { value: `Error: ${error.message}` },
          overviewRuler: {
            color: Colors.pink,
            position: monaco.editor.OverviewRulerLane.Right,
          },
          stickiness: monaco.editor.TrackedRangeStickiness.NeverGrowsWhenTypingAtEdges,
        },
      });
      context.setPositionToMessage(error, true);
    }
    errorDecorations.current = context.model.deltaDecorations(errorDecorations.current, decorations);
  }, [error]);

  return <>
    {status.translatorError && <NoticeBanner>
      <div>Translator error: {status.translatorError}</div>
      <Button color="grey" size="small" onClick={actions.startTranslator}>Restart Translator</Button>
    </NoticeBanner>}
    {error && <NoticeBanner>
      <div>Error (1/1): {error.message}</div>
      <Icons.ArrowExpanded onClick={onErrorClick} title="Show Error"/>
    </NoticeBanner>}
  </>;
};
