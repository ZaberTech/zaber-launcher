import _ from 'lodash';

import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import { PreferenceState } from './types';


export interface State {
  nameError: string | null;
  settingName: boolean;
  preferenceStates: _.Dictionary<PreferenceState>;
}

const initialState: State = {
  nameError: null,
  settingName: false,
  preferenceStates: { },
};

const setName = (state: State): State =>
  ({
    ...state,
    settingName: true,
  });

const setNameDone = (state: State, { err }: ActionsToPayloads[ActionTypes.SET_NAME_DONE]): State =>
  ({
    ...state,
    settingName: false,
    nameError: err ?? null,
  });

const setPreference = (state: State, { key, value }: ActionsToPayloads[ActionTypes.SET_PREFERENCE]): State =>
  ({
    ...state,
    preferenceStates: {
      ...state.preferenceStates,
      [key]: {
        value,
        updating: true,
      },
    },
  });

const preferenceUpdated = (state: State, { key }: ActionsToPayloads[ActionTypes.PREFERENCE_UPDATED]): State =>
  ({
    ...state,
    preferenceStates: {
      ...state.preferenceStates,
      [key]: {
        ...state.preferenceStates[key],
        updating: false,
      },
    },
  });

export const reducer = createReducer<ActionsToPayloads, State>({
  [ActionTypes.SET_NAME]: setName,
  [ActionTypes.SET_NAME_DONE]: setNameDone,
  [ActionTypes.SET_PREFERENCE]: setPreference,
  [ActionTypes.PREFERENCE_UPDATED]: preferenceUpdated,
}, initialState);
