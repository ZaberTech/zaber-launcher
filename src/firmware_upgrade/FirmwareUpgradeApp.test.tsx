import * as fs from 'fs';

jest.mock('../dialogs', () => ({
  Dialogs: {
    showOpenDialog: jest.fn(async () => ({ filePaths: ['local.fwu'] }))
  },
}));
jest.mock('@electron/remote', () => ({
  app: {
    getPath: jest.fn((descriptor: string) => {
      if (descriptor === 'userData') {
        return '/path/to/user/data/AppName';
      } else {
        return '/';
      }
    }),
    getName: () => 'ElectronMock',
  },
}));

let streamPromise: Defer<number | 'done'>;
class FirmwareUpgradeStreamerMock {
  async beginUpgrade(): Promise<number> {
    return 0;
  }

  stream = jest.fn(async () => {
    streamPromise = defer();
    return streamPromise.promise;
  });
}

jest.mock('../app_components', () => ({
  ...jest.requireActual('../app_components'),
  FirmwareUpgradeStreamer: FirmwareUpgradeStreamerMock,
}));

import { fireEvent, getByText, render, RenderResult, within } from '@testing-library/react';
import { ascii, RequestTimeoutException } from '@zaber/motion';
import { Container, injectable } from 'inversify';
import _ from 'lodash';
import React from 'react';

import { Dialogs } from '../dialogs';
import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { connectionManagerActions, IdentifiedDeviceInfo, selectDevices } from '../connection_manager';
import {
  mockSingleDevice, MOCK_DEVICE_NUMBER,
  mockSingleDeviceWithPeripherals
} from '../connection_manager/mocks';
import { createContainer, destroyContainer } from '../container';
import { MessageRoutersService } from '../message_router';
import { waitTick, waitUntilPass, wrapWithNewStore, mockStorage, StorageMock, Defer, defer } from '../test';
import *  as desktop from '../desktop';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase
} from '../test/mocks/ascii';
import { extractConnectionKey, getDeviceAddress, makeDeviceKey } from '../keys';
import { mockReloadDevices } from '../test/mocks/reload_devices';
import { FirmwareWebQuery, VersionList } from '../app_components';

import { FirmwareUpgradeApp } from './FirmwareUpgradeApp';
import { testLoadingOfFirmwareVersions } from './sagas';
import { selectDeviceKey } from './selectors';
import { CanSetStateDeviceResponse } from '@zaber/motion/ascii';

beforeAll(() => {
  testLoadingOfFirmwareVersions();
});

jest.spyOn(fs.promises, 'mkdir').mockResolvedValue('');
jest.spyOn(fs.promises, 'writeFile').mockResolvedValue();

let container: Container;
let storageMock: StorageMock;

let axes: AxisMock[];
class AxisMock extends AxisMockBase {
  constructor(axisNumber: number, device: DeviceMockBase<AxisMockBase>) {
    super(axisNumber, device);
    axes.push(this);
  }

  identity?: Partial<ascii.AxisIdentity>;

  settings = {
    get: jest.fn(_name => 1),
    set: jest.fn((_name, _value) => 1),
  };
}

let lockstepMock: LockstepMock;
class LockstepMock {
  isEnabled = jest.fn(async () => false);
  getAxisNumbers = jest.fn(async () => [1, 2]);
}

const MOCK_FW_6_DEVICE_ADDRESS = MOCK_DEVICE_NUMBER + 1;

const MOCK_EMIT_1 = 'SEND';
const MOCK_EMIT_2 = 'TO';
const MOCK_EMIT_3 = 'STREAM';

const EXTENDED_TIMEOUT = 3000;

const MOCK_SERIAL_NUMBER = 123456;
const MOCK_PLATFORM_ID = 987343;

const RELEASE_NOTES = 'Release Notes for the current device';

/** The device will always fail this command */
let failCmd = '';

let failGetState = '';
let canSetStateResult: CanSetStateDeviceResponse = { error: null, axisErrors: [] };

let device: DeviceMock;
class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    device = this;
  }
  identity: Partial<ascii.DeviceIdentity> = {
    serialNumber: MOCK_SERIAL_NUMBER,
    firmwareVersion: { major: 7, minor: 0, build: 0 },
  };
  get serialNumber() { return this.identity.serialNumber }
  get firmwareVersion() { return this.identity.firmwareVersion }
  _settings: Record<string, number> = {
    'lockstep.numgroups': 1,
  };
  settings = {
    get: jest.fn(name => Promise.resolve(this._settings[name])),
  };

  identify = jest.fn(async () => this.identity);

  getLockstep = jest.fn((_n: number) => lockstepMock);

  genericCommand = jest.fn<Promise<{data: string}>, [string]>(async (cmd: string) => {
    await waitTick();
    if (cmd === failCmd) {
      return { data: 'This data cannot be parsed by ZML and will cause an error to be thrown' };
    }
    switch (cmd) {
      case 'get bootloader.keyed': return { data: '0' };
      case 'get system.platform': return { data: `${MOCK_PLATFORM_ID}` };
      case 'system upgrade start': return { data: `${MOCK_EMIT_1.length}` };
      case `system upgrade data ${btoa(MOCK_EMIT_1)}`: return { data: `${MOCK_EMIT_2.length}` };
      case `system upgrade data ${btoa(MOCK_EMIT_2)}`: return { data: `${MOCK_EMIT_3.length}` };
      case `system upgrade data ${btoa(MOCK_EMIT_3)}`: return { data: '0' };
      case 'system reset': {
        pollsUntilDeviceReconnects = 3;
        return { data: '' };
      }
      case 'system upgrade end': return { data: '' };
      default: throw new Error(`DeviceMock cannot parse command: "${cmd}"`);
    }
  });

  getState = jest.fn(() => {
    if (failGetState === '') {
      return 'device_state';
    } else {
      throw new Error(failGetState);
    }
  });
  setState = jest.fn((_: string) => null);
  canSetState = jest.fn(() => canSetStateResult);
}

let deviceAddressAfterUpdate: number | null = null;

let pollsUntilDeviceReconnects: number;
let errorDuringGenericPolling = '';
let otherDeviceSerialNumbers: { data: string }[];
class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  genericCommandNoResponse = jest.fn().mockResolvedValue(undefined);

  genericCommandMultiResponse = jest.fn<Promise<{data: string}[]>, [string]>(async (cmd: string) => {
    await waitTick();
    switch (cmd) {
      case 'get system.serial': {
        pollsUntilDeviceReconnects--;
        if (pollsUntilDeviceReconnects > 1) {
          if (errorDuringGenericPolling !== '') {
            throw new Error(errorDuringGenericPolling);
          }
          throw new RequestTimeoutException('Test that timeout exceptions are handled');
        }
        if (pollsUntilDeviceReconnects > 0) {
          // Test that the wait function keeps polling while the mocked device is not returned
          return [...otherDeviceSerialNumbers];
        } else {
          return [...otherDeviceSerialNumbers, { data: `${MOCK_SERIAL_NUMBER}`, deviceAddress: deviceAddressAfterUpdate ?? 1 }];
        }
      }
      default: throw new Error(`ConnectionMock cannot parse command: "${cmd}"`);
    }
  });
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

let firmwareWebQueryMock: FirmwareWebQueryMock;
@injectable()
class FirmwareWebQueryMock {
  getVersions = jest.fn<Promise<VersionList>, []>(async () => ({
    [String(MOCK_DEVICE_NUMBER)]: [
      {
        parsed: { major: 7, minor: 3, build: 699 },
        str: '7.3',
        notes: 'http://zaber-release-notes',
      },
      {
        parsed: { major: 7, minor: 11, build: 1233 },
        str: '7.11',
        notes: 'http://zaber-release-notes',
      },
      {
        parsed: { major: 7, minor: 11, build: 1234 },
        str: '7.11.1234',
        notes: RELEASE_NOTES,
      },
      {
        parsed: { major: 7, minor: 23, build: 1235 },
        str: '7.23',
        notes: 'http://zaber-release-notes',
      },
      {
        parsed: { major: 0, minor: 0, build: 1236 },
        str: 'unlock',
        notes: 'Use this to unlock',
      },
      {
        parsed: { major: 7, minor: 98, build: 1237 },
        str: '7.98',
        notes: 'http://zaber-release-notes',
      },
      {
        parsed: { major: 7, minor: 99, build: 1231 },
        str: '7.99',
        Notes: 'Dev release notes',
      },
    ],
    [String(MOCK_FW_6_DEVICE_ADDRESS)]: [
      {
        parsed: { major: 6, minor: 2, build: 123 },
        str: '6.2',
        notes: 'http://zaber-release-notes',
      },
    ]
  }));

  reportInstall = jest.fn(async () => undefined);
  getUpdatePackage = jest.fn(async () => 'upgrade.bin');
}

const TestFirmwareUpgradeApp = wrapWithNewStore(FirmwareUpgradeApp);

let modifyAfterUpgrade: ((device: IdentifiedDeviceInfo) => void) | null = null;

mockReloadDevices(() => TestFirmwareUpgradeApp.testStore, (devices, connectionKey) => {
  if (deviceAddressAfterUpdate != null) {
    devices[0].key = makeDeviceKey(connectionKey, deviceAddressAfterUpdate);
    devices[0].address = deviceAddressAfterUpdate;
  }
  modifyAfterUpgrade?.(devices[0] as IdentifiedDeviceInfo);
});

let wrapper: RenderResult;
function modal() {
  return within(wrapper.baseElement.querySelector('.firmware-upgrade-progress-modal')!);
}


beforeEach(() => {
  pollsUntilDeviceReconnects = 0;
  errorDuringGenericPolling = '';
  otherDeviceSerialNumbers = [{ data: `${MOCK_SERIAL_NUMBER + 1}` }];
  failCmd = '';
  failGetState = '';
  canSetStateResult = { error: null, axisErrors: [] };

  axes = [];

  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  storageMock = mockStorage(container);
  lockstepMock = new LockstepMock();
  container.bind<unknown>(FirmwareWebQuery).to(FirmwareWebQueryMock);
  firmwareWebQueryMock = container.get<unknown>(FirmwareWebQuery) as FirmwareWebQueryMock;
  streamPromise = defer();

  wrapper = render(<TestFirmwareUpgradeApp/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  axes = null!;
  device = null!;

  destroyContainer();
  container = null!;
  lockstepMock = null!;

  deviceAddressAfterUpdate = null;
  modifyAfterUpgrade = null;
  firmwareWebQueryMock = null!;
  streamPromise = null!;
  storageMock.stored = {};
});

async function waitUntilSuccess() {
  await waitUntilPass(() => modal().getByText('Cancel'), EXTENDED_TIMEOUT);
  streamPromise.resolve('done');
  await waitUntilPass(() => modal().getByText('Firmware upgrade of the device was successfully completed.'), EXTENDED_TIMEOUT);
}

describe('Firmware Upgrade Main page', () => {
  beforeEach(() => {
    const store = TestFirmwareUpgradeApp.testStore;
    mockSingleDevice(store, d => ({ ...d, identity: { ...d.identity, serialNumber: MOCK_SERIAL_NUMBER } }));
  });

  test('Shows a message before a device is selected', async () => {
    await waitUntilPass(() => wrapper.getByText(/Select a device to upgrade firmware/i));
  });

  test('Select device from the connections view and loads versions', async () => {
    const device = _.sample(selectDevices(TestFirmwareUpgradeApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText(/Firmware Version 7.11.1234/));

    getByText(wrapper.getByTestId('section_new_versions_available'), '7.23');
    getByText(wrapper.getByTestId('section_older_versions'), '7.3');
    getByText(wrapper.getByTestId('section_other_versions'), '7.98');
    getByText(wrapper.getByTestId('section_internal_versions'), '7.99');
    getByText(wrapper.getByTestId('section_internal_versions'), 'unlock');
  });

  describe('device selected', () => {
    beforeEach(async () => {
      const device = _.sample(selectDevices(TestFirmwareUpgradeApp.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);

      await waitUntilPass(() => wrapper.getByText('New version available'));
    });

    test('Skip version', async () => {
      fireEvent.click(wrapper.getByText('Skip'));
      await waitUntilPass(() => wrapper.getByText('Skipped Versions'));
      expect(wrapper.queryByText('New Versions Available')).toBeNull();
      wrapper.getByText('Up to date');
    });

    test('Refresh versions', async () => {
      firmwareWebQueryMock.getVersions.mockResolvedValue({
        [String(MOCK_DEVICE_NUMBER)]: [
          {
            parsed: { major: 7, minor: 53, build: 699 },
            str: '7.53',
            notes: '',
          },
        ]
      });
      fireEvent.click(wrapper.getByTitle('Refresh Versions'));
      await waitUntilPass(() => wrapper.getByText('7.53'));
    });

    test('Cancel device upgrade', async  () => {
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      await waitUntilPass(() => wrapper.getByTitle('Cancel Upgrade'));
      fireEvent.click(wrapper.getByTitle('Cancel Upgrade'));
      await waitUntilPass(
        () => wrapper.getByText('The upgrade was cancelled and your device was reset to its former version.'),
        EXTENDED_TIMEOUT
      );
      fireEvent.click(wrapper.getByTitle('Leave Upgrade Dialogue'));
    });

    test('See release notes in dialog', async  () => {
      fireEvent.click(wrapper.getByTitle('Additional Device Firmware Actions'));
      fireEvent.click(wrapper.getByText('Read Release Notes'));
      wrapper.getByText(RELEASE_NOTES);
      fireEvent.click(wrapper.getByTitle('Close Dialog'));
      expect(wrapper.queryByText(RELEASE_NOTES)).toBeNull();
    });

    test('Open external release notes', async  () => {
      const openExternalSpy = jest.spyOn(desktop, 'openExternalLink').mockImplementation();
      fireEvent.click(wrapper.getAllByTitle(/Open Menu/)[1]);
      fireEvent.click(wrapper.getByText('Read Release Notes'));
      expect(openExternalSpy).toHaveBeenCalledWith('http://zaber-release-notes');
    });

    test('Upgrade device without save', async  () => {
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Clear')));
      await waitUntilSuccess();
      await waitUntilPass(() => wrapper.getByText('View Release Notes'));
      fireEvent.click(wrapper.getByTitle('Leave Upgrade Dialogue'));
    });

    test('Upgrade device with save', async () => {
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Preserve')));
      await waitUntilSuccess();
      expect(device.setState).toHaveBeenCalled();
    });

    test('Cannot save if there are many devices with the same serial number', async () => {
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      otherDeviceSerialNumbers = [{ data: `${MOCK_SERIAL_NUMBER}` }, { data: `${MOCK_SERIAL_NUMBER}` }];
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Preserve')));
      const err = `There are multiple devices with serial number ${MOCK_SERIAL_NUMBER}. We do not support saving in this case.`;
      await waitUntilPass(() => modal().getByText(RegExp(err)));
      fireEvent.click(wrapper.getByText('Upgrade Without Saving'));
      await waitUntilSuccess();
      expect(device.setState).not.toHaveBeenCalled();
    });

    test('Upgrade to internal version', async () => {
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.99'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Attempt Save')));
      await waitUntilSuccess();
    });

    test('Pre-upgrade save fails', async () => {
      failGetState = 'Cannot get state';
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Preserve')));
      await waitUntilPass(() => wrapper.getByText(/There was an error saving your device's state: Cannot get state/));
      failGetState = 'Still cannot get state';
      fireEvent.click(wrapper.getByText('Try Again'));
      await waitUntilPass(() => wrapper.getByText(/There was an error saving your device's state: Still cannot get state/));
      fireEvent.click(wrapper.getByText('Upgrade Without Saving'));
      await waitUntilSuccess();
    });

    test('Will fail to set future state for device error', async () => {
      canSetStateResult = { error: 'unsupported stuff in the new version', axisErrors: [] };
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Preserve')));
      await waitUntilPass(() => wrapper.getByText(/After the upgrade to 7.23, Zaber Launcher will be unable to recreate your device's/));
      await waitUntilPass(() => wrapper.getByText(/current state because unsupported stuff in the new version/));
    });

    test('Will fail to set future state for axis errors', async () => {
      canSetStateResult = { error: null, axisErrors: [{ error: 'a1 problem', axisNumber: 1 }, { error: 'bad vibes', axisNumber: 3 }] };
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Preserve')));
      await waitUntilPass(() => wrapper.getByText(/After the upgrade to 7.23, Zaber Launcher will be unable to recreate your device's/));
      await waitUntilPass(() => wrapper.getByText(/current state because "a1 problem" on axis 1 and "bad vibes" on axis 3/));
    });

    test('Downgrade device', async () => {
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.3'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Downgrade')));
      await waitUntilSuccess();
    });

    test('Upgrade device from local file', async () => {
      fireEvent.click(wrapper.getByTitle('Install from File'));
      expect(Dialogs.showOpenDialog).toHaveBeenCalledWith({
        title: 'Open Firmware Upgrade File',
        filters: expect.anything(),
      });
      await waitUntilPass(() => fireEvent.click(wrapper.getByTitle('Install')));
      await waitUntilSuccess();
      fireEvent.click(wrapper.getByText('OK'));
    });

    test('re-selects device if the address changes after the upgrade', async  () => {
      const NEW_ADDRESS = 2;
      deviceAddressAfterUpdate = NEW_ADDRESS;
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Clear')));
      await waitUntilPass(() => modal().getByText('Cancel'), EXTENDED_TIMEOUT);
      streamPromise.resolve('done');
      await waitUntilPass(
        () => wrapper.getByText(/successfully completed/),
        EXTENDED_TIMEOUT
      );
      const deviceKey = selectDeviceKey(TestFirmwareUpgradeApp.testStore.getState())!;
      expect(getDeviceAddress(deviceKey)).toBe(NEW_ADDRESS);
    });

    test('reports back successful upgrade', async  () => {
      modifyAfterUpgrade = device => {
        device.identity.firmwareVersion = {
          major: 7,
          minor: 23,
          build: 1235,
        };
      };
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Clear')));
      await waitUntilSuccess();
      expect(firmwareWebQueryMock.reportInstall).toHaveBeenCalledWith([{
        BuildNumber: 1235,
        DeviceID: 50081,
        DeviceNumber: 1,
        FWVersion: '7.23',
        IsKeyed: false,
        PlatformID: 987343,
        SerialNumber: 123456,
      }]);
    });

    test('restores the state to the correct device after address change', async  () => {
      const NEW_ADDRESS = 2;
      deviceAddressAfterUpdate = NEW_ADDRESS;
      fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
      await waitUntilPass(() => fireEvent.click(wrapper.getByText('Preserve')));
      await waitUntilSuccess();
      expect(device.deviceAddress).toBe(NEW_ADDRESS);
      expect(device.setState).toHaveBeenCalled();
    });
  });
});

test ('Upgrade controller', async () => {
  const store = TestFirmwareUpgradeApp.testStore;
  mockSingleDeviceWithPeripherals(store, {
    modifier: d => ({ ...d, identity: { ...d.identity, serialNumber: MOCK_SERIAL_NUMBER } }),
  });


  const device = _.sample(selectDevices(store.getState()))!;
  connectionViewMockInstance.props.onSelect(device.key);

  await waitUntilPass(() => fireEvent.click(wrapper.getByTitle('Install Firmware 7.23')));
  await waitUntilPass(() => fireEvent.click(wrapper.getByText('Preserve')));
  await waitUntilSuccess();

  for (const axis of axes) {
    expect(axis.settings.set).toHaveBeenCalledWith('peripheral.id', 43211);
  }
});

test('failure to load information', async () => {
  firmwareWebQueryMock.getVersions.mockRejectedValueOnce(new Error('Offline'));

  const store = TestFirmwareUpgradeApp.testStore;
  mockSingleDevice(store, d => ({ ...d, identity: { ...d.identity, serialNumber: MOCK_SERIAL_NUMBER } }));

  const device = _.sample(selectDevices(store.getState()))!;
  connectionViewMockInstance.props.onSelect(device.key);

  wrapper.getByText('Checking for updates...');
  await waitUntilPass(() => wrapper.getByText(/Error loading online firmware updates: Offline/));
});

test('fails to load information for internal device on public edition', async () => {
  const store = TestFirmwareUpgradeApp.testStore;
  mockSingleDevice(store, d => ({ ...d, identity: { ...d.identity, serialNumber: 4294967295 } }));

  const device = _.sample(selectDevices(store.getState()))!;
  connectionViewMockInstance.props.onSelect(device.key);

  await waitUntilPass(() => wrapper.getByText(/Updating Zaber Launcher Internal devices is not supported by Zaber Launcher Public/));
});

describe('test failure states', () => {
  beforeEach(() => {
    const store = TestFirmwareUpgradeApp.testStore;
    mockSingleDevice(store, d => ({ ...d, identity: { ...d.identity, serialNumber: MOCK_SERIAL_NUMBER } }));
  });

  test('Cannot upgrade FW6 device', async () => {
    const store = TestFirmwareUpgradeApp.testStore;
    mockSingleDevice(
      store,
      d => ({ ...d, identity: { ...d.identity, firmwareVersion: { major: 6, minor: 1, build: 2 } } }),
      MOCK_FW_6_DEVICE_ADDRESS
    );

    const device = _.sample(selectDevices(TestFirmwareUpgradeApp.testStore.getState()))!;

    TestFirmwareUpgradeApp.testStore.dispatch(connectionManagerActions.devicesLoaded(
      extractConnectionKey(device.key),
      [{ ...device, axes: [] }]
    ));
    await waitTick();
    expect(connectionViewMockInstance.props.attentionRequests?.requestAttention).toHaveLength(0);
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getByText(
      'The selected device has version 6.01.2. Zaber Launcher does not ' +
      'support upgrading devices below firmware version 7. Use Zaber Console.'
    ));
  });

  test('Cannot upgrade device with lockstep enabled', async () => {
    const store = TestFirmwareUpgradeApp.testStore;
    mockSingleDeviceWithPeripherals(store);
    lockstepMock.isEnabled.mockResolvedValue(true);

    const device = _.sample(selectDevices(TestFirmwareUpgradeApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);
    await waitUntilPass(() => wrapper.getByText('New version available'));
    fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));

    await waitUntilPass(() => wrapper.getByText(
      'Error: The device has lockstep enabled on axes 1 and 2. Please mechanically decouple and disable the lockstep before upgrading.'),
    EXTENDED_TIMEOUT);
  });

  test('Device fails to receive data', async () => {
    const device = _.sample(selectDevices(TestFirmwareUpgradeApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText(/New Versions Available/));
    fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
    await waitUntilPass(() => fireEvent.click(wrapper.getByText('Clear')));
    await waitUntilPass(() => modal().getByText('Cancel'), EXTENDED_TIMEOUT);
    streamPromise.reject(new Error('Upgrade failed'));
    await waitUntilPass(() => modal().getByText('The upgrade failed. Your device has been reset to its former version.'), EXTENDED_TIMEOUT);
  });

  test('Device throws error while restarting', async () => {
    errorDuringGenericPolling = 'Test that generic errors cause a failure';
    const device = _.sample(selectDevices(TestFirmwareUpgradeApp.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(device.key);

    await waitUntilPass(() => wrapper.getByText(/New Versions Available/));
    fireEvent.click(wrapper.getByTitle('Install Firmware 7.23'));
    await waitUntilPass(() => fireEvent.click(wrapper.getByText('Clear')));
    await waitUntilPass(() => modal().getByText('Cancel'), EXTENDED_TIMEOUT);
    streamPromise.resolve('done');
    await waitUntilPass(() => modal().getByText('The upgrade failed. Your device has been reset to its former version.'), EXTENDED_TIMEOUT);
  });
});
