import type { AnyAction } from 'redux';
import type { SagaIterator } from 'redux-saga';
import { all, apply, call, put, take, takeEvery, takeLatest, select, SagaReturnType as SRT, fork, race, delay } from 'redux-saga/effects';
import type { Observable } from 'rxjs';
import { ascii, RequestTimeoutException } from '@zaber/motion';
import { Axis, Device } from '@zaber/motion/ascii';

import { getContainer } from '../container';
import {
  EntityKey, EntityKeyType, getAxisNumber, getConnectionId, getDeviceAddress, getEntityType, getRouterUrl, tryExtractConnectionKey,
  tryExtractDeviceKey,
} from '../keys';
import { MessageRoutersService } from '../message_router';
import type { MonitorMessage } from '../app_components';
import { Action, makeString, RT, rxToEventChannel, SagaIter, throwUnexpectedError } from '../utils';
import { getAxis, getConnection, getDevice, selectConnections, selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import { environment } from '../environment';

import { parseMessage } from './MessageParser';
import {
  selectCommandList,
  selectCommandLists,
  selectConnection,
  selectConnectionState,
  selectCommandBatch,
  selectSelectedKey
} from './selectors';
import { ActionTypes, ActionsToPayloads, actions } from './actions';
import { ConnectionStates, CommandBatchSettings } from './types';
import { generateCommandList } from './GenerateCommandList';
import { CommandWithMeasurements, getCommandWithMeasurements } from './utils';


export function* terminalSaga(): SagaIterator {
  yield all([
    fork(selectEntity),
    takeLatest([ActionTypes.MONITOR_CONNECTION, ActionTypes.WILL_UNMOUNT], monitorMessages),
    takeEvery(ActionTypes.SEND_COMMAND, sendCommand),
    takeLatest(ActionTypes.COMMAND_BATCH_EXECUTE, commandBatchExecute),
    takeEvery(ActionTypes.GENERATE_COMMAND_LIST, commandList),
  ]);
}

function* selectEntity(): SagaIterator {
  while (true) {
    const lastKey: ReturnType<typeof selectSelectedKey> = yield select(selectSelectedKey);
    const { payload: { key } }: Action<ActionsToPayloads[ActionTypes.SELECT_ENTITY]> = yield take(ActionTypes.SELECT_ENTITY);
    if (key !== lastKey) {
      const commandLists: RT<typeof selectCommandLists> = yield select(selectCommandLists);
      if (key != null && commandLists[key] == null) {
        yield put(actions.generateCommandList(key));
      }

      if (tryExtractConnectionKey(key) !== tryExtractConnectionKey(lastKey)) {
        yield put(actions.monitorConnection());
      }
    }
  }
}

function* monitorMessages(action: AnyAction): SagaIterator {
  if (action.type === ActionTypes.WILL_UNMOUNT) {
    return;
  }
  const connection: ReturnType<typeof selectConnection> = yield select(selectConnection);
  if (connection == null) {
    return;
  }

  yield put(actions.setConnectionState(connection.key, ConnectionStates.Connecting));

  try {
    const messageRouterService = getContainer().get(MessageRoutersService);
    const routerConnection: SRT<typeof messageRouterService.getConnection> = yield apply(
      messageRouterService,
      messageRouterService.getConnection,
      [getRouterUrl(connection.key)]
    );

    const monitorConnection = routerConnection.createMonitorConnection(getConnectionId(connection.key));
    try {
      yield call([monitorConnection, monitorConnection.start]);
      yield put(actions.setConnectionState(connection.key, ConnectionStates.Connected));

      yield call(monitorConnectionMessages, connection.key, monitorConnection.monitorMessages);
    } finally {
      yield call([monitorConnection, monitorConnection.stop]);
    }
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.appendToHistory(connection.key, [parseMessage(err.toString(), true, connection.key)]));
  } finally {
    yield put(actions.setConnectionState(connection.key, ConnectionStates.Ended));
  }
}

export function* prepareCommand(
  asciiDeviceOrAxis: Device | Axis,
  command: string,
  commandWithMeasurements: CommandWithMeasurements,
): SagaIterator<string> {
  try {
    const command: RT<typeof asciiDeviceOrAxis.prepareCommand> =  yield call(
      [asciiDeviceOrAxis, asciiDeviceOrAxis.prepareCommand],
      commandWithMeasurements.command,
      ...commandWithMeasurements.measurements
    );
    return command;
  } catch (err) {
    throw new Error(`Unit conversion failed for '${command}': ${makeString(err)}`);
  }
}

function* sendCommandAxisOrDevice(
  asciiDeviceOrAxis: Device | Axis,
  command: string,
  commandWithMeasurements: CommandWithMeasurements,
): SagaIter {
  if (commandWithMeasurements.measurements.length === 0) {
    yield asciiDeviceOrAxis.genericCommandNoResponse(commandWithMeasurements.command);
  } else {
    const preparedCommand: RT<typeof prepareCommand> = yield call(prepareCommand,
      asciiDeviceOrAxis, command, commandWithMeasurements);
    yield asciiDeviceOrAxis.genericCommandNoResponse(preparedCommand);
  }
}

const regexCommandDevice = /^((?<axis>\d+)\s)?(?<data>.*)$/;
const regexCommandConnection = /^\/?((?<device>\d+)\s)?((?<axis>\d+)\s)?(?<data>.*)$/;

function* sendCommand({ payload: { command } }: Action<ActionsToPayloads[ActionTypes.SEND_COMMAND]>): SagaIter {
  const selectedKey: RT<typeof selectSelectedKey> = yield select(selectSelectedKey);
  const connection: RT<typeof selectConnection> = yield select(selectConnection);
  const connectionState: RT<typeof selectConnectionState> = yield select(selectConnectionState);
  if (connectionState !== ConnectionStates.Connected || connection == null || selectedKey == null) {
    return;
  }

  const commandList: RT<typeof selectCommandList> = yield select(selectCommandList);
  const commandWithMeasurements = getCommandWithMeasurements(commandList, command);

  try {
    if (getEntityType(selectedKey) === EntityKeyType.AXIS) {
      const axis: RT<typeof getAxis> = yield call(getAxis, selectedKey);
      yield sendCommandAxisOrDevice(axis, command, commandWithMeasurements);
    } else if (getEntityType(selectedKey) === EntityKeyType.DEVICE) {
      const device: RT<typeof getDevice> = yield call(getDevice, selectedKey);
      const parsedCommand = command.match(regexCommandDevice);
      if (parsedCommand?.groups?.data != null && parsedCommand.groups.axis != null) {
        const axis = parseInt(parsedCommand.groups.axis, 10);
        yield device.genericCommandNoResponse(parsedCommand.groups.data, { axis });
      } else {
        yield sendCommandAxisOrDevice(device, command, commandWithMeasurements);
      }
    } else {
      const messageRouterService = getContainer().get(MessageRoutersService);
      const terminalRoutedConnection: SRT<typeof messageRouterService.getRoutedConnection> = yield apply(
        messageRouterService,
        messageRouterService.getRoutedConnection,
        [getRouterUrl(connection.key), getConnectionId(connection.key)]
      );
      const asciiConnection = terminalRoutedConnection.connection;
      const parsedCommand = command.match(regexCommandConnection);
      if (parsedCommand?.groups?.data != null) {
        if (parsedCommand.groups.device != null) {
          const device = parseInt(parsedCommand.groups.device, 10);
          if (parsedCommand.groups.axis != null) {
            const axis = parseInt(parsedCommand.groups.axis, 10);
            yield asciiConnection.genericCommandNoResponse(parsedCommand.groups.data, { device, axis });
          } else {
            yield asciiConnection.genericCommandNoResponse(parsedCommand.groups.data, { device });
          }
        } else {
          yield asciiConnection.genericCommandNoResponse(parsedCommand.groups.data);
        }
      }
    }
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.appendToHistory(connection.key, [parseMessage(err.toString(), true, connection.key)]));
  } finally {
    yield put(actions.setCommandInput(''));
  }
}

function* monitorConnectionMessages(connectionKey: EntityKey, observable: Observable<MonitorMessage[]>): SagaIterator {
  const channel = rxToEventChannel(observable);
  try {
    while (true) {
      const messages: MonitorMessage[] = yield take(channel);
      const historyItems = messages.map(({ message, source }) =>
        parseMessage(message, !source.includes('other'), connectionKey));
      yield put(actions.appendToHistory(connectionKey, historyItems));
    }
  } finally {
    channel.close();
  }
}

function* commandBatchExecute(): SagaIter {
  const selected: ReturnType<typeof selectSelectedKey> = yield select(selectSelectedKey);
  if (selected == null) { return null }
  const commandBatch: ReturnType<typeof selectCommandBatch> = yield select(selectCommandBatch);
  yield race([
    call(function* () {
      try {
        const connection: RT<typeof getConnection> = yield getConnection(selected);

        for (const command of commandBatch.commands) {
          const keepGoing = yield* commandBatchCommand(command, connection, commandBatch.settings, selected);
          if (!keepGoing) {
            break;
          }
        }
        yield put(actions.commandBatchDone());
      } catch (err) {
        throwUnexpectedError(err);
        yield put(actions.commandBatchDone(err.message ?? String(err)));
      }
    }),
    take([ActionTypes.COMMAND_BATCH_CLOSE, ActionTypes.COMMAND_BATCH_STOP]),
  ]);
}

function parseCommand(command: string) {
  // matches everything starting with slash
  const commandMatch = command.match(/^\/\s*(\d+\s+)?(\d+\s+)?(.*)$/);
  if (commandMatch == null) {
    return null;
  }
  const device = commandMatch[1] ? +commandMatch[1] : null;
  const axis = commandMatch[2] ? +commandMatch[2] : null;
  return {
    device,
    axis,
    command: commandMatch[3] ?? '',
  };
}

function* commandBatchCommand(command: string, connection: ascii.Connection, settings: CommandBatchSettings, selected: EntityKey) {
  try {
    let parsed = parseCommand(command);
    if (parsed == null) {
      parsed = {
        device: tryExtractDeviceKey(selected) ? getDeviceAddress(selected) : null,
        axis: getEntityType(selected) === EntityKeyType.AXIS ? getAxisNumber(selected) : null,
        command,
      };
    }

    const options: Parameters<typeof connection.genericCommand>[1] = {
      device: parsed.device ?? undefined,
      axis: parsed.axis ?? undefined,
      checkErrors: false,
    };
    let replies: RT<typeof connection.genericCommandMultiResponse>;
    if (options.device) {
      const reply: RT<typeof connection.genericCommand> = yield connection.genericCommand(parsed.command, options);
      replies = [reply];
    } else {
      replies = yield connection.genericCommandMultiResponse(parsed.command, options);
    }
    replies = replies.filter(reply => reply.messageType === ascii.MessageType.REPLY);

    yield put(actions.commandBatchReply(replies));

    if (settings.waitUntilIdle) {
      yield* waitUntilIdle(replies, connection);
    }

    if (settings.stopOnError && replies.some(reply => reply.replyFlag !== 'OK')) {
      return false;
    }

    return true;
  } catch (err) {
    if (err instanceof RequestTimeoutException) {
      yield put(actions.commandBatchReply([{ replyFlag: 'TIMEOUT' }]));
      return !settings.stopOnError;
    } else {
      throw err;
    }
  }
}

function* waitUntilIdle(replies: ascii.Response[], connection: ascii.Connection) {
  yield all(replies.filter(reply => reply.status !== 'IDLE').map(reply => call(function* () {
    const device = connection.getDevice(reply.deviceAddress);
    const axis = reply.axisNumber ? device.getAxis(reply.axisNumber) : device.allAxes;
    while (yield axis.isBusy()) {
      yield delay(environment.isTest ? 0 : 50);
    }
  })));
}

function* commandList({ payload: { entityKey } }: Action<ActionsToPayloads[ActionTypes.GENERATE_COMMAND_LIST]>)
  : SagaIter {
  const connections: ReturnType<typeof selectConnections> = yield (select(selectConnections));
  const devices: ReturnType<typeof selectIdentifiedDevices> = yield (select(selectIdentifiedDevices));
  const axes: ReturnType<typeof selectIdentifiedAxes> = yield (select(selectIdentifiedAxes));
  const commandList = generateCommandList(connections, devices, axes, entityKey);
  yield put(actions.storeCommandList(entityKey, commandList));
}
