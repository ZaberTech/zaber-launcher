import React, { Component } from 'react';
import { injectable } from 'inversify';
import { Acceleration, CommandFailedException, CommandFailedExceptionData, Length, Units, Velocity } from '@zaber/motion';
import type { SimpleSelect } from '@zaber/react-library';
import { fireEvent, RenderResult, within } from '@testing-library/react';

import {
  AxisMockBase,
  ConnectionMockBase,
  DeviceMockBase,
  MessageRoutersServiceMockBase,
  RouterConnectionMockBase
} from '../../test/mocks/ascii';

declare namespace SelectMock {
  interface Props {
    placeholder: string;
    value: SimpleSelect.Option<number>;
    onChange: (item: unknown) => void;
    options: SimpleSelect.Option<number>[];
    'data-testid': string;
  }
}

export class SelectMock extends Component<SelectMock.Props> {
  render() {
    const { placeholder, options, value, 'data-testid': testid, onChange } = this.props;

    return <input
      placeholder={placeholder}
      onChange={e => onChange(options.find(opt => `${opt.value}` === e.target.value))}
      data-testid={testid}
      value={value?.value}/>;
  }

  static createFilter() { return true }
}


export const chartsMock = jest.fn();

export class EChartsReactMock extends Component {
  render(): React.ReactNode {
    chartsMock(this.props);
    return <div>Chart</div>;
  }

  getEchartsInstance = jest.fn().mockReturnThis();
  getZr = jest.fn().mockReturnThis();
  on = jest.fn().mockReturnThis();
  getDataURL = jest.fn().mockReturnValue('data:image/png;base64,R29waGVyID4gUHl0aG9u');
}


export const makeRejectReason = (data: string): CommandFailedExceptionData => ({
  responseData: data,
  replyFlag: 'RJ',
} as CommandFailedExceptionData);


let axisCallback: (axis: PvtAxisMock) => void;
let deviceCallback: (device: PvtDeviceMock) => void;

export function setAxisCallback(cb: ((axis: PvtAxisMock) => void) | null) {
  axisCallback = cb!;
}

export function setDeviceCallback(cb: ((device: PvtDeviceMock) => void) | null) {
  deviceCallback = cb!;
}

export class PvtAxisMock extends AxisMockBase {
  constructor(id: number, device: PvtDeviceMock) {
    super(id, device);

    if (axisCallback) { axisCallback(this) }
  }

  _settings: Record<string, number> = {
    'motion.accelonly': 205,
    'motion.decelonly': 205,
    'maxspeed': 240000,
    'limit.min': 0,
    'limit.max': 384000,
  };

  _pos: number = 0;
  _isHomed = true;
  _flags: string[] = [];

  settings = {
    get: jest.fn(setting => {
      const value = this._settings[setting];
      if (value !== undefined) {
        return Promise.resolve(value);
      }

      return Promise.reject(new CommandFailedException(`AxisMock has no setting ${setting}`, makeRejectReason('BADCOMMAND')));
    }),
    convertFromNativeUnits: jest.fn((_, value, units) => {
      switch (units) {
        case Length.mm: return value / 1000;
        case Velocity.MILLIMETRES_PER_SECOND: return value / 100;
        case Acceleration.MILLIMETRES_PER_SECOND_SQUARED: return value / 10;
      }

      throw new Error(`Unknown axis units ${units}`);
    }),
    convertToNativeUnits: jest.fn((_, value, units) => {
      switch (units) {
        case Length.mm: return value * 1000;
        case Velocity.MILLIMETRES_PER_SECOND: return value * 100;
        case Acceleration.MILLIMETRES_PER_SECOND_SQUARED: return value * 10;
      }

      throw new Error(`Unknown axis units ${units}`);
    }),
  };

  isHomed = jest.fn(() => this._isHomed);
  stop = jest.fn().mockResolvedValue(undefined);
  home = jest.fn(() => this._isHomed = true);
  genericCommandNoResponse = jest.fn();
  waitUntilIdle = jest.fn();
  moveAbsolute = jest.fn(async (position: number, units: Units) => {
    if (units === Length.cm) {
      this._pos = position * 100;
    } else {
      this._pos = position;
    }
  });

  warnings = {
    getFlags: jest.fn(async () => new Set(this._flags)),
  };
}


export class PvtDeviceMock extends DeviceMockBase<PvtAxisMock> {
  constructor(readonly deviceAddress: number, readonly connection: ConnectionMockBase<PvtAxisMock, DeviceMockBase<PvtAxisMock>>) {
    super(deviceAddress, connection);
    deviceCallback?.(this);
  }

  _flags: string[] = [];
  genericCommand = jest.fn();
  genericCommandMultiResponse = jest.fn();

  warnings = {
    getFlags: jest.fn(async () => new Set(this._flags)),
  };

  settings = {
    getFromAllAxes: jest.fn(),
  };

  pvt = {
    getSequence: jest.fn(() => this._pvt),
    getBuffer: jest.fn((bufferNumber: number) => ({
      getContent: jest.fn(async () => this._pvtBuffers[bufferNumber.toString()] ?? []),
    })),
  };

  getLockstep = jest.fn((_n: number) => this._lockstep);

  _lockstep = {
    stop: jest.fn(),
    moveAbsolute: jest.fn(),
    waitUntilIdle: jest.fn(),
  };

  _pvt = {
    disable: jest.fn(),
    setupLiveComposite: jest.fn(),
    point: jest.fn(),
    cork: jest.fn(),
    uncork: jest.fn(),
    waitUntilIdle: jest.fn(),
    isBusy: jest.fn(),
  };

  _pvtBuffers: _.Dictionary<string[]> = {};
}


export class DeviceMockIntegrated extends PvtDeviceMock {
  isIntegrated = true;
  axisCount = 1;
  _pos = 0;

  stop = jest.fn().mockResolvedValue(undefined);
  moveAbsolute = jest.fn(async (position: number, units: Units) => {
    if (units === Length.cm) {
      this._pos = position * 100;
    } else {
      this._pos = position;
    }
  });
}


export class DeviceMockMultiAxis extends PvtDeviceMock {
  isIntegrated = false;
  axisCount = 2;
}


export class ConnectionMock extends ConnectionMockBase<PvtAxisMock, DeviceMockIntegrated> {
  genericCommandNoResponse = jest.fn().mockResolvedValue(undefined);
}

export class RouterConnectionMock extends RouterConnectionMockBase<PvtAxisMock, DeviceMockIntegrated, ConnectionMock> {
}

@injectable()
export class MessageRoutersServiceMockIntegrated
  extends MessageRoutersServiceMockBase<PvtAxisMock, DeviceMockIntegrated, ConnectionMock, RouterConnectionMock> {
  AxisCtor = PvtAxisMock;
  DeviceCtor = DeviceMockIntegrated;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}


@injectable()
export class MessageRoutersServiceMockMultiAxis
  extends MessageRoutersServiceMockBase<PvtAxisMock, DeviceMockMultiAxis, ConnectionMock, RouterConnectionMock> {
  AxisCtor = PvtAxisMock;
  DeviceCtor = DeviceMockMultiAxis;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}


export function selectPoint(wrapper: RenderResult, index: number) {
  fireEvent.change(wrapper.getByTestId('index-selector'), { target: { value: index } });
}


export function expandPointPropertiesGroup(wrapper: RenderResult, seriesName: string) {
  const header = within(wrapper.getByTestId(`point-header-${seriesName}`));
  const expander = header.getByRole('button');
  expect(expander).toHaveClass('collapsed');
  fireEvent.click(expander);
}
