import React, { HTMLProps } from 'react';
import { Text } from '@zaber/react-library';
import classNames from 'classnames';

import type { AxisInfo, DeviceInfo } from '../types';
import { axisDescription } from '../utils';
import { Thumbnail } from '../../components/Thumbnail';

interface Props extends HTMLProps<HTMLDivElement> {
  axis: AxisInfo;
  device: DeviceInfo;
  displayName?: boolean;
}

export const axisLabel: (axis: AxisInfo) => React.ReactNode = ({ nonIdentifiedInfo, identity, label }) => {
  if (nonIdentifiedInfo?.peripheralId != null) {
    return `Unknown Peripheral ${nonIdentifiedInfo.peripheralId}`;
  } else if (!identity) {
    return 'Unknown Axis';
  } else if (identity.isPeripheral) {
    return label ?? identity.peripheralName;
  } else {
    return label ?? 'Generic Axis';
  }
};

export const AxisHeader: React.FC<Props> = ({ axis, device, className, displayName = true, ...rest }) => {
  const description = axisDescription(axis, device);
  return (
    <div className={classNames('axis-header', className)} {...rest}>
      <div className="address">
        <Text t={Text.Type.H5}>Axis {axis.axisNumber}</Text>
      </div>
      {(axis.identity == null || axis.identity.isPeripheral) && <div className="device-picture">
        <Thumbnail deviceOrPeripheralId={axis.identity?.peripheralId} title={description} altExt="peripheral"/>
      </div>}
      {displayName && <div className="axis-name">
        <Text t={Text.Type.H5}>{axisLabel(axis)}</Text>
      </div>}
    </div>
  );
};
