import React, { useState } from 'react';
import { Button, ButtonPrevious, ButtonRow, Checkbox, Icons, LinkClasses, Text } from '@zaber/react-library';
import { Time } from '@zaber/motion';

import { makeAxisKey } from '../../keys';
import { convertNullable, EditableInputWithUnits, getUnits, measurementOK } from '../../units';
import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import type { ControllerInfo } from '../types';
import { InputWithFixedUnit } from '../../units/InputWithFixedUnit';
import { Graph } from '../components/Graph';
import { POLL_RETENTION_SEC } from '../constants';
import { useUnitInfo } from '../../units/hooks';

import type { AutoTuneSettings, AutoTuneTest } from './types';

function calculatePID(tune: AutoTuneTest, smooth: boolean): { kp: number; ki: number; kd: number } {
  if (smooth) { // Tune the controller based on smooth (Tyreus Luyben method) tuning
    return {
      kp: tune.criticalGain / 2.2,
      ki: tune.criticalGain * tune.criticalFrequency / 4.84,
      kd: tune.criticalGain / (tune.criticalFrequency * 2.2 * 6.3),
    };
  } else { // Tune the controller based on stiff (Ziegler Nicholes method) tuning
    return {
      kp: 0.6 * tune.criticalGain,
      ki: 1.2 * tune.criticalGain * tune.criticalFrequency,
      kd: 0.075 * tune.criticalGain / tune.criticalFrequency,
    };
  }
}

type AutoTuneSetupProps = {
  controller: ControllerInfo;
  tune: AutoTuneTest;
  settings: AutoTuneSettings;
};

export const AutoTuneModalTest: React.FC<AutoTuneSetupProps> = ({ controller, tune, settings }) => {
  const actions = useActions(actionDefinitions);
  const processKey = makeAxisKey(tune.entity, tune.process);
  const [smooth, setSmooth] = useState(true);
  const { kp, ki, kd } = calculatePID(tune, smooth);
  const { 'process.control.setpoint.temperature': setpoint, 'process.control.setpoint.tf': setpointTf } = settings;
  const setpointTfUnitInfo = useUnitInfo({ setting: 'process.control.setpoint.tf', entity: processKey });
  const recommendedSetpointTf = convertNullable(setpointTfUnitInfo, 1 / tune.criticalFrequency, Time.s, getUnits(setpointTf));
  const [tested, setTested] = useState(false);

  return <>
    <Text>
      Run the process with the PID values found in the previous step. Once you are satisfied that the system is behaving properly
      you can save the results to your device.
    </Text>
    <div className="controls-columns">
      <Text t={Text.Type.H3} className="left">Controls</Text>
      <EditableInputWithUnits
        className="left"
        labelContent="Setpoint"
        measure={setpoint}
        mode={setpoint.mode}
        unitFrom={{ setting: 'process.control.setpoint.temperature', entity: processKey }}
        onChange={update => actions.updateProcessSetting(tune.process, 'process.control.setpoint.temperature', update)}
        unitsEditable={false}
      />
      {controller.states[tune.process].on ? (
        <Button
          className="left"
          onClick={() => actions.processPower(tune.process, false)}
        >
          <Icons.Stop/> Stop
        </Button>
      ) : (
        <Button
          title="Test Identified Parameters"
          className="left"
          onClick={measurementOK(setpoint) ?
            () => { actions.autoTuneRunTest(tune, { kp, ki, kd }); setTested(true) } :
            'disabled'
          }
        >
          <Icons.RightNormal/> Test
        </Button>
      )}
      <EditableInputWithUnits
        className="setpoint-tf left"
        labelContent="Setpoint Filter Time Constant"
        measure={setpointTf}
        mode={setpointTf.mode}
        unitFrom={{ setting: 'process.control.setpoint.tf', entity: processKey }}
        onChange={update => actions.updateProcessSetting(tune.process, 'process.control.setpoint.tf', update)}
        message={recommendedSetpointTf != null ? (
          <Text
            className={LinkClasses.Default}
            onClick={() => actions.updateProcessSetting(tune.process, 'process.control.setpoint.tf', {
              mode: 'writing',
              value: recommendedSetpointTf,
              units: Time.SECONDS,
              preEditMeasure: setpointTf,
            })}
          >
            Recommended Value: {recommendedSetpointTf?.toPrecision(4)}s
          </Text>
        ) : (
          <Text>No Recommended Value</Text>
        )}
        hideMessage={settings['process.control.setpoint.tf'].value?.toPrecision(4) === recommendedSetpointTf?.toPrecision(4)}
      />

      <Text t={Text.Type.H3} className="right">PID Parameters</Text>
      <Checkbox
        labelContent="Smooth"
        checked={smooth}
        onChecked={checked => { setTested(false); setSmooth(checked) }}
        className="right"
        disabled={controller.states[tune.process].on}
      />
      <InputWithFixedUnit
        className="right"
        labelContent="Kp"
        value={kp}
        unit="V/˚C"
      />
      <InputWithFixedUnit
        className="right"
        labelContent="Ki"
        value={ki}
        unit="V/˚Cs"
      />
      <InputWithFixedUnit
        className="right"
        labelContent="Kd"
        value={kd}
        unit="Vs/˚C"
      />
    </div>

    <Graph mode={tune.initial.mode} source={tune.initial.source} setpoint={true} readings={tune.readings} duration={POLL_RETENTION_SEC}/>

    <ButtonRow justify="spread">
      <ButtonPrevious
        title="Return To Identify Step"
        onClick={() => actions.autoTuneGoToStep({ ...tune, step: 'identify', run: null })}
      >
        Back
      </ButtonPrevious>
      <Button
        disabled={!tested}
        onClick={() => actions.endAutoTune(tune.entity, tune.process, tune.initial, false)}
      >
        Save
      </Button>
    </ButtonRow>
  </>;
};
