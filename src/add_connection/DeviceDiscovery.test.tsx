import { EventEmitter } from 'events';

import React from 'react';
import { RenderResult, fireEvent, render } from '@testing-library/react';
import type { MDNSAnswer, MDNSResponse } from 'multicast-dns';
import type { DeepPartial } from 'redux';
import { injectable } from 'inversify';
import type { RecursivePartial } from '@zaber/toolbox';

jest.mock('../environment', () => {
  const actual = jest.requireActual('../environment');
  return ({
    ...actual,
    environment: {
      ...actual.environment,
      edition: 'public',
    },
  });
});

let mdnsMock: MdnsMock;
const createMdns = jest.fn(() => new MdnsMock());

class MdnsMock extends EventEmitter {
  constructor() {
    super();
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    mdnsMock = this;
  }

  query = jest.fn();
  destroy = jest.fn();

  response(response: DeepPartial<MDNSResponse>) {
    this.emit('response', response);
  }
}

jest.mock('multicast-dns', () => createMdns);

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore } from '../test';
import { ZaberApi } from '../app_components';

import { AddConnection } from './AddConnection';
import { MDNS_DISCOVERY_INACTIVE_TIMEOUT, MDNS_DISCOVERY_SERVICE_SUFFIX_SINGLE, MDNS_DISCOVERY_SERVICE_SUFFIX_CHAIN } from './types';

@injectable()
class ZaberApiMock {
  getDeviceInfo = jest.fn<Promise<RecursivePartial<ZaberApi.DeviceInfo>>, [number]>().mockResolvedValue({
    device: {
      name: 'ZaberDevice3000'
    },
  });
}

const TestAddConnection = wrapWithNewStore(AddConnection);

let wrapper: RenderResult;

beforeAll(() => {
  jest.useFakeTimers();
});
afterAll(() => {
  jest.useRealTimers();
});

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(ZaberApi).to(ZaberApiMock);

  wrapper = render(<TestAddConnection/>);
});
afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  mdnsMock = null!;
});

function createMockRecord(name: string, port: number, connectionType: 'chain' | 'single'): DeepPartial<MDNSAnswer[]> {
  const dnsName = `${name.toLocaleLowerCase()}.${connectionType === 'chain' ?
    MDNS_DISCOVERY_SERVICE_SUFFIX_CHAIN : MDNS_DISCOVERY_SERVICE_SUFFIX_SINGLE}`;
  return [{
    type: 'SRV',
    name: dnsName,
    data: {
      target: name.toLocaleLowerCase(),
      port,
      priority: 0,
      weight: 0,
    },
  }, {
    type: 'TXT',
    name: dnsName,
    data: ['txtvers=1', 'devid=50411', 'sn=80444', 'v=7.99.12345'].map(str => Buffer.from(str)),
  }];
}

beforeEach(async () => {
  fireEvent.click(wrapper.getByText(/Add New Connection/));
  fireEvent.click(wrapper.getByLabelText(/TCP\/IP/));
  fireEvent.click(wrapper.getByText(/Next/));
});

test('explores clients', async () => {
  await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  mdnsMock.response({
    answers: createMockRecord('somePC', 4567, 'chain'),
    additionals: [],
  });
  await waitUntilPass(() => wrapper.getByText(/somepc/));
});

test('destroys mDNS after timeout', async () => {
  jest.advanceTimersByTime(MDNS_DISCOVERY_INACTIVE_TIMEOUT);
  await waitUntilPass(() => expect(mdnsMock.destroy).toHaveBeenCalled());
});

test('destroys mDNS on unmount', async () => {
  wrapper.unmount();
  await waitUntilPass(() => expect(mdnsMock.destroy).toHaveBeenCalled());
});

test('filters duplicates and other records', async () => {
  await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  mdnsMock.response({
    answers: [
      { type: 'TXT', name: 'something' },
      { type: 'TXT', name: `broken.${MDNS_DISCOVERY_SERVICE_SUFFIX_CHAIN}`, data: [Buffer.from('invalid')] },
      { type: 'AAAA', name: 'something' },
      ...createMockRecord('SomePC', 4567, 'chain'),
      ...createMockRecord('SomePC', 4567, 'chain'),
    ],
    additionals: [],
  });
  mdnsMock.response({
    answers: [
      ...createMockRecord('SomePC', 4567, 'chain'),
      ...createMockRecord('OtherPC', 4567, 'chain'),
    ],
    additionals: [],
  });
  await waitUntilPass(() => wrapper.getByText(/otherpc/));
  expect(wrapper.queryAllByText(/somepc/)).toHaveLength(1);
});

test('combines single and chain records into one record', async () => {
  await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  mdnsMock.response({
    answers: [
      ...createMockRecord('SomePC', 4567, 'chain'),
      ...createMockRecord('SomePC', 1234, 'single'),
    ],
    additionals: [],
  });
  await waitUntilPass(() => wrapper.getByText(/somepc/));
  expect(wrapper.queryAllByText(/somepc/)).toHaveLength(1);
});

test('displays device serial number and device id, then replaces id when name is fetched', async () => {
  await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  mdnsMock.response({
    answers: [
      ...createMockRecord('SomePC', 4567, 'chain'),
      ...createMockRecord('SomePC', 1234, 'single'),
    ],
    additionals: [],
  });
  wrapper.getByText(/80444/);
  await waitUntilPass(() => wrapper.getByText(/50411/));
  await waitUntilPass(() => wrapper.getByText(/ZaberDevice3000/));
  expect(wrapper.queryByText(/50411/)).toBeNull();
});

describe('after selecting a connection', () => {
  beforeEach(async () => {
    await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
    mdnsMock.response({
      answers: [
        ...createMockRecord('SomePC', 1234, 'chain'),
        ...createMockRecord('SomePC', 5678, 'single'),
      ],
      additionals: [],
    });
    fireEvent.click(wrapper.getByText(/Select/));
  });

  test('clicking select opens TCP/IP connection page with correct hostname and port', async () => {
    wrapper.getByLabelText(/Hostname/);
    wrapper.getByText(/Device Chain/);
    wrapper.getByDisplayValue(/somepc/);
    wrapper.getByDisplayValue(/1234/);
    expect(wrapper.getByDisplayValue(/1234/)).toBeDisabled();
  });

  test('changing Connection Type changes the port or hostname accordingly', async () => {
    fireEvent.click(wrapper.getByDisplayValue(/single/));
    expect(wrapper.getByLabelText(/Hostname/)).toHaveValue('');
    expect(wrapper.getByLabelText(/Port/)).toHaveValue('5678');
    expect(wrapper.getByLabelText(/Port/)).toBeDisabled();

    fireEvent.click(wrapper.getByDisplayValue(/custom/));
    expect(wrapper.getByLabelText(/Hostname/)).toHaveValue('');
    expect(wrapper.getByLabelText(/Port/)).toHaveValue('');
    expect(wrapper.getByLabelText(/Port/)).not.toBeDisabled();
  });

  test('Little Device Simulator TCP connection type does not display in public edition', async () => {
    expect(wrapper.queryByDisplayValue(/simulator/)).toBeNull();
  });
});

describe('after first discovery done', () => {
  beforeEach(async () => {
    jest.advanceTimersByTime(MDNS_DISCOVERY_INACTIVE_TIMEOUT);
    await waitUntilPass(() => expect(mdnsMock.destroy).toHaveBeenCalled());
  });

  test('allows to repeat discovery', async () => {
    mdnsMock = null!;
    fireEvent.click(wrapper.getByText(/Discover Again/));
    await waitUntilPass(() => expect(mdnsMock.query).toHaveBeenCalled());
  });

  test('shows error when discovery fails', async () => {
    createMdns.mockImplementation(() => {
      const mdns = new MdnsMock();
      mdns.query.mockImplementationOnce(() => { throw new Error('Cannot query mDNS.') });
      return mdns;
    });

    fireEvent.click(wrapper.getByText(/Discover Again/));

    await waitUntilPass(() => wrapper.getByText(/Cannot query/));
  });
});
