import React, { useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Modal, ProgressBar } from '@zaber/react-library';
import { match, P } from 'ts-pattern';
import moment, { duration } from 'moment';
import _ from 'lodash';

import { useActions } from '../../utils';
import { AppIconsNeutral } from '../../apps';
import { selectMonitorState } from '../selectors';

import { selectAcquisition } from './selectors';
import { actions as actionsDefinition } from './actions';
import { DoneReport, ProgressReport } from './types';

function displayDuration(duration: moment.Duration): string {
  return `${duration.hours()}:${_.padStart(duration.minutes().toString(), 2, '0')}:${_.padStart(duration.seconds().toString(), 2, '0')}`;
}

const InProgress: React.FC<{ progress: ProgressReport }> = ({ progress: { taken, total } }) => {
  const setTicks = useState(0)[1];
  useEffect(() => {
    const interval = setInterval(() => setTicks(ticks => ticks + 1), 1000);
    return () => clearInterval(interval);
  }, []);

  const started = useMemo(() => moment(), []);
  const elapsedTime = moment().diff(started);
  const elapsed = duration(elapsedTime);
  const eta = duration(elapsedTime * total / taken - elapsedTime).add(1, 's');

  if (taken === total) {
    return <div>Finishing...</div>;
  }

  const takenStr = _.padStart(taken.toString(), total.toString().length, '0');
  return <>
    <div>Acquired {takenStr}/{total} images.</div>
    <div>Elapsed time: {displayDuration(elapsed)}</div>
    <div>ETA: {displayDuration(eta)}</div>
    <ProgressBar progress={taken / total}/>
  </>;
};

const Done: React.FC<{ report: DoneReport }> = ({ report: { elapsedTime, total } }) => {
  const elapsed = duration(elapsedTime, 'ms');
  return <>
    <div>Acquisition complete.</div>
    <div>Acquired {total} images in {displayDuration(elapsed)}.</div>
  </>;
};

export const StartStop: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const acquisition = useSelector(selectAcquisition);

  const monitor = useSelector(selectMonitorState);
  const ready = acquisition.state === 'ready' && monitor.info != null && monitor.error == null;
  const dialogOpen = acquisition.state !== 'ready';

  return <>
    <Button disabled={!ready} onClick={actions.start}>Start</Button>
    <Modal
      className="multi-d-progress"
      headerIcon={<AppIconsNeutral.Microscope/>}
      headerText="Multi-D Acquisition"
      isOpen={dialogOpen}
      onRequestClose={actions.cancel}
      buttons={match(acquisition)
        .with({ state: P.union('started', 'inProgress', 'cancelling') }, () =>
          <Button color="grey" onClick={actions.cancel}>
            Stop
          </Button>)
        .with({ state: P.union('done', 'error') }, () =>
          <Button color="red" onClick={actions.reset}>
            Close
          </Button>)
        .with({ state: 'ready' }, () => null)
        .exhaustive()}>
      {match(acquisition)
        .with({ state: 'started' }, () => <>Starting...</>)
        .with({ state: 'inProgress' }, state => <InProgress progress={state.progress}/>)
        .with({ state: 'done' }, state => <Done report={state.report}/>)
        .with({ state: 'error' }, state => <>Error: {state.error}</>)
        .with({ state: 'cancelling' }, () => <>Cancelling...</>)
        .with({ state: 'ready' }, () => null)
        .exhaustive()}
    </Modal>
  </>;
};
