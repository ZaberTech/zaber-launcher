export type {
  State as ConnectionManagerState,
  AxisState as ConnectionManagerAxisState,
  DeviceState as ConnectionManagerDeviceState,
  ConnectionState as ConnectionManagerConnectionState,
  RouterState as ConnectionManagerRouterState,
} from './reducer';
export {
  reducer as connectionManagerReducer,
} from './reducer';
export {
  actions as connectionManagerActions,
  ActionTypes as ConnectionManagerActionTypes,
} from './actions';
export type {
  ActionsToPayloads as ConnectionManagerActionPayloads,
  DevicesLoadedPayload, DevicesLoadedAction,
} from './actions';
export { connectionManagerSaga, queryDevices, queryBinaryDevices, fetchDeviceImages } from './sagas';
export { ConnectionManager } from './ConnectionManager';
export { ConnectionsView } from './connection_view/ConnectionsView';
export type { AttentionRequests } from './connection_view/types';
export { ConnectionTable } from './connection_table';
export { NewRouters } from './components/NewRouters';
export * from './selectors';
export type {
  IdentifiedDeviceState as ConnectionManagerIdentifiedDeviceState,
  IdentifiedAxisState as ConnectionManagerIdentifiedAxisState,
} from './selectors';
export {
  getConnection, getDevice, getAxis, getDeviceOrAxis, getAxisOrOnlyAxis, waitForDeviceIdentification, reloadDevices,
  peripheralLikeAxesOfDevice, takeDevicesLoaded,
} from './devices';
export { IdentificationHooks } from './identification_hooks';
export type {
  AxisInfo, IdentifiedAxisInfo, DeviceInfoWithAxes, IdentifiedDeviceInfo, DeviceInfo, ProductInfo, LockstepGroup,
} from './types';
export { RouterType } from './types';
export {
  parseCloudUrl, makeCloudUrl, isCloudUrl, parseLocalShareUrl, makeLocalShareUrl, connectionName, axisDescription,
  isIdentified, isAxisIdentified, peripheralNameWithLabel, peripheralLabelOrName, deviceNameWithLabel, deviceLabelOrName,
} from './utils';
