import * as Sentry from '@sentry/react';
import _ from 'lodash';
import type { StoreEnhancer } from 'redux';

import { ActionTypes as AddConnectionsActionTypes } from '../add_connection/actions';
import { ActionTypes as CloudActionTypes } from '../cloud/actions';
import { ActionTypes as ConnectionManagerActionType } from '../connection_manager/actions';
import type { DeviceState, AxisState } from '../connection_manager/reducer';
import { ActionTypes as FirmwareUpgradeActionTypes } from '../firmware_upgrade/actions';
import type { State as FirmwareUpgradeState } from '../firmware_upgrade/reducer';
import { ActionTypes as FeedbackActionTypes } from '../feedback/actions';

import type { State } from './reducer';

const removePayloadActions: string[] = [
  AddConnectionsActionTypes.DETECT_DEVICES_DONE,
  CloudActionTypes.CHECK_AUTH_RESULT,
  CloudActionTypes.LOGIN_RESULT,
  CloudActionTypes.UPDATE_LOGIN_DATA,
  ConnectionManagerActionType.DEVICES_LOADED,
  FirmwareUpgradeActionTypes.FIRMWARE_VERSIONS_LOADED,
  FirmwareUpgradeActionTypes.SET_VERSIONS_SKIPPED,
  FirmwareUpgradeActionTypes.BEGIN_FROM_FILE_UPGRADE,
  FeedbackActionTypes.SEND_FEEDBACK,
  FeedbackActionTypes.SET_EMAIL,
];

const removePiiFromDevices = (devices: Record<string, DeviceState>) => _.mapValues(devices, deviceState => ({
  ...deviceState,
  identity: deviceState.identity && {
    ...deviceState.identity,
    serialNumber: null!,
  }
}));
const removePiiFromAxes = (axes: Record<string, AxisState>) => _.mapValues(axes, axisState => ({
  ...axisState,
  serialNumber: null,
}));

const removePiiFromFirmwareUpgradeIdentifiers = (
  connections: FirmwareUpgradeState['connections']) => _.mapValues(connections, connection => ({
  ...connection,
  identifiers: connection.identifiers.map(identifier => ({
    ...identifier,
    SerialNumber: null!,
  }))
}));

export const sentryReduxEnhancer: StoreEnhancer = Sentry.createReduxEnhancer({
  actionTransformer: action => {
    const { type } = action;
    if (typeof type === 'string' && removePayloadActions.includes(type)) {
      return { type };
    }

    return action;
  },
  stateTransformer: (state: State): State => ({
    ...state,
    addConnection: {
      ...state.addConnection,
      connectCloud: null!,
      localShare: null!,
    },
    cloud: {
      ...state.cloud,
      loggedInUser: null!,
      loginData: null!,
      sharingStatus: null!,
    },
    connectionManager: {
      ...state.connectionManager,
      devices: removePiiFromDevices(state.connectionManager.devices),
      axes: removePiiFromAxes(state.connectionManager.axes),
    },
    firmwareUpgrade: {
      ...state.firmwareUpgrade,
      versionsSkipped: null!,
      connections: removePiiFromFirmwareUpgradeIdentifiers(state.firmwareUpgrade.connections),
    },
    feedback: {
      ...state.feedback,
      email: null!,
    },
  })
});
