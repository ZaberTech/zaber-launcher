import React from 'react';
import { useSelector } from 'react-redux';
import { DeepPartial } from 'redux';
import { Button, HeaderCard, NumericInput, SimpleSelect, Text } from '@zaber/react-library';

import { InputWithUnits } from '../../units';
import { POSITION_DIMENSION } from '../types';
import { useActions } from '../../utils';
import { displayMeasurement, getPrecision, MeasurementMath } from '../utils';
import { selectMonitorState, selectPositionWithoutOffset } from '../selectors';
import { Field } from '../components';

import { selectFocus } from './selectors';
import { actions as actionsDefinition } from './actions';
import { MultiDAcqConfig } from './types';

const DIR_LABELS = {
  neg: 'Negative',
  pos: 'Positive',
} as const;

const AUTOFOCUS_LABELS = {
  off: 'Disabled',
  reference: 'Reference Position',
  previous: 'Previous Focused Position',
} as const;

const AUTOFOCUS_OPTIONS = Object.entries(AUTOFOCUS_LABELS)
  .map(([value, label]) => ({ value: value as keyof typeof AUTOFOCUS_LABELS, label }));

const Travel = () => {
  const { reference, spacing, count } = useSelector(selectFocus);

  if (Number.isNaN(reference.value) || Number.isNaN(spacing.value)) {
    return null;
  }

  const { units } = reference;

  const convert = MeasurementMath.toNumber;
  const convertBack = MeasurementMath.fromNumber;

  const negValue = (convert(spacing) * (count.neg ?? 0));
  const posValue = (convert(spacing) * (count.pos ?? 0));
  const fromValue = convert(reference) - negValue;
  const toValue = convert(reference) + posValue;

  const from = convertBack(fromValue, units);
  const to = convertBack(toValue, units);
  const neg = convertBack(negValue, spacing.units);
  const pos = convertBack(posValue, spacing.units);

  return <>
    From: {displayMeasurement(from)} (-{displayMeasurement(neg)})&emsp;
    To: {displayMeasurement(to)} (+{displayMeasurement(pos)})&emsp;
    Number of Positions: {(count.neg ?? 0) + (count.pos ?? 0) + 1}
  </>;
};

type FocusConfig = MultiDAcqConfig['focus'];

export const Focus: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const state = useSelector(selectFocus);
  const { stableInfo } = useSelector(selectMonitorState);

  const { focusAxis: position } = useSelector(selectPositionWithoutOffset);

  const update = (focus: DeepPartial<FocusConfig>) => {
    actions.updateConfig({ focus });
  };

  return <HeaderCard className="focus-dimension"
    header={<Text t={Text.Type.H4}>Focus Dimension</Text>}>
    <div className="row">
      <Field className="reference" label="Reference Position"
        help={<>
          The position used to calculate the final focus axis positions.
          The reference position does not include objective offset.
        </>}>
        <InputWithUnits
          placeholder="Focus Position"
          displayPrecision={getPrecision(state.reference)}
          unitFrom={{ dimension: POSITION_DIMENSION }}
          measure={state.reference}
          onChange={reference => update({ reference })}/>

        <Button disabled={position == null} color="grey"
          onClick={() => update({ reference: position })}>
          Set to Current
        </Button>
      </Field>

      {stableInfo?.autofocus &&
        <Field className="autofocus" label="Autofocus">
          <SimpleSelect options={AUTOFOCUS_OPTIONS} placeholder="Autofocus"
            value={state.autofocus} onValueChange={autofocus => update({ autofocus })}
            portalToBody/>
        </Field>}
    </div>
    <div className="row">
      <Field label="Step Size">
        <InputWithUnits
          placeholder="Focus Step"
          displayPrecision={getPrecision(state.spacing)}
          unitFrom={{ dimension: POSITION_DIMENSION }}
          measure={state.spacing}
          onChange={spacing => update({ spacing })}/>
      </Field>

      {(['neg', 'pos'] as const).map(dir => (
        <Field key={dir} label={`Number of ${DIR_LABELS[dir]} Steps`}>
          <NumericInput
            key={dir}
            placeholder={`${DIR_LABELS[dir]} Focus Step Count`}
            status={(state.count[dir] ?? 0) < 0 ? 'error' : undefined}
            value={state.count[dir]}
            onNumberChange={value => update({ count: { [dir]: Math.round(value ?? Number.NaN) } })}/>
        </Field>
      ))}
    </div>

    <div className="row">
      <Field label="Summary">
        <Travel/>
      </Field>
    </div>
  </HeaderCard>;
};
