
import _ from 'lodash';
import {
  AbsoluteTemperature,
  Acceleration,
  ACElectricCurrent,
  Angle,
  AngularAcceleration,
  AngularVelocity,
  Force,
  ForceConstant,
  Inertia,
  Length,
  RelativeTemperature,
  RotationalInertia,
  Torque,
  TorqueConstant,
  Units,
  Velocity
} from '@zaber/motion';

import type { IdentifiedAxisState, IdentifiedDeviceState, ProductInfo } from '../connection_manager';
import { EntityKey, EntityKeyType, getEntityType, makeAxisKey } from '../keys';
import { convertFromDefaultUnits, convertToDefaultUnits, dimensionsByName, getDimensionName } from '../units';

import type { Channel, Sample, UnitConversionCache } from './types';
import { isIoChannel, isSettingChannel, MATH_DIMENSIONS } from './channels';


// Don't consider these units for automatic scaling.
const UNWANTED_UNITS = new Set<Units>([
  AbsoluteTemperature.DEGREES_FAHRENHEIT,
  AbsoluteTemperature.DEGREES_RANKINE,
  AbsoluteTemperature.KELVINS,
  Acceleration.INCHES_PER_SECOND_SQUARED,
  ACElectricCurrent.AMPERES_RMS,
  Angle.RADIANS,
  AngularVelocity.RADIANS_PER_SECOND,
  AngularAcceleration.RADIANS_PER_SECOND_SQUARED,
  Force.POUNDS_FORCE,
  ForceConstant.POUNDS_FORCE_PER_AMP,
  Inertia.GRAMS,
  Inertia.OUNCES,
  Inertia.POUNDS,
  Length.INCHES,
  RelativeTemperature.DEGREES_FAHRENHEIT,
  RelativeTemperature.DEGREES_RANKINE,
  RelativeTemperature.KELVINS,
  RotationalInertia.GRAM_SQUARE_METRE,
  RotationalInertia.POUND_SQUARE_FEET,
  Torque.OUNCE_FORCE_INCHES,
  Torque.POUND_FORCE_FEET,
  TorqueConstant.POUND_FORCE_FEET_PER_AMP,
  Velocity.INCHES_PER_SECOND,
]);


export const VERTICAL_DIVISIONS = 8;
export const HORIZONTAL_DIVISIONS = 10;
export const MIN_VERTICAL_RANGE = 0.000000001;


export interface DataPoint {
  time: number;
  [key: string]: number;
}


function getProductInfo(
  key: EntityKey | null,
  devices: _.Dictionary<IdentifiedDeviceState>,
  axes: _.Dictionary<IdentifiedAxisState>): ProductInfo | null {
  const keyType = getEntityType(key);
  if (keyType === EntityKeyType.AXIS) {
    return axes[key!]?.product ?? null;
  } else if (keyType === EntityKeyType.DEVICE) {
    return devices[key!]?.product ?? null;
  }

  return null;
}


// Determines how to group channels into charts.
export function groupByDimension(
  channels: Channel[],
  deviceKey: EntityKey | null,
  devices: _.Dictionary<IdentifiedDeviceState>,
  axes: _.Dictionary<IdentifiedAxisState>): Record<string, Channel[]> {
  const result: Record<string, Channel[]> = {};
  const productInfoCache: Record<number, ProductInfo | null> = {};

  for (const channel of channels) {
    let axisProductInfo = channel.axis != 0 ? productInfoCache[channel.axis] : null;
    let controllerProductInfo = productInfoCache[0];
    if (deviceKey) {
      if (!axisProductInfo && channel.axis > 0) {
        axisProductInfo = getProductInfo(makeAxisKey(deviceKey, channel.axis), devices, axes);
        productInfoCache[channel.axis] = axisProductInfo;
      }

      if (!controllerProductInfo) {
        controllerProductInfo = getProductInfo(deviceKey, devices, axes);
        productInfoCache[0] = controllerProductInfo;
      }
    }

    const selectedProductInfo = axisProductInfo ?? controllerProductInfo;

    const settingsByName = _.keyBy(selectedProductInfo?.settings?.rows ?? [], 'name');
    const allConversionTableRows =
      (controllerProductInfo?.conversionTable?.rows ?? []).concat(axisProductInfo?.conversionTable?.rows ?? []);
    const dimensionsByName = _.keyBy(allConversionTableRows, 'dimension_name');
    const dimensionsById = _.keyBy(allConversionTableRows, 'contextual_dimension_id');

    let dimensionId: number | null = null;
    if (isSettingChannel(channel)) {
      dimensionId = settingsByName[channel.name]?.contextual_dimension_id ?? null;
    } else if (isIoChannel(channel)) {
      if (channel.name.startsWith('a')) {
        dimensionId = dimensionsByName.Voltage?.contextual_dimension_id ?? null;
      }
    } else {
      for (const dimensionName of MATH_DIMENSIONS[channel.name]) {
        if (dimensionName in dimensionsByName) {
          dimensionId = dimensionsByName[dimensionName].contextual_dimension_id;
          break;
        }
      }
    }

    const dimensionName = (dimensionId ? dimensionsById[dimensionId]?.dimension_name : null) ?? 'Native';
    result[dimensionName] = (result[dimensionName] ?? []).concat([channel]);
  }

  return result;
}


// First step of auto-scaling: Choose the unit of measure that gets the values closest to the nice range 1-10.
export function autoSelectUnit(channel: Channel,
  deviceProduct: ProductInfo | null,
  axisProduct: ProductInfo | null,
  nativeConversions: UnitConversionCache): Units {
  if (!axisProduct && !deviceProduct) {
    return Units.NATIVE;
  }

  const min = _.min(channel.samples.map(s => s.value)) ?? 0;
  const max = _.max(channel.samples.map(s => s.value)) ?? 1;
  const range = max - min;

  const settingsByName = _.keyBy(axisProduct?.settings?.rows ?? deviceProduct?.settings?.rows ?? [], 'name');
  let dimensionId = settingsByName[channel.name]?.contextual_dimension_id ?? null;
  if (dimensionId === null) {
    // Not a setting name - check math channel IDs.
    const dimensionsByName =
      _.keyBy(axisProduct?.conversionTable?.rows ?? deviceProduct?.conversionTable?.rows ?? [], 'dimension_name');

    for (const dimensionName of MATH_DIMENSIONS[channel.name] ?? []) {
      if (dimensionName in dimensionsByName) {
        dimensionId = dimensionsByName[dimensionName].contextual_dimension_id;
        break;
      }
    }
  }

  let unit: Units = Units.NATIVE;
  if (dimensionId !== null) {
    const axisDimensionsById = _.keyBy(axisProduct?.conversionTable?.rows ?? [], 'contextual_dimension_id');
    const deviceDimensionsById = _.keyBy(deviceProduct?.conversionTable?.rows ?? [], 'contextual_dimension_id');
    const dimensionName = axisDimensionsById[dimensionId]?.dimension_name ?? deviceDimensionsById[dimensionId]?.dimension_name;
    const dimension = dimensionsByName[dimensionName];
    const scale = channel.prescale *
      (nativeConversions.contextualDimensionScales[dimensionId] ?? nativeConversions.baseDimensionScales[dimensionName] ?? 0);
    // If the range is much smaller than the value, prefer larger units to get smaller numbers.
    const peak = Math.max(Math.abs(min), Math.abs(max));
    const rangeOrPeak = Math.max(peak, range);
    let rangeinDefaultUnits = rangeOrPeak * scale;
    // Special case to choose units closest to microstep size when all data is 0.
    rangeinDefaultUnits = rangeinDefaultUnits ? rangeinDefaultUnits : scale;

    let bestRange = range;
    for (const unitDef of dimension.units.filter(u => !UNWANTED_UNITS.has(u.id as Units))) {
      const candidateRange = convertFromDefaultUnits(rangeinDefaultUnits, unitDef.id as Units);
      // Select the non-native unit that gives us a range closest to being within 1-10.
      if ((bestRange >= 1.0 && candidateRange >= 1.0 && candidateRange < bestRange) ||
        (bestRange < 1.0 && candidateRange > bestRange) || (unit === Units.NATIVE)) {
        unit = unitDef.id as Units;
        bestRange = candidateRange;
      }
    }
  }

  return unit;
}


export function convertNativeData(
  data: Sample[],
  prescale: number,
  unit: Units,
  channelName: string,
  unitCache: UnitConversionCache): Sample[] {
  if (unit === Units.NATIVE) {
    return data;
  }

  const contextualDimensionId = unitCache.settingContextualDimensions[channelName];
  if (contextualDimensionId != null && contextualDimensionId in unitCache.contextualDimensionScales) {
    const scale = prescale * unitCache.contextualDimensionScales[contextualDimensionId];
    return data.map(sample => ({
      time: sample.time,
      value: convertFromDefaultUnits(sample.value * scale, unit),
    }));
  }

  const dimensionName = getDimensionName(unit);
  if (dimensionName && dimensionName in unitCache.baseDimensionScales) {
    const scale = prescale * unitCache.baseDimensionScales[dimensionName];
    return data.map(sample => ({
      time: sample.time,
      value: convertFromDefaultUnits(sample.value * scale, unit),
    }));
  }

  return data;
}


// Select which unit to use when grouping lines on the same Y axis scale.
export function selectGroupUnit(units: Units[]): Units {
  if (units.length === 0 || _.some(units, u => u === Units.NATIVE)) {
    return Units.NATIVE;
  }

  return _.maxBy(units, u => convertToDefaultUnits(1, u))!;
}
