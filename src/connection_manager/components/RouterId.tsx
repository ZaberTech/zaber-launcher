import { CopyToClipboard } from '@zaber/react-library';
import React from 'react';

import { RouterType, StoredRouter } from '../types';
import { parseCloudUrl, parseLocalShareUrl } from '../utils';

interface Props {
  router: StoredRouter;
  copyable?: boolean;
}

export function getRouterId(router: StoredRouter) {
  switch (router.type) {
    case RouterType.Cloud:
      return parseCloudUrl(router.url)?.cloudId;
    case RouterType.LocalShare: {
      const parsed = parseLocalShareUrl(router.url);
      return `${parsed?.hostname}:${parsed?.port}`;
    }
    default: return null;
  }
}

export const RouterId: React.FC<Props> = ({ router, copyable }) => {
  const id = getRouterId(router);
  if (id == null) { return null }
  if (copyable) {
    return <CopyToClipboard copyText={id}>{id}</CopyToClipboard>;
  } else {
    return <>{id}</>;
  }
};
