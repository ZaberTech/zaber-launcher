

import type { Units } from '@zaber/motion';

import type { EntityKey } from '../../keys';
import type { MeasurementWithUnit } from '../../units';
import { actionBuilder } from '../../utils';
import type { Moveable } from '../types';

import type { AxisCommand, KeyPressCombo, MoveSinState, PreciseMovementValues } from './types';

export enum ActionTypes {
  ISSUE_COMMAND = 'BASIC_CONTROLS-MOVEMENT-ISSUE_COMMAND',
  COMMAND_COMPLETE = 'BASIC_CONTROLS-MOVEMENT-COMMAND_COMPLETE',
  SET_POS_UNITS = 'BASIC_CONTROLS-MOVEMENT-SET_POS_UNITS',

  EXPAND_PRECISE_MOVEMENT = 'BASIC_CONTROLS-MOVEMENT-EXPAND_PRECISE_MOVEMENT',
  SET_PRECISE_MOVEMENT = 'BASIC_CONTROLS-MOVEMENT-SET_PRECISE_MOVEMENT',
  SET_KEYBOARD_SHORTCUT = 'BASIC_CONTROLS-MOVEMENT-SET_KEYBOARD_SHORTCUT',
  SET_MOVE_SIN_STATE = 'BASIC_CONTROLS-MOVEMENT-SET_MOVE_SIN_STATE',
}

export interface ActionsToPayloads {
  [ActionTypes.ISSUE_COMMAND]: { moveable: Moveable; command: AxisCommand };
  [ActionTypes.COMMAND_COMPLETE]: { axisKey: EntityKey };
  [ActionTypes.SET_POS_UNITS]: { axisKey: EntityKey; units: Units };

  [ActionTypes.EXPAND_PRECISE_MOVEMENT]: { axisKey: EntityKey; expanded: boolean };
  [ActionTypes.SET_PRECISE_MOVEMENT]: { axisKey: EntityKey; movement: keyof PreciseMovementValues; value: MeasurementWithUnit | null };
  [ActionTypes.SET_KEYBOARD_SHORTCUT]: { axisKey: EntityKey; keyPress: KeyPressCombo };

  [ActionTypes.SET_MOVE_SIN_STATE]: { axisKey: EntityKey; moveSinState: Partial<MoveSinState> };
}

type K = keyof ActionsToPayloads;
const buildAction = (type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actionDefinitions = {
  issueCommand: (moveable: Moveable, command: AxisCommand) => buildAction(ActionTypes.ISSUE_COMMAND, { moveable, command }),
  commandComplete: (axisKey: EntityKey) => buildAction(ActionTypes.COMMAND_COMPLETE, { axisKey }),
  setPosUnits: (axisKey: EntityKey, units: Units) => buildAction(ActionTypes.SET_POS_UNITS, { axisKey, units }),

  expandPreciseMovement: (axisKey: EntityKey, expanded: boolean) => buildAction(ActionTypes.EXPAND_PRECISE_MOVEMENT, { axisKey, expanded }),
  setPreciseMovement: (axisKey: EntityKey, movement: keyof PreciseMovementValues, value: MeasurementWithUnit | null) => (
    buildAction(ActionTypes.SET_PRECISE_MOVEMENT, { axisKey, movement, value })
  ),
  setKeyboardShortcut: (axisKey: EntityKey, keyPress: KeyPressCombo) => (
    buildAction(ActionTypes.SET_KEYBOARD_SHORTCUT, { axisKey, keyPress })
  ),

  setMoveSinState: (axisKey: EntityKey, moveSinState: Partial<MoveSinState>) =>
    buildAction(ActionTypes.SET_MOVE_SIN_STATE, { axisKey, moveSinState }),
};
