import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { ButtonRowConfirmCancel, Input, Text } from '@zaber/react-library';
import { useSelector } from 'react-redux';
import { IoPortType } from '@zaber/motion/ascii';

import type { EntityKey } from '../../keys';
import type { IoInfo } from '../types';
import { useActions } from '../../utils';

import { selectDeviceStates } from './selectors';
import { actionDefinitions } from './actions';

type IoSectionProps = {
  onClose: () => void;
  deviceKey: EntityKey;
  info: IoInfo;
};

const convertPort = (portType: string) => {
  switch (portType) {
    case '3': return IoPortType.DIGITAL_INPUT;
    case '4': return IoPortType.DIGITAL_OUTPUT;
    case '1': return IoPortType.ANALOG_INPUT;
    case '2': return IoPortType.ANALOG_OUTPUT;
    default: return IoPortType.DIGITAL_INPUT;
  }
};

export const EditLabel: React.FC<IoSectionProps> = ({ onClose, deviceKey, info }) => {
  const labelsFromStore = useSelector(selectDeviceStates)[deviceKey]?.labels;
  const actions = useActions(actionDefinitions);

  const [localLabels, setLocalLabels] = useState<Record<string, string>>(labelsFromStore ?? {});
  const [newLabels, setNewLabels] = useState<Record<string, string>>({});

  const handleSave = () => {
    Object.keys(newLabels).forEach(key => {
      const [portType, channelNumber] = key.split('-');
      actions.setLabelDevice(
        deviceKey, convertPort(portType), parseInt(channelNumber, 10), localLabels[key], labelsFromStore?.[key] ?? ''
      );
    });
    setNewLabels({});
    onClose();
  };

  const handleCancel = () => {
    setLocalLabels(labelsFromStore ?? {});
    setNewLabels({});
    onClose();
  };

  const handleChange = (portType: IoPortType, channelNumber: number, value: string) => {
    setLocalLabels(prev => ({
      ...prev,
      [`${portType}-${channelNumber}`]: value,
    }));
    setNewLabels(prev => ({
      ...prev,
      [`${portType}-${channelNumber}`]: value,
    }));
  };

  useEffect(() => {
    setLocalLabels(labelsFromStore ?? {});
  }, [labelsFromStore]);

  return <>
    <div><Text e={Text.Emphasis.Regular}>Set label for the device IO. The label may be up to 12 characters long.</Text></div>
    <div className="io">
      <div className="label">
        {info.di > 0 && (<div className="di">
          <Text t={Text.Type.H5}>Digital Inputs</Text>
          {_.range(1, info.di + 1).map(channel => {
            const labelKey = `${IoPortType.DIGITAL_INPUT}-${channel}`;
            const label = localLabels[labelKey] ?? '';
            return <Input clearable key={channel} className="edit-label" labelContent={`di-${channel}`}
              placeholder="Set Label..." maxLength={12} value={label}
              onChange={e => handleChange(IoPortType.DIGITAL_INPUT, channel, e.target.value ?? '')}/>;
          })}
        </div>
        )}
        {info.do > 0 && (<div className="do">
          <Text t={Text.Type.H5}>Digital Outputs</Text>
          {_.range(1, info.do + 1).map(channel => {
            const labelKey = `${IoPortType.DIGITAL_OUTPUT}-${channel}`;
            const label = localLabels[labelKey] ?? '';
            return <Input clearable key={channel} className="edit-label" labelContent={`do-${channel}`}
              placeholder="Set Label..." maxLength={12} value={label}
              onChange={e => handleChange(IoPortType.DIGITAL_OUTPUT, channel, e.target.value ?? '')}/>;
          })}
        </div>
        )}
        {info.ai > 0 && (<div className="ai">
          <Text t={Text.Type.H5}>Analog Inputs</Text>
          {_.range(1, info.ai + 1).map(channel => {
            const labelKey = `${IoPortType.ANALOG_INPUT}-${channel}`;
            const label = localLabels[labelKey] ?? '';

            return <Input clearable key={channel} className="edit-label" labelContent={`ai-${channel}`}
              placeholder="Set Label..." maxLength={12} value={label}
              onChange={e => handleChange(IoPortType.ANALOG_INPUT, channel, e.target.value ?? '')}/>;
          })}
        </div>
        )}
        {info.ao > 0 && (<div className="ao">
          <Text t={Text.Type.H5}>Analog Outputs</Text>
          {_.range(1, info.ao + 1).map(channel => {
            const labelKey = `${IoPortType.ANALOG_OUTPUT}-${channel}`;
            const label = localLabels[labelKey] ?? '';
            return <Input clearable key={channel} className="edit-label" labelContent={`ao-${channel}`}
              placeholder="Set Label..." maxLength={12} value={label}
              onChange={e => handleChange(IoPortType.ANALOG_OUTPUT, channel, e.target.value ?? '')}/>;
          })}
        </div>
        )}
      </div>
    </div>
    <ButtonRowConfirmCancel confirmText="Save" onConfirm={handleSave} onCancel={handleCancel}/>
  </>;
};
