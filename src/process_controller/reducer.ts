
import { ProcessControllerSourceSensor } from '@zaber/motion/product';
import { match, P } from 'ts-pattern';

import type { EntityKey } from '../keys';
import { changeDictionary, createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { AutoTune } from './auto_tune/types';
import { POLL_RETENTION_MS } from './constants';
import { setpointSetting, SettingsMap } from './settings';
import type { ControllerInfo, ProcessInfo, Reading } from './types';

export type State = {
  entity: EntityKey | null;
  /** Info for the controller. Null signifies that the info is still being loaded */
  info: ControllerInfo | 'not-process-controller' | null;
  /** The process to show, or null if no process is selected */
  process: number | null;
  processes: Partial<Record<number, ProcessInfo>>;
  /** The readings to display on the graph. */
  readings: Record<number, Reading[]>;
  error: { title: string; details?: string } | null;
  autoTune: AutoTune | null;
};

const NO_READINGS: State['readings'] = { 1: [], 2: [], 3: [], 4: [] };

const initialState: State = {
  entity: null,
  info: null,
  process: null,
  processes: [],
  readings: NO_READINGS,
  error: null,
  autoTune: null,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const chooseEntity: Reducer<ActionTypes.CHOOSE_ENTITY> = (_, { entity, process }) => ({ ...initialState, entity, process });

const setInputsInfo: Reducer<ActionTypes.SET_CONTROLLER_INFO> = (state, { info, time }) => {
  const updateProcessReadings = (process: number) => (
    updateReadings(state.readings[process], info, state.processes[process], time, POLL_RETENTION_MS)
  );
  return {
    ...state,
    info,
    readings: { 1: updateProcessReadings(1), 2: updateProcessReadings(2), 3: updateProcessReadings(3), 4: updateProcessReadings(4) }
  };
};

const detectNonPcEntity: Reducer<ActionTypes.DETECT_NON_PC_ENTITY> = state => ({ ...state, info: 'not-process-controller' });

const setError: Reducer<ActionTypes.SET_APP_ERROR> = (state, { title, details }) => ({ ...state, error: { title, details } });

const chooseProcess: Reducer<ActionTypes.CHOOSE_PROCESS> = (state, { process }) => ({ ...state, process, error: null });

const setProcess: Reducer<ActionTypes.SET_PROCESS> = (state, { process, info }) => ({
  ...state,
  processes: {
    ...state.processes, [process]: {
      ...info,
      settings: { ...state.processes[process]?.settings, ...info.settings },
    }
  },
});

const processPower: Reducer<ActionTypes.PROCESS_POWER> = (state, { process }) => {
  if (state.info == null || state.info === 'not-process-controller') { return state }
  return {
    ...state,
    info: { ...state.info, states: changeDictionary(state.info.states, `${process}`, state => ({ ...state, reading: true })) }
  };
};

const updateProcessSetting: Reducer<ActionTypes.UPDATE_PROCESS_SETTING> = (state, { process, setting, update }) => {
  const prevProcess = state.processes[process];
  if (prevProcess == null) { return state }
  const settings: SettingsMap = { ...prevProcess.settings, [setting]: update };
  const processes: State['processes'] = { ...state.processes, [process]: { ...prevProcess, settings } };
  return { ...state, processes };
};

const updateMode: Reducer<ActionTypes.UPDATE_MODE> = (state, { process, mode }) => {
  const readings: State['readings'] = { ...state.readings, [process]: [] };
  const prevProcess = state.processes[process];
  const processes: State['processes'] = { ...state.processes, [process]: prevProcess && { ...prevProcess, mode } };
  return { ...state, processes, readings };
};

const updateSource: Reducer<ActionTypes.UPDATE_SOURCE> = (state, { process, source }) => {
  const readings: State['readings'] = { ...state.readings, [process]: [] };
  const prevProcess = state.processes[process];
  const processes: State['processes'] = { ...state.processes, [process]: prevProcess && { ...prevProcess, source } };
  return { ...state, processes, readings };
};

const setProcessError: Reducer<ActionTypes.SET_PROCESS_ERROR> = (state, update) => {
  const prevProcess = state.processes[update.process];
  if (prevProcess == null) { return state }
  const process: State['processes'][number] = match(update)
    .with({ title: P.string }, ({ title, message }) => ({ ...prevProcess, error: { title, message } }))
    .otherwise(() => ({ ...prevProcess, error: null }));
  return { ...state, processes: { ...state.processes, [update.process]: process } };
};

const bridge: Reducer<ActionTypes.BRIDGE_PROCESSES> = (state, { process, bridge }) => {
  if (state.info == null || state.info === 'not-process-controller') { return state }
  const bridges = {
    ...state.info.bridges,
    [process]: { on: bridge, reading: true },
    [process + 1]: { on: bridge, reading: true },
  };
  return {
    ...state,
    process: state.process === process + 1 ? process : state.process,
    info: { ...state.info, bridges }
  };
};

/**
 * Adds a new Reading to a list of readings and removes old readings
 * @param prevReadings The old list of readings
 * @param controllerInfo The information about the controller's current state
 * @param process The process number this is a reading of
 * @param time The time to attach to this reading
 * @param retention How long into the past readings should be retained (if undefined, forever)
 * @returns The new list of readings
 */
const updateReadings = (
  prevReadings: Reading[],
  controllerInfo: State['info'],
  processInfo: State['processes'][number],
  time: number,
  retention?: number
): Reading[] => {
  if (!(controllerInfo instanceof Object) || processInfo == null) {
    return prevReadings;
  }
  const measured = match(processInfo.source)
    .with({ sensor: ProcessControllerSourceSensor.ANALOG_INPUT }, ({ port }) => controllerInfo.analogIns[port].value)
    .with({ sensor: ProcessControllerSourceSensor.THERMISTOR }, ({ port }) => controllerInfo.temperatures[port].value)
    .run();
  const output = controllerInfo.voltsOut[processInfo.number];
  const setpoint = match(processInfo.settings[setpointSetting(processInfo.source)])
    .with({ mode: P.union('editing', 'writing')  }, ({ preEditMeasure }) => preEditMeasure.value ?? 0)
    .with({ mode: P.string }, ({ value }) => value ?? 0)
    .otherwise(() => 0);
  return [
    // Tosses reads out once they're out of the retention window for 2 seconds so data is drawn all the way off the graph
    ...prevReadings.filter(reading => retention == null || reading.time > time - retention - 2000),
    { time, measured, output, setpoint },
  ];
};

const beginAutoTune: Reducer<ActionTypes.BEGIN_AUTO_TUNE> = (state, { entity, process }) => ({
  ...state,
  autoTune: { step: 'begin', entity, process, readings: [] }
});

const updateAutoTune: Reducer<ActionTypes.UPDATE_AUTO_TUNE> = (state, { time, measured, voltage, on }) => {
  const tune = state.autoTune;
  if (tune == null) { return state }
  const retentionTime = tune.step === 'identify' ? undefined : POLL_RETENTION_MS;

  const processInfo = state.processes[tune.process];
  const controllerInfo = match(state.info)
    .with({ states: P._ }, ({ states, analogIns, temperatures, voltsOut, ...info }): ControllerInfo => ({
      states: { ...states, [tune.process]: { on, reading: false } },
      analogIns: match(processInfo?.source)
        .with({ sensor: ProcessControllerSourceSensor.ANALOG_INPUT }, ({ port }) => ({ ...analogIns, [port]: measured }))
        .otherwise(() => analogIns),
      temperatures: match(processInfo?.source)
        .with({ sensor: ProcessControllerSourceSensor.THERMISTOR }, ({ port }) => ({ ...temperatures, [port]: measured }))
        .otherwise(() => temperatures),
      voltsOut: { ...voltsOut, [tune.process]: voltage },
      ...info
    }))
    .otherwise(info => info);

  return {
    ...state,
    info: controllerInfo,
    autoTune: { ...tune, readings: updateReadings(tune.readings, controllerInfo, processInfo, time, retentionTime) },
  };
};

const autoTuneError: Reducer<ActionTypes.AUTO_TUNE_ERROR> = (state, { error }) => ({
  ...state,
  autoTune: state.autoTune && { ...state.autoTune, error, run: null },
});

const autoTuneGoToStep: Reducer<ActionTypes.AUTO_TUNE_GO_TO_STEP> = (state, { tune }) => ({
  ...state,
  autoTune: { ...tune, step: 'transition' },
});

const autoTuneStepReady: Reducer<ActionTypes.AUTO_TUNE_GO_TO_STEP> = (state, { tune }) => ({
  ...state,
  autoTune: { ...tune, readings: state.autoTune?.readings ?? tune.readings },
});

const autoTuneRunIdentification: Reducer<ActionTypes.AUTO_TUNE_RUN_IDENTIFICATION> = (state, { tune, hysteresis }) => ({
  ...state,
  autoTune: {
    ...tune,
    run: { hysteresis, progress: 0, voltage: 0, message: 'Commencing system identification' },
    readings: [],
    error: undefined,
  },
});

const autoTuneSetIdentifyProgress: Reducer<ActionTypes.AUTO_TUNE_SET_IDENTIFY_PROGRESS> = (state, { clearData, ...update }) => {
  if (state.autoTune?.step !== 'identify' || state.autoTune.run === null) {
    return state;
  }
  return {
    ...state,
    autoTune: {
      ...state.autoTune,
      run: { ...state.autoTune.run, ...update },
      readings: clearData ? [] : state.autoTune.readings
    },
  };
};

const autoTuneCancelIdentification: Reducer<ActionTypes.AUTO_TUNE_CANCEL_IDENTIFICATION> = state => {
  const autoTune = state.autoTune;
  if (autoTune == null || autoTune.step !== 'identify') {
    return state;
  }
  return {
    ...state,
    autoTune: {
      ...autoTune,
      run: null,
    }
  };
};

const autoTuneCompleteIdentification: Reducer<ActionTypes.AUTO_TUNE_COMPLETE_IDENTIFICATION> = (state, identity) => {
  const autoTune = state.autoTune;
  if (autoTune == null || autoTune.step !== 'identify') {
    return state;
  }
  const { criticalFrequency, criticalGain } = identity;
  return {
    ...state,
    autoTune: {
      ...autoTune,
      criticalGain,
      criticalFrequency,
      run: null,
      complete: true,
      error: undefined,
    }
  };
};

const AutoTuneRunTest: Reducer<ActionTypes.AUTO_TUNE_RUN_TEST> = (state, { tune }) => ({
  ...state,
  autoTune: { ...tune, error: undefined }
});

const endAutoTune: Reducer<ActionTypes.END_AUTO_TUNE> = state => ({
  ...state,
  readings: NO_READINGS,
  autoTune: null,
});

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.CHOOSE_ENTITY]: chooseEntity,
  [ActionTypes.SET_CONTROLLER_INFO]: setInputsInfo,
  [ActionTypes.DETECT_NON_PC_ENTITY]: detectNonPcEntity,
  [ActionTypes.SET_APP_ERROR]: setError,

  [ActionTypes.CHOOSE_PROCESS]: chooseProcess,
  [ActionTypes.SET_PROCESS]: setProcess,
  [ActionTypes.PROCESS_POWER]: processPower,
  [ActionTypes.UPDATE_PROCESS_SETTING]: updateProcessSetting,
  [ActionTypes.UPDATE_MODE]: updateMode,
  [ActionTypes.UPDATE_SOURCE]: updateSource,
  [ActionTypes.SET_PROCESS_ERROR]: setProcessError,
  [ActionTypes.BRIDGE_PROCESSES]: bridge,

  [ActionTypes.BEGIN_AUTO_TUNE]: beginAutoTune,
  [ActionTypes.UPDATE_AUTO_TUNE]: updateAutoTune,
  [ActionTypes.AUTO_TUNE_ERROR]: autoTuneError,
  [ActionTypes.AUTO_TUNE_GO_TO_STEP]: autoTuneGoToStep,
  [ActionTypes.AUTO_TUNE_STEP_READY]: autoTuneStepReady,
  [ActionTypes.AUTO_TUNE_RUN_IDENTIFICATION]: autoTuneRunIdentification,
  [ActionTypes.AUTO_TUNE_SET_IDENTIFY_PROGRESS]: autoTuneSetIdentifyProgress,
  [ActionTypes.AUTO_TUNE_CANCEL_IDENTIFICATION]: autoTuneCancelIdentification,
  [ActionTypes.AUTO_TUNE_COMPLETE_IDENTIFICATION]: autoTuneCompleteIdentification,
  [ActionTypes.AUTO_TUNE_RUN_TEST]: AutoTuneRunTest,
  [ActionTypes.END_AUTO_TUNE]: endAutoTune,
}, initialState);
