import { Loader, Text } from '@zaber/react-library';
import React from 'react';

export const Loading: React.FC<{ type: 'row' | 'screen' }> = ({ type }) => (
  <div className={`loading-${type}`}>
    <Loader size={type === 'screen' ? 'large' : 'medium'}/>
    <Text>Connecting...</Text>
  </div>);
