/** Key to a boolean value representing if third party setup was completed and successfully applied */
export const SETUP_COMPLETE_KEY = 'zaber.3pp.setup';
/** Key to the steps per rotation of this device's motor */
export const STEPS_PER_ROTATION_KEY = 'zaber.3pp.steps';
/** Key to the rated revolutions per minute of this device's motor */
export const RPM_KEY = 'zaber.3pp.rpm';
/** TODO: description of what this param is */
export const HOLDING_TORQUE_KEY = 'zaber.3pp.hold-torque';
/** Key to the rated current of this device's motor */
export const RATED_CURRENT_KEY = 'zaber.3pp.rated';
/** Key to a value representing how aggressively this device can be run (5-100) */
export const RUN_FACTOR_KEY = 'zaber.3pp.run-factor';
/** Key to a value representing how aggressively this device will hold position (5-100) */
export const HOLD_FACTOR_KEY = 'zaber.3pp.hold-factor';
