import { Button, ButtonRow, Flex, Icons, Input, Text } from '@zaber/react-library';
import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { match } from 'ts-pattern';

import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import { ParamTable } from '../components/ParamTable';
import { paramsetNames } from '../names';
import { selectSavedRows } from '../selectors';
import type { ChosenParamset } from '../types';

import type { SavedRow } from './saved_rows';

const Header: React.FC = () => {
  const actions = useActions(actionDefinitions);

  return <div className="saved-row header">
    <Text className="name" t={Text.Type.H5}>Name</Text>
    <Text className="gain" t={Text.Type.H5}>Gain</Text>
    <Text className="raw" t={Text.Type.H5}>Raw Data</Text>
    <div className="pad"></div>
    <div className="apply"></div>
    <div className="delete"><Icons.Trash onClick={() => actions.deleteTable()} data-testid="delete-table" title="Delete All Rows"/></div>
  </div>;
};

const Row: React.FC<ChosenParamset & { row: SavedRow; i: number }> = ({ entity, paramset, row, i }) => {
  const actions = useActions(actionDefinitions);
  const [showParams, setShowParams] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);
  useEffect(() => {
    if (inputRef.current != null) {
      inputRef.current.focus();
      inputRef.current.select();
    }
  }, [inputRef]);

  return <div className="saved-data">
    <div className="saved-row">
      <Input
        ref={inputRef} className="name" value={row.name} data-testid={`row-name-${i}`}
        onValueChange={name => actions.renameRow(i, name)}
      />
      <Text className="gain">{row.params.gain?.toPrecision(4) ?? 'N/A'}</Text>
      <div className="raw">
        <Icons.Information onClick={() => setShowParams(!showParams)} activated={showParams} data-testid={`row-raw-${i}`}/>
      </div>
      <div className="pad"></div>
      <div className="apply">
        <Button color="grey" onClick={() => actions.applySavedRow(entity, paramset, row)}>{`Write to ${paramsetNames[paramset]}`}</Button>
      </div>
      <div className="delete">
        <Icons.Trash onClick={() => actions.deleteRow(i)} data-testid={`delete-row-${i}`} title="Delete Row"/>
      </div>
    </div>
    {showParams && <ParamTable params={row.params}/>}
  </div>;
};

export const ImportExport: React.FC<ChosenParamset> = ({ entity, paramset }) => {
  const actions = useActions(actionDefinitions);
  const rows = useSelector(selectSavedRows);

  return <div className="csv">
    <Text>Save and restore tuning parameters from spreadsheet files.</Text>
    <ButtonRow justify="left">
      <Button onClick={() => actions.importCSV()} color="grey">Open From File</Button>
      <Button onClick={() => actions.exportCsv()} color="grey" disabled={rows.length <= 0}>Save To File</Button>
      <Flex.Spacer/>
    </ButtonRow>
    {match(rows)
      .with([], () => <Text>
        You have no tunings here yet. Open a file to access saved tunings,
        or export the tuning from the currently selected paramset to get started.
      </Text>)
      .otherwise(rows => <div className="saved-rows">
        <Header/>
        {rows.map((row, i) => <Row key={i} {...{ entity, paramset, row, i }}/>)}
      </div>)
    }
    <ButtonRow>
      <Button onClick={() => actions.addRowOfParamset(entity, paramset)}>New Row From {paramsetNames[paramset]}</Button>
    </ButtonRow>
  </div>;
};
