import React from 'react';
import { useSelector } from 'react-redux';
import { CodeSnippetGroup, CodeSnippetTemplates, Text } from '@zaber/react-library';

import type { SerialPortConfig } from '../app_components';
import { ExternalLink } from '../components';
import { selectConnections } from '../connection_manager';
import { extractConnectionKey, getDeviceAddress } from '../keys';

import exampleCpp from './assets/watch_trigger.cpp.template';
import exampleCs from './assets/watch_trigger.cs.template';
import exampleJava from './assets/watch_trigger.java.template';
import exampleJs from './assets/watch_trigger.js.template';
import exampleMatlab from './assets/watch_trigger.matlab.template';
import exampleOctave from './assets/watch_trigger.octave.template';
import examplePy from './assets/watch_trigger.py.template';
import { selectSelectedConnectionIsSerialPort, selectSelectedDeviceKey } from './selectors';

const ZML_LINK = 'https://www.zaber.com/software/docs/motion-library/v3.1.1/ascii/';
const SCOPE_LINK = 'https://www.zaber.com/protocol-manual#topic_command_scope';
const SCOPE_START_LINK = 'https://www.zaber.com/protocol-manual#topic_command_scope_start';
const ZML_SCOPE_LINK = 'https://www.zaber.com/software/docs/motion-library/v3.1.1/ascii/references/python/#oscilloscope_1';

const templates: CodeSnippetTemplates = {
  python: examplePy,
  javascript: exampleJs,
  csharp: exampleCs,
  cpp: exampleCpp,
  java: exampleJava,
  matlab: exampleMatlab,
  octave: exampleOctave,
};


export const ExternallyTriggeredHelp: React.FC = () => {
  const deviceKey = useSelector(selectSelectedDeviceKey);
  const connections = useSelector(selectConnections);
  const isSerialPort = useSelector(selectSelectedConnectionIsSerialPort);

  const templateData = {
    connectionName: 'COM1',
    deviceAddress: 1,
  };

  if (isSerialPort) {
    const connectionKey = extractConnectionKey(deviceKey!);
    templateData.deviceAddress = getDeviceAddress(deviceKey!);
    const connection = connections[connectionKey];
    templateData.connectionName = (connection.config as SerialPortConfig).serialPort;
  }

  if (!isSerialPort) {
    return <div className="embedded-help">
      <p><Text>
        This mode only works with devices on a USB or RS-232 serial connection.
      </Text></p>
    </div>;
  }

  return <div className="embedded-help">
    <p><Text t={Text.Type.Instruction}>Watch Mode</Text></p>
    <p><Text>
      To use this mode, you must configure and start
      the <ExternalLink url={SCOPE_LINK}>Firmware oscilloscope feature</ExternalLink> either
      in another Zaber Launcher app such as the Terminal, or in your own program connected to
      the same device via the <ExternalLink url={ZML_LINK}>Zaber Motion Library</ExternalLink>.
    </Text></p>
    <p><Text>
      Once you press the Start button, the Oscilloscope app will wait for you or your program to send
      the <ExternalLink url={SCOPE_START_LINK}>"scope start"</ExternalLink> command to the selected device,
      then will retrieve and plot the recorded data when it is available from the device.
    </Text></p>
    <p><Text>
      Below is example code for configuring and starting the Firmware data capture. Please
      see the <ExternalLink url={ZML_SCOPE_LINK}> Zaber Motion Library documentation</ExternalLink> for
      more detailed information.
    </Text></p>
    <CodeSnippetGroup templates={templates} templateData={templateData}/>
  </div>;
};
