import fs from 'fs/promises';
import dns from 'dns';

import { mergeWith, observeOn, take } from 'rxjs/operators';
import { asapScheduler, Observable, ReplaySubject, Subject } from 'rxjs';
import { Tools } from '@zaber/motion';
import { makeString, throwUnexpectedError } from '@zaber/toolbox';

import { handleUnexpectedError } from '../errors';
import { TimeMeasuring } from '../time';
import { getContainer } from '../container';
import { parseCloudUrl, parseLocalShareUrl } from '../connection_manager/utils';
import {
  RoutedConnection, RpcIoTcp, RpcClient, RpcNotifications,
  RpcIoIoT, IoTService, MessageRouterApi, Log, Logger, MonitorConnection
} from '../app_components';
import { environment } from '../environment';
import { sleep } from '../utils';

import { LOCAL_ROUTER_URL } from './constants';


export const CHECK_UNRELIABLE_CONNECTION_STATUS = 30 * 1000;
export class MessageRouterConnection {
  private static regexSerialPortOrLocalhost = /^(COM\d+|\/dev\/.+|localhost:\d+)$/;

  private io: RpcIoTcp | RpcIoIoT;
  private ioType: { type: 'local'; path: string } | { type: 'network' | 'cloud' };
  private rpc: RpcClient;
  private notifications: RpcNotifications;
  public readonly api: MessageRouterApi;

  private connecting?: Promise<void>;
  private connected = false;

  private lastStatusCheckTs: number = 0;
  private checkStatusInterval: ReturnType<typeof setInterval> | null = null;

  public readonly errors: Observable<Error>;
  private readonly ownErrors = new Subject<Error>();

  private readonly endedSubject = new ReplaySubject<true>();
  public readonly ended: Observable<true> = this.endedSubject;

  private readonly newRoutedConnectionSubject = new Subject<RoutedConnection>();
  public readonly newRoutedConnection = this.newRoutedConnectionSubject.pipe(observeOn(asapScheduler));

  private connections = new Map<string, RoutedConnection>();

  private log: Logger;
  private time: TimeMeasuring;

  public constructor(
    public readonly url: string,
    private readonly timeout: { local?: number; remote?: number },
  ) {
    const container = getContainer();
    this.log = container.get(Log).getLogger(`MessageRouter(${url})`);
    this.time = container.get(TimeMeasuring);

    let parsedUrl: ReturnType<typeof parseCloudUrl> | ReturnType<typeof parseLocalShareUrl>;
    if (this.url === LOCAL_ROUTER_URL) {
      const routerPath = Tools.getMessageRouterPipePath();
      this.io = new RpcIoTcp({
        path: routerPath,
      });
      this.ioType = { type: 'local', path: routerPath };
    } else if ((parsedUrl = parseLocalShareUrl(url)) !== null) {
      this.io = new RpcIoTcp({
        host: parsedUrl.hostname,
        port: parsedUrl.port,
      });
      this.ioType = { type: 'network' };
    } else if ((parsedUrl = parseCloudUrl(url)) !== null) {
      this.io = new RpcIoIoT(
        getContainer().get(IoTService),
        {
          realm: parsedUrl.realm,
          routerId: parsedUrl.cloudId,
        }
      );
      this.ioType = { type: 'cloud' };
    } else {
      throw new Error(`URL not supported: ${url}`);
    }
    this.rpc = new RpcClient(this.io, this.time);
    this.rpc.handlerErrors.subscribe(handleUnexpectedError);

    this.notifications = new RpcNotifications(this.rpc);

    this.api = new MessageRouterApi(this.rpc);

    this.errors = this.ownErrors.pipe(
      mergeWith(this.rpc.errors, this.io.errors),
      take(1),
    );
  }

  public async ensureConnected(): Promise<void> {
    if (this.connected) {
      return;
    }
    if (this.connecting) {
      await this.connecting;
      return;
    }

    this.connecting = this.connect();
    await this.connecting;
    this.connected = true;
  }

  public async close(): Promise<void> {
    this.rpc.end();
    await this.io.close();

    if (this.checkStatusInterval !== null) {
      clearInterval(this.checkStatusInterval);
      this.checkStatusInterval = null;
    }

    this.endedSubject.next(true);
    this.endedSubject.complete();

    this.newRoutedConnectionSubject.complete();
  }

  private async connect(): Promise<void> {
    try {
      if (this.ioType.type === 'local') {
        await this.waitForLocalRouter();
      }

      dns.setDefaultResultOrder('ipv4first');
      await this.io.connect();
      if (!this.io.isReliable) {
        await this.api.ping();
      }
    } catch (err) {
      throwUnexpectedError(err);

      switch (this.ioType.type) {
        case 'local':
          throw new Error(`Zaber Message Router is not running on this computer. (${err.message})`);
        case 'network':
          throw new Error(`Cannot reach network destination. (${err.message})`);
        case 'cloud':
          throw new Error(`Cannot reach cloud destination. Ensure that both this and the remote computer are online. (${err.message})`);
        default:
          throw err;
      }
    }

    await this.api.ensureCompatibility();

    if (!this.io.isReliable) {
      this.checkStatusInterval = setInterval(() => {
        this.onCheckStatus().catch(() => null);
      }, CHECK_UNRELIABLE_CONNECTION_STATUS);
    }
  }

  private removeConnection(connection: RoutedConnection): void {
    if (this.connections.get(connection.id) !== connection) {
      return;
    }
    this.connections.delete(connection.id);
  }

  private isConnectionIdLocal(id: string): boolean {
    if (this.ioType.type === 'local') {
      return id.match(MessageRouterConnection.regexSerialPortOrLocalhost) !== null;
    } else {
      return false;
    }
  }

  public async getRoutedConnection(id: string): Promise<RoutedConnection> {
    let connection = this.connections.get(id);
    if (!connection) {
      const isLocal = this.isConnectionIdLocal(id);
      const timeout = isLocal ? this.timeout.local : this.timeout.remote;
      connection = new RoutedConnection(id, this.rpc, this.notifications, { defaultTimeout: timeout });

      this.connections.set(id, connection);
      connection.ended.subscribe(() => this.removeConnection(connection!));
      this.newRoutedConnectionSubject.next(connection);
    }

    await connection.ensureOpen();

    return connection;
  }

  public async closeAll(): Promise<void> {
    for (const connection of [...this.connections.values()]) {
      await connection.close();
    }
  }

  private async onCheckStatus(): Promise<void> {
    const isActive = this.io.lastMessageTs > this.lastStatusCheckTs || this.notifications.hasSubscriptions();
    if (!isActive) {
      return;
    }
    try {
      await this.api.ping();
    } catch (err) {
      this.log.warn(`Cannot get status ${this.url}: ${makeString(err)}`);
      throwUnexpectedError(err);
      this.ownErrors.next(err);
    }
    this.lastStatusCheckTs = this.io.lastMessageTs;
  }

  public createMonitorConnection(id: string) {
    return new MonitorConnection(id, this.rpc, this.notifications);
  }

  private async waitForLocalRouter(): Promise<void> {
    if (this.ioType.type !== 'local') {
      throw new Error('Not a local router');
    }

    const routerPath = this.ioType.path;

    const TIMEOUT = 15000;
    const SLEEP = 200;

    const deadline = this.time.now() + TIMEOUT;
    while (this.time.now() < deadline) {
      try {
        await fs.access(routerPath, fs.constants.R_OK);
        return;
      } catch (err) {
        this.log.info(`Waiting for local router ${routerPath}: ${makeString(err)}`);

        if (!environment.isTest) {
          await sleep(SLEEP);
        } else {
          await Promise.resolve();
        }
      }
    }
  }
}
