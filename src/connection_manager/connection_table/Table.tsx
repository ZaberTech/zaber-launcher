import React from 'react';
import { useSelector } from 'react-redux';
import _ from 'lodash';

import { selectRoutersArray } from '../selectors';
import type { RouterState } from '../reducer';

import { Router } from './Router';
import type { PassedProps } from './types';

interface Props extends PassedProps {
  connectionNameEditable?: boolean;
  filter?: (router: RouterState) => boolean;
}

export const ConnectionTable: React.FC<Props> = ({ filter, ...rest }: Props) => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const routers = useSelector(selectRoutersArray).filter(filter ?? _.identity);

  return (
    <div className="connection-table">
      {routers.map(router => <Router key={router.key} router={router} {...rest}/>)}
    </div>
  );
};
