import { actionBuilder } from '../utils';

export enum ActionTypes {
  DISMISS_MESSAGE = 'HELP_DISMISS_MESSAGE',
  MESSAGE_READY = 'HELP_MESSAGE_READY',
  UPDATE_MESSAGE_VISIBILITY = 'HELP_UPDATE_MESSAGE_VISIBILITY',
  UPDATE_FULLSCREEN_NOTICE_VISIBILITY = 'UPDATE_SHOW_FULLSCREEN_NOTICE_VISIBILITY',
}

export interface ActionsToPayloads {
  [ActionTypes.DISMISS_MESSAGE]: { messageId: string };
  [ActionTypes.MESSAGE_READY]: { messageId: string };
  [ActionTypes.UPDATE_MESSAGE_VISIBILITY]: { messageId: string; visible: boolean };
  [ActionTypes.UPDATE_FULLSCREEN_NOTICE_VISIBILITY]: { visibility: boolean };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  dismissMessage: (messageId: string) => buildAction(ActionTypes.DISMISS_MESSAGE, { messageId }),
  messageReady: (messageId: string) => buildAction(ActionTypes.MESSAGE_READY, { messageId }),
  updateMessageVisibility: (messageId: string, visible: boolean) =>
    buildAction(ActionTypes.UPDATE_MESSAGE_VISIBILITY, { messageId, visible }),
  updateFullscreenNoticeVisibility: (visibility: boolean) =>
    buildAction(ActionTypes.UPDATE_FULLSCREEN_NOTICE_VISIBILITY, { visibility }),
};
