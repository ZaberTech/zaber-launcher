import type { Nullable } from '@zaber/toolbox';
import type { FirmwareVersion } from '@zaber/motion';

import type { IdentifiedDeviceState } from '../connection_manager/selectors';
import type { DeviceInfo, IdentifiedDeviceInfo } from '../connection_manager/types';
import { DeviceIdentifiers, Version, compareFirmwareVersions } from '../app_components';
import { isSerialNumberValid } from '../devices';

import type { VersionCategory } from './types';


export function compareVersions(v1: Nullable<Version>, v2: Nullable<Version>): number {
  return compareFirmwareVersions(v1?.parsed, v2?.parsed);
}

export function compareWithDeviceVersion(fwVersion: Version, device: IdentifiedDeviceState | IdentifiedDeviceInfo): number {
  return compareFirmwareVersions(fwVersion.parsed, device.identity.firmwareVersion);
}

export function versionCategory(version: FirmwareVersion): VersionCategory {
  if (version.major === 0 || version.minor >= 99) {
    return 'internal';
  } else if (version.minor >= 95) {
    return 'other';
  } else {
    return 'public';
  }
}

export function isPossibleUpgrade(fwVersion: Version, device: IdentifiedDeviceState | IdentifiedDeviceInfo) {
  const { firmwareVersion: deviceVersion } = device.identity;
  const { parsed: version } = fwVersion;
  return deviceVersion.major === version.major && versionCategory(deviceVersion) === versionCategory(version);
}

export function canUpgrade(device: DeviceInfo) {
  return device.platformId != null && (device.identity?.firmwareVersion.major ?? 0) >= 7;
}

export function isInternal(device: DeviceInfo) {
  return device.identity?.serialNumber == null || !isSerialNumberValid(device.identity?.serialNumber);
}

export function makeIdentifiers(device: IdentifiedDeviceState | IdentifiedDeviceInfo): DeviceIdentifiers {
  return {
    SerialNumber: device.identity.serialNumber,
    IsKeyed: device.isKeyed,
    DeviceNumber: device.address,
    PlatformID: device.platformId,
    DeviceID: device.identity.deviceId,
  };
}
