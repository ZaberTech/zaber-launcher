declare module 'enzyme-adapter-react-16';
declare module 'filewatcher';

declare module 'multicast-dns' {
  export interface MDNSAnswer {
    name: string;
    type: string;
    ttl: number;
    class: string;
    flush: boolean;
    data: Buffer[] | SRVResponseData;
  }

  export interface MDNSResponse {
    answers: MDNSAnswer[];
    additionals: MDNSAnswer[];
  }

  export interface SRVResponseData {
    priority: number;
    weight: number;
    port: number;
    target: string;
  }

  export interface MDNS extends NodeJS.EventEmitter {
    on(event: 'response', callback: (response: MDNSResponse) => void): this;
    on(event: 'error', callback: (error: Error) => void): this;
    query(query: unknown): void;
    destroy(): void;
  }

  export default function (): MDNS;
}

declare module 'net-snmp' {
  export function createSession(host: string, community?: string, options?: {
    timeout?: number;
    version?: number;
  }): Session;

  interface Session {
    on(event: 'error', callback: (error: Error) => void): this;
    on(event: 'close', callback: () => void): this;
    get(oids: string[], callback: (error: Error | null, varbinds: Varbind[]) => void): void;
    close(): void;
  }

  interface Varbind {
    oid: string;
    type: number;
    value: Buffer | string;
  }

  export const Version2c: number;
}

declare module 'extract-svg-path';

declare module '*.jpg' {
  const content: string;
  export default content;
}

declare module '*.png' {
  const content: string;
  export default content;
}

declare module '*.gif' {
  const content: string;
  export default content;
}

declare module '*.mp3' {
  const content: string;
  export default content;
}

declare module '*.svg?url' {
  const content: string;
  export default content;
}

declare module '*.svg?raw' {
  const content: string;
  export default content;
}

declare module '*.svg' {
  const content: new () => React.Component<React.DetailedHTMLProps<React.HTMLAttributes<HTMLOrSVGElement>, HTMLOrSVGElement>>;
  export default content;
}

declare module '*.json' {
  const content: string;
  export default content;
}

declare module '*.yml' {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const content: any;
  export default content;
}

declare module '*.template' {
  const content: string;
  export default content;
}
