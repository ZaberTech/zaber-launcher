export { reducer as peripheralsReducer } from './reducer';
export type { State as PeripheralsState } from './reducer';
export {
  actions as peripheralsActions,
  ActionTypes as PeripheralsActionTypes,
} from './actions';
export type {
  ActionsToPayloads as PeripheralsActionPayloads,
} from './actions';
export { peripheralsSaga } from './sagas';
