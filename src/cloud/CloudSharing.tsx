import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Toggle, Text, Icons, HelpTooltip, CopyToClipboard, Loader } from '@zaber/react-library';

import type { RootState } from '../store';
import { OnlineStatus } from '../components';
import { ConnectionTable, RouterType } from '../connection_manager';
import { LauncherName } from '../preferences';

import { selectSharingStatus, selectSharingActionInProgress, selectSharingError } from './selectors';
import { actions as actionsDefinition } from './actions';

interface Props {
  actions: typeof actionsDefinition;
  status: ReturnType<typeof selectSharingStatus>;
  inProgress: boolean;
  error: ReturnType<typeof selectSharingError>;
}

export const REFRESH_STATUS_INTERVAL = 5000;

class CloudSharingBase extends Component<Props> {
  private refreshInterval?: ReturnType<typeof setInterval>;

  componentDidMount(): void {
    const { actions } = this.props;
    actions.cloudSharingStatus();

    this.refreshInterval = setInterval(() => {
      if (!this.props.inProgress) {
        actions.cloudSharingStatus();
      }
    }, REFRESH_STATUS_INTERVAL);
  }

  componentWillUnmount(): void {
    if (this.refreshInterval != null) {
      clearInterval(this.refreshInterval);
    }
  }

  render(): React.ReactNode {
    const { status, inProgress, error, actions } = this.props;

    return (<div className="cloud-sharing">
      {!!error && <div>Error: {error}</div>}
      {!status && <div>
        <Loader size="small"/>
      </div>}
      {status && <div className="status">
        <Icons.Network/>
        &emsp;
        <Text t={Text.Type.H4}>Share my Devices over Cloud &emsp;</Text>
        <Toggle
          title={`Turn sharing ${status.sharingOn ? 'off' : 'on'}`}
          loading={inProgress} value={status.sharingOn}
          onValueChange={actions.turnCloudSharing}/>
        &ensp;
        <HelpTooltip>
          Allows you to use devices connected to this computer over the cloud.
          The devices are only shared within your account.
        </HelpTooltip>
        <div className="spacer"/>
        <OnlineStatus online={status.isConnected}/>
      </div>}

      <div className="additional-config">
        <LauncherName/>
        {status?.id && <div className="status-value">
          <Text>Cloud ID:&emsp;<CopyToClipboard copyText={status.id}>{status.id}</CopyToClipboard></Text>
          <HelpTooltip>
            Unique identifier of this Zaber Launcher.
            Can be used with Zaber Motion Library to connect to the shared devices over cloud.
          </HelpTooltip>
        </div>}
      </div>

      <Text t={Text.Type.Instruction}>Below are your connections that will be shared over the cloud:</Text>

      <ConnectionTable filter={router => router.type === RouterType.Local}/>
    </div>);
  }
}

export const CloudSharing = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    status: selectSharingStatus(state),
    inProgress: selectSharingActionInProgress(state),
    error: selectSharingError(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(CloudSharingBase);
