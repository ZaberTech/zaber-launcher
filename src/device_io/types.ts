import type { Nullable } from '@zaber/toolbox';

export interface IoInfo {
  digitalInputs: number;
  digitalOutputs: number;
  analogInputs: number;
  analogOutputs: number;
}

export interface IoState {
  digitalInputs: boolean[];
  digitalOutputs: boolean[];
  analogInputs: number[];
  analogOutputs: number[];
}

export const VOLTAGE_DECIMAL_NUM = 3;

export function maybeHasIO(state: Nullable<IoState>): boolean {
  if (state == null) { return true }
  return state.analogInputs.length > 0 ||
    state.digitalInputs.length > 0 ||
    state.analogOutputs.length > 0 ||
    state.digitalOutputs.length > 0;
}
