import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import { SimulationState } from '@zaber/serverless-service-definitions';

import { createContainer, destroyContainer } from '../container';
import { defer, waitTick, waitUntilPass, wrapWithNewStore } from '../test';
import { MessageRoutersService } from '../message_router';
import { connectionManagerActions, RouterType } from '../connection_manager';
import { ZaberApi } from '../app_components';
import { actions as cloudActions } from '../cloud/actions';

import { AddConnection } from './AddConnection';

const addRouterMock = jest.fn(() => ({ type: 'NOTHING', payload: undefined }));
connectionManagerActions.addRouter = addRouterMock;

const TestAddConnection = wrapWithNewStore(AddConnection);

let container: Container;
let wrapper: RenderResult;

class RouterConnectionMock {
}

let routerService: MessageRoutersServiceMock;

@injectable()
class MessageRoutersServiceMock {
  routerMock = new RouterConnectionMock();

  getConnection = jest.fn(async () => this.routerMock);
}

let zaberApi: ZaberApiMock;

@injectable()
class ZaberApiMock {
  virtualDeviceExists = jest.fn<ReturnType<ZaberApi['virtualDeviceExists']>, [string]>()
    .mockImplementation(async cloudId => ({
      cloudId,
      exists: true,
      public: true,
    }));

  getVirtualDeviceChain = jest.fn<ReturnType<ZaberApi['getVirtualDeviceChain']>, [string, boolean]>()
    .mockImplementation(async cloudId => ({
      cloudId,
      name: 'name',
      state: SimulationState.On,
      devices: [{ deviceId: 1 }],
    }));
}

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  routerService = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;
  container.bind<unknown>(ZaberApi).to(ZaberApiMock);
  zaberApi = container.get<unknown>(ZaberApi) as ZaberApiMock;

  wrapper = render(<TestAddConnection/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
  routerService = null!;
  zaberApi = null!;

  addRouterMock.mockClear();
});

beforeEach(async () => {
  fireEvent.click(wrapper.getByText(/Add New Connection/));
  fireEvent.click(wrapper.getByLabelText(/Virtual Device/));
  fireEvent.click(wrapper.getByText(/Next/));

  fireEvent.input(wrapper.getByLabelText('Cloud ID'), { target: { value: 'cloud-id' } });
});

test('cannot create connection when the ID is empty', async () => {
  fireEvent.input(wrapper.getByLabelText('Cloud ID'), { target: { value: '  ' } });
  expect(wrapper.getByText('Create Connection')).toHaveAttribute('disabled');
});

test('adds public instance after successful connection', async () => {
  fireEvent.click(wrapper.getByText('Create Connection'));

  await waitUntilPass(() => expect(connectionManagerActions.addRouter).toHaveBeenCalledWith(
    expect.objectContaining({
      type: RouterType.Cloud,
      url: 'iot://unauthenticated/cloud-id',
    }),
  ));
});

test('adds router with a custom URL', async () => {
  fireEvent.input(wrapper.getByLabelText('Cloud ID'), { target: { value: 'iot://custom/cloud-id' } });

  fireEvent.click(wrapper.getByText('Create Connection'));

  await waitUntilPass(() => expect(connectionManagerActions.addRouter).toHaveBeenCalledWith(
    expect.objectContaining({
      type: RouterType.Cloud,
      url: 'iot://custom/cloud-id',
    }),
  ));
});

describe('logged in', () => {
  beforeEach(() => {
    TestAddConnection.testStore.dispatch(cloudActions.loginResult({
      'custom:identity_id': 'identity',
      'email': '',
      'sub': 'user',
    }));
  });

  test('adds private instance after successful connection', async () => {
    zaberApi.virtualDeviceExists.mockResolvedValueOnce({
      cloudId: 'some-id',
      exists: true,
      public: false,
    });

    fireEvent.click(wrapper.getByText('Create Connection'));

    await waitUntilPass(() => expect(connectionManagerActions.addRouter).toHaveBeenCalledWith(
      expect.objectContaining({
        type: RouterType.Cloud,
        url: 'iot://identity/some-id',
      }),
    ));
  });

  test('adds router with a custom URL', async () => {
    fireEvent.input(wrapper.getByLabelText('Cloud ID'), { target: { value: 'iot:///cloud-id' } });

    fireEvent.click(wrapper.getByText('Create Connection'));

    await waitUntilPass(() => expect(connectionManagerActions.addRouter).toHaveBeenCalledWith(
      expect.objectContaining({
        type: RouterType.Cloud,
        url: 'iot://identity/cloud-id',
      }),
    ));
  });
});

test('displays device does not exist error', async () => {
  zaberApi.virtualDeviceExists.mockResolvedValueOnce({
    cloudId: '',
    exists: false,
    public: false,
  });

  fireEvent.click(wrapper.getByText('Create Connection'));

  await waitUntilPass(() => wrapper.getByText(/does not exist/));
});

test('displays is private instance error', async () => {
  zaberApi.virtualDeviceExists.mockResolvedValueOnce({
    cloudId: '',
    exists: true,
    public: false,
  });

  fireEvent.click(wrapper.getByText('Create Connection'));

  await waitUntilPass(() => wrapper.getByText(/is a private instance/));
});

test('displays connection error', async () => {
  routerService.getConnection.mockRejectedValueOnce(new Error('Cannot connect'));

  fireEvent.click(wrapper.getByText('Create Connection'));

  await waitUntilPass(() => wrapper.getByText(/your computer is online/));
});

test('displays initializing error during creation', async () => {
  zaberApi.getVirtualDeviceChain.mockResolvedValueOnce({
    cloudId: 'some-id',
    name: 'name',
    state: SimulationState.Creating,
    devices: [{ deviceId: 1 }],
  });

  fireEvent.click(wrapper.getByText('Create Connection'));

  await waitUntilPass(() => wrapper.getByText(/Virtual device is still starting/));
});

test('displays initializing error during turning on', async () => {
  zaberApi.getVirtualDeviceChain.mockResolvedValueOnce({
    cloudId: 'some-id',
    name: 'name',
    state: SimulationState.TurningOn,
    devices: [{ deviceId: 1 }],
  });

  fireEvent.click(wrapper.getByText('Create Connection'));

  await waitUntilPass(() => wrapper.getByText(/Virtual device is still starting/));
});

test('cancels connection when dialog is closed or goes back', async () => {
  const deferred = defer<RouterConnectionMock>();
  routerService.getConnection.mockReturnValue(deferred.promise);

  fireEvent.click(wrapper.getByText('Create Connection'));
  fireEvent.click(wrapper.getByText('Back'));

  deferred.resolve(new RouterConnectionMock());
  await waitTick();

  expect(connectionManagerActions.addRouter).not.toHaveBeenCalled();
});
