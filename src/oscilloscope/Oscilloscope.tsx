import _ from 'lodash';
import React, { useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import {
  Button, ButtonRowConfirmCancel, Checkbox, Colors, ContextMenu, HelpTooltip, Icons, MinorMenu, Modal, Text, TextType, Toggle,
  UnorderedList
} from '@zaber/react-library';

import { ZoomPanSet, NoContentMessage, Title } from '../components';
import { ConnectionsView } from '../connection_manager';
import { selectOscilloscope } from '../store';
import { useActions } from '../utils';

import { actions as actionDefinitions } from './actions';
import { ChannelsTab } from './ChannelsTab';
import { Charts, ChartScreenshot } from './Charts';
import { RecordingTabs } from './RecordingTabs';
import {
  selectAllChannels,
  selectCanRepeatCurrentRecording,
  selectCaptureSettings,
  selectSelectedDeviceState,
  selectSelectedRecording,
  selectSelectedRecordingIndex,
  selectStopButtonEnabled
} from './selectors';
import { SettingsTab } from './SettingsTab';
import {
  ControlTabId,
  START_RECORDING_BUTTON_LABEL,
  CAPTURE_SETTINGS_TAB_LABEL,
  SHARE_SETTINGS_LABEL,
  STOP_RECORDING_BUTTON_LABEL,
  REPEAT_RECORDING_BUTTON_LABEL,
  REPLACE_RECORDING_BUTTON_LABEL,
  DELETE_ALL_BUTTON_LABEL,
  Mode,
  DOWNLOAD_BUTTON_LABEL
} from './types';
import { isNewRecTab } from './utils';
import { extractIoChannels, extractSettingsChannels } from './channels';
import { ExternallyTriggeredHelp } from './ExternallyTriggeredHelp';


interface MenuProps {
  onSaveImage: () => void;
}


const Menu: React.FC<MenuProps> = ({ onSaveImage }) => {
  const actions = useActions(actionDefinitions);
  const recordingIndex = useSelector(selectSelectedRecordingIndex);

  return <ContextMenu align="end" position="top" className="control-area-menu" data-testid="control-area-menu">
    <ContextMenu.Item onClick={() => actions.saveRecordingToSpreadsheet(recordingIndex)}
      icon={<Icons.Download title="Save CSV"/>}>
      Save CSV
    </ContextMenu.Item>
    <ContextMenu.Item onClick={onSaveImage} icon={<Icons.Download title="Save Image"/>}>
      Save Image
    </ContextMenu.Item>
  </ContextMenu>;
};


interface AxisLabelProps {
  label: string;
}

const AxisLabel: React.FC<AxisLabelProps> = ({ label }) => {
  const parts = label.split('>').map(s => s.trim());

  return <div className="axis-label">
    {parts.map((str, i) => (<React.Fragment key={i}>
      {(i > 0) && <Icons.ArrowCollapsed/>}
      <Text t={TextType.H5}>{str}</Text>
    </React.Fragment>)
    )}
  </div>;
};

interface ConfirmDeleteAllProps {
  confirm: () => void;
  cancel: () => void;
}

const ConfirmDeleteAllModal: React.FC<ConfirmDeleteAllProps> = ({ confirm, cancel }) => (
  <Modal
    headerIcon={<Icons.Oscilloscope/>}
    headerText="Delete All Tabs"
    className="confirm-delete-all"
    small
    bodyIcon="warning"
    buttons={<ButtonRowConfirmCancel onConfirm={confirm} onCancel={cancel} confirmText="Delete All"/>}
    onRequestClose={cancel}>
    <Text>Are you sure you want to delete all the tabs? This will delete the recorded data and the closed tabs cannot be retrieved.</Text>
  </Modal>
);


type ChartZoomCache = Record<number, ZoomPanSet>;


const MouseHelp = <HelpTooltip title="Mouse Commands">
  <UnorderedList header={<Text t={Text.Type.H4}>Mouse commands for the charts:</Text>}>
    <Text><Text e={Text.Emphasis.Bold}>Shift + mouse wheel on the chart</Text>: zoom in and out at the mouse location</Text>
    <Text><Text e={Text.Emphasis.Bold}>Shift + mouse wheel on the scale bars</Text>: zoom only the X or Y axis</Text>
    <Text><Text e={Text.Emphasis.Bold}>Left-click and draw a rectangle</Text>: zoom to that area</Text>
    <Text><Text e={Text.Emphasis.Bold}>Right-click</Text>: reset the zoom to show all data</Text>
  </UnorderedList></HelpTooltip>;


export const Oscilloscope: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const {
    controlsVisible,
    connectionManagerSelectedKey,
    selectedDeviceKey,
    shareChannelSettings,
    errorMessage,
    activeRecording,
    recordings,
    selectedRecordingIndex,
    unitConversionEnabled,
  } = useSelector(selectOscilloscope);
  const { selectedControlTab } = useSelector(selectSelectedDeviceState);
  const channels = useSelector(selectAllChannels);
  const stopButtonEnabled = useSelector(selectStopButtonEnabled);
  const selectedDeviceState = useSelector(selectSelectedDeviceState);

  const zooms = useRef<ChartZoomCache>({});
  const chartRef = useRef(null);

  const [showDeleteAll, setShowDeleteAll] = useState(false);

  const numSettingsAdded = extractSettingsChannels(channels).length + extractIoChannels(channels).length;
  const selectedRecording = useSelector(selectSelectedRecording);
  const { mode } = useSelector(selectCaptureSettings);
  const isNewRecordingTab = isNewRecTab(selectedRecordingIndex);
  const axisName = selectedRecording?.deviceName;
  const selectedRecordingId = selectedRecording?.id ?? -1;

  const onZoom = (zoom: ZoomPanSet) => {
    zooms.current[selectedRecordingId] = zoom;
  };

  const canRepeat = useSelector(selectCanRepeatCurrentRecording);
  const showUI = activeRecording != null || recordings.length > 0 || selectedDeviceKey;
  const enableDeleteAll = recordings.length > 0 && !isNewRecTab(selectedRecordingIndex);

  const showChannels = !(selectedDeviceState.mode === Mode.EXTERNAL &&
    !_.some((selectedRecording?.channels ?? []), channel => channel.samples.length > 0));

  return <div className="connection-view-and-app">
    <ConnectionsView
      selectable={['device', 'integrated']}
      selected={connectionManagerSelectedKey ?? null}
      onSelect={actions.onDeviceSelected}/>

    <div className="oscilloscope">
      <Title>Oscilloscope</Title>

      {!showUI && <NoContentMessage
        title="Welcome to the Oscilloscope!"
        message="Select a device to begin."/>}

      {showDeleteAll && <ConfirmDeleteAllModal
        confirm={() => {
          actions.deleteAllRecordings();
          setShowDeleteAll(false);
        }}
        cancel={() => setShowDeleteAll(false)}
      />}

      {showUI && <>
        <div className="recording-tab-panel">
          <RecordingTabs/>
          <div className="spacer"/>
          <Button color="clear"
            title="Delete all recordings"
            disabled={!enableDeleteAll}
            onClick={() => setShowDeleteAll(true)}>
            <div><Icons.Trash color={enableDeleteAll ? Colors.darkGrey : Colors.paleGrey}/>{DELETE_ALL_BUTTON_LABEL}</div>
          </Button>
        </div>

        <div className="recording-tab-body">
          <div className="status-bar">
            <AxisLabel label={axisName ?? ''}/>
            <div className="spacer"/>
            <div className="unit-toggle">
              <Text>Native Units</Text>
              <Toggle data-testid="native-toggle"
                value={!unitConversionEnabled}
                onValueChange={val => actions.setUnitConversionEnabled(!val)}/>
            </div>
            {selectedRecording?.actualSampleRate &&
              <Text t={TextType.H5} className="sample-rate">
                {`Sample rate: ${selectedRecording.actualSampleRate.toLocaleString()}Hz`}
              </Text>}
            {!activeRecording && !isNewRecordingTab
              ? <>
                <Menu onSaveImage={() => {
                  const chart = chartRef.current as unknown as ChartScreenshot;
                  chart?.saveToImage();
                }}/>
                {MouseHelp}
              </>
              : <div className="control-area-menu-placeholder"/>}
          </div>

          <Charts key={selectedRecordingId}
            ref={chartRef}
            initialZoom={zooms.current[selectedRecordingId]}
            onZoomChanged={onZoom}/>

          {errorMessage && <Modal
            headerIcon={<Icons.Oscilloscope/>}
            headerText="Device Error"
            small
            bodyIcon="error"
            data-testid="oscilloscope-error"
            buttons={<Button onClick={() => actions.setError(null)}>OK</Button>}
            onRequestClose={() => actions.setError(null)}>
            <Text>{errorMessage}</Text>
          </Modal>}
        </div>

        {!isNewRecordingTab && <Icons.RightNormal className={classNames('control-panel-expander', controlsVisible ? 'hide' : 'show')}
          data-testid="control-panel-expander"
          title={controlsVisible ? 'Hide Controls' : 'Show Controls'}
          onClick={() => actions.setControlPanelVisibility(!controlsVisible)}/>}

        {(controlsVisible || isNewRecordingTab) && <div className="control-panel">
          <div className="tab-header">
            <MinorMenu>
              <div key={1} className={classNames({ active: selectedControlTab === ControlTabId.CHANNELS })} data-testid="channels-tab">
                <MinorMenu.Item onClick={() => actions.selectControlTab(ControlTabId.CHANNELS)}>
                  Channels
                </MinorMenu.Item>
              </div>
              <div key={2} className={classNames({ active: selectedControlTab === ControlTabId.SETTINGS })}>
                <MinorMenu.Item onClick={() => actions.selectControlTab(ControlTabId.SETTINGS)}>
                  {CAPTURE_SETTINGS_TAB_LABEL}
                </MinorMenu.Item>
              </div>
            </MinorMenu>

            <div className="martin-feature">
              {!isNewRecTab(selectedRecordingIndex) && <>
                <Checkbox checked={shareChannelSettings}
                  onChecked={actions.setSharedChannelSettings}
                  labelContent={SHARE_SETTINGS_LABEL}/>
                <HelpTooltip>While checked, the following will be synchronized across all existing recordings:
                  channel color, visibility, Y axis selection and math channel selections.
                </HelpTooltip>
              </>}
            </div>

            {activeRecording &&
              <Button className="stop-button"
                data-testid="stop-button"
                disabled={!stopButtonEnabled}
                onClick={() => actions.stopRecording()}>{STOP_RECORDING_BUTTON_LABEL}</Button>}
            {!activeRecording && isNewRecordingTab && mode !== Mode.DOWNLOAD && <Button
              className="record-button"
              data-testid="record-button"
              disabled={!selectedDeviceKey || numSettingsAdded === 0}
              onClick={actions.startRecording}>{START_RECORDING_BUTTON_LABEL}</Button>}
            {!activeRecording && isNewRecordingTab && mode === Mode.DOWNLOAD && <Button
              className="download-button"
              data-testid="download-button"
              disabled={!selectedDeviceKey}
              onClick={() => actions.downloadExistingData()}>{DOWNLOAD_BUTTON_LABEL}</Button>}
            {!activeRecording && !isNewRecordingTab && <>
              <Button
                className="record-button"
                data-testid="record-button"
                disabled={!canRepeat}
                onClick={() => actions.repeatRecording(selectedRecordingIndex)}>
                {REPEAT_RECORDING_BUTTON_LABEL}
              </Button>
              <Button
                className="replace-button"
                data-testid="replace-button"
                color="grey"
                disabled={!canRepeat}
                onClick={() => actions.replaceRecording(selectedRecordingIndex)}>
                {REPLACE_RECORDING_BUTTON_LABEL}
              </Button>
            </>}
          </div>

          <div className="tab-body">
            {selectedControlTab === ControlTabId.CHANNELS && !showChannels && <ExternallyTriggeredHelp/>}
            {selectedControlTab === ControlTabId.CHANNELS && showChannels && <ChannelsTab/>}
            {selectedControlTab === ControlTabId.SETTINGS && <SettingsTab/>}
          </div>
        </div>}
      </>}
    </div>
  </div>;
};
