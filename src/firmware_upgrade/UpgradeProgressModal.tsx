import React, { ReactElement } from 'react';
import {
  Icons,
  Modal,
  Text,
  Button,
  ProgressBar,
  TextType,
  Loader,
  ButtonRow,
  ButtonRowConfirmCancel,
  NoticeMessage,
  UnorderedList
} from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import { useSelector } from 'react-redux';
import { match } from 'ts-pattern';
import { ModalButtons } from '@zaber/react-library/dist/types/elements/Modal/Modal';

import { openExternalLink } from '../desktop';
import { deviceNameWithLabel, type IdentifiedDeviceInfo } from '../connection_manager';
import { ExternalLink } from '../components';
import { LoadErrorDetails } from '../save_load';
import { environment, Flavors } from '../environment';
import { compareFirmwareVersions } from '../app_components';

import { actions as actionsDefinition } from './actions';
import { selectDeviceInfoWithAxes, selectUpgradeProgress } from './selectors';
import type { InstallParams, UpgradeProgress } from './types';
import { isReleaseNotesLink } from './ReleaseNotes';
import { SetStateResponseWarnings } from '../save_load/components/SetStateWarning';

const STEPS_WITH_CLOSER: UpgradeProgress['step'][] = [
  'ask-if-save',
  'could-not-save',
  'cancelled',
  'failed',
  'failed-reload',
  'done',
];

const Streaming: React.FC<{written: number; total: number; remainingTime?: number }> = ({ written, total, remainingTime }) => {
  let displayTime = '';
  if (remainingTime !== undefined) {
    if (remainingTime > 60) {
      const minutes = Math.floor(remainingTime / 60);
      displayTime = `${minutes} ${minutes === 1 ? 'minute' : 'minutes'}`;
    } else {
      const seconds = Math.floor(remainingTime);
      displayTime = `${seconds} ${seconds === 1 ? 'second' : 'seconds'}`;
    }
  }
  const message = `Uploading the firmware upgrade. ${displayTime} remaining...`;

  return (
    <div className="info">
      <Text>{message}</Text>
      <ProgressBar progress={written / total}/>
    </div>
  );
};

const Working: React.FC<{ message: string }> = ({ message }) => (
  <div className="info working">
    <Loader size="large"/>
    <Text>{message}</Text>
  </div>
);

const advisory = <UnorderedList header={undefined}>
  <Text>Your settings will revert to factory defaults</Text>
  <Text>Any Stream Buffers, Triggers, and Servo Tuning Parameters will be cleared</Text>
</UnorderedList>;

const CancelInstallButton: React.FC = () => {
  const actions = useActions(actionsDefinition);
  return <Button title="Cancel Upgrade" color="red" onClick={actions.cancelInstallNewFirmware}>
    Cancel
  </Button>;
};

const OkCloseButton: React.FC = () => {
  const actions = useActions(actionsDefinition);
  return <Button title="Leave Upgrade Dialogue" color="red" onClick={() => actions.setInstallProgress(null)}>
    OK
  </Button>;
};

const getAskIfSave = (installParams: InstallParams, device: IdentifiedDeviceInfo, actions: typeof actionsDefinition) => {
  const continueWithoutSave = () => actions.installFirmware(installParams, false);
  const closeAction = () => actions.setInstallProgress(null);
  if (installParams.version == null) {
    return {
      content:
        <div className="info">
          <Text>You are about to install firmware from a file</Text>
          <Text t={TextType.H5}>Please be advised that:</Text>
          {advisory}
        </div>,
      buttons:
        <ButtonRowConfirmCancel confirmText="Install" onConfirm={continueWithoutSave} onCancel={closeAction}/>
    };
  }
  if (
    installParams.version.parsed.minor === 99 || installParams.version.parsed.minor === 98 ||
    device.identity.firmwareVersion.minor === 99 || device.identity.firmwareVersion.minor === 98
  ) {
    return {
      content:
        <div className="info">
          <Text>You are about to install Firmware on this device that is either for an ENG or internal testing.</Text>
          <Text>You can attempt to save the device state, but this will not always work for devices with this type of firmware</Text>
          <Text t={TextType.H5}>Please be advised that if the save fails:</Text>
          {advisory}
        </div>,
      buttons:
        <ButtonRowConfirmCancel
          confirmText="Attempt Save"
          cancelText="Don't Save"
          onConfirm={() => actions.installFirmware(installParams, true)}
          onCancel={continueWithoutSave}
        />
    };
  }
  const isDowngrade = compareFirmwareVersions(installParams.version.parsed, device.identity.firmwareVersion) < 0;
  if (isDowngrade) {
    return {
      content:
        <div className="info">
          <Text>You are about to downgrade to firmware version {installParams.version.str}</Text>
          <Text t={TextType.H5}>Please be advised that:</Text>
          {advisory}
        </div>,
      buttons:
        <ButtonRowConfirmCancel confirmText="Downgrade" onConfirm={continueWithoutSave} onCancel={closeAction}/>
    };
  } else {
    return {
      content:
        <div className="info">
          <Text>You are about to upgrade to firmware version {installParams.version.str}</Text>
          <Text t={TextType.H5}>Would you like to preserve your device state across the upgrade?</Text>
          <Text>
            If you choose "Preserve", the program will automatically restore things like settings, stream
            buffers, triggers, and servo tuning parameters after the upgrade.
          </Text>
          <Text>If you do not preserve:</Text>
          {advisory}
        </div>,
      buttons:
        <ButtonRowConfirmCancel
          confirmText="Preserve"
          cancelText="Clear"
          onConfirm={() => actions.installFirmware(installParams, true)}
          onCancel={continueWithoutSave}
        />
    };
  }
};

const getCouldNotSave = (installParams: InstallParams, error: string, actions: typeof actionsDefinition) => {
  const supportLink = <ExternalLink url=" https://www.zaber.com/contact">Customer Support</ExternalLink>;

  return {
    content:
      <NoticeMessage type="error" headline="Error Saving The Device State">
        <Text>There was an error saving your device's state: {error}</Text>
        <br/><br/>
        <Text>If you upgrade without saving:</Text>
        {advisory}
        <Text>If that is OK, click "Upgrade without saving" below. Otherwise contact {supportLink}.</Text>
      </NoticeMessage>,
    buttons:
      <ButtonRowConfirmCancel
        confirmText="Try Again"
        cancelText="Upgrade Without Saving"
        onConfirm={() => actions.installFirmware(installParams, true)}
        onCancel={() => actions.installFirmware(installParams, false)}
      />
  };
};

const getUpgradeProgressModalContent = (progress: UpgradeProgress, device: IdentifiedDeviceInfo, actions: typeof actionsDefinition) =>
  match<UpgradeProgress, ReactElement>(progress)
    .returnType<{content: React.ReactNode; buttons?: ModalButtons}>()
    .with({ step: 'connecting' }, () => ({
      content: <Working message="Connecting to the device..."/>
    }))
    .with({ step: 'canceling' }, () => ({
      content: <Working message="Upgrade cancelled. Please wait while your device restarts."/>
    }))
    .with({ step: 'cancelled' }, () => ({
      content:
        <NoticeMessage type="warning" headline="Cancelled">
          The upgrade was cancelled and your device was reset to its former version.
        </NoticeMessage>,
      buttons:
        <OkCloseButton/>
    }))
    .with({ step: 'time_out' }, () => ({
      content:
        <NoticeMessage type="error" headline="Failed">
          Your device did not restart in the allotted time.
          Make sure that it is still connected, and try refreshing the connection under "All Connections".
        </NoticeMessage>,
      buttons:
        <OkCloseButton/>
    }))
    .with({ step: 'checking-device' }, () => ({
      content: <Working message="Checking if the device can upgrade..."/>,
      buttons: <CancelInstallButton/>
    }))
    .with({ step: 'downloading-fwu' }, () => ({
      content: <Working message="Downloading the firmware data..."/>
    }))
    .with({ step: 'preparing-stream' }, () => ({
      content: <Working message="Preparing the update..."/>,
      buttons: <CancelInstallButton/>
    }))
    .with({ step: 'applying' }, () => ({
      content: <Working message="Applying the update. Please do not disconnect the power."/>
    }))
    .with({ step: 'reporting-success' }, () => ({
      content: <Working message="Reporting successful installation."/>
    }))
    .with({ step: 'ask-if-save' }, ({ installParams }) =>
      getAskIfSave(installParams, device, actions)
    )
    .with({ step: 'could-not-save' }, ({ installParams, error }) =>
      getCouldNotSave(installParams, error, actions)
    )
    .with({ step: 'saving-state' }, () => ({
      content: <Working message="Saving this device's state..."/>
    }))
    .with({ step: 'reloading-state' }, () => ({
      content: <Working message="Reloading this device's saved state..."/>
    }))
    .with({ step: 'streaming' }, ({ bytesWritten, bytesTotal, remainingTime }) => ({
      content: <Streaming written={bytesWritten} total={bytesTotal} remainingTime={remainingTime}/>,
      buttons: <CancelInstallButton/>
    }))
    .with({ step: 'failing' }, () => ({
      content: <Working message="There was an error! Your device is being reset to its previous state."/>
    }))
    .with({ step: 'failed' }, ({ error }) => ({
      content:
        <NoticeMessage type="error" headline="Failed">
          <Text>The upgrade failed. Your device has been reset to its former version.</Text>
          <Text className="detail">Error: {error}</Text>
        </NoticeMessage>,
      buttons: <OkCloseButton/>
    }))
    .with({ step: 'failed-reload' }, ({ pathToSaveFile, error, details }) => ({
      content:
        <NoticeMessage type="error" headline="Failed to reload device state">
          <Text>The device was upgraded, but a failure occurred attempting to reload the device's state.</Text>
          <Text>The device's previous state was saved to {pathToSaveFile}.</Text>
          <Text>To fully restore the device to its pre-upgrade state, fix these errors, then manually load this file to the device.</Text>
          <Text className="detail">Error: {error}</Text>
          <LoadErrorDetails details={details}/>
        </NoticeMessage>,
      buttons:
        <OkCloseButton/>
    }))
    .with({ step: 'done' }, ({ version, setStateResponse }) => ({
      content:
        <NoticeMessage type="success" headline="Completed">
          <Text>Firmware upgrade of the device was successfully completed.</Text>
          {setStateResponse && <SetStateResponseWarnings
            header="The following exceptions were handled, but prevented the device state from being fully restored:"
            response={setStateResponse}
          />}
        </NoticeMessage>,
      buttons:
        <ButtonRow>
          <OkCloseButton/>
          {environment.flavor === Flavors.Zaber && version != null && isReleaseNotesLink(version) &&
          <Button onClick={() => openExternalLink(version.notes)} color="grey">
            View Release Notes
          </Button>}
        </ButtonRow>
    }))
    .exhaustive();

export const UpgradeProgressModal: React.FC<{ customTitle?: string }> = ({ customTitle }) => {
  const device = useSelector(selectDeviceInfoWithAxes);
  const progress = useSelector(selectUpgradeProgress);
  const actions = useActions(actionsDefinition);

  if (!progress || !device) { return null }

  const { content, buttons } = getUpgradeProgressModalContent(progress, device, actions);
  return (
    <Modal
      onRequestClose={STEPS_WITH_CLOSER.includes(progress.step) ? () => actions.setInstallProgress(null) : undefined}
      className="firmware-upgrade-progress-modal" isOpen={true}
      headerIcon={<Icons.Upgrade/>}
      headerText={customTitle ?? `${deviceNameWithLabel(device)} Firmware Upgrade`}
      buttons={buttons}>
      {content}
    </Modal>
  );
};
