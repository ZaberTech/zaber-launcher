import { END, eventChannel, EventChannel, SagaIterator } from 'redux-saga';
import { all, call, put, select, takeLeading, takeLatest, fork, delay, race, take, takeEvery } from 'redux-saga/effects';
import { Auth } from 'aws-amplify';
import type { CognitoUser } from 'amazon-cognito-identity-js';
import _ from 'lodash';
import { filter, map } from 'rxjs/operators';
import type { Observable } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { cast, castAny, ensureError, makeString, notNil, throwUnexpectedError } from '@zaber/toolbox';
import schema, { validate } from 'jsonschema';
import { isMatching, P } from 'ts-pattern';

import { getContainer } from '../container';
import type { Action, AsyncReturnType as ART } from '../utils';
import { environment } from '../environment';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import { makeCloudUrl } from '../connection_manager';
import { IoTService, Message, IotConnection, AwsCredentialsService, Logger, ZaberApi, HttpError, Storage } from '../app_components';
import { getLogger } from '../log';

import './amplify';
import { selectIotRealm, selectLoginData, selectSharingStatus } from './selectors';
import { actions, ActionsToPayloads, ActionTypes } from './actions';
import type { DiscoveredClient, LoggedInUser } from './types';

let logger: Logger;

export const TEST_CHECK_LOGGED_IN = 'TEST_CHECK_LOGGED_IN';
export const CLOUD_SHARE_TOKEN_STORAGE_KEY = 'CLOUD_SHARE_TOKEN_ID';
export const CLOUD_SHARE_ID_STORAGE_KEY = 'CLOUD_SHARE_ID';

export function* cloudSaga(): SagaIterator {
  logger = getLogger('cloudSaga');

  yield all([
    takeLeading(ActionTypes.LOGIN, login),
    takeLeading(ActionTypes.LOGOUT, logout),
    takeLeading(ActionTypes.TURN_CLOUD_SHARING, turnCloudSharing),
    takeLatest(ActionTypes.CLOUD_SHARING_STATUS, checkCloudSharingStatus),
    takeLeading(ActionTypes.START_DISCOVERY, startDiscovery),
    !environment.isTest ? fork(checkLoggedIn) : null,
    environment.isTest ? takeEvery(TEST_CHECK_LOGGED_IN, checkLoggedIn) : null,
  ].filter(notNil));
}

function getErrorMessage(err: unknown): string {
  if (isMatching({ message: P.string }, err)) {
    return err.message;
  } else {
    return `Unknown error: ${makeString(err)}`;
  }
}

async function getUserData(user: CognitoUser): Promise<LoggedInUser> {
  const attributes = await Auth.userAttributes(user);
  const attributesDict = _.mapValues(_.keyBy(attributes, 'Name'), value => value.Value);
  return attributesDict as unknown as LoggedInUser;
}

function* login(): SagaIterator {
  const loginData: ReturnType<typeof selectLoginData> = yield select(selectLoginData);

  try {
    if (!loginData.email) {
      throw new Error('Email not provided.');
    }
    if (!loginData.password) {
      throw new Error('Password not provided.');
    }

    const user: CognitoUser = yield call([Auth, Auth.signIn], {
      username: loginData.email,
      password: loginData.password,
    });
    const loggedUser: ART<typeof getUserData> = yield call(getUserData, user);

    yield call(setAwsCredentials, user);
    yield put(actions.loginResult(loggedUser));

    logger.info('Logged in', loggedUser.sub);
  } catch (err) {
    // can sometimes throw non-Error objects
    logger.warn(err);
    yield put(actions.loginResultErr(getErrorMessage(err)));
  }
}

function* checkLoggedIn(): SagaIterator {
  let newAttemptDelaySeconds = 1;
  while (true) {
    try {
      // correctly currentAuthenticatedUser should be used instead of currentUserPoolUser
      // unfortunately it does not correctly distinguish between network error and not being authenticated
      const user: CognitoUser = yield call([Auth, Auth.currentUserPoolUser]);
      const loggedUser: ART<typeof getUserData> = yield call(getUserData, user);

      yield call(setAwsCredentials, user);
      yield put(actions.checkAuthResult(loggedUser));

      logger.info('Logged in', loggedUser.sub);
      return;
    } catch (err) {
      // can sometimes throw non-Error objects
      logger.warn(err);

      const tryAgain = err instanceof Object && 'code' in err && err.code === 'NetworkError';
      if (!tryAgain) {
        yield call(setAwsCredentials);
        yield put(actions.checkAuthResult());
        return;
      }
    }

    const { now } = yield race({
      delay: delay(newAttemptDelaySeconds * 1000),
      now: take(ActionTypes.CHECK_AUTH_NOW),
    });
    if (now) {
      newAttemptDelaySeconds = 1;
    } else if (newAttemptDelaySeconds < 128) {
      newAttemptDelaySeconds *= 2;
    }
  }
}

function* logout(): SagaIterator {
  const sharingStatus: ReturnType<typeof selectSharingStatus> = yield select(selectSharingStatus);
  if (sharingStatus?.sharingOn) {
    yield put(actions.turnCloudSharing(false));
    yield take(ActionTypes.TURN_CLOUD_SHARING_DONE);
  }

  const storage = getContainer().get(Storage);
  storage.save(CLOUD_SHARE_ID_STORAGE_KEY, null);

  try {
    yield call([Auth, Auth.signOut]);
  } catch (err) {
    // can sometimes throw non-Error objects
    logger.warn(err);
  }

  yield call(setAwsCredentials);
}

async function setAwsCredentials(user?: CognitoUser): Promise<void> {
  const awsCredentials = getContainer().get(AwsCredentialsService);
  await awsCredentials.setUser(user ?? 'unauthenticated');
}

function* turnCloudSharing({ payload: { on } }: Action<ActionsToPayloads[ActionTypes.TURN_CLOUD_SHARING]>): SagaIterator {
  try {
    const service = getContainer().get(MessageRoutersService);
    const api = getContainer().get(ZaberApi);
    const storage = getContainer().get(Storage);
    const connection: ART<typeof service.getConnection> = yield call([service, service.getConnection], LOCAL_ROUTER_URL);
    if (on) {
      let cloudId = storage.load<string>(CLOUD_SHARE_ID_STORAGE_KEY);
      if (cloudId == null) {
        cloudId = uuidv4();
        storage.save(CLOUD_SHARE_ID_STORAGE_KEY, cloudId);
      }

      const realm: ReturnType<typeof selectIotRealm> = yield select(selectIotRealm);
      const name: ART<typeof connection.api.getName> = yield call([connection.api, connection.api.getName]);

      const token: ART<typeof api.createUserAccessToken> = yield call([api, api.createUserAccessToken], {
        createdBy: 'zaberLauncher',
        description: `Token created for cloud sharing on ${name}`,
        scopes: ['iot'],
      });
      storage.save(CLOUD_SHARE_TOKEN_STORAGE_KEY, token.tokenId);

      yield call([connection.api, connection.api.setCloudConfig], {
        id: cloudId,
        token: token.token,
        apiUrl: environment.apiUrl,
        realm: realm!,
      });
    } else {
      const tokenId = storage.load<string>(CLOUD_SHARE_TOKEN_STORAGE_KEY);
      if (tokenId) {
        try {
          yield call([api, api.deleteUserAccessToken], tokenId);
        } catch (err) {
          const isNotExistError = cast(castAny(err, ZaberApi.Error)?.innerError, HttpError)?.status === 404;
          if (!isNotExistError) {
            throw err;
          }
        }
        storage.save(CLOUD_SHARE_TOKEN_STORAGE_KEY, null);
      }
      yield call([connection.api, connection.api.setCloudConfig], null);
    }

    yield put(actions.turnCloudSharingDone());
    yield put(actions.cloudSharingStatus());
  } catch (err) {
    throwUnexpectedError(err);
    logger.warn(err);
    yield put(actions.turnCloudSharingDone(err.message));
  }
}

function* checkCloudSharingStatus(): SagaIterator {
  try {
    const service = getContainer().get(MessageRoutersService);
    const connection: ART<typeof service.getConnection> = yield call([service, service.getConnection], LOCAL_ROUTER_URL);
    const status: ART<typeof connection.api.getCloudStatus> = yield call([connection.api, connection.api.getCloudStatus]);
    const config: ART<typeof connection.api.getCloudConfig> = yield call([connection.api, connection.api.getCloudConfig]);
    yield put(actions.cloudSharingStatusResult({
      isConnected: status.isConnected,
      sharingOn: !!config,
      id: config?.id ?? null,
      token: config?.token ?? null,
    }));
  } catch (err) {
    throwUnexpectedError(err);
    logger.warn(err);
    yield put(actions.cloudSharingStatusResultErr(err.message));
  }
}

const discoveredClientSchema: schema.Schema = {
  type: 'object',
  properties: {
    id: {
      type: 'string',
      minLength: 1,
    },
    name: {
      type: 'string',
      minLength: 1,
    },
  },
  required: ['id', 'name'],
};

function parseAndValidateDiscoveryReply(message: Message, realm: string): DiscoveredClient | null {
  try {
    const client: { id: string; name: string } = JSON.parse(message.payload.toString());
    validate(client, discoveredClientSchema, { throwFirst: true });

    return {
      ...client,
      url: makeCloudUrl(realm, client.id),
    };
  } catch (err) {
    logger.warn(err);
    return null;
  }
}

function createDiscoveryActionChannel(topic: Observable<Message>, realm: string): EventChannel<Action<unknown>> {
  return eventChannel<Action<unknown>>(emit => {
    const subscription = topic.pipe(
      map(message => parseAndValidateDiscoveryReply(message, realm)),
      filter(notNil)
    ).subscribe(
      client => emit(actions.discoveryClientFound(client)),
      error => {
        emit(actions.discoveryError(ensureError(error).message));
        emit(END);
      },
      () => emit(END),
    );

    return () => subscription.unsubscribe();
  });
}

export const BROADCAST_DISCOVERY_REQUEST_PERIOD = 10000;

function* discoverRealm(iot: IotConnection, realm: string): SagaIterator {
  const topic: ART<typeof iot.subscribe> = yield call([iot, iot.subscribe], `${realm}/message-router/discovery-response`);
  const channel = createDiscoveryActionChannel(topic, realm);
  try {
    let broadcast = true;
    for (;;) {
      if (broadcast) {
        yield call([iot, iot.publish], `${realm}/message-router/discovery-request`, '');
        broadcast = false;
      }

      const { action } = yield race({
        action: take(channel),
        shouldBroadcast: delay(BROADCAST_DISCOVERY_REQUEST_PERIOD),
      });
      if (action) {
        yield put(action);
      } else {
        broadcast = true;
      }
    }
  } finally {
    channel.close();
  }
}

function* startDiscovery(): SagaIterator {
  try {
    const defaultRealm: ReturnType<typeof selectIotRealm> = yield select(selectIotRealm);
    const iot = getContainer().get(IoTService);
    const connection: ART<typeof iot.getConnection> = yield call([iot, iot.getConnection]);
    yield race({
      realm: call(discoverRealm, connection, defaultRealm!),
      stop: take(ActionTypes.STOP_DISCOVERY),
    });
  } catch (err) {
    throwUnexpectedError(err);
    logger.warn(err);
    yield put(actions.discoveryError(err.message));
  }
}
