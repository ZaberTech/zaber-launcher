import path from 'path';
import os from 'os';
import net from 'net';
import childProcess from 'child_process';

import { ReplaySubject, throwError } from 'rxjs';
import { timeoutWith } from 'rxjs/operators';
import { app } from 'electron';
import { injectable } from 'inversify';
import { Tools } from '@zaber/motion';

import { handleNonCriticalError } from '../src/errors/errors';
import { environment } from '../src/environment';

export const EXIT_TIMEOUT = 5000;

export enum MessageRouterExitCode {
  OK = 0,
  GenericError = 1,
  AnotherInstanceRunning = 2,
}

@injectable()
export class MessageRouter {
  private routerProcess: childProcess.ChildProcess | null = null;
  private stopped = false;
  private hasExit = false;
  private readonly runSubject = new ReplaySubject<void>();
  private _runPromise?: Promise<void>;

  public get runPromise() {
    if (this._runPromise == null) {
      this._runPromise = this.runSubject.toPromise();
    }
    return this._runPromise;
  }

  public startRouter(): void {
    if (!environment.isProduction && process.env.NO_MESSAGE_ROUTER) {
      return;
    }

    let installDir: string;
    if (environment.isProduction) {
      installDir = path.dirname(app.getPath('exe'));
      if (os.platform() === 'darwin') {
        installDir = path.join(installDir, '..');
      }
    } else {
      installDir = path.join(__dirname, '..');
    }
    let routerExe = path.join(installDir, 'message_router', `zaber-message-router-${os.arch()}`);
    if (os.platform() === 'win32') {
      routerExe += '.exe';
    }

    const configFile = path.join(app.getPath('userData'), 'message_router_config.yml');
    this.routerProcess = childProcess.spawn(routerExe, ['run'], {
      stdio: 'inherit',
      env: {
        ...process.env,
        ZABER_MESSAGE_ROUTER_CONFIG_FILE: configFile,
      },
      windowsHide: true,
    });

    this.routerProcess.once('error', this.onError);
    this.routerProcess.once('exit', this.onExit);

    // failsafe when the process exits unexpectedly the router is not left running
    process.once('exit', this.killProcess);
  }

  public async stopRouter(): Promise<void> {
    if (this.stopped) {
      return;
    }
    this.stopped = true;

    if (this.routerProcess == null || this.hasExit) {
      return;
    }

    try {
      await this.requestExit();
      await this.runSubject.pipe(
        timeoutWith(EXIT_TIMEOUT, throwError(new Error('Message router has not exit')))
      ).toPromise();
    } catch (err) {
      handleNonCriticalError(err);
    }

    if (!this.hasExit) {
      this.killProcess();
    }
  }

  private async requestExit() {
    const pipe = net.createConnection({
      path: Tools.getMessageRouterPipePath(),
    });
    pipe.on('data', () => null); // if the callback is not present the process hangs randomly
    await new Promise((resolve, reject) => {
      pipe.once('connect', resolve);
      pipe.once('error', reject);
    });

    const exitNotification = `${JSON.stringify({ jsonrpc: '2.0', method: 'control_exit' })}`;
    pipe.end(exitNotification);
  }

  private onError = (error: Error) => {
    this.hasExit = true;
    (process as NodeJS.EventEmitter).off('exit', this.killProcess);

    this.runSubject.error(new MessageRouterError(`Cannot start: ${error.message}`, MessageRouterExitCode.GenericError));
  };

  private onExit = (exitCode: number) => {
    this.hasExit = true;
    (process as NodeJS.EventEmitter).off('exit', this.killProcess);

    if (this.stopped) {
      if (exitCode === MessageRouterExitCode.OK) {
        this.runSubject.next();
        this.runSubject.complete();
      } else {
        this.runSubject.error(new MessageRouterError(`Non-zero exit code: ${exitCode}`, exitCode));
      }
    } else {
      this.runSubject.error(new MessageRouterError('Unexpected exit', exitCode));
    }
  };

  private killProcess = () => {
    try {
      this.routerProcess?.kill('SIGKILL');
    } catch {
      // if this fails there is nothing we can do
    }
  };
}

export class MessageRouterError extends Error {
  constructor(
    message: string,
    public readonly exitCode: MessageRouterExitCode,
  ) {
    super(message);
    Object.setPrototypeOf(this, MessageRouterError.prototype);
  }
}
