import React from 'react';
import classNames from 'classnames';
import { Icons, Text } from '@zaber/react-library';

export const Message: React.FC<{ title: string; type: 'error' | 'warning' | 'info'; children: string }> =
({ title, type, children }) => (
  <div className={classNames('message', type)}>
    <div className="title">
      {type === 'error' && <Icons.ErrorFault/>}
      {type === 'warning' && <Icons.ErrorWarning/>}
      {type === 'info' && <Icons.ErrorNote/>}
      &nbsp;{title}
    </div>
    <Text e={Text.Emphasis.Light} title={children} className="content">{children}</Text>
  </div>);
