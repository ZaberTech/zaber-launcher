import { createSelector } from 'reselect';
import _ from 'lodash';
import { P } from 'ts-pattern';
import { ascii } from '@zaber/motion';

import { selectMicroscope } from '../../store';
import {
  selectIdentifiedDevices,
  selectIdentifiedAxes,
} from '../../connection_manager';
import { X_MOR_DEVICE_ID } from '../types';
import { selectConnection } from '../selectors';
import { isMatch } from '../../utils';

export const selectState = createSelector(selectMicroscope, state => state.app);

export const selectConfigState = createSelector(selectState, state => state.config);

export const selectDevices = createSelector(selectConnection, selectIdentifiedDevices,
  (connection, devices) => connection && _.pick(devices, ...(connection.devices ?? [])));

export const selectIlluminators = createSelector(selectDevices, selectIdentifiedAxes,
  (devices, allAxes) => {
    if (!devices) { return [] }

    const illuminators = _.values(devices).filter(device =>
      device.axes.some(key => allAxes[key].identity.axisType === ascii.AxisType.LAMP));

    return illuminators.map(illuminator => ({
      ...illuminator,
      axes: illuminator.axes.map(key => allAxes[key]),
    }));
  });

export const selectObjectiveChanger = createSelector(selectDevices, devices => {
  if (!devices) { return null }

  // Identifies the objective changer the same way as ZML (using device.id).
  const changers = _.values(devices).filter(device => X_MOR_DEVICE_ID.includes(device.identity.deviceId));
  switch (changers.length) {
    case 0:
      return null;
    case 1:
      return changers[0];
    default:
      return { error: 'Multiple objective changers found' };
  }
});

export const selectLoading = createSelector(selectConfigState, state => state.loading);
export const selectEditedConfig = createSelector(selectConfigState, state => state.edited);
export const selectWriting = createSelector(selectConfigState, state => state.writing);
export const selectWdiSearch = createSelector(selectConfigState, state => state.wdiSearch);

export const selectHasConfigChanges = createSelector(selectLoading, selectEditedConfig, (loading, editedConfig) =>
  isMatch({ loaded: P.any }, loading) && !_.isEqual(loading.loaded, editedConfig));

export const selectDialogOpen = createSelector(selectConfigState, state => state.dialogOpen);
