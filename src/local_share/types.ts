import type { LocalShareConfig } from '../app_components';

export const LOCAL_SHARE_DEFAULT_PORT = 11421;
export const LOCAL_SHARE_DISCOVERY_SERVICE_SUFFIX = '_zaber_message_router._tcp.local';
export const LOCAL_SHARE_DISCOVERY_INACTIVE_TIMEOUT = 3000;

export const DEFAULT_CONFIG: Omit<LocalShareConfig, 'instanceName'> = {
  enabled: false,
  discoverable: true,
  port: LOCAL_SHARE_DEFAULT_PORT,
};

export const MAX_PORT_NUM = 65535;

export interface LocalShareCandidate {
  serviceName: string;
  url: string;
  name: string;
}
