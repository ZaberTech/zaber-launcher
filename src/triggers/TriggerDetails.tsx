import { Button, ButtonRow, Flex, Loader, NoticeBanner, PopUp, Text } from '@zaber/react-library';
import React, { useEffect, useState } from 'react';

import type { EntityKey } from '../keys';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { TriggerCondition } from './TriggerCondition';
import { TriggerAction, getActionLetter } from './TriggerAction';
import type { TriggerState } from './reducer';
import { useTriggerSaveableError, useTriggerSupportedError } from './hooks';


const UNSUPPORTED_TRIGGER_MESSAGE =
  'A trigger was loaded from the device that is not supported by this app. Change the following before saving the trigger:';
interface TriggerSettingsProps {
  deviceKey: EntityKey;
  triggerIndex: number;
  state: TriggerState;
}

export const TriggerDetails: React.FC<TriggerSettingsProps> = ({ deviceKey, triggerIndex, state }) => {
  const actions = useActions(actionsDefinition);

  const triggerSaveableError = useTriggerSaveableError(state);
  const triggerSupportedError = useTriggerSupportedError(state);

  const [showUnsupportedMessage, setShowUnsupportedMessage] = useState(true);

  useEffect(() => {
    actions.requestDetailsDevice(triggerIndex, true);
  }, []);

  const { conditionInput, conditionDevice, actionsInput, actionsDevice, inputChanged, error } = state;

  return <div title="Trigger Details" className="details">
    {triggerSupportedError != null && showUnsupportedMessage &&
      <NoticeBanner
        type="warning"
        collapsible
        headline="Unsupported Trigger"
        closer={() => setShowUnsupportedMessage(false)}
      >
        {UNSUPPORTED_TRIGGER_MESSAGE}
        <br/>
        <br/>
        <Text className="trigger-supported-error">{triggerSupportedError}</Text>
      </NoticeBanner>
    }
    <Flex.Row className="details-row">
      {(conditionInput !== null || error === null) &&
        <Text className="details-row-label" t={Text.Type.H4}>Condition</Text>
      }
      <Flex.Row className="details-row-content">
        {conditionInput !== null && conditionDevice !== null &&
          <TriggerCondition deviceKey={deviceKey} triggerIndex={triggerIndex} inputState={conditionInput}/>
        }
        {conditionInput === null && error === null &&
          <DetailsLoader/>
        }
      </Flex.Row>
    </Flex.Row>
    <div className="actions-area">
      {actionsInput.length > 0 &&
        actionsInput.map((actionInput, i) =>
          actionsDevice[i] &&
            <Flex.Row key={i} className="details-row">
              <Text className="details-row-label" t={Text.Type.H4}>Action {getActionLetter(i)}</Text>
              <Flex.Row className="details-row-content">
                <TriggerAction
                  deviceKey={deviceKey}
                  triggerIndex={triggerIndex}
                  actionIndex={i}
                  inputState={actionInput}
                />
              </Flex.Row>
            </Flex.Row>)
      }
      {actionsInput.length === 0 && error === null &&
        <Flex.Row>
          <Text className="details-row-label" t={Text.Type.H4}>Actions</Text>
          <DetailsLoader/>
        </Flex.Row>
      }
    </div>
    {inputChanged && conditionDevice !== null && conditionInput !== null &&
      <SaveControls
        triggerIndex={triggerIndex}
        error={triggerSaveableError}
      />
    }
  </div>;
};

const DetailsLoader: React.FC = () =>
  <div className="loading-details">
    <Loader size="small"/>
    <Text t={Text.Type.Body}>Loading...</Text>
  </div>;

interface SaveControlsProps {
  triggerIndex: number;
  error: string | null;
}

const SaveControls: React.FC<SaveControlsProps> = ({ triggerIndex, error }) => {
  const actions = useActions(actionsDefinition);

  return <ButtonRow title="Save Controls" className="save-controls" justify="right">
    <PopUp title="Save Disabled" triggerAction={error == null ? 'none' : 'hover'} position="top" trigger={() =>
      <Button
        title="Save Changes"
        color="red"
        disabled={error != null}
        onClick={() => actions.sendDetailsDevice(triggerIndex)}
      >
          Save
      </Button>
    }>
      {error}
    </PopUp>
    <Button title="Cancel Changes" color="grey" onClick={() => actions.resetInput(triggerIndex)}>Cancel</Button>
  </ButtonRow>;
};
