import { actionBuilder, AnyAction } from '../utils';

export enum ActionTypes {
  IPC_BROADCAST = 'IPC_BROADCAST',
}

export interface ActionsToPayloads {
  [ActionTypes.IPC_BROADCAST]: { actionToBroadcast: AnyAction; dispatchLocally: boolean };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  ipcBroadcast: (actionToBroadcast: AnyAction, dispatchLocally = false) =>
    buildAction(ActionTypes.IPC_BROADCAST, { actionToBroadcast, dispatchLocally }),
};
