const FILENAME = 'Foo.csv';

const mockOpenDialog = jest.fn(() => ({
  cancelled: false,
  filePaths: [FILENAME],
}));

jest.mock('../dialogs', () => ({
  Dialogs: {
    showOpenDialog: mockOpenDialog,
    showMessageBox: jest.fn(async ({ message }: { message: string }) => {
      let response = 0;
      if (message.startsWith('Discard unsaved changes')) {
        response = 1;
      }
      return { response };
    }),
  },
}));

let fileContent: string = '';

const fsMock = {
  readFile: jest.fn(async () => fileContent),
};

jest.mock('fs/promises', () => fsMock);

import { EChartsReactMock, selectPoint } from './testing/mocks';

jest.mock('echarts-for-react', () => EChartsReactMock);

import _ from 'lodash';
import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import type { Container } from 'inversify';

import { createContainer, destroyContainer } from '../container';
import { FileWatcher } from '../file_watcher';
import { getTableCells, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import { FileWatcherMock } from '../test/mocks/file_watcher';

import { actions } from './actions';
import { IMPORT_BUTTON_LABEL } from './CsvImportDialog';
import { FILE_MENU_ID, OPEN_FILE_LABEL, PvtApp, TABLE_BUTTON_LABEL } from './PvtApp';
import { selectUserPath } from './selectors';

const file1 = 'Time (s),X Position (mm),X Velocity (mm/s),Y Position(mm), Y Velocity (mm/s)\n' +
  '0,1,2,3,4\n' +
  '0.1,5,6,7,8\n' +
  '0.2,9,10,11,12\n';

const file2 = 'Time (s),X Position (deg),X Velocity (deg/s)\n' +
  '0,1,2\n' +
  '0.1,3,4\n' +
  '0.2,5,6\n';

let container: Container;
const TestApp = wrapWithNewStore(wrapWithRouter(PvtApp));
let wrapper: RenderResult;


async function loadFile(content: string) {
  fileContent = content;
  fireEvent.click(wrapper.getByTestId(FILE_MENU_ID));
  fireEvent.click(wrapper.getByText(OPEN_FILE_LABEL));
  await waitUntilPass(() => fireEvent.click(wrapper.getByText(IMPORT_BUTTON_LABEL)));
  fireEvent.click(wrapper.getByTitle(TABLE_BUTTON_LABEL));
}


beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(FileWatcher).to(FileWatcherMock);
  wrapper = render(<TestApp/>);
});


afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});


describe('Table view', () => {
  beforeEach(async () => {
    await loadFile(file1);
  });


  it('displays the file name', () => {
    wrapper.getByText(FILENAME);
  });


  it('displays expected headers', () => {
    const table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
    expect(table[0]).toEqual(['', 'Time (s)', 'X', 'Y']);
    expect(table[1]).toEqual(
      ['#', 'Segment', 'Accumulated', 'Position (mm)', 'Velocity (mm/s)', 'Position (mm)', 'Velocity (mm/s)']);
  });


  it('displays angular units using canonical symbols', async () => {
    await loadFile(file2);
    const table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
    expect(table[0]).toEqual(['', 'Time (s)', 'X']);
    expect(table[1]).toEqual(
      ['#', 'Segment', 'Accumulated', 'Position (°)', 'Velocity (°/s)']);
  });


  it('displays expected data', () => {
    const table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
    expect(table[2]).toEqual([expect.stringMatching('1.*S.*'), '0', '0', '1', '0', '3', '0']);
    expect(table[3]).toEqual(['2', '0.1', '0.1', '5', '6', '7', '8']);
    expect(table[4]).toEqual(['3', '0.1', '0.2', '9', '0', '11', '0']);
  });


  it('indicates relative points', () => {
    const path = _.cloneDeep(selectUserPath(TestApp.testStore.getState())!);
    path.series[0].points[1].relative = true;
    TestApp.testStore.dispatch(actions.storeUserPath(path));

    const table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
    expect(table[3]).toEqual([expect.stringMatching('2.*R.*'), '0.1', '0.1', '5', '6', '7', '8']);
  });


  it('displays add/delete buttons when a row is selected', () => {
    expect(wrapper.queryByTestId('table-add-point-button')).toBeNull();
    expect(wrapper.queryByTestId('table-delete-point-button')).toBeNull();
    selectPoint(wrapper, 1);
    wrapper.getByTestId('table-add-point-button');
    wrapper.getByTestId('table-delete-point-button');
  });


  it('adds and deletes rows when the buttons are clicked', () => {
    let table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
    expect(table).toHaveLength(5);
    selectPoint(wrapper, 1);
    fireEvent.click(wrapper.getByTestId('table-add-point-button'));
    table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
    expect(table).toHaveLength(6);
    fireEvent.click(wrapper.getByTestId('table-delete-point-button'));
    table = getTableCells(wrapper.getByTestId('main-table') as HTMLTableElement);
    expect(table).toHaveLength(5);
  });
});
