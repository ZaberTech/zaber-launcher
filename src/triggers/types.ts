export const CONDITION_TYPES = [
  'Trajectory Position',
  'Distance Interval',
  'Encoder Distance Interval',
  'IO Channel',
  'Time Elapsed',
  'Setting',
  'Absolute Setting',
] as const;
export type ConditionType = typeof CONDITION_TYPES[number];

export const IO_TYPES = [
  'Digital Input',
  'Analog Input',
  'Digital Output',
  'Analog Output'
] as const;
export type IoType = typeof IO_TYPES[number];

export const OPERATORS = [
  '==',
  '≠',
  '<',
  '>',
  '≤',
  '≥'
] as const;
export type Operator = typeof OPERATORS[number];

export const ACTION_TYPES = [
  'None',
  'Command',
  'Set Setting to Value',
  'Set Setting to Setting'
] as const;
export type ActionType = typeof ACTION_TYPES[number];

export const OPERATIONS = [
  '=',
  '+=',
  '-='
] as const;
export type Operation = typeof OPERATIONS[number];

export type SettingsList = Record<string, SettingInfo>;
export interface SettingInfo {
    writeable: boolean;
  }
