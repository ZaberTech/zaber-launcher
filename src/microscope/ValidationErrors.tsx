import React from 'react';
import { useSelector } from 'react-redux';
import { Icons, NoticeBanner } from '@zaber/react-library';

import { useActions } from '../utils';

import { selectValidationErrors } from './selectors';
import { actions as actionDefinitions } from './actions';

export const ValidationErrors: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const errors = useSelector(selectValidationErrors);

  if (!errors.length) {
    return null;
  }

  return <NoticeBanner type="warning" className="no-config-warning" closer={{ action: actions.clearValidationErrors, button: 'Dismiss' }}>
    Some of the microscope components have settings outside the recommended range.<br/>
    The microscope may not function correctly with these settings.
    <br/>
    <br/>
    <ul>
      {errors.map((error, i) => <li key={i}><Icons.BulletPoint/>{error}</li>)}
    </ul>
  </NoticeBanner>;
};
