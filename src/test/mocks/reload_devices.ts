import { realTimers } from '@zaber/toolbox/lib/test';
import _ from 'lodash';
import type { Store } from 'redux';

import { connectionManagerActions, DeviceInfoWithAxes, selectAxes, selectDevices } from '../../connection_manager';
import { EntityKey, extractConnectionKey } from '../../keys';
import type { RootState } from '../../store';


interface ReloadDeviceMock {
  waitForReload: Promise<void> | null;
  reload(connectionKey: EntityKey): void;
  spy: jest.SpyInstance;
}

export const mockReloadDevices = (
  getStore: () => Store<RootState>,
  modifier?: (devices: DeviceInfoWithAxes[], connectionKey: EntityKey) => void,
) => {
  const mock: ReloadDeviceMock = {
    waitForReload: null,
    reload: (connectionKey: EntityKey) => {
      const store = getStore();
      store.dispatch(connectionManagerActions.loadDevices(connectionKey, false));
    },
    spy: jest.fn(),
  };

  mock.spy = jest.spyOn(connectionManagerActions, 'loadDevices')
    .mockImplementation((connectionKey: string) => {
      mock.waitForReload = new Promise(resolve => {
        const store = getStore();
        const state = store.getState();

        const axes = selectAxes(state);
        const devices = _.chain(selectDevices(state))
          .filter((_, key) => extractConnectionKey(key) === connectionKey)
          .map(device => ({
            ...device,
            axes: device.axes.map(key => axes[key]),
          }))
          .cloneDeep()
          .value();

        modifier?.(devices, connectionKey);

        realTimers.setTimeout(() => {
          store.dispatch(connectionManagerActions.devicesLoaded(connectionKey, devices));
          resolve();
        });
      });

      return  ({ type: 'MOCK_RELOAD_DEVICES', payload: null! });
    });

  return mock;
};
