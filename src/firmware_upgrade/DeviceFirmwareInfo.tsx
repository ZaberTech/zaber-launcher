import React from 'react';
import { ContextMenu, Icons, Text } from '@zaber/react-library';
import classNames from 'classnames';
import { useSelector } from 'react-redux';

import type { IdentifiedDeviceInfo } from '../connection_manager/types';
import { fwToString } from '../devices';
import { environment, Flavors } from '../environment';

import { ReleaseNotes } from './ReleaseNotes';
import { selectDeviceInfoWithAxes, selectCurrentVersion, selectNotSkippedNewerVersions, selectCurrentConnection } from './selectors';

interface Props {
  device: IdentifiedDeviceInfo;
}

const DeviceFirmwareVersion: React.FC<Props> = ({ device }) =>
  <Text>{`Firmware Version ${fwToString(device.identity.firmwareVersion)}`}</Text>;

const NewVersion: React.FC<{ available: boolean }> = ({ available }) => (
  <div className={classNames('new-version', available ? 'available' : 'up-to-date')}>
    <Icons.Dot className="red"/>&nbsp;{available ? 'New version available' : 'Up to date'}
  </div>
);

export const DeviceFirmwareInfo: React.FC = () => {
  const device = useSelector(selectDeviceInfoWithAxes)!;
  const currentVersion = useSelector(selectCurrentVersion);
  const canUpdate = useSelector(selectNotSkippedNewerVersions).length > 0;
  const connection = useSelector(selectCurrentConnection)!;
  return (
    <div className="device-firmware-info">
      <div className="current-version">
        <DeviceFirmwareVersion device={device}/>
        {currentVersion && environment.flavor === Flavors.Zaber && <ReleaseNotes
          version={currentVersion}
          trigger={openReleaseNotes => (
            <ContextMenu title="Additional Device Firmware Actions">
              <ContextMenu.Item onClick={openReleaseNotes} icon={<Icons.Notes/>}>
                Read Release Notes
              </ContextMenu.Item>
            </ContextMenu>
          )}/>}
      </div>
      {!environment.offline && connection.state === 'done' && <NewVersion available={canUpdate}/>}
    </div>
  );
};
