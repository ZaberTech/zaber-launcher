import type { SetDeviceStateExceptionData, SetPeripheralStateExceptionData } from '@zaber/motion';
import type { CanSetStateAxisResponse, SetStateAxisResponse, SetStateDeviceResponse } from '@zaber/motion/ascii';

import type { IdentifiedAxisInfo } from '../connection_manager';
import type { EntityKey } from '../keys';

export type PeripheralCompatibilityError = CanSetStateAxisResponse & { error: NonNullable<CanSetStateAxisResponse['error']> };

export type LoadStateCompatibility = {
  type: 'controller' | 'axis' | 'device';
  error?: string | null;
  peripheralErrors: PeripheralCompatibilityError[] | null;
};

export interface LoadErrorData {
  message: string;
  details?: SetDeviceStateExceptionData | SetPeripheralStateExceptionData;
  recommendRestore?: boolean;
}

type TargetInfo = {
  name: string;
  key: EntityKey;
  serialNumber: number;
  deviceProductId: number;
};

type SaveLoadDeviceTarget = TargetInfo & {
  type: 'device' | 'controller';
  address: number;
  axes: IdentifiedAxisInfo[];
  description: string;
};

type SaveLoadAxisTarget = TargetInfo & {
  type: 'axis';
  axisNumber: number;
  description: 'axis';
  peripheralId: number;
};

type BadTarget = {
  type: 'unidentified' | 'not-a-device';
};

export type SaveLoadTarget = SaveLoadDeviceTarget | SaveLoadAxisTarget;
export type SaveLoadTargetMaybe = SaveLoadTarget |  BadTarget;

export type LoadOpenContext = {
  fileName: string;
  saveState: string;
  /** A peripheral ID to set before loading if required */
  setPeripheralId?: number;
};

export type SaveLoadOpen = {
  step: 'choose' | 'saved';
} | {
  step: 'choosing-file';
} | {
  step: 'working';
  message: string;
} | {
  step: 'save-error';
  error: string;
} | {
  step: 'load-error';
  error: LoadErrorData;
} | {
  step: 'restored';
} | {
  step: 'restore-error';
  error: string;
};

export type LoadOpen = {
  step: 'load-check-result';
  compatibility: LoadStateCompatibility;
  context: LoadOpenContext;
} | {
  step: 'loaded';
  context: LoadOpenContext;
  response: SetStateDeviceResponse | SetStateAxisResponse;
};

export type SaveLoadUiState = SaveLoadOpen | LoadOpen;
