import { changeDictionary } from '@zaber/toolbox';

import { EntityKey, extractConnectionKey } from '../keys';
import { createReducer } from '../utils';
import { ConnectionManagerActionTypes, ConnectionManagerActionPayloads, DevicesLoadedPayload } from '../connection_manager';
import { DeviceIdentifiers, VersionList } from '../app_components';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { UpgradeProgress, VersionSkipMap } from './types';

export interface ConnectionState {
  key: EntityKey;
  identifiers: DeviceIdentifiers[];
  versions: VersionList;
  error: string | null;
  state: 'loading' | 'error' | 'done';
}

export interface State {
  selectedDeviceKey: EntityKey | null;
  progress: UpgradeProgress | null;
  connections: Record<string, ConnectionState>;
  versionsSkipped: VersionSkipMap;
}

const initialState: State = {
  selectedDeviceKey: null,
  progress: null,
  connections: {},
  versionsSkipped: {},
};

const setSelectedDevice = (state: State, { deviceKey }: ActionsToPayloads[ActionTypes.SET_SELECTED_DEVICE]): State => ({
  ...state,
  selectedDeviceKey: deviceKey,
});

const firmwareVersionLoaded = (
  state: State,
  { connectionKey, identifiers, versions }: ActionsToPayloads[ActionTypes.FIRMWARE_VERSIONS_LOADED]
) => ({
  ...state,
  connections: changeDictionary(state.connections, connectionKey, { state: 'done', identifiers, versions })
});

const firmwareVersionErr = (state: State, { connectionKey, error }: ActionsToPayloads[ActionTypes.FIRMWARE_VERSIONS_ERR]) => ({
  ...state,
  connections: changeDictionary(state.connections, connectionKey, { state: 'error', error })
});

const setVersionsSkipped = (state: State, { versionsSkipped }: ActionsToPayloads[ActionTypes.SET_VERSIONS_SKIPPED]): State => ({
  ...state,
  versionsSkipped,
});

const setInstallProgress = (state: State, { progress }: ActionsToPayloads[ActionTypes.SET_INSTALL_PROGRESS]): State => ({
  ...state,
  progress,
});

const refreshVersions = (state: State, { deviceKey }: ActionsToPayloads[ActionTypes.REFRESH_VERSIONS]): State => ({
  ...state,
  connections: changeDictionary(state.connections, extractConnectionKey(deviceKey), {
    state: 'loading',
    identifiers: [],
    versions: {},
    error: null,
  }),
});

const devicesLoaded = (state: State, { connectionKey }: DevicesLoadedPayload): State => ({
  ...state,
  connections: {
    ...state.connections,
    [connectionKey]: {
      key: connectionKey,
      state: 'loading',
      identifiers: [],
      versions: {},
      error: null,
    },
  },
});

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.SET_SELECTED_DEVICE]: setSelectedDevice,
  [ActionTypes.REFRESH_VERSIONS]: refreshVersions,
  [ActionTypes.FIRMWARE_VERSIONS_LOADED]: firmwareVersionLoaded,
  [ActionTypes.FIRMWARE_VERSIONS_ERR]: firmwareVersionErr,
  [ActionTypes.SET_VERSIONS_SKIPPED]: setVersionsSkipped,
  [ActionTypes.SET_INSTALL_PROGRESS]: setInstallProgress,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
}, initialState);
