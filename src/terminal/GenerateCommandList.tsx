import _ from 'lodash';

import type { selectConnections, selectDevices, selectAxes } from '../connection_manager';
import { getEntityType, type EntityKey, EntityKeyType } from '../keys';
import { UnitDefinition } from '../units';

import type { Command } from './types';
import { commandTreeToCommandList, getUnitDefinitions } from './utils';


function areCommandsEqual(commandA: Command, commandB: Command): boolean {
  if (commandA.parts.length !== commandB.parts.length) {
    return false;
  }
  return commandA.parts.every((partA, index) => {
    const partB = commandB.parts[index];
    return _.isEqual(partA.text, partB.text);
  });
}

function mergeCommandLists(commandListA: Command[], commandListB: Command[]): Command[] {
  const commandList = _.cloneDeep(commandListA);
  commandListB.forEach(commandB => {
    const command = commandList.find(command => areCommandsEqual(command, commandB));
    if (command == null) {
      commandList.push(commandB);
    }
  });
  return commandList;
}

function stripUnitsFromCommandList(commandList: Command[]): Command[] {
  return commandList.map(command => ({
    ...command,
    parts: command.parts.map(part => {
      if (part.type === 'param') {
        return { ...part, units: [] };
      } else {
        return part;
      }
    }),
  }));
}

function getCommandListForAxis(axes: ReturnType<typeof selectAxes>, entityKey: EntityKey): Command[] {
  const axis = axes[entityKey];

  if (axis?.product) {
    return commandTreeToCommandList(axis.product.commandTree, axis.product);
  }

  return [];
}

function getCommandListForDevice(
  devices: ReturnType<typeof selectDevices>,
  axes: ReturnType<typeof selectAxes>,
  entityKey: EntityKey): Command[] {
  const device = devices[entityKey];

  if (device?.product != null) {
    const commandListDevice = commandTreeToCommandList(device.product.commandTree, device.product);
    const commandListAxes = device.axes.reduce((commandList: Command[], axisKey) =>
      mergeCommandLists(commandList, getCommandListForAxis(axes, axisKey)),
    []);
    return mergeCommandLists(commandListDevice, stripUnitsFromCommandList(commandListAxes));
  }

  return [];
}

function getCommandListForConnection(
  connections: ReturnType<typeof selectConnections>,
  devices: ReturnType<typeof selectDevices>,
  axes: ReturnType<typeof selectAxes>,
  entityKey: EntityKey): Command[] {
  const connection = connections[entityKey];

  if (connection.devices != null) {
    const commandList = connection.devices.reduce((commandList: Command[], deviceKey) =>
      mergeCommandLists(commandList, getCommandListForDevice(devices, axes, deviceKey)),
    []);
    return stripUnitsFromCommandList(commandList);
  }

  return [];
}

function getSettingCommands(name: string, units?: UnitDefinition[]): Command[] {
  return [
    {
      type: 'setting',
      parts: [
        { type: 'command', text: 'get' },
        { type: 'command', text: name }
      ]
    },
    {
      type: 'setting',
      parts: [
        { type: 'command', text: 'set' },
        { type: 'command', text: name },
        { type: 'param', text: 'value', multiple: false, units: units ?? [] }
      ]
    }
  ];
}

function getSettingCommandListForAxis(
  axes: ReturnType<typeof selectAxes>,
  entityKey: EntityKey,
): Command[] {
  const axis = axes[entityKey];

  if (axis?.product != null) {
    const productInfo = axis.product;
    return axis.product.settings.rows.flatMap(setting => {
      if (setting.contextual_dimension_id != null) {
        const units = getUnitDefinitions(productInfo, setting.contextual_dimension_id);
        return getSettingCommands(setting.name, units);
      } else {
        return getSettingCommands(setting.name);
      }
    });
  } else {
    return [];
  }
}

function getSettingCommandListForDevice(
  devices: ReturnType<typeof selectDevices>,
  axes: ReturnType<typeof selectAxes>,
  entityKey: EntityKey,
): Command[] {
  const device = devices[entityKey];

  if (device?.product != null) {
    const productInfo = device.product;
    const commandListDevice = device.product.settings.rows.flatMap(setting => {
      if (setting.contextual_dimension_id != null) {
        const units = getUnitDefinitions(productInfo, setting.contextual_dimension_id);
        return getSettingCommands(setting.name, units);
      } else {
        return getSettingCommands(setting.name);
      }
    });

    const commandListAxes = device.axes.reduce((commandList: Command[], axisKey) =>
      mergeCommandLists(commandList, getSettingCommandListForAxis(axes, axisKey)),
    []);
    return mergeCommandLists(commandListDevice, stripUnitsFromCommandList(commandListAxes));
  } else {
    return [];
  }
}

function getSettingCommandListForConnection(
  connections: ReturnType<typeof selectConnections>,
  devices: ReturnType<typeof selectDevices>,
  axes: ReturnType<typeof selectAxes>,
  entityKey: EntityKey,
): Command[] {
  const connection = connections[entityKey];

  if (connection.devices != null) {
    const commandList = connection.devices.reduce((commandList: Command[], deviceKey) =>
      mergeCommandLists(commandList, getSettingCommandListForDevice(devices, axes, deviceKey)),
    []);
    return stripUnitsFromCommandList(commandList);
  }

  return [];
}

export function generateCommandList(
  connections: ReturnType<typeof selectConnections>,
  devices: ReturnType<typeof selectDevices>,
  axes: ReturnType<typeof selectAxes>,
  entityKey: EntityKey): Command[] {
  switch (getEntityType(entityKey)) {
    case EntityKeyType.CONNECTION: {
      return [
        ...getCommandListForConnection(connections, devices, axes, entityKey),
        ...getSettingCommandListForConnection(connections, devices, axes, entityKey),
      ];
    }
    case EntityKeyType.DEVICE: {
      return [
        ...getCommandListForDevice(devices, axes, entityKey),
        ...getSettingCommandListForDevice(devices, axes, entityKey),
      ];
    }
    case EntityKeyType.AXIS: {
      return [
        ...getCommandListForAxis(axes, entityKey),
        ...getSettingCommandListForAxis(axes, entityKey),
      ];
    }
    default:
      return [];
  }
}
