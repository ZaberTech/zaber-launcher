import _ from 'lodash';

import { EntityKey, isSubKeyOf } from '../keys';
import { createReducer } from '../utils';
import { ConnectionManagerActionTypes, ConnectionManagerActionPayloads } from '../connection_manager';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { UnitInfo } from './types';

type DeviceUnits = {
  settings: Partial<Record<string, UnitInfo>>;
  commands: Partial<Record<string, UnitInfo>>;
};

export interface State {
  info: Partial<Record<EntityKey, DeviceUnits>>;
}

const initialState: State = {
  info: {},
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;
type CmReducer<AT extends ConnectionManagerActionTypes> = (state: State, payload: ConnectionManagerActionPayloads[AT]) => State;

const devicesReload: CmReducer<ConnectionManagerActionTypes.DEVICES_LOADED> = (state, { connectionKey }) => ({
  info: _.pickBy(state.info, (_, key) => !isSubKeyOf(key, connectionKey))
});

const fetchSettingInfoDone: Reducer<ActionTypes.FETCH_SETTING_UNIT_INFO_DONE> = (state, { entityKey, setting, info }) => {
  const deviceUnitInfo = state.info[entityKey] ?? { settings: {}, commands: {} };
  return {
    ...state,
    info: {
      ...state.info,
      [entityKey]: {
        ...deviceUnitInfo,
        settings: {
          ...deviceUnitInfo.settings,
          [setting]: info,
        },
      }
    },
  };
};

const fetchCommandInfoDone: Reducer<ActionTypes.FETCH_COMMAND_UNIT_INFO_DONE> = (state, { entityKey, command, info }) => {
  const deviceUnitInfo = state.info[entityKey] ?? { settings: {}, commands: {} };
  return {
    ...state,
    info: {
      ...state.info,
      [entityKey]: {
        ...deviceUnitInfo,
        commands: {
          ...deviceUnitInfo.commands,
          [command]: info,
        },
      }
    },
  };
};

export const reducer = createReducer<ActionsToPayloads | ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.FETCH_SETTING_UNIT_INFO_DONE]: fetchSettingInfoDone,
  [ActionTypes.FETCH_COMMAND_UNIT_INFO_DONE]: fetchCommandInfoDone,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesReload,
}, initialState);
