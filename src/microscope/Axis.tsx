import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, HelpTooltip, Icons, Text } from '@zaber/react-library';
import _ from 'lodash';
import type { Length } from '@zaber/motion';

import { useActions } from '../utils';
import { Shortcut } from '../components';
import { EditableInputWithUnits, measurementOK, EditableInputValueState } from '../units';

import { selectAxes, selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations, type MonitorInfo, Axes, POSITION_DIMENSION, AXES, AXES_LABELS } from './types';
import { OperationError } from './components';
import { StorePositionButton, StoredPositions } from './StoredPositions';
import { getPrecision } from './utils';

const MOVE_OPERATIONS = {
  focusAxis: Operations.moveFocus,
  xAxis: Operations.moveX,
  yAxis: Operations.moveY,
};

const SHORTCUTS: Record<Axes, [string, string]> = {
  focusAxis: ['PageUp', 'PageDown'],
  xAxis: ['ArrowLeft', 'ArrowRight'],
  yAxis: ['ArrowUp', 'ArrowDown'],
};

type AxisMonitorInfo = NonNullable<MonitorInfo['xAxis' | 'yAxis' | 'focusAxis']>;

const Position: React.FC<{
  axisType: Axes;
  info: AxisMonitorInfo;
}> = ({ axisType, info }) => {
  const actions = useActions(actionsDefinition);

  const [inputState, setInputState] = useState<EditableInputValueState | null>(null);
  const measure = inputState ?? info.position;

  return (
    <EditableInputWithUnits
      className="position"
      title="Position"
      mode={inputState?.mode ?? 'displaying'}
      displayPrecision={getPrecision(measure)}
      unitFrom={{ dimension: POSITION_DIMENSION }}
      measure={measure}
      onChange={position => {
        switch (position.mode) {
          case 'displaying':
            setInputState(null);
            break;
          case 'writing':
            setInputState(null);
            actions.operationStart(Operations.moveAbsolute, { axis: axisType, position });
            break;
          case 'editing':
            setInputState(position);
            break;
        }

        if (position.units !== info.position.units) {
          actions.updateUnits(axisType, position.units as Length);
        }
      }}/>);
};

const Step: React.FC<{
  axisType: Axes;
}> = ({ axisType }) => {
  const actions = useActions(actionsDefinition);
  const state = useSelector(selectAxes)[axisType];
  const [stepEdit, setStepEdit] = useState<EditableInputValueState | null>(null);

  return (
    <EditableInputWithUnits
      className="step"
      title="Step"
      unitFrom={{ dimension: POSITION_DIMENSION }}
      displayPrecision={3}
      mode={stepEdit?.mode ?? 'displaying'}
      measure={stepEdit ?? state.step}
      onChange={step => {
        switch (step.mode) {
          case 'displaying':
            if (measurementOK(step)) {
              actions.updateAxisStep(axisType, step);
            }
            setStepEdit(null);
            break;
          case 'writing':
            actions.updateAxisStep(axisType, step);
            setStepEdit(null);
            break;
          case 'editing':
            setStepEdit(step);
            break;
        }
      }}/>);
};

const Axis: React.FC<{
  axisType: Axes;
}> = ({ axisType }) => {
  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.[axisType];
  const info = monitor.info?.[axisType];

  if (stableInfo == null || info == null) {
    return null;
  }

  return <>
    <span>{AXES_LABELS[axisType]}</span>

    <Position axisType={axisType} info={info}/>

    <Step axisType={axisType}/>
  </>;
};

function moveTypeFromEvent(e: { shiftKey: boolean }): 'gradual' | 'steady' {
  return e.shiftKey ? 'steady' : 'gradual';
}

const AxisJogs: React.FC<{
  axisType: Axes;
  titleNegative: string;
  titlePositive: string;
  iconNegative?: typeof Icons.LeftNormal;
  iconPositive?: typeof Icons.RightNormal;
}> = ({
  axisType, titlePositive, titleNegative, iconNegative, iconPositive
}) => {
  const actions = useActions(actionsDefinition);
  const monitorState = useSelector(selectMonitorState);
  const stableInfo = monitorState.stableInfo?.[axisType];

  const [[movingNeg, movingPos], setMoving] = useState([false, false]);

  const stop = () => {
    setMoving([false, false]);
    actions.stopAxisMovement(axisType);
  };
  const move = (positive: boolean, move: 'gradual' | 'steady') => {
    if (stableInfo == null) { return }
    setMoving([!positive, positive]);
    const devicePositive = positive !== stableInfo.inverted;
    actions.operationStart(MOVE_OPERATIONS[axisType], { positive: devicePositive, move });
  };

  const className = _.kebabCase(axisType);
  const IconNegativeCtor = iconNegative ?? Icons.LeftNormal;
  const IconPositiveCtor = iconPositive ?? Icons.RightNormal;

  return <>
    <IconNegativeCtor appearsClickable className={`neg ${className}`} title={titleNegative}
      disabled={stableInfo == null}
      activated={movingNeg}
      onMouseDown={e => move(false, moveTypeFromEvent(e))}
      onMouseUp={stop} onMouseLeave={stop}/>
    <IconPositiveCtor appearsClickable className={`pos ${className}`} title={titlePositive}
      disabled={stableInfo == null}
      activated={movingPos}
      onMouseDown={e => move(true, moveTypeFromEvent(e))}
      onMouseUp={stop} onMouseLeave={stop}/>

    {[0, 1].map(direction =>
      <Shortcut key={direction}
        k={SHORTCUTS[axisType][direction]} shift="ignore"
        onDown={e => move(Boolean(direction), moveTypeFromEvent(e))}
        onUp={stop}
        preventDefault/>)}
  </>;
};

const AxisErrors: React.FC = () => (<>
  {[
    Operations.moveAbsolute,
    Operations.moveFocus,
    Operations.moveX,
    Operations.moveY,
  ].map(operation => <OperationError key={operation} operation={operation}/>)}
</>);

const HELP = <HelpTooltip>
  <Text t={Text.Type.Body}>
    Use the arrow keys to move the microscope plate.<br/>
    Use the Page Down and Page Up keys to move the focus axis.<br/>
    Combine with Shift key to move at constant high speed (maximum 10% of travel range per second).
  </Text>
</HelpTooltip>;

export const Motion: React.FC = () => (
  <HeaderCard
    className="motion controls"
    header={<>
      <Text t={Text.Type.H4}>Position</Text>
      <div className="icons"><StorePositionButton/>{HELP}</div>
    </>}>
    <div className="table-and-arrows">
      <div className="table">
        <div className="theader">Axis</div>
        <div className="theader pos">Position</div>
        <div className="theader step">Step</div>

        {AXES.map(axisType =>
          <Axis key={axisType} axisType={axisType}/>
        )}
      </div>

      <div className="arrows">
        <div className="cross">
          <AxisJogs axisType="xAxis" titleNegative="Move Left" titlePositive="Move Right"/>
          <AxisJogs axisType="yAxis" titleNegative="Move Up" titlePositive="Move Down"/>
        </div>
        <div className="focus">
          <AxisJogs axisType="focusAxis"
            titleNegative="Focus Out" titlePositive="Focus In"/>
        </div>
      </div>
    </div>

    <AxisErrors/>

    <StoredPositions/>
  </HeaderCard>);
