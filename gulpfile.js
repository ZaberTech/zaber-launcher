/* eslint-disable no-console */
/* eslint-disable camelcase */
const path = require('path');
const os = require('os');
const fs = require('fs').promises;
const fs2 = require('fs');
const { https } = require('follow-redirects');
const decompress = require('decompress');
const {
  isCI, shouldSign, exec, currentEdition, editions, currentFlavor, flavors,
  readJson, writeJson, readYaml, writeYaml, isOffline,
} = require('./scripts/common');
const util = require('util');
const { series, parallel } = require('gulp');
const rimraf = util.promisify(require('rimraf'));
const _ = require('lodash');
const replaceInFile = require('replace-in-file');
const AWS = require('aws-sdk');
const yml = require('yaml');
const glob = util.promisify(require('glob'));
const jsonSchemaToTypescript = require('json-schema-to-typescript');
const { getLatestDatabaseFile, signFile } = require('@zaber/tools');
const { generateCard } = require('./scripts/gchat');

const electronBuilderFile = path.join(__dirname, 'electron-builder.yml');

const PACKAGE_JSON = JSON.parse(fs2.readFileSync(path.join(__dirname, 'package.json'), 'utf-8'));

const dist = async () => {
  if (!isCI) {
    console.warn('This installer won\'t be published for automatic update.');
  }

  const platform = os.platform();
  let arch = '';
  if (platform === 'linux') {
    // Electron builder does not detect architecture correctly for arm.
    // Specifying architecture explicitly fixes it.
    switch (os.arch()) {
      case 'arm':
        arch = '--armv7l';
        break;
      case 'arm64':
        arch = '--arm64';
        break;
    }
  } else if (platform === 'darwin' || platform === 'win32') {
    if (platform === 'darwin') {
      arch = '--universal';
    } else {
      arch = '--x64 --arm64';
    }
    // For universal build, ZML bindings required for both architectures
    switch (os.arch()) {
      case 'x64':
        await exec('npm rebuild @zaber/motion --target_arch=arm64');
        break;
      case 'arm64':
        await exec('npm rebuild @zaber/motion --target_arch=x64');
        break;
    }
  }

  await exec(`npm run dist:${os.platform()} -- --publish ${isCI ? 'always' : 'never'} ${arch}`);

  if (os.platform() === 'win32' && os.arch() === 'x64' && currentEdition === editions.PUBLIC) {
    await dist_inno();
  }
};

const dist_inno = async () => {
  const distDir = path.join(__dirname, 'dist');

  const disableUpdates = path.join(distDir, 'disable-updates.txt');
  await fs.writeFile(disableUpdates, 'This file disables automatic updates.');

  const template = await fs.readFile(path.join(__dirname, 'scripts', 'installer.iss'), 'utf-8');
  const script = template.replace(/\$\$(\w+)\$\$/g, (match, name) => {
    switch (name) {
      case 'NAME': return PACKAGE_JSON.productName;
      case 'VERSION': return PACKAGE_JSON.version;
      default: throw new Error(`Unknown variable: ${name}`);
    }
  });
  const scriptPath = path.join(distDir, 'installer.iss');
  await fs.writeFile(scriptPath, script);

  await exec(`"C:\\Program Files (x86)\\Inno Setup 6\\ISCC.exe" "${scriptPath}"`, { maxBuffer: 10 * (2 ** 20) });

  for (const file of [scriptPath, disableUpdates]) {
    await fs.unlink(file);
  }

  const installerFiles = await glob('*Alt-Setup*.exe', { cwd: distDir });
  if (installerFiles.length !== 1) {
    throw new Error(`Installer not found. ${installerFiles.join()}`);
  }
  const installer = installerFiles[0];
  const installerPath = path.join(distDir, installer);

  if (shouldSign) {
    await signFile(installerPath);
  }

  if (isCI) {
    const stableName = installer.replace(/Alt-Setup.*$/, 'Alt-Setup.exe').replace(/ /g, '');

    const electronBuilder = await readYaml(electronBuilderFile);
    const s3 = new AWS.S3();

    for (const name of [installer, stableName]) {
      await s3.putObject({
        Bucket: electronBuilder.publish.bucket,
        Key: `${electronBuilder.publish.path}${name}`,
        ACL: currentEdition === editions.PUBLIC ? 'public-read' : undefined,
        ContentDisposition: `attachment; filename=${encodeURI(name)}`,
        Body: fs2.createReadStream(installerPath),
      }).promise();
    }
  }
};

const build = async () => {
  await exec('npm run build', {
    env: {
      ...process.env,
      NODE_ENV: 'production',
    },
  });
};

const test_only = async () => {
  if (isCI) {
    await exec('npm run test:clear-cache');
  }
  await exec('npm run test');
};

const lint = async () => {
  await exec('npm run lint');
};

const test = series(lint, test_only);

const getDownloadOsArch = (platform, arch) => {
  switch (platform) {
    case 'win32':
      return 'win64';
    case 'darwin':
      switch (arch) {
        case 'x64':
          return 'darwin64';
        case 'arm64':
          return 'darwinarm64';
      }
      break;
    case 'linux':
      switch (arch) {
        case 'x64':
          return 'linux64';
        case 'x32':
        case 'ia32':
          return 'linux32';
        case 'arm':
          return 'linuxarm';
        case 'arm64':
          return 'linuxarm64';
      }
      break;
  }
  throw new Error(`Not supported: ${platform}-${arch}`);
};

const download_executable = async (name, executable, arch) => {
  const version = PACKAGE_JSON[name];
  console.log(`Downloading ${name} ${arch} version ${version}...`);

  const osArch = getDownloadOsArch(os.platform(), arch);

  const extractTo = path.join(__dirname, name);
  const zipFile = `${extractTo}.zip`;

  const zipFileStream = fs2.createWriteStream(zipFile);
  const request = https.get(
    `https://api.zaber.io/downloads/software-downloads?product=${name}_${osArch}&version=${version}`,
    response => {
      if (response.statusCode !== 200) {
        throw new Error(`Code ${response.statusCode} !== 200`);
      }
      response.pipe(zipFileStream);
    });
  request.end();

  await new Promise((resolve, reject) => {
    zipFileStream.once('close', resolve);
    zipFileStream.once('error', reject);
    request.once('error', reject);
  });

  await decompress(zipFile, extractTo);
  await rimraf(zipFile);

  const executablePath = path.join(extractTo, executable);

  if (os.platform() == 'win32') {
    await fs.rename(`${executablePath}.exe`, `${executablePath}-${arch}.exe`);
  } else {
    await exec(`chmod +x ${executablePath}`);
    await fs.rename(executablePath, `${executablePath}-${arch}`);
  }
};

const download_folder = async (name, executable) => {
  await rimraf(path.join(__dirname, name));
  // Mac and Win builds universal installer, requires both binaries
  const platform = os.platform();
  if (platform === 'darwin' || platform === 'win32') {
    await download_executable(name, executable, 'x64');
    await download_executable(name, executable, 'arm64');
  } else {
    await download_executable(name, executable, os.arch());
  }
};

const download_message_router = async () => {
  await download_folder('message_router', 'zaber-message-router');
};

const download_db_service = async () => {
  await download_folder('device_db_service', 'zaber-device-db-service');
};

const download_db = async () => {
  const db_at = await getLatestDatabaseFile();
  await fs.rename(db_at, path.join(__dirname, 'device_db_service', 'device_database.sqlite'));
};

const download_db_and_service = parallel(download_db_service, download_db);

const download_protocol_manual = async () => {
  console.log('Downloading latest protocol documentation...');
  const extractTo = path.join(__dirname, 'protocol_manual');
  await rimraf(extractTo);

  const getPublic = currentEdition === editions.PUBLIC;
  const distUrl = getPublic ?
    'https://www.zaber.com/software/protocol-manual/v1/dist.zip' :
    'https://zshared.izaber.com/software/protocol-manual/dist.zip';


  const zipFile = `${extractTo}.zip`;
  const zipFileStream = fs2.createWriteStream(zipFile);
  const request = https.get(
    distUrl,
    response => {
      if (response.statusCode !== 200) {
        throw new Error(`Code ${response.statusCode} !== 200`);
      }
      response.pipe(zipFileStream);
    });
  request.end();

  await new Promise((resolve, reject) => {
    zipFileStream.once('close', resolve);
    zipFileStream.once('error', reject);
    request.once('error', reject);
  });

  await decompress(zipFile, extractTo);
  await rimraf(zipFile);

  console.log('Moving assets from the protocol manual into place...');

  const cpToSrc = async (filename, toName) => {
    const from = path.join(__dirname, 'protocol_manual', filename);
    const to = path.join(__dirname, 'src', 'protocol_manual', toName ?? filename);
    await fs.copyFile(from, to);
  };

  await cpToSrc('ascii-7.html');
  await cpToSrc('commonltr.css');
  await cpToSrc(getPublic ? 'external_zc.css' : 'internal_zc.css', 'protocol_manual.css');
};

const get_protocol_manual_data = async () => {
  console.log('downloading protocol manual data');
  const writeTo = path.join(__dirname, 'src', 'protocol_manual', 'settings.json');

  await new Promise((resolve, reject) => https.get(
    'https://jenkins.izaber.com/job/protocol-manual/job/master/lastStableBuild/artifact/ascii/7/external/json/settings.json',
    response => {
      if (response.statusCode !== 200) {
        return reject(`Code ${response.statusCode} !== 200`);
      }

      const file = fs2.createWriteStream(writeTo);
      file.on('close', resolve);
      file.on('error', reject);

      response.pipe(file);
      response.on('error', reject);
    }
  ).on('error', reject));
};

const get_protocol_manual = series(download_protocol_manual, get_protocol_manual_data);

const hack_mqtt_module = async () => {
  // mqtt module used to have unconventional index.js which was also an executable.
  // aws-iot-device-sdk-js uses the index (requiring old version).
  // shebang breaks loading of the module in electron.
  await replaceInFile({
    files: 'node_modules/mqtt/mqtt.js',
    from: '#!/usr/bin/env node',
    to: '',
  });
};

const rebuild_native_modules = async () => {
  await exec('npx electron-rebuild --only node-pty');
};

const post_install = series(download_message_router, get_protocol_manual, hack_mqtt_module, rebuild_native_modules);

const prepare_edition = async () => {
  const packageFile = path.join(__dirname, 'package.json');
  const packageJson = await readJson(packageFile);

  const electronBuilder = await readYaml(electronBuilderFile);

  if (currentFlavor === flavors.REXROTH) {
    packageJson.name = 'rexroth-dashboard';
    packageJson.productName = 'Rexroth Dashboard';
    packageJson.description = '';
    electronBuilder.appId = 'com.zaber.rexroth-dashboard';
  }

  if (currentEdition === editions.DEV) {
    packageJson.version = `${get_version_date()}-${process.env.CI_PIPELINE_ID || 0}`;
  }
  if (currentEdition !== editions.PUBLIC) {
    packageJson.name += `-${currentEdition}`;
    packageJson.productName += ` ${_.capitalize(currentEdition)}`;
    electronBuilder.appId += `-${currentEdition}`;
    electronBuilder.publish.bucket += `-${currentEdition}`;
    electronBuilder.publish.acl = null; // ACL disabled on private buckets
  }

  if (isOffline) {
    packageJson.name += '-offline';
    packageJson.productName += ' Offline';
    electronBuilder.appId += '-offline';
  }

  const publishPath = [];
  // The edition is part of both bucket and path to maintain the legacy structure.
  if (currentFlavor !== flavors.ZABER) {
    publishPath.push(currentFlavor);
  }
  publishPath.push(currentEdition);
  if (isOffline) {
    publishPath.push('offline');
  }
  electronBuilder.publish.path = `${publishPath.join('-')}/`;

  await writeJson(packageFile, packageJson);
  await writeYaml(electronBuilderFile, electronBuilder);

  if (currentFlavor === flavors.ZABER) {
    const iconDir = isOffline ? 'offline' : currentEdition;
    if (iconDir !== editions.PUBLIC) {
      for (const file of await glob(`./icons/${iconDir}/**/*`)) {
        await fs.copyFile(file, file.replace(`/${iconDir}`, ''));
      }
    }
  } else if (currentFlavor === flavors.REXROTH) {
    await replaceInFile({
      files: 'LICENSE.txt',
      from: /.*Zaber.*\r?\n\r?\n/,
      to: '',
    });
    await replaceInFile({
      files: 'icons/site.webmanifest',
      from: /Zaber Launcher/g,
      to: 'Rexroth Dashboard',
    });

    for (const file of await glob('./src/assets/rexroth/replace/**/*')) {
      await fs.copyFile(file, file.replace('/rexroth/replace', ''));
    }

    for (const file of await glob('./icons/rexroth/**/*')) {
      await fs.copyFile(file, file.replace('/rexroth', ''));
    }
  }
};

const copy_latest_version = async () => {
  if (!isCI) {
    return;
  }

  const electronBuilder = await readYaml(electronBuilderFile);
  let latestFileSuffix = '';
  if (os.platform() === 'darwin') {
    latestFileSuffix = '-mac';
  } else if (os.platform() === 'linux') {
    latestFileSuffix = '-linux';
    if (['arm', 'arm64'].includes(os.arch())) {
      latestFileSuffix += `-${os.arch()}`;
    }
  }

  const s3 = new AWS.S3();
  const response = await s3.getObject({
    Bucket: electronBuilder.publish.bucket,
    Key: `${electronBuilder.publish.path}latest${latestFileSuffix}.yml`,
  }).promise();
  const latestData = yml.parse(response.Body.toString('utf-8'));
  console.log('Version', latestData.version);

  const installerFileName = latestData.files.find(file => file.url.match(/(exe|dmg|AppImage)$/)).url;
  const latestFileName = installerFileName
    .replace(latestData.version, '')
    .replace(/[ -]\./, '.')
    .replace('--', '-')
    .replace(/ /g, '');

  await s3.copyObject({
    CopySource: `${electronBuilder.publish.bucket}/${electronBuilder.publish.path}${installerFileName}`,
    Bucket: electronBuilder.publish.bucket,
    Key: `${electronBuilder.publish.path}${latestFileName}`,
    ACL: currentEdition === editions.PUBLIC ? 'public-read' : undefined,  // ACL disabled on private buckets
    ContentDisposition: `attachment; filename=${encodeURI(latestFileName)}`,
    MetadataDirective: 'REPLACE',
  }).promise();
};

const generate_schema_types = async () => {
  const FILES = [
    ['src/internal/production_app/metadata/metadata.schema.yml', 'metadata_schema.ts'],
    ['src/internal/scripting/metadata.schema.yml', 'metadata_schema.ts'],
  ];
  for (const [schemaFile, targetFile] of FILES) {
    const schema = await readYaml(schemaFile);
    const schemaName = _.camelCase(path.basename(targetFile).match(/[^.]+/)[0]);
    const typeText = await jsonSchemaToTypescript.compile(schema, schemaName);
    const finalText = [
      '/* eslint-disable */',
      '// To generate run: npx gulp generate_schema_types',
      typeText,
    ];
    await fs.writeFile(path.join(path.dirname(schemaFile), targetFile), finalText.join(os.EOL));
  }
};

const set_version = async () => {
  const newVersion = get_version_date();

  const oldVersionMatch = PACKAGE_JSON.version.match(/(?<version>\d+\.\d+\.\d+)-(?<build>\d+)/);

  if (oldVersionMatch && oldVersionMatch.groups.version === newVersion) {
    await exec(`npm version ${newVersion}-${parseInt(oldVersionMatch.groups.build) + 1}`);
  } else {
    await exec(`npm version ${newVersion}-1`);
  }
};

const get_version_date = () => {
  const date = new Date();
  return `${date.getFullYear()}.${date.getMonth() + 1}.${date.getDate()}`;
};

const broadcast = async () => {
  const edition = process.env.ZL_OFFLINE === 'true' ? 'offline' : currentEdition;
  if (edition == null) {
    return;
  }
  const changelog = getChangelog(PACKAGE_JSON.version);
  const card = generateCard(edition, PACKAGE_JSON.version, changelog);

  const webhookUrls = process.env.GCHAT_WEBHOOK_URLS?.split(' ');
  if (webhookUrls) {
    for (const webhookUrl of webhookUrls) {
      await fetch(webhookUrl, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json; charset=UTF-8' },
        body: JSON.stringify(card)
      });
    }
  }
};

const getChangelog = version => {
  const changelog = fs2.readFileSync(path.join(__dirname, 'CHANGELOG.md'), 'utf-8');
  const versionIndex = changelog.indexOf(version);
  const lastChangeStartIndex = changelog.indexOf('*', versionIndex);
  const lastChangeEndIndex = changelog.indexOf('##', versionIndex);
  return changelog.slice(lastChangeStartIndex, lastChangeEndIndex).trim();
};

module.exports = {
  dist,
  dist_inno,
  test,
  test_only,
  build,
  lint,
  download_message_router,
  download_db_service,
  download_db,
  download_db_and_service,
  download_protocol_manual,
  get_protocol_manual_data,
  get_protocol_manual,
  post_install,
  prepare_edition,
  copy_latest_version,
  generate_schema_types,
  set_version,
  broadcast,
};
