import React from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import { routerActions } from 'connected-react-router';

import { createContainer, destroyContainer } from '../container';
import { wrapWithNewStore, wrapWithRouter, mockStorage, StorageMock } from '../test';

import { Menu } from './Menu';
import { MENU_PINNED_STORAGE_KEY, setPinnedAfterStart } from './sagas';

const TestMenu = wrapWithNewStore(wrapWithRouter(Menu));

let wrapper: RenderResult;
let storageMock: StorageMock;

beforeEach(() => {
  const container = createContainer();
  storageMock = mockStorage(container);
});

afterEach(() => {
  destroyContainer();
});

describe('default render', () => {
  beforeEach(() => {
    wrapper = render(<TestMenu/>);
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;
  });

  test('renders collapsed', () => {
    expect(wrapper.getByTestId('menu')).toHaveClass('collapsed');
  });

  test('allows to pin and unpin the menu storing the state', () => {
    fireEvent.click(wrapper.getByTitle('Pin Menu'));
    expect(wrapper.getByTestId('menu')).toHaveClass('fixed');
    expect(storageMock.stored[MENU_PINNED_STORAGE_KEY]).toBe(true);

    fireEvent.click(wrapper.getByTitle('Pin Menu'));
    expect(wrapper.getByTestId('menu')).not.toHaveClass('fixed');
    expect(storageMock.stored[MENU_PINNED_STORAGE_KEY]).toBe(false);
  });

  test('loads pinned state on start', () => {
    storageMock.stored[MENU_PINNED_STORAGE_KEY] = true;
    TestMenu.testStore.saga.run(setPinnedAfterStart);
    expect(wrapper.getByTestId('menu')).toHaveClass('fixed');
  });

  test('keeps the menu extended for certain paths', () => {
    expect(wrapper.getByTestId('menu')).not.toHaveClass('fixed');
    TestMenu.testStore.dispatch(routerActions.push('/about'));
    expect(wrapper.getByTestId('menu')).toHaveClass('fixed');
  });

  test('extends the menu when mouse is over', () => {
    fireEvent.mouseEnter(wrapper.getByTestId('menu-container'));
    expect(wrapper.getByTestId('menu')).toHaveClass('extended');
    fireEvent.mouseLeave(wrapper.getByTestId('menu-container'));
    expect(wrapper.getByTestId('menu')).toHaveClass('collapsed');
  });

  test('stays fixed (not covering the content) when URL changes, collapsing afterwards', () => {
    TestMenu.testStore.dispatch(routerActions.push('/about'));
    expect(wrapper.getByTestId('menu')).toHaveClass('fixed');

    fireEvent.mouseEnter(wrapper.getByTestId('menu-container'));
    TestMenu.testStore.dispatch(routerActions.push('/should-be-collapsed-url'));
    expect(wrapper.getByTestId('menu')).toHaveClass('fixed');

    fireEvent.mouseLeave(wrapper.getByTestId('menu-container'));
    expect(wrapper.getByTestId('menu')).toHaveClass('collapsed');

    fireEvent.mouseEnter(wrapper.getByTestId('menu-container'));
    expect(wrapper.getByTestId('menu')).toHaveClass('extended');
  });
});
