import { createSelector } from 'reselect';
import _ from 'lodash';
import { categoriesMetadata, CategoryMetadata, Categories } from '@zaber/device-db-metadata';

import { selectDeviceSettings, RootState } from '../store';
import {
  selectAxes as selectManagerAxes,
  selectIdentifiedDevices as selectManagerDevices,
  ConnectionManagerAxisState,
  IdentifiedDeviceState,
  selectConnections,
} from '../connection_manager';
import { extractDeviceKey, EntityKey, tryExtractConnectionKey, getEntityType, EntityKeyType } from '../keys';
import { tryAccess } from '../utils';

import type { DeviceOrAxisState, SettingsEntry } from './reducer';
import { ACCESS_LEVEL_SETTING, DEFAULT_ACCESS_LEVEL, getValueAsString, NO_CATEGORY, NO_CATEGORY_META } from './types';

export interface DeviceOrAxisStateComplete extends DeviceOrAxisState {
  axis?: ConnectionManagerAxisState;
  device: IdentifiedDeviceState;
}

export const selectCurrentKey = createSelector(selectDeviceSettings, state => state.selectedDeviceOrAxis);

export const selectCurrentEntity = createSelector(selectDeviceSettings, selectManagerAxes, selectManagerDevices,
  (state, axes, devices): DeviceOrAxisStateComplete | null => {
    const key = state.selectedDeviceOrAxis;
    if (!key) { return null }

    const device = devices[extractDeviceKey(key)];
    if (!device) { return null }

    const axis = tryAccess(axes, key);
    if (axis == null && getEntityType(key) === EntityKeyType.AXIS) { return null }

    return ({
      device,
      axis,
      ...state.devicesAndAxes[key],
    });
  }
);

export const selectCurrentDevice = createSelector(selectDeviceSettings, selectManagerDevices,
  (state, devices): DeviceOrAxisStateComplete | null => {
    const deviceKey = state.selectedDeviceOrAxis && extractDeviceKey(state.selectedDeviceOrAxis);
    if (!deviceKey) { return null }
    const device = devices[deviceKey];
    if (!device) { return null }
    return ({
      device,
      ...state.devicesAndAxes[deviceKey],
    });
  }
);

export const selectSearch = createSelector(selectDeviceSettings, state => state.search);

export const selectSettingToBeRead = createSelector(selectDeviceSettings, state => state.settingsReadQueue[0]);
export const selectSettingsBeingRead = createSelector(selectDeviceSettings, state => state.settingsReadQueue.length > 0);
export const selectCurrentAccessLevel = createSelector(selectCurrentDevice,
  currentDevice => currentDevice?.currentAccessLevel
);

export const selectReadableSettingIDs = createSelector(selectCurrentEntity, selectCurrentAccessLevel,
  (currentDeviceOrAxis, currentAccessLevel) => {
    if (!currentDeviceOrAxis?.settingIDsSorted) {
      return [];
    }

    if (currentAccessLevel == null) {
      if (currentDeviceOrAxis.settingIDsSorted.includes(ACCESS_LEVEL_SETTING)) {
        return [ACCESS_LEVEL_SETTING];
      } else {
        return [];
      }
    } else {
      return currentDeviceOrAxis.settingIDsSorted.filter(
        settingId => currentDeviceOrAxis.settings[settingId].readAccessLevel <= currentAccessLevel
      );
    }
  }
);

export const selectVisibleSettings = createSelector(selectCurrentEntity, selectCurrentAccessLevel,
  (currentDeviceOrAxis, currentAccessLevel) => {
    if (currentDeviceOrAxis?.settingIDsSorted == null) {
      return {};
    }
    const assumedLevel = currentAccessLevel ?? DEFAULT_ACCESS_LEVEL;
    return _.pickBy(currentDeviceOrAxis.settings, setting => setting.readAccessLevel <= assumedLevel);
  }
);

export const selectSearchFilteredSettings = createSelector(selectVisibleSettings, selectSearch, (settings, search) => {
  if (!search) { return settings }
  const searchWith = search.toLowerCase();
  return _.pickBy(settings, setting =>
    setting.id.includes(searchWith) || setting.meta?.humanName?.toLowerCase().includes(search)
  );
});

export interface Category {
  key: string;
  meta: CategoryMetadata[];
  settings: SettingsEntry[];
}

function getCategoryMetadata(categoryName?: string) {
  let current = categoryName;
  const path: CategoryMetadata[] = [];
  while (current != null) {
    const category = categoriesMetadata[current as Categories];
    path.push(category);
    current = category.parent;
  }
  if (path.length === 0) {
    path.push(NO_CATEGORY_META);
  }
  return path;
}

export const selectDisplayedSettingsByCategories = createSelector(selectSearchFilteredSettings, selectCurrentEntity,
  (displayedSettings, currentDeviceOrAxis): Category[] => {
    if (currentDeviceOrAxis?.settingIDsSorted == null) { return [] }

    const sortedSettings = currentDeviceOrAxis.settingIDsSorted.map(setting => currentDeviceOrAxis.settings[setting]);
    const byCategoriesSorted = _.groupBy(sortedSettings, setting => setting.meta?.category ?? NO_CATEGORY);

    const categories = _.mapValues(byCategoriesSorted, (categorySettings, categoryKey): Category => ({
      key: categoryKey,
      meta: getCategoryMetadata(categorySettings[0].meta?.category),
      settings: categorySettings.filter(setting => setting.id in displayedSettings),
    }));

    return Object.values(categories).sort((a, b) => a.key.localeCompare(b.key));
  }
);

export const selectSettingState = (deviceOrAxis: EntityKey, setting: string) =>
  (state: RootState) => {
    const device = tryAccess(selectDeviceSettings(state).devicesAndAxes, deviceOrAxis);
    return device && tryAccess(device.settings, setting);
  };

export const selectCustomDialog = createSelector(selectDeviceSettings, state => state.customDialog);
export const selectConfiguringNetwork = createSelector(selectDeviceSettings, state => state.configuringNetwork);
export const selectConfigureNetworkErr = createSelector(selectDeviceSettings, state => state.configureNetworkErr);

export const selectNetworkSettings = createSelector(selectCurrentDevice, state => {
  if (!state) { return null }

  return {
    ipAddress: getValueAsString(state.settings['comm.en.ipv4.address']),
    subnetMask: getValueAsString(state.settings['comm.en.ipv4.netmask']),
    defaultGateway: getValueAsString(state.settings['comm.en.ipv4.gateway']),
    dhcp: getValueAsString(state.settings['comm.en.ipv4.dhcp.enabled']),
  };
});

export const selectSelectedConnection = createSelector(selectDeviceSettings, selectConnections, (state, connections) => {
  const connectionKey = tryExtractConnectionKey(state.selectedDeviceOrAxis);
  return tryAccess(connections, connectionKey);
});

export const selectError = createSelector(selectCurrentEntity, axisOrDevice => axisOrDevice?.error);
