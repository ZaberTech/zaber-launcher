const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const { shared, isDevelopment } = require('./shared');
const { currentFlavor, flavors, currentEdition, editions } = require('../scripts/common');

const TITLES = {
  [flavors.ZABER]: 'Zaber Launcher',
  [flavors.REXROTH]: 'Rexroth Dashboard',
};

const INTERNAL_STUB_REPLACE = {
  '../internal': '../internal-stub',
  './internal': './internal-stub',
};

module.exports = {
  ...shared,
  entry: './src/index.tsx',
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, '..', 'build'),
  },
  target: 'node',
  node: false,
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json'],
    alias: {
      ...(currentEdition === editions.PUBLIC ? INTERNAL_STUB_REPLACE : {})
    },
  },
  module: {
    rules: [
      { enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: { transpileOnly: isDevelopment },
      },
      {
        test: /\.(sa|s?c)ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              additionalData: `$flavor: ${currentFlavor};`,
            },
          },
        ],
      },
      {
        test: /\.(ttf|eot|png|jpe?g|woff2?|gif|mp3)(\?[\s\S]+)?$/,
        type: 'asset/resource',
      },
      {
        test: /\.svg$/i,
        issuer: /\.[jt]sx?$/,
        oneOf: [
          {
            resourceQuery: /url/, // *.svg?url
            type: 'asset/resource',
          },
          {
            resourceQuery: /raw/, // *.svg?raw
            type: 'asset/source',
          },
          {
            use: [{
              loader: '@svgr/webpack',
              options: {
                titleProp: true,
              }
            }]
          }
        ]
      },
      {
        test: /\.svg$/,
        issuer: /\.s[ac]ss$/i,
        type: 'asset/resource',
      },
      {
        test: /\.html$/,
        exclude: /index\.html/,
        type: 'asset/source',
      },
      {
        test: /\.ya?ml$/,
        use: 'yaml-loader',
      },
      {
        test: /\.template$/,
        type: 'asset/source',
      }
    ]
  },
  plugins: shared.plugins.concat([
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
    new HtmlPlugin({
      template: './index.html',
      title: TITLES[currentFlavor],
    }),
    new ForkTsCheckerWebpackPlugin(),
    new MonacoWebpackPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        { from: 'protocol_manual/resource/**/*.png', to: '' },
        { from: 'protocol_manual/resource/**/*.svg', to: '', noErrorOnMissing: true },
      ],
    }),
  ]),
  ignoreWarnings: [
    { message: /Failed to parse source map/, module: /monaco-editor\/esm\/vs\/base\/browser\/dompurify\/dompurify.js/ },
  ],
};
