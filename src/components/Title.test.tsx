import React from 'react';
import { render, RenderResult } from '@testing-library/react';

import { Title } from './Title';

let wrapper: RenderResult;

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;
});

test('appends to title when being rendered', () => {
  wrapper = render(<Title>T1</Title>);

  expect(document.title).toBe('Zaber Launcher (dev) - T1');

  wrapper.unmount();

  expect(document.title).toBe('Zaber Launcher (dev)');
});

test('updates the title when props change', () => {
  wrapper = render(<Title>T1</Title>);

  expect(document.title).toBe('Zaber Launcher (dev) - T1');

  wrapper.rerender(<Title>T2</Title>);

  expect(document.title).toBe('Zaber Launcher (dev) - T2');
});
