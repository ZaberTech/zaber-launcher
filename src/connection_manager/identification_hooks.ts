import type { SagaIterator } from 'redux-saga';
import { call } from 'redux-saga/effects';
import _ from 'lodash';
import { injectable } from 'inversify';

import { getContainer } from '../container';
import { notNil } from '../utils';

import type { DeviceInfoWithAxes, IdentifiedDeviceInfo } from './types';
import { isIdentified } from './utils';

type Hook = (device: IdentifiedDeviceInfo) => SagaIterator;

@injectable()
export class IdentificationHooks {
  private hooks = new Map<number, Hook[]>();

  register(deviceOrPeripheralIDs: number[], hook: Hook): void {
    for (const deviceID of deviceOrPeripheralIDs) {
      let existingHooks = this.hooks.get(deviceID);
      if (existingHooks == null) {
        existingHooks = [];
        this.hooks.set(deviceID, existingHooks);
      }
      existingHooks.push(hook);
    }
  }

  getHooksToRun(device: IdentifiedDeviceInfo): Hook[] {
    const allDeviceIDs = [...device.axes.map(axis => axis.identity.peripheralId), device.identity.deviceId];
    const hooksToRun = _.uniq(_.flatten(allDeviceIDs.map(deviceID => this.hooks.get(deviceID)).filter(notNil)));
    return hooksToRun;
  }
}

export function* runIdentificationHooks(devices: DeviceInfoWithAxes[]): SagaIterator {
  const hooks = getContainer().get(IdentificationHooks);

  for (const device of devices.filter(isIdentified)) {
    for (const hook of hooks.getHooksToRun(device)) {
      yield call(hook, device);
    }
  }
}
