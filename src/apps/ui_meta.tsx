import React from 'react';
import { iconFromSvg } from '@zaber/react-library';

// red
import deviceSettingsRedSvg from '../assets/ds_logo_red.svg?url';
import controlsRedSvg from '../assets/bm_logo_red.svg?url';
import firmwareUpgradeRedSvg from '../assets/fu_logo_red.svg?url';
import lc40SetupRedSvg from '../assets/lc40_logo_red.svg?url';
import terminalRedSvg from '../assets/terminal_logo_red.svg?url';
import currentTunerRedSvg from '../assets/current_tuner_logo_red.svg?url';
import hardwareModificationRedSvg from '../assets/hard_mod_logo_red.svg?url';
import oscilloscopeRedSvg from '../assets/oscilloscope_logo_red.svg?url';
import gcodeRedSvg from '../assets/gct_logo_red.svg?url';
import lockstepConfigurationRedSvg from '../assets/lockstep_logo_red.svg?url';
import deviceIoRedSvg from '../assets/device_io_logo_red.svg?url';
import joystickRedSvg from '../assets/joystick_logo_red.svg?url';
import servoTunerRedSvg from '../assets/servo_tuner_logo_red.svg?url';
import productionAppRedSvg from '../assets/production_app_logo_red.svg?url';
import fw7ToolRedSvg from '../assets/fw7_programmer_red.svg?url';
import pvtRedSvg from '../assets/pvt_app_logo_red.svg?url';
import processControllerRedSvg from '../assets/process_controller_logo_red.svg?url';
import microscopeRedSvg from '../assets/microscope_logo_red.svg?url';
import triggersRedSvg from '../assets/triggers_logo_red.svg?url';
// grey
import BasicMovementGreySvg from '../assets/bm_logo_grey.svg';
import DeviceSettingsGreySvg from '../assets/ds_logo_grey.svg';
import TerminalGreySvg from '../assets/terminal_logo_grey.svg';
import FirmwareUpgraderGreySvg from '../assets/fu_logo_grey.svg';
import CurrentTunerGreySvg from '../assets/current_tuner_logo_grey.svg';
import LC40SetupGreySvg from '../assets/lc40_logo_grey.svg';
import HardwareModificationGreySvg from '../assets/hard_mod_logo_grey.svg';
import OscilloscopeGreySvg from '../assets/oscilloscope_logo_grey.svg';
import GCodeGreySvg from '../assets/gct_logo_grey.svg';
import LockstepConfigurationGreySvg from '../assets/lockstep_logo_grey.svg';
import DeviceIoGreySvg from '../assets/device_io_logo_grey.svg';
import JoystickGreySvg from '../assets/joystick_logo_grey.svg';
import ServoTuningGreySvg from '../assets/servo_tuner_logo_grey.svg';
import ProductionAppGreySvg from '../assets/production_app_logo_grey.svg';
import Fw7ToolGreySvg from '../assets/fw7_programmer_grey.svg';
import PvtGreySvg from '../assets/pvt_app_logo_grey.svg';
import ProcessControllerGreySvg from '../assets/process_controller_logo_grey.svg';
import MicroscopeGreySvg from '../assets/microscope_logo_grey.svg';
import TriggersGreySvg from '../assets/triggers_logo_grey.svg';
// neutral
import CurrentTunerNeutralSvg from '../assets/current_tuner_logo_neutral.svg';
import FirmwareUpgraderNeutralSvg from '../assets/fu_logo_neutral.svg';
import GCodeNeutralSvg from '../assets/gct_logo_neutral.svg';
import HardwareModificationNeutralSvg from '../assets/hard_mod_logo_neutral.svg';
import ProductionAppNeutralSvg from '../assets/production_app_logo_neutral.svg';
import LC40SetupNeutralSvg from '../assets/lc40_logo_neutral.svg';
import LockstepConfigurationNeutralSvg from '../assets/lockstep_logo_neutral.svg';
import MicroscopeNeutralSvg from '../assets/microscope_logo_neutral.svg';
import OscilloscopeNeutralSvg from '../assets/oscilloscope_logo_neutral.svg';
import ServoTuningNeutralSvg from '../assets/servo_tuner_logo_neutral.svg';
import PvtNeutralSvg from '../assets/pvt_app_logo_neutral.svg';


import type { AppUrls } from './list';

interface AppUIMeta {
  description: string;
  logoSrc: string;
  icon: JSX.Element;
}

export const AppIconsGrey = {
  BasicMovement: iconFromSvg(BasicMovementGreySvg),
  DeviceSettings: iconFromSvg(DeviceSettingsGreySvg),
  Terminal: iconFromSvg(TerminalGreySvg),
  FirmwareUpgrader: iconFromSvg(FirmwareUpgraderGreySvg),
  CurrentTuner: iconFromSvg(CurrentTunerGreySvg),
  LC40Setup: iconFromSvg(LC40SetupGreySvg),
  HardwareModification: iconFromSvg(HardwareModificationGreySvg),
  Oscilloscope: iconFromSvg(OscilloscopeGreySvg),
  GCode: iconFromSvg(GCodeGreySvg),
  LockstepConfiguration: iconFromSvg(LockstepConfigurationGreySvg),
  DeviceIo: iconFromSvg(DeviceIoGreySvg),
  Joystick: iconFromSvg(JoystickGreySvg),
  ServoTuner: iconFromSvg(ServoTuningGreySvg),
  ProductionApp: iconFromSvg(ProductionAppGreySvg),
  Fw7Tool: iconFromSvg(Fw7ToolGreySvg),
  Pvt: iconFromSvg(PvtGreySvg),
  ProcessController: iconFromSvg(ProcessControllerGreySvg),
  Microscope: iconFromSvg(MicroscopeGreySvg),
  Triggers: iconFromSvg(TriggersGreySvg),
};

export const AppIconsNeutral = {
  CurrentTuner: iconFromSvg(CurrentTunerNeutralSvg),
  FirmwareUpgrader: iconFromSvg(FirmwareUpgraderNeutralSvg),
  GCode: iconFromSvg(GCodeNeutralSvg),
  HardwareModification: iconFromSvg(HardwareModificationNeutralSvg),
  ProductionApp: iconFromSvg(ProductionAppNeutralSvg),
  LC40Setup: iconFromSvg(LC40SetupNeutralSvg),
  LockstepConfiguration: iconFromSvg(LockstepConfigurationNeutralSvg),
  Microscope: iconFromSvg(MicroscopeNeutralSvg),
  Oscilloscope: iconFromSvg(OscilloscopeNeutralSvg),
  ServoTuner: iconFromSvg(ServoTuningNeutralSvg),
  Pvt: iconFromSvg(PvtNeutralSvg),
};

export const APPS_UI_META: Record<AppUrls, AppUIMeta> = {
  '/basic-controls': {
    description: 'Explore your devices\' functionalities and issue basic commands.',
    logoSrc: controlsRedSvg,
    icon: <AppIconsGrey.BasicMovement/>,
  },
  '/basic-movement': {
    description: 'Issue basic movement commands to your device. Explore the full range of speed and travel.',
    logoSrc: controlsRedSvg,
    icon: <AppIconsGrey.BasicMovement/>,
  },
  '/device-settings': {
    description: 'View and modify device settings.',
    logoSrc: deviceSettingsRedSvg,
    icon: <AppIconsGrey.DeviceSettings/>,
  },
  '/terminal': {
    description: 'Send arbitrary commands to devices. Examine the raw communication protocol.',
    logoSrc: terminalRedSvg,
    icon: <AppIconsGrey.Terminal/>,
  },
  '/firmware-upgrade': {
    description: 'Check the firmware upgrade availability for your devices.',
    logoSrc: firmwareUpgradeRedSvg,
    icon: <AppIconsGrey.FirmwareUpgrader/>,
  },
  '/current-tuner': {
    description: 'Tune the electrical current controller of your devices.',
    logoSrc: currentTunerRedSvg,
    icon: <AppIconsGrey.CurrentTuner/>,
  },
  '/lc40-setup': {
    description: 'Set up your LC40 stages.',
    logoSrc: lc40SetupRedSvg,
    icon: <AppIconsGrey.LC40Setup/>,
  },
  '/shb40-setup': {
    description: 'Set up your SHB40 stages.',
    logoSrc: lc40SetupRedSvg,
    icon: <AppIconsGrey.LC40Setup/>,
  },
  '/hardware-modification': {
    description: 'Set up modified Zaber hardware and non-Zaber peripherals.',
    logoSrc: hardwareModificationRedSvg,
    icon: <AppIconsGrey.HardwareModification/>,
  },
  '/servo-tuner': {
    description: 'Tune the position controller of your device.',
    logoSrc: servoTunerRedSvg,
    icon: <AppIconsGrey.ServoTuner/>
  },
  '/process-controller': {
    description: 'Manage your Process Controller.',
    logoSrc: processControllerRedSvg,
    icon: <AppIconsGrey.ProcessController/>
  },
  '/gcode': {
    description: 'Drive your devices with G-Code.',
    logoSrc: gcodeRedSvg,
    icon: <AppIconsGrey.GCode/>,
  },
  '/lockstep-configuration': {
    description: 'Configure controllers to drive axes in lockstep.',
    logoSrc: lockstepConfigurationRedSvg,
    icon: <AppIconsGrey.LockstepConfiguration/>
  },
  '/demo': {
    description: 'Lets you do repetitive movements on a lot of devices at the same time.',
    logoSrc: controlsRedSvg,
    icon: <AppIconsGrey.BasicMovement/>,
  },
  '/device-io': {
    description: 'Control analog/digital inputs and outputs of your device.',
    logoSrc: deviceIoRedSvg,
    icon: <AppIconsGrey.DeviceIo/>,
  },
  '/joystick': {
    description: 'Configure your X-JOY (joystick) device.',
    logoSrc: joystickRedSvg,
    icon: <AppIconsGrey.Joystick/>,
  },
  '/oscilloscope': {
    description: 'Capture and plot data from your device.',
    logoSrc: oscilloscopeRedSvg,
    icon: <AppIconsGrey.Oscilloscope/>,
  },
  '/internal/production-app': {
    description: 'Application for production technicians.',
    logoSrc: productionAppRedSvg,
    icon: <AppIconsGrey.ProductionApp/>,
  },
  '/internal/fw7-tool': {
    description: 'Program FW7 boards.',
    logoSrc: fw7ToolRedSvg,
    icon: <AppIconsGrey.Fw7Tool/>,
  },
  '/pvt': {
    description: 'Plot PVT trajectories.',
    logoSrc: pvtRedSvg,
    icon: <AppIconsGrey.Pvt/>,
  },
  '/calibration': {
    description: 'Save/Load position calibration data.',
    logoSrc: hardwareModificationRedSvg,
    icon: <AppIconsGrey.HardwareModification/>,
  },
  '/microscope': {
    description: 'Control your microscope.',
    logoSrc: microscopeRedSvg,
    icon: <AppIconsGrey.Microscope/>,
  },
  '/triggers': {
    description: 'Set up and control device triggers.',
    logoSrc: triggersRedSvg,
    icon: <AppIconsGrey.Triggers/>,
  }
};
