import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import {
  Button, ButtonPrevious, ButtonRow, Input, NoticeBanner, Text,
} from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { AuthState } from '../cloud';
import { selectAuthState } from '../cloud/selectors';
import { ExternalLink } from '../components';
import { environment } from '../environment';

import { actions as actionsDefinition } from './actions';
import { Steps } from './types';
import { selectCloud } from './selectors';
import { Loading } from './Elements';

export const ConnectCloud: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const { cloudId, connecting, error } = useSelector(selectCloud);
  const auth = useSelector(selectAuthState);
  const isValid = !!cloudId.trim();
  const vdLandingPage = `${environment.cloudPortalUrl}/virtual-device/home`;
  const vdManager = `${environment.cloudPortalUrl}/virtual-device/manager`;

  useEffect(() => () => { actions.connectCloudClear() }, []);

  if (environment.offline) {
    return <div className="connect-cloud">
      <h3>Virtual Device Unavailable</h3>
      <p>This type of connection is unavailable in Offline Zaber Launcher.</p>
    </div>;
  }

  return (
    <div className="connect-cloud">
      <Text className="subheading" t={Text.Type.BodyLg}>
        Please enter the Virtual Device's Cloud ID:
      </Text>

      <Input className="cloud-id"
        labelContent="Cloud ID"
        disabled={connecting}
        value={cloudId}
        onValueChange={value => actions.changeCloud(value)}/>

      {connecting && <Loading type="row"/>}
      {error && <NoticeBanner>{error.title}: {error.message}</NoticeBanner>}
      {auth === AuthState.Authenticated && <ExternalLink url={vdManager}>Manage my Virtual Devices</ExternalLink>}
      {auth !== AuthState.Authenticated && <ExternalLink url={vdLandingPage}>Create a Virtual Device</ExternalLink>}

      <ButtonRow justify="spread">
        <ButtonPrevious onClick={() => actions.changeStep(Steps.CHOOSE_CONNECTION_TYPE)}>Back</ButtonPrevious>
        <Button disabled={!isValid || connecting} onClick={() => actions.connectCloud()}>Create Connection</Button>
      </ButtonRow>
    </div>
  );
};
