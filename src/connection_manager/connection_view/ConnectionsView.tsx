import React, { Component } from 'react';
import { connect, useSelector } from 'react-redux';
import { Button, Text } from '@zaber/react-library';
import { NavLink } from 'react-router-dom';
import _ from 'lodash';
import { bindActionCreators } from 'redux';

import { selectSearchString } from '../../react_router';
import { selectHasConnections, selectLoadingRouters, selectRoutersArray, selectConnections } from '../selectors';
import noDeviceFound from '../../assets/no_device_found.svg?url';
import { getIncludedKeys, isSubKeyOf, EntityKey, extractRouterKey } from '../../keys';
import type { RootState } from '../../store';
import { omit } from '../../utils';

import { Router } from './Router';
import { PassedProperties, selectableEntitiesToEntityType } from './types';
import { actions as actionsCreators } from './actions';
import { selectValidLastKey } from './selectors';

const NoConnections = () => (
  <div className="no-connections-to-view">
    <img src={noDeviceFound} alt="Generic device with error symbol" className="no-connections-img"/>
    <div className="message">
      <Text t={Text.Type.H4}>No devices connected.</Text>
      <Text>Please setup your connections first through My Connections and come back here.</Text>
    </div>
    <NavLink to="/connection-manager"><Button>My Connections</Button></NavLink>
  </div>
);

const ListOfRouters: React.FC<PassedProperties> = passedProperties => {
  const routers = useSelector(selectRoutersArray);
  const hasConnections = useSelector(selectHasConnections);
  if (!hasConnections) {
    return <NoConnections/>;
  }
  return <>{routers.map(router => (
    <Router
      key={router.key}
      router={router}
      {...passedProperties}/>)
  )}</>;
};

interface DispatchProps {
  actions: typeof actionsCreators;
}
interface StateProps {
  stateProps: {
    urlSelected: string | null;
    routers: ReturnType<typeof selectRoutersArray>;
    connections: ReturnType<typeof selectConnections>;
    lastKey: ReturnType<typeof selectValidLastKey>;
    loadingRouters: ReturnType<typeof selectLoadingRouters>;
  };
}

type Props = PassedProperties & StateProps & DispatchProps;

class ConnectionsViewBase extends Component<Props> {
  static SELECT_PARAM = 'selected';

  /** don't store key updating to this value */
  private dontStoreKey: EntityKey | null = null;

  componentDidMount() {
    const { stateProps: { lastKey, urlSelected, connections, routers }, actions, selectable, selected, onSelect } = this.props;

    if (urlSelected) {
      if (urlSelected !== selected) {
        this.dontStoreKey = urlSelected;
        onSelect(urlSelected);

        // make sure to expand router in case we are in newly opened browser and
        // ConnectionsView is in default state
        actions.setExpanded(extractRouterKey(urlSelected), true);
      }
    } else if (lastKey) {
      let lastKeyUsed = false;

      const includedKeys = getIncludedKeys(lastKey);
      for (const [selectableEntity, entityType] of selectableEntitiesToEntityType) {
        const newKey = includedKeys[entityType];
        if (newKey && selectable.includes(selectableEntity)) {
          if (newKey !== selected) {
            this.dontStoreKey = newKey;
            onSelect(newKey);
          }
          lastKeyUsed = true;
          break;
        }
      }

      if (!lastKeyUsed && !isSubKeyOf(selected, lastKey) && selected) {
        onSelect(null);
      }
    } else if (!selected) {
      if (_.size(routers) === 1 && _.size(connections) === 1) {
        const firstConnection = _.values(connections)[0];
        if (selectable.includes('connection')) {
          onSelect(firstConnection.key);
        } else if (selectable.includes('device') && !selectable.includes('axis') && _.size(firstConnection.devices) === 1) {
          onSelect(firstConnection.devices![0]);
        }
      }
    }
  }

  componentDidUpdate(prevProps: Props) {
    const { actions, selected } = this.props;
    if (selected !== prevProps.selected) {
      if (selected && this.dontStoreKey !== selected) {
        actions.storeLastKey(selected);
      }
      this.dontStoreKey = null;
    }
  }

  render() {
    const { stateProps, ...rest } = this.props;
    const passedProperties = omit(rest, 'actions');
    const { loadingRouters } = stateProps;

    if (loadingRouters) {
      return null;
    }
    return (
      <div className="connections-view">
        <div className="all"><Text t={Text.Type.H4}>All Connections</Text></div>
        <ListOfRouters {...passedProperties}/>
      </div>
    );
  }
}

export const ConnectionsView = connect<StateProps, DispatchProps, PassedProperties, RootState>(state => ({
  stateProps: {
    urlSelected: selectSearchString(state).get(ConnectionsViewBase.SELECT_PARAM),
    routers: selectRoutersArray(state),
    connections: selectConnections(state),
    lastKey: selectValidLastKey(state),
    loadingRouters: selectLoadingRouters(state),
  },
}), dispatch => ({
  actions: bindActionCreators(actionsCreators, dispatch),
}))(ConnectionsViewBase);
