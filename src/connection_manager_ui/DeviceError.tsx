import React, { FC, useEffect } from 'react';
import { NoticeMessage } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { EntityKey } from '../keys';
import { useActions } from '../utils';

import { actions as actionDefinitions } from './actions';
import { selectDevicesSystemErrors } from './selectors';


interface Props {
  deviceKey: EntityKey;
}

export const DeviceError: FC<Props> = ({ deviceKey }) => {
  const actions = useActions(actionDefinitions);
  const deviceSystemErrors = useSelector(selectDevicesSystemErrors)[deviceKey];

  useEffect(() => {
    actions.getDeviceSystemErrors(deviceKey);
  }, []);

  return <NoticeMessage className="connection-manager-ui ff-notice-message"
    headline="Fault - Critical System Error - (FF)" type="error" collapsible>
    <p>
        The device has experienced a critical system error. This should not occur during normal operation.
        Please contact <a href="https://www.zaber.com/contact">Zaber Support</a> with the device's <b>system errors</b> response below:
    </p>
    {deviceSystemErrors == null ?
      <p>Awaiting <b>system errors</b> response from the device.</p> :
      deviceSystemErrors.map((error, index) => <p key={index}> <b>{error}</b> </p>)
    }
  </NoticeMessage>;
};
