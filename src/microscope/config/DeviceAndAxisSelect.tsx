import React from 'react';
import { P } from 'ts-pattern';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import { Checkbox, NoticeBanner, SimpleSelect } from '@zaber/react-library';

import { EntityKey, extractDeviceKey, makeAxisKey, tryExtractDeviceKey } from '../../keys';
import { isMatch, useActions } from '../../utils';
import { selectIdentifiedAxes } from '../../connection_manager';
import type { DetectableConfig } from '../types';
import { actions as actionsDefinition } from '../actions';

import { selectDevices, selectEditedConfig } from './selectors';

export function useDeviceOptions() {
  const devices = useSelector(selectDevices);
  return _.map(devices, device => ({
    label: `${device.address.toString().padStart(2, '0')} ${device.label ?? device.identity.name}`,
    value: device.key,
    isController: device.isController,
  })).concat({ label: 'None', value: '', isController: false });
}

export function useAxisOptions(entityKey: EntityKey | null) {
  const allAxes = useSelector(selectIdentifiedAxes);
  return _.filter(allAxes, axis => extractDeviceKey(axis.key) === tryExtractDeviceKey(entityKey)).map(axis => ({
    label: `Axis ${axis.axisNumber} ${axis.label ?? axis.identity.peripheralName}`,
    value: axis.key,
  }));
}

type DetectableConfigData<TConfigKey extends keyof DetectableConfig> = Extract<DetectableConfig[TConfigKey], { key: string }>;

export const DeviceAndAxisSelect = <TConfigKey extends keyof DetectableConfig>({
  configKey, deviceOnly = false, label, children, defaultConfig,
}: {
  configKey: keyof DetectableConfig;
  label: string;
  deviceOnly?: boolean;
  children?: (ctx: {
    data: DetectableConfigData<TConfigKey> | null;
    onChange: (changed: Partial<DetectableConfigData<TConfigKey>>) => void;
  }) => JSX.Element;
  defaultConfig: Omit<DetectableConfigData<TConfigKey>, 'key'>;
}): JSX.Element => {
  const actions = useActions(actionsDefinition);
  const config = useSelector(selectEditedConfig)[configKey];
  const isKey = isMatch({ key: P.string }, config);
  const selected = isKey ? config.key : null;

  const deviceOptions = useDeviceOptions();
  const selectedDeviceKey = tryExtractDeviceKey(selected);
  const selectedDevice = deviceOptions.find(device => device.value === selectedDeviceKey);
  const axisOptions = useAxisOptions(selected);

  const onDeviceChange = (newKey: string) => {
    if (!newKey) {
      actions.editConfig({ [configKey]: null });
      return;
    }

    if (!deviceOnly) {
      newKey = makeAxisKey(newKey, 1);
    }
    actions.editConfig({ [configKey]: { ...defaultConfig, key: newKey } });
  };
  const onAxisChange = (newKey: string) =>
    actions.editConfig({ [configKey]: { ...defaultConfig, key: newKey } });

  return <>
    <div className="label">{label}</div>
    <SimpleSelect className="device" placeholder={`${label} Device`}
      options={deviceOptions} value={selectedDeviceKey ?? ''} onValueChange={onDeviceChange}
      portalToBody/>
    {!deviceOnly && (axisOptions.length > 1 || selectedDevice?.isController) &&
      <SimpleSelect className="axis" placeholder={label}
        options={axisOptions} value={selected} onValueChange={onAxisChange}
        portalToBody/> }
    {children?.({
      data: isKey ? (config as DetectableConfigData<TConfigKey>) : null,
      onChange: changed => {
        if (isKey) {
          actions.editConfig({ [configKey]: { ...config, ...changed } });
        }
      },
    })}
    {isMatch({ error: P.string }, config) &&
      <NoticeBanner className="message" type="warning">{config.error}</NoticeBanner>}
  </>;
};

export const MotionAxisSelect = ({ configKey, label }: { configKey: 'x' | 'y' | 'focus'; label: string }) => (
  <DeviceAndAxisSelect<typeof configKey>
    configKey={configKey}
    defaultConfig={{ inverted: false }}
    label={label}>{({ data, onChange }) =>
      <Checkbox
        className="inverted"
        labelContent="Inverted"
        checked={data?.inverted ?? false}
        onChecked={inverted => onChange({ inverted })}/>
    }</DeviceAndAxisSelect>);
