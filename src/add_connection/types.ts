export enum Steps {
  CHOOSE_CONNECTION_TYPE = 'CHOOSE_CONNECTION_TYPE',
  CONFIGURE_SERIAL = 'CONFIGURE_SERIAL',
  CONFIGURE_TCP = 'CONFIGURE_TCP',
  DISCOVER_MDNS = 'DISCOVER_MDNS',
  DETECT_DEVICES = 'DETECT_DEVICES',
  ADD_TO_ROUTER = 'ADD_TO_ROUTER',
  CONNECT_CLOUD = 'CONNECT_CLOUD',
  DISCOVER_LOCAL_SHARE = 'DISCOVER_LOCAL_SHARE',
  CONNECT_LOCAL_SHARE = 'CONNECT_LOCAL_SHARE',
  DISCOVER_CLOUD = 'DISCOVER_CLOUD',
}

export const MAX_PORT_NUM = 65535;
export const DEFAULT_BAUD_RATE = 115200;

export const MDNS_DISCOVERY_SERVICE_SUFFIX_SINGLE = '_zaber-non-fwd._tcp.local';
export const MDNS_DISCOVERY_SERVICE_SUFFIX_CHAIN = '_zaber._tcp.local';
export const MDNS_DISCOVERY_INACTIVE_TIMEOUT = 5000;

export const MDNS_SINGLE_PORT = 55551;
export const MDNS_CHAIN_PORT = 55550;

export interface ErrorWithTitle {
  message: string;
  title: string | null;
}

export const UNAUTHENTICATED_REALM = 'unauthenticated';

export interface MDNSCandidate {
  hostname: string;
  singlePort: number | null;
  chainPort: number | null;
  deviceId: number;
  name: string | null;
  serialNumber: number;
}

export type tcpConnectionType = 'chain' | 'single' | 'simulator' | 'custom';
