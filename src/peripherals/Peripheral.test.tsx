/* eslint-disable camelcase */
import React, { Component } from 'react';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import { injectable } from 'inversify';
import { CommandFailedException, CommandFailedExceptionData } from '@zaber/motion';

declare namespace SelectMock {
  interface Item {
    isDisabled?: boolean;
  }
  interface Props {
    placeholder: string;
    noOptionsMessage: (props: { inputValue: string }) => string | null;
    onChange: (item: unknown) => void;
    loadOptions: (input: string, cb: (items: Item[]) => void) => void;
    formatOptionLabel: (item: unknown) => string;
    getOptionValue: (item: unknown) => string;
    styles: {
      option: (style: unknown, optionProps: { isDisabled: boolean }) => unknown;
    };
  }
  interface State {
    items: Item[];
    input: string;
  }
}

class SelectMock extends Component<SelectMock.Props, SelectMock.State> {
  constructor(props: SelectMock.Props) {
    super(props);
    this.state = { items: [], input: '' };
  }
  render() {
    const { placeholder, loadOptions, formatOptionLabel, getOptionValue, onChange, styles, noOptionsMessage } = this.props;
    const { items, input } = this.state;
    return <>
      <input placeholder={placeholder}
        onChange={e => loadOptions(e.target.value, items => this.setState({ items, input: e.target.value }))}/>
      {items.map(item => {
        styles.option({}, { isDisabled: item.isDisabled ?? false });
        return <div key={getOptionValue(item)} onClick={() => onChange(item)}>{formatOptionLabel(item)}</div>;
      })}
      {items.length === 0 && <div>{noOptionsMessage({ inputValue: input })}</div>}
    </>;
  }
}

jest.mock('react-select/async', () => SelectMock);

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore } from '../test';
import { selectAxes } from '../connection_manager';
import { mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { ZaberApi } from '../app_components';
import { MessageRoutersService } from '../message_router';
import { actions as managerActions } from '../connection_manager/actions';

import { Peripheral } from './Peripheral';
import { actions } from './actions';
import { selectAxesErrors } from './selectors';

const loadDevicesActionMock = jest.fn(() => ({ type: 'NOTHING', payload: undefined }));
managerActions.loadDevices = loadDevicesActionMock;

let axes: AxisMock[];
let axisCallback: (axis: AxisMock) => void;

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);

    axes.push(this);
    if (axisCallback) { axisCallback(this) }
  }
  _flags = [] as string[];
  _settings: Record<string, number> = {
    'peripheral.id': 43211,
    'peripheral.id.pending': 0,
  };
  warnings = {
    getFlags: jest.fn(async () => new Set(this._flags)),
  };
  settings = {
    get: jest.fn(async setting => this._settings[setting]),
    set: jest.fn(async (setting, value) => {
      this._settings[setting] = value;
    }),
  };
  identity = {
    peripheralId: 43211,
  };
  genericCommand = jest.fn(() => Promise.resolve());
}

class DeviceMock extends DeviceMockBase<AxisMock> {
  identity = {
    firmwareVersion: { major: 7 },
  };
}

let connections: ConnectionMock[];

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  genericCommand = jest.fn().mockResolvedValue(undefined);

  constructor(id: string, router: RouterConnectionMock) {
    super(id, router);
    connections.push(this);
  }
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

let zaberApi: ZaberApiMock;

@injectable()
class ZaberApiMock {
  getSupportedPeripherals = jest.fn<Promise<ZaberApi.SupportedPeripherals>, [number]>().mockResolvedValue({
    peripherals: [43211, ..._.range(56111, 56118)].map(p =>
      ({ peripheral_id: p, product_id: p * 10, name: `Peri ${p}` })),
  });
}

const Peripherals: React.FC = () => {
  const axes = useSelector(selectAxes);
  return <>{_.map(axes, axis => <Peripheral key={axis.key} axisKey={axis.key}/>)}</>;
};

const TestPeripherals = wrapWithNewStore(Peripherals);

let wrapper: RenderResult;

beforeEach(() => {
  axes = [];
  connections = [];
  axisCallback = null!;
  loadDevicesActionMock.mockClear();

  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(ZaberApi).to(ZaberApiMock);
  zaberApi = container.get<unknown>(ZaberApi) as ZaberApiMock;

  wrapper = render(<TestPeripherals/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
});

const openMenu = (axis: number = 0) => fireEvent.click(wrapper.queryAllByTitle('Open Menu')[axis]);

describe('legacy peripherals', () => {
  beforeEach(() => {
    mockSingleDeviceWithPeripherals(TestPeripherals.testStore);
  });

  test('renders', async () => {
    expect(wrapper.queryAllByText('LHM025A-T3')).toHaveLength(4);
  });

  test('renders error for unexpected error', async () => {
    axisCallback = axis => axis.settings.get.mockRejectedValue(new Error('Something went wrong'));
    await waitUntilPass(() =>
      expect(wrapper.queryAllByTitle(/Something went wrong/)).toHaveLength(4)
    );
  });

  describe('after monitor populated', () => {
    beforeEach(async () => {
      await waitUntilPass(() => expect(wrapper.queryAllByTitle('Change Peripheral')).toHaveLength(4));
    });

    async function editPeripheral() {
      fireEvent.click(wrapper.queryAllByTitle('Change Peripheral')[0]);
      await waitUntilPass(() => wrapper.getByPlaceholderText(/Enter Peripheral/));
    }
    function enterPeripheral(value: string) {
      fireEvent.input(wrapper.getByPlaceholderText(/Enter Peripheral/), { target: { value } });
    }

    test('renders', async () => {
      expect(wrapper.queryAllByText('LHM025A-T3')).toHaveLength(4);
    });

    test('allows user to manually change peripheral and reloads devices', async () => {
      await editPeripheral();

      enterPeripheral('2');
      fireEvent.click(wrapper.getByText(/Peri 56112/));

      fireEvent.click(wrapper.getByTitle('Confirm'));

      await waitUntilPass(() => expect(axes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 56112));
      await waitUntilPass(() => expect(loadDevicesActionMock).toHaveBeenCalled());
    });

    test('indicates that there more peripherals matched', async () => {
      await editPeripheral();

      enterPeripheral('56');
      wrapper.getByText(/and 1 more/);
    });

    test('displays message when there no peripherals', async () => {
      await editPeripheral();

      enterPeripheral('something');
      wrapper.getByText('Peripheral cannot be found');
    });

    test('allows user to cancel editing', async () => {
      await editPeripheral();

      fireEvent.click(wrapper.getByTitle('Cancel'));
      expect(wrapper.queryByTitle('Cancel')).toBeNull();
    });

    test('handles error on setting peripheral', async () => {
      await editPeripheral();

      enterPeripheral('1');
      fireEvent.click(wrapper.getByText(/Peri 56111/));

      axes[0].settings.set.mockRejectedValueOnce(new CommandFailedException('Command failed', null!));
      fireEvent.click(wrapper.getByTitle('Confirm'));

      await waitUntilPass(() => wrapper.getByText(/Command failed/));
    });

    test('allows user to disable the axis', async () => {
      openMenu();
      fireEvent.click(wrapper.getByText('Disable Axis'));

      openMenu();
      expect(wrapper.getByText('Disable Axis')).toHaveAttribute('aria-disabled', 'true');
      await waitUntilPass(() => expect(axes[0].settings.set).toHaveBeenCalledWith('peripheral.id', 0));
    });

    test('allows to reload supported peripherals in case of failure', async () => {
      zaberApi.getSupportedPeripherals.mockRejectedValueOnce(new Error('Cannot load'));
      fireEvent.click(wrapper.queryAllByTitle('Change Peripheral')[0]);

      await waitUntilPass(() => wrapper.getByText('Try again'));
      fireEvent.click(wrapper.getByText('Try again'));
      expect(wrapper.queryByText('Try again')).toBeNull();

      await waitUntilPass(() => wrapper.getByPlaceholderText(/Enter Peripheral/));
    });
  });

  describe('peripheral disconnected', () => {
    beforeEach(async () => {
      axisCallback = axis => {
        if (axis.axisNumber !== 1) { return }
        axis._flags.push('FZ');
      };

      await waitUntilPass(() => wrapper.getByText('Reactivate'));
    });

    function prepareError(responseData: string) {
      axes[0].genericCommand.mockRejectedValueOnce(new CommandFailedException('Some failure', {
        responseData, warningFlag: 'FZ',
      } as CommandFailedExceptionData));
    }
    async function waitForEnd() {
      await waitUntilPass(() => expect(wrapper.getByText('Reactivate')).not.toHaveAttribute('disabled'));
    }

    test('can reactivate the peripheral', async () => {
      fireEvent.click(wrapper.getByText('Reactivate'));
      expect(wrapper.getByText('Reactivate')).toHaveAttribute('disabled');

      await waitForEnd();
      expect(axes[0].genericCommand).toHaveBeenCalledWith('activate');
    });

    test('handles activation error', async () => {
      prepareError('BADSOMETHING');
      fireEvent.click(wrapper.getByText('Reactivate'));

      await waitForEnd();
      wrapper.getByText(/Some failure/);
    });

    test('ignores activation error when reason is INACTIVE', async () => {
      prepareError('INACTIVE');
      fireEvent.click(wrapper.getByText('Reactivate'));

      await waitForEnd();
      expect(wrapper.queryByText(/Some failure/)).toBeNull();
    });

    describe('selectAxesErrors', () => {
      test('returns error for disconnected peripheral', async () => {
        await waitUntilPass(() => wrapper.getByText('Reactivate'));
        const errors = selectAxesErrors(TestPeripherals.testStore.getState());
        expect(Object.values(errors)).toMatchObject(['error', null, null, null]);
      });
    });
  });
});

describe('smart peripherals', () => {
  const Activate = 'Activate';

  beforeEach(() => {
    mockSingleDeviceWithPeripherals(TestPeripherals.testStore, { type: 'smart' });
  });

  test('renders', async () => {
    expect(wrapper.queryAllByText('LRM150A-E03T4A')).toHaveLength(4);
  });

  describe('peripheral disconnected', () => {
    beforeEach(() => {
      axisCallback = axis => {
        if (axis.axisNumber !== 1) { return }
        axis._flags.push('FZ');
      };
    });

    test('shows message to reconnect, allows to edit and disable, no activate button', async () => {
      await waitUntilPass(() => wrapper.getByText(/Please reconnect the peripheral/));

      wrapper.getByTitle('Change Peripheral');
      expect(wrapper.queryByText(Activate)).toBeNull();
      openMenu();
      expect(wrapper.getByText('Disable Axis')).not.toHaveAttribute('aria-disabled', 'true');
    });

    test('cancels editing when it stops being editable', async () => {
      await waitUntilPass(() => wrapper.getByText(/Please reconnect the peripheral/));

      fireEvent.click(wrapper.getByTitle('Change Peripheral'));
      wrapper.getByTitle('Confirm');

      axes[0]._flags.pop();
      TestPeripherals.testStore.dispatch(actions.refreshAxes());

      await waitUntilPass(() => expect(wrapper.queryByTitle('Confirm')).toBeNull());
    });
  });

  describe('peripheral disconnected and new one connected', () => {
    beforeEach(() => {
      axisCallback = axis => {
        if (axis.axisNumber !== 1) { return }
        axis._flags.push('FZ');
        axis._settings['peripheral.id.pending'] = 56113;
      };
    });

    test('allows to activate', async () => {
      await waitUntilPass(() => wrapper.getByText(/New peripheral Peri 56113 is connected/));

      fireEvent.click(wrapper.getByText(Activate));
      expect(wrapper.getByText(Activate)).toHaveAttribute('disabled');

      await waitUntilPass(() => expect(axes[0].genericCommand).toHaveBeenCalledWith('activate'));
    });
  });

  describe('peripheral disconnected and incompatible one connected', () => {
    beforeEach(() => {
      axisCallback = axis => {
        if (axis.axisNumber !== 1) { return }
        axis._flags.push('FZ');
        axis._flags.push('FN');
        axis._settings['peripheral.id.pending'] = 56113;
      };
    });

    test('renders message', async () => {
      await waitUntilPass(() => wrapper.getByText(/is not compatible/));
    });
  });
});

describe('unused peripherals', () => {
  beforeEach(() => {
    mockSingleDeviceWithPeripherals(TestPeripherals.testStore, { type: 'unused' });
  });

  test('renders, allows to edit, cannot disable', async () => {
    expect(wrapper.queryAllByText('Unused')).toHaveLength(4);

    await waitUntilPass(() => expect(wrapper.queryAllByTitle('Change Peripheral')).toHaveLength(4));

    openMenu();
    expect(wrapper.getByText('Disable Axis')).toHaveAttribute('aria-disabled', 'true');
  });
});
