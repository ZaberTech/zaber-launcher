import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';

export interface State {
  messageVisibility: Record<string, boolean | undefined>;
  fullscreenNoticeVisibility: boolean;
}

const initialState: State = {
  messageVisibility: {},
  fullscreenNoticeVisibility: false,
};

const dismissMessage = (state: State, { messageId }: ActionsToPayloads[ActionTypes.DISMISS_MESSAGE]): State =>
  ({
    ...state,
    messageVisibility: { ...state.messageVisibility, [messageId]: false },
  });

const updateMessageVisibility = (state: State, { messageId, visible }: ActionsToPayloads[ActionTypes.UPDATE_MESSAGE_VISIBILITY]): State =>
  ({
    ...state,
    messageVisibility: { ...state.messageVisibility, [messageId]: visible },
  });

const updateFullscreenNoticeVisibility = (
  state: State,
  { visibility }: ActionsToPayloads[ActionTypes.UPDATE_FULLSCREEN_NOTICE_VISIBILITY]
): State =>
  ({
    ...state,
    fullscreenNoticeVisibility: visibility,
  });

export const reducer = createReducer<ActionsToPayloads, State>({
  [ActionTypes.DISMISS_MESSAGE]: dismissMessage,
  [ActionTypes.UPDATE_MESSAGE_VISIBILITY]: updateMessageVisibility,
  [ActionTypes.UPDATE_FULLSCREEN_NOTICE_VISIBILITY]: updateFullscreenNoticeVisibility,
}, initialState);
