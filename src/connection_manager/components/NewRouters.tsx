/* eslint-disable import-x/export */
import React from 'react';
import { Button, Icons, Text } from '@zaber/react-library';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';

import { actions } from '../actions';
import { RouterType } from '../types';
import { selectRouters } from '../selectors';

import { RouterId } from './RouterId';

interface Props {
  routers: NewRouters.NewRouter[];
  onAdded?: (router: NewRouters.NewRouter) => void;
}

export const NewRouters: React.FC<Props> = ({ routers, onAdded }) => {
  const dispatch = useDispatch();
  const addConnection = (router: NewRouters.NewRouter) => {
    const { url, type } = router;
    dispatch(actions.addRouter({ url, type }));
    onAdded?.(router);
  };
  const existingRouters = useSelector(selectRouters);

  const routersWithInfo = routers.map(router => ({
    ...router,
    added: _.some(existingRouters, existingRouter => existingRouter.url === router.url),
  }));
  return (
    <div className="new-routers">
      {routersWithInfo.map(router => <div key={router.url} className="router">
        {router.type === RouterType.LocalShare && <Icons.Network/>}
        {router.type === RouterType.Cloud && <Icons.Cloud/>}
        &emsp;
        <Text className="name" t={Text.Type.H4}>{router.name}</Text>
        <RouterId router={router} copyable/>
        <div className="spacer"/>
        {!router.added && <Button onClick={() => addConnection(router)}>Create Connection</Button>}
        {router.added && <div className="added"><Icons.Tick/> Already exists</div>}
      </div>)}
    </div>
  );
};


export declare namespace NewRouters {
  export interface NewRouter {
    url: string;
    type: RouterType;
    name: string;
  }
}
