import type { EntityKey } from '../keys';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { Step } from './types';

export interface State {
  currentKey: EntityKey | null;
  step: Step | null;
  error: string | null;
}

const initialState: State = {
  currentKey: null,
  step: null,
  error: null,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const begin: Reducer<ActionTypes.BEGIN> = (state, { deviceKey }) => ({
  ...state,
  currentKey: deviceKey,
  step: 'confirm',
});

const confirm: Reducer<ActionTypes.CONFIRM> = state => ({
  ...state,
  step: 'resetting',
});

const done: Reducer<ActionTypes.DONE> = (state, { error }) => ({
  ...state,
  currentKey: null,
  step: null,
  error: error ?? null,
});

const clear: Reducer<ActionTypes.CLEAR> = () => initialState;

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.BEGIN]: begin,
  [ActionTypes.CONFIRM]: confirm,
  [ActionTypes.DONE]: done,
  [ActionTypes.CLEAR]: clear,
}, initialState);
