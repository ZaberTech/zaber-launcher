import { useEffect, useRef } from 'react';

export const useKeyboard = (onDown?: (event: KeyboardEvent) => void, onUp?: (event: KeyboardEvent) => void) => {
  const onDownRef = useRef<(event: KeyboardEvent) => void>();
  const onUpRef = useRef<(event: KeyboardEvent) => void>();

  onDownRef.current = onDown;
  onUpRef.current = onUp;

  useEffect(() => {
    const onDownHandler = (event: KeyboardEvent) => onDownRef.current?.(event);
    const onUpHandler = (event: KeyboardEvent) => onUpRef.current?.(event);

    window.addEventListener('keydown', onDownHandler);
    window.addEventListener('keyup', onUpHandler);
    return () => {
      window.removeEventListener('keydown', onDownHandler);
      window.removeEventListener('keyup', onUpHandler);
    };
  }, []);
};
