import _ from 'lodash';

import type { ConnectionManagerActionPayloads } from '../connection_manager';
import type { EntityKey } from '../keys';
import { changeDictionary, createReducer } from '../utils';
import type { Measurement } from '../units';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { IoType, Operation, Operator } from './types';

export interface State {
  selectedDevice: EntityKey | null;
  devices: Record<EntityKey, DeviceState>;
  triggers: TriggerState[];
}

export interface DeviceState {
  hasTriggers: boolean | null;
  isController: boolean | null;
  ioInfo: Record<IoType, number> | null;
  triggersExpanded: boolean[];
  error: string | null;
}

export interface TriggerState {
  enabledLoading: boolean;
  firesTotal: number;
  conditionDevice: TriggerConditionState | null;
  conditionInput: TriggerConditionState | null;
  actionsInput: TriggerActionState[];
  actionsDevice: TriggerActionState[];
  inputChanged: boolean;
  labelInput: string;
  labelDevice: string;
  error: string | null;
}

export type TriggerConditionState =
  TriggerConditionNoneState |
  TriggerConditionPosState |
  TriggerConditionDistState |
  TriggerConditionIoState |
  TriggerConditionTimeState |
  TriggerConditionSettingState;

export interface TriggerConditionNoneState {
  type: 'none';
}

export interface TriggerConditionPosState {
  type: 'Trajectory Position';
  axis: number;
  operator: Operator;
  value: Measurement;
}

export interface TriggerConditionDistState {
  type: 'Distance Interval' | 'Encoder Distance Interval';
  axis: number;
  value: Measurement;
}

export type TriggerConditionIoState = {
  type: 'IO Channel';
  ioType: IoType;
  ioChannel: number;
  operator: Operator;
  value: Measurement;
};

export type TriggerConditionTimeState = {
  type: 'Time Elapsed';
  value: Measurement;
};

export type TriggerConditionSettingState = {
  type: 'Setting' | 'Absolute Setting';
  setting: string;
  axis: number;
  operator: Operator;
  value: Measurement;
};

export type TriggerActionState =
  TriggerActionNoneState |
  TriggerActionCommandState |
  TriggerActionSettingState |
  TriggerActionSettingToSettingState;

export interface TriggerActionNoneState {
  type: 'None';
}
export interface TriggerActionCommandState {
  type: 'Command';
  axis: number;
  command: string;
}

export interface TriggerActionSettingState {
  type: 'Set Setting to Value';
  setting: string;
  axis: number;
  operation: Operation;
  value: Measurement;
}

export interface TriggerActionSettingToSettingState {
  type: 'Set Setting to Setting';
  setting: string;
  axis: number;
  operation: Operation;
  fromSetting: string;
  fromAxis: number;
}

const initialState: State = {
  selectedDevice: null,
  devices: {},
  triggers: [],
};

const initialDeviceState: DeviceState = {
  hasTriggers: null,
  isController: null,
  ioInfo: null,
  triggersExpanded: [],
  error: null,
};

const initialTriggerState: TriggerState = {
  enabledLoading: true,
  firesTotal: -1,
  conditionDevice: null,
  conditionInput: null,
  actionsInput: [],
  actionsDevice: [],
  inputChanged: false,
  labelInput: '',
  labelDevice: '',
  error: null,
};

export const defaultValue: Measurement = { value: 0, units: 'default' };

export const defaultTriggerConditionState: TriggerConditionState = {
  type: 'none',
};

export const defaultTriggerConditionPosState: TriggerConditionPosState = {
  type: 'Trajectory Position',
  axis: 1,
  operator: '==',
  value: defaultValue,
};

export const defaultTriggerConditionDistState: TriggerConditionDistState = {
  type: 'Distance Interval',
  axis: 1,
  value: defaultValue,
};

export const defaultTriggerConditionEncoderState: TriggerConditionDistState = {
  type: 'Encoder Distance Interval',
  axis: 1,
  value: defaultValue,
};

export const defaultTriggerConditionIoState: TriggerConditionIoState = {
  type: 'IO Channel',
  ioType: 'Digital Input',
  ioChannel: 1,
  operator: '==',
  value: defaultValue,
};

export const defaultTriggerConditionTimeState: TriggerConditionTimeState = {
  type: 'Time Elapsed',
  value: defaultValue,
};

export const defaultTriggerConditionSettingState: TriggerConditionSettingState = {
  type: 'Setting',
  setting: '',
  axis: 0,
  operator: '==',
  value: defaultValue,
};

export const defaultTriggerConditionSettingAbsoluteState: TriggerConditionSettingState = {
  type: 'Absolute Setting',
  setting: '',
  axis: 0,
  operator: '==',
  value: defaultValue,
};

export const defaultTriggerActionState: TriggerActionState = {
  type: 'None',
};

export const defaultTriggerActionCommandState: TriggerActionCommandState = {
  type: 'Command',
  axis: 0,
  command: '',
};

export const defaultTriggerActionSettingState: TriggerActionSettingState = {
  type: 'Set Setting to Value',
  setting: '',
  axis: 0,
  operation: '=',
  value: defaultValue,
};

export const defaultTriggerActionSettingToSettingState: TriggerActionSettingToSettingState = {
  type: 'Set Setting to Setting',
  setting: '',
  axis: 0,
  operation: '=',
  fromSetting: '',
  fromAxis: 0,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const setSelectedDevice: Reducer<ActionTypes.SET_SELECTED_DEVICE> = (state, { deviceKey }) => {
  if (deviceKey === state.selectedDevice) {
    return state;
  } else {
    return {
      ...state,
      selectedDevice: deviceKey,
      devices: deviceKey ?
        changeDictionary(state.devices, deviceKey, {}, () => initialDeviceState)
        : state.devices,
      triggers: [],
    };
  }
};

const setDeviceNoTriggers: Reducer<ActionTypes.SET_DEVICE_NO_TRIGGERS> = (state, { deviceKey }) => ({
  ...state,
  devices: changeDictionary(state.devices, deviceKey, { hasTriggers: false }),
});

const setDeviceCapabilities: Reducer<ActionTypes.SET_DEVICE_CAPABILITIES> =
  (state, { isController, numTriggers, deviceKey, ioInfo }) => {
    let deviceIoInfo = null;
    if (ioInfo.numberAnalogInputs || ioInfo.numberAnalogOutputs || ioInfo.numberDigitalInputs || ioInfo.numberDigitalOutputs) {
      deviceIoInfo = {
        'Digital Input': ioInfo.numberDigitalInputs,
        'Analog Input': ioInfo.numberAnalogInputs,
        'Digital Output': ioInfo.numberDigitalOutputs,
        'Analog Output': ioInfo.numberAnalogOutputs,
      };
    }

    return {
      ...state,
      devices: changeDictionary(
        state.devices,
        deviceKey,
        {
          hasTriggers: numTriggers > 0,
          isController,
          triggersExpanded: _.times(numTriggers).map(() => false), ioInfo: deviceIoInfo
        }),
    };
  };

const setDeviceTriggerExpanded: Reducer<ActionTypes.SET_DEVICE_TRIGGER_EXPANDED> = (state, { deviceKey, triggerIndex, expanded }) => ({
  ...state,
  devices: changeDictionary(
    state.devices,
    deviceKey,
    { triggersExpanded: state.devices[deviceKey].triggersExpanded.map((t, i) => triggerIndex === i ? expanded : t) }),
});

const setDeviceError: Reducer<ActionTypes.SET_DEVICE_ERROR> = (state, { deviceKey, error }) => ({
  ...state,
  devices: changeDictionary(state.devices, deviceKey, { error }),
});

const initializeTriggers: Reducer<ActionTypes.INITIALIZE_TRIGGERS> = (state, { numTriggers }) => {
  if (state.triggers.length === numTriggers) {
    return state;
  } else {
    return {
      ...state,
      triggers: _.times(numTriggers).map(() => ({ ...initialTriggerState })),
    };
  }
};

const setTriggerError: Reducer<ActionTypes.SET_TRIGGER_ERROR> = (state, { triggerIndex, error }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => i === triggerIndex ? { ...t, error } : t),
});

const setEnabledLoading: Reducer<ActionTypes.SET_ENABLED_LOADING> = (state, { triggerIndex, loading }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => i === triggerIndex ? { ...t, enabledLoading: loading } : t),
});

const setFiresTotal: Reducer<ActionTypes.SET_FIRES_TOTAL> = (state, { triggerIndex, firesTotal }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => i === triggerIndex ? { ...t, firesTotal } : t),
});

const setLabel: Reducer<ActionTypes.SET_LABEL> = (state, { triggerIndex, label }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => i === triggerIndex ? { ...t, labelInput: label } : t),
});

const storeLabel: Reducer<ActionTypes.STORE_LABEL> = (state, { triggerIndex, label }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => i === triggerIndex ? { ...t, labelInput: label, labelDevice: label } : t),
});

const setConditionDevice: Reducer<ActionTypes.SET_CONDITION_DEVICE> = (state, { triggerIndex, state: conditionDevice }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => i === triggerIndex ? { ...t, conditionDevice } : t),
});

const setConditionInput: Reducer<ActionTypes.SET_CONDITION_INPUT> = (state, { triggerIndex, state: conditionInput, inputChanged }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => i === triggerIndex ? {
    ...t,
    conditionInput,
    inputChanged: inputChanged ? true : t.inputChanged
  } : t),
});

const setActionInput: Reducer<ActionTypes.SET_ACTION_INPUT> = (state, { triggerIndex, actionIndex, state: actionInput, inputChanged }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => {
    if (i !== triggerIndex) {
      return t;
    } else if (actionIndex >= t.actionsInput.length) {
      return {
        ...t,
        actionsInput: [...t.actionsInput, actionInput],
        inputChanged: inputChanged ? true : t.inputChanged
      };
    } else {
      return {
        ...t,
        actionsInput: t.actionsInput.map((a, j) => j === actionIndex ? actionInput : a),
        inputChanged: inputChanged ? true : t.inputChanged
      };
    }
  }),
});

const setActionDevice: Reducer<ActionTypes.SET_ACTION_DEVICE> = (state, { triggerIndex, actionIndex, state: actionDevice }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => {
    if (i !== triggerIndex) {
      return t;
    } else if (actionIndex >= t.actionsDevice.length) {
      return { ...t, actionsDevice: [...t.actionsDevice, actionDevice] };
    } else {
      return { ...t, actionsDevice: t.actionsDevice.map((a, j) => j === actionIndex ? actionDevice : a) };
    }
  }),
});

const resetInput: Reducer<ActionTypes.RESET_INPUT> = (state, { triggerIndex }) => ({
  ...state,
  triggers: state.triggers.map((t, i) => i === triggerIndex ? {
    ...t,
    conditionInput: _.cloneDeep(t.conditionDevice),
    actionsInput: _.cloneDeep(t.actionsDevice),
    inputChanged: false,
    error: null
  } : t),
});

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.SET_SELECTED_DEVICE]: setSelectedDevice,
  [ActionTypes.SET_DEVICE_NO_TRIGGERS]: setDeviceNoTriggers,
  [ActionTypes.SET_DEVICE_CAPABILITIES]: setDeviceCapabilities,
  [ActionTypes.SET_DEVICE_TRIGGER_EXPANDED]: setDeviceTriggerExpanded,
  [ActionTypes.SET_DEVICE_ERROR]: setDeviceError,
  [ActionTypes.INITIALIZE_TRIGGERS]: initializeTriggers,
  [ActionTypes.SET_TRIGGER_ERROR]: setTriggerError,
  [ActionTypes.SET_ENABLED_LOADING]: setEnabledLoading,
  [ActionTypes.SET_FIRES_TOTAL]: setFiresTotal,
  [ActionTypes.SET_LABEL]: setLabel,
  [ActionTypes.STORE_LABEL]: storeLabel,
  [ActionTypes.SET_CONDITION_DEVICE]: setConditionDevice,
  [ActionTypes.SET_CONDITION_INPUT]: setConditionInput,
  [ActionTypes.SET_ACTION_INPUT]: setActionInput,
  [ActionTypes.SET_ACTION_DEVICE]: setActionDevice,
  [ActionTypes.RESET_INPUT]: resetInput,
}, initialState);
