import type { ascii } from '@zaber/motion';

import type { AsciiComms } from '../connection_manager/types';


import { dataDefinitionTemplate, FieldIds, SectionIds } from './data_definitions';
import type { SectionDataDefinition, SectionValues, Values } from './types';

export function getSectionFields<S extends SectionIds>(sectionId: S, values: Values) {
  return values[sectionId] as SectionValues<S>;
}

export function fieldEnabled<S extends SectionIds>(sectionId: S, fieldId: FieldIds<S>, values: SectionValues<S>): boolean {
  const template = dataDefinitionTemplate[sectionId] as SectionDataDefinition<S>;
  const condition = template.fieldEnabled[fieldId];
  if (condition == null) { return true }

  return condition(values);
}

export function getAxis(comms: AsciiComms): ascii.Axis {
  if ((comms as Partial<ascii.Axis>).axisNumber != null) {
    return comms as ascii.Axis;
  } else {
    return (comms as ascii.Device).getAxis(1);
  }
}
