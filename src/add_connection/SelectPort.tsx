import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import {
  ButtonNext, ButtonPrevious, Input, HelpTooltip, NoticeBanner, Checkbox, Text, SimpleSelect,
  ButtonRow
} from '@zaber/react-library';

import { MyNumber, useActions } from '../utils';
import { environment } from '../environment';

import { actions as actionsDefinition } from './actions';
import { selectPortsWithInfo, selectSerialPort } from './selectors';
import { Steps, DEFAULT_BAUD_RATE } from './types';

export const SelectPort: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const ports = useSelector(selectPortsWithInfo);
  const  { selectedPort, baudRate, showOnlyRecommendedPorts } = useSelector(selectSerialPort);
  useEffect(() => {
    actions.startMonitoringPorts();
    return () => { actions.stopMonitoringPorts() };
  }, []);

  if (!ports) {
    return null;
  }
  return (
    <div className="select-port">
      <Text className="subheading" t={Text.Type.BodyLg}>Select the port from the list below:</Text>

      <SimpleSelect<string>
        title="Select Port"
        value={selectedPort ?? ''}
        onValueChange={actions.changePort}
        options={ports.map(port => ({ label: `${port.name}${port.exists ? ' (Already added)' : ''}`,  value: port.name }))}
        portalToBody
      />

      {environment.platform === 'darwin' && <Checkbox
        labelContent="Show Only Recommended Serial Ports"
        checked={showOnlyRecommendedPorts}
        onChecked={checked => {
          actions.showOnlyRecommendedPorts(checked);
          if (checked) {
            actions.clearPort();
          }
        }}
      />}

      <div className="baud-rate">
        <Input type="number" step="1" min="1" max="10000000"
          labelContent="Baud Rate"
          value={MyNumber.isFinite(baudRate) ? baudRate : DEFAULT_BAUD_RATE}
          onValueChange={value => actions.changeBaudRate(value !== '' ? +value : null)}/>
        <HelpTooltip>
          Baud rate refers to the data transmission rate of the serial port.
          When in doubt, leave it at the default value of 115200.
        </HelpTooltip>
      </div>

      <NoticeBanner type="info">
        Connecting the USB cable to the computer will select the port for you.
      </NoticeBanner>

      <ButtonRow justify="spread">
        <ButtonPrevious onClick={() => actions.changeStep(Steps.CHOOSE_CONNECTION_TYPE)}>Back</ButtonPrevious>
        <ButtonNext disabled={!selectedPort} onClick={() => actions.changeStep(Steps.DETECT_DEVICES)}>Next</ButtonNext>
      </ButtonRow>
    </div>
  );
};
