import { Container } from 'inversify';

import { createContainer, destroyContainer } from '../container';
import { mockStorage, StorageMock } from '../test';

import { PreferencesService, PREFERENCES_STORAGE_KEYS } from './service';
import { PreferenceKeys, DEFAULT_VALUES } from './types';

let service: PreferencesService;
let container: Container;
let storageMock: StorageMock;

beforeEach(() => {
  container = createContainer();
  storageMock = mockStorage(container);
});

afterEach(() => {
  destroyContainer();
  service = null!;
});

describe('retrieveInitialValues', () => {
  test('uses default value if no value in storage', async () => {
    const defaultValue = DEFAULT_VALUES[PreferenceKeys.ZML_LOGGING];
    const initialValues = container.get(PreferencesService).retrieveInitialValues();

    expect(initialValues[PreferenceKeys.ZML_LOGGING]).toEqual(defaultValue);
  });

  test('gets value from storage', async () => {
    storageMock.stored[PREFERENCES_STORAGE_KEYS[PreferenceKeys.ZML_LOGGING]] = false;
    const initialValues = container.get(PreferencesService).retrieveInitialValues();

    expect(initialValues[PreferenceKeys.ZML_LOGGING]).toEqual(false);
  });
});

describe('setter', () => {
  beforeEach(() => {
    service = container.get(PreferencesService);
  });

  test('notifies listeners when value is set', async () => {
    await service.setPreferenceValue(PreferenceKeys.ZML_LOGGING, true);

    const handler = jest.fn().mockResolvedValue(null);
    service.registerHandler(PreferenceKeys.ZML_LOGGING, handler);
    const unusedHandler = jest.fn().mockResolvedValue(null);
    service.registerHandler(PreferenceKeys.TIMEOUT_LOCAL, unusedHandler);

    expect(handler).toHaveBeenCalledTimes(0);
    expect(unusedHandler).toHaveBeenCalledTimes(0);

    await service.setPreferenceValue(PreferenceKeys.ZML_LOGGING, false);

    expect(handler).toHaveBeenCalledTimes(1);
    expect(handler).toHaveBeenLastCalledWith(false);
    expect(unusedHandler).toHaveBeenCalledTimes(0);
  });

  test('stores value if store arg is true', async () => {
    storageMock.stored[PREFERENCES_STORAGE_KEYS[PreferenceKeys.ZML_LOGGING]] = true;

    await service.setPreferenceValue(PreferenceKeys.ZML_LOGGING, false, true);

    expect(storageMock.save).toHaveBeenCalledTimes(1);
    expect(storageMock.stored[PREFERENCES_STORAGE_KEYS[PreferenceKeys.ZML_LOGGING]]).toEqual(false);
  });

  test('does not store value if store arg is false', async () => {
    await service.setPreferenceValue(PreferenceKeys.ZML_LOGGING, false, false);

    expect(storageMock.save).toHaveBeenCalledTimes(0);
  });
});
