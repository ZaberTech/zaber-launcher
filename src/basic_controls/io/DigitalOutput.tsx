import { Toggle, Text, Flex } from '@zaber/react-library';
import React from 'react';
import { useSelector } from 'react-redux';
import { IoPortType } from '@zaber/motion/ascii';

import type { EntityKey } from '../../keys';
import { useActions } from '../../utils';

import { actionDefinitions } from './actions';
import { selectDeviceStates } from './selectors';

type DigitalOutputProps = {
  deviceKey: EntityKey;
  channel: number;
  ports: boolean[] | null;
};

export const DigitalOutput: React.FC<DigitalOutputProps> = ({ deviceKey, channel, ports }) => {
  const output = ports?.[channel - 1];
  const pendingChange = useSelector(selectDeviceStates)[deviceKey]?.digitalChanges[channel];
  const isOn = pendingChange ?? output === true;
  const isLoading = output == null || pendingChange != null;
  const actions = useActions(actionDefinitions);
  const labels = useSelector(selectDeviceStates)[deviceKey]?.labels;
  const label = labels?.[`${IoPortType.DIGITAL_OUTPUT}-${channel}`] ?? '';

  return <Flex.Row className="do control">
    <Text e={Text.Emphasis.Bold}>{channel}.</Text>
    <Toggle
      title={isLoading ? `Digital Output ${channel} Loading...` : `Turn Digital Output ${channel} ${isOn ? 'Off' : 'On'}`}
      value={isOn}
      loading={isLoading}
      onValueChange={on => actions.setDigital(deviceKey, channel, on)}/>
    <Text>{label}</Text>
  </Flex.Row>;
};
