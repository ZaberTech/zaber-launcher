import { Button, Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import classNames from 'classnames';
import React from 'react';
import { useSelector } from 'react-redux';

import { selectUser } from '../cloud/selectors';

import { actions as actionDefinitions } from './actions';

const DEFAULT_FEEDBACK_BUTTON_CONTENT = <><Icons.Feedback/>&ensp;Feedback</>;

type ButtonProps = Omit<React.ComponentProps<typeof Button>, 'onClick'>;

export const FeedbackButton: React.FC<ButtonProps> = ({
  children = DEFAULT_FEEDBACK_BUTTON_CONTENT,
  className,
  title = 'Open Feedback Form',
  ...rest
}) => {
  const actions = useActions(actionDefinitions);
  const loggedInUserEmail = useSelector(selectUser)?.email;

  return (
    <Button
      className={classNames('feedback-button', className)}
      onClick={() => {
        if (loggedInUserEmail) {
          actions.setEmail(loggedInUserEmail);
        }
        actions.open();
      }}
      title={title}
      {...rest}>
      {children}
    </Button>
  );
};
