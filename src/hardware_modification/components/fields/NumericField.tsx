import { NumericInput } from '@zaber/react-library';
import React from 'react';

import type { HwModInputProps } from './HwModField';

const STATUS_MAP: Record<HwModInputProps['status'], React.ComponentProps<typeof NumericInput>['status']> = {
  none: null,
  unset: 'warning',
  invalid: 'error',
  changed: 'info',
};

export const HwModNumericInput: React.FC<HwModInputProps> = ({ value, status, disabled, testid, onUpdate }) => (
  <NumericInput
    {...{ value, disabled, status: STATUS_MAP[status] }} onNumberChange={value => onUpdate(value)}
    className={status} data-testid={`${testid}-input`}
  />
);
