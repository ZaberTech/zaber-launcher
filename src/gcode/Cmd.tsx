import React, { useEffect, useState } from 'react';
import { Icons, Input, NoticeBanner } from '@zaber/react-library';
import { useSelector } from 'react-redux';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectCommand, selectDisabled, selectStatus } from './selectors';

const MAX_HISTORY = 20;

export const Cmd: React.FC = () => {
  const actions = useActions(actionsDefinition);

  const [input, setInput] = useState('');
  const [history, setHistory] = useState<string[]>([]);
  const [historyIndex, setHistoryIndex] = useState(0);

  const disabled = useSelector(selectDisabled);
  const status = useSelector(selectStatus);
  const command = useSelector(selectCommand);
  const success = (command.error == null && command.warnings.length === 0);

  useEffect(() => {
    if (!command.executing && success) {
      setInput('');
    }
  }, [command.executing]);

  const send = () => {
    if (disabled) { return }

    actions.command(input);
    if (input) {
      setHistory([input, ...history.filter(old => old !== input)].slice(0, MAX_HISTORY));
    }
    setHistoryIndex(0);
  };
  const clear = () => {
    setInput('');
    setHistoryIndex(0);
    actions.commandClear();
  };

  const onKeyDown: React.KeyboardEventHandler<HTMLInputElement> = e => {
    if (e.key === 'Enter') {
      send();
    } else if (e.key === 'Escape') {
      clear();
    } else if (e.key === 'ArrowUp') {
      e.preventDefault();
      if (historyIndex < history.length) {
        setInput(history[historyIndex]);
        setHistoryIndex(Math.min(historyIndex + 1, history.length - 1));
      }
    } else if (e.key === 'ArrowDown') {
      e.preventDefault();
      if (historyIndex > 0) {
        setInput(history[historyIndex - 1]);
        setHistoryIndex(historyIndex - 1);
      } else {
        setInput('');
      }
    }
  };

  return (<div className="cmd">
    {command.error && <NoticeBanner type="error">{command.error}</NoticeBanner>}
    {command.warnings.map((warning, i) => <NoticeBanner key={i} type="warning">{warning}</NoticeBanner>)}
    <div className="line">
      <Input disabled={!status.translatorRunning}
        value={input} onValueChange={setInput} onKeyDown={onKeyDown}
        placeholder="Input your command..."/>
      <Icons.Enter disabled={disabled || !status.translatorRunning} title="Send" onClick={send}/>
      <Icons.Cross title="Clear" onClick={clear}/>
    </div>
  </div>);
};
