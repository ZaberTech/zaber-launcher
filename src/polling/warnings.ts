import type { RT } from '../utils';

import type { usePolledProductWarnings } from './hooks';

const DISABLING_FLAGS = ['FH', 'FV', 'FO', 'FC', 'FM', 'FD'];

export function includesDisablingFlag(warnings: RT<typeof usePolledProductWarnings>) {
  return warnings.type === 'flags' && warnings.flags.some(flag => DISABLING_FLAGS.includes(flag));
}
