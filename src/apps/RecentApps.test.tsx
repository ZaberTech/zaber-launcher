import React from 'react';
import { routerActions } from 'connected-react-router';
import { RenderResult, render } from '@testing-library/react';

import { wrapWithNewStore, wrapWithRouter, mockStorage, StorageMock  } from '../test';
import { createContainer, destroyContainer } from '../container';
import { reduxStoreSymbol } from '../store';

import { RecentApps } from './RecentApps';
import { actions } from './actions';
import { loadRecent, PINNED_APPS_KEY, RECENT_APPS_KEY, registerPageUnload } from './sagas';
import { AppUrls, FILTERED_APP_LIST as APP_LIST, MAX_RECENT_APPS } from './list';

const TestRecentApps = wrapWithNewStore(wrapWithRouter(RecentApps));

let wrapper: RenderResult;

let storageMock: StorageMock;

beforeEach(() => {
  const container = createContainer();
  storageMock = mockStorage(container);

  wrapper = render(<TestRecentApps/>);
  container.bind(reduxStoreSymbol).toConstantValue(TestRecentApps.testStore);
});

afterEach(() => {
  TestRecentApps.testStore.dispatch(routerActions.push('/nowhere'));
  destroyContainer();

  wrapper.unmount();
  wrapper = null!;
});

test('it loads default recent app for the first time', () => {
  TestRecentApps.testStore.saga.run(loadRecent);
  wrapper.getByText('Basic Controls');
  wrapper.getByText('Device Settings');
  wrapper.getByText('Terminal');
});

test('it loads previously stored apps', () => {
  const previouslyStored: AppUrls[] = ['/current-tuner'];
  storageMock.stored[RECENT_APPS_KEY] = previouslyStored;

  TestRecentApps.testStore.saga.run(loadRecent);
  wrapper.getByText('Current Tuner');
});

test('after accessing app it appears in the recent apps', () => {
  expect(wrapper.queryByText(APP_LIST[0].name)).toBeNull();
  TestRecentApps.testStore.dispatch(routerActions.push(APP_LIST[0].url));
  wrapper.getByText(APP_LIST[0].name);
});

function fillRecentApps() {
  for (let i = 0; i < MAX_RECENT_APPS; i++) {
    TestRecentApps.testStore.dispatch(routerActions.push(APP_LIST[i].url));
  }
}

test('once runs out of space it replaces the app accessed the longest ago', () => {
  fillRecentApps();

  let oldestApp = APP_LIST[0];
  wrapper.getByText(oldestApp.name);

  let newApp = APP_LIST[MAX_RECENT_APPS];
  TestRecentApps.testStore.dispatch(routerActions.push(newApp.url));

  wrapper.getByText(newApp.name);
  expect(wrapper.queryByText(oldestApp.name)).toBeNull();

  const oldestToBecomeYounger = APP_LIST[1];
  TestRecentApps.testStore.dispatch(routerActions.push(oldestToBecomeYounger.url));
  oldestApp = APP_LIST[2];

  newApp = APP_LIST[0];
  TestRecentApps.testStore.dispatch(routerActions.push(newApp.url));

  wrapper.getByText(newApp.name);
  wrapper.getByText(oldestToBecomeYounger.name);
  expect(wrapper.queryByText(oldestApp.name)).toBeNull();
});

test('pinning app places on top of menu; unpinning removes it', () => {
  fillRecentApps();

  const pinnedApp = APP_LIST[MAX_RECENT_APPS + 1];
  TestRecentApps.testStore.dispatch(actions.pinApp(pinnedApp.url, true));

  wrapper.getByText(pinnedApp.name);
  expect(wrapper.baseElement.textContent?.startsWith(pinnedApp.name)).toBeTruthy();

  TestRecentApps.testStore.dispatch(actions.pinApp(pinnedApp.url, false));
  expect(wrapper.queryByText(pinnedApp.name)).toBeNull();
});

test('visiting pinned app does not change recent apps', () => {
  fillRecentApps();

  const pinnedApp = APP_LIST[MAX_RECENT_APPS + 1];
  TestRecentApps.testStore.dispatch(actions.pinApp(pinnedApp.url, true));

  TestRecentApps.testStore.dispatch(routerActions.push(pinnedApp.url));

  for (let i = 0; i < MAX_RECENT_APPS; i++) {
    wrapper.getByText(APP_LIST[i].name);
  }
});

test('pinning a recent app removes it from recent', () => {
  fillRecentApps();

  const app = APP_LIST[0];
  TestRecentApps.testStore.dispatch(actions.pinApp(app.url, true));

  wrapper.getByText(app.name);

  const NEW_RECENT = 2;
  for (let i = 0; i < NEW_RECENT; i++) {
    TestRecentApps.testStore.dispatch(routerActions.push(APP_LIST[MAX_RECENT_APPS + i].url));
  }

  wrapper.getByText(app.name);

  // all the recent apps are still present
  for (let i = 0; i < MAX_RECENT_APPS; i++) {
    wrapper.getByText(APP_LIST[NEW_RECENT + i].name);
  }
});

test('stores recent and pinned apps on unload', async () => {
  registerPageUnload();

  for (let i = 0; i < 2; i++) {
    TestRecentApps.testStore.dispatch(routerActions.push(APP_LIST[i].url));
  }
  for (let i = 2; i < 4; i++) {
    TestRecentApps.testStore.dispatch(actions.pinApp(APP_LIST[i].url, true));
  }

  window.dispatchEvent(new Event('unload'));

  const recentApps = storageMock.stored[RECENT_APPS_KEY] as AppUrls[];
  expect(recentApps).toEqual([APP_LIST[1].url, APP_LIST[0].url]);

  const pinnedApps = storageMock.stored[PINNED_APPS_KEY] as AppUrls[];
  expect(pinnedApps).toEqual([APP_LIST[2].url, APP_LIST[3].url]);
});
