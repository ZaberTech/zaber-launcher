import dns from 'dns';

import { eventChannel, EventChannel, SagaIterator } from 'redux-saga';
import { all, put, delay, take, race, takeLeading } from 'redux-saga/effects';
import MDNS, { MDNSAnswer, MDNSResponse, SRVResponseData } from 'multicast-dns';
import _ from 'lodash';
import schema, { validate } from 'jsonschema';

import type { Logger } from '../app_components';
import { ensureError, splitOnce } from '../utils';
import { makeLocalShareUrl } from '../connection_manager';
import { getLogger } from '../log';

import { actions, ActionTypes } from './actions';
import {
  LocalShareCandidate, LOCAL_SHARE_DISCOVERY_INACTIVE_TIMEOUT,
  LOCAL_SHARE_DISCOVERY_SERVICE_SUFFIX,
} from './types';

let logger: Logger;

export function* discoverySaga(): SagaIterator {
  logger = getLogger('discovery');

  yield all([
    takeLeading(ActionTypes.DISCOVER, discoverCandidates),
  ]);
}

interface TxtRecordData {
  name: string;
}

const LOCAL_SUFFIX = /\.local\.?$/;

const txtRecordDataSchema: schema.Schema = {
  type: 'object',
  properties: {
    name: {
      type: 'string',
      minLength: 1,
    },
  },
  required: ['name'],
};

interface DiscoveryContext {
  candidates: Record<string, Partial<LocalShareCandidate>>;
  emit: (candidate: LocalShareCandidate) => void;
}

async function processRecord({ candidates, emit }: DiscoveryContext, record: MDNSAnswer) {
  let candidate = candidates[record.name];
  if (!candidate) {
    candidate = {
      serviceName: record.name,
    };
    candidates[record.name] = candidate;
  }

  if (record.type === 'SRV') {
    const data = record.data as SRVResponseData;
    let hostname = data.target;
    let canResolve = await dnsCanResolve(hostname);
    if (!canResolve) {
      hostname = hostname.replace(LOCAL_SUFFIX, '');
      canResolve = await dnsCanResolve(hostname);
    }
    if (!canResolve) {
      return;
    }
    const url = makeLocalShareUrl(hostname, data.port);
    candidate.url = url;
  } else if (record.type === 'TXT') {
    try {
      const records = (record.data as Buffer[]).map(buffer => splitOnce(buffer.toString('ascii'), '='));
      const info: TxtRecordData = _.fromPairs(records) as unknown as TxtRecordData;

      validate(info, txtRecordDataSchema, { throwFirst: true });

      candidate.name = info.name;
    } catch (err) {
      logger.warn(err);
    }
  }

  const isComplete = candidate.name && candidate.url;
  if (isComplete) {
    emit(_.cloneDeep(candidate as LocalShareCandidate));
  }
}

function mdnsDiscoveryChannel(search: string): EventChannel<LocalShareCandidate | Error> {
  return eventChannel<LocalShareCandidate | Error>(emit => {
    const context: DiscoveryContext = {
      candidates: {},
      emit,
    };

    const handler = (response: MDNSResponse) => {
      (async () => {
        const records = response.answers.concat(response.additionals)
          .filter(record => record.name?.endsWith(search) && ['SRV', 'TXT'].includes(record.type));

        for (const record of records) {
          await processRecord(context, record);
        }
      })().catch(err => emit(ensureError(err)));
    };

    const mdns = MDNS();
    mdns.on('response', handler);
    mdns.on('error', err => emit(ensureError(err)));
    mdns.query({ questions: [{ name: search, type: 'PTR' }] });

    return () => {
      mdns.off('response', handler);
      mdns.destroy();
    };
  });
}

export function* discoverCandidates(): SagaIterator {
  yield put(actions.discoverClearCandidates());

  try {
    const channel = mdnsDiscoveryChannel(LOCAL_SHARE_DISCOVERY_SERVICE_SUFFIX);
    try {
      while (true) {
        const { candidate }: {
          candidate?: LocalShareCandidate | Error;
        } = yield race({
          candidate: take(channel),
          timeout: delay(LOCAL_SHARE_DISCOVERY_INACTIVE_TIMEOUT),
          cancelled: take(ActionTypes.DISCOVER_CANCEL),
        });

        if (!candidate) { break }

        if (candidate instanceof Error) {
          throw candidate;
        }
        yield put(actions.discoverAddCandidate(candidate));
      }
    } finally {
      channel.close();
    }
    yield put(actions.discoverDone());
  } catch (err) {
    yield put(actions.discoverDone(ensureError(err).message));
  }
}

function dnsCanResolve(name: string) {
  return new Promise<boolean>(resolve => dns.lookup(name, 0, err => resolve(err == null)));
}
