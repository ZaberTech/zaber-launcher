import _ from 'lodash';
import { Acceleration, CommandFailedException, CommandFailedExceptionData, Length, Time, Units, Velocity } from '@zaber/motion';
import type { GetSetting } from '@zaber/motion/ascii';
import { injectable } from 'inversify';
import React, { Component } from 'react';
import { Subject } from 'rxjs';

import type { ComboBoxOption } from '../types';
import {
  AxisMockBase,
  ConnectionMockBase,
  DeviceMockBase,
  MessageRoutersServiceMockBase,
  RouterConnectionMockBase
} from '../../test/mocks/ascii';
import { MonitorMessage, MonitorMessageSource } from '../../app_components';


declare namespace SelectMock {
  interface Props {
    placeholder: string;
    value: ComboBoxOption;
    onChange: (item: unknown) => void;
    options: ComboBoxOption[];
    'data-testid': string;
  }
}

export class SelectMock extends Component<SelectMock.Props> {
  render() {
    const { placeholder, options, value, 'data-testid': testid, onChange } = this.props;

    return <input
      placeholder={placeholder}
      onChange={e => onChange(options.find(opt => `${opt.value}` === e.target.value))}
      data-testid={testid}
      value={value?.value}/>;
  }

  static createFilter() { return true }
}


export const chartsMock = jest.fn();

export class EChartsReactMock extends Component {
  render(): React.ReactNode {
    chartsMock(this.props);
    return <div>Chart</div>;
  }

  getEchartsInstance = jest.fn().mockReturnThis();
  getZr = jest.fn().mockReturnThis();
  on = jest.fn().mockReturnThis();
  getDataURL = jest.fn().mockReturnValue('data:image/png;base64,R29waGVyID4gUHl0aG9u');
}


export const makeRejectReason = (data: string): CommandFailedExceptionData => ({
  responseData: data,
  replyFlag: 'RJ',
} as CommandFailedExceptionData);


let axisCallback: (axis: AxisMock) => void;
let deviceCallback: (device: DeviceMockBaseForScope) => void;
let monitorConnections: MonitorConnectionMock[] = [];

export function setAxisCallback(cb: ((axis: AxisMock) => void) | null) {
  axisCallback = cb!;
}

export function setDeviceCallback(cb: ((axis: DeviceMockBaseForScope) => void) | null) {
  deviceCallback = cb!;
}

export function getMonitor() {
  return _.last(monitorConnections);
}

export function clearMonitors() {
  monitorConnections = [];
}

export class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMockBaseForScope) {
    super(id, device);

    if (axisCallback) { axisCallback(this) }
  }

  _settings: Record<string, number> = {
  };

  _pos = 0;
  getPosition = jest.fn(() => Promise.resolve(this._pos));

  settings = {
    get: jest.fn(setting => {
      const value = this._settings[setting];
      if (value !== undefined) {
        return Promise.resolve(value);
      }

      return Promise.reject(new CommandFailedException('', makeRejectReason('BADCOMMAND')));
    }),
    convertFromNativeUnits: jest.fn((_, value, units) => {
      switch (units) {
        case Length.mm: return value / 1000000;
        case Velocity.MILLIMETRES_PER_SECOND: return value / 1000;
        case Acceleration.MILLIMETRES_PER_SECOND_SQUARED: return value / 10;
      }

      throw new Error(`Unknown axis units ${units}`);
    }),
  };

  stop = jest.fn().mockResolvedValue(undefined);
  genericCommandNoResponse = jest.fn();
  moveAbsolute = jest.fn(async (position: number, units: Units) => {
    if (units === Length.cm) {
      this._pos = position * 100;
    } else {
      this._pos = position;
    }
  });

  moveRelative = jest.fn(async (position: number, _units: Units) => {
    this._pos = this._pos + position;
  });
}


export class DeviceMockBaseForScope extends DeviceMockBase<AxisMock> {
  _settings: Record<string, number> = {};
  _channels: string[] = [];
  _running = false;
  _data: unknown[] = [];
  _timebase = 0;

  settings = {
    get: jest.fn(setting => {
      const value = this._settings[setting];
      if (value !== undefined) {
        return Promise.resolve(value);
      }

      return Promise.reject(new CommandFailedException('', makeRejectReason('BADCOMMAND')));
    }),
    getMany: jest.fn((gets: GetSetting[]) => Promise.resolve(_.map(gets, (getting: GetSetting) => ({
      setting: getting.setting,
      values: [this._settings[getting.setting]],
      unit: getting.unit,
    })))),
    convertFromNativeUnits: jest.fn((_, value, units) => {
      switch (units) {
        case Time.MILLISECONDS: return value / 1000;
      }

      throw new Error(`Unknown device units ${units}`);
    }),
  };

  oscilloscope = {
    clear: jest.fn(async () => this._channels = []),
    setDelay: jest.fn(),
    getDelay: jest.fn(() => Promise.resolve(0)),
    setTimebase: jest.fn(val => this._timebase = val),
    getTimebase: jest.fn(() => Promise.resolve(this._timebase)),
    addChannel: jest.fn(async name => this._channels.push(name)),
    start: jest.fn(() => { this._running = true }),
    stop: jest.fn(() => this._running = false),
    read: jest.fn(() => Promise.resolve(this._data)),
    getMaxBufferSize: jest.fn(() => Promise.resolve(6000)),
    getBufferSize: jest.fn(() => Promise.resolve(6000)),
  };

  firmwareVersion = { major: 7, minor: 33, build: 0 };

  genericCommandNoResponse = jest.fn();
}


export class DeviceMockIntegrated extends DeviceMockBaseForScope {
  constructor(readonly deviceAddress: number, readonly connection: ConnectionMockBase<AxisMock, DeviceMockBase<AxisMock>>) {
    super(deviceAddress, connection);
    this._settings = {
      ...this._settings,
      ...{
        'scope.delay': 0,
        'scope.timebase': 1,
        'system.access': 2,
        'pos': 0,
        'encoder.pos': 0,
      },
    };

    deviceCallback?.(this);
  }

  isIntegrated = true;
  axisCount = 1;

  _pos = 0;
  getPosition = jest.fn(() => Promise.resolve(this._pos));

  stop = jest.fn().mockResolvedValue(undefined);
  moveAbsolute = jest.fn(async (position: number, units: Units) => {
    if (units === Length.cm) {
      this._pos = position * 100;
    } else {
      this._pos = position;
    }
  });

  moveRelative = jest.fn(async (position: number, _units: Units) => {
    this._pos = this._pos + position;
  });

  io = {
    getChannelsInfo: jest.fn(async () => Promise.resolve({
      numberAnalogInputs: 0,
      numberAnalogOutputs: 0,
      numberDigitalInputs: 0,
      numberDigitalOutputs: 0,
    })),
    getAllAnalogInputs: jest.fn(async () => Promise.resolve([])),
    getAllAnalogOutputs: jest.fn(async () => Promise.resolve([])),
    getAllDigitalInputs: jest.fn(async () => Promise.resolve([])),
    getAllDigitalOutputs: jest.fn(async () => Promise.resolve([])),
  };
}


export class DeviceMockMultiAxis extends DeviceMockBaseForScope {
  constructor(readonly deviceAddress: number, readonly connection: ConnectionMockBase<AxisMock, DeviceMockBase<AxisMock>>) {
    super(deviceAddress, connection);
    this._settings = {
      ...this._settings,
      ...{
        'scope.delay': 0,
        'scope.timebase': 1,
        'system.access': 2,
      },
    };

    deviceCallback?.(this);
  }

  io = {
    getChannelsInfo: jest.fn(async () => Promise.resolve({
      numberAnalogInputs: 4,
      numberAnalogOutputs: 1,
      numberDigitalInputs: 4,
      numberDigitalOutputs: 4,
    })),
    getAllAnalogInputs: jest.fn(async () => Promise.resolve([12.1, 12.2, 12.3, 12.4])),
    getAllAnalogOutputs: jest.fn(async () => Promise.resolve([11])),
    getAllDigitalInputs: jest.fn(async () => Promise.resolve([0, 1, 0, 1])),
    getAllDigitalOutputs: jest.fn(async () => Promise.resolve([1, 0, 1, 0])),
  };

  isIntegrated = false;
  axisCount = 2;
}


export class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMockIntegrated> {
  genericCommandNoResponse = jest.fn().mockResolvedValue(undefined);
}


class MonitorConnectionMock {
  monitorMessages = new Subject<MonitorMessage[]>();

  constructor(public readonly id: string, public readonly router: RouterConnectionMock) {
    monitorConnections.push(this);
  }

  start = jest.fn();
  stop = jest.fn().mockResolvedValue(undefined);

  mockRequest = jest.fn((message: string, source: MonitorMessageSource = 'cmd') => this.monitorMessages.next([{ message, source }]));
  mockResponse = jest.fn((message: string, source: MonitorMessageSource = 'reply') => this.monitorMessages.next([{ message, source }]));
}


export class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMockIntegrated, ConnectionMock> {
  createMonitorConnection = jest.fn((id: string) => new MonitorConnectionMock(id, this));
}

@injectable()
export class MessageRoutersServiceMockIntegrated
  extends MessageRoutersServiceMockBase<AxisMock, DeviceMockIntegrated, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMockIntegrated;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

@injectable()
export class MessageRoutersServiceMockMultiAxis
  extends MessageRoutersServiceMockBase<AxisMock, DeviceMockMultiAxis, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMockMultiAxis;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}
