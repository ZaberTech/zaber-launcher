import { DeviceIdentifiers, Version, VersionList } from '../app_components';
import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { InstallParams, UpgradeProgress, VersionSkipMap } from './types';

export enum ActionTypes {
  // Manage app state
  SET_SELECTED_DEVICE = 'FWU_SET_SELECTED_DEVICE',
  REFRESH_VERSIONS = 'FWU_REFRESH_VERSIONS',
  FIRMWARE_VERSIONS_LOADED = 'FWU_FIRMWARE_VERSIONS_LOADED',
  FIRMWARE_VERSIONS_ERR = 'FWU_FIRMWARE_VERSIONS_ERR',
  SET_VERSIONS_SKIPPED = 'FWU_SET_VERSIONS_SKIPPED',
  SKIP_VERSION = 'FWU_SKIP_VERSION',
  SET_INSTALL_PROGRESS = 'FWU_SET_INSTALL_PROGRESS',
  OPEN_FILE = 'FWU-OPEN_FILE',
  // Manage installing the new firmware
  BEGIN_SELF_SERVE_UPGRADE = 'FWU-BEGIN_SELF_SERVE_UPGRADE',
  BEGIN_FROM_FILE_UPGRADE = 'FWU-BEGIN_FROM_FILE_UPGRADE',
  INSTALL_FIRMWARE = 'FWU_INSTALL_FIRMWARE',
  CANCEL_INSTALL_NEW_FIRMWARE = 'FWU_CANCEL_INSTALL_NEW_FIRMWARE',
}

export interface ActionsToPayloads {
  [ActionTypes.SET_SELECTED_DEVICE]: { deviceKey: EntityKey | null };
  [ActionTypes.REFRESH_VERSIONS]: { deviceKey: EntityKey };
  [ActionTypes.FIRMWARE_VERSIONS_LOADED]: { connectionKey: EntityKey; identifiers: DeviceIdentifiers[]; versions: VersionList };
  [ActionTypes.FIRMWARE_VERSIONS_ERR]: { connectionKey: EntityKey; error: string };
  [ActionTypes.SET_VERSIONS_SKIPPED]: { versionsSkipped: VersionSkipMap };
  [ActionTypes.SKIP_VERSION]: { deviceSerialNumber: number; version: Version};
  [ActionTypes.SET_INSTALL_PROGRESS]: { progress: UpgradeProgress | null };
  [ActionTypes.OPEN_FILE]: void;

  [ActionTypes.BEGIN_SELF_SERVE_UPGRADE]: {
    deviceKey: EntityKey;
    identifiers: DeviceIdentifiers;
    version: Version;
    offerSaveState: boolean;
  };
  [ActionTypes.BEGIN_FROM_FILE_UPGRADE]: InstallParams;
  [ActionTypes.INSTALL_FIRMWARE]: {installParams: InstallParams; withSave: boolean };
  [ActionTypes.CANCEL_INSTALL_NEW_FIRMWARE]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  setSelectedDevice: (deviceKey: EntityKey | null) => buildAction(ActionTypes.SET_SELECTED_DEVICE, { deviceKey }),
  refreshVersions: (deviceKey: EntityKey) => buildAction(ActionTypes.REFRESH_VERSIONS, { deviceKey }),
  firmwareVersionsLoaded: (connectionKey: EntityKey, identifiers: DeviceIdentifiers[], versions: VersionList) =>
    buildAction(ActionTypes.FIRMWARE_VERSIONS_LOADED, { connectionKey, identifiers, versions }),
  firmwareVersionsErr: (connectionKey: EntityKey, error: string) =>
    buildAction(ActionTypes.FIRMWARE_VERSIONS_ERR, { connectionKey, error }),
  setVersionsSkipped: (versionsSkipped: VersionSkipMap) =>
    buildAction(ActionTypes.SET_VERSIONS_SKIPPED, { versionsSkipped }),
  skipVersion: (deviceSerialNumber: number, version: Version) =>
    buildAction(ActionTypes.SKIP_VERSION, { deviceSerialNumber, version }),
  setInstallProgress: (progress: UpgradeProgress | null) => buildAction(ActionTypes.SET_INSTALL_PROGRESS, { progress }),
  openFile: () => buildAction(ActionTypes.OPEN_FILE),

  downloadAndInstall: (deviceKey: EntityKey, identifiers: DeviceIdentifiers, version: Version, offerSaveState = true) =>
    buildAction(ActionTypes.BEGIN_SELF_SERVE_UPGRADE, { deviceKey, identifiers, version, offerSaveState }),
  beginInstall: (deviceKey: EntityKey, identifiers: DeviceIdentifiers, filePath: string) => (
    buildAction(ActionTypes.BEGIN_FROM_FILE_UPGRADE, { deviceKey, identifiers, filePath })
  ),
  installFirmware: (installParams: InstallParams, withSave: boolean) => (
    buildAction(ActionTypes.INSTALL_FIRMWARE, { installParams, withSave })
  ),
  cancelInstallNewFirmware: () => buildAction(ActionTypes.CANCEL_INSTALL_NEW_FIRMWARE),
};
