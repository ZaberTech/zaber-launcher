import { changeDictionary } from '@zaber/toolbox';
import _ from 'lodash';

import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes, DevicesLoadedPayload } from '../connection_manager';
import { EntityKey, extractConnectionKey, removeWithKey } from '../keys';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { IoState } from './types';

export interface State {
  selectedKey: EntityKey | null;
  devices: DevicesLiveStates;
}

export type DevicesLiveStates = Record<string, DeviceState>;

export interface DeviceState {
  key: EntityKey;
  mounted: boolean;
  readError: string | null;
  ioState: IoState | null;
  setOutputError: string | null;
}

const defaultDeviceState: Omit<DeviceState, 'key'> = {
  mounted: false,
  readError: null,
  ioState: null,
  setOutputError: null,
};

const initialState: State = {
  selectedKey: null,
  devices: {},
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const mount: Reducer<ActionTypes.MOUNT> = (state, { deviceKey }) => ({
  ...state,
  devices: changeDictionary(state.devices, deviceKey, {
    mounted: true,
    readError: null,
  }, key => ({ key, ...defaultDeviceState })),
});

const unmount: Reducer<ActionTypes.UNMOUNT> = (state, { deviceKey }) => ({
  ...state,
  devices: changeDictionary(state.devices, deviceKey, { mounted: false }),
});

function fullIoStateOrNull(stateUpdate: Partial<IoState>): IoState | null {
  if (Object.keys(stateUpdate).length !== 4) {
    return null;
  }
  return stateUpdate as IoState;
}

const stateRead: Reducer<ActionTypes.STATE_READ> = (state, { deviceKey, stateUpdate }) => ({
  ...state,
  devices: changeDictionary(state.devices, deviceKey, deviceState => ({
    ...deviceState,
    readError: null,
    ioState: deviceState.ioState != null ? {
      ...deviceState.ioState,
      ...stateUpdate,
    } : fullIoStateOrNull(stateUpdate), // do not accept partial state update unless have entire state
  })),
});

const stateReadError: Reducer<ActionTypes.STATE_READ_ERROR> = (state, { deviceKey, error }) => ({
  ...state,
  devices: changeDictionary(state.devices, deviceKey, { readError: error }),
});

const setOutputDone: Reducer<ActionTypes.SET_OUTPUT_DONE> = (state, { deviceKey, error }) => ({
  ...state,
  devices: changeDictionary(state.devices, deviceKey, { setOutputError: error ?? null }),
});

const setSelectedKey: Reducer<ActionTypes.SET_SELECTED_KEY> = (state, { key  }) => ({
  ...state,
  selectedKey: key,
});

const devicesLoaded = (state: State, { connectionKey, devices }: DevicesLoadedPayload): State => ({
  ...state,
  devices: {
    ...removeWithKey(state.devices, extractConnectionKey, connectionKey),
    ..._.keyBy(devices
      .filter(device => device.key in state.devices)
      .map((device): DeviceState => ({
        ...state.devices[device.key],
        key: device.key,
        ioState: null,
      })), 'key')
  }
});

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.MOUNT]: mount,
  [ActionTypes.UNMOUNT]: unmount,
  [ActionTypes.STATE_READ]: stateRead,
  [ActionTypes.STATE_READ_ERROR]: stateReadError,
  [ActionTypes.SET_OUTPUT_DONE]: setOutputDone,
  [ActionTypes.SET_SELECTED_KEY]: setSelectedKey,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
}, initialState);
