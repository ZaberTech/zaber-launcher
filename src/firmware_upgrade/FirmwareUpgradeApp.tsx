import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Icons } from '@zaber/react-library';

import type { RootState } from '../store';
import { ConnectionsView } from '../connection_manager';
import type { EntityKey } from '../keys';
import type { AttentionRequests } from '../connection_manager/connection_view/types';
import { NoContentMessage, Title } from '../components';

import { actions as actionsDefinition } from './actions';
import { selectDeviceInfoWithAxes } from './selectors';
import { UpgradeDeviceFirmware } from './UpgradeDeviceFirmware';
import { selectDevicesWithHigherVersionsAvailable } from './selectors_external';

interface DispatchProps {
  actions: typeof actionsDefinition;
}

interface StateProps {
  device: ReturnType<typeof selectDeviceInfoWithAxes>;
  requestAttention: EntityKey[];
}

const NoSelection = () => <NoContentMessage title="Welcome to Firmware Upgrade!" message="Select a device to upgrade firmware."/>;
const AttentionIcon = () => <Icons.UpgradeWithNotification className="attention" title="Newer Firmware Available"/>;

class FirmwareUpgradeAppBase extends Component<DispatchProps & StateProps> {
  render(): React.ReactNode {
    const { device, actions, requestAttention } = this.props;

    const attentionRequests: AttentionRequests = {
      requestAttention,
      showIcon: <AttentionIcon/>,
    };

    return (
      <div className="connection-view-and-app">
        <Title>Firmware Upgrade</Title>
        <ConnectionsView
          selectable={['device']}
          selected={device?.key ?? null}
          onSelect={actions.setSelectedDevice}
          attentionRequests={attentionRequests}
          showAxes={false}
        />

        <div className="app-ui firmware-upgrade">
          {device ? <UpgradeDeviceFirmware/> : <NoSelection/>}
        </div>
      </div>
    );
  }
}

export const FirmwareUpgradeApp = connect<StateProps, DispatchProps, unknown, RootState>(
  (state: RootState): StateProps => ({
    device: selectDeviceInfoWithAxes(state),
    requestAttention: selectDevicesWithHigherVersionsAvailable(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(FirmwareUpgradeAppBase);
