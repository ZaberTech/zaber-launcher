import React from 'react';
import { Icons } from '@zaber/react-library';
import classNames from 'classnames';

export const Indicator: React.FC<{
  value: boolean; disabled: boolean; titles: [string, string];
}> = ({ value, disabled, titles }) => {
  const Icon = value ? Icons.Tick : Icons.Cross;
  return <Icon className={classNames('indicator', { ok: value, nok: !value, disabled })} title={titles[Number(value)]}/>;
};
