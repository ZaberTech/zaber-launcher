import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { NoticeBanner } from '@zaber/react-library';
import { Length } from '@zaber/motion';

import { useActions } from '../utils';
import { EditableInputValueState, EditableInputWithUnits } from '../units';

import { selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { MonitorInfo, Operations, POSITION_DIMENSION } from './types';
import { useOperationValue } from './hooks';
import { getPrecision } from './utils';

export const Limit: React.FC<{
  limit: 'min' | 'max';
  info: NonNullable<MonitorInfo['autofocus']>;
}> = ({ limit, info }) => {
  const actions = useActions(actionsDefinition);

  const [limitValue, setLimitValue, inProgress] = useOperationValue(info.limits[limit], Operations.autofocusSetLimit);

  const [inputState, setInputState] = useState<EditableInputValueState | null>(null);
  const measure = inputState ?? limitValue;

  return (
    <EditableInputWithUnits
      className="position"
      title={`Limit ${limit}`}
      mode={inputState?.mode ?? (inProgress ? 'reading' : 'displaying')}
      displayPrecision={getPrecision(measure)}
      unitFrom={{ dimension: POSITION_DIMENSION }}
      measure={measure}
      onChange={position => {
        switch (position.mode) {
          case 'displaying':
            setInputState(null);
            break;
          case 'writing':
            setInputState(null);
            setLimitValue(position);
            actions.operationStart(Operations.autofocusSetLimit, { limit, position });
            break;
          case 'editing':
            setInputState(position);
            break;
        }

        if (position.units !== info.limits[limit].units) {
          actions.updateUnits('focusAxis', position.units as Length);
        }
      }}/>);
};

export const ExternalSoftwareWarnings: React.FC = () => {
  const { info } = useSelector(selectMonitorState);
  const currentObjective = info?.objectiveChanger?.objective ?? 1;
  const providerObjective = info?.autofocus?.providerObjective;
  if (providerObjective == null || currentObjective === 0 || currentObjective === providerObjective) {
    return null;
  }

  return <div className="row">
    <NoticeBanner type="warning">
      The current objective is set by external software (objective {providerObjective}).
      Some of the parameters controller by this application may not apply.
    </NoticeBanner>
  </div>;
};
