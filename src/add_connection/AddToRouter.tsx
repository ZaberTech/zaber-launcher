import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { NoticeMessage, Loader } from '@zaber/react-library';

import type { RootState } from '../store';

import { actions as actionsDefinition } from './actions';
import { selectAddingConnection, selectAddingConnectionError } from './selectors';

interface Props {
  actions: typeof actionsDefinition;
  adding: boolean;
  error: ReturnType<typeof selectAddingConnectionError>;
}

class AddToRouterBase extends Component<Props> {
  componentDidMount(): void {
    const { actions } = this.props;
    actions.addConnection();
  }

  render(): React.ReactNode {
    const { adding, error } = this.props;
    return (
      <div className="add-to-router">
        {adding && <>
          <Loader/>
          <div>Adding Connection</div>
        </>}
        {!!error && <NoticeMessage>
          <div>Cannot add connection: {error}</div>
          <div>Close the dialog and try again.</div>
        </NoticeMessage>}
      </div>
    );
  }
}

export const AddToRouter = connect(
  (state: RootState): Omit<Props, 'actions'> => ({
    adding: selectAddingConnection(state),
    error: selectAddingConnectionError(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(AddToRouterBase);
