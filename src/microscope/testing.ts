/* eslint-disable camelcase */
import { Length, Time } from '@zaber/motion';

import { deepFreeze } from '../utils';

import type { Preset } from './presets';

export const PRESET1 = {
  name: 'Preset 1',
  data: {
    filter: {
      value: 2,
      apply: true,
    },
    objective: {
      value: 3,
      apply: true,
    },
    illuminator: {
      1: { on: { value: true, apply: true }, intensity: { value: 0.5, apply: true } },
      2: { on: { value: false, apply: true }, intensity: { value: 0.3, apply: true } },
      3: { on: { value: false, apply: false }, intensity: { value: 0, apply: false } },
      4: { on: { value: false, apply: false }, intensity: { value: 0, apply: false } },
    },
    axes: {
      focusAxis: {
        position: { value: { value: 123, units: Length.mm }, apply: true },
        step: { value: { value: 30, units: Length.MICROMETRES }, apply: true },
      },
      xAxis: {
        position: { value: { value: 2500, units: Length.mm }, apply: true },
        step: { value: { value: 60, units: Length.MICROMETRES }, apply: true },
      },
      yAxis: {
        position: { value: { value: 2600, units: Length.mm }, apply: true },
        step: { value: { value: 62, units: Length.MICROMETRES }, apply: true },
      },
    },
    autofocus: {
      limitMin: { value: { value: 10, units: Length.mm }, apply: true },
      limitMax: { value: { value: 20, units: Length.mm }, apply: true },
      settings: {
        control_loop_ki: { value: 0.5, apply: true },
        control_loop_kp: { value: 0.1, apply: true },
        magnification: { value: 20, apply: true },
        max_exposure: { value: 500, apply: true },
        signal_to_noise_ratio: { value: 0.06, apply: true },
      },
    },
    cameraTrigger: {
      exposure: { value: { value: 100, units: Time.ms }, apply: true },
      waitBefore: { value: { value: 200, units: Time.ms }, apply: true },
      waitAfter: { value: { value: 300, units: Time.ms }, apply: true },
    },
  },
} satisfies Preset;

export const NO_PRESET = deepFreeze({
  name: 'No Preset',
  data: {
    filter: {
      value: 1,
      apply: false,
    },
    objective: {
      value: 1,
      apply: false,
    },
    illuminator: {
      1: { on: { value: false, apply: false }, intensity: { value: 0, apply: false } },
      2: { on: { value: false, apply: false }, intensity: { value: 0, apply: false } },
      3: { on: { value: false, apply: false }, intensity: { value: 0, apply: false } },
      4: { on: { value: false, apply: false }, intensity: { value: 0, apply: false } },
    },
    axes: {
      focusAxis: {
        position: { value: { value: 0, units: Length.mm }, apply: false },
        step: { value: { value: 0, units: Length.MICROMETRES }, apply: false },
      },
      xAxis: {
        position: { value: { value: 0, units: Length.mm }, apply: false },
        step: { value: { value: 0, units: Length.MICROMETRES }, apply: false },
      },
      yAxis: {
        position: { value: { value: 0, units: Length.mm }, apply: false },
        step: { value: { value: 0, units: Length.MICROMETRES }, apply: false },
      },
    },
    autofocus: {
      limitMin: { value: { value: 0, units: Length.mm }, apply: false },
      limitMax: { value: { value: 0, units: Length.mm }, apply: false },
      settings: {
        control_loop_ki: { value: 0, apply: false },
        control_loop_kp: { value: 0, apply: false },
        magnification: { value: 0, apply: false },
        max_exposure: { value: 0, apply: false },
        signal_to_noise_ratio: { value: 0, apply: false },
      },
    },
    cameraTrigger: {
      exposure: { value: { value: 0, units: Time.ms }, apply: false },
      waitBefore: { value: { value: 0, units: Time.ms }, apply: false },
      waitAfter: { value: { value: 0, units: Time.ms }, apply: false },
    },
  },
} satisfies Preset);
