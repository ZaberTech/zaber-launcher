
import React from 'react';
import { RenderResult, render } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { wrapWithNewStore } from '../test';

import { IfAuth } from './IfAuth';
import { AuthState } from './types';

const TestIfAuth = wrapWithNewStore(IfAuth);

let wrapper: RenderResult;

beforeEach(() => {
  createContainer();
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
});

test('renders content if authenticated', () => {
  wrapper = render(<TestIfAuth preloadedState={{ cloud: { authState: AuthState.Authenticated } }}>Authenticated</TestIfAuth>);
  wrapper.getByText('Authenticated');
});

test('does not render content if not authenticated', () => {
  wrapper = render(<TestIfAuth>Authenticated</TestIfAuth>);
  expect(wrapper.queryByText('Authenticated')).toBeNull();
});

test('allows to specify on which state to render the content', () => {
  wrapper = render(<TestIfAuth
    desiredState={AuthState.Unauthenticated}
    preloadedState={{ cloud: { authState: AuthState.Unauthenticated } }}
  >Unauthenticated</TestIfAuth>);
  wrapper.getByText('Unauthenticated');
});
