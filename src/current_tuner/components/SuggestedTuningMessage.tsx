import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { Button, ButtonRow, NoticeMessage, Text } from '@zaber/react-library';

import { actions as actionsDefinition } from '../actions';
import type { DeviceOrAxisStateComplete } from '../selectors';

interface Props {
  axisOrDevice: DeviceOrAxisStateComplete;
}

export const SuggestedTuningMessage: React.FC<Props> = ({ axisOrDevice }) => {
  const actions = useActions(actionsDefinition);

  return (
    <NoticeMessage type="info" headline="Incompatible Tuning">
      <div className="message-div">
        <Text>
            Axis tuning is not compatible with the application.
            Press the Accept button to use the suggested tuning.
        </Text>
      </div>
      <ButtonRow>
        <Button onClick={() => {
          actions.updateSliderValues(axisOrDevice.key, axisOrDevice.defaultPositions!);
          actions.applySliderValues(axisOrDevice.key);
        }}>
            Accept
        </Button>
      </ButtonRow>
    </NoticeMessage>
  );
};
