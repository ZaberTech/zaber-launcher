export { reducer as firmwareUpgradeReducer } from './reducer';
export type { State as FirmwareUpgradeState } from './reducer';

export type {
  ActionsToPayloads as FirmwareUpgradeActionPayloads,
} from './actions';
export { firmwareUpdateSaga } from './sagas';
export { selectDevicesWithHigherVersionsAvailable, selectUpgradeInProgress } from './selectors_external';

export { FirmwareUpgradeApp } from './FirmwareUpgradeApp';
export { FirmwareUpgradeContextMenuItem } from './FirmwareUpgradeContextMenuItem';
export { ExternalUpgrade } from './ExternalUpgrade';
