import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, Icons, Slider, Text, Toggle } from '@zaber/react-library';
import { Percent } from '@zaber/motion';

import { useActions, useDebounceValue } from '../utils';
import { SetLabelClick } from '../connection_manager_ui';
import { EditableInputValueState, EditableInputWithUnits, measurementOK } from '../units';

import { selectMonitorState } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Operations, PERCENT_DIMENSION, type MonitorInfo, type MonitorStableInfo } from './types';
import { useOperationValue } from './hooks';
import { OperationError } from './components';

const Channel: React.FC<{
  channel: NonNullable<MonitorStableInfo['illuminator']>['channels'][0];
  info: NonNullable<MonitorInfo['illuminator']>['channels'][0];
}> = ({ channel: { channel, name, axisKey }, info }) => {
  const actions = useActions(actionsDefinition);

  const [intensityDeb, setIntensityDeb] = useDebounceValue<number>(
    intensity => actions.operationStart(Operations.channelSetIntensity, { channel, intensity }),
    200);
  const [intensity, setIntensityOp] = useOperationValue(intensityDeb ?? info.intensity, Operations.channelSetIntensity);

  const [isOn, setOn] = useOperationValue(info.on, Operations.channelOn);

  const [inputEdit, setInputEdit] = useState<EditableInputValueState | null>(null);

  const setIntensity = (value: number) => {
    const intensity = value / 100;
    setIntensityOp(intensity);
    setIntensityDeb(intensity);
  };

  const value = Number.isNaN(intensity) ? 0 : Math.round(intensity * 100);

  return (<HeaderCard
    className="controls illuminator"
    header={<>
      <Text t={Text.Type.H4}>Illuminator Channel {channel} ({name})</Text>
      <SetLabelClick entityKey={axisKey} label={name}>
        {onClick => <Icons.Settings onClick={onClick}/>}
      </SetLabelClick>
    </>}>
    <div className="control-row">
      <div>On/Off</div>
      <Toggle
        value={isOn}
        onValueChange={on => {
          setOn(on);
          actions.operationStart(Operations.channelOn, { channel, on });
        }}/>

      <div className="spacer"/>
      <div className="intensity-label">Intensity</div>
      <EditableInputWithUnits
        className="intensity"
        title="Intensity"
        unitFrom={{ dimension: PERCENT_DIMENSION }}
        displayPrecision={0}
        mode={inputEdit?.mode ?? 'displaying'}
        measure={inputEdit ?? { value, units: Percent.PERCENT }}
        onChange={newValue => {
          switch (newValue.mode) {
            case 'displaying':
              if (measurementOK(newValue)) {
                setIntensity(newValue.value);
              }
              setInputEdit(null);
              break;
            case 'writing':
              setIntensity(newValue.value);
              setInputEdit(null);
              break;
            case 'editing':
              setInputEdit(newValue);
              break;
          }
        }}/>
    </div>
    <div className="control-row">
      <Slider min={0} max={100} step={1} allowMouseWheel
        value={value}
        onValueChange={setIntensity}/>
    </div>
  </HeaderCard>);
};

export const Illuminator: React.FC = () => {
  const monitor = useSelector(selectMonitorState);
  const stableInfo = monitor.stableInfo?.illuminator;
  const info = monitor.info?.illuminator;
  if (stableInfo == null || info == null) {
    return null;
  }

  return <div className="illuminators">
    {Object.values(stableInfo.channels).map(channel =>
      <Channel key={channel.channel} channel={channel} info={info.channels[channel.channel]}/>)}

    {[
      Operations.channelOn,
      Operations.channelSetIntensity,
    ].map(operation =>
      <OperationError key={operation} operation={operation}>Illuminator Error:</OperationError>)}
  </div>;
};
