import { AnimationClasses, NoticeMessage, Icons, Text } from '@zaber/react-library';
import React from 'react';
import classNames from 'classnames';
import _ from 'lodash';

import type { RouterState } from '../reducer';
import { RouterType } from '../types';
import { RouterName } from '../components/RouterName';
import { RouterId } from '../components/RouterId';
import { actions as actionsDefinition } from '../actions';
import { naturalSort, useActions } from '../../utils';

import { Connection } from './Connection';
import type { PassedProps } from './types';

interface Props extends PassedProps {
  router: RouterState;
  connectionNameEditable?: boolean;
}

export const Router: React.FC<Props> = ({
  router, routerContent, expandedConnections, onConnectionExpand, connectionNameEditable, ...rest
}) => {
  const actions = useActions(actionsDefinition);
  return (
    <div className="router">
      <div className="router-header">
        <RouterName router={router}/>
        {router.type !== RouterType.Local && <>
          &ensp;
          <Icons.Refresh title="Refresh Connections"
            className={classNames({ [AnimationClasses.Rotation]: router.loadingConnections })}
            disabled={router.loadingConnections}
            activated={router.loadingConnections}
            onClick={() => actions.loadConnections(router.key, false)}/>
          &ensp;
          <Text t={Text.Type.Helper} e={Text.Emphasis.Light}><RouterId router={router} copyable/></Text>
        </>}
        {routerContent && <>
          <div className="spacer"/>
          {routerContent(router)}
        </>}
      </div>
      <div className="router-connections">
        {router.connectionsErr && <NoticeMessage>{router.connectionsErr}</NoticeMessage>}
        {[...(router.connections ?? [])]
          .sort(naturalSort)
          .map(connectionKey => (
            <Connection
              key={connectionKey} connectionKey={connectionKey}
              editable={connectionNameEditable && router.type === RouterType.Local}
              expanded={expandedConnections?.[connectionKey]}
              onExpand={onConnectionExpand && (expanded => onConnectionExpand(connectionKey, expanded))}
              {..._.pick(rest,
                'axisContent',
                'deviceContent',
                'deviceErrorContent',
                'controllerContent',
                'connectionContent',
                'axisErrors',
                'displayMultiAxis',
              )}/>
          ))}
        {router.connections?.length === 0 && <div className="no-connections">No connections available</div>}
      </div>
    </div>
  );
};
