import React from 'react';
import { ContextMenu, Text } from '@zaber/react-library';

import { SaveLoadContextMenuItem } from '../save_load';
import { FactoryResetAxisMenuItem } from '../factory_reset';
import { SetLabelContextMenuItem } from '../connection_manager_ui';
import type { IdentifiedAxisState } from '../connection_manager';

interface Props {
  axis: IdentifiedAxisState;
}

export const IntegratedAxis: React.FC<Props> = ({ axis }) => <div className="axis-integrated">
  {axis.label && <Text t={Text.Type.H5}>{axis.label}</Text>}
  <div className="spacer"/>
  <ContextMenu>
    <SaveLoadContextMenuItem productKey={axis.key}/>
    <FactoryResetAxisMenuItem axisKey={axis.key}/>
    <SetLabelContextMenuItem entityKey={axis.key} label={axis.label}/>
  </ContextMenu>
</div>;
