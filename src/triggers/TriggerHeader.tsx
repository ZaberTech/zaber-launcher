import { Chevron, Flex, PopUp, Toggle, Text, Input, EditableField } from '@zaber/react-library';
import React, { useState } from 'react';
import { TriggerEnabledState } from '@zaber/motion/ascii';
import _ from 'lodash';
import classNames from 'classnames';

import { useActions } from '../utils';
import { EntityKey } from '../keys';

import { actions as actionsDefinition } from './actions';
import { TriggerState } from './reducer';


interface TriggerHeaderProps {
  deviceKey: EntityKey;
  triggerIndex: number;
  enabledState: TriggerEnabledState | null;
  triggerState: TriggerState;
  expanded: boolean;
}

export const TriggerHeader: React.FC<TriggerHeaderProps> =
  ({ deviceKey, triggerIndex, enabledState, triggerState, expanded }) => {
    const actions = useActions(actionsDefinition);

    const [editingLabel, setEditingLabel] = useState(false);

    const { enabledLoading, inputChanged, labelInput, firesTotal } = triggerState;
    const enabled = enabledState?.enabled ?? false;
    const toggleDisabled = (!enabled && inputChanged);
    const defaultLabel = `Trigger ${triggerIndex + 1}`;

    return <Flex.Row className="trigger-header">
      <Chevron
        title={expanded ? 'Collapse Trigger' : 'Expand Trigger'}
        expanded={expanded}
        onClick={() => {
          if (expanded) {
            actions.resetInput(triggerIndex);
          }
          actions.setDeviceTriggerExpanded(deviceKey, triggerIndex, !expanded);
        }}
      />
      <PopUp triggerAction={toggleDisabled ? 'hover' : 'none'} trigger={() =>
        <Toggle
          title="Enable Trigger Toggle"
          disabled={toggleDisabled}
          value={enabled}
          onValueChange={v => {
            actions.setEnabledLoading(triggerIndex, true);
            if (v) {
              actions.enableTrigger(triggerIndex, _.max([0, firesTotal])!);
            } else {
              actions.disableTrigger(triggerIndex);
            }
          }}
          loading={enabledLoading}/>
      }>
        Save or cancel changes first
      </PopUp>
      <EditableField
        title="Trigger Label"
        textType={Text.Type.H4}
        value={labelInput === '' ? defaultLabel : labelInput}
        placeholder={defaultLabel}
        defaultValue=""
        onValueChange={value => actions.setLabel(triggerIndex, value)}
        onEditStateChanged={editing => setEditingLabel(editing)}
      />
      {!(labelInput === '' || editingLabel) &&
        <Text t={Text.Type.Instruction}>{defaultLabel}</Text>
      }
      <Flex.Spacer/>
      {enabledState && <Count triggerIndex={triggerIndex} enabledState={enabledState} firesTotal={firesTotal}/>}
    </Flex.Row>;
  };

interface CountProps {
  triggerIndex: number;
  enabledState: TriggerEnabledState;
  firesTotal: number;
}

const Count: React.FC<CountProps> = ({ triggerIndex, enabledState, firesTotal }) => {
  const actions = useActions(actionsDefinition);

  const { enabled, firesRemaining } = enabledState;

  if (enabled) {
    const infinite = firesRemaining === -1;
    return <>
      <Text t={Text.Type.Body}>Repeating</Text>
      <PopUp className="trigger-count-enabled" triggerAction="hover" trigger={() =>
        <Text title="Trigger Count" t={infinite ? Text.Type.H3 : Text.Type.Body} e={Text.Emphasis.Bold}>
          {infinite ? '∞' : `${firesRemaining} / ${firesTotal > 0 ? firesTotal : ''}`}
        </Text>
      }>
      Disable trigger to change count
      </PopUp>
      <Text t={Text.Type.Body}>times</Text>
    </>;
  } else {
    return <>
      <Text t={Text.Type.Body}>Repeat</Text>
      <Input
        title="Trigger Count Input"
        className={classNames('trigger-count', { 'trigger-count-infinity': firesTotal <= 0 })}
        placeholder="∞"
        value={firesTotal <= 0 ? '' : firesTotal}
        onValueChange={value => {
          if (value === '') {
            actions.setFiresTotal(triggerIndex, -1);
          } else {
            const num = Number(value);
            if (Number.isInteger(num) && num >= 1) {
              actions.setFiresTotal(triggerIndex, num);
            }
          }
        }}
        clearable
      />
      <Text t={Text.Type.Body}>times</Text>
    </>;
  }
};
