import { Button, Checkbox, Icons, Input, Loader, Modal, Text, TextArea } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import classNames from 'classnames';
import React from 'react';
import { useSelector } from 'react-redux';

import { NoContentMessage } from '../components';
import { environment, Editions } from '../environment';

import { actions as actionDefinitions } from './actions';
import { selectEmail, selectText, selectError, selectModalState, selectAnonymous } from './selectors';
import type { FeedbackModalOpenState } from './types';


function isValid(emailRequired: boolean, text: string, email: string | undefined) {
  const emailRegex = /^\S+@\S+\.\S+$/;
  if (emailRequired) {
    if (!email || !emailRegex.test(email)) {
      return false;
    }
  }
  return !!text;
}

const Offline: React.FC = () => <NoContentMessage
  title="Automatic Feedback Unavailable"
  message={[
    'This feature is not supported in Offline Zaber Launcher.',
    'If you\'d like to send us your comments or suggestions, please email contact@zaber.com.'
  ].join(' ')}
/>;

const Gather: React.FC<{ actions: typeof actionDefinitions }> = ({ actions }) => {
  const isInternal = environment.edition === Editions.Internal;
  const anonymous = useSelector(selectAnonymous);
  const email = useSelector(selectEmail);
  const text = useSelector(selectText);
  const emailRequired = !anonymous || isInternal;
  const valid = isValid(emailRequired, text, email);

  return <>
    <Text>Do you have a suggestion or have you found a bug? Let us know in the field below.</Text>
    <TextArea value={text} onValueChange={actions.setText} title="Input feedback"></TextArea>
    {!isInternal &&
      <Checkbox labelContent={
        <>Send feedback anonymously</>
      } checked={anonymous} onChecked={actions.setAnonymous}/>}
    {emailRequired && <Input labelContent="Email" value={email} onValueChange={actions.setEmail} title="Input email"/>}
    <div className="spacer"/>
    <Button disabled={!valid} onClick={actions.sendFeedback} title="Send feedback">Send</Button>
  </>;
};

interface InfoProps {
  actions: typeof actionDefinitions;
  modalState: Exclude<FeedbackModalOpenState, 'gather'>;
}
const Info: React.FC<InfoProps> = ({ modalState, actions }) => {
  const error = useSelector(selectError);
  switch (modalState) {
    case 'sending': {
      return <>
        <Loader size="large"/>
        <Text t={Text.Type.Body}>Sending your feedback</Text>
      </>;
    }
    case 'sent': {
      return <>
        <Icons.Confirmation className="confirmation"/>
        <Text t={Text.Type.Body}>We appreciate you taking the time to share your thoughts!</Text>
      </>;
    }
    case 'error': {
      return <>
        <Icons.ErrorFault className="send-error"/>
        <Text t={Text.Type.Body}>There was a problem sending your feedback.</Text>
        <Text t={Text.Type.BodySm} e={Text.Emphasis.Light}>{error}</Text>
        <Button onClick={actions.sendFeedback} title="Send feedback">Try Again</Button>
      </>;
    }
  }
};

export const FeedbackModal: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const modalState = useSelector(selectModalState);
  if (modalState === 'closed') { return null }
  return <Modal
    className={classNames('feedback-modal', { center: modalState !== 'gather' })}
    headerIcon={<Icons.Feedback/>}
    headerText="Send Feedback"
    onRequestClose={actions.close}
  >
    {
      environment.offline ? <Offline/> :
      modalState === 'gather' ? <Gather actions={actions}/> :
      <Info modalState={modalState} actions={actions}/>
    }
  </Modal>;
};
