import { throwUnexpectedError } from '@zaber/toolbox';
import { match } from 'ts-pattern';

import type { AsciiComms } from '../connection_manager/types';
import { hasSetting, getStoredBoolOrDefault } from '../devices';

import type { HwModDeviceOrAxisState } from './selectors';
import { allFieldInitializers, allSectionIds, dataDefinitionTemplate, SectionIds } from './data_definitions';
import type {
  SectionValues,
  Values,
  Initializer,
  FieldValue,
  DescribedSettings,
  SectionDataDefinition,
} from './types';
import { SETUP_COMPLETE_KEY } from './storageKeys';
import { is3rdPartyId, isRotary3rdPartyId } from './peripheral_id_picker/peripheralIdMapping';

async function getSkippedSettings(ascii: AsciiComms, info: HwModDeviceOrAxisState): Promise<DescribedSettings[]> {
  const peripheralId = info.axis?.identity?.peripheralId;
  if (is3rdPartyId(peripheralId)) {
    if (await getStoredBoolOrDefault(ascii, SETUP_COMPLETE_KEY, false)) {
      // If setup has already been completed, nothing should be skipped
      return [];
    }
    const skipped: DescribedSettings[] = [
      'motor.inductance', 'motor.resistance', 'motion.index.dist',
      'encoder.1.ratio.mult', 'encoder.1.ratio.div', 'encoder.2.ratio.mult', 'encoder.2.ratio.div',
    ];
    if (isRotary3rdPartyId(peripheralId)) {
      skipped.push('limit.cycle.dist');
    }
    return skipped;
  }
  return [];
}

async function initializeField(
  init: Initializer, ascii: AsciiComms, info: HwModDeviceOrAxisState, skipSettings: string[]
): Promise<FieldValue> {
  const baseFieldValue: Omit<FieldValue, 'setting'> = {
    is: null, was: null, updateApplied: false, error: null,
  };
  if (init.type === 'custom') {
    const result = await init.backing(ascii, info);
    if (result != null && typeof result === 'object') {
      return { ...baseFieldValue, ...result, setting: null };
    }
    return { ...baseFieldValue, is: result, was: result, setting: null };
  }

  const setting = match(init)
    .with({ type: 'setting' }, ({ backing }) => backing)
    .with({ type: 'variant' }, ({ backing }) => backing(info))
    .exhaustive();

  if (skipSettings.includes(setting)) {
    return { ...baseFieldValue, is: null, was: null, setting };
  }

  try {
    const value = await ascii.settings.get(setting);
    return { ...baseFieldValue, is: value, was: value, setting };
  } catch (e) {
    throwUnexpectedError(e);
    throw new HwModInitializationError([setting]);
  }
}

class HwModInitializationError extends Error {
  constructor(public missing: string[]) {
    super(`Could not initialize the following required fields: ${missing.map(f => `"${f}"`).join(', ')}`);
  }
}

const isFieldPresent = (initializer: Initializer, info: HwModDeviceOrAxisState) => match(initializer)
  .with({ type: 'setting' }, ({ backing }) => hasSetting(backing, info.axis ?? info.device))
  .with({ type: 'variant' }, ({ backing }) => hasSetting(backing(info), info.axis ?? info.device))
  .with({ type: 'custom' }, ({ requires }) => {
    for (const setting of requires ?? []) {
      if (!hasSetting(setting, info.axis ?? info.device)) { return false }
    }
    return true;
  })
  .exhaustive();

async function initializeSectionValues<S extends SectionIds>(
  sectionId: S, template: SectionDataDefinition<S>, ascii: AsciiComms, info: HwModDeviceOrAxisState, skipSettings: string[]
): Promise<SectionValues<S> | null> {
  const sectionEnabled = await template.sectionPresent({ ascii, info });
  if (!sectionEnabled) { return null }

  const section: SectionValues<S> = {};
  for (const [fieldId, initializer] of allFieldInitializers(sectionId)) {
    if (isFieldPresent(initializer, info)) {
      section[fieldId] = await initializeField(initializer, ascii, info, skipSettings);
    }
  }

  return section;
}

export async function initializeValues(device: AsciiComms, deviceInfo: HwModDeviceOrAxisState): Promise<Values> {
  const values: Values = {
    motion: null, motorCharacteristics: null,
    encoder1: null, encoder2: null,
    limitHome: null, limitC: null, limitAway: null,
  };
  const skipSettings = await getSkippedSettings(device, deviceInfo);
  for (const sectionId of allSectionIds) {
    const sectionTemplate = dataDefinitionTemplate[sectionId] as SectionDataDefinition<typeof sectionId>;
    values[sectionId] = await initializeSectionValues(sectionId, sectionTemplate, device, deviceInfo, skipSettings);
  }
  return values;
}
