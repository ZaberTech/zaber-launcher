import React from 'react';
import { render } from '@testing-library/react';

import { wrapWithNewStore, wrapWithRouter } from '../test';
import { createContainer, destroyContainer } from '../container';

import { UnauthenticatedRoutes } from './UnauthenticatedRoutes';

const TestUnauthenticated = wrapWithNewStore(wrapWithRouter(UnauthenticatedRoutes));

beforeEach(() => {
  createContainer();
});

afterEach(() => {
  destroyContainer();
});

test('renders intro as default', () => {
  const wrapper = render(<TestUnauthenticated/>);
  wrapper.getByText(/Sign In/);
  wrapper.unmount();
});
