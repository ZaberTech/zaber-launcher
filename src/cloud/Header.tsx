import React from 'react';
import { Text } from '@zaber/react-library';

import { environment } from '../environment';

export const Header: React.FC = () => (<div className="service-header">
  <Text t={Text.Type.H3}>Cloud Service{environment.cloudPortalUrl.includes('staging') ? ' (staging environment)' : ''}</Text>
</div>);
