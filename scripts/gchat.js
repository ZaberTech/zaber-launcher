const _  = require('lodash');

const WINDOWS_ICON = 'https://drive.google.com/uc?export=view&id=1d6v5Li2WHLD2YiUhT6BZ49ZuwJx86akL';
const MAC_ICON = 'https://drive.google.com/uc?export=view&id=1miJHxtb4SsegFz9CLaFSzTxM96gLlKSb';
const LINUX_ICON = 'https://drive.google.com/uc?export=view&id=1Ev8EF4j2ZPdqknTgwDUIhuJ6rzSgsVqM';

const zlIcons = {
  internal: 'https://i.imgur.com/mEoiN3w.png',
  public: 'https://i.imgur.com/AOjEUYW.png',
  offline: 'https://i.imgur.com/6kZVGmR.png',
};

const windowsDownloadLinks = {
  internal: 'https://zaber-launcher.izaber.com/internal/internal/ZaberLauncherInternalSetup.exe',
  public: 'https://zaber-launcher-release.s3-us-west-2.amazonaws.com/public/ZaberLauncherSetup.exe',
  offline: 'https://zaber-launcher-release.s3-us-west-2.amazonaws.com/public-offline/ZaberLauncherOfflineSetup.exe'
};

const macDownloadLinks = {
  internal: 'https://zaber-launcher.izaber.com/internal/internal/ZaberLauncherInternal.dmg',
  public: 'https://zaber-launcher-release.s3-us-west-2.amazonaws.com/public/ZaberLauncher.dmg',
  offline: 'https://zaber-launcher-release.s3-us-west-2.amazonaws.com/public-offline/ZaberLauncherOffline.dmg'
};

const linuxDownloadLinks = {
  internal: 'https://zaber-launcher.izaber.com/internal/internal/ZaberLauncherInternal.AppImage',
  public: 'https://zaber-launcher-release.s3-us-west-2.amazonaws.com/public/ZaberLauncher.AppImage',
  offline: 'https://zaber-launcher-release.s3-us-west-2.amazonaws.com/public-offline/ZaberLauncherOffline.AppImage'
};

const generateCard = (edition, version, changeLog) => ({
  cardsV2: [
    {
      cardId: `zaber-launcher-release-${edition}-${version}`,
      card: {
        header: {
          title: `Zaber Launcher ${_.capitalize(edition)} Release`,
          subtitle: `${version}`,
          imageUrl: zlIcons[edition],
        },
        sections: [
          ...changeLog ? [{
            header: 'Changelog',
            collapsible: true,
            widgets: [
              {
                textParagraph: {
                  text: changeLog
                }
              }
            ]
          }] : [],
          {
            header: 'Download',
            widgets: [
              {
                buttonList: {
                  buttons: [
                    {
                      icon: {
                        iconUrl: WINDOWS_ICON
                      },
                      text: 'Windows',
                      onClick: {
                        openLink: {
                          url: windowsDownloadLinks[edition]
                        }
                      },
                    },
                    {
                      icon: {
                        iconUrl: MAC_ICON
                      },
                      text: 'Mac',
                      onClick: {
                        openLink: {
                          url: macDownloadLinks[edition]
                        }
                      },
                    },
                    {
                      icon: {
                        iconUrl: LINUX_ICON
                      },
                      text: 'Linux',
                      onClick: {
                        openLink: {
                          url: linuxDownloadLinks[edition]
                        }
                      },
                    },
                  ]
                }
              }
            ]
          }
        ]
      }
    }
  ]
});

module.exports = {
  generateCard
};
