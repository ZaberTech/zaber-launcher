import { createSelector } from 'reselect';

import { selectMenu, selectRouter } from '../store';

import type { MenuState } from './types';

const FIXED_MENU_PATHS = [
  '/connection-manager',
  '/preferences',
  '/contact',
  '/about',
  '/apps',
  '/local-share',
  '/cloud',
];

export const selectPinned = createSelector(selectMenu, state => state.userPinned);
export const selectFixed = createSelector(selectMenu, selectRouter, (state, router) =>
  state.userPinned || FIXED_MENU_PATHS.some(path => router.location.pathname.startsWith(path)));
export const selectMenuState = createSelector(selectMenu, selectFixed, (state, fixed): MenuState => {
  if (fixed) {
    return 'fixed';
  } else if (state.mouseOver) {
    return 'extended';
  } else {
    return 'collapsed';
  }
});
