import type { EntityKey } from '../keys';

import { parseMessage } from './MessageParser';

const entityKey: EntityKey = '@connection|local|COM0';

test('parse command empty', () => {
  const message = '/';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'command');
});

test('parse command simple', () => {
  const message = '/home';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'command');
  expect(item).toHaveProperty('parsed.data', 'home');
});

test('parse command', () => {
  const message = '/1 1 move abs 400000';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'command');
  expect(item).toHaveProperty('parsed.device', '1');
  expect(item).toHaveProperty('parsed.axis', '1');
  expect(item).toHaveProperty('parsed.data', 'move abs 400000');
});

test('parse command with id and checksum', () => {
  const message = '/0 01 44 set maxspeed 100000:A3';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'command');
  expect(item).toHaveProperty('parsed.device', '0');
  expect(item).toHaveProperty('parsed.axis', '01');
  expect(item).toHaveProperty('parsed.id', '44');
  expect(item).toHaveProperty('parsed.data', 'set maxspeed 100000');
  expect(item).toHaveProperty('parsed.checksum.text', 'A3');
  expect(item).toHaveProperty('parsed.checksum.error', false);
});

test('parse command garbage', () => {
  const message = '/12345:67890:44';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'command');
});

test('parse reply OK', () => {
  const message = '@3 1 OK BUSY -- 00';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'reply');
  expect(item).toHaveProperty('parsed.device', '3');
  expect(item).toHaveProperty('parsed.axis', '1');
  expect(item).toHaveProperty('parsed.replyFlag', 'OK');
  expect(item).toHaveProperty('parsed.status', 'BUSY');
  expect(item).toHaveProperty('parsed.errorFlag.text', '--');
  expect(item).toHaveProperty('parsed.errorFlag.type', null);
  expect(item).toHaveProperty('parsed.data', '00');
});

test('parse reply RJ', () => {
  const message = '@3 1 RJ BUSY -- TWISTED';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'reply');
  expect(item).toHaveProperty('parsed.device', '3');
  expect(item).toHaveProperty('parsed.axis', '1');
  expect(item).toHaveProperty('parsed.replyFlag', 'RJ');
  expect(item).toHaveProperty('parsed.status', 'BUSY');
  expect(item).toHaveProperty('parsed.errorFlag.text', '--');
  expect(item).toHaveProperty('parsed.errorFlag.type', null);
  expect(item).toHaveProperty('parsed.data', 'TWISTED');
});

test('parse reply RJ', () => {
  const message = '@3 1 RJ BUSY -- TWISTED';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'reply');
  expect(item).toHaveProperty('parsed.device', '3');
  expect(item).toHaveProperty('parsed.axis', '1');
  expect(item).toHaveProperty('parsed.replyFlag', 'RJ');
  expect(item).toHaveProperty('parsed.status', 'BUSY');
  expect(item).toHaveProperty('parsed.errorFlag.text', '--');
  expect(item).toHaveProperty('parsed.errorFlag.type', null);
  expect(item).toHaveProperty('parsed.data', 'TWISTED');
});

test('parse reply RJ and errorFlag', () => {
  const message = '@3 1 RJ BUSY FF TWISTED';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'reply');
  expect(item).toHaveProperty('parsed.device', '3');
  expect(item).toHaveProperty('parsed.axis', '1');
  expect(item).toHaveProperty('parsed.replyFlag', 'RJ');
  expect(item).toHaveProperty('parsed.status', 'BUSY');
  expect(item).toHaveProperty('parsed.errorFlag.text', 'FF');
  expect(item).toHaveProperty('parsed.errorFlag.type', 'fault');
  expect(item).toHaveProperty('parsed.data', 'TWISTED');
});

test('parse reply RJ errorFlag messageID and checksum', () => {
  const message = '@3 1 23 RJ IDLE WL BADDATA:10';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'reply');
  expect(item).toHaveProperty('parsed.device', '3');
  expect(item).toHaveProperty('parsed.axis', '1');
  expect(item).toHaveProperty('parsed.id', '23');
  expect(item).toHaveProperty('parsed.replyFlag', 'RJ');
  expect(item).toHaveProperty('parsed.status', 'IDLE');
  expect(item).toHaveProperty('parsed.errorFlag.text', 'WL');
  expect(item).toHaveProperty('parsed.errorFlag.type', 'warn');
  expect(item).toHaveProperty('parsed.data', 'BADDATA');
  expect(item).toHaveProperty('parsed.checksum.text', '10');
  expect(item).toHaveProperty('parsed.checksum.error', true);
});

test('parse reply garbage', () => {
  const message = '@x24nasj';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'unknown');
});

test('parse info', () => {
  const message = '#0 0 Hello World!';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'info');
  expect(item).toHaveProperty('parsed.device', '0');
  expect(item).toHaveProperty('parsed.axis', '0');
  expect(item).toHaveProperty('parsed.data', 'Hello World!');
});

test('parse info with messageID and checksum', () => {
  const message = '#02 0 22 Hello World!:99';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'info');
  expect(item).toHaveProperty('parsed.device', '02');
  expect(item).toHaveProperty('parsed.axis', '0');
  expect(item).toHaveProperty('parsed.id', '22');
  expect(item).toHaveProperty('parsed.data', 'Hello World!');
  expect(item).toHaveProperty('parsed.checksum.text', '99');
  expect(item).toHaveProperty('parsed.checksum.error', true);
});

test('parse alert', () => {
  const message = '!01 1 IDLE --';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'alert');
  expect(item).toHaveProperty('parsed.device', '01');
  expect(item).toHaveProperty('parsed.axis', '1');
  expect(item).toHaveProperty('parsed.status', 'IDLE');
  expect(item).toHaveProperty('parsed.errorFlag.text', '--');
  expect(item).toHaveProperty('parsed.errorFlag.type', null);
});

test('parse alert', () => {
  const message = '!01 1 IDLE --';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'alert');
  expect(item).toHaveProperty('parsed.device', '01');
  expect(item).toHaveProperty('parsed.axis', '1');
  expect(item).toHaveProperty('parsed.status', 'IDLE');
  expect(item).toHaveProperty('parsed.errorFlag.text', '--');
  expect(item).toHaveProperty('parsed.errorFlag.type', null);
});

test('parse alert with errorFlag', () => {
  const message = '!01 1 IDLE NC Hello World!';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'alert');
  expect(item).toHaveProperty('parsed.device', '01');
  expect(item).toHaveProperty('parsed.axis', '1');
  expect(item).toHaveProperty('parsed.status', 'IDLE');
  expect(item).toHaveProperty('parsed.errorFlag.text', 'NC');
  expect(item).toHaveProperty('parsed.errorFlag.type', 'note');
  expect(item).toHaveProperty('parsed.data', 'Hello World!');
});

test('parse garbage message', () => {
  const message = 'aslfdbsgds,bgbldsjbi';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'unknown');
});

test('parse garbage message with checksum', () => {
  const message = 'aslfdbsgds,bgbldsjbi:75';
  const item = parseMessage(message, true, entityKey);
  expect(item).toHaveProperty('text', message);
  expect(item).toHaveProperty('parsed.type', 'unknown');
});

