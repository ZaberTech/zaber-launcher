import path from 'path';

import * as remote from '@electron/remote';

import { environment } from './environment';

const xdgCacheHome = process.env.XDG_CACHE_HOME ?? `${process.env.HOME}/.cache`;
const xdgCachePath = path.join(xdgCacheHome, remote.app.getName());
const useXDGPaths = !environment.isTest && environment.platform === 'linux';

// do not read these fields from node process (only from browser)
// if this file is loaded anywhere in src_node, then app.getPath would be evaluated
// before setPaths() is called in src_node/main.ts, which means we get the wrong path!
export class Paths {
  public static readonly CACHE = useXDGPaths ? xdgCachePath : remote.app.getPath('userData');
  public static readonly LOG = useXDGPaths ? xdgCachePath : remote.app.getPath('logs');
}
