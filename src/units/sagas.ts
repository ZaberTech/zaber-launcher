
import type { SagaIterator } from 'redux-saga';
import { all, select, call, put } from 'redux-saga/effects';

import { Action, RT, takeLeadingUniq } from '../utils';
import { selectUnits } from '../store';
import { getContainer } from '../container';

import { actionDefinitions as actions, ActionsToPayloads, ActionTypes } from './actions';
import { UnitInfoService } from './service';

export function* unitSaga(): SagaIterator {
  yield all([
    takeLeadingUniq(ActionTypes.FETCH_SETTING_UNIT_INFO,
      ({ payload: { entityKey, setting } }) => `${entityKey}:${setting}`,
      fetchSettingUnitInfo),
    takeLeadingUniq(ActionTypes.FETCH_COMMAND_UNIT_INFO,
      ({ payload: { entityKey, command } }) => `${entityKey}:${command}`,
      fetchCommandUnitInfo),
  ]);
}

function* fetchSettingUnitInfo(params: Action<ActionsToPayloads[ActionTypes.FETCH_SETTING_UNIT_INFO]>) {
  const { entityKey, setting } = params.payload;

  const units: RT<typeof selectUnits> = yield select(selectUnits);
  if (units.info[entityKey]?.settings[setting] != null) {
    return;
  }
  const service = getContainer().get(UnitInfoService);
  const unitInfo: RT<typeof service.getSettingUnitInfo> = yield call([service, service.getSettingUnitInfo], entityKey, setting);
  if (unitInfo != null) {
    yield put(actions.fetchSettingUnitInfoDone(entityKey, setting, unitInfo));
  }
}

function* fetchCommandUnitInfo(params: Action<ActionsToPayloads[ActionTypes.FETCH_COMMAND_UNIT_INFO]>) {
  const { entityKey, command } = params.payload;

  const units: RT<typeof selectUnits> = yield select(selectUnits);
  if (units.info[entityKey]?.commands[command] != null) {
    return;
  }
  const service = getContainer().get(UnitInfoService);
  const unitInfo: RT<typeof service.getCommandUnitInfo> = yield call([service, service.getCommandUnitInfo], entityKey, command);
  if (unitInfo != null) {
    yield put(actions.fetchCommandUnitInfoDone(entityKey, command, unitInfo));
  }
}
