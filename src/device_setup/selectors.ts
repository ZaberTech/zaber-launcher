import { createSelector } from 'reselect';

import { selectDeviceSetup } from '../store';

export const selectPendingSetups = createSelector(selectDeviceSetup, state => state.pendingSetups);
