/* eslint-disable @typescript-eslint/no-this-alias */
import React from 'react';
import _ from 'lodash';
import { RenderResult, render, fireEvent, getByRole, getByText, getByTitle, queryByText } from '@testing-library/react';
import { injectable } from 'inversify';

let resizeObserver: ResizeObserverMock;
class ResizeObserverMock {
  constructor(readonly callback: ResizeObserverCallback) {
    resizeObserver = this;
  }
  observe = jest.fn();
  disconnect = jest.fn();
}
(global.ResizeObserver as unknown) = ResizeObserverMock;

let model: MonacoModel;
class MonacoModel {
  value: string = '';

  constructor() {
    model = this;
  }

  setValue = jest.fn(value => this.value = value);
  getValue = jest.fn(() => this.value);
  deltaDecorations = jest.fn<string[], [string[], unknown[]]>(() => []);
  dispose = jest.fn();
  getLinesContent = jest.fn(() => this.value.split('\n').map(l => l.trimEnd()));
}

let editor: MonacoEditor;
class MonacoEditor {
  constructor() {
    editor = this;
  }
  dispose = jest.fn();
  position = ({ lineNumber: 1, column: 1 });
  setPosition = jest.fn(position => { this.position = position });
  getPosition = jest.fn(() => this.position);
  revealLinesInCenterIfOutsideViewport = jest.fn();
  focus = jest.fn();
  getTopForLineNumber = jest.fn(() => 0);
  getLayoutInfo = jest.fn(() => ({ width: 0, height: 0 }));
  setScrollPosition = jest.fn();
  layout = jest.fn();
}

const enumMock = new Proxy({}, {
  get(target, prop) {
    return prop;
  }
});

jest.mock('monaco-editor/esm/vs/editor/editor.api', () => ({
  languages: {
    register: jest.fn(),
    setLanguageConfiguration: jest.fn(),
    setMonarchTokensProvider: jest.fn(),
  },
  editor: {
    defineTheme: jest.fn(),
    createModel: jest.fn(() => new MonacoModel()),
    create: jest.fn(() => new MonacoEditor()),
    TrackedRangeStickiness: enumMock,
    OverviewRulerLane: enumMock,
    ScrollType: enumMock,
  },
  Range: jest.fn((...data) => data),
}));

let translator: TranslatorMock;
let translatorCallback: (t: TranslatorMock) => void;
class TranslatorMock {
  constructor() {
    translator = this;
    if (translatorCallback) { translatorCallback(this) }
  }

  static setupFromDevice = jest.fn(async () => new TranslatorMock());

  coordinateSystem = 'G54';
  setFeedRateOverride = jest.fn();
  setTraverseRate = jest.fn();
  getAxisCoordinateSystemOffset = jest.fn<number, [string, string]>(() => 0);
  setAxisPosition = jest.fn();
  getAxisPosition = jest.fn((_axis: string) => 0);
  translate = jest.fn((_block: string): TranslateResult => ({ warnings: [], commands: [] }));
  flush = jest.fn((): string[] => ([]));
  resetAfterStreamError = jest.fn();
}

jest.mock('@zaber/motion/gcode', () => ({
  OfflineTranslator: TranslatorMock,
}));

import {
  AngularVelocity, ascii, CommandFailedException, CommandFailedExceptionData, GCodeSyntaxException, InvalidOperationException,
  Length, StreamModeException, StreamMovementFailedException, Units, Velocity,
} from '@zaber/motion';
import type { TranslateResult } from '@zaber/motion/gcode';

import { createContainer, destroyContainer } from '../container';
import {
  waitUntil, waitUntilPass, wrapWithNewStore, wrapWithRouter, StorageMock, mockStorage, defer, getParentWithClass, waitTick,
} from '../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { MessageRoutersService } from '../message_router';
import { mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import { TimeMeasuringMock } from '../test/mocks/times';
import { TimeMeasuring } from '../time';

import { CONTENT_STORAGE_KEY } from './sagas';
import { GCode } from './GCode';
import { actions } from './actions';

async function asyncNoop() { }

function badData() {
  return new CommandFailedException('Rejected', {
    replyFlag: 'RJ',
    responseData: 'BADDATA',
  } as CommandFailedExceptionData);
}
function badCmd() {
  return new CommandFailedException('Rejected', {
    replyFlag: 'RJ',
    responseData: 'BADCOMMAND',
  } as CommandFailedExceptionData);
}

let axes: AxisMock[];
let axisCallback: (axis: AxisMock) => void;

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);

    axes.push(this);
    if (axisCallback) { axisCallback(this) }
  }
  identity = {
    peripheralId: 43211 + this.axisNumber,
    axisType: ascii.AxisType.LINEAR,
  };
  genericCommand = jest.fn(() => Promise.resolve());
  _settings: Record<string, number> = {
    pos: 0,
    resolution: 64,
    maxspeed: 110 * this.axisNumber,
  };
  settings = {
    get: jest.fn(async setting => this._settings[setting]),
    set: jest.fn(async (setting, value) => {
      this._settings[setting] = value;
    }),
    convertFromNativeUnits: jest.fn((setting: string, value: number) => value / 10),
  };
  _warnings: string[] = [];
  warnings = {
    getFlags: jest.fn(async () => new Set(this._warnings)),
  };
  home = jest.fn(asyncNoop);
  moveVelocity = jest.fn(asyncNoop);
  moveRelative = jest.fn(asyncNoop);
  waitUntilIdle = jest.fn(asyncNoop);
  stop = jest.fn(asyncNoop);
}

class StreamMock {
  disabled = true;
  disable = jest.fn(async () => {
    this.disabled = true;
  });
  checkDisabled = jest.fn(async () => this.disabled);
  waitUntilIdle = jest.fn(asyncNoop);
  genericCommandBatch = jest.fn<Promise<void>, [string[]]>(asyncNoop);
  setupLiveComposite = jest.fn(async () => {
    this.disabled = false;
  });
  setHold = jest.fn(asyncNoop);
}

let device: DeviceMock;
class DeviceMock extends DeviceMockBase<AxisMock> {
  stream = new StreamMock();
  lockstep = {
    home: jest.fn(asyncNoop),
  };
  identity = { };
  warnings = {
    clearFlags: jest.fn(),
  };
  allAxes = {
    stop: jest.fn(),
  };

  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    device = this;
  }

  genericCommand = jest.fn(async (cmd: string) => {
    if (cmd === 'get pos') {
      return { data: _.range(4).map(i => this.getAxis(i + 1)._settings.pos).join(' ') };
    }
    return {};
  });

  streams = {
    getStream: jest.fn(() => this.stream),
  };

  getLockstep = jest.fn(() => this.lockstep);
}

let connections: ConnectionMock[];

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  constructor(id: string, router: RouterConnectionMock) {
    super(id, router);
    connections.push(this);
  }
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

let storageMock: StorageMock;
let timeMeasuringMock: TimeMeasuringMock;

const TestGCode = wrapWithNewStore(wrapWithRouter(GCode));

let wrapper: RenderResult;

beforeEach(() => {
  device = null!;
  axes = [];
  connections = [];
  axisCallback = null!;

  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  storageMock = mockStorage(container);
  container.bind<unknown>(TimeMeasuring).to(TimeMeasuringMock);
  timeMeasuringMock = container.get<unknown>(TimeMeasuring) as TimeMeasuringMock;

  wrapper = render(<TestGCode/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();

  storageMock = null!;
  timeMeasuringMock = null!;

  resizeObserver = null!;
  model = null!;
  editor = null!;
  translator = null!;
  translatorCallback = null!;

  TranslatorMock.setupFromDevice.mockClear();
});

function getAxisFromPanel(axis: string) {
  return wrapper.getByText(new RegExp(`Axis ${axis}`)).parentElement!.parentElement!;
}

function warningsRegex(n: number, total: number) {
  return new RegExp(`Warning \\(${n}\\/${total}\\):`);
}

async function selectController() {
  fireEvent.click(wrapper.getByText('Select Controller'));
  fireEvent.click(wrapper.getByTitle('Expand Connection'));
  wrapper.getByText('X-MCC4');
  fireEvent.click(wrapper.getByText('Select'));
  await waitUntil(() => wrapper.queryByText(/Loading/) == null);
}

async function configureTranslator() {
  fireEvent.click(wrapper.getByText('Configure'));
  await waitUntilPass(() => wrapper.getByText('Ready'));
}

test('renders', () => {
  wrapper.getByText(/no controller/);
});

test('resizes editor when app resizes', () => {
  editor.layout.mockClear();
  resizeObserver.callback([], null!);
  expect(editor.layout).toHaveBeenCalled();
});

describe('storing of the code', () => {
  test('displays default code if nothing is stored', () => {
    expect(model.value).toMatch(/Paste your G-Code here/);
  });

  test('stores the old code and displays it next time', () => {
    const OLD_CODE = 'My old code';
    model.value = OLD_CODE;

    wrapper.unmount();
    model = null!;

    expect(storageMock.stored[CONTENT_STORAGE_KEY]).toBe(OLD_CODE);

    wrapper = render(<TestGCode/>);
    expect(model.value).toBe(OLD_CODE);
  });

  test('stores the code also when ZL is about to quit', () => {
    const OLD_CODE = 'My old code';
    model.value = OLD_CODE;

    window.dispatchEvent(new Event('beforeunload'));

    expect(storageMock.stored[CONTENT_STORAGE_KEY]).toBe(OLD_CODE);
  });
});

describe('basic XYZ', () => {
  beforeEach(() => {
    mockSingleDeviceWithPeripherals(TestGCode.testStore, {
      peripheralCount: 4,
      type: ['smart', 'smart', 'smart', 'unused'],
    });
  });

  describe('setup', () => {
    afterEach(() => {
      const close = wrapper.queryByTitle('Close Dialog');
      if (close) {
        fireEvent.click(close);
      }
    });

    test('provides working default configuration', async () => {
      await selectController();
      await configureTranslator();

      expect(TranslatorMock.setupFromDevice).toHaveBeenLastCalledWith(
        expect.any(DeviceMock),
        [1, 2, 3],
        expect.objectContaining({
          axisMappings: [
            { axisIndex: 0, axisLetter: 'X' },
            { axisIndex: 1, axisLetter: 'Y' },
            { axisIndex: 2, axisLetter: 'Z' },
          ],
          axisTransformations: [],
        }));
    });

    test('allows to disable, remap and invert axes', async () => {
      await selectController();

      const axis1 = wrapper.getByText(/Axis 1/).parentElement!;
      fireEvent.change(getByRole(axis1, 'combobox'), { target: { value: '' } });

      const axis2 = wrapper.getByText(/Axis 2/).parentElement!;
      fireEvent.change(getByRole(axis2, 'combobox'), { target: { value: 'X' } });

      const axis3 = wrapper.getByText(/Axis 3/).parentElement!;
      fireEvent.click(getByRole(axis3, 'checkbox'));

      await configureTranslator();

      expect(TranslatorMock.setupFromDevice).toHaveBeenLastCalledWith(
        expect.any(DeviceMock),
        [2, 3],
        expect.objectContaining({
          axisMappings: [
            { axisIndex: 0, axisLetter: 'X' },
            { axisIndex: 1, axisLetter: 'Z' },
          ],
          axisTransformations: [
            { axisLetter: 'Z', scaling: -1 },
          ],
        }));
    });

    test('provides default maxspeed as the smallest of all axes and allows to change it', async () => {
      const maxspeed = [12, 7, 14];
      axisCallback = axis => { axis._settings.maxspeed = maxspeed[axis.axisNumber - 1] };

      await selectController();

      const traverseRate = getByRole(wrapper.getByText('Traverse rate:').parentElement!, 'spinbutton');
      expect(traverseRate).toHaveValue(7);

      fireEvent.change(traverseRate, { target: { value: '13' } });

      await configureTranslator();

      expect(translator.setTraverseRate).toHaveBeenLastCalledWith(13, Velocity.MILLIMETRES_PER_SECOND);
    });

    test('shows error if the device does not work and allows to select different controller', async () => {
      axisCallback = axis => {
        if (axis.axisNumber === 2) {
          axis.settings.get.mockRejectedValueOnce(new Error('Something went wrong'));
        }
      };

      await selectController();

      wrapper.getByText(/Something went wrong/);
      fireEvent.click(wrapper.getByText('Select Different Controller'));

      fireEvent.click(wrapper.getByTitle('Expand Connection'));
      wrapper.getByText('X-MCC4');
      fireEvent.click(wrapper.getByText('Select'));
      await waitUntil(() => wrapper.queryByText(/Loading/) == null);

      await configureTranslator();
    });

    test('it validates dialog for duplicate axes', async () => {
      await selectController();
      const axis2 = wrapper.getByText(/Axis 2/).parentElement!;

      fireEvent.change(getByRole(axis2, 'combobox'), { target: { value: 'X' } });
      expect(wrapper.getByText('Configure')).toBeDisabled();

      fireEvent.change(getByRole(axis2, 'combobox'), { target: { value: 'Y' } });
      expect(wrapper.getByText('Configure')).not.toBeDisabled();
    });
  });

  describe('after setup', () => {
    beforeEach(async () => {
      await selectController();

      const axis3 = wrapper.getByText(/Axis 3/).parentElement!;
      fireEvent.click(getByRole(axis3, 'checkbox'));

      await configureTranslator();
    });

    describe('settings', () => {
      test('changing configuration sets up a new translator', async () => {
        fireEvent.click(wrapper.getByTitle('Settings'));

        const axis2 = wrapper.queryAllByText(/Axis 2/)[1].parentElement!;
        fireEvent.change(getByRole(axis2, 'combobox'), { target: { value: '' } });

        await configureTranslator();
        expect(TranslatorMock.setupFromDevice).toHaveBeenLastCalledWith(expect.any(DeviceMock), [1, 3], expect.anything());
      });

      test('opening and closing settings does not change anything', async () => {
        fireEvent.click(wrapper.getByTitle('Settings'));
        fireEvent.click(wrapper.getByTitle('Close Dialog'));
        await waitUntilPass(() => wrapper.getByText('Ready'));
        expect(TranslatorMock.setupFromDevice).toHaveBeenCalledTimes(1);
      });
    });

    describe('validation', () => {
      async function validate() {
        const button = wrapper.getByTitle('Validate');
        fireEvent.click(button);
        expect(button).toHaveClass('disabled');
        await waitUntilPass(() => expect(button).not.toHaveClass('disabled'));
        await waitTick(); // wait for all the effects
      }

      beforeEach(() => {
        model.value = _.range(6).map(i => `M${i}`).join('\n');
      });

      afterEach(() => {
        jest.useRealTimers();
      });

      test('displays report progress when validation takes too long', async () => {
        jest.useFakeTimers();

        timeMeasuringMock.now = jest.fn(() => {
          timeMeasuringMock.mockTime = (timeMeasuringMock.mockTime ?? 0) + 500;
          return timeMeasuringMock.mockTime;
        });

        const button = wrapper.getByTitle('Validate');
        fireEvent.click(button);

        await waitUntilPass(() => {
          jest.advanceTimersToNextTimer();
          wrapper.getByText(/Validating/);
          wrapper.getByText(/\d%/);
        });

        await waitUntilPass(() => {
          jest.advanceTimersToNextTimer();
          expect(button).not.toHaveClass('disabled');
        });
      });

      test('reports warnings and errors', async () => {
        translatorCallback = translator => {
          translator.translate.mockImplementation((block: string) => {
            const result: TranslateResult = {
              commands: [],
              warnings: [],
            };
            if (block === 'M2') {
              result.warnings.push({
                fromBlock: 1,
                toBlock: 2,
                message: 'Two is not a nice number.',
              });
            } else if (block === 'M3') {
              result.warnings.push({
                fromBlock: 0,
                toBlock: 2,
                message: 'M3 is not a nice code.',
              });
            } else if (block === 'M4') {
              throw new GCodeSyntaxException('M4 is not valid.', {
                fromBlock: 0,
                toBlock: 2,
              });
            }
            return result;
          });
        };

        await validate();

        expect(
          _.flatMapDeep(model.deltaDecorations.mock.calls, call => call[1])
        ).toEqual([{
          options: {
            hoverMessage: {
              value: 'Warning: Two is not a nice number.',
            },
            inlineClassName: 'block-warning',
            stickiness: 'NeverGrowsWhenTypingAtEdges',
            overviewRuler: {
              color: expect.anything(),
              position: 'Right',
            },
          },
          range: [3, 2, 3, 3],
        }, {
          options: {
            hoverMessage: {
              value: 'Warning: M3 is not a nice code.',
            },
            inlineClassName: 'block-warning',
            stickiness: 'NeverGrowsWhenTypingAtEdges',
            overviewRuler: {
              color: expect.anything(),
              position: 'Right',
            },
          },
          range: [4, 1, 4, 3],
        }, {
          options: {
            glyphMarginHoverMessage: {
              value: 'Error: M4 is not valid.',
            },
            hoverMessage: {
              value: 'Error: M4 is not valid.',
            },
            inlineClassName: 'block-error',
            isWholeLine: false,
            marginClassName: 'block-error-margin',
            overviewRuler: {
              color: expect.anything(),
              position: 'Right',
            },
            stickiness: 'NeverGrowsWhenTypingAtEdges',
          },
          range: [5, 1, 5, 3]
        }]);
        wrapper.getByText('Warning (1/2): Two is not a nice number.');
        wrapper.getByText('Error (1/1): M4 is not valid.');
      });

      test('allows search through errors and warnings', async () => {
        translatorCallback = translator => {
          translator.translate.mockImplementation((block: string) => {
            const result: TranslateResult = {
              commands: [],
              warnings: [{
                fromBlock: 0,
                toBlock: 2,
                message: `First warning ${block}`,
              }, {
                fromBlock: 3,
                toBlock: 5,
                message: `Second warning ${block}`,
              }],
            };
            if (block === 'M5') {
              throw new GCodeSyntaxException('M5 is not valid', {
                fromBlock: 2,
                toBlock: 4,
              });
            }
            return result;
          });
        };

        await validate();

        wrapper.getByText(warningsRegex(1, 10));
        wrapper.getByText('Error (1/1): M5 is not valid');
        editor.setPosition({ lineNumber: 1, column: 1 });

        fireEvent.click(wrapper.getByTitle('Next Warning'));
        wrapper.getByText(warningsRegex(2, 10));
        expect(editor.setPosition).toHaveBeenLastCalledWith({ lineNumber: 1, column: 4 });

        fireEvent.click(wrapper.getByTitle('Next Warning'));
        wrapper.getByText(warningsRegex(3, 10));
        expect(editor.setPosition).toHaveBeenLastCalledWith({ lineNumber: 2, column: 1 });

        editor.setPosition({ lineNumber: 3, column: 4 });
        fireEvent.click(wrapper.getByTitle('Next Warning'));
        wrapper.getByText(warningsRegex(7, 10));
        expect(editor.setPosition).toHaveBeenLastCalledWith({ lineNumber: 4, column: 1 });

        fireEvent.click(wrapper.getByTitle('Show Error'));
        expect(editor.setPosition).toHaveBeenLastCalledWith({ lineNumber: 6, column: 3 });
      });

      test('reports unexpected error', async () => {
        translatorCallback = translator => {
          translator.translate.mockImplementation(() => {
            throw new Error('Unexpected bad');
          });
        };

        await validate();

        expect(model.deltaDecorations).toHaveBeenLastCalledWith(expect.anything(), [{
          options: expect.objectContaining({
            glyphMarginHoverMessage: {
              value: 'Error: Unexpected bad',
            },
            hoverMessage: {
              value: 'Error: Unexpected bad',
            },
            isWholeLine: true,
          }),
          range: [1, 0, 1, 0],
        }]);
        wrapper.getByText('Error (1/1): Unexpected bad');
      });
    });

    describe('axes panel', () => {
      test('displays position of the axes in G53 and current coordinate system', async () => {
        const axis = getAxisFromPanel('X');
        getByText(getByText(axis, /G53/).parentElement!, '0.00 mm');
        getByText(getByText(axis, /G54/).parentElement!, '0.00 mm');

        translator.coordinateSystem = 'G55';
        translator.getAxisCoordinateSystemOffset.mockImplementation(system => {
          switch (system) {
            case 'G92': return 7;
            case 'G55': return 100;
            default: return 0;
          }
        });
        axes[0]._settings.pos = 300;

        TestGCode.testStore.dispatch(actions.updatePosition());
        await waitUntilPass(() => getByText(axis, 'G55'));

        getByText(getByText(axis, /G53/).parentElement!, '23.00 mm');
        getByText(getByText(axis, /G55/).parentElement!, '-77.00 mm');
        expect(translator.getAxisCoordinateSystemOffset).toHaveBeenCalledWith('G55', 'X', Length.mm);
      });

      test('homes axis', async () => {
        const axis = getAxisFromPanel('X');
        fireEvent.click(getByTitle(axis, 'Home'));
        await waitUntilPass(() => expect(axes[0].home).toHaveBeenCalled());
      });

      test('jogs axis left and right on mouse down stopping on mouse up', async () => {
        axes[0].moveVelocity.mockImplementation(async () => {
          axes[0]._settings.pos = 200;
        });
        axes[2].moveVelocity.mockImplementation(async () => {
          axes[2]._settings.pos = 300;
        });
        const deferred = defer<void>();
        axes[0].waitUntilIdle.mockReturnValue(deferred.promise);
        axes[2].waitUntilIdle.mockReturnValue(deferred.promise);

        const axis1 = getAxisFromPanel('X');
        fireEvent.mouseDown(getByTitle(axis1, 'Jog Right'));
        await waitUntilPass(() => expect(axes[0].waitUntilIdle).toHaveBeenCalled());
        expect(axes[0].stop).not.toHaveBeenCalled();
        expect(axes[0].moveVelocity).toHaveBeenLastCalledWith(11, Velocity['mm/s']);

        fireEvent.mouseUp(getByTitle(axis1, 'Jog Right'));
        expect(axes[0].stop).toHaveBeenCalled();
        await waitUntilPass(() =>
          getByText(getByText(axis1, /G53/).parentElement!, '20.00 mm'));

        const axis3 = getAxisFromPanel('Z');
        fireEvent.mouseDown(getByTitle(axis3, 'Jog Left'));
        await waitUntilPass(() => expect(axes[2].moveVelocity).toHaveBeenLastCalledWith(33, Velocity['mm/s']));

        fireEvent.mouseUp(getByTitle(axis3, 'Jog Left'));
        expect(axes[2].stop).toHaveBeenCalled();
        await waitUntilPass(() =>
          getByText(getByText(axis3, /G53/).parentElement!, '-30.00 mm'));

        deferred.resolve();
      });

      test('step axis left and right', async () => {
        const axis1 = getAxisFromPanel('X');

        fireEvent.click(getByTitle(axis1, /Step Right 1 mm/));
        await waitUntilPass(() => expect(axes[0].moveRelative).toHaveBeenLastCalledWith(1, Length.mm));

        fireEvent.click(getByTitle(axis1, /Step Right 1 mm/), { shiftKey: true });
        await waitUntilPass(() => expect(axes[0].moveRelative).toHaveBeenLastCalledWith(0.1, Length.mm));

        fireEvent.click(getByTitle(axis1, /Step Right 1 mm/), { ctrlKey: true });
        await waitUntilPass(() => expect(axes[0].moveRelative).toHaveBeenLastCalledWith(0.01, Length.mm));

        fireEvent.click(getByTitle(axis1, /Step Left 1 mm/));
        await waitUntilPass(() => expect(axes[0].moveRelative).toHaveBeenLastCalledWith(-1, Length.mm));

        const axis3 = getAxisFromPanel('Z');
        fireEvent.click(getByTitle(axis3, /Step Left 1 mm/));
        await waitUntilPass(() => expect(axes[2].moveRelative).toHaveBeenLastCalledWith(1, Length.mm));
      });

      test('passes general error', async () => {
        axes[0].home.mockRejectedValueOnce(new Error('Unexpected error'));
        const axis = getAxisFromPanel('X');
        fireEvent.click(getByTitle(axis, 'Home'));
        await waitUntilPass(() => wrapper.getByText(/Translator error: Unexpected error/));
      });

      test('sets offset to current position when zeroing axis', async () => {
        const axis = getAxisFromPanel('Z');
        axes[2]._settings.pos = 300;

        TestGCode.testStore.dispatch(actions.updatePosition());
        await waitUntilPass(() =>
          getByText(getByText(axis, /G53/).parentElement!, '-30.00 mm'));

        fireEvent.click(getByTitle(axis, 'Zero Axis'));
        await waitUntilPass(() =>
          expect(translator.translate).toHaveBeenCalledWith('G10 L2 P0 Z-30'));
      });
    });

    describe('performing', () => {
      const BLOCK_COUNT = 6;

      async function waitUntilEnd() {
        await waitUntilPass(() => wrapper.getByTitle(/Execute/), 3000);
      }

      async function executeCode(waitAsWell = true) {
        fireEvent.click(wrapper.getByTitle(/Execute/));
        await waitUntilPass(() => wrapper.getByTitle(/Pause/));
        if (waitAsWell) {
          await waitUntilEnd();
        }
      }

      function checkLastProgressDecoration(line: number) {
        expect(model.deltaDecorations).toHaveBeenLastCalledWith(expect.anything(), [expect.objectContaining({
          options: expect.objectContaining({
            glyphMarginClassName: 'block-performing-margin',
          }),
          range: [line, -1, line, -1],
        })]);
      }

      beforeEach(() => {
        model.value = _.range(BLOCK_COUNT).map(i => `G${i}`).join('\n');

        translator.translate.mockImplementation(block => ({
          commands: [`cmd ${block}`],
          warnings: [],
        }));
        translator.flush.mockImplementation(() => ['flush cmd']);
      });

      test('runs G-Code on the device updating the progress', async () => {
        const waitUntilIdle = defer<void>();
        device.stream.waitUntilIdle.mockReturnValue(waitUntilIdle.promise);

        let cmdBatch = defer<void>();
        const resolveCmd = () => {
          const last = cmdBatch;
          cmdBatch = defer<void>();
          last.resolve();
        };
        device.stream.genericCommandBatch.mockImplementation(() => cmdBatch.promise);

        await executeCode(false);

        expect(device.stream.setupLiveComposite).toHaveBeenCalledWith(
          { axisNumber: 1 }, { axisNumber: 2 }, { axisNumber: 3 });

        for (let i = 0; i < BLOCK_COUNT; i++) {
          await waitUntilPass(() => checkLastProgressDecoration(i + 1));
          expect(editor.setPosition).toHaveBeenLastCalledWith({ lineNumber: i + 1, column: 1 });

          expect(device.stream.genericCommandBatch).toHaveBeenLastCalledWith([`cmd G${i}`]);
          resolveCmd();
        }

        await waitUntilPass(() => expect(translator.flush).toBeCalledTimes(2));
        expect(device.stream.genericCommandBatch).toHaveBeenLastCalledWith(['flush cmd']);
        resolveCmd();

        await waitUntilPass(() => expect(device.stream.waitUntilIdle).toHaveBeenCalled());
        waitUntilIdle.resolve();

        await waitUntilEnd();
        checkLastProgressDecoration(7);
        expect(editor.setPosition).toHaveBeenLastCalledWith({ lineNumber: 7, column: 1 });
      });

      test('steps through the G-Code waiting for each movement to be finished', async () => {
        fireEvent.click(wrapper.getByLabelText('Step Mode'));

        let waitUntilIdle = defer<void>();
        const resolveWait = () => {
          const last = waitUntilIdle;
          waitUntilIdle = defer<void>();
          last.resolve();
        };
        device.stream.waitUntilIdle.mockImplementation(() => waitUntilIdle.promise);

        await executeCode(false);

        for (let i = 0; i < BLOCK_COUNT; i++) {
          await waitUntilPass(() => {
            expect(translator.translate).toBeCalledTimes(i + 1);
            expect(translator.flush).toBeCalledTimes(i + 2);
            expect(device.stream.waitUntilIdle).toBeCalledTimes(i + 1);
          });
          resolveWait();
        }

        await waitUntilPass(() =>
          expect(device.stream.waitUntilIdle).toBeCalledTimes(7));
        resolveWait();

        await waitUntilEnd();
        expect(translator.flush).toBeCalledTimes(8);
      });

      test('allows to pause in the middle of the streaming', async () => {
        let cmdBatch = defer<void>();
        const resolveCmd = () => {
          const last = cmdBatch;
          cmdBatch = defer<void>();
          last.resolve();
        };
        device.stream.genericCommandBatch.mockImplementation(() => cmdBatch.promise);

        await executeCode(false);

        for (let i = 0; i < 3; i++) {
          await waitUntilPass(() => expect(device.stream.genericCommandBatch).toHaveBeenCalledTimes(i + 1));
          resolveCmd();
        }

        await waitUntilPass(() => expect(device.stream.genericCommandBatch).toHaveBeenCalledTimes(4));
        checkLastProgressDecoration(4);

        fireEvent.click(wrapper.getByTitle(/Pause/));

        await waitUntilPass(() =>
          expect(device.stream.genericCommandBatch).toHaveBeenLastCalledWith(['flush cmd']));
        resolveCmd();

        await waitUntilPass(() =>
          expect(device.stream.waitUntilIdle).toBeCalled());

        await waitUntilEnd();
        checkLastProgressDecoration(5);
      });

      test('allows to hard stop the device', async () => {
        const cmdBatch = defer<void>();
        device.stream.genericCommandBatch.mockReturnValue(cmdBatch.promise);
        device.stream.waitUntilIdle.mockRejectedValue(new StreamModeException('Stream disabled'));

        await executeCode(false);

        await waitUntilPass(() => expect(device.stream.genericCommandBatch).toHaveBeenCalled());

        fireEvent.click(wrapper.getByTitle(/Immediate Stop/));
        cmdBatch.reject(new StreamModeException('Stream disabled'));

        await waitUntilEnd();
        expect(device.allAxes.stop).toHaveBeenCalled();
        expect(device.stream.disable).toHaveBeenCalled();
        wrapper.getByText(/Streaming was unexpectedly interrupted/);
      });

      test('reports warnings during translation', async () => {
        translator.translate.mockImplementation(block => {
          const result: TranslateResult = {
            commands: [],
            warnings: [],
          };
          if (block === 'G2') {
            result.warnings.push({ message: 'G2 is awful', fromBlock: 0, toBlock: 2 });
          } else if (block === 'G4') {
            result.warnings.push({ message: '4 is an ugly number', fromBlock: 1, toBlock: 2 });
          }
          return result;
        });

        await executeCode(true);

        expect(model.deltaDecorations).toHaveBeenCalledWith(expect.anything(), [{
          options: expect.objectContaining({
            hoverMessage: {
              value: 'Warning: G2 is awful',
            },
          }),
          range: [3, 1, 3, 3],
        }]);
        expect(model.deltaDecorations).toHaveBeenCalledWith(expect.anything(), [{
          options: expect.objectContaining({
            hoverMessage: {
              value: 'Warning: 4 is an ugly number',
            },
          }),
          range: [5, 2, 5, 3],
        }]);
        wrapper.getByText('Warning (1/2): G2 is awful');
      });

      test('handles errors during translation', async () => {
        translator.translate.mockImplementation(block => {
          const result: TranslateResult = {
            commands: [],
            warnings: [],
          };
          if (block === 'G3') {
            throw new GCodeSyntaxException('G3 is not valid', { fromBlock: 0, toBlock: 2 });
          }
          return result;
        });

        await executeCode(true);

        expect(model.deltaDecorations).toHaveBeenCalledWith(expect.anything(), [{
          options: expect.objectContaining({
            hoverMessage: {
              value: 'Error: G3 is not valid',
            },
          }),
          range: [4, 1, 4, 3],
        }]);
        wrapper.getByText('Error (1/1): G3 is not valid');
      });

      test('handles errors during streaming', async () => {
        device.stream.genericCommandBatch.mockImplementation(async commands => {
          if (commands.includes('cmd G3')) {
            throw badData();
          }
        });

        await executeCode(true);

        expect(model.deltaDecorations).toHaveBeenCalledWith(expect.anything(), [{
          options: expect.objectContaining({
            hoverMessage: {
              value: 'Error: Movement is outside of axis limits (BADDATA).',
            },
          }),
          range: [3, 0, 4, 0],
        }]);
        expect(device.stream.disable).toHaveBeenCalled();
        wrapper.getByText('Error (1/1): Movement is outside of axis limits (BADDATA).');
      });

      test('handles errors during streaming (flush)', async () => {
        device.stream.genericCommandBatch.mockImplementation(async commands => {
          if (commands.includes('flush cmd')) {
            throw badCmd();
          }
        });

        await executeCode(true);

        expect(model.deltaDecorations).toHaveBeenCalledWith(expect.anything(), [{
          options: expect.objectContaining({
            hoverMessage: {
              value: 'Error: Rejected',
            },
          }),
          range: [6, 0, 7, 0],
        }]);
        expect(device.stream.disable).toHaveBeenCalled();
        wrapper.getByText('Error (1/1): Rejected');
      });

      test('handles errors during streaming (waitUntilIdle)', async () => {
        device.stream.waitUntilIdle.mockImplementation(async () => {
          throw badCmd();
        });

        await executeCode(true);

        expect(model.deltaDecorations).toHaveBeenCalledWith(expect.anything(), [{
          options: expect.objectContaining({
            hoverMessage: {
              value: 'Error: Rejected',
            },
          }),
          range: [6, 0, 7, 0],
        }]);
        expect(device.stream.disable).toHaveBeenCalled();
        wrapper.getByText('Error (1/1): Rejected');
      });

      test('passes unexpected error', async () => {
        device.stream.genericCommandBatch.mockImplementation(async commands => {
          if (commands.includes('cmd G3')) {
            throw new Error('Unexpected error');
          }
        });

        await executeCode(true);

        await waitUntilPass(() => {
          expect(model.deltaDecorations).toHaveBeenCalledWith(expect.anything(), [{
            options: expect.objectContaining({
              hoverMessage: {
                value: 'Error: Unexpected error',
              },
            }),
            range: [3, 0, 4, 0],
          }]);
          wrapper.getByText(/Translator error: Unexpected error/);
        });
      });

      test('resets the stream after an error', async () => {
        device.stream.waitUntilIdle.mockImplementationOnce(async () => {
          throw badCmd();
        });

        await executeCode(true);

        expect(device.stream.disable).toHaveBeenCalled();
        device.stream.setupLiveComposite.mockClear();

        await executeCode(true);

        expect(device.stream.setupLiveComposite).toHaveBeenCalled();
      });

      test('sets position of the translator if it sufficiently differs from the device position', async () => {
        translator.getAxisPosition.mockImplementation(axis => {
          if (axis === 'Y') { throw new InvalidOperationException('Position of axis Y is not initialized.') }
          return 50.3;
        });

        axes[0]._settings.pos = 50;
        axes[1]._settings.pos = 50;
        axes[2]._settings.pos = 300;

        await executeCode(true);

        expect(translator.setAxisPosition).toHaveBeenCalledTimes(2);
        expect(translator.setAxisPosition).toHaveBeenCalledWith('Y', 50, Units.NATIVE);
        expect(translator.setAxisPosition).toHaveBeenCalledWith('Z', 300, Units.NATIVE);
      });

      test('rewind returns position to the beginning removing all the warnings', async () => {
        model.deltaDecorations.mockImplementation((delta, decorations) =>
          decorations.length ? ['token'] : []
        );
        translator.translate.mockImplementation(block => {
          const result: TranslateResult = {
            commands: [],
            warnings: [],
          };
          if (block === 'G2') {
            result.warnings.push({ message: 'G2 is awful', fromBlock: 0, toBlock: 2 });
          } else if (block === 'G4') {
            throw badData();
          }
          return result;
        });

        await executeCode(true);
        wrapper.getByText(warningsRegex(1, 1));

        model.deltaDecorations.mockClear();
        editor.setPosition.mockClear();
        fireEvent.click(wrapper.getByTitle(/Go to the beginning/));

        expect(model.deltaDecorations).toHaveBeenCalledTimes(3);
        for (let i = 1; i <= 3; i++) {
          expect(model.deltaDecorations).toHaveBeenNthCalledWith(i, ['token'], []);
        }
        expect(wrapper.queryByText(warningsRegex(1, 1))).toBeNull();
        expect(editor.setPosition).toHaveBeenCalledWith({ lineNumber: 1, column: 1 });
      });

      test('it allows to step line by line', async () => {
        translator.translate.mockImplementation(() => ({
          commands: [],
          warnings: [{
            fromBlock: 0,
            toBlock: 1,
            message: 'strange code',
          }],
        }));

        fireEvent.click(wrapper.getByTitle(/Step one line/));
        await waitUntilPass(() => wrapper.getByTitle(/Pause/));
        await waitUntilEnd();

        expect(translator.translate).toBeCalledTimes(1);
        expect(translator.translate).toHaveBeenLastCalledWith('G0');
        expect(translator.flush).toBeCalledTimes(2);
        expect(device.stream.waitUntilIdle).toBeCalledTimes(1);

        fireEvent.click(wrapper.getByTitle(/Step one line/));
        await waitUntilPass(() => wrapper.getByTitle(/Pause/));
        await waitUntilEnd();

        expect(translator.translate).toBeCalledTimes(2);
        expect(translator.translate).toHaveBeenLastCalledWith('G1');
        expect(translator.flush).toBeCalledTimes(4);
        expect(device.stream.waitUntilIdle).toBeCalledTimes(2);

        wrapper.getByText(warningsRegex(1, 2));
      });

      test('stops translation if devices are reloaded', async () => {
        device.stream.genericCommandBatch.mockReturnValueOnce(defer<void>().promise);

        await executeCode(false);
        await waitUntilPass(() => expect(device.stream.genericCommandBatch).toHaveBeenCalled());
        device.stream.disable.mockClear();

        mockSingleDeviceWithPeripherals(TestGCode.testStore, {
          peripheralCount: 1,
          type: ['unused'],
        });
        await waitUntilEnd();

        wrapper.getByText(/You have no controller selected/);
        expect(device.stream.disable).toHaveBeenCalledTimes(1);
      });

      test('keeps going over and over in the demo loop', async () => {
        let cmdBatch = defer<void>();
        const resolveCmd = () => {
          const last = cmdBatch;
          cmdBatch = defer<void>();
          last.resolve();
        };
        device.stream.genericCommandBatch.mockImplementation(() => cmdBatch.promise);

        fireEvent.click(wrapper.getByTitle(/Open Menu/));
        fireEvent.click(wrapper.getByText(/Demo Loop/));
        await waitUntilPass(() => wrapper.getByTitle(/Pause/));

        for (let i = 0; i < BLOCK_COUNT * 2; i++) {
          await waitUntilPass(() => expect(device.stream.genericCommandBatch).toHaveBeenCalledTimes(i + 1));
          resolveCmd();
        }
        await waitUntilPass(() =>
          expect(device.stream.genericCommandBatch).toHaveBeenLastCalledWith(['cmd G0']));

        device.stream.genericCommandBatch.mockRejectedValue(new Error('Some failure'));
        resolveCmd();
        await waitUntilEnd();
      });

      test('holds and frees the stream', async () => {
        const cmdBatch = defer<void>();
        device.stream.genericCommandBatch.mockImplementation(() => cmdBatch.promise);
        await executeCode(false);

        fireEvent.click(wrapper.getByTitle('Freehold'));
        await waitUntilPass(() =>
          expect(device.stream.setHold).toHaveBeenCalledWith(true));

        fireEvent.click(wrapper.getByTitle('Freehold'));
        await waitUntilPass(() =>
          expect(device.stream.setHold).toHaveBeenCalledWith(false));

        cmdBatch.resolve();
        await waitUntilEnd();
      });

      test('holds the stream after setup', async () => {
        device.stream.setHold.mockImplementationOnce(async () => {
          throw new StreamModeException('Stream disabled');
        });

        fireEvent.click(wrapper.getByTitle('Freehold'));
        await waitUntilPass(() =>
          expect(device.stream.setHold).toHaveBeenCalledWith(true));
        device.stream.setHold.mockClear();

        await executeCode(true);

        expect(device.stream.setHold).toHaveBeenCalledWith(true);
      });

      test('handles hold errors', async () => {
        device.stream.setHold.mockImplementationOnce(async () => {
          throw badCmd();
        });

        fireEvent.click(wrapper.getByTitle('Freehold'));
        await waitUntilPass(() => wrapper.getByText(/Hold is not supported/));
      });
    });

    describe('command line', () => {
      let cmdInput: HTMLElement;
      let cmdLine: HTMLElement;

      async function waitUntilEnd() {
        await waitUntilPass(() => expect(getByTitle(cmdLine, 'Send')).not.toHaveClass('disabled'));
      }

      async function executeCode() {
        fireEvent.keyDown(cmdInput, { key: 'Enter' });
      }

      beforeEach(() => {
        cmdInput = wrapper.getByRole('textbox');
        cmdLine = getParentWithClass(cmdInput, 'cmd')!;

        fireEvent.change(cmdInput, { target: { value: 'G1' } });
      });

      afterEach(() => {
        cmdInput = null!;
        cmdLine = null!;
      });

      test('allows to send commands to the translator', async () => {
        translator.translate.mockReturnValueOnce({
          commands: ['stream cmd'],
          warnings: [],
        });

        const sendButton = getByTitle(cmdLine, 'Send');
        fireEvent.click(sendButton, sendButton);
        expect(sendButton).toHaveClass('disabled');
        await waitUntilEnd();

        expect(translator.translate).toHaveBeenLastCalledWith('G1');
        expect(translator.flush).toBeCalledTimes(2);
        expect(device.stream.genericCommandBatch).toHaveBeenLastCalledWith(['stream cmd']);
        expect(device.stream.waitUntilIdle).toHaveBeenCalled();
        expect(cmdInput).toHaveValue('');
      });

      test('displays warnings', async () => {
        translator.translate.mockReturnValueOnce({
          commands: ['stream cmd'],
          warnings: [{ message: 'bad G1', fromBlock: 0, toBlock: 2 }],
        });

        await executeCode();
        await waitUntilEnd();

        getByText(cmdLine, /bad G1/);
      });

      test('handles stream error', async () => {
        device.stream.waitUntilIdle.mockRejectedValueOnce(new StreamMovementFailedException('Device has stalled', null!));

        await executeCode();
        await waitUntilEnd();

        getByText(cmdLine, /Device has stalled/);

        expect(device.stream.disable).toBeCalled();
        expect(translator.resetAfterStreamError).toBeCalled();
      });

      test('handles G-Code error', async () => {
        translator.translate.mockImplementation(() => {
          throw new GCodeSyntaxException('Bad syntax', { fromBlock: 0, toBlock: 2 });
        });

        await executeCode();
        await waitUntilEnd();

        getByText(cmdLine, /Bad syntax/);
      });

      test('clears warnings and errors and line on Escape', async () => {
        translator.translate.mockReturnValueOnce({
          commands: ['stream cmd'],
          warnings: [{ message: 'bad G1', fromBlock: 0, toBlock: 2 }],
        });
        device.stream.waitUntilIdle.mockRejectedValueOnce(new StreamMovementFailedException('Device has stalled', null!));

        await executeCode();
        await waitUntilEnd();

        getByText(cmdLine, /bad G1/);
        getByText(cmdLine, /stalled/);
        expect(cmdInput).toHaveValue('G1');

        fireEvent.keyDown(cmdInput, { key: 'Escape' });

        expect(queryByText(cmdLine, /bad G1/)).toBeNull();
        expect(queryByText(cmdLine, /stalled/)).toBeNull();
        expect(cmdInput).toHaveValue('');
      });

      test('remembers all the past commands', async () => {
        for (const cmd of ['G1', 'G2', 'G3']) {
          fireEvent.change(cmdInput, { target: { value: cmd } });
          await executeCode();
          await waitUntilEnd();
        }

        expect(cmdInput).toHaveValue('');
        fireEvent.keyDown(cmdInput, { key: 'ArrowUp' });
        expect(cmdInput).toHaveValue('G3');
        fireEvent.keyDown(cmdInput, { key: 'ArrowUp' });
        expect(cmdInput).toHaveValue('G2');
        for (let i = 0; i < 4; i++) {
          fireEvent.keyDown(cmdInput, { key: 'ArrowUp' });
        }
        expect(cmdInput).toHaveValue('G1');
        fireEvent.keyDown(cmdInput, { key: 'ArrowDown' });
        expect(cmdInput).toHaveValue('G2');
        fireEvent.keyDown(cmdInput, { key: 'ArrowDown' });
        expect(cmdInput).toHaveValue('G3');
        fireEvent.keyDown(cmdInput, { key: 'ArrowDown' });
        expect(cmdInput).toHaveValue('');
        fireEvent.keyDown(cmdInput, { key: 'ArrowUp' });
        expect(cmdInput).toHaveValue('G3');
      });
    });

    test('allows to change feed rate', async () => {
      const feedRate = wrapper.getByText(/Feed Rate/).parentElement!.parentElement!;
      fireEvent.click(getByTitle(feedRate, 'Edit Value'));
      fireEvent.change(getByRole(feedRate, 'spinbutton'), { target: { value: 200 } });
      fireEvent.click(getByTitle(feedRate, 'Confirm'));
      expect(translator.setFeedRateOverride).toHaveBeenLastCalledWith(2);
    });

    test('stopping the translator', async () => {
      wrapper.getByText('Ready');

      fireEvent.click(wrapper.getByTitle(/Open Menu/));
      fireEvent.click(wrapper.getByText(/Stop Translator/));

      await waitUntilPass(() => wrapper.getByText('Translator not running'));
    });
  });
});

describe('lockstep specific', () => {
  beforeEach(async () => {
    mockSingleDeviceWithPeripherals(TestGCode.testStore, {
      modifier: device => {
        device.locksteps = [{
          groupNumber: 1,
          axisNumbers: [1, 2],
        }];
        return device;
      },
    });
  });

  describe('setting up', () => {
    test('it provides correct data to setup ignoring secondary axes', async () => {
      await selectController();
      await configureTranslator();

      expect(TranslatorMock.setupFromDevice).toHaveBeenLastCalledWith(expect.any(DeviceMock), [1, 3, 4], expect.anything());
    });

    test('changing axis letter changes it for entire lockstep', async () => {
      await selectController();

      const axis1 = wrapper.getByText(/Axis 1/).parentElement!;
      fireEvent.change(getByRole(axis1, 'combobox'), { target: { value: 'A' } });

      const axis2 = wrapper.getByText(/Axis 2/).parentElement!;
      expect(getByRole(axis2, 'combobox')).toHaveValue('A');

      fireEvent.click(wrapper.getByTitle('Close Dialog'));
    });
  });

  describe('after setup', () => {
    beforeEach(async () => {
      await selectController();
      await configureTranslator();
    });

    test('provides correct information to stream setup', async () => {
      fireEvent.click(wrapper.getByTitle(/Step one line/));

      await waitUntilPass(() => wrapper.getByTitle(/Pause/));
      await waitUntil(() => wrapper.queryByTitle(/Pause/) == null);

      expect(device.stream.setupLiveComposite).toHaveBeenCalledWith(
        { axisNumber: 1, axisType: ascii.StreamAxisType.LOCKSTEP },
        { axisNumber: 3 }, { axisNumber: 4 });
    });

    test('moves lockstep', async () => {
      const axis = getAxisFromPanel('X');
      fireEvent.click(getByTitle(axis, 'Home'));
      await waitUntilPass(() => expect(device.lockstep.home).toHaveBeenCalled());
    });
  });
});

describe('rotary axis', () => {
  beforeEach(async () => {
    axisCallback = axis => {
      if (axis.axisNumber === 3) {
        axis.identity.axisType = ascii.AxisType.ROTARY;
      }
    };

    mockSingleDeviceWithPeripherals(TestGCode.testStore, {
      type: ['smart', 'smart', 'rotary', 'unused'],
    });

    await selectController();
    await configureTranslator();
  });

  test('moves rotary device and displays position', async () => {
    axes[2].moveVelocity.mockImplementation(async () => {
      axes[2]._settings.pos = 100;
    });

    const axis = getAxisFromPanel('Z');
    fireEvent.mouseDown(getByTitle(axis, 'Jog Right'));

    await waitUntilPass(() => expect(axes[2].waitUntilIdle).toHaveBeenCalled());
    expect(axes[2].moveVelocity).toHaveBeenLastCalledWith(33, AngularVelocity.DEGREES_PER_SECOND);

    getByText(getByText(axis, /G53/).parentElement!, '10.00 °');
  });
});
