import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { defer, waitTick, waitUntilPass, wrapWithNewStore } from '../test';
import { MessageRoutersService } from '../message_router';
import { connectionManagerActions, RouterType } from '../connection_manager';

import { AddConnection } from './AddConnection';

jest.mock('../local_share/LocalShareDiscover', () => ({
  LocalShareDiscover: () => null,
}));

const addRouterMock = jest.fn(() => ({ type: 'NOTHING', payload: undefined }));
connectionManagerActions.addRouter = addRouterMock;

const TestAddConnection = wrapWithNewStore(AddConnection);

let container: Container;
let wrapper: RenderResult;

class RouterConnectionMock {
}

let routerService: MessageRoutersServiceMock;

@injectable()
class MessageRoutersServiceMock {
  routerMock = new RouterConnectionMock();

  getConnection = jest.fn(async () => this.routerMock);
}


beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  routerService = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;

  wrapper = render(<TestAddConnection/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
  routerService = null!;

  addRouterMock.mockClear();
});

beforeEach(async () => {
  fireEvent.click(wrapper.getByText(/Add New Connection/));
  fireEvent.click(wrapper.getByLabelText(/Network Sharing/));
  fireEvent.click(wrapper.getByText(/Next/));
  fireEvent.click(wrapper.getByText(/Connect Manually/));
});

function inputHostname() {
  fireEvent.input(wrapper.getByLabelText(/Hostname/), { target: { value: 'host' } });
}

test('validates hostname and port', () => {
  const button = wrapper.getByText('Create Connection');
  expect(button).toHaveAttribute('disabled');

  fireEvent.input(wrapper.getByLabelText(/Address/), { target: { value: '192.168.1.2' } });
  expect(button).not.toHaveAttribute('disabled');

  fireEvent.input(wrapper.getByLabelText(/Port/), { target: { value: '' } });
  expect(button).toHaveAttribute('disabled');

  fireEvent.input(wrapper.getByLabelText(/Port/), { target: { value: '1234567' } });
  expect(button).toHaveAttribute('disabled');

  fireEvent.input(wrapper.getByLabelText(/Port/), { target: { value: '6789' } });
  expect(button).not.toHaveAttribute('disabled');
});

test('adds router after successful connection', async () => {
  inputHostname();
  fireEvent.click(wrapper.getByText('Create Connection'));

  await waitUntilPass(() => expect(connectionManagerActions.addRouter).toHaveBeenCalledWith(
    expect.objectContaining({
      type: RouterType.LocalShare,
      url: 'local://host:11421',
    }),
  ));
});

test('displays error', async () => {
  inputHostname();
  routerService.getConnection.mockRejectedValueOnce(new Error('Cannot connect'));

  fireEvent.click(wrapper.getByText('Create Connection'));

  await waitUntilPass(() => wrapper.getByText(/Cannot connect/));
});

test('cancels connection when dialog is closed or goes back', async () => {
  inputHostname();

  const deferred = defer<RouterConnectionMock>();
  routerService.getConnection.mockReturnValue(deferred.promise);

  fireEvent.click(wrapper.getByText('Create Connection'));
  fireEvent.click(wrapper.getByText('Back'));

  deferred.resolve(new RouterConnectionMock());
  await waitTick();

  expect(connectionManagerActions.addRouter).not.toHaveBeenCalled();
});
