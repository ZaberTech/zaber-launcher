import React, { HTMLProps } from 'react';
import classNames from 'classnames';

import type { AxisInfo, DeviceInfo } from '../types';
import { axisDescription } from '../utils';

interface Props extends HTMLProps<HTMLDivElement> {
  axis: AxisInfo;
  device: DeviceInfo;
}

export const AxisDescription: React.FC<Props> = ({ axis, device, className, ...rest }) => (
  <div className={classNames('axis-description', className)} {...rest}>{axisDescription(axis, device)}</div>
);
