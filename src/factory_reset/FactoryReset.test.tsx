/* eslint-disable @typescript-eslint/no-this-alias */
import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { wrapWithNewStore, waitUntilPass } from '../test';
import { connectionManagerActions } from '../connection_manager';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import {
  mockSingleDeviceWithPeripherals, MOCK_DEVICE_NUMBER, MOCK_SERIAL_PORT,
} from '../connection_manager/mocks';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { makeAxisKey, makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';

import { FactoryResetDeviceMenuItem, FactoryResetAxisMenuItem, FactoryResetModal } from './FactoryReset';

const RESETTING_MESSAGE = /Resetting/;

let container: Container;

let axes: AxisMock[] = [];
class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);
    axes.push(this);
  }

  restore = jest.fn().mockResolvedValue(undefined);
}

let device: DeviceMock;
let deviceCallback: (device: DeviceMock) => void;
class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    device = this;
    if (deviceCallback) { deviceCallback(this) }
  }

  axisCount = 4;
  restore = jest.fn().mockResolvedValue(undefined);
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
  genericCommandMultiResponse = jest.fn(async (command: string) => {
    switch (command) {
      case 'get system.serial':
        return [{ data: '1235' }];
      default:
        throw new Error('Unexpected command');
    }
  });
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const connectionKey = makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), MOCK_SERIAL_PORT);
const deviceKey = makeDeviceKey(connectionKey, MOCK_DEVICE_NUMBER);
const axisKey = makeAxisKey(deviceKey, 1);

const TestResetDevice = wrapWithNewStore(() => <>
  <FactoryResetDeviceMenuItem deviceKey={deviceKey}/>
  <FactoryResetModal/>
</>);
const TestResetAxis = wrapWithNewStore(() => <>
  <FactoryResetAxisMenuItem axisKey={axisKey}/>
  <FactoryResetModal/>
</>);

const loadDevicesActionMock = jest.fn(() => {
  setTimeout(() => mockSingleDeviceWithPeripherals(TestResetDevice.testStore ?? TestResetAxis.testStore));
  return { type: 'NOTHING', payload: undefined };
});
connectionManagerActions.loadDevices = loadDevicesActionMock;

let wrapper: RenderResult;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;

  device = null!;
  axes = [];
  deviceCallback = null!;

  loadDevicesActionMock.mockClear();
});

describe('device', () => {
  beforeEach(() => {
    wrapper = render(<TestResetDevice/>);
    mockSingleDeviceWithPeripherals(TestResetDevice.testStore);
  });

  test('restores a device and reloads devices', async () => {
    fireEvent.click(wrapper.getByText(/Factory Reset Device/));

    const confirmText = wrapper.getByText(/Are you sure/).textContent!;
    expect(confirmText).toBe('Are you sure you want to factory reset device 01 X-MCC4?');

    fireEvent.click(wrapper.getByText('Reset'));
    wrapper.getByText(RESETTING_MESSAGE);

    await waitUntilPass(() =>
      expect(wrapper.queryByText(RESETTING_MESSAGE)).toBeNull());

    expect(device.restore).toHaveBeenCalledWith();
    expect(loadDevicesActionMock).toHaveBeenCalled();
  });

  test('can hard reset', async () => {
    fireEvent.click(wrapper.getByText(/Factory Reset Device/));

    fireEvent.click(wrapper.getByText(/Reset communication settings/));
    fireEvent.click(wrapper.getByText('Reset'));

    await waitUntilPass(() =>
      expect(wrapper.queryByText(RESETTING_MESSAGE)).toBeNull());

    expect(device.restore).toHaveBeenCalledWith(true);
  });

  test('can cancel the reset', () => {
    fireEvent.click(wrapper.getByText(/Factory Reset Device/));
    wrapper.getByText(/Are you sure/);
    fireEvent.click(wrapper.getByText('Cancel'));
    expect(wrapper.queryByText(/Are you sure/)).toBeNull();
  });

  test('can handle an error', async () => {
    deviceCallback = device => device.restore.mockRejectedValueOnce(new Error('Something went wrong'));
    fireEvent.click(wrapper.getByText(/Factory Reset Device/));
    fireEvent.click(wrapper.getByText('Reset'));
    await waitUntilPass(() => wrapper.getByText(/Something went wrong/));
  });
});

describe('axis', () => {
  beforeEach(() => {
    wrapper = render(<TestResetAxis/>);
    mockSingleDeviceWithPeripherals(TestResetAxis.testStore);
  });

  test('restores an axis', async () => {
    fireEvent.click(wrapper.getByText(/Factory Reset Axis/));
    const confirmText = wrapper.getByText(/Are you sure/).textContent!;
    expect(confirmText).toBe('Are you sure you want to factory reset Axis 1 of device 01 X-MCC4?');

    fireEvent.click(wrapper.getByText('Reset'));
    await waitUntilPass(() =>
      expect(wrapper.queryByText(RESETTING_MESSAGE)).toBeNull());

    expect(axes[0].restore).toHaveBeenCalled();
  });
});
