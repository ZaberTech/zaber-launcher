import { EntityKey, extractDeviceKey, tryGetAxisNumber } from '../keys';
import { changeDictionary, createReducer, writeToDictionary } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import { PollEntry, PollResult, PollRequest, Polled, PolledGetter, keyGenerators } from './types';

export interface State {
  requests: Record<EntityKey, PollRequest>;
  results: Partial<Record<string, PollResult>>;
}

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const initialState: State = {
  requests: {},
  results: {},
};

const initialPollRequest: PollRequest = { settings: {}, analogIo: {}, warnings: {}, triggers: {} };

function startPolling<P extends Polled>(state: State, deviceKey: EntityKey, polled: P, getter: PolledGetter[P], fast: boolean): State {
  const pollKey = keyGenerators[polled](getter);
  const fastIncrement = fast ? 1 : 0;
  const slowIncrement = fast ? 0 : 1;
  return {
    ...state,
    requests: writeToDictionary(state.requests, deviceKey, (_, devicePolls = initialPollRequest) => ({
      ...devicePolls,
      [polled]: writeToDictionary<PollEntry<PolledGetter[P]>, string>(devicePolls[polled], pollKey, (_, poll) => (
        { get: getter, fast: (poll?.fast ?? 0) + fastIncrement, slow: (poll?.slow ?? 0) + slowIncrement }
      ))
    }))
  };
}

function stopPolling<P extends Polled>(state: State, deviceKey: EntityKey, polled: P, getter: PolledGetter[P], fast: boolean): State {
  const pollKey = keyGenerators[polled](getter);
  const fastDecrement = fast ? 1 : 0;
  const slowDecrement = fast ? 0 : 1;
  return {
    ...state,
    requests: writeToDictionary(state.requests, deviceKey, (_, devicePolls = initialPollRequest) => ({
      ...devicePolls,
      [polled]: changeDictionary(devicePolls[polled], pollKey, poll => (
        { ...poll, fast: poll.fast - fastDecrement, slow: poll.slow - slowDecrement }
      )),
    }))
  };
}

const startPollingSetting: Reducer<ActionTypes.START_POLLING_SETTING> = (state, { deviceKey, poll, fast }) => (
  startPolling(state, deviceKey, 'settings', poll, fast)
);

const stopPollingSetting: Reducer<ActionTypes.STOP_POLLING_SETTING> = (state, { deviceKey, poll, fast }) => (
  stopPolling(state, deviceKey, 'settings', poll, fast)
);

const startPollingAnalogIo: Reducer<ActionTypes.START_POLLING_ANALOG_IO> = (state, { deviceKey, port, fast }) => (
  startPolling(state, deviceKey, 'analogIo', port, fast)
);

const stopPollingAnalogIo: Reducer<ActionTypes.STOP_POLLING_ANALOG_IO> = (state, { deviceKey, port, fast }) => (
  stopPolling(state, deviceKey, 'analogIo', port, fast)
);

const startPollingFlags: Reducer<ActionTypes.START_POLLING_FLAGS> = (state, { deviceKey, axisNumber }) => (
  startPolling(state, deviceKey, 'warnings', { axisNumber }, false)
);

const stopPollingFlags: Reducer<ActionTypes.STOP_POLLING_FLAGS> = (state, { deviceKey, axisNumber }) => (
  stopPolling(state, deviceKey, 'warnings', { axisNumber }, false)
);

const startPollingTriggers: Reducer<ActionTypes.START_POLLING_TRIGGERS> = (state, { deviceKey }) => (
  startPolling(state, deviceKey, 'triggers', null, false)
);

const stopPollingTriggers: Reducer<ActionTypes.STOP_POLLING_TRIGGERS> = (state, { deviceKey }) => (
  stopPolling(state, deviceKey, 'triggers', null, false)
);

const clearFlags: Reducer<ActionTypes.CLEAR_FLAGS> = (state, { deviceOrAxisKey }) => {
  const deviceKey = extractDeviceKey(deviceOrAxisKey);
  const axisNumber = tryGetAxisNumber(deviceOrAxisKey) ?? 0;
  return {
    ...state,
    results: changeDictionary(state.results, deviceKey, results => {
      if (results == null || results.type === 'error') { return results }
      return {
        ...results,
        warnings: writeToDictionary(results.warnings, axisNumber, () => []),
        triggers: results.triggers,
      };
    }),
  };
};

const receiveResults: Reducer<ActionTypes.RECEIVE_RESULTS> = (state, { results }) => ({ ...state, results });

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.START_POLLING_SETTING]: startPollingSetting,
  [ActionTypes.STOP_POLLING_SETTING]: stopPollingSetting,

  [ActionTypes.START_POLLING_ANALOG_IO]: startPollingAnalogIo,
  [ActionTypes.STOP_POLLING_ANALOG_IO]: stopPollingAnalogIo,

  [ActionTypes.START_POLLING_FLAGS]: startPollingFlags,
  [ActionTypes.STOP_POLLING_FLAGS]: stopPollingFlags,
  [ActionTypes.CLEAR_FLAGS]: clearFlags,

  [ActionTypes.START_POLLING_TRIGGERS]: startPollingTriggers,
  [ActionTypes.STOP_POLLING_TRIGGERS]: stopPollingTriggers,

  [ActionTypes.RECEIVE_RESULTS]: receiveResults,
}, initialState);
