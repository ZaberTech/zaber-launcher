import type { GetAxisSetting, GetSetting, TriggerEnabledState } from '@zaber/motion/ascii';
import _ from 'lodash';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { EntityKey, extractDeviceKey, getAxisNumber, tryGetAxisNumber } from '../keys';
import { useActions } from '../utils';

import { actionDefinitions } from './actions';
import { selectResults } from './selectors';
import { keyGenerators, PollingError } from './types';

export function usePolledSetting(deviceKey: EntityKey, get: GetSetting, fast = false) {
  const actions = useActions(actionDefinitions);
  const id = keyGenerators.settings(get);
  useEffect(() => {
    actions.startPollingSetting(deviceKey, get, fast);
    return () => {
      actions.stopPollingSetting(deviceKey, get, fast);
    };
  }, [deviceKey, id, fast]);
  const results = useSelector(selectResults)[deviceKey];
  if (results?.type === 'error') {
    return results;
  }
  const setting = results?.settings[id];
  return setting ? { type: 'setting' as const, ...setting } : { type: 'reading' as const };
}

export function usePolledAxisSetting(axisKey: EntityKey, get: GetAxisSetting, fast = false) {
  const deviceKey = extractDeviceKey(axisKey);
  const axisNumber = getAxisNumber(axisKey);
  const result =  usePolledSetting(deviceKey, { ...get, axes: [axisNumber] }, fast);
  return result.type === 'setting' ? result.values[0] : null;
}

export function usePolledWarnings(deviceKey: EntityKey, axisNumber: number) {
  const actions = useActions(actionDefinitions);
  useEffect(() => {
    actions.startPollingFlags(deviceKey, axisNumber);
    return () => {
      actions.stopPollingFlags(deviceKey, axisNumber);
    };
  }, [deviceKey, axisNumber]);
  const results = useSelector(selectResults)[deviceKey];
  if (results?.type === 'error') {
    return results;
  }
  const flags = results?.warnings[axisNumber] ?? [];
  return { type: 'flags' as const, flags };
}

export type PolledAxisWarnings = PollingError | { type: 'flags'; flags: string[] };

export function usePolledProductWarnings(entity: EntityKey): PolledAxisWarnings {
  const deviceKey = extractDeviceKey(entity);
  const axisNumber = tryGetAxisNumber(entity) ?? 0;
  const deviceFlags = usePolledWarnings(deviceKey, axisNumber);
  return deviceFlags;
}

/**
 * Poll all of the digital in or out ports
 * @param deviceKey The device to poll
 * @param type The type of port to poll
 * @param channels The number of ports of `type` on this device
 */
export function usePolledDigitalIo(deviceKey: EntityKey, type: 'in' | 'out', channels: number): boolean[] | null {
  const setting = type === 'in' ? 'io.di.port' : 'io.do.port';
  const pollResult = usePolledSetting(deviceKey, { setting });
  if (pollResult == null || pollResult.type === 'error' || pollResult.type === 'reading') {
    return null;
  }
  const value = pollResult.values[0];
  const portsOn = _.range(channels).map(i => (value & (1 << i)) !== 0);
  return portsOn;
}

export function usePolledAnalogIo(deviceKey: EntityKey, type: 'in' | 'out', channel: number) {
  const actions = useActions(actionDefinitions);
  const getter = { type, channel };
  useEffect(() => {
    actions.startPollingAnalogIo(deviceKey, getter, false);
    return () => {
      actions.stopPollingAnalogIo(deviceKey, getter, false);
    };
  }, [deviceKey, type, channel]);
  const results = useSelector(selectResults)[deviceKey];
  if (results == null || results.type === 'error') {
    return null;
  }
  const io = results.analogIo[keyGenerators.analogIo(getter)];
  return io ?? null;
}

export type PolledTriggers = PollingError | { type: 'triggers'; states: TriggerEnabledState[] };

export function usePolledTriggers(deviceKey: EntityKey): PolledTriggers {
  const actions = useActions(actionDefinitions);
  useEffect(() => {
    actions.startPollingTriggers(deviceKey);
    return () => {
      actions.stopPollingTriggers(deviceKey);
    };
  }, [deviceKey]);
  const results = useSelector(selectResults)[deviceKey];
  if (results?.type === 'error') {
    return results;
  }
  return { type: 'triggers', states: results?.triggers.all ?? [] };
}
