import React from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { Icons, Text } from '@zaber/react-library';
import { match } from 'ts-pattern';
import { AxisType } from '@zaber/motion/ascii';

import { selectAxes } from '../selectors';

import { PassedProperties } from './types';
import { TreeLine } from './TreeLine';
import { AttentionRequester } from './AttentionRequester';

interface Props extends PassedProperties {
  axisKey: string;
  isLastDevice: boolean;
  isLast: boolean;
}

export const Axis: React.FC<Props> = ({
  axisKey, isLastDevice, isLast, selected, onSelect, selectable, pinnedAxesKeys, onPinClick, attentionRequests,
}) => {
  const canSelect = selectable.includes('axis');
  const axis = useSelector(selectAxes)[axisKey];
  const onClick = canSelect ? (() => onSelect(axis.key)) : undefined;
  const onPin = (e: React.MouseEvent<unknown, MouseEvent>) => {
    e.stopPropagation();
    onPinClick?.(axis.key);
  };
  return (
    <div className={classNames({ selectable: canSelect }, 'axis-container')} key={axis.axisNumber} onClick={onClick}>
      {isLastDevice && <div className="axis-level tree-spacer"/>}
      {!isLastDevice && <>
        <div className="device-level tree-spacer"/>
        <TreeLine connectRight={false} connectDown={true}/>
        <div className="device-axis-level-separation tree-spacer"/>
      </>}
      <TreeLine connectRight connectDown={!isLast}/>
      <div className={classNames({ selected: selected === axis.key }, 'axis-name')}>
        <Text>
          {match(axis.identity?.axisType)
            .with(AxisType.PROCESS, () => 'Process')
            .with(AxisType.LAMP, () => 'Lamp')
            .otherwise(() => 'Axis')} {axis.axisNumber}
        </Text>
        {axis.identity?.isPeripheral && <Text>{axis.label ?? axis.identity.peripheralName}</Text>}
        {axis.identity?.isPeripheral === false && axis.label && <Text>{axis.label}</Text>}
        {axis.nonIdentifiedInfo?.peripheralId != null && <Text>Peripheral {axis.nonIdentifiedInfo.peripheralId}</Text>}
        <div className="spacer"/>
        {pinnedAxesKeys?.includes(axis.key) && <Icons.Pin
          className="pin-axis"
          title="Pin Axis"
          onClick={onPin}
          activated={true}/>}
        <AttentionRequester entityKey={axis.key} extractor={e => e} attentionRequests={attentionRequests}/>
      </div>
    </div>
  );
};
