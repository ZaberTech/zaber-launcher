export { wrapWithNewStore } from './wrap_with_store';
export { wrapWithRouter } from './wrap_with_router';
export * from '@zaber/toolbox/lib/test';
export * from '@zaber/app-components/lib/mocks';

export function getParentWithClass(element: HTMLElement | null | undefined, className: string) {
  while (element != null) {
    if (element.classList.contains(className)) {
      return element;
    }
    element = element.parentElement;
  }
  return null;
}


export function getTableCells(table: HTMLTableElement): string[][] {
  const result: string[][] = [];
  const rows = table.querySelectorAll('tr');
  rows.forEach(row => {
    const values = [];
    // Row length - 1 because of blank last column for the Add/Delete Point button.
    for (let i = 0; i < row.cells.length - 1; i++) {
      values.push(row.cells[i].innerHTML);
    }

    result.push(values);
  });

  return result;
}


export const htmlCollectionToArray = function <T extends Element>(collection: HTMLCollectionOf<T>): T[] {
  const result: T[] = [];
  for (const item of collection) {
    result.push(item);
  }

  return result;
};
