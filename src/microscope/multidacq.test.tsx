import React from 'react';
import _ from 'lodash';
import {
  RenderResult, fireEvent, render,
} from '@testing-library/react';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { asyncNoop, badData, CONNECTION_KEY, MessageRoutersServiceMock, microscopeMock } from './mocks';

import { createContainer, destroyContainer } from '../container';
import { defer, StorageMock, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import { MessageRoutersService } from '../message_router';
import { mockSingleDevice } from '../connection_manager/mocks';
import { selectConnections } from '../connection_manager';

import { MicroscopeApp } from './MicroscopeApp';
import { RT, TEST_SKIP_SAGA_DELAYS } from '../utils';
import { Storage } from '../app_components';
import { PRESETS_STORAGE_KEY, type StoredPresets } from './sagas';
import { Preset } from './presets';
import { NO_PRESET } from './testing';
import { AUTOFOCUS_STORAGE_KEY, TEST_END } from './types';
import { DeepPartial } from 'redux';

let storageMock: StorageMock;
const TestApp = wrapWithNewStore(wrapWithRouter(MicroscopeApp));

let wrapper: RenderResult;

beforeEach(() => {
  const container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(Storage).to(StorageMock);
  storageMock = container.get<unknown>(Storage) as StorageMock;
  storageMock.stored[AUTOFOCUS_STORAGE_KEY + CONNECTION_KEY] = 'wdi://host:1234';

  storageMock.stored[PRESETS_STORAGE_KEY + CONNECTION_KEY] = {
    version: 2,
    presets: [
      _.merge({}, NO_PRESET, {
        name: 'Preset 1',
        data: {
          illuminator: {
            1: { intensity: { value: 0.2, apply: true } },
          },
          filter: { value: 1, apply: true },
          objective: { value: 1, apply: true },
        }
      } satisfies DeepPartial<Preset>),
      _.merge({}, NO_PRESET, {
        name: 'Preset 2',
        data: {
          illuminator: {
            1: { intensity: { value: 0.5, apply: true } },
          },
          filter: { value: 2, apply: true },
          objective: { value: 2, apply: true },
        }
      } satisfies DeepPartial<Preset>),
      _.merge({}, NO_PRESET, {
        name: 'Preset 3',
        data: {
          illuminator: {
            1: { intensity: { value: 0.8, apply: true } },
          },
          filter: { value: 3, apply: true },
          objective: { value: 3, apply: true },
        }
      } satisfies DeepPartial<Preset>),
    ],
  } satisfies StoredPresets;
});

afterEach(() => {
  const store = TestApp.testStore;
  wrapper?.unmount();
  wrapper = null!;

  if (store) {
    store.dispatch({ type: TEST_END });
  }

  destroyContainer();

  storageMock = null!;
});

async function doAcquisition() {
  fireEvent.click(wrapper.getByText('Start'));
  await waitUntilPass(() => wrapper.getByText(/Acquisition complete/));
  fireEvent.click(wrapper.getByText('Close'));
}

function selectPresets(presets: string[]) {
  const select = wrapper.getByPlaceholderText(/Select presets/);
  for (const option of select.getElementsByTagName('option')) {
    option.selected = presets.includes(option.textContent!);
  }
  fireEvent.change(select);
}

describe('detected and loaded', () => {
  beforeEach(async () => {
    wrapper = render(<TestApp/>);
    mockSingleDevice(TestApp.testStore);
    const connection = _.sample(selectConnections(TestApp.testStore.getState()))!;

    connectionViewMockInstance.props.onSelect(connection.key);
    await waitUntilPass(() => expect(wrapper.queryByText('Loading Components...')).toBeNull());

    fireEvent.click(wrapper.getByText(/Multi-D Acquisition/));
  });

  describe('position generation', () => {
    let positions: {x: number; y: number; z: number}[] = [];

    beforeEach(() => {
      positions = [];
      microscopeMock.cameraTrigger.trigger = jest.fn(() => {
        positions.push({
          x: Math.round(microscopeMock.xAxis._position),
          y: Math.round(microscopeMock.yAxis._position),
          z: Math.round(microscopeMock.focusAxis._position),
        });
        return Promise.resolve();
      });
    });

    test('simple acquisition with traveling axes', async () => {
      microscopeMock.currentObjective = 4;
      fireEvent.input(wrapper.getByPlaceholderText('X Position'), { target: { value: '5' } });
      fireEvent.input(wrapper.getByPlaceholderText('X Count'), { target: { value: '2' } });
      fireEvent.input(wrapper.getByPlaceholderText('X Step'), { target: { value: '300' } });
      fireEvent.input(wrapper.getByPlaceholderText('Y Position'), { target: { value: '11' } });
      fireEvent.input(wrapper.getByPlaceholderText('Y Count'), { target: { value: '2' } });
      fireEvent.input(wrapper.getByPlaceholderText('Y Step'), { target: { value: '200' } });
      fireEvent.input(wrapper.getByPlaceholderText('Focus Position'), { target: { value: '20' } });
      fireEvent.input(wrapper.getByPlaceholderText('Focus Step'), { target: { value: '100' } });
      fireEvent.input(wrapper.getByPlaceholderText('Positive Focus Step Count'), { target: { value: '1' } });
      fireEvent.input(wrapper.getByPlaceholderText('Negative Focus Step Count'), { target: { value: '2' } });
      fireEvent.change(wrapper.getByPlaceholderText('Order'), { target: { value: 'columns' } });

      await doAcquisition();

      expect(positions).toEqual([
        { x: 5000, y: 11000, z: 19800 },
        { x: 5000, y: 11000, z: 19900 },
        { x: 5000, y: 11000, z: 20000 },
        { x: 5000, y: 11000, z: 20100 },
        { x: 5000, y: 11200, z: 19800 },
        { x: 5000, y: 11200, z: 19900 },
        { x: 5000, y: 11200, z: 20000 },
        { x: 5000, y: 11200, z: 20100 },
        { x: 5300, y: 11000, z: 19800 },
        { x: 5300, y: 11000, z: 19900 },
        { x: 5300, y: 11000, z: 20000 },
        { x: 5300, y: 11000, z: 20100 },
        { x: 5300, y: 11200, z: 19800 },
        { x: 5300, y: 11200, z: 19900 },
        { x: 5300, y: 11200, z: 20000 },
        { x: 5300, y: 11200, z: 20100 },
      ]);
    });

    test('xy orders', async () => {
      microscopeMock.currentObjective = 4;
      fireEvent.input(wrapper.getByPlaceholderText('X Count'), { target: { value: '2' } });
      fireEvent.input(wrapper.getByPlaceholderText('Y Count'), { target: { value: '2' } });
      fireEvent.change(wrapper.getByPlaceholderText('Order'), { target: { value: 'snake-rows' } });

      await doAcquisition();

      expect(positions).toEqual([
        { x: 0, y: 0, z: 0 },
        { x: 100, y: 0, z: 0 },
        { x: 100, y: 100, z: 0 },
        { x: 0, y: 100, z: 0 },
      ]);

      fireEvent.change(wrapper.getByPlaceholderText('Order'), { target: { value: 'snake-columns' } });
      positions = [];
      await doAcquisition();

      expect(positions).toEqual([
        { x: 0, y: 0, z: 0 },
        { x: 0, y: 100, z: 0 },
        { x: 100, y: 100, z: 0 },
        { x: 100, y: 0, z: 0 },
      ]);

      fireEvent.change(wrapper.getByPlaceholderText('Order'), { target: { value: 'rows' } });
      positions = [];
      await doAcquisition();

      expect(positions).toEqual([
        { x: 0, y: 0, z: 0 },
        { x: 100, y: 0, z: 0 },
        { x: 0, y: 100, z: 0 },
        { x: 100, y: 100, z: 0 },
      ]);
    });

    test('focus respects objective offsets', async () => {
      fireEvent.input(wrapper.getByPlaceholderText('Focus Position'), { target: { value: '10' } });
      fireEvent.input(wrapper.getByPlaceholderText('Focus Step'), { target: { value: '1000' } });
      fireEvent.input(wrapper.getByPlaceholderText('Positive Focus Step Count'), { target: { value: '1' } });
      fireEvent.input(wrapper.getByPlaceholderText('Negative Focus Step Count'), { target: { value: '2' } });

      microscopeMock.currentObjective = 1;
      await doAcquisition();
      expect(positions.map(p => p.z)).toEqual([
        9100, 10100, 11100, 12100,
      ]);

      microscopeMock.currentObjective = 2;
      positions = [];
      await doAcquisition();

      expect(positions.map(p => p.z)).toEqual([
        10200, 11200, 12200, 13200,
      ]);
    });

    test('xy grid centering', async () => {
      fireEvent.change(wrapper.getByPlaceholderText('Alignment'), { target: { value: 'center' } });
      fireEvent.change(wrapper.getByPlaceholderText('Order'), { target: { value: 'rows' } });

      microscopeMock.currentObjective = 4;
      fireEvent.input(wrapper.getByPlaceholderText('X Position'), { target: { value: '10' } });
      fireEvent.input(wrapper.getByPlaceholderText('X Count'), { target: { value: '2' } });
      fireEvent.input(wrapper.getByPlaceholderText('X Step'), { target: { value: '300' } });
      fireEvent.input(wrapper.getByPlaceholderText('Y Position'), { target: { value: '20' } });
      fireEvent.input(wrapper.getByPlaceholderText('Y Count'), { target: { value: '2' } });
      fireEvent.input(wrapper.getByPlaceholderText('Y Step'), { target: { value: '200' } });

      await doAcquisition();
      expect([0, 1].map(i => positions[i].x)).toEqual([
        9850, 10150,
      ]);
      expect([0, 2].map(i => positions[i].y)).toEqual([
        19900, 20100,
      ]);

      fireEvent.input(wrapper.getByPlaceholderText('X Count'), { target: { value: '3' } });
      fireEvent.input(wrapper.getByPlaceholderText('Y Count'), { target: { value: '3' } });
      positions = [];
      await doAcquisition();

      expect([0, 1, 2].map(i => positions[i].x)).toEqual([
        9700, 10000, 10300,
      ]);
      expect([0, 3, 6].map(i => positions[i].y)).toEqual([
        19800, 20000, 20200,
      ]);
    });

    test('goes to initial position after acquisition', async () => {
      fireEvent.input(wrapper.getByPlaceholderText('X Position'), { target: { value: '10' } });
      fireEvent.input(wrapper.getByPlaceholderText('Y Position'), { target: { value: '20' } });
      fireEvent.input(wrapper.getByPlaceholderText('Focus Position'), { target: { value: '5' } });

      await doAcquisition();
      expect(positions).toEqual([
        { x: 10000, y: 20000, z: 6100 },
      ]);

      expect(microscopeMock.xAxis._position).toBe(0);
      expect(microscopeMock.yAxis._position).toBe(0);
      expect(microscopeMock.focusAxis._position).toBe(0);
    });

    test('allows to set current position as reference', async () => {
      microscopeMock.xAxis._position = 123000;
      microscopeMock.yAxis._position = 45000;
      microscopeMock.focusAxis._position = 17000;

      TestApp.testStore.dispatch(TEST_SKIP_SAGA_DELAYS);

      await waitUntilPass(() => {
        for (const button of wrapper.getAllByText('Set to Current')) {
          fireEvent.click(button);
        }
        expect(wrapper.getByPlaceholderText('X Position')).toHaveValue(123);
        expect(wrapper.getByPlaceholderText('Y Position')).toHaveValue(45);
        const FOCUS_OFFSET = 1.1;
        expect(wrapper.getByPlaceholderText('Focus Position')).toHaveValue(17 - FOCUS_OFFSET);
      });
    });
  });

  describe('presets dimension', () => {
    let states: {
      objective: number;
      filter: number;
      illuminator: number;
      z: number;
    }[] = [];

    beforeEach(() => {
      states = [];
      microscopeMock.cameraTrigger.trigger = jest.fn(() => {
        states.push({
          objective: microscopeMock.currentObjective,
          filter: microscopeMock.currentFilter,
          illuminator: microscopeMock.illuminator.getChannel(1)._intensity,
          z: Math.round(microscopeMock.focusAxis._position),
        });
        return Promise.resolve();
      });
    });

    test('basic acquisition', async () => {
      selectPresets(['Preset 2', 'Preset 3']);

      await doAcquisition();

      expect(states).toEqual([{
        objective: 2,
        filter: 2,
        illuminator: 0.5,
        z: expect.anything(),
      }, {
        objective: 3,
        filter: 3,
        illuminator: 0.8,
        z: expect.anything(),
      }]);
    });

    test('respects order of dimensions', async () => {
      selectPresets(['Preset 2', 'Preset 3']);

      await doAcquisition();
      expect(states.map(state => state.objective)).toEqual([2, 3]);

      selectPresets(['Preset 3', 'Preset 2']);

      states = [];
      await doAcquisition();
      expect(states.map(state => state.objective)).toEqual([2, 3]);
    });

    test('reverts state after the acquisition', async () => {
      selectPresets(['Preset 2']);

      await doAcquisition();

      expect(states).toHaveLength(1);

      expect(microscopeMock.currentObjective).toBe(1);
      expect(microscopeMock.currentFilter).toBe(1);
      expect(microscopeMock.illuminator.getChannel(1)._intensity).toBe(0);
    });

    test('applies proper focus offset (no matter the order)', async () => {
      fireEvent.input(wrapper.getByPlaceholderText('Focus Position'), { target: { value: '10' } });
      fireEvent.input(wrapper.getByPlaceholderText('Focus Step'), { target: { value: '50' } });
      fireEvent.input(wrapper.getByPlaceholderText('Positive Focus Step Count'), { target: { value: '1' } });

      selectPresets(['Preset 1', 'Preset 2', 'Preset 3']);

      await doAcquisition();
      expect(states.map(state => state.z)).toEqual([
        11100, 11150, 12200, 12250, 10000, 10050,
      ]);

      for (let i = 0; i <= 2; i++) {
        fireEvent.click(wrapper.getByTitle('Move Presets Down'));
      }

      states = [];
      await doAcquisition();
      expect(states.map(state => state.z)).toEqual([
        11100, 12200, 10000, 11150, 12250, 10050,
      ]);
    });
  });

  describe('ETA, cancelling', () => {
    let trigger: RT<typeof defer<void>>;

    beforeEach(() => {
      microscopeMock.cameraTrigger.trigger = jest.fn(() => {
        trigger = defer();
        return trigger.promise;
      });
    });

    test('ETA', async () => {
      fireEvent.input(wrapper.getByPlaceholderText('X Count'), { target: { value: '3' } });
      fireEvent.input(wrapper.getByPlaceholderText('Positive Focus Step Count'), { target: { value: '2' } });
      selectPresets(['Preset 1', 'Preset 2']);

      fireEvent.click(wrapper.getByText('Start'));
      await waitUntilPass(() => {
        wrapper.getByText(/Starting/);
        expect(microscopeMock.cameraTrigger.trigger).toHaveBeenCalledTimes(1);
      });
      trigger.resolve();
      await waitUntilPass(() => {
        wrapper.getByText(/Acquired 01\/18 images/);
        expect(microscopeMock.cameraTrigger.trigger).toHaveBeenCalledTimes(2);
      });
      trigger.resolve();
      await waitUntilPass(() => {
        wrapper.getByText(/Acquired 02\/18 images/);
        expect(microscopeMock.cameraTrigger.trigger).toHaveBeenCalledTimes(3);
      });
      microscopeMock.cameraTrigger.trigger = jest.fn(asyncNoop);
      trigger.resolve();

      await waitUntilPass(() => wrapper.getByText(/Acquisition complete/));
      wrapper.getByText(/Acquired 18 images/);
    });

    test('can cancel, returns to initial state', async () => {
      fireEvent.input(wrapper.getByPlaceholderText('Focus Position'), { target: { value: '10' } });
      fireEvent.input(wrapper.getByPlaceholderText('Positive Focus Step Count'), { target: { value: '2' } });

      fireEvent.click(wrapper.getByText('Start'));
      await waitUntilPass(() =>
        expect(microscopeMock.cameraTrigger.trigger).toHaveBeenCalledTimes(1));
      expect(microscopeMock.focusAxis._position).not.toBe(0);

      fireEvent.click(wrapper.getByText('Stop'));
      wrapper.getByText(/Cancelling/);

      await waitUntilPass(() => expect(wrapper.queryByText(/Cancelling/)).toBeNull());
      expect(microscopeMock.focusAxis._position).toBe(0);
    });

    test('waits for all movement to stop after cancellation', async () => {
      fireEvent.input(wrapper.getByPlaceholderText('Positive Focus Step Count'), { target: { value: '2' } });

      let busyCounter = 3;
      microscopeMock.connection.genericCommandMultiResponse.mockImplementation(async () => {
        const status = --busyCounter > 0 ? 'BUSY' : 'IDLE';
        return [{ status }, { status }];
      });

      fireEvent.click(wrapper.getByText('Start'));
      await waitUntilPass(() =>
        expect(microscopeMock.cameraTrigger.trigger).toHaveBeenCalledTimes(1));

      fireEvent.click(wrapper.getByText('Stop'));
      await waitUntilPass(() => expect(wrapper.queryByText('Stop')).toBeNull());

      expect(microscopeMock.connection.genericCommandMultiResponse).toHaveBeenCalledTimes(3);
    });
  });

  describe('error handling', () => {
    let trigger: RT<typeof defer<void>>;

    beforeEach(() => {
      microscopeMock.cameraTrigger.trigger = jest.fn(() => {
        trigger = defer();
        return trigger.promise;
      });
      fireEvent.input(wrapper.getByPlaceholderText('Focus Position'), { target: { value: '10' } });
      fireEvent.input(wrapper.getByPlaceholderText('Positive Focus Step Count'), { target: { value: '2' } });
    });

    test('displays single error and returns to initial state', async () => {
      fireEvent.click(wrapper.getByText('Start'));
      await waitUntilPass(() =>
        expect(microscopeMock.cameraTrigger.trigger).toHaveBeenCalledTimes(1));
      expect(microscopeMock.focusAxis._position).not.toBe(0);

      microscopeMock.focusAxis.moveAbsolute.mockRejectedValueOnce(badData());
      trigger.resolve();

      await waitUntilPass(() => {
        wrapper.getByText(/Error: Rejected/);
        expect(microscopeMock.focusAxis._position).toBe(0);
      });
    });

    test('displays error in case of persistent failure', async () => {
      fireEvent.click(wrapper.getByText('Start'));
      await waitUntilPass(() =>
        expect(microscopeMock.cameraTrigger.trigger).toHaveBeenCalledTimes(1));
      expect(microscopeMock.focusAxis._position).not.toBe(0);

      microscopeMock.focusAxis.moveAbsolute.mockRejectedValue(badData());
      trigger.resolve();

      await waitUntilPass(() => wrapper.getByText(/Error: Could not apply preset: Focus Axis: Rejected/));
      expect(microscopeMock.focusAxis._position).not.toBe(0);
    });
  });

  describe('dimension ordering', () => {
    let states: {
      objective: number;
      x: number;
      z: number;
    }[] = [];

    beforeEach(() => {
      states = [];
      microscopeMock.cameraTrigger.trigger = jest.fn(() => {
        states.push({
          objective: microscopeMock.currentObjective,
          x: Math.round(microscopeMock.xAxis._position),
          z: Math.round(microscopeMock.focusAxis._position),
        });
        return Promise.resolve();
      });

      selectPresets(['Preset 2', 'Preset 3']);
      fireEvent.input(wrapper.getByPlaceholderText('Focus Position'), { target: { value: '10' } });
      fireEvent.input(wrapper.getByPlaceholderText('Focus Step'), { target: { value: '50' } });
      fireEvent.input(wrapper.getByPlaceholderText('Positive Focus Step Count'), { target: { value: '1' } });
      fireEvent.input(wrapper.getByPlaceholderText('X Count'), { target: { value: '2' } });
    });

    test('order: focus, xy, presets', async () => {
      for (let i = 0; i <= 2; i++) {
        fireEvent.click(wrapper.getByTitle('Move Presets Down'));
      }
      fireEvent.click(wrapper.getByTitle('Move XY Position Down'));

      await doAcquisition();

      expect(states).toEqual([
        { objective: 2, x: 0, z: 12200 },
        { objective: 3, x: 0, z: 10000 },
        { objective: 2, x: 100, z: 12200 },
        { objective: 3, x: 100, z: 10000 },
        { objective: 2, x: 0, z: 12250 },
        { objective: 3, x: 0, z: 10050 },
        { objective: 2, x: 100, z: 12250 },
        { objective: 3, x: 100, z: 10050 },
      ]);
    });

    test('order: presets, focus, xy', async () => {
      fireEvent.click(wrapper.getByTitle('Move Focus Up'));

      await doAcquisition();

      expect(states).toEqual([
        { objective: 2, x: 0, z: 12200 },
        { objective: 2, x: 100, z: 12200 },
        { objective: 2, x: 0, z: 12250 },
        { objective: 2, x: 100, z: 12250 },
        { objective: 3, x: 0, z: 10000 },
        { objective: 3, x: 100, z: 10000 },
        { objective: 3, x: 0, z: 10050 },
        { objective: 3, x: 100, z: 10050 },
      ]);
    });
  });

  describe('Autofocus', () => {
    let states: {
      x: number;
      z: number;
    }[] = [];

    let focusAttempt = 0;
    let autofocusPositions: { x: number; z: number }[] = [];

    beforeEach(() => {
      states = [];
      focusAttempt = 0;
      autofocusPositions = [];

      microscopeMock.cameraTrigger.trigger = jest.fn(() => {
        states.push({
          x: Math.round(microscopeMock.xAxis._position),
          z: Math.round(microscopeMock.focusAxis._position),
        });
        return Promise.resolve();
      });
      microscopeMock.autofocus.focusOnce = jest.fn(async () => {
        autofocusPositions.push({
          z: Math.round(microscopeMock.focusAxis._position),
          x: Math.round(microscopeMock.xAxis._position),
        });
        focusAttempt++;
        microscopeMock.focusAxis._position += focusAttempt;
      });

      selectPresets(['Preset 2', 'Preset 3']);
      fireEvent.input(wrapper.getByPlaceholderText('Focus Position'), { target: { value: '10' } });
      fireEvent.input(wrapper.getByPlaceholderText('Focus Step'), { target: { value: '50' } });
      fireEvent.input(wrapper.getByPlaceholderText('Positive Focus Step Count'), { target: { value: '1' } });
      fireEvent.input(wrapper.getByPlaceholderText('Negative Focus Step Count'), { target: { value: '1' } });
      fireEvent.input(wrapper.getByPlaceholderText('X Count'), { target: { value: '2' } });
    });

    test('focuses for every X and Z reference change', async () => {
      fireEvent.change(wrapper.getByPlaceholderText('Autofocus'), { target: { value: 'reference' } });
      await doAcquisition();

      expect(states).toEqual([
        { x: 0, z: 12151 },
        { x: 0, z: 12201 },
        { x: 0, z: 12251 },
        { x: 100, z: 12152 },
        { x: 100, z: 12202 },
        { x: 100, z: 12252 },
        { x: 0, z: 9953 },
        { x: 0, z: 10003 },
        { x: 0, z: 10053 },
        { x: 100, z: 9954 },
        { x: 100, z: 10004 },
        { x: 100, z: 10054 },
      ]);

      expect(autofocusPositions).toEqual([
        { x: 0, z: 12200 },
        { x: 100, z: 12200 },
        { x: 0, z: 10000 },
        { x: 100, z: 10000 },
      ]);
    });

    test('focuses for every reference change using the latest position', async () => {
      fireEvent.change(wrapper.getByPlaceholderText('Autofocus'), { target: { value: 'previous' } });
      await doAcquisition();

      expect(autofocusPositions).toEqual([
        { x: 0, z: 12200 },
        { x: 100, z: 12201 },
        { x: 0, z: 10003 },
        { x: 100, z: 10006 },
      ]);
    });
  });
});
