import type { Units } from '@zaber/motion';

import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

import type { SettingsEntryInfo, SettingValue, customDialog, NetworkConfig } from './types';

export enum ActionTypes {
  SELECT_DEVICE_OR_AXIS = 'DEVICE_SETTINGS_SELECT_DEVICE_OR_AXIS',
  POPULATE_SETTINGS = 'DEVICE_SETTINGS_POPULATE_SETTINGS',
  READ_SETTING = 'DEVICE_SETTINGS_READ_SETTING',
  READ_ALL_SETTINGS = 'DEVICE_SETTINGS_READ_ALL_SETTINGS',
  CLEAR_SETTING_QUEUE = 'DEVICE_SETTINGS_CLEAR_SETTING_QUEUE',
  READ_SETTING_DONE = 'DEVICE_SETTINGS_READ_SETTING_DONE',
  WRITE_SETTING = 'DEVICE_SETTINGS_WRITE_SETTING',
  WRITE_STRING_SETTING = 'DEVICE_SETTINGS_WRITE_STRING_SETTING',
  WRITE_SETTING_DONE = 'DEVICE_SETTINGS_WRITE_SETTING_DONE',
  UPDATE_SETTING = 'DEVICE_SETTINGS_UPDATE_SETTING',
  UPDATE_CURRENT_ACCESS_LEVEL = 'DEVICE_SETTINGS_UPDATE_CURRENT_ACCESS_LEVEL',
  SET_SEARCH_STRING = 'DEVICE_SETTINGS_SET_SEARCH_STRING',
  UPDATE_SCROLL_POSITION = 'DEVICE_SETTINGS_UPDATE_SCROLL_POSITION',
  SET_CUSTOM_DIALOG = 'SET_CUSTOM_DIALOG',
  CONFIGURE_NETWORK = 'CONFIGURE_NETWORK',
  CONFIGURE_NETWORK_DONE = 'CONFIGURE_NETWORK_DONE',
  SET_ERROR = 'DEVICE_SETTINGS_SET_ERROR',
}

export interface ActionsToPayloads {
  [ActionTypes.SELECT_DEVICE_OR_AXIS]: { deviceOrAxis: EntityKey | null };
  [ActionTypes.POPULATE_SETTINGS]: { deviceOrAxis: EntityKey; settings: SettingsEntryInfo[] };
  [ActionTypes.READ_SETTING]: { deviceOrAxis: EntityKey; settings: string | string[] };
  [ActionTypes.READ_ALL_SETTINGS]: void;
  [ActionTypes.CLEAR_SETTING_QUEUE]: void;
  [ActionTypes.READ_SETTING_DONE]: { deviceOrAxis: EntityKey; setting: string; value?: SettingValue; error?: string };
  [ActionTypes.WRITE_SETTING]: { deviceOrAxis: EntityKey; setting: string; value: number; units: Units };
  [ActionTypes.WRITE_STRING_SETTING]: { deviceOrAxis: EntityKey; setting: string; value: string };
  [ActionTypes.WRITE_SETTING_DONE]: { deviceOrAxis: EntityKey; setting: string; error?: string };
  [ActionTypes.UPDATE_SETTING]: { deviceOrAxis: EntityKey; setting: string; update: SettingValue };
  [ActionTypes.UPDATE_CURRENT_ACCESS_LEVEL]: { deviceOrAxis: EntityKey; currentAccessLevel: number };
  [ActionTypes.SET_SEARCH_STRING]: string;
  [ActionTypes.UPDATE_SCROLL_POSITION]: { deviceOrAxis: EntityKey; scrollPosition: number };
  [ActionTypes.SET_CUSTOM_DIALOG]: { customDialog: customDialog | null };
  [ActionTypes.CONFIGURE_NETWORK]: { config: NetworkConfig };
  [ActionTypes.CONFIGURE_NETWORK_DONE]: { error?: string };
  [ActionTypes.SET_ERROR]: { deviceOrAxis: EntityKey; error: string | null};
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  selectDeviceOrAxis: (deviceOrAxis: EntityKey | null) => buildAction(ActionTypes.SELECT_DEVICE_OR_AXIS, { deviceOrAxis }),
  populateSettings: (deviceOrAxis: EntityKey, settings: SettingsEntryInfo[]) =>
    buildAction(ActionTypes.POPULATE_SETTINGS, { deviceOrAxis, settings }),
  readSetting: (deviceOrAxis: EntityKey, settings: string | string[]) =>
    buildAction(ActionTypes.READ_SETTING, { deviceOrAxis, settings }),
  readAllSettings: () => buildAction(ActionTypes.READ_ALL_SETTINGS),
  clearSettingQueue: () => buildAction(ActionTypes.CLEAR_SETTING_QUEUE),
  readSettingDone: (deviceOrAxis: EntityKey, setting: string, value: SettingValue) =>
    buildAction(ActionTypes.READ_SETTING_DONE, { deviceOrAxis, setting, value }),
  readSettingDoneErr: (deviceOrAxis: EntityKey, setting: string, error?: string) =>
    buildAction(ActionTypes.READ_SETTING_DONE, { deviceOrAxis, setting, error }),
  writeSetting: (deviceOrAxis: EntityKey, setting: string, value: number, units: Units) =>
    buildAction(ActionTypes.WRITE_SETTING, { deviceOrAxis, setting, value, units }),
  writeStringSetting: (deviceOrAxis: EntityKey, setting: string, value: string) =>
    buildAction(ActionTypes.WRITE_STRING_SETTING, { deviceOrAxis, setting, value }),
  writeSettingDone: (deviceOrAxis: EntityKey, setting: string, error?: string) =>
    buildAction(ActionTypes.WRITE_SETTING_DONE, { deviceOrAxis, setting, error }),
  updateSetting: (deviceOrAxis: EntityKey, setting: string, update: SettingValue) =>
    buildAction(ActionTypes.UPDATE_SETTING, { deviceOrAxis, setting, update }),
  updateCurrentAccessLevel: (deviceOrAxis: EntityKey, currentAccessLevel: number) =>
    buildAction(ActionTypes.UPDATE_CURRENT_ACCESS_LEVEL, { deviceOrAxis, currentAccessLevel }),
  setSearchString: (searchWith: string) => buildAction(ActionTypes.SET_SEARCH_STRING, searchWith),
  updateScrollPosition: (deviceOrAxis: EntityKey, scrollPosition: number) =>
    buildAction(ActionTypes.UPDATE_SCROLL_POSITION, { deviceOrAxis, scrollPosition }),
  setCustomDialog: (customDialog: customDialog | null) => buildAction(ActionTypes.SET_CUSTOM_DIALOG, { customDialog }),
  configureNetwork: (config: NetworkConfig) => buildAction(ActionTypes.CONFIGURE_NETWORK, { config }),
  configureNetworkDone: (error?: string) => buildAction(ActionTypes.CONFIGURE_NETWORK_DONE, { error }),
  setError: (deviceOrAxis: EntityKey, error: string | null) =>
    buildAction(ActionTypes.SET_ERROR, { deviceOrAxis, error }),
};
