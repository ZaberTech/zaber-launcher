import type { CanSetStateDeviceResponse, SetStateAxisResponse, SetStateDeviceResponse } from '@zaber/motion/ascii';

import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { LoadErrorData, LoadOpenContext as LoadContext, SaveLoadTarget } from './types';

export enum ActionTypes {
  // Control Modal
  BEGIN = 'SAVE_LOAD-BEGIN',
  CLOSE = 'SAVE_LOAD-CLOSE',
  WORKING = 'SAVE_LOAD-WORKING',

  // Save
  BEGIN_SAVE = 'SAVE_LOAD-BEGIN_SAVE',
  CHOOSE_FILE = 'SAVE_LOAD-CHOOSE_FILE',
  SAVE_DONE = 'SAVE_LOAD-SAVE_DONE',

  // Load
  BEGIN_LOAD = 'SAVE_LOAD-BEGIN_LOAD',
  CANCEL_FILE_SELECT = 'CANCEL_FILE_SELECT',
  CHECK_LOAD_DONE = 'SAVE_LOAD-CHECK_LOAD_DONE',
  APPLY_LOAD = 'SAVE_LOAD-APPLY_LOAD',
  LOAD_ERROR = 'SAVE_LOAD-LOAD_ERROR',
  LOAD_DONE = 'SAVE_LOAD-LOAD_DONE',

  // Restore
  RESTORE = 'SAVE_LOAD-RESTORE',
  RESTORE_DONE = 'SAVE_LOAD-RESTORE_DONE',
  RESTORE_ERROR = 'SAVE_LOAD-RESTORE_ERROR',
}

export interface ActionsToPayloads {
  // Control Modal
  [ActionTypes.BEGIN]: { entity: EntityKey };
  [ActionTypes.CLOSE]: null;
  [ActionTypes.WORKING]: { message: string };
  [ActionTypes.CANCEL_FILE_SELECT]: null;

  // Save
  [ActionTypes.BEGIN_SAVE]: { target: SaveLoadTarget };
  [ActionTypes.CHOOSE_FILE]: null;
  [ActionTypes.SAVE_DONE]: { error?: string };

  // Load
  [ActionTypes.BEGIN_LOAD]: { target: SaveLoadTarget };
  [ActionTypes.CHECK_LOAD_DONE]: { context: LoadContext; result: string | null | CanSetStateDeviceResponse };
  [ActionTypes.APPLY_LOAD]: { target: SaveLoadTarget; context: LoadContext; deviceOnly: boolean };
  [ActionTypes.LOAD_ERROR]: { error: LoadErrorData };
  [ActionTypes.LOAD_DONE]: { entity: EntityKey; context: LoadContext; response: SetStateDeviceResponse | SetStateAxisResponse};

  // Restore
  [ActionTypes.RESTORE]: { target: SaveLoadTarget };
  [ActionTypes.RESTORE_DONE]: void;
  [ActionTypes.RESTORE_ERROR]: { error: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  // Control Modal
  begin: (entity: EntityKey) => buildAction(ActionTypes.BEGIN, { entity }),
  close: () => buildAction(ActionTypes.CLOSE),
  working: (message: string) => buildAction(ActionTypes.WORKING, { message }),
  cancelFileSelect: () => buildAction(ActionTypes.CANCEL_FILE_SELECT),

  // Save
  beginSave: (target: SaveLoadTarget) => buildAction(ActionTypes.BEGIN_SAVE, { target }),
  chooseFile: () => buildAction(ActionTypes.CHOOSE_FILE),
  saveError: (error: string) => buildAction(ActionTypes.SAVE_DONE, { error }),
  saveDone: () => buildAction(ActionTypes.SAVE_DONE, {}),

  // Load
  beginLoad: (target: SaveLoadTarget) => buildAction(ActionTypes.BEGIN_LOAD, { target }),
  checkLoadDone: (context: LoadContext, result: string | null | CanSetStateDeviceResponse) => (
    buildAction(ActionTypes.CHECK_LOAD_DONE, { context, result })
  ),
  applyLoad: (target: SaveLoadTarget, context: LoadContext, deviceOnly: boolean) => buildAction(
    ActionTypes.APPLY_LOAD, { target, context, deviceOnly }
  ),
  loadError: (error: LoadErrorData) => buildAction(ActionTypes.LOAD_ERROR, { error }),
  loadDone: (entity: EntityKey, context: LoadContext, response: SetStateDeviceResponse | SetStateAxisResponse) => (
    buildAction(ActionTypes.LOAD_DONE, { entity, context, response })
  ),

  // Restore
  restore: (target: SaveLoadTarget) => buildAction(ActionTypes.RESTORE, { target }),
  restoreDone: () => buildAction(ActionTypes.RESTORE_DONE),
  restoreError: (error: string) => buildAction(ActionTypes.RESTORE_ERROR, { error }),
};
