import React from 'react';
import { Text } from '@zaber/react-library';
import type { Units } from '@zaber/motion';

import type { Setting } from '../types';
import { getUnitLabel } from '../../units';

interface Props {
  targetSettings: Setting[];
  actualSettings: Setting[];
}

export const SettingsTable: React.FC<Props> = ({ targetSettings, actualSettings }) => (
  <table className="setting-table">
    <thead>
      <tr>
        <th><Text t={Text.Type.H4}>Setting</Text></th>
        <th><Text t={Text.Type.H4}>Actual Value</Text></th>
        <th><Text t={Text.Type.H4}>Target Value</Text></th>
        <th><Text t={Text.Type.H4}>Units</Text></th>
      </tr>
    </thead>
    <tbody>
      {actualSettings.map((setting, index) => <tr key={setting.name}>
        <td><Text>{setting.name}</Text></td>
        <td><Text>{setting.value}</Text></td>
        <td><Text>{targetSettings[index].value}</Text></td>
        <td>{setting.units && <Text>{getUnitLabel(setting.units as Units)}</Text>}</td>
      </tr>)}
    </tbody>
  </table>
);
