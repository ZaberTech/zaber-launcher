/* eslint-disable @typescript-eslint/no-this-alias */
import React from 'react';
import { fireEvent, render, RenderResult, screen } from '@testing-library/react';
import { Container, injectable } from 'inversify';
import { MessageType } from '@zaber/motion/ascii';

import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import {
  AxisMockBase,
  DeviceMockBase,
  ConnectionMockBase,
  RouterConnectionMockBase,
  MessageRoutersServiceMockBase
} from '../test/mocks/ascii';
import { createContainer, destroyContainer } from '../container';
import { makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';
import { MOCK_DEVICE_NUMBER, MOCK_SERIAL_PORT } from '../connection_manager/mocks';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';

import { DeviceError } from './DeviceError';

let wrapper: RenderResult;
let container: Container;

class AxisMock extends AxisMockBase { }

let device: DeviceMock;
class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    device = this;
  }
  axisCount = 1;
  genericCommandMultiResponse = jest.fn().mockResolvedValue([
    { data: '0', messageType: MessageType.REPLY },
    { data: 'p0 data-access-violation 134932248 0', messageType: MessageType.INFO },
    { data: 'p0 data-pizza-violation 134932248 0', messageType: MessageType.INFO },
    { data: 'p0 data-lasagna-violation 134932248 0', messageType: MessageType.INFO },
  ]);
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> { }
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> { }

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const connectionKey = makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), MOCK_SERIAL_PORT);
const deviceKey = makeDeviceKey(connectionKey, MOCK_DEVICE_NUMBER);

const TestDeviceError = wrapWithNewStore(wrapWithRouter(DeviceError));

describe('DeviceError Component', () => {
  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  });

  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;

    device = null!;
    jest.clearAllMocks();
  });

  it('retrieves system errors when rendered', async () => {
    wrapper = render(<TestDeviceError deviceKey={deviceKey}/>);

    await waitUntilPass(() => expect(device.genericCommandMultiResponse).toHaveBeenCalledWith('system errors'));

    fireEvent.click(screen.getByText('Show Details...'));
    expect(screen.getByText('p0 data-access-violation 134932248 0')).toBeInTheDocument();
    expect(screen.getByText('p0 data-pizza-violation 134932248 0')).toBeInTheDocument();
    expect(screen.getByText('p0 data-lasagna-violation 134932248 0')).toBeInTheDocument();
  });

  it('displays generic message when errors are loading', async () => {
    wrapper = render(<TestDeviceError deviceKey={deviceKey}/>);

    fireEvent.click(screen.getByText('Show Details...'));
    expect(screen.getByText(/Awaiting.*response from the device\./));
  });
});
