import React from 'react';
import { Icons, SvgIconProps } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import type { Moveable } from '../types';
import { isMatch } from '../../utils';

import { actionDefinitions } from './actions';
import type { AxisMovementState } from './types';


interface Props {
  moveable: Moveable;
  direction: 'home' | 'away';
  state: AxisMovementState;
}

export const Jog: React.FC<Props> = ({ moveable, direction, state }) => {
  const actions = useActions(actionDefinitions);
  const { motion } = moveable;

  const move = () => {
    actions.issueCommand(moveable, { type: 'vel', initiator: 'jog', direction, velocity: motion.maxSpeed });
  };
  const jogDone = () => {
    if (isMatch({ type: 'vel', initiator: 'jog', direction }, state.lastCommand)) {
      actions.issueCommand(moveable, { type: 'stop' });
    }
  };
  const props: SvgIconProps = {
    title: direction === 'home' ? 'Move towards Beginning' : 'Move towards End',
    className: `jog-${direction}`,
    appearsClickable: true,
    onMouseDown: move,
    onTouchStart: move,
    onTouchEnd: jogDone,
    onMouseLeave: jogDone,
    onDragLeave: jogDone,
    onMouseUp: jogDone,
    activated: isMatch({ type: 'vel', initiator: 'jog', direction }, state.lastCommand),
  };

  if (motion.type === 'cyclic') {
    return <Icons.Rotate highlight="off" {...props}/>;
  } else if (direction === 'home') {
    return <Icons.LeftNormal highlight="off" {...props}/>;
  } else {
    return <Icons.RightNormal highlight="off" {...props}/>;
  }
};
