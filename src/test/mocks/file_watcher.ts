import { injectable } from 'inversify';

import type { Callback } from '../../file_watcher';

@injectable()
export class FileWatcherMock {
  private callbacks: _.Dictionary<Callback> = {};


  register(path: string, callback: Callback) {
    this.callbacks[path] = callback;
  }


  unregister(path: string) {
    delete this.callbacks[path];
  }


  fireChanged(path: string) {
    this.callbacks[path]?.(path);
  }
}
