import React from 'react';
import { useActions } from '@zaber/toolbox/lib/redux';
import { settingsMetadata } from '@zaber/device-db-metadata';
import { useSelector } from 'react-redux';

import settingsData from '../../../protocol_manual/settings.json';
import { actions as actionsDefinition } from '../../actions';
import { SettingField, UNDEFINED_NAME } from '../fields/SettingField';
import { HwModField } from '../fields/HwModField';
import { AxisType } from '../../types';
import { is3rdPartyId } from '../../peripheral_id_picker/peripheralIdMapping';
import { selectDeviceOrAxisInfo } from '../../selectors';

import type { ContentProps, HwModSectionTemplate } from './HwModSection';
import cyclicDocs from './custom_manuals/is_cyclic.html';
import unitConversionEnableDocs from './custom_manuals/unit_conversion_enable.html';
import unitConversionOverrideDocs from './custom_manuals/unit_conversion_override.html';
import unitConversionLinear from './custom_manuals/unit_conversion_linear.html';
import unitConversionRotary from './custom_manuals/unit_conversion_rotary.html';

const MotionSection: React.FC<ContentProps<'motion'>> = ({ values }) => {
  const actions = useActions(actionsDefinition);
  const product = useSelector(selectDeviceOrAxisInfo);
  const is3rdParty = is3rdPartyId(product?.axis?.identity?.peripheralId);

  const sectionId = 'motion';
  const cyclic = values.isCyclic?.is === 1;
  const isInfinite = values.isInfinite;
  const axisType: AxisType = values.axisType?.is ?? AxisType.UNKNOWN;

  return <>
    {axisType !== AxisType.UNKNOWN && <>
      <HwModField
        sectionId="motion" fieldId="unitConversionEnabled" values={values}
        name={is3rdParty ? 'Enable Unit Conversions' : 'Override Unit Conversions'}
        description={is3rdParty ? 'Allows use of real-world units in software' : 'Change the default unit conversions supplied by software'}
        input={{ type: 'toggle' }}
        onUpdate={enabled => {
          actions.updateValues([
            { sectionId, fieldId: 'unitConversionEnabled', value: enabled },
            { sectionId, fieldId: 'unitConversion', value: enabled ? 'revert' : null },
          ]);
        }}
        docs={{ html: is3rdParty ? unitConversionEnableDocs : unitConversionOverrideDocs, type: 'custom' }}/>
      <HwModField
        sectionId="motion" fieldId="unitConversion" values={values}
        name="Conversion Factor"
        description={`Number of position units per ${axisType === AxisType.LINEAR ? '1 mm' : '360 degrees'}`}
        onUpdate={newValue => {
          actions.updateValues([
            { sectionId, fieldId: 'unitConversion', value: newValue },
          ]);
        }}
        input={{ type: 'numeric' }}
        docs={{ html: axisType === AxisType.LINEAR ? unitConversionLinear : unitConversionRotary, type: 'custom' }}/>
    </>}
    <HwModField
      sectionId="motion" fieldId="isCyclic" values={values}
      name="Cyclic"
      description="The axis can move repeatedly through the same area of travel without changing direction"
      input={{ type: 'toggle' }}
      onUpdate={isCyclic => {
        actions.updateValues([
          { sectionId, fieldId: 'isCyclic', value: isCyclic },
          { sectionId, fieldId: 'cyclicDist', value: isCyclic ? 'revert' : 0 },
        ]);
      }}
      docs={{ html: cyclicDocs, type: 'custom' }}/>
    <SettingField {...{ values, sectionId, fieldId: 'cyclicDist' }} locked={!cyclic}/>
    <HwModField
      sectionId="motion" fieldId="isInfinite" values={values}
      name={settingsMetadata['limit.range.mode']?.humanName ?? UNDEFINED_NAME}
      settingName="limit.range.mode"
      description={settingsData['limit.range.mode'].description}
      onUpdate={isInfinite => {
        if (isInfinite) {
          actions.updateValues([
            { sectionId, fieldId: 'isInfinite', value: isInfinite },
            { sectionId, fieldId: 'limitMin', value: -1000000000 },
            { sectionId, fieldId: 'limitMax', value: 1000000000 },
          ]);
        } else {
          actions.updateValues([
            { sectionId, fieldId: 'isInfinite', value: isInfinite },
            { sectionId, fieldId: 'limitMin', value: 'revert' },
            { sectionId, fieldId: 'limitMax', value: 'revert' },
          ]);
        }
      }}
      docs={{ type: 'setting', setting: 'limit.range.mode' }}
      input={{ type: 'enum', options: settingsData['limit.range.mode'].options }}
    />
    <SettingField {...{ values, sectionId, fieldId: 'limitMin' }} locked={isInfinite?.is === 1}/>
    <SettingField {...{ values, sectionId, fieldId: 'limitMax' }} locked={isInfinite?.is === 1}/>
  </>;
};

export const motionTemplate: HwModSectionTemplate<'motion'> = {
  sectionId: 'motion',
  titleName: 'Motion',
  description: 'General information about product motion.',
  generateContent: values => (<MotionSection values={values}/>),
};
