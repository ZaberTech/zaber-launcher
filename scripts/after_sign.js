const { notarize } = require('@electron/notarize');
const { shouldSign } = require('./common');

exports.default = async function afterSign(context) {
  const { electronPlatformName, appOutDir } = context;
  if (electronPlatformName !== 'darwin' || !shouldSign) {
    return;
  }

  const { productFilename } = context.packager.appInfo;

  return await notarize({
    tool: 'notarytool',
    appPath: `${appOutDir}/${productFilename}.app`,
    appleId: process.env.APPLEID,
    appleIdPassword: process.env.APPLEIDPASS,
    teamId: process.env.APPLETEAMID,
  });
};
