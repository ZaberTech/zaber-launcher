import { actionBuilder } from '../utils';
import type { EntityKey } from '../keys';

import type { AxisMonitorInfo, MonitoringError, SupportedPeripheral } from './types';

export enum ActionTypes {
  DID_MOUNT = 'PERIPHERALS_DID_MOUNT',
  WILL_UNMOUNT = 'PERIPHERALS_WILL_UNMOUNT',
  STOP_MONITORING = 'PERIPHERALS_STOP_MONITORING',
  AXIS_MONITOR_DATA = 'PERIPHERALS_AXIS_MONITOR_DATA',
  REFRESH_AXES = 'PERIPHERALS_REFRESH_AXES',
  SET_PERIPHERAL_ID = 'PERIPHERALS_SET_PERIPHERAL_ID',
  ACTIVATE = 'PERIPHERALS_ACTIVATE',
  AXIS_COMMAND_DONE = 'PERIPHERALS_AXIS_COMMAND_DONE',
  FETCH_SUPPORTED_PERIPHERALS = 'PERIPHERALS_FETCH_SUPPORTED_PERIPHERALS',
  FETCH_SUPPORTED_PERIPHERALS_DONE = 'PERIPHERALS_FETCH_SUPPORTED_PERIPHERALS_DONE',
}

export interface ActionsToPayloads {
  [ActionTypes.DID_MOUNT]: { axisKey: EntityKey };
  [ActionTypes.WILL_UNMOUNT]: { axisKey: EntityKey };
  [ActionTypes.STOP_MONITORING]: void;
  [ActionTypes.AXIS_MONITOR_DATA]: { axisKey: EntityKey; info?: AxisMonitorInfo; error?: MonitoringError };
  [ActionTypes.REFRESH_AXES]: void;
  [ActionTypes.SET_PERIPHERAL_ID]: { axisKey: EntityKey; peripheralId: number };
  [ActionTypes.ACTIVATE]: { axisKey: EntityKey };
  [ActionTypes.AXIS_COMMAND_DONE]: { axisKey: EntityKey; error?: string };
  [ActionTypes.FETCH_SUPPORTED_PERIPHERALS]: { axisKey: EntityKey };
  [ActionTypes.FETCH_SUPPORTED_PERIPHERALS_DONE]: { axisKey: EntityKey; peripherals?: SupportedPeripheral[]; error?: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  axisMonitorData: (axisKey: EntityKey, info: AxisMonitorInfo) => buildAction(ActionTypes.AXIS_MONITOR_DATA, { axisKey, info }),
  axisMonitorDataErr: (axisKey: EntityKey, error: MonitoringError) => buildAction(ActionTypes.AXIS_MONITOR_DATA, { axisKey, error }),
  refreshAxes: () => buildAction(ActionTypes.REFRESH_AXES),
  stopMonitoring: () => buildAction(ActionTypes.STOP_MONITORING),
  didMount: (axisKey: EntityKey) => buildAction(ActionTypes.DID_MOUNT, { axisKey }),
  willUnmount: (axisKey: EntityKey) => buildAction(ActionTypes.WILL_UNMOUNT, { axisKey }),
  activate: (axisKey: EntityKey) => buildAction(ActionTypes.ACTIVATE, { axisKey }),
  setPeripheralId: (axisKey: EntityKey, peripheralId: number) => buildAction(ActionTypes.SET_PERIPHERAL_ID, { axisKey, peripheralId }),
  axisCommandDone: (axisKey: EntityKey, error?: string) => buildAction(ActionTypes.AXIS_COMMAND_DONE, { axisKey, error }),
  fetchSupportedPeripherals: (axisKey: EntityKey) => buildAction(ActionTypes.FETCH_SUPPORTED_PERIPHERALS, { axisKey }),
  fetchSupportedPeripheralsDoneErr: (axisKey: EntityKey, error: string) =>
    buildAction(ActionTypes.FETCH_SUPPORTED_PERIPHERALS_DONE, { axisKey, error }),
  fetchSupportedPeripheralsDone: (axisKey: EntityKey, peripherals: SupportedPeripheral[]) =>
    buildAction(ActionTypes.FETCH_SUPPORTED_PERIPHERALS_DONE, { axisKey, peripherals }),
};
