import { ascii } from '@zaber/motion';

import type { SerialPortConfig, RawTcpConfig } from '../app_components';
import { isMobile, hasCapability, Capabilities } from '../devices';
import { blankToNull } from '../utils';

import type { AxisInfo, DeviceInfo, DeviceInfoWithAxes, IdentifiedAxisInfo, IdentifiedDeviceInfo } from './types';

const CLOUD_URL_PREFIX = 'iot://';
const LOCAL_SHARE_URL_PREFIX = 'local://';

export function connectionName(connection: { config: (SerialPortConfig | RawTcpConfig) & { name?: string } }): string {
  return connection.config.name || originalConnectionName(connection);
}


export function originalConnectionName(connection: { config: (SerialPortConfig | RawTcpConfig) }): string {
  const config = (connection.config as SerialPortConfig & RawTcpConfig);
  return config.serialPort ?? `${config.hostname}:${config.tcpPort}`;
}

export function peripheralNameWithLabel(axis: IdentifiedAxisInfo): string {
  if (axis.label) {
    return `${axis.label} (${axis.identity.peripheralName})`;
  } else {
    return axis.identity.peripheralName;
  }
}

export function peripheralLabelOrName(axis: IdentifiedAxisInfo): string | null {
  return axis.label ?? blankToNull(axis.identity.peripheralName) ?? null;
}

export function deviceNameWithLabel(device: Omit<DeviceInfo, 'axes'>): string {
  if (device.identity != null) {
    if (device.label) {
      return `${device.label} (${device.identity.name})`;
    } else {
      return device.identity.name;
    }
  }
  return device.label ?? 'Unidentified Device';
}

export function deviceLabelOrName(device: Omit<DeviceInfo, 'axes'>): string {
  return device.label ?? device.identity?.name ?? 'Unidentified Device';
}

export function axisDescription(axis: AxisInfo, device: DeviceInfo): string {
  if (!isAxisIdentified(axis)) {
    if (axis.nonIdentifiedInfo?.peripheralId != null) {
      return `Non-identified peripheral with Peripheral ID ${axis.nonIdentifiedInfo?.peripheralId}`;
    } else {
      return 'Non-identified axis';
    }
  } else if (axis.identity.peripheralId === 0) {
    return 'Unused axis';
  } else if (hasCapability(device, Capabilities.Controller)) {
    if (isMobile(axis)) {
      switch (axis.identity.axisType) {
        case ascii.AxisType.LINEAR:
          return 'Linear stage peripheral';
        case ascii.AxisType.ROTARY:
          return 'Rotary stage peripheral';
      }
    } else if (axis.identity.peripheralId === 0) {
      return 'Unused Axis';
    } else if (hasCapability(axis, Capabilities.Lamp)) {
      return 'Lamp peripheral';
    }
    return 'Generic peripheral';
  } else {
    if (isMobile(axis)) {
      switch (axis.identity.axisType) {
        case ascii.AxisType.LINEAR:
          return 'Linear stage axis';
        case ascii.AxisType.ROTARY:
          return 'Rotary stage axis';
      }
    }
    return 'Generic axis';
  }
}

export function deviceDescription(device: DeviceInfoWithAxes): string {
  if (!isIdentified(device)) {
    return `Non-identified device with Device ID ${device.nonIdentifiedInfo!.deviceId}`;
  } else if (hasCapability(device, Capabilities.Controller)) {
    return `${device.axisCount}-axis controller`;
  } else if (device.isMultiAxis) {
    return `${device.axisCount}-axis integrated device`;
  } else if (device.axes.length > 0 && isMobile(device.axes[0])) {
    switch (device.axes[0].identity.axisType) {
      case ascii.AxisType.LINEAR:
        return 'Linear stage integrated device';
      case ascii.AxisType.ROTARY:
        return 'Rotary stage integrated device';
    }
  }
  return 'Input or other non-moving device';
}

export function makeCloudUrl(realm: string, cloudId: string): string {
  return `${CLOUD_URL_PREFIX}${realm}/${cloudId}`;
}

export function parseCloudUrl(cloudUrl: string): { cloudId: string; realm: string } | null {
  if (!cloudUrl.startsWith(CLOUD_URL_PREFIX)) {
    return null;
  }
  const [realm, cloudId] =  cloudUrl.substring(CLOUD_URL_PREFIX.length).split('/');
  return { realm, cloudId };
}

export function isCloudUrl(cloudUrl: string) {
  return cloudUrl.startsWith(CLOUD_URL_PREFIX);
}

export function makeLocalShareUrl(hostname: string, port: number): string {
  return `${LOCAL_SHARE_URL_PREFIX}${hostname}:${port}`;
}

export function parseLocalShareUrl(url: string): { hostname: string; port: number } | null {
  if (!url.startsWith(LOCAL_SHARE_URL_PREFIX)) {
    return null;
  }
  const lastColonIndex = url.lastIndexOf(':');
  const hostname = url.substring(LOCAL_SHARE_URL_PREFIX.length, lastColonIndex);
  const port = Number(url.substring(lastColonIndex + 1));
  return {
    hostname,
    port
  };
}

export function isIdentified(device: DeviceInfoWithAxes | null | undefined): device is IdentifiedDeviceInfo {
  return device?.identity != null;
}
export function isAxisIdentified(axis: AxisInfo | null | undefined): axis is IdentifiedAxisInfo {
  return axis?.identity != null;
}
export function isInternalDevice(device: DeviceInfo) {
  return (device.identity ?? device.nonIdentifiedInfo)?.firmwareVersion?.minor === 99;
}

export const BOSCH_REXROTH_SUPPORTED_DEVICES = /^(BR-X|SHB|SHL)/;
