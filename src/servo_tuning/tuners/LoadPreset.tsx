import React, { useEffect, useState } from 'react';
import { Button, Text, SimpleSelect } from '@zaber/react-library';
import type { ServoTuningParamset } from '@zaber/motion/ascii';
import { useSelector } from 'react-redux';

import type { ChosenParamset } from '../types';
import { actionDefinitions } from '../actions';
import { useActions } from '../../utils';
import { paramsetNames, userParamsets } from '../names';
import { selectAdvancedTuningInfo } from '../selectors';
import { ParamTable } from '../components/ParamTable';
import { NoContentMessage } from '../../components';

export const LoadPreset: React.FC<ChosenParamset> = ({ entity, paramset }) => {
  const actions = useActions(actionDefinitions);
  const info = useSelector(selectAdvancedTuningInfo);
  const [selection, setSelection] = useState<ServoTuningParamset | undefined>(undefined);
  useEffect(() => {
    if (selection != null) {
      if (info?.method !== 'advanced' || info.paramset !== selection) {
        actions.readTuningParams({ entity, paramset: selection, method: 'advanced' });
      }
    }
  }, [info?.method, info?.paramset, selection]);

  const otherParamsets = userParamsets.filter(p => p !== paramset).map(p => ({ value: p, label: paramsetNames[p] }));

  return <div className="load">
    <Text>
      Select a preset to load into {paramsetNames[paramset]}:
    </Text>
    <div className="controls">
      <SimpleSelect value={selection} options={otherParamsets} onValueChange={value => setSelection(value)} data-testid="paramset-to-load"/>
      <div className="pad"/>
      <Button onClick={selection ? () => actions.loadParamset(entity, paramset, selection) : undefined} disabled={selection == null}>
        Load
      </Button>
    </div>
    {
      selection == null ? null :
      info?.tuning == null ? <NoContentMessage message="Loading..." type="working"/> :
      <ParamTable params={info?.tuning}/>
    }
  </div>;
};
