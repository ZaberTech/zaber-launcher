import { ContextMenu, Flex, HeaderCard, Icons, Modal, Text } from '@zaber/react-library';
import classnames from 'classnames';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import type { EntityKey } from '../../keys';
import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import { selectIoExpanded } from '../selectors';
import type { IoInfo } from '../types';

import { IoSection } from './IoSection';
import { EditLabel } from './EditLabel';


type IoCardProps = { deviceKey: EntityKey; info: IoInfo; type: 'device' | 'controller' };

export const IoCard: React.FC<IoCardProps> = ({ deviceKey, info }) => {
  const actions = useActions(actionDefinitions);
  const stateExpanded = useSelector(selectIoExpanded)[deviceKey];
  const expanded = stateExpanded ?? false;
  const [editLabelsOpen, setEditLabelsOpen] = useState(false);

  return <>
    <Flex.Row>
      <HeaderCard className={classnames('io-card', 'subsection')}
        edge="outline"
        collapsible expanded={expanded}
        onToggle={() => actions.setIoExpanded(deviceKey, !expanded)}
        titleExpanded="Collapse Io Section"
        titleCollapsed="Expand Io Section"
        header={<>
          <Text t={Text.Type.H5}>Device I/O</Text>
          <Flex.Spacer/>
          <ContextMenu align="end">
            <ContextMenu.Item icon={<Icons.Edit/>} title="Edit Labels" onClick={() => setEditLabelsOpen(true)}>
              Edit Labels
            </ContextMenu.Item>
          </ContextMenu>
        </>}>
        <IoSection deviceKey={deviceKey} info={info}/>
      </HeaderCard>
    </Flex.Row>
    {editLabelsOpen && (
      <Modal headerIcon={<Icons.Edit/>} headerText="Edit Labels" isOpen={editLabelsOpen} onRequestClose={() => setEditLabelsOpen(false)}>
        <EditLabel onClose={() => setEditLabelsOpen(false)} deviceKey={deviceKey} info={info}/>
      </Modal>
    )}
  </>;
};
