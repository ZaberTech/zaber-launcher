import { Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import React from 'react';
import { useSelector } from 'react-redux';

import { NoContentMessage, Title } from '../components';
import { ConnectionsView, AttentionRequests } from '../connection_manager';

import { actions as actionsDefinition } from './actions';
import { selectCurrentAxisOrDevice, selectIsCurrentSupported, selectNeedSetupKeys } from './selectors';
import { Setup } from './Setup';
import { DEVICE_NAME, LC40_TITLE } from './types';


const noSelection = <NoContentMessage title={LC40_TITLE} message="Select an axis or device to setup."/>;
const controllerSelection = <NoContentMessage
  title={LC40_TITLE} type="info"
  message="You have selected a controller. Please select its axis or an integrated device."/>;
const unsupportedSelection = <NoContentMessage
  title={LC40_TITLE} type="info"
  message="The device or axis is not supported by this application."/>;
const attentionIcon = <div className="attention" title={`${DEVICE_NAME} Device, Setup Required`}>
  <Icons.Lc40WithNotification/>
</div>;

const Content: React.FC = () => {
  const deviceOrAxis = useSelector(selectCurrentAxisOrDevice);
  const isSupported = useSelector(selectIsCurrentSupported);

  if (deviceOrAxis?.selectedKey == null) {
    return noSelection;
  } else if (deviceOrAxis.axis == null && deviceOrAxis.device.isController) {
    return controllerSelection;
  } else if (!isSupported) {
    return unsupportedSelection;
  }
  return <Setup entityKey={deviceOrAxis.selectedKey}/>;
};

export const LC40Setup: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const deviceOrAxis = useSelector(selectCurrentAxisOrDevice);
  const requestAttention = useSelector(selectNeedSetupKeys);
  const attentionRequests: AttentionRequests = {
    requestAttention,
    showIcon: attentionIcon,
  };

  return (
    <div className="connection-view-and-app lc40-setup-root">
      <Title>{`${DEVICE_NAME} Setup`}</Title>
      <ConnectionsView
        selectable={['device', 'axis']}
        selected={deviceOrAxis?.selectedKey ?? null}
        onSelect={actions.selectDeviceOrAxis}
        attentionRequests={attentionRequests}
      />
      <div className="app-ui lc40-setup">
        <Content/>
      </div>
    </div>
  );
};
