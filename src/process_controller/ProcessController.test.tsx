let settings: {
  allProcesses: Record<string, number[]>;
  process: Record<string, number>;
};

let processModes: Record<number, number>;
let processOn: Set<number>;
let processBridges: Set<number>;


let processMocks: ProcessMock[];
class ProcessMock {
  constructor(public readonly controller: ProcessControllerMock, private readonly processNumber: number) {}

  settings = {
    get: jest.fn(async (setting: string) => {
      if (settings.process[setting] != null) {
        return settings.process[setting];
      }
      return 0;
    }),
    set: jest.fn((setting: string, value: number) => settings.process[setting] = value),
  };

  getMode = jest.fn(() => processModes[this.processNumber]);
  setMode = jest.fn((mode: number) => { processModes[this.processNumber] = mode });
  getSource = jest.fn().mockResolvedValue({ sensor: 10, port: 1 } as ProcessControllerSource);
  setSource = jest.fn();
  on = jest.fn(async () => processOn.add(this.processNumber));
  off = jest.fn(async () => processOn.delete(this.processNumber));
  bridge = jest.fn(async () => processBridges.add(this.processNumber));
  unbridge = jest.fn(async () => processBridges.delete(this.processNumber));
  getInput = jest.fn(async () => ({ value: temperature1, unit: '' }));
  enable = jest.fn();
}

const getProcessDefault = (controller: ProcessControllerMock, processNumber: number) => {
  if (processMocks[processNumber] == null) {
    processMocks[processNumber] = new ProcessMock(controller, processNumber);
  }
  return processMocks[processNumber];
};

class ProcessControllerMock {
  private getProcessDefault = getProcessDefault.bind(null, this);

  constructor(public readonly device: DeviceMock) {
    if (device.processControllerException) {
      throw device.processControllerException;
    }

    if (device.processGetter) {
      this.getProcess = device.processGetter.bind(null, this);
    }
  }

  getProcess = this.getProcessDefault;
}

jest.mock('@zaber/motion/product', () => ({
  ...jest.requireActual('@zaber/motion/product'),
  ProcessController: ProcessControllerMock,
  Process: ProcessMock,
}));

import { fireEvent, render, RenderResult, within } from '@testing-library/react';
import { Container, injectable } from 'inversify';
import _ from 'lodash';
import React from 'react';
import { MotionLibException, RelativeTemperature, Units, Voltage } from '@zaber/motion';
import { WarningFlags } from '@zaber/motion/ascii';
import {
  ProcessControllerMode,
  ProcessControllerSource,
  ProcessControllerSourceSensor as SourceSensor
} from '@zaber/motion/product';
import 'jest-canvas-mock';
import type { SagaIterator } from 'redux-saga';
import { match, P } from 'ts-pattern';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { mockStorage, StorageMock, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase
} from '../test/mocks/ascii';
import { selectDevices } from '../connection_manager';
import { createContainer, destroyContainer } from '../container';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import { mockSingleDevice } from '../connection_manager/mocks';
import { getValueInputByTitle, setValueInputByTitle, UnitInfoServiceMockBase, Wrapper } from '../units/test_utils';
import { UnitFrom, UnitInfo, unsupportedUnitInfo } from '../units';
import { UnitInfoService } from '../units/service';
import { selectProcessController } from '../store';
import { DRIVER_DISABLED_FLAGS } from '../protocol_manual';

import { App } from './App';
import { POLL_DELAY_MS } from './constants';
import { actionDefinitions as actions } from './actions';

const LONG_TIMEOUT = 2500;

let wrapper: RenderResult;
let container: Container;
let storageMock: StorageMock;


class AxisMock extends AxisMockBase {
  warnings = {
    getFlags: jest.fn().mockResolvedValue(new Set())
  };
}


let temperature1: number;
let temperature2: number;
class DeviceMock extends DeviceMockBase<AxisMock> {
  getAxis = jest.fn((axisNumber: number): AxisMock => new AxisMock(axisNumber, this));
  settings = {
    get: jest.fn(async (setting: string) => {
      if (setting === 'sensor.temperature.1') {
        return temperature1;
      } else if (setting === 'sensor.temperature.2') {
        return temperature2;
      }
      return 0;
    }),
    getFromAllAxes: jest.fn(async (setting: string): Promise<number[]> => {
      if (setting === 'process.state') {
        return [1, 2, 3, 4].map(process => processOn.has(process) ? 1 : 0);
      }
      if (setting === 'driver.bipolar') {
        return [1, 2, 3, 4].map(process => processBridges.has(process) ? 1 : 0);
      }
      if (settings.allProcesses[setting] != null) {
        return settings.allProcesses[setting];
      }
      return [0, 0, 0, 0];
    }),
    set: jest.fn(),
  };
  io = {
    getAllAnalogInputs: jest.fn().mockResolvedValue([6, 12]),
    getAllDigitalInputs: jest.fn().mockResolvedValue([0, 0, 0, 0]),
    getAllDigitalOutputs: jest.fn().mockResolvedValue([0, 0]),
  };
  warnings = {
    getFlags: jest.fn().mockResolvedValue(new Set())
  };
  processControllerException?: MotionLibException;
  processGetter?: (controller: ProcessControllerMock, processNumber: number) => ProcessMock;
}

class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

@injectable()
class UnitInfoServiceMock extends UnitInfoServiceMockBase {
  public* getUnitInfo(from: UnitFrom): SagaIterator<UnitInfo> {
    return match(from)
      .with({ setting: P.union('process.control.voltage.max', 'process.voltage.on', 'process.control.hysteresis.voltage')  }, () => (
        this.mockUnitInfo('Voltage', 1, 3)
      ))
      .with({ setting: 'process.control.hysteresis.temperature' }, () => this.mockUnitInfo('Relative Temperature', 1, 3))
      .otherwise(() => unsupportedUnitInfo());
  }
}

const TestProcessControllerApp = wrapWithNewStore(wrapWithRouter(App));

const AWAIT_POLL_TIME = 2 * POLL_DELAY_MS;

const checkInputOfLabel = (label: string, expectedValue: number | string) => {
  const input = wrapper.getByLabelText(label) as HTMLInputElement;
  expect(input.value).toBe(expectedValue.toString());
};

const selectDevice = () => {
  const store = TestProcessControllerApp.testStore;
  mockSingleDevice(store);
  const device = _.sample(selectDevices(TestProcessControllerApp.testStore.getState()))!;
  connectionViewMockInstance.props.onSelect(device.key);
};

beforeEach(async () => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(UnitInfoService).to(UnitInfoServiceMock);
  storageMock = mockStorage(container);

  temperature1 = 20;
  temperature2 = 30;
  settings = {
    allProcesses: {
      'process.voltage': [5, 6, 7, 8],
    },
    process: {
      'process.control.setpoint.voltage': 1,
      'process.control.setpoint.temperature': 2,
      'process.voltage.tf': 3,
      'process.voltage.on': 4,
      'process.voltage.start': 5,
      'process.voltage.start.duration': 6,
      'process.control.voltage.max': 7,
      'process.control.voltage.min': 8,
      'process.control.setpoint.tf': 9,
      'process.control.hysteresis.temperature': 10,
      'process.control.hysteresis.voltage': 10,
      'process.pid.kp': 11,
      'process.pid.ki': 12,
      'process.pid.kd': 13,
      'process.pid.offset': 14,
    },
  };
  processModes = {
    1: ProcessControllerMode.MANUAL,
    2: ProcessControllerMode.MANUAL,
    3: ProcessControllerMode.MANUAL,
    4: ProcessControllerMode.MANUAL,
  };
  processOn = new Set();
  processBridges = new Set();

  processMocks = [];

  wrapper = render(<TestProcessControllerApp/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
  storageMock.stored = {};
});

describe('Process Controller App', () => {
  beforeEach(() => {
    wrapper = render(<TestProcessControllerApp/>);
  });

  describe('SCA4', () => {
    beforeEach(async () => {
      selectDevice();
      await waitUntilPass(() => wrapper.getByText('X-SCA4 Driver'), LONG_TIMEOUT);
    });

    test('Dashboard', async () => {
      checkInputOfLabel('TC1 Measured', 20);
      checkInputOfLabel('TC2 Measured', 30);
      checkInputOfLabel('AI1 Measured', 6);
      checkInputOfLabel('AI2 Measured', 12);

      fireEvent.click(wrapper.getByTitle('Turn Channel 3 on'));
      await waitUntilPass(() => wrapper.getByTitle('Turn Channel 3 off'), AWAIT_POLL_TIME);
    });

    describe('Process', () => {
      beforeEach(async () => {
        const channelSelect = within(wrapper.container.querySelector('.process-controller .minor-menu')!);
        fireEvent.click(channelSelect.getByText(/Channel\s*1/));
        await waitUntilPass(() => wrapper.getByText('Control Type'));
      });

      test('Manual Mode', async () => {
        await waitUntilPass(() => {
          checkInputOfLabel('Control Type', ProcessControllerMode.MANUAL);
          checkInputOfLabel('Output Filter Time Constant', settings.process['process.voltage.tf']);
          checkInputOfLabel('Steady Voltage', settings.process['process.voltage.on']);
          checkInputOfLabel('Initial Pulse Voltage', settings.process['process.voltage.start']);
          checkInputOfLabel('Voltage Delay', settings.process['process.voltage.start.duration']);
        }, LONG_TIMEOUT);
      });

      test('On/Off Mode', async () => {
        fireEvent.change(wrapper.getByLabelText('Control Type'), { target: { value: `${ProcessControllerMode.ON_OFF}` } });
        await waitUntilPass(() => {
          checkInputOfLabel('Output Filter Time Constant', settings.process['process.voltage.tf']);
          checkInputOfLabel('On Voltage', settings.process['process.control.voltage.max']);
          checkInputOfLabel('Off Voltage', settings.process['process.control.voltage.min']);
          checkInputOfLabel('Setpoint Filter Time Constant', settings.process['process.control.setpoint.tf']);
          checkInputOfLabel('Hysteresis', settings.process['process.control.hysteresis.temperature']);
        }, LONG_TIMEOUT);
      });

      test('PID Mode', async () => {
        fireEvent.change(wrapper.getByLabelText('Control Type'), { target: { value: `${ProcessControllerMode.PID}` } });
        await waitUntilPass(() => {
          checkInputOfLabel('Output Filter Time Constant', settings.process['process.voltage.tf']);
          checkInputOfLabel('Maximum Voltage', settings.process['process.control.voltage.max']);
          checkInputOfLabel('Minimum Voltage', settings.process['process.control.voltage.min']);
          checkInputOfLabel('Setpoint Filter Time Constant', settings.process['process.control.setpoint.tf']);
          checkInputOfLabel('Kp', settings.process['process.pid.kp']);
          checkInputOfLabel('Ki', settings.process['process.pid.ki']);
          checkInputOfLabel('Kd', settings.process['process.pid.kd']);
          checkInputOfLabel('Offset', settings.process['process.pid.offset']);
        }, LONG_TIMEOUT);
      });

      test('Update Setting', async () => {
        const newValue = 24;
        fireEvent.click(wrapper.getByTitle('Edit process.voltage.on'));
        await waitUntilPass(() => fireEvent.change(wrapper.getByLabelText('Steady Voltage'), { target: { value: newValue } }));
        await waitUntilPass(() => fireEvent.click(wrapper.getByTitle('Confirm process.voltage.on edit')));
        await waitUntilPass(() => wrapper.getByTitle('Edit process.voltage.on'));
        checkInputOfLabel('Steady Voltage', newValue);
      }, LONG_TIMEOUT);

      test('Update Input Source', async () => {
        await waitUntilPass(() => wrapper.getByText('Thermistor 1'));
        TestProcessControllerApp.testStore.dispatch(actions.updateSource(1, { sensor: SourceSensor.ANALOG_INPUT, port: 2 }));
        await waitUntilPass(() => wrapper.getByText('Analog Input 2'));
      });
    });

    describe('Bridging', () => {
      test('Bridging Processes', async () => {
        fireEvent.click(wrapper.getByTitle('Bridge process 1 and 2'));
        await waitUntilPass(() => expect(wrapper.queryAllByText('Bridge 1 + 2').length).toBe(2));
        await waitUntilPass(() => expect(wrapper.queryByText('Channel 1')).toBeNull());
        await waitUntilPass(() => fireEvent.click(wrapper.getByTitle('Unbridge process 1 and 2')), AWAIT_POLL_TIME);
        await waitUntilPass(() => expect(wrapper.queryByText('Bridge 1 + 2')).toBeNull());
        await waitUntilPass(() => expect(wrapper.queryAllByText('Channel 1').length).toBe(2));
      }, LONG_TIMEOUT);
    });

    describe('Tuning Assistant', () => {
      const PROCESS_NUMBER = 1;
      let section: Wrapper;

      beforeEach(async () => {
        const channelSelect = within(wrapper.container.querySelector('.process-controller .minor-menu')!);
        fireEvent.click(channelSelect.getByText(new RegExp(`Channel\\s*${PROCESS_NUMBER}`)));
        await waitUntilPass(() => wrapper.getByText('Control Type'));
        fireEvent.change(wrapper.getByLabelText('Control Type'), { target: { value: `${ProcessControllerMode.PID_HEATER}` } });
        await waitUntilPass(() => wrapper.getByText('Tuning Assistant'));
        section = within(wrapper.getByTestId('auto-tune-section'));
      });

      const fetchedInitialSettings = () => {
        const mock = processMocks[PROCESS_NUMBER];
        expect(mock.settings.get).toHaveBeenCalledWith('process.voltage.on', '');
        expect(mock.settings.get).toHaveBeenCalledWith('process.pid.kp', '');
        expect(mock.settings.get).toHaveBeenCalledWith('process.pid.ki', '');
        expect(mock.settings.get).toHaveBeenCalledWith('process.pid.kd', '');
      };

      const switchedToManualMode = () => {
        expect(processModes[PROCESS_NUMBER]).toBe(ProcessControllerMode.MANUAL);
        expect(processOn.has(PROCESS_NUMBER)).toBeTruthy();
      };

      test('Setup', async () => {
        fireEvent.click(section.getByText('Tuning Assistant'));
        await waitUntilPass(fetchedInitialSettings, LONG_TIMEOUT);
        await waitUntilPass(switchedToManualMode);
        await waitUntilPass(() => section.getByText(/Set the maximum voltage and a setpoint/));
      });

      const setField = async (title: string, value: number, setting?: string, units?: Units) => {
        await setValueInputByTitle(section, title, value, true);
        await waitUntilPass(() => {
          expect(processMocks[PROCESS_NUMBER].settings.set).toHaveBeenLastCalledWith(setting ?? title, value, units ?? '');
        });
      };

      test('Identify and Test', async () => {
        fireEvent.click(section.getByText('Tuning Assistant'));
        await waitUntilPass(() => section.getByTitle('Go To Identify Step'));
        await setValueInputByTitle(section, 'Maximum Voltage', 10, true);
        await setField('process.control.setpoint.temperature', 100);
        await setField('Stable Voltage', 5, 'process.voltage.on', Voltage.V);
        temperature1 = 102;
        fireEvent.click(section.getByTitle('Go To Identify Step'));
        await waitUntilPass(() => getValueInputByTitle(section, 'Hysteresis'));
        expect(processMocks[PROCESS_NUMBER].off).toHaveBeenCalled();
        await setField('Hysteresis', 1, 'process.control.hysteresis.temperature', RelativeTemperature['°C']);
        fireEvent.click(section.getByText(/Start/));
        await waitUntilPass(() => section.getByText('Preparing to identify'), LONG_TIMEOUT);
        expect(processMocks[PROCESS_NUMBER].on).toHaveBeenCalled();
        expect(processMocks[PROCESS_NUMBER].settings.set).toHaveBeenLastCalledWith('process.voltage.on', 0);
        for (let i = 0; i < 4; i++) {
          temperature1 = 98;
          await waitUntilPass(() => {
            expect(processMocks[PROCESS_NUMBER].settings.set).toHaveBeenLastCalledWith('process.voltage.on', 7.0710678118654755);
            section.getByText('System Heating');
          });
          temperature1 = 102;
          await waitUntilPass(() => {
            expect(processMocks[PROCESS_NUMBER].settings.set).toHaveBeenLastCalledWith('process.voltage.on', 0);
            section.getByText('System Cooling');
          });
        }
        temperature1 = 98;
        await waitUntilPass(() => section.getByText('System identification is complete.'));

        fireEvent.click(section.getByTitle('Go To Test Step'));
        processMocks[PROCESS_NUMBER].on.mockClear();
        processMocks[PROCESS_NUMBER].setMode.mockClear();
        await waitUntilPass(() => fireEvent.click(section.getByTitle('Test Identified Parameters')));
        await waitUntilPass(() => {
          expect(processMocks[PROCESS_NUMBER].on).toHaveBeenCalled();
          expect(processMocks[PROCESS_NUMBER].setMode).toHaveBeenCalledWith(ProcessControllerMode.PID_HEATER);
        });
      });
    });
  });
});

describe('Process Controller Errors', () => {
  let asciiDevice: DeviceMock;

  beforeEach(async () => {
    const routerServiceMock = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;
    const connection = await routerServiceMock.getAsciiConnection(LOCAL_ROUTER_URL, 'COM1');
    asciiDevice = connection.getDevice(1);
    connection.getDevice = jest.fn().mockReturnValue(asciiDevice);
    routerServiceMock.getAsciiConnection = jest.fn().mockReturnValue(connection);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('Not Enough Analog Inputs', async () => {
    asciiDevice.io.getAllAnalogInputs.mockResolvedValue([]);

    selectDevice();

    await waitUntilPass(() => wrapper.getByText(/This app expects a process controller with 2 analog inputs.*/));
  });

  test('Not Enough Volts Out', async () => {
    settings.allProcesses['process.voltage'] = [];

    selectDevice();

    await waitUntilPass(() => wrapper.getByText(/This app expects a process controller with 4 outputs.*/));
  });

  test('Trigger Non-PC Identity', async () => {
    asciiDevice.processControllerException = new MotionLibException('Test exception.');

    selectDevice();

    await waitUntilPass(() => wrapper.getByText(/This is not a Process Controller/));
  });

  test('Process Power Error', async () => {
    asciiDevice.processGetter = (controller: ProcessControllerMock, processNumber: number) => {
      const ret = new ProcessMock(controller, processNumber);
      ret.on = jest.fn().mockRejectedValue(new MotionLibException('Test exception.'));
      return ret;
    };

    selectDevice();

    await waitUntilPass(() => wrapper.getByText('X-SCA4 Driver'), LONG_TIMEOUT);
    TestProcessControllerApp.testStore.dispatch(actions.processPower(1, true));
    await waitUntilPass(() => expect(selectProcessController(TestProcessControllerApp.testStore.getState())?.processes[1]?.error).toEqual({
      message: 'Test exception.',
      title: 'There was an error turning this process on',
    }));
  });

  test('Refresh Process Error', async () => {
    asciiDevice.processGetter = (controller: ProcessControllerMock, processNumber: number) => {
      const ret = new ProcessMock(controller, processNumber);
      ret.getMode = jest.fn().mockRejectedValue(new MotionLibException('Test exception.'));
      ret.getSource = jest.fn().mockRejectedValue(new MotionLibException('Test exception.'));
      return ret;
    };

    selectDevice();

    await waitUntilPass(() => wrapper.getByText(/There was an error.*/), LONG_TIMEOUT);
  });

  test('Display Faults', async () => {
    asciiDevice.warnings.getFlags = jest.fn().mockResolvedValue(new Set([WarningFlags.CRITICAL_SYSTEM_ERROR]));
    const axisMock = new AxisMock(1, asciiDevice);
    axisMock.warnings.getFlags = jest.fn().mockResolvedValue(new Set([WarningFlags.CRITICAL_SYSTEM_ERROR]));
    asciiDevice.getAxis = jest.fn((n: number) => n === 1 ? axisMock : new AxisMock(n, asciiDevice));

    selectDevice();

    await waitUntilPass(() => wrapper.getByText('X-SCA4 Driver'), LONG_TIMEOUT);
    const channelSelect = within(wrapper.container.querySelector('.process-controller .minor-menu')!);
    fireEvent.click(channelSelect.getByText(/Channel\s*1/));

    await waitUntilPass(() => wrapper.getByText('Warning Flag FF'));
  });

  test('Reenable Process Error', async () => {
    asciiDevice.processGetter = (controller: ProcessControllerMock, processNumber: number) => {
      const ret = new ProcessMock(controller, processNumber);
      ret.enable = jest.fn().mockRejectedValue(new MotionLibException('Test exception.'));
      return ret;
    };

    const flag = _.sample(DRIVER_DISABLED_FLAGS)!;
    asciiDevice.warnings.getFlags = jest.fn().mockResolvedValue(new Set([flag]));
    const axisMock = new AxisMock(1, asciiDevice);
    axisMock.warnings.getFlags = jest.fn().mockResolvedValue(new Set([flag]));
    asciiDevice.getAxis = jest.fn((n: number) => n === 1 ? axisMock : new AxisMock(n, asciiDevice));

    selectDevice();

    await waitUntilPass(() => wrapper.getByText('X-SCA4 Driver'), LONG_TIMEOUT);
    const channelSelect = within(wrapper.container.querySelector('.process-controller .minor-menu')!);
    fireEvent.click(channelSelect.getByText(/Channel\s*1/));

    fireEvent.click(wrapper.getByText('Re-enable'));
    await waitUntilPass(() => wrapper.getByText(/There was an error.*/), LONG_TIMEOUT);
  });
});
