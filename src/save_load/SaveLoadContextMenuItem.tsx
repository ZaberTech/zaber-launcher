import { ContextMenu, Icons } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';
import React from 'react';

import type { EntityKey } from '../keys';

import { actions as actionDefinitions } from './actions';

export const SaveLoadContextMenuItem: React.FC<{ productKey: EntityKey; title?: string }> = ({ productKey, title }) => {
  const actions = useActions(actionDefinitions);
  return (
    <ContextMenu.Item onClick={() => actions.begin(productKey)}
      title={title ?? 'Save or Load State'}
      icon={<Icons.SaveLoad/>}>
      Save/Load State
    </ContextMenu.Item>
  );
};
