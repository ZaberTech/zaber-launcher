import { Angle, AngularVelocity, ascii, FirmwareVersion, gcode, Length, Measurement, Units, Velocity } from '@zaber/motion';

import type { LockstepGroup } from '../connection_manager';
import { versionCategory } from '../firmware_upgrade/utility';
import type { EntityKey } from '../keys';
import { compareFirmwareVersions } from '../app_components';

export interface BlockMessage extends gcode.TranslateMessage {
  blockIndex: number;
  blockIndexEnd: number;
}

export interface Settings {
  deviceKey: EntityKey;
  traverseRate: Measurement;
  axes: AxisSettings[];
}

export interface AxisSettings {
  axisNumber: number;
  peripheralId: number;
  mobile: boolean;
  letter: string | null;
  microstepResolution: number;
  axisType: ascii.AxisType;
  lockstep: LockstepGroup | null;
  primaryAxisNumber: number;
  inverted: boolean;
  maxSpeed: Measurement;
}

export enum Paths {
  Root = '/gcode',
  AllSettings = '/gcode/settings',
  TranslatorSettings = '/gcode/settings/translator',
  SelectController = '/gcode/settings/controller',
}

export const GCODE_AXES = ['X', 'Y', 'Z', 'A', 'B', 'C', 'E'];

export function defaultVelocityUnits(axisType: ascii.AxisType) {
  switch (axisType) {
    case ascii.AxisType.LINEAR: return Velocity.MILLIMETRES_PER_SECOND;
    case ascii.AxisType.ROTARY: return AngularVelocity.DEGREES_PER_SECOND;
    default: return Units.NATIVE;
  }
}
export function defaultPositionUnits(axisType: ascii.AxisType) {
  switch (axisType) {
    case ascii.AxisType.LINEAR: return Length.mm;
    case ascii.AxisType.ROTARY: return Angle.DEGREES;
    default: return Units.NATIVE;
  }
}
export function defaultPositionUnitsLabel(axisType: ascii.AxisType) {
  switch (axisType) {
    case ascii.AxisType.LINEAR: return 'mm';
    case ascii.AxisType.ROTARY: return '°';
    default: return '';
  }
}

export type MoveType = 'home' | 'jog-left' | 'jog-right'
| 'step-left' | 'step-right'
| 'step-left-small' | 'step-right-small'
| 'step-left-xsmall' | 'step-right-xsmall';

export const MANUAL_JOG_SCALE = 0.1;

export function sortAxes(axes: AxisSettings[]) {
  return axes.slice().sort((a, b) => GCODE_AXES.indexOf(a.letter!) - GCODE_AXES.indexOf(b.letter!));
}
export function sortMessages(messages: BlockMessage[]) {
  return messages.slice().sort((a, b) => a.blockIndex !== b.blockIndex ? a.blockIndex - b.blockIndex : a.fromBlock - b.fromBlock);
}
export function isPrimaryAxis(axis: AxisSettings): boolean {
  return axis.letter != null && axis.primaryAxisNumber === axis.axisNumber;
}

export function isSupported(version: FirmwareVersion) {
  return compareFirmwareVersions(version, { major: 7, minor: 28, build: 0 }) >= 0 || versionCategory(version) !== 'public';
}
