import React from 'react';
import { useSelector } from 'react-redux';
import { Text } from '@zaber/react-library';

import { selectCurrentFile } from './selectors';

export const Filename: React.FC = () => {
  const fileName = useSelector(selectCurrentFile);

  if (!fileName) {
    return null;
  }

  return <div className="file-name">
    <Text e={Text.Emphasis.Bold}>{fileName}</Text>
  </div>;
};
