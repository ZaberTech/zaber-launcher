import { ipcRenderer } from 'electron';
import { inject, injectable } from 'inversify';
import { filter, lastValueFrom, map, Subject, take } from 'rxjs';

import { IPC } from '../ipc';
import type { Action } from '../utils';

import {
  actions, ActionTypes, ActionsToPayloads, PROCESS_RX_IPC_CHANNEL, PROCESS_TX_IPC_CHANNEL, SpawnOptions, TransmittedData,
} from './common_types';

type ExitAction = Action<ActionsToPayloads[ActionTypes.EXIT]>;

@injectable()
export class Pty {
  private readonly rxSubject = new Subject<TransmittedData>();

  constructor(@inject(IPC) private readonly ipc: IPC) {
    ipcRenderer.on(PROCESS_RX_IPC_CHANNEL, this.onRx.bind(this));
  }

  public getDataSteam(id: string) {
    return this.rxSubject.pipe(filter(rx => rx.id === id), map(rx => rx.data));
  }

  public sendData(id: string, data: string) {
    ipcRenderer.send(PROCESS_TX_IPC_CHANNEL, { id, data });
  }

  private onRx(event: unknown, data: TransmittedData) {
    this.rxSubject.next(data);
  }

  public spawn(options: SpawnOptions): Promise<ExitAction['payload']> {
    const exitObservable = this.ipc.getActions<ExitAction>(ActionTypes.EXIT).pipe(
      filter(a => a.payload.id === options.id),
      take(1),
      map(a => a.payload));
    const exitPromise = lastValueFrom(exitObservable);
    this.ipc.sendActionToMainProcess(actions.spawn(options));
    return exitPromise;
  }

  public kill(id: string) {
    this.ipc.sendActionToMainProcess(actions.kill(id));
  }

  /** Sends CTRL+C like control character. */
  public interrupt(id: string) {
    ipcRenderer.send(PROCESS_TX_IPC_CHANNEL, { id, data: '\x03' });
  }
}
