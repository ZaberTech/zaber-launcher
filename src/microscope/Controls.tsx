import React from 'react';
import { useSelector } from 'react-redux';
import { Loader, NoticeBanner } from '@zaber/react-library';

import { selectMonitorError, selectMonitorState } from './selectors';
import { FilterChanger } from './FilterChanger';
import { ObjectiveChanger } from './ObjectiveChanger';
import { Illuminator } from './Illuminator';
import { Motion } from './Axis';
import { PresetsPanel } from './PresetsPanel';
import { Initialization } from './Initialization';
import { NoConfig } from './NoConfig';
import { RestartInProgress } from './Restart';
import { ConfigSuccess } from './config/ConfigSuccess';
import { Autofocus } from './Autofocus';
import { ValidationErrors } from './ValidationErrors';
import { Trigger } from './Trigger';

const MonitorError: React.FC = () => {
  const error = useSelector(selectMonitorError);
  if (error == null) { return null }
  return <NoticeBanner type="error">
    Loading Error: {error}
  </NoticeBanner>;
};

export const Controls: React.FC = () => {
  const { stableInfo, info } = useSelector(selectMonitorState);

  if (stableInfo == null || info == null) {
    return <>
      <div className="loading"><Loader size="small"/>Loading Components...</div>
      <MonitorError/>
    </>;
  }

  return <>
    <RestartInProgress/>
    <MonitorError/>
    <NoConfig/>
    <ValidationErrors/>
    <ConfigSuccess/>
    <Initialization/>
    <PresetsPanel/>
    <Motion/>
    <Autofocus/>
    <Illuminator/>
    <div className="changers">
      <FilterChanger/>
      <ObjectiveChanger/>
    </div>
    <div className="trigger-row">
      <Trigger/>
    </div>
  </>;
};
