import { combineReducers } from 'redux';

import { createReducer, filterDictionary } from '../utils';
import { EntityKey, extractConnectionKey, isSubKeyOf, getEntityType, EntityKeyType } from '../keys';
import { ConnectionManagerActionTypes, ConnectionManagerActionPayloads } from '../connection_manager';
import { ConnectionStatusActionPayloads, ConnectionStatusActionTypes } from '../connection_status';

import type { MotionAxisInfo, Moveable, ProductInfo } from './types';
import { ActionsToPayloads, ActionTypes } from './actions';
import { basicControlsMovementReducer } from './movement';
import type { ProductError } from './errors/error';
import { basicControlsIoReducer } from './io';

export interface State {
  selectedEntity: EntityKey | null;
  deviceInfo: Partial<Record<EntityKey, ProductInfo>>;
  pinned: EntityKey[];
  ioExpanded: Partial<Record<EntityKey, boolean>>;
  errors: Record<EntityKey, ProductError | null>;
  moveSinModal: Moveable | null;
  travelLimitModal: MotionAxisInfo | null;
}

const initialState: State = {
  selectedEntity: null,
  deviceInfo: {},
  pinned: [],
  ioExpanded: {},
  errors: {},
  moveSinModal: null,
  travelLimitModal: null,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const fetchDeviceInfoDone: Reducer<ActionTypes.FETCH_DEVICE_INFO_DONE> = (state: State, { key, info }): State => ({
  ...state,
  deviceInfo: {
    ...state.deviceInfo,
    [key]: info,
  },
});

const setError: Reducer<ActionTypes.SET_ERROR> = (state: State, { entity, error }): State => ({
  ...state,
  errors: { ...state.errors, [entity]: error },
});

const clearConnection = (state: State, connectionKey: EntityKey): Partial<State> => ({
  deviceInfo: filterDictionary(state.deviceInfo, (_, key) => extractConnectionKey(key) !== connectionKey),
  errors: filterDictionary(state.errors, (_, key) => extractConnectionKey(key) !== connectionKey),
  pinned: state.pinned.filter(key => extractConnectionKey(key) !== connectionKey),
  ioExpanded: filterDictionary(state.ioExpanded, (_, key) => extractConnectionKey(key) !== connectionKey),
});

const devicesLoaded = (state: State, { connectionKey }: { connectionKey: EntityKey }): State => ({
  ...state,
  ...clearConnection(state, connectionKey),
  selectedEntity: isSubKeyOf(state.selectedEntity, connectionKey)
    && getEntityType(state.selectedEntity) !== EntityKeyType.CONNECTION ? null : state.selectedEntity,
});

const disconnect = (state: State, { connectionKey }: { connectionKey: EntityKey }): State => ({
  ...state,
  ...clearConnection(state, connectionKey),
  selectedEntity: isSubKeyOf(state.selectedEntity, connectionKey) ? null : state.selectedEntity,
});

const chooseEntity: Reducer<ActionTypes.CHOOSE_ENTITY> = (state, { key }) => ({ ...state, selectedEntity: key });

const pinProduct: Reducer<ActionTypes.PIN_PRODUCT> = (state, { key, pin }) => ({
  ...state,
  pinned: pin ? [...state.pinned, key] : state.pinned.filter(k => k !== key),
});

const setIoExpanded: Reducer<ActionTypes.SET_IO_EXPANDED> = (state, { key, expanded }) => ({
  ...state,
  ioExpanded: { ...state.ioExpanded, [key]: expanded },
});

const setMoveSinModal: Reducer<ActionTypes.SET_MOVE_SIN_MODAL> = (state, { moveable }) => ({
  ...state,
  moveSinModal: moveable,
});

const setTravelLimitModal: Reducer<ActionTypes.SET_TRAVEL_LIMIT_MODAL> = (state, { moveable }) => ({
  ...state,
  travelLimitModal: moveable,
});

// TODO ZL-790: Make sure that removing a device in the connection monitor results in it being removed from this state as well
type Payloads = ActionsToPayloads & ConnectionManagerActionPayloads & ConnectionStatusActionPayloads;
const basicControlsReducer = createReducer<Payloads, typeof initialState>({
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
  [ConnectionStatusActionTypes.ROUTED_CONNECTION_DISCONNECT]: disconnect,

  [ActionTypes.FETCH_DEVICE_INFO_DONE]: fetchDeviceInfoDone,

  [ActionTypes.CHOOSE_ENTITY]: chooseEntity,
  [ActionTypes.PIN_PRODUCT]: pinProduct,
  [ActionTypes.SET_ERROR]: setError,
  [ActionTypes.SET_IO_EXPANDED]: setIoExpanded,

  [ActionTypes.SET_MOVE_SIN_MODAL]: setMoveSinModal,
  [ActionTypes.SET_TRAVEL_LIMIT_MODAL]: setTravelLimitModal,
}, initialState);

export const reducer = combineReducers({
  controls: basicControlsReducer,
  movement: basicControlsMovementReducer,
  io: basicControlsIoReducer,
});
