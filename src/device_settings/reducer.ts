import _ from 'lodash';
import type { Units } from '@zaber/motion';

import { changeDictionary, createReducer, ensureArray } from '../utils';
import { EntityKey, extractConnectionKey, removeWithKey, tryExtractConnectionKey } from '../keys';
import { ConnectionManagerActionTypes, ConnectionManagerActionPayloads, AxisInfo, DevicesLoadedPayload } from '../connection_manager';
import type { DeviceInfo } from '../connection_manager/types';

import { ActionsToPayloads, ActionTypes } from './actions';
import type { SettingsEntryInfo, SettingValue, customDialog } from './types';

export type SettingsEntry = SettingsEntryInfo & {
  readError: string | null;
  writeError: string | null;
};

function defaultSettingState(info: SettingsEntryInfo): SettingsEntry {
  return ({
    ...info,
    readError: null,
    writeError: null,
  });
}

export interface DeviceOrAxisState {
  key: EntityKey;
  settings: Record<string, SettingsEntry>;
  settingIDsSorted: string[] | null;
  currentAccessLevel: number | null;
  scrollPosition: number;
  error: string | null;
}

function defaultDeviceOrAxisState(axisOrDevice: AxisInfo | DeviceInfo): DeviceOrAxisState {
  return {
    key: axisOrDevice.key,
    settings: {},
    settingIDsSorted: null,
    currentAccessLevel: null,
    scrollPosition: 0,
    error: null,
  };
}

export interface State {
  selectedDeviceOrAxis: EntityKey | null;
  devicesAndAxes: Record<string, DeviceOrAxisState>;
  search: string;
  settingsReadQueue: { deviceOrAxis: EntityKey; setting: string; units?: Units | null }[];
  customDialog: customDialog | null;
  configuringNetwork: boolean;
  configureNetworkErr: string | null;
}

const initialState: State = {
  selectedDeviceOrAxis: null,
  devicesAndAxes: {},
  search: '',
  settingsReadQueue: [],
  customDialog: null,
  configuringNetwork: false,
  configureNetworkErr: null,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const devicesLoaded = (state: State, { devices, connectionKey }: DevicesLoadedPayload): State => {
  const identified = devices.filter(device => device.identity);
  const currentConnection = tryExtractConnectionKey(state.selectedDeviceOrAxis);
  return ({
    ...state,
    selectedDeviceOrAxis: connectionKey === currentConnection ? null : state.selectedDeviceOrAxis,
    devicesAndAxes: {
      ...removeWithKey(state.devicesAndAxes, extractConnectionKey, connectionKey),
      ..._.keyBy(devices.map(defaultDeviceOrAxisState), 'key'),
      ..._.keyBy(_.flatMap(identified, device => device.axes.map(defaultDeviceOrAxisState)), 'key'),
    },
  });
};

const selectDeviceOrAxis = (state: State, { deviceOrAxis }: ActionsToPayloads[ActionTypes.SELECT_DEVICE_OR_AXIS]): State =>
  ({
    ...state,
    selectedDeviceOrAxis: deviceOrAxis,
    devicesAndAxes: deviceOrAxis ? changeDictionary(state.devicesAndAxes, deviceOrAxis, device => ({
      ...device,
      populatingSettings: {
        error: null,
        working: device.settingIDsSorted == null,
      },
    })) : state.devicesAndAxes,
  });

function settingsComparator(a: SettingsEntryInfo, b: SettingsEntryInfo) {
  return (a.meta?.humanName ?? a.name).localeCompare(b.meta?.humanName ?? b.name);
}

const populateSettings = (
  state: State,
  { deviceOrAxis, settings }: ActionsToPayloads[ActionTypes.POPULATE_SETTINGS],
): State =>
  ({
    ...state,
    devicesAndAxes: changeDictionary(state.devicesAndAxes, deviceOrAxis, {
      settingIDsSorted: settings.slice().sort(settingsComparator).map(setting => setting.id),
      settings: _.keyBy(settings.map(defaultSettingState), 'id'),
    }),
  });

function getReadUnits(prevValue: SettingValue): Units | null {
  if (prevValue.type !== 'number') {
    return null;
  }
  const prevUnits = prevValue.value?.units;
  if (prevUnits == null || prevUnits === 'default' || prevUnits === 'readable') {
    return null;
  }
  return prevUnits;
}

const readSetting = (state: State, { deviceOrAxis, settings }: ActionsToPayloads[ActionTypes.READ_SETTING]): State => {
  settings = ensureArray(settings);
  return {
    ...state,
    devicesAndAxes: changeDictionary(state.devicesAndAxes, deviceOrAxis, device => ({
      ...device,
      settings: {
        ...device.settings,
        ..._.mapValues(_.keyBy(settings), (setting): SettingsEntry => ({
          ...device.settings[setting],
          mode: 'initialize',
          readError: null,
          writeError: null,
        })),
      },
    })),
    settingsReadQueue: [
      ...state.settingsReadQueue,
      ...settings.map(setting => ({
        deviceOrAxis,
        setting,
        units: getReadUnits(state.devicesAndAxes[deviceOrAxis].settings[setting]),
      })),
    ],
  };
};

const clearSettingQueue: Reducer<ActionTypes.CLEAR_SETTING_QUEUE> = state => ({
  ...state,
  settingsReadQueue: [],
  devicesAndAxes: _.mapValues(state.devicesAndAxes, (deviceOrAxis): DeviceOrAxisState => ({
    ...deviceOrAxis,
    settings: _.mapValues(deviceOrAxis.settings, (setting): SettingsEntry => ({
      ...setting,
      mode: 'display',
    })),
  })),
});

const readSettingDone = (state: State, { deviceOrAxis, setting, value, error }: ActionsToPayloads[ActionTypes.READ_SETTING_DONE]): State =>
  ({
    ...state,
    devicesAndAxes: changeDictionary(state.devicesAndAxes, deviceOrAxis, device => ({
      ...device,
      settings: changeDictionary(device.settings, setting, (error ? {
        mode: 'display', readError: error,
      } : {
        mode: 'display', ...value,
      })),
      error: null,
    })),
    settingsReadQueue: state.settingsReadQueue.filter(({ deviceOrAxis: d, setting: s }) => d !== deviceOrAxis || s !== setting),
  });

type WriteStartPayload = ActionsToPayloads[ActionTypes.WRITE_SETTING] | ActionsToPayloads[ActionTypes.WRITE_STRING_SETTING];
const writeSetting = (state: State, { deviceOrAxis, setting }: WriteStartPayload): State =>
  ({
    ...state,
    devicesAndAxes: changeDictionary(state.devicesAndAxes, deviceOrAxis, device => ({
      ...device,
      settings: changeDictionary(device.settings, setting, ({ mode: 'write', writeError: null }))
    })),
  });
const writeSettingDone = (state: State, { deviceOrAxis, setting, error }: ActionsToPayloads[ActionTypes.WRITE_SETTING_DONE]): State =>
  ({
    ...state,
    devicesAndAxes: changeDictionary(state.devicesAndAxes, deviceOrAxis, device => ({
      ...device,
      settings: changeDictionary(device.settings, setting, ({ mode: error ? 'edit' : 'display', writeError: error }))
    })),
  });
const updateSetting: Reducer<ActionTypes.UPDATE_SETTING> = (state, { deviceOrAxis, setting, update }) => ({
  ...state,
  devicesAndAxes: changeDictionary(state.devicesAndAxes, deviceOrAxis, device => ({
    ...device,
    settings: changeDictionary(device.settings, setting, ({ writeError: null, ...update }))
  })),
});

const updateCurrentAccessLevel = (
  state: State,
  { deviceOrAxis, currentAccessLevel }: ActionsToPayloads[ActionTypes.UPDATE_CURRENT_ACCESS_LEVEL]
): State =>
  ({
    ...state,
    devicesAndAxes: changeDictionary(state.devicesAndAxes, deviceOrAxis, device => ({
      ...device,
      currentAccessLevel,
    }))
  });

const setSearchString = (state: State, searchWith: ActionsToPayloads[ActionTypes.SET_SEARCH_STRING]): State => ({
  ...state,
  search: searchWith,
});

const updateScrollPosition = (
  state: State,
  { deviceOrAxis, scrollPosition }: ActionsToPayloads[ActionTypes.UPDATE_SCROLL_POSITION],
): State => ({
  ...state,
  devicesAndAxes: changeDictionary(state.devicesAndAxes, deviceOrAxis, { scrollPosition })
});

const setCustomDialog = (state: State, { customDialog }: ActionsToPayloads[ActionTypes.SET_CUSTOM_DIALOG]): State =>
  ({
    ...state,
    customDialog,
    configureNetworkErr: null,
  });

const configureNetwork = (state: State): State =>
  ({
    ...state,
    configuringNetwork: true,
  });
const configureNetworkDone = (state: State, { error }: ActionsToPayloads[ActionTypes.CONFIGURE_NETWORK_DONE]): State =>
  ({
    ...state,
    configuringNetwork: false,
    configureNetworkErr: error ?? null,
  });

const setError = (state: State, { deviceOrAxis, error }: ActionsToPayloads[ActionTypes.SET_ERROR]): State =>
  ({
    ...state,
    devicesAndAxes: changeDictionary(state.devicesAndAxes, deviceOrAxis, { error }),
  });

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
  [ActionTypes.SELECT_DEVICE_OR_AXIS]: selectDeviceOrAxis,
  [ActionTypes.POPULATE_SETTINGS]: populateSettings,
  [ActionTypes.READ_SETTING]: readSetting,
  [ActionTypes.CLEAR_SETTING_QUEUE]: clearSettingQueue,
  [ActionTypes.READ_SETTING_DONE]: readSettingDone,
  [ActionTypes.WRITE_SETTING]: writeSetting,
  [ActionTypes.WRITE_STRING_SETTING]: writeSetting,
  [ActionTypes.WRITE_SETTING_DONE]: writeSettingDone,
  [ActionTypes.UPDATE_SETTING]: updateSetting,
  [ActionTypes.UPDATE_CURRENT_ACCESS_LEVEL]: updateCurrentAccessLevel,
  [ActionTypes.SET_SEARCH_STRING]: setSearchString,
  [ActionTypes.UPDATE_SCROLL_POSITION]: updateScrollPosition,
  [ActionTypes.SET_CUSTOM_DIALOG]: setCustomDialog,
  [ActionTypes.CONFIGURE_NETWORK]: configureNetwork,
  [ActionTypes.CONFIGURE_NETWORK_DONE]: configureNetworkDone,
  [ActionTypes.SET_ERROR]: setError,
}, initialState);
