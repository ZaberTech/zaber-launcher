import React from 'react';
import { useSelector } from 'react-redux';

import { EntityKey, extractConnectionKey } from '../keys';
import { tryAccess } from '../utils';

import { defaultConnectionState } from './reducer';
import { selectConnections } from './selectors';

interface Props {
  deviceKey: EntityKey;
  children?: (reachable: boolean) => React.ReactNode;
}

export const DeviceReachable: React.FC<Props> = ({ deviceKey, children }) => {
  const connection = tryAccess(useSelector(selectConnections), extractConnectionKey(deviceKey)) ?? defaultConnectionState;
  const reachable = !connection.detected || !!connection.error || connection.detectedDevices.includes(deviceKey);

  return <>{children?.(reachable)}</>;
};
