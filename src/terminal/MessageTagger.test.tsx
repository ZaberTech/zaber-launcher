import React from 'react';
import { render, RenderResult } from '@testing-library/react';

import { MessageTagger } from './MessageTagger';

let wrapper: RenderResult;

function renderAndMatch(element: JSX.Element) {
  wrapper?.unmount();

  wrapper = render(element);
  expect(wrapper.container).toMatchSnapshot();
}

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;
});

test('renders unknown message', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: 'gibberish message',
      parsed: {
        type: 'unknown',
      }
    }}
    onClickError={() => null}></MessageTagger>);
  expect(wrapper.getByText('gibberish message')).toHaveClass('part');
});

test('renders command message', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '/home',
      parsed: {
        type: 'command',
        device: null,
        axis: null,
        id: null,
        data: 'home',
        checksum: null,
      }
    }}
    onClickError={() => null}></MessageTagger>);
});

test('renders command message with device and axis', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '/01 2 33 home',
      parsed: {
        type: 'command',
        device: '01',
        axis: '2',
        id: '33',
        data: 'home',
        checksum: null,
      }
    }}
    onClickError={() => null}></MessageTagger>);
});

test('renders alert message', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '!03 4 IDLE --',
      parsed: {
        type: 'alert',
        device: '03',
        axis: '4',
        status: 'IDLE',
        errorFlag: {
          text: '--',
          type: null,
        },
        data: null,
        checksum: null,
      }
    }}
    onClickError={() => null}></MessageTagger>);
});

test('renders alert message with data', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '!03 4 IDLE -- 00',
      parsed: {
        type: 'alert',
        device: '03',
        axis: '4',
        status: 'IDLE',
        errorFlag: {
          text: '--',
          type: null,
        },
        data: '00',
        checksum: null,
      }
    }}
    onClickError={() => null}></MessageTagger>);
});

test('renders info message with checksum', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '#01 02 11 some info message:78',
      parsed: {
        type: 'info',
        device: '1',
        axis: '2',
        id: '11',
        data: 'some info message',
        checksum: {
          text: '78',
          error: false,
        },
      }
    }}
    onClickError={() => null}></MessageTagger>);
});

test('renders reply OK', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '@02 1 67 OK IDLE -- 00:CE',
      parsed: {
        type: 'reply',
        device: '2',
        axis: '1',
        id: '67',
        replyFlag: 'OK',
        status: 'IDLE',
        errorFlag: {
          text: '--',
          type: null,
        },
        data: '00',
        checksum: {
          text: 'CE',
          error: false,
        },
      }
    }}
    onClickError={() => null}></MessageTagger>);
});

test('renders reply RJ', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '@02 1 67 RJ IDLE -- BADDATA:4B',
      parsed: {
        type: 'reply',
        device: '2',
        axis: '1',
        id: '67',
        replyFlag: 'RJ',
        status: 'IDLE',
        errorFlag: {
          text: '--',
          type: null,
        },
        data: 'BADDATA',
        checksum: {
          text: '4B',
          error: false,
        },
      }
    }}
    onClickError={() => null}></MessageTagger>);
  expect(wrapper.getByText('RJ')).toHaveClass('fault');
  expect(wrapper.getByText('BADDATA')).toHaveClass('fault');
});

test('renders reply errorFlag fault', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '@02 1 67 OK IDLE FS 0:BF',
      parsed: {
        type: 'reply',
        device: '2',
        axis: '1',
        id: '67',
        replyFlag: 'OK',
        status: 'IDLE',
        errorFlag: {
          text: 'FS',
          type: 'fault',
        },
        data: '0',
        checksum: {
          text: 'BF',
          error: false,
        },
      }
    }}
    onClickError={() => null}></MessageTagger>);
  expect(wrapper.getByText('FS')).toHaveClass('fault');
});

test('renders reply errorFlag warning', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '@02 1 67 OK IDLE WJ 0:BF',
      parsed: {
        type: 'reply',
        device: '2',
        axis: '1',
        id: '67',
        replyFlag: 'OK',
        status: 'IDLE',
        errorFlag: {
          text: 'WJ',
          type: 'warn',
        },
        data: '0',
        checksum: {
          text: 'BF',
          error: false,
        },
      }
    }}
    onClickError={() => null}></MessageTagger>);
  expect(wrapper.getByText('WJ')).toHaveClass('warn');
});

test('renders reply errorFlag fault', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '@02 1 67 OK IDLE NC 0:BF',
      parsed: {
        type: 'reply',
        device: '2',
        axis: '1',
        id: '67',
        replyFlag: 'OK',
        status: 'IDLE',
        errorFlag: {
          text: 'NC',
          type: 'note',
        },
        data: '0',
        checksum: {
          text: 'BF',
          error: false,
        },
      }
    }}
    onClickError={() => null}></MessageTagger>);
  expect(wrapper.getByText('NC')).toHaveClass('note');
});

test('renders checksum invalid', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '@02 1 67 OK IDLE -- 00:00',
      parsed: {
        type: 'reply',
        device: '2',
        axis: '1',
        id: '67',
        replyFlag: 'OK',
        status: 'IDLE',
        errorFlag: {
          text: '--',
          type: null,
        },
        data: '00',
        checksum: {
          text: '00',
          error: true,
        },
      }
    }}
    onClickError={() => null}></MessageTagger>);
  expect(wrapper.getAllByText('00')[1]).toHaveClass('fault');
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '@02 1 67 OK IDLE -- 00:00',
      parsed: {
        type: 'reply',
        device: '2',
        axis: '1',
        id: '67',
        replyFlag: 'OK',
        status: 'IDLE',
        errorFlag: {
          text: '--',
          type: null,
        },
        data: '00',
        checksum: {
          text: 'BS',
          error: true,
        }
      }
    }}
    onClickError={() => null}></MessageTagger>);
  expect(wrapper.getByText('BS')).toHaveClass('fault');
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '#01 0 some invalid:01',
      parsed: {
        type: 'info',
        device: '1',
        axis: '0',
        id: null,
        data: 'some invalid',
        checksum: {
          text: '01',
          error: true,
        },
      }
    }}
    onClickError={() => null}></MessageTagger>);
  expect(wrapper.getByText('01')).toHaveClass('fault');
});

test('renders reply no checksum', () => {
  renderAndMatch(<MessageTagger
    historyItem={{
      deviceOrAxisEntityKey: null,
      local: true,
      text: '@02 1 OK IDLE -- 00',
      parsed: {
        type: 'reply',
        device: '2',
        axis: '1',
        id: null,
        replyFlag: 'OK',
        status: 'IDLE',
        errorFlag: {
          text: '--',
          type: null,
        },
        data: '00',
        checksum: null,
      }
    }}
    onClickError={() => null}></MessageTagger>);
});
