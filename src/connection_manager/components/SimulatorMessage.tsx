import React from 'react';
import { NoticeBanner } from '@zaber/react-library';

import { OneTimeMessage } from '../../help';
import { ExternalLink } from '../../components';
import { Editions, environment } from '../../environment';


const SIMULATOR_ONE_TIME_MESSAGE_ID = 'simulator';

export const SimulatorMessage: React.FC = () => {
  const simulatorLink = <ExternalLink url="https://simulator.izaber.com">Little Device Simulator</ExternalLink>;

  return <>Check out the {simulatorLink} to simulate a simple version of any Zaber device, available on the internal Zaber network.</>;
};

export const SimulatorOneTimeMessage: React.FC = () => {
  if (![Editions.Dev, Editions.Internal].includes(environment.edition)) {
    return null;
  }

  return (
    <OneTimeMessage messageId={SIMULATOR_ONE_TIME_MESSAGE_ID} message={dismiss => (
      <NoticeBanner
        type="info"
        headline="Little Device Simulator"
        closer={dismiss}
        className="one-time-message"
      >
        <SimulatorMessage/>
      </NoticeBanner>
    )}/>
  );
};
