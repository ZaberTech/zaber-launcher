import { all, takeLatest, put, call, takeEvery, race, take, select, delay, spawn, SagaReturnType as SRT, fork } from 'redux-saga/effects';
import type { SagaIterator } from 'redux-saga';
import _ from 'lodash';
import { ascii, binary, ConnectionFailedException, NoDeviceFoundException, NotSupportedException } from '@zaber/motion';
import { SimulationState } from '@zaber/serverless-service-definitions';

import { Action, throwUnexpectedError } from '../utils';
import { MessageRoutersService, LOCAL_ROUTER_URL, MessageRouterConnection } from '../message_router';
import { Connection, isConnectionSerialPort, isConnectionTcpIp, ZaberApi } from '../app_components';
import { getContainer } from '../container';
import { getRouterUrl, getConnectionId, makeRouterKey, extractRouterKey, makeConnectionKey } from '../keys';
import {
  connectionManagerActions, queryDevices, queryBinaryDevices, fetchDeviceImages, RouterType, makeCloudUrl, parseCloudUrl,
  isCloudUrl, ConnectionManagerActionTypes, DevicesLoadedAction,
} from '../connection_manager';
import { actions as connectionViewActions } from '../connection_manager/connection_view/actions';
import { ipcActions } from '../ipc';
import { ZML } from '../zml';
import { getLogger } from '../log';
import { selectIotRealm, selectIsAuthenticated } from '../cloud';
import { environment } from '../environment';

import { ActionTypes, ActionsToPayloads, actions } from './actions';
import { selectConfiguration, selectPorts, selectCloud, selectLastAddedConnection } from './selectors';
import {
  DEFAULT_BAUD_RATE, ErrorWithTitle, UNAUTHENTICATED_REALM,
} from './types';
import { discoverySaga } from './mdnsDiscovery';
import { NoInternetConnectionException } from '../connection_manager/exception';


export const LIST_PORT_LOOP_DELAY = 500;
let logger: ReturnType<typeof getLogger>;

export function* addConnectionSaga(): SagaIterator {
  logger = getLogger('addConnectionSaga');
  yield all([
    takeLatest(ActionTypes.ADD_CONNECTION, addConnection),
    takeEvery(ActionTypes.REMOVE_CONNECTION, removeConnection),
    takeEvery(ActionTypes.START_MONITORING_PORTS, startMonitoringPorts),
    takeEvery(ActionTypes.DETECT_DEVICES, detectDevices),
    takeEvery(ActionTypes.CONNECT_CLOUD, connectToCloud),
    takeEvery(ActionTypes.CONNECT_LOCAL_SHARE, connectLocalShare),
    takeEvery(ConnectionManagerActionTypes.DEVICES_LOADED, devicesLoaded),
    fork(discoverySaga),
  ]);
}

function* addConnection(): SagaIterator {
  try {
    const service = getContainer().get(MessageRoutersService);
    const connection: MessageRouterConnection = yield call([service, service.getConnection], LOCAL_ROUTER_URL);
    const api = connection.api;
    const configuration: ReturnType<typeof selectConfiguration> = yield select(selectConfiguration);

    let addedConnection: Connection;
    if (isConnectionTcpIp(configuration)) {
      addedConnection = yield call([api, api.addTcpIp], configuration.config);
    } else if (isConnectionSerialPort(configuration)) {
      addedConnection = yield call([api, api.addSerialPort], configuration.config);
    } else {
      throw new Error(`Unknown connection ${configuration.type}`);
    }

    const routerKey = makeRouterKey(LOCAL_ROUTER_URL);
    const connectionKey = makeConnectionKey(routerKey, addedConnection.id);

    yield put(actions.addConnectionDone(connectionKey));
    yield put(connectionViewActions.setExpanded(connectionKey, true));

    const reloadConnectionsAction = connectionManagerActions.loadConnections(routerKey, false);
    yield put(ipcActions.ipcBroadcast(reloadConnectionsAction, true));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.addConnectionErr((String)(err)));
  }
}

function* removeConnection({ payload: { connectionKey } }: Action<ActionsToPayloads[ActionTypes.REMOVE_CONNECTION]>): SagaIterator {
  try {
    const service = getContainer().get(MessageRoutersService);
    const connection: MessageRouterConnection = yield call([service, service.getConnection], getRouterUrl(connectionKey));
    const api = connection.api;
    yield call([api, api.removeConnection], getConnectionId(connectionKey));

    yield put(actions.removeConnectionDone(connectionKey));

    const reloadConnectionsAction = connectionManagerActions.loadConnections(extractRouterKey(connectionKey), false);
    yield put(ipcActions.ipcBroadcast(reloadConnectionsAction, true));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.removeConnectionDone(connectionKey, String(err)));
  }
}

function* startMonitoringPorts(): SagaIterator {
  yield race({
    loop: call(monitorPorts),
    cancel: take(ActionTypes.STOP_MONITORING_PORTS),
  });
}

function* monitorPorts(): SagaIterator {
  const zml = getContainer().get<ZML>(ZML);

  while (true) {
    const ports: string[] = yield call([zml, zml.listPorts]);
    const currentPorts: ReturnType<typeof selectPorts> = yield select(selectPorts);
    if (!currentPorts) {
      yield put(actions.portsChanged(ports));
    } else {
      const addedPorts = _.difference(ports, currentPorts);
      const removePorts = _.difference(currentPorts, ports);

      if (addedPorts.length > 0 || removePorts.length > 0) {
        yield put(actions.portsChanged(ports));
        if (addedPorts.length === 1) {
          yield put(actions.newPortConnected(addedPorts[0]));
        }
      }
    }

    yield delay(LIST_PORT_LOOP_DELAY);
  }
}

const DEFAULT_BINARY_BAUD_RATE = 9600;

function* isBinaryDevicePresent(): SagaIterator<boolean> {
  let connection: binary.Connection | null = null;
  try {
    const config: ReturnType<typeof selectConfiguration> = yield select(selectConfiguration);

    const zml = getContainer().get<ZML>(ZML);
    if (isConnectionSerialPort(config)) {
      const { serialPort } = config.config;
      connection = yield call([zml, zml.openBinarySerialPort], serialPort, DEFAULT_BINARY_BAUD_RATE);
    } else if (isConnectionTcpIp(config)) {
      const { hostname, tcpPort } = config.config;
      connection = yield call([zml, zml.openBinaryTcp], hostname, tcpPort);
    } else {
      throw new Error(`Unknown connection ${config.type}`);
    }

    const count: SRT<typeof queryBinaryDevices> = yield call(queryBinaryDevices, connection!);
    return count > 0;
  } catch (err) {
    throwUnexpectedError(err);
    logger.warn('Error detecting binary devices', err);
    return false;
  } finally {
    if (connection) {
      yield call([connection, connection.close]);
    }
  }
}

const SERIAL_PORT_PERMISSION_ERR = 'Cannot open serial port: Permission denied';
const SERIAL_PORT_PERMISSION_HELP = 'You may need to add user to the "dialout" group to access serial ports.';

function* detectDevices(): SagaIterator {
  let connection: ascii.Connection | null = null;
  try {
    const config: ReturnType<typeof selectConfiguration> = yield select(selectConfiguration);

    const zml = getContainer().get<ZML>(ZML);
    if (isConnectionSerialPort(config)) {
      const { serialPort, baudRate } = config.config;
      connection = yield call([zml, zml.openSerialPort], serialPort, baudRate ?? DEFAULT_BAUD_RATE);
    } else if (isConnectionTcpIp(config)) {
      const { hostname, tcpPort } = config.config;
      connection = yield call([zml, zml.openTcp], hostname, tcpPort);
    } else {
      throw new Error(`Unknown connection ${config.type}`);
    }

    const dummyKey = makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), 'dummy');
    const devices: SRT<typeof queryDevices> = yield call(queryDevices, connection!, dummyKey, { noDevicesOk: false });

    yield put(actions.detectDevicesDone(devices));
    yield spawn(fetchDeviceImages, devices);
  } catch (err) {
    throwUnexpectedError(err);

    let message: ErrorWithTitle;
    if (err instanceof ConnectionFailedException) {
      let messageStr = err.message;
      if (messageStr.endsWith(SERIAL_PORT_PERMISSION_ERR) && environment.platform === 'linux') {
        messageStr += `. ${SERIAL_PORT_PERMISSION_HELP}`;
      }
      message = { title: 'Cannot Open Connection', message: messageStr };
    } else if (err instanceof NoDeviceFoundException) {
      yield call([connection!, connection!.close]);
      connection = null;

      if (yield call(isBinaryDevicePresent)) {
        message = {
          title: 'Binary Device Detected',
          message: 'Zaber Launcher does not support the Zaber Binary Protocol. Please use Zaber Console instead.',
        };
      } else {
        message = {
          title: 'No Device Detected',
          message: 'Make sure that devices are powered on and try again.',
        };
      }
    } else if (err instanceof NotSupportedException) {
      message = {
        title: 'Unsupported Device Detected',
        message: [
          'The device either has a firmware version lower than 6.14',
          'or a version that is not supported by our software yet.',
          'Please contact Zaber support.'
        ].join(' '),
      };
    } else if (err instanceof NoInternetConnectionException) {
      message = {
        title: 'No Internet Connection',
        message: [
          'A connection is required to download device data and identify Zaber devices.',
          'If your computer is connected to the internet, please check your organization\'s firewall settings.',
          'If an internet connection is not available, consider installing Zaber Launcher Offline Edition instead.'
        ].join(' ')
      };
    } else {
      message = { title: null, message: err.message || String(err) };
    }

    yield put(actions.detectDevicesDoneErr(message));
  } finally {
    if (connection) {
      yield call([connection, connection.close]);
    }
  }
}

function* connectToCloud(): SagaIterator {
  yield race({
    connect: call(function* () {
      const service = getContainer().get<MessageRoutersService>(MessageRoutersService);
      const api = getContainer().get<ZaberApi>(ZaberApi);
      const cloud: ReturnType<typeof selectCloud> = yield select(selectCloud);
      const isAuthenticated: ReturnType<typeof selectIsAuthenticated> = yield select(selectIsAuthenticated);
      const privateRealm: ReturnType<typeof selectIotRealm> = yield select(selectIotRealm);

      try {
        let url: string;
        if (isCloudUrl(cloud.cloudId)) {
          const parsed = parseCloudUrl(cloud.cloudId)!;
          if (parsed.realm) {
            url = cloud.cloudId;
          } else {
            url = makeCloudUrl(privateRealm, parsed.cloudId);
          }
        } else {
          const data: SRT<typeof api.virtualDeviceExists> = yield api.virtualDeviceExists(cloud.cloudId);
          if (!data.exists) {
            yield put(actions.connectCloudDoneErr({
              title: 'Cannot connect',
              message: 'Virtual device with given ID does not exist.',
            }));
            return;
          } else if (!data.public && !isAuthenticated) {
            yield put(actions.connectCloudDoneErr({
              title: 'Cannot connect',
              message: 'Virtual device is a private instance but user is not signed in. Sign in under the cloud section.',
            }));
            return;
          } else {
            const properties: SRT<typeof api.getVirtualDeviceChain> = yield api.getVirtualDeviceChain(cloud.cloudId, data.public);
            if ([SimulationState.Creating, SimulationState.TurningOn].includes(properties.state)) {
              yield put(actions.connectCloudDoneErr({
                title: 'Initializing',
                message: 'Virtual device is still starting. Make sure the virtual device is running and try again.',
              }));
              return;
            }
          }

          url = makeCloudUrl(data.public ? UNAUTHENTICATED_REALM : privateRealm!, data.cloudId);
        }

        yield call([service, service.getConnection], url);
        yield put(connectionManagerActions.addRouter({ type: RouterType.Cloud, url }));
        yield put(actions.changeDialogOpen(false));
      } catch (err) {
        throwUnexpectedError(err);
        yield put(actions.connectCloudDoneErr({
          title: 'Cannot connect',
          message: 'Make sure the virtual device is running, the Cloud ID is correct, and your computer is online.',
        }));
      }
    }),
    cancel: take(ActionTypes.CONNECT_CLOUD_CLEAR),
  });
}

function* connectLocalShare({ payload: { url } }: Action<ActionsToPayloads[ActionTypes.CONNECT_LOCAL_SHARE]>): SagaIterator {
  yield race({
    connect: call(function* () {
      const service = getContainer().get<MessageRoutersService>(MessageRoutersService);
      try {
        yield call([service, service.getConnection], url);
        yield put(connectionManagerActions.addRouter({ type: RouterType.LocalShare, url }));
        yield put(actions.changeDialogOpen(false));
      } catch (err) {
        throwUnexpectedError(err);
        const message: ErrorWithTitle = {
          title: 'Cannot connect',
          message: 'Make sure that hostname and port is correct and both computers are on the same network.',
        };
        yield put(actions.connectLocalShareErr(message));
      }
    }),
    cancel: take(ActionTypes.CONNECT_LOCAL_SHARE_CLEAR),
  });
}

function* devicesLoaded({ payload: { connectionKey, devices } }: DevicesLoadedAction): SagaIterator {
  const lastAddedConnection: ReturnType<typeof selectLastAddedConnection> = yield select(selectLastAddedConnection);
  if (lastAddedConnection === connectionKey) {
    yield put(actions.firstDevicesLoad(connectionKey, devices));
  }
}
