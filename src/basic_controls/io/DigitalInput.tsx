import { Flex, Text } from '@zaber/react-library';
import classnames from 'classnames';
import React from 'react';
import { useSelector } from 'react-redux';
import { IoPortType } from '@zaber/motion/ascii';

import type { EntityKey } from '../../keys';

import { selectDeviceStates } from './selectors';

type DigitalInputProps = {
  deviceKey: EntityKey;
  channel: number;
  ports: boolean[] | null;
};

export const DigitalInput: React.FC<DigitalInputProps> = ({ deviceKey, channel, ports }) => {
  const input = ports?.[channel - 1];
  const text = input == null ? 'Loading...' : input ? 'On' : 'Off';
  const styleClass = text === 'On' ? 'on' : 'off';
  const labels = useSelector(selectDeviceStates)[deviceKey]?.labels;
  const label = labels?.[`${IoPortType.DIGITAL_INPUT}-${channel}`] ?? '';

  return <Flex.Row className="di control">
    <Text e={Text.Emphasis.Bold}>{channel}.</Text>
    <Text e={Text.Emphasis.Bold} className={classnames('status', styleClass)} title={`Digital Input ${channel} ${text}`}>{text}</Text>
    <Text>{label}</Text>
  </Flex.Row>;
};
