import { HOLDING_TORQUE_KEY, HOLD_FACTOR_KEY, RATED_CURRENT_KEY, RPM_KEY, RUN_FACTOR_KEY, STEPS_PER_ROTATION_KEY } from '../storageKeys';
import { is3rdPartyId } from '../peripheral_id_picker/peripheralIdMapping';
import type { SectionDataDefinition } from '../types';
import { hasSetting, getStoredNumberOrDefault } from '../../devices';

// C = Rated Motor Current (Amps/Phase)
// R = Driver Run Current Percentage (5-100)
// H = Driver Hold Current Percentage (5-100)
const AMPS_TO_NATIVE = 1000.0 / 20.0;
/** Max Current = C * sqrt(2) */
export const RATED_TO_MAX_CURRENT = Math.sqrt(2) * AMPS_TO_NATIVE;
/** driver.current.run = C * (R/100) * sqrt(2) / 20 = C * R * RATED_TO_RUN_CURRENT */
export const RATED_TO_RUN_CURRENT = Math.sqrt(2) * AMPS_TO_NATIVE / 100.0;
/** driver.current.hold = C * (H/100) / 20 = C * H * RATED_TO_HOLD_CURRENT */
export const RATED_TO_HOLD_CURRENT = AMPS_TO_NATIVE / 100.0;
export const RATED_TO_MAXSPEED_FACTOR = 1048576.0 / 600000.0;


export const DEFAULT_DRIVER_RUN_FACTOR = 70;
export const DEFAULT_DRIVER_HOLD_FACTOR = 35;

export const motorCharacteristics: SectionDataDefinition<'motorCharacteristics'> = {
  initializers: {
    stepsPerRotation: {
      type: 'custom', backing: async ascii => await getStoredNumberOrDefault(ascii, STEPS_PER_ROTATION_KEY, null)
    },
    rpm: { type: 'custom', backing: async ascii => await getStoredNumberOrDefault(ascii, RPM_KEY, null) },
    holdingTorque: {
      type: 'custom',
      requires: ['motor.ke'],
      backing: async (ascii, info) => {
        if (!hasSetting('motor.ke', info.axis ?? info.device)) {
          return { is: null, was: null, settingExists: false };
        }
        return await getStoredNumberOrDefault(ascii, HOLDING_TORQUE_KEY, null);
      }
    },
    ratedCurrent: { type: 'custom', backing: async ascii => await getStoredNumberOrDefault(ascii, RATED_CURRENT_KEY, null) },
    runFactor: {
      type: 'custom', backing: async ascii => await getStoredNumberOrDefault(ascii, RUN_FACTOR_KEY, DEFAULT_DRIVER_RUN_FACTOR)
    },
    holdFactor: {
      type: 'custom',
      backing: async ascii => await getStoredNumberOrDefault(ascii, HOLD_FACTOR_KEY, DEFAULT_DRIVER_HOLD_FACTOR)
    },

    driverDirection: { type: 'setting', backing: 'driver.dir' },
    motorInductance: { type: 'setting', backing: 'motor.inductance' },
    motorResistance: { type: 'setting', backing: 'motor.resistance' },

    ictrlType: { type: 'custom', requires: ['ictrl.type'], backing: ascii => ascii.settings.get('ictrl.type') },
  },
  sectionPresent: async ctx => {
    const axisId = ctx.info.axis?.identity?.peripheralId;
    if (is3rdPartyId(axisId)) {
      return true;
    }
    return false;
  },
  fieldEnabled: {},
};
