import { createSelector } from 'reselect';

import { selectUpdates } from '../store';

export const selectCurrentAppVersion = createSelector(selectUpdates, state => state.currentVersion);
