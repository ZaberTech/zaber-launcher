import _ from 'lodash';
import { Length, Time } from '@zaber/motion';

import { convertBetweenUnits, type MeasurementOK } from '../units';
import { deepFreeze } from '../utils';

import { EMPTY_OBJECTIVE_PARAMETERS, ILLUMINATOR_CHANNELS, type Axes, type ObjectiveParameterKeys } from './types';

type PresetValue<T> = { value: T; apply: boolean };

export function makePresetValue<T>(value: T): PresetValue<T> {
  return { value, apply: false };
}

export function isPresetValue(value: unknown): value is PresetValue<unknown> {
  return _.isObject(value) && 'value' in value && 'apply' in value;
}

export function isMeasurementOK(value: unknown): value is MeasurementOK {
  return _.isObject(value) && 'value' in value && 'units' in value;
}

export const PRESETS_VERSION = 4;

export interface IlluminatorChannelPreset {
  on: PresetValue<boolean>;
  intensity: PresetValue<number>;
}

export interface AxisPreset {
  step: PresetValue<MeasurementOK>;
  position: PresetValue<MeasurementOK>;
}

export interface AutofocusPreset {
  limitMin: PresetValue<MeasurementOK>;
  limitMax: PresetValue<MeasurementOK>;
  settings: Record<ObjectiveParameterKeys, PresetValue<number>>;
}

export interface TriggerPreset {
  exposure: PresetValue<MeasurementOK>;
  waitBefore: PresetValue<MeasurementOK>;
  waitAfter: PresetValue<MeasurementOK>;
}

/**
 * Represents the data that can be stored in a preset.
 * All the channels and axes are always populated no matter the setup.
 * This allows to conveniently compare values.
 **/
export interface PresetData {
  filter: PresetValue<number>;
  objective: PresetValue<number>;
  illuminator: Record<number, IlluminatorChannelPreset>;
  axes: Record<Axes, AxisPreset>;
  autofocus: AutofocusPreset;
  cameraTrigger: TriggerPreset;
}

export interface Preset {
  name: string;
  data: PresetData;
}

/** Compares the structure of two presets making sure that all the keys are present. */
export function isPresetStructureSound(structureTemplate: Preset, testedPreset: Preset) {
  return _.isEqualWith(structureTemplate, testedPreset, (a, b) => {
    if (['string', 'number', 'boolean'].includes(typeof a) && typeof a === typeof b) {
      return true;
    }
    return;
  });
}

type AllKeys = keyof PresetData | keyof IlluminatorChannelPreset | keyof AxisPreset | keyof AutofocusPreset;

/** Compares a preset to a preset derived from device state. */
export function isApplied(preset: PresetData, statePreset: PresetData): boolean {
  return _.isEqualWith(preset, statePreset, (applied, stateValue, keyOrIndex, parent) => {
    const key = keyOrIndex as AllKeys;
    if (key === 'position') {
      // We don't care about the position of the axes.
      return true;
    } else if (key === 'settings' && parent === preset.autofocus) {
      // We don't always have fresh values of autofocus settings.
      return true;
    }

    if (isPresetValue(applied) && isPresetValue(stateValue)) {
      if (!applied.apply) { return true }

      if (key === 'intensity') {
        return Math.abs((applied.value as number) - (stateValue.value as number)) < 0.1;
      }

      if (isMeasurementOK(applied.value) && isMeasurementOK(stateValue.value)) {
        const convertedValue = convertBetweenUnits(stateValue.value.value, stateValue.value.units, applied.value.units);
        return Math.abs(convertedValue - applied.value.value) < 0.0001;
      }

      return _.isEqual(applied.value, stateValue.value);
    }

    return;
  });
}

export function overrideValues(current: PresetData, values: PresetData): PresetData {
  const update = _.cloneDeep(current);
  _.mergeWith(update, values, (objValue, srcValue) => {
    if (isPresetValue(objValue) && isPresetValue(srcValue)) {
      return { apply: objValue.apply, value: srcValue.value };
    }
    return undefined;
  });
  return update;
}

export function migratePreset(preset: Preset, fromVersion: number) {
  if (fromVersion <= 1) {
    preset.data.autofocus = {
      limitMax: makePresetValue({ value: 0, units: Length.mm }),
      limitMin: makePresetValue({ value: 0, units: Length.mm }),
      settings: _.mapValues(EMPTY_OBJECTIVE_PARAMETERS, () => makePresetValue(0)),
    };
  }
  if (fromVersion <= 2) {
    const { settings } = preset.data.autofocus;
    // eslint-disable-next-line camelcase
    settings.max_exposure = makePresetValue(0);
    // eslint-disable-next-line camelcase
    settings.signal_to_noise_ratio = makePresetValue(0);
  }
  if (fromVersion <= 3) {
    preset.data.cameraTrigger = {
      exposure: makePresetValue({ value: 0, units: Time.ms }),
      waitBefore: makePresetValue({ value: 0, units: Time.ms }),
      waitAfter: makePresetValue({ value: 0, units: Time.ms }),
    };
  }
}

/** Compares one data with another marking all values that are different for application. */
export function compareAndMarkForApply(current: PresetData, past: PresetData) {
  const update = _.cloneDeep(current);
  _.mergeWith(update, past, (objValue, srcValue) => {
    if (isPresetValue(objValue) && isPresetValue(srcValue)) {
      if (!_.isEqual(objValue.value, srcValue.value)) {
        objValue.value = srcValue.value;
        objValue.apply = true;
      }
      return objValue;
    }
    return undefined;
  });
  return update;
}

const ZERO_AXIS_PRESET: AxisPreset = {
  step: makePresetValue({ value: 0, units: Length.mm }),
  position: makePresetValue({ value: 0, units: Length.mm }),
};

export const ZERO_PRESET: PresetData = deepFreeze({
  filter: makePresetValue(1),
  objective: makePresetValue(1),
  illuminator: _.mapValues(ILLUMINATOR_CHANNELS, () => ({
    on: makePresetValue(false),
    intensity: makePresetValue(0),
  })),
  axes: {
    xAxis: ZERO_AXIS_PRESET,
    yAxis: ZERO_AXIS_PRESET,
    focusAxis: ZERO_AXIS_PRESET,
  },
  autofocus: {
    limitMin: makePresetValue({ value: 0, units: Length.mm }),
    limitMax: makePresetValue({ value: 0, units: Length.mm }),
    settings: _.mapValues(EMPTY_OBJECTIVE_PARAMETERS, () => makePresetValue(0)),
  },
  cameraTrigger: {
    exposure: makePresetValue({ value: 0, units: Time.ms }),
    waitBefore: makePresetValue({ value: 0, units: Time.ms }),
    waitAfter: makePresetValue({ value: 0, units: Time.ms }),
  },
});
