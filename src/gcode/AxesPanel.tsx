import React from 'react';
import { Button, Icons, Text, iconFromSvg } from '@zaber/react-library';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { ascii } from '@zaber/motion';
import { tryAccess } from '@zaber/toolbox';

import noDeviceFound from '../assets/no_device_found.svg?url';
import SetOriginSvg from '../assets/set_origin.svg';
import { useActions } from '../utils';
import { connectionName } from '../connection_manager';

import { actions as actionsDefinition } from './actions';
import { AxisSettings, defaultPositionUnitsLabel, isPrimaryAxis, Paths, sortAxes, MoveType } from './types';
import { selectSettings, selectCurrentDevice, selectDisabled, selectLiveStates, selectStatus } from './selectors';
import { FeedRateOverride } from './FeedRateOverride';
import { FreeHold } from './FreeHold';

function getStepUnitLabel(axis: AxisSettings) {
  switch (axis.axisType) {
    case ascii.AxisType.LINEAR: return '1 mm (Shift 0.1 mm) (Ctrl 0.01 mm)';
    case ascii.AxisType.ROTARY: return '1 ° (Shift 0.1 °) (Ctrl 0.01 °)';
    default: return '1 position (Shift 0.1) (Ctrl 0.01)';
  }
}

function addStepSize(movement: 'step-left' | 'step-right', e: React.MouseEvent<unknown>): MoveType {
  if (e.shiftKey) {
    return `${movement}-small`;
  } else if (e.ctrlKey) {
    return `${movement}-xsmall`;
  }
  return movement;
}

const SetOrigin = iconFromSvg(SetOriginSvg);

const AxisPanel: React.FC<{
  axis: AxisSettings;
}> = ({
  axis,
}) => {
  const letter = axis.letter!;
  const device = useSelector(selectCurrentDevice)!;
  const physicalAxis = device.axes[axis.axisNumber - 1];
  const actions = useActions(actionsDefinition);
  const disabled = useSelector(selectDisabled);
  const { coordinateSystem } = useSelector(selectStatus);
  const liveState = tryAccess(useSelector(selectLiveStates), axis.letter);
  const onePos = getStepUnitLabel(axis);
  return <div className="axis">
    <div className="header"><Text t={Text.Type.H4}>Axis {axis.letter}</Text></div>
    <div><Text>Axis {axis.axisNumber}&emsp;{physicalAxis.label ?? physicalAxis.identity.peripheralName}</Text></div>
    <div className="controls">
      <Icons.Home title="Home" disabled={disabled}
        highlight={liveState?.needsHoming ? 'on' : 'off'}
        onClick={() => actions.moveAxis(letter, 'home')}/>
      <Icons.LeftFast title="Jog Left" disabled={disabled} appearsClickable
        onMouseDown={() => actions.moveAxis(letter, 'jog-left')}
        onMouseUp={() => actions.stopAxis(letter)}/>
      <Icons.LeftNormal title={`Step Left ${onePos}`} disabled={disabled}
        onClick={e => actions.moveAxis(letter, addStepSize('step-left', e))}/>
      <Icons.RightNormal title={`Step Right ${onePos}`} disabled={disabled}
        onClick={e => actions.moveAxis(letter, addStepSize('step-right', e))}/>
      <Icons.RightFast title="Jog Right" disabled={disabled} appearsClickable
        onMouseDown={() => actions.moveAxis(letter, 'jog-right')}
        onMouseUp={() => actions.stopAxis(letter)}/>
    </div>
    {liveState && <>
      <div className="position">
        <b>G53</b>&emsp;
        {liveState.position?.toFixed(2) ?? '-'} {defaultPositionUnitsLabel(axis.axisType)}
      </div>
      <div className="position">
        <b>{coordinateSystem || '-'}</b>&emsp;
        {liveState.positionCoordinateSystem?.toFixed(2) ?? '-'} {defaultPositionUnitsLabel(axis.axisType)}
        <span className="spacer"/>
        <SetOrigin disabled={disabled} onClick={() => actions.zeroAxis(letter)} title="Zero Axis"/>
      </div>
    </>}
  </div>;
};

export const AxesPanel: React.FC = () => {
  const settings = useSelector(selectSettings);
  const device = useSelector(selectCurrentDevice);
  return (<div className="axes-panel">
    {device && <div className="header">
      <Text t={Text.Type.H4}>{connectionName(device.connection)}</Text>
      <Icons.ArrowCollapsed/>
      <Text t={Text.Type.H4}>{device.label ?? device.identity.name}</Text>
      <div className="spacer"/>
      {settings && <Link to={Paths.TranslatorSettings}>
        <Icons.Settings title="Settings" appearsClickable/>
      </Link>}
    </div>}
    {(device == null || settings == null) && <div className="no-controller">
      <img src={noDeviceFound} alt="No controller selected"/>
      <div>You have no controller selected.</div>
      <Link to={Paths.SelectController}>
        <Button>Select Controller</Button>
      </Link>
    </div>}
    {device && settings && <>
      {sortAxes(settings.axes.filter(isPrimaryAxis))
        .map(axis => <AxisPanel key={axis.axisNumber} axis={axis}/>)}
      <div className="separator"/>
      <FeedRateOverride/>
      <FreeHold/>
    </>}
  </div>);
};
