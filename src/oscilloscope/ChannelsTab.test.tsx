import React, { Component } from 'react';
import _ from 'lodash';
import { fireEvent, render, RenderResult, within } from '@testing-library/react';
import type { Container } from 'inversify';

import { IntersectionObserverMockBase } from '../test/mocks/intersection_observer';
import 'jest-canvas-mock';
import { MessageRoutersServiceMockIntegrated, MessageRoutersServiceMockMultiAxis } from './testing/mocks';

declare namespace ColorPickerMock {
  interface Props {
    color: string;
    onChange: (color: string) => void;
    ['data-testid']: string;
  }
}

const colorChangeCallbacks: Record<string, (color: string) => void> = {};

class ColorPickerMock extends Component<ColorPickerMock.Props> {
  render() {
    const testId = this.props['data-testid'];
    colorChangeCallbacks[testId] = this.props.onChange;
    return <div data-testid={this.props['data-testid']} className="color-picker" style={{ backgroundColor: this.props.color }}/>;
  }
}

jest.mock('@zaber/react-library', () => ({
  ...jest.requireActual('@zaber/react-library'),
  ColorPicker: ColorPickerMock,
}));

jest.mock('echarts-for-react');

class IntersectionObserverMock extends IntersectionObserverMockBase {
}

(window as unknown as { IntersectionObserver: typeof IntersectionObserverMock }).IntersectionObserver = IntersectionObserverMock;

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { selectDevices } from '../connection_manager';
import { mockSingleDevice, mockSingleDeviceWithPeripherals } from '../connection_manager/mocks';
import { createContainer, destroyContainer } from '../container';
import { MessageRoutersService } from '../message_router';
import type { AppStore } from '../store/build';
import { htmlCollectionToArray, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';

import { actions } from './actions';
import { extractMathChannels, getMathChannelSubtitle, makeIoChannelId, makeMathChannelId,  makeSettingChannelId } from './channels';
import { Oscilloscope } from './Oscilloscope';
import { selectActiveRecording, selectAllChannels, selectSavedRecordings } from './selectors';
import { addDeviceScopeSettingsInfo, openAddIoDialog, openAddSettingsDialog, selectRecordingTab } from './testing/utils';
import {
  ADD_IO_BUTTON_LABEL,
  MathChannelNames,
  NEW_RECORDING_TAB_LABEL,
  SHARE_SETTINGS_LABEL
} from './types';


const TEST_SAMPLE_COUNT = 10;
const doRecording = async function(store: AppStore) {
  store.dispatch(actions.startRecording());
  await waitUntilPass(() => {
    const recording = selectActiveRecording(store.getState());
    expect(recording?.channels[0].samples.length ?? 0).toBeGreaterThanOrEqual(TEST_SAMPLE_COUNT);
  }, 1000);
  store.dispatch(actions.stopRecording());
};


let container: Container;
const TestOscilloscope = wrapWithNewStore(wrapWithRouter(Oscilloscope));
let wrapper: RenderResult;
const ID_POS = makeSettingChannelId('pos', 0);
const ID_ENC_POS = makeSettingChannelId('encoder.pos', 0);
const ID_TRAJ_VEL = makeMathChannelId(MathChannelNames.TRAJECTORY_VEL, 0);
const ID_FOLLOW_ERR = makeMathChannelId(MathChannelNames.FOLLOW_ERR, 0);
const ID_TRAJ_ACCEL = makeMathChannelId(MathChannelNames.TRAJECTORY_ACCEL, 0);


/* =================================== Start of tests ================================== */

describe('With an integrated device', () => {
  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockIntegrated);
    wrapper = render(<TestOscilloscope/>);
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  describe('the channels tab', () => {
    beforeEach(async () => {
      const store = TestOscilloscope.testStore;
      mockSingleDevice(store, addDeviceScopeSettingsInfo);
      const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => wrapper.getByTestId('Data Source add'));
    });

    it('add settings dialog excludes non-scopable settings', async () => {
      const dialog = openAddSettingsDialog(wrapper);
      fireEvent.click(wrapper.getByTestId('show-all-settings'));
      const rows = (await within(dialog).findAllByTestId(/row-subtitle.*/)).map(element => element.innerHTML);
      expect(rows).not.toContain('comm.next.owner');
    });

    it('add settings dialog sorts settings within groups', async () => {
      const dialog = openAddSettingsDialog(wrapper);
      let rows = (await within(dialog).findAllByTestId(/row-subtitle.*/)).map(element => element.innerHTML);
      expect(rows).toEqual(['pos', 'encoder.pos']);
      fireEvent.click(wrapper.getByTestId('show-all-settings'));
      rows = (await within(dialog).findAllByTestId(/row-subtitle.*/)).map(element => element.innerHTML);
      expect(rows).toEqual(['encoder.pos', 'limit.max', 'limit.min', 'accel', 'maxspeed', 'pos', 'scope.delay', 'scope.timebase']);
    });

    it('add settings dialog sorts pills', async () => {
      const dialog = openAddSettingsDialog(wrapper);
      let pills = (await within(dialog).findAllByTestId('pill-label')).map(element => element.innerHTML);
      expect(pills).toEqual(['pos', 'encoder.pos']);
      fireEvent.click(wrapper.getByTestId('show-all-settings'));
      pills = (await within(dialog).findAllByTestId('pill-label')).map(element => element.innerHTML);
      expect(pills).toEqual(['encoder.pos', 'pos']);
    });


    it('add settings dialog sorts groups', () => {
      const dialog = openAddSettingsDialog(wrapper);
      let labels = htmlCollectionToArray(dialog.getElementsByClassName('header')).map(element => element.children[0].innerHTML);
      expect(labels).toEqual(['Motion', 'Encoder']);
      fireEvent.click(wrapper.getByTestId('show-all-settings'));
      labels = htmlCollectionToArray(dialog.getElementsByClassName('header')).map(element => element.children[0].innerHTML);
      expect(labels).toEqual(['Encoder', 'Limits', 'Motion', 'Scope']);
    });


    it('add settings dialog can filter', async () => {
      const dialog = openAddSettingsDialog(wrapper);
      fireEvent.click(wrapper.getByTestId('show-all-settings'));
      let rows = (await within(dialog).findAllByTestId(/row-subtitle.*/)).map(element => element.innerHTML);
      expect(rows).toEqual(['encoder.pos', 'limit.max', 'limit.min', 'accel', 'maxspeed', 'pos', 'scope.delay', 'scope.timebase']);
      let labels = htmlCollectionToArray(dialog.getElementsByClassName('header')).map(element => element.children[0].innerHTML);
      expect(labels).toEqual(['Encoder', 'Limits', 'Motion', 'Scope']);

      const input = wrapper.getByTestId('setting-name-filter');
      fireEvent.change(input, { target: { value: 'pos' } });
      rows = (await within(dialog).findAllByTestId(/row-subtitle.*/)).map(element => element.innerHTML);
      expect(rows).toEqual(['encoder.pos', 'limit.max', 'limit.min', 'pos']);
      labels = htmlCollectionToArray(dialog.getElementsByClassName('header')).map(element => element.children[0].innerHTML);
      expect(labels).toEqual(['Encoder', 'Limits', 'Motion']);

      fireEvent.change(input, { target: { value: '.pos' } });
      rows = (await within(dialog).findAllByTestId(/row-subtitle.*/)).map(element => element.innerHTML);
      expect(rows).toEqual(['encoder.pos']);
      labels = htmlCollectionToArray(dialog.getElementsByClassName('header')).map(element => element.children[0].innerHTML);
      expect(labels).toEqual(['Encoder']);
    });


    it('add settings dialog can remove and add settings channels', async () => {
      // Remove the encoder.pos channel.
      fireEvent.click(wrapper.getByTestId(`close-channel-${ID_ENC_POS}`));
      await waitUntilPass(() => !wrapper.queryByText('encoder.pos'));
      expect(wrapper.getAllByTestId(/close-channel-/)).toHaveLength(1);

      // Add it back.
      fireEvent.click(wrapper.getByTestId('Data Source add'));
      await waitUntilPass(() => {
        fireEvent.click(wrapper.getByTestId(`dlg-show-channel ${ID_ENC_POS}`));
      });

      fireEvent.click(within(wrapper.getByTestId('add-data-source-dialog')).getByText('Apply'));
      await waitUntilPass(() => expect(wrapper.getAllByTestId(/close-channel-/)).toHaveLength(2));
      within(wrapper.getByTestId('channel-card-Data Source')).getByText('encoder.pos');
      within(wrapper.getByTestId('channel-card-Data Source')).getByText('Trajectory Position');
    });


    it('add settings dialog can add custom channels', async () => {
      let dialog = openAddSettingsDialog(wrapper);
      fireEvent.change(wrapper.getByTestId('setting-name-filter'), { target: { value: 'foo' } });
      fireEvent.click(within(dialog).getByText('Add'));

      // The channel should now be in the channel list.
      const channelList = wrapper.getByTestId('channel-card-Data Source');
      expect(within(channelList).getAllByText('foo')).toHaveLength(2); // Title and subtitle.

      // Verify that it's also shown in the dialog next time it's opened.
      dialog = openAddSettingsDialog(wrapper);
      fireEvent.click(wrapper.getByTestId('show-all-settings'));
      within(dialog).getByTestId('row-subtitle foo');
    });


    it('add math dialog can add and removing math channels', async () => {
      expect(wrapper.getAllByTestId(/close-channel-/)).toHaveLength(2);

      // Click the add math button and verify the dialog is shown.
      const channelList = wrapper.getByTestId('channel-list-right');
      fireEvent.click(wrapper.getByTestId('Computed Math add'));

      // Check the content of the dialog.
      const dialog = wrapper.getByTestId('add-math-dialog');
      within(dialog).getByText('Trajectory Velocity');
      within(dialog).getByText('Trajectory Acceleration');
      within(dialog).getByText('Measured Velocity');
      within(dialog).getByText('Measured Acceleration');
      within(dialog).getByText('Position Following Error');
      within(dialog).getByText('Velocity Following Error');
      within(dialog).getByText('Acceleration Following Error');

      // Select a math channel and close the dialog.
      fireEvent.click(dialog.querySelector('input')!);
      fireEvent.click(within(dialog).getByText('Apply'));

      // The channel should now be in the channel list.
      await waitUntilPass(() => expect(wrapper.getAllByTestId(/close-channel-/)).toHaveLength(3));
      within(channelList).getByText('Position Following Error');

      // Now delete the math channel and check results.
      fireEvent.click(wrapper.getByTestId(`close-channel-${ID_FOLLOW_ERR}`));
      await waitUntilPass(() => expect(wrapper.getAllByTestId(/close-channel-/)).toHaveLength(2));
      expect(within(channelList).queryByText('Position Following Error')).toBeFalsy();
    });


    it('groups and sorts channels', async () => {
      // Enable some math channels.
      const channelList = wrapper.getByTestId('channel-list-right');
      fireEvent.click(within(channelList).getByTestId('Computed Math add'));
      const dialog = wrapper.container.getElementsByClassName('modal add-math-dialog')[0] as HTMLElement;
      fireEvent.click(within(dialog).getByTestId(`dlg-show-channel ${ID_TRAJ_VEL}`));
      fireEvent.click(within(dialog).getByTestId(`dlg-show-channel ${ID_TRAJ_ACCEL}`));
      fireEvent.click(within(dialog).getByText('Apply'));

      // The channel should now be in the channel list.
      const elements = await within(channelList).findAllByTestId(/channel-list-subtitle-.*/);
      const content = elements.map(e => e.innerHTML);
      expect(content).toEqual([
        getMathChannelSubtitle(MathChannelNames.TRAJECTORY_VEL)!,
        getMathChannelSubtitle(MathChannelNames.TRAJECTORY_ACCEL)!,
      ]);
    });


    it('does not display the add I/O button when there are no I/O pins', () => {
      const channelList = wrapper.getByTestId('channel-list-right');
      expect(within(channelList).queryByText(ADD_IO_BUTTON_LABEL)).toBeNull();
    });


    describe('with existing recordings', () => {
      let store: AppStore;

      beforeEach(async () => {
        store = TestOscilloscope.testStore;
        const device = _.sample(selectDevices(store.getState()))!;
        connectionViewMockInstance.props.onSelect(device.key);
        await waitUntilPass(() => expect(wrapper.queryByTestId('Data Source add')).not.toBeNull());

        // Add two math channels.
        const math = _.cloneDeep(extractMathChannels(selectAllChannels(store.getState())));
        math.find(ch => ch.id === ID_TRAJ_VEL)!.showInUi = true;
        math.find(ch => ch.id === ID_FOLLOW_ERR)!.showInUi = true;
        store.dispatch(actions.closeAddMathDialog(math));

        // Create two recordings.
        await doRecording(store);
        await selectRecordingTab(wrapper, NEW_RECORDING_TAB_LABEL);
        await doRecording(store);
        expect(selectSavedRecordings(store.getState()).length).toEqual(2);
      });


      it('allows deleting recordings', async () => {
        fireEvent.click(within(wrapper.getByTestId('recording-tab Recording 1')).getAllByRole('button')[0]);
        await waitUntilPass(() => {
          const recordings = selectSavedRecordings(store.getState());
          expect(recordings).toHaveLength(1);
        });

        fireEvent.click(within(wrapper.getByTestId('recording-tab Recording 2')).getAllByRole('button')[0]);
        await waitUntilPass(() => {
          const recordings = selectSavedRecordings(store.getState());
          expect(recordings).toHaveLength(0);
        });
      });


      describe('and shared settings disabled', () => {
        test('changing channel visibility affects only one recording', async () => {
          await selectRecordingTab(wrapper, 'Recording 1');
          fireEvent.click(wrapper.getByTestId(`hide-channel-${ID_TRAJ_VEL}`));
          wrapper.getByTestId(`hide-channel-${ID_POS}`);
          wrapper.getByTestId(`hide-channel-${ID_ENC_POS}`);
          wrapper.getByTestId(`show-channel-${ID_TRAJ_VEL}`);
          wrapper.getByTestId(`hide-channel-${ID_FOLLOW_ERR}`);
          await selectRecordingTab(wrapper, 'Recording 2');
          wrapper.getByTestId(`hide-channel-${ID_POS}`);
          wrapper.getByTestId(`hide-channel-${ID_ENC_POS}`);
          wrapper.getByTestId(`hide-channel-${ID_TRAJ_VEL}`);
          wrapper.getByTestId(`hide-channel-${ID_FOLLOW_ERR}`);
        });


        test('changing channel color affects only one recording', async () => {
          await selectRecordingTab(wrapper, 'Recording 1');
          colorChangeCallbacks[`color-channel-${ID_TRAJ_VEL}`]('#ffffff');
          await waitUntilPass(() =>
            expect(wrapper.getByTestId(`color-channel-${ID_TRAJ_VEL}`).style.backgroundColor)
              .toEqual('rgb(255, 255, 255)'));
          expect(wrapper.getByTestId(`color-channel-${ID_POS}`).style.backgroundColor).not.toEqual('rgb(255, 255, 255)');
          expect(wrapper.getByTestId(`color-channel-${ID_ENC_POS}`).style.backgroundColor).not.toEqual('rgb(255, 255, 255)');
          expect(wrapper.getByTestId(`color-channel-${ID_FOLLOW_ERR}`).style.backgroundColor).not.toEqual('rgb(255, 255, 255)');
          await selectRecordingTab(wrapper, 'Recording 2');
          expect(wrapper.getByTestId(`color-channel-${ID_POS}`).style.backgroundColor).not.toEqual('rgb(255, 255, 255)');
          expect(wrapper.getByTestId(`color-channel-${ID_ENC_POS}`).style.backgroundColor).not.toEqual('rgb(255, 255, 255)');
          expect(wrapper.getByTestId(`color-channel-${ID_TRAJ_VEL}`).style.backgroundColor)
            .not.toEqual('rgb(255, 255, 255)');
          expect(wrapper.getByTestId(`color-channel-${ID_FOLLOW_ERR}`).style.backgroundColor).not.toEqual('rgb(255, 255, 255)');
        });


        test('changing Y axis affects only one recording', async () => {
          await selectRecordingTab(wrapper, 'Recording 1');
          fireEvent.click(wrapper.getByTestId(`right-channel-${ID_TRAJ_VEL}`));
          expect(wrapper.getByTestId(`left-channel-${ID_POS}`).classList.contains('activated')).toBeTruthy();
          expect(wrapper.getByTestId(`left-channel-${ID_ENC_POS}`).classList.contains('activated')).toBeTruthy();
          expect(wrapper.getByTestId(`left-channel-${ID_TRAJ_VEL}`).classList.contains('activated')).toBeFalsy();
          expect(wrapper.getByTestId(`left-channel-${ID_FOLLOW_ERR}`).classList.contains('activated')).toBeFalsy();
          expect(wrapper.getByTestId(`right-channel-${ID_POS}`).classList.contains('activated')).toBeFalsy();
          expect(wrapper.getByTestId(`right-channel-${ID_ENC_POS}`).classList.contains('activated')).toBeFalsy();
          expect(wrapper.getByTestId(`right-channel-${ID_TRAJ_VEL}`).classList.contains('activated')).toBeTruthy();
          expect(wrapper.getByTestId(`right-channel-${ID_FOLLOW_ERR}`).classList.contains('activated')).toBeTruthy();
          await selectRecordingTab(wrapper, 'Recording 2');
          expect(wrapper.getByTestId(`left-channel-${ID_POS}`).classList.contains('activated')).toBeTruthy();
          expect(wrapper.getByTestId(`left-channel-${ID_ENC_POS}`).classList.contains('activated')).toBeTruthy();
          expect(wrapper.getByTestId(`left-channel-${ID_TRAJ_VEL}`).classList.contains('activated')).toBeTruthy();
          expect(wrapper.getByTestId(`left-channel-${ID_FOLLOW_ERR}`).classList.contains('activated')).toBeFalsy();
        });
      });


      describe('and shared settings enabled', () => {
        beforeEach(() => {
          fireEvent.click(wrapper.getByText(SHARE_SETTINGS_LABEL));
        });


        test('changing channel visibility affects all recordings', async () => {
          await selectRecordingTab(wrapper, 'Recording 1');
          fireEvent.click(wrapper.getByTestId(`hide-channel-${ID_TRAJ_VEL}`));
          wrapper.getByTestId(`show-channel-${ID_TRAJ_VEL}`);
          await selectRecordingTab(wrapper, 'Recording 2');
          wrapper.getByTestId(`show-channel-${ID_TRAJ_VEL}`);
        });


        test('changing channel color affects all recordings', async () => {
          await selectRecordingTab(wrapper, 'Recording 1');
          colorChangeCallbacks[`color-channel-${ID_TRAJ_VEL}`]('#ffffff');
          await waitUntilPass(() =>
            expect(wrapper.getByTestId(`color-channel-${ID_TRAJ_VEL}`).style.backgroundColor)
              .toEqual('rgb(255, 255, 255)'));
          await selectRecordingTab(wrapper, 'Recording 2');
          expect(wrapper.getByTestId(`color-channel-${ID_TRAJ_VEL}`).style.backgroundColor).toEqual('rgb(255, 255, 255)');
        });


        test('changing Y axis affects all recordings', async () => {
          await selectRecordingTab(wrapper, 'Recording 1');
          fireEvent.click(wrapper.getByTestId(`right-channel-${ID_TRAJ_VEL}`));
          expect(wrapper.getByTestId(`left-channel-${ID_TRAJ_VEL}`).classList.contains('activated')).toBeFalsy();
          expect(wrapper.getByTestId(`right-channel-${ID_TRAJ_VEL}`).classList.contains('activated')).toBeTruthy();
          await selectRecordingTab(wrapper, 'Recording 2');
          expect(wrapper.getByTestId(`left-channel-${ID_TRAJ_VEL}`).classList.contains('activated')).toBeFalsy();
          expect(wrapper.getByTestId(`right-channel-${ID_TRAJ_VEL}`).classList.contains('activated')).toBeTruthy();
        });


        test('adding a math channel affects all recordings and fills in chart data', async () => {
          await selectRecordingTab(wrapper, 'Recording 1');
          const channelList = wrapper.getByTestId('channel-list-right');
          expect(within(channelList).queryAllByTestId(`hide-channel-${ID_TRAJ_ACCEL}`).length).toBe(0);

          // Add a math channel.
          fireEvent.click(wrapper.getByTestId('Computed Math add'));
          const dialog = wrapper.getByTestId('add-math-dialog');
          fireEvent.click(within(dialog).getByTestId(`dlg-show-channel ${ID_TRAJ_ACCEL}`));
          fireEvent.click(within(dialog).getByText('Apply'));

          // Check that it is enabled in both recordings.
          within(channelList).getByTestId(`hide-channel-${ID_TRAJ_ACCEL}`);
          await selectRecordingTab(wrapper, 'Recording 2');
          within(channelList).getByTestId(`hide-channel-${ID_TRAJ_ACCEL}`);

          // Check that both recordings have chart data for this channel.
          await waitUntilPass(() => {
            for (const recording of selectSavedRecordings(store.getState())) {
              const channel = recording.channels.find(ch => ch.id === ID_TRAJ_ACCEL);
              expect(channel?.convertedData.length).toBeGreaterThan(0);
            }
          });
        });
      });
    });
  });
});


describe('With a multi-axis device', () => {
  beforeEach(() => {
    container = createContainer();
    container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockMultiAxis);
    wrapper = render(<TestOscilloscope/>);
  });


  afterEach(() => {
    wrapper.unmount();
    wrapper = null!;

    destroyContainer();
    container = null!;
  });


  describe('the channels tab', () => {
    beforeEach(async () => {
      const store = TestOscilloscope.testStore;
      mockSingleDeviceWithPeripherals(store, {
        peripheralCount: 2,
        modifier: addDeviceScopeSettingsInfo,
      });
      const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => wrapper.getByTestId('Data Source add'));
    });


    it('has a tab for each axis in the add settings dialog', async () => {
      const dialog = openAddSettingsDialog(wrapper);
      const tabs = (await within(dialog).findAllByTestId(/data-source-axis-tab-.*/)).map(element => element.children[0].innerHTML);
      expect(tabs).toContain('X-MCC2');
      expect(tabs).toContain('LHM025A-T3');
    });


    it('has a tab for each axis in the add math dialog', async () => {
      expect(wrapper.getAllByTestId(/close-channel-/)).toHaveLength(4);

      // Click the add math button and verify the dialog is shown.
      fireEvent.click(wrapper.getByTestId('Computed Math add'));

      // Check the content of the dialog.
      const dialog = wrapper.getByTestId('add-math-dialog');

      const tabs = (await within(dialog).findAllByTestId(/math-axis-tab-.*/)).map(element => element.children[0].innerHTML);
      expect(tabs).not.toContain('X-MCC2');
      expect(tabs).toContain('Axis 1');
      expect(tabs).toContain('Axis 2');
    });


    it('groups settings by axis', async () => {
      // Add a controller setting to the default axes selections.
      fireEvent.click(wrapper.getByTestId('Data Source add'));
      await waitUntilPass(() => {
        fireEvent.click(wrapper.getByTestId('show-all-settings'));
        fireEvent.click(wrapper.getByTestId('data-source-axis-tab-0'));
        fireEvent.click(wrapper.getByTestId(`dlg-show-channel ${makeSettingChannelId('scope.delay', 0)}`));
      });
      fireEvent.click(within(wrapper.getByTestId('add-data-source-dialog')).getByText('Apply'));

      // Verify setting grouping.
      await waitUntilPass(() => expect(wrapper.getAllByTestId(/close-channel-/)).toHaveLength(5));
      const channelList = wrapper.getByTestId('channel-card-Data Source');
      const sections = htmlCollectionToArray(channelList.children[1].children)
        .map(elem => elem as HTMLElement)
        .filter(elem => elem.tagName === 'SPAN' || elem.className.includes('channel-group'));

      expect(sections[0].innerHTML).toEqual('X-MCC2');
      expect((await within(sections[1]).findAllByTestId(/channel-list-subtitle.*/)).map(e => e.innerHTML))
        .toEqual(['scope.delay']);
      expect(sections[2].innerHTML).toMatch(/Axis 1.+LHM025A-T3/);
      expect((await within(sections[3]).findAllByTestId(/channel-list-subtitle.*/)).map(e => e.innerHTML))
        .toEqual(['pos', 'encoder.pos']);
      expect(sections[4].innerHTML).toMatch(/Axis 2.+LHM025A-T3/);
      expect((await within(sections[5]).findAllByTestId(/channel-list-subtitle.*/)).map(e => e.innerHTML))
        .toEqual(['pos', 'encoder.pos']);
    });


    describe('the add I/O dialog', () => {
      beforeEach(() => {
        openAddIoDialog(wrapper);
      });

      it('shows the expected channels', () => {
        within(wrapper.getByTestId('pins-list')).getByText('DI - Pin 1');
        within(wrapper.getByTestId('pins-list')).getByText('DI - Pin 2');
        within(wrapper.getByTestId('pins-list')).getByText('DI - Pin 3');
        within(wrapper.getByTestId('pins-list')).getByText('DI - Pin 4');
        within(wrapper.getByTestId('pins-list')).getByText('DO - Pin 1');
        within(wrapper.getByTestId('pins-list')).getByText('DO - Pin 2');
        within(wrapper.getByTestId('pins-list')).getByText('DO - Pin 3');
        within(wrapper.getByTestId('pins-list')).getByText('DO - Pin 4');

        fireEvent.click(wrapper.getByTestId('analog-io-tab'));
        within(wrapper.getByTestId('pins-list')).getByText('AO - Pin 1');
        within(wrapper.getByTestId('pins-list')).getByText('AI - Pin 1');
        within(wrapper.getByTestId('pins-list')).getByText('AI - Pin 2');
        within(wrapper.getByTestId('pins-list')).getByText('AI - Pin 3');
        within(wrapper.getByTestId('pins-list')).getByText('AI - Pin 4');
      });


      it('disallows selecting too many channels but allows combining digital', async () => {
        // Add all five analog channels.
        fireEvent.click(wrapper.getByTestId('analog-io-tab'));
        await waitUntilPass(() => {
          fireEvent.click(wrapper.getByTestId(`dlg-show-channel ${makeIoChannelId('ao', 1)}`));
        });
        fireEvent.click(wrapper.getByTestId(`dlg-show-channel ${makeIoChannelId('ai', 1)}`));
        fireEvent.click(wrapper.getByTestId(`dlg-show-channel ${makeIoChannelId('ai', 2)}`));
        fireEvent.click(wrapper.getByTestId(`dlg-show-channel ${makeIoChannelId('ai', 3)}`));
        fireEvent.click(wrapper.getByTestId(`dlg-show-channel ${makeIoChannelId('ai', 4)}`));

        // Switch to the digital tab and add one input for a total of six channels.
        fireEvent.click(wrapper.getByTestId('digital-io-tab'));
        await waitUntilPass(() => {
          fireEvent.click(wrapper.getByTestId(`dlg-show-channel ${makeIoChannelId('di', 1)}`));
        });

        // The digital outputs should now be disabled but we should be able to add more digital inputs.
        expect(wrapper.getByTestId(`dlg-show-channel ${makeIoChannelId('di', 2)}`)).not.toHaveAttribute('disabled');
        expect(wrapper.getByTestId(`dlg-show-channel ${makeIoChannelId('do', 1)}`)).toHaveAttribute('disabled');
      });
    });
  });
});
