import React from 'react';
import { LinkClasses, NumericInput, Text, PopUp } from '@zaber/react-library';
import { useSelector } from 'react-redux';
import { Acceleration } from '@zaber/motion';
import { settingsMetadata } from '@zaber/device-db-metadata';

import type { ServoTuningSetting, ServoTuningSettings } from '../types';
import { selectRecommendedAccelMetersPerSecond2 } from '../selectors';
import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import type { EntityKey } from '../../keys';
import { convertNullable, getUnitLabel, getUnits } from '../../units';
import { useUnitInfo } from '../../units/hooks';
import { MeasurementInputEditor } from '../../units/MeasurementInputEditor';

type SettingMessage = Pick<React.ComponentProps<typeof NumericInput>, 'message' | 'status' | 'hideMessage'> | undefined;

interface SettingProps  {
  entity: EntityKey;
  setting: ServoTuningSetting;
  help: React.ReactNode;
  message?: SettingMessage;
  align: PopUp.Align;
}

const Setting: React.FC<SettingProps> = ({ entity, setting, help, message: settingMessage, align }) => {
  const actions = useActions(actionDefinitions);
  const message: SettingMessage = setting.error ? { message: setting.error, status: 'error', hideMessage: false } : settingMessage;
  return <MeasurementInputEditor
    unitFrom={{ setting: setting.name, entity }}
    measure={setting.measure}
    onChange={update => actions.updateSetting(setting.name, update)}
    mode={setting.mode}
    labelContent={{ label: settingsMetadata[setting.name].humanName ?? setting.name, help }}
    displayPrecision={2}
    className="servo-tuner-setting-editor"
    align={align}
    placeholderMessage={settingMessage?.message}
    {...message}
  />;
};

interface Props {
  entity: EntityKey;
  settings: ServoTuningSettings;
}

/** Returns true if inputs are within 1 part in 1000 of each other */
function numbersAreClose(n1: number | null, n2?: number | null) {
  if (n1 == null || n2 == null) { return false }
  const diff = n1 - n2;
  const diffFrac = Math.abs(diff / n1);
  return diffFrac < 0.001;
}

export const Settings: React.FC<Props> = ({ entity, settings }) => {
  const actions = useActions(actionDefinitions);
  const recommendedAccelMPerS2 = useSelector(selectRecommendedAccelMetersPerSecond2);
  const accelUnitInfo = useUnitInfo({ setting: 'accel', entity });
  const { accel, 'cloop.settle.period': period, 'cloop.settle.tolerance': tolerance } = settings;
  const accelRecommendation = (): SettingMessage => {
    const units = getUnits(accel.measure);
    const recommendedAccel = convertNullable(accelUnitInfo, recommendedAccelMPerS2, Acceleration.METRES_PER_SECOND_SQUARED, units);
    const hideMessage = recommendedAccel == null || numbersAreClose(recommendedAccel, accel.measure?.value);
    const recommendation = recommendedAccel ? recommendedAccel.toPrecision(4) : '0.000';
    const message = <>
      Recommended Acceleration: <Text
        t={Text.Type.Helper} className={LinkClasses.Default} data-testid="recommended-accel"
        onClick={() => actions.updateSetting('accel', {
          value: {
            value: Number.parseFloat(recommendation),
            units,
          },
          mode: 'write',
        })}
      >
        {recommendation} {getUnitLabel(units)}
      </Text>
    </>;
    if (hideMessage) {
      return { message, hideMessage };
    } if (accel.measure?.value == null || accel.measure.value <= (recommendedAccel ?? 0)) {
      return { status: 'info', message, hideMessage };
    } else {
      return { status: 'warning', message, hideMessage };
    }
  };

  return  <div className="settings" data-testid="servo-tuner-settings">
    <Setting
      entity={entity} setting={accel}
      help={<>
        The acceleration and deceleration used when changing speeds. In order for the control loop to be accurate,
        it must have enough force capability to keep up with the trajectory's acceleration. For simple tuning,
        we will provide a recommended maximum acceleration value.
      </>}
      message={accelRecommendation()}
      align="start"
    />
    <div className="separator"/>
    <Setting
      entity={entity} setting={tolerance}
      help={<>The maximum acceptable error for the axis to be considered settled after a movement</>}
      align="center"
    />
    <div className="separator"/>
    <Setting
      entity={entity} setting={period}
      help={<>The duration the axis position must be within tolerance to be considered settled after a movement</>}
      align="end"
    />
  </div>;
};
