const fs = require('fs').promises;
const child = require('child_process');

const INKSCAPE = process.env.INKSCAPE || 'inkscape';

const sizes = [
  16,
  32,
  48,
  64,
  96,
  128,
  192,
  256,
  512,
  1024,
];
const MAC_SIZES = [
  16,
  32,
  64,
  128,
  256,
  512,
];
const MAC_BORDER = '--export-area=-31:-31:287:287';

async function main() {
  for (const size of sizes) {
    child.execSync(`"${INKSCAPE}" --export-type="png" --export-filename="icons/${size}x${size}.png" -w ${size} -h ${size} icons/logo.svg`);
  }

  for (const size of [16, 32]) {
    await fs.copyFile(`icons/${size}x${size}.png`, `icons/favicon-${size}x${size}.png`);
  }

  await fs.mkdir('icons/mac', { recursive: true });
  for (const size of MAC_SIZES) {
    child.execSync([
      `"${INKSCAPE}" --export-type="png" --export-filename="icons/mac/icon_${size}x${size}.png"`,
      `${MAC_BORDER} -w ${size} -h ${size} icons/logo.svg`,
    ].join(' '));
    child.execSync([
      `"${INKSCAPE}" --export-type="png" --export-filename="icons/mac/icon_${size}x${size}@2x.png"`,
      `${MAC_BORDER} -w ${size * 2} -h ${size * 2} icons/logo.svg`,
    ].join(' '));
  }
}

main().catch(err => {
  console.error(err);
  process.exit(1);
});
