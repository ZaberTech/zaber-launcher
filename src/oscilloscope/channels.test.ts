import _ from 'lodash';
import { CircularBuffer } from 'mnemonist';

import {
  MATH_CHANNELS,
  addChannelAndDependents,
  addOrReplaceChannelAndUpdateDependents,
  channelHasDependency,
  channelSorter,
  createChannelForIo,
  createChannelForSetting,
  extractMathChannelsToCalculate,
  getAllChannelDependencies,
  getChannelRootDependencies,
  makeChannelIdWithoutAxis,
  makeMathChannelId,
  makeSettingChannelId,
  numFwChannelsUsed,
  removeChannelAndDependents,
  updateMathChannels,
} from './channels';
import { derivative } from './math';
import { MATH_BUFFER_SIZE } from './sagas';
import {
  Channel,
  CHANNEL_DEFAULTS,
  MathChannelNames,
  RecordingBuffer,
  Sample
} from './types';
import { UNSET_COLOR } from './colors';


const TEST_CHANNEL_DEFAULTS: Channel = {
  ...CHANNEL_DEFAULTS,
  color: '#000000',
  id: '',
  name: '',
  axis: 1,
  title: '',
  subtitle: '',
  showInUi: true,
};


describe('Channel sorter', () => {
  it('sorts by number of dependencies', () => {
    const channels: Channel[] = [{
      ...TEST_CHANNEL_DEFAULTS,
      id: 'c',
      dependencyIds: ['b', 'a'],
    }, {
      ...TEST_CHANNEL_DEFAULTS,
      id: 'b',
      dependencyIds: ['a'],
    }, {
      ...TEST_CHANNEL_DEFAULTS,
      id: 'a',
    }];

    channels.sort(channelSorter);
    expect(channels.map(ch => ch.id)).toEqual(['a', 'b', 'c']);
  });


  it('preserves order when there are no dependencies', () => {
    const channels: Channel[] = [{
      ...TEST_CHANNEL_DEFAULTS,
      id: 'c',
    }, {
      ...TEST_CHANNEL_DEFAULTS,
      id: 'a',
    }, {
      ...TEST_CHANNEL_DEFAULTS,
      id: 'b',
    }];

    channels.sort(channelSorter);
    expect(channels.map(ch => ch.id)).toEqual(['c', 'a', 'b']);
  });


  it('uses hardcoded order for math channels with the same number of dependencies', () => {
    const mvel = makeMathChannelId(MathChannelNames.MEASURED_VEL, 1);
    const tvel = makeMathChannelId(MathChannelNames.TRAJECTORY_VEL, 1);
    const acc = makeMathChannelId(MathChannelNames.TRAJECTORY_ACCEL, 1);

    const channels: Channel[] = [{
      ...TEST_CHANNEL_DEFAULTS,
      id: mvel,
      name: MathChannelNames.MEASURED_VEL,
      dependencyIds: ['encoder.pos'],
    }, {
      ...TEST_CHANNEL_DEFAULTS,
      id: tvel,
      name: MathChannelNames.TRAJECTORY_VEL,
      dependencyIds: ['pos'],
    }, {
      ...TEST_CHANNEL_DEFAULTS,
      id: acc,
      name: MathChannelNames.TRAJECTORY_ACCEL,
      dependencyIds: [MathChannelNames.TRAJECTORY_VEL],
    }];

    channels.sort(channelSorter);
    expect(channels.map(ch => ch.id)).toEqual(
      [tvel, acc, mvel]);
  });
});


describe('Adding and removing channels', () => {
  it('by name adds all dependents including those with multiple dependencies', () => {
    let channels = addChannelAndDependents([], 'pos', 1);
    expect(channels.map(ch => ch.name)).toEqual(['pos', MathChannelNames.TRAJECTORY_VEL, MathChannelNames.TRAJECTORY_ACCEL]);
    expect(channels.map(ch => ch.dependencyIds)).toEqual([
      [],
      [makeChannelIdWithoutAxis('setting', 'pos')],
      [makeChannelIdWithoutAxis('math', MathChannelNames.TRAJECTORY_VEL)]]);

    channels = addChannelAndDependents(channels, 'encoder.pos', 1);
    expect(channels.map(ch => ch.name)).toEqual(
      ['pos', 'encoder.pos',
        MathChannelNames.TRAJECTORY_VEL, MathChannelNames.TRAJECTORY_ACCEL,
        MathChannelNames.MEASURED_VEL, MathChannelNames.MEASURED_ACCEL,
        MathChannelNames.FOLLOW_ERR, MathChannelNames.FOLLOW_ERR_VEL, MathChannelNames.FOLLOW_ERR_ACCEL]);
  });


  it('by instance adds all dependents including those with multiple dependencies', () => {
    let channels = addOrReplaceChannelAndUpdateDependents([], createChannelForSetting('pos', 1));
    expect(channels.map(ch => ch.name)).toEqual(['pos', MathChannelNames.TRAJECTORY_VEL, MathChannelNames.TRAJECTORY_ACCEL]);
    expect(channels.map(ch => ch.dependencyIds)).toEqual([
      [],
      [makeChannelIdWithoutAxis('setting', 'pos')],
      [makeChannelIdWithoutAxis('math', MathChannelNames.TRAJECTORY_VEL)]]);

    channels = addChannelAndDependents(channels, 'encoder.pos', 1);
    expect(channels.map(ch => ch.name)).toEqual(
      ['pos', 'encoder.pos',
        MathChannelNames.TRAJECTORY_VEL, MathChannelNames.TRAJECTORY_ACCEL,
        MathChannelNames.MEASURED_VEL, MathChannelNames.MEASURED_ACCEL,
        MathChannelNames.FOLLOW_ERR, MathChannelNames.FOLLOW_ERR_VEL, MathChannelNames.FOLLOW_ERR_ACCEL]);
  });


  it('by instance replaces the channel and does not affect dependencies', () => {
    let channels = addChannelAndDependents(addChannelAndDependents([], 'pos', 1), 'encoder.pos', 1);
    const originalIds = channels.map(ch => ch.id);
    const originalPos = { ...(channels.find(ch => ch.name === 'pos')!) };
    const newPos = createChannelForSetting('pos', 1);
    newPos.color = '#000000';
    newPos.title = 'Foo!';

    channels = addOrReplaceChannelAndUpdateDependents(channels, newPos);
    expect(channels.map(ch => ch.id)).toEqual(originalIds);
    expect(channels.find(ch => ch.name === 'pos')).not.toMatchObject(originalPos);
    expect(channels.find(ch => ch.name === 'pos')!.title).toBe('Foo!');
  });


  it('also removes dependencies', () => {
    const expectedids = ['pos'].map(id => makeSettingChannelId(id, 1))
      .concat([MathChannelNames.TRAJECTORY_VEL, MathChannelNames.TRAJECTORY_ACCEL].map(id => makeMathChannelId(id, 1)));
    let channels = addChannelAndDependents(addChannelAndDependents([], 'pos', 1), 'encoder.pos', 1);
    expect(channels.map(ch => ch.id)).not.toEqual(expectedids);
    channels = removeChannelAndDependents(channels, makeSettingChannelId('encoder.pos', 1));
    expect(channels.map(ch => ch.id)).toEqual(expectedids);
  });
});


describe('Dependency checks', () => {
  const a = makeSettingChannelId('a', 1);
  const b = makeSettingChannelId('b', 1);
  const c = makeSettingChannelId('c', 1);
  const d = makeSettingChannelId('d', 1);
  const e = makeSettingChannelId('e', 1);

  const channels: Channel[] = [{
    ...TEST_CHANNEL_DEFAULTS,
    id: a,
    dependencyIds: [makeChannelIdWithoutAxis('setting', 'b'), makeChannelIdWithoutAxis('setting', 'c')],
  }, {
    ...TEST_CHANNEL_DEFAULTS,
    id: b,
    dependencyIds: [makeChannelIdWithoutAxis('setting', 'c'), makeChannelIdWithoutAxis('setting', 'e')],
  }, {
    ...TEST_CHANNEL_DEFAULTS,
    id: c,
  }, {
    ...TEST_CHANNEL_DEFAULTS,
    id: d,
    dependencyIds: [makeChannelIdWithoutAxis('setting', 'e')],
  }, {
    ...TEST_CHANNEL_DEFAULTS,
    id: e,
  }];

  const channelMap = _.keyBy(channels, 'id');


  it('answers dependency queries', () => {
    expect(channelHasDependency(channelMap[a], b, channelMap)).toBeTruthy();
    expect(channelHasDependency(channelMap[a], e, channelMap)).toBeTruthy();
    expect(channelHasDependency(channelMap[d], e, channelMap)).toBeTruthy();
    expect(channelHasDependency(channelMap[a], d, channelMap)).toBeFalsy();
    expect(channelHasDependency(channelMap[b], a, channelMap)).toBeFalsy();
  });


  it('identifies dependency roots', () => {
    expect(getChannelRootDependencies(channelMap[a], channelMap)).toEqual([c, e]);
    expect(getChannelRootDependencies(channelMap[c], channelMap)).toEqual([c]);
    expect(getChannelRootDependencies(channelMap[d], channelMap)).toEqual([e]);
  });


  it('identifies all dependencies', () => {
    expect([...getAllChannelDependencies(channelMap[a], channelMap)].sort()).toEqual([a, b, c, e]);
    expect([...getAllChannelDependencies(channelMap[d], channelMap)].sort()).toEqual([d, e]);
  });
});


describe('Updating math channel data', () => {
  it('automatically computes intermediate results even if not visible', () => {
    const posId = makeSettingChannelId('pos', 1);
    const velId = makeMathChannelId(MathChannelNames.TRAJECTORY_VEL, 1);
    const accId = makeMathChannelId(MathChannelNames.TRAJECTORY_ACCEL, 1);
    const channels: Channel[] = [{
      ...TEST_CHANNEL_DEFAULTS,
      id: posId,
    }, {
      ...TEST_CHANNEL_DEFAULTS,
      id: velId,
      dependencyIds: [makeChannelIdWithoutAxis('setting', 'pos')],
      showInChart: false,
      showInUi: false,
      mathFunc: derivative,
    }, {
      ...TEST_CHANNEL_DEFAULTS,
      id: accId,
      dependencyIds: [makeChannelIdWithoutAxis('math', MathChannelNames.TRAJECTORY_VEL)],
      mathFunc: derivative,
    }];

    const data = [{ time: 0, value: 0 }, { time: 1000, value: 1 }, { time: 2000, value: 3 },
      { time: 3000, value: 5 }, { time: 4000, value: 9 }];

    const output: RecordingBuffer = {
      [posId]: CircularBuffer.from(data, Array),
      [velId]: new CircularBuffer<Sample>(Array, 10),
      [accId]: new CircularBuffer<Sample>(Array, 10),
    };

    const input: RecordingBuffer = {
      [posId]: new CircularBuffer<Sample>(Array, MATH_BUFFER_SIZE),
      [velId]: new CircularBuffer<Sample>(Array, MATH_BUFFER_SIZE),
      [accId]: new CircularBuffer<Sample>(Array, MATH_BUFFER_SIZE),
    };

    const channelsToUpdate = extractMathChannelsToCalculate(channels, _.keyBy(channels, 'id'));
    for (const sample of data) {
      input[posId].push(sample);
      updateMathChannels(channelsToUpdate, input, 2000, output);
    }

    expect(output[velId].toArray()).toEqual(
      [{ time: 500, value: 1 }, { time: 1000, value: 1.5 }, { time: 2000, value: 2 }, { time: 3000, value: 3 }]);
    expect(output[accId].toArray()).toEqual([{ time: 750, value: 1 }, { time: 1250, value: 2 / 3 }, { time: 2000, value: 0.75 }]);
  });
});


describe('Channel counting', () => {
  const settingChannel = createChannelForSetting('pos', 0);
  const mathChannel: Channel = {
    ...CHANNEL_DEFAULTS,
    ...MATH_CHANNELS[0],
    id: makeMathChannelId(MATH_CHANNELS[0].name, 0),
    axis: 1,
    color: UNSET_COLOR,
  };
  const ioChannel1 = createChannelForIo('di', 1);
  const ioChannel2 = createChannelForIo('di', 2);

  it('ignores math channels', () => {
    expect(numFwChannelsUsed([settingChannel, ioChannel1])).toBe(2);
    expect(numFwChannelsUsed([settingChannel, mathChannel, ioChannel1])).toBe(2);
  });

  it('combines digital I/Os', () => {
    expect(numFwChannelsUsed([ioChannel1, ioChannel2])).toBe(1);
  });
});

