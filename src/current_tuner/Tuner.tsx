import React from 'react';
import { useSelector } from 'react-redux';
import { Button, Text, NoticeBanner, ButtonRow, Flex } from '@zaber/react-library';
import { useActions } from '@zaber/toolbox/lib/redux';

import { SingleDeviceTable } from '../connection_manager/device_table/SingleDeviceTable';
import { SingleAxisTable } from '../connection_manager/device_table/SingleAxisTable';
import { getAxisNumber } from '../keys';
import { ExternalLink, ExpandableSection, NewWindowLink } from '../components';

import { actions as actionsDefinition } from './actions';
import {
  selectCurrentAxisOrDeviceState, selectDeviceInfoWithAxes, selectResetOrApplyDisabled,
  selectDefaultDisabled, selectSettingDriver, selectDriverEnabled
} from './selectors';
import { SettingsTable } from './components/SettingsTable';
import { TuningControl } from './components/TuningControl';
import { tuningManualContent } from './components/content';
import { SLIDER_APPLIED_CONFIRMATION_MESSAGE, Sliders, tuningGuidelineUrl } from './types';
import { TunerOneTimeMessage } from './TunerOneTimeMessage';
import { ConnectionsView } from '../connection_manager';

export const Tuner: React.FC = () => {
  const device = useSelector(selectDeviceInfoWithAxes)!;
  const axisOrDevice = useSelector(selectCurrentAxisOrDeviceState)!;
  const actions = useActions(actionsDefinition);

  const disableResetOrApply = useSelector(selectResetOrApplyDisabled);
  const disableDefault = useSelector(selectDefaultDisabled);

  const driverEnabled = useSelector(selectDriverEnabled);
  const settingDriver = useSelector(selectSettingDriver);

  const { key, apply, targetIctrlSettings, actualIctrlSettings, slidersRange } = axisOrDevice;
  const isPending = apply?.state === 'pending';
  const sliderPositions = axisOrDevice.sliderPositions!;

  function updateSliderValues(slider: Partial<Sliders>) {
    actions.updateSliders(key, slider);
  }

  const links = {
    basicControls: <NewWindowLink url={`/basic-controls?${ConnectionsView.SELECT_PARAM}=${device.key}`}>
      Basic Controls
    </NewWindowLink>,
    terminal: <NewWindowLink url={`/terminal?${ConnectionsView.SELECT_PARAM}=${device.key}`}>
      Terminal
    </NewWindowLink>,
  };

  return (<div className="tuner">
    {device.isController
      ? <SingleAxisTable axis={device.axes[getAxisNumber(key) - 1]} device={device}/>
      : <SingleDeviceTable device={device}/>}

    <TunerOneTimeMessage/>

    <div className="tuning-controls-header">
      <Text t={Text.Type.H4}>
        Tuning Controls
      </Text>
      <ExternalLink url={tuningGuidelineUrl}>
        Tuning Guidelines
      </ExternalLink>
    </div>

    <TuningControl
      name="Back EMF Compensation"
      disabled={isPending}
      sliderValue={sliderPositions.backEMFCompensation}
      {...slidersRange!.backEMFCompensation}
      manual={tuningManualContent.backEMFCompensation}
      onChange={value => updateSliderValues({ backEMFCompensation: value })}/>
    <TuningControl
      name="Overall Gain"
      disabled={isPending}
      sliderValue={sliderPositions.overallGain}
      {...slidersRange!.overallGain}
      manual={tuningManualContent.overallGain}
      onChange={value => updateSliderValues({ overallGain: value })}/>
    <TuningControl
      name="Response Correction Speed"
      disabled={isPending}
      sliderValue={sliderPositions.responseCorrectionSpeed}
      {...slidersRange!.responseCorrectionSpeed}
      manual={tuningManualContent.responseCorrectionSpeed}
      onChange={value => updateSliderValues({ responseCorrectionSpeed: value })}/>

    {apply?.state === 'error' && <NoticeBanner className="banner">
      <Text>
        {apply.error}
      </Text>
    </NoticeBanner>}

    <ButtonRow className="buttons-container">
      <Button color={driverEnabled ? 'red' : 'grey-red'}
        disabled={driverEnabled == null || settingDriver}
        onClick={() => actions.setDriver(!driverEnabled)}>{driverEnabled ? 'Disable Driver' : 'Enable Driver'}</Button>
      <Flex.Spacer/>
      <Button color="red" disabled={isPending || disableResetOrApply} title="Apply current tuning values"
        onClick={() => actions.applySliderValues(key)}>
        Apply
      </Button>
      <Button color="grey" disabled={isPending || disableResetOrApply}
        title="Reset sliders to last applied values"
        onClick={() => updateSliderValues(axisOrDevice.appliedPositions!)}>Reset</Button>
      <Button color="grey" disabled={isPending || disableDefault} title="Reset sliders to suggested tuning values"
        onClick={() => updateSliderValues(axisOrDevice.defaultPositions!)}>
        Restore Defaults
      </Button>
    </ButtonRow>

    {apply?.state === 'success' &&
      <NoticeBanner type="success" className="banner" closer={() => actions.clearApplyResult(key)}>
        <div>{SLIDER_APPLIED_CONFIRMATION_MESSAGE}<Text>The details section below lists the current setting values.</Text></div>
        <div className="instruction-message">
          Use {links.basicControls} or {links.terminal} to test your device.
        </div>
      </NoticeBanner>}

    <ExpandableSection title="Details" titleExpanded="Collapse Tuning Settings"
      titleCollapsed="Expand Tuning Settings">
      <SettingsTable actualSettings={actualIctrlSettings} targetSettings={targetIctrlSettings}/>
    </ExpandableSection>
  </div>);
};
