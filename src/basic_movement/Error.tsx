import React, { useState } from 'react';
import classNames from 'classnames';
import { Text, Button, Icons } from '@zaber/react-library';
import { ascii } from '@zaber/motion';
import { match, P } from 'ts-pattern';

import { DRIVER_DISABLED_FLAGS, ProtocolManualFormatter } from '../protocol_manual';
import { makeString, tryAccess } from '../utils';
import type { WarningFlagManual } from '../protocol_manual/types';
import { useWarningFlagManual } from '../protocol_manual/hooks';
import type { EntityKey } from '../keys';

import { AxisError, AxisErrorOrigin, AxisErrorType } from './types';

interface Props {
  entity: EntityKey;
  error: AxisError;
  onDismiss: () => void;
  enableDriver: () => void;
}

const warningFlagHelpTitles: Record<string, string> = {
  [ascii.WarningFlags.NO_REFERENCE_POSITION]: 'Homing Required',
};

const DISMISS_ERRORS: AxisErrorOrigin[] = ['movement', 'set_driver'];

const Title: React.FC<{ error: AxisError; flag?: WarningFlagManual }> = ({ error, flag }) => {
  if (error.title) {
    return <>{error.title}</>;
  } else if (flag != null || error.origin === 'warning_flag') {
    const helpTitle = tryAccess(warningFlagHelpTitles, error.warningFlag);
    return <>
      {error.origin === 'movement' && 'Movement Failed: '}
      {flag ? `${flag.name} (${error.warningFlag})` : error.warningFlag}{helpTitle && ` - ${helpTitle}`}
    </>;
  } else if (error.origin === 'movement') {
    return <>Movement Failed</>;
  } else if (error.origin === 'connection') {
    return <>Cannot reach the device</>;
  }  else if (error.origin === 'set_driver') {
    return <>Cannot enable/disable driver</>;
  } else {
    throw new Error(`Invalid error ${makeString(error.origin)}`);
  }
};

export const ErrorView: React.FC<Props> = ({ entity, error, onDismiss, enableDriver }) => {
  const [isCollapsed, setCollapsed] = useState(true);
  const flagInfo = useWarningFlagManual(entity, error.warningFlag);
  return (
    <div className={classNames(error.type, 'error')}>
      <div className="header">
        <div className="description">
          {error.type === AxisErrorType.Note && <Icons.ErrorNote/>}
          {error.type === AxisErrorType.Warning && <Icons.ErrorWarning/>}
          {error.type === AxisErrorType.Fault && <Icons.ErrorFault/>}

          <Text className="title">
            <Title error={error} flag={flagInfo}/>
          </Text>

          <Text
            className="show-details"
            appearsClickable
            onClick={() => setCollapsed(!isCollapsed)}>
            {`${isCollapsed ? 'Show' : ' Hide'} Details...`}
          </Text>
        </div>
        {DISMISS_ERRORS.includes(error.origin) && <Button size="small" color="grey" onClick={onDismiss}>Dismiss</Button>}
        {error.warningFlag != null && DRIVER_DISABLED_FLAGS.includes(error.warningFlag) &&
          <Button size="small" color="grey" onClick={enableDriver}>Enable Driver</Button>}
      </div>

      {!isCollapsed && <div className="content">
        {match(flagInfo)
          .with({ html: P.string }, ({ html }) => <ProtocolManualFormatter html={html}/>)
          .otherwise(() => <p>{error.message}</p>)
        }
      </div>}
    </div>
  );
};
