import React, { useEffect } from 'react';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { Icons, Text } from '@zaber/react-library';

import { DEFAULT_POLL_INTERVAL } from '../types';
import { unitsById } from '../units';
import { useActions } from '../utils';

import { actions as actionDefinitions } from './actions';
import { selectUserPath, selectSelectedPointIndex } from './selectors';
import { isFirstPointRelative } from './utils';
import { Filename } from './Filename';


export const Table: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const userPath = useSelector(selectUserPath);
  const selectedPointIndex = useSelector(selectSelectedPointIndex);

  useEffect(() => {
    const polling = userPath && isFirstPointRelative(userPath) ? DEFAULT_POLL_INTERVAL : 'stop';
    actions.setPollDefault(polling);
    actions.pollDevicePosition('default');
  }, []);

  return <div className="table-view">
    <Filename/>
    <table data-testid="main-table">
      <thead>
        <tr key="header-row-1">
          <th></th>
          <th colSpan={2}>Time (s)</th>
          {(userPath?.series ?? []).map((series, i) => <th key={i} colSpan={2}>{series.name}</th>)}
          <th className="hidden"/>
        </tr>
        <tr key="header-row-2">
          <th>#</th>
          <th>Segment</th>
          <th>Accumulated</th>
          {(userPath?.series ?? []).map((__, i) => <React.Fragment key={i}>
            <th>Position ({unitsById[userPath?.positionUnit ?? '']?.ShortName ?? 'Native'})</th>
            <th>Velocity ({unitsById[userPath?.velocityUnit ?? '']?.ShortName ?? 'Native'})</th>
          </React.Fragment>)}
          <th className="hidden"/>
        </tr>
      </thead>
      <tbody>
        {_.range(0, userPath?.series[0].points.length ?? 0).map(i =>
          <tr key={i}
            className={classNames({ selected: i === selectedPointIndex })}
            onClick={() => {
              userPath?.series.forEach(series => actions.setSeriesDetailsVisible(series.name, true));
              actions.setSelection(i);
            }}>
            <td>
              {i + 1}
              {i === 0 && <Text>&emsp;S</Text>}
              {userPath!.series[0].points[i].relative && <Text>&emsp;R</Text>}
            </td>
            <td>{userPath!.series[0].points[i].dt.toLocaleString(undefined, { maximumFractionDigits: 3 })}</td>
            <td>{userPath!.series[0].points[i].time.toLocaleString(undefined, { maximumFractionDigits: 3 })}</td>
            {userPath!.series.map((series, j) => <React.Fragment key={j}>
              <td>{series.points[i].pos.toLocaleString(undefined, { maximumFractionDigits: 6 })}</td>
              <td>{series.points[i].vel.toLocaleString(undefined, { maximumFractionDigits: 6 })}</td>
            </React.Fragment>)}
            <td className="hidden">
              {selectedPointIndex != null && selectedPointIndex < userPath!.series[0].points.length - 1 && i === selectedPointIndex && <>
                <Icons.Plus
                  data-testid="table-add-point-button"
                  title="Add a new point after this one"
                  onClick={() => {
                    actions.addPoint(selectedPointIndex);
                    actions.commitEdit();
                    actions.setSelection(selectedPointIndex + 1);
                  }}/>
                {userPath!.series[0].points.length > 2 &&
                  <Icons.Trash
                    data-testid="table-delete-point-button"
                    title="Delete this point"
                    onClick={() => {
                      actions.deletePoint(selectedPointIndex);
                      actions.commitEdit();
                    }}/>}
              </>}
            </td>
          </tr>)}
      </tbody>
    </table>
    <div className="footnote">
      <Text t={Text.Type.Instruction}>R: Relative to previous point<br/>S: Start position</Text>
    </div>
  </div>;
};
