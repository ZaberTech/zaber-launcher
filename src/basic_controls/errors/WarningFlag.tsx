import React from 'react';
import { NoticeMessage, NoticeType } from '@zaber/react-library';
import { ascii } from '@zaber/motion';
import { match, P } from 'ts-pattern';

import { DRIVER_DISABLED_FLAGS, ProtocolManualFormatter } from '../../protocol_manual';
import { tryAccess, useActions } from '../../utils';
import { useWarningFlagManual } from '../../protocol_manual/hooks';
import type { EntityKey } from '../../keys';
import { isMoveable, ProductInfo, productKey } from '../types';
import { actionDefinitions } from '../actions';
import { actionDefinitions as pollingActionDefinitions } from '../../polling/actions';
import { CLEARABLE_FLAGS } from '../../protocol_manual/generate/warning_manual';


function flagToErrorType(flag: string): NoticeType {
  switch (flag[0]) {
    case 'F': return 'error';
    case 'W': return 'warning';
    default: return 'info';
  }
}

const warningFlagHelpTitles: Record<string, string> = {
  [ascii.WarningFlags.NO_REFERENCE_POSITION]: 'Homing Required',
};

interface Props {
  entity: EntityKey;
  info?: ProductInfo;
  flag: string;
}

export const WarningFlag: React.FC<Props> = ({ entity, info, flag }) => {
  const actions = useActions(actionDefinitions);
  const pollingActions = useActions(pollingActionDefinitions);
  const flagInfo = useWarningFlagManual(productKey(info, entity), flag);
  const flagType = flagToErrorType(flag);

  const titleAddendum = tryAccess(warningFlagHelpTitles, flag);
  const mainTitle = flagInfo ? `${flagInfo.name} (${flag})` : flag;
  const title = mainTitle + (titleAddendum ? ` - ${titleAddendum}` : '');

  const closer: React.ComponentProps<typeof NoticeMessage>['closer'] =
    DRIVER_DISABLED_FLAGS.includes(flag) && isMoveable(info) ? { button: 'Enable Driver', action: () => actions.enableDriver(info) } :
    CLEARABLE_FLAGS.includes(flag) ? { action: () => pollingActions.clearFlags(entity), title: `Clear ${flag} Flag` } :
    undefined;

  return <NoticeMessage headline={title} type={flagType} closer={closer} className="device-error" collapsible>
    {match(flagInfo)
      .with({ html: P.string }, ({ html }) => <ProtocolManualFormatter html={html}/>)
      .otherwise(() => null)
    }
  </NoticeMessage>;
};
