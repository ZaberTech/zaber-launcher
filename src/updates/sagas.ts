import type { SagaIterator } from 'redux-saga';
import { all, fork, put, takeEvery } from 'redux-saga/effects';
import * as remote from '@electron/remote';
import { notNil } from '@zaber/toolbox';

import { getContainer } from '../container';
import type { Action } from '../utils';
import { IPC } from '../ipc';
import { environment, processArguments } from '../environment';
import { Storage } from '../app_components';

import { actions, ActionTypes } from './actions';

export function* updateSaga(): SagaIterator {
  yield all([
    !environment.isTest ? fork(checkIfNewVersionInstalled) : null,
    takeEvery([
      ActionTypes.QUIT_AND_INSTALL,
      ActionTypes.CHECK_NEW_VERSION,
      ActionTypes.NEW_VERSION_NOTIFICATION_DISPLAYED,
      ActionTypes.IGNORE_UPDATE,
    ], sendActionToMainProcess),
  ].filter(notNil));
}

function sendActionToMainProcess(action: Action<unknown>) {
  const ipc = getContainer().get(IPC);
  ipc.sendActionToMainProcess(action);
}

export const LATEST_CHECKED_VERSION_KEY = 'LATEST_CHECKED_VERSION_KEY';
export function* checkIfNewVersionInstalled(): SagaIterator {
  const storage = getContainer().get(Storage);
  const latestVersion = storage.load(LATEST_CHECKED_VERSION_KEY, '');
  const installedVersion = remote.app.getVersion();
  const newVersionInstalled = installedVersion !== latestVersion && !processArguments.disableUpdates;
  if (newVersionInstalled) {
    storage.save(LATEST_CHECKED_VERSION_KEY, installedVersion);
  }
  if (environment.isProduction || environment.isTest) {
    yield put(actions.setCurrentVersion(installedVersion, newVersionInstalled));
  } else {
    yield put(actions.setCurrentVersion(process.env.npm_package_version ?? 'dev', newVersionInstalled));
  }
}
