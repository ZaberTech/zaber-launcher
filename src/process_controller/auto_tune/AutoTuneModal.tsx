import { Button, Flex, Modal, NoticeBanner, Text, UnorderedList } from '@zaber/react-library';
import React from 'react';
import { useSelector } from 'react-redux';
import { match, P } from 'ts-pattern';

import type { EntityKey } from '../../keys';
import { useActions } from '../../utils';
import { actionDefinitions } from '../actions';
import { selectAutoTune, selectControllerInfoOrNull, selectProcess } from '../selectors';
import { NoContentMessage } from '../../components';
import { AppIconsNeutral } from '../../apps';

import { AutoTune, hasAutoTuneSettings } from './types';
import { AutoTuneModalSetup } from './Setup';
import { AutoTuneModalIdentify } from './Identify';
import { AutoTuneModalTest } from './Test';
import { MAX_DUTY_CYCLE, MIN_DUTY_CYCLE } from './constants';


const AutoTuneSteps: React.FC<{ step: AutoTune['step'] }> = ({ step }) => (
  <Flex.Row className="steps">
    <Text t={Text.Type.H3} e={step === 'setup' ? Text.Emphasis.Regular : Text.Emphasis.Light}>Setup</Text>
    <Flex.Spacer/>
    <Text t={Text.Type.H3} e={step === 'setup' ? Text.Emphasis.Regular : Text.Emphasis.Light}>&gt;&gt;</Text>
    <Flex.Spacer/>
    <Text t={Text.Type.H3} e={step === 'identify' ? Text.Emphasis.Regular : Text.Emphasis.Light}>Identify</Text>
    <Flex.Spacer/>
    <Text t={Text.Type.H3} e={step === 'identify' ? Text.Emphasis.Regular : Text.Emphasis.Light}>&gt;&gt;</Text>
    <Flex.Spacer/>
    <Text t={Text.Type.H3} e={step === 'test' ? Text.Emphasis.Regular : Text.Emphasis.Light}>Test</Text>
  </Flex.Row>
);

const AutoTuneErrorMessage: React.FC<{ error: AutoTune['error'] }> = ({ error }) => {
  const actions = useActions(actionDefinitions);
  return match(error)
    .with({ type: 'error' }, ({ message, details }) => (
      <NoticeBanner headline={message} closer={() => actions.clearAutotuneError()}>{details}</NoticeBanner>
    ))
    .with({ type: 'invalid-duty-cycle' }, ({ dutyCycle }) => (
      <NoticeBanner
        headline={`Average duty cycle of ${dutyCycle.toPrecision(3)}% falls outside of the allowed range`}
        closer={() => actions.clearAutotuneError()}
        collapsible
      >
        <UnorderedList header={<Text>Possible Causes:</Text>}>
          {[
            [
              `Average duty cycle of ${dutyCycle.toPrecision(3)}% was detected during identification.`,
              `Values outside of the ${MIN_DUTY_CYCLE}-${MAX_DUTY_CYCLE}% range indicate that results may be inaccurate.`,
              'Please consider re-running the identification process.',
            ].join(' '),
            [
              'The temperature was not stable when identification started.',
              'Please go back to setup and wait for the temperature to settle at the setpoint after adjusting the voltage value.',
            ].join(' '),
            [
              'The system was affected by a disturbance during identification.',
              'Please remove any disturbances affecting the system as much as possible.',
            ].join(' '),
            'Please ensure that the chosen value of steady-state voltage makes the temperature settle as close to the setpoint as possible',
          ]}
        </UnorderedList>
      </NoticeBanner>))
    .with(P.nullish, () => null)
    .exhaustive();
};

const AutoTuneModalContent: React.FC<{ tune: AutoTune }> = ({ tune }) => {
  const settings = useSelector(selectProcess)?.settings;
  const controller = useSelector(selectControllerInfoOrNull);

  if (settings == null || !hasAutoTuneSettings(settings) || tune.step === 'begin' || tune.step === 'transition' || controller == null) {
    if (tune.error?.type === 'error') {
      return <NoContentMessage bannerTitle={tune.error.message} message={tune.error.details} type="error"/>;
    }
    return <NoContentMessage message="Loading..." type="working"/>;
  }

  return <>
    <AutoTuneSteps step={tune.step}/>
    <AutoTuneErrorMessage error={tune.error}/>
    {match(tune)
      .with({ step: 'setup' }, tune => (
        <AutoTuneModalSetup tune={tune} settings={settings}/>
      ))
      .with({ step: 'identify' }, tune => (
        <AutoTuneModalIdentify tune={tune} settings={settings}/>
      ))
      .with({ step: 'test' }, tune => (
        <AutoTuneModalTest controller={controller} tune={tune} settings={settings}/>
      ))
      .exhaustive()
    }
  </>;
};

export const AutoTuneButton: React.FC<{ entity: EntityKey; process: number }> = ({ entity, process }) => {
  const actions = useActions(actionDefinitions);
  const autoTune = useSelector(selectAutoTune);

  return <div data-testid="auto-tune-section">
    <Button title={`Auto Tune Process ${process}`} onClick={() => actions.beginAutoTune(entity, process)}>
      Tuning Assistant
    </Button>
    {autoTune != null && (
      <Modal
        className="auto-tune"
        headerIcon={<AppIconsNeutral.ServoTuner/>}
        headerText="Auto Tune"
        onRequestClose={autoTune.step === 'begin' ? undefined : () => actions.endAutoTune(entity, process, autoTune.initial, true)}
      >
        <AutoTuneModalContent tune={autoTune}/>
      </Modal>
    )}
  </div>;
};
