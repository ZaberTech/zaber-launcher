import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectProductManuals as selectState } from '../store';
import { selectIdentifiedAxes, selectIdentifiedDevices } from '../connection_manager';
import { extractDeviceKey } from '../keys';

import { ManualProfilingData } from './types';
import { createBaseUrl, createIncludes } from './generate/profile';

export const selectProductManuals = createSelector(selectState, state => state.productManuals);
export const selectCommandIdList = createSelector(selectState, state => state.commandIdList);
export const selectSettingIdList = createSelector(selectState, state => state.settingIdList);

export const selectAxisProfilingData = createSelector(selectIdentifiedAxes, selectIdentifiedDevices,
  (axes, devices) => _.mapValues(axes, (axis, axisKey): ManualProfilingData => {
    const device = devices[extractDeviceKey(axisKey)];

    const capabilities = device.product.capabilities.concat(axis.product.capabilities);
    const settings = device.product.settings.rows.concat(axis.product.settings.rows).map(row => row.name);
    const { firmwareVersion, name: deviceName } = device.identity;
    const { peripheralName } = axis.identity;
    return {
      capabilities, firmwareVersion, deviceName, peripheralName,
      baseUrl: createBaseUrl(firmwareVersion, deviceName, peripheralName),
      includes: createIncludes(capabilities, firmwareVersion),
      settings,
    };
  }));

export const selectDeviceProfilingData = createSelector(
  selectIdentifiedDevices, selectIdentifiedAxes, selectAxisProfilingData,
  (devices, axes, axisProfilingData) => _.mapValues(devices, (device): ManualProfilingData => {
    const peripheralLikeAxes = device.axes.map(axisKey => axes[axisKey]).filter(axis => axis.isPeripheralLike);
    const firstPeripheralAxis = peripheralLikeAxes.at(0);
    if (firstPeripheralAxis != null) {
      return axisProfilingData[firstPeripheralAxis.key];
    }

    const { capabilities } = device.product;
    const settings = device.product.settings.rows.map(row => row.name);
    const { firmwareVersion, name: deviceName } = device.identity;
    return {
      capabilities, firmwareVersion, deviceName,
      baseUrl: createBaseUrl(firmwareVersion, deviceName),
      includes: createIncludes(capabilities, firmwareVersion),
      settings,
    };
  }));

export const selectProfilingData = createSelector(selectDeviceProfilingData, selectAxisProfilingData,
  (deviceProfilingData, axisProfilingData) => ({
    ...deviceProfilingData,
    ...axisProfilingData,
  }));
