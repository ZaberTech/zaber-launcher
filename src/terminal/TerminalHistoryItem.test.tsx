import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';

import { waitUntilPass } from '../test';
import type { CommandRejectedReasonManual, WarningFlagManual } from '../protocol_manual/types';

import { TerminalHistoryItem } from './TerminalHistoryItem';

jest.mock('../protocol_manual', () => ({
  ...jest.requireActual('../protocol_manual'),
  useCommandRejectedReasonManual: (): CommandRejectedReasonManual => ({
    html: 'Command Rejected Details',
    name: 'Command Rejected'
  }),
  useWarningFlagManual: (): WarningFlagManual => ({
    html: 'Error Flag Details',
    name: 'Error Flag'
  }),
  ProtocolManualFormatter: ({ html }: { html: string }) => html,
}));

let wrapper: RenderResult;

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;
});

test('empty command', async () => {
  wrapper = render(
    <TerminalHistoryItem
      historyItem={{
        deviceOrAxisEntityKey: null,
        local: true,
        text: '/',
        parsed: {
          type: 'command',
          device: null,
          axis: null,
          id: null,
          data: null,
          checksum: null,
        }
      }}
    />
  );

  await waitUntilPass(() => {
    wrapper.getByText('/');
  });
  expect(wrapper.getByText('/').classList).not.toContain('fault');

  fireEvent.click(wrapper.getByText('/'));
});

test('command with checksum error', async () => {
  const onUpdate = jest.fn();

  wrapper = render(
    <TerminalHistoryItem
      historyItem={{
        deviceOrAxisEntityKey: null,
        local: true,
        text: '/home:29',
        parsed: {
          type: 'command',
          device: null,
          axis: null,
          id: null,
          data: 'home',
          checksum: {
            text: '29',
            error: true,
          },
        }
      }}
      onLayoutUpdate={onUpdate}
    />
  );
  expect(onUpdate).toHaveBeenCalledTimes(1);

  await waitUntilPass(() => {
    wrapper.getByText('/home');
    wrapper.getByText('29');
  });
  expect(wrapper.getByText('29').classList).toContain('fault');

  fireEvent.click(wrapper.getByText('29'));
  expect(onUpdate).toHaveBeenCalledTimes(2);
  await waitUntilPass(() => {
    wrapper.getByText('Checksum Error');
    wrapper.getByText('Checksum value is invalid', { exact: false });
  });

  fireEvent.click(wrapper.getByRole('button'));
  expect(onUpdate).toHaveBeenCalledTimes(3);
  expect(() => wrapper.getByText('Checksum Error')).toThrowError();
});


test('reply with command rejected', async () => {
  wrapper = render(
    <TerminalHistoryItem
      historyItem={{
        deviceOrAxisEntityKey: null,
        local: true,
        text: '@01 1 RJ BUSY -- BADCOMMAND',
        parsed: {
          type: 'reply',
          device: '01',
          axis: '1',
          id: null,
          replyFlag: 'RJ',
          status: 'BUSY',
          errorFlag: {
            text: '--',
            type: null,
          },
          data: 'BADCOMMAND',
          checksum: null,
        }
      }}
    />
  );

  await waitUntilPass(() => {
    wrapper.getByText('RJ');
    wrapper.getByText('BADCOMMAND');
  });
  expect(wrapper.getByText('RJ').classList).toContain('fault');
  expect(wrapper.getByText('BADCOMMAND').classList).toContain('fault');

  fireEvent.click(wrapper.getByText('RJ'));
  await waitUntilPass(() => {
    wrapper.getByText('Command Rejected Details').classList.contains('fault');
  });

  fireEvent.click(wrapper.getByRole('button'));
  expect(() => wrapper.getByText('Command Rejected Details')).toThrowError();

  fireEvent.click(wrapper.getByText('BADCOMMAND'));
  await waitUntilPass(() => {
    wrapper.getByText('Command Rejected Details').classList.contains('fault');
  });

  fireEvent.click(wrapper.getByRole('button'));
  expect(() => wrapper.getByText('Command Rejected Details')).toThrowError();
});


test('reply with fault', async () => {
  wrapper = render(
    <TerminalHistoryItem
      historyItem={{
        deviceOrAxisEntityKey: null,
        local: true,
        text: '@01 1 OK BUSY FZ 0',
        parsed: {
          type: 'reply',
          device: '01',
          axis: '1',
          id: null,
          replyFlag: 'OK',
          status: 'BUSY',
          errorFlag: {
            text: 'FZ',
            type: 'fault',
          },
          data: '0',
          checksum: null,
        }
      }}
    />
  );

  await waitUntilPass(() => {
    wrapper.getByText('FZ');
  });
  expect(wrapper.getByText('FZ').classList).toContain('fault');

  fireEvent.click(wrapper.getByText('FZ'));
  await waitUntilPass(() => {
    wrapper.getByText('Error Flag Details').classList.contains('fault');
  });

  fireEvent.click(wrapper.getByRole('button'));
  expect(() => wrapper.getByText('Error Flag Details')).toThrowError();
});

test('reply with warning', async () => {
  wrapper = render(
    <TerminalHistoryItem
      historyItem={{
        deviceOrAxisEntityKey: null,
        local: true,
        text: '@01 1 OK BUSY WL 0',
        parsed: {
          type: 'reply',
          device: '01',
          axis: '1',
          id: null,
          replyFlag: 'OK',
          status: 'BUSY',
          errorFlag: {
            text: 'WL',
            type: 'warn',
          },
          data: '0',
          checksum: null,
        }
      }}
    />
  );

  await waitUntilPass(() => {
    wrapper.getByText('WL');
  });
  expect(wrapper.getByText('WL').classList).toContain('warn');

  fireEvent.click(wrapper.getByText('WL'));
  await waitUntilPass(() => {
    wrapper.getByText('Error Flag Details');
  });

  fireEvent.click(wrapper.getByRole('button'));
  expect(() => wrapper.getByText('Error Flag Details')).toThrowError();
});

test('reply with note', async () => {
  wrapper = render(
    <TerminalHistoryItem
      historyItem={{
        deviceOrAxisEntityKey: null,
        local: true,
        text: '@01 1 OK BUSY NC 0',
        parsed: {
          type: 'reply',
          device: '01',
          axis: '1',
          id: null,
          replyFlag: 'OK',
          status: 'BUSY',
          errorFlag: {
            text: 'NC',
            type: 'note',
          },
          data: '0',
          checksum: null,
        }
      }}
    />
  );

  await waitUntilPass(() => {
    wrapper.getByText('NC');
  });
  expect(wrapper.getByText('NC').classList).toContain('note');

  fireEvent.click(wrapper.getByText('NC'));
  await waitUntilPass(() => {
    wrapper.getByText('Error Flag Details');
  });

  fireEvent.click(wrapper.getByRole('button'));
  expect(() => wrapper.getByText('Error Flag Details')).toThrowError();
});

test('garbage message', async () => {
  wrapper = render(
    <TerminalHistoryItem
      historyItem={{
        deviceOrAxisEntityKey: null,
        local: true,
        text: 'lin bfdsfsbbfda',
        parsed: {
          type: 'unknown',
        }
      }}
    />
  );

  await waitUntilPass(() => {
    wrapper.getByText('lin bfdsfsbbfda');
  });
  expect(wrapper.getByText('lin bfdsfsbbfda').parentNode?.parentNode).toHaveClass('error');
});
