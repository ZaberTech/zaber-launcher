import _ from 'lodash';
import type { FirmwareVersion } from '@zaber/motion';

import { changeDictionary, createReducer } from '../utils';
import { EntityKey, extractConnectionKey, removeWithKey } from '../keys';
import {
  ConnectionManagerActionTypes, ConnectionManagerActionPayloads,
  DevicesLoadedPayload, DeviceInfoWithAxes
} from '../connection_manager';

import { ActionsToPayloads, ActionTypes } from './actions';
import {
  Setting, Sliders, SlidersRange,
  isIntegratedMultiAxisDevice, isIntegratedSingleAxisDevice, UNUSED_AXIS_PERIPHERAL_ID, ThirdPartyInfo, LoadingError
} from './types';

export interface AxisOrDeviceState {
  key: EntityKey;
  inputSettings: Setting[] | null;
  sliderPositions: Sliders | null;
  appliedPositions: Sliders | null;
  defaultPositions: Sliders | null;
  slidersRange: SlidersRange | null;
  apply: { state: 'pending' | 'success' } | {  state: 'error'; error: string } | null;
  targetIctrlSettings: Setting[];
  actualIctrlSettings: Setting[];
  firmwareVersion: FirmwareVersion;
  thirdPartyInfo: ThirdPartyInfo | null;
}

export interface State {
  selectedAxisOrDevice: EntityKey | null;
  axesAndDevices: Record<string, AxisOrDeviceState>;
  loaded: boolean;
  loadingError: LoadingError | null;
  settingDriver: boolean;
  driverEnabled: boolean | null;
  setDriverErr: string | null;
  showWarning: boolean | null;
  thirdPartyConfirmed: boolean;
}

const initialState: State = {
  selectedAxisOrDevice: null,
  axesAndDevices: {},
  loaded: false,
  loadingError: null,
  settingDriver: false,
  driverEnabled: null,
  setDriverErr: null,
  showWarning: null,
  thirdPartyConfirmed: false,
};

const selectAxisOrDevice = (state: State, { axisOrDevice }:
  ActionsToPayloads[ActionTypes.SELECT_AXIS_OR_DEVICE]): State => ({
  ...state,
  selectedAxisOrDevice: axisOrDevice,
  loaded: false,
  loadingError: null,
  thirdPartyConfirmed: state.selectedAxisOrDevice === axisOrDevice ? state.thirdPartyConfirmed : false,
});

const updateLoaded = (state: State, { value }:
  ActionsToPayloads[ActionTypes.UPDATE_LOADED]): State => ({
  ...state,
  loaded: value,
});

const loadingErr = (state: State, { error }:
  ActionsToPayloads[ActionTypes.LOADING_ERR]): State => ({
  ...state,
  loadingError: error,
});

const unknownFirmware = { major: 7, minor: 99, build: 0 };

function defaultAxisOrDeviceState(device: DeviceInfoWithAxes): AxisOrDeviceState[] {
  if (isIntegratedMultiAxisDevice(device)) {
    return [];
  }

  const firmwareVersion = (device.identity ?? device.nonIdentifiedInfo)?.firmwareVersion ?? unknownFirmware;

  if (isIntegratedSingleAxisDevice(device)) {
    return ([{
      key: device.key,
      inputSettings: null,
      sliderPositions: null,
      appliedPositions: null,
      defaultPositions: null,
      slidersRange: null,
      apply: null,
      targetIctrlSettings: [],
      actualIctrlSettings: [],
      firmwareVersion,
      thirdPartyInfo: null,
    }]);
  }

  const axisList: AxisOrDeviceState[] = [];
  for (const axis of device.axes) {
    if ((axis.identity ?? axis.nonIdentifiedInfo)?.peripheralId === UNUSED_AXIS_PERIPHERAL_ID) {
      continue;
    }
    axisList.push({
      key: axis.key,
      inputSettings: null,
      sliderPositions: null,
      appliedPositions: null,
      defaultPositions: null,
      slidersRange: null,
      apply: null,
      targetIctrlSettings: [],
      actualIctrlSettings: [],
      firmwareVersion,
      thirdPartyInfo: null,
    });
  }
  return axisList;
}

const devicesLoaded = (state: State, { devices, connectionKey }: DevicesLoadedPayload): State => ({
  ...state,
  selectedAxisOrDevice: null,
  axesAndDevices: {
    ...removeWithKey(state.axesAndDevices, extractConnectionKey, connectionKey),
    ..._.keyBy(_.flatMap(devices, device => defaultAxisOrDeviceState(device)), 'key'),
  },
});

const updateSliders = (state: State, { axisOrDevice, sliders }:
  ActionsToPayloads[ActionTypes.UPDATE_SLIDERS]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, item => ({
    ...item,
    sliderPositions: item.sliderPositions && {
      ...item.sliderPositions,
      ...sliders,
    },
    apply: null,
  })),
});

const updateAppliedSliderValues = (state : State, { axisOrDevice, appliedSliders }:
  ActionsToPayloads[ActionTypes.UPDATE_APPLIED_SLIDERS]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, {
    appliedPositions: appliedSliders,
  }),
});

const updateSliderValues = (state: State, { axisOrDevice, sliders }:
  ActionsToPayloads[ActionTypes.UPDATE_SLIDER_VALUES]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, {
    sliderPositions: sliders,
  }),
});

const applySliderValues = (state: State, { axisOrDevice }:
  ActionsToPayloads[ActionTypes.APPLY_SLIDER_VALUES]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, item => ({
    ...item,
    apply: { state: 'pending' },
  }))
});

const applySliderValuesDone = (state: State, { axisOrDevice, error }:
  ActionsToPayloads[ActionTypes.APPLY_SLIDER_VALUES_DONE]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, item => ({
    ...item,
    apply: error ? { state: 'error', error } : { state: 'success' },
  }))
});

const updateApplyErr = (state: State, { axisOrDevice, error }:
  ActionsToPayloads[ActionTypes.UPDATE_APPLY_ERR]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, item => ({
    ...item,
    apply: { state: 'error', error },
  }))
});

const clearApplyResult = (state: State, { axisOrDevice }:
  ActionsToPayloads[ActionTypes.CLEAR_APPLY_RESULT]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, item => ({
    ...item,
    apply: null,
  })),
});

const populateTuningInfo = (state: State, { axisOrDevice, inputSettings, info: { defaultSliderPositions, slidersRange } }:
  ActionsToPayloads[ActionTypes.POPULATE_TUNING_INFO]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, item => ({
    ...item,
    inputSettings,
    defaultPositions: defaultSliderPositions,
    slidersRange
  })),
});

const updateTargetSettings = (state: State, { axisOrDevice, targetIctrlSettings }:
  ActionsToPayloads[ActionTypes.UPDATE_TARGET_SETTINGS]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, { targetIctrlSettings }),
});

const updateActualSettings = (state: State, { axisOrDevice, actualIctrlSettings }:
  ActionsToPayloads[ActionTypes.UPDATE_ACTUAL_SETTINGS]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, { actualIctrlSettings }),
});

const setDriver = (state: State): State => ({
  ...state,
  settingDriver: true,
});

const setDriverDone = (state: State, { error }:
  ActionsToPayloads[ActionTypes.SET_DRIVER_DONE]): State => ({
  ...state,
  settingDriver: false,
  setDriverErr: error ?? null,
});

const updateDriverEnabled = (state: State, { axisOrDevice, driverEnabled }:
  ActionsToPayloads[ActionTypes.UPDATE_DRIVER_ENABLED]): State => ({
  ...state,
  driverEnabled: state.selectedAxisOrDevice === axisOrDevice ? driverEnabled : state.driverEnabled,
});

const updateShowWarning = (state: State, { showWarning }:
  ActionsToPayloads[ActionTypes.UPDATE_SHOW_WARNING]): State => ({
  ...state,
  showWarning,
});

const set3rdPartyInfo = (state: State, { axisOrDevice, info }: ActionsToPayloads[ActionTypes.SET_3RD_PARTY_INFO]): State => ({
  ...state,
  axesAndDevices: changeDictionary(state.axesAndDevices, axisOrDevice, { thirdPartyInfo: info }),
});

const confirm3rdParty = (state: State): State => ({
  ...state,
  thirdPartyConfirmed: true,
});


export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.UPDATE_LOADED]: updateLoaded,
  [ActionTypes.LOADING_ERR]: loadingErr,
  [ActionTypes.SELECT_AXIS_OR_DEVICE]: selectAxisOrDevice,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
  [ActionTypes.UPDATE_SLIDERS]: updateSliders,
  [ActionTypes.UPDATE_SLIDER_VALUES]: updateSliderValues,
  [ActionTypes.APPLY_SLIDER_VALUES]: applySliderValues,
  [ActionTypes.APPLY_SLIDER_VALUES_DONE]: applySliderValuesDone,
  [ActionTypes.POPULATE_TUNING_INFO]: populateTuningInfo,
  [ActionTypes.UPDATE_TARGET_SETTINGS]: updateTargetSettings,
  [ActionTypes.UPDATE_ACTUAL_SETTINGS]: updateActualSettings,
  [ActionTypes.UPDATE_APPLIED_SLIDERS]: updateAppliedSliderValues,
  [ActionTypes.UPDATE_APPLY_ERR]: updateApplyErr,
  [ActionTypes.CLEAR_APPLY_RESULT]: clearApplyResult,
  [ActionTypes.SET_DRIVER]: setDriver,
  [ActionTypes.SET_DRIVER_DONE]: setDriverDone,
  [ActionTypes.UPDATE_DRIVER_ENABLED]: updateDriverEnabled,
  [ActionTypes.UPDATE_SHOW_WARNING]: updateShowWarning,
  [ActionTypes.SET_3RD_PARTY_INFO]: set3rdPartyInfo,
  [ActionTypes.CONFIRM_3RD_PARTY]: confirm3rdParty,
}, initialState);
