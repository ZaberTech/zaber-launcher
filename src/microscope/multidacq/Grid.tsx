import React from 'react';
import { useSelector } from 'react-redux';
import { DeepPartial } from 'redux';
import { Button, HeaderCard, NumericInput, SimpleSelect, Text } from '@zaber/react-library';

import { getUnitLabel, InputWithUnits } from '../../units';
import { POSITION_DIMENSION } from '../types';
import { useActions } from '../../utils';
import { displayMeasurement, getPrecision, MeasurementMath } from '../utils';
import { selectPositions } from '../selectors';
import { Field } from '../components';

import { selectGrid } from './selectors';
import { actions as actionsDefinition } from './actions';
import { MultiDAcqConfig } from './types';

type GridConfig = MultiDAcqConfig['xy']['grid'];

const ALIGN_OPTIONS: SimpleSelect.Option<GridConfig['align']>[] = [
  { value: 'top-left', label: 'Reference is Corner' },
  { value: 'center', label: 'Reference is Center' },
];

const ORDER_OPTIONS: SimpleSelect.Option<GridConfig['order']>[] = [
  { value: 'snake-rows', label: 'Snake by Rows' },
  { value: 'snake-columns', label: 'Snake by Columns' },
  { value: 'rows', label: 'By Rows' },
  { value: 'columns', label: 'By Columns' },
];

const Coordinates = () => {
  const { reference, spacing, size, align } = useSelector(selectGrid);

  const convert = MeasurementMath.toNumber;
  const convertBack = MeasurementMath.fromNumber;

  const cols = size.x ?? 1;
  const rows = size.y ?? 1;

  const alignOffset = align === 'center' ? {
    x: (cols - 1) / 2 * -convert(spacing.x),
    y: (rows - 1) / 2 * -convert(spacing.y),
  } : {
    x: 0, y: 0
  };
  let coordinates = {
    x1: convert(reference.x) + alignOffset.x,
    y1: convert(reference.y) + alignOffset.y,
    x2: convert(reference.x) + convert(spacing.x) * cols + alignOffset.x,
    y2: convert(reference.y) + convert(spacing.y) * rows + alignOffset.y,
  };
  coordinates = {
    x1: Math.min(coordinates.x1, coordinates.x2),
    y1: Math.min(coordinates.y1, coordinates.y2),
    x2: Math.max(coordinates.x1, coordinates.x2),
    y2: Math.max(coordinates.y1, coordinates.y2),
  };
  const units = reference.x.units;
  const converted = {
    x1: convertBack(coordinates.x1, units).value,
    y1: convertBack(coordinates.y1, units).value,
    x2: convertBack(coordinates.x2, units).value,
    y2: convertBack(coordinates.y2, units).value,
  };
  const gridSize = {
    x: convertBack(Math.abs(convert(spacing.x)) * cols, spacing.x.units),
    y: convertBack(Math.abs(convert(spacing.y)) * rows, spacing.y.units),
  };
  const refPosition = align === 'center' ? {
    top: `${0.5 * (rows - 1) / rows * 100}%`,
    left: `${0.5 * (cols - 1) / cols * 100}%`,
  } : { top: 0, left: 0 };
  const precision = getPrecision(reference.x);

  return <Field label="Summary">
    <div className="visual-summary">
      <div className="corner1">({converted.x1.toFixed(precision)}, {converted.y1.toFixed(precision)}) {getUnitLabel(units)}</div>
      <div className="width">{displayMeasurement(gridSize.x)}</div>
      <div className="square">
        <div className="ref" style={refPosition}>
          <div className="ref-dot"/>
          <Text>Ref</Text>
        </div>
        <div className="size">{(rows)} × {(cols)}</div>
      </div>
      <div className="height">{displayMeasurement(gridSize.y)}</div>
      <div className="corner2">({converted.x2.toFixed(precision)}, {converted.y2.toFixed(precision)}) {getUnitLabel(units)}</div>
    </div>
    <div>
      Number of Positions: {cols * rows}
    </div>
  </Field>;
};

export const Grid: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const state = useSelector(selectGrid);

  const currentPositions = useSelector(selectPositions);

  const update = (grid: DeepPartial<GridConfig>) => {
    actions.updateConfig({ xy: { grid } });
  };

  return <HeaderCard className="xy-dimension"
    header={<Text t={Text.Type.H4}>XY Dimension (Grid)</Text>}>
    <div className="grid">
      <span/>
      <Text t={Text.Type.H5}>Reference Position</Text>
      <Text t={Text.Type.H5}>Spacing</Text>
      <Text t={Text.Type.H5}>Number of Column/Rows</Text>

      {(['x', 'y'] as const).map(axis => (<React.Fragment key={axis}>
        <Text>{axis.toUpperCase()}</Text>

        <div className="reference">
          <InputWithUnits
            placeholder={`${axis.toUpperCase()} Position`}
            displayPrecision={3}
            unitFrom={{ dimension: POSITION_DIMENSION }}
            measure={state.reference[axis]}
            onChange={value => update({ reference: { [axis]: value } })}/>

          <Button color="grey"
            disabled={currentPositions[axis] == null}
            onClick={() => update({ reference: { [axis]: currentPositions[axis] } })}>
            Set to Current
          </Button>
        </div>

        <InputWithUnits
          placeholder={`${axis.toUpperCase()} Step`}
          displayPrecision={3}
          unitFrom={{ dimension: POSITION_DIMENSION }}
          measure={state.spacing[axis]}
          onChange={value => update({ spacing: { [axis]: value } })}/>

        <NumericInput
          placeholder={`${axis.toUpperCase()} Count`}
          status={(state.size[axis] ?? 0) < 1 ? 'error' : undefined}
          value={state.size[axis]}
          onNumberChange={value => update({ size: { [axis]: value } })}/>
      </React.Fragment>))}
    </div>

    <div className="row">
      <Field label="Order">
        <SimpleSelect<GridConfig['order']>
          placeholder="Order"
          options={ORDER_OPTIONS}
          value={state.order}
          onValueChange={value => update({ order: value })}
          portalToBody/>
      </Field>

      <Field label="Alignment">
        <SimpleSelect<GridConfig['align']>
          placeholder="Alignment"
          options={ALIGN_OPTIONS}
          value={state.align}
          onValueChange={value => update({ align: value })}
          portalToBody/>
      </Field>
    </div>

    <div className="row">
      <Coordinates/>
    </div>
  </HeaderCard>;
};
