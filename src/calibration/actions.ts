
import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

import type { CalibrationTable, EditedCalibrationTable } from './types';

export enum ActionTypes {
  SELECT_DEVICE_OR_AXIS = 'CALIBRATION_SELECT_DEVICE_OR_AXIS',
  LOAD_TABLE = 'CALIBRATION_LOAD_TABLE',
  TABLE_LOADED = 'CALIBRATION_TABLE_LOADED',
  TABLE_ERR = 'CALIBRATION_TABLE_ERR',
  LOAD_FROM_FILE = 'CALIBRATION_LOAD_FROM_FILE',
  SAVE_TO_FILE = 'CALIBRATION_SAVE_TO_FILE',
  FILE_ERR = 'CALIBRATION_FILE_ERR',
  CLEAR_FILE_ERR = 'CALIBRATION_CLEAR_FILE_ERR',
  UPDATE_TABLE = 'CALIBRATION_UPDATE_TABLE',
  APPLY = 'CALIBRATION_APPLY',
  APPLY_DONE = 'CALIBRATION_APPLY_DONE',
  APPLY_ERR = 'CALIBRATION_APPLY_ERR',
  APPLY_DELETE = 'CALIBRATION_APPLY_DELETE',
  APPLY_DELETE_DONE = 'CALIBRATION_APPLY_DELETE_DONE',
  CLEAR_APPLY_ERROR = 'CALIBRATION_CLEAR_APPLY_ERROR',
  SET_FACTORY_DATA = 'CALIBRATION_SET_FACTORY_DATA',
}

export interface ActionsToPayloads {
  [ActionTypes.SELECT_DEVICE_OR_AXIS]: { selectedKey: EntityKey | null };
  [ActionTypes.LOAD_TABLE]: void;
  [ActionTypes.TABLE_LOADED]: { table: CalibrationTable };
  [ActionTypes.TABLE_ERR]: { error: string };
  [ActionTypes.LOAD_FROM_FILE]: void;
  [ActionTypes.SAVE_TO_FILE]: void;
  [ActionTypes.FILE_ERR]: { error: string };
  [ActionTypes.CLEAR_FILE_ERR]: void;
  [ActionTypes.UPDATE_TABLE]: { table: Partial<EditedCalibrationTable> };
  [ActionTypes.APPLY]: void;
  [ActionTypes.APPLY_DONE]: { table: CalibrationTable };
  [ActionTypes.APPLY_ERR]: { error: string };
  [ActionTypes.APPLY_DELETE]: void;
  [ActionTypes.APPLY_DELETE_DONE]: void;
  [ActionTypes.CLEAR_APPLY_ERROR]: void;
  [ActionTypes.SET_FACTORY_DATA]: { factoryData: boolean };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  selectDeviceOrAxis: (selectedKey: EntityKey | null) => buildAction(ActionTypes.SELECT_DEVICE_OR_AXIS, { selectedKey }),
  loadTable: () => buildAction(ActionTypes.LOAD_TABLE),
  tableLoaded: (table: CalibrationTable) => buildAction(ActionTypes.TABLE_LOADED, { table }),
  tableErr: (error: string) => buildAction(ActionTypes.TABLE_ERR, { error }),
  loadFromFile: () => buildAction(ActionTypes.LOAD_FROM_FILE),
  saveToFile: () => buildAction(ActionTypes.SAVE_TO_FILE),
  fileErr: (error: string) => buildAction(ActionTypes.FILE_ERR, { error }),
  clearFileErr: () => buildAction(ActionTypes.CLEAR_FILE_ERR),
  updateTable: (table: Partial<EditedCalibrationTable>) => buildAction(ActionTypes.UPDATE_TABLE, { table }),
  apply: () => buildAction(ActionTypes.APPLY),
  applyDone: (table: CalibrationTable) => buildAction(ActionTypes.APPLY_DONE, { table }),
  applyErr: (error: string) => buildAction(ActionTypes.APPLY_ERR, { error }),
  applyDelete: () => buildAction(ActionTypes.APPLY_DELETE),
  applyDeleteDone: () => buildAction(ActionTypes.APPLY_DELETE_DONE),
  clearApplyError: () => buildAction(ActionTypes.CLEAR_APPLY_ERROR),
  setFactoryData: (factoryData: boolean) => buildAction(ActionTypes.SET_FACTORY_DATA, { factoryData }),
};
