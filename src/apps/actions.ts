import { actionBuilder } from '../utils';

import type { AppUrls } from './list';

export enum ActionTypes {
  STORED_APPS_LOADED = 'APPS_STORED_APPS_LOADED',
  PIN_APP = 'APPS_PIN_APP',
  UNLOCK_APP = 'APPS_UNLOCK_APP',
}

export interface ActionsToPayloads {
  [ActionTypes.STORED_APPS_LOADED]: { recent: AppUrls[]; pinned: AppUrls[] };
  [ActionTypes.PIN_APP]: { app: AppUrls; pinned: boolean };
  [ActionTypes.UNLOCK_APP]: { url: AppUrls };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  storedAppsLoaded: (recent: AppUrls[], pinned: AppUrls[]) => buildAction(ActionTypes.STORED_APPS_LOADED, { recent, pinned }),
  pinApp: (app: AppUrls, pinned: boolean) => buildAction(ActionTypes.PIN_APP, { app, pinned }),
  unlockApp: (url: AppUrls) => buildAction(ActionTypes.UNLOCK_APP, { url }),
};
