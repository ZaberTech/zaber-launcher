export { reducer as demoReducer } from './reducer';
export type { State as DemoState } from './reducer';
export {
  actions as demoActions,
  ActionTypes as DemoActionTypes,
} from './actions';
export type {
  ActionsToPayloads as DemoActionPayloads,
} from './actions';
export { rootSaga as demoSaga } from './sagas';
export { Demo } from './Demo';
