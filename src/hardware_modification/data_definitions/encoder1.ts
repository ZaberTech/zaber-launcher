import { hasSetting } from '../../devices';
import type { SectionDataDefinition, SectionValues } from '../types';

import { encoderConversionRatioEditable, encoderIsOn, EncoderMode } from './encoder';

function conversionRatioEditable(values: SectionValues<'encoder1'>): boolean {
  if (values.mode == null) { return false }
  return encoderConversionRatioEditable(values.mode.is, values);
}

export const encoder1: SectionDataDefinition<'encoder1'> = {
  initializers: {
    motorType: { type: 'custom', backing: ascii => ascii.settings.get('motor.type') },
    mode: { type: 'setting', backing: 'encoder.1.mode' },
    type: { type: 'setting', backing: 'encoder.1.type' },
    dir: { type: 'setting', backing: 'encoder.1.dir' },
    fault: { type: 'setting', backing: 'encoder.1.fault.type' },
    indexMode: { type: 'setting', backing: 'encoder.1.index.mode' },
    filter: { type: 'setting', backing: 'encoder.1.filter' },
    mult: { type: 'setting', backing: 'encoder.1.ratio.mult' },
    div: { type: 'setting', backing: 'encoder.1.ratio.div' },
    phase: { type: 'setting', backing: 'encoder.1.ref.phase' },
    powerUpDelay: { type: 'setting', backing: 'encoder.1.power.up.delay' },
  },
  sectionPresent: async ({ info }) => {
    const e = hasSetting('encoder.1.mode', info.axis ?? info.device);
    return e;
  },
  fieldEnabled: {
    type: encoderIsOn,
    dir: encoderIsOn,
    fault: encoderIsOn,
    indexMode: encoderIsOn,
    filter: encoderIsOn,
    mult: conversionRatioEditable,
    div: conversionRatioEditable,
    phase: values => {
      if (values.mode == null) { return false }
      return values.mode.is === EncoderMode.ABSOLUTE;
    },
  },
};
