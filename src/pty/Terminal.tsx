import { Colors } from '@zaber/react-library';
import React, { useEffect, useLayoutEffect, useRef } from 'react';
import { Terminal as XTerm, ITerminalOptions } from 'xterm';

import { getContainer } from '../container';

import { DEFAULT_TERMINAL_COLUMNS, DEFAULT_TERMINAL_ROWS } from './common_types';
import { Pty } from './pty';

interface Props {
  id: string;
  cols?: number;
  rows?: number;
  /** Focuses terminal when changed (except when undefined). */
  focusKey?: unknown;
  /** Resets terminal when changed (except when undefined). */
  resetKey?: unknown;
  fontSize?: number;
  theme?: 'default' | 'zaber';
}

const ZABER_THEME: ITerminalOptions = {
  fontFamily: 'Roboto Mono',
  theme: {
    black: Colors.darkGrey,
    red: Colors.zaberRed,
    green: '#008000',
    yellow: Colors.orange,
    blue: '#00008b',
    magenta: '#800080',
    cyan: '#008b8b',
    white: Colors.paleGrey,
    brightBlack: Colors.zaberGrey,
    brightRed: Colors.brightRed,
    brightGreen: Colors.green,
    brightYellow: Colors.yellow,
    brightBlue: Colors.blue,
    brightMagenta: '#dda0dd',
    brightCyan: '#00ffff',
    brightWhite: Colors.almostWhite,
  },
};

export const Terminal: React.FC<Props> = ({
  id,
  cols = DEFAULT_TERMINAL_COLUMNS, rows = DEFAULT_TERMINAL_ROWS, fontSize = 16,
  focusKey, resetKey, theme,
}) => {
  const pty = getContainer().get(Pty);

  const divRef = useRef(null);
  const xterm = useRef<XTerm>();

  useEffect(() => {
    const dataStream = pty.getDataSteam(id);
    const token = dataStream.subscribe(data => xterm.current!.write(data));
    return () => token.unsubscribe();
  }, [id]);

  useEffect(() => {
    const token = xterm.current!.onData(data => pty.sendData(id, data));
    return () => token.dispose();
  }, [id]);

  useLayoutEffect(() => {
    xterm.current?.resize(cols, rows);
  }, [cols, rows]);

  useLayoutEffect(() => {
    if (focusKey !== undefined) {
      xterm.current?.focus();
    }
  }, [focusKey]);

  useLayoutEffect(() => {
    if (resetKey !== undefined) {
      xterm.current?.reset();
    }
  }, [resetKey]);

  useLayoutEffect(() => {
    const term = new XTerm({
      cols, rows,
      fontSize,
      ...(theme === 'zaber' ?  ZABER_THEME : null)
    });
    xterm.current = term;
    term.open(divRef.current!);

    return () => term.dispose();
  }, []);

  return <div ref={divRef}/>;
};
