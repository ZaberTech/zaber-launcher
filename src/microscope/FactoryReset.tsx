import React from 'react';
import { useSelector } from 'react-redux';
import { Button, ButtonRow, ButtonRowConfirmCancel, Icons, Loader, Modal, NoticeBanner } from '@zaber/react-library';

import { deviceNameWithLabel } from '../connection_manager';
import { useActions } from '../utils';

import { selectFactoryResetErrors, selectFactoryResetStep, selectFactoryResettableDevices } from './selectors';
import { actions as actionsDefinition } from './actions';


const DeviceList: React.FC = () => {
  const deviceNames = useSelector(selectFactoryResettableDevices)
    .map(device => deviceNameWithLabel(device))
    .sort();

  if (!deviceNames.length) {
    return <NoticeBanner type="warning">No devices found to reset</NoticeBanner>;
  }

  return <ul>
    {deviceNames.map((name, i) => <li key={i}><Icons.BulletPoint/>{name}</li>)}
  </ul>;
};


export const FactoryResetDialog: React.FC = () => {
  const step = useSelector(selectFactoryResetStep);
  const errors = useSelector(selectFactoryResetErrors);
  const actions = useActions(actionsDefinition);

  if (!step) {
    return null;
  }

  return (<div className="factory-reset-modal">
    {step === 'confirm' && <Modal
      headerIcon={<Icons.FactoryReset/>}
      headerText="Confirm Factory Reset"
      buttons={<ButtonRowConfirmCancel
        confirmText="Reset"
        onConfirm={() => actions.factoryResetSetStep('perform')}
        onCancel={() => actions.factoryResetSetStep(null)}/>}
      onRequestClose={() => actions.factoryResetSetStep(null)}
      small
      bodyIcon="warning"
    >
      <div>Are you sure you want to factory reset the following devices?</div>
      <hr/>
      <DeviceList/>
    </Modal>}

    {step === 'perform' &&
    <Modal headerIcon={<Icons.FactoryReset/>} headerText="Performing Factory Reset" small>
      <div className="resetting">
        <Loader size="large"/> Resetting...
      </div>
    </Modal>}

    {step === 'show-errors' && errors.length > 0 && <Modal
      headerIcon={<Icons.FactoryReset/>}
      headerText="Failed Factory Reset"
      small
      bodyIcon="error"
      buttons={<ButtonRow justify="right">
        <Button onClick={() => actions.factoryResetSetStep(null)}>OK</Button>
      </ButtonRow>}
      onRequestClose={() => actions.factoryResetSetStep(null)}>
      Errors resetting the device:<br/><br/>
      <ul>
        {errors.map((error, i) => <li key={i}><Icons.BulletPoint/>{error}</li>)}
      </ul>
    </Modal>}
  </div>);
};
