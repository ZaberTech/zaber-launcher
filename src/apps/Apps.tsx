import { routerActions as routerActionsDefinition } from 'connected-react-router';
import _ from 'lodash';
import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { Text } from '@zaber/react-library';

import { InternalOnlyWarning, Title } from '../components';
import { useActions } from '../utils';

import { actions as appActions } from './actions';
import { ApplicationCard } from './ApplicationCard';
import { UNLOCKABLE_APPS } from './list';
import { selectAppsToRender } from './selectors';

const MAX_CODE_LENGTH = _.max(_.keys(UNLOCKABLE_APPS).map(s => s.length)) ?? 20;

export const Apps = () => {
  const actions = useActions(appActions);
  const routerActions = useActions(routerActionsDefinition);
  const appsToRender = useSelector(selectAppsToRender);
  const [deprecatedApps, currentApps] = _.partition(appsToRender, app => app.deprecated);
  const basicControlsApp = currentApps.find(app => app.url === '/basic-controls');
  const apps = currentApps.filter(app => app.url !== '/basic-controls');

  const code = useRef('');

  useEffect(() => {
    function onKeyDown(event: KeyboardEvent): void {
      if (event.key.length !== 1 || event.ctrlKey || event.altKey || event.metaKey) {
        return;
      }

      let newCode = (code.current + event.key).slice(-MAX_CODE_LENGTH);
      const key = _.keys(UNLOCKABLE_APPS).find(key => newCode.endsWith(key));
      if (key) {
        actions.unlockApp(UNLOCKABLE_APPS[key]);
        newCode = '';
      }

      code.current = newCode;
    }

    window.addEventListener('keydown', onKeyDown);
    return () => window.removeEventListener('keydown', onKeyDown);
  }, []);

  const openApp = (appUrl: string, newWindow: boolean) => {
    if (newWindow) {
      const newUrl = window.location.href.replace(/#.*$/, `#${appUrl}`);
      window.open(newUrl);
    } else {
      routerActions.push(appUrl);
    }
  };

  return (
    <div className="apps">
      <Title>All Applications</Title>
      <InternalOnlyWarning/>
      <Text t={Text.Type.H3} e={Text.Emphasis.Light}>Applications</Text>
      {basicControlsApp && (
        <ApplicationCard app={basicControlsApp!} onOpen={newWindow => openApp('/basic-controls', newWindow)} className="standalone"/>
      )}
      <div className="app-list">
        {apps.map(app => <ApplicationCard key={app.url} app={app} onOpen={newWindow => openApp(app.url, newWindow)}/>)}
      </div>

      {deprecatedApps.length > 0 && <>
        <Text t={Text.Type.H4} e={Text.Emphasis.Light}>Deprecated Applications</Text>
        <Text>These apps have been deprecated. They are no longer maintained and will be removed in a future release</Text>
        <div className="app-list">
          {deprecatedApps.map(app => <ApplicationCard key={app.url} app={app} onOpen={newWindow => openApp(app.url, newWindow)}/>)}
        </div>
      </>}
    </div>
  );
};
