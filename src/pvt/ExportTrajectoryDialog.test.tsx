const mockSaveDialog = jest.fn(() => ({
  cancelled: false,
  filePath: 'foo.csv',
}));

jest.mock('../dialogs', () => ({
  Dialogs: {
    showSaveDialog: mockSaveDialog,
  },
}));

let fileContent: string = '';

const fileHandleMock = {
  async write(data: string) {
    fileContent = fileContent + data;
  },

  async sync() { },

  async close() { },
};


jest.mock('fs/promises', () => ({
  open: jest.fn().mockResolvedValue(fileHandleMock),
}));

import { EChartsReactMock } from './testing/mocks';

jest.mock('echarts-for-react', () => EChartsReactMock);

import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import type { Container } from 'inversify';
import { toBeDeepCloseTo } from 'jest-matcher-deep-close-to';

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';

import { PvtApp } from './PvtApp';
import { FileWatcher } from '../file_watcher';
import { FileWatcherMock } from '../test/mocks/file_watcher';
import { actions } from './actions';
import { Length, Time, Velocity } from '@zaber/motion';
import type { MeasurementOK } from '../units';
import _ from 'lodash';
import { setValueAndUnitsByTitle } from '../units/test_utils';
import { parseCsv } from './csv';

expect.extend({ toBeDeepCloseTo });

let container: Container;
const TestApp = wrapWithNewStore(wrapWithRouter(PvtApp));
let wrapper: RenderResult;


async function doExport(timeStep: MeasurementOK) {
  fileContent = '';
  fireEvent.click(wrapper.getByTestId('trajectory-export-button'));
  await waitUntilPass(() => wrapper.getByTitle('Value of Time Interval'));
  setValueAndUnitsByTitle(wrapper, 'Time Interval', timeStep.value, timeStep.units);
  fireEvent.click(wrapper.getByTitle('Export'));
  await waitUntilPass(() => expect(fileContent).not.toBe(''));
}


beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(FileWatcher).to(FileWatcherMock);
  wrapper = render(<TestApp/>);
  TestApp.testStore.dispatch(actions.setCurrentFile('foo.csv'));
});


afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});


describe('Export trajectory', () => {
  it('produces expected headings and key values', async () => {
    TestApp.testStore.dispatch(actions.storeUserPath({
      positionUnit: Length.MILLIMETRES,
      velocityUnit: Velocity.MILLIMETRES_PER_SECOND,
      series: [{
        name: 'X',
        points: [{ dt: 0, time: 0, pos: 1.1, vel: 0.1 }, { dt: 0.2, time: 0.2, pos: 1.5, vel: 0.5 }]
      }, {
        name: 'Y',
        points: [{ dt: 0, time: 0, pos: 2.1, vel: 1 }, { dt: 0.2, time: 0.2, pos: 3.1, vel: 0 }]
      }],
    }));

    await doExport({ value: 0.1, units: Time.MILLISECONDS });
    const lines = fileContent.split('\n');
    expect(lines[0].trim())
      .toBe('Time Step (ms),Accumulated Time (ms),X Position (mm),X Velocity (mm/s),Y Position (mm),Y Velocity (mm/s)');
    expect(lines[1].trim()).toBe('0,0,1.1,0.1,2.1,1');
    expect(lines.length).toBeCloseTo(2000, -1); // 0.2s at 0.1ms intervals
    const lastLine = _.last(lines.filter(l => l.trim().length > 0));
    const data = parseCsv(lastLine!);
    expect(data).toBeDeepCloseTo([[0.1, 200, 1.5, 0.5, 3.1, 0]], 3);
  });
});
