import { createSelector } from 'reselect';
import { tryAccess } from '@zaber/toolbox';

import { selectFactoryReset } from '../store';
import {
  selectDevices,
  selectAxes,
} from '../connection_manager/selectors';
import { tryExtractDeviceKey } from '../keys';

export const selectAll = createSelector(selectFactoryReset, state => state);

export const selectAxis = createSelector(selectFactoryReset, selectAxes,
  (state, axes) => tryAccess(axes, state.currentKey));

export const selectDevice = createSelector(selectFactoryReset, selectDevices,
  (state, devices) => tryAccess(devices, tryExtractDeviceKey(state.currentKey)));
