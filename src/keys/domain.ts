import type { Nullable } from '@zaber/toolbox';
import _ from 'lodash';

import {
  makeKey,
  extendKey,
  EntityKeyType,
  EntityKey,
  extractKey,
  getIdFromKey,
  tryExtractKey,
  getEntityType,
  tryGetIdFromKey,
} from './generic';

export function makeRouterKey(url: string): EntityKey {
  return makeKey(EntityKeyType.ROUTER, url);
}

export function makeConnectionKey(routerKey: EntityKey, connectionId: string): EntityKey {
  return extendKey(routerKey, EntityKeyType.ROUTER, EntityKeyType.CONNECTION, connectionId);
}

export function makeDeviceKey(connectionKey: EntityKey, deviceAddress: string | number): EntityKey {
  return extendKey(connectionKey, EntityKeyType.CONNECTION, EntityKeyType.DEVICE, deviceAddress);
}

export function makeAxisKey(deviceKey: EntityKey, axisNumber: string | number): EntityKey {
  return extendKey(deviceKey, EntityKeyType.DEVICE, EntityKeyType.AXIS, axisNumber);
}

const includedInKeys = {
  [EntityKeyType.AXIS]: [EntityKeyType.AXIS],
  [EntityKeyType.DEVICE]: [EntityKeyType.AXIS, EntityKeyType.DEVICE],
  [EntityKeyType.CONNECTION]: [EntityKeyType.AXIS, EntityKeyType.DEVICE, EntityKeyType.CONNECTION],
  [EntityKeyType.ROUTER]: [EntityKeyType.AXIS, EntityKeyType.DEVICE, EntityKeyType.CONNECTION, EntityKeyType.ROUTER],
};

const keyParts = {
  [EntityKeyType.ROUTER]: 0,
  [EntityKeyType.CONNECTION]: 1,
  [EntityKeyType.DEVICE]: 2,
  [EntityKeyType.AXIS]: 3,
};

export function extractDeviceKey(key: EntityKey): EntityKey {
  return extractKey(key, includedInKeys[EntityKeyType.DEVICE], EntityKeyType.DEVICE, keyParts[EntityKeyType.DEVICE] + 1);
}

export function tryExtractDeviceKey(key: Nullable<EntityKey>): Nullable<EntityKey> {
  return tryExtractKey(key, includedInKeys[EntityKeyType.DEVICE], EntityKeyType.DEVICE, keyParts[EntityKeyType.DEVICE] + 1);
}

export function extractConnectionKey(key: EntityKey): EntityKey {
  return extractKey(key, includedInKeys[EntityKeyType.CONNECTION], EntityKeyType.CONNECTION, keyParts[EntityKeyType.CONNECTION] + 1);
}

export function tryExtractConnectionKey(key: Nullable<EntityKey>): Nullable<EntityKey> {
  return tryExtractKey(key, includedInKeys[EntityKeyType.CONNECTION], EntityKeyType.CONNECTION, keyParts[EntityKeyType.CONNECTION] + 1);
}

export function extractRouterKey(key: EntityKey): EntityKey {
  return extractKey(key, includedInKeys[EntityKeyType.ROUTER], EntityKeyType.ROUTER, keyParts[EntityKeyType.ROUTER] + 1);
}

export function tryExtractRouterKey(key: Nullable<EntityKey>): Nullable<EntityKey> {
  return tryExtractKey(key, includedInKeys[EntityKeyType.ROUTER], EntityKeyType.ROUTER, keyParts[EntityKeyType.ROUTER] + 1);
}

export function getRouterUrl(key: EntityKey): string {
  return getIdFromKey(key, includedInKeys[EntityKeyType.ROUTER], keyParts[EntityKeyType.ROUTER]);
}

export function getConnectionId(key: EntityKey): string {
  return getIdFromKey(key, includedInKeys[EntityKeyType.CONNECTION], keyParts[EntityKeyType.CONNECTION]);
}

export function getDeviceAddress(key: EntityKey): number {
  return +getIdFromKey(key, includedInKeys[EntityKeyType.DEVICE], keyParts[EntityKeyType.DEVICE]);
}

export function getAxisNumber(key: EntityKey): number {
  return +getIdFromKey(key, includedInKeys[EntityKeyType.AXIS], keyParts[EntityKeyType.AXIS]);
}

export function tryGetAxisNumber(key: Nullable<EntityKey>): Nullable<number> {
  const axisString = tryGetIdFromKey(key, includedInKeys[EntityKeyType.AXIS], keyParts[EntityKeyType.AXIS]);
  return axisString ? +axisString : null;
}

export function getIncludedKeys(key: Nullable<EntityKey>): Partial<Record<EntityKeyType, EntityKey>> {
  const keys: Partial<Record<EntityKeyType, EntityKey>> = {};
  const keyType = getEntityType(key);
  if (!keyType || !key) {
    return keys;
  }
  for (const [testedTypeStr, includedIn] of _.toPairs(includedInKeys)) {
    const testedType = testedTypeStr as EntityKeyType;
    if (includedIn.includes(keyType)) {
      keys[testedType] = extractKey(key, includedIn, testedType, keyParts[testedType] + 1);
    }
  }
  return keys;
}
