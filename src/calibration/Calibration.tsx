import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useActions } from '@zaber/toolbox/lib/redux';
import { match, P } from 'ts-pattern';

import { ConnectionsView } from '../connection_manager';
import { NoContentMessage, Title } from '../components';

import { actions as actionsDefinition } from './actions';
import { selectState } from './selectors';
import { Table } from './Table';
import { Controls } from './Controls';
import { PopulatedChart } from './Chart';

const TITLE = 'Welcome to Calibration!';

const NoneSelected = () => <NoContentMessage title={TITLE} message="Select device or axis to calibrate."/>;
const Loading = () => <NoContentMessage title={TITLE} type="working" message="Loading calibration..."/>;

export const Calibration: React.FC = () => {
  const state = useSelector(selectState);
  const actions = useActions(actionsDefinition);
  useEffect(() => {
    if (state.selected != null) {
      actions.loadTable();
    }
  }, [state.selected]);
  return (
    <div className="calibration-app connection-view-and-app">
      <Title>Calibration</Title>
      <ConnectionsView
        selectable={['integrated', 'axis']}
        selected={state?.selected ?? null}
        onSelect={actions.selectDeviceOrAxis}/>
      <div className="app-ui">
        {match(state?.table)
          .with(P.nullish, () => <NoneSelected/>)
          .with({ state: 'loading' }, () => <Loading/>)
          .with({ state: 'error' }, ({ error }) => <NoContentMessage title={TITLE} message={error} type="error"/>)
          .with({ state: 'loaded' }, () => <>
            <Table/>
            <PopulatedChart/>
            <Controls/>
          </>)
          .exhaustive()}
      </div>
    </div>
  );
};
