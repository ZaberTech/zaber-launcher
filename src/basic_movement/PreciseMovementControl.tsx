import { Units } from '@zaber/motion';
import _ from 'lodash';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { InputLabel } from '@zaber/react-library';

import { convertValueUnits, getUnits, InputWithUnits, Measurement, MeasurementOK, measurementOK } from '../units';
import type { RT } from '../utils';
import { RootState, selectUnits } from '../store';

import { actions as actionDefinitions } from './actions';
import { getDisplayPrecision } from './types';
import type { PreciseMovementValues } from './reducer';
import type { AxisStateComplete } from './selectors';

interface Limits {
  min: number;
  max: number;
}

interface Props {
  axis: AxisStateComplete;
  setting: string;
  label: InputLabel.Content;
  /**
   * The controls that will be placed to the right of this input to control it.
   * They are passed the value and units to move to, or null if the input value is invalid
   * */
  controls: (target: MeasurementOK | null) => React.ReactNode;
  onEnterPress?: (target: MeasurementOK) => void;
  limits?: Limits | null;
  defaultValue?: number;
  movementType: keyof PreciseMovementValues;
  title: string;
}

interface StateProps {
  actions: typeof actionDefinitions;
  unitInfo: RT<typeof selectUnits>;
}

class BasePreciseMovementControl extends Component<Props & StateProps> {
  getMeasurement(): Measurement {
    const { axis, movementType, defaultValue } = this.props;
    const measurement = axis.preciseMovement[movementType];
    if (measurement === null) {
      return { value: defaultValue ?? 0, units: 'default' };
    }

    return measurement;
  }


  checkOutOfRange() {
    const { limits, axis } = this.props;
    const precision = getDisplayPrecision(axis);
    const valueAndUnits = this.getMeasurement();
    const value = valueAndUnits.value;
    const units = getUnits(valueAndUnits);
    const unitInfo = this.props.unitInfo.info[this.props.axis.key]?.settings[this.props.setting];
    if (value == null || limits?.min == null || limits?.max == null || !unitInfo?.supported) {
      return null;
    }
    const limitsInUnit = {
      min: convertValueUnits(unitInfo, limits.min, Units.NATIVE, units),
      max: convertValueUnits(unitInfo, limits.max, Units.NATIVE, units),
    };
    limitsInUnit.min = _.round(limitsInUnit.min, precision);
    limitsInUnit.max = _.round(limitsInUnit.max, precision);

    const valueRounded = _.round(value, precision);
    const isOut = valueRounded < limitsInUnit.min || limitsInUnit.max < valueRounded;
    return {
      isOut,
      ...limitsInUnit,
    };
  }

  onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    const valueAndUnits = this.getMeasurement();
    if (e.key === 'Enter' && this.props.onEnterPress && measurementOK(valueAndUnits)) {
      this.props.onEnterPress(valueAndUnits);
    }
  };

  render(): React.ReactNode {
    const {
      actions,
      setting,
      label,
      controls,
      movementType,
      title,
      axis,
    } = this.props;
    const { key } = axis;
    const bounds = this.checkOutOfRange();

    const valueAndUnits = this.getMeasurement();
    const target = measurementOK(valueAndUnits) ? valueAndUnits : null;

    return (
      <InputWithUnits
        title={title}
        className="precise-control"
        unitFrom={{ setting, entity: key }}
        measure={valueAndUnits}
        onChange={valueAndUnits => actions.setPreciseMeasurement(key, movementType, valueAndUnits)}
        onKeyDown={this.onKeyDown}
        displayPrecision={getDisplayPrecision(axis)}
        labelContent={label}
        status={bounds?.isOut ? 'error' : null}
        message={bounds?.isOut ? `Value must be between ${bounds.min} and ${bounds.max}.` : undefined}
      >
        <div className="controls">{controls(target)}</div>
      </InputWithUnits>
    );
  }
}

export const PreciseMovementControl = connect(
  (state: RootState): Omit<StateProps, 'actions'> => ({
    unitInfo: selectUnits(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionDefinitions, dispatch),
  }),
)(BasePreciseMovementControl);
