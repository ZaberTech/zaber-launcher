import React, { useState } from 'react';
import { Text, Icons } from '@zaber/react-library';
import { isArray } from 'lodash';

import { LockstepGroup, connectionName, deviceNameWithLabel, peripheralNameWithLabel } from '../connection_manager';
import { DeviceNumber } from '../components';

import { AxisStateComplete, Movable, getAxisFromMovable } from './selectors';

export const MovableLabel: React.FC<{ movable: Movable }> = ({ movable }) => {
  const [expanded, setState] = useState(false);
  const primaryAxis = getAxisFromMovable(movable);
  const { device, connection, lockstep } = primaryAxis;
  return (
    <div className="axis-label">
      {primaryAxis.isPinned && <>
        <Text t={Text.Type.H5}>
          {connectionName(connection)}
        </Text>
        <Icons.ArrowCollapsed/>
      </>}
      <Text t={Text.Type.H5}>
        <DeviceNumber>{device.address}</DeviceNumber>&nbsp;
        {deviceNameWithLabel(device)}
      </Text>
      {(device.isMultiAxis || device.isController) && <>
        <Icons.ArrowCollapsed/>
        {!lockstep && <Text t={Text.Type.H5}>{axisDescription(primaryAxis)}</Text>}
        {lockstep && (
          <span className="dropdown-related">
            <span className="dropdown-initiator" title="Click for more info" data-testid={getLockstepTestId(primaryAxis, 'initiator')}
              onClick={() => { setState(!expanded) }}>
              <Text t={Text.Type.H5}>
                {lockstepLabel(lockstep, primaryAxis)}&nbsp;
                <Icons.DropdownArrow className="dropdown-arrow"/>
              </Text>
            </span>

            {expanded && <div data-testid={getLockstepTestId(primaryAxis, 'info')} className="dropdown-info">
              {isArray(movable) && movable.map((axis, key) => <div key={key}>{axisDescription(axis)}</div>)}
            </div>}
          </span>)}
      </>}
    </div>
  );
};

const axisDescription = (primaryAxis: AxisStateComplete) => {
  let label: string | undefined;
  if (primaryAxis.identity.isPeripheral) {
    label = peripheralNameWithLabel(primaryAxis);
  } else if (primaryAxis.label) {
    label = primaryAxis.label;
  }
  return (<span> Axis {primaryAxis.axisNumber}{label && <>&nbsp;{label}</>} </span>);
};

const getLockstepTestId = (primaryAxis: AxisStateComplete, modifier: 'info'|'initiator') => {
  const { device, connection, lockstep } = primaryAxis;
  return `con ${connectionName(connection)} > dev ${device.address} > lockstep ${modifier} ${lockstep?.groupNumber}`;
};

const lockstepLabel = (lockstep: LockstepGroup, primaryAxis: AxisStateComplete) => {
  let label = `Lockstep Group ${lockstep.groupNumber}`;
  if (primaryAxis.label) {
    label = `${primaryAxis.label} (${label})`;
  }
  return label;
};
