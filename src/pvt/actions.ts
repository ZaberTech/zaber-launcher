import type { EntityKey } from '../keys';
import type { MeasurementOK } from '../units';
import { actionBuilder } from '../utils';

import type { Bug, CsvImportOptions, Message, Path, SeriesToAxisMap, Severity, Span, ViewMode } from './types';


export enum ActionTypes {
  // File operations
  FILE_OPEN = 'PVT_FILE_OPEN',
  FILE_SAVE = 'PVT_FILE_SAVE',
  FILE_SAVE_AS = 'PVT_FILE_SAVE_AS',
  FILE_SAVE_AND_OPEN_EXAMPLE = 'PVT_FILE_SAVE_AND_OPEN_EXAMPLE',
  FILE_SET_LAST_SAVED = 'PVT_FILE_SET_LAST_SAVED',
  SET_AUTO_RELOAD = 'PVT_SET_AUTO_RELOAD',
  RELOAD = 'PVT_RELOAD',
  SET_CURRENT_FILE = 'PVT_SET_CURRENT_FILE',
  SAVE_TIME_SERIES_IMAGE = 'PVT_SAVE_TIME_SERIES_IMAGE',
  EXPORT_IMAGE = 'PVT_EXPORT_IMAGE',
  EXPORT_TRAJECTORY = 'PVT_EXPORT_TRAJECTORY',

  // Document
  STORE_USER_PATH = 'PVT_STORE_USER_PATH',
  SET_SERIES_LIMITS = 'PVT_SET_SERIES_LIMITS',
  EDIT_SEGMENT_TIME = 'PVT_EDIT_SEGMENT_TIME',
  EDIT_POSITION = 'PVT_EDIT_POINT_POSITION',
  EDIT_VELOCITY = 'PVT_EDIT_POINT_VELOCITY',
  ADD_POINT = 'PVT_ADD_POINT',
  DELETE_POINT = 'PVT_DELETE_POINT',
  AUTO_CORRECT = 'PVT_AUTO_CORRECT',

  // Undo/Redo
  COMMIT_EDIT = 'PVT_COMMIT_EDIT',
  UNDO = 'PVT_UNDO',
  REDO = 'PVT_REDO',
  CLEAR_UNDO_HISTORY = 'PVT_CLEAR_UNDO_HISTORY',

  // Display
  STORE_DEVICE_PATH = 'PVT_STORE_DEVICE_PATH',
  SET_VIEW_MODE = 'PVT_SET_VIEW_MODE',
  SET_SELECTION = 'PVT_SET_SELECTION',
  SET_SERIES_DETAILS_VISIBLE = 'PVT_SET_SERIES_DETAILS_VISIBLE',
  SET_DRAWING_AXIS = 'PVT_SET_DRAWING_AXIS',
  SET_TIME_SERIES_VISIBLE = 'PVT_SET_TIME_SERIES_VISIBLE',
  SHOW_ERRORS = 'PVT_SHOW_ERROR',
  STORE_BUGS = 'PVT_STORE_BUGS',
  SET_FIDELITY = 'PVT_SET_FIDELITY',

  // Device-related
  SELECT_DEVICE = 'PVT_SELECT_DEVICE',
  SELECT_AXES = 'PVT_SELECT_AXES',
  STORE_AXIS_SELECTIONS = 'PVT_STORE_AXIS_SELECTIONS',
  CLEAR_AXIS_SELECTION = 'PVT_CLEAR_AXIS_SELECTION',
  SET_DEVICE_LIMITS = 'PVT_SET_DEVICE_LIMITS',

  // Dialog-related
  SET_CSV_IMPORT_OPTIONS = 'PVT_SET_CSV_IMPORT_OPTIONS',
  CONTINUE_IMPORT = 'PVT_CONTINUE_IMPORT',
  CANCEL_IMPORT = 'PVT_CANCEL_IMPORT',
  SET_TRAJECTORY_EXPORT_DIALOG_VISIBLE = 'PVT_SET_TRAJECTORY_EXPORT_DIALOG_VISIBLE',
  SET_DEVICE_SELECTION_DIALOG_VISIBLE = 'PVT_SET_DEVICE_DIALOG_VISIBLE',
  SET_AXIS_DIALOG_VISIBLE = 'PVT_SET_AXIS_DIALOG_VISIBLE',
  COMMIT_DEVICE_SELECTION = 'PVT_COMMIT_DEVICE_SELECTION',
  SET_EXPORT_IN_PROGRESS = 'PVT_SET_EXPORT_IN_PROGRESS',
  SET_BUFFER_LOAD_DIALOG_VISIBLE = 'PVT_SET_DEVICE_LOAD_DIALOG_VISIBLE',

  // Device operations
  RESET = 'PVT_RESET',
  RUN = 'PVT_RUN',
  STOP = 'PVT_STOP',
  POLL_DEVICE_POSITION = 'PVT_POLL_DEVICE_POSITION',
  SET_POLL_DEFAULT = 'PVT_SET_POLL_DEFAULT',
  STORE_DEVICE_POSITION = 'PVT_STORE_DEVICE_POSITION',
  STORE_RELATIVE_START = 'PVT_STORE_RELATIVE_START',
  STORE_START_POSITION = 'PVT_STORE_START_POSITION',

  // Device buffer operations
  GET_BUFFER_AXIS_COUNT = 'PVT_GET_BUFFER_AXIS_COUNT',
  STORE_BUFFER_AXIS_COUNT = 'PVT_STORE_BUFFER_AXIS_COUNT',
  STORE_BUFFER_LIST = 'PVT_STORE_BUFFER_LIST',
  LOAD_FROM_DEVICE_BUFFER = 'PVT_LOAD_FROM_DEVICE_BUFFER',
}


export interface ActionsToPayloads {
  // File operations
  [ActionTypes.FILE_OPEN]: void;
  [ActionTypes.FILE_SAVE]: void;
  [ActionTypes.FILE_SAVE_AS]: void;
  [ActionTypes.FILE_SAVE_AND_OPEN_EXAMPLE]: { path: Path };
  [ActionTypes.SET_AUTO_RELOAD]: { autoReload: boolean };
  [ActionTypes.RELOAD]: { forced: boolean };
  [ActionTypes.SET_CURRENT_FILE]: { path: string | null };
  [ActionTypes.SAVE_TIME_SERIES_IMAGE]: { format: 'png' | 'jpg'; data: string };
  [ActionTypes.EXPORT_IMAGE]: { region: DOMRect };
  [ActionTypes.EXPORT_TRAJECTORY]: { timeStep: MeasurementOK };
  [ActionTypes.FILE_SET_LAST_SAVED]: { path: Path | null };

  // Document
  [ActionTypes.STORE_USER_PATH]: { path: Path };
  [ActionTypes.SET_SERIES_LIMITS]: { seriesIndex: number; posLimits: Span | null; velLimits: Span | null; accelLimits: Span | null };
  [ActionTypes.EDIT_SEGMENT_TIME]: { index: number; value: number };
  [ActionTypes.EDIT_POSITION]: { index: number; values: Record<string, number> };
  [ActionTypes.EDIT_VELOCITY]: { index: number; values: Record<string, number> };
  [ActionTypes.ADD_POINT]: { index: number; dt?: number };
  [ActionTypes.DELETE_POINT]: { index: number };
  [ActionTypes.AUTO_CORRECT]: void;

  // Undo/Redo
  [ActionTypes.COMMIT_EDIT]: void;
  [ActionTypes.UNDO]: void;
  [ActionTypes.REDO]: void;
  [ActionTypes.CLEAR_UNDO_HISTORY]: void;

  // Display
  [ActionTypes.STORE_DEVICE_PATH]: { path: Path | null };
  [ActionTypes.SET_VIEW_MODE]: { mode: ViewMode };
  [ActionTypes.SET_SELECTION]: { index: number | null };
  [ActionTypes.SET_SERIES_DETAILS_VISIBLE]: { series: string; visible: boolean };
  [ActionTypes.SET_DRAWING_AXIS]: { axis: number; series: number };
  [ActionTypes.SET_TIME_SERIES_VISIBLE]: { index: number; visible: boolean };
  [ActionTypes.SHOW_ERRORS]: { errors: Message | Message[] | null };
  [ActionTypes.STORE_BUGS]: { bugs: Bug[] };
  [ActionTypes.SET_FIDELITY]: { fidelity: 'low' | 'high' };

  // Device-related
  [ActionTypes.SELECT_DEVICE]: { deviceKey: EntityKey | null };
  [ActionTypes.SELECT_AXES]: { axes: SeriesToAxisMap };
  [ActionTypes.STORE_AXIS_SELECTIONS]: { selections: SeriesToAxisMap };
  [ActionTypes.CLEAR_AXIS_SELECTION]: void;
  [ActionTypes.SET_DEVICE_LIMITS]: { position: (Span | null)[]; velocity: (Span | null)[]; acceleration: (Span | null)[] };

  // Dialog-related
  [ActionTypes.SET_CSV_IMPORT_OPTIONS]: { options: Partial<CsvImportOptions> };
  [ActionTypes.CONTINUE_IMPORT]: void;
  [ActionTypes.CANCEL_IMPORT]: void;
  [ActionTypes.SET_TRAJECTORY_EXPORT_DIALOG_VISIBLE]: { visible: boolean };
  [ActionTypes.SET_DEVICE_SELECTION_DIALOG_VISIBLE]: { visible: boolean };
  [ActionTypes.SET_AXIS_DIALOG_VISIBLE]: { visible: boolean; showBackButton: boolean };
  [ActionTypes.COMMIT_DEVICE_SELECTION]: void;
  [ActionTypes.SET_EXPORT_IN_PROGRESS]: { exporting: boolean };
  [ActionTypes.SET_BUFFER_LOAD_DIALOG_VISIBLE]: { visible: boolean };

  // Device operations
  [ActionTypes.RESET]: void;
  [ActionTypes.RUN]: void;
  [ActionTypes.STOP]: void;
  [ActionTypes.POLL_DEVICE_POSITION]: { interval: number | 'stop' | 'default' };
  [ActionTypes.SET_POLL_DEFAULT]: { interval: number | 'stop' };
  [ActionTypes.STORE_DEVICE_POSITION]: { pos: number[]; native: number[]; playbackTime: number | null };
  [ActionTypes.STORE_RELATIVE_START]: { pos: number[]; axisSelections: SeriesToAxisMap };
  [ActionTypes.STORE_START_POSITION]: { startPosition: number[] };

  // Device buffer operations
  [ActionTypes.STORE_BUFFER_LIST]: { buffers: string[] | null };
  [ActionTypes.STORE_BUFFER_AXIS_COUNT]: { bufferId: string; axisCount: number };
  [ActionTypes.GET_BUFFER_AXIS_COUNT]: { bufferId: string };
  [ActionTypes.LOAD_FROM_DEVICE_BUFFER]: { bufferId: string; axes: SeriesToAxisMap };
}


const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  // File operations
  fileOpen: () => buildAction(ActionTypes.FILE_OPEN),
  fileSave: () => buildAction(ActionTypes.FILE_SAVE),
  fileSaveAs: () => buildAction(ActionTypes.FILE_SAVE_AS),
  fileSaveAndOpenExample: (path: Path) => buildAction(ActionTypes.FILE_SAVE_AND_OPEN_EXAMPLE, { path }),
  fileSetLastSavedContent: (path: Path | null) => buildAction(ActionTypes.FILE_SET_LAST_SAVED, { path }),
  setAutoReload: (autoReload: boolean) => buildAction(ActionTypes.SET_AUTO_RELOAD, { autoReload }),
  reload: (forced: boolean = false) => buildAction(ActionTypes.RELOAD, { forced }),
  setCurrentFile: (path: string | null) => buildAction(ActionTypes.SET_CURRENT_FILE, { path }),
  saveTimeSeriesImage: (format: 'png' | 'jpg', data: string) => buildAction(ActionTypes.SAVE_TIME_SERIES_IMAGE, { format, data }),
  exportImage: (region: DOMRect) => buildAction(ActionTypes.EXPORT_IMAGE, { region }),
  exportTrajectory: (timeStep: MeasurementOK) => buildAction(ActionTypes.EXPORT_TRAJECTORY, { timeStep }),

  // Document
  storeUserPath: (path: Path) => buildAction(ActionTypes.STORE_USER_PATH, { path }),
  setSeriesLimits: (seriesIndex: number, posLimits: Span | null, velLimits: Span | null, accelLimits: Span | null) =>
    buildAction(ActionTypes.SET_SERIES_LIMITS, { seriesIndex, posLimits, velLimits, accelLimits }),
  editSegmentTime: (index: number, value: number) => buildAction(ActionTypes.EDIT_SEGMENT_TIME, { index, value }),
  editPosition: (index: number, values: Record<string, number>) => buildAction(ActionTypes.EDIT_POSITION, { index, values }),
  editVelocity: (index: number, values: Record<string, number>) => buildAction(ActionTypes.EDIT_VELOCITY, { index, values }),
  addPoint: (index: number, dt?: number) => buildAction(ActionTypes.ADD_POINT, { index, dt }),
  deletePoint: (index: number) => buildAction(ActionTypes.DELETE_POINT, { index }),
  autoCorrect: () => buildAction(ActionTypes.AUTO_CORRECT),

  // Undo/Redo
  commitEdit: () => buildAction(ActionTypes.COMMIT_EDIT),
  undo: () => buildAction(ActionTypes.UNDO),
  redo: () => buildAction(ActionTypes.REDO),
  clearUndoHistory: () => buildAction(ActionTypes.CLEAR_UNDO_HISTORY),

  // Display
  storeDevicePath: (path: Path | null) => buildAction(ActionTypes.STORE_DEVICE_PATH, { path }),
  setViewMode: (mode: ViewMode) => buildAction(ActionTypes.SET_VIEW_MODE, { mode }),
  setSelection: (index: number | null) => buildAction(ActionTypes.SET_SELECTION, { index }),
  setSeriesDetailsVisible: (series: string, visible: boolean) => buildAction(ActionTypes.SET_SERIES_DETAILS_VISIBLE, { series, visible }),
  showError: (error: string, severity?: Severity) =>
    buildAction(ActionTypes.SHOW_ERRORS, { errors: [{ text: error, severity: severity ?? 'error' }] }),
  showErrors: (errors: Message[]) => buildAction(ActionTypes.SHOW_ERRORS, { errors }),
  clearError: () => buildAction(ActionTypes.SHOW_ERRORS, { errors: null }),
  setDrawingAxis: (axis: number, series: number) => buildAction(ActionTypes.SET_DRAWING_AXIS, { axis, series }),
  setTimeSeriesVisible: (index: number, visible: boolean) => buildAction(ActionTypes.SET_TIME_SERIES_VISIBLE, { index, visible }),
  storeBugs: (bugs: Bug[]) => buildAction(ActionTypes.STORE_BUGS, { bugs }),
  setFidelity: (fidelity: 'low' | 'high') => buildAction(ActionTypes.SET_FIDELITY, { fidelity }),

  // Device-related
  selectDevice: (deviceKey: EntityKey | null) => buildAction(ActionTypes.SELECT_DEVICE, { deviceKey }),
  selectAxes: (axes: SeriesToAxisMap) => buildAction(ActionTypes.SELECT_AXES, { axes }),
  clearAxisSelection: () => buildAction(ActionTypes.CLEAR_AXIS_SELECTION),
  setDeviceLimits: (position: (Span | null)[], velocity: (Span | null)[], acceleration: (Span | null)[]) =>
    buildAction(ActionTypes.SET_DEVICE_LIMITS, { position, velocity, acceleration }),

  // Dialog-related
  setCsvImportOptions: (options: Partial<CsvImportOptions>) => buildAction(ActionTypes.SET_CSV_IMPORT_OPTIONS, { options }),
  continueImport: () => buildAction(ActionTypes.CONTINUE_IMPORT),
  cancelImport: () => buildAction(ActionTypes.CANCEL_IMPORT),
  showTrajectoryExportDialog: (visible: boolean) => buildAction(ActionTypes.SET_TRAJECTORY_EXPORT_DIALOG_VISIBLE, { visible }),
  setDeviceSelectionDialogVisible: (visible: boolean) => buildAction(ActionTypes.SET_DEVICE_SELECTION_DIALOG_VISIBLE, { visible }),
  setAxisDialogVisible: (visible: boolean, showBackButton: boolean) =>
    buildAction(ActionTypes.SET_AXIS_DIALOG_VISIBLE, { visible, showBackButton }),
  commitDeviceSelection: () => buildAction(ActionTypes.COMMIT_DEVICE_SELECTION),
  setExportInProgress: (exporting: boolean) => buildAction(ActionTypes.SET_EXPORT_IN_PROGRESS, { exporting }),
  setBufferLoadDialogVisible: (visible: boolean) => buildAction(ActionTypes.SET_BUFFER_LOAD_DIALOG_VISIBLE, { visible }),

  // Device operations
  reset: () => buildAction(ActionTypes.RESET),
  run: () => buildAction(ActionTypes.RUN),
  stop: () => buildAction(ActionTypes.STOP),
  pollDevicePosition: (interval: number | 'stop' | 'default') => buildAction(ActionTypes.POLL_DEVICE_POSITION, { interval }),
  setPollDefault: (interval: number | 'stop') => buildAction(ActionTypes.SET_POLL_DEFAULT, { interval }),
  storeDevicePosition: (pos: number[], native: number[], playbackTime: number | null = null) =>
    buildAction(ActionTypes.STORE_DEVICE_POSITION, { pos, native, playbackTime }),
  storeRelativeStart: (pos: number[], axisSelections: SeriesToAxisMap) =>
    buildAction(ActionTypes.STORE_RELATIVE_START, { pos, axisSelections }),
  storeStartPosition: (startPosition: number[]) => buildAction(ActionTypes.STORE_START_POSITION, { startPosition }),

  // Device buffer operations
  storeBufferList: (buffers: string[] | null) => buildAction(ActionTypes.STORE_BUFFER_LIST, { buffers }),
  getBufferAxisCount: (bufferId: string) => buildAction(ActionTypes.GET_BUFFER_AXIS_COUNT, { bufferId }),
  storeBufferAxisCount: (bufferId: string, axisCount: number) => buildAction(ActionTypes.STORE_BUFFER_AXIS_COUNT, { bufferId, axisCount }),
  loadFromDeviceBuffer: (bufferId: string, axes: SeriesToAxisMap) => buildAction(ActionTypes.LOAD_FROM_DEVICE_BUFFER, { bufferId, axes }),
  storeAxisSelections: (selections: SeriesToAxisMap) => buildAction(ActionTypes.STORE_AXIS_SELECTIONS, { selections }),
};
