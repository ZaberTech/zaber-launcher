import _ from 'lodash';
import React, { useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { P, match } from 'ts-pattern';
import { Icons, Modal, Text, SimpleSelect, ButtonRowConfirmCancel } from '@zaber/react-library';
import { Angle, Length } from '@zaber/motion';

import { useActions } from '../utils';
import {
  IdentifiedAxisState,
  deviceLabelOrName,
  peripheralLabelOrName,
  selectIdentifiedAxes,
  selectIdentifiedDevices
} from '../connection_manager';
import { fwToString } from '../devices';

import { actions as actionDefinitions } from './actions';
import { selectDeviceLoadDialog, selectDeviceOptions, selectViewMode } from './selectors';
import { ViewMode, type SeriesToAxisMap } from './types';
import { axisHasDimensionOf, makeBufferAxisName } from './utils';


export const BufferLoadDialog: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const { buffers, bufferAxes } = useSelector(selectDeviceLoadDialog);
  const { deviceKey } = useSelector(selectDeviceOptions);
  const identifiedDevices = useSelector(selectIdentifiedDevices);
  const identifiedAxes = useSelector(selectIdentifiedAxes);
  const viewMode = useSelector(selectViewMode);
  const device = identifiedDevices[deviceKey!];

  const [selectedBuffer, setSelectedBuffer] = useState<string | null>(null);
  const [axisSelections, setAxisSelections] = useState<(IdentifiedAxisState | null)[]>([]);
  const numAxesToSelect = bufferAxes[selectedBuffer ?? ''] ?? 0;


  // Generate list of supported axes to select from.
  const selectableAxes = useMemo(() => {
    if (!device) {
      return [];
    }

    return device.axes.map(axisKey => {
      const axis = identifiedAxes[axisKey];
      if (axis && (axisHasDimensionOf(axis, Length.METRES) || axisHasDimensionOf(axis, Angle.DEGREES))) {
        return axis;
      }

      return null;
    }).filter(axis => axis != null) as IdentifiedAxisState[];
  }, [device]);

  // Select default buffer when the buffer list gets loaded.
  useEffect(() => {
    if (buffers && buffers.length > 0) {
      changeBufferSelection(buffers[0]);
    }
  }, [buffers]);


  // Select default axes when number of buffer axes for the selected buffer is known.
  useEffect(() => {
    const axes = selectableAxes.slice(0, numAxesToSelect);

    if (!_.isEqual(axes, axisSelections)) {
      setAxisSelections(axes);
    }
  }, [selectableAxes, bufferAxes]);


  // Select default axes after the default buffer is selected.
  useEffect(() => {
    const axes = selectableAxes.slice(0, numAxesToSelect);

    if (axisSelections.length === 0) {
      setAxisSelections(axes);
    }
  }, [selectedBuffer]);


  function changeBufferSelection(bufferId: string) {
    actions.getBufferAxisCount(bufferId);
    setSelectedBuffer(bufferId);
  }


  function changeAxisSelection(bufferAxisIndex: number, axisNumber: number) {
    const axis = selectableAxes.find(axis => axis.axisNumber === axisNumber)!;
    const newAxisSelections = axisSelections.map(axis => axis?.axisNumber === axisNumber ? null : axis);
    newAxisSelections[bufferAxisIndex] = axis;
    const unselectedAxes = selectableAxes.filter(axis => !newAxisSelections.includes(axis));
    for (let i = 0; i < newAxisSelections.length; i++) {
      if (newAxisSelections[i] == null) {
        newAxisSelections[i] = unselectedAxes.shift() ?? null;
      }
    }
    setAxisSelections(newAxisSelections);
  }

  const confirmDisabled = !selectedBuffer || axisSelections.filter(a => a != null).length !== numAxesToSelect;

  return <Modal
    headerIcon={<Icons.Buffer/>}
    headerText="Buffer Selection"
    isOpen
    buttons={<ButtonRowConfirmCancel
      confirmText="Read"
      onConfirm={confirmDisabled ? 'disabled' : () => {
        actions.setBufferLoadDialogVisible(false);

        const selectionMap: SeriesToAxisMap = {};
        axisSelections.forEach((_, bufferAxisIndex) => {
          selectionMap[makeBufferAxisName(selectedBuffer!, bufferAxisIndex)] = bufferAxisIndex;
        });

        actions.loadFromDeviceBuffer(selectedBuffer!, selectionMap);

        if (viewMode === ViewMode.Welcome) {
          actions.setViewMode(ViewMode.TimeSeries);
        }
      }}
      onCancel={() => actions.setBufferLoadDialogVisible(false)}
    />}
    onRequestClose={() => actions.setBufferLoadDialogVisible(false)}
  >
    <div className="buffer-load-dialog">
      <Text e={Text.Emphasis.Bold}>{device ? deviceLabelOrName(device) : 'No device selected'}</Text>
      <Text>{`SN: ${device?.identity?.serialNumber}`}&emsp;
        {`FW: ${device?.identity?.firmwareVersion ? fwToString(device?.identity?.firmwareVersion) : '<unidentified>'}`}
      </Text>
      {match(buffers)
        .with(P.nullish, () => <Text>Checking device...</Text>)
        .with([], () => <div className="notice">
          <Icons.ErrorWarning className="info-icon"/>
          <Text>There are no PVT buffers stored on this device.</Text>
        </div>)
        .otherwise(() => <SimpleSelect
          data-testid="buffer-select"
          value={selectedBuffer}
          onValueChange={changeBufferSelection}
          options={buffers!.map(name => ({
            label: name,
            value: name,
          }))}
        />)}
      {numAxesToSelect > 0 && <>
        <hr/>
        <Text>Please select the axes this buffer is intended to use.</Text>
        <div className="axis-selections">
          {_.range(numAxesToSelect).map(bufferAxisIndex => <React.Fragment key={bufferAxisIndex}>
            <Text>{makeBufferAxisName(selectedBuffer!, bufferAxisIndex)}</Text>
            <SimpleSelect
              data-testid={`buffer-axis-${bufferAxisIndex}`}
              value={axisSelections[bufferAxisIndex]?.axisNumber ?? null}
              options={selectableAxes.map(axis => ({
                label: peripheralLabelOrName(axis) ?? `${deviceLabelOrName(device)} axis ${axis.axisNumber}`,
                value: axis.axisNumber,
              }))}
              onValueChange={axisNumber => changeAxisSelection(bufferAxisIndex, axisNumber)}
              portalToBody/>
          </React.Fragment>)}
        </div>
      </>}
    </div>
  </Modal>;
};
