import React, { useEffect, useState } from 'react';
import { Icons, ContextMenu } from '@zaber/react-library';
import { useSelector } from 'react-redux';
import { clipboard } from 'electron';
import classNames from 'classnames';

import { ConnectionsView } from '../connection_manager';
import { NoContentMessage, StickyScrollDiv, Title, EntityHierarchyLabel } from '../components';
import { useActions } from '../utils';
import { ConnectionState } from '../connection_manager/reducer';

import { actions as actionDefinitions } from './actions';
import {
  selectConnection, selectKeyHistory, selectConnectionState, selectSelectedKey, selectShowExternal
} from './selectors';
import { TerminalHistoryItem } from './TerminalHistoryItem';
import { TerminalOneTimeMessage } from './TerminalOneTimeMessage';
import { TerminalConnectionMessage } from './TerminalConnectionMessage';
import { CommandBatchModal } from './CommandBatchModal';
import { CommandInput } from './CommandInput';
import { AsciiProtocolReferenceLink } from './AsciiProtocolReferenceLink';


export const Terminal: React.FC = () => {
  const actions = useActions(actionDefinitions);

  const selectedKey = useSelector(selectSelectedKey);
  const connection = useSelector(selectConnection);
  const connectionState = useSelector(selectConnectionState);
  const history = useSelector(selectKeyHistory);

  const [showChecksums, setShowChecksums] = useState(false);

  useEffect(() => {
    actions.monitorConnection();
    return () => {
      actions.willUnmount();
    };
  }, []);

  return <div className="connection-view-and-app">
    <Title>Terminal</Title>
    <ConnectionsView
      selectable={['axis', 'device', 'connection']}
      selected={selectedKey}
      onSelect={key => actions.selectEntity(key)}
    />
    <div className="terminal app-ui">
      {!connection && <NoContentMessage title="Welcome to Terminal!" message="Select a connection to begin."/>}
      {connection && <>
        <div className="settings">
          <EntityHierarchyLabel className="terminal-filter" entityKey={selectedKey}/>
          <div className="right-side">
            <AsciiProtocolReferenceLink/>
            <TerminalMenu connection={connection} showChecksums={showChecksums} onSetShowChecksums={setShowChecksums}/>
          </div>
        </div>
        <StickyScrollDiv
          key={`scroll-${selectedKey}`}
          className={classNames('history', { 'hide-checksums': !showChecksums })}
        >{({ scrollToBottom }) =>
            history.map((historyItem, i) => (
              <TerminalHistoryItem
                key={historyItem.key}
                historyItem={historyItem}
                onLayoutUpdate={i === history.length - 1 ? scrollToBottom : undefined}
              />
            ))
          }</StickyScrollDiv>
        <CommandInput key={`input-${selectedKey}`} selectedKey={selectedKey}/>
        <TerminalOneTimeMessage/>
        <TerminalConnectionMessage connectionState={connectionState} reconnect={actions.monitorConnection}/>
      </>}
      <CommandBatchModal/>
    </div>
  </div>;
};

interface TerminalMenuProps {
  connection: ConnectionState;
  showChecksums: boolean;
  onSetShowChecksums: (showChecksums: boolean) => void;
}

const TerminalMenu: React.FC<TerminalMenuProps> = ({ connection, showChecksums, onSetShowChecksums }) => {
  const actions = useActions(actionDefinitions);

  const showExternal = useSelector(selectShowExternal);
  const history = useSelector(selectKeyHistory);

  return <ContextMenu title="Terminal Actions">
    <ContextMenu.Item onClick={() =>
      clipboard.writeText(history
        .map(item => item.text)
        .map(text => showChecksums ? text : text.replace(/:\w\w$/, ''))
        .join('\n'))
    }
    title="Copy Command History to Clipboard" icon={<Icons.Copy/>}>
      Copy All History
    </ContextMenu.Item>
    <ContextMenu.Item onClick={() => actions.clearConnectionHistory(connection.key)}
      title="Clear History" icon={<Icons.Trash/>}>
      Clear All History
    </ContextMenu.Item>
    <ContextMenu.Item onClick={() => actions.commandBatchEdit()}
      title="Command Batch" icon={<Icons.Notes/>}>
      Command Batch
    </ContextMenu.Item>
    <ContextMenu.CheckboxItem checked={showExternal} onChecked={actions.setShowExternal}
      title="Show Messages Originating from Outside This Window">
      Show External Messages
    </ContextMenu.CheckboxItem>
    <ContextMenu.CheckboxItem checked={showChecksums} onChecked={onSetShowChecksums}
      title="Show Checksums Values in Messages">
      Show Checksums
    </ContextMenu.CheckboxItem>
  </ContextMenu>;
};
