import classNames from 'classnames';
import React from 'react';

import type { Path } from '../types';

import { XAXIS, YAXIS } from './constants';


export interface Props {
  userPath: Path;
  drawingAxes: number[];
  selectedPointIndex: number | null;
  scale: number;
  onSelected: (i: number) => void;
  onHover: (i: number | null) => void;
}


// Draws the control points on the curve view as dots.
export const ControlPoints: React.FC<Props> = ({ userPath, drawingAxes, selectedPointIndex, scale, onSelected, onHover }) => {
  const xs: number[] = [];
  const ys: number[] = [];
  let lastX = 0;
  let lastY = 0;

  for (let i = 0; i < userPath.series[0].points.length; i++) {
    const xPoint = userPath.series[drawingAxes[XAXIS]].points[i];
    const yPoint = userPath.series[drawingAxes[YAXIS]].points[i];
    const x = xPoint.relative ? xPoint.pos + lastX : xPoint.pos;
    const y = yPoint.relative ? yPoint.pos + lastY : yPoint.pos;
    xs.push(x);
    ys.push(y);
    lastX = x;
    lastY = y;
  }

  return <>
    {xs.map((x, i) => {
      const y = ys[i];
      return <circle key={i}
        cx={x} cy={y}
        className={classNames('control-point', { selected: i === selectedPointIndex })}
        role="control-point"
        r={(i === selectedPointIndex ? 4 : 2.5) / scale}
        strokeWidth={3 / scale}
        onMouseOver={() => onHover(i)}
        onMouseLeave={() => onHover(null)}
        onMouseDown={() => onSelected(i)}/>;
    })}
  </>;
};
