export type FeedbackModalOpenState = 'gather' | 'sending' | 'sent' | 'error';
export type FeedbackModalState = 'closed' | FeedbackModalOpenState;
