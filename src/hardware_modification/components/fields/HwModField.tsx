import { Icons, NoticeBorder, NoticeMessage, Slider, Text, Toggle } from '@zaber/react-library';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { match } from 'ts-pattern';

import { ProtocolManual, ProtocolManualFormatter, ToggleIfProtocolManual } from '../../../protocol_manual';
import { selectChangedFields, selectChosenProductKey, selectUnsetFields } from '../../selectors';
import type { EnumProtocolDataOption, SectionValues } from '../../types';
import type { FieldIds, SectionIds } from '../../data_definitions';
import { fieldEnabled } from '../../utils';

import { HwModEnumInput } from './EnumeratedField';
import { HwModNumericInput } from './NumericField';

export const UNDEFINED_NAME = 'Undefined Name for this Setting';

export type Docs = {
  type: 'setting';
  setting: string;
} | {
  type: 'custom';
  html: string;
};

export type Input = {
  type: 'numeric';
} | {
  type: 'enum';
  options:  EnumProtocolDataOption[];
} | {
  type: 'slider';
  min: number;
  max: number;
  step: number;
} | {
  type: 'toggle';
};

export interface HwModInputProps {
  value: number | null;
  status: 'none' | 'unset' | 'invalid' | 'changed';
  disabled: boolean;
  testid: string;
  onUpdate: (value: number | null | 'revert') => void;
}

export interface HwModFieldProps<S extends SectionIds> {
  sectionId: S;
  fieldId: FieldIds<S>;
  values: SectionValues<S>;
  locked?: boolean;
  name: string;
  settingName?: string;
  description: string;
  onUpdate: (value: number | null | 'revert') => void;
  docs?: Docs;
  input: Input;
}

export const HwModField = <S extends SectionIds>(props: React.PropsWithChildren<HwModFieldProps<S>>) => {
  const {
    sectionId,
    fieldId,
    values,
    locked,
    name,
    settingName,
    description,
    onUpdate,
    docs,
    input,
  } = props;
  const productKey = useSelector(selectChosenProductKey);
  const [showManual, setShowManual] = useState(false);
  const unset = !!useSelector(selectUnsetFields)[sectionId]?.[fieldId];
  const changed = !!useSelector(selectChangedFields)[sectionId]?.[fieldId];

  const field = values[fieldId];
  if (field == null) {
    return null;
  }
  const { is, updateApplied, error } = field;

  const testid = `${sectionId}-${fieldId}`;
  const enabled = fieldEnabled(sectionId, fieldId, values);
  const disabled = !!locked || !enabled;

  const inputProps: HwModInputProps = {
    value: enabled ? is : null,
    status:
      error != null ? 'invalid' :
      !enabled ? 'none' :
      unset ? 'unset' :
      !changed ? 'none' :
      is == null ? 'invalid' :
      'changed',
    disabled,
    testid,
    onUpdate,
  };

  return <NoticeBorder type={error != null ? 'error' : null}>
    <div className="boxed-field">
      <div className="top-line">
        <div className="title">
          <div>
            <Text t={Text.Type.H5} e={disabled ? Text.Emphasis.Light : undefined}>{name}</Text>
            {settingName && <Text t={Text.Type.BodyXSm} e={Text.Emphasis.Light} f={Text.Family.Mono}>{` (${settingName})`}</Text>}
          </div>
          <Text t={Text.Type.BodyXSm} e={Text.Emphasis.Light}>{description}</Text>
        </div>
        {updateApplied && <Icons.Tick className="tick"/>}
        <div className="undo-container">
          {!disabled && changed &&
            <Icons.Restore
              title="Undo"
              className="revert" data-testid={`${testid}-undo`}
              onClick={() => onUpdate('revert')}
            />}
        </div>
        {match(input)
          .with({ type: 'numeric' }, () => <HwModNumericInput {...inputProps}/>)
          .with({ type: 'enum' }, ({ options }) => <HwModEnumInput {...{ ...inputProps, options }}/>)
          .with({ type: 'slider' }, ({ min, max, step }) => (
            <Slider
              value={is ?? 0}
              min={min} max={max} step={step} showTicks={true}
              onValueChange={onUpdate}
              data-test
              data-testid={`${testid}-slider`}
              disabled={disabled}
            />
          ))
          .with({ type: 'toggle' }, () => (
            <Toggle
              value={Boolean(is ?? 0)}
              onValueChange={newValue => onUpdate(+newValue)}
              data-test
              data-testid={`${testid}-toggle`}
              disabled={disabled}/>
          ))
          .exhaustive()
        }
        <div className="toggle">
          {docs && match(docs)
            .with({ type: 'setting' }, ({ setting }) => (
              <ToggleIfProtocolManual
                deviceOrAxisKey={productKey} setting={setting}
                activated={showManual} onClick={() => setShowManual(!showManual)}
                title={`Show ${name} Documentation`}
              />
            ))
            .with({ type: 'custom' }, () => (
              <Icons.QuestionMark title={`Show ${name} Documentation`} activated={showManual} onClick={() => setShowManual(!showManual)}/>
            )).exhaustive()
          }
        </div>
      </div>
      {showManual && docs && match(docs)
        .with({ type: 'setting' }, ({ setting }) => (
          <ProtocolManual deviceOrAxisKey={productKey} setting={setting} hideWhenNoDocs={true}/>
        ))
        .with({ type: 'custom' }, ({ html }) => <ProtocolManualFormatter html={html}/>).exhaustive()
      }
      {error != null &&
        <NoticeMessage id={`HwModError-${error.sectionId}-${error.fieldId}`}>
          {error.message}
        </NoticeMessage>
      }
    </div>
  </NoticeBorder>;
};
