import _ from 'lodash';

import { FirmwareVersion } from '../types';
import { environment, Flavors } from '../../environment';
import doc from '../ascii-7.html';


/**
 * All possible attributes that might be profiled on.
 * Note that "data-documents-setting" and "data-documents-command" are not
 * used by ZL, only zaber console, and therefore are excluded.
 */
const PROFILED_ATTRS = [
  'data-capability',
  // "data-product",
  // "data-setting",
  'data-min-fw',
  'data-max-fw',
] as const;
type ProfiledAttr = typeof PROFILED_ATTRS extends readonly(infer T)[] ? T : never;
export type ProfileMap = Record<ProfiledAttr, string[]>;

export const parsedManual = _.once(() => {
  const parser = new DOMParser();
  const parsedDoc = parser.parseFromString(doc, 'text/html');
  return parsedDoc.body;
});

function isProfileAttr(attr: string): attr is ProfiledAttr {
  return PROFILED_ATTRS.includes(attr as ProfiledAttr);
}

function shouldShowElement(element: HTMLElement, includes: ProfileMap): boolean {
  for (const elAttr of element.attributes) {
    const name = elAttr.name;
    if (isProfileAttr(name) && !includes[name].includes(elAttr.value)) {
      return false;
    }
  }
  return true;
}

/**
 * Creates a css selector like "[data-capability], [data-product]"
 * that finds all of the elements that can be profiled
 */
const canProfileSelector = PROFILED_ATTRS.map(attr => `[${attr}]`).join(', ');

/** Goes through and hides all fields of an element that are not on the includes */
export function profileElement(element: HTMLElement, includes: ProfileMap) {
  const elementsToProfile: NodeListOf<HTMLElement> = element.querySelectorAll(canProfileSelector);
  for (const el of elementsToProfile) {
    if (!shouldShowElement(el, includes)) {
      el.remove();
    }
  }

  for (const el of element.querySelectorAll('[data-product="general"]')) {
    el.remove();
  }
}

/** Finds all links in the provided element and prefixes then to point to our external site */
export function injectLinks(element: HTMLElement, baseUrl: string) {
  if (environment.flavor === Flavors.Zaber) {
    const elementsWithDocLinks: NodeListOf<HTMLElement> = element.querySelectorAll('[href*="#"]');
    for (const el of elementsWithDocLinks) {
      const currentHref = el.getAttribute('href');
      el.setAttribute('href', `${baseUrl}${currentHref}`);
    }
  } else {
    const elementsWithDocLinks: NodeListOf<HTMLElement> = element.querySelectorAll('[href]');
    for (const el of elementsWithDocLinks) {
      const span = element.ownerDocument.createElement('span');
      span.innerHTML = el.innerHTML;
      el.parentNode!.insertBefore(span, el);
      el.remove();
    }
  }
}

export function pointImgToSource(element: HTMLElement) {
  const imgElements: NodeListOf<HTMLElement> = element.querySelectorAll('img');
  for (const el of imgElements) {
    const relativeSource = el.getAttribute('src');
    if (relativeSource) {
      el.setAttribute('src', `protocol_manual/${relativeSource}`);
    }
  }
}

export function stripNestedArticles(element: HTMLElement) {
  for (const el of element.querySelectorAll('article')) {
    el.remove();
  }
}

export function stripZaber(element: HTMLElement) {
  if (environment.flavor === Flavors.Zaber) {
    return;
  }
  element.innerHTML = element.innerHTML.replace('Zaber', '');
}

export function createIncludes(capabilities: string[], firmwareVersion: FirmwareVersion): ProfileMap {
  const minFw: string[] = [];
  const maxFw: string[] = [];
  for (let i = 0; i < 100; i++) {
    const s = `7.${i.toString().padStart(2, '0')}`;
    if (i <= firmwareVersion.minor) { minFw.push(s) }
    if (i >= firmwareVersion.minor) { maxFw.push(s) }
  }
  return {
    'data-capability': capabilities,
    // 'data-product': [],
    // 'data-setting': [],
    'data-min-fw': minFw,
    'data-max-fw': maxFw,
  };
}

export function createBaseUrl(firmwareVersion: FirmwareVersion, deviceName: string, peripheralName?: string): string {
  const params = new URLSearchParams();
  params.append('device', deviceName);
  params.append('version', `${firmwareVersion.major}.${firmwareVersion.minor}`);
  params.append('protocol', 'ASCII');
  if (peripheralName != null) {
    params.append('peripheral',  peripheralName);
  }
  return `https://www.zaber.com/protocol-manual?${params.toString()}`;
}
