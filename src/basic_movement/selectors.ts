import { createSelector } from 'reselect';
import { notNil } from '@zaber/toolbox';
import _ from 'lodash';

import { selectBasicMovement } from '../store';
import {
  selectIdentifiedAxes as selectManagerAxes,
  selectIdentifiedDevices as selectManagerDevices,
  ConnectionManagerIdentifiedAxisState,
  selectAxesKeysInDisplayOrder,
  ConnectionManagerIdentifiedDeviceState,
  ConnectionManagerConnectionState,
  selectConnections as selectManagerConnections,
  LockstepGroup,
  IdentifiedAxisState,
} from '../connection_manager';
import { EntityKey, extractConnectionKey, extractDeviceKey, isSubKeyOf, makeAxisKey } from '../keys';

import type { AxisState } from './reducer';

export const selectSelectedKey = createSelector(selectBasicMovement, state => state.selectedKey);

/**
 * A lockstep group or a single axis
 */
export type Movable = AxisStateComplete | AxisStateComplete[];

export interface AxisStateComplete extends AxisState, ConnectionManagerIdentifiedAxisState {
  device: ConnectionManagerIdentifiedDeviceState;
  connection: ConnectionManagerConnectionState;
  lockstep: LockstepGroupComplete | null;
}

interface LockstepGroupComplete extends LockstepGroup {
  isPrimary: boolean;
  axisKeys: EntityKey[];
}

const getLockstepGroupComplete = (
  axis: ConnectionManagerIdentifiedAxisState,
  device: ConnectionManagerIdentifiedDeviceState,
): LockstepGroupComplete | null  => {
  const lockstepGroup = device.locksteps?.find(lockstepGroup => lockstepGroup.axisNumbers.includes(axis.axisNumber));
  if (!lockstepGroup) {
    return null;
  }
  const axisKeys = lockstepGroup.axisNumbers.map(axis => makeAxisKey(device.key, axis));
  const isPrimary = axis.key === axisKeys[0];
  return { ...lockstepGroup, axisKeys, isPrimary };
};

export const selectAxes = createSelector(selectBasicMovement, selectManagerAxes, selectManagerDevices, selectManagerConnections,
  (state, managerAxes, devices, connections) => {
    const identifiedAxes = _.pickBy(managerAxes, axis => state.axes[axis.key]);
    const identifiedAxesComplete = _.mapValues(identifiedAxes, (axis: IdentifiedAxisState): AxisStateComplete => {
      const device = devices[extractDeviceKey(axis.key)];
      const connection = connections[extractConnectionKey(axis.key)];
      const lockstep = getLockstepGroupComplete(axis, device);
      const axisState = state.axes[axis.key];
      return {
        ...axis,
        ...axisState,
        device,
        connection,
        lockstep
      };
    });
    return identifiedAxesComplete;
  }
);

export const selectAxesInOrder = createSelector(selectAxes, selectAxesKeysInDisplayOrder,
  (axes, orderedAxesKeys): AxisStateComplete[] => orderedAxesKeys.map(axisKey => axes[axisKey]).filter(notNil)
);

export const selectVisibleAxes = createSelector(selectSelectedKey, selectAxesInOrder,
  (selectedKey, axes) => _.filter(axes,
    axis => (axis.lockstep?.isPrimary ?? true)
      && (
        isSubKeyOf(axis.key, selectedKey)
        || axis.isPinned
        || axis.lockstep?.axisKeys.some(key => isSubKeyOf(key, selectedKey)) === true))
);

export const selectVisibleUnpinnedAxes = createSelector(selectVisibleAxes, axes => _.filter(axes, axis => !axis.isPinned));
export const selectPinnedAxes = createSelector(selectVisibleAxes, axes => axes.filter(axis => axis.isPinned));

export const selectLastInitiatorAll = createSelector(selectVisibleAxes, axes =>
  axes.find(axis => axis.lastInitiator?.includes('all'))?.lastInitiator);

function createMovables(axes: AxisStateComplete[], allAxes: ReturnType<typeof selectAxes>): Movable[] {
  return axes.map(axis => axis.lockstep ? axis.lockstep.axisKeys.map(key => allAxes[key]) : axis);
}

export const selectVisibleUnpinnedMovables = createSelector(selectVisibleUnpinnedAxes, selectAxes, createMovables);
export const selectPinnedMovables = createSelector(selectPinnedAxes, selectAxes, createMovables);

/**
 * If movable is a lockstep group, return primary axis
 * Otherwise if movable is an axis, return it
 */
export const getAxisFromMovable = (movable: Movable) => _.isArray(movable) ? movable[0] : movable;
