[:arrow_left: Index](README.md)

# Changelog

## Version 2025.2.13-1
* ZML version 7.4.1
  * Fixing G64 G-code: In cases when axes are specified out of order or not at all, the produced arc had a wrong direction.

## Version 2025.2.4-1
* Oscilloscope changes:
  * Suitable default channels are now chosen for the X-SCA.
  * Added recommended channels to the add channels dialog for the X-SCA and X-LCA.
  * Fixed default channels possibly numbering more than six on some devices.
  * Display X-SCA processes with numbers added.
  * Fixed units of measure not being identified for the X-SCA.
* Includes the encoder power up delay setting in Advanced Hardware Setup
* Allows G-Code to pause and unpause execution
* Makes Microscope App the first app in the app bar when a microscope is connected

## Version 2025.1.28-2
* Selectively block devices from save/load in 7.41 while allowing all other devices to save/load and update normally
* Fix UI bugs in Device Settings, Microscope, G-Code and Oscilloscope Apps
* Fix icon size bugs
* X-ADR travel limit detection
* Unify entity hierarchy labels
* Update the UI for adding TCP/IP connections
* Add restore-from-device feature to Joystick App
* Don't disable chains when a single device has an FF error
* Shorten timeouts for local TCP/IP connections by default

## Version 2024.12.24-1
* Temporarily blocks the persist state feature of upgrades until the bug around current controller 7 upgrades is handled
* Joystick UX improvements

## Version 2024.12.16-1
* Warns when defaulted parameters of Servo Tuning were changed in the Simple Tuning mode
* Various improvements and bug fixes in Microscope app
* Advanced Hardware Setup now supported in Zaber Launcher Offline edition (Zaber devices only)
* Adds option to stop sinusoidal moves at the end of the cycle in Basic Controls
* Various UI improvements in X-Joy app
* Various UI improvements in Oscilloscope app
* Fixes a bug that re-connects serial ports immediately after disconnection
* Fixed a bug that causes timeouts for TCP connections to be set by local timeout preference
* Various UI improvements in LC40 app
* My Connections adds support for detailed error messages for devices that experience Critical System Error (FF)

## Version 2024.11.28-1
* Fixing couple of visual issues in Servo Tuner
* Fixing a bug with values not being saved in Servo Tuner
* Fixing a bug with incorrect acceleration applied in Advanced Hardware Setup custom unit conversions
* Support for Windows arm64
* Filtering options in Advanced Hardware Setup based on device capabilities

## Version 2024.11.14-1
* Fixed the recording time estimate in the Oscilloscope not reflecting user-entered sample
  quantity or device recording limits.
* Changed the PVT 2D path view to use a higher resolution approximation of the curve when
  control points are not being moved with the mouse.
* Added support for switching between local and remote modes on future EtherCAT devices
  and handling some related errors, in the Connection Manager and the Basic Controls application.
* Added lockstep handling to the LC40 Setup application.
* Device communication timeouts are now configurable in the Preferences section.
* Linux installation instructions have been updated to reflect changes to AppImage processing in
  recent versions of Ubuntu.
* Miscellaneous UI style and edge case crash fixes.

## Version 2024.10.2-2
* Fixing update check process

## Version 2024.10.2-1
* The PVT app now supports limited interactive editing of curves.
  * The point properites panel on the right side of the window allows editing point times,
    positions and velocities, inserting a new point after the selected one, and deleting
    the selected point.
  * The table view also nas new controls in the rightmost column for inserting and deleting
    points.
  * The 2D curve view allows editing control point positions and velocitied by dragging handles
    with the mouse, and allows inserting new points at arbitrary positions along the existing curve.
    There is a new help button that lists the mouse controls.
  * The app has undo/redo functions via toolbar buttons and CTRL-Z/Y keypresses.
  * There is an indication of unsaved changes and new File save/save as menu functions.
  * Auto-reload will raise a prompt dialog if you make changes in the app and also in your
    existing CSV file using an external program.
* Application respects XDG_CACHE_HOME environment variable on Linux
* Fixing bug where state does not restore after firmware upgrade of X-MCC
* New installer for automated deployment on Windows
* Fixed crash of Terminal after connection removal
* Sinusoidal movement support in Basic Control
* Support for WDI Autofocus and FW 7.40
* Allow negative number unit conversion in Terminal
* Fixing visuals of G-Code app
* Fixed FW support in Trigger app
* Improved performance of Device Settings
* Improved visuals all around

## Version 2024.9.10-1
* Fixed "Show Checksum" button in Terminal app
* Fixing a crash in the Terminal app when the user pressed arrow up with no history
* Fixing a crash in the Terminal app when user clicked on a error help and then entered a command
* Fixing a wrong command send when picked from history in Terminal app
* Fixing an unlikely crash in Basic Control app
* Fixing visualizations of non-cyclic rotary devices in Basic Control app
* Making "Next" and "Previous" arrows respect indexed devices in Basic Control app
* New vay of editing values in Device Settings app
* Visual improvements in the Basic Controls app
* Fixing blinking "Close Port" in the Connection Manager
* Fixing of "Update on next startup" button

## Version 2024.8.22-1
* Minor UI updates in the LC40 Setup App

## Version 2024.8.20-1
* Fixed a bug in the Triggers app that could cause the trigger toggle to get stuck in loading state under certain conditions
* Oscilloscope app:
  * Added an option to specify the number of samples to record in Buffered Manual mode.
  * Added a fourth operation mode called Download that just reads and displays whatever
    data is currently in the device's oscilloscope buffer.
    * Limitation: If the device is currently recording, this function will wait for
      it to complete but there is no way to cancel the wait. This will be addressed in future.
  * Exposed a previously internal-only Recording Delay control in buffered modes that
    sets or shows the value of the `scope.delay` setting, which is a device-mediated
    delay between the start of recording and the first data point. This is different
    from the existing Timer control which is a software-mediated delay between pressing
    the Start button and sending the `scope start` command to the device.
* Various UI fixes and improvements

## Version 2024.08.15-1
* Autofocus support for microscope
* Basic Control visual improvements
* Fixing performance issues in connection view

## Version 2024.08.12-1
* Introduction of communication log (on by default)
* Stability and visual improvements for Basic Control
* Unit conversions in terminal
* Stability fixes for firmware upgrade
* New oscilloscope math channels
* Visual improvements for errors all around
* Minor visual improvements all around

## Version 2024.7.17-1
* Improved visuals and error handling in Device Settings
* Basic control reloads movement limits/speed more often
* Basic control does not crash after connection removal
* Firmware upgrade from files in offline version
* Fixing App titles across

## Version 2024.7.11-1
* Version schema changed to date-based
* Various UI Updates, Improvements, and Bugfixes
* Triggers App Changes:
  * Adds more descriptive message when loading an unsupported trigger from a device
  * Adds custom labels for triggers

## 2024-07-08, Version 1.11.0
* New Triggers App for controlling device triggers

## 2024-06-25, Version 1.10.7
* Various UI Updates, Improvements, and Bugfixes
* Added time estimate for firmware update progress
* Fixed a bug in microscope app that may causing homing to fail with certain firmware versions
* PVT Viewer changes:
  * The time series view now has different icons for limit crossing warnings for cases where the
    device will still accept the point - for example, exceeding the acceleration setting.
  * The time series view now draws two horizontal dotted lines for `motion.accelonly` and
    `motion.decelonly` if they are different, because the previous visualization gave the false
    impression that each only applied in one motion direction.
  * Polling behavior has been cleaned up so that it interferes less with tooltip display.
* LC40 app changes:
  * Added support for LC40C devices
  * Fixed a bug for devices with brakes that prevented finding the home sensor in some circumstances
* Basic Controls app changes:
  * Added keyboard shortcuts
  * Added custom IO Labels
  * App elevated to top of list in All Applications
* Deprecated apps (will be removed in future version):
  * Basic Movement
  * Device IO
* Triggers App [Internal Only] changes:
  * Axis can now be selected before Settings
  * Unsupported triggers can be loaded from device to app
  * More descriptive error message when trying to save an invalid trigger
  * Axes now appear in connection viewer pane


## 2024-05-30, Version 1.10.6
* Fixed an issue with local network discovery of shared Zaber Launcher instances on MacOS and Linux
* Fixed an issue with Advanced Hardware Setup incorrectly attempting to write to read-only setting `limit.c.pos`
* The Oscilloscope CSV export feature now respects the "Native Units" toggle setting and can write out
  device settings in native device units.
  * Note that math channels calculated by the Oscilloscope app, such as velocity and acceleration, will
    be in native position units per second rather than native velocity or acceleration units.

## 2024-05-16, Version 1.10.5
* Fixed an Oscilloscope crash when changing the differencing window in a recording that has only I/O channels.
* Fixed an Issue in Basic Controls when trying to load unused peripherals
* Triggers in internal

## 2024-05-10, Version 1.10.4

* Basic Control
  * Pinning of devices
  * Wider support for devices
* Microscope:
  * Small visual fixes
  * Illuminator channel intensity input
* Oscilloscope:
  * Fixing external trigger
* Generic:
  * Fixing "Message Router appears to not be running" error
  * Context Menu visual fixes
  * Small visual fixes in padding of app titles

## 2024-04-15, Version 1.10.3

* Launcher is now released as a Universal Binary on MacOS, and runs natively on both Intel and Apple Silicon.
* Made improvements to error messages when connecting to a Virtual Device that is still starting.
* Connection, device, and axis selection are now synchronized between different Zaber Launcher apps.
* Fixed minor UI issues in Servo Tuner app

## 2024-03-28, Version 1.10.2

* Oscilloscope changes:
  * Fixed the add setting and add I/O channel buttons sometimes being visible or enabled at the wrong times.
  * Added tooltips to controls that add settings to indicate why they're disabled when it's because the maximum
    number of device channels has been reached.
  * Device and axis labels are now displayed if set.
  * Fixed a crash when scoping I/O pins on devices that provide the system clock getting.
* PVT changes:
  * Fixed crash when loading from buffers that call invalid buffers.
  * Fixed blank axis names when loading from buffers on integrated devices.
  * Device and axis labels are now displayed if set.
* Microscope changes:
  * Stored positions feature
  * Fast movements by holding "Shift" key
  * Configurable inversion of axes
  * Fixing missing 4th illuminator channel
  * Improvements to objective switching
  * Faster refresh when coming from another app
* Basic Control fixes

## 2024-03-13, Version 1.10.1

* Oscilloscope changes:
  * Corrected estimated duration of buffered captures to account for I/O channels.
* Microscope improvements
  * Production Tools
  * Restart Microscope
  * minor bug fixes
* Servo Tuning minor bug fixes

## 2024-03-05, Version 1.10.0

* Microscope Application Release
* PVT app changes:
  * Live display of the device position during playback is now on a best-effort basis instead of at regular
    intervals, to prevent the position polling from interfering with PVT point queueing. This means that under
    some combinations of close time spacing of points, slow serial connections and slow computers, the current
    device position dots may update irregularly or not at all.
    * If you are using a USB to RS-232 converter, you may be able to improve performance by
      changing the serial port latency timer as described in this
      [https://software.zaber.com/motion-library/docs/guides/ftdi_latency](ZML Howto).
  * Performance of live playback has been improved, and the position tracking has better time accuracy on
    the time series view. That said, the best performance for high-volume PVT points is still achieved by
    writing your own Zaber Motion Library script to send the points to the device.
* Servo Tuner App simple tuning parameters now show default values
* Fixes text formatting in terminal app on Windows 11
* Device I/O App digital I/O labels are now 'on'/'off' instead of 'high'/'low' to more accurately reflect I/O hardware
* [Internal] Fixes a bug where firmware updater failed to retrieve firmware for internal devices

## 2024-02-20, Version 1.9.4

* Support for labels in Process Controller
* Driver Enable button fixes in Servo Tuner
* UX improvements in Connection Manager
* UX improvements in Firmware Update (internal devices)

## 2024-02-13, Version 1.9.3

* Fixed the Oscilloscope not correctly converting units and auto-ranging for some settings with the dimensions
  of voltage, angle and current.
* PVT app fixes:
  * The app now works with lockstep groups.
  * Fixed a case where the curve view could incorrectly show the device's current position
    as being off of the path during playback.
  * Device position polling now stops when the app is not visible and resumes when it is shown again.
  * Fixed device position polling cancelling rectangle zoom in the time series view.
* Microscope Application Preview
* Major dependency updates

## 2024-01-16, version 1.9.2

* Fixes issue where typing in a terminal command with a double-enum ending could cause a crash
  (eg. `lockstep 1 g` could become `lockstep 1 get twist` or `lockstep 1 get b twist`)
* Provides a "[F11] to exit full screen" message when user enters full screen mode.
* Sync starred apps between multiple open windows.
* New PVT icons

## 2024-01-04, version 1.9.1

* The PVT preview app can now read back the contents of a PVT buffer stored on the device and display the path, including
  called buffers. Any relative positions at the start of the buffer will be shown relative to the current device posision.
* Fixed the Oscilloscope polled mode sometimes not displaying data in polled mode when no settings from the first axis
  of a multi-axis device were selected.
* Minor usability and cosmetic improvements to the Terminal app and the connection manager.
* Feedback form now fills in contact email for internal users and users logged into cloud services from Zaber Launcher.
* [Internal] Usability fix to the product selection field, and some movement support for the OMG.

## 2023-12-13, Version 1.9.0

* Process Controller PID Tuning Assistant release
* Fix bug that caused terminal to crash when user typed non-existent device or axis number in command

## 2023-12-01, version 1.8.2

* Changes to Oscilloscope polled recording mode:
  * It will now use synchronized setting reads on devices that support it (FW 7.35 and up).
  * When using synchronized reads and if there is space in the read commands, it will use the device's
    uptime setting instead of the computer's clock for the sample time. This yields more accurate sample times.
  * Note: Neither of the above applies to I/O channels; they remain unsynchronized and timed using the computer's clock.
* PVT viewer changes:
  * Made the CSV file dialog always retain units specified in the file, and added clarifiying help text.
  * Added a button next to the open file button to re-show the welcome screen file format information.
  * Angular units and rotary devices are now supported, though there is still the restriction that all series must use the same units, meaning mixed rotary and linear paths in one file are not supported.
  * Fixed a bug in the axis mapping dialog that caused incorrect axes to be assigned if some axes of a multi-axis controller were inactive.
  * Fixed a bug that would prevent the execute button from enabling on controllers with inactive axes.
* Terminal changes:
  * Added error detail box that pops up when users click on error flags in messages
  * Added links to the relevant section of the ASCII Protocol Manual next to each command suggestion
  * Changed the behavior of the terminal to only display messages to/from the selected device or axis when a single device or axis is selected
* Renamed Settings app to Preferences app
* [Internal] Added Microscope app

## 2023-10-27, Version 1.8.1

* Several bug fixes in the PVT app.
* Fix Demo App Crash
* Improvements to unit select
  * Won't needlessly truncate values with no backing native units
  * Won't mark unpowered device units as unsupported
* Improvements of the Process Controller App:
  * Warning flag descriptions
  * Clears errors on reloads
  * Better workflow for autotune
  * Add command reference to terminal
* Support 3rd Party Peripherals with motor encoders
* Display more precision for rotary devices in Basic Movement

## 2023-10-10, Version 1.8.0

* Public launch of the Process Controller App
  * This app provides a dashboard for our new X-SCA product allowing users to see the device state and edit its settings.
* PVT app improvements:
  * It now displays the current device position on the timeline view during playback.
    * Note: The time at which the device position is displayed is only accurate to within 50ms.
  * Control points have tooltips showing the position and velocity on the timeline and path views.
  * There is now a rectangle zoom function on the path view (left-mouse-drag, same as on the time series view).
  * When the device travel limits are known, the out-of-bounds region on the path view is colored grey.
  * Device travel limit lines in the path view are now color coded to match the limit lines in the time series view.
* The first 20 Oscilloscope trace colors now come from a hand-picked palette instead of being generated,
  to avoid cases where there was not enough contrast between them and against the background.

## 2023-09-25, Version 1.7.0

* Public launch of the PVT viewer app.
  * This app helps to visualize the path that will be followed by a device running a PVT stream
    and to diagnose problems such as places where the sequence exceeds device limits.
  * The app does not currently support editing the PVT data; it reads from a .CSV spreadsheet
    and will automatically reload the data when the file changes.

## 2023-09-22, Version 1.6.31

* [Internal] Various improvements to the PVT app.
* [Internal] Various improvements to the Process Controller app.

## 2023-09-18, Version 1.6.30

* [Internal] Various cosmetic improvements to the PVT app.
* [Internal] Get the Process Controller app working with the newest FW build for X-SCA4

## 2023-08-30, Version 1.6.29

* Fixing click interaction for input with units
* Fixing login to cloud crashes app
* [Internal] PVT bounds checking
* [Internal] Process Controller Auto Tune feature
* [Internal] Calibration application

## 2023-08-23, Version 1.6.28

* [Internal] Improved error handing in PVT
* Allowing Firmware Update from file when offline

## 2023-08-17, Version 1.6.27

* Support for user-assigned labels of devices and peripherals
* Basic movement remembers expansion of Precise Movement
* Fixes to encoder handling in Advanced Hardware Setup
* Easier way to open new windows
* [Internal] PVT can deal with disabled and immobile axes
* [Internal] Various little improvements to PVT

## 2023-08-02, Version 1.6.26

* Oscilloscope externally triggered mode now displays the sample rate and start delay.
* Central difference filtering of oscilloscope externally triggered capture data now works.
* Made default trace colors in the oscilloscope more distinct.
* Improved the formatting of the message displayed for un-activated peripherals.
* Fixed handling of angular units in the Basic Movement app.
* [Internal] The Process Controller app now displays temperature units.
* [Internal] PVT app first cut available for testing.

## 2023-07-12, Version 1.6.25

* Fixing an issue with the inputs in Servo Tuner and Precise Motion Control
* Let axes use their device's unit conversion information
* Make it easier to manage the power on preset of servo tuner
* Show the correct precision for unit-less settings

## 2023-06-17, Version 1.6.24

* Fixing oscilloscope buffered recording.

## 2023-06-14, Version 1.6.23

* Fixing application crashing on Ubuntu 18
* The Oscilloscope now supports recording the I/O pins on devices that have them.
  * Firmware 7.33 or later is required.
  * Any number of digital inputs only count as one channel and any number of
    digital outputs only count as one channel towards the device limit of six.
    This does not apply in externally triggered mode.
* X-JOY better support for lockstep and keys
* Filtering unlikely serial ports on Mac OS
* Fixing crash of Advance Hardware Setup on unsupported device

## 2023-05-18, Version 1.6.22

* Support for firmware version 7.33
* Fixing error handling for save/restore state

## 2023-05-18, Version 1.6.21

* Changed the sign of the Oscilloscope calculated error channels so that positive error
  indicates overshoot instead of lag.

## 2023-05-16, Version 1.6.20
* Fixing Advanced Hardware Setup knob.maxspeed setting

## 2023-05-15, Version 1.6.19

* Fixed an Oscilloscope crash when displaying a chart tooltip that references missing data.
* Fixed a derivative calculation bug that was causing no Oscilloscope data to be produced for
  some differencing window sizes. This change may result in additional data points
  near the start of derivative channels.
* Fixed the Oscilloscope's follow error calculation being accidentally scaled by 1.6384.
* Fixing driver disable in Basic Movement
* Fixing Advanced Hardware Setup encoders

## 2023-05-05, Version 1.6.18

* Fixing startup issue.

## 2023-05-03, Version 1.6.17

* The connections list is now sorted in natural order rather than alphanumerically. This means, for example,
  COM10 will come after COM4 rather than before.
* Added an externally triggered mode to the Oscilloscope, which can be used to automatically plot data
  recorded by an external Zaber Motion Library script or program.
* Fixed an Oscilloscope bug where repeating or overwriting a recording would use the settings from
  the New Recording tab instead of the same settings as the original recording.
* Fixed the Oscilloscope displaying the wrong capture settings while repeating a recording.
* Changed the Oscilloscope CSV output format:
  * To eliminate blank cells, each data series now has its own time column.
  * Column names now identify calculated data.
* The precise movement section of the Basic Movement app now remembers your selections for each device.
* The precise movement section now has help tooltips explaining each of the movement modes.
* The default name of renamed connections appears in the tooltip of the rename button, and there is an option to reset
  to the default name when editing it.
* Dialog boxes that have a close button in the title bar can now be closed by pressing the ESC key.
* The dialog box for adding Virtual Device connections now has a link to open either the Virtual Device landing page
  or My Devices page, depending on whether or not you're signed in in Zaber Launcher.
* [Internal] Production App improvements
* [Internal] Production mode for X-JOY application

## 2023-03-21, Version 1.6.16

* [Internal] Production App improvements
* Message router upgrade

## 2023-03-13, Version 1.6.15

* Fixed lockstep mode not working with rotary devices.
* Fixed the Oscilloscope signal generator doing nothing unless "return to start" was selected.

## 2023-02-28, Version 1.6.14

* Device Settings persists unit selection
* Device Settings offers default value for applicable settings
* Firmware Update fixes
* Basic Movement icon titles improvements
* [Internal] Production App improvements

## 2023-02-08, Version 1.6.13

* [Internal] Production App Improvements

## 2023-02-06, Version 1.6.12

* Update to ZML 3.0
* [Internal] Production App Improvements

## 2023-02-06, Version 1.6.11

* Fixing application crashing on Ubuntu 20

## 2023-01-26, Version 1.6.10

* Servo Tuning documentation improvements
* Support for X-JOY ENG

## 2023-01-17, Version 1.6.9

* Fixes sliders rendering on top of overlying UI
* Allows servo tuning devices with Overshoot Compensation slider
* Fixing cloud sign-up URL
* Fixing Device Settings crash when devices change expectedly on the port
* Fixing Servo Tuning for DMQ
* Movement interrupt flag (NI) is not longer visible if caused by the user
* [Internal] Production App Improvements
* [Internal] FW7 programming tool

## 2023-01-12, Version 1.6.8

* Oscilloscope changes
  * The automatic unit selection for the Y axes has been improved; it will now avoid showing
    native units unless that is selected.
  * A unit selection error related to the ranges of derivative math channels has been fixed.
  * Error measurements (the calculated error math channels plus the `encoder.pos.error` setting)
    now default to the right-hand Y axis.
  * The central differencing window size used for calculating derivatives such as velocity and
    acceleration can now be changed before or after recording by a slider on the `Capture Settings` tab.
    Larger values produce smoother output but may hide detail. The range of window sizes depends on
    the sampling rate.
* Zaber Launcher Offline first release
* [Internal] Production App Improvements

## 2023-01-06, Version 1.6.7

* Update fix

## 2022-12-15, Version 1.6.6

* Oscilloscope changes
  * You can now rectangle select to zoom into data by dragging with the left mouse button
    (without any modifier keys). Clicking the right mouse button resets the zoom to show all data.
  * The modifier key for zoom & pan mode has changed from CTRL to Shift, for better interaction
    on Macs.
  * Added a toggle at the top right of the screen to display native instead of converted units.
    * Note that calculated derivatives do not take the Firmware time constant into account when
      native units are displayed; calculated velocity must be multiplied by 1.6384 and calculated
      acceleration by 1.6384 / 10000 to match Firmware values.
  * Added a "Clear all" button at top right to delete all recording tabs, to eliminate clutter.
  * Added an "Overwrite" button at bottom left to repeat a recording and replace the current one with
    the new one, as an option to reduce clutter.
  * The button to add math channels to an existing recording is now disabled if the device the
    data was captured from is no longer connected, because it is needed for unit conversion.
  * Fixed a crash when capturing in buffered mode from a single-axis integrated device.
  * The unit symbols for the Y axes are now briefly highlighted in orange when they change.
* [Internal] Production App Improvements

## 2022-12-07, Version 1.6.5

* Oscilloscope changes
  * Mouse interaction with the charts has improved:
    * Y axes are no longer ganged and can have separate zoom and pan positions.
    * There are now separate slider controls for the left and right Y axes.
    * CTRL + mouse wheel can now zoom the X axes and each Y axis independently by moving the mouse
      over their respective slider controls.
    * CTRL + left mouse drag over the chart pans all relevant axes together.
    * CTRL + mouse wheel over the chart zooms all relevant axes together.
    * Made the slider handles a little larger because the Y axis ones were sometimes hard to select.
* Terminal
  * Hiding message checksums
* [Internal] Production App Improvements

## 2022-12-05, Version 1.6.4

* My Connections
  * Remembers expansion of connections
* Basic Movement
  * Lockstep group warnings flags are sourced from all axes
* Oscilloscope changes
  * Device-level settings and settings from multiple axes of the same controller can now be recorded.
    * Users should now select controllers or integrated devices instead of axes.
  * The channels tab layout has changed; settings channels are now shown in the left column and math channels
    in the right column, and channels are grouped by axis or controller instead of by measurement dimension.
  * The dialogs for adding channels now have tabs to allow selection by axis or controller.
  * Default colors for channels are now assigned in the order channels are added.
  * Fixed a crash when moving the mouse over the charts while recording new data.
* [Internal] Production App Improvements

## 2022-11-28, Version 1.6.3

* [Internal] Production App Improvements

## 2022-11-23, Version 1.6.2

* Default values in Device Settings
* Precision movement in G-Code (Ctrl, Shift keys)
* Improvements to Factory Reset.
* Indicating configuration changes in Joystick app

## 2022-10-31, Version 1.6.1

* Fixing Windows icon

## 2022-10-25, Version 1.6

* Oscilloscope public release
* Servo Tuner public release
* Cycle movement for Basic Movement application
* Oscilloscope changes
  * The data tooltip now shows the value of the nearest data points of all channels, in a consistent order.
  * When the number of data points is small, dots are displayed along the chart lines to show where they are.
  * Tab headers are now renamed by clicking on the title of the selected tab; the edit, confirm and cancel
    buttons are gone. Pressing return or clicking away accepts the new name and pressing escape cancels renaming.
  * Chart mouse interaction has changed - the mouse wheel now always scrolls the chart area. To zoom and pan the
    charts, hold CTRL while using the wheel or left-dragging. CTRL also hides the data tooltip so you can see
    the lines better.
* Visual fixes for Basic Movement
* New Message Router API
* [Internal] Production App
  * Unlock button
  * Sensor diagnostics

## 2022-10-13, Version 1.5.5

* Fixes of Oscilloscope
  * The control panel now shows an estimate of how much time can be recorded.
  * Added missing tooltips for the Y axis buttons.
  * The control panel is now always expanded in the New Recording tab or when creating a new recording.
  * The list of settings no longer includes settings that Firmware will refuse to capture.
  * The data tooltip is now hidden while recording is in progress to reduce flickering.
* Fixes of Save/Load device state
* [Internal] UX Improvements of Production App

## 2022-10-05, Version 1.5.4

* Improvements to Servo Tuner
* Fixes of Firmware Update
* [Internal] Scripting for Production App
* [Internal] Fixes of Production App

## 2022-09-19, Version 1.5.3

* Improvements to Servo Tuner
* [Internal] Fixes of Production App

## 2022-09-02, Version 1.5.2

* Improvements to IoT stability
* Terminal application - coloring of messages
* Terminal application - Fixes of typing history
* Terminal application - Fixes of scrolling
* Terminal application - Fixes of suggestions
* X-Joy - Fixes of save/load from file
* Improvements to inputs across the entire ZL
* [Internal] Release for Production App
* [Internal] Release of Oscilloscope

## 2022-08-02, Version 1.5.1

* Improving documentation integration

## 2022-07-28, Version 1.5.0

* G-Code Application
* X-JOY Application
* Lockstep Application
* Driver Control in Basic Movement

## 2022-07-13, Version 1.4.2

* Fixing device pictures in Basic Movement
* Fixes a crash that could take down the app on Macs when there were multiple router instances running
* Sorts apps alphabetically on the All Apps page
* Allows setting custom unit conversions for modified devices
* Adds documentation and new validators to the Advanced Hardware Setup App
* Better handling for internal and unidentified devices
* Internal release of Lockstep Configuration
* Internal release of Joystick Configuration

## 2022-06-06, Version 1.4.1

* Fixing Advanced Hardware Setup for integrated products
* Fixing Advanced Hardware Setup application of settings
* Rendering firmware version in My Connections
* Improving lockstep setup error messages
* Improved visuals of invalid selections across applications

## 2022-05-26, Version 1.4.0

* Current Tuner application
* Visual updates to Device IO application
* Fixing images in setting manual

## 2022-05-19, Version 1.3.2

* Update UI and documentation for Advanced Hardware Setup App when setting up a Third Party Peripheral

## 2022-05-17, Version 1.3.1

* Fixing unit conversions for 3rd party peripherals
* Promoting applications in new windows
* Lockstep improvements

## 2022-05-10, Version 1.3.0

* Device IO application
* Support for Firmware version 7.27
* Factory Reset functionality

## 2022-04-21, Version 1.2.10

* Fixing crashing of applications when displayed in a new window.
* Devices get reloaded when comm.address setting is changed.

## 2022-04-18, Version 1.2.9

* Message continuation support in Terminal
* Typing history improvements in Terminal
* Improvements to precise movement in Basic Movement
* Firmware upgrade fix for firmware version 7.23
* Visual improvements for refresh buttons
* Release of cloud functionality

## 2022-04-05, Version 1.2.8

* Demo application improvements

## 2022-03-31, Version 1.2.7

* Demo application
* April special

## 2022-03-31, Version 1.2.6

* Improvements to precise movement in Basic Movement
* Improvements for tablets
* Wrapping of long names in connection side panel

## 2022-03-25, Version 1.2.5

* Improving port sharing across multiple OS users (new ZML required)
* Fixing of scaling in Basic Movement
* Improving interaction with device tree
* Improving suggestions and selection in Terminal
* Improving scroll in Terminal
* Improving interaction with `resolution` and `peripheral.id` in Device Settings
* Refreshing of firmware versions
* Faster firmware upgrade over USB on the latest controllers
* Fixing opening of apps in a new windows


## 2022-01-31, Version 1.2.4

* Fixing Save/Restore feature after a firmware upgrades for controllers with peripherals connected.
* Adding defaults and value limits to the precise movement controls.
* Fixing display glitches in basic movement

## 2022-01-31, Version 1.2.3

* Fixing Terminal crashing after connection is removed.
* Fixing save/restore issue where some settings are not restored.

## 2022-01-20, Version 1.2.2

* Fixing Mac OS update, please download Zaber Launcher again from our website.

## 2022-01-18, Version 1.2.1

* Use a default file format that works with windows for save load files
* Fixing error when app is closed during loading

## 2022-01-10, Version 1.2.0

* Advanced Hardware Setup App
* UI to save and load state of a device or axis
* Allows saving and reloading a device's state during a firmware upgrade
* Auto-complete for terminal commands

## 2021-12-17, Version 1.1.10

* Updating default TCP ports for Zaber Devices

## 2021-12-16, Version 1.1.9

* Renaming of connections
* Updating default TCP ports for Zaber Devices

## 2021-12-10, Version 1.1.8

* Fixing scrolling for dialog
* Current Tunning - UI improvements
* Small UI improvements

## 2021-11-15, Version 1.1.7

* Fixed application loads indefinitely in Asian countries
* Device Settings - Ethernet settings dialog
* Device Settings - Fixed crash when devices loaded during read
* Device Settings - Devices reload when peripheral.id changed
* Terminal - Filtering messages improvements
* Basic Movement - Travel length reloads when coming back


## 2021-10-28, Version 1.1.6

* Ethernet device support and discovery
* LC40 app fix (when over direct USB or ethernet)
* Jog button in Basic Movement
* Filtering of messages in Terminal
* Fixing issue of restarting over and over with crash
* Small UI fixes

## 2021-09-28, Version 1.1.5

* Terminal improvements
* Small UI fixes

## 2021-09-10, Version 1.1.4

* Fixing issue of setting up peripherals
* Improvements to hardware customization app

## 2021-08-26, Version 1.1.3

* Fixing issue of process persisting if application was quit before window loaded
* Improved experience of apps opening in new windows
* Fixing firmware upgrade over USB and TCP
* Adding Copy & Paste context menu
* Improved experience and functionality of Network Sharing

## 2021-08-12, Version 1.1.2

* UX Fixes
* Upgrade of firmware from file

## 2021-08-05, Version 1.1.1

* Connection View UI improvements
* Firmware compatibility fixes
* Network Sharing Improvements

## 2021-07-29, Version 1.1.0

* Network Sharing
* LC40 Setup Application
* Menu and icons redesign

## 2021-06-28, Version 1.0.10

* ZML compatibility improvements
* Device settings UX improvements

## 2021-06-17, Version 1.0.9

* ZML compatibility fixes

## 2021-06-07, Version 1.0.8

* X-JOY compatibility fixes

## 2021-06-04, Version 1.0.7

* FW6 Compatibility fixes

## 2021-05-27, Version 1.0.6

* Firmware updater fixes
* Bug fixes and UX improvements

## 2021-05-17, Version 1.0.4

* Single-axis controller fix

## 2021-05-14, Version 1.0.3

* Menu redesign
* Bug fixes and UX improvements

## 2021-05-11, Version 1.0.2

* Bug fixes and UX improvements

## 2021-04-30, Version 1.0.1

* Bug fixes and UX improvements

## 2021-04-13, Version 1.0.0

* **Release**
