import { createSelector } from 'reselect';

import { selectCloud } from '../store';
import { nullGuard } from '../utils';
import type { NewRouters } from '../connection_manager/components/NewRouters';
import { RouterType } from '../connection_manager/types';
import { selectRoutersArray } from '../connection_manager/selectors';

import { AuthState, CustomAttributes } from './types';

export const selectLoginData = createSelector(selectCloud, state => state.loginData);
export const selectLoginInProgress = createSelector(selectCloud, state => state.loginInProgress);
export const selectLoginError = createSelector(selectCloud, state => state.loginError);

export const selectAuthState = createSelector(selectCloud, state => state.authState);
export const selectIsAuthenticated = createSelector(selectCloud, state => state.authState === AuthState.Authenticated);
export const selectUser = createSelector(selectCloud, state => state.loggedInUser);
export const selectIotRealm = createSelector(selectUser, nullGuard(user => user[CustomAttributes.IdentityId]));

export const selectSharingStatus = createSelector(selectCloud, state => state.sharingStatus);
export const selectSharingError = createSelector(selectCloud, state => state.sharingError);
export const selectSharingActionInProgress = createSelector(selectCloud, state => state.sharingActionInProgress);
export const selectCanLogout = createSelector(selectCloud, state => !state.sharingActionInProgress);

export const selectDiscoveredClients = createSelector(selectCloud, selectSharingStatus,
  (state, status) =>
    (state.discoveredClients ?? [])
      .filter(client => client.id !== status?.id)
      .map<NewRouters.NewRouter>(client => ({ type: RouterType.Cloud, ...client }))
      .sort((a, b) => a.name.localeCompare(b.name))
);
export const selectNothingDiscovered = createSelector(selectDiscoveredClients, selectRoutersArray, (clients, routers) =>
  clients.every(client => routers.find(router => router.url === client.url) != null)
);
export const selectDiscoveryError = createSelector(selectCloud, state => state.discoveryError);
