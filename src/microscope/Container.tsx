import React  from 'react';
import { useSelector } from 'react-redux';
import { ContextMenu, Icons, MinorMenu } from '@zaber/react-library';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

import { useActions } from '../utils';
import { Editions, environment } from '../environment';

import { actions as actionsDefinition } from './actions';
import { selectDemoMode, selectProductionMode } from './selectors';
import { RestartMenuItem } from './Restart';
import { Paths } from './types';

const Menu: React.FC = () => {
  const actions = useActions(actionsDefinition);
  const productionMode = useSelector(selectProductionMode);

  return (
    <ContextMenu>
      <ContextMenu.Item onClick={() => actions.resetConfigDialog(true)} icon={<Icons.Settings/>}>
        Component Setup
      </ContextMenu.Item>
      <RestartMenuItem/>
      <ContextMenu.Item onClick={() => actions.factoryResetSetStep('confirm')} icon={<Icons.FactoryReset/>}>
        Factory Reset
      </ContextMenu.Item>
      {environment.edition !== Editions.Public &&
        <ContextMenu.CheckboxItem checked={productionMode} onChecked={actions.setProductionMode}>
          Production Mode
        </ContextMenu.CheckboxItem>}
    </ContextMenu>);
};


const Header: React.FC = ({ children }) => {
  const demoMode = useSelector(selectDemoMode);

  return (<div className="header">
    <MinorMenu barStyle="horizontal">
      <NavLink to={Paths.Overview}>
        <MinorMenu.Item>Overview</MinorMenu.Item>
      </NavLink>
      {(environment.edition !== Editions.Public || demoMode) && <NavLink to={Paths.MultiDAcq}>
        <MinorMenu.Item>Multi-D Acquisition (Demo)</MinorMenu.Item>
      </NavLink>}

      <div className="right-side">
        {children}
        <Menu/>
      </div>
    </MinorMenu>
  </div>);
};

interface Props {
  className?: string;
  children: React.ReactNode;
  headerChildren?: React.ReactNode;
}

export const Container: React.FC<Props> = ({ className, children, headerChildren }) => <>
  <Header>{headerChildren}</Header>
  <div className={classNames('microscope-container', className)}>
    {children}
  </div>
</>;
