import { RenderResult, render, fireEvent } from '@testing-library/react';
import { Container, injectable } from 'inversify';
import React from 'react';

import { ZaberApi } from '../app_components';
import { createContainer, destroyContainer } from '../container';
import { waitTick, waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';
import { environment } from '../environment';

import { FeedbackButton } from './FeedbackButton';
import { FeedbackModal } from './FeedbackModal';

const TestFeedback = wrapWithNewStore(wrapWithRouter(() => <>
  <FeedbackModal/>
  <FeedbackButton/>
</>));

let wrapper: RenderResult;
let container: Container;
let mockApi: ZaberApiMock;
let resolveRequest: () => Promise<void>;

@injectable()
class ZaberApiMock extends ZaberApi {
  sendFeedback = jest.fn().mockImplementation(async () => {
    await resolveRequest();
  });
}

function verifySentFeedback(mockSend: jest.Mock, comment: string, email?: string) {
  expect(mockSend).toHaveBeenCalledTimes(1);
  expect(mockSend).toHaveBeenCalledWith({
    product: 'Zaber Launcher',
    version: 'Test',
    comment,
    meta: {
      email,
      edition: 'dev',
      flavor: 'zaber',
      os: environment.platform,
    },
  });
}

beforeEach(() => {
  container = createContainer();
  container.bind(ZaberApi).to(ZaberApiMock);
  resolveRequest = () => Promise.resolve();
  mockApi = container.get(ZaberApi) as ZaberApiMock;
  wrapper = render(<TestFeedback/>);
  fireEvent.click(wrapper.getByTitle('Open Feedback Form'));
});

afterEach(() => {
  wrapper.unmount();
  mockApi = null!;
  container = null!;
  wrapper = null!;
  destroyContainer();
});

const validEmail = 'me@test.ing';
const invalidEmails = ['@test.ing', ' me@test.ing', 'test.ing ', 'me'];


test('Proper text is displayed when feedback is sent', async () => {
  await waitUntilPass(() => wrapper.getByText('Do you have a suggestion or have you found a bug? Let us know in the field below.'));
  fireEvent.input(wrapper.getByTitle('Input feedback'), { target: { value: 'test of feedback' } });
  fireEvent.click(wrapper.getByText('Send feedback anonymously'));
  fireEvent.click(wrapper.getByTitle('Send feedback'));
  verifySentFeedback(mockApi.sendFeedback, 'test of feedback');
  await waitUntilPass(() => wrapper.getByText('We appreciate you taking the time to share your thoughts!'));
});

test('Close modal', async () => {
  expect(wrapper.queryByText('Send Feedback')).not.toBeNull();
  fireEvent.click(wrapper.getByTitle('Close Dialog'));
  await waitTick();
  expect(wrapper.queryByText('Send Feedback')).toBeNull();
});

describe('Valid feedback and email', () => {
  beforeEach(async () => {
    fireEvent.input(wrapper.getByTitle('Input feedback'), { target: { value: 'test of feedback' } });
  });

  test('Send email with feedback', async () => {
    await waitUntilPass(() => wrapper.getByTitle('Input email'));
    fireEvent.input(wrapper.getByTitle('Input email'), { target: { value: validEmail } });
    fireEvent.click(wrapper.getByTitle('Send feedback'));
    verifySentFeedback(mockApi.sendFeedback, 'test of feedback', validEmail);
  });

  test('Do not send email if the checkbox is on', async () => {
    await waitUntilPass(() => wrapper.getByTitle('Input email'));
    fireEvent.input(wrapper.getByTitle('Input email'), { target: { value: validEmail } });
    fireEvent.click(wrapper.getByText('Send feedback anonymously'));
    fireEvent.click(wrapper.getByTitle('Send feedback'));
    verifySentFeedback(mockApi.sendFeedback, 'test of feedback', undefined);
  });

  test('Remember email when "Send feedback anonymously" is toggled', async () => {
    await waitUntilPass(() => wrapper.getByTitle('Input email'));
    fireEvent.input(wrapper.getByTitle('Input email'), { target: { value: validEmail } });
    fireEvent.click(wrapper.getByText('Send feedback anonymously'));
    fireEvent.click(wrapper.getByText('Send feedback anonymously'));
    fireEvent.click(wrapper.getByTitle('Send feedback'));
    verifySentFeedback(mockApi.sendFeedback, 'test of feedback', validEmail);
  });

  test('Sending feedback message', async () => {
    let resolve: () => void = () => null;
    resolveRequest = () => new Promise(res => {
      resolve = res;
    });

    fireEvent.click(wrapper.getByText('Send feedback anonymously'));
    fireEvent.click(wrapper.getByTitle('Send feedback'));
    await waitUntilPass(() => wrapper.getByText('Sending your feedback'));
    resolve();
    await waitUntilPass(() => wrapper.getByText('We appreciate you taking the time to share your thoughts!'));
  });

  test('Handles error and allows to try again', async () => {
    resolveRequest = () => Promise.reject(new Error('No Internet'));
    fireEvent.click(wrapper.getByText('Send feedback anonymously'));
    fireEvent.click(wrapper.getByTitle('Send feedback'));
    await waitUntilPass(() => wrapper.getByText('There was a problem sending your feedback.'));
    wrapper.getByText('No Internet');

    resolveRequest = () => Promise.resolve();
    fireEvent.click(wrapper.getByTitle('Send feedback'));
    await waitUntilPass(() => wrapper.getByText(/We appreciate/));
  });
});

describe('Invalid feedback or email', () => {
  test('Invalid feedback', () => {
    fireEvent.input(wrapper.getByTitle('Input feedback'), { target: { value: '' } });
    fireEvent.click(wrapper.getByTitle('Send feedback'));
    expect(mockApi.sendFeedback).toHaveBeenCalledTimes(0);
  });

  test('Invalid email', () => {
    fireEvent.input(wrapper.getByTitle('Input feedback'), { target: { value: 'test feedback' } });
    invalidEmails.forEach(email => {
      fireEvent.input(wrapper.getByTitle('Input email'), { target: { value: email } });
      fireEvent.click(wrapper.getByTitle('Send feedback'));
      expect(mockApi.sendFeedback).toHaveBeenCalledTimes(0);
    });
  });
});
