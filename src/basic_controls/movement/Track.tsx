import React from 'react';
import { match } from 'ts-pattern';

import { useActions } from '../../utils';
import { usePolledAxisSetting } from '../../polling';
import type { Moveable } from '../types';

import { actionDefinitions } from './actions';
import { TrackCyclic } from './TrackCyclic';
import { TrackLinear } from './TrackLinear';
import type { AxisMovementState } from './types';

type Props = {
  moveable: Moveable;
  axisState: AxisMovementState;
};

export const Track: React.FC<Props> = ({ moveable, axisState }) => {
  const position = usePolledAxisSetting(axisState.key, { setting: 'pos' }, axisState.lastCommand != null);
  const actions = useActions(actionDefinitions);
  const move = (position: number) => actions.issueCommand(moveable, { type: 'abs', initiator: 'track', position });

  return match(moveable.motion)
    .with({ type: 'linear' }, motion => <TrackLinear motion={motion} axis={axisState} position={position} onRequestMovement={move}/>)
    .with({ type: 'cyclic' }, motion => <TrackCyclic motion={motion} axis={axisState} position={position} onRequestMovement={move}/>)
    .exhaustive();
};
