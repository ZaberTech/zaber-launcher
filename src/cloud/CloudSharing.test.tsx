import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

jest.mock('uuid', () => ({ v4: () => 'uuid' }));

import { createContainer, destroyContainer } from '../container';
import { StorageMock, mockStorage, waitTick, waitUntilPass, wrapWithNewStore } from '../test';
import { MessageRoutersService } from '../message_router';
import { ZaberApi } from '../app_components';

import { CloudSharing, REFRESH_STATUS_INTERVAL } from './CloudSharing';
import { MessageRoutersServiceMock } from './mocks';
import { CLOUD_SHARE_ID_STORAGE_KEY, CLOUD_SHARE_TOKEN_STORAGE_KEY } from './sagas';

let container: Container;
let messageRouter: MessageRoutersServiceMock;
let storage: StorageMock;
let zaberApi: ZaberApiMock;

@injectable()
class ZaberApiMock {
  createUserAccessToken = jest.fn<ReturnType<ZaberApi['createUserAccessToken']>, Parameters<ZaberApi['createUserAccessToken']>>()
    .mockResolvedValue({ token: 'secret', tokenId: 'token_id' });
  deleteUserAccessToken = jest.fn<ReturnType<ZaberApi['deleteUserAccessToken']>, Parameters<ZaberApi['deleteUserAccessToken']>>();
}

const TestCloudSharing = wrapWithNewStore(CloudSharing);

let wrapper: RenderResult;

beforeAll(() => {
  jest.useFakeTimers();
});
afterAll(() => {
  jest.useRealTimers();
});

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  messageRouter = container.get<unknown>(MessageRoutersService) as MessageRoutersServiceMock;
  container.bind<unknown>(ZaberApi).to(ZaberApiMock);
  zaberApi = container.get<unknown>(ZaberApi) as ZaberApiMock;
  storage = mockStorage(container);

  wrapper = render(<TestCloudSharing preloadedState={{
    cloud: { loggedInUser: { 'sub': 'user_id', 'custom:identity_id': 'identity' } },
  }}/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
});

test('queries the status and displays it', async () => {
  await waitUntilPass(() => wrapper.getByText('Offline'));
  messageRouter.isConnected = true;
  jest.advanceTimersByTime(REFRESH_STATUS_INTERVAL);
  await waitUntilPass(() => wrapper.getByText('Online'));
});

test('stops querying the status once dismounted', async () => {
  await waitUntilPass(() => wrapper.getByText('Offline'));
  messageRouter.getCloudStatus.mockClear();

  wrapper.unmount();
  jest.advanceTimersByTime(REFRESH_STATUS_INTERVAL);
  await waitTick();

  expect(messageRouter.getCloudStatus).not.toHaveBeenCalled();
});

test('deals with unexpected error on status', async () => {
  messageRouter.getCloudStatus.mockRejectedValueOnce(new Error('This should not happen'));

  jest.advanceTimersByTime(REFRESH_STATUS_INTERVAL);

  await waitUntilPass(() => wrapper.getByText(/This should not happen/));
});

test('turns on the sharing', async () => {
  const toggle = wrapper.getByTitle('Turn sharing on');
  fireEvent.click(toggle);
  expect(toggle).toHaveClass('loading');

  messageRouter.isConnected = true;

  await waitUntilPass(() => wrapper.getByText('Online'));

  expect(toggle).not.toHaveClass('loading');
  expect(messageRouter.setCloudConfig).toHaveBeenCalledWith(expect.objectContaining({
    apiUrl: 'https://api.test.io',  token: 'secret', id: 'uuid', realm: 'identity',
  }));
  expect(zaberApi.createUserAccessToken).toHaveBeenCalledWith({
    createdBy: 'zaberLauncher',
    description: 'Token created for cloud sharing on name',
    scopes: ['iot'],
  });
  expect(storage.stored[CLOUD_SHARE_TOKEN_STORAGE_KEY]).toBe('token_id');
});

test('reuses stored cloud ID', async () => {
  storage.stored[CLOUD_SHARE_ID_STORAGE_KEY] = 'another_id';

  const toggle = wrapper.getByTitle('Turn sharing on');
  fireEvent.click(toggle);

  messageRouter.isConnected = true;
  await waitUntilPass(() => wrapper.getByText('Online'));

  expect(messageRouter.setCloudConfig).toHaveBeenCalledWith(expect.objectContaining({ id: 'another_id' }));
});

describe('sharing on', () => {
  beforeEach(async () => {
    storage.stored[CLOUD_SHARE_TOKEN_STORAGE_KEY] = 'token_id';

    messageRouter.mockSharingOn();
    jest.advanceTimersByTime(REFRESH_STATUS_INTERVAL);

    await waitUntilPass(() => wrapper.getByText('Online'));
  });

  test('turns off the sharing', async () => {
    const toggle = wrapper.getByTitle('Turn sharing off');
    fireEvent.click(toggle);
    expect(toggle).toHaveClass('loading');

    messageRouter.isConnected = false;

    await waitUntilPass(() => wrapper.getByText('Offline'));
    expect(toggle).not.toHaveClass('loading');
    expect(messageRouter.setCloudConfig).toHaveBeenCalledWith(null);
    expect(zaberApi.deleteUserAccessToken).toHaveBeenCalledWith('token_id');
    expect(storage.stored[CLOUD_SHARE_TOKEN_STORAGE_KEY]).toBe(null);
  });

  test('doest not disable sharing if it cannot delete the token', async () => {
    zaberApi.deleteUserAccessToken.mockRejectedValueOnce(new ZaberApi.Error('Internet down'));

    const toggle = wrapper.getByTitle('Turn sharing off');
    fireEvent.click(toggle);

    await waitUntilPass(() => wrapper.getByText(/Internet down/));
    expect(messageRouter.setCloudConfig).not.toHaveBeenCalled();
    expect(storage.stored[CLOUD_SHARE_TOKEN_STORAGE_KEY]).toBe('token_id');
  });
});

test('deals with unexpected error on turning on', async () => {
  messageRouter.setCloudConfig.mockRejectedValueOnce(new Error('This should not happen'));

  const toggle = wrapper.getByTitle('Turn sharing on');
  fireEvent.click(toggle);

  await waitUntilPass(() => wrapper.getByText(/This should not happen/));
});
