import { environment, Flavors } from '../environment';

export const COMPANY_NAME = {
  [Flavors.Zaber]: 'Zaber',
  [Flavors.Rexroth]: 'Bosch Rexroth',
}[environment.flavor];
