import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectPreferences } from '../store';

import { PreferenceState } from './types';

export const selectLauncherName = createSelector(selectPreferences, ({ settingName, nameError }) => ({
  isSetting: settingName, error: nameError,
}));

export const selectPreferenceStates = createSelector(
  selectPreferences, ({ preferenceStates }): _.Dictionary<PreferenceState> => preferenceStates,
);
