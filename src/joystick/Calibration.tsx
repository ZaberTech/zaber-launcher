import React from 'react';
import { useSelector } from 'react-redux';
import { Button, ButtonRow, Icons, Modal, NoticeBanner } from '@zaber/react-library';

import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';
import { selectCalibration } from './selectors';

export const Calibration: React.FC<{ isOpen: boolean; onClose: () => void }> = ({ isOpen, onClose }) => {
  const actions = useActions(actionsDefinition);
  const { calibrating, calibrationError } = useSelector(selectCalibration);

  if (!isOpen) {
    return null;
  }

  return (<Modal className="calibration"
    headerIcon={<Icons.Joystick/>}
    headerText="Calibrate Joystick"
    onRequestClose={() => {
      if (calibrating) { return }
      onClose();
    }}>
    {calibrationError != null && <NoticeBanner>{calibrationError}</NoticeBanner>}
    <div>
      <p>
        Zaber's joysticks are calibrated before shipping, and we do not recommend re-calibrating
        unless you encounter problems such as motion occurring while the joystick is in the neutral position,
        or an inability to reach maximum velocity even with the joystick fully displaced.
      </p>

      <p>To calibrate limits:</p>
      <ol>
        <li>Press "Calibrate Limits" button below</li>
        <li>Move joystick all the way to the left and all the way to the right</li>
        <li>Move joystick all the way up and all the way down</li>
        <li>Turn the joystick handle all the way counter-clockwise and all the way clockwise</li>
        <li>Press "Calibration Done" button</li>
      </ol>
      <ButtonRow justify="left">
        {calibrating !== 'limits' && <Button disabled={calibrating != null} onClick={() => actions.startCalibration('limits')}>
          Start Limit Calibration
        </Button>}
        {calibrating === 'limits' && <>
          <Button color="grey" onClick={() => actions.stopCalibration(false)}>Cancel Calibration</Button>
          <Button onClick={() => actions.stopCalibration(true)}>Finish Calibration</Button>
        </>}
      </ButtonRow>

      <p>To calibrate deadbands:</p>
      <ol>
        <li>Press "Calibrate Deadbands" button below</li>
        <li>
          Wiggle joystick slightly to the left and right of the neutral position.
          Try to move only slightly beyond the limits of the slack.
        </li>
        <li>Wiggle joystick slightly up and down from the neutral position.</li>
        <li>Wiggle joystick handle slightly counter-clockwise and clockwise from the neutral position</li>
        <li>Press "Calibration Done" button</li>
      </ol>
      <ButtonRow justify="left">
        {calibrating !== 'deadbands' && <Button disabled={calibrating != null} onClick={() => actions.startCalibration('deadbands')}>
          Start Deadband Calibration
        </Button>}
        {calibrating === 'deadbands' && <>
          <Button color="grey" onClick={() => actions.stopCalibration(false)}>Cancel Calibration</Button>
          <Button onClick={() => actions.stopCalibration(true)}>Finish Calibration</Button>
        </>}
      </ButtonRow>
    </div>
  </Modal>);
};
