import { changeDictionary } from '@zaber/toolbox';
import _ from 'lodash';

import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes, DevicesLoadedPayload } from '../connection_manager';
import { tryExtractConnectionKey } from '../keys';
import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import { BlockMessage, isPrimaryAxis, Settings, sortMessages } from './types';

export interface State {
  evaluating: boolean;
  evaluatingProgress: number | null;
  performing: boolean;
  warnings: BlockMessage[];
  error: BlockMessage | null;
  performingBlockIndex: number | null;
  stopping: boolean;
  hardStopping: boolean;
  step: boolean;
  currentSettings: Settings | null;
  editedSettings: Partial<Settings> | null;
  loadingDevice: boolean;
  loadingDeviceError: string | null;
  translatorRunning: boolean;
  translatorError: string | null;
  loadedContent: { content: string };
  command: { error?: string; warnings: string[]; executing: boolean };
  coordinateSystem: string;
  axesLiveStates: AxesLiveStates;
  feedRateOverride: number;
  holding: boolean;
}

export type AxesLiveStates = Record<string, AxisLiveState>;

export interface AxisLiveState {
  letter: string;
  position: number | null;
  positionCoordinateSystem: number | null;
  moving: boolean;
  needsHoming: boolean;
}

const initialState: State = {
  evaluating: false,
  evaluatingProgress: null,
  performing: false,
  warnings: [],
  error: null,
  performingBlockIndex: null,
  stopping: false,
  hardStopping: false,
  step: false,
  currentSettings: null,
  editedSettings: null,
  loadingDevice: false,
  loadingDeviceError: null,
  translatorRunning: false,
  translatorError: null,
  loadedContent: { content: '' },
  command: { warnings: [], executing: false },
  coordinateSystem: '',
  axesLiveStates: {},
  feedRateOverride: 100,
  holding: false,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const evaluate: Reducer<ActionTypes.EVALUATE> = state => ({
  ...state,
  evaluating: true,
  evaluatingProgress: null,
  warnings: [],
  error: null,
});

const evaluateProgress: Reducer<ActionTypes.EVALUATE_PROGRESS> = (state, { progress }) => ({
  ...state,
  evaluatingProgress: progress,
});

const evaluateDone: Reducer<ActionTypes.EVALUATE_DONE> = (state, { warnings, error }) => ({
  ...state,
  evaluating: false,
  evaluatingProgress: null,
  warnings: sortMessages(warnings),
  error: error ?? null,
});


const perform: Reducer<ActionTypes.PERFORM> = (state, { beginIndex }) => ({
  ...state,
  warnings: state.warnings.filter(warning => warning.blockIndex < beginIndex),
  error: null,
  performingBlockIndex: null,
});
const performing: Reducer<ActionTypes.PERFORMING> = state => ({
  ...state,
  performing: true,
});
const performingDone: Reducer<ActionTypes.PERFORMING_DONE> = (state, { error }) => ({
  ...state,
  performing: false,
  error: state.error ?? error ?? null,
  stopping: false,
  hardStopping: false,
});

const addWarnings: Reducer<ActionTypes.ADD_WARNINGS> = (state, { warnings }) => ({
  ...state,
  warnings: state.warnings.concat(sortMessages(warnings)),
});

const setPerformingBlock: Reducer<ActionTypes.SET_PERFORMING_BLOCK> = (state, { blockIndex }) => ({
  ...state,
  performingBlockIndex: blockIndex,
});

const stop: Reducer<ActionTypes.STOP> = state => ({
  ...state,
  stopping: state.performing ? true : state.stopping,
});
const hardStop: Reducer<ActionTypes.STOP> = state => ({
  ...state,
  hardStopping: state.performing ? true : state.hardStopping,
});

const updateSettings: Reducer<ActionTypes.UPDATE_SETTINGS> = (state, { settings }) => ({
  ...state,
  editedSettings: { ...state.editedSettings, ...settings },
});

const updateAxisSettings: Reducer<ActionTypes.UPDATE_AXIS_SETTINGS> = (state, { axisIndex, settings }) => ({
  ...state,
  editedSettings: {
    ...state.editedSettings,
    axes: state.editedSettings?.axes?.map((axis, i) => axisIndex === i ? ({ ...axis, ...settings }) : axis),
  },
});

const settingsEditingDone: Reducer<ActionTypes.SETTINGS_EDITING_DONE> = (state, { apply }) => {
  const currentSettings = apply ? state.editedSettings as Settings : state.currentSettings;
  return ({
    ...state,
    editedSettings: null,
    currentSettings,
    axesLiveStates: _.keyBy(currentSettings?.axes
      .filter(isPrimaryAxis)
      .map<AxisLiveState>(axis => ({
        letter: axis.letter!,
        moving: false,
        position: null,
        positionCoordinateSystem: null,
        needsHoming: false,
      })), 'letter')
  });
};

const selectDevice: Reducer<ActionTypes.SELECT_DEVICE> = (state, { deviceKey }) => ({
  ...state,
  editedSettings: { deviceKey },
  loadingDevice: true,
  loadingDeviceError: null,
});

const loadingDeviceDone: Reducer<ActionTypes.LOADING_DEVICE_DONE> = (state, { error }) => ({
  ...state,
  loadingDevice: false,
  loadingDeviceError: error ?? null,
});

const startTranslator: Reducer<ActionTypes.START_TRANSLATOR> = state => ({
  ...state,
  translatorRunning: false,
  translatorError: null,
});
const stopTranslator: Reducer<ActionTypes.STOP_TRANSLATOR> = state => ({
  ...state,
  translatorRunning: false,
  translatorError: null,
});
const translatorRunning: Reducer<ActionTypes.TRANSLATOR_RUNNING> = state => ({
  ...state,
  translatorRunning: true,
  performingBlockIndex: null,
  warnings: [],
  error: null,
  command: initialState.command,
  holding: false,
});
const translatorErr: Reducer<ActionTypes.TRANSLATOR_ERR> = (state, { error }) => ({
  ...state,
  translatorRunning: false,
  translatorError: error,
});

const loadContent: Reducer<ActionTypes.LOAD_CONTENT> = (state, { content }) => ({
  ...state,
  loadedContent: { content },
  warnings: [],
  error: null,
});

const setStep: Reducer<ActionTypes.SET_STEP> = (state, { step }) => ({
  ...state,
  step,
});

const rewind: Reducer<ActionTypes.REWIND> = state => ({
  ...state,
  performingBlockIndex: null,
  warnings: [],
  error: null,
});

const setPerformingBlockError: Reducer<ActionTypes.SET_PERFORMING_BLOCK_ERROR> = (state, { error }) =>
  state.performingBlockIndex != null ? ({
    ...state,
    error: {
      blockIndex: state.performingBlockIndex, blockIndexEnd: state.performingBlockIndex,
      fromBlock: -1, toBlock: -1,
      message: error,
    },
  }) : state;

const updatePositionDone: Reducer<ActionTypes.UPDATE_POSITION_DONE> = (state, { updates, coordinateSystem }) => ({
  ...state,
  coordinateSystem,
  axesLiveStates: _.merge({}, state.axesLiveStates, updates),
});

const moveAxis: Reducer<ActionTypes.MOVE_AXIS> = (state, { axisLetter }) => ({
  ...state,
  axesLiveStates: changeDictionary(state.axesLiveStates, axisLetter, { moving: true }),
});
const moveAxisDone: Reducer<ActionTypes.MOVE_AXIS_DONE> = (state, { axisLetter }) => ({
  ...state,
  axesLiveStates: changeDictionary(state.axesLiveStates, axisLetter, { moving: false }),
});

const command: Reducer<ActionTypes.COMMAND> = state => ({
  ...state,
  command: { ...initialState.command, executing: true },
});
const commandDone: Reducer<ActionTypes.COMMAND_DONE> = (state, { warnings, error }) => ({
  ...state,
  command: { ...state.command, executing: false, warnings, error },
});
const commandClear: Reducer<ActionTypes.COMMAND_CLEAR> = state => ({
  ...state,
  command: initialState.command,
});

const setFeedRateOverride: Reducer<ActionTypes.SET_FEED_RATE_OVERRIDE> = (state, { override }) => ({
  ...state,
  feedRateOverride: override,
});

const setFreeHold: Reducer<ActionTypes.SET_FREE_HOLD> = (state, { hold }) => ({
  ...state,
  holding: hold,
});

const devicesLoaded = (state: State, { connectionKey }: DevicesLoadedPayload): State => {
  if (tryExtractConnectionKey(state.editedSettings?.deviceKey) === connectionKey ||
    tryExtractConnectionKey(state.currentSettings?.deviceKey) === connectionKey) {
    return ({
      ...state,
      currentSettings: null,
      editedSettings: null,
    });
  }
  return state;
};

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ActionTypes.EVALUATE]: evaluate,
  [ActionTypes.EVALUATE_PROGRESS]: evaluateProgress,
  [ActionTypes.EVALUATE_DONE]: evaluateDone,
  [ActionTypes.PERFORM]: perform,
  [ActionTypes.PERFORMING]: performing,
  [ActionTypes.PERFORMING_DONE]: performingDone,
  [ActionTypes.ADD_WARNINGS]: addWarnings,
  [ActionTypes.SET_PERFORMING_BLOCK]: setPerformingBlock,
  [ActionTypes.STOP]: stop,
  [ActionTypes.HARD_STOP]: hardStop,
  [ActionTypes.UPDATE_SETTINGS]: updateSettings,
  [ActionTypes.UPDATE_AXIS_SETTINGS]: updateAxisSettings,
  [ActionTypes.SETTINGS_EDITING_DONE]: settingsEditingDone,
  [ActionTypes.SELECT_DEVICE]: selectDevice,
  [ActionTypes.LOADING_DEVICE_DONE]: loadingDeviceDone,
  [ActionTypes.START_TRANSLATOR]: startTranslator,
  [ActionTypes.STOP_TRANSLATOR]: stopTranslator,
  [ActionTypes.TRANSLATOR_ERR]: translatorErr,
  [ActionTypes.TRANSLATOR_RUNNING]: translatorRunning,
  [ActionTypes.LOAD_CONTENT]: loadContent,
  [ActionTypes.SET_STEP]: setStep,
  [ActionTypes.REWIND]: rewind,
  [ActionTypes.SET_PERFORMING_BLOCK_ERROR]: setPerformingBlockError,
  [ActionTypes.UPDATE_POSITION_DONE]: updatePositionDone,
  [ActionTypes.MOVE_AXIS]: moveAxis,
  [ActionTypes.MOVE_AXIS_DONE]: moveAxisDone,
  [ActionTypes.COMMAND]: command,
  [ActionTypes.COMMAND_DONE]: commandDone,
  [ActionTypes.COMMAND_CLEAR]: commandClear,
  [ActionTypes.SET_FEED_RATE_OVERRIDE]: setFeedRateOverride,
  [ActionTypes.SET_FREE_HOLD]: setFreeHold,
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
}, initialState);
