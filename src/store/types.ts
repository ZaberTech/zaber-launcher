import { type Store } from 'redux';
import { type State } from './reducer';

export const reduxStoreSymbol = Symbol('REDUX_STORE');
export type OurStore = Store<State>;
