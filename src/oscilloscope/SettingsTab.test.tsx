import React from 'react';
import _ from 'lodash';
import { fireEvent, render, RenderResult, within } from '@testing-library/react';
import type { Container } from 'inversify';

import { IntersectionObserverMockBase } from '../test/mocks/intersection_observer';
import 'jest-canvas-mock';

import {
  EChartsReactMock,
  MessageRoutersServiceMockIntegrated,
  SelectMock,
  setAxisCallback,
  setDeviceCallback
} from './testing/mocks';

jest.mock('react-select', () => SelectMock);

jest.mock('echarts-for-react', () => EChartsReactMock);

class IntersectionObserverMock extends IntersectionObserverMockBase {
}

(window as unknown as { IntersectionObserver: typeof IntersectionObserverMock }).IntersectionObserver = IntersectionObserverMock;

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { selectDevices } from '../connection_manager';
import { mockSingleDevice } from '../connection_manager/mocks';
import { createContainer, destroyContainer } from '../container';
import { MessageRoutersService } from '../message_router';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter } from '../test';

import { Oscilloscope } from './Oscilloscope';
import { Mode } from './types';
import {
  openAddSettingsDialog,
  selectChannelControls,
  selectRecordingControls,
  selectRecordMode,
  setNumSamples,
  setSampleRate
} from './testing/utils';


let container: Container;
const TestOscilloscope = wrapWithNewStore(wrapWithRouter(Oscilloscope));
let wrapper: RenderResult;


/* =================================== Start of tests ================================== */

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMockIntegrated);

  wrapper = render(<TestOscilloscope/>);
});


afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;

  setAxisCallback(null);
  setDeviceCallback(null);
});


describe('Capture settings tab', () => {
  beforeEach(() => {
    const store = TestOscilloscope.testStore;
    mockSingleDevice(store, device => {
      device.identity!.firmwareVersion.minor = 29;
      return device;
    });
  });


  describe('with a legacy axis selected', () => {
    beforeEach(async () => {
      const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => wrapper.getByTestId('Data Source add'));
      await selectRecordingControls(wrapper);
    });


    it('switches controls on mode select', () => {
      wrapper.getByText('Capture Mode');
      wrapper.getByText('Target Sampling Rate (Hz)');

      selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
      wrapper.getByText('High-resolution capture');
      wrapper.getByText('Movement');
      wrapper.getByText('Return to Start');
      wrapper.getByText('Timer (s)');
      wrapper.getByText('Target Sampling Rate (Hz)');
      wrapper.getByText('Number of Samples');
      wrapper.getByText('Recording Length');
      wrapper.getByText('Differencing Window (ms)');

      selectRecordMode(wrapper, Mode.POLLED);
      wrapper.getByText('Low-resolution capture');
      wrapper.getByText('Target Sampling Rate (Hz)');
      wrapper.getByText('Recording Length');
      wrapper.getByText('Differencing Window (ms)');

      selectRecordMode(wrapper, Mode.EXTERNAL);
      wrapper.getByText('Externally triggered high-resolution capture');
      wrapper.getByText('I\'m also reading the data');
      wrapper.getByText('Differencing Window (ms)');
    });


    it('changes polled time estimate when sample rate changes', () => {
      selectRecordMode(wrapper, Mode.POLLED);
      setSampleRate(wrapper, 100);
      expect(wrapper.getByTestId('time-estimate')).toHaveTextContent('500ms'); // Note polled buffer size is different in test mode.
      setSampleRate(wrapper, 10);
      expect(wrapper.getByTestId('time-estimate')).toHaveTextContent('5s');
    });


    it('changes buffered time estimate when sample rate changes', () => {
      selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
      setSampleRate(wrapper, 10000);
      expect(wrapper.getByTestId('time-estimate')).toHaveTextContent('102ms');
      setSampleRate(wrapper, 1000);
      expect(wrapper.getByTestId('time-estimate')).toHaveTextContent('1s 24ms');
    });
  });


  describe('with a reshapable axis selected', () => {
    beforeEach(async () => {
      setDeviceCallback(device => {
        device._settings['scope.numchannels'] = 6;
        device._settings['scope.channel.size.max'] = 6 * 1024;
      });

      const device = _.sample(selectDevices(TestOscilloscope.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(device.key);
      await waitUntilPass(() => wrapper.getByTestId('Data Source add'));
      await selectRecordingControls(wrapper);
    });

    it('estimates buffered time differently', () => {
      selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
      setSampleRate(wrapper, 10000);
      expect(wrapper.getByTestId('time-estimate')).toHaveTextContent('307ms');
    });


    it('changes time estimate when the sampling rate changes', () => {
      selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
      setSampleRate(wrapper, 100);
      expect(wrapper.getByTestId('time-estimate')).toHaveTextContent('30s 720ms');
    });


    it('changes time estimate when the number of settings channels changes', async () => {
      await selectChannelControls(wrapper);
      const dialog = openAddSettingsDialog(wrapper);
      fireEvent.change(wrapper.getByTestId('setting-name-filter'), { target: { value: 'foo' } });
      fireEvent.click(within(dialog).getByText('Add'));

      await selectRecordingControls(wrapper);
      selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
      setSampleRate(wrapper, 10000);

      expect(wrapper.getByTestId('time-estimate')).toHaveTextContent('204ms');
    });


    it('changes time estimate when the user enters a sample quantity', async () => {
      selectRecordMode(wrapper, Mode.BUFFERED_MANUAL);
      setNumSamples(wrapper, 100);
      expect(wrapper.getByTestId('time-estimate')).toHaveTextContent('10ms');
    });
  });
});
