import { injectable, inject } from 'inversify';

import { Storage } from '../app_components';
import { environment } from '../environment';

interface Record {
  version: string;
  value: unknown;
}

function makeKey(key: string): string {
  return `cache_${key}`;
}

@injectable()
export class Cache {
  constructor(
    @inject(Storage) private readonly storage: Storage,
  ) { }

  public save<T>(key: string, value: T): void {
    const record: Record = {
      version: environment.releaseName,
      value,
    };
    this.storage.save(makeKey(key), record);
  }

  public load<T>(key: string): T | null {
    const record: Record | null = this.storage.load(makeKey(key));
    if (!record || record.version !== environment.releaseName) {
      return null;
    }
    return record.value as T;
  }
}
