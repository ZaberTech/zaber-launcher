import type { EntityKey } from '../keys';

export const JOYSTICK_DEVICE_IDS: Record<number, JoystickDeviceId> = {
  51000: { axisCount: 3, keyCount: 8, isDefaultModel: true },
  51001: { axisCount: 0, keyCount: 4 },
};

export interface JoystickDeviceId {
  isDefaultModel?: true;
  axisCount: number;
  keyCount: number;
}

export interface JoystickConfig {
  axes: AxisConfig[];
  keys: KeyConfig[];
}

export type TargetAxisType = 'axis' | 'lockstep' | 'virtual';
export const TargetAxisTypeLabels: Record<TargetAxisType, string> = {
  axis: 'Axis',
  lockstep: 'Lockstep',
  virtual: 'Virtual Axis',
};

export interface AxisConfig {
  axisNumber: number;
  targetDevice: number | null;
  targetAxis: number | null;
  axisType: TargetAxisType;
  maxspeedNative: number | null;
  profile: SpeedProfile;
  inverted: boolean;
  resolution: number | null;
}

export enum SpeedProfile {
  Linear = 1,
  Quadratic,
  Cubic,
}

export interface KeyConfig {
  keyNumber: number;
  alerts: Record<KeyEventType, boolean>;
  commands: KeyCommand[];
}

export enum KeyEventType {
  Press = 1,
  Release,
  LongPress,
  LongPressRelease,
}
export const KeyEventTypeLabel = {
  [KeyEventType.Press]: 'Press',
  [KeyEventType.Release]: 'Release',
  [KeyEventType.LongPress]: 'Long Press',
  [KeyEventType.LongPressRelease]: 'Long Press Release',
};
export const KeyEventTypes = Object.keys(KeyEventTypeLabel).map(value => +value as KeyEventType);
export function allEventTypes<TVal>(value: TVal): Record<KeyEventType, TVal> {
  return {
    [KeyEventType.Press]: value,
    [KeyEventType.Release]: value,
    [KeyEventType.LongPress]: value,
    [KeyEventType.LongPressRelease]: value,
  };
}

export interface KeyEvent {
  event: KeyEventType;
  alert: boolean;
  commands: KeyCommand[];
}

export interface KeyCommand {
  targetDevice: number | null;
  targetAxis: number | null;
  axisType: TargetAxisType;
  event: KeyEventType;
  command: string;
}

export const DEFAULT_COMMAND: KeyCommand = {
  targetDevice: 0, targetAxis: 0, axisType: 'axis', event: KeyEventType.Press, command: '',
};

export interface ChainInfo {
  /** Device address not present on chain (typically 99). */
  nonExistentDeviceAddress: number;
  axes: Record<EntityKey, AxisChainInfo>;
}

export interface AxisChainInfo {
  maxspeedNative: number;
}

export const AXIS_NAMES = ['X', 'Y', 'Z'];

export interface SavedConfigHeader {
  type: string;
  version: number;
  deviceId: number;
}

export type SavedConfig = JoystickConfig & SavedConfigHeader;

export const SAVE_FILE_VERSION = 2;
export const SAVE_FILE_IDENT = 'zaber-joystick-config';

export type CalibrationType = 'limits' | 'deadbands';
