import { injectable } from 'inversify';
import { map, Subject } from 'rxjs';
import { EventEmitter } from 'events';

const killMock = jest.fn();
process.kill = killMock;

let ipcMain: IpcMainMock;
class IpcMainMock extends EventEmitter {
  constructor() {
    super();
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    ipcMain = this;
  }
}

jest.mock('electron', () => ({
  ipcMain: new IpcMainMock(),
}));

let ptyMock: PtyMock;
class PtyMock {
  constructor() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    ptyMock = this;
  }

  pid = 13;
  onData = jest.fn();
  onExit = jest.fn();
  write = jest.fn();
}

const spawnMock = jest.fn(() => new PtyMock());
jest.mock('node-pty', () => ({
  spawn: spawnMock,
}));

import type { AnyAction } from '../src/utils';
import {
  actions, ActionTypes,
  PROCESS_RX_IPC_CHANNEL, PROCESS_TX_IPC_CHANNEL,
  DEFAULT_TERMINAL_COLUMNS, DEFAULT_TERMINAL_ROWS,
  FAILED_EXIT_CODE,
} from '../src/pty/common_types';

import { createContainer, destroyContainer } from './container';
import { Ipc } from './ipc';
import { Pty } from './pty';

class WindowMock extends EventEmitter {
  send = jest.fn();
  isDestroyed = jest.fn(() => false);
}
let windowMock: WindowMock;

let ipcMock: IpcMock;
@injectable()
class IpcMock {
  channels: Record<string, Subject<AnyAction>> = {};

  sendAction = jest.fn<void, [unknown, AnyAction]>();
  getActions = jest.fn((actionType: string) => {
    if (!this.channels[actionType]) {
      this.channels[actionType] = new Subject<AnyAction>();
    }
    return this.channels[actionType].pipe(map(action => ({ action, sender: windowMock })));
  });
}

let container: ReturnType<typeof createContainer>;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(Ipc).to(IpcMock);
  ipcMock = container.get<unknown>(Ipc) as IpcMock;
  container.get(Pty);
  windowMock = new WindowMock();
});

afterEach(() => {
  destroyContainer();
  container = null!;
  ipcMock = null!;
  ptyMock = null!;
  spawnMock.mockClear();
  killMock.mockClear();
  windowMock = null!;
});

const SPAWN_DATA = {
  cmd: 'cmd',
  args: ['arg'],
  cwd: 'cwd',
  id: 'id',
  env: { ENV: '1' },
};

test('spawns child and notifies parent on exit', () => {
  ipcMock.channels[ActionTypes.SPAWN].next(actions.spawn(SPAWN_DATA));
  expect(spawnMock).toHaveBeenCalledWith('cmd', ['arg'], ({
    cwd: 'cwd',
    env: expect.anything(),
    cols: DEFAULT_TERMINAL_COLUMNS,
    rows: DEFAULT_TERMINAL_ROWS,
  }));

  ptyMock.onExit.mock.calls[0][0]({ exitCode: 0 });
  expect(ipcMock.sendAction).toHaveBeenCalledWith(
    windowMock,
    expect.objectContaining(actions.exit('id', 0, null)));
});

test('reports custom exit code if spawn fails', () => {
  spawnMock.mockImplementationOnce(() => { throw new Error('Fail') });
  ipcMock.channels[ActionTypes.SPAWN].next(actions.spawn(SPAWN_DATA));

  expect(ipcMock.sendAction).toHaveBeenCalledWith(
    windowMock,
    expect.objectContaining(actions.exit('id', FAILED_EXIT_CODE, null)));
});

test('writes pty data back and forth', () => {
  ipcMock.channels[ActionTypes.SPAWN].next(actions.spawn(SPAWN_DATA));

  ipcMain.emit(PROCESS_TX_IPC_CHANNEL, 'event', { id: 'id', data: 'data_tx' });
  expect(ptyMock.write).toHaveBeenCalledWith('data_tx');

  ptyMock.onData.mock.calls[0][0]('data_rx');
  expect(windowMock.send).toHaveBeenCalledWith(PROCESS_RX_IPC_CHANNEL, { id: 'id', data: 'data_rx' });
});

test('writes pty data to appropriate connections', () => {
  ipcMock.channels[ActionTypes.SPAWN].next(actions.spawn(SPAWN_DATA));
  const ptyMock1 = ptyMock;
  ipcMock.channels[ActionTypes.SPAWN].next(actions.spawn({ ...SPAWN_DATA, id: 'id2' }));
  const ptyMock2 = ptyMock;

  ipcMain.emit(PROCESS_TX_IPC_CHANNEL, 'event', { id: 'id', data: 'data1_tx' });
  ipcMain.emit(PROCESS_TX_IPC_CHANNEL, 'event', { id: 'id2', data: 'data2_tx' });
  expect(ptyMock1.write).toHaveBeenLastCalledWith('data1_tx');
  expect(ptyMock2.write).toHaveBeenLastCalledWith('data2_tx');

  ptyMock1.onData.mock.calls[0][0]('data1_rx');
  expect(windowMock.send).toHaveBeenLastCalledWith(PROCESS_RX_IPC_CHANNEL, { id: 'id', data: 'data1_rx' });
  ptyMock2.onData.mock.calls[0][0]('data2_rx');
  expect(windowMock.send).toHaveBeenLastCalledWith(PROCESS_RX_IPC_CHANNEL, { id: 'id2', data: 'data2_rx' });
});

test('receiving data for non-existing pty does nothing', () => {
  ipcMain.emit(PROCESS_TX_IPC_CHANNEL, 'event', { id: 'id', data: 'data_tx' });
});

test('receiving data for destroyed parent does nothing', () => {
  ipcMock.channels[ActionTypes.SPAWN].next(actions.spawn(SPAWN_DATA));

  windowMock.isDestroyed.mockReturnValue(true);
  ptyMock.onData.mock.calls[0][0]('data_rx');
  expect(windowMock.send).toHaveBeenCalledTimes(0);
});

test('kills pty in case parent is destroyed', () => {
  ipcMock.channels[ActionTypes.SPAWN].next(actions.spawn(SPAWN_DATA));
  windowMock.emit('destroyed');
  expect(process.kill).toHaveBeenCalled();
});

test('kills pty when requested', () => {
  ipcMock.channels[ActionTypes.SPAWN].next(actions.spawn(SPAWN_DATA));
  ipcMock.channels[ActionTypes.KILL].next(actions.kill('id'));
  expect(process.kill).toHaveBeenCalledWith(ptyMock.pid);
});

test('kills pty only once', () => {
  ipcMock.channels[ActionTypes.SPAWN].next(actions.spawn(SPAWN_DATA));

  ipcMock.channels[ActionTypes.KILL].next(actions.kill('id'));
  ipcMock.channels[ActionTypes.KILL].next(actions.kill('id'));
  windowMock.emit('destroyed');

  expect(process.kill).toHaveBeenCalledTimes(1);
});

test('killing non-existing pty does nothing', () => {
  ipcMock.channels[ActionTypes.KILL].next(actions.kill('id'));
});
