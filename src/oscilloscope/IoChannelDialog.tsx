import classNames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Checkbox, ColorPicker, Flex, HeaderCard, MinorMenu, Modal, PopUp, Text } from '@zaber/react-library';
import { changeDictionary } from '@zaber/toolbox';

import { AppIconsNeutral } from '../apps';

import { createChannelForIo, extractIoChannels, isIoChannel, makeIoChannelId, numFwChannelsUsed } from './channels';
import { UNSET_COLOR, getDefaultColor } from './colors';
import type { PerDeviceState } from './reducers';
import { selectSelectedDeviceState } from './selectors';
import { ADD_IO_BUTTON_LABEL, Channel, MAX_DEVICE_CHANNELS_TOOLTIP } from './types';


interface RowProps {
  channel: Channel;
  canSelect: boolean;
  setVisible: (visible: boolean) => void;
  setColor: (color: string) => void;
}

const ChannelRow: React.FC<RowProps> = ({ channel, canSelect, setVisible, setColor }) => <>
  <PopUp position="top"
    triggerAction={canSelect ? 'none' : 'hover'}
    trigger={() => <Checkbox checked={channel.showInUi}
      className="visibility"
      disabled={!canSelect && !channel.showInUi}
      data-testid={`dlg-show-channel ${channel.id}`}
      onChecked={() => setVisible(!channel.showInUi)}/>}>
    {MAX_DEVICE_CHANNELS_TOOLTIP}
  </PopUp>
  {channel.showInUi ?
    <ColorPicker color={channel.color} onChange={setColor}/>
    : <div className="color-picker-placeholder"/>}
  <div className="label">
    <Text t={Text.Type.Body}>{channel.title}</Text><br/>
  </div>
</>;


function groupName(portType: string): string {
  const nature = portType.startsWith('a') ? 'Analog' : 'Digital';
  const direction = portType[1] === 'i' ? 'Inputs' : 'Outputs';
  return `${nature} ${direction}`;
}


function pinCount(device: PerDeviceState, portType: string): number {
  return (device.ioPins as Record<string, number>)[portType];
}


interface Props {
  dialogOpen: boolean;
  apply: (newSelections: Channel[]) => void;
  cancel: () => void;
}


export const IoChannelDialog: React.FC<Props> = ({ dialogOpen, apply, cancel }) => {
  const device = useSelector(selectSelectedDeviceState);

  const [selectedTab, selectTab] = useState(0);
  const [channels, setChannels] = useState<_.Dictionary<Channel>>({});

  useEffect(() => {
    const allIoChannels = _.keyBy(extractIoChannels(device.axes.map(axisState => axisState.channels).flat())
      .map(ch => _.cloneDeep(ch)), ch => ch.id);
    ['ai', 'ao', 'di', 'do'].forEach(name => {
      const numPins = pinCount(device, name);
      for (let i = 1; i <= numPins; i++) {
        const key = makeIoChannelId(name, i);
        if (!(key in allIoChannels)) {
          allIoChannels[key] = {
            ...createChannelForIo(name, i),
            showInUi: false,
          };
        }
      }
    });
    setChannels(allIoChannels);
  }, []);

  const newChannels = device.axes.map(axisState => axisState.channels).flat()
    .filter(ch => !isIoChannel(ch))
    .concat(_.values(channels).filter(ch => ch.showInUi));
  const numChannelsUsed = numFwChannelsUsed(newChannels);
  const canAdd = numChannelsUsed < device.maxChannels;
  const canAddDi = _.some(newChannels, ch => isIoChannel(ch) && ch.name.startsWith('di'));
  const canAddDo = _.some(newChannels, ch => isIoChannel(ch) && ch.name.startsWith('do'));

  return <Modal
    className="add-io-channel-dialog"
    data-testid="add-io-channel-dialog"
    headerIcon={<AppIconsNeutral.Oscilloscope/>}
    headerText={ADD_IO_BUTTON_LABEL}
    isOpen={dialogOpen}
    buttons={
      <Button
        className="apply-button"
        onClick={() => apply(_.sortBy(_.values(channels).filter(ch => ch.showInUi), ch => ch.id))}
      >
        Apply
      </Button>
    }
    onRequestClose={cancel}>
    <MinorMenu barStyle="horizontal">
      {(device.ioPins.di > 0 || device.ioPins.do > 0) && <div className={classNames({ active: selectedTab === 0 })}>
        <MinorMenu.Item data-testid="digital-io-tab" onClick={() => selectTab(0)}>Digital I/O</MinorMenu.Item>
      </div>}
      {(device.ioPins.ai > 0 || device.ioPins.ao > 0) && <div className={classNames({ active: selectedTab === 1 })}>
        <MinorMenu.Item data-testid="analog-io-tab" onClick={() => selectTab(1)}>Analog I/O</MinorMenu.Item>
      </div>}
    </MinorMenu>
    <Flex.Column className="pins-list" data-testid="pins-list">
      {_.flatten(['di', 'do', 'ai', 'ao'].map((portType, portTypeIndex) => {
        const numPins = pinCount(device, portType);
        if (numPins > 0 && Math.floor(portTypeIndex / 2) === selectedTab) {
          return <HeaderCard key={portType} header={<span>{groupName(portType)}</span>}>
            <>{_.range(0, numPins).map(i => {
              const id = makeIoChannelId(portType, i + 1);
              const channel = channels[id];
              return channel && <ChannelRow channel={channel}
                setColor={color => setChannels(changeDictionary(channels, id, ch => ({
                  ...ch,
                  color,
                })))}
                setVisible={visible => setChannels(changeDictionary(channels, id, ch => ({
                  ...ch,
                  showInUi: visible,
                  color: visible && ch.color === UNSET_COLOR ? getDefaultColor(channel.id) : ch.color,
                })))}
                canSelect={canAdd || (canAddDi && portType === 'di') || (canAddDo && portType === 'do')}
                key={id}/>;
            })}</>
          </HeaderCard>;
        } else {
          return null;
        }
      }))}
    </Flex.Column>
  </Modal>;
};
