import React, { useState, memo } from 'react';
import { useSelector } from 'react-redux';
import { Icons, Text, CopyToClipboard, AnimationClasses, NoticeBorder, NoticeBanner, PopUp, TextInputEditor } from '@zaber/react-library';
import classNames from 'classnames';
import { match } from 'ts-pattern';
import { Units } from '@zaber/motion';

import { ProtocolManual, ProtocolManualToggle } from '../protocol_manual';
import { environment, Flavors } from '../environment';
import { useActions } from '../utils';
import { SearchMatch } from '../components';
import { MeasurementInputEditor } from '../units/MeasurementInputEditor';
import { Measurement, MeasurementOK, UnitInfo, convertValueUnits, measurementOK, useUnitInfo } from '../units';

import { actions as actionsDefinition } from './actions';
import { selectCurrentAccessLevel, selectCurrentKey, selectSearch } from './selectors';
import type { SettingsEntry } from './reducer';
import { DefaultValue } from './DefaultValue';
import { DEFAULT_ACCESS_LEVEL, DISPLAY_PRECISION, MAX_USER_ACCESS_LEVEL } from './types';

interface Props {
  setting: SettingsEntry;
}

const Name: React.FC<{ setting: SettingsEntry }> = ({ setting }) => {
  const search = useSelector(selectSearch);
  return (<div className="name">
    <Text t={Text.Type.H5}>
      <SearchMatch search={search}>{setting.meta?.humanName ?? setting.name}</SearchMatch>
    </Text>
    {setting.meta?.humanName != null &&
      <CopyToClipboard copyText={setting.name} type="text">
        <Text className="ascii-code" e={Text.Emphasis.Light} f={Text.Family.Mono} t={Text.Type.BodyXSm}>
          <SearchMatch search={search}>{setting.name}</SearchMatch>
        </Text>
      </CopyToClipboard>
    }
  </div>);
};

function defaultMeasure(defaultValue: string | null, measure: Measurement | null, unitInfo: UnitInfo | null): MeasurementOK | null {
  const defaultParsed = parseFloat(defaultValue ?? '');
  if (isNaN(defaultParsed)) {
    return null;
  }
  if (!measurementOK(measure) || !unitInfo?.supported) {
    if (measure?.units === Units.NATIVE) {
      return { value: defaultParsed, units: Units.NATIVE };
    } else {
      return null;
    }
  }
  return { value: convertValueUnits(unitInfo, defaultParsed, Units.NATIVE, measure.units), units: measure.units };
}

function writeErrorRenderer(e: string | null): (() => React.ReactNode) | undefined {
  return e ? () => <NoticeBanner>{e}</NoticeBanner> : undefined;
}

const SettingBase: React.FC<Props> = ({ setting }) => {
  const actions = useActions(actionsDefinition);
  const key = useSelector(selectCurrentKey)!;
  const accessLevel = useSelector(selectCurrentAccessLevel) ?? DEFAULT_ACCESS_LEVEL;
  const unitInfo = useUnitInfo({ setting: setting.name, entity: key });
  const [showManual, setShowManual] = useState(false);
  const editable = setting.writeAccessLevel != null && setting.writeAccessLevel <= accessLevel;
  const locked = setting.writeAccessLevel != null && setting.writeAccessLevel > accessLevel
    && setting.writeAccessLevel <= MAX_USER_ACCESS_LEVEL;

  return (
    <NoticeBorder className="device-setting sections-subgrid" type={setting.readError || setting.writeError ? 'error' : null}>
      <div className="device-setting-content sections-subgrid">
        <Name setting={setting}/>
        {match<typeof setting, React.ReactNode>(setting)
          .with({ type: 'number' }, ({ value: measure, mode }) => <MeasurementInputEditor
            aria-label={setting.name}
            className="setting-editor"
            unitFrom={{ setting: setting.id, entity: key }}
            measure={measure}
            onChange={update => actions.updateSetting(key, setting.id, { type: 'number', ...update })}
            disabled={editable ? false : 'readonly'}
            mode={mode}
            align="end"
            displayPrecision={DISPLAY_PRECISION}
            renderExtra={writeErrorRenderer(setting.writeError)}
            defaultMeasurement={defaultMeasure(setting.defaultValue, measure, unitInfo)}
          />)
          .with({ type: 'network' }, ({ value }) => <div className="setting-network">
            <Text>{value ?? ''}</Text>
            <Icons.Edit title="Edit Setting" onClick={() => actions.setCustomDialog('network settings')}/>
          </div>)
          .with({ type: 'string' }, ({ value, mode }) => <TextInputEditor
            className="setting-editor"
            value={value ?? ''}
            onChange={update => actions.updateSetting(key, setting.id, { type: 'string', ...update })}
            disabled={editable ? false : 'readonly'}
            mode={mode}
            align="end"
            aria-label={setting.name}
            defaultValue={setting.defaultValue}
            renderExtra={writeErrorRenderer(setting.writeError)}
          />)
          .exhaustive()
        }

        {locked && (
          <PopUp triggerAction="hover" trigger={() => <Icons.Lock aria-label={`${setting.name} locked`}/>} className="locked">
            Set system.access to at least {setting.writeAccessLevel} to edit {setting.name}.
          </PopUp>
        )}

        <Icons.Refresh
          title="Refresh Setting"
          className={classNames({ [AnimationClasses.Rotation]: setting.mode === 'initialize' }, 'refresh')}
          disabled={setting.mode === 'initialize'}
          activated={setting.mode === 'initialize'}
          onClick={() => actions.readSetting(key, setting.id)}/>

        {environment.flavor === Flavors.Zaber && <ProtocolManualToggle
          deviceOrAxisKey={key}
          setting={setting.id}
          activated={showManual}
          onClick={() => setShowManual(!showManual)}
          className="protocol-manual-toggle"
        />}
      </div>
      {showManual && <div className="protocol-manual-container">
        <ProtocolManual deviceOrAxisKey={key} setting={setting.id}/>

        <DefaultValue setting={setting} entityKey={key}/>
      </div>}
      {setting.readError && <NoticeBanner className="setting-notice" title="Read Error">{setting.readError}</NoticeBanner>}
    </NoticeBorder>
  );
};

export const Setting = memo(SettingBase);
