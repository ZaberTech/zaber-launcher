import fs from 'fs';

import * as CSV from 'csv/sync';
import { select, put, call, race, take, delay, all, takeEvery, takeLatest } from 'redux-saga/effects';
import {
  Process,
  ProcessController,
  ProcessControllerMode as Mode,
  ProcessControllerSource as Source,
  ProcessControllerSourceSensor
} from '@zaber/motion/product';
import { AbsoluteTemperature, Voltage } from '@zaber/motion';
import { throwUnexpectedError } from '@zaber/toolbox';
import _ from 'lodash';
import { match } from 'ts-pattern';
import type { SagaIterator } from 'redux-saga';

import type { Action, RT } from '../../utils';
import { convertValueUnits, MeasurementOK, UnitInfoService } from '../../units';
import { environment } from '../../environment';
import { actionDefinitions as actions, ActionsToPayloads, ActionTypes } from '../actions';
import { selectAutoTune, selectAutoTuneData } from '../selectors';
import { hysteresisSetting } from '../settings';
import { getProcessController } from '../sagas';
import { makeAxisKey } from '../../keys';
import { getContainer } from '../../container';
import { Dialogs } from '../../dialogs';

import {
  AutoTuneError,
  AutoTuneIdentify,
  autoTuneSettings,
  AutoTuneUnitConvertersUnchecked,
  autoTuneUnitConvertersValid,
  InitialState,
} from './types';
import { MAX_DUTY_CYCLE, MIN_DUTY_CYCLE } from './constants';

export function* processControllerAutoTuneSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.BEGIN_AUTO_TUNE, beginAutoTune),
    takeLatest(ActionTypes.AUTO_TUNE_GO_TO_STEP, autoTuneGoToStep),
    takeEvery(ActionTypes.AUTO_TUNE_DOWNLOAD_IDENTIFICATION, downloadAutoTuneData),
    takeEvery(ActionTypes.AUTO_TUNE_RUN_TEST, autoTuneRunTest),
    takeEvery(ActionTypes.END_AUTO_TUNE, endAutoTune),
  ]);
}

const V = Voltage.VOLTS;

type GetSetting = RT<Process['settings']['get']>;
function* beginAutoTune({ payload: { entity, process: processNumber } }: Action<ActionsToPayloads[ActionTypes.BEGIN_AUTO_TUNE]>) {
  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController, entity);
    const process = controller.getProcess(processNumber);
    const mode: RT<typeof process.getMode> = yield call([process, process.getMode]);
    const source: RT<typeof process.getSource> = yield call([process, process.getSource]);
    const maxVoltage: GetSetting = yield call([process.settings, process.settings.get], 'process.control.voltage.max', V);
    const minVoltage: GetSetting = yield call([process.settings, process.settings.get], 'process.control.voltage.min', V);
    const voltageOn: GetSetting = yield call([process.settings, process.settings.get], 'process.voltage.on', V);
    const pulseVoltage: GetSetting = yield call([process.settings, process.settings.get], 'process.voltage.start', V);
    const pulseDuration: GetSetting = yield call([process.settings, process.settings.get], 'process.voltage.start.duration');
    const kp: GetSetting = yield call([process.settings, process.settings.get], 'process.pid.kp');
    const ki: GetSetting = yield call([process.settings, process.settings.get], 'process.pid.ki');
    const kd: GetSetting = yield call([process.settings, process.settings.get], 'process.pid.kd');

    const stableVoltage =
      voltageOn > maxVoltage ? maxVoltage :
      voltageOn < 0 ? 0 :
      voltageOn;

    for (const setting of autoTuneSettings) {
      const value: GetSetting = yield call([process.settings, process.settings.get], setting);
      yield put(actions.updateProcessSetting(processNumber, setting, { value, units: 'default', mode: 'displaying' }));
    }

    const processKey = makeAxisKey(entity, processNumber);
    const unitInfoService = getContainer().get(UnitInfoService);
    const converters: AutoTuneUnitConvertersUnchecked = {
      'process.control.voltage.max': yield call(
        [unitInfoService, unitInfoService.getUnitInfo], { entity: processKey, setting: 'process.control.voltage.max' }
      ),
      'process.voltage.on': yield call(
        [unitInfoService, unitInfoService.getUnitInfo], { entity: processKey, setting: 'process.voltage.on' }
      ),
      'process.control.hysteresis.temperature': yield call(
        [unitInfoService, unitInfoService.getUnitInfo], { entity: processKey, setting: 'process.control.hysteresis.temperature' }
      ),
      'process.control.hysteresis.voltage': yield call(
        [unitInfoService, unitInfoService.getUnitInfo], { entity: processKey, setting: 'process.control.hysteresis.voltage' }
      ),
    };

    const initial: InitialState = { mode, source, voltageOn, kp, ki, kd, maxVoltage, minVoltage, pulseVoltage, pulseDuration };
    if (!autoTuneUnitConvertersValid(converters)) {
      yield put(actions.endAutoTune(entity, processNumber, initial, true));
      yield put(actions.setProcessError(processNumber, 'Could not initialize Auto Tune.', 'Fetching unit conversions failed.'));
    } else {
      yield call([process.settings, process.settings.set], 'process.voltage.start', 0);
      yield call([process.settings, process.settings.set], 'process.voltage.start.duration', 0);
      yield put(actions.autoTuneGoToStep({
        step: 'setup',
        entity,
        process: processNumber,
        initial,
        readings: [],
        converters,
        maxVoltage: { value: maxVoltage, units: V },
        stableVoltage: { value: stableVoltage, units: V },
        criticalFrequency: null,
        criticalGain: null,
      }));
    }
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setProcessError(processNumber, `There was an error initializing auto tuning ${processNumber}`, err.message));
  }
}

function* autoTuneGoToStep({ payload: { tune } }: Action<ActionsToPayloads[ActionTypes.AUTO_TUNE_GO_TO_STEP]>) {
  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController);
    const process = controller.getProcess(tune.process);
    yield call([process, process.off]);
    yield call([process, process.setMode], Mode.MANUAL);
    yield put(actions.updateProcessSetting(tune.process, 'process.voltage.on', {
      ...tune.stableVoltage,
      mode: 'writing',
      preEditMeasure: tune.stableVoltage,
    }));
    if (tune.step === 'setup') {
      yield call([process, process.on]);
    }
    yield put(actions.autoTuneStepReady(tune));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.autoTuneError({ type: 'error', message: `Failed to initialize the ${tune.step} step`, details: err.message }));
  }
}

async function getControllerInput(controller: ProcessController, source: Source): Promise<MeasurementOK> {
  return await match<Source, Promise<MeasurementOK>>(source)
    .with({ sensor: ProcessControllerSourceSensor.ANALOG_INPUT }, async ({ port }) => ({
      value: await controller.device.io.getAnalogInput(port),
      units: Voltage.VOLTS,
    }))
    .otherwise(async ({ port }) => ({
      value: await controller.device.settings.get(`sensor.temperature.${port}`),
      units: AbsoluteTemperature.DEGREES_CELSIUS,
    }));
}

/** If the auto tune modal is open, run this loop until it closes */
export function* autoTuneLoop(controller: ProcessController, processNumber: number) {
  const process = controller.getProcess(processNumber);
  const source: RT<typeof process.getSource> = yield call([process, process.getSource]);
  for (;;) {
    const tune: RT<typeof selectAutoTune> = yield select(selectAutoTune);
    if (tune == null) {
      return;
    }
    if (tune.step === 'identify') {
      if (tune.run !== null) {
        yield race([
          call(autoTuneRunIdentification, process, tune, tune.run.hysteresis, source),
          take(ActionTypes.AUTO_TUNE_CANCEL_IDENTIFICATION),
          take(ActionTypes.END_AUTO_TUNE)
        ]);
      }
    } else {
      const measured: RT<typeof getControllerInput> = yield call(getControllerInput, controller, source);
      const volts: RT<typeof process.settings.get> = yield call([process.settings, process.settings.get], 'process.voltage');
      const state: RT<typeof process.settings.get> = yield call([process.settings, process.settings.get], 'process.state');
      yield put(actions.updateAutoTune(performance.now(), measured, volts, state !== 0));
    }
    yield delay(100);
  }
}

/** number of cycles will be ignored from the first cycle, it is done to remove inaccuracy caused by the transient response */
const AUTOTUNE_IGNORE_FIRST_CYCLES = 1;
/**
 * The number of cycles will be taken into consideration for identification. Though theoretically 1 cycle is enough,
 * more cycles are averaged to reduce the uncertainty and inaccuracy of sensor measurement. */
const AUTOTUNE_N_CYCLES = 3;
/** The total number of cycles, ignored or not. */
const TOTAL_CYCLES = AUTOTUNE_IGNORE_FIRST_CYCLES + AUTOTUNE_N_CYCLES;

const TOTAL_STEPS = 2 * (AUTOTUNE_IGNORE_FIRST_CYCLES + AUTOTUNE_N_CYCLES) + 1;
function calculateProgress(cooled: boolean, iteration = 0, heating = true) {
  const stepsDone = iteration * 2 + (cooled ? 1 : 0) + (heating ? 0 : 1);
  return stepsDone / TOTAL_STEPS;
}

function* runUntil(predicate: (input: number) => boolean, process: Process, source: Source, start: number) {
  for (;;) {
    const measured: RT<typeof getControllerInput> = yield call(getControllerInput, process.controller, source);
    const volts: RT<typeof process.settings.get> = yield call([process.settings, process.settings.get], 'process.voltage');
    yield put(actions.updateAutoTune(performance.now() - start, measured, volts, true));
    if (predicate(measured.value)) { return }
    yield delay(100);
  }
}

/**
 * Calculates the average duty cycle of on-off control at identification step and checks if it is good based on
 * percent deviation from perfect 50% duty cycle. Throws if it is deviated more than defined limits.
 * @returns The average period time in seconds
 */
function verifyIdentification(processOnTimes: number[], processOffTimes:  number[]): number | AutoTuneError {
  if (processOnTimes.length !== TOTAL_CYCLES + 1) {
    return { type: 'error', message: `Expected ${TOTAL_CYCLES + 1} process on times. Received ${processOnTimes.length}.` };
  }
  if (processOffTimes.length !== TOTAL_CYCLES) {
    return { type: 'error', message: `Expected ${TOTAL_CYCLES} process off times. Received ${processOffTimes.length}.` };
  }
  const periods = _.range(AUTOTUNE_IGNORE_FIRST_CYCLES, TOTAL_CYCLES).map(i => processOnTimes[i + 1] - processOnTimes[i]);
  const processOnDurations = _.range(AUTOTUNE_IGNORE_FIRST_CYCLES, TOTAL_CYCLES).map(i => processOffTimes[i] - processOnTimes[i]);
  const averagePeriod = _.mean(periods);
  const averageOnDuration = _.mean(processOnDurations);
  const averageDutyCyclePercent = averageOnDuration / averagePeriod * 100;
  const validDutyCycle = averageDutyCyclePercent < MAX_DUTY_CYCLE && averageDutyCyclePercent > MIN_DUTY_CYCLE;
  if (validDutyCycle || environment.isTest) {
    return averagePeriod / 1000;
  } else {
    return { type: 'invalid-duty-cycle', dutyCycle: averageDutyCyclePercent };
  }
}

type Peak = { time: number; value: number };

/** finds a maximum or minimum peak in data between a start and end time */
function findPeaks(data: AutoTuneIdentify['readings'], ranges: [number, number][], type: 'max' | 'min'): Peak[] | AutoTuneError {
  const peaks: Peak[] = [];
  for (const [from, to] of ranges) {
    const window = data.filter(d => d.time > from && d.time < to);
    const peak = type === 'max' ? _.maxBy(window, 'measured') : _.minBy(window, 'measured');
    if (peak == null) {
      return { type: 'error', message: `Could not find a ${type} peak between ${from} and ${to}` };
    }
    const peakTimes = window.filter(d => d.measured === peak.measured).map(d => d.time);
    peaks.push({ time: _.mean(peakTimes), value: peak.measured });
  }

  return peaks;
}

function* autoTuneRunIdentification(process: Process, tune: AutoTuneIdentify, hysteresisMeasure: MeasurementOK, source: Source) {
  const { value: hysteresisValue, units: hysteresisUnits } = hysteresisMeasure;
  const hysteresisUnitInfo = tune.converters[hysteresisSetting(tune.initial.source)];
  const hysteresis = convertValueUnits(hysteresisUnitInfo, hysteresisValue, hysteresisUnits, hysteresisUnitInfo.default);

  const maxVoltage = convertValueUnits(tune.converters['process.control.voltage.max'], tune.maxVoltage.value, tune.maxVoltage.units, V);
  const stableVoltage = convertValueUnits(tune.converters['process.voltage.on'], tune.stableVoltage.value, tune.stableVoltage.units, V);

  const isHeater = tune.initial.mode === Mode.PID_HEATER;
  const setpoint = tune.setpoint.value;

  const processInMin = 0;
  const processInMax = isHeater ? maxVoltage ** 2 : maxVoltage;
  const processInStable = isHeater ? stableVoltage ** 2 : stableVoltage;

  // Set the limits as far as possible to either side of `processInStable` without exceeding either `processInMin` or `processInMax`
  const closerToMin = (processInStable - processInMin) < (processInMax - processInStable);
  const processInLowerLimit = closerToMin ? processInMin : processInMax - 2 * (processInMax - processInStable);
  const processInUpperLimit = closerToMin ? (processInStable - processInMin) * 2 + processInMin : processInMax;

  const voltageLowerLimit = isHeater ? Math.sqrt(processInLowerLimit) : processInLowerLimit;
  const voltageUpperLimit = isHeater ? Math.sqrt(processInUpperLimit) : processInUpperLimit;

  const startInit = performance.now();

  yield call([process.settings, process.settings.set], 'process.voltage.on', voltageLowerLimit);
  yield call([process, process.on]);
  const measured: RT<typeof getControllerInput> = yield call(getControllerInput, process.controller, source);
  if (measured.value < setpoint) {
    yield call([process.settings, process.settings.set], 'process.voltage.on', voltageUpperLimit);
    yield put(actions.autoTuneSetIdentifyProgress(calculateProgress(false), 'Preparing to identify', voltageUpperLimit));
    yield call(runUntil, input => input > setpoint, process, source, startInit);
  }
  yield call([process.settings, process.settings.set], 'process.voltage.on', voltageLowerLimit);
  yield put(actions.autoTuneSetIdentifyProgress(calculateProgress(false), 'Preparing to identify', voltageLowerLimit));
  yield call(runUntil, input => input < setpoint - hysteresis, process, source, startInit);
  yield put(actions.autoTuneSetIdentifyProgress(calculateProgress(true), 'System ready for identification.', 0, true));

  const startIdentify = performance.now();

  /** The timestamps when the algorithm switches to a high voltage state */
  const switchToHigh: number[] = [];
  /** The timestamps when the algorithm switches to a low voltage state */
  const switchToLow: number[] = [];
  for (let i = 0; i < TOTAL_CYCLES; i++) {
    // Heating cycle
    switchToHigh[i] = performance.now() - startIdentify;
    yield call([process.settings, process.settings.set], 'process.voltage.on', voltageUpperLimit);
    yield put(actions.autoTuneSetIdentifyProgress(calculateProgress(true, i), 'System Heating', voltageUpperLimit));
    yield call(runUntil, input => input > setpoint + hysteresis, process, source, startIdentify);

    // Cooling cycle
    switchToLow[i] = performance.now() - startIdentify;
    yield call([process.settings, process.settings.set], 'process.voltage.on', voltageLowerLimit);
    yield put(actions.autoTuneSetIdentifyProgress(calculateProgress(true, i, false), 'System Cooling', voltageLowerLimit));
    yield call(runUntil, input => input < setpoint - hysteresis, process, source, startIdentify);
  }
  switchToHigh[TOTAL_CYCLES] = performance.now() - startIdentify;

  const averagePeriod = verifyIdentification(switchToHigh, switchToLow);
  if (typeof averagePeriod === 'object') {
    yield put(actions.autoTuneError(averagePeriod));
    return;
  }

  const data: RT<typeof selectAutoTuneData> = yield select(selectAutoTuneData);
  const identifyingCycles = _.range(AUTOTUNE_IGNORE_FIRST_CYCLES, TOTAL_CYCLES);
  const minPeaks = findPeaks(data, identifyingCycles.map(i => [switchToHigh[i], switchToLow[i]]), 'min');
  if (!Array.isArray(minPeaks)) {
    yield put(actions.autoTuneError(minPeaks));
    return;
  }
  const maxPeaks = findPeaks(data, identifyingCycles.map(i => [switchToLow[i], switchToHigh[i + 1]]), 'max');
  if (!Array.isArray(maxPeaks)) {
    yield put(actions.autoTuneError(maxPeaks));
    return;
  }
  const averagePeakToPeak = _.mean(_.range(0, AUTOTUNE_N_CYCLES).map(i => maxPeaks[i].value - minPeaks[i].value));

  const criticalFrequency = 1 / averagePeriod;
  const criticalGain = (4 * (voltageUpperLimit - voltageLowerLimit) ** 2) / (Math.PI * averagePeakToPeak);

  yield put(actions.autoTuneCompleteIdentification(criticalGain, criticalFrequency));
  yield call([process, process.off]);
}

function* downloadAutoTuneData({ payload: { data } }: Action<ActionsToPayloads[ActionTypes.AUTO_TUNE_DOWNLOAD_IDENTIFICATION]>) {
  try {
    const csvString = CSV.stringify(data, { header: true });
    const saveLocation: RT<typeof Dialogs.showSaveDialog> = yield call(Dialogs.showSaveDialog, {
      title: 'Exporting Auto Tune Identification Data',
      defaultPath: 'data.csv',
    });
    if (saveLocation.canceled) {
      return;
    }
    if (saveLocation.filePath == null) {
      yield put(actions.autoTuneError({ type: 'error', message: 'Invalid location to save a file to.' }));
      return;
    }

    yield call(fs.promises.writeFile, saveLocation.filePath, csvString);
  } catch (e) {
    throwUnexpectedError(e);
    yield put(actions.autoTuneError({ type: 'error', message: 'Failed to get save your data to a CSV', details: e.message }));
  }
}

function* autoTuneRunTest({ payload: { tune, run } }: Action<ActionsToPayloads[ActionTypes.AUTO_TUNE_RUN_TEST]>) {
  const controller: RT<typeof getProcessController> = yield call(getProcessController, tune.entity);
  const process = controller.getProcess(tune.process);

  yield call([process, process.setMode], tune.initial.mode);
  yield call([process.settings, process.settings.set], 'process.pid.kp', run.kp);
  yield call([process.settings, process.settings.set], 'process.pid.ki', run.ki);
  yield call([process.settings, process.settings.set], 'process.pid.kd', run.kd);
  yield call([process.settings, process.settings.set], 'process.control.voltage.max', tune.maxVoltage.value, tune.maxVoltage.units);
  yield call([process.settings, process.settings.set], 'process.control.voltage.min', 0, Voltage.VOLTS);
  yield call([process, process.on]);
}

function* endAutoTune({ payload }: Action<ActionsToPayloads[ActionTypes.END_AUTO_TUNE]>) {
  const { entity, process: processNumber, initial, cancel } = payload;
  try {
    const controller: RT<typeof getProcessController> = yield call(getProcessController, entity);
    const process = controller.getProcess(processNumber);
    yield call([process.settings, process.settings.set], 'process.voltage.on', initial.voltageOn);
    if (cancel) {
      yield call([process, process.off]);
      yield call([process, process.setMode], initial.mode);
      yield call([process.settings, process.settings.set], 'process.control.voltage.max', initial.maxVoltage);
      yield call([process.settings, process.settings.set], 'process.control.voltage.min', initial.minVoltage);
      yield call([process.settings, process.settings.set], 'process.voltage.start', initial.pulseVoltage);
      yield call([process.settings, process.settings.set], 'process.voltage.start.duration', initial.pulseDuration);
      yield call([process.settings, process.settings.set], 'process.pid.kp', initial.kp);
      yield call([process.settings, process.settings.set], 'process.pid.ki', initial.ki);
      yield call([process.settings, process.settings.set], 'process.pid.kd', initial.kd);
    }

    yield put(actions.refreshProcessInfo(processNumber));
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.setProcessError(
      processNumber,
      `There was an error reverting process ${processNumber} to its initial state.`,
      err.message
    ));
  }
}
