import type { SagaIterator } from 'redux-saga';
import { all, takeEvery, fork, delay, put } from 'redux-saga/effects';
import { notNil } from '@zaber/toolbox';

import { getContainer } from '../container';
import type { Action } from '../utils';
import { environment } from '../environment';

import { IPC } from './ipc';
import { ActionTypes, ActionsToPayloads } from './actions';

export function* ipcSaga(): SagaIterator {
  yield all([
    takeEvery(ActionTypes.IPC_BROADCAST, broadcast),
    !environment.isTest ? fork(init) : null,
  ].filter(notNil));
}

function* init(): SagaIterator {
  yield delay(0); // to prevent cycle in initialization
  getContainer().get(IPC);
}

function* broadcast({
  payload: { actionToBroadcast, dispatchLocally }
}: Action<ActionsToPayloads[ActionTypes.IPC_BROADCAST]>): SagaIterator {
  const ipc = getContainer().get(IPC);
  ipc.broadcastAction(actionToBroadcast);
  if (dispatchLocally) {
    yield put(actionToBroadcast);
  }
}
