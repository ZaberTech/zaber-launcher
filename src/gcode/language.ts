import { Colors } from '@zaber/react-library';
import * as monaco from 'monaco-editor';

const languageConfig: monaco.languages.LanguageConfiguration = {
  comments: {
    blockComment: ['(', ')'],
    lineComment: ';',
  },
  brackets: [
    ['{', '}'],
    ['[',  ']'],
    ['(',  ')'],
  ],
  autoClosingPairs: [
    { open: '(', close: ')' },
    { open: '[', close: ']' },
    { open: '{', close: '}' },
    { open: '"', close: '"' },
    { open: '\'', close: '\'' },
  ],
  surroundingPairs: [
    { open: '(', close: ')' },
    { open: '[', close: ']' },
    { open: '{', close: '}' },
    { open: '"', close: '"' },
    { open: '\'', close: '\'' },
  ]
};


monaco.languages.register({
  id: 'gcode',
});
monaco.languages.setLanguageConfiguration('gcode', languageConfig);

monaco.languages.setMonarchTokensProvider('gcode', {
  tokenizer: {
    root: [
      [/G\d+/, 'gcode'],
      [/M\d+/, 'mcode'],
      [/[0-9.-]+/, 'value'],
      [/;.*/, 'comment'],
      [/\([^)]*\)/, 'comment'],
      [/[XYZABCEFSP]/, 'parameter'],
    ],
  },
  ignoreCase: true,
});

monaco.editor.defineTheme('zaber-gcode', {
  base: 'vs',
  inherit: false,
  rules: [
    { token: 'mcode', foreground: '2121fa' },
    { token: 'gcode', foreground: 'e525df' },
    { token: 'comment', foreground: '008000' },
    { token: 'value', foreground: 'a3152b' },
    { token: 'parameter', foreground: '000000' },
    { token: 'error', foreground: 'f14c4c' },
  ],
  colors: {
    'editor.background': Colors.zaberWhite,
  },
});
