import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';

export interface State {
  currentVersion: string;
  checkingNewVersion: boolean;
  checkVersionError: string | null;
  newVersion: { version: string; canUpdate: boolean } | null;
  showNewVersionInstalled: boolean;
  showNewVersionAvailable: boolean;
  waitingToQuit: boolean;
}

const initialState: State = {
  currentVersion: '',
  checkingNewVersion: false,
  checkVersionError: null,
  newVersion: null,
  showNewVersionAvailable: false,
  showNewVersionInstalled: false,
  waitingToQuit: false,
};

const newVersionReady = (state: State, { version, canUpdate }: ActionsToPayloads[ActionTypes.NEW_VERSION_READY]): State =>
  ({
    ...state,
    newVersion: { version, canUpdate },
    showNewVersionAvailable: canUpdate,
  });

const versionCheckDone = (state: State, { error }: ActionsToPayloads[ActionTypes.VERSION_CHECK_DONE]): State =>
  ({
    ...state,
    checkingNewVersion: false,
    checkVersionError: error ?? null,
  });

const checkNewVersion = (state: State): State =>
  ({
    ...state,
    checkingNewVersion: true,
    checkVersionError: null,
  });

const ignoreUpdate = (state: State): State => ({ ...state, showNewVersionAvailable: false });

const setCurrentVersion = (state: State, { version, newVersionInstalled }: ActionsToPayloads[ActionTypes.SET_CURRENT_VERSION]): State =>
  ({
    ...state,
    currentVersion: version,
    showNewVersionInstalled: newVersionInstalled,
  });

const appUpdatedNotificationClose = (state: State): State => ({ ...state, showNewVersionInstalled: false });

const quitAndInstall = (state: State): State => ({ ...state, waitingToQuit: true });

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.NEW_VERSION_READY]: newVersionReady,
  [ActionTypes.IGNORE_UPDATE]: ignoreUpdate,
  [ActionTypes.CHECK_NEW_VERSION]: checkNewVersion,
  [ActionTypes.VERSION_CHECK_DONE]: versionCheckDone,
  [ActionTypes.SET_CURRENT_VERSION]: setCurrentVersion,
  [ActionTypes.APP_UPDATED_NOTIFICATION_CLOSE]: appUpdatedNotificationClose,
  [ActionTypes.QUIT_AND_INSTALL]: quitAndInstall,
}, initialState);
