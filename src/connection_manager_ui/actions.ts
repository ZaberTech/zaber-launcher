import type { EntityKey } from '../keys';
import { actionBuilder } from '../utils';

export enum ActionTypes {
  SET_LABEL_OPEN = 'CM2_SET_LABEL_OPEN',
  SET_LABEL_CLOSE = 'CM2_SET_LABEL_CLOSE',
  SET_LABEL_SET_LABEL = 'CM2_SET_LABEL_SET_LABEL',
  SET_LABEL_WRITE = 'CM2_SET_LABEL_WRITE',
  SET_LABEL_WRITE_DONE = 'CM2_SET_LABEL_WRITE_DONE',
  SET_CONNECTION_NAME = 'CM2_SET_CONNECTION_NAME',
  SET_CONNECTION_NAME_DONE = 'CM2_SET_CONNECTION_NAME_DONE',
  GET_DEVICE_SYSTEM_ERRORS = 'CM2_GET_DEVICE_SYSTEM_ERRORS',
  SET_DEVICE_SYSTEM_ERRORS = 'CM2_SET_DEVICE_SYSTEM_ERRORS',
}

export interface ActionsToPayloads {
  [ActionTypes.SET_LABEL_OPEN]: { entityKey: EntityKey; label: string };
  [ActionTypes.SET_LABEL_CLOSE]: void;
  [ActionTypes.SET_LABEL_SET_LABEL]: { label: string };
  [ActionTypes.SET_LABEL_WRITE]: void;
  [ActionTypes.SET_LABEL_WRITE_DONE]: { error?: string };
  [ActionTypes.SET_CONNECTION_NAME]: { connectionKey: EntityKey; name: string };
  [ActionTypes.SET_CONNECTION_NAME_DONE]: { connectionKey: EntityKey; err?: string };
  [ActionTypes.GET_DEVICE_SYSTEM_ERRORS]: { deviceKey: EntityKey };
  [ActionTypes.SET_DEVICE_SYSTEM_ERRORS]: { deviceKey: EntityKey; errors: string[] };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  setLabelOpen: (entityKey: EntityKey, label: string) => buildAction(ActionTypes.SET_LABEL_OPEN, { entityKey, label }),
  setLabelClose: () => buildAction(ActionTypes.SET_LABEL_CLOSE),
  setLabelSetLabel: (label: string) => buildAction(ActionTypes.SET_LABEL_SET_LABEL, { label }),
  setLabelWrite: () => buildAction(ActionTypes.SET_LABEL_WRITE),
  setLabelWriteDone: (error?: string) => buildAction(ActionTypes.SET_LABEL_WRITE_DONE, { error }),
  setConnectionName: (connectionKey: EntityKey, name: string) =>
    buildAction(ActionTypes.SET_CONNECTION_NAME, { connectionKey, name }),
  setConnectionNameDone: (connectionKey: EntityKey, err?: string) =>
    buildAction(ActionTypes.SET_CONNECTION_NAME_DONE, { connectionKey, err }),
  getDeviceSystemErrors: (deviceKey: EntityKey) => buildAction(ActionTypes.GET_DEVICE_SYSTEM_ERRORS, { deviceKey }),
  setDeviceSystemErrors: (deviceKey: EntityKey, errors: string[]) =>
    buildAction(ActionTypes.SET_DEVICE_SYSTEM_ERRORS, { deviceKey, errors }),
};
