import { Angle, Units } from '@zaber/motion';
import _ from 'lodash';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { InputLabel } from '@zaber/react-library';

import { convertValueUnits, getDimensionName, getUnits, InputWithUnits, Measurement, MeasurementOK, measurementOK } from '../../../units';
import type { RT } from '../../../utils';
import { RootState, selectUnits } from '../../../store';
import { actionDefinitions } from '../actions';
import type { AxisMovementState, PreciseMovementValues } from '../types';
import type { IdentifiedAxisState } from '../../../connection_manager';

interface Limits {
  min: number;
  max: number;
}

interface Props {
  axis: IdentifiedAxisState;
  state: AxisMovementState;
  setting: string;
  label: InputLabel.Content;
  /**
   * The controls that will be placed to the right of this input to control it.
   * They are passed the value and units to move to, or null if the input value is invalid
   * */
  controls: (target: MeasurementOK | null) => React.ReactNode;
  onEnterPress?: (target: MeasurementOK) => void;
  limits?: Limits | null;
  defaultValue?: number;
  movementType: keyof PreciseMovementValues;
  title: string;
}

interface StateProps {
  actions: typeof actionDefinitions;
  unitInfo: RT<typeof selectUnits>;
}

class BasePreciseMovementControl extends Component<Props & StateProps> {
  componentDidUpdate(prevProps: Readonly<Props & StateProps>): void {
    const { actions, axis, movementType, defaultValue } = this.props;
    if (defaultValue !== prevProps.defaultValue) {
      actions.setPreciseMovement(axis.key, movementType, null);
    }
  }

  getMeasurement(): Measurement {
    const { movementType, defaultValue, state } = this.props;
    const measurement = state.preciseMovement[movementType];
    if (measurement === null) {
      return { value: defaultValue ?? 0, units: 'default' };
    }

    return measurement;
  }

  getDisplayPrecision() {
    return this.props.axis.movementDimensions?.position === getDimensionName(Angle.DEGREES) ? 6 : 3;
  }

  checkOutOfRange() {
    const { limits } = this.props;
    const precision = this.getDisplayPrecision();
    const valueAndUnits = this.getMeasurement();
    const value = valueAndUnits.value;
    const units = getUnits(valueAndUnits);
    const unitInfo = this.props.unitInfo.info[this.props.axis.key]?.settings[this.props.setting];
    if (value == null || limits?.min == null || limits?.max == null || !unitInfo?.supported) {
      return null;
    }
    const limitsInUnit = {
      min: convertValueUnits(unitInfo, limits.min, Units.NATIVE, units),
      max: convertValueUnits(unitInfo, limits.max, Units.NATIVE, units),
    };
    limitsInUnit.min = _.round(limitsInUnit.min, precision);
    limitsInUnit.max = _.round(limitsInUnit.max, precision);

    const valueRounded = _.round(value, precision);
    const isOut = valueRounded < limitsInUnit.min || limitsInUnit.max < valueRounded;
    return {
      isOut,
      ...limitsInUnit,
    };
  }

  onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    const valueAndUnits = this.getMeasurement();
    if (e.key === 'Enter' && this.props.onEnterPress && measurementOK(valueAndUnits)) {
      this.props.onEnterPress(valueAndUnits);
    }
  };

  render(): React.ReactNode {
    const {
      actions,
      setting,
      label,
      controls,
      movementType,
      title,
      axis,
    } = this.props;
    const { key } = axis;
    const bounds = this.checkOutOfRange();

    const valueAndUnits = this.getMeasurement();
    const target = measurementOK(valueAndUnits) ? valueAndUnits : null;

    return (<>
      <InputWithUnits
        title={title}
        className="precise-control"
        unitFrom={{ setting, entity: key }}
        measure={valueAndUnits}
        onChange={valueAndUnits => actions.setPreciseMovement(key, movementType, valueAndUnits)}
        onKeyDown={this.onKeyDown}
        displayPrecision={this.getDisplayPrecision()}
        labelContent={label}
        status={bounds?.isOut ? 'error' : null}
        message={bounds?.isOut ? `Value must be between ${bounds.min} and ${bounds.max}.` : undefined}
      />
      <div className="controls">{controls(target)}</div>
    </>
    );
  }
}

export const PreciseMovementControl = connect(
  (state: RootState): Omit<StateProps, 'actions'> => ({
    unitInfo: selectUnits(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionDefinitions, dispatch),
  }),
)(BasePreciseMovementControl);
