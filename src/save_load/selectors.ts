import { createSelector } from 'reselect';
import { match, P } from 'ts-pattern';

import { selectSaveLoad } from '../store';
import { selectDevicesWithAxes, selectIdentifiedAxes } from '../connection_manager/selectors';
import { isIdentified, deviceNameWithLabel } from '../connection_manager';
import { EntityKeyType, tryExtractDeviceKey, getEntityType } from '../keys';
import { tryAccess } from '../utils';

import type { SaveLoadTargetMaybe } from './types';

export const selectEntity = createSelector(selectSaveLoad, state => state.entity);

export const selectTarget = createSelector(
  selectEntity, selectDevicesWithAxes, selectIdentifiedAxes, (productKey, devices, axes) =>
    match<EntityKeyType | null, SaveLoadTargetMaybe | null>(getEntityType(productKey))
      .with(P.nullish, () => null)
      .with(EntityKeyType.DEVICE, (): SaveLoadTargetMaybe => {
        const device = tryAccess(devices, productKey);
        if (!isIdentified(device)) {
          return { type: 'unidentified' };
        } else {
          return {
            type: device.isController ? 'controller' : 'device',
            name: deviceNameWithLabel(device),
            description: device.isController ? 'controller and peripherals' : 'device',
            key: device.key,
            serialNumber: device.identity.serialNumber,
            address: device.address,
            deviceProductId: device.product.id,
            axes: device.axes,
          };
        }
      })
      .with(EntityKeyType.AXIS, (): SaveLoadTargetMaybe => {
        const device = tryAccess(devices, tryExtractDeviceKey(productKey));
        const axis = tryAccess(axes, productKey);
        if (axis == null || !isIdentified(device)) {
          return { type: 'unidentified' };
        } else {
          return {
            type: 'axis',
            name: axis.identity.peripheralName,
            description: 'axis',
            key: axis.key,
            serialNumber: axis.serialNumber ?? 0,
            axisNumber: axis.axisNumber,
            deviceProductId: device.product.id,
            peripheralId: axis.identity.peripheralId,
          };
        }
      })
      .otherwise(() => ({ type: 'not-a-device' }))
);
