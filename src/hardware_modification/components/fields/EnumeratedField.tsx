import React from 'react';
import { reactSelect, Colors } from '@zaber/react-library';
import Select from 'react-select';

import type { EnumProtocolDataOption } from '../../types';

import type { HwModInputProps } from './HwModField';

interface Props extends HwModInputProps {
  options: EnumProtocolDataOption[];
}

export const HwModEnumInput: React.FC<Props> = ({ value, status, disabled, testid, onUpdate, options }) => {
  const selectedOption = options.find(opt => opt.value === value);

  let maxOptionLen = 0;
  for (const option of options) {
    maxOptionLen = Math.max(maxOptionLen, option.label.length);
  }
  const width = `${4 + maxOptionLen * 0.45}rem`;

  const styles = reactSelect.getDefaultStyles<EnumProtocolDataOption>({
    control: base => ({
      ...base,
      'width': '9rem',
      'borderColor': status === 'changed' ? Colors.blue : base.borderColor,
      '&:hover': { borderColor: status === 'changed' ? Colors.darkBlue : Colors.zaberGrey }
    }),
    menu: base => ({
      ...base,
      width,
      // 16rem (title) + 2rem (padding) + 9rem (input) = 27rem (that can be relied on)
      maxWidth: '27rem',
      right: '0',
      cursor: 'pointer',
    }),
  });

  return (<Select<EnumProtocolDataOption>
    value={selectedOption}
    options={options}
    formatOptionLabel={option => `${option.value}: ${option.label}`}
    styles={styles}
    isDisabled={disabled}
    onChange={val => val && onUpdate(val.value)}
    menuPlacement="auto"
    data-testid={`${testid}-input`}
  />);
};
