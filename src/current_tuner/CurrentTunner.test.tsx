import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import _ from 'lodash';
import {
  CommandFailedException, ascii, Resistance, Inductance, CurrentControllerProportionalGain, NoValueForKeyException,
  CommandFailedExceptionData,
} from '@zaber/motion';

import { connectionViewMockInstance } from '../connection_manager/connection_view/mocks';
import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore, wrapWithRouter, mockStorage, StorageMock } from '../test';
import { selectAxes, selectDevices } from '../connection_manager';
import { MessageRoutersService } from '../message_router';
import {
  mockSingleDeviceWithPeripherals, mockSingleDevice
} from '../connection_manager/mocks';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';

import { CurrentTuner } from './CurrentTuner';
import { CurrentTunerAPI } from './components/service';
import { MotorTypes, Setting, Sliders, TuningInfo, UNUSED_AXIS_PERIPHERAL_ID } from './types';
import { CURRENT_TUNER_WARNING_STORAGE_KEY } from './sagas';

const firmwareVersion = expect.objectContaining({ major: expect.any(Number), minor: expect.any(Number), build: expect.any(Number) });
const defaultSliderPositions = {
  backEMFCompensation: 0.5,
  responseCorrectionSpeed: 0.6,
  overallGain: 0.7,
};

const VALID_MOTOR_RESISTANCE = 101;
const VALID_MOTOR_INDUCTANCE = 102;

function getSettingValue(settings: Setting[], name: string): number | undefined {
  return settings.find(setting => setting.name === name)?.value;
}

let container: Container;
let storageMock: StorageMock;

let axes: AxisMock[];
let axisCallbacks: ((axis: AxisMock) => void)[] = [];
let inputSettings: Setting[];

class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);

    axes.push(this);
    axisCallbacks.forEach(callback => callback(this));
  }
  _flags = [] as string[];
  warnings = {
    getFlags: jest.fn(async () => new Set(this._flags)),
  };
  _settings: Record<string, number> = {
    'motor.resistance': getSettingValue(inputSettings, 'motor.resistance') ?? 0,
    'motor.inductance': getSettingValue(inputSettings, 'motor.inductance') ?? 0,
    'motor.ke': getSettingValue(inputSettings, 'motor.ke') ?? 0,
    'motor.type': MotorTypes.StepperMotor,
    'ictrl.pi.kp': 21.111,
    'ictrl.pi.ki': 22.222,
    'ictrl.afcff.ki': 23.333,
    'driver.enabled': 1,
  };
  settings = {
    get: jest.fn(async (setting: string) => {
      if (typeof this._settings[setting] !== 'number') {
        throw new CommandFailedException('BADCOMMAND', null!);
      }
      return this._settings[setting];
    }),
    set: jest.fn(async (setting: string, value: number) => {
      this._settings[setting] = value;
    }),
  };
  peripheralId = 1;
  genericCommand = jest.fn(async (command: string) => {
    const parts = command.split(' ');
    if (parts[0] === 'get') {
      const value = await this.settings.get(parts[1]);
      return { data: value.toString() };
    } else if (parts[0] === 'set') {
      await this.settings.set(parts[1], +parts[2]);
      return { data: '0' };
    }
    return { data: '0' };
  });
  storage = {
    getNumber: jest.fn(async () => 456),
    getBool: jest.fn(async () => true),
  };
  driverEnable = jest.fn().mockImplementation(async () => {
    await this.settings.set('driver.enabled', 1);
  });
  driverDisable = jest.fn().mockImplementation(async () => {
    await this.settings.set('driver.enabled', 0);
  });
}

class DeviceMock extends DeviceMockBase<AxisMock> {
  _settings: Record<string, number> = {
    'system.access': 1,
  };
  settings = {
    get: jest.fn(async (setting: string) => {
      if (typeof this._settings[setting] !== 'number') {
        throw new CommandFailedException('BADCOMMAND', null!);
      }
      return this._settings[setting];
    }),
    set: jest.fn(async (setting: string, value: number) => {
      this._settings[setting] = value;
    }),
  };
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

let currentTunerAPIMock: CurrentTunerAPIMock;
@injectable()
class CurrentTunerAPIMock {
  getCurrentControllerParams = jest.fn<Promise<Setting[]>, []>(async () => ([
    { name: 'ictrl.pi.kp', value: 1.23 },
    { name: 'ictrl.pi.ki', value: 1.24 },
    { name: 'ictrl.afcff.ki', value: 1.25 },
  ]));
  getSliderPositions = jest.fn<Promise<Sliders | null>, []>(async () => ({
    backEMFCompensation: 0.1,
    responseCorrectionSpeed: 0.2,
    overallGain: 0.3,
  }));
  getInfo = jest.fn<Promise<TuningInfo>, []>(async () => ({
    defaultSliderPositions: _.cloneDeep(defaultSliderPositions),
    slidersRange: {
      backEMFCompensation: { min: 0, max: 1, step: 0.1 },
      responseCorrectionSpeed: { min: 0, max: 1, step: 0.1 },
      overallGain: { min: 0, max: 1, step: 0.1 },
    },
    settings: [
      { name: 'ictrl.pi.kp', units: CurrentControllerProportionalGain['V/A'] },
      { name: 'ictrl.pi.ki' },
      { name: 'ictrl.afcff.ki' },
    ],
    inputSettings: [
      { name: 'motor.resistance', units: Resistance.OHMS },
      { name: 'motor.inductance', units: Inductance.H },
      { name: 'motor.ke' },
    ]
  }));
}

const TestCurrentTuner = wrapWithNewStore(wrapWithRouter(CurrentTuner));

let wrapper: RenderResult;

beforeEach(() => {
  axes = [];

  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.bind<unknown>(CurrentTunerAPI).to(CurrentTunerAPIMock);
  currentTunerAPIMock = container.get<unknown>(CurrentTunerAPI) as CurrentTunerAPIMock;

  storageMock = mockStorage(container);
  storageMock.stored[CURRENT_TUNER_WARNING_STORAGE_KEY] = false;

  jest.clearAllMocks();

  inputSettings = [
    { name: 'motor.resistance', value: VALID_MOTOR_RESISTANCE, units: Resistance.OHMS },
    { name: 'motor.inductance', value: VALID_MOTOR_INDUCTANCE, units: Inductance.H },
    { name: 'motor.ke', value: 103 },
  ];
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;

  axes = null!;
  axisCallbacks = [];
  currentTunerAPIMock = null!;
  storageMock = null!;
});

async function applyTuning() {
  expect(wrapper.getByText('Apply')).not.toHaveAttribute('disabled');
  fireEvent.click(wrapper.getByText('Apply'));
  expect(wrapper.getByText('Apply')).toHaveAttribute('disabled');

  await waitUntilPass(() =>
    expect(wrapper.getByTitle('Back EMF Compensation')).not.toHaveAttribute('disabled')
  );
}

describe('unused axis', () => {
  beforeEach(() => {
    wrapper = render(<TestCurrentTuner/>);
    const store = TestCurrentTuner.testStore;
    mockSingleDeviceWithPeripherals(store, {
      peripheralCount: 1,
      modifier: device => {
        device.axes[0].identity.peripheralId = UNUSED_AXIS_PERIPHERAL_ID;
        return device;
      },
    });
  });

  test('displays error', async () => {
    const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getByText(/This axis is unused/));
  });
});

describe('warning modal', () => {
  beforeEach(() => {
    storageMock.stored[CURRENT_TUNER_WARNING_STORAGE_KEY] = true;
    wrapper = render(<TestCurrentTuner/>);

    const store = TestCurrentTuner.testStore;
    mockSingleDeviceWithPeripherals(store);
  });

  afterEach(async () => {
    await waitUntilPass(() => {
      expect(wrapper.queryByText(/Loading/)).toBeNull();
      wrapper.getByText(/Tuning Controls/);
      expect(wrapper.getByText(/Defaults/)).not.toHaveAttribute('disabled');
    });
  });

  test('shows warning modal when axis is selected', async () => {
    const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getByText(/Warning/));
    fireEvent.click(wrapper.getByText(/OK/));
  });

  test('does not show warning modal again after "OK" is clicked', async () => {
    const axes = _.values(selectAxes(TestCurrentTuner.testStore.getState()));
    connectionViewMockInstance.props.onSelect(axes[0].key);
    await waitUntilPass(() => wrapper.getByText(/Warning/));

    fireEvent.click(wrapper.getByText(/OK/));

    connectionViewMockInstance.props.onSelect(axes[1].key);
    expect(wrapper.queryByText(/Warning/)).toBeNull();
  });

  test('updates localStorage when hide warning checkbox is checked and "OK" is clicked', async () => {
    const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getByText(/Warning/));

    fireEvent.click(wrapper.getByLabelText(/Do not show this warning again./));
    fireEvent.click(wrapper.getByText(/OK/));
    await waitUntilPass(() => expect(storageMock.save).toBeCalledWith(CURRENT_TUNER_WARNING_STORAGE_KEY, false));
  });
});

describe('single linear peripheral device', () => {
  beforeEach(() => {
    wrapper = render(<TestCurrentTuner/>);
    const store = TestCurrentTuner.testStore;
    mockSingleDeviceWithPeripherals(store);
  });

  test('displays error if the axis is not stepper motor peripheral', async () => {
    axisCallbacks.push(axis => axis._settings['motor.type'] = MotorTypes.DirectDriveMotor);

    const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getByText(/Unsupported motor type/));
  });

  test('displays error if the axis does not have the right settings', async () => {
    axisCallbacks.push(axis => axis.settings.get.mockRejectedValue(
      new CommandFailedException('BADCOMMAND', { responseData: 'BADCOMMAND' } as CommandFailedExceptionData)));

    const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getByText(/Unsupported product/));
  });

  test('displays error if something goes wrong', async () => {
    axisCallbacks.push(axis => axis.settings.get.mockRejectedValue(new Error('Strange error')));

    const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getByText(/Strange error/));
  });

  test('applies suggested tuning if the settings are not compatible, also disables the reset, defaults and apply button', async () => {
    currentTunerAPIMock.getSliderPositions.mockResolvedValue(null);

    const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getByText(/Incompatible Tuning/));

    fireEvent.click(wrapper.getAllByText(/Accept/)[1]);

    await waitUntilPass(() =>
      expect(wrapper.getByTitle('Back EMF Compensation')).not.toHaveAttribute('disabled')
    );
    expect(currentTunerAPIMock.getCurrentControllerParams).toHaveBeenLastCalledWith(
      expect.anything(),
      expect.anything(),
      defaultSliderPositions,
    );
    expect(wrapper.getByText('Apply')).toHaveAttribute('disabled');
    expect(wrapper.getByText('Restore Defaults')).toHaveAttribute('disabled');
    expect(wrapper.getByText('Reset')).toHaveAttribute('disabled');
  });

  describe('peripheral axis selected', () => {
    beforeEach(async () => {
      const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
      connectionViewMockInstance.props.onSelect(axis.key);
      await waitUntilPass(() => wrapper.getByText(/Tuning Controls/));
    });

    test('disables Apply and Reset buttons before any changes', () => {
      expect(wrapper.getByText('Apply')).toHaveAttribute('disabled');
      expect(wrapper.getByText('Reset')).toHaveAttribute('disabled');
    });

    test('calls info with correct arguments', () => {
      expect(currentTunerAPIMock.getInfo).toHaveBeenLastCalledWith(
        firmwareVersion,
      );
    });

    test('calls get current controller parameters with correct arguments', () => {
      expect(currentTunerAPIMock.getCurrentControllerParams).toHaveBeenLastCalledWith(
        expect.objectContaining(inputSettings),
        firmwareVersion,
        expect.objectContaining({ backEMFCompensation: 0.1, overallGain: 0.3, responseCorrectionSpeed: 0.2 }),
      );
    });

    test(`allows to change tuning, updates target values in real time, and
      shows confirmation message when tuning is successfully applied`, async () => {
      fireEvent.change(wrapper.getByTitle('Back EMF Compensation'), { target: { value: 0.6 } });
      fireEvent.change(wrapper.getByTitle('Overall Gain'), { target: { value: 0.7 } });
      fireEvent.change(wrapper.getByTitle('Response Correction Speed'), { target: { value: 0.8 } });

      await waitUntilPass(() =>
        expect(currentTunerAPIMock.getCurrentControllerParams).toHaveBeenLastCalledWith(
          expect.objectContaining(inputSettings),
          firmwareVersion,
          expect.objectContaining({ backEMFCompensation: 0.6, overallGain: 0.7, responseCorrectionSpeed: 0.8 }))
      );

      await applyTuning();
      expect(wrapper.getByText('Apply')).toHaveAttribute('disabled');

      expect(axes[0]._settings).toMatchObject({
        'ictrl.pi.kp': 1.23,
        'ictrl.pi.ki': 1.24,
        'ictrl.afcff.ki': 1.25,
      });
      expect(currentTunerAPIMock.getCurrentControllerParams).toHaveBeenLastCalledWith(
        expect.objectContaining(inputSettings),
        firmwareVersion,
        expect.objectContaining({ backEMFCompensation: 0.6, overallGain: 0.7, responseCorrectionSpeed: 0.8 }),
      );
      expect(wrapper.getByText(/Your changes have been applied/));
    });

    test('Defaults button sets sliders to suggested tuning', async () => {
      fireEvent.click(wrapper.getByText('Restore Defaults'));
      expect(wrapper.getByText('Restore Defaults')).toHaveAttribute('disabled');

      await waitUntilPass(() =>
        expect(wrapper.getByTitle('Back EMF Compensation')).not.toHaveAttribute('disabled')
      );

      expect(wrapper.getByText('Reset')).not.toHaveAttribute('disabled');
      await waitUntilPass(() => {
        wrapper.getByText(defaultSliderPositions.backEMFCompensation);
        wrapper.getByText(defaultSliderPositions.overallGain);
        wrapper.getByText(defaultSliderPositions.responseCorrectionSpeed);
      });
    });

    test('Reset button resets sliders to last applied values', async () => {
      expect(wrapper.getByText('Reset')).toHaveAttribute('disabled');
      fireEvent.change(wrapper.getByTitle('Back EMF Compensation'), { target: { value: 0.4 } });
      fireEvent.change(wrapper.getByTitle('Overall Gain'), { target: { value: 0.4 } });
      fireEvent.change(wrapper.getByTitle('Response Correction Speed'), { target: { value: 0.4 } });
      await waitUntilPass(() => expect(wrapper.getByText('Reset')).not.toHaveAttribute('disabled'));
      fireEvent.click(wrapper.getByText('Reset'));

      await waitUntilPass(() => {
        wrapper.getByText(/0.1/);
        wrapper.getByText(/0.2/);
        wrapper.getByText(/0.3/);
      });
    });

    test('shows tunning values', async () => {
      fireEvent.click(wrapper.getByTitle(/Expand Tuning Settings/));
      wrapper.getByText('ictrl.pi.kp');
      wrapper.getByText('21.111');
      wrapper.getByText('ictrl.pi.ki');
      wrapper.getByText('22.222');
      wrapper.getByText('ictrl.afcff.ki');
      wrapper.getByText('23.333');
    });

    describe('failures', () => {
      beforeEach(() => {
        axes[0].settings.set.mockRejectedValue(new CommandFailedException('BADDATA', null!));
      });

      test('shows error if tuning cannot be applied', async () => {
        fireEvent.change(wrapper.getByTitle('Back EMF Compensation'), { target: { value: 0 } });
        await applyTuning();

        wrapper.getByText(/BADDATA/);
      });

      test('reverts access level in case of an error', async () => {
        fireEvent.change(wrapper.getByTitle('Back EMF Compensation'), { target: { value: 0 } });
        await applyTuning();

        const device = axes[0].device as DeviceMock;
        expect(device.settings.set).toHaveBeenCalledWith('system.access', 2);
        expect(device.settings.set).toHaveBeenCalledWith('system.access', 1);
        expect(device._settings['system.access']).toBe(1);
      });
    });

    test('reloads axis when remounted', async () => {
      expect(currentTunerAPIMock.getInfo).toHaveBeenCalledTimes(1);

      const store = TestCurrentTuner.testStore;
      wrapper.unmount();
      wrapper = render(<TestCurrentTuner store={store}/>);

      await waitUntilPass(() => wrapper.getByText(/Tuning Controls/));
      expect(currentTunerAPIMock.getInfo).toHaveBeenCalledTimes(2);
    });

    describe('driver', () => {
      test('disables driver during applying', async () => {
        fireEvent.change(wrapper.getByTitle('Back EMF Compensation'), { target: { value: 0 } });
        await applyTuning();

        expect(axes[0].driverDisable).toHaveBeenCalled();
        expect(axes[0].driverEnable).toHaveBeenCalled();
      });

      test('keeps driver disabled if it was already', async () => {
        axes[0]._flags.push(ascii.WarningFlags.DRIVER_DISABLED_NO_FAULT);
        fireEvent.change(wrapper.getByTitle('Back EMF Compensation'), { target: { value: 0 } });
        await applyTuning();

        expect(axes[0].driverEnable).not.toHaveBeenCalled();
      });

      test('clicking disable/enable driver button calls correct command', async () => {
        const driverButton = wrapper.getByText(/Disable Driver/);
        fireEvent.click(driverButton);
        expect(driverButton).toHaveAttribute('disabled');
        await waitUntilPass(() => {
          expect(axes[0].driverDisable).toHaveBeenCalled();
          wrapper.getByText(/Enable Driver/);
          expect(driverButton).not.toHaveAttribute('disabled');
        });
        fireEvent.click(driverButton);
        expect(driverButton).toHaveAttribute('disabled');
        await waitUntilPass(() => {
          expect(axes[0].driverEnable).toHaveBeenCalled();
          wrapper.getByText(/Disable Driver/);
          expect(driverButton).not.toHaveAttribute('disabled');
        });
      });

      test('shows error when driver cannot be enabled/disabled', async () => {
        axes[0].driverDisable.mockRejectedValueOnce(new Error('Cannot disable driver'));

        const driverButton = wrapper.getByText(/Disable Driver/);
        fireEvent.click(driverButton);
        await waitUntilPass(() => wrapper.getByText(/Cannot disable driver/));

        fireEvent.click(wrapper.getByText('Try Again'));
        await waitUntilPass(() => {
          wrapper.getByText(/Enable Driver/);
          expect(wrapper.queryByText(/Cannot disable driver/)).toBeNull();
        });
      });
    });
  });
});

test('shows multi axis integrated device not supported message', async () => {
  wrapper = render(<TestCurrentTuner/>);
  const store = TestCurrentTuner.testStore;
  mockSingleDevice(store, undefined, undefined, 2);
  const device = _.sample(selectDevices(TestCurrentTuner.testStore.getState()))!;
  connectionViewMockInstance.props.onSelect(device.key);
  await waitUntilPass(() => expect(wrapper.getByText(/Multi-axis device is not supported/)));
});

describe('3rd party', () => {
  beforeEach(() => {
    wrapper = render(<TestCurrentTuner/>);
    const store = TestCurrentTuner.testStore;
    mockSingleDeviceWithPeripherals(store, {
      peripheralCount: 1,
      type: 'thirdParty',
    });
    axisCallbacks.push(axis => {
      const axisState = _.sample(selectAxes(store.getState()))!;
      axis.peripheralId = axisState.identity!.peripheralId;
    });
  });

  test('shows special dialog with additional information', async () => {
    const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getByText(/The selected Non-Zaber product has been set/));
    wrapper.getByText('102000 mH');
    wrapper.getByText('101 Ω');
    wrapper.getByText('456 A');

    fireEvent.click(wrapper.getByText('OK'));
    wrapper.getByText(/Tuning Controls/);
  });

  test('shows error when 3rd party device does not seem to be setup', async () => {
    axisCallbacks.push(axis => {
      axis.storage.getBool.mockRejectedValue(new NoValueForKeyException('not found'));
    });

    const axis = _.sample(selectAxes(TestCurrentTuner.testStore.getState()))!;
    connectionViewMockInstance.props.onSelect(axis.key);
    await waitUntilPass(() => wrapper.getByText(/has not been fully set up/));
  });
});
