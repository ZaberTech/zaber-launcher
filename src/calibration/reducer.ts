import { ConnectionManagerActionPayloads, ConnectionManagerActionTypes, DevicesLoadedPayload } from '../connection_manager';
import { createReducer } from '../utils';
import { extractConnectionKey, type EntityKey } from '../keys';
import { environment } from '../environment';

import { ActionsToPayloads, ActionTypes } from './actions';
import { toEditedTable, type EditedCalibrationTable, CalibrationTable } from './types';

export interface State {
  selected: EntityKey | null;
  table: { state: 'loading' } | { state: 'loaded'; table: CalibrationTable } | { state: 'error'; error: string } | null;
  editedTable: EditedCalibrationTable;
  apply: { state: 'applying' } | { state: 'error'; error: string } | null;
  fileErr: string | null;
  /** store everything as factory data */
  factoryData: boolean;
}

const initialState: State = {
  selected: null,
  table: null,
  editedTable: {
    calibrationType: 0,
    width: 0,
    offset: 0,
    values: '',
  },
  apply: null,
  fileErr: null,
  factoryData: environment.isProduction || environment.isTest,
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const selectDeviceOrAxis: Reducer<ActionTypes.SELECT_DEVICE_OR_AXIS> = (state, { selectedKey }) => ({
  ...initialState,
  selected: selectedKey,
});

const loadTable: Reducer<ActionTypes.LOAD_TABLE> = state => ({
  ...state,
  table: { state: 'loading' },
});

const tableLoaded: Reducer<ActionTypes.TABLE_LOADED> = (state, { table }) =>
  state.table?.state === 'loading' ? ({
    ...state,
    table: { state: 'loaded', table },
    editedTable: toEditedTable(table),
  }) : state;

const tableErr: Reducer<ActionTypes.TABLE_ERR> = (state, { error }) =>
  state.table?.state === 'loading' ? ({
    ...state,
    table: { state: 'error', error },
  }) : state;

const fileOperation: Reducer<ActionTypes.SAVE_TO_FILE> = state => ({
  ...state,
  fileErr: null,
});

const fileErr: Reducer<ActionTypes.FILE_ERR> = (state, { error }) => ({
  ...state,
  fileErr: error,
});

const clearFileErr: Reducer<ActionTypes.CLEAR_FILE_ERR> = state => ({
  ...state,
  fileErr: null,
});

const updateTable: Reducer<ActionTypes.UPDATE_TABLE> = (state, { table }) => ({
  ...state,
  editedTable: { ...state.editedTable, ...table },
});

const apply: Reducer<ActionTypes.APPLY> = state => ({
  ...state,
  apply: { state: 'applying' },
});

const applyDone: Reducer<ActionTypes.APPLY_DONE> = (state, { table }) => ({
  ...state,
  apply: null,
  table: state.table?.state === 'loaded' ? { state: 'loaded', table } : state.table,
});

const applyErr: Reducer<ActionTypes.APPLY_ERR> = (state, { error }) => ({
  ...state,
  apply: { state: 'error', error },
});

const applyDelete: Reducer<ActionTypes.APPLY_DELETE> = state => ({
  ...state,
  apply: { state: 'applying' },
});

const applyDeleteDone: Reducer<ActionTypes.APPLY_DELETE> = state => ({
  ...state,
  apply: null,
  table: null,
});

const clearApplyError: Reducer<ActionTypes.CLEAR_APPLY_ERROR> = state => ({
  ...state,
  apply: state.apply?.state === 'error' ? null : state.apply,
});

const setFactoryData: Reducer<ActionTypes.SET_FACTORY_DATA> = (state, { factoryData }) => ({
  ...state,
  factoryData,
});

const devicesLoaded = (state: State, { connectionKey }: DevicesLoadedPayload): State => {
  if (state.selected && extractConnectionKey(state.selected) === connectionKey) {
    return initialState;
  }
  return state;
};

export const reducer = createReducer<ActionsToPayloads & ConnectionManagerActionPayloads, typeof initialState>({
  [ConnectionManagerActionTypes.DEVICES_LOADED]: devicesLoaded,
  [ActionTypes.SELECT_DEVICE_OR_AXIS]: selectDeviceOrAxis,
  [ActionTypes.LOAD_TABLE]: loadTable,
  [ActionTypes.TABLE_LOADED]: tableLoaded,
  [ActionTypes.TABLE_ERR]: tableErr,
  [ActionTypes.LOAD_FROM_FILE]: fileOperation,
  [ActionTypes.SAVE_TO_FILE]: fileOperation,
  [ActionTypes.FILE_ERR]: fileErr,
  [ActionTypes.CLEAR_FILE_ERR]: clearFileErr,
  [ActionTypes.UPDATE_TABLE]: updateTable,
  [ActionTypes.APPLY]: apply,
  [ActionTypes.APPLY_DONE]: applyDone,
  [ActionTypes.APPLY_ERR]: applyErr,
  [ActionTypes.APPLY_DELETE]: applyDelete,
  [ActionTypes.APPLY_DELETE_DONE]: applyDeleteDone,
  [ActionTypes.CLEAR_APPLY_ERROR]: clearApplyError,
  [ActionTypes.SET_FACTORY_DATA]: setFactoryData,
}, initialState);
