/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable camelcase */
jest.mock('../environment', () => {
  const actual = jest.requireActual('../environment');
  return ({
    ...actual,
    environment: {
      ...actual.environment,
      edition: 'public',
    },
  });
});

import EventEmitter from 'events';

import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';
import { ascii, CommandFailedException, ConnectionFailedException, NoDeviceFoundException, NotSupportedException } from '@zaber/motion';

const createMdns = jest.fn(() => new MdnsMock());
class MdnsMock extends EventEmitter {
  query = jest.fn();
  destroy = jest.fn();
}
jest.mock('multicast-dns', () => createMdns);

import { createContainer, destroyContainer } from '../container';
import { waitUntilPass, wrapWithNewStore } from '../test';
import { MessageRoutersService } from '../message_router';
import { IPC } from '../ipc';
import { ZML } from '../zml';
import { ZaberApi, Connection } from '../app_components';
import type { RecursivePartial } from '../utils';
import { mockLocalConnections } from '../connection_manager/mocks';
import { mockIpc, IpcMock } from '../ipc/mock';
import { Environment, environment } from '../environment';

import { AddConnection } from './AddConnection';
import { LIST_PORT_LOOP_DELAY } from './sagas';

const TestAddConnection = wrapWithNewStore(AddConnection);

let container: Container;
let wrapper: RenderResult;

@injectable()
class ZaberApiMock {
  getDeviceInfo = jest.fn<Promise<RecursivePartial<ZaberApi.DeviceInfo>>, [unknown, unknown, number[]]>().mockImplementation(
    (deviceId, fw, peripherals) => Promise.resolve({
      device: {
        product_id: 0,
      },
      capabilities: [],
      command_tree: { nodes: [] },
      conversion_table: { rows: [] },
      settings: { rows: [] },
      peripherals: (peripherals || []).map(() => ({
        device: {
          product_id: 0,
        },
        capabilities: [],
        command_tree: { nodes: [] },
        conversion_table: { rows: [] },
        settings: { rows: [] },
      }))
    })
  );
  getWebsiteImageForProducts = jest.fn<Promise<ZaberApi.WebsiteProductImage[]>, []>().mockImplementation(() => Promise.resolve([{
    id: 0,
    thumbnail: '/ufs/thumbnail-image.html',
    image: '/ufs/full-size-image.html',
  }]));
}

class RouterConnectionMock {
  api = {
    addTcpIp: jest.fn(async (): Promise<Partial<Connection>> => ({ id: 'tcp_id' })),
    addSerialPort: jest.fn(async (): Promise<Partial<Connection>> => ({ id: 'serial_id' })),
  };
}

let routerService: MessageRoutersServiceMock;

@injectable()
class MessageRoutersServiceMock {
  routerMock = new RouterConnectionMock();

  constructor() {
    routerService = this;
  }

  async getConnection(): Promise<RouterConnectionMock> {
    return this.routerMock;
  }
}

class NoDeviceConnectionMock {
  detectDevices = jest.fn().mockRejectedValue(new NoDeviceFoundException('No devices'));
  close = jest.fn().mockResolvedValue(undefined);
}

class DeviceMock {
  deviceAddress = 1;
  axisCount = 2;
  identify = jest.fn<Promise<ascii.DeviceIdentity>, []>().mockResolvedValue({
    deviceId: 1234,
    axisCount: 2,
    firmwareVersion: { major: 6, minor: 14, build: 3 },
    name: 'X-MCB2',
    serialNumber: 5678,
    isModified: false,
    isIntegrated: false,
  });
  getAxis = jest.fn<{ identity: ascii.AxisIdentity }, [number]>(axisNumber => ({
    axisNumber,
    identity: {
      axisType: ascii.AxisType.LINEAR,
      isPeripheral: true,
      peripheralId: 1235,
      peripheralName: `X-PER${axisNumber}`,
      peripheralSerialNumber: 72671,
      isModified: false,
    },
    settings: {
      canConvertNativeUnits: () => true,
    },
  }));
  _settings: Record<string, number> = {
    'system.platform': 987343,
  };
  settings = {
    get: jest.fn(async name => {
      if (name in this._settings) { return this._settings[name] }
      throw new CommandFailedException('Failed', null!);
    }),
  };
  genericCommand = jest.fn<Promise<{data: string}>, [string]>(async (cmd: string) => {
    switch (cmd) {
      case 'get peripheral.serial': return { data: '72671 NA' };
      default: throw new Error(`Not mocked ${cmd}`);
    }
  });
}

class ConnectionMock {
  detectDevices = jest.fn(async () => [new DeviceMock()]);
  forgetDevices = jest.fn().mockResolvedValue(undefined);
  close = jest.fn().mockResolvedValue(undefined);
}

class OldASCIIConnectionMock {
  detectDevices = jest.fn().mockRejectedValue(new NotSupportedException('No devices'));
  close = jest.fn().mockResolvedValue(undefined);
}

class BinaryDeviceConnectionMock {
  detectDevices = jest.fn().mockResolvedValue(['binary device']);
  close = jest.fn().mockResolvedValue(undefined);
}

let zmlMock: ZmlMock;
@injectable()
class ZmlMock {
  constructor() {
    zmlMock = this;
  }

  ConnectionCtor: typeof NoDeviceConnectionMock | typeof ConnectionMock | typeof OldASCIIConnectionMock = NoDeviceConnectionMock;
  BinaryConnectionCtor: typeof NoDeviceConnectionMock | typeof BinaryDeviceConnectionMock = NoDeviceConnectionMock;

  listPorts = jest.fn().mockResolvedValue([]);
  openTcp = jest.fn(async () => new this.ConnectionCtor());
  openSerialPort = jest.fn(async () => new this.ConnectionCtor());
  openBinaryTcp = jest.fn(async () => new this.BinaryConnectionCtor());
  openBinarySerialPort = jest.fn(async () => new this.BinaryConnectionCtor());
}

beforeEach(() => {
  (environment as Environment).platform = 'win32';
  container = createContainer();
  mockIpc(container);
  container.bind<unknown>(ZaberApi).to(ZaberApiMock);
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
  container.get<unknown>(MessageRoutersService);
  container.bind<unknown>(ZML).to(ZmlMock);
  container.get<unknown>(ZML);

  wrapper = render(<TestAddConnection/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;
  routerService = null!;
  zmlMock = null!;
});

describe('select connection type', () => {
  beforeEach(() => {
    fireEvent.click(wrapper.getByText(/Add New Connection/));
  });

  test('requires user to pick connection type', () => {
    expect(wrapper.getByText(/Next/)).toBeDisabled();
    fireEvent.click(wrapper.getByLabelText(/Serial Port/));
    expect(wrapper.getByText(/Next/)).not.toBeDisabled();
  });
});

test('On MacOS hide `cu` and bluetooth ports if only showing recommended ports', async () => {
  (environment as Environment).platform = 'darwin';
  fireEvent.click(wrapper.getByText(/Add New Connection/));
  fireEvent.click(wrapper.getByLabelText(/Serial Port/));

  zmlMock.listPorts.mockResolvedValue([
    '/dev/cu.BLTH', '/dev/cu.Bluetooth-Incoming-Port', '/dev/cu.usbserial-AB1CDE23',
    '/dev/tty.BLTH', '/dev/tty.Bluetooth-Incoming-Port', '/dev/tty.usbserial-AB1CDE23',
  ]);
  fireEvent.click(wrapper.getByText(/Next/));
  await waitUntilPass(() => wrapper.getByTitle(/Select Port/));

  wrapper.getByText('/dev/tty.usbserial-AB1CDE23');
  expect(wrapper.queryByText('/dev/cu.usbserial-AB1CDE23')).toBeNull();
  expect(wrapper.queryByText('/dev/tty.Bluetooth-Incoming-Port')).toBeNull();
  expect(wrapper.queryByText('/dev/tty.BLTH')).toBeNull();

  fireEvent.click(wrapper.getByLabelText('Show Only Recommended Serial Ports'));
  wrapper.getByText('/dev/tty.usbserial-AB1CDE23');
  wrapper.getByText('/dev/cu.usbserial-AB1CDE23');
  wrapper.getByText('/dev/tty.Bluetooth-Incoming-Port');
  wrapper.getByText('/dev/tty.BLTH');
});

describe('serial port select', () => {
  beforeEach(async () => {
    jest.useFakeTimers();

    fireEvent.click(wrapper.getByText(/Add New Connection/));
    fireEvent.click(wrapper.getByLabelText(/Serial Port/));

    zmlMock.listPorts.mockResolvedValue(['COM3', 'COM5']);
    fireEvent.click(wrapper.getByText(/Next/));
    await waitUntilPass(() => wrapper.getByText(/Select the port/));
  });

  test('preselects port when new port appears', async () => {
    zmlMock.listPorts.mockResolvedValue(['COM3', 'COM8']);
    jest.advanceTimersByTime(LIST_PORT_LOOP_DELAY);

    await waitUntilPass(() =>
      expect(wrapper.getByTitle(/Select Port/)).toHaveAttribute('data-current', 'COM8')
    );
  });

  test('reacts to serial port changes', async () => {
    wrapper.getByText('COM3');
    wrapper.getByText('COM5');

    zmlMock.listPorts.mockResolvedValue(['COM5', 'COM6']);
    jest.advanceTimersByTime(LIST_PORT_LOOP_DELAY);

    await waitUntilPass(() => wrapper.getByText('COM6'));
    wrapper.getByText('COM5');
    expect(wrapper.queryByText('COM3')).toBeNull();
  });

  test('deselects port if it disappears', async () => {
    fireEvent.change(wrapper.getByTitle(/Select Port/), { target: { value: 'COM5' } });
    expect(wrapper.getByTitle(/Select Port/)).toHaveAttribute('data-current', 'COM5');

    zmlMock.listPorts.mockResolvedValue([]);
    jest.advanceTimersByTime(LIST_PORT_LOOP_DELAY);
    await waitUntilPass(() =>
      expect(wrapper.getByTitle(/Select Port/)).toHaveAttribute('data-current', '')
    );
  });

  test('calls open connection with correct parameters', async () => {
    expect(wrapper.getByText(/Next/)).toBeDisabled();

    fireEvent.change(wrapper.getByTitle(/Select Port/), { target: { value: 'COM5' } });

    expect(wrapper.getByText(/Next/)).not.toBeDisabled();

    fireEvent.change(wrapper.getByLabelText(/Baud Rate/), { target: { value: '9600' } });

    fireEvent.click(wrapper.getByText(/Next/));

    await waitUntilPass(() =>
      expect(zmlMock.openSerialPort).toHaveBeenCalledWith('COM5', 9600)
    );
  });

  test('back button returns to connection selection', () => {
    fireEvent.click(wrapper.getByText(/Back/));

    wrapper.getByText(/Choose the type of connection/);
  });

  test('shows already added ports', async () => {
    mockLocalConnections(TestAddConnection.testStore, {
      COM3: () => [],
    });
    wrapper.getByText('COM3 (Already added)');
    wrapper.getByText('COM5');
  });
});

describe('configure TCP', () => {
  beforeEach(async () => {
    fireEvent.click(wrapper.getByText(/Add New Connection/));
    fireEvent.click(wrapper.getByLabelText(/TCP\/IP/));

    fireEvent.click(wrapper.getByText(/Next/));
    fireEvent.click(wrapper.getByText(/Connect Manually/));
  });

  test('calls open connection with correct parameters', async () => {
    expect(wrapper.getByText(/Next/)).toBeDisabled();

    fireEvent.change(wrapper.getByLabelText(/Hostname/), { target: { value: 'localhost' } });

    expect(wrapper.getByText(/Next/)).not.toBeDisabled();

    fireEvent.change(wrapper.getByLabelText(/Port/), { target: { value: '25654' } });

    fireEvent.click(wrapper.getByText(/Next/));

    await waitUntilPass(() =>
      expect(zmlMock.openTcp).toHaveBeenCalledWith('localhost', 25654)
    );
  });

  test('back buttons returns to mdns discovery, then connection selection', () => {
    fireEvent.click(wrapper.getByText(/Back/));
    wrapper.getByText(/Found Devices/);

    fireEvent.click(wrapper.getByText(/Back/));
    wrapper.getByText(/Choose the type of connection/);
  });

  test('closing and opening dialog resets the progress', () => {
    fireEvent.change(wrapper.getByLabelText(/Hostname/), { target: { value: 'localhost' } });

    fireEvent.click(wrapper.getByTitle(/Close Dialog/));

    fireEvent.click(wrapper.getByText(/Add New Connection/));
    fireEvent.click(wrapper.getByLabelText(/TCP\/IP/));

    fireEvent.click(wrapper.getByText(/Next/));
    fireEvent.click(wrapper.getByText(/Connect Manually/));
    expect(wrapper.getByLabelText(/Hostname/)).toHaveValue('');
  });
});

describe('detect devices', () => {
  beforeEach(async () => {
    fireEvent.click(wrapper.getByText(/Add New Connection/));
    fireEvent.click(wrapper.getByLabelText(/Serial Port/));

    zmlMock.listPorts.mockResolvedValue(['COM3', 'COM5']);
    fireEvent.click(wrapper.getByText(/Next/));

    await waitUntilPass(() => wrapper.getByText(/Select the port/));
    fireEvent.change(wrapper.getByTitle(/Select Port/), { target: { value: 'COM5' } });
  });

  const nextStep = () => fireEvent.click(wrapper.getByText(/Next/));

  test('displays "no device" error when devices cannot be found', async () => {
    nextStep();
    await waitUntilPass(() => wrapper.getByText(/No Device Detected/));
  });

  test('displays error when connection cannot be opened and allows to try again', async () => {
    zmlMock.openSerialPort.mockRejectedValueOnce(new ConnectionFailedException('Serial port is busy'));
    nextStep();
    await waitUntilPass(() => wrapper.getByText(/Serial port is busy/));

    fireEvent.click(wrapper.getByText(/Try Again/));
    await waitUntilPass(() => wrapper.getByText(/No Device Detected/));
  });

  test('displays error on generic error', async () => {
    zmlMock.openSerialPort.mockRejectedValueOnce(new Error('Serial port was ripped out of PC'));
    nextStep();
    await waitUntilPass(() => wrapper.getByText(/ripped out/));
  });


  test('displays devices when detected', async () => {
    zmlMock.ConnectionCtor = ConnectionMock;
    nextStep();
    await waitUntilPass(() => wrapper.getByText(/X-MCB2/));
    wrapper.getByText(/X-PER1/);
    wrapper.getByText(/X-PER2/);
  });

  test('warnings for ASCII device with firmware version before 6.14', async () => {
    zmlMock.ConnectionCtor = OldASCIIConnectionMock;
    nextStep();
    await waitUntilPass(() => wrapper.getByText(/Unsupported Device/));
  });

  test('warnings for binary devices', async () => {
    zmlMock.BinaryConnectionCtor = BinaryDeviceConnectionMock;
    nextStep();
    await waitUntilPass(() => wrapper.getByText(/Binary Device Detected/));
  });

  test('back button returns to serial port configuration', async () => {
    nextStep();
    await waitUntilPass(() => wrapper.getByText(/No Device Detected/));

    fireEvent.click(wrapper.getByText(/Back/));
    await waitUntilPass(() => wrapper.getByText(/Select the port/));
  });
});

describe('add to router', () => {
  beforeEach(async () => {
    fireEvent.click(wrapper.getByText(/Add New Connection/));
    fireEvent.click(wrapper.getByLabelText(/Serial Port/));

    zmlMock.listPorts.mockResolvedValue(['COM3', 'COM5']);
    fireEvent.click(wrapper.getByText(/Next/));

    await waitUntilPass(() => wrapper.getByText(/Select the port/));
    fireEvent.change(wrapper.getByTitle(/Select Port/), { target: { value: 'COM5' } });

    zmlMock.ConnectionCtor = ConnectionMock;
    fireEvent.click(wrapper.getByText(/Next/));
    await waitUntilPass(() => wrapper.getByText(/X-MCB2/));
  });

  const nextStep = () => fireEvent.click(wrapper.getByText(/Create Connection/));

  test('calls router API to add connection, broadcast actions', async () => {
    nextStep();

    await waitUntilPass(() => expect(wrapper.queryByText(/Create Connection/)).toBeNull());
    expect(routerService.routerMock.api.addSerialPort).toHaveBeenCalled();
    expect(routerService.routerMock.api.addSerialPort.mock.calls[0]).toMatchObject([{ serialPort: 'COM5' }]);

    const ipc = container.get<unknown>(IPC) as IpcMock;
    expect(ipc.broadcastAction).toHaveBeenCalled();
  });

  test('displays error when connection cannot be added', async () => {
    routerService.routerMock.api.addSerialPort.mockRejectedValueOnce(new Error('It is broken'));
    nextStep();

    await waitUntilPass(() => wrapper.getByText(/It is broken/));
  });
});

describe('add to router (TCP)', () => {
  test('adding TCP connection calls the API', async () => {
    fireEvent.click(wrapper.getByText(/Add New Connection/));
    fireEvent.click(wrapper.getByLabelText(/TCP\/IP/));
    fireEvent.click(wrapper.getByText(/Next/));
    fireEvent.click(wrapper.getByText(/Connect Manually/));
    fireEvent.change(wrapper.getByLabelText(/Hostname/), { target: { value: 'darkstar' } });
    zmlMock.ConnectionCtor = ConnectionMock;
    fireEvent.click(wrapper.getByText(/Next/));
    await waitUntilPass(() => wrapper.getByText(/X-MCB2/));
    fireEvent.click(wrapper.getByText(/Create Connection/));
    await waitUntilPass(() => expect(routerService.routerMock.api.addTcpIp).toHaveBeenCalled());
    expect(routerService.routerMock.api.addTcpIp.mock.calls[0]).toMatchObject([{ hostname: 'darkstar' }]);
  });

  test('TCP connection warning with old ASCII device', async () => {
    fireEvent.click(wrapper.getByText(/Add New Connection/));
    fireEvent.click(wrapper.getByLabelText(/TCP\/IP/));
    fireEvent.click(wrapper.getByText(/Next/));
    fireEvent.click(wrapper.getByText(/Connect Manually/));
    fireEvent.change(wrapper.getByLabelText(/Hostname/), { target: { value: 'darkstar' } });
    zmlMock.ConnectionCtor = OldASCIIConnectionMock;
    fireEvent.click(wrapper.getByText(/Next/));
    await waitUntilPass(() => wrapper.getByText(/Unsupported Device/));
  });
});

describe(('Little Device Sim Notice'), () => {
  test('does not display notice for public version', async () => {
    fireEvent.click(wrapper.getByText(/Add New Connection/));
    expect(wrapper.queryAllByText(/Little Device Simulator/)).toHaveLength(0);
  });
});
