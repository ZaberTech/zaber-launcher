import type { Initializer, SectionDataDefinition } from '../types';

import { motion } from './motion';
import { motorCharacteristics } from './motorCharacteristics';
import { limitAway } from './limitAway';
import { limitC } from './limitC';
import { limitHome } from './limitHome';
import { encoder1 } from './encoder1';
import { encoder2 } from './encoder2';

type SectionFieldMap = {
  motion: 'axisType' | 'isCyclic' | 'cyclicDist' | 'isInfinite' | 'limitMin' | 'limitMax' | 'unitConversionEnabled' | 'unitConversion';
  motorCharacteristics:
    'stepsPerRotation' | 'rpm' | 'ratedCurrent' | 'runFactor' | 'holdFactor' |
    'driverDirection' | 'motorInductance' | 'motorResistance' | 'holdingTorque' | 'ictrlType';
  encoder1: 'motorType' | 'mode' | 'type' | 'dir' | 'fault' | 'indexMode' | 'filter' | 'mult' | 'div' | 'phase' | 'powerUpDelay';
  encoder2:
    'motorType' | 'mode' | 'type' | 'dir' | 'fault' |
    'indexMode' | 'filter' | 'mult' | 'div' | 'interpolation' | 'minSignal' |
    'cosDC' | 'cosGain' | 'sinDC' | 'sinGain' | 'powerUpDelay';
  limitAway: 'type' | 'source' | 'edge' | 'width' | 'action' | 'preset' | 'posUpdate';
  limitC : 'type' | 'source' | 'edge' | 'width' | 'action' | 'preset' | 'posUpdate';
  limitHome: 'type' | 'source' | 'edge' | 'width' | 'bidirectional' | 'action' | 'preset' | 'posUpdate';
};

export type SectionIds = keyof SectionFieldMap;

export type FieldIds<S extends SectionIds> = SectionFieldMap[S];

export const dataDefinitionTemplate: {[S in SectionIds]: SectionDataDefinition<S>} = {
  motion,
  motorCharacteristics,
  encoder1,
  encoder2,
  limitAway,
  limitC,
  limitHome,
};

export const allSectionIds = Object.keys(dataDefinitionTemplate) as SectionIds[];

export function allFieldInitializers<S extends SectionIds>(sectionId: S) {
  return Object.entries(dataDefinitionTemplate[sectionId].initializers) as [FieldIds<S>, Initializer][];
}
