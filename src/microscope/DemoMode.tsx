import React, { useRef } from 'react';

import { useKeyboard } from '../components';
import { useActions } from '../utils';

import { actions as actionsDefinition } from './actions';

const PASSWORD = 'demodemo';

export const DemoMode: React.FC  = () => {
  const lastTyped = useRef('');

  const actions = useActions(actionsDefinition);

  useKeyboard(e => {
    lastTyped.current = (lastTyped.current + e.key).slice(-PASSWORD.length);
    if (lastTyped.current === PASSWORD) {
      actions.setDemoMode(true);
    }
  });

  return null;
};
