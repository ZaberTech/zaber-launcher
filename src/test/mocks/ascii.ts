import { injectable } from 'inversify';

import { MocksDictionary } from '../dictionary';

export class AxisMockBase {
  constructor(readonly axisNumber: number, readonly device: DeviceMockBase<AxisMockBase>) {
  }

  getDevice<T extends DeviceMockBase<any>>(): T {
    return this.device as T;
  }
}

export class DeviceMockBase<TAxis extends AxisMockBase> {
  constructor(readonly deviceAddress: number, readonly connection: ConnectionMockBase<TAxis, DeviceMockBase<TAxis>>) {
  }

  identity?: unknown = {};
  isIdentified = true;

  axes = new MocksDictionary<TAxis>(id => new this.connection.routerConnection.service.AxisCtor!(id as number, this));
  getAxis = jest.fn(axis => this.axes.get(axis));

  getConnection<T extends ConnectionMockBase<any, any>>(): T {
    return this.connection as T;
  }
}

export class ConnectionMockBase<TAxis extends AxisMockBase, TDevice extends DeviceMockBase<TAxis>> {
  constructor(readonly id: string, readonly routerConnection: RouterConnectionMockBase<any, any, any>) {
  }

  devices = new MocksDictionary<TDevice>(id => new this.routerConnection.service.DeviceCtor!(id as number, this));
  getDevice = jest.fn(address => this.devices.get(address));

  getRouterConnection<T extends RouterConnectionMockBase<any, any, any>>(): T {
    return this.routerConnection as T;
  }
}

export class RouterConnectionMockBase<
  TAxis extends AxisMockBase,
  TDevice extends DeviceMockBase<TAxis>,
  TConnection extends ConnectionMockBase<TAxis, TDevice>
> {
  constructor(readonly url: string, readonly service: MessageRoutersServiceMockBase<any, any, any, any>) {}

  connections = new MocksDictionary<TConnection>(id => new this.service.ConnectionCtor!(id as string, this));
  getConnection = jest.fn(async id => this.connections.get(id));

  getService<T extends MessageRoutersServiceMockBase<any, any, any, any>>(): T {
    return this.service as T;
  }
}

@injectable()
export class MessageRoutersServiceMockBase<
  TAxis extends AxisMockBase,
  TDevice extends DeviceMockBase<TAxis>,
  TConnection extends ConnectionMockBase<TAxis, TDevice>,
  TRouterConnection extends RouterConnectionMockBase<TAxis, TDevice, TConnection>,
> {
  RouterConnectionCtor?: new (
    url: string,
    service: MessageRoutersServiceMockBase<TAxis, TDevice, TConnection, TRouterConnection>
  ) => TRouterConnection;
  ConnectionCtor?: new (id: string, routerConnection: TRouterConnection) => TConnection;
  DeviceCtor?: new (id: number, connection: TConnection) => TDevice;
  AxisCtor?: new (id: number, device: TDevice) => TAxis;

  routerConnections = new MocksDictionary<TRouterConnection>(url => new this.RouterConnectionCtor!(url as string, this));
  getConnection = jest.fn(async url => this.routerConnections.get(url));

  getAsciiConnection = jest.fn(async (routerUrl: string, connectionId: string) => this.getMockConnection(routerUrl, connectionId));
  getRoutedConnection = jest.fn(async (routerUrl: string, connectionId: string) => this.getMockConnection(routerUrl, connectionId));

  getMockConnection = jest.fn(async (routerUrl: string, connectionId: string) => {
    // Depending on how TConnection is setup, this function can return a mock connection object
    // that either represents a RoutedConnection or an AsciiConnection. If the code being tested
    // makes use of getAsciiConnection then TConnection should be setup to represent a
    // AsciiConnection mock implementation. If the code uses getRoutedConnection then TConnection
    // should represent a RoutedConnection mock implementation.
    const router = await this.getConnection(routerUrl);
    const connection = await router.getConnection(connectionId);
    return connection;
  });
}
