import _ from 'lodash';

import { createReducer } from '../../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import {
  defaultMultiDAcqConfig,
  DoneReport,
  MultiDAcqConfig,
  ProgressReport,
} from './types';

export type AcquisitionState = {
  state: 'ready' | 'started' | 'cancelling';
} | {
  state: 'error';
  error: string;
} | {
  state: 'inProgress';
  progress: ProgressReport;
} | {
  state: 'done';
  report: DoneReport;
};

export interface State {
  config: MultiDAcqConfig;
  acquisition: AcquisitionState;
}

const initialState: State = {
  config: defaultMultiDAcqConfig,
  acquisition: { state: 'ready' },
};

type Reducer<AT extends ActionTypes> = (state: State, payload: ActionsToPayloads[AT]) => State;

const updateConfig: Reducer<ActionTypes.UPDATE_CONFIG> = (state, { config }) => ({
  ...state,
  config: _.mergeWith({}, state.config, config, (objValue, srcValue) => {
    if (_.isArray(objValue)) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return srcValue;
    }
    return undefined;
  }),
});

const start: Reducer<ActionTypes.START> = state => ({
  ...state,
  acquisition: {
    state: 'started',
  },
});

const progress: Reducer<ActionTypes.PROGRESS> = (state, { progress }) => ({
  ...state,
  acquisition: state.acquisition.state === 'started' || state.acquisition.state === 'inProgress' ? {
    state: 'inProgress',
    progress,
  } : state.acquisition,
});

const error: Reducer<ActionTypes.ERROR> = (state, { error }) => ({
  ...state,
  acquisition: {
    state: 'error',
    error,
  },
});

const end: Reducer<ActionTypes.END> = (state, { report }) => ({
  ...state,
  acquisition: {
    state: 'done',
    report,
  },
});

const cancel: Reducer<ActionTypes.CANCEL> = state => ({
  ...state,
  acquisition: state.acquisition.state === 'started' || state.acquisition.state === 'inProgress' ? {
    state: 'cancelling',
  } : state.acquisition,
});

const reset: Reducer<ActionTypes.RESET> = state => ({
  ...state,
  acquisition: initialState.acquisition,
});

export const reducer = createReducer<
  ActionsToPayloads, typeof initialState
>({
  [ActionTypes.UPDATE_CONFIG]: updateConfig,
  [ActionTypes.START]: start,
  [ActionTypes.PROGRESS]: progress,
  [ActionTypes.END]: end,
  [ActionTypes.ERROR]: error,
  [ActionTypes.CANCEL]: cancel,
  [ActionTypes.RESET]: reset,
}, initialState);
