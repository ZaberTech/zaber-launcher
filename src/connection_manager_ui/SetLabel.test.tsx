/* eslint-disable @typescript-eslint/no-this-alias */
import React from 'react';
import { Container, injectable } from 'inversify';
import { RenderResult, render, fireEvent } from '@testing-library/react';

import { createContainer, destroyContainer } from '../container';
import { wrapWithNewStore, waitUntilPass } from '../test';
import { connectionManagerActions } from '../connection_manager';
import { LOCAL_ROUTER_URL, MessageRoutersService } from '../message_router';
import {
  mockSingleDeviceWithPeripherals, MOCK_DEVICE_NUMBER, MOCK_SERIAL_PORT,
} from '../connection_manager/mocks';
import {
  AxisMockBase, ConnectionMockBase, DeviceMockBase, MessageRoutersServiceMockBase, RouterConnectionMockBase,
} from '../test/mocks/ascii';
import { makeAxisKey, makeConnectionKey, makeDeviceKey, makeRouterKey } from '../keys';

import { SetLabel, SetLabelContextMenuItem } from './SetLabel';

let container: Container;

let axes: AxisMock[] = [];
class AxisMock extends AxisMockBase {
  constructor(id: number, device: DeviceMock) {
    super(id, device);
    axes.push(this);
  }

  setLabel = jest.fn().mockResolvedValue(undefined);
}

let device: DeviceMock;
let deviceCallback: (device: DeviceMock) => void;
class DeviceMock extends DeviceMockBase<AxisMock> {
  constructor(address: number, connection: ConnectionMock) {
    super(address, connection);
    device = this;
    if (deviceCallback) { deviceCallback(this) }
  }

  axisCount = 4;
  setLabel = jest.fn().mockResolvedValue(undefined);
}

class ConnectionMock extends ConnectionMockBase<AxisMock, DeviceMock> {
}
class RouterConnectionMock extends RouterConnectionMockBase<AxisMock, DeviceMock, ConnectionMock> {
}

@injectable()
class MessageRoutersServiceMock extends MessageRoutersServiceMockBase<AxisMock, DeviceMock, ConnectionMock, RouterConnectionMock> {
  AxisCtor = AxisMock;
  DeviceCtor = DeviceMock;
  ConnectionCtor = ConnectionMock;
  RouterConnectionCtor = RouterConnectionMock;
}

const connectionKey = makeConnectionKey(makeRouterKey(LOCAL_ROUTER_URL), MOCK_SERIAL_PORT);
const deviceKey = makeDeviceKey(connectionKey, MOCK_DEVICE_NUMBER);
const axisKey = makeAxisKey(deviceKey, 1);

const TestSetLabel = wrapWithNewStore(({ entityKey }: { entityKey: string }) => <>
  <SetLabelContextMenuItem entityKey={entityKey} label="old"/>
  <SetLabel/>
</>);

const loadDevicesActionMock = jest.fn(() => {
  setTimeout(() => mockSingleDeviceWithPeripherals(TestSetLabel.testStore ?? TestSetLabel.testStore));
  return { type: 'NOTHING', payload: undefined };
});
connectionManagerActions.loadDevices = loadDevicesActionMock;

let wrapper: RenderResult;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(MessageRoutersService).to(MessageRoutersServiceMock);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null!;

  destroyContainer();
  container = null!;

  device = null!;
  axes = [];
  deviceCallback = null!;

  loadDevicesActionMock.mockClear();
});

function getSetLabelButton() {
  return wrapper.getAllByText('Set Label')[2];
}

function getCancelButton() {
  return wrapper.getByText('Cancel');
}

describe('device', () => {
  beforeEach(() => {
    wrapper = render(<TestSetLabel entityKey={deviceKey}/>);
    mockSingleDeviceWithPeripherals(TestSetLabel.testStore, {
      modifier: device => {
        device.product.commandTree.nodes?.push({
          command: 'storage',
        });
        return device;
      },
    });
  });

  test('Sets the label', async () => {
    fireEvent.click(wrapper.getByText(/Set Label/));

    fireEvent.input(wrapper.getByTitle('Enter Label'), { target: { value: 'new label' } });

    const setLabel = getSetLabelButton();
    fireEvent.click(setLabel);
    expect(setLabel).toBeDisabled();
    expect(getSetLabelButton()).toBeDisabled();
    expect(getCancelButton()).toBeDisabled();
    expect(wrapper.getByTitle('Loading')).toBeInTheDocument();

    await waitUntilPass(() =>
      expect(wrapper.queryByRole('textbox')).toBeNull());

    expect(device.setLabel).toHaveBeenCalledWith('new label');
    expect(loadDevicesActionMock).toHaveBeenCalled();
  });

  test('Clears the label', async () => {
    fireEvent.click(wrapper.getByText(/Set Label/));

    fireEvent.input(wrapper.getByTitle('Enter Label'), { target: { value: 'new label' } });

    fireEvent.click(wrapper.getAllByRole('button')[2]);

    await waitUntilPass(() => {
      expect(wrapper.getByTitle('Enter Label')).toHaveValue('');
    });
  });

  test('handles error', async () => {
    fireEvent.click(wrapper.getByText(/Set Label/));
    fireEvent.input(wrapper.getByTitle('Enter Label'), { target: { value: 'new label' } });

    deviceCallback = device => device.setLabel.mockRejectedValueOnce(new Error('Something went wrong'));

    fireEvent.click(getSetLabelButton());
    await waitUntilPass(() => wrapper.getByText('Something went wrong'));
  });

  test('disables button when characters are not valid', () => {
    fireEvent.click(wrapper.getByText(/Set Label/));

    fireEvent.input(wrapper.getByRole('textbox'), {
      target: {
        value: 'very very very very very very very long label',
      },
    });
    expect(getSetLabelButton()).toBeDisabled();
  });
});

describe('no support', () => {
  beforeEach(() => {
    wrapper = render(<TestSetLabel entityKey={deviceKey}/>);
    mockSingleDeviceWithPeripherals(TestSetLabel.testStore);
  });

  test('displays message', () => {
    fireEvent.click(wrapper.getByText(/Set Label/));

    wrapper.getByText(/Setting the label is not supported by your device./);

    expect(getSetLabelButton()).toBeDisabled();
  });
});

describe('axis', () => {
  beforeEach(() => {
    wrapper = render(<TestSetLabel entityKey={axisKey}/>);
    mockSingleDeviceWithPeripherals(TestSetLabel.testStore, {
      modifier: device => {
        device.product.commandTree.nodes?.push({
          command: 'storage',
        });
        return device;
      },
    });
  });

  test('Sets the label', async () => {
    fireEvent.click(wrapper.getByText(/Set Label/));
    fireEvent.input(wrapper.getByTitle('Enter Label'), { target: { value: 'new label' } });
    fireEvent.click(getSetLabelButton());
    await waitUntilPass(() => expect(axes[0].setLabel).toHaveBeenCalledWith('new label'));
  });
});
