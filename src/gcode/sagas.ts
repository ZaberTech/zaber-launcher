import { all, call, delay, put, SagaReturnType as ASR, takeEvery, select, fork } from '@redux-saga/core/effects';
import {
  ascii, CommandFailedException, gcode, GCodeExecutionException, GCodeSyntaxException, Measurement, Units, Velocity,
} from '@zaber/motion';
import { throwUnexpectedError } from '@zaber/toolbox';
import type { SagaIterator } from 'redux-saga';
import _ from 'lodash';

import type { Action } from '../utils';
import { getDevice } from '../connection_manager';
import { isMobile } from '../devices';
import { getContainer } from '../container';
import { Storage } from '../app_components';
import { TimeMeasuring } from '../time';

import { actions, ActionsToPayloads, ActionTypes } from './actions';
import { AxisSettings, BlockMessage, defaultVelocityUnits, GCODE_AXES, isPrimaryAxis, sortAxes } from './types';
import { selectCurrentDevice, selectSettings, selectSettingsDevice } from './selectors';
import { translatorSaga } from './translator';

export function* rootSaga(): SagaIterator {
  yield all([
    fork(translatorSaga),
    takeEvery(ActionTypes.EVALUATE, evaluate),
    takeEvery(ActionTypes.SELECT_DEVICE, selectDevice),
    takeEvery(ActionTypes.STORE_CONTENT, storeContent),
    takeEvery(ActionTypes.MOUNT, mount),
  ]);
}

export const CONTENT_STORAGE_KEY = 'gcode_content';

export const RENDER_PERIOD = 250;
export const REPORT_PROGRESS_AFTER = 500;

function* evaluate({ payload: { code } }: Action<ActionsToPayloads[ActionTypes.EVALUATE]>): SagaIterator {
  const settings: ReturnType<typeof selectSettings> = yield select(selectSettings);
  const device: ReturnType<typeof selectCurrentDevice> = yield select(selectCurrentDevice);
  if (settings == null || device == null) {
    return;
  }

  yield delay(1); // give away time to re-render

  const primaryAxes = sortAxes(settings.axes.filter(isPrimaryAxis));

  let blockIndex = 0;
  const warnings: BlockMessage[] = [];
  try {
    const asciiDevice: ASR<typeof getDevice> = yield call(getDevice, device.key);

    const axisNumbers  = primaryAxes.map(axis => axis.axisNumber);
    const translator: gcode.OfflineTranslator = yield call(gcode.OfflineTranslator.setupFromDevice, asciiDevice, axisNumbers, {
      axisMappings: primaryAxes.map((axis, axisIndex) => ({
        axisIndex,
        axisLetter: axis.letter!,
      }))
    });
    translator.setTraverseRate(settings.traverseRate.value, settings.traverseRate.unit as Velocity);

    for (const axis of primaryAxes) {
      translator.setAxisPosition(axis.letter!, 0, Units.NATIVE);
    }

    const timeMeasuring = getContainer().get(TimeMeasuring);
    let lastRender = timeMeasuring.now();
    const started = timeMeasuring.now();
    let reportProgress = false;

    for (; blockIndex < code.length; blockIndex++) {
      const line = code[blockIndex];
      const result = translator.translate(line);

      warnings.push(...result.warnings.map<BlockMessage>(w => ({ ...w, blockIndex, blockIndexEnd: blockIndex })));

      const progress = (blockIndex + 1) / code.length;
      const now = timeMeasuring.now();

      if (!reportProgress && now - started > REPORT_PROGRESS_AFTER && progress < 0.5) {
        reportProgress = true;
      }

      if (now - lastRender > RENDER_PERIOD) {
        lastRender = now;
        if (reportProgress) {
          yield put(actions.evaluateProgress(progress));
        }
        yield delay(10);
      }
    }

    yield put(actions.evaluateDone(warnings));
  } catch (err) {
    throwUnexpectedError(err);

    let error: BlockMessage;
    if (err instanceof GCodeExecutionException || err instanceof GCodeSyntaxException) {
      error = { ...err.details, blockIndex, blockIndexEnd: blockIndex, message: err.message };
    } else {
      error = {
        blockIndex,
        blockIndexEnd: blockIndex,
        message: err.message ?? String(err),
        fromBlock: -1,
        toBlock: -1,
      };
    }
    yield put(actions.evaluateDone(warnings, error));
  }
}

function* selectDevice(): SagaIterator {
  try {
    const device: ReturnType<typeof selectSettingsDevice> = yield select(selectSettingsDevice);
    if (device == null) {
      throw new Error('No device');
    }

    const asciiDevice: ASR<typeof getDevice> = yield call(getDevice, device.key);

    let traverseRate: Measurement | undefined;
    const axes: AxisSettings[] = [];

    for (let i = 0; i < device.axes.length; ++i) {
      const axis = device.axes[i];
      const asciiAxis = asciiDevice.getAxis(i + 1);
      const { axisType } = asciiAxis.identity;
      if (!isMobile(axis)) {
        axes.push({
          axisNumber: i + 1,
          primaryAxisNumber: i + 1,
          mobile: false,
          letter: null,
          microstepResolution: 0,
          peripheralId: asciiAxis.identity.peripheralId,
          lockstep: null,
          inverted: false,
          axisType,
          maxSpeed: { value: 0, unit: Units.NATIVE },
        });
        continue;
      }

      let microstepResolution = 1;
      try {
        microstepResolution = yield call([asciiAxis.settings, asciiAxis.settings.get], ascii.SettingConstants.RESOLUTION);
      } catch (err) {
        if (!(err instanceof CommandFailedException)) {
          throw err;
        }
      }

      const unit = defaultVelocityUnits(axisType);
      const maxSpeed: number = yield call([asciiAxis.settings, asciiAxis.settings.get], ascii.SettingConstants.MAXSPEED, unit);

      axes.push({
        axisNumber: i + 1,
        primaryAxisNumber: i + 1,
        mobile: true,
        letter: null,
        microstepResolution,
        peripheralId: asciiAxis.identity.peripheralId,
        lockstep: null,
        inverted: false,
        axisType,
        maxSpeed: { value: maxSpeed, unit },
      });

      if (traverseRate == null || (traverseRate.unit === unit && traverseRate.value > maxSpeed)) {
        traverseRate = { value: maxSpeed, unit };
      }
    }

    if (traverseRate) {
      traverseRate.value = _.round(traverseRate.value);
    }

    for (const lockstep of device.locksteps) {
      for (const axisNumber of lockstep.axisNumbers) {
        const axis = axes[axisNumber - 1];
        axis.primaryAxisNumber = lockstep.axisNumbers[0];
        axis.lockstep = lockstep;
      }
    }

    let letterIndex = 0;
    for (const axis of axes.filter(axis => axis.mobile && axis.axisNumber === axis.primaryAxisNumber)) {
      axis.letter = GCODE_AXES[letterIndex++];
    }
    for (const axis of axes.filter(axis => axis.axisNumber !== axis.primaryAxisNumber)) {
      axis.letter = axes[axis.primaryAxisNumber - 1].letter;
    }

    yield put(actions.updateSettings({
      axes,
      traverseRate,
    }));
    yield put(actions.loadingDeviceDone());
  } catch (err) {
    throwUnexpectedError(err);
    yield put(actions.loadingDeviceDone(err.message ?? String(err)));
  }
}

function* mount(): SagaIterator {
  const storage = getContainer().get(Storage);
  let content = storage.load(CONTENT_STORAGE_KEY, '');
  if (!content) {
    content = '; Paste your G-Code here.';
  }
  yield put(actions.loadContent(content));
}

function storeContent({ payload: { content } }: Action<ActionsToPayloads[ActionTypes.STORE_CONTENT]>) {
  const storage = getContainer().get(Storage);
  storage.save(CONTENT_STORAGE_KEY, content);
}
