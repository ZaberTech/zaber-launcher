import { tryAccess } from '@zaber/toolbox';
import { createSelector } from 'reselect';

import {
  IdentifiedAxisState, IdentifiedDeviceState, ConnectionManagerConnectionState,
  selectIdentifiedAxes, selectIdentifiedDevices, selectConnections,
} from '../connection_manager';
import { extractConnectionKey } from '../keys';
import { selectLockstepConfiguration } from '../store';

export interface DeviceStateLockstep extends Omit<IdentifiedDeviceState, 'axes'> {
  axes: IdentifiedAxisState[];
  connection: ConnectionManagerConnectionState;
}

export const selectDeviceKey = createSelector(selectLockstepConfiguration, state => state.selectedDeviceKey);

export const selectDeviceStateLockstep = createSelector(
  selectDeviceKey, selectIdentifiedDevices, selectIdentifiedAxes, selectConnections,
  (deviceKey, devices, axes, connections) => {
    const device = tryAccess(devices, deviceKey);
    if (!device) {
      return null;
    }

    const deviceStateLockstep: DeviceStateLockstep = {
      ...device,
      axes: device.axes.map(axisKey => axes[axisKey]),
      connection: connections[extractConnectionKey(device.key)]
    };
    return deviceStateLockstep;
  });

export const selectLockstepData = createSelector(selectLockstepConfiguration, state => state.lockstepData);

export const selectError = createSelector(selectLockstepConfiguration, state => state.error);

export const selectCreationModal = createSelector(selectLockstepConfiguration, state => state.creationModalState);

export const selectLockstepHierarchy = createSelector(selectDeviceStateLockstep, device => {
  if (device == null) { return {} }
  const lockstepEntries = device.locksteps.map((lockstep): [number, number[]] => [lockstep.groupNumber, lockstep.axisNumbers]);
  return Object.fromEntries(lockstepEntries);
});

interface SortedAxes {
  lockstepped: IdentifiedAxisState[];
  free: IdentifiedAxisState[];
}
/** Returns the axes of this device sorted into free and lockstepped axes */
export const selectAxesSorted = createSelector(selectDeviceStateLockstep, (device): SortedAxes => {
  if (device == null) {
    return { lockstepped: [], free: [] };
  }
  const locksteppedAxisNumbers = device.locksteps.map(group => (group.axisNumbers)).flat();
  return device.axes.reduce<SortedAxes>((sorted, axis) => {
    if (locksteppedAxisNumbers.includes(axis.axisNumber)) {
      return { ...sorted, lockstepped: [...sorted.lockstepped, axis] };
    } else {
      return { ...sorted, free: [...sorted.free, axis] };
    }
  }, { lockstepped: [], free: [] });
});
