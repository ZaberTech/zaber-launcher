import React from 'react';
import { useSelector } from 'react-redux';
import { Button, ButtonRow, Checkbox, HeaderCard, HelpTooltip, Icons, Modal, PopUp, RadioButton, Text } from '@zaber/react-library';
import { Units, Angle, type AngularVelocity, type Length, type Time, type Velocity } from '@zaber/motion';

import { UnitSelect, derivativeUnit, getDimensionName } from '../units';
import type { Props as UnitSelectProps } from '../units/UnitSelect';
import { useActions } from '../utils';
import { useKeyboard } from '../components';

import { actions as actionDefinitions } from './actions';
import { selectCsvImportOptions, selectViewMode } from './selectors';
import { ViewMode } from './types';

export const IMPORT_BUTTON_LABEL = 'Import';
export const ACCUMULATED_TIME_LABEL = 'Accumulated Time';
export const SEGMENT_TIME_LABEL = 'Segment Time';


const unitSelectStyleOverride: UnitSelectProps['styles'] = {
  control: base => ({
    ...base,
    width: 'auto',
  })
};

export const CsvImportDialog: React.FC = () => {
  const actions = useActions(actionDefinitions);
  const options = useSelector(selectCsvImportOptions);
  const viewMode = useSelector(selectViewMode);

  let canProceed = true;
  let proceedToolTip: string | null = null;
  if (options.selectedPositionUnit === Units.NATIVE || options.selectedVelocityUnit === Units.NATIVE) {
    canProceed = false;
    proceedToolTip = 'You must select units of measure; native device units are not supported.';
  } else if (getDimensionName(derivativeUnit(options.selectedPositionUnit)) !== getDimensionName(options.selectedVelocityUnit)) {
    canProceed = false;
    proceedToolTip = 'You must select compatible units of measure; either both linear or both rotary.';
  }
  const isAngular = getDimensionName(options.detectedPositionUnit) === getDimensionName(Angle.DEGREES);

  function proceed() {
    actions.setCsvImportOptions({ dialogVisible: false });
    actions.continueImport();
    if (viewMode === ViewMode.Welcome) {
      actions.setViewMode(ViewMode.TimeSeries);
    }
  }

  function cancel() {
    actions.setCsvImportOptions({ dialogVisible: false });
    actions.cancelImport();
  }

  useKeyboard((event: KeyboardEvent) => {
    if (event.key === 'Enter') {
      proceed();
    } else if (event.key === 'Escape') {
      cancel();
    }
  });

  if (!options.dialogVisible) {
    return null;
  }

  return <Modal className="display-settings-dialog"
    headerIcon={<Icons.OpenFile/>}
    headerText="Import CSV File"
    isOpen
    buttons={<ButtonRow>
      <PopUp position="top"
        align="center"
        triggerAction={canProceed ? 'none' : 'hover'}
        trigger={() => <Button
          disabled={!canProceed}
          onClick={proceed}>{IMPORT_BUTTON_LABEL}</Button>}>
        {proceedToolTip}
      </PopUp>
      <Button color="grey" onClick={cancel}>Cancel</Button>
    </ButtonRow>}
    onRequestClose={cancel}
  >
    <HeaderCard header="Display Settings">
      <div className="display-settings">
        <Text e={Text.Emphasis.Bold}>Time</Text>
        <RadioButton name="absolute-time"
          data-testid="segment-time-mode"
          checked={!options.absoluteTime}
          onValueChange={() => actions.setCsvImportOptions({ absoluteTime: false })}>
          {SEGMENT_TIME_LABEL}
        </RadioButton>
        <HelpTooltip>Select this if the time values in the spreadsheet are each relative to the previous point.</HelpTooltip>

        <div/>
        <RadioButton name="absolute-time"
          data-testid="accumulated-time-mode"
          disabled={!options.allowAbsoluteTime}
          checked={options.absoluteTime}
          onValueChange={() => actions.setCsvImportOptions({ absoluteTime: true })}>
          {ACCUMULATED_TIME_LABEL}
        </RadioButton>
        {options.allowAbsoluteTime &&
          <HelpTooltip>Select this if the time values in the spreadsheet are all relative to the start.</HelpTooltip>}
        {!options.allowAbsoluteTime &&
          <HelpTooltip>Select this if the time values in the spreadsheet are all relative to the start.<br/>
            This option is disabled because the time values in the specified file are not in ascending sequence.
          </HelpTooltip>}

        <div/>
        <UnitSelect dimensions="Time"
          data-testid="time-unit-select"
          includeNative={false}
          value={options.selectedTimeUnit}
          disabled={options.detectedTimeUnit != null}
          onChange={units => actions.setCsvImportOptions({ selectedTimeUnit: units as Time })}
          styles={unitSelectStyleOverride}
          portalToBody/>
        {options.detectedTimeUnit == null &&
          <HelpTooltip>Time units are not specified in the selected file; please set them here.</HelpTooltip>}
        {options.detectedTimeUnit != null &&
          <HelpTooltip>Time units specified in the selected file will be used.</HelpTooltip>}

        <div>&nbsp;</div><div/><div/>

        <Text e={Text.Emphasis.Bold}>Position</Text>
        <UnitSelect dimensions={getDimensionName(options.detectedPositionUnit) ?? ['Length', 'Angle']}
          title={isAngular ? 'Angle Unit Select' : 'Length Unit Select'}
          includeNative={false}
          value={options.selectedPositionUnit}
          disabled={options.detectedPositionUnit !== Units.NATIVE}
          onChange={units => actions.setCsvImportOptions({ selectedPositionUnit: units as Length | Angle })}
          styles={unitSelectStyleOverride}
          portalToBody/>
        {options.detectedPositionUnit === Units.NATIVE &&
          <HelpTooltip>Position units are not specified in the selected file; please set them here.</HelpTooltip>}
        {options.detectedPositionUnit !== Units.NATIVE &&
          <HelpTooltip>Position units specified in the selected file will be used.</HelpTooltip>}

        <div>&nbsp;</div><div/><div/>

        <Text e={Text.Emphasis.Bold}>Velocity</Text>
        <UnitSelect dimensions={getDimensionName(options.detectedVelocityUnit) ?? ['Velocity', 'Angular Velocity']}
          title="Velocity Unit Select"
          includeNative={false}
          value={options.selectedVelocityUnit}
          disabled={options.detectedVelocityUnit !== Units.NATIVE}
          onChange={units => actions.setCsvImportOptions({ selectedVelocityUnit: units as Velocity | AngularVelocity })}
          styles={unitSelectStyleOverride}
          portalToBody/>
        {options.detectedVelocityUnit === Units.NATIVE &&
          <HelpTooltip>Velocity units are not specified in the selected file; please set them here.</HelpTooltip>}
        {options.detectedVelocityUnit !== Units.NATIVE &&
          <HelpTooltip>Velocity units specified in the selected file will be used.</HelpTooltip>}
      </div>

      <div className="other-settings">
        <Checkbox checked={options.autoReload}
          labelContent="Auto Reload"
          onChecked={checked => actions.setCsvImportOptions({ autoReload: checked })}/>
        <Text t={Text.Type.Helper}>
          Edits made to the import file will automatically be reflected in the PVT Viewer app.
        </Text>
      </div>
    </HeaderCard>
  </Modal>;
};
