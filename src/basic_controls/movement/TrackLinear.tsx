import React, { Component, createRef } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import { match } from 'ts-pattern';
import { Flex } from '@zaber/react-library';

import { ValueDisplay } from '../../units';
import type { MotionInfo } from '../types';

import type { AxisMovementState } from './types';

export interface Props {
  motion: MotionInfo;
  axis: AxisMovementState;
  /** The position in native units */
  position: number | null;
  deviceIsMoving?: boolean;
  onRequestMovement: (positionNative: number) => void;
}

export interface State {
  /** The fraction from 0-1 of the total range of movement that is being hovered over */
  hover: number | null;
}

type MouseOrTouch = React.MouseEvent<HTMLDivElement> | React.TouchEvent<HTMLDivElement>;

function getClientPosition(e: MouseOrTouch) {
  if ('changedTouches' in e) {
    return { x: e.changedTouches[0].clientX, y: e.changedTouches[0].clientY };
  } else {
    return { x: e.clientX, y: e.clientY };
  }
}

export class TrackLinear extends Component<Props, State> {
  interactiveRef: React.RefObject<HTMLDivElement>;
  indicatorRef: React.RefObject<HTMLDivElement>;

  constructor(props: Props) {
    super(props);
    this.state = { hover: null };

    this.interactiveRef = createRef();
    this.indicatorRef = createRef();
  }

  move() {
    const positionFraction = this.state.hover;
    if (positionFraction == null) {
      return;
    }
    const { motion: axis, onRequestMovement } = this.props;
    const positionNative = _.round(axis.min + axis.travel * positionFraction);
    onRequestMovement(positionNative);
  }

  interactiveBoundingBox() {
    return this.interactiveRef.current?.getBoundingClientRect();
  }

  indicatorBoundingBox() {
    return this.indicatorRef.current?.getBoundingClientRect();
  }

  /** Returns the fraction along the x-axis the given event is through a bounding box, or null if is outside the box */
  positionWithinRef(e: MouseOrTouch, box?: DOMRect): number | null {
    if (!box) {
      return null;
    }
    const { x, y } = getClientPosition(e);
    if (x < box.left || x > box.right || y < box.top || y > box.bottom) {
      return null;
    }
    return (x - box.left) / box.width;
  }

  hoverState(e: MouseOrTouch): State['hover'] {
    const positionWithinInteractive = this.positionWithinRef(e, this.interactiveBoundingBox());
    if (positionWithinInteractive == null) {
      return null;
    }
    const positionWithinIndicator = this.positionWithinRef(e, this.indicatorBoundingBox());
    if (positionWithinIndicator != null) {
      return positionWithinIndicator;
    } else {
      return positionWithinInteractive < 0.5 ? 0 : 1;
    }
  }

  onHover = (e: MouseOrTouch) => {
    const hover = this.hoverState(e);
    this.setState({ hover });
  };

  hoverEnd = () => {
    this.setState({ hover: null });
  };

  onMouseUp = (e: React.MouseEvent<HTMLDivElement>) => {
    if (e.button !== 0) {
      return;
    }
    this.move();
  };

  onTouchEnd = (e: React.TouchEvent<HTMLDivElement>) => {
    this.move();
    this.setState({ hover: null });
    // by default, chrome simulates a mousemove after every touchend. If allowed, this will leave the controls in an activated state.
    e.preventDefault();
  };

  render(): React.ReactNode {
    const { motion, axis, position, deviceIsMoving } = this.props;
    const {
      hover,
    } = this.state;

    const moveTarget = match(axis.lastCommand)
      .with({ type: 'abs' }, move => (move.position - motion.min) / motion.travel)
      .otherwise(() => hover);

    const positionFraction = position && (position - motion.min) / motion.travel;

    return (
      <Flex.Column className="track">
        <Flex.Row
          className={classnames('interactive-line', { activated: !!deviceIsMoving || hover != null })}
          data-testid="interactive-linear-track" ref={this.interactiveRef}
          onMouseMove={this.onHover} onMouseLeave={this.hoverEnd} onMouseUp={this.onMouseUp}
          onTouchStart={this.onHover} onTouchMove={this.onHover} onTouchEnd={this.onTouchEnd}
        >
          <div className="end-stop left"/>
          <div className="track-line">
            <div className="line"/>
            <div className="indicators" ref={this.indicatorRef}>
              {positionFraction != null &&
                <div className="position-indicator" style={{ left: fractionToCss(positionFraction) }}/>}
              {hover != null && (
                <TrackHoverInfo fraction={hover}>
                  <ValueDisplay entityKey={axis.key} setting="pos" value={hover * motion.travel + motion.min} unit={axis.posUnit}/>
                </TrackHoverInfo>
              )}
              {moveTarget != null && (
                <MoveTarget fraction={moveTarget}/>
              )}
            </div>
          </div>
          <div className="end-stop right"/>
        </Flex.Row>
        <Flex.Row>
          <div className="limit-pos min"><ValueDisplay entityKey={axis.key} setting="pos" value={motion.min} unit={axis.posUnit}/></div>
          <Flex.Spacer/>
          <div className="limit-pos max"><ValueDisplay entityKey={axis.key} setting="pos" value={motion.max} unit={axis.posUnit}/></div>
        </Flex.Row>
      </Flex.Column>
    );
  }
}

const TrackHoverInfo: React.FC<{fraction: number}> = ({ fraction, children }) => (
  <div className="hover-info" style={{ left: fractionToCss(fraction) }}>
    <div className="above-track">{children}</div>
    <svg className="inside-track" viewBox="0 0 60 100">
      <path d="M30 0 v100" strokeDasharray="10,10"/>
    </svg>
  </div>
);

const MoveTarget: React.FC<{fraction: number}> = ({ fraction }) => (
  <svg className="move-target" style={{ left: fractionToCss(fraction) }} viewBox="-30 0 60 50">
    <path d="M0 0 l -30 50 l 60 0 l -30 -50"/>
  </svg>
);

function fractionToCss(fraction: number) {
  return `${(_.clamp(fraction, 0, 1) * 100).toFixed(2)}%`;
}
