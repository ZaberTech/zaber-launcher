import type { SagaIterator } from 'redux-saga';
import { all, takeEvery, put, call, race, take, cancelled } from 'redux-saga/effects';
import {
  Length,
  Angle,
  Velocity,
  AngularVelocity,
  Units,
  CommandFailedException,
  MovementInterruptedException,
  MovementFailedException,
  Time,
  Native,
  RemoteModeException
} from '@zaber/motion';

import { Action, throwUnexpectedError, RT } from '../../utils';
import { getAxis, getDevice } from '../../connection_manager';
import type { Logger } from '../../app_components';
import { getLogger } from '../../log';
import { moveableKey } from '../types';
import { extractDeviceKey, getAxisNumber } from '../../keys';
import { actionDefinitions as controlsActions } from '../actions';
import { actionDefinitions as pollingActions, ActionTypes as PollingActionTypes } from '../../polling/actions';

import { ActionTypes, ActionsToPayloads, actionDefinitions as actions } from './actions';

let logger: Logger;

export function* movementSaga(): SagaIterator {
  logger = getLogger('BasicControlsMovementSaga');

  yield all([
    takeEvery(ActionTypes.ISSUE_COMMAND, issueCommand),
  ]);
}

function* issueCommand({ payload: { moveable: moveableInfo, command } }: Action<ActionsToPayloads[ActionTypes.ISSUE_COMMAND]>) {
  const key = moveableKey(moveableInfo);
  yield race([
    call(function* () {
      while (true) {
        const newCommand: Action<ActionsToPayloads[ActionTypes.ISSUE_COMMAND]> = yield take(ActionTypes.ISSUE_COMMAND);
        if (moveableKey(newCommand.payload.moveable) === key) { return }
      }
    }),
    call(function* () {
      logger.info(`Issued command of type ${command.type} to ${key} with payload ${JSON.stringify(command)}`);

      try {
        const device: RT<typeof getDevice> = yield call(getDevice, extractDeviceKey(key));
        const axis = device.getAxis(getAxisNumber(key));
        const moveable = moveableInfo.type === 'lockstep' ? device.getLockstep(moveableInfo.group) : axis;

        yield put(controlsActions.setError(key, null));
        yield put(pollingActions.clearFlags(key));

        switch (command.type) {
          case 'home':
            yield call([moveable, moveable.home]);
            break;
          case 'stop':
            yield call([moveable, moveable.stop]);
            break;
          case 'abs':
            yield call([moveable, moveable.moveAbsolute], command.position, command.units as Length | Angle | '');
            break;
          case 'vel': {
            const velocity = command.direction === 'home' ? -command.velocity : command.velocity;
            yield call([moveable, moveable.moveVelocity], velocity, command.units as Velocity | AngularVelocity);
            break;
          }
          case 'rel': {
            const distance = command.direction === 'home' ? -command.distance : command.distance;
            yield call([moveable, moveable.moveRelative], distance, command.units as Length | Angle);
            break;
          }
          case 'cycle': {
            const velocity: Parameters<typeof moveable.moveMax>[0] =
              { velocity: command.velocity, velocityUnit: command.units as Velocity | AngularVelocity };
            if (moveableInfo.motion.type === 'linear') {
              for (; ;) {
                yield call([moveable, moveable.moveMax], velocity);
                yield call([moveable, moveable.moveMin], velocity);
              }
            } else {
              for (; ;) {
                const cycleDist = moveableInfo.motion.travel;
                yield call([moveable, moveable.moveRelative], cycleDist, Units.NATIVE, velocity);
                yield call([moveable, moveable.moveRelative], -cycleDist, Units.NATIVE, velocity);
              }
            }
          }
          case 'sin': {
            // move sin command can only start if the stage is IDLE
            yield call([moveable, moveable.stop]);
            yield call(
              [moveable, moveable.moveSin],
              command.amplitude.value,
              command.amplitude.units as Length | Angle | Native,
              command.period.value,
              command.period.units as Time | Native,
              { count: command.count, waitUntilIdle: false }
            );
            break;
          }
          case 'stop sin':
            yield call([moveable, moveable.moveSinStop]);
            break;
          case 'findrange hardstop': {
            yield call ([axis, axis.genericCommand], 'tools findrange hardstop save');
            yield call([axis, axis.waitUntilIdle]);
            // Re-fetch device limits
            yield put(controlsActions.fetchDeviceInfo(extractDeviceKey(key)));
            yield put(controlsActions.setTravelLimitModal(null));
            break;
          }
          case 'moveIndex': {
            yield call([axis, axis.genericCommand], `move index ${command.direction}`);
            yield call([axis, axis.waitUntilIdle]);
            break;
          }
        }

        yield call([moveable, moveable.waitUntilIdle]);
        const flags: RT<typeof axis.warnings.getFlags> = yield call([axis.warnings, axis.warnings.getFlags]);
        if (flags.has('NI')) {
          yield call([moveable, moveable.stop]);
          yield put(pollingActions.pollNow());
          yield take(PollingActionTypes.RECEIVE_RESULTS);
        }
      } catch (err) {
        throwUnexpectedError(err);
        if (err instanceof RemoteModeException) {
          yield put(controlsActions.setError(key, {
            headline: 'Fault - the device is in EtherCAT Control mode.',
            message: err.message,
            context: 'remote-mode',
          }));
        } else if (err instanceof MovementInterruptedException) {
          // handled by polling for flags
          return;
        } else if (err instanceof MovementFailedException) {
          const { warnings } = err.details;
          let persistentFlags: RT<RT<typeof getAxis>['warnings']['getFlags']> = new Set();
          try {
            const asciiAxis: RT<typeof getAxis> = yield call(getAxis, key);
            persistentFlags = yield call([asciiAxis.warnings, asciiAxis.warnings.getFlags]);
          } catch (err) {
            throwUnexpectedError(err);
            // ignored, all errors will be handled by polling for flags
          }
          const transientFlags = warnings.filter(flag => !persistentFlags.has(flag));
          if (transientFlags.length > 0) {
            const flagStr = transientFlags.length > 1 ?
              `Flags ${transientFlags.join(', ')}` :
              `Flag ${transientFlags[0]} `;
            yield put(controlsActions.setError(key, { headline: `Movement Failed with ${flagStr}`, message: err.message }));
          } else {
            yield put(controlsActions.setError(key, { headline: 'Movement Failed', message: err.message }));
          }
        } else if (err instanceof CommandFailedException && err.details.responseData === 'BADDATA') {
          yield put(controlsActions.setError(key, {
            headline: 'Invalid Position or Velocity',
            message: err.message,
          }));
        } else {
          yield put(controlsActions.setError(key, { headline: 'Movement Failed', message: err.message }));
        }

        yield put(controlsActions.setTravelLimitModal(null));
      } finally {
        const canceled = (yield cancelled()) as boolean;
        if (!canceled) {
          yield put(actions.commandComplete(key));
        }
      }
    }),
  ]);
}
