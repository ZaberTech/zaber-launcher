import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import type { EntityKey } from '../keys';
import { Nullable, tryAccess, useActions } from '../utils';

import { actionDefinitions } from './actions';
import type { ProductManuals } from './types';
import { selectCommandIdList, selectProductManuals, selectSettingIdList } from './selectors';

export function useSettingManual(entity: EntityKey | undefined | null, setting: string): ProductManuals['settings'][string] {
  const actions = useActions(actionDefinitions);
  const allManuals = useSelector(selectProductManuals);
  const entityManuals = tryAccess(allManuals, entity);
  const manual = entityManuals?.settings[setting];

  useEffect(() => {
    if (entity != null && manual == null) {
      actions.generateSettingManual(entity, setting);
    }
  }, [manual, entity]);

  return manual;
}

export function useWarningFlagManual(entity: EntityKey | undefined | null, flag: Nullable<string>): ProductManuals['warnings'][string] {
  const actions = useActions(actionDefinitions);
  const allManuals = useSelector(selectProductManuals);
  const entityManuals = tryAccess(allManuals, entity);
  const manual = tryAccess(entityManuals?.warnings ?? {}, flag);

  useEffect(() => {
    if (entity != null && flag != null && manual == null) {
      actions.generateWarningManual(entity, flag);
    }
  }, [manual, entity, flag]);

  return manual;
}

export function useCommandRejectedReasonManual(entity: EntityKey | undefined | null, reason: Nullable<string>)
  : ProductManuals['commandRejectedReasons'][string] {
  const actions = useActions(actionDefinitions);
  const allManuals = useSelector(selectProductManuals);
  const entityManuals = tryAccess(allManuals, entity);
  const manual = tryAccess(entityManuals?.commandRejectedReasons ?? {}, reason);

  useEffect(() => {
    if (entity != null && reason != null && manual == null) {
      actions.generateCommandRejectedReasonManual(entity, reason);
    }
  }, [manual, entity, reason]);

  return manual;
}

export function useCommandIdList(): string[] {
  const actions = useActions(actionDefinitions);
  const commandIdList = useSelector(selectCommandIdList);
  useEffect(() => {
    if (commandIdList.length === 0) {
      actions.generateCommandIdList();
    }
  }, [commandIdList.length]);

  return commandIdList;
}

export function useSettingIdList(): string[] {
  const actions = useActions(actionDefinitions);
  const settingIdList = useSelector(selectSettingIdList);
  useEffect(() => {
    if (settingIdList.length === 0) {
      actions.generateSettingIdList();
    }
  }, [settingIdList.length]);

  return settingIdList;
}
