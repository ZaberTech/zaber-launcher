import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { HeaderCard, NoticeBanner, UnorderedList } from '@zaber/react-library';
import { match } from 'ts-pattern';

import { useActions } from '../utils';
import { EntityKey } from '../keys';
import { EntityHierarchyLabel, ExternalLink, NoContentMessage } from '../components';

import { actions as actionsDefinition } from './actions';
import { MotorConfig } from './MotorConfig';
import { LengthConfig } from './LengthConfig';
import {
  selectCurrentAxisOrDevice,
  selectEstopSupported,
  selectLoading,
  selectInLockstep,
  selectSetupStep,
  selectResultError,
} from './selectors';
import { EStop } from './EStop';
import { TestConfig } from './TestConfig';
import { StageConfig } from './StageConfig';
import { LC40_TITLE, SetupStep } from './types';
import { ErrorContext } from './exception';


interface SetupProps {
  entityKey: EntityKey;
}
export const Setup: React.FC<SetupProps> = ({ entityKey }) => {
  const actions = useActions(actionsDefinition);
  const { config } = useSelector(selectCurrentAxisOrDevice)!;
  const estopSupported = useSelector(selectEstopSupported);
  const loading = useSelector(selectLoading);
  const inLockstep = useSelector(selectInLockstep);
  const setupStep = useSelector(selectSetupStep);
  const resultError = useSelector(selectResultError);

  const [validate, setValidate] = useState(false);

  useEffect(() => {
    actions.loadDeviceOrAxis();
  }, [entityKey]);

  if (loading) {
    return <NoContentMessage title={LC40_TITLE} type="working"/>;
  }

  const driverOrientationError = resultError?.errContext === ErrorContext.DRIVER_ORIENTATION && resultError.message;

  return <HeaderCard header={<EntityHierarchyLabel entityKey={entityKey}/>}>
    {match(setupStep)
      .with(SetupStep.CONFIG_ERROR, () =>
        <NoticeBanner className="top-notice-banner" type="error">
          There was an error loading the configuration from the device. Please setup the device again.
          If this error persists after setting up the device,&nbsp;
          <ExternalLink url="https://www.zaber.com/contact">contact Zaber support</ExternalLink>.
        </NoticeBanner>)
      .with(SetupStep.SAVE_WITH_TEST_STOPPED, () =>
        <NoticeBanner className="top-notice-banner" type="warning">
          The configuration was saved successfully, but the test was manually stopped. Please Save & Test again.
        </NoticeBanner>)
      .with(SetupStep.SAVE_WITH_TEST_ERROR, () =>
        <NoticeBanner className="top-notice-banner" type="warning">
          The configuration was saved successfully, but the test failed. Check the configuration and try again.
        </NoticeBanner>)
      .with(SetupStep.SAVE_WITH_TEST_COMPLETE, () =>
        <NoticeBanner className="top-notice-banner" type="success">
          Setup completed successfully
        </NoticeBanner>)
      .with(SetupStep.SAVE_WITHOUT_TEST_COMPLETE, () =>
        <NoticeBanner className="top-notice-banner" type="success">
          The configuration was saved successfully, but not tested
        </NoticeBanner>)
      .with(SetupStep.SAVE_WITHOUT_TEST_ERROR, () =>
        <NoticeBanner className="top-notice-banner" type="error">
          The configuration failed to save. Please try again.
        </NoticeBanner>)
      .otherwise(() => null)}
    {inLockstep &&
      <NoticeBanner className="top-notice-banner" type="warning">
        <UnorderedList
          header={`This peripheral is in a lockstep group and can be configured, but not tested.
          You may do one of the following:`}
        >{[
            `Physically disconnect the lockstep group and disable the lockstep group in the Lockstep app,
            then configure and test the device using this app (recommended)`,
            'Save the configuration without testing (not recommended)',
          ]}</UnorderedList>
      </NoticeBanner>
    }
    {driverOrientationError &&
      <NoticeBanner className="top-notice-banner" type="error">
        {driverOrientationError}
      </NoticeBanner>
    }

    {estopSupported && <EStop validate={validate}/>}
    <StageConfig/>
    {config.stageType &&
        <MotorConfig/>
    }
    {config.motorOrientation && <>
      <LengthConfig validate={validate}/>
      <TestConfig validate={validate} onValidate={() => setValidate(true)}/>
    </>}
  </HeaderCard>;
};
