import { actionBuilder } from '../../utils';
import type { EntityKey } from '../../keys';

export enum ActionTypes {
  STORE_LAST_KEY = 'CONNECTION_VIEW_STORE_LAST_KEY',
  SET_EXPANDED = 'CONNECTION_VIEW_SET_EXPANDED',
}

export interface ActionsToPayloads {
  [ActionTypes.STORE_LAST_KEY]: { key: EntityKey };
  [ActionTypes.SET_EXPANDED]: { key: EntityKey; expanded: boolean };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  storeLastKey: (key: EntityKey) => buildAction(ActionTypes.STORE_LAST_KEY, { key }),
  setExpanded: (key: EntityKey, expanded: boolean) => buildAction(ActionTypes.SET_EXPANDED, { key, expanded }),
};
