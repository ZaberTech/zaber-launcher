import { actionBuilder } from '../utils';

import type { PendingSetups } from './reducer';

export enum ActionTypes {
  SETUP_NEEDED = 'DEVICE_SETUP_SETUP_NEEDED',
  SETUP_NOT_NEEDED = 'DEVICE_SETUP_SETUP_NOT_NEEDED',
  PENDING_SETUPS_LOADED = 'DEVICE_SETUP_PENDING_SETUPS_LOADED',
  LOAD_PENDING_SETUPS = 'DEVICE_SETUP_LOAD_PENDING_SETUPS',
}

export interface ActionsToPayloads {
  [ActionTypes.SETUP_NEEDED]: { serialNo: number; path: string };
  [ActionTypes.SETUP_NOT_NEEDED]: { serialNo: number };
  [ActionTypes.PENDING_SETUPS_LOADED]: { setups: PendingSetups };
  [ActionTypes.LOAD_PENDING_SETUPS]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  setupNeeded: (serialNo: number, path: string) => buildAction(ActionTypes.SETUP_NEEDED, { serialNo, path }),
  setupNotNeeded: (serialNo: number) => buildAction(ActionTypes.SETUP_NOT_NEEDED, { serialNo }),
  pendingSetupsLoaded: (setups: PendingSetups) => buildAction(ActionTypes.PENDING_SETUPS_LOADED, { setups }),
  loadPendingSetups: () => buildAction(ActionTypes.LOAD_PENDING_SETUPS),
};
